package com.enovax.star.kiosk.group.mgnl.workbench;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.workbench.tree.drop.BaseDropConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;

/**
 * Created by tharaka on 3/8/16.
 */
public class GroupCategoryDropConstraint extends BaseDropConstraint {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private static final String REL_CMS_PRODUCT_UUID = "relatedCMSProductUUID";

    public GroupCategoryDropConstraint() {
        super(JcrWorkspace.ProductCategories.getNodeType());
    }

    @Override
    public boolean allowedAsChild(Item sourceItem, Item targetItem) {
        return false;
    }

    @Override
    public boolean allowedBefore(Item sourceItem, Item targetItem) {
        return allowedBeforeAfter(sourceItem, targetItem);
    }

    @Override
    public boolean allowedAfter(Item sourceItem, Item targetItem) {
        return allowedBeforeAfter(sourceItem, targetItem);
    }


    private boolean allowedBeforeAfter(Item sourceItem, Item targetItem) {
        try {
            JcrNodeAdapter e = (JcrNodeAdapter)sourceItem;
            JcrNodeAdapter target = (JcrNodeAdapter)targetItem;
            String sourceNodeType = e.applyChanges().getPrimaryNodeType().getName();
            String targetNodeType = target.applyChanges().getPrimaryNodeType().getName();


            if(REL_CMS_PRODUCT_UUID.equals(target.getNodeName())) {
                return false;
            }

            if(!sourceNodeType.equals(targetNodeType)) {
                log.debug("Could not move a node type \'{}\' before or after a node type \'{}\'", targetNodeType, sourceNodeType);
                return false;
            }

        } catch (RepositoryException var8) {
            log.warn("Could not check if moving before or after is allowed. ", var8);
        }

        return true;
    }

    @Override
    public boolean allowedToMove(Item sourceItem) {
        JcrNodeAdapter e = (JcrNodeAdapter)sourceItem;
        if(REL_CMS_PRODUCT_UUID.equals(e.getNodeName())) {
            return false;
        }
        return true;
    }
}
