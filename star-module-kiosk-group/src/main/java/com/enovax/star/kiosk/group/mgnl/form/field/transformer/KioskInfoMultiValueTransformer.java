package com.enovax.star.kiosk.group.mgnl.form.field.transformer;

import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.multi.MultiValueChildrenNodeTransformer;

import javax.inject.Inject;

/**
 * Created by tharaka on 3/8/16.
 */
public class KioskInfoMultiValueTransformer extends MultiValueChildrenNodeTransformer {

    @Inject
    public KioskInfoMultiValueTransformer(Item relatedFormItem, ConfiguredFieldDefinition definition, Class<PropertysetItem> type, I18NAuthoringSupport i18NAuthoringSupport) {
        super(relatedFormItem, definition, type,i18NAuthoringSupport);
        //set the child node type
        childNodeType = "mgnl:kiosk-info";
    }

    /*
    @Override
    protected String createChildItemName(Set<String> childNames, Object value, JcrNodeAdapter rootItem) {
        return value.toString();
    }
    */
}
