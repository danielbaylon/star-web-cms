package com.enovax.star.kiosk.group.mgnl.form.field.factory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.enovax.star.kiosk.group.mgnl.form.field.definition.KioskGroupFilterProductTwinColSelectFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Property;

import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.SelectFieldOptionDefinition;
import info.magnolia.ui.form.field.factory.TwinColSelectFieldFactory;
import info.magnolia.ui.form.field.transformer.TransformedProperty;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

/**
 *
 * @author Justin
 * @since 17 AUG 2016
 * @param <T>
 */
public class KioskGroupFilterProductTwinColSelectFieldFactory<T extends KioskGroupFilterProductTwinColSelectFieldDefinition>
		extends TwinColSelectFieldFactory<T> {

	private static final Logger log = LoggerFactory.getLogger(KioskGroupFilterProductTwinColSelectFieldFactory.class);

	private List<SelectFieldOptionDefinition> products = new ArrayList<>();

	private Node currentSelectedNode;

	@Inject
	public KioskGroupFilterProductTwinColSelectFieldFactory(T definition, Item relatedFieldItem, UiContext uiContext,
			I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider) {
		super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
		this.currentSelectedNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
	}

	/**
	 * This is what would be show on right side column.
	 */
	@Override
	public void setPropertyDataSourceAndDefaultValue(Property<?> property) {

		if (property.getValue() == null) {
			HashSet preSelectData = new HashSet();
			for (SelectFieldOptionDefinition def : this.products) {
				preSelectData.add(def.getValue());
			}
			TransformedProperty selectedProperites = (TransformedProperty) property;
			selectedProperites.setValue(preSelectData);
		}

		super.setPropertyDataSourceAndDefaultValue(property);

	}

	@Override
	public List<SelectFieldOptionDefinition> getSelectFieldOptionDefinition() {
		List<SelectFieldOptionDefinition> options = new ArrayList<>();
		SelectFieldOptionDefinition option = null;

		try {

			List<Node> categories = NodeUtil
					.asList(NodeUtil.getNodes(currentSelectedNode, JcrWorkspace.ProductCategories.getNodeType()));

			for (Node category : categories) {
				List<Node> grantedCategories = NodeUtil
						.asList(NodeUtil.getNodes(category, JcrWorkspace.KioskGroupProductCategory.getNodeType()));
				for (Node grantedCategory : grantedCategories) {
					String uuid = grantedCategory
							.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName()).getString();
					Node sourceCategory = NodeUtil
							.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), uuid);
					List<Node> subCategories = NodeUtil
							.asList(NodeUtil.getNodes(sourceCategory, JcrWorkspace.ProductSubCategories.getNodeType()));
					for (Node subCategory : subCategories) {

						List<Node> products = NodeUtil.asList(NodeUtil.getNodes(subCategory,
								JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
						for (Node product : products) {

							String prodUuid = product
									.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())
									.getString();
							Node productNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(),
									prodUuid);
							String name = productNode.getProperty("name").getString();
							option = new SelectFieldOptionDefinition();
							option.setLabel("[" + sourceCategory.getProperty("name").getString() + "] ["
									+ subCategory.getProperty("name").getString() + "] " + name);
							option.setValue(subCategory.getIdentifier() + "-" + prodUuid);
							option.setSelected(Boolean.TRUE);
							options.add(option);
						}

					}
				}

			}

		} catch (RepositoryException e) {
			log.error(e.getMessage(), e);
			options = super.getSelectFieldOptionDefinition();
		}
		this.products = options;
		return options;

	}

}
