package com.enovax.star.kiosk.group.mgnl.form.field.transformer;

import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.composite.NoOpCompositeTransformer;

/**
 * Created by tharaka on 10/8/16.
 */
public class KioskGroupNoOpCompositeTransformer extends NoOpCompositeTransformer {
    public KioskGroupNoOpCompositeTransformer(Item relatedFormItem, ConfiguredFieldDefinition definition, Class<PropertysetItem> type) {
        super(relatedFormItem, definition, type);
    }


}
