package com.enovax.star.kiosk.group.mgnl.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by tharaka on 05/8/16.
 */
public class KioskGroupSizeValidatorDefinition extends ConfiguredFieldValidatorDefinition{

    private String channelPath;

    private int width;

    private int height;

    private int size;



    public KioskGroupSizeValidatorDefinition() {
        setFactoryClass(KioskGroupSizeValidatorFactory.class);
    }

    public String getChannelPath() {
        return channelPath;
    }

    public void setChannelPath(String channelPath) {
        this.channelPath = channelPath;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
