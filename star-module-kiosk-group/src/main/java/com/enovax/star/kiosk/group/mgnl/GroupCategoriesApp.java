package com.enovax.star.kiosk.group.mgnl;

import com.enovax.star.cms.commons.util.UserManagementUtil;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.User;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.app.AppView;
import info.magnolia.ui.api.app.SubAppDescriptor;
import info.magnolia.ui.api.location.DefaultLocation;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.contentapp.ContentApp;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tharaka on 3/8/16.
 */
public class GroupCategoriesApp extends ContentApp {

    private SecuritySupport securitySupport;

    @Inject
    public GroupCategoriesApp(AppContext appContext, AppView view, ComponentProvider componentProvider, SecuritySupport securitySupport) {
        super(appContext, view, componentProvider);
        this.securitySupport = securitySupport;
    }

    @Override
    public void start(Location location)
    {
        boolean hasRight;
        User user = MgnlContext.getUser();
        Map<String, SubAppDescriptor> map = getAppContext().getAppDescriptor().getSubApps();
        List<String> subAppsWithRights = new ArrayList<>();
        String firstSubAppLocation = null;
        for (String subAppName : map.keySet()) {
            hasRight = false;
            if(!map.get(subAppName).isClosable()) {

                if(UserManagementUtil.isSuperUser() || "default-browser".equals(subAppName)) {
                    hasRight = true;
                }else if("b2c-slm-browser".equals(subAppName) && UserManagementUtil.hasAccessToB2CSLMCategory(user)) {
                    hasRight = true;
                }else if("b2c-mflg-browser".equals(subAppName) && UserManagementUtil.hasAccessToB2CMFLGCategory(user)) {
                    hasRight = true;
                }else if("kiosk-slm-browser".equals(subAppName) && UserManagementUtil.hasAccessToKioskSLMCategory(user)) {
                    hasRight = true;
                }else if("kiosk-mflg-browser".equals(subAppName) && UserManagementUtil.hasAccessToKioskMFLGCategory(user)) {
                        hasRight = true;
                }else if("pp-slm-browser".equals(subAppName) && UserManagementUtil.hasAccessToPPSLMCategory(user)) {
                    hasRight = true;
                }else if("pp-mflg-browser".equals(subAppName) && UserManagementUtil.hasAccessToPPMFLGCategory(user)) {
                    hasRight = true;
                }

                if(hasRight) {
                    getAppContext().openSubApp(new DefaultLocation("app", getAppContext().getName(), subAppName));
                    subAppsWithRights.add(getAppContext().getActiveSubAppContext().getInstanceId());
                    if(StringUtils.isEmpty(firstSubAppLocation) && !"default-browser".equals(subAppName)) {
                        firstSubAppLocation = subAppName;
                    }
                }
            }
        }

//        //TODO not sure if this ok to close this one like this
//        if(!subAppsWithRights.contains("1")) {
//            getAppContext().closeSubApp("1");
//        }

        if(firstSubAppLocation != null) {
            locationChanged(new DefaultLocation("app", getAppContext().getName(), firstSubAppLocation));
        }

    }

//    private void getPermission(String role) {
//        SecurityUtil.
//        Iterator iterPermission = Security.getRoleManager().getACLs(role).values().iterator();
//        this.permissionList.add("<ul>");
//
//        while(true) {
//            ACL acl;
//            do {
//                if(!iterPermission.hasNext()) {
//                    this.permissionList.add("</ul>");
//                    return;
//                }
//
//                acl = (ACL)iterPermission.next();
//            } while(acl.getList().isEmpty());
//
//            Iterator i$ = acl.getList().iterator();
//
//            while(i$.hasNext()) {
//                Permission permission = (Permission)i$.next();
//                String repoName = acl.getName();
//                String message = this.getMessages().get("permissionlist.permission", new String[]{this.getPermissionAsName(repoName, permission), repoName, permission.getPattern().getPatternString()});
//                this.permissionList.add("<li>" + message + "</li>");
//            }
//        }
//    }
}
