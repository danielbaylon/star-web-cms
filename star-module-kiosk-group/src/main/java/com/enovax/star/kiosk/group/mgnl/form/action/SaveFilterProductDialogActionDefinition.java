package com.enovax.star.kiosk.group.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

public class SaveFilterProductDialogActionDefinition extends SaveDialogActionDefinition {
    public SaveFilterProductDialogActionDefinition() {
        setImplementationClass(SaveFilterProductDialogAction.class);
    }
}