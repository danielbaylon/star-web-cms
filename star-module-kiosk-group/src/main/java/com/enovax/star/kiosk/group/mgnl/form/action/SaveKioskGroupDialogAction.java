package com.enovax.star.kiosk.group.mgnl.form.action;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.core.value.InternalValue;
import org.apache.jackrabbit.spi.commons.value.QValueValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.vaadin.data.Item;

import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

public class SaveKioskGroupDialogAction extends SaveDialogAction<SaveKioskGroupDialogActionDefinition> {

	private static final Logger log = LoggerFactory.getLogger(SaveKioskGroupDialogAction.class);

	private static final String PRODUCT_FILTER = KioskGroupProperties.ProductFilter.getPropertyName();

	private static final String EXCLUDE_PRODUCT = KioskGroupProperties.ExcludeProduct.getPropertyName();

	private static final String CATEGORY_DATA = KioskGroupProperties.CategoryData.getPropertyName();

	private static final String KIOSK_INFO = KioskGroupProperties.KioskInfo.getPropertyName();

	public SaveKioskGroupDialogAction(SaveKioskGroupDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
		super(definition, item, validator, callback);
	}

	@Override
	public void execute() throws ActionExecutionException {
		try {
			super.execute();
			JcrNodeAdapter jcr = (JcrNodeAdapter) this.item;
			Node rootNode = jcr.getJcrItem();
			if (rootNode.hasNode(CATEGORY_DATA)) {
				Node categoryParentNode = rootNode.getNode(CATEGORY_DATA);
				categoryParentNode.setPrimaryType(JcrWorkspace.ProductCategories.getNodeType());
				categoryParentNode.getSession().save();

				NodeIterator categoryIterator = categoryParentNode.getNodes();
				while (categoryIterator.hasNext()) {
					Node categroyNode = categoryIterator.nextNode();
					categroyNode.setPrimaryType(JcrWorkspace.KioskGroupProductCategory.getNodeType());
					categroyNode.getSession().save();
				}

			} else {
				log.info("node not found for [" + CATEGORY_DATA + "]");
			}
			if (rootNode.hasNode(KIOSK_INFO)) {

				Node infoParentNode = rootNode.getNode(KIOSK_INFO);
				infoParentNode.setPrimaryType(JcrWorkspace.KioskInfo.getNodeType());
				infoParentNode.getSession().save();

				NodeIterator infoIterator = infoParentNode.getNodes();
				while (infoIterator.hasNext()) {
					Node infoNode = infoIterator.nextNode();
					infoNode.setPrimaryType(JcrWorkspace.KioskGroupKioskInfo.getNodeType());
					infoNode.getSession().save();
				}
			} else {
				log.info("node not found for [" + KIOSK_INFO + "]");
			}

			if (rootNode.hasNode(KioskGroupProperties.CategoryData.getPropertyName())) {

				Node categoryParentNode = rootNode.getNode(KioskGroupProperties.CategoryData.getPropertyName());

				List<Value> allProducts = new ArrayList<>();
				NodeIterator categoryIterator = categoryParentNode.getNodes();

				while (categoryIterator.hasNext()) {
					Node categroyNode = categoryIterator.nextNode();
					String categoryUuid = categroyNode.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName()).getString();
					Node categoryNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), categoryUuid);
					List<Node> subCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));
					for (Node subCategory : subCategoryList) {
						List<Node> relatedCMSProductList = NodeUtil.asList(NodeUtil.getNodes(subCategory, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
						for (Node relatedCMSProduct : relatedCMSProductList) {
							if (relatedCMSProduct.hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
								String productUuid = relatedCMSProduct.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();
								String uuid = subCategory.getIdentifier() + "-" + productUuid;
								InternalValue internalValue = InternalValue.create(uuid);
								QValueValue qValueValue = new QValueValue(internalValue, null);
								allProducts.add(qValueValue);
							}
						}
					}
				}

				if (rootNode.hasProperty(EXCLUDE_PRODUCT)) {
					Property excludeProduct = rootNode.getProperty(EXCLUDE_PRODUCT);
					Value[] values = excludeProduct.getValues();
					for (Value value : values) {
						if (allProducts.contains(value)) {
							allProducts.remove(value);
						}
					}
				}
				if (!rootNode.hasProperty(PRODUCT_FILTER)) {
					rootNode.setProperty(PRODUCT_FILTER, new Value[] {});

				}
				rootNode.getProperty(PRODUCT_FILTER).setValue((Value[]) allProducts.toArray(new Value[allProducts.size()]));
				rootNode.getSession().save();

			}
			jcr.applyChanges();
		} catch (RepositoryException e) {
			log.error(e.getMessage(), e);
		}

	}

}
