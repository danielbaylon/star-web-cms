package com.enovax.star.kiosk.group.mgnl.workbench.contentview.column;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.ui.Table;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by tharaka on 8/03/16.
 */
public class GroupNameColumnFormatter<T extends GroupNameColumnDefinition> extends AbstractColumnFormatter<T> {

    private static final Logger log = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    public GroupNameColumnFormatter(T definition) {
        super(definition);
    }

    @Override
    public Object generateCell(Table source, Object itemId, Object columnId) {
        final Item jcrItem = getJcrItem(source, itemId);

        if (jcrItem != null && jcrItem.isNode()) {
            Node node = (Node) jcrItem;
            try {
                if (NodeUtil.isNodeType(node, JcrWorkspace.ProductCategories.getNodeType())
                        || NodeUtil.isNodeType(node, JcrWorkspace.ProductSubCategories.getNodeType())
                        || NodeUtil.isNodeType(node, JcrWorkspace.ProductDefaultSubCategories.getNodeType())) {
                    String name = PropertyUtil.getString(node, "name", node.getName());
                    return name;
                }else if(NodeUtil.isNodeType(node, JcrWorkspace.RelatedCMSProductForCategories.getNodeType())) {
                    String identifier = PropertyUtil.getString(node, "relatedCMSProductUUID", "");
                    Node cmsproduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), identifier);
                    return cmsproduct.getProperty("name").getString();
                }

            } catch (RepositoryException e) {
                log.warn("CMS Unable to get name for column", e);
            }
        }
        return "";
    }
}
