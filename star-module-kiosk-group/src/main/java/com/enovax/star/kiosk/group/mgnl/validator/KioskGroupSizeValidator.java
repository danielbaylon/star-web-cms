package com.enovax.star.kiosk.group.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import com.vaadin.event.ItemClickEvent;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.metadata.MagnoliaAssetMetadata;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.objectfactory.Components;
import info.magnolia.objectfactory.ObjectManufacturer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Created by tharaka on 05/8/16.
 */
public class KioskGroupSizeValidator extends AbstractStringValidator{

    private static final Logger log = LoggerFactory.getLogger(KioskGroupSizeValidator.class);

    private DamTemplatingFunctions damObj;
    private KioskGroupSizeValidatorDefinition definition;

    private final Item item;

    private HashMap<Integer,String> selectedItms;

    public KioskGroupSizeValidator(Item item, String errorMessage, KioskGroupSizeValidatorDefinition definition) {
        super(errorMessage);
        this.item = item;
        this.definition = definition;


    }


    @Override
    protected boolean isValidValue(String s) {
        return false;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
       validateCounter(value);
        KioskGroupSizeCounter.getCounterInstance().setErrorMsgStatus(true);
    }

    public void validateCounter(Object value) throws InvalidValueException {
        selectedItms= KioskGroupSizeCounter.getCounterInstance().getSelectedItms();
        if(selectedItms!=null && item!=null){
            selectedItms.remove(selectedItms.size()-1);

        }
        else if(value!=null && selectedItms.size()>=3 ? true : false) {
            String message = this.getErrorMessage().replace("{0}", String.valueOf(value));

            if(!KioskGroupSizeCounter.getCounterInstance().isErrorMsgStatus()) {
                throw new InvalidValueException(message);
            }

        }

    }

}
