package com.enovax.star.kiosk.group.mgnl;

import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.framework.app.BaseSubApp;

import javax.inject.Inject;

/**
 * Created by tharaka on 3/8/16.
 */
public class GroupCategoriesMainSubApp extends BaseSubApp {

    @Inject
    public GroupCategoriesMainSubApp(final SubAppContext subAppContext, GroupCategoriesMainSubAppView view) {
        super(subAppContext, view);
    }
}
