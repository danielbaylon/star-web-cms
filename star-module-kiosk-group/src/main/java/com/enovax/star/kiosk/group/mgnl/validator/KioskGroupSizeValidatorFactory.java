package com.enovax.star.kiosk.group.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by tharaka on 05/8/16.
 */
public class KioskGroupSizeValidatorFactory extends AbstractFieldValidatorFactory<KioskGroupSizeValidatorDefinition> {


    private Item item;
    private KioskGroupSizeValidatorDefinition definition;

    public KioskGroupSizeValidatorFactory(KioskGroupSizeValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition=definition;
        KioskGroupSizeCounter.getCounterInstance().addItms(definition.getName());
    }

    @Override
    public Validator createValidator() {

//        if (item.getItemProperty(AnnouncementConstant.TITLE)!= null ? item.getItemProperty(AnnouncementConstant.TITLE).getValue() != null : false) {
//            if (AnnouncementConstant.TITLE.equals(item.getItemProperty(AnnouncementConstant.TITLE).getValue().toString())) {
//                return new AnnouncementTitleMandatoryValidator(item, getI18nErrorMessage());
//            }
//        } else if (item.getItemProperty(AnnouncementConstant.IMAGE)!= null ? item.getItemProperty(AnnouncementConstant.IMAGE).getValue() !=null : false ) {
//            if (AnnouncementConstant.IMAGE.equals(item.getItemProperty(AnnouncementConstant.IMAGE).getValue().toString())) {
//                return new AnnouncementImageSizeValidator(item, getI18nErrorMessage());
//            }
//        }
         return new KioskGroupSizeValidator(item, getI18nErrorMessage(),definition);
    }
}