package com.enovax.star.kiosk.group.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

public class SaveKioskGroupDialogActionDefinition extends SaveDialogActionDefinition {
    public SaveKioskGroupDialogActionDefinition() {
        setImplementationClass(SaveKioskGroupDialogAction.class);
    }
}