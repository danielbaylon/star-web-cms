package com.enovax.star.kiosk.group.mgnl.form.action;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.core.value.InternalValue;
import org.apache.jackrabbit.spi.commons.value.QValueValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.vaadin.data.Item;

import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

public class SaveFilterProductDialogAction extends SaveDialogAction<SaveFilterProductDialogActionDefinition> {

	private static final Logger log = LoggerFactory.getLogger(SaveFilterProductDialogAction.class);

	private static final String PRODUCT_FILTER = KioskGroupProperties.ProductFilter.getPropertyName();

	private static final String EXCLUDE_PRODUCT = KioskGroupProperties.ExcludeProduct.getPropertyName();

	private static final String CATEGORY_DATA = KioskGroupProperties.CategoryData.getPropertyName();

	private static final String REL_CMS_PROD = CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName();

	private static final String REL_PROD_CAT_UUID = KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName();

	public SaveFilterProductDialogAction(SaveFilterProductDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
		super(definition, item, validator, callback);
	}

	private List<String> getAllProductList(Node kioskGroupNode) throws RepositoryException {
		List<String> allProducts = new ArrayList<>();
		Node categoryParentNode = kioskGroupNode.getNode(CATEGORY_DATA);
		NodeIterator categoryIterator = categoryParentNode.getNodes();
		while (categoryIterator.hasNext()) {
			Node categroyNode = categoryIterator.nextNode();

			String categoryUuid = categroyNode.getProperty(REL_PROD_CAT_UUID).getString();
			Node categoryNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), categoryUuid);

			List<Node> subCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));

			for (Node subCategory : subCategoryList) {
				List<Node> relatedCMSProductList = NodeUtil.asList(NodeUtil.getNodes(subCategory, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
				for (Node relatedCMSProduct : relatedCMSProductList) {
					String productUuid = relatedCMSProduct.getProperty(REL_CMS_PROD).getString();
					allProducts.add(subCategory.getIdentifier() + "-" + productUuid);
				}
			}
		}
		return allProducts;
	}

	private List<String> getGrantedProductList(Node kioskGroupNode) throws RepositoryException {
		List<String> grantedProductList = new ArrayList<>();
		for (Value value : kioskGroupNode.getProperty(PRODUCT_FILTER).getValues()) {
			grantedProductList.add(((QValueValue) value).getQValue().getString());
		}
		return grantedProductList;
	}

	@Override
	public void execute() throws ActionExecutionException {
		try {
			super.execute();
			JcrNodeAdapter jcr = (JcrNodeAdapter) this.item;
			Node kioskGroupNode = jcr.getJcrItem();
			if (!kioskGroupNode.hasProperty(PRODUCT_FILTER)) {
				kioskGroupNode.setProperty(PRODUCT_FILTER, new String[] {});
			}
			if (!kioskGroupNode.hasProperty(EXCLUDE_PRODUCT)) {
				kioskGroupNode.setProperty(EXCLUDE_PRODUCT, new String[] {});
			}

			if (kioskGroupNode.hasNode(CATEGORY_DATA)) {
				List<String> allProductList = this.getAllProductList(kioskGroupNode);
				List<String> grantedProductList = this.getGrantedProductList(kioskGroupNode);
				List<Value> excludeValues = new ArrayList<>();

				for (String product : allProductList) {
					if (!grantedProductList.contains(product)) {
						InternalValue internalValue = InternalValue.create(product);
						QValueValue qValueValue = new QValueValue(internalValue, null);
						excludeValues.add(qValueValue);
					}
				}

				kioskGroupNode.getProperty(EXCLUDE_PRODUCT).setValue((Value[]) excludeValues.toArray(new Value[excludeValues.size()]));

			}

			kioskGroupNode.getSession().save();
			jcr.applyChanges();
		} catch (RepositoryException e) {
			log.error(e.getMessage(), e);
		}

	}

}
