package com.enovax.star.kiosk.group.mgnl.validator;

import java.util.HashMap;

/**
 * Created by tharaka on 10/8/16.
 */
public class KioskGroupSizeCounter {

    private static KioskGroupSizeCounter instance = null;

    private HashMap<Integer,String> selectedItms;

    private int count;
    private boolean errorMsgStatus;

    private KioskGroupSizeCounter() {
        selectedItms = new HashMap<>();
        count=1;
    }

    public static KioskGroupSizeCounter getCounterInstance() {
        if(instance == null) {
            instance = new KioskGroupSizeCounter();
        }
        return instance;
    }

    public HashMap<Integer, String> getSelectedItms() {
        return selectedItms;
    }

    public void setSelectedItms(HashMap<Integer, String> selectedItms) {
        this.selectedItms = selectedItms;
    }

    public void addItms(String value) {
        this.selectedItms.put(count,value);
        count++;
    }

    public void clear() {
        selectedItms.clear();
    }

    public boolean isErrorMsgStatus() {
        return errorMsgStatus;
    }

    public void setErrorMsgStatus(boolean errorMsgStatus) {
        this.errorMsgStatus = errorMsgStatus;
    }
}
