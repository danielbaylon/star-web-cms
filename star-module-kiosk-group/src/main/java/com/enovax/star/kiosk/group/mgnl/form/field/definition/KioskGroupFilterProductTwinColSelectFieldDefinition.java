package com.enovax.star.kiosk.group.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.TwinColSelectFieldDefinition;

public class KioskGroupFilterProductTwinColSelectFieldDefinition extends TwinColSelectFieldDefinition {

	private String jcrNodePath;

	public String getJcrNodePath() {
		return jcrNodePath;
	}

	public void setJcrNodePath(String jcrNodePath) {
		this.jcrNodePath = jcrNodePath;
	}

}
