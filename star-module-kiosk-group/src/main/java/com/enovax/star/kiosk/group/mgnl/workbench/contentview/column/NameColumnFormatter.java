package com.enovax.star.kiosk.group.mgnl.workbench.contentview.column;

import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.vaadin.ui.Table;

import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;

public class NameColumnFormatter<T extends NameColumnDefinition> extends AbstractColumnFormatter<T> {

	private static final Logger log = LoggerFactory.getLogger(NameColumnFormatter.class);

	public NameColumnFormatter(T definition) {
		super(definition);
	}

	@Override
	public Object generateCell(Table source, Object itemId, Object columnId) {
		final Item jcrItem = getJcrItem(source, itemId);
		String name = "";
		if (jcrItem != null && jcrItem.isNode()) {
			Node node = (Node) jcrItem;
			try {
				if (NodeUtil.isNodeType(node, JcrWorkspace.KioskGroup.getNodeType())) {
					name = node.getName();
				} else if (NodeUtil.isNodeType(node, JcrWorkspace.ProductCategories.getNodeType())) {
					name = "product categories";
				} else if (NodeUtil.isNodeType(node, JcrWorkspace.KioskGroupProductCategory.getNodeType())) {
					Property uuid = node.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName());
					Node sourceNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(),
							uuid.getString());
					if (sourceNode != null) {
						name = sourceNode.getProperty("name").getString();
					}
				} else if (NodeUtil.isNodeType(node, JcrWorkspace.KioskInfo.getNodeType())) {
					name = "kiosk info";
				} else if (NodeUtil.isNodeType(node, JcrWorkspace.KioskGroupKioskInfo.getNodeType())) {
					Property uuid = node.getProperty(KioskGroupProperties.RelatedKioskInfoUUID.getPropertyName());
					Node sourceNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.KioskInfo.getWorkspaceName(),
							uuid.getString());
					if (sourceNode != null) {
						name = sourceNode.getName();
					}
				}
			} catch (RepositoryException e) {
				log.error(e.getMessage(), e);
			}
		}
		return name;
	}
}