package com.enovax.star.kiosk.group.mgnl;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by tharaka on 3/8/16.
 */
@StyleSheet({"vaadin://product-categories.css"})
public class GroupCategoriesMainSubAppViewImpl extends VerticalLayout implements GroupCategoriesMainSubAppView {

    public GroupCategoriesMainSubAppViewImpl() {
        addStyleName("app-categories");
        setMargin(true);
        setSpacing(true);
        Label title = new Label("This module is for management of CMS Product Categories.");
        title.addStyleName("group");
        addComponent(title);
    }

    @Override
    public Component asVaadinComponent() {
        return this;
    }
}
