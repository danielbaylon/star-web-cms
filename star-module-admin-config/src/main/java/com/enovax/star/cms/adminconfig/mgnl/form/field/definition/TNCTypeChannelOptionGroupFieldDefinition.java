package com.enovax.star.cms.adminconfig.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.OptionGroupFieldDefinition;

/**
 * Created by jennylynsze on 8/1/16.
 */
public class TNCTypeChannelOptionGroupFieldDefinition extends OptionGroupFieldDefinition {
    private String tncType;

    public String getTncType() {
        return tncType;
    }

    public void setTncType(String tncType) {
        this.tncType = tncType;
    }
}
