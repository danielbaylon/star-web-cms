package com.enovax.star.cms.adminconfig.mgnl.form.action;

import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.framework.action.ActivationAction;
import info.magnolia.ui.framework.action.ActivationActionDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.jcr.Node;
import java.util.List;

/**
 * Created by jennylynsze on 9/25/16.
 */
public class AnnouncementActivationAction <D extends AnnouncementActivationActionDefinition>  extends ActivationAction {

    private static final Logger log = LoggerFactory.getLogger(AnnouncementActivationAction.class);

    private Node announcementNode;
    private EventBus admincentralEventBus;
    private UiContext uiContext;
    private SimpleTranslator i18n;
    private JcrItemId changedItemId;

    public AnnouncementActivationAction(ActivationActionDefinition definition, List<JcrItemAdapter>  items, CommandsManager commandsManager, @Named("admincentral") EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, items, commandsManager, admincentralEventBus, uiContext, i18n);

        if(items != null) {
            this.announcementNode =  ((JcrNodeAdapter)items.get(0)).getJcrItem();
        }
        this.uiContext = uiContext;
        this.i18n = i18n;
        this.admincentralEventBus = admincentralEventBus;
        this.changedItemId = items.isEmpty() ? null : items.get(0).getItemId();
    }

    public AnnouncementActivationAction(ActivationActionDefinition definition, JcrItemAdapter item, CommandsManager commandsManager, EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, item, commandsManager, admincentralEventBus, uiContext, i18n);
    }

    @Override
    protected void onPostExecute() throws Exception {
        Context context = MgnlContext.getInstance();
        // yes, this is inverted, because a chain returns false when it is finished.
        boolean success = !(Boolean) context.getAttribute(COMMAND_RESULT);

        if(success) {

            String image = PropertyUtil.getString(announcementNode, "image");

            if(StringUtils.isNotEmpty(image)) {
                PublishingUtil.publishImages(image);
            }

        }

        doShowSuccessMessage(success);

//        super.onPostExecute();
    }


    public void doShowSuccessMessage(boolean success) throws  Exception{
        admincentralEventBus.fireEvent(new ContentChangedEvent(changedItemId));
        //boolean successFalse = !(Boolean) context.getAttribute(COMMAND_RESULT);
        // yes, this is inverted, because a chain returns false when it is finished.
        String message = i18n.translate(getMessage(success));
        MessageStyleTypeEnum messageStyleType = success ? MessageStyleTypeEnum.INFO : MessageStyleTypeEnum.ERROR;

        if (StringUtils.isNotBlank(message)) {
            uiContext.openNotification(messageStyleType, true, message);
        }
    }
}
