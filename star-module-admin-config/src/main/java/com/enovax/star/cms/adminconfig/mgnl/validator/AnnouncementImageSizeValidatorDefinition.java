package com.enovax.star.cms.adminconfig.mgnl.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by tharaka on 29/7/16.
 */
public class AnnouncementImageSizeValidatorDefinition extends ConfiguredFieldValidatorDefinition{

    private String channelPath;

    private int width;

    private int height;


    public AnnouncementImageSizeValidatorDefinition() {
        setFactoryClass(AnnouncementImageSizeValidatorFactory.class);
    }

    public String getChannelPath() {
        return channelPath;
    }

    public void setChannelPath(String channelPath) {
        this.channelPath = channelPath;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
