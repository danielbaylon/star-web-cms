package com.enovax.star.cms.adminconfig.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.CheckboxFieldDefinition;

/**
 * Created by jennylynsze on 7/18/16.
 */
public class TNCTypeChannelCheckboxFieldDefinition extends CheckboxFieldDefinition {
    private String tncType;

    public String getTncType() {
        return tncType;
    }

    public void setTncType(String tncType) {
        this.tncType = tncType;
    }
}
