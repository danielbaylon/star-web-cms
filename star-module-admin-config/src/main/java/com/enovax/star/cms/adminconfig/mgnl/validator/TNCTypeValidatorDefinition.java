package com.enovax.star.cms.adminconfig.mgnl.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 10/21/16.
 */
public class TNCTypeValidatorDefinition extends ConfiguredFieldValidatorDefinition {

    private String type;
    private String channel;
    private String errorMessage;

    public TNCTypeValidatorDefinition()
    {
        setFactoryClass(TNCTypeValidatorFactory.class);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
