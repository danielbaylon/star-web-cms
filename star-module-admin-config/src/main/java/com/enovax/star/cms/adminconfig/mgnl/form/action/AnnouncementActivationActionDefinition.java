package com.enovax.star.cms.adminconfig.mgnl.form.action;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

/**
 * Created by jennylynsze on 9/25/16.
 */
public class AnnouncementActivationActionDefinition extends ActivationActionDefinition {

    public AnnouncementActivationActionDefinition() {
        setImplementationClass(AnnouncementActivationAction.class);
    }
}
