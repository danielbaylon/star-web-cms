package com.enovax.star.cms.adminconfig.mgnl.form.field.factory;

import com.enovax.star.cms.adminconfig.mgnl.event.TNCTypeValueChangedEvent;
import com.enovax.star.cms.adminconfig.mgnl.form.field.definition.TNCTypeChannelOptionGroupFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractSelect;
import info.magnolia.event.EventBus;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.OptionGroupFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;

/**
 * Created by jennylynsze on 8/1/16.
 */
public class TNCTypeChannelOptionGroupFieldFactory<T extends TNCTypeChannelOptionGroupFieldDefinition> extends OptionGroupFieldFactory<T> implements TNCTypeValueChangedEvent.Handler {

    private final EventBus subAppEventBus;
    private AbstractSelect field;
    private final String tncType;
    private String selectedValue;

    @Inject
    public TNCTypeChannelOptionGroupFieldFactory(TNCTypeChannelOptionGroupFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
        this.subAppEventBus = subAppEventBus;
        this.subAppEventBus.addHandler(TNCTypeValueChangedEvent.class, this);
        this.tncType = definition.getTncType();

        if(relatedFieldItem instanceof JcrNodeAdapter) {
            Node tncNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
            this.selectedValue = PropertyUtil.getString(tncNode, "type");
        }
    }

    @Override
    protected AbstractSelect createFieldComponent() {
        this.field = super.createFieldComponent();
        setVisible();
        return this.field;
    }

    @Override
    public void onTNCTypeValueChanged(TNCTypeValueChangedEvent event) {
        if (event.getSelectedValue() == null || StringUtils.isBlank(tncType)) {
            return;
        }

        this.selectedValue = event.getSelectedValue();

        setVisible();
    }

    private void setVisible() {
        if(this.tncType.equals(this.selectedValue)) {
            this.field.setVisible(true);
        }else {
            this.field.setVisible(false);
            this.field.clear();
        }
    }
}
