package com.enovax.star.cms.adminconfig.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 10/21/16.
 */
public class TNCTypeValidatorFactory extends AbstractFieldValidatorFactory<TNCTypeValidatorDefinition> {

    private Item item;
    private TNCTypeValidatorDefinition definition;

    public TNCTypeValidatorFactory(TNCTypeValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition = definition;
    }

    @Override
    public Validator createValidator() {
        return new TNCTypeValidator(item, definition.getChannel(), definition.getType(), definition.getErrorMessage());
    }
}
