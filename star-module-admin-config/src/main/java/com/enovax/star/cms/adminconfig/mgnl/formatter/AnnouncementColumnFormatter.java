package com.enovax.star.cms.adminconfig.mgnl.formatter;

import com.enovax.star.cms.commons.common.CommonConstant;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.vaadin.ui.Table;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.ColumnDefinition;

import javax.jcr.Item;
import javax.jcr.Node;
import java.util.Calendar;

/**
 * Created by tharaka on 28/7/16.
 */
public class AnnouncementColumnFormatter extends AbstractColumnFormatter {



    public AnnouncementColumnFormatter(ColumnDefinition definition) {
        super(definition);
    }

    @Override
    public Object generateCell(Table source, Object itemId, Object columnId) {
        Item jcrItem = this.getJcrItem(source, itemId);
        if (jcrItem != null && jcrItem.isNode()) {
            Node node = (Node) jcrItem;

            Calendar date = null;
            if(columnId.equals(CommonConstant.VALID_FROM_DATE)){
                date = PropertyUtil.getDate(node, CommonConstant.VALID_FROM_DATE);
            }else if(columnId.equals(CommonConstant.VALID_TO_DATE)){
                date = PropertyUtil.getDate(node, CommonConstant.VALID_TO_DATE);
            }

            if(date!=null) {
                return NvxDateUtils.formatDate(date.getTime(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }else{
                return null;
            }

        }

        return null;
    }


}
