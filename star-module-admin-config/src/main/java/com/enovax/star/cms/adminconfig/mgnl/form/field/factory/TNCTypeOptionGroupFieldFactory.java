package com.enovax.star.cms.adminconfig.mgnl.form.field.factory;

import com.enovax.star.cms.adminconfig.mgnl.event.TNCTypeValueChangedEvent;
import com.enovax.star.cms.adminconfig.mgnl.form.field.definition.TNCTypeOptionGroupFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.AbstractSelect;
import info.magnolia.event.EventBus;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.OptionGroupFieldDefinition;
import info.magnolia.ui.form.field.factory.OptionGroupFieldFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by jennylynsze on 7/18/16.
 */
public class TNCTypeOptionGroupFieldFactory<T extends TNCTypeOptionGroupFieldDefinition> extends OptionGroupFieldFactory<T> {

    private final EventBus subAppEventBus;

    @Inject
    public TNCTypeOptionGroupFieldFactory(OptionGroupFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
        this.subAppEventBus = subAppEventBus;

//        if(relatedFieldItem instanceof JcrNodeAdapter) {
//            Node tncNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
//            Object typeProperty = PropertyUtil.getPropertyValueObject(tncNode, "type");
//
//            if(typeProperty != null && TNCType.General.toString().equals(typeProperty.toString())) {
//                subAppEventBus.fireEvent(new TNCTypeValueChangedEvent(typeProperty.toString()));
//            }
//        }
    }

    @Override
    protected AbstractSelect createFieldComponent() {
        final AbstractSelect select = super.createFieldComponent();
        select.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                subAppEventBus.fireEvent(new TNCTypeValueChangedEvent((String)select.getValue()));
            }
        });
        return select;
    }

}
