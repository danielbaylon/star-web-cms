package com.enovax.star.cms.adminconfig.mgnl.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by tharaka on 29/7/16.
 */
public class AnnouncementTitleMandatoryValidatorDefinition extends ConfiguredFieldValidatorDefinition{

    public AnnouncementTitleMandatoryValidatorDefinition() {
        setFactoryClass(AnnouncementTitleMandatoryValidatorFactory.class);
    }
}
