package com.enovax.star.cms.adminconfig.mgnl.event;

import info.magnolia.event.Event;
import info.magnolia.event.EventHandler;

/**
 * Created by jennylynsze on 7/18/16.
 */
public class TNCTypeValueChangedEvent implements Event<TNCTypeValueChangedEvent.Handler> {

    private String selectedValue;

    public TNCTypeValueChangedEvent(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public interface Handler extends EventHandler {
        void onTNCTypeValueChanged(TNCTypeValueChangedEvent event);
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    @Override
    public void dispatch(Handler handler) {
        handler.onTNCTypeValueChanged(this);
    }

}
