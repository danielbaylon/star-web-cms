package com.enovax.star.cms.adminconfig.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.metadata.MagnoliaAssetMetadata;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tharaka on 29/7/16.
 */
public class AnnouncementImageSizeValidator extends AbstractStringValidator {

    private static final Logger log = LoggerFactory.getLogger(AnnouncementImageSizeValidator.class);


    private DamTemplatingFunctions damObj;
    private AnnouncementImageSizeValidatorDefinition definition;

    private final Item item;

    public AnnouncementImageSizeValidator(Item item, String errorMessage, AnnouncementImageSizeValidatorDefinition definition) {
        super(errorMessage);
        this.item = item;
        this.definition = definition;
    }

    @Override
    protected boolean isValidValue(String value) {
        if(StringUtils.isEmpty(value)) {
            return true;
        }
        this.damObj = Components.getComponent(DamTemplatingFunctions.class);
        Asset ast = damObj.getAsset(value);
        return ast== null ? false : checkImageDimensions(ast);
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        super.validate(value);
    }

    public boolean checkImageDimensions(Asset ast){
            int originalImgWidth          = definition.getWidth();
            int originalImgHeight         = definition.getHeight();

        if (ast.supports(MagnoliaAssetMetadata.class)) {
                MagnoliaAssetMetadata metadata = ast.getMetadata(MagnoliaAssetMetadata.class);
                long width = metadata.getWidth();
                long height = metadata.getHeight();

                if (width <= originalImgWidth & height <= originalImgHeight) {
                    return true;
                }
            }

                // this method is used to get image width and height from JCR
                // Node reservationNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), definition.getChannelPath());
                // int defaultImgWidth = (int) reservationNode.getProperty(AnnouncementConstant.IMAGE_WIDTH).getLong();
                // int defaultImgHeight = (int) reservationNode.getProperty(AnnouncementConstant.IMAGE_HEIGHT).getLong();


            return false;

    }
}
