package com.enovax.star.cms.adminconfig.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by tharaka on 29/7/16.
 */
public class AnnouncementImageSizeValidatorFactory extends AbstractFieldValidatorFactory<AnnouncementImageSizeValidatorDefinition> {


    private Item item;
    private AnnouncementImageSizeValidatorDefinition definition;

    public AnnouncementImageSizeValidatorFactory(AnnouncementImageSizeValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition=definition;

    }

    @Override
    public Validator createValidator() {

//        if (item.getItemProperty(AnnouncementConstant.TITLE)!= null ? item.getItemProperty(AnnouncementConstant.TITLE).getValue() != null : false) {
//            if (AnnouncementConstant.TITLE.equals(item.getItemProperty(AnnouncementConstant.TITLE).getValue().toString())) {
//                return new AnnouncementTitleMandatoryValidator(item, getI18nErrorMessage());
//            }
//        } else if (item.getItemProperty(AnnouncementConstant.IMAGE)!= null ? item.getItemProperty(AnnouncementConstant.IMAGE).getValue() !=null : false ) {
//            if (AnnouncementConstant.IMAGE.equals(item.getItemProperty(AnnouncementConstant.IMAGE).getValue().toString())) {
//                return new AnnouncementImageSizeValidator(item, getI18nErrorMessage());
//            }
//        }
         return new AnnouncementImageSizeValidator(item, getI18nErrorMessage(),definition);
    }
}