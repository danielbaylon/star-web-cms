package com.enovax.star.cms.adminconfig.mgnl.common;

/**
 * Created by tharaka on 1/8/16.
 */
public class AnnouncementConstant {


    public final static String TITLE="title";
    public final static String VALID_FROM_DATE="validFrom";
    public final static String VALID_TO_DATE="validTo";
    public final static String CONTENT="content";
    public final static String RELATED_LINK="relatedLink";
    public final static String IMAGE="image";
    public final static String STATUS="status";

    public final static String IMAGE_WIDTH ="width";
    public final static String IMAGE_HEIGHT ="height";
}
