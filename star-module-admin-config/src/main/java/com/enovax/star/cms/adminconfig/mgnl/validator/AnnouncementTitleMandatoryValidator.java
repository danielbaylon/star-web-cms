package com.enovax.star.cms.adminconfig.mgnl.validator;

import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tharaka on 29/7/16.
 */
public class AnnouncementTitleMandatoryValidator extends AbstractStringValidator {

    private static final Logger log = LoggerFactory.getLogger(AnnouncementTitleMandatoryValidator.class);

    private final Item item;

    public AnnouncementTitleMandatoryValidator(Item item, String errorMessage) {
        super(errorMessage);
        this.item = item;
    }

    @Override
    protected boolean isValidValue(String value) {
        if(StringUtils.isEmpty(value)) {
            return false;
        }
        return true;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
//        if(true){
//           Property valued= item.getItemProperty("title");
//        }
        super.validate(value);
    }
}
