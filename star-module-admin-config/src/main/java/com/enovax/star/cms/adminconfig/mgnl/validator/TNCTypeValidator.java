package com.enovax.star.cms.adminconfig.mgnl.validator;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 10/21/16.
 */
public class TNCTypeValidator  extends AbstractStringValidator {

    private static final Logger log = LoggerFactory.getLogger(TNCTypeValidator.class);

    private final Item item;
    private final String channel;
    private final String type;

    /**
     * Constructs a validator for strings.
     * <p>
     * <p>
     * Null and empty string values are always accepted. To reject empty values,
     * set the field being validated as required.
     * </p>
     *
     * @param errorMessage the message to be included in an {@link InvalidValueException}
     *                     (with "{0}" replaced by the value that failed validation).
     */
    public TNCTypeValidator(Item item, String channel, String type, String errorMessage) {
        super(errorMessage);
        this.item = item;
        this.channel = channel;
        this.type = type;
    }

    @Override
    protected boolean isValidValue(String value) {

        if(!this.type.equals(value)) {
            return true;
        }

        if(item instanceof  JcrNewNodeAdapter) {
            //Check if there's already existing tnctype = general for the channel
            try {
                Iterable<Node> result = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(),
                        JcrWorkspace.TNC.getNodeType(),
                        "/jcr:root/" + channel + "//element(*," +
                                JcrWorkspace.TNC.getNodeType() + ")[@type='" + value + "']");

                if(result != null && result.iterator().hasNext()) {
                    return false;
                }else {
                    return true;
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }

        }else if (item instanceof JcrNodeAdapter) {

            String uuid = null;
            try {
                uuid = ((JcrNodeAdapter) item).getJcrItem().getIdentifier();

                Iterable<Node> result = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(),
                        JcrWorkspace.TNC.getNodeType(),
                        "/jcr:root/" + channel + "//element(*," +
                                JcrWorkspace.TNC.getNodeType() + ")[@type='" + value + "']");

                if(result != null && result.iterator().hasNext()) {
                    //check the uuid
                    Node savedInDB = result.iterator().next();
                    if(savedInDB.getIdentifier().equals(uuid)) {
                        return true;
                    }
                    return false;
                }else {
                    return true;
                }

            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
