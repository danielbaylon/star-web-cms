package com.enovax.star.cms.adminconfig.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.TextFieldDefinition;
import info.magnolia.ui.form.field.factory.TextFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

/**
 * Created by danielbaylon on 4/8/16.
 */
public class MerchantNameTextFieldFactory extends TextFieldFactory {

    public MerchantNameTextFieldFactory(TextFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);

        if(JcrWorkspace.MerchantDefault.getNodeType().equals(((JcrNodeAdapter) relatedFieldItem).getPrimaryNodeTypeName())) {
            definition.setReadOnly(true);
            definition.setI18n(false);
            definition.setRequired(false);
        }else{
            definition.setReadOnly(false);
            definition.setI18n(true);
            definition.setRequired(true);
        }
    }
}
