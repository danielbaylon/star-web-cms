package com.enovax.star.cms.productcategories.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 3/17/16.
 */
public class SaveProductSubCategoryDialogActionDefinition extends SaveDialogActionDefinition  {
    public SaveProductSubCategoryDialogActionDefinition() {
        setImplementationClass(SaveProductSubCategoryDialogAction.class);
    }
}
