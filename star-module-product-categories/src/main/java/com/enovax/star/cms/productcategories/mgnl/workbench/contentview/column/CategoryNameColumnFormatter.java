package com.enovax.star.cms.productcategories.mgnl.workbench.contentview.column;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.ui.Table;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/15/16.
 */
public class CategoryNameColumnFormatter<T extends CategoryNameColumnDefinition> extends AbstractColumnFormatter<T> {

    private static final Logger log = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    public CategoryNameColumnFormatter(T definition) {
        super(definition);
    }

    @Override
    public Object generateCell(Table source, Object itemId, Object columnId) {
        final Item jcrItem = getJcrItem(source, itemId);

        if (jcrItem != null && jcrItem.isNode()) {
            Node node = (Node) jcrItem;
            try {
                if (NodeUtil.isNodeType(node, JcrWorkspace.ProductCategories.getNodeType())
                        || NodeUtil.isNodeType(node, JcrWorkspace.ProductSubCategories.getNodeType())
                        || NodeUtil.isNodeType(node, JcrWorkspace.ProductDefaultSubCategories.getNodeType())) {
                    String name = PropertyUtil.getString(node, "name", node.getName());
                    return name;
                }else if(NodeUtil.isNodeType(node, JcrWorkspace.RelatedCMSProductForCategories.getNodeType())) {
                    String identifier = PropertyUtil.getString(node, "relatedCMSProductUUID", "");
                    Node cmsproduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), identifier);
                    return cmsproduct.getProperty("name").getString();
                }

            } catch (RepositoryException e) {
                log.warn("CMS Unable to get name for column", e);
            }
        }
        return "";
    }
}
