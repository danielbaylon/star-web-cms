package com.enovax.star.cms.productcategories.mgnl.form.action;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.vaadin.data.Item;
import info.magnolia.cms.core.Path;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.jackrabbit.core.value.InternalValue;
import org.apache.jackrabbit.spi.commons.value.QValueValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 3/17/16.
 */
public class SaveProductSubCategoryDialogAction extends SaveDialogAction<SaveProductSubCategoryDialogActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(SaveProductSubCategoryDialogAction.class);

    // constant to be used
    private static final String defaultNodeName = "default";
    private static final String defaultDisplayName = "Default";

    private static final String EXCLUDE_PRODUCT = KioskGroupProperties.ExcludeProduct.getPropertyName();

    public SaveProductSubCategoryDialogAction(SaveProductSubCategoryDialogActionDefinition definition, Item item,
                                              EditorValidator validator, EditorCallback callback) {
        super(definition, item, validator, callback);
    }

    private Map<String, String> getCategoryProductMap(Node channelNode) throws RepositoryException {
        Map<String, String> categoryProductMap = new HashMap<>();
        List<Node> categoryList = NodeUtil
                .asList(NodeUtil.getNodes(channelNode, JcrWorkspace.ProductCategories.getNodeType()));
        for (Node categoryNode : categoryList) {
            List<Node> subCategoryList = NodeUtil
                    .asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));
            for (Node subCategoryNode : subCategoryList) {
                List<Node> relatedCMSProductList = NodeUtil.asList(
                        NodeUtil.getNodes(subCategoryNode, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));

                for (Node relatedCMSProduct : relatedCMSProductList) {
                    if (relatedCMSProduct
                            .hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
                        String prodUuid = relatedCMSProduct
                                .getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())
                                .getString();
                        String subCategoryUuid = subCategoryNode.getIdentifier();
                        String categoryUuid = categoryNode.getIdentifier();
                        categoryProductMap.put(subCategoryUuid + "-" + prodUuid, categoryUuid);

                    }
                }
            }

        }
        return categoryProductMap;
    }

    private Node getGrantedCategoryParentNode(Node kioskGroupNode) throws RepositoryException {
        Node grantedCategoryParentNode = null;
        if (kioskGroupNode.hasProperty("status") && "Active".equals(kioskGroupNode.getProperty("status").getString())) {
            List<Node> categories = NodeUtil
                    .asList(NodeUtil.getNodes(kioskGroupNode, JcrWorkspace.ProductCategories.getNodeType()));
            if (categories.size() > 0) {
                grantedCategoryParentNode = categories.get(0);
            } else {
                throw new RepositoryException(
                        "No categoreis have been granted to kiosk group :" + kioskGroupNode.getName());
            }
        }

        return grantedCategoryParentNode;
    }

    private List<String> getGrantedCategoryUuidList(List<Node> grantedCategoryNodeList) throws RepositoryException {
        List<String> grantedCategoryUuidList = new ArrayList<>();
        for (Node categoryNode : grantedCategoryNodeList) {
            grantedCategoryUuidList.add(categoryNode
                    .getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName()).getString());
        }
        return grantedCategoryUuidList;
    }

    private List<String> getValueList(Node kioskGroupNode, String property) throws RepositoryException {

        List<String> valueList = new ArrayList<>();
        if (kioskGroupNode.hasProperty(property)) {
            for (Value value : kioskGroupNode.getProperty(property).getValues()) {
                valueList.add(((QValueValue) value).getQValue().getString());
            }
        }
        return valueList;
    }

    @Override
    public void execute() throws ActionExecutionException {
        super.execute();
        try {
            JcrNodeAdapter jcr = (JcrNodeAdapter) this.item;
            Node currentNode = jcr.getJcrItem();
            Node channelNode = currentNode.getParent().getParent();
            Map<String, String> categoryProductMap = this.getCategoryProductMap(channelNode);

            Node kioskGroupParentNode = JcrRepository.getParentNode(JcrWorkspace.KioskGroup.getWorkspaceName(),
                    "/" + channelNode.getName());

            List<Node> kioskGroupNodeList = NodeUtil
                    .asList(NodeUtil.getNodes(kioskGroupParentNode, JcrWorkspace.KioskGroup.getNodeType()));

            for (Node kioskGroupNode : kioskGroupNodeList) {
                Node grantedCategoryParentNode = this.getGrantedCategoryParentNode(kioskGroupNode);

                List<Node> grantedCategoryList = NodeUtil.asList(NodeUtil.getNodes(grantedCategoryParentNode,
                        JcrWorkspace.KioskGroupProductCategory.getNodeType()));
                List<String> grantedCategoryUuidList = this.getGrantedCategoryUuidList(grantedCategoryList);

                List<String> excludedProductList = this.getValueList(kioskGroupNode, EXCLUDE_PRODUCT);
                List<Value> productFilter = new ArrayList<>();
                for (String subCategoryProductUuid : categoryProductMap.keySet()) {
                    if (grantedCategoryUuidList.contains(categoryProductMap.get(subCategoryProductUuid))) {
                        if (excludedProductList.size() == 0 || !excludedProductList.contains(subCategoryProductUuid)) {
                            InternalValue internalValue = InternalValue.create(subCategoryProductUuid);
                            QValueValue qValueValue = new QValueValue(internalValue, null);
                            productFilter.add(qValueValue);
                        }
                    }
                }

                kioskGroupNode.getProperty(KioskGroupProperties.ProductFilter.getPropertyName())
                        .setValue((Value[]) productFilter.toArray(new Value[productFilter.size()]));
                kioskGroupNode.getSession().save();
                jcr.applyChanges();

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    /**
     * @Override public void execute() throws ActionExecutionException {
     *           this.validator.showValidation(true);
     *           if(this.validator.isValid()) { //get all the sub categories
     *           related product i guess
     *
     *           JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;
     *           List<String> relatedCMSProductUUIDSaved = new ArrayList<>();
     *           try { List<Node> relatedCMSProductNodeList =
     *           NodeUtil.asList(NodeUtil.getNodes(itemChanged.getJcrItem(),
     *           JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
     *           for(Node relatedCMSProductNode: relatedCMSProductNodeList) {
     *           if(relatedCMSProductNode.hasProperty("relatedCMSProductUUID"))
     *           {
     *           relatedCMSProductUUIDSaved.add(relatedCMSProductNode.getProperty("relatedCMSProductUUID").getString());
     *           } } } catch (RepositoryException e) { e.printStackTrace(); }
     *
     *           try { Node e = itemChanged.applyChanges(); this.setNodeName(e,
     *           itemChanged); e.getSession().save(); } catch
     *           (RepositoryException var3) { throw new
     *           ActionExecutionException(var3); }
     *
     *           List<String> relatedCMSProductUUIDAfterSaved = new
     *           ArrayList<>(); try { List<Node> relatedCMSProductNodeList =
     *           NodeUtil.asList(NodeUtil.getNodes(itemChanged.getJcrItem(),
     *           JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
     *           for(Node relatedCMSProductNode: relatedCMSProductNodeList) {
     *           if(relatedCMSProductNode.hasProperty("relatedCMSProductUUID"))
     *           { String uuid =
     *           relatedCMSProductNode.getProperty("relatedCMSProductUUID").getString();
     *           relatedCMSProductUUIDAfterSaved.add(uuid);
     *
     *           if(!relatedCMSProductUUIDSaved.contains(uuid)) { //(2) for all
     *           not in saved but in after save, need to add the category Node
     *           product =
     *           NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(),
     *           uuid); product.setProperty("category",
     *           itemChanged.getJcrItem().getIdentifier()); } } } } catch
     *           (RepositoryException e) { e.printStackTrace(); }
     *
     *           for(String uuidSaved: relatedCMSProductUUIDSaved) {
     *           if(!relatedCMSProductUUIDAfterSaved.contains(uuidSaved)) { Node
     *           product = null; try { product =
     *           NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(),
     *           uuidSaved); product.dele } catch (RepositoryException e) {
     *           e.printStackTrace(); }
     *
     *           } } //(1) for all in saved but not in after save, need to
     *           remove the category
     *
     *
     *
     *           this.callback.onSuccess(((SaveDialogActionDefinition)this.getDefinition()).getName());
     *           } else { log.info("Validation error(s) occurred. No save
     *           performed."); }
     *
     *           }
     **/

    // TODO check uniquess with node in other channels
    @Override
    protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {

        // Add this to make the default to be appeared unpublished
        if (defaultNodeName.equals(node.getName())) {
            node.setProperty("name", defaultDisplayName);
        }



        if(item instanceof JcrNewNodeAdapter) {
            String newNodeName = PropertyUtil.getString(node, "name");
            if (!node.getName().equals(Path.getValidatedLabel(newNodeName))) {
                newNodeName = Path.getUniqueLabel(node.getSession(), node.getParent().getPath(),
                        Path.getValidatedLabel(newNodeName));
                item.setNodeName(newNodeName);
                NodeUtil.renameNode(node, newNodeName);
            }
        }
    }
}
