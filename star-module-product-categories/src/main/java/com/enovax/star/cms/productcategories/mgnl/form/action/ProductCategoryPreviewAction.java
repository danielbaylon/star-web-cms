package com.enovax.star.cms.productcategories.mgnl.form.action;

import com.enovax.star.cms.commons.mgnl.definition.RemoteChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.vaadin.data.Item;
import com.vaadin.ui.UI;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.location.LocationController;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Created by jennylynsze on 4/6/16.
 */
public class ProductCategoryPreviewAction extends AbstractAction<ProductCategoryPreviewActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(ProductCategoryPreviewAction.class);

    private final ProductCategoryPreviewActionDefinition definition;
    private final Item nodeItemToEdit;
    private final LocationController locationController;
    private final ContentConnector contentConnector;
    private final UiContext uiContext;
    private final StarTemplatingFunctions templatingFunctions;

    @Inject
    protected ProductCategoryPreviewAction(ProductCategoryPreviewActionDefinition definition, Item nodeItemToEdit, LocationController locationController, ContentConnector contentConnector, UiContext uiContext, StarTemplatingFunctions templatingFunctions) {
        super(definition);
        this.definition = definition;
        this.nodeItemToEdit = nodeItemToEdit;
        this.locationController = locationController;
        this.contentConnector = contentConnector;
        this.uiContext = uiContext;
        this.templatingFunctions = templatingFunctions;
    }

    @Override
    public void execute() throws ActionExecutionException {
        try {
            Object itemId = contentConnector.getItemId(nodeItemToEdit);
            if (!contentConnector.canHandleItem(itemId)) {
                log.warn("ProductCategoryPreviewAction requested for a node type definition {}. Current node type is {}. No action will be performed.", getDefinition(), String.valueOf(itemId));
                return;
            }

            String channel = this.definition.getChannel();
            Node productCategoryNode = ((JcrNodeAdapter) this.nodeItemToEdit).getJcrItem();

            if(RemoteChannels.B2C_SLM.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getProperty("generatedURLPath").getString())
                             .replace("{1}", productCategoryNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-B2C_SLM", true);
                }
            }else if(RemoteChannels.B2C_MFLG.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getName());

                    Node productNode = templatingFunctions.getFirstProduct(RemoteChannels.B2C_MFLG.getChannel(), productCategoryNode.getName());
                    if(productNode != null) {
                        url = url.replace("{1}", productNode.getName());
                    }else {
                        url = url.replace("{1}", "");
                    }

                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-B2C_MFLG", true);
                }
            }else if(RemoteChannels.KIOSK_SLM.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-KIOSK_SLM", true);
                }
            }else if(RemoteChannels.KIOSK_MFLG.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-KIOSK_MFLG", true);
                }
            }else if(RemoteChannels.PP_SLM.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getProperty("generatedURLPath").getString())
                            .replace("{1}", productCategoryNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-PP_SLM", true);
                }
            }else if(RemoteChannels.PP_MFLG.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productCategoryNode.getProperty("generatedURLPath").getString())
                            .replace("{1}", productCategoryNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Category-Preview-PP_MFLG", true);
                }
            }

        } catch (Exception e) {
            uiContext.openNotification(MessageStyleTypeEnum.ERROR, true, "Error encountered. Could not execute the Preview feature.");
            throw new ActionExecutionException("Could not execute ProductCategoryPreviewAction: ", e);
        }
    }
}
