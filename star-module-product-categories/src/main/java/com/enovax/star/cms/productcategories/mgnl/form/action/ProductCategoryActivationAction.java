package com.enovax.star.cms.productcategories.mgnl.form.action;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.api.overlay.ConfirmationCallback;
import info.magnolia.ui.framework.action.ActivationAction;
import info.magnolia.ui.framework.action.ActivationActionDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class ProductCategoryActivationAction<D extends ProductCategoryActivationActionDefinition> extends ActivationAction {

    private static final Logger log = LoggerFactory.getLogger(ProductCategoryActivationAction.class);

    private Node categoryNode;
    private EventBus admincentralEventBus;
    private UiContext uiContext;
    private SimpleTranslator i18n;
    private JcrItemId changedItemId;
    private List<Node> productNodes = new ArrayList<>();

    public ProductCategoryActivationAction(ActivationActionDefinition definition, List<JcrItemAdapter>  items, CommandsManager commandsManager, @Named("admincentral") EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, items, commandsManager, admincentralEventBus, uiContext, i18n);

        if(items != null) {
            this.categoryNode =  ((JcrNodeAdapter)items.get(0)).getJcrItem();
        }

        this.uiContext = uiContext;
        this.i18n = i18n;
        this.admincentralEventBus = admincentralEventBus;
        this.changedItemId = items.isEmpty() ? null : items.get(0).getItemId();

    }

    public ProductCategoryActivationAction(ActivationActionDefinition definition, JcrItemAdapter item, CommandsManager commandsManager, EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, item, commandsManager, admincentralEventBus, uiContext, i18n);
    }

    //TODO might consider doing it on pre execute to check the related products thingy
    @Override
    protected void onPostExecute() throws Exception {
        Context context = MgnlContext.getInstance();
        // yes, this is inverted, because a chain returns false when it is finished.
        boolean success = !(Boolean) context.getAttribute(COMMAND_RESULT);

        if(success && StringUtils.isNotEmpty(NodeUtil.getPathIfPossible(categoryNode))) {

            boolean hasUnpublishProducts = false;
            if(categoryNode.isNodeType(JcrWorkspace.ProductCategories.getNodeType())) {
                publishRelatedCategoryImages();  //for category images
                List<Node> subCategoryNodes = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductDefaultSubCategories.getNodeType()));
                Iterable<Node> subCategoryNodesIterator = NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType());

                if(subCategoryNodes != null) {
                    subCategoryNodes.addAll(NodeUtil.asList(subCategoryNodesIterator));
                }

                for(Node sub: subCategoryNodes) {
                    if(hasUnpublishedProduct(sub)) {
                        hasUnpublishProducts = true;
                    }
                }

            }else {
                //for sub category
                if(hasUnpublishedProduct(categoryNode)) {
                    hasUnpublishProducts = true;
                }
            }


            if(hasUnpublishProducts) {
                this.uiContext.openConfirmation(MessageStyleTypeEnum.INFO, "Publish Product", "There's related product that has not been publish. Do you want to publish it?", "Yes", "No", false, new ConfirmationCallback() {
                    @Override
                    public void onCancel() {
                        //do nothing
                        try {
                            doShowSuccessMessage(success);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onSuccess() {
                        try {
                            for(Node productNode: productNodes) {
                                try {
                                    String uuid = productNode.getIdentifier();
                                    PublishingUtil.publishNodes(uuid, JcrWorkspace.CMSProducts.getWorkspaceName()); //publish product
                                    PublishingUtil.publishProductRelatedNodes(NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid));
                                }catch (Exception e) {
                                    log.error(e.getMessage(), e);
                                }

                            }
                            doShowSuccessMessage(success);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }else {
                doShowSuccessMessage(success);
            }
        } else {
            doShowSuccessMessage(success);
        }

    }

    private boolean hasUnpublishedProduct(Node subCategoryNode) {
        try {
            if(subCategoryNode.isNodeType(JcrWorkspace.ProductSubCategories.getNodeType()) ||
                    subCategoryNode.isNodeType(JcrWorkspace.ProductDefaultSubCategories.getNodeType())) {

               List<Node> relatedProductList = NodeUtil.asList(NodeUtil.getNodes(subCategoryNode, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
               for(Node relatedProduct: relatedProductList) {
                   String uuid = relatedProduct.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();

                   if(StringUtils.isNotBlank(uuid) && uuid.startsWith("jcr:")) {
                       uuid = uuid.substring(4);
                   }

                   Node productNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);

                   if(!PublishingUtil.isPublish(productNode)) {
                       this.productNodes.add(productNode);
                   }
               }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        if(productNodes.size() > 0) {
            return true;
        }

        return false;
    }

    private void publishImages(String uuid) throws Exception {
        if(StringUtils.isNotBlank(uuid) && uuid.startsWith("jcr:")) {
            uuid = uuid.substring(4);
        }
        Node imageNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Dam.getWorkspaceName(), uuid);
        if (!PublishingUtil.isPublish(imageNode)) {
            try {
                //traverse all the parents && published it first
                Node parentNode = imageNode.getParent();
                while(parentNode != null && StringUtils.isNotEmpty(parentNode.getName())) {
                    if(!PublishingUtil.isPublish(parentNode)) {
                        PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.Dam.getWorkspaceName());
                    }
                    try {
                        parentNode = parentNode.getParent();
                    }catch(Exception e) {
                        parentNode = null;
                    }
                }
                PublishingUtil.publishNodes(uuid, JcrWorkspace.Dam.getWorkspaceName());
            }catch(Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
    private void publishRelatedCategoryImages() throws  Exception{
        if(categoryNode.isNodeType(JcrWorkspace.ProductCategories.getNodeType())
                && categoryNode.hasProperty(CMSProductCategoriesProperties.IconImage.getPropertyName())) {
            String uuid = categoryNode.getProperty(CMSProductCategoriesProperties.IconImage.getPropertyName()).getString();
            publishImages(uuid);
        }
    }

    private void publishRelatedProductImages(Node productNode) throws  Exception{
        if(productNode.hasNode(CMSProductProperties.ProductImages.getPropertyName())) {
            Node productImagesNode =  productNode.getNode(CMSProductProperties.ProductImages.getPropertyName());
            List<Node> relatedProductImagesNodes =  NodeUtil.asList(NodeUtil.getNodes(productImagesNode));
            for(Node relatedProductImageNode : relatedProductImagesNodes) {
                if (relatedProductImageNode.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
                    String uuid = relatedProductImageNode.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString();
                    publishImages(uuid);
                }
            }
        }
    }

    public void doShowSuccessMessage(boolean success) throws  Exception{
        admincentralEventBus.fireEvent(new ContentChangedEvent(changedItemId));
        //boolean successFalse = !(Boolean) context.getAttribute(COMMAND_RESULT);
        // yes, this is inverted, because a chain returns false when it is finished.
        String message = i18n.translate(getMessage(success));
        MessageStyleTypeEnum messageStyleType = success ? MessageStyleTypeEnum.INFO : MessageStyleTypeEnum.ERROR;

        if (StringUtils.isNotBlank(message)) {
            uiContext.openNotification(messageStyleType, true, message);
        }
    }
}
