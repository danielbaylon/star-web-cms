package com.enovax.star.cms.productcategories.mgnl.form.action;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import info.magnolia.cms.core.Path;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/17/16.
 */
public class SaveProductCategoryDialogAction extends SaveDialogAction<SaveProductCategoryDialogActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(SaveProductCategoryDialogAction.class);

    //constant to be used
    private static final String defaultNodeName = "default";
    private static final String defaultDisplayName = "Default";
    private static final String NAME = "name";
    private static final String CMS_PRODUCT_GENERATED_URL_PATH = "generatedURLPath";

    public SaveProductCategoryDialogAction(SaveProductCategoryDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
        super(definition, item, validator, callback);
    }

    @Override
    public void execute() throws ActionExecutionException {
        this.validator.showValidation(true);
        if(this.validator.isValid()) {
            JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;

            try {
                Node e = itemChanged.applyChanges();
                this.setNodeName(e, itemChanged);

                if(e.hasProperty(NAME)) {
                    String name = e.getProperty(NAME).getString();
                    if(StringUtils.isNotEmpty(name)) {
                        String url = name.toLowerCase();
                        url = url.replaceAll(" ", "-"); //replace all space to -
                        url = url.replaceAll("[^\\w-]", "");  //remove non alphanumeric and -

                        if(url.length() > 20) {
                            url = url.substring(0, 20); //limit to only 20
                        }

                        e.setProperty(CMS_PRODUCT_GENERATED_URL_PATH, url);
                    }
                }

                e.getSession().save();

                //Create a "Default" folder on create of category
                final Node defaultNode = JcrRepository.createNode(JcrWorkspace.ProductDefaultSubCategories.getWorkspaceName(), e.getPath(), defaultNodeName , JcrWorkspace.ProductDefaultSubCategories.getNodeType());

                if(defaultNode != null) {
                    defaultNode.setProperty("name", defaultDisplayName);
                    defaultNode.getSession().save();
                }

            } catch (RepositoryException var3) {
                throw new ActionExecutionException(var3);
            }

            this.callback.onSuccess(getDefinition().getName());
        } else {
            log.info("Validation error(s) occurred. No save performed.");
        }

    }

    //TODO check uniquess with node in other channels
    @Override
    protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {
        if(item instanceof JcrNewNodeAdapter) {
            String newNodeName =  RandomStringUtils.random(5, true, true);

            //do not make it start with number please
            while(newNodeName.matches("[0-9]{1}[a-zA-Z0-9]*")) {
                newNodeName =  RandomStringUtils.random(5, true, true);
            }

            newNodeName = Path.getUniqueLabel(node.getSession(), node.getParent().getPath(), Path.getValidatedLabel(newNodeName));
            item.setNodeName(newNodeName);
            NodeUtil.renameNode(node, newNodeName);
        }
    }
}
