package com.enovax.star.cms.productcategories.mgnl.workbench.contentview.column;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.ui.Table;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.StatusColumnFormatter;
import info.magnolia.ui.workbench.column.definition.StatusColumnDefinition;

import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.security.AccessControlException;

/**
 * Created by jennylynsze on 4/26/16.
 */
public class ProductUnderSubCategoryStatusFormatter extends AbstractColumnFormatter<StatusColumnDefinition> {

    protected final SimpleTranslator i18n;

    @Inject
    public ProductUnderSubCategoryStatusFormatter(StatusColumnDefinition definition, SimpleTranslator i18n) {
        super(definition);
        this.i18n = i18n;
    }

    @Override
    public Object generateCell(Table source, Object itemId, Object columnId) {
        Item jcrItem = this.getJcrItem(source, itemId);
        if(jcrItem != null && jcrItem.isNode()) {
            Node node = (Node)jcrItem;
            try {
                if(node.isNodeType(JcrWorkspace.RelatedCMSProductForCategories.getNodeType())) {
                   if(node.hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
                       String uuid = node.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();
                       Node productNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);
                       return generateCell(productNode);
                   }
                }else {
                    return generateCell(node);
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        } else {
            return null;
        }
        return null;
    }

    private Object generateCell(Node node) {
        String activationStatus = "";
        String activationStatusMessage = "";
        String permissionStatus = "";

        // activation status
        if(((StatusColumnDefinition)this.definition).isActivation()) {
            Integer e;
            try {
                e = Integer.valueOf(NodeTypes.Activatable.getActivationStatus(node));
            } catch (RepositoryException var14) {
                e = Integer.valueOf(0);
            }

            StatusColumnFormatter.ActivationStatus activationType;
            switch(e.intValue()) {
                case 1:
                    activationType = StatusColumnFormatter.ActivationStatus.MODIFIED;
                    activationStatusMessage = this.i18n.translate("activation-status.columns.modified", new Object[0]);
                    break;
                case 2:
                    activationType = StatusColumnFormatter.ActivationStatus.ACTIVATED;
                    activationStatusMessage = this.i18n.translate("activation-status.columns.activated", new Object[0]);
                    break;
                default:
                    activationType = StatusColumnFormatter.ActivationStatus.NOT_ACTIVATED;
                    activationStatusMessage = this.i18n.translate("activation-status.columns.not-activated", new Object[0]);
            }

            activationStatus = "<span class=\"" + activationType.getStyleName() + "\" title=\"" + activationStatusMessage + "\"></span>";
            activationStatus = activationStatus + "<span class=\"hidden-for-aria\">" + activationStatusMessage + "</span>";
        }

        if(((StatusColumnDefinition)this.definition).isPermissions()) {
            try {
                node.getSession().checkPermission(node.getPath(), "add_node,remove,set_property");
            } catch (AccessControlException var12) {
                permissionStatus = "<span class=\"icon-read-only\"></span>";
            } catch (RepositoryException var13) {
                throw new RuntimeException("Could not access the JCR permissions for the following node identifier ..." + node, var13);
            }
        }

        return activationStatus + permissionStatus;
    }

//    @Override
//    public Object generateCell(Table source, Object itemId, Object columnId) {
//        Item jcrItem = this.getJcrItem(source, itemId);
//        if(jcrItem != null && jcrItem.isNode()) {
//            Node node = (Node)jcrItem;
//            try {
//                if(node.isNodeType(JcrWorkspace.RelatedCMSProductForCategories.getNodeType())) {
//
//                }else {
//                    return super.generateCell(source, itemId, columnId);
//                }
//            } catch (RepositoryException e) {
//                e.printStackTrace();
//            }
//        } else {
//            return null;
//        }
//        return null;
//    }
//
//
//    @Override
//    public Object generateCell(Table source, Object itemId, Object columnId) {
//
//        final Item jcrItem = getJcrItem(source, itemId);
//        if (jcrItem != null && jcrItem.isNode()) {
//            Node node = (Node) jcrItem;
//
//            String activationStatus = "";
//            String activationStatusMessage = "";
//            String permissionStatus = "";
//
//            // activation status
//            if (definition.isActivation()) {
//                activationStatus += "icon-shape-circle activation-status ";
//
//                Integer status;
//                try {
//                    status = NodeTypes.Activatable.getActivationStatus(node);
//                } catch (RepositoryException e) {
//                    status = NodeTypes.Activatable.ACTIVATION_STATUS_NOT_ACTIVATED;
//                }
//
//                switch (status) {
//                    case NodeTypes.Activatable.ACTIVATION_STATUS_MODIFIED:
//                        activationStatus += "color-yellow";
//                        activationStatusMessage =  i18n.translate("activation-status.columns.modified");
//                        break;
//                    case NodeTypes.Activatable.ACTIVATION_STATUS_ACTIVATED:
//                        activationStatus += "color-green";
//                        activationStatusMessage =  i18n.translate("activation-status.columns.activated");
//                        break;
//                    default:
//                        activationStatus += "color-red";
//                        activationStatusMessage =  i18n.translate("activation-status.columns.not-activated");
//                }
//                activationStatus = "<span class=\"" + activationStatus + "\" title=\"" + activationStatusMessage + "\"></span>";
//                activationStatus = activationStatus + "<span class=\"hidden-for-aria\">" + activationStatusMessage + "</span>";
//            }
//
//            // permission status
//            if (definition.isPermissions()) {
//                try {
//                    node.getSession().checkPermission(node.getPath(), Session.ACTION_ADD_NODE + "," + Session.ACTION_REMOVE + "," + Session.ACTION_SET_PROPERTY);
//                } catch (AccessControlException e) {
//                    permissionStatus = "<span class=\"icon-read-only\"></span>";
//                } catch (RepositoryException e) {
//                    throw new RuntimeException("Could not access the JCR permissions for the following node identifier " + itemId, e);
//                }
//            }
//
//            return activationStatus + permissionStatus;
//        }
//        return null;
//    }


}
