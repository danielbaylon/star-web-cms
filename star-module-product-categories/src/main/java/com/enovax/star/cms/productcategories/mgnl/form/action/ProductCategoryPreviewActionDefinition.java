package com.enovax.star.cms.productcategories.mgnl.form.action;

import info.magnolia.ui.contentapp.detail.action.AbstractItemActionDefinition;

/**
 * Created by jennylynsze on 4/6/16.
 */
public class ProductCategoryPreviewActionDefinition  extends AbstractItemActionDefinition {

    private String channel;
    private String rootPath;
    private String workspace;

    public ProductCategoryPreviewActionDefinition() {
        setImplementationClass(ProductCategoryPreviewAction.class);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getRootPath() {
        return rootPath;
    }

    public String getWorkspace() {
        return workspace;
    }
}