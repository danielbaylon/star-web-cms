package com.enovax.star.cms.productcategories.mgnl.form.field.definition;


import com.enovax.star.cms.productcategories.mgnl.form.field.transformer.ProductCategoryMultiValueTransformer;
import info.magnolia.ui.form.field.definition.MultiValueFieldDefinition;

/**
 * Created by jennylynsze on 3/15/16.
 */
public class ProductCategoryMultiValueFieldDefinition extends MultiValueFieldDefinition {

    private String channel;


    public ProductCategoryMultiValueFieldDefinition() {
        setTransformerClass(ProductCategoryMultiValueTransformer.class);
    }

}
