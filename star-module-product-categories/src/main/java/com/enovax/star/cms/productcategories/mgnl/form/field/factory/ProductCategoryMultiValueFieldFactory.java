package com.enovax.star.cms.productcategories.mgnl.form.field.factory;


import com.enovax.star.cms.productcategories.mgnl.form.field.definition.ProductCategoryMultiValueFieldDefinition;
import com.vaadin.data.Item;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;
import info.magnolia.ui.form.field.factory.MultiValueFieldFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/15/16.
 */
public class ProductCategoryMultiValueFieldFactory<T extends ProductCategoryMultiValueFieldDefinition> extends MultiValueFieldFactory<T> {

    @Inject
    public ProductCategoryMultiValueFieldFactory(ProductCategoryMultiValueFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, fieldFactoryFactory, componentProvider);
    }
}
