package com.enovax.star.cms.productcategories.mgnl.form.field.transformer;

import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.multi.MultiValueChildrenNodeTransformer;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/15/16.
 */
public class ProductCategoryMultiValueTransformer extends MultiValueChildrenNodeTransformer {

    @Inject
    public ProductCategoryMultiValueTransformer(Item relatedFormItem, ConfiguredFieldDefinition definition, Class<PropertysetItem> type, I18NAuthoringSupport i18NAuthoringSupport) {
        super(relatedFormItem, definition, type,i18NAuthoringSupport);
        //set the child node type
        childNodeType = "mgnl:cms-product-for-category";
    }

    /*
    @Override
    protected String createChildItemName(Set<String> childNames, Object value, JcrNodeAdapter rootItem) {
        return value.toString();
    }
    */
}
