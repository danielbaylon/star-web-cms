package com.enovax.star.cms.productcategories.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.productcategories.mgnl.form.field.definition.ProductSubCategoryNameTextFieldDefinition;
import com.vaadin.data.Item;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.TextFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class ProductSubCategoryNameTextFieldFactory extends TextFieldFactory {

    @Inject
    public ProductSubCategoryNameTextFieldFactory(ProductSubCategoryNameTextFieldDefinition definition, Item relatedFieldItem,UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);

        if(JcrWorkspace.ProductDefaultSubCategories.getNodeType().equals(((JcrNodeAdapter) relatedFieldItem).getPrimaryNodeTypeName())) {
            definition.setReadOnly(true);
            definition.setI18n(false);
            definition.setRequired(false);
        }else{
            definition.setReadOnly(false);
            definition.setI18n(true);
            definition.setRequired(true);
        }
    }
}
