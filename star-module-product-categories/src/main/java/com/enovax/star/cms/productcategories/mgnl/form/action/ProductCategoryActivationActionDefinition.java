package com.enovax.star.cms.productcategories.mgnl.form.action;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class ProductCategoryActivationActionDefinition extends ActivationActionDefinition {

    public ProductCategoryActivationActionDefinition() {
        setImplementationClass(ProductCategoryActivationAction.class);
    }
}
