package com.enovax.util.jcr.exception;

/**
 * Created by jace on 21/9/15.
 */
public class JcrException extends Exception {

  public JcrException(String message) {
    super(message);
  }

  public JcrException(String message, Throwable cause) {
    super(message, cause);
  }
}
