package com.enovax.util.jcr.annotation;

import info.magnolia.jcr.util.NodeTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by jace on 27/7/16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface JcrSequence {
  String workspace();

  String path();

  String propertyName();

  String nodeType() default NodeTypes.Content.NAME;
}
