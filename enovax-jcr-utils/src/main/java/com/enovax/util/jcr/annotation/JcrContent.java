package com.enovax.util.jcr.annotation;

import info.magnolia.jcr.util.NodeTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by jace on 22/9/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD, ElementType.TYPE})
public @interface JcrContent {
  String path() default "";

  String nodeType() default NodeTypes.Content.NAME;

  String subNodeType() default NodeTypes.Content.NAME;
}
