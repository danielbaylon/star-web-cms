package com.enovax.util.jcr.publishing;

import info.magnolia.commands.CommandsManager;
import info.magnolia.commands.chain.Command;
import info.magnolia.context.Context;
import info.magnolia.context.SimpleContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.objectfactory.Components;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class PublishingUtil {

    private static final Logger log = LoggerFactory.getLogger(PublishingUtil.class);

    public static void publishNodes(String uuid, String workspaceName) throws Exception {
        String cmdName = "activate";
        Command cmd = Components.getComponent(CommandsManager.class).getCommand("", cmdName);
        Context ctx = new SimpleContext();
        Map params = new HashMap<>();
        params.put("repository", workspaceName);
        params.put("uuid", uuid);
        params.put("recursive", true);

        ctx.putAll(params);
        cmd.execute(ctx);
    }

    public static void deleteNodeAndPublish(String path, String workspaceName) throws Exception {
        String cmdName = "delete";

        Command cmd = Components.getComponent(CommandsManager.class).getCommand("", cmdName);
        Context ctx = new SimpleContext();
        Map params = new HashMap<>();
        params.put("repository", workspaceName);
        params.put("path", path);

        ctx.putAll(params);
        cmd.execute(ctx);
    }

    public static void deleteNodeAndPublish(Node node, String workspaceName) throws Exception {
        deleteNodeAndPublish(node.getPath(), workspaceName);
    }

    public static boolean isPublish(Node node) {
//        try {
//            return node.hasProperty("mgnl:activationStatus")
//                    && "true".equals(node.getProperty("mgnl:activationStatus").getString());
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
        Integer e;
        try {
            e = Integer.valueOf(NodeTypes.Activatable.getActivationStatus(node));
        } catch (RepositoryException var13) {
            e = Integer.valueOf(0);
        }

        switch (e.intValue()) {
            case 1:
                return false;
            case 2:
                return true;
            default:
                return false;
        }
    }
}
