package com.enovax.util.jcr.repository;

import info.magnolia.jcr.util.ContentMap;

import java.util.List;
import java.util.Map;

/**
 * Created by jace on 29/7/16.
 */
public interface JcrReadOnlyRepository<E> {
  E find(String name);

  E find(int id);

  List<E> getAll();

  int countAll();

  List<E> find(String where, Map<String, Object> params, String order, Boolean asc, int limit, int offset);

  List<E> find(String where, Map<String, Object> params, String order, Boolean asc, Long limit, Long offset);

  int count(String where, Map<String, Object> params);

  List<E> findByProperty(String property, Object value);

  E findUniqueByProperty(String property, Object value);

  ContentMap findContent(String name);

  ContentMap findContent(int id);
}
