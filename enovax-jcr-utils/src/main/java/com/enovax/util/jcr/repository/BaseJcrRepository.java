package com.enovax.util.jcr.repository;


import com.enovax.util.jcr.annotation.*;
import com.enovax.util.jcr.exception.JcrException;
import com.enovax.util.jcr.publishing.PublishingUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.StringValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * Created by jace on 17/9/15.
 */
public abstract class BaseJcrRepository<E> implements JcrRepository<E>, JcrReadOnlyRepository<E> {

    protected static final String SLASH = "/";
    protected static final String DEFAULT_PATH_NODE_TYPE = NodeTypes.Content.NAME;
    protected static final String NODE_NAME = "NODE_NAME";

    protected final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * @return Workspace where the repository operates.
     */
    protected abstract String getJcrWorkspace();

    /**
     * @return Relative path of the repository nodes from the root of the workspace.
     */
    protected abstract String getJcrPath();

    /**
     * @return New instance of the repository item.
     */
    protected abstract E getNewInstance();

    /**
     * A primary node type or mixin is needed to facilitate easy and fast retrieval of data. The primary node type should be unique within the workspace where it is used.
     *
     * @return The mixin used to identify the repository item.
     */
    protected abstract String getPrimaryNodeType();

    protected boolean hasPrimaryNodeType() {
        return StringUtils.isNotBlank(getPrimaryNodeType());
    }

    /**
     * @return JCR session to be used by the repository.
     * @throws RepositoryException
     */
    protected final Session getSession() throws RepositoryException {
        return getSession(getJcrWorkspace());
    }

    protected final Session getSession(String workspace) throws RepositoryException {
        return MgnlContext.getJCRSession(workspace);
    }

    /**
     * @param jcrPath  The path of the node.
     * @param name     The name of the node.
     * @param absolute True if you want the path to be absolute, false if otherwise.
     * @return The node path.
     */
    protected final String getPath(String jcrPath, String name, boolean absolute) {
        StringBuilder sb = new StringBuilder();
        if (absolute) {
            sb.append(SLASH);
        }
        if (StringUtils.isNotBlank(jcrPath)) {
            sb.append(jcrPath);
            sb.append(SLASH);
        }
        if (StringUtils.isNotBlank(name)) {
            sb.append(name);
        }
        log.warn("JCR Path Accessed: " + sb.toString());
        return sb.toString();
    }

    /**
     * Gets all the repository items.
     *
     * @return List of all the repository items.
     */
    public List<E> getAll() {
        return find(null, null, null, null, null, null);
    }

    @Override
    public int countAll() {
        return getAll().size();
    }

    /**
     * Gets a specific repository item based on its name.
     *
     * @param name Name of the item.
     * @return The item if it exists, null if it doesn't.
     */
    public E find(String name) {
        try {
            if (StringUtils.isNotBlank(getPrimaryNodeType())) {
                Node item = findByPrimaryNodeType(getJcrWorkspace(), name, getPrimaryNodeType());
                if (item != null) {
                    return getElementFromNode(item);
                }
            } else {
                Node item = findByPath(getJcrWorkspace(), getJcrPath(), name);
                if (item != null) {
                    return getElementFromNode(item);
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving JCR record.", e);
        }
        return null;
    }

    public E find(int id) {
        return find(Integer.toString(id));
    }

    /**
     * Save an item to the repository.
     *
     * @param element The item to be saved.
     * @return True if the item is new, false if the item is updated.
     * @throws JcrException
     */
    public boolean save(E element) throws JcrException {
        return save(element, getNameFromAnnotation(element));
    }

    public boolean save(E element, String name) throws JcrException {
        boolean isCreate;
        try {
            Node node;
            if (StringUtils.isNotBlank(getPrimaryNodeType())) {
                node = findByPrimaryNodeType(getJcrWorkspace(), name, getPrimaryNodeType());
            } else {
                node = findByPath(getJcrWorkspace(), getJcrPath(), name);
            }

            Session session = getSession();

            if (node != null) {
                isCreate = false;
                updateNode(element, node);
            } else {
                isCreate = true;
                assignAutoGeneratedValues(element);
                node = createNode(element, session.getRootNode(), getJcrPath(), getNameFromAnnotation(element), getPrimaryNodeType());
            }
            session.save();
            PublishingUtil.publishNodes(node.getIdentifier(), getJcrWorkspace());
        } catch (Exception e) {
            log.error("Error in saving node: " + name, e);
            throw new JcrException("Error saving JCR record.", e);
        }
        return isCreate;
    }


    /**
     * Update a node.
     *
     * @param newObj The new values to be saved.
     * @return True if there are any changes in the node. False if otherwise.
     * @throws Exception
     */
    protected final Node updateNode(Object newObj, Node node) throws Exception {
        if (!isContent(newObj)) {
            throw new JcrException("Unable to find JcrContent annotation for class: " + newObj.getClass().getName());
        }


        Object oldObj = newObj.getClass().newInstance();
        fillBeanFromNode(oldObj, node);
        boolean hasChanges = false;
        for (Field field : newObj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(JcrSequence.class)) {
                continue;
            }
            if (field.isAnnotationPresent(JcrProperty.class)) {
                Object oldValue = getPropertyValue(field, oldObj);
                Object newValue = getPropertyValue(field, newObj);
                if (oldValue == null && newValue != null) {
                    PropertyUtil.setProperty(node, getPropertyName(field), newValue);
                    hasChanges = true;
                } else if (oldValue != null && newValue == null) {
                    PropertyUtil.setProperty(node, getPropertyName(field), newValue);
                    hasChanges = true;
                } else if (oldValue != null && newValue != null && !oldValue.equals(newValue)) {
                    PropertyUtil.setProperty(node, getPropertyName(field), newValue);
                    hasChanges = true;
                }
            } else if (field.isAnnotationPresent(JcrLink.class)) {
                field.setAccessible(true);
                Object oldChildNodes = field.get(oldObj);
                Object newChildNodes = field.get(newObj);
                String childNodePath = getSubPath(field);
                Node subNode;
                if (!node.hasNode(childNodePath)) {
                    String nodeType = getNodeTypeFromAnnotation(field);
                    subNode = node.addNode(childNodePath, nodeType);
                    hasChanges = true;
                } else {
                    subNode = node.getNode(childNodePath);
                }
                if (newChildNodes != null) {
                    String subNodeType = getSubNodeTypeFromAnnotation(field);
                    if (newChildNodes instanceof List<?>) {
                        for (Object childNode : (List) newChildNodes) {
                            if (!subNode.hasNode(getNameFromAnnotation(childNode))) {
                                subNode.addNode(getNameFromAnnotation(childNode), subNodeType);
                                hasChanges = true;
                            }
                        }
                    } else if (field.isAnnotationPresent(JcrContent.class)) {
                        String oldLinkName = getNameFromAnnotation(oldChildNodes);
                        String newLinkName = getNameFromAnnotation(newChildNodes);
                        if (!StringUtils.equals(oldLinkName, newLinkName)) {
                            removeAll(subNode);
                            subNode.addNode(newLinkName, subNodeType);
                            hasChanges = true;
                        }
                    } else {
                        throw new JcrException("Invalid field annotated with JcrLink: " + field.getName());
                    }
                } else {
                    if (newChildNodes instanceof List<?>) {
                        if (subNode.hasNodes()) {
                            removeAll(subNode);
                            hasChanges = true;
                        }
                    } else if (field.getType().isAnnotationPresent(JcrContent.class)) {
                        if (subNode.hasNodes()) {
                            removeAll(subNode);
                            hasChanges = true;
                        }
                    } else {
                        throw new JcrException("Invalid field annotated with JcrLink: " + field.getName());
                    }
                }
                if (oldChildNodes != null) {
                    if (oldChildNodes instanceof List<?>) {
                        for (Object oldChildNode : (List) oldChildNodes) {
                            boolean deleteOld = true;
                            String oldChildName = getNameFromAnnotation(oldChildNode);
                            for (Object newChildNode : (List) newChildNodes) {
                                if (oldChildName.equals(getNameFromAnnotation(newChildNode))) {
                                    deleteOld = false;
                                    break;
                                }
                            }
                            if (deleteOld) {
                                remove(node, childNodePath, oldChildName);
                                hasChanges = true;
                            }
                        }
                    } else if (field.getType().isAnnotationPresent(JcrContent.class)) {
                        // Do nothing.
                    } else {
                        throw new JcrException("Invalid field annotated with JcrLink: " + field.getName());
                    }
                }
            } else if (field.isAnnotationPresent(JcrContent.class)) {
                field.setAccessible(true);
                Object oldChildNodes = field.get(oldObj);
                Object newChildNodes = field.get(newObj);
                String childNodePath = getSubPath(field);
                if (newChildNodes != null) {
                    if (!node.hasNode(childNodePath)) {
                        node.addNode(childNodePath, getNodeTypeFromAnnotation(field));
                        if (newChildNodes instanceof List<?>) {
                            String subNodeType = getSubNodeTypeFromAnnotation(field);
                            for (Object childNode : (List) newChildNodes) {
                                assignAutoGeneratedValues(childNode);
                                createNode(childNode, node, childNodePath, getNameFromAnnotation(childNode), subNodeType);
                            }
                        } else if (newChildNodes instanceof Map<?, ?>) {
                            String subNodeType = getSubNodeTypeFromAnnotation(field);
                            for (Map.Entry entry : (Set<Map.Entry>) ((Map) newChildNodes).entrySet()) {
                                assignAutoGeneratedValues(entry.getValue());
                                createNode(entry.getValue(), node, childNodePath, (String) entry.getKey(), subNodeType);
                            }
                        } else {
                            throw new JcrException("Invalid field annotated with JcrContent: " + field.getName());
                        }
                        hasChanges = true;
                    } else {
                        boolean hasChildChanges = false;
                        if (newChildNodes instanceof List<?>) {
                            String subNodeType = getSubNodeTypeFromAnnotation(field);
                            for (Object childNode : (List) newChildNodes) {
                                String childNodePathWithName = getSubContentPath(field, childNode);
                                if (node.hasNode(childNodePathWithName)) {
                                    updateNode(childNode, node.getNode(childNodePathWithName));
                                } else {
                                    assignAutoGeneratedValues(childNode);
                                    createNode(childNode, node, childNodePath, getNameFromAnnotation(childNode), subNodeType);
                                }
                            }
                            if (hasChildChanges) {
                                hasChanges = true;
                            }
                        } else if (newChildNodes instanceof Map<?, ?>) {
                            String subNodeType = getSubNodeTypeFromAnnotation(field);
                            for (Map.Entry entry : (Set<Map.Entry>) ((Map) newChildNodes).entrySet()) {
                                String childNodePathWithName = getPath(getSubPath(field), (String) entry.getKey(), true);
                                if (node.hasNode(childNodePathWithName)) {
                                    updateNode(entry.getValue(), node.getNode(childNodePathWithName));
                                } else {
                                    assignAutoGeneratedValues(entry.getValue());
                                    createNode(entry.getValue(), node, childNodePath, (String) entry.getKey(), subNodeType);
                                }
                            }
                            if (hasChildChanges) {
                                hasChanges = true;
                            }
                        } else {
                            throw new JcrException("Invalid field annotated with JcrContent: " + field.getName());
                        }
                    }
                    if (oldChildNodes != null) {
                        if (oldChildNodes instanceof List<?>) {
                            for (Object oldChildNode : (List) oldChildNodes) {
                                boolean deleteOld = true;
                                String oldChildName = getNameFromAnnotation(oldChildNode);
                                for (Object newChildNode : (List) newChildNodes) {
                                    if (oldChildName.equals(getNameFromAnnotation(newChildNode))) {
                                        deleteOld = false;
                                        break;
                                    }
                                }
                                if (deleteOld) {
                                    remove(node, childNodePath, oldChildName);
                                    hasChanges = true;
                                }
                            }
                        } else if (oldChildNodes instanceof Map<?, ?>) {
                            for (Map.Entry entry : (Set<Map.Entry>) ((Map) oldChildNodes).entrySet()) {
                                if (!((Map) newChildNodes).containsKey(entry.getKey())) {
                                    remove(node, childNodePath, (String) entry.getKey());
                                    hasChanges = true;
                                }
                            }
                        } else {
                            throw new JcrException("Invalid field annotated with JcrContent: " + field.getName());
                        }
                    }
                } else if (oldChildNodes != null) {
                    removeAll(node.getNode(childNodePath));
                    hasChanges = true;
                }
            }
        }
        if (hasChanges) {
            NodeTypes.LastModified.update(node);
        }
        return node;
    }

    /**
     * Find the name of the node of an object based on the JcrName annotation.
     *
     * @param obj An object that has the JcrName annotation.
     * @return The node name that corresponds to the object.
     * @throws JcrException When there is no JcrName annotation.
     */
    protected final String getNameFromAnnotation(Object obj) throws JcrException {
        String name = null;
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(JcrName.class)) {
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }
                try {
                    if (field.getType().equals(Integer.TYPE)) {
                        name = Integer.toString((Integer) field.get(obj));
                    } else if (field.getType().equals(Long.TYPE)) {
                        name = Long.toString((Long) field.get(obj));
                    } else {
                        name = (String) field.get(obj);
                    }

                } catch (Exception e) {
                    log.error("Error finding node name for class: " + obj.getClass().getName(), e);
                    throw new JcrException("Error finding node name for class: " + obj.getClass().getName(), e);
                }
            }
        }
        return name;
    }

    /**
     * @param obj An object.
     * @return True if the object has a JcrContent annotation. False if otherwise.
     */
    protected final boolean isContent(Object obj) {
        return obj.getClass().isAnnotationPresent(JcrContent.class);
    }

    /**
     * Create a node.
     *
     * @param obj      The object to be created.
     * @param rootNode The parent node.
     * @param jcrPath  The path of the node to create.
     * @param name     The name of the node to create.
     * @throws Exception
     */
    protected final Node createNode(Object obj, Node rootNode, String jcrPath, String name, String primaryNodeType) throws Exception {
        if (!isContent(obj)) {
            throw new JcrException("Unable to find JcrContent annotation for class: " + obj.getClass().getName());
        }
        String path = getPath(jcrPath, name, false);
        if (StringUtils.isNotBlank(jcrPath) && !rootNode.hasNode(jcrPath)) {
            NodeUtil.createPath(rootNode, jcrPath, DEFAULT_PATH_NODE_TYPE);
        }
        Node node = rootNode.addNode(path, primaryNodeType);
        List<String> mixins = getMixinsFromAnnotation(obj);
        for (String mixin : mixins) {
            node.addMixin(mixin);
        }
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(JcrProperty.class)) {
                field.setAccessible(true);
                PropertyUtil.setProperty(node, getPropertyName(field), getPropertyValue(field, obj));
            } else if (field.isAnnotationPresent(JcrLink.class)) {
                field.setAccessible(true);
                Object childNodes = field.get(obj);
                if (childNodes != null) {
                    String childNodePath = getSubPath(field);
                    Node subNode;
                    if (!node.hasNode(childNodePath)) {
                        String nodeType = getNodeTypeFromAnnotation(field);
                        subNode = node.addNode(childNodePath, nodeType);
                    } else {
                        subNode = node.getNode(childNodePath);
                    }
                    String subNodeType = getSubNodeTypeFromAnnotation(field);
                    if (childNodes instanceof List<?>) {
                        for (Object childNode : (List) childNodes) {
                            subNode.addNode(getNameFromAnnotation(childNode), subNodeType);
                        }
                    } else if (childNodes.getClass().isAnnotationPresent(JcrContent.class)) {
                        subNode.addNode(getNameFromAnnotation(childNodes), subNodeType);
                    } else {
                        throw new JcrException("Invalid field annotated with JcrLink: " + field.getName());
                    }
                }
            } else if (field.isAnnotationPresent(JcrContent.class)) {
                field.setAccessible(true);
                Object childNodes = field.get(obj);
                if (childNodes != null) {
                    String childNodePath = getSubPath(field);
                    if (!node.hasNode(childNodePath)) {
                        String nodeType = getNodeTypeFromAnnotation(field);
                        node.addNode(childNodePath, nodeType);
                    }
                    if (childNodes instanceof List<?>) {
                        String subNodeType = getSubNodeTypeFromAnnotation(field);
                        for (Object childNode : (List) childNodes) {
                            assignAutoGeneratedValues(childNode);
                            createNode(childNode, node, childNodePath, getNameFromAnnotation(childNode), subNodeType);
                        }
                    } else if (childNodes instanceof Map<?, ?>) {
                        String subNodeType = getSubNodeTypeFromAnnotation(field);
                        for (Map.Entry entry : (Set<Map.Entry>) ((Map) childNodes).entrySet()) {
                            assignAutoGeneratedValues(entry.getValue());
                            createNode(entry.getValue(), node, childNodePath, (String) entry.getKey(), subNodeType);
                        }
                    } else {
                        throw new JcrException("Invalid field annotated with JcrContent: " + field.getName());
                    }
                }
            }
        }
        NodeTypes.Created.set(node);
        NodeTypes.LastModified.update(node);
        return node;
    }

    /**
     * @param field The field to get the value of. Must have the JcrProperty annotation.
     * @param obj   The object to get the value from.
     * @return The value of the field.
     * @throws JcrException If there is no JcrProperty annotation.
     */
    protected final Object getPropertyValue(Field field, Object obj) throws JcrException {
        JcrProperty jcrProperty = field.getAnnotation(JcrProperty.class);
        Object propertyValue;
        if (jcrProperty == null) {
            throw new JcrException("Unable to find JcrProperty annotation for field: " + field);
        } else {
            field.setAccessible(true);
            try {
                propertyValue = field.get(obj);
            } catch (Exception e) {
                log.error("Error retrieving value for field: " + field, e);
                throw new JcrException("Error retrieving value for field: " + field, e);
            }
        }
        if (propertyValue instanceof Integer) {
            return ((Integer) propertyValue).longValue();
        }
        return propertyValue;
    }

    /**
     * Find the property name that corresponds to the field. If there is no name specified in the JcrProperty annotation, the name of the field is used instead.
     *
     * @param field The field to get the property name from. Must have the JcrProperty annotation.
     * @return The property name of the field.
     * @throws JcrException If there is no JcrProperty annotation.
     */
    protected final String getPropertyName(Field field) throws JcrException {
        JcrProperty jcrProperty = field.getAnnotation(JcrProperty.class);
        String propertyName;
        if (jcrProperty == null) {
            throw new JcrException("Unable to find JcrProperty annotation for field: " + field);
        } else if (StringUtils.isNotBlank(jcrProperty.name())) {
            propertyName = jcrProperty.name();
        } else {
            propertyName = field.getName();
        }
        return propertyName;
    }

    /**
     * Find the path of child nodes related to the collection field. If there is no path specified in the JcrContent or JcrLink annotation, the name of the field is used instead.
     *
     * @param field The collection field to get the property name from. Must have the JcrContent annotation.
     * @return The path to the child nodes related to the field.
     * @throws JcrException If there is no JcrContent nor JCcrLink annotation.
     */
    protected final String getSubPath(Field field) throws JcrException {
        String subPath;
        if (field.isAnnotationPresent(JcrContent.class)) {
            subPath = field.getAnnotation(JcrContent.class).path();
        } else if (field.isAnnotationPresent(JcrLink.class)) {
            subPath = field.getAnnotation(JcrLink.class).path();
        } else {
            throw new JcrException("Unable to find JcrContent nor JcrLink annotation for field: " + field);
        }
        if (StringUtils.isBlank(subPath)) {
            subPath = field.getName();
        }
        return subPath;
    }

    /**
     * Find the relative path of the child node to a parent node.
     *
     * @param field The collection field that holds the child node. Must have the JcrContent annotation.
     * @param child The child node. Must have the JcrName annotation.
     * @return The relative path to the child node.
     * @throws JcrException
     */
    protected final String getSubContentPath(Field field, Object child) throws JcrException {
        return getPath(getSubPath(field), getNameFromAnnotation(child), false);
    }

    /**
     * Converts a node to a repository item.
     *
     * @param node The node that represents a repository item.
     * @return An instance of the repository item based on the node.
     * @throws Exception
     */
    protected E getElementFromNode(Node node) throws Exception {
        E element = getNewInstance();
        fillBeanFromNode(element, node);
        return element;
    }

    /**
     * Populates the fields of a bean that has the JcrProperty or JcrContent annotation based on the properties of a node.
     *
     * @param obj  A simple bean with JcrProperty or JcrContent annotations.
     * @param node A node with properties corresponding the the fields in the object.
     * @throws Exception
     */
    protected void fillBeanFromNode(Object obj, Node node) throws Exception {
        if (!isContent(obj)) {
            throw new JcrException("Unable to find JcrContent annotation for class: " + obj.getClass().getName());
        }
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(JcrName.class)) {
                String name = node.getName();
                field.setAccessible(true);
                if (field.getType().equals(Integer.TYPE)) {
                    field.set(obj, Integer.parseInt(name));
                } else if (field.getType().equals(Long.TYPE)) {
                    field.set(obj, Long.parseLong(name));
                } else if (field.getType().isAssignableFrom(String.class)) {
                    field.set(obj, name);
                } else {
                    throw new JcrException("Invalid field annotated with JcrName: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrCreated.class)) {
                if (field.getType().isAssignableFrom(Date.class)) {
                    field.setAccessible(true);
                    field.set(obj, NodeTypes.Created.getCreated(node).getTime());
                } else {
                    throw new JcrException("Invalid field annotated with JcrCreated: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrCreatedBy.class)) {
                if (field.getType().isAssignableFrom(String.class)) {
                    field.setAccessible(true);
                    field.set(obj, NodeTypes.Created.getCreatedBy(node));
                } else {
                    throw new JcrException("Invalid field annotated with JcrCreatedBy: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrLastModified.class)) {
                if (field.getType().isAssignableFrom(Date.class)) {
                    field.setAccessible(true);
                    field.set(obj, NodeTypes.LastModified.getLastModified(node).getTime());
                } else {
                    throw new JcrException("Invalid field annotated with JcrLastModified: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrLastModifiedBy.class)) {
                if (field.getType().isAssignableFrom(String.class)) {
                    field.setAccessible(true);
                    field.set(obj, NodeTypes.LastModified.getLastModifiedBy(node));
                } else {
                    throw new JcrException("Invalid field annotated with JcrLastModifiedBy: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrProperty.class)) {
                String name = getPropertyName(field);
                field.setAccessible(true);

                if (field.getType().equals(Boolean.TYPE) || field.getType().isAssignableFrom(Boolean.class)) {
                    field.set(obj, PropertyUtil.getBoolean(node, name, false));
                } else if (field.getType().isAssignableFrom(Date.class)) {
                    Calendar date = PropertyUtil.getDate(node, name);
                    if (date != null) {
                        field.set(obj, date.getTime());
                    }
                } else if (field.getType().equals(Long.TYPE) || field.getType().isAssignableFrom(Long.class)) {
                    field.set(obj, PropertyUtil.getLong(node, name));
                } else if (field.getType().equals(Integer.TYPE) || field.getType().isAssignableFrom(Integer.class)) {
                    Long longValue = PropertyUtil.getLong(node, name);
                    if (longValue != null) {
                        field.set(obj, longValue.intValue());
                    }
                } else if (field.getType().isAssignableFrom(String.class)) {
                    field.set(obj, PropertyUtil.getString(node, name));
                } else {
                    throw new JcrException("Invalid field annotated with JcrProperty: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrLink.class)) {
                String path = getSubPath(field);
                JcrLink jcrLink = field.getAnnotation(JcrLink.class);
                if (List.class.isAssignableFrom(field.getType())) {
                    field.setAccessible(true);

                    ParameterizedType type = (ParameterizedType) field.getGenericType();
                    Class paramClass = (Class) type.getActualTypeArguments()[0];
                    List list = new ArrayList();
                    if (node.hasNode(path)) {
                        NodeIterator iterator = node.getNode(path).getNodes();
                        while (iterator.hasNext()) {
                            String linkedName = iterator.nextNode().getName();
                            Node next = findLinkedNode(jcrLink.linkedWorkspace(), jcrLink.linkedPath(), linkedName, jcrLink.linkedNodeType());
                            Object listItem = paramClass.newInstance();
                            fillBeanFromNode(listItem, next);
                            list.add(listItem);
                        }
                        field.set(obj, list);
                    }
                } else if (field.getType().isAnnotationPresent(JcrContent.class)) {
                    if (node.hasNode(path) && node.getNode(path).hasNodes()) {
                        field.setAccessible(true);
                        String linkedName = node.getNode(path).getNodes().nextNode().getName();
                        Node linkedNode = findLinkedNode(jcrLink.linkedWorkspace(), jcrLink.linkedPath(), linkedName, jcrLink.linkedNodeType());
                        if (linkedNode != null) {
                            fillBeanFromNode(field.getType().newInstance(), node);
                        }
                    }
                } else {
                    throw new JcrException("Invalid field annotated with JcrLink: " + field.getName());
                }
            } else if (field.isAnnotationPresent(JcrContent.class)) {
                String path = getSubPath(field);
                field.setAccessible(true);

                if (List.class.isAssignableFrom(field.getType())) {
                    ParameterizedType type = (ParameterizedType) field.getGenericType();
                    Class paramClass = (Class) type.getActualTypeArguments()[0];
                    List list = new ArrayList();
                    if (node.hasNode(path)) {
                        NodeIterator iterator = node.getNode(path).getNodes();
                        while (iterator.hasNext()) {
                            Node next = iterator.nextNode();
                            Object listItem = paramClass.newInstance();
                            fillBeanFromNode(listItem, next);
                            list.add(listItem);
                        }
                        field.set(obj, list);
                    }
                } else if (Map.class.isAssignableFrom(field.getType())) {
                    ParameterizedType type = (ParameterizedType) field.getGenericType();
                    Class paramClass = (Class) type.getActualTypeArguments()[1];
                    Map map = new HashMap();

                    if (node.hasNode(path)) {
                        NodeIterator iterator = node.getNode(path).getNodes();
                        while (iterator.hasNext()) {
                            Node next = iterator.nextNode();
                            String name = NodeUtil.getName(next);
                            Object mapItem = paramClass.newInstance();
                            fillBeanFromNode(mapItem, next);
                            map.put(name, mapItem);
                        }
                        field.set(obj, map);
                    }
                } else {
                    throw new JcrException("Invalid field annotated with JcrContent: " + field.getName());
                }
            }
        }
    }

    protected List<E> runQuery(String workspace, String queryString, String language, Map<String, Object> params, int limit, int offset) {
        return runQuery(workspace, queryString, language, params, new Long(limit), new Long(offset));
    }


    /**
     * Runs a JCR query.
     *
     * @param queryString
     * @param language
     * @param params
     * @param limit
     * @param offset
     * @return
     */
    protected List<E> runQuery(String workspace, String queryString, String language, Map<String, Object> params, Long limit, Long offset) {
        List<E> results = new ArrayList<>();
        try {
            Session session = getSession(workspace);
            ValueFactory vf = session.getValueFactory();
            QueryManager manager = session.getWorkspace().getQueryManager();
            log.warn("Query to execute: " + queryString);
            Query query = manager.createQuery(queryString, language);
            if (params != null && params.size() > 0) {
                for (String key : params.keySet()) {
                    query.bindValue(key, PropertyUtil.createValue(params.get(key), vf));
                }
            }
            if (limit != null && limit > 0) {
                query.setLimit(limit);
            }
            if (limit != null && offset > 0) {
                query.setOffset(offset);
            }
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            while (iterator.hasNext()) {
                Node node = iterator.nextNode();
                E element = getElementFromNode(node);
                results.add(element);
            }
        } catch (Exception e) {
            log.error("Error retrieving JCR record.", e);
        }
        return results;
    }

    /**
     * Gets the mixin type related to the object.
     *
     * @param obj An object.
     * @return The mixin types related to the object, null if there is no JcrMixin annotation.
     */
    protected List<String> getMixinsFromAnnotation(Object obj) {
        if (obj instanceof Field) {
            Field field = (Field) obj;
            if (field.isAnnotationPresent(JcrMixin.class)) {
                return Arrays.asList(field.getAnnotation(JcrMixin.class).values());
            }
        } else if (obj.getClass().isAnnotationPresent(JcrMixin.class)) {
            return Arrays.asList(obj.getClass().getAnnotation(JcrMixin.class).values());
        }
        return Collections.emptyList();
    }

    /**
     * Gets the node type related to the object.
     *
     * @param obj An object.
     * @return The node type related to the object, null if there is no JcrContent nor JcrLink annotation.
     */
    protected String getNodeTypeFromAnnotation(Object obj) {
        String nodeType = null;
        if (obj instanceof Field) {
            Field field = (Field) obj;
            if (field.isAnnotationPresent(JcrContent.class)) {
                nodeType = field.getAnnotation(JcrContent.class).nodeType();
            } else if (field.isAnnotationPresent(JcrLink.class)) {
                nodeType = field.getAnnotation(JcrLink.class).nodeType();
            }
        } else if (obj.getClass().isAnnotationPresent(JcrContent.class)) {
            nodeType = obj.getClass().getAnnotation(JcrContent.class).nodeType();
        }
        return nodeType;
    }

    /**
     * Gets the sub node type related to the object.
     *
     * @param obj An object.
     * @return The sub node type related to the object, null if there is no JcrContent nor JcrLink annotation.
     */
    protected String getSubNodeTypeFromAnnotation(Object obj) {
        String nodeType = null;
        if (obj instanceof Field) {
            Field field = (Field) obj;
            if (field.isAnnotationPresent(JcrContent.class)) {
                nodeType = field.getAnnotation(JcrContent.class).subNodeType();
            } else if (field.isAnnotationPresent(JcrLink.class)) {
                nodeType = field.getAnnotation(JcrLink.class).subNodeType();
            }
        } else if (obj.getClass().isAnnotationPresent(JcrContent.class)) {
            nodeType = obj.getClass().getAnnotation(JcrContent.class).subNodeType();
        }
        return nodeType;
    }

    @Override
    public List<E> find(String where, Map<String, Object> params, String order, Boolean asc, int limit, int offset) {
        return find(where, params, order, asc, new Long(limit), new Long(offset));
    }

    @Override
    public List<E> find(String where, Map<String, Object> params, String order, Boolean asc, Long limit, Long offset) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from [");
        if (hasPrimaryNodeType()) {
            sb.append(getPrimaryNodeType());
        } else {
            return Collections.emptyList();
        }
        sb.append("]");
        if (StringUtils.isNotBlank(where)) {
            sb.append(" where ");
            sb.append(where);
        }
        if (StringUtils.isNotBlank(order)) {
            sb.append(" order by ");
            sb.append(order);
            if (BooleanUtils.isFalse(asc)) {
                sb.append(" desc");
            } else {
                sb.append(" asc");
            }
        }
        log.warn("Executed query: " + sb.toString());
        return runQuery(getJcrWorkspace(), sb.toString(), Query.JCR_SQL2, params, limit, offset);
    }

    @Override
    public int save(E... objs) throws JcrException {
        int newRecords = 0;
        for (E obj : objs) {
            boolean isNew = save(obj);
            if (isNew) {
                newRecords++;
            }
        }
        return newRecords;
    }

    @Override
    public int count(String where, Map<String, Object> params) {
        return find(where, params, null, null, null, null).size();
    }

    @Override
    public int removeAll() throws JcrException {
        try {
            Session session = getSession();
            int removed = removeAll(session.getRootNode().getNode(getJcrPath()));
            session.save();
            return removed;
        } catch (Exception e) {
            log.error("Unable to remove all nodes.", e);
            throw new JcrException("Unable to remove all nodes.", e);
        }
    }

    @Override
    public int remove(String... names) throws JcrException {
        try {
            Session session = getSession();
            int removed = 0;
            Node rootNode = session.getRootNode();
            for (String name : names) {
                if (remove(rootNode, getJcrPath(), name)) {
                    removed++;
                }
            }
            session.save();
            return removed;
        } catch (Exception e) {
            log.error("Unable to remove node.", e);
            throw new JcrException("Unable to remove node.", e);
        }
    }

    @Override
    public int remove(int... ids) throws JcrException {
        String[] names = new String[ids.length];
        for (int i = 0; i < ids.length; i++) {
            names[i] = String.valueOf(ids[i]);
        }
        return remove(names);
    }

    @Override
    public int remove(E... objs) throws JcrException {
        String[] names = new String[objs.length];
        for (int i = 0; i < objs.length; i++) {
            names[i] = getNameFromAnnotation(objs[i]);
        }
        return remove(names);
    }

    /**
     * Removes all sub nodes from the node.
     *
     * @param parentNode Node to remove from.
     * @return
     * @throws Exception
     */
    protected int removeAll(Node parentNode) throws Exception {
        int removed = 0;
        NodeIterator i = parentNode.getNodes();
        while (i.hasNext()) {
            Node node = i.nextNode();
            PublishingUtil.deleteNodeAndPublish(node, getJcrWorkspace());
            // node.remove();
            removed++;
        }
        return removed;
    }

    /**
     * Removes a specific sub node from the node.
     *
     * @param parentNode Node to remove from.
     * @param jcrPath    Path to the node to be removed.
     * @param name       Name of the node to be removed.
     * @return
     * @throws Exception
     */
    protected boolean remove(Node parentNode, String jcrPath, String name) throws Exception {
        String path = getPath(jcrPath, name, false);
        if (parentNode.hasNode(path)) {
            Node node = parentNode.getNode(path);
            PublishingUtil.deleteNodeAndPublish(node, getJcrWorkspace());
            // node.remove();
            return true;
        } else {
            return false;
        }
    }


    /**
     * Gets the next value from a sequence stored in the JCR repository. Creates the sequence if it does not yet exist.
     *
     * @param workspace    Workspace where the sequence is located.
     * @param path         Path to the sequence.
     * @param propertyName Property name of the sequence.
     * @param nodeType     Node type of the sequence when creating a new one.
     * @return
     * @throws JcrException
     */
    protected Long getNextSequence(String workspace, String path, String propertyName, String nodeType) throws JcrException {
        try {
            Session session = getSession(workspace);
            Node sequenceNode = JcrUtils.getOrCreateByPath(session.getRootNode(), path, false, nodeType, nodeType, true);
            Long nextSequence = JcrUtils.getLongProperty(sequenceNode, propertyName, 1);
            PropertyUtil.setProperty(sequenceNode, propertyName, Long.sum(nextSequence, 1));
            session.save();
            return nextSequence;
        } catch (Exception e) {
            log.error("Unable to get next sequence from: [" + workspace + "][" + path + "][" + propertyName + "]", e);
            throw new JcrException("Unable to get next sequence from: [" + workspace + "][" + path + "][" + propertyName + "]", e);
        }
    }

    /**
     * Assigns all auto-generated values in a JcrContent object
     *
     * @param obj
     * @throws JcrException
     */
    protected void assignAutoGeneratedValues(Object obj) throws JcrException {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(JcrSequence.class)) {
                assignFieldValueFromSequence(field, obj);
            }
        }
    }

    /**
     * Assigns the value of the field from a sequence stored in the JCR.
     *
     * @param field Field to be updated.
     * @param obj   Object to be updated.
     * @throws JcrException
     */
    protected void assignFieldValueFromSequence(Field field, Object obj) throws JcrException {
        JcrSequence sequence = field.getAnnotation(JcrSequence.class);
        Long nextSequence = getNextSequence(sequence.workspace(), sequence.path(), sequence.propertyName(), sequence.nodeType());
        if (nextSequence != null) {
            try {
                if (field.getType().equals(Long.TYPE) || field.getType().isAssignableFrom(Long.class)) {
                    field.setAccessible(true);
                    field.set(obj, nextSequence);
                } else if (field.getType().equals(Integer.TYPE) || field.getType().isAssignableFrom(Integer.class)) {
                    field.setAccessible(true);
                    field.set(obj, nextSequence.intValue());
                } else {
                    throw new JcrException("Invalid field annotated with JcrSequence: " + field.getName());
                }
            } catch (IllegalAccessException e) {
                log.error("Unable to set value from sequence for: " + field.getName(), e);
                throw new JcrException("Unable to set value from sequence for: " + field.getName(), e);
            }
        }
    }

    @Override
    public E findUniqueByProperty(String property, Object value) {
        List<E> result = findByProperty(property, value);
        if (result == null || result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    @Override
    public List<E> findByProperty(String property, Object value) {
        StringBuilder sb = new StringBuilder();
        sb.append(" ");
        sb.append(property);
        sb.append(" = $");
        sb.append(property);
        sb.append(" ");
        return find(sb.toString(), Collections.singletonMap(property, value), null, null, null, null);
    }

    /**
     * Retrieve linked node. If primary node type is specified, it will find the node using a jcr query. Otherwise, it will find it using the path.
     *
     * @param workspace       Workspace of the linked node.
     * @param jcrPath         Path to the linked node's parent.
     * @param name            Name of the linked node.
     * @param primaryNodeType Primary node type of the linked node.
     * @return The linked node if it was found, null if otherwise.
     */
    protected Node findLinkedNode(String workspace, String jcrPath, String name, String primaryNodeType) {
        try {
            if (StringUtils.isNotBlank(primaryNodeType)) {
                return findByPrimaryNodeType(workspace, name, primaryNodeType);
            } else {
                return findByPath(workspace, jcrPath, name);
            }
        } catch (Exception e) {
            log.error("Unable to find linked node.", e);
            return null;
        }
    }

    /**
     * Find a node in a specific path.
     *
     * @param workspace Workspace of the node.
     * @param jcrPath   Path to the node's parent.
     * @param name      Name of the node.
     * @return A node if one is found, null if otherwise.
     * @throws JcrException When there is an error getting the node.
     */
    protected Node findByPath(String workspace, String jcrPath, String name) throws JcrException {
        try {
            Session session = getSession(workspace);
            String path = getPath(jcrPath, name, true);
            if (session.getRootNode().hasNode(path)) {
                return session.getRootNode().getNode(path);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new JcrException("Error while trying to retrieve node by path.", e);
        }
    }

    /**
     * Find a node with a specific name and node type. If there are multiple results, the first one is returned.
     *
     * @param workspace       Workspace of the node.
     * @param name            Name of the node.
     * @param primaryNodeType Node type of the node.
     * @return A node if one was found, null if otherwise.
     * @throws JcrException When there is an error during execution of the query.
     */
    protected Node findByPrimaryNodeType(String workspace, String name, String primaryNodeType) throws JcrException {
        try {
            String queryString = "select * from [" + primaryNodeType + "] where NAME() = $" + NODE_NAME;
            Session session = getSession(workspace);
            QueryManager manager = session.getWorkspace().getQueryManager();
            Query query = manager.createQuery(queryString, Query.JCR_SQL2);
            query.bindValue(NODE_NAME, new StringValue(name));
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            if (iterator.hasNext()) {
                return iterator.nextNode();
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new JcrException("Error while trying to retrieve node by primary node type.", e);
        }
    }

    /**
     * Filter a list of objects to remove the objects which are already logical children of a specific node.
     *
     * @param parentWorkspace Workspace of the parent node.
     * @param pathToParent    Path to the parent node.
     * @param parentName      Name of the parent node.
     * @param parentNodeType  Node type of the parent node.
     * @param pathToChildren  Path to the node which contains the child nodes.
     * @param resultToFilter  The list of objects to filter.
     * @return A list of objects which are not logical children of the parent node.
     */
    protected List<E> filterNotLinkedTo(String parentWorkspace, String pathToParent, String parentName, String parentNodeType, String pathToChildren, List<E> resultToFilter) {
        if (resultToFilter != null && resultToFilter.size() > 0) {
            try {
                Node parentNode;
                if (StringUtils.isNotBlank(parentNodeType)) {
                    parentNode = findByPrimaryNodeType(parentWorkspace, parentName, parentNodeType);
                } else {
                    parentNode = findByPath(parentWorkspace, pathToParent, parentName);
                }
                if (parentNode != null) {
                    Node containerNode = parentNode.getNode(pathToChildren);
                    if (containerNode != null) {
                        Iterator<E> iterator = resultToFilter.iterator();
                        while (iterator.hasNext()) {
                            String name = getNameFromAnnotation(iterator.next());
                            Iterator<Node> nodes = containerNode.getNodes();
                            while (nodes.hasNext()) {
                                if (name.equals(nodes.next().getName())) {
                                    iterator.remove();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                log.error("Error while filtering result.", e);
            }
        }
        return resultToFilter;
    }

    @Override
    public ContentMap findContent(String name) {
        try {
            if (StringUtils.isNotBlank(getPrimaryNodeType())) {
                Node item = findByPrimaryNodeType(getJcrWorkspace(), name, getPrimaryNodeType());
                if (item != null) {
                    return new ContentMap(item);
                }
            } else {
                Node item = findByPath(getJcrWorkspace(), getJcrPath(), name);
                if (item != null) {
                    return new ContentMap(item);
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving JCR record.", e);
        }
        return null;
    }

    @Override
    public ContentMap findContent(int id) {
        return findContent(Integer.toString(id));
    }
}
