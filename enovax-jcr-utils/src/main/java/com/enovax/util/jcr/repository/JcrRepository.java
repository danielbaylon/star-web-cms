package com.enovax.util.jcr.repository;

import com.enovax.util.jcr.exception.JcrException;

/**
 * Created by jace on 20/7/16.
 */
public interface JcrRepository<E> extends JcrReadOnlyRepository<E> {

  boolean save(E obj) throws JcrException;

  int save(E... objs) throws JcrException;

  int removeAll() throws JcrException;

  int remove(String... names) throws JcrException;

  int remove(int... ids) throws JcrException;

  int remove(E... objs) throws JcrException;
}
