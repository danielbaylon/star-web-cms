package com.enovax.star.cms.products.mgnl.workbench.search;

import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnectorDefinition;
import info.magnolia.ui.workbench.search.SearchJcrContainer;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by jennylynsze on 3/14/16.
 */
public class AXProductsBrowserSearchJcrContainer extends SearchJcrContainer {

    private boolean showActiveOnly = true;

    public AXProductsBrowserSearchJcrContainer(JcrContentConnectorDefinition definition) {
        super(definition);
    }


    public boolean isShowActiveOnly() {
        return showActiveOnly;
    }

    public void setShowActiveOnly(boolean showActiveOnly) {
        this.showActiveOnly = showActiveOnly;
    }

    @Override
    protected String getQueryWhereClauseSearch() {
        String whereClause = super.getQueryWhereClauseSearch();
        if(!StringUtils.isEmpty(this.getFullTextExpression())) {
            if(showActiveOnly) {
                return  SELECTOR_NAME + ".status = 'Active' and (" + whereClause + ")";
            }
            return whereClause;
        }else {
            if(showActiveOnly) {
                return  SELECTOR_NAME + ".status = 'Active'";
            }
        }
        return whereClause;
    }
}
