package com.enovax.star.cms.products.mgnl.form.field.validator;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 4/4/16.
 */
public class UniqueProductNameValidator extends AbstractStringValidator {

    private static final Logger log = LoggerFactory.getLogger(UniqueProductNameValidator.class);

    private final Item item;
    /**
     * Constructs a validator for strings.
     * <p>
     * <p>
     * Null and empty string values are always accepted. To reject empty values,
     * set the field being validated as required.
     * </p>
     *
     * @param errorMessage the message to be included in an {@link InvalidValueException}
     *                     (with "{0}" replaced by the value that failed validation).
     */
    public UniqueProductNameValidator(Item item, String errorMessage) {
        super(errorMessage);
        this.item = item;
    }

    @Override
    protected boolean isValidValue(String value) {
        if (item instanceof JcrNodeAdapter) {
            // If we're editing an existing node then its allowed to use the current username of course
            if (!(item instanceof JcrNewNodeAdapter)) {
                String currentName = PropertyUtil.getString(((JcrNodeAdapter)item).getJcrItem(), "name");
                if (StringUtils.equals(value, currentName)) {
                    return true;
                }
            }

            try {
                Iterable<Node> result = JcrRepository.query(JcrWorkspace.CMSProducts.getWorkspaceName(),
                        JcrWorkspace.CMSProducts.getNodeType(),
                        "//element(*," + JcrWorkspace.CMSProducts.getNodeType() + ")[@name='" + value + "']");
                if(result != null && result.iterator().hasNext()) {
                    return false;
                }else {
                    return true;
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
