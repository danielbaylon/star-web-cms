package com.enovax.star.cms.products.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.definition.FieldDefinition;

/**
 * Created by jennylynsze on 5/5/16.
 */
public class EditRelatedAxProductsButtonDefinition extends ConfiguredFieldDefinition implements FieldDefinition {
    private String dialogName;
    private String axProductUUID;

    public String getDialogName() {
        return dialogName;
    }

    public void setDialogName(String dialogName) {
        this.dialogName = dialogName;
    }

    public String getAxProductUUID() {
        return axProductUUID;
    }

    public void setAxProductUUID(String axProductUUID) {
        this.axProductUUID = axProductUUID;
    }
}
