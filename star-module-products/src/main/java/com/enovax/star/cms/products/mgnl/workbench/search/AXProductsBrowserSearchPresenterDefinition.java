package com.enovax.star.cms.products.mgnl.workbench.search;

import info.magnolia.ui.workbench.search.SearchPresenterDefinition;

/**
 * Created by jennylynsze on 3/14/16.
 */
public class AXProductsBrowserSearchPresenterDefinition extends SearchPresenterDefinition {
    public static final String VIEW_TYPE = "searchview";

    public AXProductsBrowserSearchPresenterDefinition() {
        this.setImplementationClass(AXProductsBrowserSearchPresenter.class);
        this.setViewType("searchview");
        this.setActive(false);
        this.setIcon("icon-view-list");
    }

}
