package com.enovax.star.cms.products.mgnl.workbench.contenttools;

import com.enovax.star.cms.products.mgnl.workbench.AXProductsBrowserWorkbenchPresenter;
import com.vaadin.data.Property;
import com.vaadin.data.util.ObjectProperty;
import info.magnolia.event.EventBus;
import info.magnolia.repository.RepositoryManager;
import info.magnolia.ui.api.view.View;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnector;
import info.magnolia.ui.workbench.contenttool.ContentToolPresenter;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/9/16.
 */
public class AXProductsBrowserContextTool implements ContentToolPresenter
{
    private final AXProductsBrowserContextToolView view;
    private final EventBus eventBus;
    private final RepositoryManager repositoryManager;
    private final JcrContentConnector relatedContentConnector;
    private final AXProductsBrowserWorkbenchPresenter workbenchPresenter;


    private final String CHANNEL_DISPLAY_NAME = "displayName";

    @Inject
    public AXProductsBrowserContextTool(EventBus eventBus, RepositoryManager repositoryManager, ContentConnector relatedContentConnector,  AXProductsBrowserWorkbenchPresenter workbenchPresenter, AXProductsBrowserContextToolView view) {
        this.view =  view;
        this.eventBus = eventBus;
        this.repositoryManager = repositoryManager;
        this.relatedContentConnector = ((JcrContentConnector)relatedContentConnector);
        this.workbenchPresenter = workbenchPresenter;
        this.workbenchPresenter.setBrowserContextTool(this);
    }

    public View start() {
       // final ObjectProperty channelProperty = new ObjectProperty("ALL");
        final ObjectProperty showActiveOnlyPropertiesProperty = new ObjectProperty(Boolean.valueOf(true));
//        this.view.setChannelOptions(populateChannelNames());
//        this.view.setChannelProperty(channelProperty);
        this.view.setShowActiveOnlyProperty(showActiveOnlyPropertiesProperty);
        this.view.setEnabled(true);


        //default
        AXProductsBrowserContextTool.this.workbenchPresenter.doFilterActiveOnly(true);


        //TODO to know how the channelproperty is tied up with the selector
        //TODO handling the query here
//        channelProperty.addValueChangeListener(new Property.ValueChangeListener() {
//            public void valueChange(Property.ValueChangeEvent event) {
//                System.out.println("Value Change to " + channelProperty.getValue());
//                //AXProductsBrowserContextTool.this.relatedContentConnector.getContentConnectorDefinition().getWorkspace();
//
//                AXProductsBrowserContextTool.this.workbenchPresenter.doSearchByChannel(channelProperty.getValue().toString());
//            }
//        });


        showActiveOnlyPropertiesProperty.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                AXProductsBrowserContextTool.this.workbenchPresenter.doFilterActiveOnly(((Boolean) showActiveOnlyPropertiesProperty.getValue()).booleanValue());
                //AXProductsBrowserContextTool.this.eventBus.fireEvent(new ShowActiveOnlyToggledEvent(((Boolean)showActiveOnlyPropertiesProperty.getValue()).booleanValue()));
            }
        });

        return this.view;
    }

//    private IndexedContainer populateChannelNames() {
//        IndexedContainer container = new IndexedContainer();
//        // for now manually add things here
//        //TODO read from workspace of remote channels
//        List<String> channelList = new ArrayList();
//
//        channelList.add("ALL");
//        Iterable<Node> channels = JcrRepository.getNodes(JcrWorkspace.RemoteChannels, CHANNEL_DISPLAY_NAME);
//        Iterator<Node> channelsIterator = channels.iterator();
//        while(channelsIterator.hasNext()) {
//            try {
//                channelList.add(channelsIterator.next().getProperty(CHANNEL_DISPLAY_NAME).getString());
//            } catch (RepositoryException e) {
//                e.printStackTrace();
//            }
//        }
//
//        for (String channel: channelList) {
//            container.addItem(channel);
//        }
//        return container;
//    }


//   // public String getSelectedChannel() {
//        return this.view.getChannelProperty();
//    }
}