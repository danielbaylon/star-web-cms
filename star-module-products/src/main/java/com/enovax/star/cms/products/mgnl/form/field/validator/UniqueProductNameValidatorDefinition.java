package com.enovax.star.cms.products.mgnl.form.field.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 4/4/16.
 */
public class UniqueProductNameValidatorDefinition extends ConfiguredFieldValidatorDefinition {

    public UniqueProductNameValidatorDefinition()
    {
        setFactoryClass(UniqueProductNameValidatorFactory.class);
    }
}