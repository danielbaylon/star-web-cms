package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class ManualSyncDialogActionDefinition extends ConfiguredActionDefinition {
    private String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public ManualSyncDialogActionDefinition() {
        setImplementationClass(ManualSyncDialogAction.class);
    }
}
