package com.enovax.star.cms.products.mgnl.workbench;


import com.enovax.star.cms.products.mgnl.workbench.contenttools.AXProductsBrowserContextTool;
import com.enovax.star.cms.products.mgnl.workbench.search.AXProductsBrowserSearchContentToolPresenter;
import com.enovax.star.cms.products.mgnl.workbench.search.AXProductsBrowserSearchPresenter;
import info.magnolia.event.EventBus;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.view.View;
import info.magnolia.ui.imageprovider.definition.ImageProviderDefinition;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.workbench.WorkbenchPresenter;
import info.magnolia.ui.workbench.WorkbenchStatusBarPresenter;
import info.magnolia.ui.workbench.WorkbenchView;
import info.magnolia.ui.workbench.WorkbenchViewImpl;
import info.magnolia.ui.workbench.contenttool.ContentToolDefinition;
import info.magnolia.ui.workbench.definition.WorkbenchDefinition;
import info.magnolia.ui.workbench.search.SearchPresenterDefinition;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by jennylynsze on 3/10/16.
 */
public class AXProductsBrowserWorkbenchPresenter extends WorkbenchPresenter {

    private AXProductsBrowserContextTool tool;

    private final WorkbenchView view;
    private final ComponentProvider componentProvider;

    @Inject
    public AXProductsBrowserWorkbenchPresenter(WorkbenchView view, ComponentProvider componentProvider, WorkbenchStatusBarPresenter statusBarPresenter, ContentConnector contentConnector)
    {
        super(view, componentProvider, statusBarPresenter, contentConnector);
        this.view = view;
        this.componentProvider = componentProvider;
    }

    public void setBrowserContextTool(AXProductsBrowserContextTool tool) {
        this.tool = tool;
    }
    public AXProductsBrowserContextTool getBrowserContextTool() {
        return this.tool;
    }

    public void doFilterActiveOnly(boolean showActiveOnly) {
        ((AXProductsBrowserSearchPresenter)getActivePresenter()).searchByActive(showActiveOnly);
    }

    @Override
    public void doSearch(String searchExpression) {
        super.doSearch(searchExpression);
    }

    protected List<Object> filterExistingItems(List<Object> itemIds) {
        List<Object> filteredIds = new ArrayList<Object>();
        Iterator<Object> it = itemIds.iterator();
        while (it.hasNext()) {
            Object itemId = it.next();
            if (contentConnector.canHandleItem(itemId) && contentConnector.getItem(itemId) != null) {
                filteredIds.add(itemId);
            }
        }
        return filteredIds;
    }


    @Override
    public WorkbenchView start(WorkbenchDefinition workbenchDefinition, ImageProviderDefinition imageProviderDefinition, EventBus eventBus) {
        WorkbenchView workbenchView = super.start(workbenchDefinition, imageProviderDefinition, eventBus);
        if (hasViewType(SearchPresenterDefinition.VIEW_TYPE)) {
            AXProductsBrowserSearchContentToolPresenter searchPresenter = componentProvider.newInstance(AXProductsBrowserSearchContentToolPresenter.class, this, getEventBus());
            View searchView = searchPresenter.start();
            ((WorkbenchViewImpl) workbenchView).addContentTool(searchView, ContentToolDefinition.Alignment.RIGHT, 0.4F);
        }
        return workbenchView;
    }

//    @Override
//    protected void addSearchContentTool() {
//        AXProductsBrowserSearchContentToolPresenter searchPresenter = componentProvider.newInstance(AXProductsBrowserSearchContentToolPresenter.class, this, getEventBus());
//        View searchView = searchPresenter.start();
//        ((WorkbenchViewImpl) this.view).addContentTool(searchView, ContentToolDefinition.Alignment.RIGHT, 0);
//    }
}
