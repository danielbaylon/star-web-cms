package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.productsync.service.SyncProduct;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.api.overlay.AlertCallback;
import info.magnolia.ui.framework.message.MessagesManager;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class ManualSyncDialogAction<T extends ManualSyncDialogActionDefinition> extends AbstractAction<T> {

    private static final Logger log = LoggerFactory.getLogger(ManualSyncDialogAction.class);

    private UiContext uiContext;
    private EventBus eventBus;
    private final MessagesManager messagesManager;
    private ManualSyncDialogActionDefinition definition;

    public ManualSyncDialogAction(T definition, UiContext uiContext,  @Named("admincentral") EventBus admincentralEventBus, MessagesManager messagesManager) {
        super(definition);
        this.uiContext = uiContext;
        this.eventBus = admincentralEventBus;
        this.messagesManager = messagesManager;
        this.definition = definition;
    }

    @Override
    public void execute() throws ActionExecutionException {
        ApiResult<String> status;

        String channel = definition.getChannel();

        StoreApiChannels channelEnum = StoreApiChannels.fromCode(channel);
        if(!SyncProduct.isSynchInProgress(channelEnum)) {
            status = SyncProduct.syncProduct(channelEnum);
            if(status.isSuccess()) {
                this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
                uiContext.openAlert(MessageStyleTypeEnum.INFO, "AX Manual Sync", "Manual Sync of AX product is successful.", "OK", new AlertCallback() {
                    @Override
                    public void onOk() {
                        //dont do anything loh
                    }
                });
            }else {
                SyncProduct.setSynchInProgress(channelEnum, false);
                this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
                uiContext.openAlert(MessageStyleTypeEnum.ERROR, "AX Manual Sync", "Error encountered on Manual Sync of AX product. Please check the logs.", "OK", new AlertCallback() {
                    @Override
                    public void onOk() {
                        //dont do anything loh
                    }
                });
            }
        }else {
            uiContext.openAlert(MessageStyleTypeEnum.ERROR, "AX Manual Sync", "There's already an on-going sync for AX product.  You are not allowed to perform the manual sync at this moment.", "OK", new AlertCallback() {
                @Override
                public void onOk() {
                    //dont do anything loh
                }
            });
        }

//        this.validator.showValidation(true);
//        if(this.validator.isValid()) {
//
//            JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;
//            String channel = (String) itemChanged.getItemProperty("channel").getValue();
//            ApiResult<String> status;
//
//            //TODO send some broadcast msg... how?
////            final Message message = new Message();
////            message.setMessage("Ax Synching....");
////            message.setSubject("Ax Sync");
////            message.setType(MessageType.INFO);
////            messagesManager.broadcastMessage(message);
//
////            if("all".equals(channel)) {
////                //status = SyncProduct.syncALLAXProduct();
////                status = status?SyncProduct.syncProduct(StoreApiChannels.B2C_SLM):status;
////                status = status?SyncProduct.syncProduct(StoreApiChannels.B2C_MFLG):status;
////                status = status?SyncProduct.syncProduct(StoreApiChannels.PARTNER_PORTAL_SLM):status;
////                status = status?SyncProduct.syncProduct(StoreApiChannels.PARTNER_PORTAL_MFLG):status;
////                status = status?SyncProduct.syncProduct(StoreApiChannels.KIOSK_SLM):status;
////                status = status?SyncProduct.syncProduct(StoreApiChannels.KIOSK_MFLG):status;
////            }else {
////                status = SyncProduct.syncProduct(StoreApiChannels.fromCode(channel));
////            }
//
//            StoreApiChannels channelEnum = StoreApiChannels.fromCode(channel);
//            if(!SyncProduct.isSynchInProgress(channelEnum)) {
////                StarModuleProductSync.addManualSyncScheduleTask(channelEnum);
////                this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
////                this.callback.onCancel();
////                uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "This sync will take time.. Please wait..... ");
//                status = SyncProduct.syncProduct(StoreApiChannels.fromCode(channel));
//                if(status.isSuccess()) {
//                    this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
//                    this.callback.onCancel();
//                    uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "Manual Sync of AX product is successful.");
//                }else {
//                    this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
//                    this.callback.onCancel();
//                    uiContext.openNotification(MessageStyleTypeEnum.ERROR, true, "Error encountered on Manual Sync of AX product. Please check the logs.");
//                }
//            }else {
//                System.out.println("SYNC IS ERRROR++++++++++++++++++");
//                this.eventBus.fireEvent(new ContentChangedEvent(null)); //forced refresh
//                this.callback.onCancel();
//                uiContext.openNotification(MessageStyleTypeEnum.ERROR, true, "There's an ongoing sync happening...");
//            }
//
//
//
//        } else {
//            log.info("Validation error(s) occurred. No save performed.");
//        }

    }
}
