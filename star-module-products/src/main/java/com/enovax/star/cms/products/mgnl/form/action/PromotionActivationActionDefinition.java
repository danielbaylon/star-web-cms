package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

/**
 * Created by jennylynsze on 9/24/16.
 */
public class PromotionActivationActionDefinition extends ActivationActionDefinition {

    public PromotionActivationActionDefinition() {
        setImplementationClass(PromotionActivationAction.class);
    }

}