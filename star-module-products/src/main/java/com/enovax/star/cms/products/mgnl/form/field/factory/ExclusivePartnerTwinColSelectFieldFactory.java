package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.api.store.component.StarModuleApiBeans;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerAccountService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerExclProdService;
import com.enovax.star.cms.products.mgnl.form.event.ProductLevelValueChangedEvent;
import com.enovax.star.cms.products.mgnl.form.field.definition.ExclusivePartnerTwinColSelectFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.Field;
import info.magnolia.event.EventBus;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.SelectFieldOptionDefinition;
import info.magnolia.ui.form.field.factory.TwinColSelectFieldFactory;
import info.magnolia.ui.form.field.transformer.TransformedProperty;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by jennylynsze on 1/6/17.
 */
public class ExclusivePartnerTwinColSelectFieldFactory<T extends ExclusivePartnerTwinColSelectFieldDefinition>
        extends TwinColSelectFieldFactory<T>  implements ProductLevelValueChangedEvent.Handler {

    private Node currentSelectedNode;

    private String channel;

    private final EventBus subAppEventBus;
    private final String productLevel;
    private String selectedValue;


    public ExclusivePartnerTwinColSelectFieldFactory(ExclusivePartnerTwinColSelectFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
        this.currentSelectedNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
        this.channel = definition.getChannel();

        this.subAppEventBus = subAppEventBus;
        this.subAppEventBus.addHandler(ProductLevelValueChangedEvent.class, this);
        this.productLevel = definition.getProductLevel();

        this.selectedValue = PropertyUtil.getString(this.currentSelectedNode, "productLevel");

    }

    @Override
    public void setPropertyDataSourceAndDefaultValue(Property<?> property) {
        //get from the exclusive partner mapping and assigned the value here

        String productId = null;

        try {
            productId = this.currentSelectedNode.getName();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        HashSet preSelectData = new HashSet();

        if(productId != null) {

            StarModuleApiBeans context = Components.getComponent(StarModuleApiBeans.class);

            if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channel)) {
                IPartnerExclProdService repo = context.getPPSLMPartnerExclProdService();
                try {
                    List<String> partnerIds = repo.getPartnerByExclProd(channel, productId);
                    for(String p: partnerIds) {
                        preSelectData.add(p);
                    }
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }else if(StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(channel)) {
                com.enovax.star.cms.partnershared.service.ppmflg.IPartnerExclProdService repo = context.getPPMFLGPartnerExclProdService();
                try {
                    List<String> partnerIds = repo.getPartnerByExclProd(channel, productId);
                    for(String p: partnerIds) {
                        preSelectData.add(p);
                    }
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }

        }

        TransformedProperty selectedProperites = (TransformedProperty) property;
        selectedProperites.setValue(preSelectData);

        super.setPropertyDataSourceAndDefaultValue(property);
    }

    //retrieve all the partners
    @Override
    public List<SelectFieldOptionDefinition> getSelectFieldOptionDefinition() {

        StarModuleApiBeans context = Components.getComponent(StarModuleApiBeans.class);
        List<SelectFieldOptionDefinition> options = new ArrayList<>();

        if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channel)) {
            IPartnerAccountService paSrv = context.getPPSLMPartnerAccountService();

            List<PartnerVM> partnerVMs = paSrv.getActivePartners();
            for(PartnerVM pa: partnerVMs) {
                SelectFieldOptionDefinition option = new SelectFieldOptionDefinition();
                option.setName(pa.getOrgName());
                option.setValue(pa.getId() + "");
                option.setLabel(pa.getOrgName());
                options.add(option);
            }
        }else if(StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(channel)) {
            com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService paSrv = context.getPPMFLGPartnerAccountService();

            List<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM> partnerVMs = paSrv.getActivePartners();
            for(com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM pa: partnerVMs) {
                SelectFieldOptionDefinition option = new SelectFieldOptionDefinition();
                option.setName(pa.getOrgName());
                option.setValue(pa.getId() + "");
                option.setLabel(pa.getOrgName());
                options.add(option);
            }
        }


        return options;

    }

    @Override
    public Field<Object> createField() {
        this.field = super.createField();
        setVisible();
        return this.field;
    }

    @Override
    public void onProductLevelValueChanged(ProductLevelValueChangedEvent event) {
        if (event.getSelectedValue() == null || StringUtils.isBlank(productLevel)) {
            return;
        }

        this.selectedValue = event.getSelectedValue();

        setVisible();
    }

    private void setVisible() {
        if(this.productLevel.equals(this.selectedValue)) {
            this.field.setVisible(true);
        }else {
            this.field.setVisible(false);
        }
    }
}