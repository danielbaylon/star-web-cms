package com.enovax.star.cms.products.mgnl.form.event;

import info.magnolia.event.Event;
import info.magnolia.event.EventHandler;

/**
 * Created by jennylynsze on 10/15/16.
 */
public class ProductLevelValueChangedEvent implements Event<ProductLevelValueChangedEvent.Handler> {

    private String selectedValue;

    public ProductLevelValueChangedEvent(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public interface Handler extends EventHandler {
        void onProductLevelValueChanged(ProductLevelValueChangedEvent event);
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    @Override
    public void dispatch(Handler handler) {
        handler.onProductLevelValueChanged(this);
    }

}
