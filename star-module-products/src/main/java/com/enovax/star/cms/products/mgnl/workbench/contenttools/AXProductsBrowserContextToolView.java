package com.enovax.star.cms.products.mgnl.workbench.contenttools;

import com.vaadin.data.Property;
import info.magnolia.ui.api.view.View;

/**
 * Created by jennylynsze on 3/9/16.
 */
public interface AXProductsBrowserContextToolView extends View
{
//    void setChannelOptions(Container paramContainer);
//    void setChannelProperty(Property<String> property);
    void setShowActiveOnlyProperty(Property<Boolean> showActiveOnlyProperty);
    void setEnabled(boolean paramBoolean);
//    String getChannelProperty();
}