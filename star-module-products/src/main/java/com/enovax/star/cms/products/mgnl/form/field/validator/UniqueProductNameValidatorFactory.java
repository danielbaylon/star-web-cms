package com.enovax.star.cms.products.mgnl.form.field.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 4/4/16.
 */
public class UniqueProductNameValidatorFactory extends AbstractFieldValidatorFactory<UniqueProductNameValidatorDefinition> {

    private Item item;

    public UniqueProductNameValidatorFactory(UniqueProductNameValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
    }

    @Override
    public Validator createValidator() {
        return new UniqueProductNameValidator(item, getI18nErrorMessage());
    }
}
