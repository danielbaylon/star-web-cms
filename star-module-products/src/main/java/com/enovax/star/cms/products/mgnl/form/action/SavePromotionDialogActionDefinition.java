package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 8/3/16.
 */
public class SavePromotionDialogActionDefinition  extends SaveDialogActionDefinition {

    public SavePromotionDialogActionDefinition() {
        setImplementationClass(SavePromotionDialogAction.class);
    }
}