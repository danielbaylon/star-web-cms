package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.api.store.component.StarModuleApiBeans;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.util.PublishingUtil;
import com.enovax.star.cms.partnershared.constant.ppmflg.ProductLevels;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerExclProdService;
import com.vaadin.data.Item;
import info.magnolia.cms.core.Path;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.overlay.AlertCallback;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.framework.message.MessagesManager;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class SaveCMSProductDialogAction extends SaveDialogAction<SaveCMSProductDialogActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(SaveCMSProductDialogAction.class);
    private static final String CMS_PRODUCT_GENERATED_URL_PATH = "generatedURLPath";
    private static final String NAME = "name";

    private final MessagesManager messagesManager;

    private String channel;
    private UiContext uiContext;

    public SaveCMSProductDialogAction(SaveCMSProductDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback, MessagesManager messagesManager, UiContext uiContext) {
        super(definition, item, validator, callback);
        this.messagesManager = messagesManager;
        this.channel = definition.getChannel();
        this.uiContext = uiContext;
    }

    @Override
    public void execute() throws ActionExecutionException {
        this.validator.showValidation(true);
        boolean exclErrorThrown = false;
        if(this.validator.isValid()) {

            JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;

            Node productNode = itemChanged.getJcrItem();
            String productLevelSaved = PropertyUtil.getString(productNode, CMSProductProperties.ProductLevel.getPropertyName());

            try {
                Node newProductNode = itemChanged.applyChanges();
                this.setNodeName(newProductNode, itemChanged);

                String productId = "";
                try {
                    productId = newProductNode.getName();
                } catch (RepositoryException e2) {
                    e2.printStackTrace();
                }

                if(newProductNode.hasProperty(NAME)) {
                    String name = newProductNode.getProperty(NAME).getString();
                    if(StringUtils.isNotEmpty(name)) {
                        String url = name.toLowerCase();
                        url = url.replaceAll(" ", "-"); //replace all space to -
                        url = url.replaceAll("[^\\w-]", "");  //remove non alphanumeric and -

                        if(url.length() > 20) {
                            url = url.substring(0, 20); //limit to only 20
                        }

                        newProductNode.setProperty(CMS_PRODUCT_GENERATED_URL_PATH, url);
                    }
                }

                newProductNode.getSession().save(); //save the node first

                if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(this.channel) ||
                        StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(this.channel)) {

                    String productLevelNow = PropertyUtil.getString(newProductNode, CMSProductProperties.ProductLevel.getPropertyName());
                    StarModuleApiBeans context = Components.getComponent(StarModuleApiBeans.class);

                    int appRequestCnt = 0;

                    if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(this.channel)) {
                        appRequestCnt = context.getPPSLMApprovalLogService().getPendingRequestForProdCnt(productId);
                    }else if(StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(this.channel)) {
                        appRequestCnt = context.getPPMFLGApprovalLogService().getPendingRequestForProdCnt(productId);
                    }

                    //exclusvie before, now it's not..
                    if(ProductLevels.Exclusive.toString().equals(productLevelSaved) &&
                            !ProductLevels.Exclusive.toString().equals(productLevelNow)) {

                        if(appRequestCnt > 0){
                            newProductNode.setProperty(CMSProductProperties.ProductLevel.getPropertyName(), ProductLevels.Exclusive.toString());
                            newProductNode.getSession().save(); //save again
                            exclErrorThrown = true;
                        }else {
                            //remove all exclusive
                            try {
                                List<Node> exclProdMappings = NodeUtil.asList(JcrRepository.query(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), JcrWorkspace.PartnerExclusiveProductMapping.getNodeType(), "/jcr:root/" + this.channel + "//element(*, mgnl:partner-excl-prod-mapping)[jcr:like(@exclProdIds, '%" + productId + "%')]"));
                                List<String> exclProdList = null;
                                List<String> exclProdListFinal = new ArrayList<>();
                                for(Node exclProd: exclProdMappings) {
                                    String exclProds = PropertyUtil.getString(exclProd, ("exclProdIds"));

                                    if(StringUtils.isNotEmpty(exclProds)) {
                                        exclProdList = Arrays.asList(exclProds.split("\\s*,\\s*"));
                                        if(exclProdList != null) {
                                            for(String excl: exclProdList) {
                                                if(!productId.equals(excl)) {
                                                    exclProdListFinal.add(excl);
                                                }
                                            }
                                            String exclProdIds = StringUtils.join(exclProdListFinal, ',');
                                            exclProd.setProperty("exclProdIds", exclProdIds);
                                            exclProd.getSession().save();
                                            PublishingUtil.publishNodes(exclProd.getIdentifier(), JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName());
                                        }
                                    }
                                }
                            }catch(Exception err) {
                                log.error(err.getMessage(), err);
                            }
                        }
                    }

                    //tiered before and now is not tiered
                    if(ProductLevels.Tiered.toString().equals(productLevelSaved) &&
                            !ProductLevels.Tiered.toString().equals(productLevelNow)) {
                        try {
                            List<Node> tierProductMappings = NodeUtil.asList(JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), JcrWorkspace.TierProductMappings.getNodeType(), "/jcr:root/" + this.channel + "//element(*,mgnl:tier-product-mapping)[@productId='" + newProductNode.getName() + "']"));
                            for(Node tierProduct: tierProductMappings) {
                                PublishingUtil.deleteNodeAndPublish(tierProduct, JcrWorkspace.TierProductMappings.getWorkspaceName());
                            }
                        }catch(Exception err) {
                            log.error(err.getMessage(), err);
                        }
                    }


                    //if product exclusive.. do this
                    if(ProductLevels.Exclusive.toString().equals(productLevelNow)) {
                        if(appRequestCnt > 0) {
                            exclErrorThrown = true;
                        }else {
                            //save it save it

                            if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(this.channel)) {
                                IPartnerExclProdService exclProdService = context.getPPSLMPartnerExclProdService();
                                List<String> partnersSaved = exclProdService.getPartnerByExclProd(this.channel, productId);  //saved
                                LinkedHashSet<String> partnersInput = (LinkedHashSet) itemChanged.getItemProperty("exclPartners").getValue();
                                List<String> partnerToBeAdded = new ArrayList<>();
                                List<String> partnerToBeRemoved = new ArrayList<>();

                                if(partnersSaved != null) {
                                    for(String s: partnersSaved) {
                                        if(!partnersInput.contains(s)) {
                                            partnerToBeRemoved.add(s);
                                        }
                                    }
                                }

                                if(partnersInput != null) {
                                    for(String i: partnersInput) {
                                        if(!partnersSaved.contains(i)) {
                                            partnerToBeAdded.add(i);
                                        }
                                    }
                                }

                                if(partnerToBeAdded.size() > 0 || partnerToBeRemoved.size() > 0) {
                                    exclProdService.processExclusive(this.channel, productId, partnerToBeAdded, partnerToBeRemoved, MgnlContext.getUser().getName());
                                }

                            }else if(StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(this.channel)) {
                                com.enovax.star.cms.partnershared.service.ppmflg.IPartnerExclProdService exclProdService = context.getPPMFLGPartnerExclProdService();
                                List<String> partnersSaved = exclProdService.getPartnerByExclProd(this.channel, productId);  //saved
                                LinkedHashSet<String> partnersInput = (LinkedHashSet) itemChanged.getItemProperty("exclPartners").getValue();
                                List<String> partnerToBeAdded = new ArrayList<>();
                                List<String> partnerToBeRemoved = new ArrayList<>();

                                if(partnersSaved != null) {
                                    for(String s: partnersSaved) {
                                        if(!partnersInput.contains(s)) {
                                            partnerToBeRemoved.add(s);
                                        }
                                    }
                                }

                                if(partnersInput != null) {
                                    for(String i: partnersInput) {
                                        if(!partnersSaved.contains(i)) {
                                            partnerToBeAdded.add(i);
                                        }
                                    }
                                }

                                if(partnerToBeAdded.size() > 0 || partnerToBeRemoved.size() > 0) {
                                    exclProdService.processExclusive(this.channel, productId, partnerToBeAdded, partnerToBeRemoved, MgnlContext.getUser().getName());
                                }
                            }
                        }
                    }
                }

            } catch (RepositoryException var3) {
                throw new ActionExecutionException(var3);
            }


            if(exclErrorThrown) {
                uiContext.openAlert(MessageStyleTypeEnum.ERROR, "Alert", "You can not modify the product level and assigned partner before approver approve those assign/unassign partner request of this product.", "OK", new AlertCallback() {
                    @Override
                    public void onOk() {
                        //dont do anything loh
                        onSuccessCallback();
                    }
                });
            }else {
                onSuccessCallback();
            }


//            Collection<String> users = UserManagementUtil.getUsersWithRole("b2c-slm-publish-category");
//            final Message message = new Message();
//            message.setMessage("Product " + productName + " is updated.");
//            message.setSubject("CMS Product Updated");
//            message.setType(MessageType.INFO);
//
//            for(String user: users) {
//                messagesManager.sendMessage(user, message);
//            }



        } else {
            log.info("Validation error(s) occurred. No save performed.");
        }

    }

    public void onSuccessCallback() {
        this.callback.onSuccess(((SaveDialogActionDefinition)this.getDefinition()).getName());
    }

    //TODO check uniquess with node in other channels
    @Override
    protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {
        //set the node name if its new only
        if(item instanceof JcrNewNodeAdapter) {
            String newNodeName =  RandomStringUtils.random(5, true, true);

            //do not make it start with number please
            while(newNodeName.matches("[0-9]{1}[a-zA-Z0-9]*")) {
                newNodeName =  RandomStringUtils.random(5, true, true);
            }

            if (!node.getName().equals(Path.getValidatedLabel(newNodeName))) {
                newNodeName = Path.getUniqueLabel(node.getSession(), node.getParent().getPath(), Path.getValidatedLabel(newNodeName));
                item.setNodeName(newNodeName);
                NodeUtil.renameNode(node, newNodeName);
            }
        }
    }
}
