package com.enovax.star.cms.products.mgnl.workbench.column;

import com.enovax.star.cms.commons.mgnl.definition.AXProductPromotionProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.ui.Table;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.workbench.column.StatusColumnFormatter;
import info.magnolia.ui.workbench.column.definition.StatusColumnDefinition;

import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.security.AccessControlException;
import java.util.List;

/**
 * Created by jennylynsze on 9/23/16.
 */
public class PromotionStatusColumnFormatter  extends StatusColumnFormatter{

    @Inject
    public PromotionStatusColumnFormatter(StatusColumnDefinition definition, SimpleTranslator i18n) {
        super(definition, i18n);
    }


    //Need to add in the code for affiliation changes
    @Override
    public Object generateCell(Table source, Object itemId, Object columnId) {
        Item jcrItem = this.getJcrItem(source, itemId);
        if(jcrItem != null && jcrItem.isNode()) {
            Node node = (Node)jcrItem;
            String activationStatus = "";
            String activationStatusMessage = "";
            String permissionStatus = "";
            if(((StatusColumnDefinition)this.definition).isActivation()) {
                Integer e;
                try {
                    e = Integer.valueOf(NodeTypes.Activatable.getActivationStatus(node));
                } catch (RepositoryException var14) {
                    e = Integer.valueOf(0);
                }

                StatusColumnFormatter.ActivationStatus activationType;
                switch(e.intValue()) {
                    case 1:
                        activationType = StatusColumnFormatter.ActivationStatus.MODIFIED;
                        activationStatusMessage = this.i18n.translate("activation-status.columns.modified", new Object[0]);
                        break;
                    case 2:

                        activationType = StatusColumnFormatter.ActivationStatus.ACTIVATED;
                        activationStatusMessage = this.i18n.translate("activation-status.columns.activated", new Object[0]);

                        //OK I need to make sure it is really activated, ung iba kebs
                        try {
                            if(node.hasNode(AXProductPromotionProperties.Affiliation.getPropertyName())) {
                                Node affNode = node.getNode(AXProductPromotionProperties.Affiliation.getPropertyName());
                                List<Node> affSubNodeList = NodeUtil.asList(NodeUtil.getNodes(affNode));
                                for(Node affSubNode: affSubNodeList) {
                                    //get the status of each lo, dunno if slow leh :(
                                    String uuid = PropertyUtil.getString(affSubNode, AXProductPromotionProperties.RelatedAffUUID.getPropertyName());

                                    Node affMainNode =  NodeUtil.getNodeByIdentifier(JcrWorkspace.Affiliations.getWorkspaceName(), uuid);

                                    Integer a;
                                    try {
                                        a = Integer.valueOf(NodeTypes.Activatable.getActivationStatus(affMainNode));
                                    } catch (RepositoryException var14) {
                                        a  = Integer.valueOf(0);
                                    }

                                    if(a.intValue() != 2) {
                                        activationType = StatusColumnFormatter.ActivationStatus.MODIFIED;
                                        activationStatusMessage = this.i18n.translate("activation-status.columns.modified", new Object[0]);
                                        break;
                                    }
                                }
                            }
                        } catch (RepositoryException e1) {
                            e1.printStackTrace();
                        }

                        break;
                    default:
                        activationType = StatusColumnFormatter.ActivationStatus.NOT_ACTIVATED;
                        activationStatusMessage = this.i18n.translate("activation-status.columns.not-activated", new Object[0]);
                }

                activationStatus = "<span class=\"" + activationType.getStyleName() + "\" title=\"" + activationStatusMessage + "\"></span>";
                activationStatus = activationStatus + "<span class=\"hidden-for-aria\">" + activationStatusMessage + "</span>";
            }

            if(((StatusColumnDefinition)this.definition).isPermissions()) {
                try {
                    node.getSession().checkPermission(node.getPath(), "add_node,remove,set_property");
                } catch (AccessControlException var12) {
                    permissionStatus = "<span class=\"icon-read-only\"></span>";
                } catch (RepositoryException var13) {
                    throw new RuntimeException("Could not access the JCR permissions for the following node identifier " + itemId, var13);
                }
            }

            return activationStatus + permissionStatus;
        } else {
            return null;
        }
    }
}
