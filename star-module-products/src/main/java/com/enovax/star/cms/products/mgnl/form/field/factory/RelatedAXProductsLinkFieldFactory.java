package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.factory.CustomDisplayLinkFieldFactory;
import com.enovax.star.cms.products.mgnl.form.event.RelatedAxProductValueChangedEvent;
import com.enovax.star.cms.products.mgnl.form.field.RelatedAXProductsLinkField;
import com.enovax.star.cms.products.mgnl.form.field.definition.RelatedAXProductsLinkFieldDefinition;
import com.enovax.star.cms.products.mgnl.util.ProductsUtil;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import info.magnolia.event.EventBus;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.AppController;
import info.magnolia.ui.api.app.ChooseDialogCallback;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class RelatedAXProductsLinkFieldFactory <T extends RelatedAXProductsLinkFieldDefinition>  extends CustomDisplayLinkFieldFactory {

    private static final Logger log = LoggerFactory.getLogger(RelatedAXProductsLinkFieldFactory.class);
    private RelatedAXProductsLinkField linkField;
    private ComponentProvider componentProvider;
    private final RelatedAXProductsLinkFieldDefinition definition;
    private final Item relatedFieldItem;

    private final AppController appController;
    private final UiContext uiContext;
    private final EventBus subAppEventBus;

    @Inject
    public RelatedAXProductsLinkFieldFactory(RelatedAXProductsLinkFieldDefinition definition, Item relatedFieldItem, AppController appController, UiContext uiContext, ComponentProvider componentProvider, I18NAuthoringSupport i18NAuthoringSupport,  @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, appController, uiContext, componentProvider, i18NAuthoringSupport);
        this.definition = definition;
        this.relatedFieldItem = relatedFieldItem;
        this.componentProvider = componentProvider;
        this.subAppEventBus = subAppEventBus;
        this.appController = appController;
        this.uiContext = uiContext;
    }

    @Override
    protected Field<String> createFieldComponent() {
        linkField = getNewLinkField();
        // Set Caption
        linkField.setButtonCaptionNew("Select New");
        linkField.setButtonCaptionOther("Select Another");
        // Add a callback listener on the select button
        linkField.getSelectButton().addClickListener(createButtonClickListener());
        return linkField;
    }

    protected RelatedAXProductsLinkField getNewLinkField() {
        return new RelatedAXProductsLinkField(definition, componentProvider);
    }

    @Override
    protected String getLinkFieldDisplay(final Node selected, String defaultValue) throws RepositoryException{
        return ProductsUtil.getAXProductDetailsDisplay(selected);
    }

    /**
     * Create the Button click Listener. On click: Create a Dialog and
     * Initialize callback handling.
     */
    protected Button.ClickListener createButtonClickListener() {
        return new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ChooseDialogCallback callback = createChooseDialogCallback();
                String uuid = linkField.getValue();
                String value = "";
                if(StringUtils.isNotBlank(uuid)) {
                    try {
                        value = NodeUtil.getNodeByIdentifier(definition.getTargetWorkspace(), uuid).getPath();
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }
                }

                if (StringUtils.isNotBlank(definition.getTargetTreeRootPath())) {
                    appController.openChooseDialog(definition.getAppName(), uiContext, definition.getTargetTreeRootPath(), value, callback);
                } else {
                    appController.openChooseDialog(definition.getAppName(), uiContext, value, callback);
                }
            }
        };
    }


    /**
     * @return specific {@link ChooseDialogCallback} implementation used to process the selected value.
     */
    @Override
    protected ChooseDialogCallback createChooseDialogCallback() {
        return new ChooseDialogCallback() {

            private String path;
            private String uuid;

            @Override
            public void onItemChosen(String actionName, final Object chosenValue) {
                String newValue = null;
                Node selectedNode = null;
                if (chosenValue instanceof JcrItemId) {
                    String propertyName = definition.getTargetPropertyToPopulate();
                    try {
                        javax.jcr.Item jcrItem = JcrItemUtil.getJcrItem((JcrItemId) chosenValue);
                        if (jcrItem.isNode()) {
                            final Node selected = (Node) jcrItem;
                            boolean isPropertyExisting = StringUtils.isNotBlank(propertyName) && selected.hasProperty(propertyName);
                            newValue = isPropertyExisting ? selected.getProperty(propertyName).getString() : selected.getPath();
                            linkField.setDisplay(getLinkFieldDisplay(selected, newValue));
                            selectedNode = selected;
                        }
                    } catch (RepositoryException e) {
                        log.error("Not able to access the configured property. Value will not be set.", e);
                    }
                } else {
                    newValue = String.valueOf(chosenValue);
                }
                linkField.setValue(newValue);

                subAppEventBus.fireEvent(new RelatedAxProductValueChangedEvent(selectedNode, relatedFieldItem));
            }

            @Override
            public void onCancel() {
            }
        };
    }

}
