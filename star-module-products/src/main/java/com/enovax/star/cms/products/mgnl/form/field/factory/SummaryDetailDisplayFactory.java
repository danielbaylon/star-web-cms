package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.definition.DataType;
import com.enovax.star.cms.products.mgnl.form.field.definition.SummaryDetailDisplayDefinition;
import com.vaadin.data.Item;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.StaticFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class SummaryDetailDisplayFactory<D extends SummaryDetailDisplayDefinition> extends StaticFieldFactory {

    private Item relatedFieldItem;
    private SummaryDetailDisplayDefinition definition;

    @Inject
    public SummaryDetailDisplayFactory(SummaryDetailDisplayDefinition definition, Item relatedFieldItem,UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.relatedFieldItem = relatedFieldItem;
        this.definition = definition;
    }

    public String createFieldValue() {
        String displayValues = this.definition.getValue();
        String finalDisplay = "";
        String paramFormat = this.definition.getParamFormat();
        String paramFormatArr[];
        Map<String, String> paramFormatMap = new HashMap<>();

        if(StringUtils.isNotBlank(paramFormat)) {
            paramFormatArr = paramFormat.split(",");
            for(int i = 0; i < paramFormatArr.length; i++) {
                String format[] = paramFormatArr[i].split(":");
                String param = format[0];
                String dataType = format[1];
                paramFormatMap.put(param, dataType);
            }
        }

        String displayValuesArr[] = displayValues.split(" ");
        Node fkNode = ((JcrNodeAdapter) this.relatedFieldItem).getJcrItem();
        Node mainNode = null;
        if(this.definition.getUuidPropertyName() != null) {
            String uuid = PropertyUtil.getString(fkNode, this.definition.getUuidPropertyName());
            try {
                mainNode = NodeUtil.getNodeByIdentifier(this.definition.getWorkspace(), uuid);
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }else {
            mainNode = fkNode;
        }
        
        for(String dv: displayValuesArr) {

            if(StringUtils.isNotBlank(finalDisplay)) {
                finalDisplay = finalDisplay + " ";
            }
            try {
                if(dv.startsWith(":") && mainNode.hasProperty(dv.substring(1))) {
                    //replace
                    String key = dv.substring(1);
                    String formattedKey = "";

                    if(paramFormatMap.containsKey(key)) {
                        formattedKey = DataType.getFormattedDisplay(PropertyUtil.getPropertyValueObject(mainNode, key), paramFormatMap.get(key));
                    }else {
                        formattedKey = PropertyUtil.getString(mainNode, key);
                    }

                    finalDisplay = finalDisplay + formattedKey;
                }else {
                    finalDisplay = finalDisplay + dv;
                }

            } catch (RepositoryException e) {
                finalDisplay = finalDisplay + dv;
            }

        }

        return finalDisplay;
    }

}
