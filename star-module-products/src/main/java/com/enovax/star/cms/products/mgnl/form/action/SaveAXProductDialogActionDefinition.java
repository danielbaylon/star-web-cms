package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class SaveAXProductDialogActionDefinition  extends SaveDialogActionDefinition {

    public SaveAXProductDialogActionDefinition () {
        setImplementationClass(SaveAXProductDialogAction.class);
    }
}
