package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.products.mgnl.form.field.definition.CrossSellPriceDisplayDefinition;
import com.vaadin.data.Item;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.StaticFieldDefinition;
import info.magnolia.ui.form.field.factory.StaticFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 12/8/16.
 */
public class CrossSellPriceDisplayFactory<D extends CrossSellPriceDisplayDefinition>  extends StaticFieldFactory {

    final Item relatedFieldItem;

    @Inject
    public CrossSellPriceDisplayFactory(StaticFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.relatedFieldItem = relatedFieldItem;
    }

    public String createFieldValue() {
        String finalDisplay = "<b>Unit Price: </b> ";
        Node crossSellNode = ((JcrNodeAdapter) this.relatedFieldItem).getJcrItem();
        try {
            String listingId = PropertyUtil.getString(crossSellNode, "crossSellProductListingId");
            String path = crossSellNode.getParent().getParent().getParent().getPath();
            Node relAxProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), path + "/" + listingId);

            if(relAxProductNode != null) {
                finalDisplay = finalDisplay + PropertyUtil.getString(relAxProductNode, AXProductProperties.ProductPrice.getPropertyName());
            }

        } catch (RepositoryException e) {
            finalDisplay = finalDisplay + "Not Available (AX Product not found)";
            e.printStackTrace();
        }

        return finalDisplay + "<br/><br/>";
    }
}
