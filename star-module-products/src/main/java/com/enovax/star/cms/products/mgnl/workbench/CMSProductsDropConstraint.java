package com.enovax.star.cms.products.mgnl.workbench;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.workbench.tree.drop.BaseDropConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/14/16.
 */
public class CMSProductsDropConstraint extends BaseDropConstraint {


    protected final Logger log = LoggerFactory.getLogger(getClass());

    private final String nodeType;

    public CMSProductsDropConstraint() {
        super(JcrWorkspace.CMSProducts.getNodeType());
        this.nodeType =JcrWorkspace.CMSProducts.getNodeType();
    }


    @Override
    public boolean allowedAsChild(Item sourceItem, Item targetItem) {
        return false;
    }

    @Override
    public boolean allowedBefore(Item sourceItem, Item targetItem) {
        return allowedBeforeAfter(sourceItem, targetItem);
    }

    @Override
    public boolean allowedAfter(Item sourceItem, Item targetItem) {
      return allowedBeforeAfter(sourceItem, targetItem);
    }


    private boolean allowedBeforeAfter(Item sourceItem, Item targetItem) {
        try {
            JcrNodeAdapter e = (JcrNodeAdapter)sourceItem;
            JcrNodeAdapter target = (JcrNodeAdapter)targetItem;
            String sourceNodeType = e.applyChanges().getPrimaryNodeType().getName();
            String targetNodeType = target.applyChanges().getPrimaryNodeType().getName();


            if(!sourceNodeType.equals(targetNodeType)) {
                log.debug("Could not move a node type \'{}\' before or after a node type \'{}\'", targetNodeType, this.nodeType);
                return false;
            }

        } catch (RepositoryException var8) {
            log.warn("Could not check if moving before or after is allowed. ", var8);
        }

        return true;
    }

    @Override
    public boolean allowedToMove(Item sourceItem) {
        return true;
    }


}
