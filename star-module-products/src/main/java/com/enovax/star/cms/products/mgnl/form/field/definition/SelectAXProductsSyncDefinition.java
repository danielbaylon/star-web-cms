package com.enovax.star.cms.products.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.SelectFieldDefinition;

/**
 * Created by jennylynsze on 4/18/16.
 */
public class SelectAXProductsSyncDefinition extends SelectFieldDefinition {

    private String selected;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }
}
