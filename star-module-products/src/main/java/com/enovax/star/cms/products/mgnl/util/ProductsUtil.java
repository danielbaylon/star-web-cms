package com.enovax.star.cms.products.mgnl.util;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/22/16.
 */
public class ProductsUtil {

    public static String getAXProductDetailsDisplay(Node node) {
        try {
            return "Name: " + node.getProperty("itemName").getString()
                    + " <br/> Ticket Type: " + node.getProperty("ticketType").getString()
                    + " | Price: $" + node.getProperty("productPrice").getString()
                    + " | Item ID (AX): " + node.getProperty("displayProductNumber").getString()
                    + " | Status: " + node.getProperty("status").getString();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }
}
