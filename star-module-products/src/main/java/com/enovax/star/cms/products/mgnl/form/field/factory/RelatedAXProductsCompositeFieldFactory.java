package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.products.mgnl.form.field.definition.RelatedAXProductsCompositeFieldDefinition;
import com.vaadin.data.Item;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.CompositeFieldFactory;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 5/4/16.
 */
public class RelatedAXProductsCompositeFieldFactory<D extends RelatedAXProductsCompositeFieldDefinition> extends CompositeFieldFactory {

    @Inject
    public RelatedAXProductsCompositeFieldFactory(RelatedAXProductsCompositeFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, fieldFactoryFactory, componentProvider);
    }
}
