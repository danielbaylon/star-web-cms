package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.products.mgnl.form.event.ProductLevelValueChangedEvent;
import com.enovax.star.cms.products.mgnl.form.field.definition.ProductLevelMinQuantityTextFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.ui.Field;
import info.magnolia.event.EventBus;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.TextFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;

/**
 * Created by jennylynsze on 10/15/16.
 */
public class ProductLevelMinQuantityTextFieldFactory<T extends ProductLevelMinQuantityTextFieldDefinition> extends TextFieldFactory implements ProductLevelValueChangedEvent.Handler {

    private final EventBus subAppEventBus;
    private Field<String> field;
    private final String productLevel;
    private String selectedValue;

    @Inject
    public ProductLevelMinQuantityTextFieldFactory(ProductLevelMinQuantityTextFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.subAppEventBus = subAppEventBus;
        this.subAppEventBus.addHandler(ProductLevelValueChangedEvent.class, this);
        this.productLevel = definition.getProductLevel();

        if(relatedFieldItem instanceof JcrNodeAdapter) {
            Node productNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
            this.selectedValue = PropertyUtil.getString(productNode, "productLevel");
        }
    }

    @Override
    public Field<String> createField() {
        this.field = super.createField();
        setVisible();
        return this.field;
    }

    @Override
    public void onProductLevelValueChanged(ProductLevelValueChangedEvent event) {
        if (event.getSelectedValue() == null || StringUtils.isBlank(productLevel)) {
            return;
        }

        this.selectedValue = event.getSelectedValue();

        setVisible();
    }

    private void setVisible() {
        if(this.productLevel.equals(this.selectedValue)) {
            this.field.setVisible(true);
            this.field.setRequired(true);
        }else {
            this.field.setVisible(false);
            this.field.setRequired(false);
        }
    }
}
