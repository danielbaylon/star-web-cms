package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.form.field.EditButton;
import com.enovax.star.cms.products.mgnl.form.event.RelatedAxProductValueChangedEvent;
import com.enovax.star.cms.products.mgnl.form.field.definition.EditRelatedAxProductsButtonDefinition;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenter;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenterFactory;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.field.factory.AbstractFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 5/5/16.
 */
@StyleSheet({"vaadin://ax-products-styles.css"})
public class EditRelatedAxProductsButtonFactory<D extends EditRelatedAxProductsButtonDefinition> extends AbstractFieldFactory<D, Object> implements RelatedAxProductValueChangedEvent.Handler {

    private final FormDialogPresenterFactory formDialogPresenterFactory;
    private final UiContext uiContext;
    private EditButton button;
    private final EventBus subAppEventBus;
    private final EditRelatedAxProductsButtonDefinition definition;
    private final Item relatedFieldItem;

    @Inject
    public EditRelatedAxProductsButtonFactory(D definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18NAuthoringSupport, FormDialogPresenterFactory formDialogPresenterFactory, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18NAuthoringSupport);
        this.formDialogPresenterFactory = formDialogPresenterFactory;
        this.uiContext = uiContext;
        this.subAppEventBus = subAppEventBus;
        this.definition = definition;
        this.relatedFieldItem = relatedFieldItem;
        this.subAppEventBus.addHandler(RelatedAxProductValueChangedEvent.class, this);
    }


    private Button.ClickListener createButtonClickListener() {
        return new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                final FormDialogPresenter formDialogPresenter = formDialogPresenterFactory.createFormDialogPresenter(definition.getDialogName());
                if(formDialogPresenter == null) {
                    //this.uiContext.openNotification(MessageStyleTypeEnum.ERROR, false, this.i18n.translate("ui-framework.actions.dialog.not.registered", new Object[]{dialogName}));
                } else {
                    Map<String, String> dialogData = new HashMap<>();
                    Collection<?> itemProperties = relatedFieldItem.getItemPropertyIds();
                    for(Object id: itemProperties) {
                        if(relatedFieldItem.getItemProperty(id).getValue() != null) {
                            dialogData.put(id.toString(), relatedFieldItem.getItemProperty(id).getValue().toString());
                        }
                    }
                    Node previous = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
                    try {
                        PropertyIterator prevIterator =  previous.getProperties();
                        while(prevIterator.hasNext()) {
                            javax.jcr.Property prevProp = prevIterator.nextProperty();
                            if(!dialogData.containsKey(prevProp.getName())){
                                dialogData.put(prevProp.getName(), prevProp.getString());
                            }
                        }
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }

                    //check ticket type:
                    //TODO better way
                    if(!dialogData.containsKey("ticketType")) {
                        dialogData.put("ticketType", "Adult");
                    }

                    // System.out.println("Value is: " + itemCopy.getItemProperty("itemName").getValue().toString());

                    formDialogPresenter.start(relatedFieldItem, definition.getDialogName(), uiContext, new EditorCallback() {
                        public void onSuccess(String actionName) {
                            uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "Changes in Ax Product's Details will be applied on Save Changes.");
                            System.out.println("Success");
                            formDialogPresenter.closeDialog();
                        }

                        public void onCancel() {
                            Collection<?> propertyIds = relatedFieldItem.getItemPropertyIds();
                            for(Object id: propertyIds) {
                                String data = "";
                                if(dialogData.containsKey(id.toString())) {
                                    data = dialogData.get(id.toString());
                                }
                                relatedFieldItem.getItemProperty(id).setValue(data);
                            }
                            formDialogPresenter.closeDialog();
                        }
                    });


                }
            }
        };
    }


    @Override
    protected Field<Object> createFieldComponent()
    {
        this.button = new EditButton();
        this.button.getButton().addClickListener(createButtonClickListener());

        if(StringUtils.isEmpty(this.relatedFieldItem.getItemProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).toString())) {
            this.button.setVisible(false);
        }

        return this.button;
    }

    @Override
    public void onEventValueChanged(RelatedAxProductValueChangedEvent event) {
        Node axProductNode = (Node) event.getItemId();
        Item relItem = (Item) event.getRelatedItemId();
        if(relItem.equals(relatedFieldItem)) {
            this.button.setVisible(true); //only set visible to the same one
            try {
                if(this.relatedFieldItem.getItemProperty("itemName") == null) {
                    this.relatedFieldItem.addItemProperty("itemName", new TextField()); //init the text field here
                }
                this.relatedFieldItem.getItemProperty("itemName").setValue(axProductNode.getProperty("productName").getString());
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
    }
}
