package com.enovax.star.cms.products.mgnl.workbench.search;

import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnector;
import info.magnolia.ui.workbench.WorkbenchPresenter;
import info.magnolia.ui.workbench.container.AbstractJcrContainer;
import info.magnolia.ui.workbench.search.SearchPresenter;
import info.magnolia.ui.workbench.search.SearchView;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/14/16.
 */
public class AXProductsBrowserSearchPresenter extends SearchPresenter {

    @Inject
    public AXProductsBrowserSearchPresenter(SearchView view, ComponentProvider componentProvider, WorkbenchPresenter workbenchPresenter) {
        super(view, componentProvider);
    }


    public void searchByActive(boolean showActiveOnly) {
        ((AXProductsBrowserSearchJcrContainer)container).setShowActiveOnly(showActiveOnly);
        refresh();
    }

    public void search(String fulltextExpr) {
        ((AXProductsBrowserSearchJcrContainer)container).setFullTextExpression(fulltextExpr);
        refresh();
    }

    public void clear() {
        ((AXProductsBrowserSearchJcrContainer)container).setFullTextExpression(null);
        refresh();
    }

    @Override
    protected AbstractJcrContainer createContainer() {
        return new AXProductsBrowserSearchJcrContainer(((JcrContentConnector)contentConnector).getContentConnectorDefinition());
    }

    /*
    public void search(String field, String fulltextExpr) {
        ((AXProductsBrowserSearchJcrContainer)this.container).setFullTextExpression(fulltextExpr);
        this.refresh();
    }
    */
}
