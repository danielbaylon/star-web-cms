package com.enovax.star.cms.products.mgnl.form.field;

import com.enovax.star.cms.commons.mgnl.form.field.CustomDisplayLinkField;
import com.enovax.star.cms.products.mgnl.form.field.definition.RelatedAXProductsLinkFieldDefinition;
import com.enovax.star.cms.products.mgnl.util.ProductsUtil;
import com.vaadin.ui.Component;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.ComponentProvider;
import org.apache.commons.lang3.StringUtils;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class RelatedAXProductsLinkField extends CustomDisplayLinkField {

    private final RelatedAXProductsLinkFieldDefinition definition;

    public RelatedAXProductsLinkField(RelatedAXProductsLinkFieldDefinition linkFieldDefinition, ComponentProvider componentProvider) {
        super(linkFieldDefinition, componentProvider);
        this.definition = linkFieldDefinition;
    }

    @Override
    protected Component initContent() {
        Component component = super.initContent();
        //update the text display
        if(StringUtils.isNotEmpty(getTextField().getValue())) {
            updateComponents(getTextField().getValue());
        }
        return component;
    }

    @Override
    protected String generateTextFieldDisplay(String currentValue) {
        if( StringUtils.isNotBlank(currentValue)) {
            Session session = null;
            try {
                session = MgnlContext.getJCRSession(definition.getTargetWorkspace());
                Node node = session.getNodeByIdentifier(currentValue);
                String formattedDisplay = ProductsUtil.getAXProductDetailsDisplay(node);

                return formattedDisplay;

            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return "";
    }



}
