package com.enovax.star.cms.products.mgnl.form.action;

import com.vaadin.data.Item;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;

/**
 * Created by jennylynsze on 5/6/16.
 */
public class ApplyChangesAXProductDialogAction extends SaveDialogAction<ApplyChangesAXProductDialogActionDefinition> {

    public ApplyChangesAXProductDialogAction(ApplyChangesAXProductDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
        super(definition, item, validator, callback);
    }

    @Override
    public void execute() throws ActionExecutionException {
        if(this.validateForm()) {
            this.callback.onSuccess(((ApplyChangesAXProductDialogActionDefinition)this.getDefinition()).getName());
        }

    }
}
