package com.enovax.star.cms.products.mgnl.form.field.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 4/21/16.
 */
public class MandatoryValidatorDefinition extends ConfiguredFieldValidatorDefinition {

    public MandatoryValidatorDefinition() {
        setFactoryClass(MandatoryValidatorFactory.class);
    }

}