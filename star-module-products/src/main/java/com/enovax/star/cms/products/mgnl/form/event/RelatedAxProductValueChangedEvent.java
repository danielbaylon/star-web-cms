package com.enovax.star.cms.products.mgnl.form.event;

import info.magnolia.event.Event;
import info.magnolia.event.EventHandler;

/**
 * Created by jennylynsze on 5/5/16.
 */
public class RelatedAxProductValueChangedEvent implements Event<RelatedAxProductValueChangedEvent.Handler>{

    private Object itemId;
    private Object relatedItemId;

    public interface Handler extends EventHandler {
        void onEventValueChanged(RelatedAxProductValueChangedEvent event);
    }

    public RelatedAxProductValueChangedEvent(Object itemId, Object relatedItemId) {
        this.itemId = itemId;
        this.relatedItemId = relatedItemId;
    }

    public Object getItemId() {
        return itemId;
    }

    public Object getRelatedItemId() {
        return relatedItemId;
    }

    @Override
    public void dispatch(Handler handler) {
        handler.onEventValueChanged(this);
    }
}
