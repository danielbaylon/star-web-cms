package com.enovax.star.cms.products.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.StaticFieldDefinition;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class SummaryDetailDisplayDefinition extends StaticFieldDefinition {
    private String paramFormat;
    private String workspace;
    private String uuidPropertyName;

    public String getParamFormat() {
        return paramFormat;
    }

    public void setParamFormat(String paramFormat) {
        this.paramFormat = paramFormat;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getUuidPropertyName() {
        return uuidPropertyName;
    }

    public void setUuidPropertyName(String uuidPropertyName) {
        this.uuidPropertyName = uuidPropertyName;
    }
}
