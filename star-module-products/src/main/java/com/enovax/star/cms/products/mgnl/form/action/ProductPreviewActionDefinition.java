package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.contentapp.detail.action.AbstractItemActionDefinition;

/**
 * Created by jennylynsze on 4/5/16.
 */
public class ProductPreviewActionDefinition extends AbstractItemActionDefinition {

    private String channel;
    private String rootPath;
    private String workspace;

    public ProductPreviewActionDefinition() {
        setImplementationClass(ProductPreviewAction.class);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }
}
