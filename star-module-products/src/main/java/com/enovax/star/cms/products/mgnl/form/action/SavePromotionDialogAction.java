package com.enovax.star.cms.products.mgnl.form.action;

import com.vaadin.data.Item;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 8/3/16.
 */
public class SavePromotionDialogAction extends SaveDialogAction<SavePromotionDialogActionDefinition> {
    @Inject
    public SavePromotionDialogAction(SavePromotionDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
        super(definition, item, validator, callback);
    }

    @Override
    public void execute() throws ActionExecutionException {
        if(this.validateForm()) {
            JcrNodeAdapter item = (JcrNodeAdapter)this.item;

            try {
                Node e = item.applyChanges();
                this.setNodeName(e, item);
                e.getSession().save();
            } catch (RepositoryException var3) {
                throw new ActionExecutionException(var3);
            }

            this.callback.onSuccess(((SaveDialogActionDefinition)this.getDefinition()).getName());
        }
    }
}
