package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.products.mgnl.form.field.definition.SelectAXProductsSyncDefinition;
import com.enovax.star.cms.productsync.service.SyncProduct;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import info.magnolia.cms.security.User;
import info.magnolia.context.MgnlContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.SelectFieldOptionDefinition;
import info.magnolia.ui.form.field.factory.SelectFieldFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jennylynsze on 4/18/16.
 */
public class SelectAXProductsSyncFactory extends SelectFieldFactory {

    SelectAXProductsSyncDefinition definition;

    @Inject
    public SelectAXProductsSyncFactory(SelectAXProductsSyncDefinition definition, Item relatedFieldItem,UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.definition = definition;
    }

    @Override
    protected Object createDefaultValue(Property dataSource) {
        return definition.getSelected();
    }

    @Override
    public List<SelectFieldOptionDefinition> getSelectFieldOptionDefinition() {
       List<SelectFieldOptionDefinition> options = super.getSelectFieldOptionDefinition();
       List<SelectFieldOptionDefinition> finalOptions = new ArrayList<>();
        User user = MgnlContext.getUser();
        Collection<String> userRoles = user.getAllRoles();

        if(userRoles.contains("superuser")) {
            return options;
        }

       for(SelectFieldOptionDefinition opt: options) {

           if("ALL".equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.B2CSLM_AX_SYNC_ROLE) &&
                   userRoles.contains(SyncProduct.B2CMFLG_AX_SYNC_ROLE) &&
                   userRoles.contains(SyncProduct.PPSLM_AX_SYNC_ROLE) &&
                   userRoles.contains(SyncProduct.PPMFLG_AX_SYNC_ROLE) &&
                   userRoles.contains(SyncProduct.KIOSKSLM_AX_SYNC_ROLE) &&
                   userRoles.contains(SyncProduct.KIOSKMFLG_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }

           if(StoreApiChannels.B2C_SLM.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.B2CSLM_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }

           if(StoreApiChannels.B2C_MFLG.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.B2CMFLG_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }

           if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.PPSLM_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }

           if(StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.PPMFLG_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }

           if(StoreApiChannels.KIOSK_SLM.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.KIOSKSLM_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }


           if(StoreApiChannels.KIOSK_MFLG.code.equals(opt.getValue()) &&
                   userRoles.contains(SyncProduct.KIOSKMFLG_AX_SYNC_ROLE)) {
               finalOptions.add(opt);
           }
       }

        return finalOptions;
    }
}
