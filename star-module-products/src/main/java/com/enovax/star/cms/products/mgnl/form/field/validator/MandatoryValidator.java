package com.enovax.star.cms.products.mgnl.form.field.validator;

import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractStringValidator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jennylynsze on 4/21/16.
 */
public class MandatoryValidator  extends AbstractStringValidator {

    private static final Logger log = LoggerFactory.getLogger(MandatoryValidator.class);

    private final Item item;
    /**
     * Constructs a validator for strings.
     * <p>
     * <p>
     * Null and empty string values are always accepted. To reject empty values,
     * set the field being validated as required.
     * </p>
     *
     * @param errorMessage the message to be included in an {@link InvalidValueException}
     *                     (with "{0}" replaced by the value that failed validation).
     */
    public MandatoryValidator(Item item, String errorMessage) {
        super(errorMessage);
        this.item = item;
    }

    @Override
    protected boolean isValidValue(String value) {
        if(StringUtils.isEmpty(value)) {
            return false;
        }
        return true;
    }
}
