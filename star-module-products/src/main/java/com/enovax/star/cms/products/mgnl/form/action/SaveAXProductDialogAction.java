package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.vaadin.data.Item;
import info.magnolia.cms.core.Path;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.framework.message.MessagesManager;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class SaveAXProductDialogAction extends SaveDialogAction<SaveCMSProductDialogActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(SaveCMSProductDialogAction.class);
    private static final String CMS_PRODUCT_GENERATED_URL_PATH = "generatedURLPath";
    private static final String NAME = "name";

    private final MessagesManager messagesManager;

    public SaveAXProductDialogAction(SaveCMSProductDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback, MessagesManager messagesManager) {
        super(definition, item, validator, callback);
        this.messagesManager = messagesManager;
    }

    @Override
    public void execute() throws ActionExecutionException {
        this.validator.showValidation(true);
        String productName;
        if(this.validator.isValid()) {
            JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;

            try {
                Node e = itemChanged.applyChanges();
                this.setNodeName(e, itemChanged);

                if(e.hasProperty(NAME)) {
                    String name = e.getProperty(NAME).getString();
                    if(StringUtils.isNotEmpty(name)) {
                        String url = name.toLowerCase();
                        url = url.replaceAll(" ", "-"); //replace all space to -
                        url = url.replaceAll("[^\\w-]", "");  //remove non alphanumeric and -

                        if(url.length() > 20) {
                            url = url.substring(0, 20); //limit to only 20
                        }

                        e.setProperty(CMS_PRODUCT_GENERATED_URL_PATH, url);
                    }
                }

                e.getSession().save();

                productName = e.getProperty("name").getString();

            } catch (RepositoryException var3) {
                throw new ActionExecutionException(var3);
            }


//            Collection<String> users = UserManagementUtil.getUsersWithRole("b2c-slm-publish-category");
//            final Message message = new Message();
//            message.setMessage("Product " + productName + " is updated.");
//            message.setSubject("CMS Product Updated");
//            message.setType(MessageType.INFO);
//
//            for(String user: users) {
//                messagesManager.sendMessage(user, message);
//            }

            this.callback.onSuccess(((SaveDialogActionDefinition)this.getDefinition()).getName());
        } else {
            log.info("Validation error(s) occurred. No save performed.");
        }

    }

    //TODO check uniquess with node in other channels
    @Override
    protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {
        if(!JcrRepository.nodeExists(JcrWorkspace.CMSProducts.getWorkspaceName(), node.getPath())) {
            String newNodeName =  RandomStringUtils.random(5, true, true);
            if (!node.getName().equals(Path.getValidatedLabel(newNodeName))) {
                newNodeName = Path.getUniqueLabel(node.getSession(), node.getParent().getPath(), Path.getValidatedLabel(newNodeName));
                item.setNodeName(newNodeName);
                NodeUtil.renameNode(node, newNodeName);
            }
        }
    }
}
