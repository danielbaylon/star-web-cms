package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 5/6/16.
 */
public class ApplyChangesAXProductDialogActionDefinition extends SaveDialogActionDefinition {
    public ApplyChangesAXProductDialogActionDefinition() {
        setImplementationClass(ApplyChangesAXProductDialogAction.class);
    }
}
