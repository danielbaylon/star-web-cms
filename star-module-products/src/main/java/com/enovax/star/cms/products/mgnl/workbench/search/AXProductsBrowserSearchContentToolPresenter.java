package com.enovax.star.cms.products.mgnl.workbench.search;

import info.magnolia.event.EventBus;
import info.magnolia.ui.api.view.View;
import info.magnolia.ui.workbench.WorkbenchPresenter;
import info.magnolia.ui.workbench.contenttool.ContentToolPresenter;
import info.magnolia.ui.workbench.contenttool.search.SearchContentToolView;
import info.magnolia.ui.workbench.event.QueryStatementChangedEvent;
import info.magnolia.ui.workbench.event.SearchEvent;
import info.magnolia.ui.workbench.search.SearchPresenterDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/14/16.
 */
public class AXProductsBrowserSearchContentToolPresenter implements ContentToolPresenter, SearchContentToolView.Listener {

    private static final Logger log = LoggerFactory.getLogger(AXProductsBrowserSearchContentToolPresenter.class);
    private EventBus eventBus;
    private SearchContentToolView view; //TODO might need to customize this one
    private WorkbenchPresenter workbenchPresenter;

    @Inject
    public AXProductsBrowserSearchContentToolPresenter(WorkbenchPresenter workbenchPresenter, SearchContentToolView view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
        this.workbenchPresenter = workbenchPresenter;
    }

    @Override
    public View start() {
        view.setListener(this);

        eventBus.addHandler(QueryStatementChangedEvent.class, new QueryStatementChangedEvent.Handler() {
            @Override
            public void onSetSearchQueryEvent(QueryStatementChangedEvent event) {
                view.setSearchQuery(event.getQuery());
            }
        });

        return view;
    }

    @Override
    public void onSearch(String searchQuery) {
        if (workbenchPresenter.hasViewType(SearchPresenterDefinition.VIEW_TYPE)) {
            eventBus.fireEvent(new SearchEvent(searchQuery));
        } else {
            log.warn("Workbench view triggered search although the search view type is not configured in this workbench {}", this);
        }
    }
}
