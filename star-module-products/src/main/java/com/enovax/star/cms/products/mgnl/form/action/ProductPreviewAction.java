package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.RemoteChannels;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.vaadin.data.Item;
import com.vaadin.ui.UI;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.location.LocationController;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Created by jennylynsze on 4/5/16.
 */
public class ProductPreviewAction extends AbstractAction<ProductPreviewActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(ProductPreviewAction.class);

    private final ProductPreviewActionDefinition definition;
    private final Item nodeItemToEdit;
    private final LocationController locationController;
    private final ContentConnector contentConnector;
    private final UiContext uiContext;
    private final StarTemplatingFunctions templatingFunctions;

    @Inject
    protected ProductPreviewAction(ProductPreviewActionDefinition definition, Item nodeItemToEdit, LocationController locationController, ContentConnector contentConnector, UiContext uiContext, StarTemplatingFunctions templatingFunctions) {
        super(definition);
        this.definition = definition;
        this.nodeItemToEdit = nodeItemToEdit;
        this.locationController = locationController;
        this.contentConnector = contentConnector;
        this.uiContext = uiContext;
        this.templatingFunctions = templatingFunctions;
    }

    @Override
    public void execute() throws ActionExecutionException {
        try {
            Object itemId = contentConnector.getItemId(nodeItemToEdit);
            if (!contentConnector.canHandleItem(itemId)) {
                log.warn("ProductPreviewAction requested for a node type definition {}. Current node type is {}. No action will be performed.", getDefinition(), String.valueOf(itemId));
                return;
            }

            Node productNode = ((JcrNodeAdapter) this.nodeItemToEdit).getJcrItem();
            String channel = this.definition.getChannel();
            if(RemoteChannels.B2C_SLM.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productNode.getProperty("generatedURLPath").getString())
                            .replace("{1}", productNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Preview-B2C_SLM", true);
                }
            }else if(RemoteChannels.B2C_MFLG.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productNode.getProperty("generatedURLPath").getString())
                            .replace("{1}", productNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Preview-B2C_MFLG", true);
                }
            }else if(RemoteChannels.KIOSK_SLM.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Preview-KIOSK_SLM", true);
                }
            }else if(RemoteChannels.KIOSK_MFLG.getChannel().equals(channel)) {
                String url;
                Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                    url = urlPathNode.getProperty(channel).getString();
                    url = url.replace("{0}", productNode.getName());
                    UI.getCurrent().getPage().open(url, "Product-Preview-KIOSK_MFLG", true);
                }
            }else if(RemoteChannels.PP_SLM.getChannel().equals(channel)) {
                //productNode.getIdentifier()
                Node categoryNode = templatingFunctions.getCMSProductCategoryByCMSProduct(StoreApiChannels.PARTNER_PORTAL_SLM.code, productNode.getIdentifier());

                if(categoryNode == null) {
                    uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "Sorry, you can only preview product that is assigned to a category.");
                }else {
                    String url;
                    Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                    if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                        url = urlPathNode.getProperty(channel).getString();
                        url = url.replace("{0}", categoryNode.getProperty("generatedURLPath").getString())
                                .replace("{1}", categoryNode.getName());
                        UI.getCurrent().getPage().open(url, "Product-Preview-PP_SLM", true);
                    }
                }
            }else if(RemoteChannels.PP_MFLG.getChannel().equals(channel)) {
                //productNode.getIdentifier()
                Node categoryNode = templatingFunctions.getCMSProductCategoryByCMSProduct(StoreApiChannels.PARTNER_PORTAL_MFLG.code, productNode.getIdentifier());

                if(categoryNode == null) {
                    uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "Sorry, you can only preview product that is assigned to a category.");
                }else {
                    String url;
                    Node urlPathNode = JcrRepository.getParentNode(this.definition.getWorkspace(), this.definition.getRootPath());
                    if(urlPathNode != null && urlPathNode.hasProperty(channel)) {
                        url = urlPathNode.getProperty(channel).getString();
                        url = url.replace("{0}", categoryNode.getProperty("generatedURLPath").getString())
                                .replace("{1}", categoryNode.getName());
                        UI.getCurrent().getPage().open(url, "Product-Preview-PP_MFLG", true);
                    }
                }
            }
        } catch (Exception e) {
            uiContext.openNotification(MessageStyleTypeEnum.ERROR, true, "Error encountered. Could not execute the Preview feature.");
            throw new ActionExecutionException("Could not execute BrowserPreviewItemAction: ", e);
        }
    }
}
