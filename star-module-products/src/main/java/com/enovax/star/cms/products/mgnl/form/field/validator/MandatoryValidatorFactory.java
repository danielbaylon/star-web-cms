package com.enovax.star.cms.products.mgnl.form.field.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 4/21/16.
 */
public class MandatoryValidatorFactory  extends AbstractFieldValidatorFactory<MandatoryValidatorDefinition> {
    private Item item;

    public MandatoryValidatorFactory(MandatoryValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
    }

    @Override
    public Validator createValidator() {
        return new MandatoryValidator(item, getI18nErrorMessage());
    }
}
