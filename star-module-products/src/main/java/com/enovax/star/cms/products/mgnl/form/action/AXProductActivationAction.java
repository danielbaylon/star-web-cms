package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.framework.action.ActivationAction;
import info.magnolia.ui.framework.action.ActivationActionDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.jcr.Node;
import java.util.List;

/**
 * Created by jennylynsze on 6/27/16.
 */
public class AXProductActivationAction<D extends AXProductActivationActionDefinition> extends ActivationAction {

    private static final Logger log = LoggerFactory.getLogger(CMSProductActivationAction.class);

    private Node axProductNode;
    private static final String CHINESE_SUFFIX = "_zh_CN";

    public AXProductActivationAction(ActivationActionDefinition definition, List items, CommandsManager commandsManager, @Named("admincentral") EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, items, commandsManager, admincentralEventBus, uiContext, i18n);

        if(items != null) {
            this.axProductNode =  ((JcrNodeAdapter)items.get(0)).getJcrItem();
        }
    }

    public AXProductActivationAction(ActivationActionDefinition definition, JcrItemAdapter item, CommandsManager commandsManager, EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, item, commandsManager, admincentralEventBus, uiContext, i18n);
    }

    @Override
    protected void onPostExecute() throws Exception {
        Context context = MgnlContext.getInstance();
        // yes, this is inverted, because a chain returns false when it is finished.
        boolean success = !(Boolean) context.getAttribute(COMMAND_RESULT);

        if(success) {
            try {
                PublishingUtil.publishAXProductRelatedNodes(axProductNode);
            }catch (Exception e){
                //TODO check error loh
            }

        }

        super.onPostExecute();
    }


}
