package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class SaveCMSProductDialogActionDefinition  extends SaveDialogActionDefinition {

    private String channel;

    public SaveCMSProductDialogActionDefinition() {
        setImplementationClass(SaveCMSProductDialogAction.class);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
