package com.enovax.star.cms.products.mgnl.workbench;

import info.magnolia.event.Event;
import info.magnolia.event.EventHandler;


/**
 * Created by jennylynsze on 4/11/16.
 */
public class ShowActiveOnlyToggledEvent implements Event<ShowActiveOnlyToggledEvent.Handler> {

    private final boolean showActiveOnly;

    public ShowActiveOnlyToggledEvent(boolean showActiveOnly) {
        this.showActiveOnly = showActiveOnly;
    }

    @Override
    public void dispatch(Handler handler) {
        handler.onShowActiveOnlyToggled(this);
    }


    public static abstract interface Handler extends EventHandler
    {
        public abstract void onShowActiveOnlyToggled(ShowActiveOnlyToggledEvent showActiveOnlyToggledEvent);
    }
}
