package com.enovax.star.cms.products.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.TextFieldDefinition;

/**
 * Created by jennylynsze on 10/15/16.
 */
public class ProductLevelMinQuantityTextFieldDefinition extends TextFieldDefinition {
    private String productLevel;

    public String getProductLevel() {
        return productLevel;
    }

    public void setProductLevel(String productLevel) {
        this.productLevel = productLevel;
    }
}
