package com.enovax.star.cms.products.mgnl.form.action;

import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.api.overlay.ConfirmationCallback;
import info.magnolia.ui.framework.action.ActivationAction;
import info.magnolia.ui.framework.action.ActivationActionDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class CMSProductActivationAction<D extends CMSProductActivationActionDefinition>  extends ActivationAction {

    private static final Logger log = LoggerFactory.getLogger(CMSProductActivationAction.class);

    private List<Node> axProductNodes = new ArrayList<>();
    private Node productNode;
    private EventBus admincentralEventBus;
    private UiContext uiContext;
    private SimpleTranslator i18n;
    private JcrItemId changedItemId;

    public CMSProductActivationAction(ActivationActionDefinition definition, List<JcrItemAdapter>  items, CommandsManager commandsManager, @Named("admincentral") EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, items, commandsManager, admincentralEventBus, uiContext, i18n);

        if(items != null) {
            this.productNode =  ((JcrNodeAdapter)items.get(0)).getJcrItem();
        }
        this.uiContext = uiContext;
        this.i18n = i18n;
        this.admincentralEventBus = admincentralEventBus;
        this.changedItemId = items.isEmpty() ? null : items.get(0).getItemId();
    }

    public CMSProductActivationAction(ActivationActionDefinition definition, JcrItemAdapter item, CommandsManager commandsManager, EventBus admincentralEventBus, SubAppContext uiContext, SimpleTranslator i18n) {
        super(definition, item, commandsManager, admincentralEventBus, uiContext, i18n);
    }

    @Override
    protected void onPostExecute() throws Exception {
        Context context = MgnlContext.getInstance();
        // yes, this is inverted, because a chain returns false when it is finished.
        boolean success = !(Boolean) context.getAttribute(COMMAND_RESULT);

        if(success) {
            PublishingUtil.publishProductRelatedNodes(productNode);

            if(hasUnpublishAXProducts()) {
                this.uiContext.openConfirmation(MessageStyleTypeEnum.INFO, "Publish Product", "There's related AX Product that has not been publish. Do you want to publish it?", "Yes", "No", false, new ConfirmationCallback() {
                    @Override
                    public void onCancel() {
                        //do nothing
                        try {
                            doShowSuccessMessage(success);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onSuccess() {
                        try {
                            for(Node axProductNode: axProductNodes) {
                                try {
                                    String uuid = axProductNode.getIdentifier();
                                    PublishingUtil.publishNodes(uuid, JcrWorkspace.AXProducts.getWorkspaceName()); //publish product
                                    PublishingUtil.publishAXProductRelatedNodes(axProductNode);
                                }catch (Exception e) {
                                    log.error(e.getMessage(), e);
                                }

                            }
                            doShowSuccessMessage(success);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }else {
                doShowSuccessMessage(success);
            }
        }else {
            doShowSuccessMessage(success);
        }

//        super.onPostExecute();
    }

    private boolean hasUnpublishAXProducts() {
        try {

            if(productNode.hasNode(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                //go through all the nodes
                Node relatedAxProductsNode = productNode.getNode(CMSProductProperties.RelatedAXProducts.getPropertyName());
                Iterator<Node> relatedAxProductsNodeIterator = relatedAxProductsNode.getNodes();
                while(relatedAxProductsNodeIterator.hasNext()) {
                    Node relatedAxProductNode = relatedAxProductsNodeIterator.next();
                    String uuid = PropertyUtil.getString(relatedAxProductNode, CMSProductProperties.RelatedAXProducts.getPropertyName());
                    Node axProductNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);

                    if(!PublishingUtil.isPublish(axProductNode)) {
                        this.axProductNodes.add(axProductNode);
                    }

                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        if(axProductNodes.size() > 0) {
            return true;
        }

        return false;
    }

    public void doShowSuccessMessage(boolean success) throws  Exception{
        admincentralEventBus.fireEvent(new ContentChangedEvent(changedItemId));
        //boolean successFalse = !(Boolean) context.getAttribute(COMMAND_RESULT);
        // yes, this is inverted, because a chain returns false when it is finished.
        String message = i18n.translate(getMessage(success));
        MessageStyleTypeEnum messageStyleType = success ? MessageStyleTypeEnum.INFO : MessageStyleTypeEnum.ERROR;

        if (StringUtils.isNotBlank(message)) {
            uiContext.openNotification(messageStyleType, true, message);
        }
    }

//    private void publishRelatedImages() throws  Exception{
//
//        if(productNode.hasNode(CMSProductProperties.ProductImages.getPropertyName())) {
//            Node productImagesNode =  productNode.getNode(CMSProductProperties.ProductImages.getPropertyName());
//            List<Node> relatedProductImagesNodes =  NodeUtil.asList(NodeUtil.getNodes(productImagesNode));
//            for(Node relatedProductImageNode : relatedProductImagesNodes) {
//                if (relatedProductImageNode.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
//                    String uuid = relatedProductImageNode.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString();
//
//                    if(StringUtils.isNotBlank(uuid) && uuid.startsWith("jcr:")) {
//                        uuid = uuid.substring(4);
//                    }
//
//                    Node imageNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Dam.getWorkspaceName(), uuid);
//                    if (!PublishingUtil.isPublish(imageNode)) {
//                        try {
//                            //traverse all the parents && published it first
//                            Node parentNode = imageNode.getParent();
//                            while(parentNode != null && !"/".equals(parentNode.getPath())) {
//                                if(!PublishingUtil.isPublish(parentNode)) {
//                                    PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.Dam.getWorkspaceName());
//                                }
//                                try {
//                                    parentNode = parentNode.getParent();
//                                }catch(Exception e) {
//                                    parentNode = null;
//                                }
//                            }
//                            PublishingUtil.publishNodes(uuid, JcrWorkspace.Dam.getWorkspaceName());
//
//                        }catch(Exception e) {
//                            log.error(e.getMessage(), e);
//                        }
//                    }
//                }
//            }
//        }
//    }

}
