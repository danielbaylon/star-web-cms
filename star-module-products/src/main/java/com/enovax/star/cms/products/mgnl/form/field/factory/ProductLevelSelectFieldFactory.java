package com.enovax.star.cms.products.mgnl.form.field.factory;

import com.enovax.star.cms.products.mgnl.form.event.ProductLevelValueChangedEvent;
import com.enovax.star.cms.products.mgnl.form.field.definition.ProductLevelSelectFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.AbstractSelect;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.SelectFieldFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by jennylynsze on 10/15/16.
 */
public class ProductLevelSelectFieldFactory <T extends ProductLevelSelectFieldDefinition> extends SelectFieldFactory<T> {

    private final EventBus subAppEventBus;

    @Inject
    public ProductLevelSelectFieldFactory(T definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.subAppEventBus = subAppEventBus;
    }

    @Override
    protected AbstractSelect createFieldComponent() {
        final AbstractSelect select = super.createFieldComponent();
        select.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                subAppEventBus.fireEvent(new ProductLevelValueChangedEvent((String)select.getValue()));
            }
        });
        return select;
    }
}
