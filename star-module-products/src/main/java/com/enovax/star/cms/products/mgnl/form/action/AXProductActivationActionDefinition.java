package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

/**
 * Created by jennylynsze on 6/27/16.
 */
public class AXProductActivationActionDefinition extends ActivationActionDefinition {

    public AXProductActivationActionDefinition() {
        setImplementationClass(AXProductActivationAction.class);
    }
}
