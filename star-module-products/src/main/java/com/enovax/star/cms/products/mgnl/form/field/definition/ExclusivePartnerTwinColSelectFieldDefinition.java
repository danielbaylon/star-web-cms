package com.enovax.star.cms.products.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.TwinColSelectFieldDefinition;

/**
 * Created by jennylynsze on 1/6/17.
 */
public class ExclusivePartnerTwinColSelectFieldDefinition extends TwinColSelectFieldDefinition {
    private String channel;
    private String productLevel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProductLevel() {
        return productLevel;
    }

    public void setProductLevel(String productLevel) {
        this.productLevel = productLevel;
    }
}
