package com.enovax.star.cms.products.mgnl.workbench.contenttools;

import com.vaadin.data.Property;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

/**
 * Created by jennylynsze on 3/9/16.
 */
public class AXProductsBrowserContextToolViewImpl extends HorizontalLayout implements AXProductsBrowserContextToolView {
//    private ComboBox channelSelector;
//    private Label channelSelectorCaption;
    private final CheckBox showActiveOnlyCheckbox;

    public AXProductsBrowserContextToolViewImpl() {
        setDefaultComponentAlignment(Alignment.TOP_RIGHT);
        setSpacing(true);

        this.showActiveOnlyCheckbox = new CheckBox("Show Active Only");
        this.showActiveOnlyCheckbox.setImmediate(true);
        setHeight("40px");
        addComponent(this.showActiveOnlyCheckbox);
//        this.channelSelector = new ComboBox(null);
//        this.channelSelector.setImmediate(true);
//        this.channelSelector.setNullSelectionAllowed(false);
//        this.channelSelectorCaption = new Label("Select Channel");
//        this.channelSelectorCaption.setSizeUndefined();
//        addComponent(this.channelSelectorCaption);
//        addComponent(this.channelSelector);
    }
//
    public void setEnabled(boolean enabled) {
//        this.channelSelectorCaption.setEnabled(enabled);
//        this.channelSelector.setEnabled(enabled);
        this.showActiveOnlyCheckbox.setEnabled(enabled);
    }

//    @Override
//    public String getChannelProperty() {
//        return (String)this.channelSelector.getPropertyDataSource().getValue();
//    }

    public Component asVaadinComponent() {
        return this;
    }

//    public void setChannelOptions(Container options) {
//        this.channelSelector.setContainerDataSource(options);
//    }
//
//    @Override
//    public void setChannelProperty(Property<String> property) {
//        this.channelSelector.setPropertyDataSource(property);
//    }
//

    public void setShowActiveOnlyProperty(Property<Boolean> showActiveOnlyProperty)
    {
        this.showActiveOnlyCheckbox.setPropertyDataSource(showActiveOnlyProperty);
    }
}