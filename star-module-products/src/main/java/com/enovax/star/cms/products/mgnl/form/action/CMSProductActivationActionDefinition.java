package com.enovax.star.cms.products.mgnl.form.action;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class CMSProductActivationActionDefinition extends ActivationActionDefinition {

    public CMSProductActivationActionDefinition() {
        setImplementationClass(CMSProductActivationAction.class);
    }
}
