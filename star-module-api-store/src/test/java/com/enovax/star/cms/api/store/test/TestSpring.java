package com.enovax.star.cms.api.store.test;

import com.enovax.star.cms.api.store.config.StarModuleApiStoreAppConfig;
import com.enovax.star.cms.api.store.service.axretail.AxRetailProductExtService;
import com.enovax.star.cms.api.store.service.axretail.AxRetailService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.product.AxMainProduct;
import com.enovax.star.cms.commons.model.product.AxProductLinkPackage;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(classes = StarModuleApiStoreAppConfig.class)
@Rollback(value = true)
@RunWith(SpringJUnit4ClassRunner.class)
public class TestSpring {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxStarService axStarService;
    @Autowired
    private AxRetailService axRetailService;
    @Autowired
    private AxRetailProductExtService axRetailProductExtService;
    @Autowired
    private IProductService productService;

    @Test
    public void testProduct() throws Exception {
        final ApiResult<List<AxRetailProductExtData>> result = axRetailProductExtService.apiSearchProductExt(StoreApiChannels.B2C_SLM, true, "", 0L);
        if(result.isSuccess()) {
            List<AxRetailProductExtData> data = result.getData();
            for(AxRetailProductExtData d:data) {
               System.out.println(JsonUtil.jsonify(d));
            }
        }
    }
    /*
    http://springinpractice.com/2013/10/07/handling-json-error-object-responses-with-springs-resttemplate
     */
    @Test
    public void testPromotions() throws Exception {
        ApiResult<AxProductRelatedLinkPackage> result = productService.getValidRelatedAxProducts(StoreApiChannels.B2C_SLM);

        if(result.isSuccess()) {
            AxProductRelatedLinkPackage data = result.getData();
            AxProductLinkPackage mainLinkPackage = data.getAxProductLinkPackage();

            for(AxMainProduct prod: mainLinkPackage.getMainProductList()) {
                System.out.println(JsonUtil.jsonify(prod));
            }

            log.info("success");
        }
    }

    @Test
    public void testSomething3() throws Exception {
        log.info("testSomething3");

        AxStarServiceResult<List<AxStarRetailDiscount>> hehehe = axStarService.apiRetailDiscountsGet(StoreApiChannels.B2C_SLM);
        log.info(hehehe.toString());
    }

    @Test
    public void testSomething2() throws Exception {
        log.info("testSomething2");

        final ApiResult<List<AxRetailProductExtData>> apiResult = axRetailProductExtService.apiSearchProductExt(
                StoreApiChannels.B2C_MFLG, true, "5637154326", 202001000008L);
        log.info("Result: " + JsonUtil.jsonify(apiResult));
        log.info("Resulter: " + JsonUtil.jsonify(apiResult.getData()));
    }

    @Test
    public void testSomething() throws Exception {
        log.info("testSomething");

//        final AxStarServiceResult<String> rs = axStarService.doRestExchangeString(
//                StoreApiChannels.B2C_SLM, AxStarService.URL_FRAG_CART_ADD, HttpMethod.POST, "{\n" +
//                "    \"items\": [{\n" +
//                "        \"productId\": 5637149107,\n" +
//                "        \"quantity\": 3\n" +
//                "    }],\n" +
//                "    \"cartId\": \"\",\n" +
//                "    \"customerId\": \"\"\n" +
//                "}");
//        final AxStarServiceResult<String> rs = axStarService.doRestExchangeString(
//                StoreApiChannels.B2C_SLM, AxStarService.URL_FRAG_CART_ADD, HttpMethod.POST, "{\n" +
//                        "    \"cartId\": \"\",\n" +
//                        "    \"customerId\": \"\",\n" +
//                        "    \"items\": [{\n" +
//                        "        \"productId\": 5637149107,\n" +
//                        "        \"quantity\": 5\n" +
//                        "    }]\n" +
//                        "}");

        final AxStarInputAddCart axc = new AxStarInputAddCart();
        final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();
        final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();
        axcItem.setProductId(5637149107L);
        axcItem.setQuantity(5);
        axcItems.add(axcItem);
        axc.setCartId("");
        axc.setCustomerId("");
        axc.setItems(axcItems);
        final AxStarServiceResult<AxStarCartNested> rs = axStarService.doRestExchange(
                StoreApiChannels.B2C_SLM, AxStarService.URL_FRAG_CART_ADD, HttpMethod.POST, axc, AxStarCartNested.class);

        log.info(JsonUtil.jsonify(rs));
    }
}
