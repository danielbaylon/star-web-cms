package com.enovax.star.cms.api.store.test;

import com.enovax.star.cms.api.store.config.StarModuleApiStoreAppConfig;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.axchannel.AxPinType;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailGeneratePinInput;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.*;

@SuppressWarnings("Duplicates")
@ContextConfiguration(classes = StarModuleApiStoreAppConfig.class)
@Rollback(value = true)
@RunWith(SpringJUnit4ClassRunner.class)
public class TestAxWs {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;
    @Autowired
    private AxStarService axStarService;

    @Test
    public void doTicketGenerationFlow() throws Exception {
        final StoreApiChannels channel = StoreApiChannels.B2C_SLM;
        final String transId = "SAMBAL" + RandomStringUtils.random(7, "ABCDEFabcdef1234567890");
        final Long productId1 = 5637145352L;
        final String itemId1 = "1102101001";


        final AxStarInputAddCart addCart = new AxStarInputAddCart();
        addCart.setCartId("");
        addCart.setCustomerId("");
        final List<AxStarInputAddCartItem> addCartItems = new ArrayList<>();
        final AxStarInputAddCartItem aci = new AxStarInputAddCartItem();
        aci.setProductId(productId1);
        aci.setQuantity(1);
        addCartItems.add(aci);
        addCart.setItems(addCartItems);

        final AxStarServiceResult<AxStarCartNested> addCartResult = axStarService.apiCartAddItem(channel, addCart);
        log.info("addCartResult: " + JsonUtil.jsonify(addCartResult));
        if (!addCartResult.isSuccess()) {
            throw new Exception("FAIL LOL");
        }

        final AxStarCart initialCart = addCartResult.getData().getCart();

        final AxStarServiceResult<AxStarCartNested> checkoutCartResult =
                axStarService.apiCheckout(channel, initialCart.getId(), initialCart.getCustomerId(), transId);
        log.info("checkoutCartResult: " + JsonUtil.jsonify(checkoutCartResult));
        if (!checkoutCartResult.isSuccess()) {
            throw new Exception("FAIL LOLOLOL");
        }

        final AxStarCart checkoutCart = checkoutCartResult.getData().getCart();

        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();
        for (AxStarCartLine ccl : checkoutCart.getCartLines()) {
            AxRetailCartTicket ct = new AxRetailCartTicket();
            ct.setItemId(itemId1);
            ct.setListingId(productId1);
            ct.setQty(ccl.getQuantity());
            ct.setLineId(ccl.getLineId());
            ct.setTransactionId(transId);
            cartTickets.add(ct);
        }

        final ApiResult<List<AxRetailTicketRecord>> checkoutStartResult =
                axChannelTransactionService.apiB2CCartCheckoutStart(channel, transId, cartTickets);
        log.info("CartCheckoutStart RESULT: " + JsonUtil.jsonify(checkoutStartResult));

        final AxStarInputSalesOrderCreation soInput = new AxStarInputSalesOrderCreation();
        soInput.setCustomerId(checkoutCart.getCustomerId());
        soInput.setCartId(checkoutCart.getId());
        soInput.setEmail("jensen.ching@gmail.com");


        final Map<String, String> extMap = new HashMap<>();
        extMap.put("SalesPoolId", "Reserve");
        soInput.setStringExtensionProperties(extMap);

        final List<AxStarInputTenderDataLine> tenderDataLines = new ArrayList<>();
        final AxStarInputTenderDataLine tdl = new AxStarInputTenderDataLine();
        tdl.setTenderType("Credit Card (B2B/B2C)");
        tdl.setAmount(checkoutCart.getTotalAmount());
        tenderDataLines.add(tdl);
        soInput.setTenderDataLines(tenderDataLines);

        final AxStarServiceResult<AxStarSalesOrder> soResult = axStarService.apiSalesOrderCreate(channel, soInput);
        log.info("soResult: " + JsonUtil.jsonify(soResult));
        if (!soResult.isSuccess()) {
            throw new Exception("FAIL LOLOL");
        }

        final AxStarSalesOrder so = soResult.getData();

        final ApiResult<List<AxRetailTicketRecord>> checkoutCompleteResult =
                axChannelTransactionService.apiB2CCartCheckoutComplete(channel, transId);
        log.info("CartCheckoutComplete RESULT: " + JsonUtil.jsonify(checkoutCompleteResult));
    }

    @Test
    public void doPinGenerationFlow() throws Exception {
        final StoreApiChannels channel = StoreApiChannels.B2C_SLM;
        final String transId = "SAMBAL" + RandomStringUtils.random(7, "ABCDEFabcdef1234567890");
        final Long productId1 = 5637145352L;
        final String itemId1 = "1102101001";

        final AxStarInputAddCart addCart = new AxStarInputAddCart();
        addCart.setCartId("");
        addCart.setCustomerId("");
        final List<AxStarInputAddCartItem> addCartItems = new ArrayList<>();
        final AxStarInputAddCartItem aci = new AxStarInputAddCartItem();
        aci.setProductId(productId1);
        aci.setQuantity(1);
        addCartItems.add(aci);
        addCart.setItems(addCartItems);

        final AxStarServiceResult<AxStarCartNested> addCartResult = axStarService.apiCartAddItem(channel, addCart);
        log.info("addCartResult: " + JsonUtil.jsonify(addCartResult));
        if (!addCartResult.isSuccess()) {
            throw new Exception("FAIL LOL");
        }

        final AxStarCart initialCart = addCartResult.getData().getCart();

        final AxStarServiceResult<AxStarCartNested> checkoutCartResult =
                axStarService.apiCheckout(channel, initialCart.getId(), initialCart.getCustomerId(), transId);
        log.info("checkoutCartResult: " + JsonUtil.jsonify(checkoutCartResult));
        if (!checkoutCartResult.isSuccess()) {
            throw new Exception("FAIL LOLOLOL");
        }

        final AxStarCart checkoutCart = checkoutCartResult.getData().getCart();

        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();
        for (AxStarCartLine ccl : checkoutCart.getCartLines()) {
            AxRetailCartTicket ct = new AxRetailCartTicket();
            ct.setItemId(itemId1);
            ct.setListingId(productId1);
            ct.setQty(ccl.getQuantity());
            ct.setLineId(ccl.getLineId());
            ct.setTransactionId(transId);
            cartTickets.add(ct);
        }

        final ApiResult<List<AxRetailTicketRecord>> reserveResult =
                axChannelTransactionService.apiCartValidateAndReserveQuantity(channel, transId, cartTickets, "");
        log.info("CartValidateAndReserveQuantity RESULT: " + JsonUtil.jsonify(reserveResult));

        final AxStarInputSalesOrderCreation soInput = new AxStarInputSalesOrderCreation();
        soInput.setCustomerId(checkoutCart.getCustomerId());
        soInput.setCartId(checkoutCart.getId());
        soInput.setEmail("jensen.ching@gmail.com");

        final List<AxStarInputTenderDataLine> tenderDataLines = new ArrayList<>();
        final AxStarInputTenderDataLine tdl = new AxStarInputTenderDataLine();
        tdl.setTenderType("Credit Card (B2B/B2C)");
        tdl.setAmount(checkoutCart.getTotalAmount());
        tenderDataLines.add(tdl);
        soInput.setTenderDataLines(tenderDataLines);

        final AxStarServiceResult<AxStarSalesOrder> soResult = axStarService.apiSalesOrderCreate(channel, soInput);
        log.info("soResult: " + JsonUtil.jsonify(soResult));
        if (!soResult.isSuccess()) {
            throw new Exception("FAIL LOLOL");
        }

        final AxStarSalesOrder so = soResult.getData();

        final List<AxRetailGeneratePinInput> pinInputs = new ArrayList<>();
        for (AxStarSalesLine sl : so.getSalesLines()) {
            final AxRetailGeneratePinInput pi = new AxRetailGeneratePinInput();

            pi.setAllowPartialRedemption(false);
            pi.setCombineTicket(false);
            pi.setCustomerAccount("");
            pi.setDescription("Test");
            pi.setEndDateTime(new Date());
            //Event date logic
            pi.setEventDate(null);
            pi.setEventGroupId("");
            pi.setEventLineId("");
            pi.setGroupTicket(false);
            pi.setInventTransId(transId);
            pi.setInvoiceId("");
            pi.setItemId(sl.getItemId());
            pi.setLineNumber(BigDecimal.valueOf(sl.getLineNumber()));
            pi.setMediaType("paper");
            pi.setPackageName("Test Package");
            pi.setQty(BigDecimal.valueOf(sl.getQuantity()));
            pi.setQtyPerProduct(BigDecimal.valueOf(sl.getQuantity()));
            pi.setReferenceId(transId);
            pi.setRetailVariantId("");
            pi.setSalesId("");
            pi.setStartDateTime(new Date());
            pi.setTransactionId(transId);
            pi.setGuestName("Guest One");
            pi.setCcLast4Digits("");
            pi.setPinType(AxPinType.B2B);

            pinInputs.add(pi);
        }

        final ApiResult<List<AxRetailPinTable>> generatePinResult = axChannelTransactionService.apiGeneratePin(channel, pinInputs);
        log.info("GeneratePin RESULT: " + JsonUtil.jsonify(generatePinResult));
    }
}
