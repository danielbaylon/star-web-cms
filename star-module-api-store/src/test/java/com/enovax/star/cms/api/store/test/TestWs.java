package com.enovax.star.cms.api.store.test;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailGeneratePinInput;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.util.*;
import com.enovax.star.cms.commons.util.Base64;
import com.enovax.star.cms.commons.ws.axretail.ISDCRetailTicketTableService;
import com.enovax.star.cms.commons.ws.axretail.SDCRetailTicketTableResponse;
import com.enovax.star.cms.commons.ws.axretail.SDCRetailTicketTableService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.BasicConfigurator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Created by jensen on 22/3/16.
 */
public class TestWs {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;

    /**
     * Code from: http://stackoverflow.com/questions/10572398/how-can-i-easily-compress-and-decompress-strings-to-from-byte-arrays
     * @throws Exception
     */
    @Test
    public void compressText() throws Exception {
        String text = "{\"ed\":\"20170121 09:44:01\",\"fs\":[{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"003\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"005\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"004\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"007\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"008\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"026\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"033\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"034\",\"oids\":[\"0\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"035\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"037\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"038\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"041\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"042\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"047\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"046\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"048\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"051\",\"oids\":[\"1\"]},{\"usg\":[1,1,1,1,1,1,1],\"act\":0,\"fid\":\"053\",\"oids\":[\"4\"]}],\"qty\":1,\"sd\":\"20161021 09:44:01\",\"cd\":\"72016102116762039687\"}";
        System.out.println("Input text length: " + text.length() + " characters.");

        System.out.println("Running simple compression algorithm...");

        final byte[] compressedBytes = compress(text);
        String compressedText = com.enovax.star.cms.commons.util.Base64.encodeToString(compressedBytes, false);
        System.out.println("Compressed text: " + compressedText);
        System.out.println("Compressed text length: " + compressedText.length() + " characters.");

        System.out.println("Decompressing...");

        final String decompressedText = decompress(Base64.decode(compressedText));
        System.out.println("Decompressed text: " + decompressedText);
        System.out.println("Decompressed text length: " + decompressedText.length() + " characters.");

    }

    public static byte[] compress(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new GZIPOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    public static String decompress(byte[] bytes) throws Exception {
        InputStream in = new GZIPInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

   @Test
    public void testTime() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        System.out.println(today);

    }

    @Test
    public void testLocale(){
        System.out.println(Locale.TRADITIONAL_CHINESE.getDisplayName());
    }

    @Test
    public void testConvertion() throws Exception {
        String validity = "2154-12-31T00:00:00+07:00";
        String validDate = validity.substring(0, 10) + validity.substring(11, 19) + validity.substring(19, 22) + validity.substring(23);
        System.out.println(validDate);
        SimpleDateFormat sdf = new SimpleDateFormat();
        Date validDateDt = NvxDateUtils.parseDate(validDate, "yyyy-MM-ddHH:mm:ssZ" );
        System.out.println(validDateDt);
        System.out.println(new Date());
    }

    @Test
    public void testLogging() throws Exception {
        //Log4j basic configuration
//        BasicConfigurator.configure();
        log.info("hello");
        System.out.println("world");
    }

    @Test
    public void testAxRetailWs() throws Exception {
        //Log4j basic configuration
        BasicConfigurator.configure();
        //Outputs all SOAP calls to console
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        log.info("Calling [testAxRetailWs].");

        final String existingTransactionId = "000001-SLMOTC-02-b2ctest1xxxx";

        final String transactionId = "EnovaxTest-" + RandomStringUtils.random(6, true, false);
        log.info("Transaction ID generated: " + transactionId);

        final BigDecimal qty = BigDecimal.ONE;
        final String merlionAdultLocalItemId = "1030100001";
        final Long merlionAdultLocalProductId = 5637145331L;
        final String merlionAdultStandardItemId = "1030100002";
        final Long merlionAdultStandardProductId = 5637145332L;
        final String merlionChildLocalItemId = "1030100003";
        final Long merlionChildLocalProductId = 5637145333L;

        final String serviceEndpointUrl = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/services/SDC_RetailTicketTableService.svc";

        final SDCRetailTicketTableService ws = new SDCRetailTicketTableService(new URL(serviceEndpointUrl));
        final ISDCRetailTicketTableService stub = ws.getBasicHttpBindingISDCRetailTicketTableService();

        log.info("1. Testing retail ticket search:");
        SDCRetailTicketTableResponse response = stub.retailTicketSearch(existingTransactionId);
//        List<AxRetailTicketRecord> tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));

        log.info("2. Starting new transaction... Transaction ID: " + transactionId);

//        log.info("3. Adding Merlion Adult Local.");
//        response = stub.generateTicket(transactionId, qty, merlionAdultLocalItemId, merlionAdultLocalProductId);
//        tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));
//        log.info("4. Adding Merlion Adult Standard.");
//        response = stub.generateTicket(transactionId, qty, merlionAdultStandardItemId, merlionAdultStandardProductId);
//        tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));
//        log.info("5. Adding Merlion Child Local.");
//        response = stub.generateTicket(transactionId, qty, merlionChildLocalItemId, merlionChildLocalProductId);
//        tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));
//
//        log.info("6. ADDING ONE MORE Merlion Adult Standard.");
//        response = stub.cartUpdateQuantity(transactionId, qty, merlionAdultStandardItemId, merlionAdultStandardProductId);
//        tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));
//
//        log.info("7. Performing final checkout.");
//        response = stub.cartCheckout(transactionId);
//        tickets = AxRetailTicketRecordTransformer.fromWs(response.getRetailTicketTableCollection().getValue().getSDCRetailTicketTable());
//        log.info("Response: " + JsonUtil.jsonify(tickets));
//
//        log.info("Finished calling [testAxRetailWs].");
    }

    @Test
    public void testAxRetailWsError() throws Exception {
        //Log4j basic configuration
        BasicConfigurator.configure();
        //Outputs all SOAP calls to console
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        log.info("Calling [testAxRetailWsError].");

        final String existingTransactionId = "000001-SLMOTC-02-b2ctest1";

        final String transactionId = "EnovaxTest-" + RandomStringUtils.random(6, true, false);
        log.info("Transaction ID generated: " + transactionId);

        final BigDecimal qty = BigDecimal.ONE;
        final String merlionAdultLocalItemId = "1030100001";
        final Long merlionAdultLocalProductId = 5637145331L;
        final String merlionAdultStandardItemId = "1030100002";
        final Long merlionAdultStandardProductId = 5637145332L;
        final String merlionChildLocalItemId = "1030100003";
        final Long merlionChildLocalProductId = 5637145333L;

        final String serviceEndpointUrl = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/services/SDC_RetailTicketTableService.svc";

        final SDCRetailTicketTableService ws = new SDCRetailTicketTableService(new URL(serviceEndpointUrl));
        final ISDCRetailTicketTableService stub = ws.getBasicHttpBindingISDCRetailTicketTableService();

        SDCRetailTicketTableResponse response;
        List<AxRetailTicketRecord> tickets;

        log.info("Erroring checkout.");
//        response = stub.cartCheckout(transactionId);

        log.info("Finished calling [testAxRetailWsError].");
    }

    @Test
    public void doPinGenerationFlow() throws Exception {
        final StoreApiChannels channel = StoreApiChannels.PARTNER_PORTAL_SLM;
        final String transId = RandomStringUtils.random(7, "ABCDEFabcdef1234567890");

        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();

        AxRetailCartTicket ct = new AxRetailCartTicket();
//        ct.setItemId("1030100001");
//        ct.setListingId(5637145331L);
        ct.setItemId("1102101001");
        ct.setListingId(5637145352L);
        ct.setQty(1);
        ct.setLineId("");
        ct.setTransactionId(transId);
        cartTickets.add(ct);

        final List<AxRetailGeneratePinInput> pinInputs = new ArrayList<>();
        final AxRetailGeneratePinInput pi = new AxRetailGeneratePinInput();

        pi.setAllowPartialRedemption(false);
        pi.setCombineTicket(false);
        pi.setCustomerAccount("");
        pi.setDescription("Test");
        pi.setEndDateTime(new Date());

        pi.setEventDate(new Date());
        pi.setEventGroupId("");
        pi.setEventLineId("");
        pi.setGroupTicket(false);
        pi.setInventTransId(transId);

        pi.setInvoiceId("");
//        pi.setItemId("1030100001");
        pi.setItemId("1102101001");
        pi.setLineNumber(BigDecimal.ONE);
        pi.setMediaType("");
        pi.setPackageName("Test Package");

        pi.setQty(BigDecimal.ONE);
        pi.setQtyPerProduct(BigDecimal.ONE);
        pi.setReferenceId(transId);
        pi.setRetailVariantId("");
        pi.setSalesId("");

        pi.setStartDateTime(new Date());
        pi.setTransactionId(transId);

        pinInputs.add(pi);

        final ApiResult<List<AxRetailPinTable>> generatePinResult = axChannelTransactionService.apiGeneratePin(channel, pinInputs);
        log.info("GeneratePin RESULT: " + JsonUtil.jsonify(generatePinResult));
    }
}
