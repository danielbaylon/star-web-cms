package com.enovax.star.cms.api.store.service.batch.ppmflgaxsync;

import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerAXService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by houtao on 27/9/16.
 */
@Service("PPMFLGIPartnerProfileSyncJob")
public class AxPartnerProfileSyncJob implements IPartnerProfileSyncJob{

    private static final Logger log = LoggerFactory.getLogger(AxPartnerProfileSyncJob.class);

    @Autowired
    @Qualifier("queuedTaskExecutor")
    private ThreadPoolTaskExecutor queuedTaskExecutor;

    @Autowired
    @Qualifier("PPMFLGIPartnerAXService")
    private IPartnerAXService paAxSrv;

    private ConcurrentLinkedQueue<Integer> paIdList = new ConcurrentLinkedQueue<Integer>();

    private List<AxPartnerProfileSyncTask> taskList = new ArrayList<AxPartnerProfileSyncTask>();

    @PostConstruct
    public void init(){
        for(int i = 0 ; i < 2 ; i++){
            createNewSyncTask(null);
        }
    }

    public void sync(Integer paId){
        if(paId == null || paId.intValue() == 0){
            return;
        }
        if(paIdList.contains(paId)){
            return;
        }
        paIdList.add(paId);
    }

    public boolean check(Integer paId){
        if(paId == null || paId.intValue() == 0){
            return false;
        }
        if(paIdList == null || paIdList.size() == 0){
            return false;
        }
        return paIdList.contains(paId);
    }

    private AxPartnerProfileSyncTask createNewSyncTask(Integer paId) {
        AxPartnerProfileSyncTask a = new AxPartnerProfileSyncTask();
        a.setPaAxSrv(paAxSrv);
        if(paId != null){
            a.setPartnerId(paId);
        }else{
            a.setDone(true);
        }
        taskList.add(a);
        return a;
    }

    /**
     * This is a frontend job
     */
    @Scheduled(initialDelay = 60000, fixedRate = 30000)
    public void syncPartnerProfileJob(){
        if(queuedTaskExecutor != null){
            int queueSize = paIdList.size();
            do{
                if(queueSize > 0){
                    Integer paId = paIdList.poll();
                    boolean found = false;
                    for(AxPartnerProfileSyncTask t : taskList){
                        if(t.isDone()){
                            t.setPartnerId(paId);
                            queuedTaskExecutor.submit(t);
                            found = true;
                            break;
                        }
                    }
                    if(!found){
                        AxPartnerProfileSyncTask task = createNewSyncTask(paIdList.peek());
                        queuedTaskExecutor.submit(task);
                    }
                }
                queueSize = paIdList.size();
            }while(queueSize > 0);
        }
    }
}
