package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 29/9/16.
 */
public interface IPartnerBookingOfflinePayService {
    ApiResult<InventoryTransactionVM> doOfflineCheckout(StoreApiChannels channel, HttpSession session, PartnerAccount partnerAccount, String ip);
    public ApiResult<InventoryTransactionVM> doOfflineCheckout(StoreApiChannels channel, FunCart cart, PartnerAccount partnerAccount, String ip);
    public ApiResult<Integer> saveOfflineRequest(StoreApiChannels channel, HttpSession session, PartnerAccount account);

    ApiResult<OfflinePaymentResultVM> getOfflinePaymentRequestDetailsById(StoreApiChannels channel, PartnerAccount account, Integer reqId) throws BizValidationException;

    ApiResult<OfflinePaymentResultVM> withdrawOfflineRequest(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input);

    ApiResult<String> notifyApprovers(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input);

    ApiResult<OfflinePaymentResultVM> submitOfflinePaymentRequest(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input);

    ApiResult<PagedData<List<OfflinePaymentRequestVM>>> getPagedOfflinePaymentRequest(PartnerAccount account, String receiptNum, Date startDate, Date endDate, String status, String username, String sortField, String sortDirection, int page, int pageSize) throws Exception;

    void cleanShoppingCart(StoreApiChannels channel, String sessionId);
}
