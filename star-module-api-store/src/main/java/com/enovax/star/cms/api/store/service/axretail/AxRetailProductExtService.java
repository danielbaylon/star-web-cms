package com.enovax.star.cms.api.store.service.axretail;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.axretail.inventtable.AxRetailProductExtServiceStubDetails;
import com.enovax.star.cms.api.store.transformer.axretail.inventtable.AxRetailProductExtResponseTransformer;
import com.enovax.star.cms.commons.ws.axretail.inventtable.ISDCInventTableService;
import com.enovax.star.cms.commons.ws.axretail.inventtable.ObjectFactory;
import com.enovax.star.cms.commons.ws.axretail.inventtable.SDCInventTableExtCriteria;
import com.enovax.star.cms.commons.ws.axretail.inventtable.SDCInventTableResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
@Service
public class AxRetailProductExtService {

    private Logger log = LoggerFactory.getLogger(getClass());

    private boolean firstStart = true;

    private Map<StoreApiChannels, AxRetailProductExtServiceStubDetails> channelStubs = new HashMap<>();

    @Value("${api.store.axretail.api.url.inventtable.b2cmflg}")
    private String apiUrlB2CMFLG;
    @Value("${api.store.axretail.api.url.inventtable.b2cslm}")
    private String apiUrlB2CSLM;
    @Value("${api.store.axretail.api.url.inventtable.kiosk}")
    private String apiUrlKIOSK;
    @Value("${api.store.axretail.api.url.inventtable.partnerportal}")
    private String apiUrlPARTNERPORTAL;

    //TODO Synchronization issues on initialisation

    @PostConstruct
    private void initService() {
        try {

            if (StringUtils.isEmpty(apiUrlB2CSLM) || "${api.store.axretail.api.url.inventtable.b2cslm}".equals(apiUrlB2CSLM)) {
//                apiUrlB2CSLM = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_InventTableService.svc";
                apiUrlB2CSLM = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50000/Services/SDC_InventTableService.svc";
            }
            if (StringUtils.isEmpty(apiUrlB2CMFLG) || "${api.store.axretail.api.url.inventtable.b2cmflg}".equals(apiUrlB2CMFLG)) {
                apiUrlB2CMFLG = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50005/Services/SDC_InventTableService.svc";
            }
            if (StringUtils.isEmpty(apiUrlPARTNERPORTAL) || "${api.store.axretail.api.url.inventtable.partnerportal}".equals(apiUrlPARTNERPORTAL)) {
//                apiUrlPARTNERPORTAL = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_InventTableService.svc";
                apiUrlPARTNERPORTAL = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50001/Services/SDC_InventTableService.svc";
            }
            if (StringUtils.isEmpty(apiUrlKIOSK) || "${api.store.axretail.api.url.inventtable.kiosk}".equals(apiUrlKIOSK)) {
                apiUrlKIOSK = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50007/Services/SDC_InventTableService.svc";
            }

            channelStubs.put(StoreApiChannels.B2C_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlB2CMFLG));
            channelStubs.put(StoreApiChannels.B2C_SLM, new AxRetailProductExtServiceStubDetails(apiUrlB2CSLM));
            channelStubs.put(StoreApiChannels.KIOSK_SLM, new AxRetailProductExtServiceStubDetails(apiUrlKIOSK));
            channelStubs.put(StoreApiChannels.KIOSK_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlKIOSK));
            channelStubs.put(StoreApiChannels.PARTNER_PORTAL_SLM, new AxRetailProductExtServiceStubDetails(apiUrlPARTNERPORTAL));
            channelStubs.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlPARTNERPORTAL));

            if (firstStart) { firstStart = false; return; }

            boolean success;

            log.info("Initialising AX Retail Product Ext Service for B2C MFLG.");
            success = channelStubs.get(StoreApiChannels.B2C_MFLG).initialise();
            log.info("AX Retail Product Ext Service for B2C MFLG : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Retail Product Ext Service for B2C SLM.");
            success = channelStubs.get(StoreApiChannels.B2C_SLM).initialise();
            log.info("AX Retail Product Ext Service for B2C SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Retail Product Ext Service for KIOSK SLM.");
            success = channelStubs.get(StoreApiChannels.KIOSK_SLM).initialise();
            log.info("AX Retail Product Ext Service for KIOSK SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Retail Product Ext Service for KIOSK MFLG.");
            success = channelStubs.get(StoreApiChannels.KIOSK_MFLG).initialise();
            log.info("AX Retail Product Ext Service for KIOSK MFLG: INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Product Ext Retail Service for PARTNER PORTAL SLM.");
            success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_SLM).initialise();
            log.info("AX Retail Service Product Ext for PARTNER PORTAL SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Product Ext Retail Service for PARTNER PORTAL MFLG.");
            success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_MFLG).initialise();
            log.info("AX Retail Service Product Ext for PARTNER PORTAL MFLG : INIT " + (success ? "SUCCESS" : "FAILED"));

        } catch (Exception e) {
            log.error("Error initialising AX Retail Product Ext Service.", e);
        }
    }

    public ApiResult<List<AxRetailProductExtData>> apiSearchProductExt(StoreApiChannels channel,
                                                                       boolean isAll, String itemId, Long listingId)
            throws AxRetailServiceException {
        log.info("[AX Retail Product Ext Service Client] Calling apiSearchProductExt");
        try {


            final AxRetailProductExtServiceStubDetails channelStub = channelStubs.get(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }
            final ISDCInventTableService stub = channelStub.getStub();

            ObjectFactory factory = new ObjectFactory();

            final SDCInventTableExtCriteria criteria = factory.createSDCInventTableExtCriteria();
            if (isAll) {
                criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(""));
                criteria.setProductId(0L);
            } else {
                criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(itemId));
                criteria.setProductId(listingId);
            }

            final SDCInventTableResponse axResponse = stub.searchProductExt(criteria);
            final ApiResult<List<AxRetailProductExtData>> result = AxRetailProductExtResponseTransformer.fromWs(axResponse);

            return result;
        } catch (Exception e) {
            final String errMsg = "[AX Retail Product Ext Service Client] Error: apiSearchProductExt";
            log.error(errMsg, e);
            throw new AxRetailServiceException(errMsg, e);
        }
    }
}
