package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.*;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.partnershared.repository.ppslm.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.IPartnerRepository;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerAxDocService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerDocumentService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerEmailService;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import info.magnolia.cms.beans.runtime.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DefaultPartnerRegistrationService implements IPartnerRegistrationService {

    public static final Logger log = LoggerFactory.getLogger(DefaultPartnerRegistrationService.class);

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private ICountryRegionsRepository ctryRegionRepo;

    @Autowired
    private PPSLMPartnerRepository paRepo;

    @Autowired
    private PPSLMPartnerExtRepository paExtRepo;

    @Autowired
    private IPartnerRepository paTierRepo;

    @Autowired
    private IPartnerAccountService paAccSrv;

    @Autowired
    private IPartnerDocumentService paDocSrv;

    @Autowired
    private IPartnerAxDocService axDocSrv;


    @Autowired
    private PPSLMApprovalLogRepository apprLogRepo;

    @Autowired
    private PPSLMPPSLMReasonLogMapRepository reasonLogMapRepo;

    @Autowired
    private PPSLMReasonRepository reasonRepo;

    @Autowired
    private PPSLMPADocMappingRepository paDocMapRepo;

    @Autowired
    private PPSLMPaDistributionMappingRepository paDisMapRepo;

    @Autowired
    private IPartnerEmailService paEmailSrv;

    @Autowired
    private ILineOfBusinessRepository paTypeRepo;

    @Transactional(readOnly = true)
    public PartnerRegistrationVM getPartnerRegistrationParam() {

        List<PartnerTypeVM> partnerTypes = paTypeRepo.getPartnerTypeList(PartnerPortalConst.Partner_Portal_Ax_Data);
        List<CountryVM> countries = ctryRegionRepo.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        List<DropDownVM> partnerDocTypes = PartnerDocType.getInUseDocTypes();

        PartnerRegistrationVM vm = new PartnerRegistrationVM();
        vm.setCountries(countries);
        vm.setPartnerTypes(partnerTypes);

        if(partnerDocTypes != null && partnerDocTypes.size() > 0){
            vm.setPartnerDocTypes(partnerDocTypes);
        }
        return vm;
    }

    @Transactional
    public String registerPartner(PartnerVM paVM, Document doc1, Document doc2)throws Exception {
        if((!isValidPartnerDocument(doc1)) || (!isValidPartnerDocument(doc2))){
            return "MAXIMUM_PARTNER_DOC_SIZE";
        }
        if(paVM == null){
            return "INSUFFICIENT_DATA";
        }
        if(paVM.getId() == null){
            return registerNewPartner(paVM, doc1, doc2);
        }else{
            return updateExistingPartner(paVM, doc1, doc2);
        }
    }

    private boolean isValidPartnerDocument(Document doc) {
        if(doc == null){
            return true;
        }
        if(!(doc.getLength() > 0 && doc.getLength() < sysParamSrv.getPartnerDocumentMaximumSizeLimit())){
            return false;
        }
        String ext = doc.getExtension();
        if(ext == null || ext.trim().length() == 0){
            return false;
        }
        String supportedType = sysParamSrv.getPartnerDocumentSupportedFileType();
        if(supportedType == null || supportedType.trim().length() == 0){
            return false;
        }
        if(!(supportedType.trim().toUpperCase().indexOf("#"+ext.trim().toUpperCase()+"#") >= 0)){
            return false;
        }
        return true;
    }

    @Transactional
    private String updateExistingPartner(PartnerVM paVM, Document doc1, Document doc2) throws Exception {
        if(paVM == null){
            return "INSUFFICIENT_DATA";
        }
        PPSLMPartner partnerById = this.paRepo.findById(paVM.getId());
        if(partnerById == null){
            return "INVALID_PARTNER_ID";
        }
        PPSLMPartner partnerByCode = paRepo.findByAccountCode(paVM.getAccountCode());
        if(partnerByCode != null && partnerByCode.getId().intValue() != partnerById.getId().intValue()){
            return "REG_CODE_IN_USE";
        }
        if(!PartnerStatus.PendingReSubmit.code.equalsIgnoreCase(partnerById.getStatus())){
            return "INVALID_PARTNER_RESUBMIT_REQUEST";
        }
        PPSLMTAMainAccount taMainAcc = paAccSrv.updateTAMainAccount(partnerById, paVM);
        PPSLMPartner pa = this.updatePartner(partnerById, paVM, taMainAcc);
        updatePartnerExtProperties(pa, paVM);
        paVM.setId(pa.getId());
        paVM.setCreatedDateStr(NvxDateUtils.formatDateForDisplay(pa.getCreatedDate(), true));
        paDocSrv.savePartnerDocuments(taMainAcc, pa, doc1, paVM.getFile1Type());
        paDocSrv.savePartnerDocuments(taMainAcc, pa, doc2, paVM.getFile2Type());
        axDocSrv.syncPartnerDocuments(pa, paDocMapRepo.findByPartnerId(pa.getId()));
        PartnerHist paHist = preparePartnerHist(pa.getId());
        this.saveApprovalLog(pa, paVM, paHist, ActionType.Update, paVM.getOrgName() + PartnerPortalConst.PARTNER_RESUBMIT_LABEL);
        paEmailSrv.sendSignupUpdateEmail(paVM, paHist);
        paEmailSrv.sendSignupUpdateAlertEmail(paVM, paHist);
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public String registerNewPartner(PartnerVM paVM, Document doc1, Document doc2)throws Exception {
        if(paVM == null || doc1 == null || doc2 == null || doc1.getFile() == null || doc2.getFile() == null){
            return "INSUFFICIENT_DATA";
        }
        PPSLMPartner partner = paRepo.findByAccountCode(paVM.getAccountCode());
        if(partner != null){
            return "REG_CODE_IN_USE";
        }
        PPSLMTAMainAccount taMainAcc = paAccSrv.createTAMainAccount(paVM);
        PPSLMPartner pa = this.createPartner(paVM, taMainAcc);
        createPartnerExtProperties(paVM, pa);
        paVM.setId(pa.getId());
        paVM.setCreatedDateStr(NvxDateUtils.formatDateForDisplay(pa.getCreatedDate(), true));
        paDocSrv.savePartnerDocuments(taMainAcc, pa, doc1, paVM.getFile1Type());
        paDocSrv.savePartnerDocuments(taMainAcc, pa, doc2, paVM.getFile2Type());
        axDocSrv.syncPartnerDocuments(pa, paDocMapRepo.findByPartnerId(pa.getId()));
        PartnerHist paHist = preparePartnerHist(pa.getId());
        this.saveApprovalLog(pa, paVM, paHist, ActionType.Create, paVM.getOrgName() + PartnerPortalConst.PARTNER_REGISTER_LABEL);
        paEmailSrv.sendSignupEmail(paVM, paHist);
        paEmailSrv.sendSignupAlertEmail(paVM, paHist);
        return null;
    }

    private void updatePartnerExtProperties(PPSLMPartner pa, PartnerVM paVM) {
        if(paVM == null || paVM.getExtension() == null || paVM.getExtension().size() == 0 ) {
            if(pa == null){
                return;
            }
            if(pa.getPartnerExtList() == null || pa.getPartnerExtList().size() == 0){
                return;
            }
            List<PPSLMPartnerExt> list =  pa.getPartnerExtList();
            for(PPSLMPartnerExt ext : list){
                if(ext.getAttrName() != null && ext.getAttrName().trim().length() > 0){
                    ext.setStatus(GeneralStatus.Inactive.code);
                    paExtRepo.save(ext);
                }else{
                    paExtRepo.delete(ext);
                }
            }
            return;
        }

        List<PPSLMPartnerExt> list = paExtRepo.findAllPartnerExtByPartnerId(pa.getId());

        List<PPSLMPartnerExt> extlist = new ArrayList<PPSLMPartnerExt>();
        List<PartnerExtVM> exts = paVM.getExtension();
        if(exts != null && exts.size() > 0){
            for(PartnerExtVM e : exts){
                boolean found = false;
                if(list != null && list.size() > 0){
                    for(PPSLMPartnerExt ext : list){
                        if(ext.getAttrName() != null && ext.getAttrName().equals(e.getAttrName())){
                            ext.setAttrValue(e.getAttrValue());
                            ext.setStatus(GeneralStatus.Active.code);
                            paExtRepo.save(ext);
                            found = true;
                            break;
                        }
                    }
                }
                if(!found){
                    PPSLMPartnerExt ext = new PPSLMPartnerExt();
                    ext.setPartner(pa);
                    ext.setPartnerId(pa.getId());
                    ext.setStatus(GeneralStatus.Active.code);
                    ext.setApprovalRequired(e.getApprovalRequired());
                    ext.setAttrName(e.getAttrName());
                    ext.setAttrValue(e.getAttrValue());
                    ext.setAttrValueType(e.getAttrValueType());
                    ext.setAttrValueFormat(e.getAttrValueFormat());
                    ext.setMandatoryInd(e.getMandatoryInd());
                    ext.setDescription(e.getDescription());
                    extlist.add(ext);
                }
            }
        }
        if(extlist.size() > 0){
            paExtRepo.save(extlist);
        }
    }

    private void createPartnerExtProperties(PartnerVM paVM, PPSLMPartner pa) {
        if(paVM == null || paVM.getExtension() == null || paVM.getExtension().size() == 0 || pa == null) {
            return;
        }
        List<PPSLMPartnerExt> extlist = new ArrayList<PPSLMPartnerExt>();
        List<PartnerExtVM> exts = paVM.getExtension();
        for(PartnerExtVM e : exts){
            PPSLMPartnerExt ext = new PPSLMPartnerExt();
            ext.setPartner(pa);
            ext.setPartnerId(pa.getId());
            ext.setStatus(GeneralStatus.Active.code);
            ext.setApprovalRequired(e.getApprovalRequired());
            ext.setAttrName(e.getAttrName());
            ext.setAttrValue(e.getAttrValue());
            ext.setAttrValueType(e.getAttrValueType());
            ext.setAttrValueFormat(e.getAttrValueFormat());
            ext.setMandatoryInd(e.getMandatoryInd());
            ext.setDescription(e.getDescription());
            extlist.add(ext);
        }
        if(extlist.size() > 0) {
            paExtRepo.save(extlist);
        }
    }

    @Transactional
    public boolean isAnExistingPartner(String accountCode, Integer id) {
        if(accountCode == null || accountCode.trim().length() == 0){
            return true;
        }
        PPSLMPartner obj = paRepo.findByAccountCode(accountCode);
        if(obj != null){
            if(id != null && id.intValue() > 0){
                if(id.intValue() == obj.getId().intValue()){
                    obj = null;
                    return false;
                }
            }
            obj = null;
            return true;
        }
        return false;
    }

    @Transactional(readOnly = true)
    public PartnerVM getPartnerDetailsByPartnerId(String accCodeId) {
        PPSLMPartner partner = paRepo.findByAccountCode(accCodeId);
        if(partner == null){
            return null;
        }
        PartnerVM partnerVM = new PartnerVM(partner);
        ///partnerVM.setTierId(paTierRepo.getTierIdByPartnerId(PartnerPortalConst.Partner_Portal_Channel, partner.getId()));
        List<PartnerDocumentVM> paDocList = new ArrayList<PartnerDocumentVM>();
        List<PPSLMPADocMapping> docList = paDocMapRepo.findByPartnerId(partner.getId());
        if(docList != null){
            for(PPSLMPADocMapping map : docList){
                PPSLMPADocument paDoc = map.getPaDoc();
                if(paDoc != null){
                    paDocList.add(new PartnerDocumentVM(paDoc));
                }
            }
        }
        partnerVM.setPaDocs(paDocList);
        return partnerVM;
    }

    @Transactional(readOnly = true)
    public String populatePartnerRegistrationFormResubmitRemarks(PartnerVM paVM) {
        String reason = "";
        String remarks = "";
        if(paVM != null && paVM.getId() != null && paVM.getId().intValue() > 0){
            List<PPSLMApprovalLog> list = apprLogRepo.findPartnerLatestApproverLog(String.valueOf(paVM.getId()));
            if(list != null && list.size() > 0){
                remarks = list.get(0).getRemarks();
                Integer logId = list.get(0).getId();
                if(logId != null && logId.intValue() > 0){
                    List<PPSLMReasonLogMap> logMapList = reasonLogMapRepo.findByLogId(logId);
                    if(logMapList != null && logMapList.size() > 0){
                        PPSLMReasonLogMap reasonLogMap = logMapList.get(0);
                        if(reasonLogMap != null && reasonLogMap.getReasonId() != null && reasonLogMap.getReason() != null){
                            PPSLMReason ppslmReason = reasonRepo.findById(reasonLogMap.getReasonId());
                            if(ppslmReason != null && ppslmReason.getTitle() != null && ppslmReason.getTitle().trim().length() > 0){
                                reason = ppslmReason.getTitle();
                            }
                        }
                    }
                }
            }
        }
        if(remarks != null && remarks.trim().length() > 0){
            remarks = remarks.trim();
        }else{
            remarks = "";
        }
        if(reason != null && reason.trim().length() > 0){
            reason = reason.trim();
        }else{
            reason = "";
        }
        paVM.setResubmitReason(reason);
        paVM.setResubmitRemarks(remarks);
        paVM.setResubmitRequired(true);
        return remarks;
    }

    private void saveApprovalLog(PPSLMPartner pa, PartnerVM paVM, PartnerHist paHist, ActionType actionType, String label) {
        PPSLMApprovalLog appLog = new PPSLMApprovalLog();
        appLog.setRelatedKey(pa.getId().toString());
        appLog.setCurrentValue(NvxUtil.getXmlfromObj(paHist));
        appLog.setActionType(actionType.code);
        appLog.setDescription(label);
        appLog.setCategory(ApproverCategory.Partner.code);
        appLog.setCreatedBy(paVM.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        appLog.setCreatedDate(new Date());
        apprLogRepo.save(appLog);
    }

    private PartnerHist preparePartnerHist(Integer id) {
        log.debug("preparePartnerHist......"+id);
        PPSLMPartner pa = paRepo.findOne(id);
        List<Integer> paDocIds = new ArrayList<Integer>();
        log.debug("getDocMapByPaId...");
        List<PPSLMPADocMapping> paDocMaps = paDocMapRepo.findByPartnerId(pa.getId());
        if(paDocMaps!=null){
            for(PPSLMPADocMapping tmpObj : paDocMaps){
                if(tmpObj!=null){
                    paDocIds.add(tmpObj.getDocId());
                }
            }
        }
        List<PPSLMPaDistributionMapping> distributionMapping =  paDisMapRepo.findByPartnerId(pa.getId());
        List<PaDistributionMappingHist> pdmHist = new ArrayList<PaDistributionMappingHist>();
        if(distributionMapping!=null){
            for(PPSLMPaDistributionMapping tmpObj:distributionMapping){
                pdmHist.add(new PaDistributionMappingHist(tmpObj));
            }
        }
        PartnerHist paHist = new PartnerHist(pa);
        paHist.setPaDocIds(paDocIds);
        paHist.setDistributionMapping(pdmHist);
        PPSLMTAMainAccount mainAcc = pa.getMainAccount();
        if(mainAcc!=null){
            paHist.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
        }
        return paHist;
    }

    private PPSLMPartner createPartner(PartnerVM paVm, PPSLMTAMainAccount taMainAcc) {
        PPSLMPartner pa = new PPSLMPartner();
        pa.setAdminId(taMainAcc.getId());
        pa.setAccountCode(paVm.getAccountCode().toUpperCase());
        pa.setOrgName(paVm.getOrgName());
        pa.setOrgTypeCode(paVm.getOrgTypeCode());
        pa.setCountryCode(paVm.getCountryCode());
        pa.setUen(paVm.getUen());
        pa.setLicenseNum(paVm.getLicenseNum());
        pa.setRegistrationYear(paVm.getRegistrationYear());
        pa.setLicenseExpDate(paVm.getLicenseExpDate());
        pa.setContactPerson(paVm.getContactPerson());
        pa.setContactDesignation(paVm.getContactDesignation());
        pa.setAddress(paVm.getAddress());
        pa.setPostalCode(paVm.getPostalCode());
        pa.setCity(paVm.getCity());
        pa.setTelNum(paVm.getTelNum());
        pa.setMobileNum(paVm.getMobileNum());
        pa.setFaxNum(paVm.getFaxNum());
        pa.setEmail(paVm.getEmail().toUpperCase());
        pa.setWebsite(paVm.getWebsite());
        pa.setLanguagePreference(paVm.getLanguagePreference());
        pa.setMainDestinations(paVm.getMainDestinations());
        pa.setStatus(PartnerStatus.PendingVerify.code);
        pa.setCreatedBy(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        pa.setCreatedDate(new Date());
        pa.setModifiedBy(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        pa.setModifiedDate(new Date());
        this.paRepo.save(pa);
        return pa;
    }


    private PPSLMPartner updatePartner(PPSLMPartner pa,PartnerVM paVm, PPSLMTAMainAccount taMainAcc) {
        pa.setAdminId(taMainAcc.getId());
        pa.setAccountCode(paVm.getAccountCode().toUpperCase());
        pa.setOrgName(paVm.getOrgName());
        pa.setOrgTypeCode(paVm.getOrgTypeCode());
        pa.setCountryCode(paVm.getCountryCode());
        pa.setUen(paVm.getUen());
        pa.setLicenseNum(paVm.getLicenseNum());
        pa.setRegistrationYear(paVm.getRegistrationYear());
        pa.setLicenseExpDate(paVm.getLicenseExpDate());
        pa.setContactPerson(paVm.getContactPerson());
        pa.setContactDesignation(paVm.getContactDesignation());
        pa.setAddress(paVm.getAddress());
        pa.setPostalCode(paVm.getPostalCode());
        pa.setCity(paVm.getCity());
        pa.setTelNum(paVm.getTelNum());
        pa.setMobileNum(paVm.getMobileNum());
        pa.setFaxNum(paVm.getFaxNum());
        pa.setEmail(paVm.getEmail().toUpperCase());
        pa.setWebsite(paVm.getWebsite());
        pa.setLanguagePreference(paVm.getLanguagePreference());
        pa.setMainDestinations(paVm.getMainDestinations());
        pa.setStatus(PartnerStatus.PendingVerify.code);
        pa.setCreatedBy(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        pa.setCreatedDate(new Date());
        pa.setModifiedBy(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        pa.setModifiedDate(new Date());
        this.paRepo.save(pa);
        return pa;
    }


    @Override
    public PPSLMPartner getPartnerByAxAccountNumber(String custAccNum) {
        return this.paRepo.findByAxAccountNumber(custAccNum);
    }
}
