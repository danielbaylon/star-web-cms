package com.enovax.star.cms.api.store.controller.ppmflg.secured;


import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.api.store.controller.ppmflg.PPMFLGBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.api.store.service.ppmflg.ITelemoneyPPMFLGService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartDisplay;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerReceiptDisplay;
import com.enovax.star.cms.commons.model.product.*;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerBookingService;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerProductService;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerTncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/store-partner/")
public class PPMFLGPartnerOnlineStoreController extends PPMFLGBaseStoreApiController {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    private HttpSession session;

    @Autowired
    @Qualifier("PPMFLGIPartnerBookingService")
    private IPartnerBookingService bookingService;
    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService accountService;
    @Autowired
    private ITicketGenerationService ticketGenerationService;
    @Autowired
    @Qualifier("PPMFLGIPartnerProductService")
    private IPartnerProductService partnerProductService;
    @Autowired
    private IProductService productService;
    @Autowired
    @Qualifier("PPMFLGIPartnerTncService")
    private IPartnerTncService tncService;

    @Autowired
    private ITelemoneyPPMFLGService telemoneyService;

    public static final String SESS_TRANS_ID = "SESS_TRANS_ID";


    //TODO i18n functionality
    //TODO Switch to partner booking service
    //TODO Change implementation of login, maybe make use of Spring Security

    @RequestMapping(value = "products-ext", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiGetProductsExt(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody ProductItemCriteriaWrapper data) {
        log.info("Entered apiGetProductsExt...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final List<String> productIds = new ArrayList<>();
            for (ProductItemCriteria crit : data.getProducts()) {
                productIds.add(crit.getProductId());
            }

            final ApiResult<List<ProductExtViewModel>> result = productService.getDataForProducts(channel, productIds);
            if (result.isSuccess()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", new ProductExtViewModelWrapper(result.getData())), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(false, result.getErrorCode(), result.getMessage(), null), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetProductsExt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ProductExtViewModelWrapper.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-to-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerFunCartDisplay>> apiAddToCart(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody FunCart cart) {
        log.info("Entered apiAddToCart...");
        try {

            final ApiResult<PartnerFunCartDisplay> loggedInResult = isLoggedInWrappedResult(session, PartnerFunCartDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            PartnerAccount account = getLoginUserAccount(session);

            final ApiResult<PartnerFunCartDisplay> result = bookingService.cartAdd(channel, cart, sessionId, account.getPartner().getAxAccountNumber());

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerFunCartDisplay>> apiUpdateCart(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody FunCart cart) {
        log.info("Entered apiUpdateCart...");
        try {

            final ApiResult<PartnerFunCartDisplay> loggedInResult = isLoggedInWrappedResult(session, PartnerFunCartDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<PartnerFunCartDisplay> result = bookingService.cartUpdate(channel, cart, sessionId);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiUpdateCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-item-from-cart/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerFunCartDisplay>> apiRemoveItemFromCart(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @PathVariable("id") String id) {
        log.info("Entered apiRemoveItemFromCart...");
        try {

            final ApiResult<PartnerFunCartDisplay> loggedInResult = isLoggedInWrappedResult(session, PartnerFunCartDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<PartnerFunCartDisplay> result = bookingService.cartRemoveItem(channel, sessionId, id);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveItemFromCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-topup-to-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerFunCartDisplay>> apiAddTopupToCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                                       @RequestBody FunCart cart) {
        log.info("Entered apiAddTopupToCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<PartnerFunCartDisplay> result = bookingService.cartAddTopup(channel, sessionId, cart);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddTopupToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "clear-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiClearCart(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiClearCart...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            bookingService.cartClear(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiClearCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-cart", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<FunCartDisplay>> apiGetCart(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCart...");
        try {

            final ApiResult<FunCartDisplay> loggedInResult = isLoggedInWrappedResult(session, FunCartDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final FunCartDisplay funCartDisplay = bookingService.constructCartDisplay(channel, sessionId);

            final List<String> productIds = new ArrayList<>();
            for(FunCartDisplayCmsProduct product: funCartDisplay.getCmsProducts()) {
                productIds.add(product.getId());
            }

            if(accountService.isPartnerOfflinePaymentEnabled(getLoginUserAccount(session))){
                funCartDisplay.getPayMethodOptions().add("offlinePaymentEnabled");
            }
            session.setAttribute(SESS_TRANS_ID, productIds);


            //TODO get all the product and the tnc for the product

            //List<PartnerFunCartDisplay> funCartDisplay.getCmsProducts()

                    //SESS_PROD_IDS_FOR_TNC

            return new ResponseEntity<>(new ApiResult<>(true, "", "", funCartDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-tnc", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<List<TncVM>>> apiGetTnc(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetTnc...");
        try {

            final ApiResult<List<TncVM>> loggedInResult = isLoggedInWrappedResultForList(session, TncVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            List<String> prodIds = (List<String>)session.getAttribute(SESS_TRANS_ID);

            List<TncVM> tncList = tncService.getProductTncs(channel, prodIds);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", tncList), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCart] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, TncVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-cart-size", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Integer>> apiGetCartSize(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCartSize...");
        try {

            final ApiResult<Integer> loggedInResult = isLoggedInWrappedResult(session, Integer.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            Integer cartSize = bookingService.getCartSize(channel, sessionId);


            return new ResponseEntity<>(new ApiResult<>(true, "", "", cartSize), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCartSize] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, Integer.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-checkout-display", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<CheckoutDisplay>> apiGetCheckoutDisplay(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCheckoutDisplay...");
        try {

            final ApiResult<CheckoutDisplay> loggedInResult = isLoggedInWrappedResult(session, CheckoutDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final CheckoutDisplay checkoutDisplay = bookingService.constructCheckoutDisplayFromCart(channel, session);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", checkoutDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCheckoutDisplay] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CheckoutDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<InventoryTransactionVM>> apiCheckout(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckout...");
        try {

            final ApiResult<InventoryTransactionVM> loggedInResult = isLoggedInWrappedResult(session, InventoryTransactionVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);

            final Trackd trackd = trackr.buildTrackd(session.getId(), request);
            String ip = "";
            if(trackd != null) {
                ip = trackd.getIp();
            }
            final ApiResult<InventoryTransactionVM> result = bookingService.doCheckout(channel, session, account, ip);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, InventoryTransactionVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout-cancel", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCheckoutCancel(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckoutCancel...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            PartnerAccount account = getLoginUserAccount(session);

            final ApiResult<String> result = bookingService.cancelCheckout(channel, session, account);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckoutCancel] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "checkout-confirm", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<CheckoutConfirmResult>> apiCheckoutConfirm(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckoutConfirm...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final Trackd trackd = trackr.buildTrackd(session.getId(), request);

            final ApiResult<CheckoutConfirmResult> result = bookingService.checkoutConfirm(channel, session, trackd);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckoutConfirm] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CheckoutConfirmResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "simulate-telemoney-response", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSimulateTelemoney(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiSimulateTelemoney...");
        try {
            telemoneyService.processMerchantPost((TelemoneyResponse) session.getAttribute(PartnerPortalConst.FAKE_TELEMONEY_RESPONSE));
            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSimulateTelemoney] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


//    @RequestMapping(value = "checkout-complete-sale", method = {RequestMethod.POST})
//    public ResponseEntity<ApiResult<InventoryTransactionVM>> apiCompleteSale(
//            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
//        log.info("Entered apiCompleteSale...");
//        try {
//
//            final ApiResult<InventoryTransactionVM> loggedInResult = isLoggedInWrappedResult(session, InventoryTransactionVM.class);
//            if (!loggedInResult.isSuccess()) {
//                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
//            }
//            PartnerAccount account = getLoginUserAccount(session);
//
//            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
//            final ApiResult<InventoryTransactionVM> result = bookingService.completeSale(channel, account.getEmail(), session);
//
//            return new ResponseEntity<>(result, HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiCompleteSale] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, InventoryTransactionVM.class, ""), HttpStatus.OK);
//        }
//    }

//    @RequestMapping(value = "get-finalised-receipt", method = {RequestMethod.POST})
//    public ResponseEntity<ApiResult<ReceiptDisplay>> apiGetFinalisedTransaction(
//            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
//            @RequestHeader(StoreApiConstants.REQUEST_HEADER_LOCALE) String locale) {
//        log.info("Entered apiGetFinalisedTransaction...");
//        try {
//
//            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
//
//            //
//
//            final ApiResult<StoreTransaction> result = bookingService.getFinalisedTransaction(channel, session);
//            if (!result.isSuccess()) {
//                log.info("Transaction failed..."); //TODO
//                return new ResponseEntity<>(new ApiResult<>(result.getApiErrorCode()), HttpStatus.OK);
//            }
//
//            if (result.isSuccess()) {
//                if (result.getData() == null) {
//                    log.info("Transaction waiting..."); //TODO
//                    return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
//                }
//            }
//
//            final ReceiptDisplay receipt = cartAndDisplayService.getReceiptForDisplayByReceipt(channel, result.getData().getReceiptNumber());
//
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiGetFinalisedTransaction] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, ReceiptDisplay.class, ""), HttpStatus.OK);
//        }
//    }


    @RequestMapping(value = "get-finalised-receipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerReceiptDisplay>> apiGetFinalisedTransaction(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetFinalisedTransaction...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<InventoryTransactionVM> result = bookingService.getFinalisedTransaction(channel, session);
            if (!result.isSuccess()) {
                log.info("Transaction failed..."); //TODO
                return new ResponseEntity<>(new ApiResult<>(result.getApiErrorCode()), HttpStatus.OK);
            }

            if (result.isSuccess()) {
                if (result.getData() == null) {
                    log.info("Transaction waiting..."); //TODO
                    return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
                }
            }

            PartnerAccount account = getLoginUserAccount(session);
            final PartnerReceiptDisplay receipt = bookingService.getReceiptForDisplayBySession(channel, session, account);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetFinalisedTransaction] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerReceiptDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-receipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerReceiptDisplay>> apiGetReceipt(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetReceipt...");
        try {

            final ApiResult<PartnerReceiptDisplay> loggedInResult = isLoggedInWrappedResult(session, PartnerReceiptDisplay.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            final PartnerReceiptDisplay receipt = bookingService.getReceiptForDisplayBySession(channel, session, account);

            if (receipt == null) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "No transaction found", null), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerReceiptDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-products-by-category/{categoryNodeName}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<List<CmsMainProduct>>> apiGetProductsByCategory(@PathVariable("categoryNodeName") String categoryNodeName) {
        log.info("Entered apiGetProductsByCategory...");
        try {
            PartnerAccount account = getLoginUserAccount(session);
            List<CmsMainProduct> productList = partnerProductService.getCurrentPartnerCMSProductByCategory(categoryNodeName, account.getPartner().getId());
            return new ResponseEntity<>(new ApiResult<>(true, "", "", productList), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiGetProductsByCategory] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, CmsMainProduct.class, ""), HttpStatus.OK);
        }

    }


//    @RequestMapping(value = "get-products-by-category", method = {RequestMethod.POST})
//    public ResponseEntity<ApiResult<List<Map<String, Object>>>> apiGetProductsByCategory(@RequestBody String categoryNodeName) {
//        log.info("Entered apiGetProductsByCategory...");
//        try {
//
//            PartnerAccount account = getLoginUserAccount(session);
//
//            List<Node> productNodes = productService.getCurrentPartnerCMSProductByCategory(categoryNodeName, account.getPartner().getId());
//            List<String> productNodeName = new ArrayList<>();
//
//            for(Node productNode: productNodes) {
//                Map<String, Object> productNodePropertyMap = new HashMap<>();
//               PropertyIterator prodIterator = productNode.getProperties();
//               while(prodIterator.hasNext()) {
//                   Property prodProp = prodIterator.nextProperty();
//                   productNodePropertyMap.put(prodProp.getName(), prodProp.getString());
//               }
//
//                productNodeName.add(productNode.getName());
//            }
//
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", productNodeName), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [ apiGetProductsByCategory] !!!");
//            return new ResponseEntity<>(handleUncaughtExceptionForListOfMap(e,String.class, Object.class, ""), HttpStatus.OK);
//        }
//    }

//    @RequestMapping(value = "generate-ticket/{channel}", method = {RequestMethod.GET, RequestMethod.POST})
//    @ResponseBody
//    public ResponseEntity<ApiResult<String>> apiGenerateTicket(@PathVariable("channel") String channelCode,
//                                                               final HttpServletResponse response) {
//        log.info("Entered apiGenerateTicket...");
//        try {
//
//            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
//            if (!loggedInResult.isSuccess()) {
//                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
//            }
//
//            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
//
//            final StoreTransaction storeTransaction = bookingService.getStoreTransactionBySession(channel, session);
//
//            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
//            ticketDataCompiled.setTickets(storeTransaction.geteTickets());
//
//            final byte[] outputBytes = ticketGenerationService.generateTickets(ticketDataCompiled);
//
//            final String fileName = "etickets.pdf";
//            response.setContentType("application/pdf");
//            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
//            response.setHeader("X-Frame-Options", "SAMEORIGIN");
//
//            ServletOutputStream output = response.getOutputStream();
//            output.write(outputBytes);
//            output.flush();
//
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiGenerateTicket] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
//        }
//    }

}
