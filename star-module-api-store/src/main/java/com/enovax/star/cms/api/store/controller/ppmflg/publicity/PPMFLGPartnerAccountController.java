package com.enovax.star.cms.api.store.controller.ppmflg.publicity;


import com.enovax.star.cms.api.store.controller.ppmflg.PPMFLGBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.commons.constant.ppmflg.MessageConst;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.security.auth.login.CredentialExpiredException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(PartnerPortalConst.ROOT + "/publicity/partner-account/")
public class PPMFLGPartnerAccountController extends PPMFLGBaseStoreApiController {

    private Logger log = LoggerFactory.getLogger(PPMFLGPartnerAccountController.class);

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService accountService;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @RequestMapping(value = "login", method = {RequestMethod.POST, RequestMethod.GET})
    public String apiPartnerLogin() {
        return "please-enable-the-security-filter-config-server-filters";
    }

    @RequestMapping(value = "logout", method = {RequestMethod.POST, RequestMethod.GET})
    public String apiPartnerLogout(){
        return "please-enable-the-security-filter-config-server-filters";
    }

    @RequestMapping(value = "login-failure", method = {RequestMethod.POST, RequestMethod.GET})
    public String apiPartnerLoginFailure(HttpServletRequest request, HttpServletResponse response) {
        String errorMsg = "";
        if(request.getSession(false) != null){
            Object obj = request.getSession(false).getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
            if((obj != null) && (obj instanceof Exception)){
                if(obj instanceof BadCredentialsException){
                    errorMsg = MessageConst.login_err_LoginFailed;
                }else if(obj instanceof CredentialExpiredException){
                    errorMsg = MessageConst.login_err_ExceedPasswordAttempts;
                }else if(obj instanceof LockedException){
                    errorMsg = MessageConst.login_err_AccountLocked;
                }else if(obj instanceof DisabledException){
                    errorMsg = MessageConst.login_err_AccountInactive;
                }else if(obj instanceof AccountExpiredException){
                    errorMsg = MessageConst.login_err_LoginSuspended;
                }else{
                    errorMsg = MessageConst.login_err_LoginFailed;
                }
            }else{
                errorMsg = MessageConst.login_err_LoginFailed;
            }
            request.setAttribute(PartnerPortalConst.SESSION_LOGIN_FAILED_ERROR_KEY, errorMsg);
            String v = sysParamSrv.getSystemParamValueByKey("sys.param.partner.owner.sentosa.partner.account.support");
            if(v != null && v.trim().length() > 0){
                v = v.trim();
            }else{
                v = "";
            }
            request.setAttribute(PartnerPortalConst.SESSION_LOGIN_SUSPENDED_CUSTOMER_SUPPORT_MSG, v);
        }
        return getLoginFailuredPageView(request, true);
    }

    private String getLoginFailuredPageView(HttpServletRequest request, boolean invalidateSession) {
        if(request != null){
            if(invalidateSession) {
                invalidateSession(request.getSession(false));
            }
        }
        return "/partner-portal-mflg";
    }

    @RequestMapping(value = "logout-success", method = {RequestMethod.POST, RequestMethod.GET})
    public String apiPartnerLogoutSuccess(HttpServletRequest request) {
        return "/partner-portal-mflg";
    }

    @RequestMapping(value = "is-logged-in", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPartnerIsLoggedIn(HttpServletRequest request) {
        try {
            boolean loggedIn = getLoginUserAccount(request.getSession(false)) != null;
            return new ResponseEntity<>(new ApiResult<>(loggedIn,"", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerIsLoggedIn] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value ="reset-password", method = RequestMethod.POST)
    public ResponseEntity<String> apiPartnerForgotPwd(HttpServletRequest request, HttpServletResponse response, @RequestParam(name="un")String un, @RequestParam(name="email")String email){
        ApiResult<String> apiRet = new ApiResult<String>();
        try {
            if(sysParamSrv.isRecaptchaEnabled()){
                ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
                reCaptcha.setPrivateKey(sysParamSrv.getRecaptchaPrivateKey());
                ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(sysParamSrv.getUserRequestAddress(request), request.getParameter("recaptcha_challenge_field"), request.getParameter("recaptcha_response_field"));
                if(!reCaptchaResponse.isValid()) {
                    apiRet.setSuccess(false);
                    apiRet.setErrorCode("ERROR");
                    apiRet.setMessage("INVALID_VERIFICATION_CODE");
                    return new ResponseEntity<>(JsonUtil.jsonify(apiRet), HttpStatus.OK);
                }
            }
            String retCode = accountService.resetPartnerPassword(un, email);
            if(retCode == null){
                apiRet.setSuccess(true);
                apiRet.setMessage("RESET_USERNAME_AND_PWD_SUCCESS");
            }else{
                apiRet.setSuccess(false);
                apiRet.setMessage(retCode);
            }
            return new ResponseEntity<>(JsonUtil.jsonify(apiRet), HttpStatus.OK);
        } catch (Exception e) {
            apiRet = new ApiResult<String>();
            apiRet.setSuccess(false);
            apiRet.setMessage(e.getMessage());
            return new ResponseEntity<>(JsonUtil.jsonify(apiRet), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-login-username", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetPartnerLoginUser(HttpServletRequest request) {
        try {
            PartnerAccount account = null;
            if(request != null ){
                account = getLoginUserAccount(request.getSession(false));
            }
            return new ResponseEntity<>(new ApiResult<>(account != null,"", "", account != null && account.getUsername() != null && account.getUsername().trim().length() > 0 ? account.getUsername() : ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerIsLoggedIn] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}
