package com.enovax.star.cms.api.store.controller.ppmflg.secured;


import com.enovax.star.cms.api.store.controller.ppmflg.PPMFLGBaseStoreApiController;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.IReportService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lavanya on 14/11/16.
 */
@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/report/")
public class PPMFLGPartnerReportController extends PPMFLGBaseStoreApiController{
    @Autowired
    @Qualifier("PPMFLGIReportService")
    IReportService reportService;
    @Autowired
    private HttpSession session;
    boolean asc;
    public static final String SESS_FILTER_INV = "sess-filter-inv";
    public static final String SESS_FILTER_PIN = "sess-filter-pin";
    public static final String SESS_FILTER_TR = "sess-filter-tr";
    protected Map<String, Object> sessionMap = new HashMap<>();
    @RequestMapping(value = "transaction-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitTransactionRptPage() {
        try {

            TransactionReportInitVM transactionReportInitVM = reportService.initTransPage(false,getLoginUserAccount(session).getMainAccountId());
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTransactionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiTransactionRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                       @RequestParam("take") int take,
                                                                       @RequestParam("skip") int skip,
                                                                       @RequestParam("page") int page,
                                                                       @RequestParam("pageSize") int pageSize,
                                                                       @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                       @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(false);
            filter.setPartnerId(getLoginUserAccount(session).getMainAccountId());
            sessionMap.put(SESS_FILTER_TR,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "txnDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<TransReportModel> reportResult = reportService.generateTransReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generateTransReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfileRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "transaction-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiTransactionRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)sessionMap.get(SESS_FILTER_TR);
            Workbook wb = reportService.generateTransReportExcel(filter,"txnDate",asc);
            final String fileName = "transaction-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfielRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "inventory-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitInventoryRptPage() {
        try {
            TransactionReportInitVM transactionReportInitVM = reportService.initInventoryPage(false,getLoginUserAccount(session).getMainAccountId());
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTransactionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "inventory-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiInventoryRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                     @RequestParam("take") int take,
                                                                     @RequestParam("skip") int skip,
                                                                     @RequestParam("page") int page,
                                                                     @RequestParam("pageSize") int pageSize,
                                                                     @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                     @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(false);
            filter.setPartnerId(getLoginUserAccount(session).getMainAccountId());
            sessionMap.put(SESS_FILTER_INV,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "txnDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<InventoryReportModel> reportResult = reportService.generateInventoryReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generateInventoryReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfileRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "inventory-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiInventoryRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)sessionMap.get(SESS_FILTER_INV);
            Workbook wb = reportService.generateInventoryReportExcel(filter,"txnDate",asc);
            final String fileName = "inventory-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfielRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "package-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitPackageRptPage() {
        try {
            TransactionReportInitVM transactionReportInitVM = reportService.initPackagePage(false,getLoginUserAccount(session).getMainAccountId());
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTransactionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "package-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiPackageRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                   @RequestParam("take") int take,
                                                                   @RequestParam("skip") int skip,
                                                                   @RequestParam("page") int page,
                                                                   @RequestParam("pageSize") int pageSize,
                                                                   @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                   @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(false);
            filter.setPartnerId(getLoginUserAccount(session).getMainAccountId());
            sessionMap.put(SESS_FILTER_PIN,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "ticketGeneratedDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if("pinGeneratedDateText".equalsIgnoreCase(orderBy)) {
                    orderBy = "ticketGeneratedDate";
                }
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<PackageReportModel> reportResult = reportService.generatePackageReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generatePackageReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfileRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "package-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPackageRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)sessionMap.get(SESS_FILTER_PIN);
            Workbook wb = reportService.generatePackageReportExcel(filter,"ticketGeneratedDate",asc);
            final String fileName = "package-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfielRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}
