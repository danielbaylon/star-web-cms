package com.enovax.star.cms.api.store.service.batch.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.api.store.service.ppslm.ILogService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTelemoneyLog;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerPurchaseTransactionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 1/3/17.
 */
@Service
public class PurchaseTransQueryService {

    public static final String FAILED_SUBJ = "B2B System Alert: Telemoney Query Task Failed Inventory Transaction Queries";

    @Autowired
    private IPartnerPurchaseTransactionService purchaseTransService;
    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private ILogService logService;
    @Autowired
    private IMailService mailService;
    @Autowired
    private ITmParamRepository tmParamRepository;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppslm.purchase.trans.query.job}")
    public void checkStatus(){
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    log.info("=== PPMSLMMixAndMatchPackages Pin Status ===");
                    executeQueryTask();
                }
            });
        }
    }

    public void executeQueryTask() throws RuntimeException {
        log.info("=== Starting periodic [Inventory Trans] Telemoney Query Task...");

        final TelemoneyParamPackage tmParams;
        Integer lowerBoundMillis = 150;
        Integer upperBoundMillis = 20;
        try {
            tmParams = tmParamRepository.getTmParams(StoreApiChannels.PARTNER_PORTAL_SLM);
            lowerBoundMillis = tmParams.getTransTimeoutLowerBoundMillis();
            upperBoundMillis = tmParams.getTransTimeoutUpperBoundMillis();
        } catch (JcrRepositoryException e) {
            e.printStackTrace();
        }

        final List<PPSLMInventoryTransaction> transactions = this.purchaseTransService.getTransactionsForStatusUpdate(lowerBoundMillis, upperBoundMillis);
        final List<PPSLMInventoryTransaction> failedQueries = new ArrayList<>();

        for (PPSLMInventoryTransaction trans : transactions) {
            final String receiptNum = trans.getReceiptNum();
            final String status = trans.getStatus();
            final String tmStatus = trans.getTmStatus();

            if (TicketStatus.Reserved.toString().equals(status) && StringUtils.isEmpty(tmStatus)) {
                log.info("Cleaning up incomplete transaction " + receiptNum);
                trans.setStatus(TicketStatus.Incomplete.toString());
                trans.setModifiedDate(new Date());
                this.purchaseTransService.saveTransaction(trans);

                log.info("Releasing ticket.");
                try {

                    purchaseTransService.processTransactionReleaseReservation(trans.getId(), true);
                } catch (Exception e) {
                    log.error("Unable to releast ticket due to an exception.", e);
                    //TODO should email?
                }
            } else {
                log.info("Setting transaction stage to TM Query for " + receiptNum);
                trans.setTmStatus(EnovaxTmSystemStatus.TmQuerySent.toString());
                trans.setModifiedDate(new Date());
                this.purchaseTransService.saveTransaction(trans);

                log.info("Sending status update request to Telemoney for " + receiptNum);

                final TmServiceResult result = tmServiceProvider.sendQueryWithRetry(trans.getTmMerchantId(), receiptNum, null);

                if (result.isSuccess()) {
                    log.info("Successfully sent query to Telemoney.");
                } else {
                    log.info("Unable to send query to Telemoney. Performing cleanup tasks.");

                    log.info("Sending void to Telemoney.");
                    final TmServiceResult voidResult = tmServiceProvider.sendVoidWithRetry(
                            trans.getTmMerchantId(), TmCurrency.fromCode(trans.getCurrency()), trans.getTotalAmount(), receiptNum, null);

                    if (!voidResult.isSuccess() || voidResult.getTmResponse() == null) {
                        log.info("Unable to send void to Telemoney.");
                    } else {
                        log.info("Send Void successful. Logging the result.");
                        final PPSLMTelemoneyLog telemoneyLog = logService.createTelemoneyObject(true, voidResult.getTmResponse());
                        this.logService.logTelemoneyDetailsToDB(telemoneyLog);
                    }

                    log.info("Releasing ticket.");
                    boolean released = false;
                    try {

                        if (EnovaxTmSystemStatus.TmQuerySent.toString().equals(trans.getTmStatus()) ||
                                TicketStatus.Reserved.toString().equals(trans.getStatus())) {
                            log.info("This transaction has already been released. No need to release this time.");
                            released = true;
                        } else {
                            purchaseTransService.processTransactionReleaseReservation(trans.getId(), true);
                            released = true;
                        }
                    } catch (Exception e) {
                        log.error("Unable to releast ticket due to an exception.", e);
                    }

                    if (released) {
                        log.info("Successfully released ticket.");
                    } else {
                        log.info("Unsuccessfully released ticket.");
                    }

                    if (!released || voidResult == null) {
                        failedQueries.add(trans);
                    }
                }
            }
        }

        if (!failedQueries.isEmpty()) {
            try {

                final MailProperties mailProps = new MailProperties();

                //TODO send to the admin
                mailProps.setToUsers(new String[]{"jennylyn@enovax.com"}); //TODO TODO change this
                mailProps.setSender(mailService.getDefaultSender());
                mailProps.setSubject(FAILED_SUBJ);
                mailProps.setBody(constructFailedEmail(failedQueries));

                mailService.sendEmail(mailProps, new MailCallback() {
                    @Override
                    public void complete(String trackingId, boolean success) {
                        log.info("Mail status update for {}. Status: {}", trackingId, success ? "SUCCESS" : "FAILED");
                    }
                });
            } catch (Exception e) {
                log.error("Email sending failed for TM Query Task.", e);
            }
        }
    }

    private String constructFailedEmail(List<PPSLMInventoryTransaction> failedQueries) {
        StringBuilder sb = new StringBuilder();

        sb.append("Dear Admin,\n\n")
                .append("B2B [Inventory Transaction] Telemoney Query Task was unable to resolve the following transactions " +
                        "(unable to query, and unable to either send a void or release the transaction): \n\n\n");

        for (PPSLMInventoryTransaction trans : failedQueries) {
            sb.append("Transaction Receipt No.: " + trans.getReceiptNum() + "\n")
                    .append("Status: " + trans.getStatus() + "\n")
                    .append("TM Status: " + trans.getTmStatus() + "\n")
                    .append("- end Transaction -\n");
        }

        sb.append("Yours Sincerely,\n")
                .append("B2B System Support Team\n\n")
                .append("*This is a system generated message. Please do not reply to this email.\n");

        return sb.toString();
    }
}
