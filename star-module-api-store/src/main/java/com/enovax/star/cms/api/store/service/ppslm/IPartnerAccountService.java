package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.api.store.model.partner.ppslm.PartnerUser;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxProductPriceMap;
import com.enovax.star.cms.commons.model.partner.ppslm.PPSLMTAAccessGroupVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface IPartnerAccountService {

    void logout(String id, String sessionId);

    PPSLMTAMainAccount createTAMainAccount(PartnerVM pVM);

    PPSLMTAMainAccount updateTAMainAccount(PPSLMPartner partner, PartnerVM paVM);

    String resetPartnerPassword(String un, String email) throws Exception;

    public PPSLMPartner resetFailAttempts(String username);

    void updateFailAttempts(String name, String errorCode);

    PartnerUser getUserDetailsByUserName(String username) throws UsernameNotFoundException;

    //String getLoginFailedErrorMessageByLoginName(String name);

    void renewPassword(String username, String pw, String newPw, String newPw2) throws Exception;

    PartnerAccount getPartnerAccountByLoginId(Integer adminId) throws Exception;

    Integer findMainAccountIdByAccountNumber(String axAccountNumber) throws Exception;

    AxProductPriceMap getCustomerItemPrice(StoreApiChannels channel, String axAccountNumber);

    public void syncPartnerProfileWithAX(Integer paId);

    void cachePartnerConfigs(PPSLMPartner profile);

    boolean isPartnerOfflinePaymentEnabled(PartnerAccount loginUserAccount);

    PPSLMTASubAccount getSubAccountByUserName(String username);

    List<PPSLMTAMainAccount> findMainAccountListByUserName(String trim);

    List<PPSLMTASubAccount> getSubAccountListByUserName(String trim);

    List<PartnerAccountVM> getPartnerSubaccountListByMainAccountId(Integer id);

    PartnerAccountVM getPartnerSubaccountDetailsByUserId(Integer userId);

    List<PPSLMTAAccessGroupVM> getPartnerSubaccountAccessGroupList();

    public ApiResult<String> saveTASubAccount(PPSLMPartner partner, Boolean isNew, Integer userId, String username, String name, String email, String title, String status, String access, String loginUserId, Integer loginId, boolean isSubAccount) throws BizValidationException;

    ApiResult<String> savePartnerSubAccountRightsChanges(PPSLMPartner partner, Integer mainAccId, String loginUserName, Integer loginId, boolean isSubAccount, List<PartnerAccountVM> accounts) throws BizValidationException;

    boolean isCurrentUserAccountInvalid(Integer id, boolean subAccount, Integer paId);

    List<PartnerVM> getActivePartners();
}
