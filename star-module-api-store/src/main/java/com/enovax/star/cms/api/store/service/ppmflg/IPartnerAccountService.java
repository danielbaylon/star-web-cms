package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.api.store.model.partner.ppmflg.PartnerUser;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxProductPriceMap;
import com.enovax.star.cms.commons.model.partner.ppmflg.PPMFLGTAAccessGroupVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;

import java.util.List;

public interface IPartnerAccountService {

    void logout(String id, String sessionId);

    PPMFLGTAMainAccount createTAMainAccount(PartnerVM pVM);

    PPMFLGTAMainAccount updateTAMainAccount(PPMFLGPartner partner, PartnerVM paVM);

    String resetPartnerPassword(String un, String email) throws Exception;

    public PPMFLGPartner resetFailAttempts(String username);

    void updateFailAttempts(String name, String errorCode);

    PartnerUser getUserDetailsByUserName(String username);

    void renewPassword(String username, String pw, String newPw, String newPw2) throws Exception;

    PartnerAccount getPartnerAccountByLoginId(Integer adminId) throws Exception;

    Integer findMainAccountIdByAccountNumber(String axAccountNumber) throws Exception;

    AxProductPriceMap getCustomerItemPrice(StoreApiChannels channel, String axAccountNumber);

    public void syncPartnerProfileWithAX(Integer paId);

    void cachePartnerConfigs(PPMFLGPartner profile);

    boolean isPartnerOfflinePaymentEnabled(PartnerAccount loginUserAccount);

    PPMFLGTASubAccount getSubAccountByUserName(String username);

    List<PPMFLGTAMainAccount> findMainAccountListByUserName(String trim);

    List<PPMFLGTASubAccount> getSubAccountListByUserName(String trim);

    List<PartnerAccountVM> getPartnerSubaccountListByMainAccountId(Integer id);

    PartnerAccountVM getPartnerSubaccountDetailsByUserId(Integer userId);

    List<PPMFLGTAAccessGroupVM> getPartnerSubaccountAccessGroupList();

    public ApiResult<String> saveTASubAccount(PPMFLGPartner partner, Boolean isNew, Integer userId, String username, String name, String email, String title, String status, String access, String loginUserId, Integer loginId, boolean isSubAccount) throws BizValidationException;

    ApiResult<String> savePartnerSubAccountRightsChanges(PPMFLGPartner partner, Integer mainAccId, String loginUserName, Integer loginId, boolean isSubAccount, List<PartnerAccountVM> accounts) throws BizValidationException;

    boolean isCurrentUserAccountInvalid(Integer id, boolean subAccount, Integer paId);

    List<PartnerVM> getActivePartners();
}
