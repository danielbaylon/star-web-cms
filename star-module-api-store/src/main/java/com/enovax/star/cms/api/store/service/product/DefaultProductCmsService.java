package com.enovax.star.cms.api.store.service.product;

import com.enovax.star.cms.api.store.service.axretail.AxRetailServiceException;
import com.enovax.star.cms.commons.constant.ppslm.TicketType;
import com.enovax.star.cms.commons.jcrrepository.merchant.IMerchantRepository;
import com.enovax.star.cms.commons.mgnl.definition.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeRequest;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeResult;
import com.enovax.star.cms.commons.model.booking.promotions.TicketUpdateModel;
import com.enovax.star.cms.commons.model.booking.promotions.UnlockedTicketModel;
import com.enovax.star.cms.commons.model.merchant.Merchant;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Service
public class DefaultProductCmsService implements IProductCmsService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private StarTemplatingFunctions starfn;
    @Autowired
    private DamTemplatingFunctions damfn;

    @Autowired
    private IMerchantRepository merchantRepo;

    @Override
    public ApiResult<List<CrossSellCartItem>> getAllCrossSellCartItemsFor(FunCartDisplay kioskTypeCart, String channelCode, String productNodeName, String locale) throws AxRetailServiceException {

        log.info("Calling DefaultProductCmsService.getAllCrossSellCartItemsFor() .......");

        List<CrossSellCartItem> crossSellCartItems = new ArrayList<>();

        List<FunCartDisplayCmsProduct> cmsProducts = kioskTypeCart.getCmsProducts();

        for (FunCartDisplayCmsProduct cmsProduct : cmsProducts) {

            if (cmsProduct.getId().equals(productNodeName)) {

                List<FunCartDisplayItem> cmsProductItems = cmsProduct.getItems();

                Map<String, FunCartDisplayItem> axProductItems = new HashMap<>();
                Map<String, FunCartDisplayItem> topupProductItems = new HashMap<>();

                for (FunCartDisplayItem cmsProductItem : cmsProductItems) {

                    if (cmsProductItem.isTopup()) {
                        topupProductItems.put(cmsProductItem.getParentListingId(), cmsProductItem);
                    }
                }

                for (FunCartDisplayItem cmsProductItem : cmsProductItems) {

                    if (cmsProductItem.isTopup()) {
                        continue;
                    }

                    axProductItems.put(cmsProductItem.getListingId(), cmsProductItem);
                }

                for (FunCartDisplayItem cmsProductItem : axProductItems.values()) {

                    List<Node> crossSellItemNodes = starfn.getAXProductCrossSellsByListingId(channelCode, cmsProductItem.getListingId());

                    for (Node itemNode : crossSellItemNodes) {

                        CrossSellCartItem crossSellCartItem = new CrossSellCartItem();

                        crossSellCartItem.setCrossSellProductListingId(starfn.getPropertyValue(itemNode, "crossSellProductListingId", locale));
                        crossSellCartItem.setCrossSellItemImage(starfn.getPropertyValue(itemNode, "crossSellItemImage", locale));
                        crossSellCartItem.setCrossSellItemName(starfn.getPropertyValue(itemNode, "crossSellItemName", locale));
                        crossSellCartItem.setCrossSellItemDesc(starfn.getPropertyValue(itemNode, "crossSellItemDesc", locale));
                        crossSellCartItem.setCrossSellProductCode(starfn.getPropertyValue(itemNode, "crossSellProductCode", locale));

                        Node productNode = starfn.getAxProductByProductCode(channelCode, starfn.getPropertyValue(itemNode, "crossSellProductCode", locale));

                        crossSellCartItem.setProductPrice(starfn.getPropertyValue(productNode, "productPrice", locale));
                        crossSellCartItem.setTicketType(starfn.getPropertyValue(productNode, "ticketType", locale));

                        String imageIconPath = damfn.getAssetLink(starfn.getPropertyValue(itemNode, "crossSellItemImage", locale));

                        crossSellCartItem.setImageIconPath(imageIconPath);

                        FunCartDisplayItem funCartDisplayItem = topupProductItems.get(crossSellCartItem.getCrossSellProductListingId());

                        if (funCartDisplayItem != null) {
                            crossSellCartItem.setQuantity(funCartDisplayItem.getQty());
                        }

                        crossSellCartItem.setParentListingId(cmsProductItem.getListingId());
                        crossSellCartItem.setParentCmsProductId(cmsProduct.getId());

                        crossSellCartItems.add(crossSellCartItem);
                    }
                }
            }
        }

        return new ApiResult<>(true, "", "", crossSellCartItems);
    }

    @Override
    public ApiResult<PromoCodeResult> checkPromoCode(PromoCodeRequest promoCodeRequest, String channelCode) {
        ApiResult<PromoCodeResult> apiResult = new ApiResult<>();
        PromoCodeResult result = new PromoCodeResult();
        result.setPromoCode(promoCodeRequest.getPromoCode());

        try {
            Node cmsProduct = starfn.getCMSProduct(channelCode, promoCodeRequest.getProductId());
            if (cmsProduct != null) {
                Map<String, Node> axProducts = starfn.getAXProductsDetailByCMSProductNode(cmsProduct, true);
                if (axProducts != null) {
                    for (Node axProduct : axProducts.values()) {
                        List<Node> promotions = starfn.getAXProductPromotions(axProduct, true);
                        if (promotions != null) {
                            for (Node promotion : promotions) {
                                if (AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName())) &&
                                    promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                                    NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                                    while (discountCodes.hasNext()) {
                                        final Node discountCode = discountCodes.nextNode();
                                        if (AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))
                                            && promoCodeRequest.getPromoCode().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.Code.getPropertyName()))) {
                                            if (PropertyUtil.getBoolean(promotion, AXProductPromotionProperties.UsedAsUnlocker.getPropertyName(), false)) {
                                                result.getUnlockedTickets().add(convertToUnlockedTicket(axProduct, promotion));
                                            } else {
                                                // result.getUnlockedTickets().add(convertToUnlockedTicket(axProduct, promotion));
                                                // result.getUpdatedTickets().add(convertToTicketUpdate(axProduct, promotion));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Error checking discount code.", e);
        }


        if (result.getUpdatedTickets().isEmpty() && result.getUnlockedTickets().isEmpty()) {
            //TODO: Standardize error message
            apiResult.setSuccess(false);
            apiResult.setMessage("Promo code is not valid.");
        } else {
            apiResult.setSuccess(true);
        }
        apiResult.setData(result);

        return apiResult;
    }

    private UnlockedTicketModel convertToUnlockedTicket(Node axProduct, Node promotion) {
        UnlockedTicketModel unlockedTicket = new UnlockedTicketModel();
        unlockedTicket.setDisplayProductNumber(PropertyUtil.getLong(axProduct, AXProductProperties.DisplayProductNumber.getPropertyName()));
        unlockedTicket.setProductDesc(PropertyUtil.getString(axProduct, AXProductProperties.Description.getPropertyName()));
        unlockedTicket.setProductListingId(PropertyUtil.getLong(axProduct, AXProductProperties.ProductListingId.getPropertyName()));
        unlockedTicket.setProductName(PropertyUtil.getString(axProduct, AXProductProperties.ProductName.getPropertyName()));
        unlockedTicket.setProductPriceText(PropertyUtil.getString(axProduct, AXProductProperties.ProductPrice.getPropertyName()));
        try {
            unlockedTicket.setProductPrice(NvxNumberUtils.parseBigDecimal(PropertyUtil.getString(axProduct, AXProductProperties.ProductPrice.getPropertyName()), NvxNumberUtils.DEFAULT_CURRENCY_FORMAT));
        } catch (ParseException e) {
            unlockedTicket.setProductPrice(BigDecimal.ZERO);
            log.error("Unable to parse price", e);
        }

        String promotionIconUUID = PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionIcon.getPropertyName());
        String promotionIconImageURL = damfn.getAssetLink(promotionIconUUID);
        unlockedTicket.setPromotionDisplayName(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName()));
        unlockedTicket.setPromotionIconImageURL(promotionIconImageURL);
        unlockedTicket.setPromotionText(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName()));
        unlockedTicket.setAdditionalInfo(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionAdditionalInfo.getPropertyName()));
        BigDecimal cachedDiscountPrice = new BigDecimal(PropertyUtil.getString(promotion, AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), ""));
        unlockedTicket.setCachedDiscountPrice(NvxNumberUtils.formatToCurrency(cachedDiscountPrice));
        TicketType ticketType = TicketType.fromType(unlockedTicket.getProductType());
        if (ticketType != null) {
            unlockedTicket.setTicketTypeText(ticketType.display);
        }


        return unlockedTicket;
    }

    private TicketUpdateModel convertToTicketUpdate(Node axProduct, Node promotion) {
        TicketUpdateModel ticketUpdate = new TicketUpdateModel();

        ticketUpdate.setProductListingId(PropertyUtil.getLong(axProduct, AXProductProperties.ProductListingId.getPropertyName()));

        String promotionIconUUID = PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionIcon.getPropertyName());
        String promotionIconImageURL = damfn.getAssetLink(promotionIconUUID);
        ticketUpdate.setPromotionDisplayName(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName()));
        ticketUpdate.setPromotionIconImageURL(promotionIconImageURL);
        ticketUpdate.setPromotionText(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName()));
        ticketUpdate.setAdditionalInfo(PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionAdditionalInfo.getPropertyName()));
        BigDecimal cachedDiscountPrice = new BigDecimal(PropertyUtil.getString(promotion, AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), ""));
        ticketUpdate.setCachedDiscountPrice(NvxNumberUtils.formatToCurrency(cachedDiscountPrice));

        return ticketUpdate;
    }

    @Override
    public ApiResult<PromoCodeResult> searchPromoCode(PromoCodeRequest promoCodeRequest, String channelCode) {
        ApiResult<PromoCodeResult> apiResult = new ApiResult<>();

        if (BooleanUtils.isTrue(promoCodeRequest.getFromHeader())) {
            try {
                List<Node> categories = starfn.getCMSProductCategories(channelCode);
                for (Node category : categories) {
                    List<Node> cmsProducts = starfn.getCMSProductByCategory(channelCode, category.getName());
                    if (cmsProducts != null) {
                        for (Node cmsProduct : cmsProducts) {
                            Map<String, Node> axProducts = starfn.getAXProductsDetailByCMSProductNode(cmsProduct, true);
                            if (axProducts != null) {
                                for (Node axProduct : axProducts.values()) {
                                    List<Node> promotions = starfn.getAXProductPromotions(axProduct, true);
                                    if (promotions != null) {
                                        for (Node promotion : promotions) {
                                            if (AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName())) &&
                                                PropertyUtil.getBoolean(promotion, AXProductPromotionProperties.UsedAsUnlocker.getPropertyName(), false) &&
                                                promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                                                NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                                                while (discountCodes.hasNext()) {
                                                    final Node discountCode = discountCodes.nextNode();
                                                    if (AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))
                                                        && promoCodeRequest.getPromoCode().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.Code.getPropertyName()))) {
                                                        final StringBuilder sb = new StringBuilder();

                                                        if (channelCode.equals("b2c-mflg")) {
                                                            sb.append("/category/product~");
                                                            sb.append(PropertyUtil.getString(cmsProduct, "generatedURLPath", ""));
                                                            sb.append("~");
                                                            sb.append(promoCodeRequest.getLanguage());
                                                            sb.append("~");
                                                            sb.append(category.getName());
                                                            sb.append("~");
                                                            sb.append(cmsProduct.getName());
                                                            sb.append("~");
                                                            sb.append(promoCodeRequest.getPromoCode());
                                                            sb.append("~.html?category=");
                                                            sb.append(PropertyUtil.getString(category, "generatedURLPath", ""));
                                                        }

                                                        PromoCodeResult result = new PromoCodeResult();
                                                        result.setPromoCode(promoCodeRequest.getPromoCode());
                                                        result.setRedirect(true);
                                                        result.setRedirectUrl(sb.toString());
                                                        apiResult.setSuccess(true);
                                                        apiResult.setData(result);
                                                        return apiResult;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                apiResult.setSuccess(false);
                apiResult.setMessage("No active promotions found.");
            } catch (Exception e) {
                apiResult.setSuccess(false);
                apiResult.setMessage("Error searching for promo code. Please try again later.");
                log.error("Error search for promo code.", e);
            }
        } else {
            // Implement if there is need for promo code searching in other areas.
            apiResult.setSuccess(false);
            apiResult.setMessage("Unsupported promo code search request.");
        }
        return apiResult;
    }

    /**
     * TODO This assumes that the promo code is unique... or it will just get the first offer lel.
     *
     * @param channelCode
     * @param promoCode
     * @return
     */
    @Override
    public ApiResult<String> getOfferIdForPromoCode(String channelCode, String promoCode) {
        return getOfferIdForPromoCode(channelCode, promoCode, false);
    }
    @Override
    public ApiResult<String> getOfferIdForPromoCode(String channelCode, String promoCode, boolean includeInactive) {
        try {

            final List<Node> promotions = starfn.getAllPromotions(channelCode);
            if (promotions != null) {
                for (Node promotion : promotions) {
                    if (includeInactive || AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName()))) {
                        if (promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                            NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                            while (discountCodes.hasNext()) {
                                final Node discountCode = discountCodes.nextNode();
                                if (includeInactive || AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))) {
                                    if (promoCode.equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.Code.getPropertyName()))) {
                                        final String offerId = PropertyUtil.getString(promotion, AXProductPromotionProperties.OfferId.getPropertyName());
                                        return new ApiResult<>(true, "", "", offerId);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return new ApiResult<>(false, "", "No Offer ID found", "");
        } catch (RepositoryException e) {
            log.error("Error retrieving promotion for discount code");
            return new ApiResult<>(false, "", "Error encountered while retrieving Offer ID.", "");
        }
    }

    @Override
    public ApiResult<String> getOfferIdForPromoCode(String channelCode, String cmsProductId, String promoCode) {
        try {

            final Node cmsProduct = starfn.getCMSProduct(channelCode, cmsProductId);
            if (cmsProduct != null) {
                final Map<String, Node> axProducts = starfn.getAXProductsDetailByCMSProductNode(cmsProduct, true);
                if (axProducts != null) {
                    for (Node axProduct : axProducts.values()) {
                        List<Node> promotions = starfn.getAXProductPromotions(axProduct, true);
                        if (promotions != null) {
                            for (Node promotion : promotions) {
                                if (AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName())) &&
                                    promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                                    NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                                    while (discountCodes.hasNext()) {
                                        final Node discountCode = discountCodes.nextNode();
                                        if (AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))
                                            && promoCode.equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.Code.getPropertyName()))) {
                                            final String offerId = PropertyUtil.getString(promotion, AXProductPromotionProperties.OfferId.getPropertyName());
                                            return new ApiResult<>(true, "", "", offerId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return new ApiResult<>(false, "", "No Offer ID found", "");
        } catch (RepositoryException e) {
            log.error("Error retrieving promotion for discount code");
            return new ApiResult<>(false, "", "Error encountered while retrieving Offer ID.", "");
        }
    }

    @Override
    public List<Merchant> getPromotionMerchants(String channel, CheckoutDisplay cart) {
        final Set<Merchant> promoMerchants = new HashSet<>();
        try {
            if (cart.getCmsProducts() != null) {
                for (FunCartDisplayCmsProduct p : cart.getCmsProducts()) {
                    if (p.getItems() != null) {
                        for (FunCartDisplayItem i : p.getItems()) {
                            final Node axProduct = starfn.getAxProductByProductCode(channel, i.getProductCode());
                            if (axProduct != null) {
                                final List<Node> promotions = starfn.getAXProductPromotions(axProduct);
                                if (promotions != null) {
                                    for (Node promotion : promotions) {
                                        if (promotion.hasNode(AXProductPromotionProperties.Affiliation.getPropertyName())) {
                                            final NodeIterator affiliations = promotion.getNode(AXProductPromotionProperties.Affiliation.getPropertyName()).getNodes();
                                            if (affiliations.hasNext()) {
                                                final Node linkNode = affiliations.nextNode();
                                                final String affUuid = PropertyUtil.getString(linkNode, AXProductPromotionProperties.RelatedAffUUID.getPropertyName(), "");
                                                final Node affNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Affiliations.getWorkspaceName(), affUuid);

                                                if (affNode != null) {
                                                    final FunCartAffiliation affiliation = new FunCartAffiliation();
                                                    affiliation.setAffiliationId(Long.valueOf(PropertyUtil.getString(affNode, AXProductAffiliationProperties.RecordId.getPropertyName(), "0")));
                                                    affiliation.setAffiliationName(PropertyUtil.getString(affNode, AXProductAffiliationProperties.SystemName.getPropertyName(), ""));
                                                    affiliation.setAffiliationType(PropertyUtil.getString(affNode, AXProductAffiliationProperties.DiscountType.getPropertyName(), ""));

                                                    final String discountType = PropertyUtil.getString(affNode, AXProductAffiliationProperties.DiscountType.getPropertyName(), "");

                                                    if (AxProductAffiliationDiscountType.PaymentType.name().equals(discountType)) {
                                                        final String promoLabel = PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName(), "");
                                                        if (StringUtils.isNotBlank(promoLabel)) {
                                                            final String offerId = PropertyUtil.getString(promotion, AXProductPromotionProperties.OfferId.getPropertyName(), "");
                                                            if (offerId != null && !i.getOfferIds().contains(offerId)) {
                                                                i.getPromoLabels().add(promoLabel);
                                                            }
                                                        }

                                                        affiliation.setPaymentType(PropertyUtil.getString(affNode, AXProductAffiliationProperties.DiscountTypePaymentType.getPropertyName(), ""));
                                                        affiliation.setCreditCard("");
                                                        cart.getAffiliations().add(affiliation);
                                                        cart.setHasAffiliations(true);
                                                    } else if (AxProductAffiliationDiscountType.CreditCard.name().equals(discountType)) {
                                                        final String merchUuid = PropertyUtil.getString(affNode, AXProductAffiliationProperties.DiscountTypeCreditCard.getPropertyName(), "");
                                                        final Merchant merchant = merchantRepo.findByUuid(merchUuid);
                                                        if (merchant != null) {
                                                            promoMerchants.add(merchant);

                                                            final String promoLabel = PropertyUtil.getString(promotion, AXProductPromotionProperties.PromotionText.getPropertyName(), "");
                                                            if (StringUtils.isNotBlank(promoLabel)) {
                                                                final String offerId = PropertyUtil.getString(promotion, AXProductPromotionProperties.OfferId.getPropertyName(), "");
                                                                if (offerId != null && !i.getOfferIds().contains(offerId)) {
                                                                    i.getPromoLabels().add(promoLabel);
                                                                }
                                                            }

                                                            affiliation.setCreditCard(merchant.getMerchantId());
                                                            affiliation.setPaymentType("");
                                                            cart.getAffiliations().add(affiliation);
                                                            cart.setHasAffiliations(true);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Error retrieving promotion merchants.", e);
        }
        return new ArrayList<>(promoMerchants);
    }
}