package com.enovax.star.cms.api.store.handler.ppmflg;

import com.enovax.star.cms.api.store.service.batch.axsync.IPartnerProfileSyncJob;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component("PPMFLGDaoAuthenticationProvider")
public class LoginAuthenticationProvider extends DaoAuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(LoginAuthenticationProvider.class);

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService paAccSrv;

    @Autowired
    private IPartnerProfileSyncJob paProfileSyncJob;

    @Autowired
    @Qualifier("PPMFLGUserDetailsService")
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Authentication auth = super.authenticate(authentication);
            if(auth.isAuthenticated()){
                PPMFLGPartner partner = paAccSrv.resetFailAttempts(authentication.getName());
                if(partner != null){
                    paProfileSyncJob.sync(partner.getId());
                    paAccSrv.cachePartnerConfigs(partner);
                }
            }
            return auth;
        } catch (AuthenticationException e) {
            paAccSrv.updateFailAttempts(authentication.getName(), e.getMessage());
            throw e;
        }
    }
}
