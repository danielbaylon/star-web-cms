package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.constant.TmRequestType;
import com.enovax.payment.tm.model.TelemoneyResponse;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by houtao on 14/11/16.
 */
@Service("PPMFLGITelemoneyPPMFLGService")
public class DefaultTelemoneyPPMFLGService implements ITelemoneyPPMFLGService {

    @Autowired
    @Qualifier("PPMFLGDefaultTelemoneyDepositTopupProcessor")
    private ObjectFactory<DefaultTelemoneyDepositTopupProcessor> tmDepositProcessorFactory;

    @Autowired
    @Qualifier("PPMFLGDefaultTelemoneyInventoryPurchaseProcessor")
    private ObjectFactory<DefaultTelemoneyInventoryPurchaseProcessor> tmInventoryPurchaseProcessorFactory;

    @Autowired
    @Qualifier("PPMFLGDefaultTelemoneyRevalidationProcessor")
    private ObjectFactory<DefaultTelemoneyRevalidationProcessor> tmRevalidationProcessorFactory;


    @Override
    public void processMerchantPost(TelemoneyResponse response) {
        if(response != null){
            if(TmRequestType.DepositTopup.name().equalsIgnoreCase(response.getTmUserField2())){
                DefaultTelemoneyDepositTopupProcessor tmProcessor = tmDepositProcessorFactory.getObject();
                tmProcessor.process(response);
            }else if(TmRequestType.InventoryPurchase.name().equalsIgnoreCase(response.getTmUserField2())) {
                DefaultTelemoneyInventoryPurchaseProcessor tmProcessor = tmInventoryPurchaseProcessorFactory.getObject();
                tmProcessor.process(response);
            }else if(TmRequestType.Revalidation.name().equalsIgnoreCase(response.getTmUserField2())) {
                DefaultTelemoneyRevalidationProcessor tmProcessor = tmRevalidationProcessorFactory.getObject();
                tmProcessor.process(response);
            }
        }
    }
}
