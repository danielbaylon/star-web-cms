package com.enovax.star.cms.api.store.service.batch.ppmflgaxsync;

import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerAXService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * Created by houtao on 27/9/16.
 */
public class AxPartnerProfileSyncTask implements Callable {

    private static final Logger log = LoggerFactory.getLogger(AxPartnerProfileSyncTask.class);

    private IPartnerAXService paAxSrv;
    private boolean isDone;
    private Integer partnerId;

    public IPartnerAXService getPaAxSrv() {
        return paAxSrv;
    }

    public void setPaAxSrv(IPartnerAXService paAxSrv) {
        this.paAxSrv = paAxSrv;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        if(partnerId != null && partnerId.intValue() > 0) {
            this.partnerId = partnerId;
            this.isDone = false;
        }
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public Object call() throws Exception {
        try{
            isDone = false;
            syncPartnerProfile(partnerId);
        }catch (Exception ex){
            log.error("sync is failed for "+partnerId+", error "+ex.getMessage(), ex);

        }finally {
            isDone = true;
        }
        return null;
    }

    private void syncPartnerProfile(Integer partnerId) {
        paAxSrv.syncPartnerProfile(partnerId);
    }
}
