package com.enovax.star.cms.api.store.service.ppmflg;


import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.util.Date;

/**
 * Created by jennylynsze on 6/13/16.
 */
public interface IPartnerDepositReportService {
    SXSSFWorkbook exportToExcel(PartnerAccount account, Date startDate, Date endDate, String depositTxnQueryType);

    SXSSFWorkbook exportEmptyExcelWithError(String message);
}
