package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.WOTReservationVM;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 6/21/16.
 */
@Repository
public class JcrWOTReservationRepository implements  IWOTReservationRepository {

    @Override
    public void save(WOTReservationVM reservation) {
        try {

            Node reservationNode;
            if(JcrRepository.nodeExists(JcrWorkspace.WOTReservations.getWorkspaceName(), "/" + reservation.getId())) {
                reservationNode = JcrRepository.getParentNode(JcrWorkspace.WOTReservations.getWorkspaceName(), "/" + reservation.getId());
            }else {
                reservationNode = JcrRepository.createNode(JcrWorkspace.WOTReservations.getWorkspaceName(), "/", reservation.getId() + "", "mgnl:content");
            }

            reservationNode.setProperty("data", JsonUtil.jsonify(reservation));
            Calendar showDateTime = Calendar.getInstance();
            showDateTime.setTime(reservation.getEventDate());
            reservationNode.setProperty("showDateTime", showDateTime);
            reservationNode.setProperty("status", reservation.getStatus());
            reservationNode.setProperty("showDate", reservation.getShowDate());
            reservationNode.setProperty("showTime", reservation.getShowTime());
            reservationNode.setProperty("mainAccountId", reservation.getMainAccountId());
            JcrRepository.updateNode(JcrWorkspace.WOTReservations.getWorkspaceName(), reservationNode);


        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<WOTReservationVM> getReservations(Integer adminId, String fromDateStr, String toDateStr, String displayCancelled, String showTimes, String orderBy, String orderWith, Integer page, Integer pagesize) {
        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "']";
            List<WOTReservationVM> reservationList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.WOTReservations.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                WOTReservationVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(),WOTReservationVM.class);
                reservationList.add(item);
            }

            //only get those with status for main transaction as "Available"
            List<WOTReservationVM> pagedReservationList = new ArrayList<>();
            //filter pag page also
            if(page > 0 && pagesize > 0) {
                for(int i = (page - 1) * pagesize; pagedReservationList.size() < pagesize && i < reservationList.size(); i++) {
                    pagedReservationList.add(reservationList.get(i));
                }
            }else {
                return reservationList;
            }

            return pagedReservationList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int getReservationsSize(Integer adminId, String fromDateStr, String toDateStr, String displayCancelled, String showTimes) {
        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "']";
            List<WOTReservationVM> reservationList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.WOTReservations.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            int cnt = 0;
            while(nodeIterator.hasNext()) {
               nodeIterator.next();
               cnt++;
            }

            return cnt;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public WOTReservationVM getReservationById(int id) {
        Node reservationNode = null;
        try {
            reservationNode = JcrRepository.getParentNode(JcrWorkspace.WOTReservations.getWorkspaceName(), "/" + id);

            return JsonUtil.fromJson(PropertyUtil.getString(reservationNode, "data"), WOTReservationVM.class);

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node reservationSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/wot-reservations");
            int currId = Integer.parseInt(reservationSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            reservationSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), reservationSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }
}
