package com.enovax.star.cms.api.store.controller.ppslm;


import com.enovax.star.cms.api.store.controller.BaseStoreApiController;
import com.enovax.star.cms.api.store.model.partner.ppslm.PartnerUser;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

public abstract class PPSLMBaseStoreApiController extends BaseStoreApiController {

    protected PartnerAccount invalidateSession(HttpSession session){
        try{
            if(session != null){
                Enumeration<String> e = session.getAttributeNames();
                if(e != null){
                    while(e.hasMoreElements()){
                        String k = e.nextElement();
                        if(k != null){
                            session.removeAttribute(k);
                        }
                    }
                }
                session.invalidate();
            }
        }catch (Exception ex){
        }catch (Throwable ex){
        }
        return null;
    }

    protected PartnerAccount getLoginUserAccount(HttpSession session) {
        if(session == null){
            return invalidateSession(session);
        }
        SecurityContext securityContext = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
        if(securityContext == null) {
            return invalidateSession(session);
        }
        Authentication authentication = securityContext.getAuthentication();
        if(authentication == null || (!authentication.isAuthenticated())){
            return invalidateSession(session);
        }
        PartnerUser partnerUser = (PartnerUser)authentication.getPrincipal();
        if(partnerUser == null){
            return invalidateSession(session);
        }
        if(partnerUser.getAuthorities() == null || partnerUser.getAuthorities().size() == 0) {
            return invalidateSession(session);
        }
        boolean hasRequiredAuthority = false;
        Collection<GrantedAuthority> authorities = partnerUser.getAuthorities();
        for(GrantedAuthority authority : authorities){
            if(authority == null){
                continue;
            }
            if(PartnerPortalConst.AUTHORNIZED_USER.equals(authority.getAuthority())){
                hasRequiredAuthority = true;
                break;
            }
        }
        if(!hasRequiredAuthority){
            return invalidateSession(session);
        }
        PartnerAccount pa = partnerUser.getPartnerAccount();
        if(pa == null){
            return invalidateSession(session);
        }
        return pa;
    }


    protected <T> ApiResult<T> isLoggedInWrappedResult(HttpSession session, Class<T> theClass) {
        if(session == null){
            return new ApiResult<>(ApiErrorCodes.NotLoggedIn);
        }
        if(getLoginUserAccount(session) == null){
            return new ApiResult<>(ApiErrorCodes.NotLoggedIn);
        }
        return (ApiResult<T>) new ApiResult<T>(true,"","",null);
    }

    protected <T> ApiResult<List<T>> isLoggedInWrappedResultForList(HttpSession session, Class<T> innerListClass) {
        if(session == null){
            return new ApiResult<>(ApiErrorCodes.NotLoggedIn);
        }
        if(getLoginUserAccount(session) == null){
            return new ApiResult<>(ApiErrorCodes.NotLoggedIn);
        }
        return (ApiResult<List<T>>) new ApiResult<T>(true,"","",null);
    }
}
