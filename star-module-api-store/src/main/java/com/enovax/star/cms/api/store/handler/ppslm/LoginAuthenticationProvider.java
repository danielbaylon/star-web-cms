package com.enovax.star.cms.api.store.handler.ppslm;

import com.enovax.star.cms.api.store.service.batch.axsync.IPartnerProfileSyncJob;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerAccountService;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component("PPSLMDaoAuthenticationProvider")
public class LoginAuthenticationProvider extends DaoAuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(LoginAuthenticationProvider.class);

    @Autowired
    private IPartnerAccountService paAccSrv;

    @Autowired
    private IPartnerProfileSyncJob paProfileSyncJob;

    @Autowired
    @Qualifier("PPSLMUserDetailsService")
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Authentication auth = super.authenticate(authentication);
            if(auth.isAuthenticated()){
                PPSLMPartner partner = paAccSrv.resetFailAttempts(authentication.getName());
                if(partner != null){
                    paProfileSyncJob.sync(partner.getId());
                    paAccSrv.cachePartnerConfigs(partner);
                }
            }
            return auth;
        } catch (AuthenticationException e) {
            paAccSrv.updateFailAttempts(authentication.getName(), e.getMessage());
            throw e;
        }
    }
}
