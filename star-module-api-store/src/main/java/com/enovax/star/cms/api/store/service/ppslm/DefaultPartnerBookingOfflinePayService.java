package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.OfflinePaymentStatus;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartItem;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartEmailDisplay;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppslm.PPSLMOfflinePaymentRequestSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.partnershared.repository.ppslm.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.IPartnerBookingRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.cart.IPartnerCartManager;
import com.enovax.star.cms.partnershared.service.ppslm.*;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DefaultPartnerBookingOfflinePayService implements IPartnerBookingOfflinePayService{

    @Autowired
    private IPartnerBookingService bookingSrv;
    @Autowired
    protected AxChannelTransactionService axChannelTransactionService;
    @Autowired
    protected AxStarService axStarService;
    @Autowired
    protected IPartnerBookingRepository bookingRepository;
    @Autowired
    protected IPartnerCartManager cartManager;
    @Autowired
    protected PPSLMInventoryTransactionRepository transRepo;
    @Autowired
    protected PPSLMInventoryTransactionItemRepository itemRepo;
    @Autowired
    protected PPSLMAxSalesOrderRepository axSalesOrderRepo;
    @Autowired
    protected PPSLMAxCheckoutCartRepository axCheckoutCartRepo;
    @Autowired
    protected PPSLMAxSalesOrderLineNumberQuantityRepository salesOrderLineNumberQuantityRepo;
    @Autowired
    protected StarTemplatingFunctions starfn;
    @Autowired
    protected IProductService productService;
    @Autowired
    protected IPartnerProductService partnerProductService;
    @Autowired
    protected IPartnerPurchaseTransactionService purchaseTransactionService;
    @Autowired
    private PPSLMOfflinePaymentRequestReqpository offlinePayRepo;
    @Autowired
    protected IPartnerEmailService emailService;
    @Autowired
    private ICountryRegionsRepository countrySrv;
    @Autowired
    private IOfflinePaymentEmailService offlineReqEmailSrv;
    @Autowired
    private ISystemParamService paramSrv;
    @Autowired
    private PPSLMInventoryTransactionItemRepository txnItemRepo;
    @Autowired
    private IOfflinePaymentService offlinePaySrv;

    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void cleanShoppingCart(StoreApiChannels channel, String sessionId) {
        bookingSrv.cartClear(channel, sessionId);
    }

    @Override
    public ApiResult<InventoryTransactionVM> doOfflineCheckout(StoreApiChannels channel, HttpSession session, PartnerAccount partnerAccount, String ip){
        final String sessionId = session.getId();
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            ApiResult<InventoryTransactionVM> apiTxn = new ApiResult<InventoryTransactionVM>();
            apiTxn.setErrorCode(ApiErrorCodes.NoCartFound.code);
            apiTxn.setMessage(ApiErrorCodes.NoCartFound.message);
            apiTxn.setSuccess(false);
            return apiTxn;
        }
        final ApiResult<InventoryTransactionVM> result = doOfflineCheckout(channel, savedCart, partnerAccount, ip);
        if (result.isSuccess()) {
            final InventoryTransactionVM txn = result.getData();
            bookingSrv.saveTransactionInSession(channel, session, txn);
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResult<InventoryTransactionVM> doOfflineCheckout(StoreApiChannels channel, FunCart cart, PartnerAccount partnerAccount, String ip) {
        final Date now = new Date();
        final List<FunCartItem> cartItems =cart.getItems();

        ApiResult<InventoryTransactionVM> apiTxn = null;

        //validate
        apiTxn = bookingSrv.validateCheckOut(partnerAccount, ip, cartItems); //TODO ok this is weird, it should check the status
        if(!(apiTxn != null && apiTxn.isSuccess())){
            apiTxn.setSuccess(false);
            return apiTxn;
        }
        // only general product is allowed
        List<String> productIds = new ArrayList<String>();
        List<FunCartItem> items = cart.getItems();
        for(FunCartItem item : items){
            final boolean isEvent = StringUtils.isNotEmpty(item.getEventGroupId());
            if(isEvent){
                apiTxn = new ApiResult<>();
                apiTxn.setErrorCode(ApiErrorCodes.CheckoutResultFailedMixEventInOffline.code);
                apiTxn.setMessage(ApiErrorCodes.CheckoutResultFailedMixEventInOffline.message);
                apiTxn.setSuccess(false);
                return apiTxn;
            }
            productIds.add(item.getListingId().trim());
        }
        try{
            ApiResult<List<ProductExtViewModel>> productExtRes = productService.getDataForProducts(channel, productIds);
            if(productExtRes != null && productExtRes.isSuccess()){
                List<ProductExtViewModel> extViewModels = productExtRes.getData();
                if(extViewModels != null && extViewModels.size() > 0){
                    for(ProductExtViewModel e: extViewModels) {
                        if(e != null){
                            if(e.getPrinterType() == 3){
                                apiTxn = new ApiResult<>();
                                apiTxn.setErrorCode(ApiErrorCodes.CheckoutResultFailedMixBarcodeInOffline.code);
                                apiTxn.setMessage(ApiErrorCodes.CheckoutResultFailedMixBarcodeInOffline.message);
                                apiTxn.setSuccess(false);
                                return apiTxn;
                            }
                            productIds.remove(e.getProductId());
                        }
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            apiTxn = new ApiResult<>();
            apiTxn.setErrorCode(ApiErrorCodes.CheckoutResultFailedProductDetailsCheckInOffline.code);
            apiTxn.setMessage(ApiErrorCodes.CheckoutResultFailedProductDetailsCheckInOffline.message);
            apiTxn.setSuccess(false);
            return apiTxn;
        }

        if(productIds.size() > 0){
            apiTxn = new ApiResult<>();
            apiTxn.setErrorCode(ApiErrorCodes.CheckoutResultFailedProductDetailsCheckInOffline.code);
            apiTxn.setMessage(ApiErrorCodes.CheckoutResultFailedProductDetailsCheckInOffline.message);
            apiTxn.setSuccess(false);
            return apiTxn;
        }

        apiTxn = doOfflineCheckBizValidation(channel, partnerAccount, cart);
        if(!(apiTxn != null && apiTxn.isSuccess())){
            apiTxn.setSuccess(false);
            return apiTxn;
        }

        //TODO refer to PurchaseServiceImpl.java doCheckout method
        final String receiptNumber = ProjectUtils.generateReceiptNumber(channel, now); //TODO follow the B2B receipt format

        if(receiptNumber == null) {
            apiTxn.setSuccess(false);
            apiTxn.setErrorCode(ApiErrorCodes.CheckoutResultFailedReceiptSequence.code);
            apiTxn.setMessage(ApiErrorCodes.CheckoutResultFailedReceiptSequence.message);
            return apiTxn;
        }

        apiTxn = bookingSrv.makeTransaction(channel, receiptNumber, partnerAccount, cart, ip, true);
        if(!apiTxn.isSuccess()) {
            apiTxn.setSuccess(false);
            return apiTxn;
        }

        return apiTxn;
    }

    private ApiResult<InventoryTransactionVM> doOfflineCheckBizValidation(StoreApiChannels channel, PartnerAccount account, FunCart cart) {
        ApiResult<InventoryTransactionVM> api = new ApiResult<InventoryTransactionVM>();
        // TODO: need to call validaty check and reservation cap check api before proceed with save offlien transaction
        api.setSuccess(true);
        return api;
    }

    private ApiResult<InventoryTransactionVM> doOfflineCheckBizValidation(StoreApiChannels channel, PartnerAccount account, PPSLMOfflinePaymentRequest request) {
        ApiResult<InventoryTransactionVM> api = new ApiResult<InventoryTransactionVM>();
        // TODO: need to call validaty check and reservation cap check api before proceed with save offlien transaction
        api.setSuccess(true);
        return api;
    }
    @Override
    public ApiResult<Integer> saveOfflineRequest(StoreApiChannels channel, HttpSession session, PartnerAccount account) {
        InventoryTransactionVM txn = bookingSrv.getStoreTransactionBySession(channel, session);
        PPSLMOfflinePaymentRequest request = new PPSLMOfflinePaymentRequest();
        request.setTransactionId(txn.getId());
        request.setStatus(OfflinePaymentStatus.Pending_Submit.code);
        request.setCreatedBy(account.getUsername());
        request.setMainAccountId(account.getMainAccountId());
        request.setCreatedDate(new Date(System.currentTimeMillis()));
        offlinePayRepo.save(request);
        return new ApiResult<>(true, null, null, request.getId());
    }

    @Transactional(readOnly = true)
    public ApiResult<OfflinePaymentResultVM> getOfflinePaymentRequestDetailsById(StoreApiChannels channel, PartnerAccount account, Integer reqId) throws BizValidationException {
        List<PPSLMOfflinePaymentRequest> requests = offlinePayRepo.findRequestByIdAndMainAccountId(reqId, account.getMainAccountId());
        if(requests == null || requests.size() != 1){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        PPSLMOfflinePaymentRequest request = requests.get(0);
        PPSLMInventoryTransaction txn = transRepo.findById(request.getTransactionId());
        PartnerFunCartEmailDisplay cart = offlinePaySrv.constcutOfflinePaymentCart(txn);
        OfflinePaymentRequestVM offlineReq = OfflinePaymentRequestVM.buildPaymentRequest(request, txn, account.getName(), account.getAccountCode());
        if(OfflinePaymentStatus.Pending_Submit.code.equals(offlineReq.getStatus())){
//            offlineReq.setCountries(countrySrv.getAllCountryVmList(channel.code));
            OfflinePaymentRequestVM.constructOfflinePaymentFormParameters(offlineReq);
        }
        OfflinePaymentResultVM vm = new OfflinePaymentResultVM();
        vm.setCart(cart);
        vm.setPaymentDetail(offlineReq);
        return new ApiResult<>(true, null,null, vm);
    }

    @Transactional(rollbackFor = {Exception.class, BizValidationException.class})
    public ApiResult<OfflinePaymentResultVM> withdrawOfflineRequest(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input) {
        if(input == null || input.getId() == null || input.getId().intValue() == 0 || input.getTransId() == null || input.getTransId().intValue() == 0){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        List<PPSLMOfflinePaymentRequest> requests = offlinePayRepo.findRequestByIdAndMainAccountId(input.getId(), account.getMainAccountId());
        if(requests == null || requests.size() != 1){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        PPSLMOfflinePaymentRequest request = requests.get(0);
        if(!OfflinePaymentStatus.Pending_Submit.code.equals(request.getStatus())){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        PPSLMInventoryTransaction txn = transRepo.findById(request.getTransactionId());
        if(input.getTransId().intValue() != txn.getId().intValue()){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        txn.setStatus(TicketStatus.Incomplete.name());
        txn.setModifiedDate(new Date(System.currentTimeMillis()));
        transRepo.save(txn);
        request.setStatus(OfflinePaymentStatus.Cancelled.code);
        offlinePayRepo.save(request);
        OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(request, txn, account.getName(), account.getAccountCode());

        OfflinePaymentResultVM vm = new OfflinePaymentResultVM();
        vm.setPaymentDetail(offReqVM);
        return new ApiResult<>(true, null, null, vm);
    }

    @Transactional(rollbackFor = {Exception.class, BizValidationException.class})
    public ApiResult<OfflinePaymentResultVM> submitOfflinePaymentRequest(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input) {
        if(input == null || input.getId() == null || input.getId().intValue() == 0 || input.getTransId() == null || input.getTransId().intValue() == 0){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        List<PPSLMOfflinePaymentRequest> requests = offlinePayRepo.findRequestByIdAndMainAccountId(input.getId(), account.getMainAccountId());
        if(requests == null || requests.size() != 1){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        PPSLMOfflinePaymentRequest request = requests.get(0);
        if(!OfflinePaymentStatus.Pending_Submit.code.equals(request.getStatus())){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        PPSLMInventoryTransaction txn = request.getInventoryTransaction();
        if(input.getTransId().intValue() != txn.getId().intValue()){
            return new ApiResult<>(false, null,"Invalid Request", null);
        }
        Date paymentDate = null;
        if(input.getPaymentDateStr() != null && input.getPaymentDateStr().trim().length() > 0){
            try {
                paymentDate = NvxDateUtils.parseDate(input.getPaymentDateStr().trim(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(paymentDate == null){
            return new ApiResult<>(false, null,"Payment Date is Required.", null);
        }
        ApiResult<InventoryTransactionVM> validateResult = this.doOfflineCheckBizValidation(channel, account, request);
        if(!validateResult.isSuccess()){
            return new ApiResult<>(false, null,validateResult.getMessage(), null);
        }

        txn.setStatus(TicketStatus.Pending_Approval.name());
        txn.setModifiedDate(new Date(System.currentTimeMillis()));
        transRepo.save(txn);

        request.setStatus(OfflinePaymentStatus.Pending_Approval.code);
        request.setPaymentDate(paymentDate);
        request.setPaymentType(input.getPaymentType());
        request.setBankCountry(input.getBankCountry());
//        request.setBankCountryCode(input.getBankCountry());
        request.setBankName(input.getBankName());
        request.setReferenceNum(input.getReferenceNum());
        request.setRemarks(offlinePaySrv.makeChatHists(request.getRemarks(), input.getRemarks(), account.getUsername()));
        request.setSubmitBy(account.getUsername());
        request.setSubmitDate(new Date(System.currentTimeMillis()));
        offlinePayRepo.save(request);

        OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(request, txn, account.getName(), account.getAccountCode());
        OfflinePaymentNotifyVM vm = new OfflinePaymentNotifyVM();
        vm.setPaymentDetail(offReqVM);
        vm.setCart(offlinePaySrv.constcutOfflinePaymentCart(transRepo.findById(request.getTransactionId())));
        vm.setMsg(input.getRemarks());
        vm.setBaseUrl(paramSrv.getApplicationContextPath());

        offlineReqEmailSrv.sendingOfflinePaymentRequestSubmissionEmail(vm, request, txn);
        return new ApiResult<>(true, null, null, vm);
    }

    @Override
    public ApiResult<String> notifyApprovers(StoreApiChannels channel, PartnerAccount account, OfflinePaymentRequestVM input) {
        try {
            if(input == null || input.getId() == null || input.getId().intValue() == 0 || input.getTransId() == null || input.getTransId().intValue() == 0 || input.getId() == -1){
                return new ApiResult<>(false, null,"Invalid Offline Payment Notification Request.", null);
            }
            Integer requestId = input.getId();
            if(input.getRemarks() == null || input.getRemarks().trim().length() < 20){
                return new ApiResult<>(false, null,"Minimum characters for \"Content of Notification\" is 20.", null);
            }
            if(input.getRemarks() == null || input.getRemarks().length() > 1000){
                return new ApiResult<>(false, null, "Maximum characters for \"Content of Notification\" is 1000.", null);
            }
            ApiResult<String> res = this.processPartnerNotification(channel, requestId, account, input);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResult<>(false, "","Processing notification failed, Please try again.", "");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    private ApiResult<String> processPartnerNotification(StoreApiChannels channel, Integer requestId, PartnerAccount account, OfflinePaymentRequestVM input) {
        List<PPSLMOfflinePaymentRequest> oprs = offlinePayRepo.findRequestByIdAndMainAccountId(requestId, account.getMainAccountId());
        if(oprs == null || oprs.size() == 0 || oprs.size() > 1){
            return new ApiResult<>(false, null,"Invalid Offline Payment Notification Request.", null);
        }
        PPSLMOfflinePaymentRequest opr = oprs.get(0);
        if(!(opr != null && OfflinePaymentStatus.Pending_Approval.name().equals(opr.getStatus()))){
            return new ApiResult<>(false, null,"Invalid Offline Payment Notification Request.", null);
        }
        String remarks = opr.getRemarks();
        String approverRemarks = opr.getApproverRemarks();
        ChatHistVM approverChat = offlinePaySrv.getLatestChatHistVM(null, approverRemarks, null, false);
        StringBuilder lastSubmitCountAfterApprovalChat = new StringBuilder();
        ChatHistVM requestChat = offlinePaySrv.getLatestChatHistVM(lastSubmitCountAfterApprovalChat, remarks, approverChat != null ? NvxDateUtils.parseDateFromDisplay(approverChat.getCreateDtStr(), true) : null, true);
        boolean allowed = false;
        if(approverChat == null){
            if(requestChat == null){
                allowed = true;
            }else{
                if(lastSubmitCountAfterApprovalChat != null && lastSubmitCountAfterApprovalChat.length() == 1 && Integer.parseInt(lastSubmitCountAfterApprovalChat.toString()) > 2){
                    allowed = false;
                }else{
                    allowed = true;
                }
            }
        }else{
            if(lastSubmitCountAfterApprovalChat != null && lastSubmitCountAfterApprovalChat.length() == 1 && Integer.parseInt(lastSubmitCountAfterApprovalChat.toString()) > 2){
                allowed = false;
            }else{
                allowed = true;
            }
        }
        if(!allowed){
            return new ApiResult<>(false, null,"You many only submit another message upon admin replied.", null);
        }
        opr.setRemarks(offlinePaySrv.makeChatHists(opr.getRemarks(), input.getRemarks(), account.getUsername()));
        offlinePayRepo.save(opr);

        PPSLMInventoryTransaction txn = transRepo.findById(opr.getTransactionId());

        OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(opr, txn, account.getName(), account.getAccountCode());
        OfflinePaymentNotifyVM vm = new OfflinePaymentNotifyVM();
        vm.setPaymentDetail(offReqVM);
        vm.setCart(offlinePaySrv.constcutOfflinePaymentCart(txn));
        vm.setMsg(input.getRemarks());
        vm.setBaseUrl(paramSrv.getApplicationContextPath());

        offlineReqEmailSrv.sendingOfflinePaymentPartnerToAdminNotification(vm, opr, transRepo.findById(opr.getTransactionId()));
        return new ApiResult<>(true, null, null, "");
    }

    @Transactional(readOnly = true)
    public ApiResult<PagedData<List<OfflinePaymentRequestVM>>> getPagedOfflinePaymentRequest(PartnerAccount account, String receiptNum, Date startDate, Date endDate,
                                                                                             String status, String username, String sortField, String sortDirection,
                                                                                             int page, int pageSize) throws Exception {

        List<Integer> txnList = new ArrayList<Integer>();
        txnList.add(-9876);

        if(receiptNum != null && receiptNum.trim().length() > 0){
            PPSLMInventoryTransaction txn = transRepo.findByReceiptNum(receiptNum);
            if(txn != null && txn.getId() != null){
                txnList.clear();
                txnList.add(txn.getId().intValue());
            }
        }

        Date now = new Date(System.currentTimeMillis());
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.EQ, account.getMainAccountId()));
        if(startDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.LE, endDate));
        }
        if(username != null && username.trim().length() > 0){
            criterias.add(new QCriteria("createdBy", QCriteria.Operation.LIKE, username));
        }
        if(status != null && status.trim().length() > 0){
            criterias.add(new QCriteria("status", QCriteria.Operation.EQ, status));
        }
        if(receiptNum != null && receiptNum.trim().length() > 0){
            criterias.add(new QCriteria("transactionId", QCriteria.Operation.IN, txnList, QCriteria.ValueType.Integer));
        }
        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.DESC, "createdDate");
        SpecificationBuilder builder = new SpecificationBuilder();
        Specifications querySpec = builder.build(criterias, PPSLMOfflinePaymentRequestSpecification.class);
        Page<PPSLMOfflinePaymentRequest> list = offlinePayRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        PagedData<List<OfflinePaymentRequestVM>> paged = new PagedData<List<OfflinePaymentRequestVM>>();
        paged.setData(getOfflinePaymentRequestVM(list, account.getPartner().getOrgName(), account.getPartner().getAccountCode(), now));
        paged.setSize(Long.valueOf(list.getTotalElements()).intValue());
        ApiResult<PagedData<List<OfflinePaymentRequestVM>>> api = new ApiResult<PagedData<List<OfflinePaymentRequestVM>>>();
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    private List<OfflinePaymentRequestVM> getOfflinePaymentRequestVM(Page<PPSLMOfflinePaymentRequest> list, String orgName, String accountCode, Date now) {
        List<OfflinePaymentRequestVM> ret = new ArrayList<OfflinePaymentRequestVM>();
        if(list != null && list.getSize() > 0){
            for(PPSLMOfflinePaymentRequest i : list){
                ret.add(OfflinePaymentRequestVM.buildPaymentRequestBaisc(i, transRepo.findById(i.getTransactionId()), orgName, accountCode));
            }
        }
        return ret;
    }
}
