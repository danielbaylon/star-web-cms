package com.enovax.star.cms.api.store.service.batch.axsync;

import com.enovax.star.cms.commons.service.axcrt.AxStarRestResponseErrorHandler;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;


/**
 * Created by jennylynsze on 11/25/16.
 * This is to just make sure that the Ax Star Service don't become idle...
 */
@Service
public class AxStarServiceAccessBatchJob {
    private static final Logger log = LoggerFactory.getLogger(AxStarServiceAccessBatchJob.class);

    private static boolean isAuthorInstance = false;

    private final String apiUrlB2CSLM = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.b2cslm");

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
            callAxService();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron ="0 0/15 * * * ?")
    public void callAxService(){
        if(isAuthorInstance) {
            try {
                final RestTemplate client = new RestTemplate();
                client.setErrorHandler(new AxStarRestResponseErrorHandler());
                SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) client.getRequestFactory();
                rf.setOutputStreaming(Boolean.FALSE);
                rf.setReadTimeout(300 * 1000);// todo:
                rf.setConnectTimeout(300 * 1000);// todo:
                final ResponseEntity<String> response = client.exchange(apiUrlB2CSLM + AxStarService.URL_FRAG_PRODUCT_GET + "/1", HttpMethod.GET, null, String.class);
            }catch(Exception e) {

            }
        }
    }
}
