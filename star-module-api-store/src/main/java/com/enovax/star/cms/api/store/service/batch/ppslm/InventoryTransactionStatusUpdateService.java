package com.enovax.star.cms.api.store.service.batch.ppslm;

import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerInventoryService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerPkgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by jennylynsze on 10/27/16.
 */
@Service
public class InventoryTransactionStatusUpdateService {

    @Autowired
    IPartnerInventoryService inventoryService;

    @Autowired
    IPartnerPkgService pkgService;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    private Logger log = LoggerFactory.getLogger(this.getClass());


    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    //Copied from TransStatusTask
    //once every 12:01 to update the status
    @Scheduled(cron = "${ppslm.inventory.status.update.job}")
    public void checkStatus(){
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    log.info("=== Starting Update Expired PPSLM Transactions...");
                    inventoryService.updateExpiredStatus();
                    log.info("=== Update Expired PPSLM Transactions End...");
                    log.info("=== Starting Update Forfeited PPSLM Transactions...");
                    inventoryService.updateForfeitedStatus();
                    log.info("=== Update Forfeited PPSLM Transactions End...");

                    log.info("=== Starting Update Expired PPSLM Packages...");
                    pkgService.updateExpiredStatus();
                    log.info("=== Update Expired PPSLM Packages End...");
                }
            });
        }
    }

}
