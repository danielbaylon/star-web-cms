package com.enovax.star.cms.api.store.batch.cache;


import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.template.ITicketTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by jennylynsze on 12/15/16.
 */
@Service
public class TicketTemplateDataCacheBatchJob {
    private Logger log = LoggerFactory.getLogger(TicketTemplateDataCacheBatchJob.class);

    @Autowired
    ITicketTemplateService templateService;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    @Scheduled(cron = "${cache.online.ticket.template.job}")
    public void retrieveExtDataCache() {
        realTimeTaskExecutor.submit(new Runnable() {
            @Override
            public void run() {
                StoreApiChannels channel[] = new StoreApiChannels[]{StoreApiChannels.B2C_SLM, StoreApiChannels.B2C_MFLG, StoreApiChannels.PARTNER_PORTAL_MFLG, StoreApiChannels.PARTNER_PORTAL_SLM};
                for (StoreApiChannels cha : channel) {
                    try {
                        templateService.updateTicketTemplateCache(cha);
                    } catch (AxChannelException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        });
    }
}
