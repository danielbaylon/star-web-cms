package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.util.Date;

/**
 * Created by houtao on 1/12/16.
 */
public interface IPartnerWoTReportService {
    SXSSFWorkbook exportAsExcel(PartnerAccount account, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String filterStatus, String showTimes);

    SXSSFWorkbook exportEmptyExcelWithError(String message);
}
