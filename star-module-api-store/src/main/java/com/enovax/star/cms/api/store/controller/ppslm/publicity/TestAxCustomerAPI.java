package com.enovax.star.cms.api.store.controller.ppslm.publicity;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.api.store.controller.BaseStoreApiController;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(PartnerPortalConst.ROOT + "/publicity/test-customer-api/")
public class TestAxCustomerAPI extends BaseStoreApiController {

    @Autowired
    private AxStarService axStarSrv;

    @RequestMapping(value="create-new-customer", method = RequestMethod.GET)
    public String apiCreateCustomer(@RequestParam(name = "key")String key){
        try{
            AxStarCustomer customer = populateAXCustomer123();
            System.out.println("====> customer creation details === > "+ JsonUtil.jsonify(customer));

            AxStarServiceResult<AxStarCustomer> result = axStarSrv.apiCustomerCreate(StoreApiChannels.PARTNER_PORTAL_SLM, customer);
            System.out.println("====> customer creation details === > "+ JsonUtil.jsonify(result));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return "/partner-portal-slm";
    }

    @RequestMapping(value="view-customer", method = RequestMethod.GET)
    public ResponseEntity<ApiResult<String>> apiGetCustomer(@RequestParam(name = "id")String id){
        try{
            AxStarServiceResult<AxStarCustomer> result = axStarSrv.apiCustomerGet(StoreApiChannels.PARTNER_PORTAL_SLM,id);
            System.out.println("====> customer details === > "+ JsonUtil.jsonify(result));

            return new ResponseEntity<>(new ApiResult<>(true, "", "", JsonUtil.jsonify(result)), HttpStatus.OK);

        }catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity<>(new ApiResult<>(false, "", "", ex.getMessage()), HttpStatus.OK);
        }
    }
    private static AxStarCustomer populateAXCustomer123() {


        AxStarCustomer c = new AxStarCustomer();
        c.setBlocked(false);
        c.setUrl("www.google.com/TEST18Aug");
        c.setEmail("hou.tao+18Aug@enovax.com");
        c.setName("htadmin test");
        c.setPhone("");
        c.setCellphone("96509210");
        c.setCustomerGroup("PPSLM-TA");
        c.setFirstName("htadmin");
        c.setLanguage("");
        c.setOrganizationId("");
        c.setIdentificationNumber("HTADMIN000001");
        c.setCreatedDateTime(new Date(System.currentTimeMillis()));
        c.setCurrencyCode(PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE);
        c.setPriceGroup(String.valueOf("2"));
        c.setExtensionProperties(new ArrayList<AxStarExtensionProperty>());

        AxStarAddress addr = new AxStarAddress();
        addr.setPrimary(true);
        addr.setPrivate(false);
        addr.setDeactivate(false);
        addr.setCity("Singapore");
        addr.setName("Vertex htadmin Singapore");
        //addr.setAddressTypeValue(); // business address
        addr.setZipCode("408868");
        //addr.setFullAddress(address);
        addr.setAttentionTo("");
        addr.setBuildingCompliment("");
        addr.setUrl("");

        addr.setStreet("");
        addr.setStreetNumber("");
        addr.setState("");
        addr.setStateName("");
        addr.setPhone("");
        addr.setCounty("");
        addr.setCountyName("");
        addr.setPhoneExt("");
        addr.setTaxGroup("");
        addr.setPostbox("");
        addr.setDistrictName("");
        addr.setTwoLetterISORegionName("");
        addr.setThreeLetterISORegionName("SGP");
        addr.setExtensionProperties(new ArrayList<AxStarExtensionProperty>());

        List<AxStarAddress> addrs = new ArrayList<AxStarAddress>();
        addrs.add(addr);
        c.setAddresses(addrs);

        /***


        AxStarCustomer c = new AxStarCustomer();
        c.setBlocked(false);
        c.setCurrencyCode("SGD");
        c.setCustomerGroup("PPSLM-TA"); // length limitation
        c.setName("HT3335TEST");
        c.setEmail("hou.tao+HT3335Test@enovax.com");
        c.setIdentificationNumber("G18998767T");
        c.setFirstName("HT3335");
        c.setPriceGroup("Tier2"); //TODO:CHECK
        c.setUrl("www.google.com/website/subcompany/HT3335TEST");
        c.setCellphone("+65 6908 1869");
        c.setPhone("+65 9650 9210");
        c.setLanguage("en-us");
        c.setName("Inc Name");
        c.setFirstName("HT3335TEST");
        c.setCreatedDateTime(new Date(System.currentTimeMillis()));

        AxStarAddress address = new AxStarAddress();
        address.setFullAddress("018945 Singapore, HouGang, NewCity, Singapora");
        address.setAddressTypeValue(9); // business address
        address.setZipCode("018945");
        address.setCity("Singapore");
        address.setBuildingCompliment("BCP123");
        address.setDeactivate(false);
        address.setName("HT Testing 456");
        address.setPrimary(true);
        address.setPrivate(false);
        address.setUrl("Hello123URL");
        //address.setStreet("MAR0065");
        //address.setStreetNumber("MAR0065");
        address.setState("SGP");
        address.setStateName("Singapore");
        address.setPhone("");
        address.setCounty("");
        address.setCountyName("");
        address.setPhoneExt("");
        address.setTaxGroup("");
        address.setDistrictName("");
        address.setPostbox("");
        address.setTwoLetterISORegionName("");
        address.setThreeLetterISORegionName("SGP");

        //address.setEmail("hou.tao+HT3335TestAddr@enovax.com");
        //address.setName("Address name of the company");
        //address.setPrimary(false);
        //address.setPrivate(false);
        //address.setDeactivate(false);
        //address.setUrl("www.google.com/httesting123/HT3335");

        List<AxStarAddress> addresses = new ArrayList<AxStarAddress>();
        addresses.add(address);
        c.setAddresses(addresses);

        List<AxStarRichMediaLocation> imgs = new ArrayList<AxStarRichMediaLocation>();
        AxStarRichMediaLocations media = new AxStarRichMediaLocations();
        media.setItems(imgs);

         ***/

        return c;
    }

    private AxStarCustomer populateAXCustomer( ) {

        String orgName        =  "ABAI INC 3";                          // maximum 100
        String accountCode    =  "ABAIINC3";                            // maximum 10
        String orgType        =  "PPSLM-CORP";                          // maximum 10
        String uen            =  "UNE10012002";
        String contactPerson  =  "CPABAI1001";
        String address        =  "ABAI INC 3, USA";
        String postalCode     =  "018927";
        String city           =  "SINGAPORE";
        String officeNo       =  "908790099";
        String mobileNo       =  "986800993";
        String email          =  "hou.tao+ABAIINC3@gmail.com";
        String website        =  "www.googel.com/abai";
        String preferenceLang =  "en-us";                               // maximum 7

        AxStarCustomer c = new AxStarCustomer();
        c.setBlocked(false);
        c.setUrl(website);
        c.setEmail(email);
        c.setName(orgName);
        c.setPhone(officeNo);
        c.setCellphone(mobileNo);
        c.setCustomerGroup(orgType);
        c.setFirstName(contactPerson);
        c.setLanguage(preferenceLang);
        c.setOrganizationId(accountCode);
        c.setIdentificationNumber(uen);
        c.setCreatedDateTime(new Date(System.currentTimeMillis()));
        c.setCurrencyCode("SGD");
        c.setPriceGroup(String.valueOf("2"));
        c.setExtensionProperties(new ArrayList<AxStarExtensionProperty>());

        AxStarAddress addr = new AxStarAddress();
        addr.setPrimary(true);
        addr.setPrivate(false);
        addr.setDeactivate(false);
        addr.setCity(city);
        addr.setName(address);
        addr.setAddressTypeValue(9); // business address
        addr.setZipCode(postalCode);
        addr.setFullAddress(address);
        addr.setAttentionTo(contactPerson);
        addr.setBuildingCompliment(orgName);
        addr.setUrl(website);


        addr.setStreet("");
        addr.setStreetNumber("");
        addr.setState("");
        addr.setStateName("");
        addr.setPhone("");
        addr.setCounty("");
        addr.setCountyName("");
        addr.setPhoneExt("");
        addr.setTaxGroup("");
        addr.setPostbox("");
        addr.setDistrictName("");
        addr.setTwoLetterISORegionName("");
        addr.setThreeLetterISORegionName("SGP");
        addr.setExtensionProperties(new ArrayList<AxStarExtensionProperty>());

        List<AxStarAddress> addrs = new ArrayList<AxStarAddress>();
        addrs.add(addr);
        c.setAddresses(addrs);

        return c;
    }

    private static AxStarExtensionPropertyValue populateAxStarExtensionProperty(String val) {
        AxStarExtensionPropertyValue pv = new AxStarExtensionPropertyValue();
        pv.setStringValue(val);
        return pv;
    }

    private static String getNonEmptyStringValue(String v) {
        if(v != null && v.trim().length() > 0){
            return v.trim();
        }
        return "";
    }

    private static String getFormatedDate(Date licenseExpDt) {
        if(licenseExpDt == null){
            return "";
        }
        return NvxDateUtils.formatDateForDisplay(licenseExpDt, false);
    }

    private static AxStarExtensionProperty populateAxStarExtensionProperty(String key, String val) {
        AxStarExtensionProperty p  = new AxStarExtensionProperty();
        p.setKey(key);
        AxStarExtensionPropertyValue pv = new AxStarExtensionPropertyValue();
        pv.setStringValue(val);
        p.setValue(pv);
        return p;
    }
}
