package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.pin.*;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.WOTVerifyProdQtyVM;
import com.enovax.star.cms.commons.model.partner.ppslm.WingsOfTimeRequestCommentModel;
import com.enovax.star.cms.commons.model.partner.ppslm.WoTVM;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.ppslm.WingOfTimeChannel;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import com.enovax.star.cms.partnershared.service.ppslm.IWingsOfTimeLogService;
import com.enovax.star.cms.partnershared.service.ppslm.IWingsOfTimeSharedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Service
public class DefaultWingsOfTimeService implements IWingsOfTimeService {

    private static Logger logger = LoggerFactory.getLogger(DefaultWingsOfTimeService.class);

    @Autowired
    private AxStarService axStarSrv;

    @Autowired
    private AxChannelTransactionService txnSrv;

    @Autowired
    private IWingsOfTimeLogService wotLogSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private IWingsOfTimeSharedService wotSharedSrv;


    public ApiResult<AxWOTSalesOrder> saveWingsOfTimeReservationOrder(
            PartnerAccount paAcc,
            ApiResult<AxWOTSalesOrder> retResponse, PPSLMWingsOfTimeReservation wot) {
        try{
            addToShoppingCart(paAcc, wot);
            checkoutShoppingCart(paAcc, wot);
            ApiResult<AxWOTSalesOrder> response = null;
            response = saveWingsOfTimeSalesOrder(paAcc, wot);
            retResponse = processSalesOrder(paAcc, response, retResponse, wot);
        }catch (BizValidationException ex){
            if(!retResponse.isSuccess()){
                retResponse.setSuccess(false);
                retResponse.setMessage(ex.getMessage());
            }
            ex.printStackTrace();
        }catch (Exception ex){
            ex.printStackTrace();
        }catch (Throwable ex){
            ex.printStackTrace();
        }
        return retResponse;
    }

    @Override
    public ApiResult<AxWOTSalesOrder> saveWingsOfTimeReservationOrderFromReservedPool(
            PartnerAccount paAcc,
            ApiResult<AxWOTSalesOrder> retResponse,
            PPSLMWingsOfTimeReservation wot,
            PPSLMWingsOfTimeReservation srcWoT) {
        boolean isSuccess = false;
        boolean isReduced = false;
        try{
            ApiResult<List<AxRetailTicketRecord>> reduceResponse = reduceReservedTicketPool(paAcc, srcWoT, wot, true);
            if(!(reduceResponse != null && reduceResponse.isSuccess())){
                throw new BizValidationException(reduceResponse != null && reduceResponse.getMessage() != null && reduceResponse.getMessage().trim().length() > 0 ? reduceResponse.getMessage() : "Updating reserved ticket quantity failed.");
            }
            isReduced = true;
            addToShoppingCart(paAcc, wot);
            checkoutShoppingCart(paAcc, wot);
            ApiResult<AxWOTSalesOrder> response = null;
            response = saveWingsOfTimeSalesOrder(paAcc, wot);
            retResponse = processSalesOrder(paAcc, response, retResponse, wot);
            if(response != null && response.isSuccess()){
                isSuccess = true;
            }
        }catch (BizValidationException ex){
            if(!retResponse.isSuccess()){
                retResponse.setSuccess(false);
                retResponse.setMessage(ex.getMessage());
            }
            ex.printStackTrace();
        }catch (Exception ex){
            ex.printStackTrace();
        }catch (Throwable ex){
            ex.printStackTrace();
        }finally{
            if(isReduced){
                if(!isSuccess){
                    reduceReservedTicketPool(paAcc, srcWoT, wot, false);
                }
            }
        }
        return retResponse;
    }

    private ApiResult<List<AxRetailTicketRecord>> reduceReservedTicketPool(PartnerAccount paAcc, PPSLMWingsOfTimeReservation srcWoT, PPSLMWingsOfTimeReservation wot, boolean isReduce) {
        PPSLMWingsOfTimeReservation temp = new PPSLMWingsOfTimeReservation();
        temp.setId(srcWoT.getId());
        if(isReduce){
            temp.setQty(srcWoT.getQty() - srcWoT.getQtySold() - wot.getQty());
        }else{
            temp.setQty(srcWoT.getQty() - srcWoT.getQtySold() + wot.getQty());
        }
        temp.setItemId(srcWoT.getItemId());
        temp.setReceiptNum(srcWoT.getReceiptNum());
        temp.setEventLineId(srcWoT.getEventLineId());
        temp.setEventGroupId(srcWoT.getEventGroupId());
        temp.setAxCheckoutLineId(srcWoT.getAxCheckoutLineId());
        temp.setProductId(srcWoT.getProductId());
        temp.setEventDate(srcWoT.getEventDate());
        ApiResult<List<AxRetailTicketRecord>> retResponse = new ApiResult<List<AxRetailTicketRecord>>();
        wotSharedSrv.saveOrUpdatePreservationOrder(retResponse, paAcc.getPartner().getAxAccountNumber(), temp, true);
        return retResponse;
    }

    private ApiResult<AxWOTSalesOrder> processSalesOrder(
            PartnerAccount paAcc,
            ApiResult<AxWOTSalesOrder> response, ApiResult<AxWOTSalesOrder> retResponse,
            PPSLMWingsOfTimeReservation wot) {
        if(response != null && response.isSuccess()){
            storeSaveWOTOderResponse(paAcc, wot, response);
            retResponse = response;
        }else{
            if(response != null){
                retResponse.setData(response.getData());
                retResponse.setMessage(response.getMessage());
                retResponse.setErrorCode(response.getErrorCode());
            }else{
                retResponse.setSuccess(false);
            }
        }
        return retResponse;
    }

    private void storeSaveWOTOderResponse(PartnerAccount paAcc,
                                          PPSLMWingsOfTimeReservation wot,
                                          ApiResult<AxWOTSalesOrder> tempResult) {
        if(wot == null || tempResult == null){
            return;
        }
        AxWOTSalesOrder salesOrder = tempResult.getData();
        if(salesOrder.getGeneratedPIN() != null && salesOrder.getGeneratedPIN().trim().length() > 0){
            wot.setPinCode(salesOrder.getGeneratedPIN());
        }
        AxSalesOrder o = salesOrder.getSalesOrder();
        if(o != null){
            //wot.setAxrtId(o.getId());
            //wot.setAxrtValChannelId(o.getChannelId());
            wot.setAxrtConfirmationId(o.getConfirmationId());
            wot.setAxrtSalesOrderStatus(o.getStatus());
            wot.setAxrtSalesId(o.getSalesId());
            //wot.setAxrtRequestedDeliveryDate(o.getRequestedDeliveryDate());
            //wot.setAxrtPaymentStatus(o.getPaymentStatus());
            //wot.setAxrtOrderPlacedDate(o.getOrderPlacedDate());
            //wot.setAxrtChargeAmountWithCurrency(o.getChargeAmountWithCurrency());
            //wot.setAxrtDeliveryModeDescription(o.getDeliveryModeDescription());
            //wot.setAxrtDeliveryModeId(o.getDeliveryModeId());
            //wot.setAxrtDiscountAmount(o.getDiscountAmount());
            //wot.setAxrtSubtotalWithCurrency(o.getSubtotalWithCurrency());
            //wot.setAxrtTotalAmountWithCurrency(o.getTotalAmountWithCurrency());
            wot.setAxrtTotalAmount(o.getTotalAmount());
            //wot.setAxrtTaxAmountWithCurrency(o.getTaxAmountWithCurrency());
            List<AxTransactionItem> txnItems = o.getTxnItems();
            AxTransactionItem txnItem = txnItems != null && txnItems.size() > 0 ? txnItems.get(0) : null;
            if(txnItem != null){
                wot.setAxrtItemId(txnItem.getItemId());
                wot.setAxrtItemType(txnItem.getItemType().value());
                wot.setAxrtLineId(txnItem.getLineId());
                //wot.setAxrtNetAmountWithCurrency(txnItem.getNetAmountWithCurrency());
                //wot.setAxrtpPriceWithCurrency(txnItem.getNetAmountWithCurrency());
                //wot.setAxrtpPriceWithCurrency(txnItem.getPriceWithCurrency());
                wot.setAxrtProductId(txnItem.getProductId());
                wot.setAxrtQuantity(txnItem.getQuantity());
            }

            wot.setModifiedDate(new Date(System.currentTimeMillis()));
            wot.setModifiedBy(paAcc.getId().toString());
        }
    }

    private void addToShoppingCart(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot) throws Exception {
        AxStarInputAddCart request = populateShoppingCart(partnerAccount, wot);
        AxStarServiceResult<AxStarCartNested> response = axStarSrv.apiCartAddItem(WingOfTimeChannel.CHANNEL, request);
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                "add-wot-to-cart",
                response.isSuccess(),
                null,
                JsonUtil.jsonify(response.getError()),
                JsonUtil.jsonify(request),
                JsonUtil.jsonify(response.getData())
        );
        verifyCartResponse(partnerAccount, wot, response);
        storeAdd2CartResponse(wot, response);
    }

    private void checkoutShoppingCart(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot) throws Exception {
        AxStarServiceResult<AxStarCartNested> response = axStarSrv.apiCheckout(
                WingOfTimeChannel.CHANNEL,
                wot.getAxCartId(),
                partnerAccount.getMainAccount().getProfile().getAxAccountNumber(),
                wot.getReceiptNum()
        );
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                "checkout-wot-cart",
                response != null ? response.isSuccess() : false, null,
                response != null ? JsonUtil.jsonify(response.getError()) : null,
                String.format("{ \"cartId\" : \"%s\", \"customerId\" : \"%s\", \"salesOrderNumber\" : \"%s\"",
                        String.valueOf(wot.getAxCartId()),
                        String.valueOf(partnerAccount.getMainAccount().getProfile().getAxAccountNumber()),
                        String.valueOf(wot.getReceiptNum())
                ),
                response != null && response.getData() != null ? JsonUtil.jsonify(response.getData()) : null
        );
        if(response == null){
            throw new BizValidationException("Checkout wings of time shopping card failed!");
        }
        if(!response.isSuccess()){
            throw new BizValidationException("Checkout wings of time shopping card failed!");
        }
        verifyCartResponse(partnerAccount, wot, response);
        storeCheckoutCartResponse(wot, response);
    }


    private ApiResult<AxWOTSalesOrder> saveWingsOfTimeSalesOrder(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot) throws AxChannelException {
        AxWOTOrder order = new AxWOTOrder();
        order.setCartId(wot.getAxCheckoutCartId());
        order.setCustomerAccount(partnerAccount.getMainAccount().getProfile().getAxAccountNumber());
        order.setReceiptEmail(wot.getReceiptEmail());
        order.setOnlineSalesPool(PartnerPortalConst.WOT_ONLINE_SALES_POOL);
        ApiResult<AxWOTSalesOrder> wotOrder = txnSrv.apiWotSaveOrder(WingOfTimeChannel.CHANNEL, order);
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                "save-wot-order",
                wotOrder != null ? wotOrder.isSuccess() : false,
                wotOrder != null ? wotOrder.getErrorCode() : null,
                wotOrder != null ? wotOrder.getMessage() : null,
                JsonUtil.jsonify(order),
                wotOrder != null && wotOrder.getData() != null ? JsonUtil.jsonify(wotOrder.getData()) : null
        );
        return wotOrder;
    }

    private void storeAdd2CartResponse(PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) {
        AxStarCartNested cartWrap = response.getData();
        AxStarCart cart = cartWrap.getCart();
        AxStarCartLine line = cart.getCartLines().get(0);
        wot.setAxCartId(cart.getId());
        //wot.setAxLineId(line.getLineId());
        //wot.setAxSubtotalAmt(cart.getSubtotalAmount());
        //wot.setAxTotalAmt(cart.getTotalAmount());
        //wot.setAxPrice(line.getPrice());
        //wot.setAxExternalPrice(line.getExtendedPrice());
        //wot.setAxTaxAmt(line.getTaxAmount());
        //wot.setAxItemTaxGroupId(line.getItemTaxGroupId());
        //wot.setAxNetAmtWoTax(line.getNetAmountWithoutTax());
        //wot.setAxDiscountAmt(line.getDiscountAmount());

    }

    private void storeCheckoutCartResponse(PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) {
        AxStarCartNested cartWrap = response.getData();
        AxStarCart cart = cartWrap.getCart();
        AxStarCartLine line = cart.getCartLines().get(0);
        wot.setAxCheckoutCartId(cart.getId());
        wot.setAxCheckoutLineId(line.getLineId());
        wot.setReceiptEmail(cart.getReceiptEmail());
    }

    private AxStarInputAddCart populateShoppingCart(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot) throws Exception {
        String custId = partnerAccount.getMainAccount().getProfile().getAxAccountNumber();
        if(custId == null || custId.trim().length() == 0){
            throw new BizValidationException("No customer account number found!");
        }

        Map<String, String> extPropsMap = new HashMap<String, String>();
        extPropsMap.put("EventLineId", wot.getEventLineId());
        extPropsMap.put("EventGroupId", wot.getEventGroupId());
        extPropsMap.put("EventDate", NvxDateUtils.formatDate(wot.getEventDate(), NvxDateUtils.AX_ONLINE_CAT_SERVICE_WOT_RESERVE_DATE_FORMAT));

        AxStarInputAddCartItem item = new AxStarInputAddCartItem();
        item.setProductId(Long.valueOf(wot.getProductId()));
        item.setQuantity(wot.getQty());
        item.setComment(populateShoppingCartComment(wot));
        item.setStringExtensionProperties(extPropsMap);

        List<AxStarInputAddCartItem> items = new ArrayList<AxStarInputAddCartItem>();
        items.add(item);

        AxStarInputAddCart cart = new AxStarInputAddCart();
        cart.setItems(items);
        cart.setCustomerId(custId);
        cart.setCartId("");
        cart.setSalesOrderNumber(wot.getReceiptNum());
        return cart;
    }

    private String populateShoppingCartComment(PPSLMWingsOfTimeReservation r){
        WingsOfTimeRequestCommentModel vm = new WingsOfTimeRequestCommentModel();
        vm.setReceiptNum(r.getReceiptNum());
        vm.setProductId(r.getProductId());
        vm.setEventLineId(r.getEventLineId());
        vm.setShowDateTime(NvxDateUtils.formatDate(r.getEventDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
        vm.setQty(r.getQty());
        vm.setTicketPrice(r.getTicketPrice().toString());
        vm.setCreatedDate(NvxDateUtils.formatDate(r.getCreatedDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
        vm.setItemId(r.getItemId());
        vm.setMediaTypeId(r.getMediaTypeId());
        vm.setEventGroupId(r.getEventGroupId());
        vm.setRemarks(r.getRemarks());
        return JsonUtil.jsonify(vm);
    }

    private void verifyCartResponse(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) throws Exception  {
        if(response == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        if(!response.isSuccess()){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        AxStarCartNested cartWrap = response.getData();
        if(cartWrap == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        AxStarCart cart = cartWrap.getCart();
        if(cart == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        if(!partnerAccount.getMainAccount().getProfile().getAxAccountNumber().equals(cart.getCustomerId())){
            throw new BizValidationException("Verify customer no. failed for add to wings of time shopping cart!");
        }
        if(!(cart.getCartLines() != null && cart.getCartLines().size() == 1 && cart.getCartLines().get(0) != null)){
            throw new BizValidationException("Verify product/items failed for add to wings of time shopping cart!");
        }
        AxStarCartLine line = cart.getCartLines().get(0);
        if(!wot.getProductId().equalsIgnoreCase(line.getProductId().toString())){
            throw new BizValidationException("Verify product id failed for add to wings of time shopping cart!");
        }
        if(!wot.getItemId().equalsIgnoreCase(line.getItemId())){
            throw new BizValidationException("Verify item id failed for add to wings of time shopping cart!");
        }
        if(!(wot.getQty().intValue() == line.getQuantity())){
            throw new BizValidationException("Verify item quantity failed for add to wings of time shopping cart!");
        }
    }

    public ApiResult<String> updateWingsOfTimeReservationOrder(PartnerAccount paAcc, ApiResult<?> result, PPSLMWingsOfTimeReservation wot, String markupCode, BigDecimal markupAmt) throws Exception {
        try{
            AxWOTPinUpdate request = new AxWOTPinUpdate();
            request.setPinCode(wot.getPinCode());
            request.setQty(wot.getQty());
            request.setEventLineId(wot.getEventLineId());
            request.setEventDate(wot.getEventDate());
            request.setMarkupCode(markupCode);
            request.setMarkupAmt(markupAmt);
            request.setItemId(wot.getItemId());
            request.setInventTransId(wot.getAxInventTransId());
            ApiResult<String> apiResult = txnSrv.apiWotUpdatePin(WingOfTimeChannel.CHANNEL, request);
            wotLogSrv.saveWingsOfTimeReservationLog(
                    wot,
                    WingOfTimeChannel.CHANNEL.code,
                    "update-wot-order",
                    apiResult != null ? apiResult.isSuccess() : false,
                    apiResult != null ? apiResult.getErrorCode() : null,
                    apiResult != null ? apiResult.getMessage() : null,
                    JsonUtil.jsonify(wot),
                    apiResult != null && apiResult.getData() != null ? JsonUtil.jsonify(apiResult.getData()) : null
            );
            return apiResult;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex);
        }
    }

    @Override
    public ApiResult<String> cancelWingsOfTimeReservationOrder(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot, String markupCode, BigDecimal markupAmt) throws Exception {
        try{
            ApiResult<String> apiResult = txnSrv.apiWotCancelPin(WingOfTimeChannel.CHANNEL, wot.getPinCode(), markupCode, markupAmt);
            wotLogSrv.saveWingsOfTimeReservationLog(
                    wot,
                    WingOfTimeChannel.CHANNEL.code,
                    "cancel-wot-order",
                    apiResult != null ? apiResult.isSuccess() : false,
                    apiResult != null ? apiResult.getErrorCode() : null,
                    apiResult != null ? apiResult.getMessage() : null,
                    JsonUtil.jsonify(wot),
                    apiResult != null && apiResult.getData() != null ? JsonUtil.jsonify(apiResult.getData()) : null
            );
            return apiResult;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex);
        }
    }


    @Override
    public ApiResult<String> checkProductItemAvailableQuantity(WOTVerifyProdQtyVM vm) {
        if(vm == null){
            return null;
        }
        AxRetailCartTicket c = new AxRetailCartTicket();
        c.setAccountNum(vm.getCustAccountNo());
        c.setEventDate(vm.getEventDate());
        c.setEventGroupId(vm.getEventGroupId());
        c.setEventLineId(vm.getEventLineId());
        c.setItemId(vm.getItemId());
        c.setListingId(vm.getProductId());
        c.setQty(vm.getQty());
        c.setUpdateCapacity(vm.isUpdateCapacity() ? 1 : 0);
        if(!vm.isNewReservation()){
            c.setLineId(vm.getLineId());
            c.setTransactionId(vm.getTransactionId());
        }

        List<AxRetailCartTicket> clist = new ArrayList<AxRetailCartTicket>();
        clist.add(c);

        ApiResult<List<AxRetailTicketRecord>> result = null;
        try {
            result = txnSrv.apiCartValidateQuantity(WingOfTimeChannel.CHANNEL, vm.getTransactionId() != null && vm.getTransactionId().trim().length() > 0 ? vm.getTransactionId().trim() : null, clist);
        } catch (AxChannelException e) {
            e.printStackTrace();
        }

        if(result  != null){
            System.out.println("checkProductItemAvailableQuantity >> "+JsonUtil.jsonify(result));
        }

        return null;
    }

//    @Override
//    public Map<String, String> getAllWoTPinCodeReferenceIdMapByAxAccountNumber(String axAccountNumber) {
//        Map<String, String> map = new HashMap<String, String>();
//        ApiResult<List<AxWOTPinViewTable>> pinCodeList = null;
//        try {
//            pinCodeList = txnSrv.apiWotViewPin(WingOfTimeChannel.CHANNEL, axAccountNumber, sysParamSrv.getWingsOfTimeReservationType());
//        } catch (AxChannelException e) {
//            e.printStackTrace();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if(pinCodeList != null && pinCodeList.getData() != null && pinCodeList.getData().size() > 0){
//            List<AxWOTPinViewTable> tables = pinCodeList.getData();
//            for(AxWOTPinViewTable i : tables){
//                map.put(i.getPinCode(), i.getReferenceId());
//            }
//            tables = null;
//        }
//        pinCodeList = null;
//        return map;
//    }


    public ApiResult<WoTVM> updateWingsOfTimeReservationOrderDetails(String axAccountNumber, final PPSLMWingsOfTimeReservation wot) {
        ApiResult<WoTVM> apiResult = new ApiResult<WoTVM>();
        String errorMsg = null;
        WoTVM woTVM = new WoTVM();
        woTVM.setAxReferenceId(wot.getAxReferenceId());

        ApiResult<AxWOTPinViewTable> pinResultView = null;
        try {
            pinResultView = txnSrv.apiGetB2BPINTableInquiry(WingOfTimeChannel.CHANNEL, wot.getPinCode());
        } catch (AxChannelException e) {
            errorMsg = e.getMessage();
            logger.error("Retrieve pin code details for "+wot.getPinCode()+" is failed : "+e.getMessage(), e);
            e.printStackTrace();
        } catch (ParseException e) {
            errorMsg = e.getMessage();
            logger.error("Retrieve pin code details for "+wot.getPinCode()+" is failed : "+e.getMessage(), e);
            e.printStackTrace();
        }catch (Exception ex){
            errorMsg = ex.getMessage();
            logger.error("Retrieve pin code details for "+wot.getPinCode()+" is failed : "+ex.getMessage(), ex);
            ex.printStackTrace();
        }catch (Throwable ex){
            errorMsg = ex.getMessage();
            logger.error("Retrieve pin code details for "+wot.getPinCode()+" is failed : "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
        if((pinResultView == null) || (!pinResultView.isSuccess())){
            if(errorMsg == null || errorMsg.trim().length() == 0){
                errorMsg = pinResultView.getMessage();
            }
            if(errorMsg == null || errorMsg.trim().length() == 0){
                errorMsg = "Retrieving Pin Code details failed";
            }
            logger.error("Retrieve pin code details not found for "+wot.getPinCode()+", error message : "+errorMsg);
            apiResult.setSuccess(false);
            apiResult.setMessage(errorMsg);
            return apiResult;
        }

        AxWOTPinViewTable pinViewTable = pinResultView.getData();
        List<AxWOTPinViewLine> lines = null;
        if(pinViewTable != null){
            lines = pinViewTable.getPinViewLineCollection();
        }
        if(lines != null && lines.size() > 0){
            for(AxWOTPinViewLine line : lines){
                if(wot.getEventGroupId() != null && wot.getEventGroupId().equalsIgnoreCase(line.getEventGroupId()) &&
                        wot.getEventLineId() != null && wot.getEventLineId().equalsIgnoreCase(line.getEventLineId()) &&
                        wot.getItemId() != null && wot.getItemId().equalsIgnoreCase(line.getItemId())
                ){
                    woTVM.setIsWasheddown(pinViewTable.getIsWashedDown());
                    woTVM.setQtyWashedDown(line.getQtyWashedDown());
                    woTVM.setQtyAutoGenerated(line.getQtyAutoGenerated());
                    woTVM.setAxReferenceId(line.getReferenceId());
                    woTVM.setAxInventTransId(line.getInventTransId());
                    woTVM.setQty(line.getQty());
                    woTVM.setQtyRedeemed(line.getQtyRedeemed());
                    woTVM.setQtyReturned(line.getQtyReturned());
                }
            }
        }else{
            errorMsg = "Pin Code '"+wot.getPinCode()+"' details not found.";
        }
        apiResult.setData(woTVM);
        apiResult.setSuccess(true);
        if(errorMsg != null && errorMsg.trim().length() > 0){
            apiResult.setMessage(errorMsg);
            apiResult.setSuccess(false);
        }
        return apiResult;
    }

//    /**
//     * Old solution using PinView And PinViewLine, replaced by new API GetB2BPINTableInquiry
//     * @param axAccountNumber
//     * @param wot
//     * @return
//     */
//    @Deprecated
//    public ApiResult<WoTVM> updateWingsOfTimeReservationOrderDetails_oos(String axAccountNumber, final PPSLMWingsOfTimeReservation wot) {
//        ApiResult<WoTVM> apiResult = new ApiResult<WoTVM>();
//        String errorMsg = null;
//        WoTVM woTVM = new WoTVM();
//        woTVM.setAxReferenceId(wot.getAxReferenceId());
//        if(wot.getAxReferenceId() == null || wot.getAxReferenceId().trim().length() == 0){
//            ApiResult<List<AxWOTPinViewTable>> pinCodeList = null;
//            try {
//                pinCodeList = null;// txnSrv.apiWotViewPin(WingOfTimeChannel.CHANNEL, axAccountNumber, sysParamSrv.getWingsOfTimeReservationType());
//            } catch (AxChannelException e) {
//                e.printStackTrace();
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            if(pinCodeList != null && pinCodeList.getData() != null && pinCodeList.getData().size() > 0){
//                List<AxWOTPinViewTable> tables = pinCodeList.getData();
//                for(AxWOTPinViewTable i : tables){
//                    if(wot.getPinCode() != null && wot.getPinCode().trim().length() > 0 && wot.getPinCode().trim().equals(i.getPinCode())){
//                        woTVM.setAxReferenceId(i.getReferenceId());
//                    }
//                }
//                tables = null;
//            }
//            pinCodeList = null;
//        }
//        ApiResult<List<AxWOTPinViewLine>> pinCodeDtls = null;
//        if(woTVM.getAxReferenceId() != null && woTVM.getAxReferenceId().trim().length() > 0){
//            try {
//                pinCodeDtls =null;// txnSrv.apiWotViewPinLine(WingOfTimeChannel.CHANNEL, woTVM.getAxReferenceId());
//            } catch (AxChannelException e) {
//                errorMsg = e.getMessage();
//                e.printStackTrace();
//            } catch (ParseException e) {
//                errorMsg = e.getMessage();
//                e.printStackTrace();
//            }
//        }
//        if(pinCodeDtls != null && pinCodeDtls.getData() != null && pinCodeDtls.getData().size() > 0){
//            List<AxWOTPinViewLine> lines = pinCodeDtls.getData();
//            for(AxWOTPinViewLine line : lines){
//                if(wot.getEventGroupId() != null && wot.getEventGroupId().equalsIgnoreCase(line.getEventGroupId()) &&
//                        wot.getEventLineId() != null && wot.getEventLineId().equalsIgnoreCase(line.getEventLineId()) &&
//                        wot.getItemId() != null && wot.getItemId().equalsIgnoreCase(line.getItemId())
//                /***
//                 && wot.getAxrtConfirmationId() != null && wot.getAxrtConfirmationId().equalsIgnoreCase(line.getSalesId())
//                 && wot.getEventDate() != null && NvxDateUtils.formatDateForDisplay(wot.getEventDate(),false).equalsIgnoreCase(NvxDateUtils.formatDateForDisplay(line.getEventDate(),false))
//                 ***/
//                        ){
//                    woTVM.setAxInventTransId(line.getInventTransId());
//                    woTVM.setQty(line.getQty());
//                    woTVM.setQtyRedeemed(line.getQtyRedeemed());
//                    woTVM.setQtyReturned(line.getQtyReturned());
//                }
//            }
//        }
//        apiResult.setData(woTVM);
//        apiResult.setSuccess(true);
//        if(errorMsg != null && errorMsg.trim().length() > 0){
//            apiResult.setMessage(errorMsg);
//            apiResult.setSuccess(false);
//        }
//        return apiResult;
//    }
}
