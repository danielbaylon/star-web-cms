package com.enovax.star.cms.api.kioskprint.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cornelius on 15/6/16.
 */
public class PrintTicketResult implements Serializable {

    private Boolean success;
    private String errorCode;
    private String errorMessage;
    private Boolean printFinished;
    private List<TicketStatus> printHistory = new ArrayList<>();

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<TicketStatus> getPrintHistory() {
        return printHistory;
    }

    public void setPrintHistory(List<TicketStatus> printHistory) {
        this.printHistory = printHistory;
    }

    public Boolean getPrintFinished() {
        return printFinished;
    }

    public void setPrintFinished(Boolean printFinished) {
        this.printFinished = printFinished;
    }

    public boolean hasErrors() {

        for (TicketStatus ticketStatus : printHistory) {
            if (ticketStatus.getSuccess() == false)
                return true;
        }

        return false;
    }

    public String getErrorMessages() {

        StringBuilder errorMessages = new StringBuilder();

        for(TicketStatus ticketStatus : printHistory) {

            if (ticketStatus.getSuccess() == false) {

                List<String> errors = ticketStatus.getStatusCodes();

                for (String errorMessage : errors) {
                    errorMessages.append("- " + errorMessage);
                    errorMessages.append("\n");
                }
            }
        }

        return errorMessages.toString();
    }
}