package com.enovax.star.cms.api.store.service.b2c;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.promotions.ApplyAffiliationRequest;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.tracking.Trackd;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface IBookingService {

  ApiResult<FunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId);

  ApiResult<FunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, FunCart topupCart);

  ApiResult<FunCartDisplay> cartUpdate(StoreApiChannels channel, FunCart cartToUpdate, String sessionId);

    ApiResult<String> cartApplyAffiliation(StoreApiChannels channel, String sessionId, ApplyAffiliationRequest applyAffiliationRequest);

    ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode);

  ApiResult<List<AxProduct>> checkPromoCode(StoreApiChannels channel, String promoCode);

  ApiResult<FunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId);

  boolean cartClear(StoreApiChannels channel, String sessionId);

  CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session);

  ApiResult<StoreTransaction> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets, String locale);

  ApiResult<StoreTransaction> doCheckout(StoreApiChannels channel, FunCart cart, CustomerDetails deets, String locale);

  ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session);

  StoreTransaction getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber);

  StoreTransaction getStoreTransactionBySession(StoreApiChannels channel, HttpSession session);

  ReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session);

    ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd);

    ApiResult<StoreTransaction> getFinalisedTransaction(StoreApiChannels channel, HttpSession session);
}
