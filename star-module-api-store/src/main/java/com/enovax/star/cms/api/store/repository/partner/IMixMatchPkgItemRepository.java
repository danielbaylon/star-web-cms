package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgItemVM;

import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
public interface IMixMatchPkgItemRepository {
    void save(MixMatchPkgItemVM mixMatchPkgItemVM);
    List<MixMatchPkgItemVM> getPkgItemsByTransItemIds(Integer[] transIdList);
    List<MixMatchPkgItemVM> getPkgItemsByPkgId(Integer packageId);
    Integer getNextId();
}
