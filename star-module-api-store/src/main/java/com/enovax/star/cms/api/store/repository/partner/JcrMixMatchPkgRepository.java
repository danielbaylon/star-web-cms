package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.constant.ppslm.PkgStatus;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
@Repository
public class JcrMixMatchPkgRepository implements  IMixMatchPkgRepository {
    @Override
    public void save(MixMatchPkgVM mixMatchPkgVM) {
//        int packageId = getNextMixMatchPackageId();
//        try {
//            mixMatchPkgVM.setId(packageId);
//            Node mixMatchPackageNode = JcrRepository.createNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "/", packageId + "", "mgnl:content");
//
//            List<MixMatchPkgItemVM> itemList = mixMatchPkgVM.getTransItems();
//
//            for(MixMatchPkgItemVM item: itemList) {
//                try {
//                    int itemId = getNextMixMatchPackageItemId();
//                    item.setId(itemId);
//                    item.setPackageId(packageId);
//                    Node mixMatchPackageItemNode = JcrRepository.createNode(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), "/", itemId + "", "mgnl:content");
//                    mixMatchPackageItemNode.setProperty("data", JsonUtil.jsonify(item));
//                    mixMatchPackageItemNode.setProperty("id", itemId + "");
//                    mixMatchPackageItemNode.setProperty("packageId", packageId + "");
//                    JcrRepository.updateNode(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), mixMatchPackageItemNode);
//
//                } catch (RepositoryException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            mixMatchPackageNode.setProperty("data", JsonUtil.jsonify(mixMatchPkgVM));
//            mixMatchPackageNode.setProperty("id", packageId + "");
//            mixMatchPackageNode.setProperty("name", mixMatchPkgVM.getName());
//            mixMatchPackageNode.setProperty("status", mixMatchPkgVM.getStatus());
//            mixMatchPackageNode.setProperty("mainAccountId", mixMatchPkgVM.getMainAccountId() + "");
//
//            JcrRepository.updateNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), mixMatchPackageNode);
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void update(MixMatchPkgVM mixMatchPkgVM) {
        try {
            Node mixMatchNode =  JcrRepository.getParentNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "/" + mixMatchPkgVM.getId());
            mixMatchNode.setProperty("data", JsonUtil.jsonify(mixMatchPkgVM));
            mixMatchNode.setProperty("status", mixMatchPkgVM.getStatus());
            JcrRepository.updateNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), mixMatchNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removePkg(int id) {
        try {
            Node mixMatchNode =  JcrRepository.getParentNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "/" + id);
            MixMatchPkgVM mixMatchPkgVM = JsonUtil.fromJson(mixMatchNode.getProperty("data").getString(), MixMatchPkgVM.class);

            if(PkgStatus.Reserved.toString().equals(mixMatchPkgVM.getStatus())) {
                mixMatchPkgVM.setStatus(PkgStatus.Failed.toString());
                mixMatchNode.setProperty("data", JsonUtil.jsonify(mixMatchPkgVM));
                JcrRepository.updateNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), mixMatchNode);
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }

    @Override
    public MixMatchPkgVM getMixMatchPackage(int id) {
        try {
            Node mixMatchNode =  JcrRepository.getParentNode(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "/" + id);
            MixMatchPkgVM mixMatchPkgVM = JsonUtil.fromJson(mixMatchNode.getProperty("data").getString(), MixMatchPkgVM.class);
            if(NvxDateUtils.clearTime(new Date()).compareTo(mixMatchPkgVM.getExpiryDate()) > 0) {
                mixMatchPkgVM.setStatus(TicketStatus.Expired.toString());
            }
            return mixMatchPkgVM;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getNextMixMatchPackageId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/mix-match-packages");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    @Override
    public Integer getNextMixMatchPackageItemId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/mix-match-package-items");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    @Override
    public List<MixMatchPkgVM> getPkgsByPage(Integer adminId, Date fromDate, Date toDate, String status, String pkgNm, String orderField, String orderWith, Integer pageNumber, Integer pageSize) {
        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "' and @status = 'Available']"; //TODO the status should be more
            List<MixMatchPkgVM> pkgList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                MixMatchPkgVM pkg = JsonUtil.fromJson(itemNode.getProperty("data").getString(), MixMatchPkgVM.class);
                if(NvxDateUtils.clearTime(new Date()).compareTo(pkg.getExpiryDate()) > 0) {
                    pkg.setStatus(TicketStatus.Expired.toString());
                }
                pkgList.add(pkg);
            }

            List<MixMatchPkgVM> pagedPkgList = new ArrayList<>();
            //filter pag page also
            if(pageNumber > 0 && pageSize > 0) {
                for(int i = (pageNumber - 1) * pageSize; pagedPkgList.size() < pageSize && i < pkgList.size(); i++) {
                    pagedPkgList.add(pkgList.get(i));
                }
            }else {
                return pkgList;
            }

            return pagedPkgList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getPkgsSize(Integer adminId, Date fromDate, Date toDate, String status, String pkgNm) {
        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "' and @status = 'Available']"; //TODO the status should be more
            List<MixMatchPkgVM> pkgList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.MixMatchPackages.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                MixMatchPkgVM pkg = JsonUtil.fromJson(itemNode.getProperty("data").getString(), MixMatchPkgVM.class);
                pkgList.add(pkg);
            }

            return pkgList.size();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
