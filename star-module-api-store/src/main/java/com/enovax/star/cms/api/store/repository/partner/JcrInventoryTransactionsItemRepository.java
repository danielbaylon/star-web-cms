package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionItemVM;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
@Repository
public class JcrInventoryTransactionsItemRepository implements IInventoryTransactionsItemRepository{

    @Override
    public List<InventoryTransactionItemVM> getInventoryTransactionsItem
            (Integer adminId, Date fromDate, Date toDate, boolean showAvailable, String prodNm,
             String orderBy, String orderWith, Integer pageNumber, Integer pageSize) {

        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "']";
            List<InventoryTransactionItemVM> inventoryItemList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                InventoryTransactionItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), InventoryTransactionItemVM.class);

                //get the status
                Node transNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/" + item.getTransId());
                InventoryTransactionVM trans = JsonUtil.fromJson(transNode.getProperty("data").getString(), InventoryTransactionVM.class);
                item.setStatus(item.getExpiringStatus(trans));

                if(TicketStatus.Expired.toString().equals(item.getStatus())) {
                    item.setAbleToPkg(false);
                }else {
                    item.setAbleToPkg(item.checkItemAvailable());
                }

                inventoryItemList.add(item);
            }

            //only get those with status for main transaction as "Available"


            List<InventoryTransactionItemVM> pagedInventoryItemList = new ArrayList<>();
            //filter pag page also
            if(pageNumber > 0 && pageSize > 0) {
                for(int i = (pageNumber - 1) * pageSize; pagedInventoryItemList.size() < pageSize && i < inventoryItemList.size(); i++) {
                    pagedInventoryItemList.add(inventoryItemList.get(i));
                }
            }else {
                return inventoryItemList;
            }

            return pagedInventoryItemList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int getInventoryTransactionsItemSize(Integer adminId, Date fromDate, Date toDate, boolean showAvailable, String prodNm) {
        try {
            //TODO ADD MORE query here

            String query = "[@mainAccountId = '" + adminId + "']";
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            int cnt = 0;
            while(nodeIterator.hasNext()) {
                nodeIterator.next();
                cnt++;
            }

            return cnt;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public List<InventoryTransactionItemVM> getTransItemsByTransIds(Integer[] tranIds) {
        try {

            List<InventoryTransactionItemVM> inventoryItemList = new ArrayList<>();
            String query = "";

            for(int id: tranIds) {
                if(StringUtils.isBlank(query)) {
                    query = query + "[";
                }else {
                    query = query + " or ";
                }
                query = query + "@id='" + id + "'";
            }

            query = query + "]";

            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                InventoryTransactionItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), InventoryTransactionItemVM.class);

                //get the status
                Node transNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/" + item.getTransId());
                InventoryTransactionVM trans = JsonUtil.fromJson(transNode.getProperty("data").getString(), InventoryTransactionVM.class);
                item.setStatus(item.getExpiringStatus(trans));

                if(TicketStatus.Expired.toString().equals(item.getStatus())) {
                    item.setAbleToPkg(false);
                }else {
                    item.setAbleToPkg(item.checkItemAvailable());
                }

                inventoryItemList.add(item);
            }

            return inventoryItemList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<InventoryTransactionItemVM> getTransItemsByTransId(Integer transId) {
        try {
            List<InventoryTransactionItemVM> inventoryItemList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)[@transId='" + transId + "']");
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                InventoryTransactionItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), InventoryTransactionItemVM.class);
                //get the status
                Node transNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/" + item.getTransId());
                InventoryTransactionVM trans = JsonUtil.fromJson(transNode.getProperty("data").getString(), InventoryTransactionVM.class);
                item.setStatus(item.getExpiringStatus(trans));

                if(TicketStatus.Expired.toString().equals(item.getStatus())) {
                    item.setAbleToPkg(false);
                }else {
                    item.setAbleToPkg(item.checkItemAvailable());
                }

                inventoryItemList.add(item);
            }

            return inventoryItemList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public InventoryTransactionItemVM getTransItemById(Integer id) {
        try {

            Node itemNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "/" + id);
            InventoryTransactionItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), InventoryTransactionItemVM.class);
            return item;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void updateTransItem(InventoryTransactionItemVM item) {
        try {

            Node itemNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "/" + item.getId());
            itemNode.setProperty("data", JsonUtil.jsonify(item));
            itemNode.setProperty("unpackedQty", item.getUnpackagedQty() + "");
            itemNode.setProperty("validityEndDate", item.getValidityEndDate().getTime() + "");
            JcrRepository.updateNode(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), itemNode);

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
}
