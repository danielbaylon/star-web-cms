package com.enovax.star.cms.api.store.controller;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.tracking.Trackr;
import com.enovax.star.cms.commons.util.ProjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public abstract class BaseStoreApiController {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    protected Trackr trackr;

    public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";

    protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<List<K>> handleUncaughtExceptionForList(Throwable t, Class<K> innerListClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K, V> ApiResult<Map<K, V>> handleUncaughtExceptionForMap(
            Throwable t, Class<K> innerMapKey, Class<V> innerMapValue, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<K> handleException(String methodName, Throwable t, Class<K> resultClass) {
        if(t != null){
            log.error("!!! System exception encountered ["+methodName+"] !!!", t);
        }else{
            log.error("!!! System exception encountered ["+methodName+"] !!!");
        }
        if(t instanceof BizValidationException){
            BizValidationException be = (BizValidationException) t;
            if(be.getErrorMsg() != null && be.getErrorMsg().trim().length() > 0){
                return new ApiResult<>(false, "", be.getErrorMsg(), null);
            }
        }
        return handleUncaughtException(t, resultClass, null);
    }
}
