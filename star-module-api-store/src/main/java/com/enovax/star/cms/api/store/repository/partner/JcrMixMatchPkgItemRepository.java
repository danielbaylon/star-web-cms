package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgItemVM;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
@Repository
public class JcrMixMatchPkgItemRepository implements IMixMatchPkgItemRepository {

    @Override
    public void save(MixMatchPkgItemVM mixMatchPkgItemVM) {
        int id = getNextId();
        try {
            mixMatchPkgItemVM.setId(id);
            Node mixMatchPackageItemNode = JcrRepository.createNode(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), "/", id + "", "mgnl:content");
            mixMatchPackageItemNode.setProperty("data", JsonUtil.jsonify(mixMatchPkgItemVM));
            mixMatchPackageItemNode.setProperty("id", id + "");
            mixMatchPackageItemNode.setProperty("packageId", mixMatchPkgItemVM.getPackageId() + "");
            mixMatchPackageItemNode.setProperty("transactionItemId", mixMatchPkgItemVM.getTransactionItemId() + "");



            JcrRepository.updateNode(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), mixMatchPackageItemNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<MixMatchPkgItemVM> getPkgItemsByTransItemIds(Integer[] transIdList) {
        try {
            List<MixMatchPkgItemVM> mixMatchPkgItemList = new ArrayList<>();
            String query = "";

            for(int id: transIdList) {
                if(StringUtils.isBlank(query)) {
                    query = query + "[";
                }else {
                    query = query + " or ";
                }
                query = query + "@transactionItemId='" + id + "'";
            }

            query = query + "]";

            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                MixMatchPkgItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), MixMatchPkgItemVM.class);
                mixMatchPkgItemList.add(item);
            }

            return mixMatchPkgItemList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<MixMatchPkgItemVM> getPkgItemsByPkgId(Integer packageId) {
        try {
            List<MixMatchPkgItemVM> mixMatchPkgItemList = new ArrayList<>();
            String query = "[@packageId = '" + packageId + "']";

            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.MixMatchPackageItems.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node itemNode = nodeIterator.next();
                MixMatchPkgItemVM item = JsonUtil.fromJson(itemNode.getProperty("data").getString(), MixMatchPkgItemVM.class);
                mixMatchPkgItemList.add(item);
            }

            return mixMatchPkgItemList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/mix-match-package-items");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }
}
