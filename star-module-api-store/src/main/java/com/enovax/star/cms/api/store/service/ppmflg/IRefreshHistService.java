package com.enovax.star.cms.api.store.service.ppmflg;

/**
 * Created by houtao on 20/8/16.
 */
public interface IRefreshHistService {

    public String getRefreshHist(String key1);

    public String getRefreshHist(String key1, String key2);

    public String getRefreshHist(String key1, String key2, String key3);

    public void storeRefreshHist(String key1, String key2, String key3, String value);

}
