package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.processor.TmSaleProcessor;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAxCheckoutCart;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTelemoneyLog;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.AxStarCart;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.TelemoneyProcessingDataPartnerInventoryPurchase;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMAxCheckoutCartRepository;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerBookingService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerPurchaseTransactionService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerSharedAccountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 11/15/16.
 */
@SuppressWarnings("Duplicates")
@Component
@Scope("prototype")
public class DefaultTelemoneyInventoryPurchaseProcessor extends TmSaleProcessor<TelemoneyProcessingDataPartnerInventoryPurchase>  {

    @Autowired
    private ILogService logService;
    @Autowired
    private IPartnerPurchaseTransactionService purchaseTransactionService;
    @Autowired
    private IPartnerSharedAccountService accSrv;
    @Autowired
    private IPartnerBookingService bookingService;
    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private PPSLMAxCheckoutCartRepository checkoutCartRepo;
    @Autowired
    private AxChannelTransactionService axChannelTransactionService;

    @Transactional
    public void process(TelemoneyResponse tmResponse) {
        log.info("Start processing Telemoney POST for Partner Portal SLM - Inventory Purchase Flow...");
        log.info("Logging Telemoney Response...");
        log.info("[Telemoney Response] => " + JsonUtil.jsonify(tmResponse));

        boolean isFromQuery = false;

        String payment = TmPaymentType.getTitle(tmResponse.getTmPaymentType());
        log.debug("initialize payment: " + payment);

        final String tmDataReceiptNumber = tmResponse.getTmRefNo();
        final String tmDataChannel = tmResponse.getTmUserField1();
        final String tmDataSessionId = tmResponse.getTmUserField3();
        final String tmStatus = tmResponse.getTmStatus();
        final String tmMerchantId = tmResponse.getTmMerchantId();

        PPSLMInventoryTransaction invTrans = purchaseTransactionService.getTransaction(tmDataReceiptNumber, true);
        invTrans.setPaymentType(payment);

        this.logService.logInventoryTransactionDetails(log, "ProcessTMStatusAction :: Retrieved Inventory Transaction (RefNo)", invTrans);
        isFromQuery = isTMStatusInitiatedByQuery(invTrans);
        log.info("Creating Telemoney object...");
        final PPSLMTelemoneyLog telemoneyLog = logService.createTelemoneyObject(isFromQuery, tmResponse);
        log.info("Logging TM object and transaction.");
        this.logService.logTelemoneyDetails(log, "ProcessTMStatusAction :: TM Parameters Accepted", telemoneyLog);
        final boolean tmDbSaved = this.logService.logTelemoneyDetailsToDB(telemoneyLog);
        if (!tmDbSaved) {
            log.warn("Telemoney details were not saved to the database.");
        }

        log.info("Checking transaction existence.");
        if (invTrans == null) {
            log.error("Retrieved transaction is null.");
            logService.processError(ErrorTypes.TKT_INVALID_TRANS.cd,
                    "ProcessTMStatusAction.execute-checkTransNull", null, true);
            return;
        }

        log.info("Checking transaction status.");
        if (!TicketStatus.Reserved.toString().equals(invTrans.getStatus())) {
            log.error("Transaction status has already been set.");
            logService.processError(ErrorTypes.TKT_STATUS_SET.cd,
                    "ProcessTMStatusAction.execute-checkStatusSet", invTrans, true);
            return;
        }

        log.info("Checking transaction TM Merchant.");
        if (TmStatus.Success.equals(tmStatus) && !invTrans.getTmMerchantId().equals(tmMerchantId)) {
            log.error("Transaction's merchant code did not correspond to TM's code.");
            logService.processError(ErrorTypes.TKT_WRONG_TMMID.cd,
                    "ProcessTMStatusAction.execute-checkTMmid", invTrans, true);
            return;
        }

        //We purposely only handle SALE type requests here. VOID type requests are handled in a different processing flow.
        final String tmTransType = tmResponse.getTmTransType();
        if (!TmTransType.Sale.tmCode.equalsIgnoreCase(tmTransType)) {
            log.info("Transaction was not of SALE type. Processing will not continue from here.");
            return;
        }

        //TODO maybe dont need to pass the channel
        StoreApiChannels channel;
        try {

            channel = StoreApiChannels.fromCode(tmDataChannel);
        } catch (Exception e) {
            log.error("Unable to find a valid channel for this response. EX: " + e.getMessage(), e);
            return;
        }

        final TelemoneyProcessingDataPartnerInventoryPurchase data = new TelemoneyProcessingDataPartnerInventoryPurchase();
        data.setChannel(channel);
        data.setReceiptNumber(tmDataReceiptNumber);
        data.setSlm(true);
        data.setMainAccount(accSrv.getMainUser(invTrans.getUsername(), false, false));
        data.setIsSubAccount(invTrans.isSubAccountTrans());
        data.setSubAccount(data.isSubAccount() ? accSrv.getSubUser(invTrans.getUsername(), false, false) : null);
        data.setEmail(data.isSubAccount() ? data.getSubAccount().getEmail() : data.getMainAccount().getEmail());
        data.setSlmTxn(invTrans);

        try {

            processTelemoneySalePost(tmResponse, data);
        } catch (Exception e) {
            log.error("General exception encountered while processing...", e);
        }
    }

    private boolean isTMStatusInitiatedByQuery(PPSLMInventoryTransaction trans) {
        if (trans != null && EnovaxTmSystemStatus.TmQuerySent.toString().equals(trans.getTmStatus())) {
            return true;
        }
        return false;
    }


    @Override
    protected void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerInventoryPurchase data) {
        log.info("Telemoney response is SUCCESSFUL for the transaction. Processing...");
        final BigDecimal tmDebitAmt = tmResponse.getTmDebitAmt().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        final BigDecimal transAmount = data.getSlmTxn().getTotalAmount().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);

        if (!transAmount.equals(tmDebitAmt)) {
            final String errMsg = "TM debit amount of " + tmDebitAmt + " is different from the transaction amount of " + transAmount;
            log.error(errMsg);
            _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.Failed.toString(), "TMIncorrectAmount", errMsg, TicketStatus.Incomplete.toString());
        } else {
            log.info("Amount validated.");
            _processSuccessfulTransaction(tmResponse, data);
        }
    }

    @Override
    protected void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerInventoryPurchase data) {
        log.info("Telemoney response is NOT FOUND for the transaction. Processing...");
        _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.NA.toString(), "TelemoneyNotFound", "Transaction not found at Telemoney.", TicketStatus.Incomplete.toString());
    }

    @Override
    protected void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerInventoryPurchase data) {
        log.info("Telemoney response is FAILED for the transaction. Processing...");

        final String tmErr = tmResponse.getTmError();
        final String tmErrMsg = tmResponse.getTmErrorMessage();
        _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.Failed.toString(),
                StringUtils.isEmpty(tmErr) ? "FAIL" : tmErr,
                StringUtils.isEmpty(tmErrMsg) ? "Telemoney returned Fail status" : tmErrMsg, TicketStatus.Failed.toString());
    }

    @Override
    protected void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerInventoryPurchase data) {
        log.info("Telemoney {} has indicated a REDIRECT status for the current transaction. " +
                "Transaction {} is still pending at a third-party site.", tmResponse.getTmXid(), data.getSlmTxn().getReceiptNum());
        //Do nothing, and wait for the next response.
    }

    private void _processSuccessfulTransaction(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerInventoryPurchase data) {
        PPSLMInventoryTransaction trans = data.getSlmTxn();

        log.info("Updating transaction with successful result.");
        trans.setTmStatus(EnovaxTmSystemStatus.Success.toString());
        trans.setTmApprovalCode(tmResponse.getTmApprovalCode());
        trans.setMaskedCc(tmResponse.getTmCcNum());
        trans.setTmStatusDate(new Date());

        _injectTs(trans);

        this.logService.logInventoryTransactionDetails(log, "TicketingTmEvent :: Successful Transaction", trans);

        boolean saved = true;
        try {
            purchaseTransactionService.saveTransaction(trans);
        } catch (Exception e) {
            log.error("Error saving transaction on success.", e);
            saved = false;
        }

        if (!saved) {
            log.info("Transaction was not saved to the database. Once in a blue moon lah.");
            _voidTransaction(data, ErrorTypes.TKT_DBFAILED_VOID1, ErrorTypes.TKT_DBFAILED_VOID1, "");
        } else {
            boolean confirmed = false;
            try {
                confirmed = _confirmSale(data);
            } catch (Exception e) {
                log.error("Transaction cannot be confirmed after tmsuccess! Receipt no.: " + trans.getReceiptNum());
                ErrorTypes etVoidSuccess = ErrorTypes.TKT_CONF0_VOID1;
                ErrorTypes etVoidFailed = ErrorTypes.TKT_CONF0_VOID0;
                _voidTransaction(data, etVoidSuccess, etVoidFailed, e.getMessage());
            }
            if (confirmed) {
                _completeSuccessfulTrans(data);
            }
        }
    }


    private void _processUnsuccessfulTrans(TelemoneyProcessingDataPartnerInventoryPurchase data, String tmStatus, String errorCode, String errorMessage, String status) {
        PPSLMInventoryTransaction trans = data.getSlmTxn();
        trans.setTmStatus(tmStatus);
        trans.setTmErrorMessage(errorMessage);
        trans.setTmErrorCode(errorCode);
        _injectTs(trans);
        trans.setStatus(status);
        boolean saveSuccess = this.purchaseTransactionService.saveTransaction(trans);
        log.info(saveSuccess ? "Successfully saved status=na/failed transaction in DB." :
                "Failed to saved status=na/failed transaction in DB.");
        _releaseTransaction(data, ErrorTypes.TM_STATUSFAIL_REL1, ErrorTypes.TM_STATUSFAIL_REL0);
    }

    private void _injectTs(PPSLMInventoryTransaction theTrans) {
        theTrans.setModifiedDate(new Date());
    }



    private boolean _confirmSale(TelemoneyProcessingDataPartnerInventoryPurchase data) {
        PPSLMInventoryTransaction trans = data.getSlmTxn();
        StringBuilder sbError = new StringBuilder();

        //TODO create sales order
        log.info("Create Sales Order...");
        boolean success = false;

        PPSLMAxCheckoutCart axCheckoutCart = checkoutCartRepo.findByReceiptNum(trans.getReceiptNum());
        //get the AxCartCheckout

        AxStarCart axStarCart = JsonUtil.fromJson(axCheckoutCart.getCheckoutCartJson(), AxStarCart.class);

        ApiResult<InventoryTransactionVM> salesOrderResult = bookingService.createSalesOrder(data.getChannel(), data.getSlmTxn(), data.getEmail(), axStarCart);

        if (salesOrderResult.isSuccess()) {
            log.info("Transaction {} has been confirmed. Processing continues...", trans.getReceiptNum());
            return true;
        }else {
            log.error("Error encountered in creating the sales order...");
            success = false;
        }

        ErrorTypes etVoidSuccess = ErrorTypes.TKT_CONF0_VOID1;
        ErrorTypes etVoidFailed = ErrorTypes.TKT_CONF0_VOID0;

        log.error("Transaction cannot be confirmed! Receipt no.: " + trans.getReceiptNum());
        _voidTransaction(data, etVoidSuccess, etVoidFailed, sbError.toString());
        _releaseTransaction(data, ErrorTypes.TKT_CONF0_REL1, ErrorTypes.TKT_CONF0_REL0);

        return false;
    }


    private void _completeSuccessfulTrans(TelemoneyProcessingDataPartnerInventoryPurchase data) {
        log.info("Completing successful transaction...");
        PPSLMInventoryTransaction trans = data.getSlmTxn();
        trans.setStatus(TicketStatus.Available.toString());
        _injectTs(trans);
        trans.setReprintCount(0);
        this.purchaseTransactionService.saveTransaction(trans);

        try {
            log.info("Generating and sending receipt PDF + email.");
            boolean emailSuccess = this.purchaseTransactionService.generateAndEmailPurchaseReceipt(trans);
            if (!emailSuccess) {
                log.warn("Email was not sent for transaction " + trans.getReceiptNum() + " with email " + data.getEmail());
            }
            this.logService.logInventoryTransactionDetails(log, "TicketingTmEvent :: SUCCESSFUL RECEIPT SENT", trans);
        } catch (Exception e) {
            //TODO eval if need to void or what
            log.error("Error occurred during receipt generation. Performing error steps. Receipt no.: " + trans.getReceiptNum(), e);
//            ErrorTypes etVoidSuccess = ErrorTypes.TKT_RECGEN_GENERAL_VOID1;
//            ErrorTypes etVoidFailed = ErrorTypes.TKT_RECGEN_GENERAL_VOID0;
//            String extraErrMsg = "";
//
//            try {
//                log.info("Voiding transaction....");
//                //TODO we dont have this eh, double check if there is
//            } catch (Exception ae) {
//                log.error("Error encountered for void recur during receipt gen!!", ae);
//                etVoidSuccess = ErrorTypes.TKT_RECGEN_GENERAL_VOID1_SALE0;
//                etVoidFailed = ErrorTypes.TKT_RECGEN_GENERAL_VOID0_SALE0;
//            }
//
//            _voidTransaction(data, etVoidSuccess, etVoidFailed, extraErrMsg);
        }
    }

    private boolean _releaseTransaction(TelemoneyProcessingDataPartnerInventoryPurchase data, ErrorTypes errReleaseSuccess, ErrorTypes errReleaseFail) {
        PPSLMInventoryTransaction trans = data.getSlmTxn();
        boolean released = true;
        String releaseError = "";
        try {

            this.purchaseTransactionService.processTransactionReleaseReservation(trans);

            //rebuild the cart
            PPSLMAxCheckoutCart axCheckoutCart = checkoutCartRepo.findByReceiptNum(trans.getReceiptNum());

            FunCart cart = JsonUtil.fromJson(axCheckoutCart.getSessionCartJson(), FunCart.class);


            final List<AxRetailCartTicket> cartTickets = cart.getAxCart().getReservedTickets();

            if(cartTickets!= null && cartTickets.size() > 0) {
                for(AxRetailCartTicket axRetailCartTicket: cartTickets) {
                    axRetailCartTicket.setQty(0);
                    axRetailCartTicket.setUpdateCapacity(1);
                }

                final ApiResult<List<AxRetailTicketRecord>> result;
                try {
                    result = axChannelTransactionService.apiCartValidateAndReserveQuantity(data.getChannel(), trans.getReceiptNum(), cartTickets, cart.getAxCustomerId());
                    if (!result.isSuccess()) {
                        log.error("Failed to perform validate and reserve quantity at AX Retail service. (qty to set to 0)");
                        released = false;
                    }
                } catch (AxChannelException e) {
                    released = false;
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            log.error("Error occurred while trying to process transaction reservation release.", e);
            releaseError = e.getMessage();
            released = false;
        }

        this.logService.logInventoryTransactionDetails(log, "TicketingTmEvent :: Failed Transaction", trans);
        final String errCode = released ? errReleaseSuccess.cd : errReleaseFail.cd;
        final String errMsg = released ? errReleaseSuccess.msg : errReleaseFail.msg + " | Specific error: " + releaseError;
        this.logService.processError(errCode, "TicketingTmEvent.releaseTransaction", errMsg, trans, false);

        return released;
    }

    private boolean _voidTransaction(TelemoneyProcessingDataPartnerInventoryPurchase data, ErrorTypes errCodeVoided, ErrorTypes errCodeUnvoided, String extraErrorMsg) {
        log.info("Voiding transaction with Telemoney.");

        PPSLMInventoryTransaction trans = data.getSlmTxn();
        final TmServiceResult voidResult = this.tmServiceProvider.sendVoidWithRetry(
                trans.getTmMerchantId(), TmCurrency.valueOf(trans.getCurrency()), trans.getTotalAmount(), trans.getReceiptNum(), null);

        final TelemoneyResponse response = voidResult.getTmResponse();
        final String tmErrMsg = response==null?"FAIL":response.getTmErrorMessage();
        final String tmErr = response==null?"Null void result. Failed voiding.": response.getTmError();

        if (voidResult.isSuccess() && TmStatus.Success.equals(response.getTmStatus())) {

            log.info("Transaction successfully voided. Updating transaction in Database.");
            String errorMessage = (StringUtils.isEmpty(tmErrMsg) ? errCodeVoided.msg : tmErrMsg) +
                    (StringUtils.isEmpty(extraErrorMsg) ? "" : extraErrorMsg);
            String errorCode = StringUtils.isEmpty(tmErr) ? errCodeVoided.cd : tmErr;

            trans.setTmStatus(EnovaxTmSystemStatus.VoidSuccess.toString());
            trans.setTmErrorCode(errorCode);
            trans.setTmErrorMessage((StringUtils.isEmpty(errorMessage) ? "Voided transaction, blank error message" : errorMessage));
            trans.setStatus(TicketStatus.Failed.toString());

            _injectTs(trans);

            this.purchaseTransactionService.saveTransaction(trans);

            this.purchaseTransactionService.sendVoidEmail(true, trans, data.getEmail(), data.isSubAccount(), data.getSubAccount(), data.getMainAccount());

            this.logService.logInventoryTransactionDetails(log, "TicketingTmEvent :: Voided Transaction (Success)", trans);
            this.logService.processError(errCodeVoided.cd, "TicketingTmEvent._voidTransaction", errorMessage, trans, true);
            return true;

        } else {

            log.info("Transaction voiding failed. Updating transaction in Database.");
            String errorCode = tmErr;
            errorCode = StringUtils.isEmpty(errorCode) ? tmErr : errorCode;
            String errorMsg = tmErrMsg;
            errorMsg = (StringUtils.isEmpty(errorCode) ? tmErrMsg : errorMsg) +
                    (StringUtils.isEmpty(extraErrorMsg) ? "" : extraErrorMsg);

            trans.setTmStatus(EnovaxTmSystemStatus.VoidFailed.toString());
            trans.setTmErrorMessage(errorMsg);
            trans.setTmErrorCode(errorCode);
            trans.setStatus(TicketStatus.Failed.toString());
            _injectTs(trans);

            this.purchaseTransactionService.saveTransaction(trans);

            this.purchaseTransactionService.sendVoidEmail(true, trans, data.getEmail(), data.isSubAccount(), data.getSubAccount(), data.getMainAccount());

            this.logService.logInventoryTransactionDetails(log, "TicketingTmEvent :: Voided Transaction (Failed)", trans);
            this.logService.processError(errCodeUnvoided.cd, "TicketingTmEvent._voidTransaction",
                    errorMsg, trans, true);
            return false;

        }
    }
}
