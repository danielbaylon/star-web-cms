package com.enovax.star.cms.api.store.transformer.axretail.crosssell;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellLine;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellProduct;
import com.enovax.star.cms.commons.ws.axretail.crosssell.*;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
@Deprecated
public class AxRetailUpCrossSellResponseTransformer {

    public static ApiResult<List<AxRetailUpCrossSellProduct>> fromWs(SDCGetUpCrossSellResponse wsResponse) {
        if (wsResponse == null) {
            return new ApiResult<>();
        }

        final ArrayOfResponseError arrayOfResponseError = wsResponse.getErrors().getValue();
        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), null);
        }

        ArrayOfSDCUpCrossSellTable arrayOfSDCUpCrossSellTable = wsResponse.getUpCrossSellEntity().getValue();
        if (arrayOfSDCUpCrossSellTable == null) {
            return new ApiResult<>(true, "", "Successful but no records returned.", new ArrayList<>());
        }

        final List<SDCUpCrossSellTable> axRecords = arrayOfSDCUpCrossSellTable.getSDCUpCrossSellTable();
        final List<AxRetailUpCrossSellProduct> products = new ArrayList<>();
        for (SDCUpCrossSellTable axRecord : axRecords) {
            final AxRetailUpCrossSellProduct prod = new AxRetailUpCrossSellProduct();
            final List<AxRetailUpCrossSellLine> lines = new ArrayList<>();

            prod.setActive(axRecord.getACTIVE() == 1);
            prod.setChannel(axRecord.getCHANNEL());
            prod.setItemId(axRecord.getITEMID().getValue());
            prod.setMinimumQty(axRecord.getMINIMUMQTY().intValue());
            prod.setMultipleQty(axRecord.getMULTIPLEQTY().intValue());
            prod.setRecId(axRecord.getRECID());
            prod.setTableAll(axRecord.getTABLEALL());
            prod.setUnitId(axRecord.getUNITIDId().getValue());

            final ArrayOfSDCUpCrossSellLineTable arrayOfSDCUpCrossSellLineTable = axRecord.getUpCrossSellLines().getValue();
            if (arrayOfSDCUpCrossSellLineTable != null) {
                final List<SDCUpCrossSellLineTable> axRecordLines = arrayOfSDCUpCrossSellLineTable.getSDCUpCrossSellLineTable();
                if (axRecordLines != null) {
                    for (SDCUpCrossSellLineTable axRecordLine : axRecordLines) {
                        final AxRetailUpCrossSellLine line = new AxRetailUpCrossSellLine();
                        
                        line.setInventDimId(axRecordLine.getINVENTDIMID().getValue());
                        line.setOriginalItemId(axRecordLine.getORIGITEMID().getValue());
                        line.setQty(axRecordLine.getQTY().intValue());
                        line.setUnitId(axRecordLine.getUNITID().getValue());
                        line.setItemId(axRecordLine.getUPCROSSSELLITEM().getValue());
                        line.setTableRecId(axRecordLine.getUPCROSSSELLTABLERECID());
                        line.setUpsellType(axRecordLine.getUPSELLTYPE());
                        line.setVariantId(axRecordLine.getVARIANTID().getValue());

                        lines.add(line);
                    }
                }
            }

            prod.setLines(lines);

            products.add(prod);
        }

        return new ApiResult<>(true, "", "", products);
    }
}
