package com.enovax.star.cms.api.store.controller.ppslm.publicity;


import com.enovax.star.cms.api.store.controller.ppslm.PPSLMBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerRegistrationService;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerRegistrationVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerDocumentService;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

@Controller
@RequestMapping(PartnerPortalConst.ROOT_PUBLIC + "/partner-registration/")
public class PartnerRegistrationController extends PPSLMBaseStoreApiController {

    @Autowired
    private IPartnerRegistrationService partnerRegSrv;

    @Autowired
    private IPartnerDocumentService partnerDocSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @RequestMapping(value="params")
    public ResponseEntity<ApiResult<PartnerRegistrationVM>> apiGetPartnerRegistrationParam(){
        try{
            PartnerRegistrationVM result = partnerRegSrv.getPartnerRegistrationParam();
            return new ResponseEntity<>(new ApiResult(true, "", "", result), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("apiGetPartnerRegistrationParam", ex, PartnerRegistrationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value="check", method = RequestMethod.POST)
    public ResponseEntity<ApiResult<ResultVM>> apiCheckPartnerAccountCode(@RequestParam(name="code") String accountCode, @RequestParam(name="id")Integer id){
        try{
            if(accountCode == null || accountCode.trim().length() == 0){
                return new ResponseEntity<>(new ApiResult(false, "ERROR", "REG_CODE_INVALID", null), HttpStatus.OK);
            }
            if(partnerRegSrv.isAnExistingPartner(accountCode.trim(), id)){
                return new ResponseEntity<>(new ApiResult(false, "ERROR", "REG_CODE_IN_USE", null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult(true, "INFO", "REG_CODE_NOT_IN_USE", null), HttpStatus.OK);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("apiCheckPartnerAccountCode", ex, ResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value="register", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    public ResponseEntity<String> apiPartnerRegistration(HttpServletRequest request){
        try {
            if(sysParamSrv.isRecaptchaEnabled()){
                ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
                reCaptcha.setPrivateKey(sysParamSrv.getRecaptchaPrivateKey());
                ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(sysParamSrv.getUserRequestAddress(request), request.getParameter("recaptcha_challenge_field"), request.getParameter("recaptcha_response_field"));
                if(!reCaptchaResponse.isValid()) {
                    return new ResponseEntity<>(JsonUtil.jsonify(new ApiResult(false, "ERROR", "INVALID_VERIFICATION_CODE", null)), HttpStatus.OK);
                }
            }
            PartnerVM partnerVM = JsonUtil.fromJson(request.getParameter("partnerVmJson"), PartnerVM.class);
            if (partnerVM == null || partnerVM.getAccountCode() == null || partnerVM.getAccountCode().trim().length() == 0 || partnerRegSrv.isAnExistingPartner(partnerVM.getAccountCode(), partnerVM.getId())) {
                return new ResponseEntity<>(JsonUtil.jsonify(new ApiResult(false, "ERROR", "REG_CODE_IN_USE", null)), HttpStatus.OK);
            }
            MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
            Document doc1 = mpf.getDocument("file1");
            Document doc2 = mpf.getDocument("file2");
            String retCode = partnerRegSrv.registerPartner(partnerVM, doc1, doc2);
            if (retCode != null && retCode.trim().length() > 0) {
                return new ResponseEntity<>(JsonUtil.jsonify(new ApiResult(false, "ERROR", retCode, null)), HttpStatus.OK);
            }
            return new ResponseEntity<>(JsonUtil.jsonify(new ApiResult(true, "", "", null)), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(JsonUtil.jsonify(handleException("apiPartnerRegistration", ex, ResultVM.class)), HttpStatus.OK);
        }
    }

    @RequestMapping(value="details", method = RequestMethod.POST)
    public ResponseEntity<ApiResult<PartnerVM>> apiGetPartnerDetailsById(@RequestParam(name="code")String accCodeId){
        try{
            if(accCodeId == null || accCodeId.trim().length() == 0){
                return new ResponseEntity<>(new ApiResult(false, "ERROR", "INVALID_PARTNER_ID", null), HttpStatus.OK);
            }
            PartnerVM paVM = partnerRegSrv.getPartnerDetailsByPartnerId(accCodeId.trim());
            if(paVM == null){
                return new ResponseEntity<>(new ApiResult(false, "ERROR", "INVALID_PARTNER_ID", null), HttpStatus.OK);
            }
            if(!(paVM != null && PartnerStatus.PendingReSubmit.code.equalsIgnoreCase(paVM.getStatus()))){
                return new ResponseEntity<>(new ApiResult(false, "ERROR", "INVALID_PARTNER_RESUBMIT_REQUEST", paVM), HttpStatus.OK);
            }
            partnerRegSrv.populatePartnerRegistrationFormResubmitRemarks(paVM);
            return new ResponseEntity<>(new ApiResult(true, "", "", paVM), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("apiGetPartnerDetailsById", ex, PartnerVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value="attachment")
    public void apiDownloadPartnerFile(HttpServletRequest request, HttpServletResponse response, @RequestParam(name="id")Integer id){
        if(id != null && id.intValue() > 0){
            PPSLMPADocument doc = partnerDocSrv.getPartnerDocumentById(id);
            if(doc != null){
                String filePath = doc.getFilePath();
                if(filePath != null && filePath.trim().length() > 0){
                    String docRootDir = sysParamSrv.getPartnerDocumentRootDir();
                    String fileFullPath = docRootDir + File.separator + filePath;
                    File f = new File(fileFullPath);
                    if(f.exists() && f.canRead()){
                        f = null;
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition","attachment;filename=" + doc.getFileName());
                        InputStream is = null;
                        OutputStream os = null;
                        try{
                            is = new FileInputStream(fileFullPath);
                            int read=0;
                            byte[] bytes = new byte[500];
                            os = response.getOutputStream();
                            while((read = is.read(bytes))!= -1){
                                os.write(bytes, 0, read);
                            }
                            os.flush();
                        }catch(Exception ex){
                            log.error("!!! System exception encountered [apiDownloadPartnerFile] !!!");
                            ex.printStackTrace();
                        }finally{
                            if(is != null){
                                try{
                                    is.close();
                                    is = null;
                                }catch(Exception ex){
                                }
                            }
                            if(os != null){
                                os = null;
                            }
                        }
                    }
                }
            }
        }
    }

}
