package com.enovax.star.cms.api.store.service.axretail;

import com.enovax.star.cms.api.store.transformer.axretail.crosssell.AxRetailUpCrossSellResponseTransformer;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellProduct;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellServiceStubDetails;
import com.enovax.star.cms.commons.ws.axretail.crosssell.ISDCUpCrossSellService;
import com.enovax.star.cms.commons.ws.axretail.crosssell.ObjectFactory;
import com.enovax.star.cms.commons.ws.axretail.crosssell.SDCGetUpCrossSellResponse;
import com.enovax.star.cms.commons.ws.axretail.crosssell.UpCrossSellTableCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jensen on 24/6/16.
 */
@Deprecated
@Service
public class AxRetailUpCrossSellService {

    private Logger log = LoggerFactory.getLogger(getClass());

    private boolean firstStart = true;

    private Map<StoreApiChannels, AxRetailUpCrossSellServiceStubDetails> channelStubs = new HashMap<>();

    @Value("${api.store.axretail.api.url.upcrosssell.b2cmflg}")
    private String apiUrlB2CMFLG;
    @Value("${api.store.axretail.api.url.upcrosssell.b2cslm}")
    private String apiUrlB2CSLM;
    @Value("${api.store.axretail.api.url.upcrosssell.kiosk}")
    private String apiUrlKIOSK;
    @Value("${api.store.axretail.api.url.upcrosssell.partnerportal}")
    private String apiUrlPARTNERPORTAL;

    //TODO Change the way to retrieve lo

    private Long channelIdB2CMFLG = 0L;
    private Long channelIdB2CSLM = 0L;
    private Long channelIdKIOSK = 0L;
    private Long channelIdPARTNERPORTAL = 0L;


    //TODO Synchronization issues on initialisation

    @PostConstruct
    private void initService() {
        try {

            //TODO Right now it's just deployed to one URL

            if (StringUtils.isEmpty(apiUrlB2CSLM) || "${api.store.axretail.api.url.upcrosssell.b2cslm}".equals(apiUrlB2CSLM)) {
//                apiUrlB2CSLM = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_UpCrossSellService.svc";
                apiUrlB2CSLM = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50001/Services/SDC_UpCrossSellService.svc";
            }
            if (StringUtils.isEmpty(apiUrlB2CMFLG) || "${api.store.axretail.api.url.upcrosssell.b2cmflg}".equals(apiUrlB2CMFLG)) {
                apiUrlB2CMFLG = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_UpCrossSellService.svc";
            }
            if (StringUtils.isEmpty(apiUrlPARTNERPORTAL) || "${api.store.axretail.api.url.upcrosssell.partnerportal}".equals(apiUrlPARTNERPORTAL)) {
//                apiUrlPARTNERPORTAL = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_UpCrossSellService.svc";
                apiUrlPARTNERPORTAL = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50001/Services/SDC_UpCrossSellService.svc";
            }
            if (StringUtils.isEmpty(apiUrlKIOSK) || "${api.store.axretail.api.url.upcrosssell.kiosk}".equals(apiUrlKIOSK)) {
                apiUrlKIOSK = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_UpCrossSellService.svc";
            }

            channelStubs.put(StoreApiChannels.B2C_MFLG, new AxRetailUpCrossSellServiceStubDetails(apiUrlB2CMFLG));
            channelStubs.put(StoreApiChannels.B2C_SLM, new AxRetailUpCrossSellServiceStubDetails(apiUrlB2CSLM));
            channelStubs.put(StoreApiChannels.KIOSK_SLM, new AxRetailUpCrossSellServiceStubDetails(apiUrlKIOSK));
            channelStubs.put(StoreApiChannels.KIOSK_MFLG, new AxRetailUpCrossSellServiceStubDetails(apiUrlKIOSK));
            channelStubs.put(StoreApiChannels.PARTNER_PORTAL_SLM, new AxRetailUpCrossSellServiceStubDetails(apiUrlPARTNERPORTAL));
            channelStubs.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new AxRetailUpCrossSellServiceStubDetails(apiUrlPARTNERPORTAL));

            if (firstStart) { firstStart = false; return; }

            boolean success;

            log.info("Initialising AX Up-Cross-Sell Retail Service for B2C MFLG.");
            success = channelStubs.get(StoreApiChannels.B2C_MFLG).initialise();
            log.info("AX Up-Cross-Sell Retail Service for B2C MFLG : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Up-Cross-Sell Retail Service for B2C SLM.");
            success = channelStubs.get(StoreApiChannels.B2C_SLM).initialise();
            log.info("AX Up-Cross-Sell Retail Service for B2C SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Up-Cross-Sell Retail Service for KIOSK SLM.");
            success = channelStubs.get(StoreApiChannels.KIOSK_SLM).initialise();
            log.info("AX Up-Cross-Sell Retail Service for KIOSK SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Up-Cross-Sell Retail Service for KIOSK MFLG.");
            success = channelStubs.get(StoreApiChannels.KIOSK_MFLG).initialise();
            log.info("AX Up-Cross-Sell Retail Service for KIOSK MFLG: INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Up-Cross-Sell Retail Service for PARTNER PORTAL.");
            success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_SLM).initialise();
            log.info("AX Up-Cross-Sell Retail Service for PARTNER PORTAL : INIT " + (success ? "SUCCESS" : "FAILED"));

            log.info("Initialising AX Up-Cross-Sell Retail Service for PARTNER PORTAL.");
            success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_MFLG).initialise();
            log.info("AX Up-Cross-Sell Retail Service for PARTNER PORTAL : INIT " + (success ? "SUCCESS" : "FAILED"));


        } catch (Exception e) {
            log.error("Error initialising AX Up-Cross-Sell Retail Service.", e);
        }
    }

    public ApiResult<List<AxRetailUpCrossSellProduct>> apiGetUpCrossSell(StoreApiChannels channel) throws AxRetailServiceException {
        log.info("[AX Retail Product Ext Service Client] Calling apiSearchProductExt");
        try {

            final AxRetailUpCrossSellServiceStubDetails channelStub = channelStubs.get(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }
            final ISDCUpCrossSellService stub = channelStub.getStub();

            Long channelId = 0L;
            switch (channel) {
                case B2C_SLM:
                    channelId = channelIdB2CSLM; break;
                case B2C_MFLG:
                    channelId = channelIdB2CMFLG; break;
                case PARTNER_PORTAL_SLM:
                    channelId = channelIdPARTNERPORTAL; break;
                case PARTNER_PORTAL_MFLG:
                    channelId = channelIdPARTNERPORTAL; break;
                case KIOSK_SLM:
                    channelId = channelIdKIOSK; break;
                case KIOSK_MFLG:
                    channelId = channelIdKIOSK; break;
            }

            ObjectFactory factory = new ObjectFactory();

            final UpCrossSellTableCriteria criteria = factory.createUpCrossSellTableCriteria();
            criteria.setItemId(factory.createUpCrossSellTableCriteriaItemId(""));
            criteria.setChannel(channelId);
            criteria.setDataLevelValue(0);

            final SDCGetUpCrossSellResponse axResponse = stub.getUpCrossSell(criteria);

            ApiResult<List<AxRetailUpCrossSellProduct>> result = AxRetailUpCrossSellResponseTransformer.fromWs(axResponse);

            return result;
        } catch (Exception e) {
            final String errMsg = "[AX Retail Product Ext Service Client] Error: apiSearchProductExt";
            log.error(errMsg, e);
            throw new AxRetailServiceException(errMsg, e);
        }
    }
}
