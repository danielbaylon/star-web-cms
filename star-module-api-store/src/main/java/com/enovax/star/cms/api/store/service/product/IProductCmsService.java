package com.enovax.star.cms.api.store.service.product;

import com.enovax.star.cms.api.store.service.axretail.AxRetailServiceException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutDisplay;
import com.enovax.star.cms.commons.model.booking.CrossSellCartItem;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeRequest;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeResult;
import com.enovax.star.cms.commons.model.merchant.Merchant;

import java.util.List;

public interface IProductCmsService {

  ApiResult<List<CrossSellCartItem>> getAllCrossSellCartItemsFor(FunCartDisplay kioskTypeCart, String channelCode, String productNodeName, String locale) throws AxRetailServiceException;

  ApiResult<String> getOfferIdForPromoCode(String channelCode, String promoCode);

    ApiResult<String> getOfferIdForPromoCode(String channelCode, String promoCode, boolean includeInactive);

    ApiResult<String> getOfferIdForPromoCode(String channelCode, String cmsProductId, String promoCode);

  ApiResult<PromoCodeResult> checkPromoCode(PromoCodeRequest promoCodeRequest, String channelCode);

  ApiResult<PromoCodeResult> searchPromoCode(PromoCodeRequest promoCodeRequest, String channelCode);

  List<Merchant> getPromotionMerchants(String channel, CheckoutDisplay cart);
}