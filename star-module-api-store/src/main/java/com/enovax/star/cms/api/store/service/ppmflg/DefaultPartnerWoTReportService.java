package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.WOTReservationStatus;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.PagedData;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.WOTReservationVM;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 1/12/16.
 */
@Service("PPMFLGIPartnerWoTReportService")
public class DefaultPartnerWoTReportService implements IPartnerWoTReportService {

    private static final int[] columnWidth = new int[]{10,20,10, 20, 18, 10, 10, 10, 10, 20, 50};
    private static final String[] columnNames = new String[]{"Show Date","Show Date","Show Date", "Receipt No", "PIN CODE", "QTY", "RDM", "UNR", "WDN", "Status", "Remarks"};
    private static final int columnCount = columnNames.length;
    private static final String emptyString = "";

    class WorkbookPropertyHolder{
        private int ri = 0;
        private int ci = 0;
        private int sn = 0;

        public int getRi() {
            return ri;
        }

        public void setRi(int ri) {
            this.ri = ri;
        }

        public int getCi() {
            return ci;
        }

        public void setCi(int ci) {
            this.ci = ci;
        }

        public int getSn() {
            return sn;
        }

        public void setSn(int sn) {
            this.sn = sn;
        }

        public int getNextRowIndex(){
            int temp = ri;
            ri += 1;
            ci = 0;
            return temp;
        }
        public int getNextColumnIndex(){
            int temp = ci;
            ci += 1;
            return temp;
        }
    }

    @Autowired
    private IPartnerWoTReservationService wotSrv;

    @Override
    public SXSSFWorkbook exportEmptyExcelWithError(String message) {
        SXSSFWorkbook wb = new SXSSFWorkbook(3);
        wb.createSheet("Errors").createRow(0).createCell(0).setCellValue(String.valueOf(message));
        return wb;
    }

    @Override
    public SXSSFWorkbook exportAsExcel(PartnerAccount account, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String filterStatus, String showTimes) {
        int pageSize  = 100;
        ApiResult<PagedData<List<WOTReservationVM>>> data = null;
        try {
            data = wotSrv.getPagedWingsOfTimeReservations(account, startDate, endDate, reservationStartDate, reservationEndDate, filterStatus, showTimes, "eventDate", "DESC", 1, 100);
        } catch (Exception e) {
            return exportEmptyExcelWithError(e.getMessage());
        }
        WorkbookPropertyHolder holder = new WorkbookPropertyHolder();
        SXSSFWorkbook wb = new SXSSFWorkbook(3);

        CellStyle wrapTextStyle = wb.createCellStyle();
        wrapTextStyle.setWrapText(true);

        Sheet sheet = wb.createSheet("Wings of Time");
        int total = data.getData() != null ? data.getData().getSize() : 0;
        holder = writeReportHeader(holder, wb, sheet);
        holder = writeReportContent(holder, wb, sheet, data.getData() != null ? data.getData().getData() : null, wrapTextStyle);
        if(total > pageSize){
            int pages = total / pageSize;
            if((total % pageSize) > 0){
                pages += 1;
            }
            pages += 1;
            for(int i = 2 ; i< pages ; i++){
                try {
                    data = wotSrv.getPagedWingsOfTimeReservations(account, startDate, endDate, reservationStartDate, reservationEndDate, filterStatus, showTimes, "eventDate", "DESC", i, 100);
                    holder = writeReportContent(holder, wb, sheet, data.getData() != null ? data.getData().getData() : null, wrapTextStyle);
                } catch (Exception e) {
                    try{
                        wb.close();
                        wb = null;
                    }catch (Exception ex1){
                    }catch (Throwable ex2){}
                    return exportEmptyExcelWithError(e.getMessage());
                }
            }
        }
        autoResizeColumns(wb,sheet);
        return wb;
    }

    private void autoResizeColumns(SXSSFWorkbook wb, Sheet sheet) {
        for(int i = 0  ; i < columnCount ; i++){
            sheet.setColumnWidth(i, columnWidth[i] * 256);
        }
    }

    private WorkbookPropertyHolder writeReportHeader(WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet) {
        Font font = wb.createFont();
        font.setBold(true);
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font);
        Row row = null;
        Cell cell = null;
        row = sheet.createRow(holder.getNextRowIndex());
        for(int i = 0 ;  i < columnCount ; i++){
            cell = row.createCell(holder.getNextColumnIndex());
            cell.setCellStyle(style);
            cell.setCellValue(columnNames[i]);
        }
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 2));
        return holder;
    }

    private WorkbookPropertyHolder writeReportContent(WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet, List<WOTReservationVM> data, CellStyle wrapTextStyle) {
        Row row = null;
        Cell cell = null;
        if(data != null && data.size() > 0){
            int total = data.size();
            for(int i = 0 ; i < total ; i++ ){
                WOTReservationVM vm = data.get(i);
                row = sheet.createRow(holder.getNextRowIndex());
                holder = writeReportRowContent(holder, wb, sheet, row, cell, vm, wrapTextStyle);
            }
        }
        return holder;
    }

    private WorkbookPropertyHolder writeReportRowContent(WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet, Row row, Cell cell, WOTReservationVM vm, CellStyle wrapTextStyle) {
        String showDateWeek = vm.getEventDate() != null ? (NvxDateUtils.formatDate(vm.getEventDate(), NvxDateUtils.WOT_TICKET_DATE_EXPORT_FORMAT_WEEK)) : "";
        String showDateDate = vm.getEventDate() != null ? (NvxDateUtils.formatDate(vm.getEventDate(), NvxDateUtils.WOT_TICKET_DATE_EXPORT_FORMAT_DATE)) : "";
        String showDateDisplay = vm.getShowTimeDisplay();
        String receiptNo = vm.getReceiptNo();
        String pincode = vm.getPinCode();

        int qty = vm.getQty() - vm.getQtySold();
        int rdm = vm.getRdm();
        int unr = vm.getUnr();

        Integer isWashdone = vm.getIsWasheddown();
        Integer qtyWashdone = vm.getQtyWashedDown();

        String status = vm.getStatus();
        if(status != null && status.trim().length() > 0){
            if(WOTReservationStatus.FullyPurchased.name().equalsIgnoreCase(status) || WOTReservationStatus.PartiallyPurchased.name().equalsIgnoreCase(status)){
                status = "Reserved";
            }
        }
        String remarks = vm.getRemarks();
        if(WOTReservationStatus.Released.name().equalsIgnoreCase(status)){
            remarks = String.format("%s \n%s %s", vm.getRemarks(), "SYSTEM: Reservation released at", vm.getReleasedDate());
        }
        // show date
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(showDateWeek));
        // show date
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(showDateDate));
        // show date
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(showDateDisplay));
        // receipt number
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(getNonEmptyString(receiptNo));
        // pin code
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(getNonEmptyString(pincode));
        // qty
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(qty);
        // rdm
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(rdm);
        // unr
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(unr);
        // wdn
        cell = row.createCell(holder.getNextColumnIndex());
        if(isWashdone != null && isWashdone.intValue() == 1){
            int wdn = qtyWashdone != null ? qtyWashdone.intValue() : 0;
            cell.setCellValue(wdn);
        }else{
            cell.setCellValue("");
        }
        // status
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(getNonEmptyString(status));
        // remarks
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(remarks));

        cell = null;
        showDateWeek = null;
        showDateDate = null;
        showDateDisplay = null;
        receiptNo = null;
        pincode = null;
        status = null;
        remarks = null;

        return holder;
    }

    public String getNonEmptyString(String str){
        if(str == null || str.trim().length() == 0){
            return emptyString;
        }
        return str.trim();
    }
}

