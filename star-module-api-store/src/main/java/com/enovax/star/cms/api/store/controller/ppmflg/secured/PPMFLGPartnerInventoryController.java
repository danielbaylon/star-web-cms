package com.enovax.star.cms.api.store.controller.ppmflg.secured;


import com.enovax.star.cms.api.store.controller.ppmflg.PPMFLGBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.partner.ppmflg.PkgUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/partner-inventory/")
public class PPMFLGPartnerInventoryController extends PPMFLGBaseStoreApiController {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    private HttpSession session;

    @Autowired
    @Qualifier("PPMFLGIPartnerInventoryService")
    private IPartnerInventoryService inventoryService;

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService accountService;

    @Autowired
    @Qualifier("PPMFLGIPartnerPkgService")
    private IPartnerPkgService pkgService;

    @Autowired
    private ITicketGenerationService ticketGenerationService;

    @Autowired
    @Qualifier("PPMFLGIPkgExcelFileTicketGenerator")
    private IPkgExcelFileTicketGenerator excelFileTicketGenerator;

    @Autowired
    @Qualifier("PPMFLGIPartnerRevalidationTransactionService")
    private IPartnerRevalidationTransactionService revalTransService;

    @Autowired
    @Qualifier("PPMFLGIPartnerRevalidatePurchaseService")
    private IPartnerRevalidatePurchaseService revalPurchaseService;

    @Autowired
    @Qualifier("PPMFLGIPartnerRevalidationService")
    private IPartnerRevalidationService revalService;

    @Autowired
    @Qualifier("PPMFLGIInventoryExcelGenerator")
    private IInventoryExcelGenerator inventoryExcelGenerator;

    @Autowired
    @Qualifier("PPMFLGIPkgExcelGenerator")
    private IPkgExcelGenerator pkgExcelGenerator;

    //http://sentosab2b.enovax.com/searchInventory?startDateStr=&endDateStr=&productName=&availableOnly=null&take=10&skip=0&page=1&pageSize=10&_=1462853007748
    @RequestMapping(value = "search-inventory", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<InventoryResult>> apiSearchInventory(
            @RequestParam(value = "startDateStr", required = false) String startDateStr,
            @RequestParam(value = "endDateStr", required = false) String endDateStr,
            @RequestParam(value = "productName", required = false) String productName,
            @RequestParam(value = "availableOnly", required = false) String availableOnly,
            @RequestParam("take") int take,
            @RequestParam("skip") int skip,
            @RequestParam("page") int page,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered apiSearchInventory...");
        try {
            final ApiResult<InventoryResult> loggedInResult = isLoggedInWrappedResult(session, InventoryResult.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            //TODO i dunno what's the adminId is for
            //References TAInventoryAction.java
            log.debug("[ACTION] Initializing searchInventory page."
                    + availableOnly);
            log.debug("[ACTION-AJAX] Processing getInventoryTransactionsItem--->"
                    + sortField + " dir " + sortDirection);

            // page = (page == null ? 1 : page);
            //pageSize = (pageSize == null ? NvxUtil.DEFAULT_PAGE_SIZE : pageSize);
//            Integer tktepweeks = sysService.getObjByKey(
//                    SysParamConst.TicketExpiringAlertPeriod.toString(), Integer.class,
//                    true);

            PartnerAccount account = getLoginUserAccount(session);
            final List<InventoryTransactionItemVM> items = inventoryService
                    .getInventoryTransactionsItem(account.getId(), startDateStr, endDateStr, availableOnly, productName, sortField, sortDirection, page, pageSize);


            //TODO check this one
//            Integer tktepweeks = sysService.getObjByKey(
//                    SysParamConst.TicketExpiringAlertPeriod.toString(), Integer.class,
//                    true);
//
//            transItemVms = ViewModelUtil.convertInventoryItemVM(transItemList,tktepweeks);
//
            long total = inventoryService.getInventoryTransactionsItemSize(account.getId(), startDateStr, endDateStr, availableOnly, productName);

            final InventoryResult result = new InventoryResult();
            result.setTotal(total);
            result.setTransItemVms(items);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSearchInventory] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, InventoryResult.class, ""), HttpStatus.OK);
        }
    }

    //
    @RequestMapping(value = "create-pkg", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCreatePackage(@RequestBody PkgVM packageInfo) {
        log.info("Entered apiCreatePackage...");
        try {
            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            String pkgName = PkgUtil.generatePkgName(account.getAccountCode());

            //TODO check TAPkgAction.java
            ResultVM result = pkgService.getBundledItemsByTransItemIds(
                    packageInfo.getTransItemIds(),
                    pkgName,
                    packageInfo.getPkgQtyPerProd(),
                    packageInfo.getPkgTktType(),
                    packageInfo.getItemIdToAdd(),
                    packageInfo.getPkgDesc(),
                    packageInfo.getPkgTktMedia());

            //"{"pkgName":"LUCY123_10052016_DZRW","bundledItems":[{"itemCode":11300,"displayTitle":"SG50 Sentosa Tourist Pack -TA","transItems":[{"itemId":392,"prodId":28,"receiptNum":"SB2B160505WQPCP","unpackagedQty":23,"validityEndDateStr":"05/11/2016"}],"topupItems":[]}],"pkgExpiryDateStr":"05/11/2016","pkgTktType":0,"pkgDesc":""}"
            PkgVM pkgvm = (PkgVM) result.getViewModel();
            String bundledPkg = JsonUtil.jsonify(pkgvm);


            //TODO TNC
//            //Populate TNC
//            final List<Integer> prodIds = new ArrayList<>();
//            for (PkgBundledItemVM bundle : pkgvm.getBundledItems()) {
//                for (PkgTransItemVM transItem : bundle.getTransItems()) {
//                    prodIds.add(transItem.getProdId());
//                }
//            }
//            if (!prodIds.isEmpty()) {
//                this.session.put(SESS_PROD_IDS_FOR_TNC, prodIds);
//            }


            return new ResponseEntity<>(new ApiResult<>(true, "", result.getMessage(), bundledPkg), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCreatePackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "verify-pkg", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiVerifyPackage(@RequestBody PkgVM packageInfo) {
        log.info("Entered apiVerifyPackage...");
        try {
            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);
            ResultVM result = pkgService.verifyPkg
                    (packageInfo.getTransItemIds(), packageInfo.getPkgName(), account.getAccountCode(),
                            packageInfo.getPkgQtyPerProd(), packageInfo.getPkgTktType(), packageInfo.getPkgTktMedia());

            if (result.isStatus()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ApiResult<>(false, "", result.getMessage(), ""), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiVerifyPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "create-pkg-do", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCreatePackageDo(@RequestBody PkgVM packageInfo) {
        log.info("Entered apiCreatePackage...");
        try {
            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ResultVM resultVm = pkgService.createPkg(packageInfo.getTransItemIds(),
                    packageInfo.getPkgName(),
                    packageInfo.getPkgQtyPerProd(),
                    packageInfo.getPkgTktType(),
                    account,
                    packageInfo.getPkgDesc(),
                    packageInfo.getPkgTktMedia());


            //TODO evaluate if this is needed
            if (resultVm.isStatus()) {
                MixMatchPkgVM mixMatchPkgVM = (MixMatchPkgVM) resultVm.getViewModel();
                List<Integer> pkgIdList = (List<Integer>) session.getAttribute("PKG_RESERVED_ID_LIST");
                if (pkgIdList == null) {
                    pkgIdList = new ArrayList<>();

                }
                pkgIdList.add(mixMatchPkgVM.getId());
                session.setAttribute("PKG_RESERVED_ID_LIST", pkgIdList);
            }

            //"{"pkgName":"LUCY123_10052016_DZRW","bundledItems":[{"itemCode":11300,"displayTitle":"SG50 Sentosa Tourist Pack -TA","transItems":[{"itemId":392,"prodId":28,"receiptNum":"SB2B160505WQPCP","unpackagedQty":23,"validityEndDateStr":"05/11/2016"}],"topupItems":[]}],"pkgExpiryDateStr":"05/11/2016","pkgTktType":0,"pkgDesc":""}"
            session.setAttribute("CREATE_PKG_PARAM_DATA", packageInfo);
            session.setAttribute("CREATE_PKG_DATA", resultVm.getViewModel());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCreatePackageDo] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-confirm-pkg", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<MixMatchPkgVM>> apiGetConfirmPackage() {
        log.info("Entered apiGetConfirmPackage...");
        try {

            final ApiResult<MixMatchPkgVM> loggedInResult = isLoggedInWrappedResult(session, MixMatchPkgVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            if (session.getAttribute("CREATE_PKG_DATA") != null) {
                MixMatchPkgVM pkgVM = (MixMatchPkgVM) session.getAttribute("CREATE_PKG_DATA");
                return new ResponseEntity<>(new ApiResult<>(true, "", "", pkgVM), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ApiResult<>(false, "", "", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetConfirmPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, MixMatchPkgVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "cancel-package", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCancelPackage() {
        log.info("Entered apiCancelPackage...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            List<Integer> pkgIdList = (List<Integer>) this.session.getAttribute("PKG_RESERVED_ID_LIST");
            pkgService.removeReservedPkg(pkgIdList);
            this.session.removeAttribute("PKG_RESERVED_ID_LIST");
            this.session.removeAttribute("CREATE_PKG_DATA");

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "edit-inventory-package", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiEditInventoryPackage() {
        log.info("Entered apiCancelPackage...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            this.session.setAttribute("EDIT_PCK", true);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-edit-package-info", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PkgVM>> apiGetEditPackageInfo() {
        log.info("Entered apiGetEditPackageInfo...");
        try {
            final ApiResult<PkgVM> loggedInResult = isLoggedInWrappedResult(session, PkgVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PkgVM pkgVM = null;

            if (this.session.getAttribute("EDIT_PCK") != null) {
                pkgVM = (PkgVM) this.session.getAttribute("CREATE_PKG_PARAM_DATA");
            }

            this.session.removeAttribute("EDIT_PCK");
            this.session.removeAttribute("CREATE_PKG_PARAM_DATA");

            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkgVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiGetEditPackageInfo] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PkgVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "gen-excel-ticket/{pkgId}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGenerateExcelTicket(@PathVariable Integer pkgId) {
        log.info("Entered apiGenerateExcelTicket...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ResultVM resultVm = pkgService.genExcelTicket(pkgId, account.getPartner().getAxAccountNumber());

            if (resultVm.isStatus()) {
                MixMatchPkgVM mmPkgVm = (MixMatchPkgVM) resultVm.getViewModel();
                this.storeTicketDataInSession(mmPkgVm);
                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            } else {
                log.error(resultVm.getMessage());
                return new ResponseEntity<>(new ApiResult<>(false, "", resultVm.getMessage(), ""), HttpStatus.OK);
            }


        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "gen-eticket/{pkgId}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGenerateETicket(@PathVariable Integer pkgId) {
        log.info("Entered apiGenerateETicket...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ResultVM resultVm = pkgService.genETicket(pkgId, account.getPartner().getAxAccountNumber());

            if (resultVm.isStatus()) {
                MixMatchPkgVM mmPkgVm = (MixMatchPkgVM) resultVm.getViewModel();
                this.storeTicketDataInSession(mmPkgVm);
                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            } else {
                log.error(resultVm.getMessage());
                return new ResponseEntity<>(new ApiResult<>(false, "", resultVm.getMessage(), ""), HttpStatus.OK);
            }


        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "gen-pincode/{pkgId}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGeneratePinCode(@PathVariable Integer pkgId) {
        log.info("Entered apiGeneratePinCode...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            ResultVM resultVm = pkgService.genPinCode(pkgId);

            if (resultVm.isStatus()) {
                MixMatchPkgVM mmPkgVm = (MixMatchPkgVM) resultVm.getViewModel();
                this.storeTicketDataInSession(mmPkgVm);

                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            } else {
                log.error(resultVm.getMessage());
                return new ResponseEntity<>(new ApiResult<>(false, "", resultVm.getMessage(), ""), HttpStatus.OK);
            }


        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    private void storeTicketDataInSession(MixMatchPkgVM mmPkgVm) {
        this.session.setAttribute("PKG_TICKET_DATA", mmPkgVm);

        //remove previous store attributes coz it's no longer needed
        this.session.removeAttribute("PKG_RESERVED_ID_LIST");
        this.session.removeAttribute("CREATE_PKG_PARAM_DATA");
        this.session.removeAttribute("CREATE_PKG_DATA");
    }

    @RequestMapping(value = "get-eticket-info", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<MixMatchPkgVM>> apiGetETicketInfo() {
        log.info("Entered apiGetETicketInfo...");
        try {
            final ApiResult<MixMatchPkgVM> loggedInResult = isLoggedInWrappedResult(session, MixMatchPkgVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            MixMatchPkgVM mmPkgVm = null;
            if (this.session.getAttribute("PKG_TICKET_DATA") != null) {
                mmPkgVm = (MixMatchPkgVM) this.session.getAttribute("PKG_TICKET_DATA");
                this.session.removeAttribute("PKG_TICKET_DATA");
                return new ResponseEntity<>(new ApiResult<>(true, "", "", mmPkgVm), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ApiResult<>(false, "", "", null), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiGetETicketInfo] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, MixMatchPkgVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "download-ticket/{pkgId}", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ApiResult<String>> apiDownloadTicket(@PathVariable Integer pkgId, HttpServletResponse response) {
        log.info("Entered apiDownloadTicket...");
        try {
            //TODO check if the pkg is really from the login user
            MixMatchPkgVM mmPackage = pkgService.getPkgByID(pkgId);
            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(pkgService.getETickets(pkgId));

            final byte[] outputBytes = ticketGenerationService.generateTickets(StoreApiChannels.PARTNER_PORTAL_MFLG, ticketDataCompiled);

            final String fileName = pkgId + "ET" + mmPackage.getName();
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setHeader("X-Frame-Options", "SAMEORIGIN");

            ServletOutputStream output = response.getOutputStream();
            output.write(outputBytes);
            output.flush();

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGenerateTicket] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "download-excel-ticket/{pkgId}", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ApiResult<String>> apiDownloadExcelTicket(@PathVariable Integer pkgId, HttpServletResponse response) {
        log.info("Entered apiDownloadExcelTicket...");
        try {

            //TODO check if the pkg is really from the login user
            MixMatchPkgVM mmPackage = pkgService.getPkgByID(pkgId);
            List<ETicketData> ticketDataList = pkgService.getExcelTickets(pkgId);
            MixMatchPkgVM mixMatchPkgVM = pkgService.getPkgByID(pkgId);

            Workbook wb = excelFileTicketGenerator.generatePkgExcel(mixMatchPkgVM, ticketDataList);
            final String fileName = pkgId + "EXCEL" + mmPackage.getName() + ".xlsx";
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream os = response.getOutputStream();
            wb.write(os);
            wb.close();
            os.flush();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiDownloadExcelTicket] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


//    @RequestMapping(value = "gen-eticket", method = {RequestMethod.POST})
//    public ResponseEntity<ApiResult<String>> apiGenETicket() {
//        log.info("Entered apiGenerateETicket...");
//        try {
//            ResultVM resultVm = pkgService.genETicket()
//            List<Integer> pkgIdList = (List<Integer>) this.session.getAttribute("PKG_RESERVED_ID_LIST");
//            pkgService.removeReservedPkg(pkgIdList);
//            this.session.removeAttribute("PKG_RESERVED_ID_LIST");
//            this.session.removeAttribute("CREATE_PKG_DATA");
//
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
//        }catch (Exception e) {
//            log.error("!!! System exception encountered [ apiCancelPackage] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
//        }
//    }


    @RequestMapping(value = "view-inventory-receipt/{transId}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<InventoryTransactionVM>> apiGetInventoryReceipt(@PathVariable("transId") Integer transId) {
        log.info("Entered apiGetInventoryReceipt...");
        try {

            final ApiResult<InventoryTransactionVM> loggedInResult = isLoggedInWrappedResult(session, InventoryTransactionVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);
            InventoryTransactionVM currentTransaction = inventoryService.getTransByUser(transId, account);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", currentTransaction), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiGetInventoryReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, InventoryTransactionVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-package", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchPackage(
            @RequestParam(value = "startDateStr", required = false) String startDateStr,
            @RequestParam(value = "endDateStr", required = false) String endDateStr,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "ticketMedia", required = false) String ticketMedia,
            @RequestParam(value = "pkgName", required = false) String packageName,
            @RequestParam("take") int take,
            @RequestParam("skip") int skip,
            @RequestParam("page") int page,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered apiSearchPackage...");
        try {
            final ApiResult<ResultVM> loggedInResult = isLoggedInWrappedResult(session, ResultVM.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);


            ResultVM result = pkgService.getPkgsByPage(account.getId(), startDateStr, endDateStr, status, ticketMedia, packageName, sortField, sortDirection, page, pageSize);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSearchPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-package/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<MixMatchPkgVM>> apiGetPackage(@PathVariable("id") Integer pkgId) {
        log.info("Entered apiGetPackage...");
        try {

            final ApiResult<MixMatchPkgVM> loggedInResult = isLoggedInWrappedResult(session, MixMatchPkgVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            MixMatchPkgVM pkgVM = pkgService.getPkgByID(pkgId);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkgVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetConfirmPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, MixMatchPkgVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "regenerate-receipt/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRegenerateReceipt(@PathVariable("id") Integer transId) {
        log.info("Entered apiRegenerateReceipt...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            String message = inventoryService.generateReceiptAndEmail(transId);

            return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "send-email-pincode/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSendEmailPincode(@PathVariable("id") Integer pkgId) {
        log.info("Entered apiSendEmailPincode...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            String message = pkgService.sendPincodeEmail(pkgId);

            return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "send-email-eticket/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSendEmailEticket(@PathVariable("id") Integer pkgId) {
        log.info("Entered apiSendEmailEticket...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            String message = pkgService.sendEticketEmail(pkgId);

            return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "send-email-excel/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSendEmailExcel(@PathVariable("id") Integer pkgId) {
        log.info("Entered apiSendEmailEticket...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            String message = pkgService.sendExcelEmail(pkgId);

            return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "check-redeem-status/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCheckRedeemStatus(@PathVariable("id") Integer pkgId) {
        log.info("Entered apiSendEmailEticket...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            ResultVM result = pkgService.checkRdmStatus(pkgId);

            if (result.isStatus()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ApiResult<>(false, "", result.getMessage(), ""), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "revalidate-receipt/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptVM>> apiRevalidateReceipt(@PathVariable("id") Integer transId) {
        log.info("Entered apiRevalidateReceipt...");
        try {

            final ApiResult<ReceiptVM> loggedInResult = isLoggedInWrappedResult(session, ReceiptVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ApiResult<ReceiptVM> result = revalService.revalidateReceipt(transId, account);

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRevalidateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ReceiptVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "finalise-reval-trans/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<CheckoutConfirmResult>> apiFinaliseRevalTrans(@PathVariable("id") Integer transId) {
        log.info("Entered apiRevalidateReceipt...");
        try {

            final ApiResult<CheckoutConfirmResult> loggedInResult = isLoggedInWrappedResult(session, CheckoutConfirmResult.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            final Trackd trackd = trackr.buildTrackd(session.getId(), request);

            ApiResult<CheckoutConfirmResult> result = revalService.finaliseRevalTransAndRedirect(transId, account, session, trackd);

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiFinaliseRevalTrans] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CheckoutConfirmResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "check-reval-status", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCheckRevalStatus() {
        log.info("Entered apiRevalidateReceipt...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }
            PartnerAccount account = getLoginUserAccount(session);
            String receiptNo = (String) session.getAttribute(StoreApiChannels.PARTNER_PORTAL_MFLG.code + account.getUsername() + StoreApiConstants.SESS_ATTR_REVAL_TXN_RECEIPT_NUM);
            ApiResult<String> result = revalService.checkStatus(receiptNo);
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckRevalStatus] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

//    @RequestMapping(value = "finalise-reval-trans-telemoney/{receiptNo}", method = {RequestMethod.GET, RequestMethod.POST})
//    public ResponseEntity<ApiResult<String>> apiFinaliseRevalTransTelemoney(@PathVariable("receiptNo") String receiptNumber) {
//        log.info("Entered apiFinaliseRevalTransTelemoney...");
//        try {
//
//            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
//            if (!loggedInResult.isSuccess()) {
//                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
//            }
//
//            PartnerAccount account = getLoginUserAccount(session);
//
//            ApiResult<String> result = revalService.finaliseRevalTransTelemoney(receiptNumber);
//
//            return new ResponseEntity<>(result, HttpStatus.OK);
//
//        }catch (Exception e) {
//            log.error("!!! System exception encountered [apiFinaliseRevalTrans] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
//        }
//    }


    @RequestMapping(value = "get-revalidated-receipt-by-receiptnum/{receiptNum}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptVM>> apiGetRevalidatedReceiptByReceiptNum(@PathVariable("receiptNum") String receiptNum) {
        log.info("Entered apiRevalidateReceipt...");
        try {

            final ApiResult<ReceiptVM> loggedInResult = isLoggedInWrappedResult(session, ReceiptVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ApiResult<ReceiptVM> result = revalService.getRevalidatedReceiptByReceiptNum(receiptNum, account);

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRevalidateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ReceiptVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-revalidated-receipt-by-transid/{transId}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptVM>> apiGetRevalidatedReceiptByTransId(@PathVariable("transId") Integer transId) {
        log.info("Entered apiRevalidateReceipt...");
        try {

            final ApiResult<ReceiptVM> loggedInResult = isLoggedInWrappedResult(session, ReceiptVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);

            ApiResult<ReceiptVM> result = revalService.getRevalidatedReceiptByTransId(transId, account);

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRevalidateReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ReceiptVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "send-email-reval-ticket/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSendRevalEmail(@PathVariable("id") Integer transId) {
        log.info("Entered apiSendRevalEmail...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            String message = revalTransService.generateRevalReceiptAndEmail(transId);

            return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSendRevalEmail] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "download-inventory-report", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> downloadInventoryReport(@RequestParam(value = "startDateStr", required = false) String startDateStr,
                                                                     @RequestParam(value = "endDateStr", required = false) String endDateStr,
                                                                     @RequestParam(value = "productName", required = false) String productName,
                                                                     @RequestParam(value = "availableOnly", required = false) String availableOnly,
                                                                     HttpServletResponse response) {
        log.info("Entered downloadInventoryReport...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);
            final List<InventoryTransactionItemVM> items = inventoryService
                    .getInventoryTransactionsItem(account.getId(), startDateStr, endDateStr, availableOnly, productName, null, null, null, null);

            Workbook wb = inventoryExcelGenerator.generateInventoryeExcel(items, startDateStr, endDateStr, availableOnly, productName);

            final String fileName = "Inventory_Item_List.xls";
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream os = response.getOutputStream();
            wb.write(os);
            wb.close();
            os.flush();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [downloadInventoryReport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "download-pkg-report", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> downloadPackageReport(@RequestParam(value = "startDateStr", required = false) String startDateStr,
                                                                   @RequestParam(value = "endDateStr", required = false) String endDateStr,
                                                                   @RequestParam(value = "status", required = false) String status,
                                                                   @RequestParam(value = "ticketMedia", required = false) String ticketMedia,
                                                                   @RequestParam(value = "pkgName", required = false) String packageName,
                                                                   HttpServletResponse response) {
        log.info("Entered downloadPackageReport...");
        try {

            final ApiResult<String> loggedInResult = isLoggedInWrappedResult(session, String.class);
            if (!loggedInResult.isSuccess()) {
                log.error("User is not login.");
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }

            PartnerAccount account = getLoginUserAccount(session);


            ResultVM result = pkgService.getPkgsByPage(account.getId(), startDateStr, endDateStr, status, ticketMedia, packageName, null, null, null, null);

            Workbook wb = pkgExcelGenerator.generatePkgExcel((List<MixMatchPkgVM>) result.getViewModel(),
                    startDateStr,
                    endDateStr,
                    status,
                    ticketMedia,
                    packageName);

            final String fileName = "Package_List.xls";
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream os = response.getOutputStream();
            wb.write(os);
            wb.close();
            os.flush();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [downloadPackageReport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


//    @RequestMapping(value = "revalidate-receipt/{id}", method = {RequestMethod.POST})
//    public ResponseEntity<ApiResult<ReceiptVM>> apiRevalidateReceipt(@PathVariable("id") Integer transId) {
//        log.info("Entered apiRevalidateReceipt...");
//        try {
//
//            final ApiResult<ReceiptVM> loggedInResult = isLoggedInWrappedResult(session, ReceiptVM.class);
//            if (!loggedInResult.isSuccess()) {
//                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
//            }
//
//            PartnerAccount account = getLoginUserAccount(session);
//
//            ApiResult<ReceiptVM> result = revalService.revalidateReceipt(transId, account);
//
//            return new ResponseEntity<>(result, HttpStatus.OK);
//
//        }catch (Exception e) {
//            log.error("!!! System exception encountered [apiRevalidateReceipt] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, ReceiptVM.class, ""), HttpStatus.OK);
//        }
//    }
}