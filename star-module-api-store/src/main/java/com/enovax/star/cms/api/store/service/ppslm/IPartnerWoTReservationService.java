package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by jennylynsze on 6/7/16.
 */
public interface IPartnerWoTReservationService {

    WOTReservationVM getReservationByID(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer reservationId) throws Exception;

    WOTReservationVM getReservationPrintingDetailsByID(PartnerAccount partnerAccount, Integer reservationId)throws Exception;

    ApiResult<String> cancelReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer reservationId, Double refund, boolean isAmendment) throws Exception;

    ApiResult<WOTReservationVM> updateReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM reservation) throws Exception;

    ApiResult<WOTReservationVM> saveReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM reservation, boolean isUpdateRequest) throws Exception;

    void refreshPartnerReservations(Integer loginId, String custAccNum);

    ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(PartnerAccount account, Date startDate, Date endDate,  Date reservationStartDate, Date reservationEndDate, String displayCancelled, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception;

    public Date calculateWingsOfTimeShowCutOffTimeByShowTime(final PPSLMWingsOfTimeReservation wot) throws Exception;

    ApiResult<String> refreshPinCodeReferenceIdAndInventTransId(PartnerAccount account, Integer reservationId) throws Exception;

    ApiResult<String> verifyAndCancelSplitReservationOrder(PartnerAccount account, Integer id) throws Exception;

    ApiResult<WoTVM> verifyAndConfirmSplitReservationOrder(PartnerAccount account, Integer id) throws Exception;

    ApiResult<WOTReservationCharge> validateNewReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM reservation) throws BizValidationException;

    ApiResult<WOTReservationCharge> validateUpdateReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM reservation) throws BizValidationException;

    ApiResult<WOTReservationCharge> validateCancelReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer reservationId) throws BizValidationException;

    ApiResult<WoTVM> verifyAndGetSplitReservationOrder(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer id, Integer qty) throws Exception;

    WOTReservationVM generateWoTPinCodeBarcode(WOTReservationVM reservation) throws Exception;

    ApiResult<WoTVM> verifyAndGetPurchaseReservationOrder(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer id, Integer qty) throws BizValidationException;

    ApiResult<String> verifyAndCancelPurchaseReservationOrder(PartnerAccount account, Integer id) throws BizValidationException;

    ApiResult<WoTVM> verifyAndConfirmPurchaseReservationOrder(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer id) throws BizValidationException;

    ApiResult<WOTReservationCharge> validatePurchaseReservedReservationPrice(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer id) throws BizValidationException;

    void refreshUnredeemedPinCodeService();

    ApiResult<String> verifyWoTReservationByDate(PartnerAccount account, Date validDate) throws BizValidationException;

    ApiResult<String> verifyWoTReservationByMonth(PartnerAccount account, Date validDate) throws BizValidationException;

    String genWoTReservationByDate(PartnerAccount account, Date validDate);

    String genWoTReservationByMonth(PartnerAccount account, Date validDate);
}