package com.enovax.star.cms.api.store.repository.partner;


import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;

/**
 * Created by jennylynsze on 5/18/16.
 */
public interface IPartnerAccountRepository {
    boolean hasUsername(String username);
    PartnerAccount getUserInfo(String username, String password);
    PartnerAccount getUserInfo(Integer id);
}
