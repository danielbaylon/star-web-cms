package com.enovax.star.cms.api.store.service.ppslm;


import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppslm.DepositVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PagedData;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/13/16.
 */
public interface IPartnerDepositService {

    ApiResult<AxCustomerBalances> getPartnerDepositBalanceWithResponse(String axAccountNumber) throws AxChannelException;

    BigDecimal getPartnerDepositBalance(String axAccountNumber) throws BizValidationException;

    PPSLMDepositTransaction createTopUpBalanceTxn(PartnerAccount account, PartnerVM partner, BigDecimal amount, String ipAddress, String userSessionId) throws BizValidationException;

    ApiResult<String> updateAxCustomerDepositBalance(String axAccountNumber, String receiptNum) throws Exception;

    PPSLMDepositTransaction updateTopUpBalanceTxnStatus(String receiptNum, ApiResult<String> axResponse);

    ApiResult<PagedData<List<DepositVM>>> getPagedViewDepositHist(String axCustAccNum, Integer mainAccountId, Date startDate, Date endDate, String sortField, String sortDirection, String depositTxnQueryType, int page, int pageSize) throws Exception;

    void validateTopUpBalanceAmount(BigDecimal amount) throws BizValidationException;

    void refreshWoTReservationListWithDateRange(String axCustNumber, Map<String, Integer> wotList, Date startDateTime, Date endDateTime) throws BizValidationException;

    String populateDepositToupPaymentRedirectURL(String channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException;

    PPSLMDepositTransaction getDepositTopupTransaction(String tmDataReceiptNumber, String tmDataSessionId, String tmUserField2);

    PPSLMDepositTransaction saveDepositTransactionForTMFailedProcessing(Integer id, String name, String name1, String ec, String em, Date now, boolean forceFailed) throws BizValidationException;

    PPSLMDepositTransaction saveDepositTransactionForTMSuccessProcessing(Integer id, String name, String name1, String tmCcNum, String tmApprovalCode, String tmPaymentType, Date now) throws BizValidationException;

    PPSLMDepositTransaction saveDepositTransactionForTMVoidProcessing(PPSLMDepositTransaction slmTxn, TmServiceResult voidResult) throws BizValidationException;

    ApiResult<String> updateAxCustomerDepositBalance(PPSLMDepositTransaction slmTxn) throws Exception;

    void sendDepositTopupTransactionReceiptEmail(PPSLMDepositTransaction txn);

    ApiResult<String> populateDepositToupPaymentStatusInfo(PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException;

    void sendVoidEmail(PPSLMDepositTransaction txn, TmServiceResult voidResult);

    void sendTmQueryForPendingTrans();

    String getDepositTopupTransactionTelemoneyReturnURL(String channel);

    public CheckoutConfirmResult fireLoadTestingResult(String partner_portal_channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String userRequestAddress, String id) throws BizValidationException;

    public void fireLoadTestingResult(String channel, CheckoutConfirmResult ccr2);
}
