package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
public interface IMixMatchPkgRepository {
    void save(MixMatchPkgVM mixMatchPkgVM);
    void update(MixMatchPkgVM mixMatchPkgVM);
    void removePkg(int id);
    MixMatchPkgVM getMixMatchPackage(int id);
    Integer getNextMixMatchPackageId();
    Integer getNextMixMatchPackageItemId();
    List<MixMatchPkgVM> getPkgsByPage(Integer adminId, Date fromDate,
                                               Date toDate, String status, String pkgNm, String orderField,
                                               String orderWith, Integer pageNumber, Integer pageSize);

    int getPkgsSize(Integer adminId,
                    Date fromDate, Date toDate, String status, String pkgNm);
}
