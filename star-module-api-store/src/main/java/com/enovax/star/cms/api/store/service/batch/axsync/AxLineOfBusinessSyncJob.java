package com.enovax.star.cms.api.store.service.batch.axsync;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxLineOfBusiness;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.repository.ppslm.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 22/9/16.
 */
@Service
public class AxLineOfBusinessSyncJob {

    private static final Logger log = LoggerFactory.getLogger(AxLineOfBusinessSyncJob.class);

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    @Autowired
    private AxChannelCustomerService custExtSrv;

    @Autowired
    private ILineOfBusinessRepository lbRepo;

    @Autowired
    private ISystemParamService sysParamSrv;

    private static volatile Runnable runner;
    private static volatile boolean isDone;


    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
        if(isAuthorInstance){
            runner = new Runnable() {
                @Override
                public void run() {
                    try{
                        isDone = false;
                        syncAxLineOfBusinessIntoJCR();
                    }catch (Exception ex){
                        log.error("Ax line of business sync job completed with failure", ex);
                    }finally {
                        isDone = true;
                    }
                }
            };
            isDone = true;
        }
    }

    @Scheduled(cron = "${ppslm.ax.line.of.business.sync.job}")
    public void axLineOfBusinessSyncJob(){
        boolean isJobEnabled = sysParamSrv.isPPSLMAxLineOfBusinessJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[axLineOfBusinessSyncJob] is not enabled.");
            return;
        }
        if(isAuthorInstance){
            if(isDone){
                realTimeTaskExecutor.submit(runner);
            }
        }
    }

    private void syncAxLineOfBusinessIntoJCR() {
        ApiResult<List<AxLineOfBusiness>> list = this.custExtSrv.getAxLineOfBusinessList(StoreApiChannels.PARTNER_PORTAL_SLM);
        if (list == null) {
            log.error("Ax line of business sync job completed with failure because of ax service response is empty.");
            return;
        }
        if (!list.isSuccess()) {
            log.error("Ax line of business sync job completed with failure because of ax service response is not success, error message : "
                    + (list.getErrorCode() != null ? list.getErrorCode() : "<empty>")
                    +" , "
                    + (list.getMessage() != null ? list.getMessage() : "<empty>")
            );
            return;
        }
        List<AxLineOfBusiness> tmpList = list.getData();
        if (tmpList == null) {
            log.error("Ax line of business sync job completed with failure because of country list in ax service response is empty.");
            return;
        }
        List<String> idList = new ArrayList<String>();
        for (AxLineOfBusiness i : tmpList) {
            if(i.getLineOfBusinessId() != null && i.getLineOfBusinessId().trim().length() > 0 && (!idList.contains(i.getLineOfBusinessId().trim()))){
                idList.add(i.getLineOfBusinessId());
            }
        }

        String wsName = PartnerPortalConst.Partner_Portal_Ax_Data;
        lbRepo.deleteLineOfBusinessIfNotInList(wsName,idList);
        for (AxLineOfBusiness i : tmpList) {
            if(!lbRepo.existsLineOfBusinessId(wsName,i.getLineOfBusinessId())){
                lbRepo.createLineOfBusiness(wsName,i);
            }
        }
        lbRepo.refreshCache(wsName);
    }
}
