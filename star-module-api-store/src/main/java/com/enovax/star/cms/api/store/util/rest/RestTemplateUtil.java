package com.enovax.star.cms.api.store.util.rest;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class RestTemplateUtil {

    public static <T> ResponseEntity<T> getForEntity(String url, Class<T> responseClass) {
        RestTemplate client = new RestTemplate();
        return client.getForEntity(url, responseClass);
    }

    public static <T,U> ResponseEntity<T> postForEntity(String url, U request, Class<T> responseClass) {
        RestTemplate client = new RestTemplate();
        return client.postForEntity(url, request, responseClass);
    }

    public static <T> ResponseEntity<T> exchange(String url, HttpMethod httpMethod, HttpEntity<?> request, ParameterizedTypeReference<T> returnedTypeRef) {
        RestTemplate client = new RestTemplate();
        return client.exchange(url, httpMethod, request, returnedTypeRef);
    }

    public static <T,U> ResponseEntity<T> exchange(String url, HttpMethod httpMethod, U requestObject, ParameterizedTypeReference<T> returnedTypeRef) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<U> request = new HttpEntity<>(requestObject, headers);

        return exchange(url, httpMethod, request, returnedTypeRef);
    }

    public static <T,U> ResponseEntity<T> exchange(String url, U requestObject, ParameterizedTypeReference<T> returnedTypeRef) {
        return exchange(url, HttpMethod.POST, requestObject, returnedTypeRef);
    }

}
