package com.enovax.star.cms.api.store.service.axretail;

@Deprecated
public class AxRetailServiceException extends Exception {

    public AxRetailServiceException() {
    }

    public AxRetailServiceException(String message) {
        super(message);
    }

    public AxRetailServiceException(Throwable t) {
        super(t);
    }

    public AxRetailServiceException(String message, Throwable t) {
        super(message, t);
    }

}
