package com.enovax.star.cms.api.store.service.ppmflg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("PPMFLGUserDetailsService")
public class CustomizedUserDetailsService implements UserDetailsService {
	private final Logger log = LoggerFactory.getLogger(CustomizedUserDetailsService.class);

	@Autowired
	@Qualifier("PPMFLGIPartnerAccountService")
	private IPartnerAccountService paAccSrv;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return paAccSrv.getUserDetailsByUserName(username);
	}
}