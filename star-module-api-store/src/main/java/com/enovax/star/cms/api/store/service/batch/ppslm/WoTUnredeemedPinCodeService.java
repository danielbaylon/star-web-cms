package com.enovax.star.cms.api.store.service.batch.ppslm;

import com.enovax.star.cms.api.store.service.ppslm.IPartnerWoTReservationService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class WoTUnredeemedPinCodeService {

    private static final Logger log = LoggerFactory.getLogger(WoTUnredeemedPinCodeService.class);

    @Autowired
    private IPartnerWoTReservationService wotSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;


    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppslm.wot.refresh.unredeemed.pincode.job}")
    public void refreshWoTUnredeemedPinCodeJob(){
        boolean isJobEnabled = sysParamSrv.isPPSLMWoTRefreshUnredeemedPinCodeJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[refreshWoTUnredeemedPinCodeJob] is not enabled.");
            return;
        }
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    try{
                        wotSrv.refreshUnredeemedPinCodeService();
                    }catch (Exception ex){
                        log.error("WoT refresh redeemed pin code service", ex);
                    }
                }
            });
        }
    }

}
