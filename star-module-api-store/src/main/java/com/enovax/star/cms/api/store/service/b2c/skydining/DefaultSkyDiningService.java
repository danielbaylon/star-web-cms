package com.enovax.star.cms.api.store.service.b2c.skydining;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TmFraudCheckPackage;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.b2cshared.service.DefaultB2CSysParamService;
import com.enovax.star.cms.b2cshared.service.IB2CSysParamService;
import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.TNCType;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppslm.TicketType;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.model.booking.skydining.*;
import com.enovax.star.cms.commons.model.merchant.Merchant;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.skydining.constant.*;
import com.enovax.star.cms.skydining.datamodel.*;
import com.enovax.star.cms.skydining.model.ReservationModel;
import com.enovax.star.cms.skydining.model.SessionRecord;
import com.enovax.star.cms.skydining.model.api.SkyDiningCheckPromoResult;
import com.enovax.star.cms.skydining.model.result.SkyDiningBookingItem;
import com.enovax.star.cms.skydining.model.result.SkyDiningBookingMainCourse;
import com.enovax.star.cms.skydining.model.result.SkyDiningCalendarResult;
import com.enovax.star.cms.skydining.util.ValidationUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jace on 30/8/16.
 */
@Service
public class DefaultSkyDiningService extends BaseSkyDiningService implements ISkyDiningService {

    public static final String RECEIPT_PREFIX = "MFLG";
    public static final String RECEIPT_DATE_FORMAT = "yyMMdd";
    public static final String DEFAULT_CURRENCY = "SGD";

    private Logger log = LoggerFactory.getLogger(DefaultSkyDiningService.class);

    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private IB2CSysParamService paramService;

    @Autowired
    private ISystemParamRepository paramRepo;

    public boolean isTMServiceEnabled() {
        String str = paramRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_TM_SERVICE_KEY);
        if (str != null && str.trim().length() > 0) {
            if (Boolean.TRUE.toString().equalsIgnoreCase(str.trim())) {
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public ApiResult<SkyDiningFunCartDisplay> cartAdd(StoreApiChannels channel, SkyDiningFunCart cartToAdd, String sessionId) {
        try {
            final ReservationModel model = createReservationModelFromFunCart(cartToAdd);
            if (StringUtils.isNotEmpty(model.getDateText())) {
                final Date date = NvxDateUtils.parseDate(model.getDateText(),
                    SysConst.DEFAULT_DATE_FORMAT);
                model.setDate(date);
            }
            if (StringUtils.isNotEmpty(model.getTimeText())) {
                final Time time = NvxDateUtils.parseTime(model.getTimeText(),
                    SysConst.DEFAULT_TIME_FORMAT);
                model.setTime(time);
            }
            model.setStatus(ReservationStatus.Unpaid.value);
            model.setType(ReservationType.Online.value);

            final SkyDiningCartResult result = saveCart(model, sessionId);

            Boolean success = false;
            String message;

            switch (result) {
                case FullSessionCapacity:
                    message = MSG_FULL_SESSION_CAPACITY;
                    break;
                case FullThemeCapacity:
                    message = MSG_FULL_THEME_CAPACITY;
                    break;
                case PackageAfterCutOff:
                    message = MSG_PACKAGE_CUT_OFF;
                    break;
                case MenuAfterCutOff:
                    message = MSG_MENU_CUT_OFF;
                    break;
                case TopUpAfterCutOff:
                    message = MSG_TOP_UP_CUT_OFF;
                    break;
                case CartNotEmpty:
                    message = MSG_CART_NOT_EMPTY;
                    break;
                case ValidationFailed:
                    message = MSG_GENERAL_VALIDATION_ERROR;
                    break;
                case Success:
                    success = true;
                    message = "";
                    break;
                default:
                    message = SysConst.KEY_MSG_ERR_GENERAL;
                    break;
            }

            ApiResult<SkyDiningFunCartDisplay> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(success);
            apiResult.setMessage(message);
            apiResult.setData(null);

            return apiResult;
        } catch (ParseException e) {
            log.error("Unable to parse value.", e);

            ApiResult<SkyDiningFunCartDisplay> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(false);
            apiResult.setMessage(MSG_GENERAL_VALIDATION_ERROR);
            apiResult.setData(null);

            return apiResult;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SkyDiningCheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session) {
        final SkyDiningCart cart = cartRepo.findOneByWebSessionId(session.getId());
        final SkyDiningFunCartDisplay cartVm = constructCartDisplay(cart);
        final SkyDiningCheckoutDisplay checkout = new SkyDiningCheckoutDisplay();

        checkout.setCartId(cartVm.getCartId());
        checkout.setTotalQty(cartVm.getTotalQty());
        checkout.setTotalMainQty(cartVm.getTotalMainQty());
        checkout.setCmsProducts(cartVm.getCmsProducts());
        checkout.setPayMethod(cartVm.getPayMethod());
        checkout.setPaymentTypeLabel("");
        if (StringUtils.isNotEmpty(checkout.getPayMethod())) {
            checkout.setMessage("");
        }

        final boolean hasBookingFee = false;
        checkout.setHasBookingFee(hasBookingFee);
        checkout.setBookFeeMode("");

        BigDecimal grandTotal = cartVm.getTotal();

        checkout.setWaivedTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        checkout.setTotal(grandTotal);
        checkout.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal, "S$ "));

        checkout.getSkyDiningProducts().addAll(cartVm.getSkyDiningProducts());

        return checkout;
    }

    @Override
    @Transactional
    public ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode) {
        //TODO: Implement method
        return null;
    }

    @Override
    @Transactional
    public ApiResult<SkyDiningFunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId) {
        cartClear(channel, sessionId);
        ApiResult<SkyDiningFunCartDisplay> apiResult = new ApiResult<>();
        apiResult.setErrorCode("200");
        apiResult.setSuccess(true);
        apiResult.setMessage("");
        apiResult.setData(null);
        return apiResult;
    }

    @Override
    @Transactional
    public boolean cartClear(StoreApiChannels channel, String sessionId) {
        cartRepo.deleteByWebSessionId(sessionId);
        return true;
    }

    @Override
    @Transactional
    public ApiResult<SkyDiningStoreTransaction> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets) {
        final SkyDiningCart cart = cartRepo.findOneByWebSessionId(session.getId());
        final SkyDiningCartResult ccr = validateCart(cart.getId());

        // Revalidate cart
        if (!SkyDiningCartResult.Success.equals(ccr)) {
            ApiResult<SkyDiningStoreTransaction> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(false);
            apiResult.setMessage(MSG_FAILED);
            apiResult.setData(null);
            return apiResult;
        }
        // Validate customer details
        if (!validateCustomerDetails(deets)) {
            ApiResult<SkyDiningStoreTransaction> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(false);
            apiResult.setMessage(MSG_CUSTOMER_DETAILS_FAILED);
            apiResult.setData(null);
            return apiResult;
        }

        // Generate receipt sequence
        String receiptSequence = "";
        int tries = 0;
        boolean seqSuccess = false;
        do {
            try {
                receiptSequence = generateSequence();
                seqSuccess = true;
            } catch (final Exception e) {
                log.debug("Error generating sequence. Retrying.");
                tries++;
            }
        } while (!seqSuccess && (tries < 3));
        if (!seqSuccess) {
            ApiResult<SkyDiningStoreTransaction> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(false);
            apiResult.setMessage(MSG_FAILED);
            apiResult.setData(null);
            return apiResult;
        }

        // Create new transaction
        final SkyDiningTransaction trans = new SkyDiningTransaction();

        final Date now = new Date();

        trans.setBookFeeWaived(true);
        trans.setBookFeeCents(0);
        trans.setBookFeeItemCode(0);
        trans.setBookFeeType(SkyDiningSysParamConst.BookFeeModeVals.None.toString());

        trans.setCreatedBy(SysConst.SYSTEM);
        trans.setCreatedDate(now);
        trans.setModifiedBy(SysConst.SYSTEM);
        trans.setModifiedDate(now);
        trans.setReceiptNum(formatReceiptNumber(RECEIPT_PREFIX, now,
            receiptSequence));
        trans.setCurrency(DEFAULT_CURRENCY);
        trans.setStage(Stage.Reserved.toString());

        trans.setCustEmail(deets.getEmail());
        trans.setCustIdNo(deets.getIdNo());
        trans.setCustIdType(deets.getIdType().toString());
        trans.setCustMobile(deets.getMobile());
        trans.setCustName(deets.getName());
        trans.setCustSubscribe(deets.isSubscribed());
        trans.setPaymentType(deets.getPaymentType());
        trans.setCustSalutation(deets.getSalutation());
        trans.setCustCompany(deets.getCompanyName());

        final SkyDiningReservation reservation = createReservationFromCart(
            cart, trans);

        trans.setReservation(reservation);

        String tmMerchantId = "";
        List<Merchant> defaultMerchants = merchRepo.getAll("b2c-mflg", true, true);
        if (defaultMerchants == null || defaultMerchants.isEmpty()) {
            ApiResult<SkyDiningStoreTransaction> apiResult = new ApiResult<>();
            apiResult.setErrorCode("200");
            apiResult.setSuccess(false);
            apiResult.setMessage(MSG_CUSTOMER_DETAILS_FAILED);
            apiResult.setData(null);
            return apiResult;
        } else {
            tmMerchantId = defaultMerchants.get(0).getMerchantId();
        }
        final List<Merchant> possibleMerchs = new ArrayList<Merchant>();
        if (StringUtils.isNotBlank(cart.getMerchantId())) {
            final Merchant selectedMerchant = merchRepo.find("b2c-mflg", cart.getMerchantId());
            if (selectedMerchant != null) {
                possibleMerchs.add(selectedMerchant);
            }
        }


        final String jewelCard = "";
        if (StringUtils.isNotEmpty(cart.getJewelCard())) {
            trans.setCustJewelCard(cart.getJewelCard());
        }
        trans.setCustJewelCard(jewelCard);

        boolean foundSpecific = false;
        for (final Merchant m : possibleMerchs) {
            if (m.isAcceptsAllCards()) {
                tmMerchantId = m.getMerchantId();
                foundSpecific = true;
            }
            if (!foundSpecific) {
                tmMerchantId = m.getMerchantId();
            }
        }
        trans.setTmMerchantId(tmMerchantId);
        trans.setTotalAmount(computeTotalAmount(cart));
        transRepo.save(trans);

        session.setAttribute(SESS_TRANS_ID, trans.getId());

        ApiResult<SkyDiningStoreTransaction> apiResult = new ApiResult<>();
        apiResult.setErrorCode("200");
        apiResult.setSuccess(true);
        apiResult.setMessage("");
        apiResult.setData(null);
        return apiResult;
    }


    @Override
    @Transactional
    public ApiResult<SkyDiningStoreTransaction> completeSale(StoreApiChannels channel, HttpSession session) {
        //TODO: Implement Payment
        ApiResult<SkyDiningStoreTransaction> result = new ApiResult<>();
        SkyDiningStoreTransaction transaction = new SkyDiningStoreTransaction();
        result.setData(transaction);
        result.setErrorCode("");
        result.setMessage("");
        result.setSuccess(true);
        return result;
    }

    @Override
    @Transactional
    public ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session) {
        cartRepo.deleteByWebSessionId(session.getId());
        session.removeAttribute(SESS_TRANS_ID);
        return new ApiResult<>(true, "", "", "");
    }

    @Override
    @Transactional(readOnly = true)
    public SkyDiningReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session) {
        final SkyDiningCart cart = cartRepo.findOneByWebSessionId(session.getId());
        if (cart != null) {
            final SkyDiningFunCartDisplay cartVm = constructCartDisplay(cart);
            Integer transId = (Integer) session.getAttribute(SESS_TRANS_ID);
            SkyDiningTransaction trans = transRepo.findOne(transId);
            SkyDiningReceiptDisplay receiptDisplay = constructReceiptDisplay(cartVm, trans);
            return receiptDisplay;
        }
        return null;
    }

    @Override
    public SkyDiningReceiptDisplay getReceiptForDisplayByReceiptNum(String receiptNum) {
        final SkyDiningTransaction trans = transRepo.findOneByReceiptNum(receiptNum);
        if (trans != null && trans.getReservation() != null) {
            final SkyDiningReservation res = trans.getReservation();
            final SkyDiningFunCartDisplay cartVm = constructCartDisplayFromReservation(res);
            SkyDiningReceiptDisplay receiptDisplay = constructReceiptDisplay(cartVm, trans);
            return receiptDisplay;
        }
        return null;
    }

    protected SkyDiningReceiptDisplay constructReceiptDisplay(SkyDiningFunCartDisplay cartVm, SkyDiningTransaction trans) {
        final SkyDiningReceiptDisplay receipt = new SkyDiningReceiptDisplay();

        receipt.setCartId(cartVm.getCartId());
        receipt.setTotalQty(cartVm.getTotalQty());
        receipt.setTotalMainQty(cartVm.getTotalMainQty());
        receipt.setCmsProducts(cartVm.getCmsProducts());
        receipt.setPayMethod(cartVm.getPayMethod());
        receipt.setPaymentTypeLabel("");
        if (StringUtils.isNotEmpty(receipt.getPayMethod())) {
            receipt.setMessage("");
        }

        BigDecimal grandTotal = cartVm.getTotal();

        receipt.setTotal(grandTotal);
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal, "S$ "));

        receipt.getSkyDiningProducts().addAll(cartVm.getSkyDiningProducts());


        if (trans != null) {
            receipt.setTransStatus(trans.getStage());

            CustomerDetails details = new CustomerDetails();
            details.setCompanyName(trans.getCustCompany());
            details.setPaymentType(trans.getPaymentType());
            details.setEmail(trans.getCustEmail());
            details.setIdNo(trans.getCustIdNo());
            details.setIdType(trans.getCustIdType());
            details.setMerchantId(trans.getTmMerchantId());
            details.setMobile(trans.getCustMobile());
            details.setName(trans.getCustName());
            details.setSalutation(trans.getCustSalutation());
            details.setSubscribed(trans.getCustSubscribe());

            receipt.setCustomer(details);

            receipt.setDateOfPurchase(NvxDateUtils.formatDateForDisplay(trans.getCreatedDate(), false));
            receipt.setReceiptNumber(trans.getReceiptNum());
            receipt.setPaymentType(trans.getPaymentType());
            receipt.setPaymentTypeLabel(trans.getPaymentType());
            receipt.setError(StringUtils.isNotBlank(trans.getTmErrorMessage()));
            receipt.setErrorCode(trans.getErrorCode());
            receipt.setErrorMessage(trans.getTmErrorMessage());
            receipt.setMessage("");
        }

        receipt.setHasTnc(cartVm.getHasTnc());
        receipt.setTncs(cartVm.getTncs());

        final String appContextRoot = paramService.getSystemParamValueByKey("b2c-mflg", DefaultB2CSysParamService.SYS_PARAM_B2C_APP_CONTEXT_ROOT);
        receipt.setBaseUrl(appContextRoot);

        return receipt;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SessionRecord> getAvailableSessions(Integer packageId, Date dateOfVisit) {
        List<SkyDiningSession> sessions = sessionRepo.findAvailable(new Date(), dateOfVisit, packageId);
        if (sessions != null) {
            List<SessionRecord> records = new ArrayList<>();
            for (SkyDiningSession session : sessions) {
                records.add(new SessionRecord(session));
            }
            return records;
        }
        return Collections.emptyList();
    }

    protected ReservationModel createReservationModelFromFunCart(SkyDiningFunCart c) {
        ReservationModel r = null;
        if (c.getSkyDiningItems() != null && !c.getSkyDiningItems().isEmpty()) {
            SkyDiningFunCartItem i = c.getSkyDiningItems().get(0);
            r = new ReservationModel();
            r.setId(0);
            r.setDateText(i.getSelectedEventDate());
            r.setTimeText(i.getEventSessionName());
            if (StringUtils.isNotBlank(i.getCmsProductId())) {
                r.setPackageId(Integer.parseInt(i.getCmsProductId()));
            }
            if (StringUtils.isNotBlank(i.getThemeId())) {
                r.setThemeId(Integer.parseInt(i.getThemeId()));
            }
            if (StringUtils.isNotBlank(i.getEventGroupId())) {
                r.setSessionId(Integer.parseInt(i.getEventGroupId()));
            }
            if (StringUtils.isNotBlank(i.getDiscountId())) {
                r.setDiscountId(Integer.parseInt(i.getDiscountId()));
            }
            if (StringUtils.isNotBlank(i.getListingId())) {
                r.setMenuId(Integer.parseInt(i.getListingId()));
            }
            r.setSpecialRequest(i.getRemarks());

            List<Integer> mcIds = new ArrayList<>();
            List<Integer> mcQtys = new ArrayList<>();
            if (i.getMainCourses() != null) {
                for (SkyDiningFunCartMainCourse mc : i.getMainCourses()) {
                    if (StringUtils.isNotBlank(mc.getMenuId())) {
                        mcIds.add(Integer.parseInt(mc.getMenuId()));
                        mcQtys.add(mc.getQty());
                    }
                }
            }
            r.setMainCourseIds(mcIds);
            r.setMainCourseQuantities(mcQtys);

            List<Integer> tuIds = new ArrayList<>();
            List<Integer> tuQtys = new ArrayList<>();
            if (i.getTopUps() != null) {
                for (SkyDiningFunCartTopUp tu : i.getTopUps()) {
                    if (StringUtils.isNotBlank(tu.getTopUpId())) {
                        tuIds.add(Integer.parseInt(tu.getTopUpId()));
                        tuQtys.add(tu.getQty());
                    }
                }
            }
            r.setTopUpIds(tuIds);
            r.setTopUpQuantities(tuQtys);

            r.setMerchantId(i.getMerchantId());
            r.setPromoCode(i.getPromoCode());
            r.setJewelCard(i.getJewelCard());
            r.setJewelCardExpiry(i.getJewelCardExpiry());

            r.setPax(i.getQty());
        }
        return r;
    }

    @Transactional
    protected SkyDiningCartResult saveCart(ReservationModel model,
                                           String internalSid) {

        final SkyDiningCartResult validationResult = validateReservation(model);
        if (validationResult != SkyDiningCartResult.Success) {
            return validationResult;
        }

        SkyDiningCart entity = cartRepo.findOne(model.getId());

        final Date timestamp = new Date();

        if (entity == null) {
            entity = new SkyDiningCart();
            entity.setId(model.getId());
            entity.setWebSessionId(internalSid);
            entity.setCreatedDate(timestamp);
        }

        if (model.getGuestName() != null) {
            entity.setGuestName(model.getGuestName());
        }

        if (model.getContactNumber() != null) {
            entity.setContactNumber(model.getContactNumber());
        }

        if (model.getEmailAddress() != null) {
            entity.setEmailAddress(model.getEmailAddress());
        }

        if (model.getDate() != null) {
            entity.setDate(model.getDate());
        }

        if (model.getTime() != null) {
            entity.setTime(model.getTime());
        }

        if (model.getPax() != null) {
            entity.setPax(model.getPax());
        }

        if (model.getSpecialRequest() != null) {
            entity.setSpecialRequest(model.getSpecialRequest());
        }

        if (model.getType() != null) {
            entity.setType(model.getType());
        }

        if (model.getStatus() != null) {
            entity.setStatus(model.getStatus());
        }

        if (model.getPackageId() != null) {
            if (model.getPackageId() > 0) {
                final SkyDiningPackage sdPackage = packageRepo.find(model
                    .getPackageId());
                entity.setPackageId(sdPackage.getId());
            }
        }

        if (model.getSessionId() != null) {
            if (model.getSessionId() > 0) {
                final SkyDiningSession session = sessionRepo.find(model
                    .getSessionId());
                entity.setSessionId(session.getId());
            }
        }

        if (model.getThemeId() != null) {
            if (model.getThemeId() > 0) {
                final SkyDiningTheme theme = themeRepo.find(model.getThemeId());
                entity.setThemeId(theme.getId());
            } else {
                entity.setThemeId(null);
            }
        }

        if (model.getMenuId() != null) {
            if (model.getMenuId() > 0) {
                final SkyDiningMenu menu = menuRepo.find(model.getMenuId());
                entity.setMenuId(menu.getId());
            }
        }

        if ((model.getMainCourseIds() != null)
            && (model.getMainCourseQuantities() != null)) {
            final List<SkyDiningCartMainCourse> mainCourses = entity
                .getMainCourses();
            final List<Integer> newIds = model.getMainCourseIds();
            final List<Integer> newQuantities = model.getMainCourseQuantities();

            final Iterator<SkyDiningCartMainCourse> iterator = mainCourses
                .iterator();
            while (iterator.hasNext()) {
                final SkyDiningCartMainCourse mainCourse = iterator.next();
                boolean removeFlag = true;
                for (final Integer newId : newIds) {
                    if (newId == mainCourse.getMainCourseId()) {
                        removeFlag = false;
                        break;
                    }
                }
                if (removeFlag) {
                    // Remove missing
                    iterator.remove();
                }
            }

            for (int i = 0; i < newIds.size(); i++) {
                final Integer newId = newIds.get(i);
                final Integer newQuantity = newQuantities.get(i);
                boolean newFlag = true;

                for (final SkyDiningCartMainCourse mainCourse : mainCourses) {
                    if (newId == mainCourse.getMainCourseId()) {
                        // Update existing
                        mainCourse.setQuantity(newQuantity);
                        newFlag = false;
                        break;
                    }
                }

                if (newFlag) {
                    final SkyDiningMainCourse mainCourse = mainCourseRepo.find(newId);
                    if (mainCourse != null) {
                        final SkyDiningCartMainCourse resMainCourse = new SkyDiningCartMainCourse();
                        resMainCourse.setId(0);
                        resMainCourse.setMainCourseId(mainCourse.getId());
                        resMainCourse.setQuantity(newQuantity);

                        // Add new
                        entity.getMainCourses().add(resMainCourse);
                    }
                }

            }
        }

        if ((model.getTopUpIds() != null)
            && (model.getTopUpQuantities() != null)) {
            final List<SkyDiningCartTopUp> topUps = entity.getTopUps();
            final List<Integer> newIds = model.getTopUpIds();
            final List<Integer> newQuantities = model.getTopUpQuantities();

            final Iterator<SkyDiningCartTopUp> iterator = topUps.iterator();
            while (iterator.hasNext()) {
                final SkyDiningCartTopUp topUp = iterator.next();
                boolean removeFlag = true;
                for (final Integer newId : newIds) {
                    if (newId == topUp.getTopUpId()) {
                        removeFlag = false;
                        break;
                    }
                }
                if (removeFlag) {
                    // Remove missing
                    iterator.remove();
                }
            }

            for (int i = 0; i < newIds.size(); i++) {
                final Integer newId = newIds.get(i);
                final Integer newQuantity = newQuantities.get(i);
                boolean newFlag = true;

                for (final SkyDiningCartTopUp topUp : topUps) {
                    if (newId == topUp.getTopUpId()) {
                        // Update existing
                        topUp.setQuantity(newQuantity);
                        newFlag = false;
                        break;
                    }
                }

                if (newFlag) {
                    final SkyDiningTopUp topUp = topUpRepo.find(newId);
                    if (topUp != null) {
                        final SkyDiningCartTopUp resTopUp = new SkyDiningCartTopUp();
                        resTopUp.setId(0);
                        resTopUp.setTopUpId(topUp.getId());
                        resTopUp.setQuantity(newQuantity);

                        // Add new
                        entity.getTopUps().add(resTopUp);
                    }
                }

            }
        }

        entity.setModifiedDate(timestamp);

        if (model.getDiscountId() != null) {
            final SkyDiningDiscount discount = discountRepo.find(model
                .getDiscountId());
            if (discount == null) {
                return SkyDiningCartResult.ValidationFailed;
            }

            final SkyDiningMenu menu = menuRepo.find(model.getMenuId());
            if (menu == null) {
                return SkyDiningCartResult.ValidationFailed;
            }

            entity.setDiscountId(model.getDiscountId());
            entity.setPriceType(discount.getPriceType());
            entity.setDiscountPrice(discount.getDiscountPrice());
            entity.setOriginalPrice(menu.getPrice());
        }
        if (model.getJewelCard() != null) {
            entity.setJewelCard(model.getJewelCard());
        }
        if (model.getJewelCardExpiry() != null) {
            entity.setJewelCardExpiry(model.getJewelCardExpiry());
        }
        if (model.getPromoCode() != null) {
            entity.setPromoCode(model.getPromoCode());
        }
        if (model.getMerchantId() != null) {
            entity.setMerchantId(model.getMerchantId());
        }

        // Clear cart before saving
        cartRepo.deleteByWebSessionId(internalSid);

        cartRepo.save(entity);

        return SkyDiningCartResult.Success;
    }

    @Transactional(readOnly = true)
    protected SkyDiningCartResult validateReservation(ReservationModel reservation) {
        Date dateOfVisit = null;
        try {
            dateOfVisit = NvxDateUtils.parseDate(reservation.getDateText(),
                SysConst.DEFAULT_DATE_FORMAT);
        } catch (final ParseException e) {
            log.info("Unable to parse date: " + reservation.getDateText());
            return SkyDiningCartResult.ValidationFailed;
        }

        if ((reservation.getPackageId() == null)
            || (reservation.getSessionId() == null)) {
            return SkyDiningCartResult.ValidationFailed;
        }

        final Date dateOfBooking = new Date();
        boolean hasValidSession = false;
        final List<SkyDiningSession> sessions = sessionRepo.findAvailable(
            dateOfBooking, dateOfVisit, reservation.getPackageId());

        for (final SkyDiningSession session : sessions) {
            if (session.getId() == reservation.getSessionId()) {
                final long booked = reservationRepo.countBySessionIdAndDate(
                    reservation.getSessionId(), dateOfVisit);
                if (booked >= session.getCapacity()) {
                    return SkyDiningCartResult.FullSessionCapacity;
                }
                hasValidSession = true;
                break;
            }
        }
        if (!hasValidSession) {
            return SkyDiningCartResult.ValidationFailed;
        }

        final SkyDiningPackage sdPackage = packageRepo.find(reservation
            .getPackageId());
        if (sdPackage == null) {
            return SkyDiningCartResult.ValidationFailed;
        }

        if (!isWithinCutoff(dateOfBooking, dateOfVisit,
            sdPackage.getSalesCutOffDays())) {
            return SkyDiningCartResult.PackageAfterCutOff;
        }

        if (sdPackage.getThemes().size() > 0) {
            if ((reservation.getThemeId() == null)
                || (reservation.getThemeId() < 0)) {
                return SkyDiningCartResult.ValidationFailed;
            }

            boolean hasValidTheme = false;
            final List<SkyDiningTheme> themes = sdPackage.getThemes();

            for (final SkyDiningTheme theme : themes) {
                if (theme.getId() == reservation.getThemeId()) {
                    final long booked = reservationRepo.countByThemeIdAndSessionIdAndDate(
                        reservation.getThemeId(),
                        reservation.getSessionId(), dateOfVisit);
                    if (booked >= theme.getCapacity()) {
                        return SkyDiningCartResult.FullThemeCapacity;
                    }
                    hasValidTheme = true;
                    break;
                }
            }
            if (!hasValidTheme) {
                return SkyDiningCartResult.ValidationFailed;
            }
        }

        boolean hasValidMenu = false;
        List<SkyDiningMenu> menus = null;
        if ((reservation.getDiscountId() != null)
            && (reservation.getDiscountId() > 0)) {
            boolean hasValidDiscount = false;
            final List<SkyDiningDiscount> discounts = sdPackage.getDiscounts();
            for (final SkyDiningDiscount discount : discounts) {
                if (SkyDiningDiscountType.EarlyBird.name().equals(discount.getType())) {
                    if (BooleanUtils.isTrue(discount.getActive() && NvxDateUtils.inDateRangeFlexible(null, sdPackage.getEarlyBirdEndDate(), new Date(), true, true))) {
                        hasValidDiscount = true;
                        menus = discount.getMenus();
                        break;
                    }
                } else if (BooleanUtils.isTrue(discount.getActive())
                    && NvxDateUtils.inDateRangeFlexible(discount.getValidFrom(),
                    discount.getValidTo(), dateOfBooking, true,
                    true)
                    && (discount.getId() == reservation.getDiscountId())) {
                    if (discount.getMerchantId() != null) {
                        if ((reservation.getMerchantId() != null)
                            && (reservation.getMerchantId().equals(discount
                            .getMerchantId()))) {
                            hasValidDiscount = true;
                            menus = discount.getMenus();
                            break;
                        }
                    } else {
                        hasValidDiscount = true;
                        menus = discount.getMenus();
                        break;
                    }
                }
            }
            if (!hasValidDiscount) {
                return SkyDiningCartResult.ValidationFailed;
            }
        } else {
            menus = sdPackage.getMenus();
        }

        for (final SkyDiningMenu menu : menus) {
            if (menu.getId() == reservation.getMenuId()) {
                if (!isWithinCutoff(dateOfBooking, dateOfVisit,
                    menu.getSalesCutOffDays())) {
                    return SkyDiningCartResult.MenuAfterCutOff;
                }

                final List<SkyDiningMainCourse> mainCourses = menu
                    .getMainCourses();
                final List<Integer> resMcs = reservation.getMainCourseIds();
                for (final Integer resMc : resMcs) {
                    boolean hasValidMainCourse = false;
                    for (final SkyDiningMainCourse mainCourse : mainCourses) {
                        if (mainCourse.getId() == resMc) {
                            hasValidMainCourse = true;
                            break;
                        }
                    }
                    if (!hasValidMainCourse) {
                        return SkyDiningCartResult.ValidationFailed;
                    }
                }

                hasValidMenu = true;
                break;
            }
        }
        if (!hasValidMenu) {
            return SkyDiningCartResult.ValidationFailed;
        }

        int pax = 0;
        final List<Integer> resMcQtys = reservation.getMainCourseQuantities();
        for (final Integer resMcQty : resMcQtys) {
            pax += resMcQty;
        }
        if ((pax < sdPackage.getMinPax()) || (pax > sdPackage.getMaxPax())) {
            return SkyDiningCartResult.ValidationFailed;
        }

        final List<SkyDiningTopUp> topUps = sdPackage.getTopUps();
        final List<Integer> resTus = reservation.getTopUpIds();
        for (final Integer resTu : resTus) {
            boolean hasValidTopUp = false;
            for (final SkyDiningTopUp topUp : topUps) {
                if (topUp.getId() == resTu) {
                    if (!isWithinCutoff(dateOfBooking, dateOfVisit,
                        topUp.getSalesCutOffDays())) {
                        return SkyDiningCartResult.TopUpAfterCutOff;
                    }

                    hasValidTopUp = true;
                    break;
                }
            }
            if (!hasValidTopUp) {
                return SkyDiningCartResult.ValidationFailed;
            }
        }

        final List<Integer> resTuQtys = reservation.getTopUpQuantities();
        for (final Integer resTuQty : resTuQtys) {
            if (resTuQty > 10) {
                return SkyDiningCartResult.ValidationFailed;
            }
        }

        if (reservation.getMainCourseQuantities().size() != reservation
            .getMainCourseIds().size()) {
            return SkyDiningCartResult.ValidationFailed;
        }

        if (reservation.getTopUpQuantities().size() != reservation
            .getTopUpIds().size()) {
            return SkyDiningCartResult.ValidationFailed;
        }

        return SkyDiningCartResult.Success;
    }

    private boolean isWithinCutoff(Date bookingDate, Date visitDate,
                                   Integer minBookingDays) {
        final Calendar bookingDateCalendar = Calendar.getInstance();
        final Calendar minBookingDateCalendar = Calendar.getInstance();
        minBookingDateCalendar.setTime(visitDate);
        if ((minBookingDays == null) || (minBookingDays == 0)) {

            bookingDateCalendar.setTime(bookingDate);

            if (bookingDateCalendar.get(Calendar.AM_PM) == Calendar.PM) {
                minBookingDateCalendar.add(Calendar.DATE, -1);
            }
        } else {
            minBookingDateCalendar.add(Calendar.DATE, minBookingDays * -1);
        }
        return DateUtils.isSameDay(bookingDateCalendar, minBookingDateCalendar)
            || bookingDate.before(minBookingDateCalendar.getTime());
    }

    protected SkyDiningCartResult validateCart(int cartId) {
        // No need to implement unless needed
        return SkyDiningCartResult.Success;
    }

    protected boolean validateCustomerDetails(CustomerDetails cd) {
        if (StringUtils.isEmpty(cd.getEmail()) || StringUtils.isEmpty(cd.getName()) || StringUtils.isEmpty(cd.getPaymentType())) {
            cd.setIsErr(true);
            cd.setErrMsg("One or more customer details are invalid.");
            return false;
        }
        if (!ValidationUtil.validateEmail(cd.getEmail())) {
            cd.setIsErr(true);
            cd.setErrMsg("One or more customer details are invalid.");
            return false;
        }
        return true;
    }

    @Transactional
    protected String generateSequence() {
        final Date now = new Date();

        //Check if there are existing sequences for today.
        final long seqForToday = seqLogRepo.countByModifiedDate(now);

        //Reset using truncate table if all sequences are stale
        if (seqForToday == 0) {
            seqLogRepo.deleteAll();
        }

        SkyDiningSequenceLog seqLog = new SkyDiningSequenceLog();
        seqLog.setSeqId(NvxUtil.generateRandomAlphaNumeric(5, true));
        seqLog.setModifiedDate(now);
        log.info("Inserting new unique sequence into log " + seqLog.getSeqId());
        seqLogRepo.save(seqLog);

        return seqLog.getSeqId();
    }

    protected String formatReceiptNumber(String prefix, Date date, String sequence) {
        final DateFormat sdf = new SimpleDateFormat(RECEIPT_DATE_FORMAT);
        return prefix + sdf.format(date) + sequence;
    }

    protected SkyDiningReservation createReservationFromCart(SkyDiningCart cart,
                                                             SkyDiningTransaction transaction) {
        try {
            final Date now = new Date();
            final SkyDiningReservation reservation = new SkyDiningReservation();

            reservation.setContactNumber(transaction.getCustMobile());
            reservation.setCreatedBy(SysConst.SYSTEM);
            reservation.setCreatedDate(now);
            reservation.setDate(cart.getDate());
            reservation.setEmailAddress(transaction.getCustEmail());
            String guestName = null;
            if (StringUtils.isNotEmpty(transaction.getCustSalutation())) {
                guestName = transaction.getCustSalutation() + " "
                    + transaction.getCustName();
            } else {
                guestName = transaction.getCustName();
            }
            reservation.setGuestName(guestName);
            reservation.setId(0);
            reservation.setModifiedBy(SysConst.SYSTEM);
            reservation.setModifiedDate(now);
            reservation.setMenuId(cart.getMenuId());
            reservation.setPackageId(cart.getPackageId());
            reservation.setSessionId(cart.getSessionId());
            reservation.setThemeId(cart.getThemeId());
            reservation.setSpecialRequest(cart.getSpecialRequest());
            reservation.setStatus(cart.getStatus());
            reservation.setTime(cart.getTime());
            reservation.setType(ReservationType.Online.value);

            int pax = 0;
            final List<SkyDiningResMainCourse> resMcs = new ArrayList<SkyDiningResMainCourse>();
            for (final SkyDiningCartMainCourse cartMc : cart.getMainCourses()) {
                final SkyDiningResMainCourse resMc = new SkyDiningResMainCourse();
                resMc.setCreatedBy(SysConst.SYSTEM);
                resMc.setCreatedDate(now);
                resMc.setId(0);
                resMc.setModifiedBy(SysConst.SYSTEM);
                resMc.setModifiedDate(now);
                resMc.setQuantity(cartMc.getQuantity());
                resMc.setMainCourseId(cartMc.getMainCourseId());

                resMcs.add(resMc);
                pax += resMc.getQuantity();
            }
            reservation.setPax(pax);
            reservation.setMainCourses(resMcs);

            final List<SkyDiningResTopUp> resTus = new ArrayList<SkyDiningResTopUp>();
            for (final SkyDiningCartTopUp cartTu : cart.getTopUps()) {
                final SkyDiningResTopUp resTu = new SkyDiningResTopUp();

                resTu.setCreatedBy(SysConst.SYSTEM);
                resTu.setCreatedDate(now);
                resTu.setId(0);
                resTu.setModifiedBy(SysConst.SYSTEM);
                resTu.setModifiedDate(now);
                resTu.setQuantity(cartTu.getQuantity());
                resTu.setTopUpId(cartTu.getTopUpId());

                resTus.add(resTu);
            }
            reservation.setTopUps(resTus);

            reservation.setDiscountId(cart.getDiscountId());
            reservation.setJewelCard(cart.getJewelCard());
            reservation.setJewelCardExpiry(cart.getJewelCardExpiry());
            reservation.setPromoCode(cart.getPromoCode());
            reservation.setMerchantId(cart.getMerchantId());
            reservation.setPriceType(cart.getPriceType());
            reservation.setDiscountPrice(cart.getDiscountPrice());
            reservation.setOriginalPrice(cart.getOriginalPrice());

            if (cart.getPackageId() != null) {
                final SkyDiningPackage sdPackage = packageRepo.find(cart.getPackageId());
                if (packageRepo != null) {
                    reservation.setPackageName(sdPackage.getName());
                }
            }

            reservationRepo.save(reservation);

            if ((reservation.getDiscountId() != null)
                && StringUtils.isNotEmpty(reservation.getPromoCode())) {
                final SkyDiningDiscount discount = discountRepo.find(reservation.getDiscountId());
                if (discount != null) {
                    if (SkyDiningDiscountType.valueOf(discount.getType()) == SkyDiningDiscountType.PromoCode) {
                        promoCodeRepo.updatePromoCodeUseCount(reservation
                            .getPromoCode(), reservation
                            .getPackageId(), 1, true);
                    }
                }
            }

            return reservation;
        } catch (final Exception e) {
            log.error("Error creating reservation from cart.", e);
            return null;
        }
    }

    @Transactional(readOnly = true)
    protected BigDecimal computeTotalAmount(SkyDiningCart cart) {
        final int numberofPax = countNumberOfPax(cart.getMainCourses());

        final SkyDiningMenu selectedMenu = menuRepo.find(cart.getMenuId());
        if (selectedMenu != null) {
            int unitPriceInCents = selectedMenu.getPrice();
            if (cart.getDiscountId() != null) {
                final SkyDiningDiscount discount = discountRepo.find(cart.getDiscountId());
                if (discount != null) {
                    unitPriceInCents = getDiscountUnitPriceInCents(discount, selectedMenu);
                }
            }

            final BigDecimal menuTotal = NvxNumberUtils.centsToMoney(unitPriceInCents
                * numberofPax);

            final BigDecimal serviceChargeTotal = menuTotal
                .multiply(new BigDecimal(selectedMenu.getServiceCharge()).divide(new BigDecimal(100)));
            final BigDecimal gstTotal = menuTotal.add(serviceChargeTotal).multiply(
                new BigDecimal(selectedMenu.getGst())
                    .divide(new BigDecimal(100)));

            BigDecimal topUpTotal = new BigDecimal(0);
            for (final SkyDiningCartTopUp topUp : cart.getTopUps()) {
                final SkyDiningTopUp selectedTopUp = topUpRepo.find(topUp.getTopUpId());
                topUpTotal = topUpTotal.add(NvxNumberUtils.centsToMoney(selectedTopUp.getPrice() * topUp.getQuantity()));
            }

            return menuTotal.add(gstTotal).add(serviceChargeTotal).add(topUpTotal);
        }

        return BigDecimal.ZERO;
    }

    protected int countNumberOfPax(List<SkyDiningCartMainCourse> mainCourses) {
        int total = 0;

        if (mainCourses != null) {
            for (final SkyDiningCartMainCourse mainCourse : mainCourses) {
                total += mainCourse.getQuantity();
            }
        }

        return total;
    }

    protected BigDecimal getDiscountUnitPrice(SkyDiningDiscount discount,
                                              SkyDiningMenu menu) {
        final int priceInCents = menu.getPrice();
        BigDecimal unitPrice = NvxNumberUtils.centsToMoney(priceInCents);
        if ((discount.getPriceType() != null)
            && (discount.getDiscountPrice() != null)) {
            switch (discount.getPriceType()) {
                case 1:
                    unitPrice = NvxNumberUtils.percentOff(menu.getPrice(),
                        discount.getDiscountPrice());
                    break;
                case 2:
                    unitPrice = NvxNumberUtils.centsToMoney(menu.getPrice()
                        - discount.getDiscountPrice());
                    break;
                case 3:
                    unitPrice = NvxNumberUtils.centsToMoney(discount.getDiscountPrice());
                    break;
                default:
                    break;
            }
        }
        return unitPrice;
    }

    protected Integer getDiscountUnitPriceInCents(SkyDiningDiscount discount,
                                                  SkyDiningMenu menu) {
        Integer unitPrice = menu.getPrice();
        if ((discount.getPriceType() != null)
            && (discount.getDiscountPrice() != null)) {
            switch (discount.getPriceType()) {
                case 1:
                    unitPrice = NvxNumberUtils.percentOffInCents(unitPrice,
                        discount.getDiscountPrice());
                    break;
                case 2:
                    unitPrice = unitPrice - discount.getDiscountPrice();
                    break;
                case 3:
                    unitPrice = discount.getDiscountPrice();
                    break;
                default:
                    break;
            }
        }
        return unitPrice;
    }

    @Transactional(readOnly = true)
    protected SkyDiningFunCartDisplay constructCartDisplay(SkyDiningCart cart) {
        SkyDiningMenu selectedMenu = menuRepo.find(cart.getMenuId());
        SkyDiningPackage selectedPackage = packageRepo.find(cart.getPackageId());
        SkyDiningSession selectedSession = sessionRepo.find(cart.getSessionId());
        SkyDiningTheme selectedTheme = null;
        if (cart.getThemeId() != null) {
            selectedTheme = themeRepo.find(cart.getThemeId());
        }

        final SkyDiningFunCartDisplay cd = new SkyDiningFunCartDisplay();
        cd.setCartId(Integer.toString(cart.getId()));

        Integer totalMainQty = 0;
        Integer totalQty = 0;

        List<SkyDiningFunCartDisplayProduct> products = new ArrayList<>();
        SkyDiningFunCartDisplayItem displayItem = new SkyDiningFunCartDisplayItem();
        int realQty = 0;

        StringBuilder mainCourseText = new StringBuilder();

        for (SkyDiningCartMainCourse mc : cart.getMainCourses()) {
            final SkyDiningMainCourse selectedMainCourse = mainCourseRepo.find(mc.getMainCourseId());

            final SkyDiningFunCartDisplayMainCourse displayMc = new SkyDiningFunCartDisplayMainCourse();
            displayMc.setMenuId(Integer.toString(selectedMainCourse.getId()));
            displayMc.setMenuName(selectedMainCourse.getDescription());
            displayMc.setQty(mc.getQuantity());

            displayItem.getMainCourses().add(displayMc);
            if (mainCourseText.length() > 0) {
                mainCourseText.append("<br/>");
            }
            mainCourseText.append(mc.getQuantity());
            mainCourseText.append("x ");
            mainCourseText.append(selectedMainCourse.getDescription());

            realQty += mc.getQuantity();
            totalMainQty += mc.getQuantity();
            totalQty += mc.getQuantity();
        }
        displayItem.setQty(realQty);
        if (StringUtils.isNotBlank(mainCourseText.toString())) {
            displayItem.setHasMainCourse(true);
        }
        displayItem.setMainCourseText(mainCourseText.toString());

        displayItem.setName(selectedMenu.getName());
        displayItem.setCartItemId(Integer.toString(selectedPackage.getId()));

        final List<TncVM> tncs = new ArrayList<>();
        if (selectedPackage.getTncs() != null && !selectedPackage.getTncs().isEmpty() && selectedPackage.getSelectedTncId() != null) {
            for (SkyDiningTnc tnc : selectedPackage.getTncs()) {
                if (selectedPackage.getSelectedTncId().equals(tnc.getId())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(tnc.getTncContent());
                    tncVM.setTitle(tnc.getTitle());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        }

        //Default discount values
        displayItem.setHasDiscount(false);
        displayItem.setDiscountName("");
        displayItem.setDiscountLabel("");
        displayItem.setDiscountTotal(new BigDecimal(0));
        displayItem.setDiscountTotalText("");

        if (cart.getDiscountId() != null) {
            SkyDiningDiscount discount = discountRepo.find(cart.getDiscountId());
            if (discount != null) {
                displayItem.setHasDiscount(true);
                displayItem.setDiscountName(discount.getName());
                displayItem.setPrice(getDiscountUnitPrice(discount, selectedMenu));
                displayItem.setDiscountTotal(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()).subtract(displayItem.getPrice()).multiply(new BigDecimal(realQty)));
                displayItem.setDiscountTotalText(NvxNumberUtils.formatToCurrency(displayItem.getDiscountTotal()));
                if (StringUtils.isNotBlank(discount.getTnc())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(discount.getTnc());
                    tncVM.setTitle(discount.getName());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        } else {
            displayItem.setPrice(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()));
        }
        displayItem.setPriceText(NvxNumberUtils.formatToCurrency(displayItem.getPrice()));

        displayItem.setTotal(displayItem.getPrice().multiply(new BigDecimal(realQty)));
        displayItem.setTotalText(NvxNumberUtils.formatToCurrency(displayItem.getTotal()));


        if (StringUtils.isNotBlank(cart.getSpecialRequest())) {
            displayItem.setHasRemarks(true);
        }
        displayItem.setRemarks(cart.getSpecialRequest());
        if (selectedTheme != null) {
            displayItem.setThemeId(Integer.toString(selectedTheme.getId()));
            displayItem.setThemeName(selectedTheme.getDescription());
        }

        StringBuilder description = new StringBuilder();
        description.append("Date of Visit: " + NvxDateUtils.formatDate(cart.getDate(), SysConst.DEFAULT_DATE_FORMAT));
        description.append("<br/>");
        description.append("Session Time: " + selectedSession.getTime());
        if (selectedTheme != null) {
            description.append("<br/>");
            description.append("Theme: ");
            description.append(selectedTheme.getDescription());
        }

        displayItem.setDescription(description.toString());

        SkyDiningFunCartDisplayProduct product = new SkyDiningFunCartDisplayProduct();
        product.setId(Integer.toString(selectedPackage.getId()));
        product.setName(selectedPackage.getName());

        product.getSkyDiningItems().add(displayItem);

        BigDecimal subTotal = displayItem.getTotal();
        BigDecimal topUpPrice = BigDecimal.ZERO;

        if (cart.getTopUps() != null) {
            for (SkyDiningCartTopUp ctu : cart.getTopUps()) {
                SkyDiningTopUp tu = topUpRepo.find(ctu.getTopUpId());
                if (tu != null) {
                    SkyDiningFunCartDisplayItem di = new SkyDiningFunCartDisplayItem();
                    di.setIsTopup(true);
                    di.setName(tu.getName());
                    di.setPrice(NvxNumberUtils.centsToMoney(tu.getPrice()));
                    di.setPriceText(NvxNumberUtils.formatToCurrency(di.getPrice()));
                    di.setCartItemId(Integer.toString(tu.getId()));
                    di.setQty(ctu.getQuantity());
                    di.setTotal(NvxNumberUtils.centsToMoney(tu.getPrice()).multiply(new BigDecimal(ctu.getQuantity())));
                    di.setTotalText(NvxNumberUtils.formatToCurrency(di.getTotal()));

                    product.getSkyDiningItems().add(di);
                    // subTotal.add(di.getTotal());
                    topUpPrice = topUpPrice.add(di.getTotal());
                    totalQty += ctu.getQuantity();
                }
            }
        }

        product.setSubtotal(subTotal);
        product.setSubtotalText(NvxNumberUtils.formatToCurrency(subTotal, ""));

        product.setTopUpPrice(topUpPrice);
        product.setTopUpPriceText(NvxNumberUtils.formatToCurrency(topUpPrice, ""));

        product.setServiceCharge(selectedMenu.getServiceCharge());
        product.setServiceChargeTotal(subTotal.multiply(new BigDecimal(selectedMenu.getServiceCharge())).divide(new BigDecimal(100)));
        product.setServiceChargeTotalText(NvxNumberUtils.formatToCurrency(product.getServiceChargeTotal(), ""));

        product.setGst(selectedMenu.getGst());
        product.setGstTotal(subTotal.add(product.getServiceChargeTotal()).multiply(new BigDecimal(selectedMenu.getGst())).divide(new BigDecimal(100)));
        product.setGstTotalText(NvxNumberUtils.formatToCurrency(product.getGstTotal(), ""));

        products.add(product);

        BigDecimal total = product.getSubtotal().add(product.getServiceChargeTotal()).add(product.getGstTotal()).add(product.getTopUpPrice());

        cd.setSkyDiningProducts(products);
        cd.setTncs(tncs);
        cd.setHasTnc(tncs != null && !tncs.isEmpty());
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        return cd;
    }

    @Transactional(readOnly = true)
    protected SkyDiningFunCartDisplay constructCartDisplayFromReservation(SkyDiningReservation res) {
        SkyDiningMenu selectedMenu = menuRepo.find(res.getMenuId());
        SkyDiningPackage selectedPackage = packageRepo.find(res.getPackageId());
        SkyDiningSession selectedSession = sessionRepo.find(res.getSessionId());
        SkyDiningTheme selectedTheme = null;
        if (res.getThemeId() != null) {
            selectedTheme = themeRepo.find(res.getThemeId());
        }

        final SkyDiningFunCartDisplay cd = new SkyDiningFunCartDisplay();
        cd.setCartId(Integer.toString(res.getId()));

        Integer totalMainQty = 0;
        Integer totalQty = 0;

        List<SkyDiningFunCartDisplayProduct> products = new ArrayList<>();
        SkyDiningFunCartDisplayItem displayItem = new SkyDiningFunCartDisplayItem();
        int realQty = 0;

        StringBuilder mainCourseText = new StringBuilder();

        for (SkyDiningResMainCourse mc : res.getMainCourses()) {
            final SkyDiningMainCourse selectedMainCourse = mainCourseRepo.find(mc.getMainCourseId());

            final SkyDiningFunCartDisplayMainCourse displayMc = new SkyDiningFunCartDisplayMainCourse();
            displayMc.setMenuId(Integer.toString(selectedMainCourse.getId()));
            displayMc.setMenuName(selectedMainCourse.getDescription());
            displayMc.setQty(mc.getQuantity());

            displayItem.getMainCourses().add(displayMc);
            if (mainCourseText.length() > 0) {
                mainCourseText.append("<br/>");
            }
            mainCourseText.append(mc.getQuantity());
            mainCourseText.append("x ");
            mainCourseText.append(selectedMainCourse.getDescription());

            realQty += mc.getQuantity();
            totalMainQty += mc.getQuantity();
            totalQty += mc.getQuantity();
        }
        displayItem.setQty(realQty);
        if (StringUtils.isNotBlank(mainCourseText.toString())) {
            displayItem.setHasMainCourse(true);
        }
        displayItem.setMainCourseText(mainCourseText.toString());

        displayItem.setName(selectedMenu.getName());
        displayItem.setCartItemId(Integer.toString(selectedPackage.getId()));

        final List<TncVM> tncs = new ArrayList<>();
        if (selectedPackage.getTncs() != null && !selectedPackage.getTncs().isEmpty() && selectedPackage.getSelectedTncId() != null) {
            for (SkyDiningTnc tnc : selectedPackage.getTncs()) {
                if (selectedPackage.getSelectedTncId().equals(tnc.getId())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(tnc.getTncContent());
                    tncVM.setTitle(tnc.getTitle());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        }

        //Default discount values
        displayItem.setHasDiscount(false);
        displayItem.setDiscountName("");
        displayItem.setDiscountLabel("");
        displayItem.setDiscountTotal(new BigDecimal(0));
        displayItem.setDiscountTotalText("");

        if (res.getDiscountId() != null) {
            SkyDiningDiscount discount = discountRepo.find(res.getDiscountId());
            if (discount != null) {
                displayItem.setHasDiscount(true);
                displayItem.setDiscountName(discount.getName());
                displayItem.setPrice(getDiscountUnitPrice(discount, selectedMenu));
                displayItem.setDiscountTotal(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()).subtract(displayItem.getPrice()).multiply(new BigDecimal(realQty)));
                displayItem.setDiscountTotalText(NvxNumberUtils.formatToCurrency(displayItem.getDiscountTotal()));
                if (StringUtils.isNotBlank(discount.getTnc())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(discount.getTnc());
                    tncVM.setTitle(discount.getName());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        } else {
            displayItem.setPrice(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()));
        }
        displayItem.setPriceText(NvxNumberUtils.formatToCurrency(displayItem.getPrice()));

        displayItem.setTotal(displayItem.getPrice().multiply(new BigDecimal(realQty)));
        displayItem.setTotalText(NvxNumberUtils.formatToCurrency(displayItem.getTotal()));


        if (StringUtils.isNotBlank(res.getSpecialRequest())) {
            displayItem.setHasRemarks(true);
        }
        displayItem.setRemarks(res.getSpecialRequest());
        if (selectedTheme != null) {
            displayItem.setThemeId(Integer.toString(selectedTheme.getId()));
            displayItem.setThemeName(selectedTheme.getDescription());
        }

        StringBuilder description = new StringBuilder();
        description.append("Date of Visit: " + NvxDateUtils.formatDate(res.getDate(), SysConst.DEFAULT_DATE_FORMAT));
        description.append("<br/>");
        description.append("Session Time: " + selectedSession.getTime());
        if (selectedTheme != null) {
            description.append("<br/>");
            description.append("Theme: ");
            description.append(selectedTheme.getDescription());
        }

        displayItem.setDescription(description.toString());

        SkyDiningFunCartDisplayProduct product = new SkyDiningFunCartDisplayProduct();
        product.setId(Integer.toString(selectedPackage.getId()));
        product.setName(selectedPackage.getName());

        product.getSkyDiningItems().add(displayItem);

        BigDecimal subTotal = displayItem.getTotal();
        BigDecimal topUpPrice = BigDecimal.ZERO;

        if (res.getTopUps() != null) {
            for (SkyDiningResTopUp ctu : res.getTopUps()) {
                SkyDiningTopUp tu = topUpRepo.find(ctu.getTopUpId());
                if (tu != null) {
                    SkyDiningFunCartDisplayItem di = new SkyDiningFunCartDisplayItem();
                    di.setIsTopup(true);
                    di.setName(tu.getName());
                    di.setPrice(NvxNumberUtils.centsToMoney(tu.getPrice()));
                    di.setPriceText(NvxNumberUtils.formatToCurrency(di.getPrice()));
                    di.setCartItemId(Integer.toString(tu.getId()));
                    di.setQty(ctu.getQuantity());
                    di.setTotal(NvxNumberUtils.centsToMoney(tu.getPrice()).multiply(new BigDecimal(ctu.getQuantity())));
                    di.setTotalText(NvxNumberUtils.formatToCurrency(di.getTotal()));

                    product.getSkyDiningItems().add(di);
                    // subTotal.add(di.getTotal());
                    topUpPrice = topUpPrice.add(di.getTotal());
                    totalQty += ctu.getQuantity();
                }
            }
        }

        product.setSubtotal(subTotal);
        product.setSubtotalText(NvxNumberUtils.formatToCurrency(subTotal, ""));

        product.setTopUpPrice(topUpPrice);
        product.setTopUpPriceText(NvxNumberUtils.formatToCurrency(topUpPrice, ""));

        product.setServiceCharge(selectedMenu.getServiceCharge());
        product.setServiceChargeTotal(subTotal.multiply(new BigDecimal(selectedMenu.getServiceCharge())).divide(new BigDecimal(100)));
        product.setServiceChargeTotalText(NvxNumberUtils.formatToCurrency(product.getServiceChargeTotal(), ""));

        product.setGst(selectedMenu.getGst());
        product.setGstTotal(subTotal.add(product.getServiceChargeTotal()).multiply(new BigDecimal(selectedMenu.getGst())).divide(new BigDecimal(100)));
        product.setGstTotalText(NvxNumberUtils.formatToCurrency(product.getGstTotal(), ""));

        products.add(product);

        BigDecimal total = product.getSubtotal().add(product.getServiceChargeTotal()).add(product.getGstTotal()).add(product.getTopUpPrice());

        cd.setSkyDiningProducts(products);
        cd.setTncs(tncs);
        cd.setHasTnc(tncs != null && !tncs.isEmpty());
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        return cd;
    }

    @Override
    public SkyDiningCheckPromoResult checkJewelCard(String jcNumber, String jcExpiry, int productId, boolean getRelatedItems) {
        final SkyDiningJewelCard jc = jcRepo.findUniqueByCardNumber(jcNumber);
        if (jc == null) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.InvalidJewelCard, null);
        }
        if (!NvxDateUtils.formatDate(jc.getExpiryDate(), "MM/yyyy").equals(jcExpiry)) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.InvalidJewelCard, null);
        }
        try {
            final DateTime inputExpiry = new DateTime(NvxDateUtils.parseDate(
                jcExpiry, "MM/yyyy"));
            final DateTime now = new DateTime();
            if ((now.getYear() == inputExpiry.getYear())
                && (inputExpiry.getMonthOfYear() < now.getMonthOfYear())) {
                return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.Expired, null);
            }
        } catch (final ParseException pe) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.Failed, null);
        }

        if (!getRelatedItems) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.Success, null);
        }

        final SkyDiningPackage prod = packageRepo.find(productId);
        if (prod == null) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.Failed, null);
        }

        final List<SkyDiningBookingItem> itemz = new ArrayList<>();
        final Date now = new Date();
        List<SkyDiningDiscount> dcs = prod.getDiscounts();
        for (SkyDiningDiscount dc : dcs) {
            if (SkyDiningDiscountType.JewelCard.toString().equals(dc.getType())) {
                final List<SkyDiningMenu> dims = dc.getMenus();
                for (final SkyDiningMenu dim : dims) {
                    final SkyDiningBookingItem bi = createDiscountBookingItem(dc, dim, now);
                    if (bi != null) {
                        itemz.add(bi);
                    }
                }
            }
        }

        if (itemz.isEmpty()) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.NoRelatedItems, null);
        }

        return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.Success, itemz);
    }

    @Override
    public SkyDiningCheckPromoResult checkPromoCode(String promoCode, int productId, String dateOfVisit, boolean getRelatedItems) {
        final SkyDiningPromoCode pc = promoCodeRepo.checkPromoCode(promoCode,
            productId);
        final Date now = new Date();
        if ((pc == null) || !pc.getActive()
            || (pc.getHasLimit() && (pc.getTimesUsed() == pc.getLimit()))) {
            return new SkyDiningCheckPromoResult(
                SkyDiningCheckPromoResult.Result.InvalidOrMaxed, null);
        }

        final PromoCodeType codeType = PromoCodeType.valueOf(pc.getType());
        switch (codeType) {
            case VisitBased:
                if (StringUtils.isEmpty(dateOfVisit)) {
                    return new SkyDiningCheckPromoResult(
                        SkyDiningCheckPromoResult.Result.OutOfVisitRange, null);
                }
                final Date dov;
                try {
                    dov = NvxDateUtils.parseDate(dateOfVisit,
                        SysConst.DEFAULT_DATE_FORMAT);
                } catch (final Exception e) {
                    return new SkyDiningCheckPromoResult(
                        SkyDiningCheckPromoResult.Result.OutOfVisitRange, null);
                }
                if ((pc.getStartDate() != null) && (pc.getEndDate() != null)) {
                    boolean isInRange = false;
                    if (NvxDateUtils.inDateRange(pc.getStartDate(), pc.getEndDate(),
                        dov, null, true, true)) {
                        isInRange = true;
                    }
                    if (!isInRange) {
                        return new SkyDiningCheckPromoResult(
                            SkyDiningCheckPromoResult.Result.OutOfVisitRange,
                            null);
                    }
                }
                break;
            case BookingBased:
                if ((pc.getStartDate() != null) && (pc.getEndDate() != null)) {
                    if (!NvxDateUtils.inDateRange(pc.getStartDate(), pc.getEndDate(),
                        now, null, true, true)) {
                        return new SkyDiningCheckPromoResult(
                            SkyDiningCheckPromoResult.Result.OutOfBookingRange,
                            null);
                    }
                }
                break;
            default:
                break;
        }

        if (!getRelatedItems) {
            return new SkyDiningCheckPromoResult(
                SkyDiningCheckPromoResult.Result.Success, null);
        }

        final List<SkyDiningBookingItem> itemz = new ArrayList<>();
        final SkyDiningDiscount dc = discountRepo.getParentDiscount(pc);
        if (dc != null) {
            final List<SkyDiningMenu> dims = dc.getMenus();
            for (final SkyDiningMenu dim : dims) {
                final SkyDiningBookingItem bi = createDiscountBookingItem(dc, dim,
                    now);
                if (bi != null) {
                    itemz.add(bi);
                }
            }
        }

        if (itemz.isEmpty()) {
            return new SkyDiningCheckPromoResult(SkyDiningCheckPromoResult.Result.NoRelatedItems, null);
        }

        return new SkyDiningCheckPromoResult(
            SkyDiningCheckPromoResult.Result.Success, itemz);
    }

    private SkyDiningBookingItem createDiscountBookingItem(
        SkyDiningDiscount dc, SkyDiningMenu menu, Date now) {
        final SkyDiningPackage sdPackage = packageRepo.getParentPackage(dc);
        if (!NvxDateUtils.inDateRange(menu.getStartDate(), menu.getEndDate(), now,
            null, true, true)) {
            return null;
        }
        final SkyDiningBookingItem item = new SkyDiningBookingItem();
        final TicketType ticketType = TicketType.Standard;
        item.setDiscountId(dc.getId());
        item.setDiscountItem(true);
        item.setId(menu.getId());
        item.setTicketType(ticketType.toString());
        item.setDisplayName(getItemDisplayTitle(menu.getName(),
            ticketType));
        item.setSubName(dc.getName());
        final int priceInCents = getDiscountUnitPriceInCents(dc, menu);
        final BigDecimal unitPrice = NvxNumberUtils.centsToMoney(priceInCents);
        final String displayPrice = NvxNumberUtils.formatToCurrency(unitPrice
            .multiply(new BigDecimal(sdPackage.getMinPax())));
        item.setUnitPrice(unitPrice);
        item.setDisplayPrice(displayPrice);
        item.setDisplayDesc(menu.getDescription());
        item.setQty(0);

        item.setServiceCharge(menu.getServiceCharge());
        item.setGst(menu.getGst());

        final List<SkyDiningBookingMainCourse> bmcs = new ArrayList<SkyDiningBookingMainCourse>();
        final List<SkyDiningMainCourse> mcs = menu.getMainCourses();
        for (final SkyDiningMainCourse mc : mcs) {
            final SkyDiningBookingMainCourse bmc = new SkyDiningBookingMainCourse();
            bmc.setId(mc.getId());
            bmc.setName(mc.getDescription());
            bmc.setQty(0);
            bmcs.add(bmc);
        }
        item.setHasMainCourses(!bmcs.isEmpty());
        item.setSdMainCourses(bmcs);

        item.setUsualPrice(NvxNumberUtils.formatToCurrency(NvxNumberUtils.centsToMoney(menu
            .getPrice() * sdPackage.getMinPax())));

        if (dc.getMerchantId() != null) {
            item.setMerchantId(dc.getMerchantId());
        } else {
            item.setMerchantId("");
        }
        item.setDiscountId(dc.getId());
        item.setPriceType(dc.getPriceType());
        item.setDiscountPrice(dc.getDiscountPrice());

        return item;
    }

    public static String getItemDisplayTitle(String name, TicketType type) {
        if (!TicketType.Standard.equals(type)) {
            return name + " - " + type.toString();
        }
        return name;
    }

    @Override
    public ApiResult<SkyDiningCalendarResult> getCalendarParams(int packageId) {
        final SkyDiningCalendarResult result = new SkyDiningCalendarResult();
        final SkyDiningPackage sdPackage = packageRepo.find(packageId);

        if (sdPackage.getSalesCutOffDays() == null) {
            sdPackage.setSalesCutOffDays(0);
        }

        final Date now = new Date();
        final Calendar adjustedNow = Calendar.getInstance();
        adjustedNow.setTime(now);
        if (sdPackage.getSalesCutOffDays().equals(0)) {
            if (adjustedNow.get(Calendar.AM_PM) == Calendar.PM) {
                adjustedNow.add(Calendar.DATE, 1);
            }
        } else {
            adjustedNow.add(Calendar.DATE, sdPackage.getSalesCutOffDays());
        }


        Date earliestStartDate = null;
        Date latestEndDate = null;
        final List<SkyDiningSession> sessions = sessionRepo.findAvailable(now, null, packageId);
        for (final SkyDiningSession session : sessions) {
            if (latestEndDate == null
                || session.getEndDate().after(latestEndDate)) {
                latestEndDate = session.getEndDate();
            }
            Date actualStartDate;
            if (session.getStartDate().after(adjustedNow.getTime())) {
                actualStartDate = session.getStartDate();
            } else {
                actualStartDate = adjustedNow.getTime();
            }
            if (earliestStartDate == null
                || earliestStartDate.after(actualStartDate)) {
                earliestStartDate = actualStartDate;
            }
        }

        if (earliestStartDate.before(adjustedNow.getTime())) {
            earliestStartDate = adjustedNow.getTime();
        }

        result.setMinDateArr(convertToDateAray(earliestStartDate));

        // TODO: Implement configuration for months ahead
        final int monthsAhead = 3;
        DateTime maxDate = new DateTime();
        maxDate = maxDate.plusMonths(monthsAhead);

        if (latestEndDate.after(maxDate.toDate())) {
            latestEndDate = maxDate.toDate();
        }

        result.setMaxDateArr(convertToDateAray(latestEndDate));


        final List<Integer[]> blockOutDates = getBlockOutDates(sdPackage,
            sessions, earliestStartDate, latestEndDate);
        result.setBlackoutDatesArr(blockOutDates);

        ApiResult<SkyDiningCalendarResult> apiResult = new ApiResult<>();
        apiResult.setErrorCode("200");
        apiResult.setSuccess(true);
        apiResult.setMessage("");
        apiResult.setData(result);
        return apiResult;
    }

    public static Integer[] convertToDateAray(Date date) {
        final DateTime dt = new DateTime(date);
        final Integer[] dateArr = {dt.getYear(),
            dt.getMonthOfYear() - 1, dt.getDayOfMonth()};
        return dateArr;
    }

    private List<Integer[]> getBlockOutDates(SkyDiningPackage sdPackage, List<SkyDiningSession> sessions,
                                             Date startDate, Date endDate) {

        final List<Integer[]> blockOutDates = new ArrayList<>();
        if ((sessions == null) || sessions.isEmpty()) {
            return blockOutDates;
        }

        final Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        final Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        end.set(Calendar.HOUR_OF_DAY, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);

        final List<SkyDiningSession> allSessions = sessionRepo.findAvailable(null, null, null);

        for (Date date = start.getTime(); !start.after(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            if (!hasAvailableSession(sdPackage, date, allSessions)) {
                blockOutDates.add(convertToDateAray(date));
            }
        }
        return blockOutDates;
    }


    private boolean hasAvailableSession(SkyDiningPackage sdPackage, Date date,
                                        List<SkyDiningSession> allSessions) {
        final Date now = new Date();
        final List<SkyDiningSession> sessions = sessionRepo.findAvailable(now, date, sdPackage.getId());
        final boolean useDateSessionType = useDateSessionType(date, allSessions);

        for (final SkyDiningSession session : sessions) {
            if (session.getSessionType() == SessionType.Normal.value) {
                if (useDateSessionType) {
                    continue;
                }
                if (StringUtils.isNotBlank(session.getEffectiveDays())) {
                    final Calendar dovCalendar = Calendar.getInstance();
                    dovCalendar.setTime(date);
                    final String effectiveDays = session.getEffectiveDays();
                    final int dayOfWeek = dovCalendar.get(Calendar.DAY_OF_WEEK);
                    boolean isApplicableToDayOfWeek = false;
                    switch (dayOfWeek) {
                        case Calendar.MONDAY:
                            if (effectiveDays.indexOf('1') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.TUESDAY:
                            if (effectiveDays.indexOf('2') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.WEDNESDAY:
                            if (effectiveDays.indexOf('3') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.THURSDAY:
                            if (effectiveDays.indexOf('4') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.FRIDAY:
                            if (effectiveDays.indexOf('5') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.SATURDAY:
                            if (effectiveDays.indexOf('6') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        case Calendar.SUNDAY:
                            if (effectiveDays.indexOf('7') > -1) {
                                isApplicableToDayOfWeek = true;
                            }
                            break;
                        default:
                            break;
                    }
                    if (!isApplicableToDayOfWeek) {
                        continue;
                    }
                } else {
                    continue;
                }
            }
            if (isBlockedOut(session, date)) {
                continue;
            }
            return true;
        }

        return false;
    }

    private boolean isBlockedOut(SkyDiningSession session, Date date) {
        boolean isBlockedOut = false;
        final List<SkyDiningBlockOutDate> blockOutDates = session
            .getBlockOutDates();
        for (final SkyDiningBlockOutDate blockOutDate : blockOutDates) {
            if (NvxDateUtils.inDateRange(blockOutDate.getStartDate(),
                blockOutDate.getEndDate(), date, null, true, true)) {
                isBlockedOut = true;
            }
        }
        return isBlockedOut;
    }

    private boolean useDateSessionType(Date date,
                                       List<SkyDiningSession> allSessions) {
        for (final SkyDiningSession session : allSessions) {
            if (BooleanUtils.isNotTrue(session.getActive())) {
                continue;
            }
            if (!NvxDateUtils.inDateRange(session.getStartDate(),
                session.getEndDate(), date, null, true, true)) {
                continue;
            }
            if (session.getSessionType() == SessionType.Date.value) {
                return true;
            }
        }
        return false;
    }

    private SkyDiningTransaction getTransactionInSession(HttpSession session) {
        Integer transId = (Integer) session.getAttribute(SESS_TRANS_ID);
        return transRepo.findOne(transId);
    }

    @Transactional
    @Override
    public ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd) {
        //TODO This is simplified validation of checkout confirm. May need to add more complex validation.

        final SkyDiningTransaction txn = getTransactionInSession(session);
        if (txn == null) {
            return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.General);
        }

        final String stage = txn.getStage();

        if (TransactionStage.Success.name().equals(stage)) {
            log.error("Transaction " + txn.getReceiptNum() + " has an invalid status at the confirm checkout stage: " + stage);
            return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.TransactionSuccess);
        }
        if (!TransactionStage.Reserved.name().equals(stage)) {
            log.error("Transaction " + txn.getReceiptNum() + " has an invalid status at the confirm checkout stage: " + stage);
            return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.TransactionNotReserved);
        }

        final String tmStatus = txn.getTmStatus();

        if (StringUtils.isNotEmpty(tmStatus)) {
            //TODO Handle pa more
            log.error("Transaction " + txn.getReceiptNum() + " has already proceeded with payment or been processed: " + tmStatus);
            return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.TransactionNotReserved);
        }

        txn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
        txn.setModifiedDate(new Date());
        transRepo.save(txn);

        final CheckoutConfirmResult ccr = new CheckoutConfirmResult();
        final String custName = txn.getCustName();
//        ccr.setTmRedirect(!custName.contains("skiptm"));
        ccr.setTmRedirect(isTMServiceEnabled());

        if (ccr.isTmRedirect()) {
            final String receiptNumber = txn.getReceiptNum();
            final BigDecimal totalAmount = txn.getTotalAmount();
            final String merchantId = txn.getTmMerchantId();
            final TmCurrency tmCurrency = TmCurrency.SGD;
            final TmPaymentType tmPaymentType = TmPaymentType.fromPaymentModeString(txn.getPaymentType());

            final TmFraudCheckPackage fraudCheckPackage = new TmFraudCheckPackage();
            fraudCheckPackage.setFirstName(custName);
            fraudCheckPackage.setEmail(txn.getCustEmail());
            fraudCheckPackage.setIp(trackd.getIp()); //TODO Get IP should be getting from the transaction IP instead of this.
            fraudCheckPackage.setEncoding(NvxUtil.DEFAULT_ENCODING);

            final TmLocale tmLocale = TmLocale.EnglishUs; //TODO apply i18n

            final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
            merchantSignatureConfig.setSignatureEnabled(false);

            final TelemoneyParamPackage tmParams;
            try {
                tmParams = tmParamRepository.getTmParams(channel);
            } catch (JcrRepositoryException e) {
                //TODO Additional handling
                log.error("Error retrieving telemoney parameters", e);
                return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.General);
            }

            String returnUrl = tmParams.getTmReturnUrlSkyDining();
            String statusUrl = tmParams.getTmStatusUrl();
            String returnUrlParamString = "";

            //TODO Namespace for i18n purposes

            final TmPaymentRedirectInput input = new TmPaymentRedirectInput(returnUrl, statusUrl, returnUrlParamString, receiptNumber,
                totalAmount, merchantId, tmPaymentType, tmCurrency, fraudCheckPackage, tmLocale, merchantSignatureConfig);
            input.setUserField1(channel.code);
            input.setUserField2("sky-dining");
            input.setUserField3(session.getId());

            final String redirectUrl = tmServiceProvider.constructPaymentRedirectUrl(input);

            ccr.setRedirectUrl(redirectUrl);

        } else {
            //Shortcut for load testing
            Date now = new Date();

            txn.setTmStatus(TmStatus.Success.name());
            txn.setTmApprovalCode("12345");
            txn.setStage(Stage.SuccessFinalized.name());
            txn.setModifiedDate(now);
            txn.setModifiedBy(SysConst.SYSTEM);
            transRepo.save(txn);

            SkyDiningReservation res = txn.getReservation();
            res.setReceiptNum("12345");
            res.setReceivedBy(SysConst.SYSTEM);
            res.setPaymentDate(now);
            res.setStatus(ReservationStatus.PaidInsideSystem.value);
            res.setPaymentMode(PaymentMode.OtherCard.value);
            res.setPaidAmount(txn.getTotalAmount());
            res.setModifiedDate(now);
            res.setModifiedBy(SysConst.SYSTEM);
            reservationRepo.save(res);
        }

        return new ApiResult<>(true, "", "", ccr);
    }

}