package com.enovax.star.cms.api.store.service.ppmflg;


import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRefreshHist;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGRefreshHistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("PPMFLGIRefreshHistService")
public class DefaultRefreshHistService implements IRefreshHistService {

    @Autowired
    private PPMFLGRefreshHistRepository refreshHistRepo;

    private boolean notEmpty(String k){
        return k != null && k.trim().length() > 0;
    }

    public void storeRefreshHist(String key1, String key2, String key3, String value){
        if(key1 == null){
            return;
        }
        if(value == null || value.trim().length() == 0){
            return;
        }
        if(notEmpty(key1) && notEmpty(key2) && notEmpty(key3)){
            store(key1, key2, key3, value);
            return;
        }
        if(notEmpty(key1) && notEmpty(key2)){
            store(key1, key2, null, value);
            return;
        }
        if(notEmpty(key1)){
            store(key1, null, null, value);
            return;
        }
    }

    public String getRefreshHist(String key1){
        PPMFLGRefreshHist hist = refreshHistRepo.findByKey1(key1);
        if(hist == null){
            return null;
        }
        return hist.getValue();
    }

    public String getRefreshHist(String key1, String key2){
        PPMFLGRefreshHist hist = refreshHistRepo.findByKey1AndKey2(key1, key2);
        if(hist == null){
            return null;
        }
        return hist.getValue();
    }

    public String getRefreshHist(String key1, String key2, String key3){
        PPMFLGRefreshHist hist = refreshHistRepo.findByKey1AndKey2AndKey3(key1, key2, key3);
        if(hist == null){
            return null;
        }
        return hist.getValue();
    }

    @Transactional(rollbackFor = Exception.class)
    private void store(String key1, String key2, String key3, String value) {
        PPMFLGRefreshHist hist = null;
        if(notEmpty(key1) && notEmpty(key2) && notEmpty(key3)){
            hist = refreshHistRepo.findByKey1AndKey2AndKey3(key1, key2, key3);
        }else if(notEmpty(key1) && notEmpty(key2) && (!notEmpty(key3))){
            hist = refreshHistRepo.findByKey1AndKey2(key1, key2);
        }else if(notEmpty(key1) && (!notEmpty(key2)) && (!notEmpty(key3))){
            hist = refreshHistRepo.findByKey1(key1);
        }
        if(hist == null){
            hist = new PPMFLGRefreshHist();
        }
        hist.setValue(value);
        hist.setKey1(key1);
        hist.setKey2(key2);
        hist.setKey3(key3);
        refreshHistRepo.save(hist);
    }

}
