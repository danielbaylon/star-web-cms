package com.enovax.star.cms.api.store.service.b2c.search;

import com.enovax.star.cms.commons.model.search.QuickSearchProductGraph;

/**
 * Created by jonathan on 28/12/16.
 */
public interface IQuickSearch {

    QuickSearchProductGraph doSearch(String searchTerm, String locale);

    QuickSearchProductGraph loadMainGraph();

    void refreshGraph();

}
