package com.enovax.star.cms.api.store.service.batch.tm;

import com.enovax.star.cms.api.store.service.ppslm.IPartnerDepositService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by houtao on 16/11/16.
 */
@Service
public class DepositTopupPendingTransQueryJob {
    //ppslm.deposit.topup.query.job

    private static final Logger log = LoggerFactory.getLogger(DepositTopupPendingTransQueryJob.class);

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private IPartnerDepositService depositSrv;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppslm.deposit.topup.query.job}")
    public void pendingPaymentTransCheck(){
        boolean isJobEnabled = sysParamSrv.isPPSLMDepositTopupTelemoneyQueryJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[checkDepositTopupTelemoneyQueryJob] is not enabled.");
            return;
        }
        if(!isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    try{
                        depositSrv.sendTmQueryForPendingTrans();
                    }catch (Exception ex){
                        log.error("Deposit topup telemoney query job completed with failure", ex);
                    }
                }
            });
        }
    }

}
