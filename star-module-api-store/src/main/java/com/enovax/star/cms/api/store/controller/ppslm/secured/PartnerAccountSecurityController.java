package com.enovax.star.cms.api.store.controller.ppslm.secured;


import com.enovax.star.cms.api.store.controller.ppslm.PPSLMBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerAccountService;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.repository.ppslm.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerAccountSharedService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerDocumentService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerSharedService;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/partner-account/")
public class PartnerAccountSecurityController extends PPSLMBaseStoreApiController {

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private IPartnerAccountService accountService;

    @Autowired
    private IPartnerDocumentService paDocSrv;

    @Autowired
    private IPartnerSharedService paSrv;

    @Autowired
    private ICountryRegionsRepository ctryRepo;

    @Autowired
    private ILineOfBusinessRepository paTypeRepo;

    private IPartnerAccountSharedService paAccSharedSrv;

    @RequestMapping(value = "login-success")
    public String apiLoginSuccess(Principal principal, HttpServletRequest request){
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            // return to login page
            return "/partner-portal-slm";
        }else{
            String successView = null;
            if(partnerAccount.isPwdForceChange()){
                request.setAttribute("renewRequired", true);
                if(request.getSession(false) != null){
                    request.getSession(false).setAttribute("renewRequired", true);
                }
                // return to first successful page
                successView = getFirstLoginChangingPasswordPage();
            }else{
                // return to first successful page
                successView = getFirstLoginPage();
            }

            return successView;
        }
    }

    @RequestMapping(value = "partner-main-account-username-check",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> apiGetPartnerMainAccountLoginNameCheckDetails(HttpServletRequest request, @RequestParam(name = "code", required = false) String username){
        ApiResult<String> apiResult = new ApiResult<String>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(username == null || username.trim().length() == 0){
            apiResult.setSuccess(false);
            apiResult.setMessage("Main Account Login Name is mandatory.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        username = username.trim();
        List<PPSLMTAMainAccount> mainAccountList = accountService.findMainAccountListByUserName(username.trim());
        if(mainAccountList != null && mainAccountList.size() > 0){
            if(mainAccountList.size() > 1){
                apiResult.setSuccess(false);
                apiResult.setMessage("REG_LOGIN_NAME_IN_USE");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            if(mainAccountList.size() == 0){
                PPSLMTAMainAccount mainAccount = mainAccountList.get(0);
                if(mainAccount != null){
                    if(mainAccount.getId().intValue() != partnerAccount.getMainAccount().getId().intValue()){
                        apiResult.setSuccess(false);
                        apiResult.setMessage("REG_LOGIN_NAME_IN_USE");
                        return new ResponseEntity<>(apiResult, HttpStatus.OK);
                    }
                }
            }
        }
        List<PPSLMTASubAccount> subAccountList = accountService.getSubAccountListByUserName(username.trim());
        if(subAccountList != null && subAccountList.size() > 0){
            apiResult.setSuccess(false);
            apiResult.setMessage("REG_LOGIN_NAME_IN_USE");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult.setMessage("REG_LOGIN_NAME_NOT_IN_USE");
        apiResult.setSuccess(true);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @RequestMapping(value = "update-partner-profile",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> apiUpdatePartnerProfileDetails(HttpServletRequest request, @RequestParam(name = "partner") String partnerVMJsonString){

        PartnerVM partnerVM = JsonUtil.fromJson(partnerVMJsonString, PartnerVM.class);

        ApiResult<String> apiResult = new ApiResult<String>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        try{
            paSrv.selfUpdateProfileAndSyncProfileChangesToAX(partnerAccount, partnerVM);
            apiResult.setSuccess(true);
        }catch (Exception ex){
            return new ResponseEntity<>(handleException("apiUpdatePartnerProfileDetails",ex, String.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @RequestMapping(value = "get-partner-profile",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<PartnerVM>> apiGetPartnerProfileDetails(HttpServletRequest request){
        ApiResult<PartnerVM> apiResult = new ApiResult<PartnerVM>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PartnerVM partnerVM = new PartnerVM(partner, true);
        partnerVM.setPaDocs(paDocSrv.getPartnerDocumentsByPartnerId(partner.getId()));
        partnerVM.setCountryName(ctryRepo.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data, partnerVM.getCountryCode()));
        partnerVM.setOrgTypeName(paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data, partnerVM.getOrgTypeCode()));
        apiResult.setData(partnerVM);
        apiResult.setSuccess(true);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @RequestMapping(value = "get-partner-subaccount-roles",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<List<PPSLMTAAccessGroupVM>>> apiGetPartnerSubAccountRoles(HttpServletRequest request){
        ApiResult<List<PPSLMTAAccessGroupVM>> apiResult = new ApiResult<List<PPSLMTAAccessGroupVM>>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        List<PPSLMTAAccessGroupVM> roles = accountService.getPartnerSubaccountAccessGroupList();
        apiResult.setData(roles != null ? roles : new ArrayList<PPSLMTAAccessGroupVM>());
        apiResult.setSuccess(true);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



    @RequestMapping(value = "renew-password", method = { RequestMethod.POST })
    public String apiRenewPassword(HttpServletRequest request){
        String pw = request.getParameter("pw");
        String newPw = request.getParameter("newPw");
        String newPw2 = request.getParameter("newPw2");
        boolean success = false;
        try{
            HttpSession session = request.getSession(false);
            if(session == null){
                return getLoginPage();
            }else{
                SecurityContext sc = (SecurityContext)session.getAttribute(PartnerPortalConst.SESSION_ATTR_SPRING_SECURITY_KEY);
                if(sc == null){
                    return getLoginPage();
                }else{
                    Authentication auth = sc.getAuthentication();
                    if(auth == null || auth.isAuthenticated() == false){
                        return getLoginPage();
                    }else{
                        User user = (User) auth.getPrincipal();
                        if(user == null){
                            return getLoginPage();
                        }else{
                            accountService.renewPassword(user.getUsername(), pw, newPw, newPw2);
                            success = true;
                        }
                    }
                }
            }
        }catch (Exception ex){
            success = false;
            ex.printStackTrace();
            String errorMsg = ex.getMessage();
            if(errorMsg != null && errorMsg.trim().length() > 0){
                request.setAttribute("error-message", errorMsg);
            }
        }
        if((!success) && request.getSession(false) != null &&  request.getSession(false).getAttribute("renewRequired") != null){
            request.setAttribute("renewRequired", request.getSession(false).getAttribute("renewRequired"));
            return getFirstLoginChangingPasswordPage();
        }
        return getFirstLoginPage();
    }

    private String getFirstLoginChangingPasswordPage() {
        return "/partner-portal-slm/renew-password";
    }

    private String getFirstLoginPage() {
        return "/partner-portal-slm/dashboard";
    }
    private String getLoginPage() {
        return "/partner-portal-slm";
    }

    @RequestMapping(value = "get-all-subaccounts",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<List<PartnerAccountVM>>> apiGetPartnerSubAccounts(HttpServletRequest request){
        ApiResult<List<PartnerAccountVM>> apiResult = new ApiResult<List<PartnerAccountVM>>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        List<PartnerAccountVM> list = accountService.getPartnerSubaccountListByMainAccountId(partner.getMainAccount().getId());
        apiResult.setTotal(list != null && list.size() > 0 ? list.size() : 0);
        apiResult.setData(list != null ? list : new ArrayList<PartnerAccountVM>());
        apiResult.setSuccess(true);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @RequestMapping(value = "save-subaccount-rights",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> apiSaveSubAccountDetails(HttpServletRequest request, @RequestParam("subAccountRightsStr") String subAccountRightsStr) throws BizValidationException {
        ApiResult<String> apiResult = new ApiResult<String>();
        PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
        if(subAccountRightsStr == null || subAccountRightsStr.trim().length() == 0){
            apiResult.setSuccess(false);
            apiResult.setMessage("Invalid Request.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PartnerAccountVMWrapper wrapper = null;
        try{
            wrapper = JsonUtil.fromJson(subAccountRightsStr, PartnerAccountVMWrapper.class);
            if(wrapper == null || wrapper.getAccounts() == null || wrapper.getAccounts().size() == 0){
                apiResult.setSuccess(false);
                apiResult.setMessage("Invalid Request.");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
        }catch (Exception ex1){
            ex1.printStackTrace();
            apiResult.setSuccess(false);
            apiResult.setMessage("Invalid Request.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }catch (Throwable ex2){
            ex2.printStackTrace();
            apiResult.setSuccess(false);
            apiResult.setMessage("Invalid Request.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(partnerAccount == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found, please logout and login again.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
        if(partner == null){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner details not found.");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            apiResult.setSuccess(false);
            apiResult.setMessage("Partner profile is not in Active status");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        try {
            apiResult = accountService.savePartnerSubAccountRightsChanges(partner, partnerAccount.getMainAccountId(), partnerAccount.getUsername(), partnerAccount.getId(), partnerAccount.isSubAccount(), wrapper.getAccounts());
            apiResult.setData("");
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (BizValidationException e) {
            return new ResponseEntity<>(handleException("apiSaveSubAccountDetails",e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-subaccount-details",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<PartnerAccountVM>> apiGetPartnerSubAccounts(HttpServletRequest request, @RequestParam("userId") Integer userId){
        ApiResult<PartnerAccountVM> apiResult = new ApiResult<PartnerAccountVM>();
        try{
            if(userId == null || userId.intValue() == 0){
                throw new BizValidationException("Invalid request, invalid sub account id found.");
            }
            PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
            if(partnerAccount == null){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner details not found, please logout and login again.");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
            if(partner == null){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner details not found.");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner profile is not in Active status");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            PartnerAccountVM vm = accountService.getPartnerSubaccountDetailsByUserId(userId);
            if(vm == null){
                throw new BizValidationException("Invalid request, sub account not found.");
            }
            if(partnerAccount.isSubAccount()){
                vm.setNotSelf(partnerAccount.getId().intValue() != vm.getId().intValue());
            }else{
                vm.setNotSelf(true);
            }
            apiResult.setData(vm);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetPartnerSubAccounts",e, PartnerAccountVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-subaccount-details",  method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> apiSavePartnerSubAccountDetails(HttpServletRequest request,
                                                                             @RequestParam("isNew") Boolean isNew,
                                                                             @RequestParam("userId") Integer userId,
                                                                             @RequestParam("username") String username,
                                                                             @RequestParam("name") String name,
                                                                             @RequestParam("email") String email,
                                                                             @RequestParam("title") String title,
                                                                             @RequestParam("status") String status,
                                                                             @RequestParam("access") String access) {
        ApiResult<String> apiResult = new ApiResult<String>();
        try{
            PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
            if(partnerAccount == null){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner details not found, please logout and login again.");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            PPSLMPartner partner = paSrv.getPartnerById(partnerAccount.getPartner().getId());
            if(partner == null){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner details not found.");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
                apiResult.setSuccess(false);
                apiResult.setMessage("Partner profile is not in Active status");
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
            ApiResult<String> res = accountService.saveTASubAccount(partner, isNew, userId, username, name, email, title, status, access, partnerAccount.getUsername(), partnerAccount.getId(), partnerAccount.isSubAccount());
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSavePartnerSubAccountDetails",e, String.class), HttpStatus.OK);
        }
    }
}