package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.api.store.model.partner.ppmflg.PPMFLGTransactionAdapter;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTelemoneyLog;
import org.slf4j.Logger;

/**
 * Created by jennylynsze on 11/15/16.
 */

public interface ILogService {
    void logTelemoneyDetails(String title, PPMFLGTelemoneyLog tm);
    void logTelemoneyDetails(Logger logger, String title, PPMFLGTelemoneyLog tm);
    boolean logErrorDetails(String errorCode, String methodName, String details, PPMFLGInventoryTransaction trans);
    void saveTelemoneyDetails(PPMFLGTelemoneyLog tm);
    boolean logTelemoneyDetailsToDB(PPMFLGTelemoneyLog tm);

    PPMFLGTelemoneyLog createTelemoneyObject(Boolean isQuery, TelemoneyResponse response);

    boolean processError(String errorCode, String methodName, PPMFLGInventoryTransaction trans, boolean sendEmail);

    boolean processDepositTopupError(String errorCode, String methodName, String details, PPMFLGDepositTransaction trans, boolean sendEmail);

    boolean processError(String errorCode, String methodName, String details, PPMFLGInventoryTransaction trans,
                         boolean sendEmail);

    void logInventoryTransactionDetails(String title, PPMFLGInventoryTransaction tran);

    void logInventoryTransactionDetails(Logger logger, String title, PPMFLGInventoryTransaction tran);

    boolean logErrorTransDetails(String errorCode, String methodName,
                                        String details, PPMFLGTransactionAdapter trans);

    public boolean processTransError(String errorCode, String methodName, String details,
                                     PPMFLGTransactionAdapter trans, boolean sendEmail);

    public void logTransDetails(Logger logger, String title, PPMFLGTransactionAdapter tran);

}
