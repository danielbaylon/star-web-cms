package com.enovax.star.cms.api.store.service.b2c.skydining;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.skydining.*;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.skydining.model.SessionRecord;
import com.enovax.star.cms.skydining.model.api.SkyDiningCheckPromoResult;
import com.enovax.star.cms.skydining.model.result.SkyDiningCalendarResult;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 16/6/16.
 */
public interface ISkyDiningService {
  ApiResult<SkyDiningFunCartDisplay> cartAdd(StoreApiChannels channel, SkyDiningFunCart cartToAdd, String sessionId);

  SkyDiningCheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session);

  ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode);

  ApiResult<SkyDiningFunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId);

  boolean cartClear(StoreApiChannels channel, String sessionId);

  ApiResult<SkyDiningStoreTransaction> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets);

  ApiResult<SkyDiningStoreTransaction> completeSale(StoreApiChannels channel, HttpSession session);

  ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session);

  SkyDiningReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session);

  SkyDiningReceiptDisplay getReceiptForDisplayByReceiptNum(String receiptNum);

  List<SessionRecord> getAvailableSessions(Integer packageId, Date dateOfVisit);

  SkyDiningCheckPromoResult checkJewelCard(String jcNumber, String jcExpiry,
                                           int productId, boolean getRelatedItems);

  SkyDiningCheckPromoResult checkPromoCode(String promoCode, int productId,
                                           String dateOfVisit, boolean getRelatedItems);

  ApiResult<SkyDiningCalendarResult> getCalendarParams(int packageId);

  ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd);
}
