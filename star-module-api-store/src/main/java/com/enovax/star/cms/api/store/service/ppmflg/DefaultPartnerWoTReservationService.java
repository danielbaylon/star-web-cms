package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.WOTReservationStatus;
import com.enovax.star.cms.commons.constant.ppmflg.WOTReservationType;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.penaltycharge.AxPenaltyCharge;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTSalesOrder;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGWingsOfTimeReservationRepository;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppmflg.PPMFLGWingsOfTimeReservationSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

/**
 * Created by jennylynsze on 6/7/16.
 */
@Service("PPMFLGIPartnerWoTReservationService")
public class DefaultPartnerWoTReservationService implements IPartnerWoTReservationService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerWoTReservationService.class);

    @Autowired
    PPMFLGWingsOfTimeReservationRepository slmReservationRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerDepositService")
    private IPartnerDepositService depositSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService paAccSrv;

    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeService")
    private IWingsOfTimeService wotSrv;

    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeSharedService")
    private IWingsOfTimeSharedService wotSharedSrv;

    @Autowired
    private PPMFLGWingsOfTimeReservationRepository wotRepo;

    @Autowired
    @Qualifier("PPMFLGIWoTPenaltyChargeService")
    private IWoTPenaltyChargeService wotPenaltyChargeSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerWoTEmailService")
    private IPartnerWoTEmailService wotEmailSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountSharedService")
    private IPartnerAccountSharedService paAccSharedSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerSharedService")
    private IPartnerSharedService paSharedSrv;


    private final StoreApiChannels channel = StoreApiChannels.PARTNER_PORTAL_MFLG;


    private void checkCustomerDepositBalance(String axAccountNumber, BigDecimal depositToBeCharged, BigDecimal depositToBeRefunded) throws BizValidationException {
        if(depositToBeCharged.doubleValue() > 0){
            BigDecimal required = depositToBeCharged.subtract(depositToBeRefunded);
            if(required.doubleValue() > 0){
                BigDecimal balance = depositSrv.getPartnerDepositBalance(axAccountNumber);
                if(balance.compareTo(required) <= 0){
                    throw new BizValidationException("Reservation will be charged for S$ "+(NvxNumberUtils.formatToCurrency(required))+", Please top-up your deposit balance to purchase.");
                }
            }
        }
    }

    @Override
    public WOTReservationVM generateWoTPinCodeBarcode(WOTReservationVM reservation) throws Exception {
        reservation.setPinCodeImage(BarcodeGenerator.toBase64(reservation.getPinCode(), 300, 75));
        return reservation;
    }

    private String generateWoTPinCodeBarcode(String pinCode) {
        try {
            return BarcodeGenerator.toBase64(pinCode, 300, 75);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return "";
    }

    @Override
    public WOTReservationVM getReservationPrintingDetailsByID(PartnerAccount partnerAccount, Integer reservationId) throws Exception {
        if(partnerAccount == null || partnerAccount.getMainAccount() == null || partnerAccount.getMainAccount().getId() == null || reservationId == null || reservationId.intValue() == 0){
            throw new Exception("Invalid request found");
        }
        PPMFLGWingsOfTimeReservation wot = wotRepo.findById(reservationId);
        if(wot.getMainAccountId() == null || wot.getMainAccountId().intValue() != partnerAccount.getMainAccount().getId()){
            throw new Exception("Invalid request found");
        }
        this.refreshWingsOfTimeReservationOrderDetails(partnerAccount.getId(), partnerAccount.getPartner().getAxAccountNumber(), wot);
        wot = wotRepo.findById(reservationId);
        return getWOTReservationVM(wot, new Date(System.currentTimeMillis()));
    }

    @Override
    public WOTReservationVM getReservationByID(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer reservationId) throws Exception {
        if(partnerAccount == null || partnerAccount.getMainAccount() == null || partnerAccount.getMainAccount().getId() == null || reservationId == null || reservationId.intValue() == 0){
            throw new Exception("Invalid request found");
        }
        PPMFLGWingsOfTimeReservation wot = wotRepo.findById(reservationId);
        if(wot.getMainAccountId() == null || wot.getMainAccountId().intValue() != partnerAccount.getMainAccount().getId()){
            throw new Exception("Invalid request found");
        }
        if(WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            this.refreshWingsOfTimeReservationOrderDetails(partnerAccount.getId(), partnerAccount.getPartner().getAxAccountNumber(), wot);
            wot = wotRepo.findById(reservationId);
        }
        return getWOTReservationVM(wot, new Date(System.currentTimeMillis()));
    }

    public ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(PartnerAccount account, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String filterStatus, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        Date now = new Date(System.currentTimeMillis());
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.EQ, account.getMainAccountId()));
        if(startDate != null){
            criterias.add(new QCriteria("eventDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("eventDate", QCriteria.Operation.LE, endDate));
        }
        if(reservationStartDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.GE, reservationStartDate));
        }
        if(reservationEndDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.LE, reservationEndDate));
        }
        if(showTimes != null && showTimes.trim().length() > 0){
            criterias.add(new QCriteria("eventLineId", QCriteria.Operation.EQ, showTimes));
        }

        List<String> status = new ArrayList<String>();
        if(filterStatus == null || filterStatus.trim().length() == 0 || "All".equalsIgnoreCase(filterStatus)){
            status.add("Reserved");
            status.add("PartiallyPurchased");
            status.add("FullyPurchased");
            status.add("Released");
            status.add("Confirmed");
            status.add("Cancelled");
            status.add("PartiallyRedeemed");
            status.add("FullyRedeemed");
        }else if("Reserved".equalsIgnoreCase(filterStatus)){
            status.add("Reserved");
            status.add("PartiallyPurchased");
            status.add("FullyPurchased");
        }else{
            String fs = filterStatus.replace(" ","");
            status.add(fs.trim());
        }

        criterias.add(new QCriteria("status", QCriteria.Operation.IN, status, QCriteria.ValueType.String));

        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.DESC, "eventDate");
        SpecificationBuilder builder = new SpecificationBuilder();
        Specifications querySpec = builder.build(criterias, PPMFLGWingsOfTimeReservationSpecification.class);
        Page<PPMFLGWingsOfTimeReservation> list = wotRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        PagedData<List<WOTReservationVM>> paged = new PagedData<List<WOTReservationVM>>();
        paged.setData(getWOTReservationVMs(list, now));
        paged.setSize(Long.valueOf(list.getTotalElements()).intValue());
        ApiResult<PagedData<List<WOTReservationVM>>> api = new ApiResult<PagedData<List<WOTReservationVM>>>();
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    private List<WOTReservationVM> getWOTReservationVMs(Page<PPMFLGWingsOfTimeReservation> data, Date now) throws Exception {
        if(data == null || data.getContent() == null || data.getContent().size() == 0){
            return null;
        }
        List<WOTReservationVM> wots = new ArrayList<WOTReservationVM>();
        List<PPMFLGWingsOfTimeReservation> list = data.getContent();
        Map<Integer, Integer> paIdMapping = new HashMap<Integer, Integer>();
        Map<String, String> paNameMapping = new HashMap<String, String>();
        Map<String, String> nameMapping = new HashMap<String, String>();
        for(PPMFLGWingsOfTimeReservation i : list){
            wots.add(getWOTReservationVM(paIdMapping, paNameMapping, nameMapping, i, now));
        }
        return wots;
    }

    private WOTReservationVM getWOTReservationVM(PPMFLGWingsOfTimeReservation i, Date now) throws Exception {
        return getWOTReservationVM(null, null, null, i, now);
    }

    private WOTReservationVM getWOTReservationVM(Map<Integer, Integer> paIdMapping, Map<String, String> paNameMapping, Map<String, String> nameMapping, PPMFLGWingsOfTimeReservation i, Date now) throws Exception {
        WOTReservationVM vm = new WOTReservationVM();
        vm.setId(i.getId());
        vm.setMainAccountId(i.getMainAccountId());
        vm.setShowDate(NvxDateUtils.formatDateForDisplay(i.getEventDate(), false));
        vm.setShowTime(i.getEventLineId());
        vm.setShowTimeDisplay(i.getEventName());
        vm.setEventLineId(i.getEventLineId());
        vm.setProductId(i.getProductId());
        vm.setEventDate(i.getEventDate());
        vm.setNow(now);
        vm.setQty(i.getQty());
        vm.setAdminRequest(i.isAdminRequest());
        vm.setPartnerId(paSharedSrv.getPartnerIdByMainAccountIdWithCache(paIdMapping, i.getMainAccountId()));
        vm.setPartnerName(paSharedSrv.getPartnerNameByMainAccountIdWithCache(paNameMapping, i.getMainAccountId()));
        vm.setReservationType(i.getReservationType());
        vm.setQtySold(i.getQtySold() != null ? i.getQtySold().intValue() : 0);
        vm.setPurchaseSrcId(i.getPurchaseSrcId() != null ? i.getPurchaseSrcId() : 0);
        vm.setPurchaseSrcQty(i.getPurchaseSrcQty() != null ? i.getPurchaseSrcQty() : 0);
        vm.setSplitSrcId(i.getSplitSrcId() != null ? i.getSplitSrcId().intValue() : 0);
        vm.setSplitSrcQty(i.getSplitSrcQty() != null ? i.getSplitSrcQty().intValue() : 0);
        vm.setRdm(i.getQtyRedeemed());
        vm.setUnr(i.getQtyUnredeemed());
        vm.setStatus(i.getStatus());
        vm.setRemarks(i.getRemarks());
        vm.setReceiptNo(i.getReceiptNum());
        vm.setPinCode(i.getPinCode());
        vm.setSystemRemarks(i.getRemarks());
        vm.setTicketPrice(i.getTicketPrice());
        vm.setItemId(i.getAxrtItemId());
        vm.setMediaTypeId(i.getMediaTypeId());
        vm.setEventGroupId(i.getEventGroupId());
        vm.setEventStartTime(i.getEventStartTime());
        vm.setEventEndTime(i.getEventEndTime());
        vm.setEventCapacityId(i.getEventCapacityId());
        vm.setCutOffDateTime(calculateWingsOfTimeShowCutOffTimeByShowTime(i));
        if(i.getOpenValidityStartDate() != null){
            vm.setOpenValidityStartDate(NvxDateUtils.formatDateForDisplay(i.getOpenValidityStartDate(), false));
        }
        if(i.getOpenValidityEndDate() != null){
            vm.setOpenValidityEndDate(NvxDateUtils.formatDateForDisplay(i.getOpenValidityEndDate(), false));
        }
        if(i.getReleasedDate() != null){
            vm.setReleasedDate(NvxDateUtils.formatDateForDisplay(i.getReleasedDate(), true));
        }
        if(i.getWasheddown() != null){
            vm.setIsWasheddown(i.getWasheddown());
        }
        if(i.getQtyWashedDown() != null){
            vm.setQtyWashedDown(i.getQtyWashedDown());
        }
        return vm;
    }

    @Override
    public Date calculateWingsOfTimeShowCutOffTimeByShowTime(final PPMFLGWingsOfTimeReservation wot) throws Exception {
        if(wot.getEventTime() != null){
            return new Date(wot.getEventDate().getTime() + wot.getEventTime() * 1000 - sysParamSrv.getWingsOfTimeShowCutoffInMinutes() * 60000);
        }
        return new Date(wot.getEventDate().getTime() + wot.getEventStartTime() * 1000 - sysParamSrv.getWingsOfTimeShowCutoffInMinutes() * 60000);

    }

    @Transactional(rollbackFor = Exception.class)
    public ApiResult<String> cancelReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer reservationId, Double refund, boolean isAmendment) throws Exception {

        String markupCode = "";
        BigDecimal markupAmt = new BigDecimal(0.0);

        ApiResult<WOTReservationCharge> calculatedAmount = validateCancelReservation(partnerAccount, productItemPriceMap, reservationId);

        if(calculatedAmount.isSuccess()) {

            markupCode = calculatedAmount.getData().getMarkupCode();
            markupAmt = calculatedAmount.getData().getPenaltyCharge();

            if(isAmendment){
                if(refund.doubleValue() != calculatedAmount.getData().getPenaltyCharge().doubleValue()){
                    throw new BizValidationException("Ticket status has been changed, please refresh and try again");
                }
            }else{
                if(refund.doubleValue() != calculatedAmount.getData().getDepositToBeRefunded().doubleValue()){
                    throw new BizValidationException("Ticket status has been changed, please refresh and try again");
                }

            }
            //update reservation status
            PPMFLGWingsOfTimeReservation wot = wotRepo.findById(reservationId);
            if(partnerAccount.getMainAccount().getId().intValue() != wot.getMainAccountId().intValue()){
                throw new Exception("Invalid request found.");
            }

            boolean canncelled = false;
            ApiResult<String> result = this.wotSrv.cancelWingsOfTimeReservationOrder(partnerAccount, wot, markupCode, markupAmt);
            if(result != null){
                if(result.isSuccess()){
                    canncelled = true;
                }
            }
            if(canncelled){
                wot.setStatus(WOTReservationStatus.Cancelled.name());
                wot.setModifiedBy(String.valueOf(partnerAccount.getId()));
                wot.setModifiedDate(new Date(System.currentTimeMillis()));
                this.wotRepo.save(wot);
                try{
                    wot.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(wot));
                }catch (Exception ex2){}
                if(!isAmendment){
                    this.wotEmailSrv.sendingWoTCancellationEmail(partnerAccount, wot, generateWoTPinCodeBarcode(wot.getPinCode()));
                }
            }
            return new ApiResult<>(canncelled, "", canncelled ? "" : "Cancel Wings of Time failed, Please try again.", "");
        }

        return new ApiResult<>(false, calculatedAmount.getErrorCode(), calculatedAmount.getMessage(), "");
    }

    private void verifyWoTReservationQty(Date requestDate, Integer paId, Integer mainAccId, String prodId, String itemId, String eventGroupId, String eventLineId, Integer newQty) throws BizValidationException, Exception {
        Integer reservationCap = paSharedSrv.getWoTReservationCap(paId);
        if(reservationCap == null){
            throw new BizValidationException("No reservation capacity configuration found!");
        }
        if(reservationCap.intValue() < (newQty)){
            throw new BizValidationException("The quantity reserved should not exceed "+reservationCap+" per Show Date.");
        }
        Integer noOfTicketReserved = this.getTotalTicketReservedPerShow(mainAccId, prodId, itemId, eventGroupId, eventLineId, requestDate);
        if(noOfTicketReserved != null && noOfTicketReserved.intValue() >= reservationCap.intValue()){
            throw new BizValidationException("You have 0 Wings of Time tickets are allowed to purchase for this show!");
        }
        if(noOfTicketReserved != null && (noOfTicketReserved.intValue() + newQty) > reservationCap.intValue()){
            int allowed =  reservationCap.intValue() - noOfTicketReserved.intValue();
            if(allowed < 0){
                allowed = 0;
            }
            throw new BizValidationException("You have "+(allowed)+" Wings of Time tickets are allowed to purchase for this show!");
        }
    }

    @Transactional
    public ApiResult<WOTReservationVM> saveReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM request, boolean isUpdateRequest) throws BizValidationException, Exception {
        Date now = new Date();
        verifyWoTReservationQty(request.getEventDate(), partnerAccount.getPartner().getId(), partnerAccount.getId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), request.getQty());

        ApiResult<WOTReservationCharge> validateResult = validateNewReservation(partnerAccount, productItemPriceMap, request);
        if(!validateResult.isSuccess()) {
            return new ApiResult<>(false, validateResult.getErrorCode(), validateResult.getMessage(), null);
        }
        String receiptNum = ProjectUtils.generateReceiptNumber(channel, now);
        PPMFLGWingsOfTimeReservation reservation;
        reservation = new PPMFLGWingsOfTimeReservation();
        reservation.setReceiptNum(receiptNum);
        reservation.setPinCode("");
        reservation.setReservationType(WOTReservationType.Purchase.name());
        reservation.setAdminRequest(false);
        reservation.setMainAccountId(partnerAccount.getMainAccount().getId());
        reservation.setIsSubAccountTrans(partnerAccount.isSubAccount());
        reservation.setUsername(partnerAccount.getUsername());
        reservation.setModifiedDate(now);
        reservation.setCreatedDate(now);
        reservation.setCreatedBy(String.valueOf(partnerAccount.getId()));
        reservation.setModifiedBy(String.valueOf(partnerAccount.getId()));
        if(request.getShowDate() != null && request.getShowDate().trim().length() > 0){
            reservation.setEventDate(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        reservation.setEventLineId(request.getEventLineId());
        reservation.setProductId(request.getProductId());
        reservation.setEventName(request.getShowTimeDisplay());
        reservation.setRemarks(request.getRemarks());
        reservation.setQty(request.getQty());
        reservation.setQtyRedeemed(0);
        reservation.setQtyUnredeemed(request.getQty());
        reservation.setStatus(WOTReservationStatus.Pending.name());
        reservation.setTicketPrice(validateResult.getData().getTicketPrice());
        reservation.setQtyUnredeemed(request.getQty() - request.getRdm()); //update
        reservation.setItemId(request.getItemId());
        reservation.setMediaTypeId(request.getMediaTypeId());
        reservation.setEventGroupId(request.getEventGroupId());
        reservation.setReceiptEmail(partnerAccount.getEmail());
        reservation.setEventStartTime(request.getEventStartTime());
        reservation.setEventTime(request.getEventTime());
        reservation.setEventEndTime(request.getEventEndTime());
        reservation.setEventCapacityId(request.getEventCapacityId());
        try{
            reservation.setOpenValidityStartDate(new Date(Long.valueOf(request.getOpenValidityStartDate())));
        }catch (Exception ex){}
        try{
            reservation.setOpenValidityEndDate(new Date(Long.valueOf(request.getOpenValidityEndDate())));
        }catch (Exception ex){}
        slmReservationRepo.save(reservation);
        ApiResult<AxWOTSalesOrder> result = new ApiResult<AxWOTSalesOrder>();
        try{
            result = wotSrv.saveWingsOfTimeReservationOrder(partnerAccount, result, reservation);
        }catch (Exception ex){
            log.error("saveReservation failed ", ex);
            ex.printStackTrace();
        }
        if(result.isSuccess()){
            reservation.setStatus(WOTReservationStatus.Confirmed.name());
        }else{
            reservation.setStatus(WOTReservationStatus.Failed.name());
        }
        slmReservationRepo.save(reservation);
        request.setId(reservation.getId());
        request.setStatus(reservation.getStatus());
        if(result.isSuccess()){
            try{
                reservation.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(reservation));
            }catch (Exception ex2){}
            if(isUpdateRequest){
                try{
                    this.wotEmailSrv.sendingWoTAmendmentEmail(partnerAccount, reservation, generateWoTPinCodeBarcode(reservation.getPinCode()));
                }catch (Exception ex){
                    log.error("saveAdminReservation send update reservation email failed "+ex.getMessage());
                }catch (Throwable ex2){
                    log.error("saveAdminReservation send update reservation email failed "+ex2.getMessage());
                }
            }else{
                try{
                    this.wotEmailSrv.sendingWoTConfirmationEmail(partnerAccount, reservation, generateWoTPinCodeBarcode(reservation.getPinCode()));
                }catch (Exception ex){
                    log.error("saveAdminReservation send new reservation email failed "+ex.getMessage());
                }catch (Throwable ex2){
                    log.error("saveAdminReservation send new reservation email failed "+ex2.getMessage());
                }
            }
            return new ApiResult<>(true, "", "", request);
        }else{
            return new ApiResult<>(false, result.getErrorCode(), result.getMessage(), request);
        }
    }

    private Integer getTotalTicketReservedPerShow(Integer mainAccId, String productId, String itemId, String eventGroupId, String eventLineId, Date date) throws BizValidationException {
        if(mainAccId == null || mainAccId.intValue() == 0 || productId == null || productId.trim().length() == 0 || itemId == null || itemId.trim().length() ==0 || date == null){
            throw new BizValidationException("Invalid wings of time reservation request");
        }
        Long total = wotRepo.sumPartnerWoTTicketPurchased(mainAccId, productId, itemId, eventGroupId, eventLineId, date);
        if(total != null){
            return total.intValue();
        }
        return null;
    }

    public ApiResult<String> verifyAndCancelSplitReservationOrder(PartnerAccount partnerAccount, Integer id) throws Exception {
        ApiResult<String> vm = new ApiResult<>();
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(!WOTReservationStatus.Pending.name().equals(wot.getStatus())){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        // note : split does not generate penalty charge

        wot.setStatus(WOTReservationStatus.Discarded.name());
        wot.setModifiedDate(new Date(System.currentTimeMillis()));
        wot.setModifiedBy(partnerAccount.getId().toString());
        slmReservationRepo.save(wot);
        vm.setSuccess(true);
        return vm;
    }

    private ApiResult<WoTVM> splitStepRefreshWoTOrderDetails(PartnerAccount account, PPMFLGWingsOfTimeReservation srcWoT) {
        return this.refreshWingsOfTimeReservationOrderDetails(account.getId(), account.getPartner().getAxAccountNumber(), srcWoT);
    }

    private ApiResult<String> splitStepUpdateOrignialQuantityInAX(PartnerAccount partnerAccount, PPMFLGWingsOfTimeReservation srcWoT, boolean subtract) {
        ApiResult<String> result = new ApiResult<>();
        try{
            result = wotSrv.updateWingsOfTimeReservationOrder(partnerAccount, result, wotRepo.findById(srcWoT.getId()), "", new BigDecimal(0.0));
        }catch (Exception ex){
            log.error("splitStepUpdateOrignialQuantityInAX failed ", ex);
            ex.printStackTrace();
        }
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    private PPMFLGWingsOfTimeReservation splitStepUpdateOrignialQuantity(PPMFLGWingsOfTimeReservation srcWoT, Integer quantity, boolean subtract) throws Exception {
        PPMFLGWingsOfTimeReservation wot = wotRepo.findById(srcWoT.getId());
        if(subtract){
            if(!(wot.getQty() > quantity)){
                throw new Exception("INVALID_SPLIT_REQUEST_IN_NOT_ENOUGH_QTY");
            }
        }
        if(subtract){
            wot.setQty(wot.getQty() - quantity);
        }else{
            wot.setQty(wot.getQty() + quantity);
        }
        wotRepo.save(wot);
        return wot;
    }

    @Transactional(rollbackFor = Exception.class)
    private boolean saveWotDetails(PPMFLGWingsOfTimeReservation wot){
        int maxTry = 2;
        int reTry = 0;
        boolean success = false;
        do{
            reTry += 1;
            try{
                wotRepo.save(wot);
                success = true;
            }catch (Exception ex){
                success = false;
                log.error("splitStepSaveWoTDetails ["+JsonUtil.jsonify(wot)+"] failed at "+reTry+" try.", ex);
                try{
                    Thread.currentThread().sleep(2000);
                }catch (Exception ex2){}
            }
        }while(reTry <= maxTry && (!success));
        return success;
    }
    @Transactional(rollbackFor = Exception.class)
    public ApiResult<WoTVM> verifyAndGetSplitReservationOrder(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer id, Integer qty) throws Exception {
        ApiResult<WoTVM> vm = new ApiResult<>();
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }

        WOTReservationVM tempRequest = new WOTReservationVM();
        tempRequest.setProductId(wot.getProductId());
        tempRequest.setItemId(wot.getItemId());
        tempRequest.setEventLineId(wot.getEventLineId());
        tempRequest.setQty(qty);
        Date now = new Date();
        String receiptNum = ProjectUtils.generateReceiptNumber(channel, now);
        PPMFLGWingsOfTimeReservation reservation;
        reservation = new PPMFLGWingsOfTimeReservation();
        reservation.setReceiptNum(receiptNum);
        reservation.setPinCode("");
        reservation.setSplitSrcId(wot.getId());
        reservation.setSplitSrcQty(qty);
        reservation.setReservationType(WOTReservationType.Purchase.name());
        reservation.setAdminRequest(false);
        reservation.setMainAccountId(partnerAccount.getMainAccount().getId());
        reservation.setIsSubAccountTrans(partnerAccount.isSubAccount());
        reservation.setUsername(partnerAccount.getUsername());
        reservation.setModifiedDate(now);
        reservation.setCreatedDate(now);
        reservation.setCreatedBy(String.valueOf(partnerAccount.getId()));
        reservation.setModifiedBy(String.valueOf(partnerAccount.getId()));
        reservation.setEventDate(wot.getEventDate());
        reservation.setEventLineId(wot.getEventLineId());
        reservation.setProductId(wot.getProductId());
        reservation.setEventName(wot.getEventName());
        reservation.setRemarks("SYSTEM: Split Reservation from "+wot.getReceiptNum());
        reservation.setQty(qty);
        reservation.setQtyRedeemed(0);
        reservation.setQtyUnredeemed(qty);
        reservation.setStatus(WOTReservationStatus.Pending.name());
        reservation.setTicketPrice(wot.getTicketPrice());
        reservation.setItemId(wot.getItemId());
        reservation.setMediaTypeId(wot.getMediaTypeId());
        reservation.setEventGroupId(wot.getEventGroupId());
        reservation.setReceiptEmail(partnerAccount.getEmail());
        reservation.setEventStartTime(wot.getEventStartTime());
        reservation.setEventTime(wot.getEventTime());
        reservation.setEventEndTime(wot.getEventEndTime());
        reservation.setEventCapacityId(wot.getEventCapacityId());
        try{
            reservation.setOpenValidityStartDate(wot.getOpenValidityStartDate());
        }catch (Exception ex){}
        try{
            reservation.setOpenValidityEndDate(wot.getOpenValidityEndDate());
        }catch (Exception ex){}
        slmReservationRepo.save(reservation);
        vm.setData(new WoTVM(reservation));
        vm.setSuccess(true);
        return vm;
    }

    @Transactional(rollbackFor = Exception.class)
    public ApiResult<WoTVM> verifyAndConfirmSplitReservationOrder(PartnerAccount partnerAccount, Integer id) throws Exception {
        ApiResult<WoTVM> api = new ApiResult<WoTVM>();
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(!WOTReservationStatus.Pending.name().equals(wot.getStatus())){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(!WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        Integer srcReserveId = wot.getSplitSrcId();
        if(srcReserveId == null || srcReserveId.intValue() == 0){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        PPMFLGWingsOfTimeReservation srcWoT = slmReservationRepo.findById(srcReserveId);
        if(srcWoT == null){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(srcWoT.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new Exception("INVALID_RESERVATION_SPLIT_REQUEST");
        }
        if(!WOTReservationStatus.Confirmed.name().equals(srcWoT.getStatus())){
            throw new Exception("INVALID_SPLIT_REQUEST_IN_OTHER_STATUS");
        }
        /**
         * Step 1 : Refresh Pin Code Status
         */
        ApiResult<WoTVM> refreshStatus = null;
        refreshStatus = splitStepRefreshWoTOrderDetails(partnerAccount, srcWoT);
        if(!refreshStatus.isSuccess()){
            api.setSuccess(refreshStatus.isSuccess());
            api.setMessage(refreshStatus.getMessage());
            return api;
        }
        srcWoT = slmReservationRepo.findById(srcReserveId);
        if(!WOTReservationStatus.Confirmed.name().equals(srcWoT.getStatus())){
            throw new Exception("INVALID_SPLIT_REQUEST_IN_OTHER_STATUS");
        }
        /**
         * Section 2. reduce the quantity in Local DB & AX
         */
        ApiResult<String> updateResponse = null;
        try{
            // 2.1 reduce quantity in DB first
            srcWoT = splitStepUpdateOrignialQuantity(srcWoT, wot.getQty(), true);
        }catch(Exception ex){
            api.setSuccess(false);
            api.setMessage(ex.getMessage());
            api.setData(new WoTVM(wot));
            return api;
        }
        // 2.2 reduce quantity in AX
        updateResponse = splitStepUpdateOrignialQuantityInAX(partnerAccount, srcWoT, true);
        if(!updateResponse.isSuccess()){
            try{
                // add quantity back to DB if reduce in AX is failed
                srcWoT = splitStepUpdateOrignialQuantity(srcWoT, wot.getQty(), false);
            }catch(Exception ex){
                splitStepRefreshWoTOrderDetails(partnerAccount, srcWoT);
            }
            api.setSuccess(updateResponse.isSuccess());
            api.setMessage(updateResponse.getMessage());
            api.setData(new WoTVM(wot));
            return api;
        }
        boolean refreshOK = true;
        refreshStatus = splitStepRefreshWoTOrderDetails(partnerAccount, srcWoT);
        if(!refreshStatus.isSuccess()){
            refreshOK = false;
            api.setSuccess(refreshStatus.isSuccess());
            api.setMessage(refreshStatus.getMessage());
            return api;
        }
        /**
         * Section 3. check out WOT order
         */
        ApiResult<AxWOTSalesOrder> checkoutResult = new ApiResult<AxWOTSalesOrder>();
        try{
            // 2. save wot order
            checkoutResult = wotSrv.saveWingsOfTimeReservationOrder(partnerAccount, checkoutResult, wot);
        }catch (Exception ex){
            log.error("saveReservation failed ", ex);
            ex.printStackTrace();
        }
        boolean success = false;
        if(checkoutResult.isSuccess()){
            success = true;
            wot.setStatus(WOTReservationStatus.Confirmed.name());
        }else{
            wot.setStatus(WOTReservationStatus.Failed.name());
        }
        api.setSuccess(checkoutResult.isSuccess());
        api.setMessage(checkoutResult.getMessage());
        // save wot check out status
        saveWotDetails(wot);
        api.setSuccess(checkoutResult.isSuccess());
        api.setMessage(checkoutResult.getMessage());
        if(!checkoutResult.isSuccess()){
            // add quantity back to AX
            srcWoT.setQty(srcWoT.getQty() + wot.getQty());
            splitStepUpdateOrignialQuantityInAX(partnerAccount, srcWoT, false);
            // refresh local database by latest data from AX
            splitStepRefreshWoTOrderDetails(partnerAccount, srcWoT);
        }
        if(!refreshOK){
            splitStepRefreshWoTOrderDetails(partnerAccount, srcWoT);
        }
        /**
         * Section 4. checkout success
         */
        api.setData(new WoTVM(wot));

        if(success){
            wot = wotRepo.findById(wot.getId());
            try{
                wot.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(wot));
            }catch (Exception ex2){}
            this.wotEmailSrv.sendingWoTSplitConfirmationEmail(partnerAccount, wot, generateWoTPinCodeBarcode(wot.getPinCode()));
        }

        return api;
    }

    @Transactional(rollbackFor = Exception.class)
    public ApiResult<WOTReservationVM> updateReservation(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM request) throws Exception {
        Date now = new Date();
        PPMFLGWingsOfTimeReservation existingReservation = slmReservationRepo.findById(request.getId());
        if(existingReservation == null){
            throw new Exception("INVALID_RESERVATION_UPDATE_REQUEST");
        }
        ApiResult<WOTReservationCharge> validateResult = validateUpdateReservation(partnerAccount, productItemPriceMap, request);
        if(!validateResult.isSuccess()) {
            return new ApiResult<>(false, validateResult.getErrorCode(), validateResult.getMessage(), null);
        }
        int requestQty   = request.getQty();
        if(requestQty < existingReservation.getQtyRedeemed()){
            throw new Exception("Quantity must include total number of quantities redeemed.");
        }
        boolean isCancelRequired = checkIsCanncelReservationRequied(existingReservation, request);
        if(isCancelRequired){
            verifyWoTReservationQty(request.getEventDate(), partnerAccount.getPartner().getId(), partnerAccount.getId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), requestQty);
        }else{
            if(request.getQty() > existingReservation.getQty()){
                verifyWoTReservationQty(request.getEventDate(), partnerAccount.getPartner().getId(), partnerAccount.getId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), requestQty - existingReservation.getQty());
            }
        }
        if(isCancelRequired){
            return cancelAndCreateReservation(now, request, existingReservation, partnerAccount, validateResult.getData(), productItemPriceMap);
        }else{
            return updateFullReservation(now, request, existingReservation, partnerAccount, validateResult.getData());
        }
    }

    private boolean checkIsCanncelReservationRequied(PPMFLGWingsOfTimeReservation c, WOTReservationVM r) throws BizValidationException {
        if(!(c != null && r != null && c.getItemId() != null && r.getItemId() != null && c.getItemId().trim().length() > 0 && r.getItemId().trim().length() > 0)){
            throw new BizValidationException("Invalid Request Found!");
        }
        if(c.getItemId().trim().equalsIgnoreCase(r.getItemId().trim())){
            return false;
        }else{
            return true;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    private ApiResult<WOTReservationVM> cancelAndCreateReservation(Date now, WOTReservationVM request, PPMFLGWingsOfTimeReservation currentWot, PartnerAccount account, WOTReservationCharge data, Map<String, BigDecimal> productItemPriceMap) throws Exception {
        ApiResult<String> cancelResponse = cancelReservation(account, productItemPriceMap, currentWot.getId(), data.getRebookPenaltyCharge().doubleValue(), true);
        if(!cancelResponse.isSuccess()){
            throw new BizValidationException(cancelResponse.getMessage());
        }
        ApiResult<WOTReservationVM> createResponse = this.saveReservation(account, productItemPriceMap, request, true);
        if(!createResponse.isSuccess()){
            throw new BizValidationException(createResponse.getMessage());
        }
        return createResponse;
    }

    @Transactional(rollbackFor = Exception.class)
    private ApiResult<WOTReservationVM> updateFullReservation(Date now, WOTReservationVM request, PPMFLGWingsOfTimeReservation reservation, PartnerAccount partnerAccount, WOTReservationCharge charge) throws Exception {

        PPMFLGWingsOfTimeReservation copy = reservation.getDataCopy();
        copy.setModifiedDate(now);
        copy.setModifiedBy(partnerAccount.getId().toString());
        copy.setRemarks(request.getRemarks() != null && request.getRemarks().trim().length() > 0 ? request.getRemarks() : reservation.getRemarks());
        if(request.getShowDate() != null && request.getShowDate().trim().length() > 0){
            copy.setEventDate(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        copy.setEventLineId(request.getEventLineId());
        copy.setProductId(request.getProductId());
        copy.setEventName(request.getShowTimeDisplay());
        copy.setQty(request.getQty());
        copy.setQtyUnredeemed(request.getQty() - copy.getQtyRedeemed());
        copy.setTicketPrice(charge.getTicketPrice());
        copy.setItemId(request.getItemId());
        copy.setMediaTypeId(request.getMediaTypeId());
        copy.setEventGroupId(request.getEventGroupId());
        copy.setReceiptEmail(partnerAccount.getEmail());
        if(charge.isNoChangesFound()){
            reservation.setModifiedDate(now);
            reservation.setModifiedBy(partnerAccount.getId().toString());
            reservation.setRemarks(request.getRemarks() != null && request.getRemarks().trim().length() > 0 ? request.getRemarks() : reservation.getRemarks());
            slmReservationRepo.save(reservation);
            request.setStatus(reservation.getStatus());
            request.setId(reservation.getId());
            return new ApiResult<>(true, "", "", request);
        }else{
            ApiResult<String> result = new ApiResult<>();
            try{
                result = wotSrv.updateWingsOfTimeReservationOrder(partnerAccount, result, copy, charge.getMarkupCode(), charge.getPenaltyCharge());
            }catch (Exception ex){
                log.error("updateFullReservation failed ", ex);
                ex.printStackTrace();
            }
            boolean updated = false;
            if(result != null && result.isSuccess()){
                updated = true;
            }

            if(updated){
                reservation.setModifiedDate(now);
                reservation.setModifiedBy(partnerAccount.getId().toString());
                reservation.setRemarks(request.getRemarks() != null && request.getRemarks().trim().length() > 0 ? request.getRemarks() : reservation.getRemarks());
                if(request.getShowDate() != null && request.getShowDate().trim().length() > 0){
                    reservation.setEventDate(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
                }
                reservation.setEventLineId(request.getEventLineId());
                reservation.setProductId(request.getProductId());
                reservation.setEventName(request.getShowTimeDisplay());
                reservation.setQty(request.getQty());
                reservation.setQtyUnredeemed(request.getQty() - reservation.getQtyRedeemed() - reservation.getQtyWashedDown());
                reservation.setTicketPrice(charge.getTicketPrice());
                reservation.setItemId(request.getItemId());
                reservation.setMediaTypeId(request.getMediaTypeId());
                reservation.setEventGroupId(request.getEventGroupId());
                reservation.setReceiptEmail(partnerAccount.getEmail());

                if(reservation.getQtyRedeemed() > 0 && reservation.getQtyUnredeemed() == 0){
                    reservation.setStatus(WOTReservationStatus.FullyRedeemed.name());
                }else if(reservation.getQtyRedeemed() > 0) {
                    reservation.setStatus(WOTReservationStatus.PartiallyRedeemed.name());
                }else{
                    reservation.setStatus(WOTReservationStatus.Confirmed.name());
                }

                slmReservationRepo.save(reservation);
                request.setStatus(reservation.getStatus());

                try{
                    reservation.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(reservation));
                }catch (Exception ex3){}
                this.wotEmailSrv.sendingWoTAmendmentEmail(partnerAccount, reservation, generateWoTPinCodeBarcode(reservation.getPinCode()));

                return new ApiResult<>(true, "", "", request);
            }else{
                request.setStatus(WOTReservationStatus.Failed.name());
                return new ApiResult<>(false, result.getErrorCode(), result.getMessage(), request);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void refreshPartnerReservations(Integer loginId, String axAccountNumber) {
        if(axAccountNumber == null || axAccountNumber.trim().length() == 0){
            return;
        }
        Integer mainAccId = null;
        try{
            mainAccId = paAccSrv.findMainAccountIdByAccountNumber(axAccountNumber);
        }catch (Exception ex){
            log.error("Refresh partner reservation details failed for Customer ["+axAccountNumber+"]");
            ex.printStackTrace();
        }
        if(mainAccId == null || mainAccId.intValue() == 0){
            return;
        }
        List<Integer> list = this.wotRepo.findAllActiveUnRedeemdedTicketsByMainAccountId(mainAccId);
        if(list != null && list.size() > 0){
            for(Integer id : list) {
                PPMFLGWingsOfTimeReservation wot = wotRepo.findById(id);
                if (wot != null) {
                    refreshWingsOfTimeReservationOrderDetails(loginId, axAccountNumber, wot);
                }
            }
        }
    }

    public void refreshUnredeemedPinCodeService() {
        List<Integer> list = this.wotRepo.findAllActiveUnRedeemdedTickets();
        if(list != null && list.size() > 0){
            Map<Integer, String> map = new HashMap<Integer, String>();
            for(Integer id : list){
                try {
                    refreshUnredeemedPinCodeById(id,map);
                }catch (Exception ex){
                    log.error("refresh unredeemed Pin Code ["+id+"] failed : "+ex.getMessage(), ex);
                }
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    private void refreshUnredeemedPinCodeById(Integer id,Map<Integer, String> map) {
        PPMFLGWingsOfTimeReservation wot = this.wotRepo.findById(id);
        if(wot != null){
            if(wot.getAxReferenceId() != null && wot.getAxReferenceId().trim().length() > 0){
                String axAccountNum = map.get(wot.getMainAccountId());
                if(axAccountNum == null || axAccountNum.trim().length() == 0){
                    PPMFLGPartner partner = paSharedSrv.getPartnerByMainAccountId(wot.getMainAccountId());
                    if(partner != null){
                        axAccountNum = partner.getAxAccountNumber();
                    }
                }
                if(axAccountNum != null && axAccountNum.trim().length() > 0){
                    map.put(wot.getMainAccountId(), axAccountNum);
                    refreshWingsOfTimeReservationOrderDetails(wot.getMainAccountId(), axAccountNum, wot);
                }
            }
        }
    }

    public ApiResult<String> refreshPinCodeReferenceIdAndInventTransId(PartnerAccount account, Integer reservationId) throws Exception {
        if(reservationId == null || reservationId.intValue() == 0){
            throw new Exception("Invalid WoT Reservation id");
        }
        final PPMFLGWingsOfTimeReservation wotView = wotRepo.findById(reservationId);
        if(wotView.getMainAccountId().intValue() != account.getMainAccountId().intValue()){
            throw new Exception("Invalid WoT Reservation Access");
        }
        if(wotView.isAdminRequest() || WOTReservationType.Reserve.name().equals(wotView.getReservationType())){
            throw new Exception("Invalid WoT Reservation Access");
        }
        String axRefId = wotView.getAxReferenceId();
        String axInvestTxnId = wotView.getAxInventTransId();
        if(axRefId != null && axRefId.trim().length() > 0 && axInvestTxnId != null && axInvestTxnId.trim().length() > 0){
           return new ApiResult<>(true, "","", "");
        }
        ApiResult<WoTVM> apiResult = refreshWingsOfTimeReservationOrderDetails(account.getId(), account.getPartner().getAxAccountNumber(), wotView);
        return new ApiResult<>(apiResult != null ? apiResult.isSuccess() : false, apiResult != null ? apiResult.getErrorCode() : "", apiResult != null ? apiResult.getMessage() : "Refresh wings of time reservation order details failed.", "");
    }

    @Transactional(rollbackFor = Exception.class)
    private ApiResult<WoTVM> refreshWingsOfTimeReservationOrderDetails(Integer loginUserId, String axAccountNumber, PPMFLGWingsOfTimeReservation wotView){
        ApiResult<WoTVM> woTVM = wotSrv.updateWingsOfTimeReservationOrderDetails(axAccountNumber, wotView);
        if(woTVM != null && woTVM.getData() != null){
            WoTVM vm = woTVM.getData();
            PPMFLGWingsOfTimeReservation wotUpdate = wotRepo.findById(wotView.getId());
            if(vm.getAxReferenceId() != null && vm.getAxReferenceId().trim().length() > 0){
                wotUpdate.setAxReferenceId(vm.getAxReferenceId());
            }
            if(vm.getAxInventTransId() != null && vm.getAxInventTransId().trim().length() > 0){
                wotUpdate.setAxInventTransId(vm.getAxInventTransId());
            }
            if(vm.getQty() != null){
                wotUpdate.setQty(vm.getQty());
            }
            if(vm.getQtyRedeemed() != null){
                wotUpdate.setQtyRedeemed(vm.getQtyRedeemed());
            }
            if(vm.getQtyReturned() != null){
                wotUpdate.setQtyReturned(vm.getQtyReturned());
            }
            if(vm.getQty() != null && vm.getQtyRedeemed() != null){
                if(vm.getQtyWashedDown() != null && vm.getQtyWashedDown().intValue() > 0){
                    wotUpdate.setQtyUnredeemed(vm.getQty() - vm.getQtyRedeemed() - vm.getQtyWashedDown());
                }else{
                    wotUpdate.setQtyUnredeemed(vm.getQty() - vm.getQtyRedeemed());
                }
            }
            if(vm.getIsWasheddown() != null){
                wotUpdate.setWasheddown(vm.getIsWasheddown());
            }
            if(vm.getQtyWashedDown() != null){
                wotUpdate.setQtyWashedDown(vm.getQtyWashedDown());
            }
            if(vm.getQtyAutoGenerated() != null){
                wotUpdate.setQtyAutoGenerated(vm.getQtyAutoGenerated());
            }

            //changing of status
            if(wotUpdate.getQtyRedeemed() != null && wotUpdate.getQtyUnredeemed() != null && wotUpdate.getQtyRedeemed() > 0 && wotUpdate.getQtyUnredeemed() > 0){
                //if qtyRedeemed > 0 and qtyUnRedeemed > 0 (means there's still some unredeem stufff
                if(WOTReservationStatus.Confirmed.name().equalsIgnoreCase(wotUpdate.getStatus())){
                    wotUpdate.setStatus(WOTReservationStatus.PartiallyRedeemed.name());
                }
            }else if(wotUpdate.getQtyRedeemed() != null && wotUpdate.getQtyUnredeemed() != null && wotUpdate.getQtyRedeemed() > 0 && wotUpdate.getQtyUnredeemed() == 0){
                //Full redeem is when  qtyUnRedeemd == 0 and qtyRedeemed is > 0 (means there's nothing to redeem anymore
                if( WOTReservationStatus.Confirmed.name().equalsIgnoreCase(wotUpdate.getStatus()) || WOTReservationStatus.PartiallyRedeemed.name().equalsIgnoreCase(wotUpdate.getStatus()) ){
                    wotUpdate.setStatus(WOTReservationStatus.FullyRedeemed.name());
                }
            }

            if(loginUserId != null){
                wotUpdate.setModifiedBy(loginUserId.toString());
            }
            wotUpdate.setModifiedDate(new Date(System.currentTimeMillis()));
            wotRepo.save(wotUpdate);
        }
        return woTVM;
    }

    @Transactional(readOnly = true)
    public ApiResult<WOTReservationCharge> validateNewReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM reservation) throws BizValidationException {
        ApiResult<WOTReservationCharge> apiPC = new ApiResult<WOTReservationCharge>();
        BigDecimal penaltyCharge        = BigDecimal.ZERO;
        BigDecimal ticketPrice          = BigDecimal.ZERO;
        BigDecimal refundedAmount       = BigDecimal.ZERO;
        BigDecimal ticketPurchased      = BigDecimal.ZERO;
        BigDecimal depositToBeCharged   = BigDecimal.ZERO;
        BigDecimal depositToBeRefunded  = BigDecimal.ZERO;
        BigDecimal penaltyChargeCfg     = BigDecimal.ZERO;

        try{
            ticketPrice = getServerTicketPrice(productItemPriceMap, reservation.getProductId(), reservation.getItemId(), reservation.getEventLineId());
            ticketPurchased = ticketPrice.multiply(new BigDecimal(reservation.getQty()));
            depositToBeCharged = ticketPurchased;
        }catch (Exception ex){
            apiPC.setSuccess(false);
            apiPC.setMessage(ex.getMessage());
            return apiPC;
        }

        WOTReservationCharge wotPC = new WOTReservationCharge();
        wotPC.setRefundedAmount(refundedAmount);
        wotPC.setTotalQty(reservation.getQty());
        wotPC.setMarkupCode("");//it is not required for new purchase
        wotPC.setTicketPrice(ticketPrice);
        wotPC.setPenaltyCharge(penaltyCharge);
        wotPC.setDepositToBeCharged(depositToBeCharged);
        wotPC.setDepositToBeRefunded(depositToBeRefunded);
        wotPC.setTicketPurchased(ticketPurchased);
        wotPC.setPenaltyChargeCfg(penaltyChargeCfg);
        try {
            wotPC.setShowDateDisplay(populateWoTShowDateDisplay(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY)));
        } catch (ParseException e) {
            throw new BizValidationException(e);
        }
        wotPC.setShowTimeDisplay(populateWoTShowTimeDisplay(reservation.getShowTimeDisplay()));

        checkCustomerDepositBalance(account.getPartner().getAxAccountNumber(), depositToBeCharged, depositToBeRefunded);

        apiPC.setSuccess(true);
        apiPC.setData(wotPC);

        return apiPC;
    }

    private String populateWoTShowTimeDisplay(String str) {
        if(str != null && str.trim().length() > 0){
            return str.trim();
        }
        return "";
    }

    private String populateWoTShowDateDisplay(Date date) {
        if(date != null){
            return NvxDateUtils.formatDate(date, NvxDateUtils.BOOKING_DATE_FORMAT_DISPLAY);
        }
        return "";
    }

    @Transactional(readOnly = true)
    public ApiResult<WOTReservationCharge> validatePurchaseReservedReservationPrice(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer id) throws BizValidationException {
        if(id == null || id.intValue() == 0){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!WOTReservationStatus.Pending.name().equals(wot.getStatus())){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        Integer srcReserveId = wot.getPurchaseSrcId();
        if(srcReserveId == null || srcReserveId.intValue() == 0){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        PPMFLGWingsOfTimeReservation srcWoT = slmReservationRepo.findById(srcReserveId);
        if(srcWoT == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(srcWoT.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!( WOTReservationStatus.Reserved.name().equals(srcWoT.getStatus()) || WOTReservationStatus.PartiallyPurchased.name().equals(srcWoT.getStatus()))){
            throw new BizValidationException("RESERVED_TICKETS_IS_NOT_ELIGIBLE");
        }
        if(srcWoT.getQty() - srcWoT.getQtySold() < wot.getQty()){
            throw new BizValidationException("RESERVED_TICKETS_QUANTITY_IS_NOT_ENOUGH");
        }
        return validatePurchaseReservedReservationPrice(partnerAccount, productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventLineId(), wot.getQty(), wot.getEventDate(), wot.getEventName());
    }

    @Transactional(readOnly = true)
    private ApiResult<WOTReservationCharge> validatePurchaseReservedReservationPrice(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, String productId, String itemId, String eventLineId, int qty, Date eventDate, String eventName) throws BizValidationException {
        ApiResult<WoTVM> api = new ApiResult<WoTVM>();

        ApiResult<WOTReservationCharge> apiPC = new ApiResult<WOTReservationCharge>();
        BigDecimal penaltyCharge        = BigDecimal.ZERO;
        BigDecimal ticketPrice          = BigDecimal.ZERO;
        BigDecimal refundedAmount       = BigDecimal.ZERO;
        BigDecimal ticketPurchased      = BigDecimal.ZERO;
        BigDecimal depositToBeCharged   = BigDecimal.ZERO;
        BigDecimal depositToBeRefunded  = BigDecimal.ZERO;
        BigDecimal penaltyChargeCfg     = BigDecimal.ZERO;

        try{
            ticketPrice = getServerTicketPrice(productItemPriceMap, productId, itemId, eventLineId);
            ticketPurchased = ticketPrice.multiply(new BigDecimal(qty));
            depositToBeCharged = ticketPurchased;
        }catch (Exception ex){
            apiPC.setSuccess(false);
            apiPC.setMessage(ex.getMessage());
            return apiPC;
        }

        WOTReservationCharge wotPC = new WOTReservationCharge();
        wotPC.setRefundedAmount(refundedAmount);
        wotPC.setMarkupCode(""); // it is not required for new purchase.
        wotPC.setTicketPrice(ticketPrice);
        wotPC.setTotalQty(qty);
        wotPC.setPenaltyCharge(penaltyCharge);
        wotPC.setDepositToBeCharged(depositToBeCharged);
        wotPC.setDepositToBeRefunded(depositToBeRefunded);
        wotPC.setTicketPurchased(ticketPurchased);

        wotPC.setPenaltyChargeCfg(penaltyChargeCfg);
        try {
            wotPC.setShowDateDisplay(populateWoTShowDateDisplay(eventDate));
        } catch (Exception e) {
            throw new BizValidationException(e);
        }
        wotPC.setShowTimeDisplay(populateWoTShowTimeDisplay(eventName));

        checkCustomerDepositBalance(account.getPartner().getAxAccountNumber(), depositToBeCharged, depositToBeRefunded);

        apiPC.setSuccess(true);
        apiPC.setData(wotPC);

        return apiPC;
    }

    @Transactional(readOnly = true)
    public ApiResult<WOTReservationCharge> validateUpdateReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, WOTReservationVM request) throws BizValidationException {
        ApiResult<WOTReservationCharge> apiPC = new ApiResult<WOTReservationCharge>();
        if(request == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid modification reservation request found.");
            return apiPC;
        }
        if(request.getId() == 0){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid modification reservation id found.");
            return apiPC;
        }
        if(request.getQty() <= 0){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid modification reservation request found.");
            return apiPC;
        }
        if(request.getItemId() == null || request.getItemId().trim().length() == 0){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid modification reservation request found.");
            return apiPC;
        }
        if(request.getEventLineId() == null || request.getEventLineId().trim().length() == 0){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid modification reservation request found.");
            return apiPC;
        }
        PPMFLGWingsOfTimeReservation wot = wotRepo.findById(request.getId());
        if(wot == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid reservation id found.");
            return apiPC;
        }
        if(account.getMainAccountId().intValue() != wot.getMainAccountId().intValue()){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid access found.");
            return apiPC;
        }
        if(!WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid access found.");
            return apiPC;
        }
        if(!WOTReservationStatus.Confirmed.name().equals(wot.getStatus())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Reservation modification is only allowed in "+WOTReservationStatus.Confirmed.name()+" status");
            return apiPC;
        }
        Date now = new Date(System.currentTimeMillis());
        Date cutOffTime = null;
        try {
            cutOffTime = calculateWingsOfTimeShowCutOffTimeByShowTime(wot);
        } catch (Exception e) {
            apiPC.setSuccess(false);
            apiPC.setMessage(e.getMessage());
            return apiPC;
        }
        if(cutOffTime == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("Retrieve cut-off time for PIN code ["+wot.getPinCode()+"] is failed, please try again");
            return apiPC;
        }
        if(cutOffTime.before(now)){
            apiPC.setSuccess(false);
            apiPC.setMessage("Reservation modification is only allowed before the cut-off time of this show");
            return apiPC;
        }
        ApiResult<WoTVM> wotVM = this.refreshWingsOfTimeReservationOrderDetails(account.getId(), account.getPartner().getAxAccountNumber(), wot);
        if(!wotVM.isSuccess()){
            apiPC.setSuccess(false);
            apiPC.setMessage("Retrieve latest status of PIN code ["+wot.getPinCode()+"] is failed, please try again");
            return apiPC;
        }
        wot = wotRepo.findById(request.getId());
        if(!WOTReservationStatus.Confirmed.name().equals(wot.getStatus())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Reservation modification is only allowed in "+WOTReservationStatus.Confirmed.name()+" status");
            return apiPC;
        }

        AxPenaltyCharge pc = null;
        try{
            pc = wotPenaltyChargeSrv.getPenaltyCharge(account.getPartner().getAxAccountNumber(), wot.getEventGroupId(), wot.getEventLineId());
        }catch (Exception ex){
            apiPC.setSuccess(false);
            apiPC.setMessage(ex.getMessage());
            return apiPC;
        }
        if(pc == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("No penalty charge configuration found");
            return apiPC;
        }

        BigDecimal ticketPrice          = BigDecimal.ZERO;
        BigDecimal originaTicketPrice   = BigDecimal.ZERO;
        BigDecimal penaltyCharge        = BigDecimal.ZERO;
        BigDecimal refundedAmount       = BigDecimal.ZERO;
        BigDecimal ticketPurchased      = BigDecimal.ZERO;
        BigDecimal depositToBeCharged   = BigDecimal.ZERO;
        BigDecimal depositToBeRefunded  = BigDecimal.ZERO;
        BigDecimal rebookPenaltyCharge  = BigDecimal.ZERO;
        BigDecimal penaltyChargeCfg     = BigDecimal.ZERO;
        penaltyChargeCfg = pc.getMarkupAmt();

        // 1. change the show date
        boolean changeProductId     = !(wot.getProductId().equals(request.getProductId()));
        boolean changeEventGroupId  = !(wot.getEventGroupId().equals(request.getEventGroupId()));
        boolean changeEventLineId   = !(wot.getEventLineId().equals(request.getEventLineId()));
        boolean changeItemId        = !(wot.getItemId().equals(request.getItemId()));
        boolean changeShowDate      = !(NvxDateUtils.formatDateForDisplay(wot.getEventDate(), false).equalsIgnoreCase(request.getShowDate()));
        boolean noChangesFound      = false;

        originaTicketPrice = wot.getTicketPrice();
        if(changeProductId || changeEventGroupId || changeEventLineId || changeShowDate || changeItemId){
            // change the show date/time with reduce the qty -- has penalty charge
            penaltyCharge = pc.getMarkupAmt().multiply(new BigDecimal(wot.getQty().intValue()));
            rebookPenaltyCharge = penaltyCharge;
            BigDecimal tobeRefund = null;
            BigDecimal toBuy = null;
            try {
                tobeRefund = originaTicketPrice.multiply(new BigDecimal(wot.getQty().intValue()));
                ticketPrice = getServerTicketPrice(productItemPriceMap, request.getProductId(), request.getItemId(), request.getEventLineId());
                toBuy = ticketPrice.multiply(new BigDecimal(request.getQty()));
            } catch (Exception e) {
                e.printStackTrace();
                apiPC.setSuccess(false);
                apiPC.setMessage(e.getMessage());
                return apiPC;
            }
            refundedAmount = tobeRefund;
            ticketPurchased = toBuy;
            toBuy = toBuy.add(penaltyCharge);
            if(tobeRefund.compareTo(toBuy) > 0){
                depositToBeRefunded = tobeRefund.subtract(toBuy);
            }else{
                depositToBeCharged = toBuy.subtract(tobeRefund);
            }
        }else{
            if(wot.getQty().intValue() > request.getQty()){
                // reduce the qty -- has penalty charge
                penaltyCharge = pc.getMarkupAmt().multiply(new BigDecimal( wot.getQty().intValue() - request.getQty()));
                rebookPenaltyCharge = penaltyCharge;
                try {
                    ticketPrice = getServerTicketPrice(productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventName());
                    ticketPurchased = ticketPrice.multiply(new BigDecimal(request.getQty()));
                    refundedAmount = ticketPrice.multiply(new BigDecimal( wot.getQty().intValue() - request.getQty()));
                } catch (Exception e) {
                    e.printStackTrace();
                    apiPC.setSuccess(false);
                    apiPC.setMessage(e.getMessage());
                    return apiPC;
                }
                if(refundedAmount.compareTo(penaltyCharge) > 0){
                    depositToBeRefunded = refundedAmount.subtract(penaltyCharge);
                }else{
                    depositToBeCharged = penaltyCharge.subtract(refundedAmount);
                }
            }else if(wot.getQty().intValue() < request.getQty()){
                try {
                    ticketPrice = getServerTicketPrice(productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventName());
                    ticketPurchased = ticketPrice.multiply(new BigDecimal(request.getQty()));
                    BigDecimal actualTicketPurchased = ticketPrice.multiply(new BigDecimal( request.getQty() - wot.getQty().intValue()));
                    depositToBeCharged = actualTicketPurchased;
                } catch (Exception e) {
                    e.printStackTrace();
                    apiPC.setSuccess(false);
                    apiPC.setMessage(e.getMessage());
                    return apiPC;
                }
            }else{
                // no changes on quantity.
                noChangesFound = true;
            }
        }
        
        BigDecimal originalRefundAmount = BigDecimal.ZERO;
        if(wot.getQty() != null && originaTicketPrice != null){
            originalRefundAmount = originaTicketPrice.multiply(new BigDecimal(wot.getQty()));
        }

        WOTReservationCharge wotPC = new WOTReservationCharge();
        wotPC.setTotalQty(request.getQty());
        wotPC.setRefundedAmount(refundedAmount);
        wotPC.setMarkupCode(pc.getMarkupCode());
        wotPC.setTicketPrice(ticketPrice);
        wotPC.setPenaltyCharge(penaltyCharge);
        wotPC.setDepositToBeCharged(depositToBeCharged);
        wotPC.setDepositToBeRefunded(depositToBeRefunded);
        wotPC.setTicketPurchased(ticketPurchased);
        wotPC.setRebookPenaltyCharge(rebookPenaltyCharge);
        wotPC.setOriginalTotalQty(wot.getQty());
        wotPC.setOriginalTicketPrice(originaTicketPrice);
        wotPC.setOriginalTotalQty(wot.getQty());
        wotPC.setOriginalTicketPrice(originaTicketPrice);
        wotPC.setOriginalRefundAmount(originalRefundAmount);
        checkCustomerDepositBalance(account.getPartner().getAxAccountNumber(), depositToBeCharged, depositToBeRefunded);
        wotPC.setNoChangesFound(noChangesFound);

        wotPC.setPenaltyChargeCfg(penaltyChargeCfg);
        try {
            wotPC.setShowDateDisplay(populateWoTShowDateDisplay(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY)));
        } catch (Exception e) {
            throw new BizValidationException(e);
        }
        wotPC.setShowTimeDisplay(populateWoTShowTimeDisplay(request.getShowTimeDisplay()));

        apiPC.setSuccess(true);
        apiPC.setData(wotPC);

        return apiPC;
    }

    private BigDecimal getServerTicketPrice(Map<String, BigDecimal> prodItemPriceMap, String productId, String itemId, String eventName) throws BizValidationException {
        if(productId == null || productId.trim().length() == 0 || itemId == null || itemId.trim().length() == 0 || eventName == null || eventName.trim().length() == 0){
            throw new BizValidationException("Invalid event "+eventName+".");
        }
        if(prodItemPriceMap != null && prodItemPriceMap.containsKey(productId+"-"+itemId)){
           return prodItemPriceMap.get(productId+"-"+itemId);
        }
        throw new BizValidationException("No price configuration found for event "+eventName);
    }

    @Transactional(readOnly = true)
    public ApiResult<WOTReservationCharge> validateCancelReservation(PartnerAccount account, Map<String, BigDecimal> productItemPriceMap, Integer reservationId) throws BizValidationException {
        ApiResult<WOTReservationCharge> apiPC = new ApiResult<WOTReservationCharge>();

        if(reservationId == null || reservationId.intValue() == 0){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid reservation id found.");
            return apiPC;
        }
        PPMFLGWingsOfTimeReservation wot = wotRepo.findById(reservationId);
        if(wot == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid reservation id found.");
            return apiPC;
        }
        if(account.getMainAccountId().intValue() != wot.getMainAccountId().intValue()){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid access found.");
            return apiPC;
        }
        if(!WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Invalid access found.");
            return apiPC;
        }
        if(!WOTReservationStatus.Confirmed.name().equals(wot.getStatus())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Cancellation is only allowed in "+WOTReservationStatus.Confirmed.name()+" status");
            return apiPC;
        }
        Date now = new Date(System.currentTimeMillis());
        Date cutOffTime = null;
        try {
            cutOffTime = calculateWingsOfTimeShowCutOffTimeByShowTime(wot);
        } catch (Exception e) {
            apiPC.setSuccess(false);
            apiPC.setMessage(e.getMessage());
            return apiPC;
        }
        if(cutOffTime == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("Retrieve cut-off time for PIN code ["+wot.getPinCode()+"] is failed, please try again");
            return apiPC;
        }
        if(cutOffTime.before(now)){
            apiPC.setSuccess(false);
            apiPC.setMessage("Cancellation is only allowed before the cut-off time of this show");
            return apiPC;
        }
        ApiResult<WoTVM> wotVM = this.refreshWingsOfTimeReservationOrderDetails(account.getId(), account.getPartner().getAxAccountNumber(), wot);
        if(!wotVM.isSuccess()){
            apiPC.setSuccess(false);
            apiPC.setMessage("Retrieve latest status of PIN code ["+wot.getPinCode()+"] is failed, please try again");
            return apiPC;
        }
        wot = wotRepo.findById(reservationId);
        if(!WOTReservationStatus.Confirmed.name().equals(wot.getStatus())){
            apiPC.setSuccess(false);
            apiPC.setMessage("Cancellation is only allowed in "+WOTReservationStatus.Confirmed.name()+" status");
            return apiPC;
        }

        AxPenaltyCharge pc = null;
        try{
            pc = wotPenaltyChargeSrv.getPenaltyCharge(account.getPartner().getAxAccountNumber(), wot.getEventGroupId(), wot.getEventLineId());
        }catch (Exception ex){
            apiPC.setSuccess(false);
            apiPC.setMessage(ex.getMessage());
            return apiPC;
        }
        if(pc == null){
            apiPC.setSuccess(false);
            apiPC.setMessage("No penalty charge configuration found");
            return apiPC;
        }

        BigDecimal ticketPrice          = BigDecimal.ZERO;
        BigDecimal penaltyCharge        = BigDecimal.ZERO;
        BigDecimal refundedAmount       = BigDecimal.ZERO;
        BigDecimal ticketPurchased      = BigDecimal.ZERO;
        BigDecimal depositToBeCharged   = BigDecimal.ZERO;
        BigDecimal depositToBeRefunded  = BigDecimal.ZERO;
        BigDecimal penaltyChargeCfg     = BigDecimal.ZERO;
        penaltyChargeCfg = pc.getMarkupAmt();

        penaltyCharge = pc.getMarkupAmt().multiply(new BigDecimal(wot.getQty()));
        try {
            ticketPrice = wot.getTicketPrice();// should i use purchase price or server latest price...
            refundedAmount = ticketPrice.multiply(new BigDecimal(wot.getQty()));
        } catch (Exception e) {
            e.printStackTrace();
            apiPC.setSuccess(false);
            apiPC.setMessage(e.getMessage());
            return apiPC;
        }

        if(refundedAmount.compareTo(penaltyCharge) < 0){
            depositToBeCharged = penaltyCharge.subtract(refundedAmount);
        }else{
            depositToBeRefunded = refundedAmount.subtract(penaltyCharge);
        }

        WOTReservationCharge wotPC = new WOTReservationCharge();
        wotPC.setRefundedAmount(refundedAmount);
        wotPC.setTicketPrice(ticketPrice);
        wotPC.setTotalQty(wot.getQty());
        wotPC.setMarkupCode(pc.getMarkupCode());
        wotPC.setPenaltyCharge(penaltyCharge);
        wotPC.setDepositToBeCharged(depositToBeCharged);
        wotPC.setDepositToBeRefunded(depositToBeRefunded);
        wotPC.setTicketPurchased(ticketPurchased);
        wotPC.setPenaltyChargeCfg(penaltyChargeCfg);
        try {
            wotPC.setShowDateDisplay(populateWoTShowDateDisplay(wot.getEventDate()));
        } catch (Exception e) {
            throw new BizValidationException(e);
        }
        wotPC.setShowTimeDisplay(populateWoTShowTimeDisplay(wot.getEventName()));

        checkCustomerDepositBalance(account.getPartner().getAxAccountNumber(), depositToBeCharged, depositToBeRefunded);

        apiPC.setSuccess(true);
        apiPC.setData(wotPC);

        return apiPC;
    }


    @Override
    public ApiResult<WoTVM> verifyAndGetPurchaseReservationOrder(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer id, Integer qty) throws BizValidationException {
        if(id == null || id.intValue() == 0){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        ApiResult<WoTVM> vm = new ApiResult<>();
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!(wot.isAdminRequest() && wot.getMainAccountId().intValue() == partnerAccount.getMainAccountId().intValue() && WOTReservationType.Reserve.name().equals(wot.getReservationType()) && ( WOTReservationStatus.Reserved.name().equals(wot.getStatus()) || WOTReservationStatus.PartiallyPurchased.name().equals(wot.getStatus())  || WOTReservationStatus.FullyPurchased.name().equals(wot.getStatus()) ))){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(wot.getQty() - wot.getQtySold() <= 0 || wot.getQty() - wot.getQtySold() < qty.intValue() ){
            throw new BizValidationException("No enough tickets for purchase.");
        }

        Date now = new Date();
        ApiResult<WOTReservationCharge> validateResult = this.validatePurchaseReservedReservationPrice(partnerAccount, productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventLineId(), qty, wot.getEventDate(), wot.getEventName());
        if(!validateResult.isSuccess()) {
            return new ApiResult<>(false, validateResult.getErrorCode(), validateResult.getMessage(), null);
        }

        String receiptNum = ProjectUtils.generateReceiptNumber(channel, now);
        PPMFLGWingsOfTimeReservation reservation;
        reservation = new PPMFLGWingsOfTimeReservation();
        reservation.setReceiptNum(receiptNum);
        reservation.setPinCode("");
        reservation.setReservationType(WOTReservationType.Purchase.name());
        reservation.setAdminRequest(false);
        reservation.setMainAccountId(partnerAccount.getMainAccount().getId());
        reservation.setIsSubAccountTrans(partnerAccount.isSubAccount());
        reservation.setUsername(partnerAccount.getUsername());
        reservation.setModifiedDate(now);
        reservation.setCreatedDate(now);
        reservation.setCreatedBy(String.valueOf(partnerAccount.getId()));
        reservation.setModifiedBy(String.valueOf(partnerAccount.getId()));
        reservation.setEventDate(wot.getEventDate());
        reservation.setEventLineId(wot.getEventLineId());
        reservation.setProductId(wot.getProductId());
        reservation.setEventName(wot.getEventName());
        reservation.setRemarks("SYSTEM: Purchase Reservation from "+wot.getReceiptNum());
        reservation.setQty(qty);
        reservation.setQtyRedeemed(0);
        reservation.setQtyUnredeemed(qty);
        reservation.setStatus(WOTReservationStatus.Pending.name());
        reservation.setTicketPrice(getServerTicketPrice(productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventLineId()));
        reservation.setItemId(wot.getItemId());
        reservation.setMediaTypeId(wot.getMediaTypeId());
        reservation.setEventGroupId(wot.getEventGroupId());
        reservation.setReceiptEmail(partnerAccount.getEmail());
        reservation.setEventStartTime(wot.getEventStartTime());
        reservation.setEventTime(wot.getEventTime());
        reservation.setEventEndTime(wot.getEventEndTime());
        reservation.setEventCapacityId(wot.getEventCapacityId());
        reservation.setPurchaseSrcId(wot.getId());
        reservation.setPurchaseSrcQty(qty);
        try{
            reservation.setOpenValidityStartDate(wot.getOpenValidityStartDate());
        }catch (Exception ex){}
        try{
            reservation.setOpenValidityEndDate(wot.getOpenValidityEndDate());
        }catch (Exception ex){}

        slmReservationRepo.save(reservation);
        vm.setData(new WoTVM(reservation));
        vm.setSuccess(true);
        return vm;
    }

    @Override
    public ApiResult<String> verifyAndCancelPurchaseReservationOrder(PartnerAccount partnerAccount, Integer id) throws BizValidationException {
        ApiResult<String> vm = new ApiResult<>();
        if(id == null || id.intValue() == 0){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!WOTReservationStatus.Pending.name().equals(wot.getStatus())){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        wot.setStatus(WOTReservationStatus.Discarded.name());
        wot.setModifiedDate(new Date(System.currentTimeMillis()));
        wot.setModifiedBy(partnerAccount.getId().toString());
        slmReservationRepo.save(wot);
        vm.setSuccess(true);
        return vm;
    }

    @Override
    public ApiResult<WoTVM> verifyAndConfirmPurchaseReservationOrder(PartnerAccount partnerAccount, Map<String, BigDecimal> productItemPriceMap, Integer id) throws BizValidationException {
        ApiResult<WoTVM> api = new ApiResult<WoTVM>();
        PPMFLGWingsOfTimeReservation wot = slmReservationRepo.findById(id);
        if(wot == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(wot.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!WOTReservationStatus.Pending.name().equals(wot.getStatus())){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!WOTReservationType.Purchase.name().equals(wot.getReservationType())){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        Integer srcReserveId = wot.getPurchaseSrcId();
        if(srcReserveId == null || srcReserveId.intValue() == 0){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        PPMFLGWingsOfTimeReservation srcWoT = slmReservationRepo.findById(srcReserveId);
        if(srcWoT == null){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(srcWoT.getMainAccountId().intValue() != partnerAccount.getMainAccountId().intValue()){
            throw new BizValidationException("INVALID_PURCHASE_RESERVATION_REQUEST");
        }
        if(!( WOTReservationStatus.Reserved.name().equals(srcWoT.getStatus()) || WOTReservationStatus.PartiallyPurchased.name().equals(srcWoT.getStatus()))){
            throw new BizValidationException("RESERVED_TICKETS_IS_NOT_ELIGIBLE");
        }
        if(srcWoT.getQty() - srcWoT.getQtySold() < wot.getQty()){
            throw new BizValidationException("RESERVED_TICKETS_QUANTITY_IS_NOT_ENOUGH");
        }
        WOTReservationVM reserveVM = new WOTReservationVM();
        reserveVM.setProductId(wot.getProductId());
        reserveVM.setItemId(wot.getItemId());
        reserveVM.setEventLineId(wot.getEventLineId());
        reserveVM.setQty(wot.getQty());

        Date now = new Date();
        ApiResult<WOTReservationCharge> validateResult = this.validatePurchaseReservedReservationPrice(partnerAccount, productItemPriceMap, wot.getProductId(), wot.getItemId(), wot.getEventLineId(), wot.getQty(), wot.getEventDate(), wot.getEventName());
        if(!validateResult.isSuccess()) {
            return new ApiResult<>(false, validateResult.getErrorCode(), validateResult.getMessage(), null);
        }
        wot.setTicketPrice(validateResult.getData().getTicketPrice());
        /**
         * Section 3. check out WOT order
         */
        ApiResult<AxWOTSalesOrder> checkoutResult = new ApiResult<AxWOTSalesOrder>();
        try{
            // 2. save wot order
            checkoutResult = wotSrv.saveWingsOfTimeReservationOrderFromReservedPool(partnerAccount, checkoutResult, wot, srcWoT);
        }catch (Exception ex){
            log.error("saveReservation failed ", ex);
            ex.printStackTrace();
        }
        boolean success = false;
        if(checkoutResult.isSuccess()){
            success = true;
            wot.setStatus(WOTReservationStatus.Confirmed.name());
            srcWoT = slmReservationRepo.findById(srcReserveId);
            int qtySold = srcWoT.getQtySold() != null ? srcWoT.getQtySold().intValue() : 0;
            srcWoT.setQtySold(qtySold + wot.getQty());
            if(srcWoT.getQtySold().intValue() == srcWoT.getQty()){
                srcWoT.setStatus(WOTReservationStatus.FullyPurchased.name());
            }else{
                srcWoT.setStatus(WOTReservationStatus.PartiallyPurchased.name());
            }
        }else{
            wot.setStatus(WOTReservationStatus.Failed.name());
        }
        // save wot check out status
        saveWotDetails(wot);
        saveWotDetails(srcWoT);
        api.setSuccess(checkoutResult.isSuccess());
        api.setMessage(checkoutResult.getMessage());
        /**
         * Section 4. checkout success
         */
        api.setData(new WoTVM(wot));

        if(success){
            wot = wotRepo.findById(wot.getId());
            try{
                wot.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(wot));
            }catch (Exception ex2){}
            this.wotEmailSrv.sendingWoTConfirmationEmail(partnerAccount, wot, generateWoTPinCodeBarcode(wot.getPinCode()));
        }

        return api;
    }

    @Override
    public ApiResult<String> verifyWoTReservationByDate(PartnerAccount account, Date date) throws BizValidationException {
        Date startDateTime = new Date(date.getTime() - 1000l);
        Date endDateTime = new Date(date.getTime() +  86400000l - 1000l);
        Map<String, Integer> wotList = new HashMap<String, Integer>();
        List<Integer> pendingList = wotRepo.findAllPendingInvoicedWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(pendingList != null && pendingList.size() > 0){
            for(Integer id : pendingList){
                if(id == null || id.intValue() == 0){
                    continue;
                }
                PPMFLGWingsOfTimeReservation wot = wotRepo.findById(id);
                if(wot == null){
                    continue;
                }
                wotList.put(wot.getReceiptNum(), wot.getId());
            }
            depositSrv.refreshWoTReservationListWithDateRange(account.getPartner().getAxAccountNumber(), wotList, startDateTime, endDateTime);
        }
        List<Integer> pending2List = wotRepo.findAllPendingInvoicedWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(pending2List != null && pending2List.size() > 0){
            throw new BizValidationException("Invoice have not yet generated, please try again later!");
        }
        Long count = wotRepo.findTotalInvoicedTransactionWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(count == null || count.intValue() == 0){
            throw new BizValidationException("No wings of time reservations found on "+(NvxDateUtils.formatDate(date, NvxDateUtils.BOOKING_DATE_FORMAT_DISPLAY))+" !");
        }
        return new ApiResult<>(true, null, null, null);
    }

    @Override
    public ApiResult<String> verifyWoTReservationByMonth(PartnerAccount account, Date validDate) throws BizValidationException {
        Date endOfMonth = new Date(validDate.getYear(), validDate.getMonth(), validDate.getDate(), 23, 59, 59);
        endOfMonth.setMonth(endOfMonth.getMonth()+1);
        endOfMonth.setDate(endOfMonth.getDate() - 1);
        Date now = new Date(System.currentTimeMillis());
        if(now.before(endOfMonth)){
            Date firstOfNextMonth = new Date(validDate.getYear(), validDate.getMonth(), validDate.getDate(), 0, 0, 0);
            firstOfNextMonth.setMonth(endOfMonth.getMonth()+1);
            throw new BizValidationException("Monthly receipt of "+NvxDateUtils.formatDate(endOfMonth, NvxDateUtils.WOT_TICKET_MONTH_PRINT_FORMAT)+" can be generated at "+NvxDateUtils.formatDate(firstOfNextMonth, NvxDateUtils.WOT_TICKET_MONTH_PRINT_FORMAT));
        }
        Date date = new Date(validDate.getYear(), validDate.getMonth(), validDate.getDate(), 0, 0, 0);
        Date startDateTime = new Date( date.getTime() - 1000l );
        date.setMonth(date.getMonth() + 1);
        Date endDateTime = new Date( date.getTime());
        Map<String, Integer> wotList = new HashMap<String, Integer>();
        List<Integer> pendingList = wotRepo.findAllPendingInvoicedWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(pendingList != null && pendingList.size() > 0){
            for(Integer id : pendingList){
                if(id == null || id.intValue() == 0){
                    continue;
                }
                PPMFLGWingsOfTimeReservation wot = wotRepo.findById(id);
                if(wot == null){
                    continue;
                }
                wotList.put(wot.getReceiptNum(), wot.getId());
            }
            depositSrv.refreshWoTReservationListWithDateRange(account.getPartner().getAxAccountNumber(), wotList, startDateTime, endDateTime);
        }
        List<Integer> pending2List = wotRepo.findAllPendingInvoicedWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(pending2List != null && pending2List.size() > 0){
            throw new BizValidationException("Invoice have not yet generated, please try again later!");
        }
        Long count = wotRepo.findTotalInvoicedTransactionWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        if(count == null || count.intValue() == 0){
            throw new BizValidationException("No wings of time reservations found in "+(NvxDateUtils.formatDate(endOfMonth, NvxDateUtils.WOT_TICKET_MONTH_PRINT_FORMAT))+" !");
        }
        return new ApiResult<>(true, null, null, null);
    }

    @Override
    public String genWoTReservationByDate(PartnerAccount account, Date date) {
        Date startDateTime = new Date(date.getTime() - 1000l);
        Date endDateTime = new Date(date.getTime() +  86400000l - 1000l);
        List<PPMFLGWingsOfTimeReservation> list = wotRepo.findAllInvoicedTransactionWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        return wotEmailSrv.generateWoTReservationDailyReceipts(account, list, date);
    }

    @Override
    public String genWoTReservationByMonth(PartnerAccount account, Date validDate) {
        Date endOfMonth = new Date(validDate.getYear(), validDate.getMonth(), validDate.getDate(), 23, 59, 59);
        endOfMonth.setMonth(endOfMonth.getMonth()+1);
        endOfMonth.setDate(endOfMonth.getDate() - 1);
        Date date = new Date(validDate.getYear(), validDate.getMonth(), validDate.getDate(), 0, 0, 0);
        Date startDateTime = new Date( date.getTime() - 1000l );
        date.setMonth(date.getMonth() + 1);
        Date endDateTime = new Date( date.getTime());
        List<PPMFLGWingsOfTimeReservation> list = wotRepo.findAllInvoicedTransactionWithDateRange(account.getMainAccount().getId(), startDateTime, endDateTime);
        return wotEmailSrv.generateWoTReservationMonthlyReceipts(account, list, date);
    }
}
