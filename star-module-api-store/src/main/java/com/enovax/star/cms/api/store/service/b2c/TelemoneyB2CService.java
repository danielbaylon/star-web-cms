package com.enovax.star.cms.api.store.service.b2c;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.service.TmServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@SuppressWarnings("Duplicates")
@Service
public class TelemoneyB2CService {

    private Logger log = LoggerFactory.getLogger(TelemoneyB2CService.class);

    @Autowired
    private TmServiceProvider tmServiceProvider;

    @Autowired
    private ObjectFactory<TelemoneyB2CSaleProcessor> tmB2CSaleProcessorFactory;

    public void processMerchantPost(TelemoneyResponse tmResponse) {
        final TelemoneyB2CSaleProcessor tmProcessorB2C = tmB2CSaleProcessorFactory.getObject();
        tmProcessorB2C.process(tmResponse);
    }
}
