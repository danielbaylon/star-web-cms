package com.enovax.star.cms.api.kioskprint.controller;

import com.enovax.star.cms.api.kioskprint.model.PrintTicketResult;
import com.enovax.star.cms.api.store.controller.OnlineStoreController;
import com.enovax.star.cms.b2cshared.repository.IB2CBookingRepository;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.ticketgen.KioskTicketsPackage;
import com.enovax.star.cms.commons.model.ticketgen.TicketXmlRecord;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.google.zxing.WriterException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

/**
 * Created by cornelius on 14/6/16.
 */
@Deprecated
public abstract class PrintApiController {

    private final static Logger logger = LoggerFactory.getLogger(PrintApiController.class.getName());

    private final static String PRINT_TICKET_URL = "http://127.0.0.1:9100/api/print-ticket";
    private final static String PRINT_RECEIPT_URL = "http://127.0.0.1:9100/api/print-receipt";
    public final static String PRINT_RECEIPT_DETAILS_URL = "http://127.0.0.1:9100/api/print-receipt-details";


    @Autowired
    private OnlineStoreController storeController;
    @Autowired
    private IB2CBookingRepository bookingRepository;

    @RequestMapping(value = "printTicketsAndReceipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<?>> printTicketsAndReceipt(@RequestBody String receiptNumber) {

        try {
            if(StringUtils.isEmpty(receiptNumber)) {
                logger.error("Empty parameter value supplied when calling API.");
                return generateExceptionResponse(ApiErrorCodes.General, "");
            }

            KioskTicketsPackage ticketPackage = retrieveTicketsFromSession();

            ResponseEntity<PrintTicketResult> printTicketResponse = printTickets(ticketPackage.getTicketXmls());

            PrintTicketResult printTicketResult = printTicketResponse.getBody();

            String receiptXml = ticketPackage.getReceiptXml();

            if(printTicketResult.hasErrors()) {
                logPrintErrors("Tickets", printTicketResult.getErrorMessages());

                receiptXml = getReceiptXMLWithQRCode(receiptNumber);
            }

            ResponseEntity<PrintTicketResult> printReceiptResponse = printReceipt(receiptXml);

            PrintTicketResult printReceiptResult = printReceiptResponse.getBody();

            if(printReceiptResult.hasErrors()) {
                logPrintErrors("Receipt", printTicketResult.getErrorMessages());
            }

            if(printTicketResult.hasErrors() && !printReceiptResult.hasErrors()) {
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "Please bring this slip and print your tickets <br /> at other ticketing kiosks or counters. <br /><span class=\"sub-line\">We are sorry for any inconvenience caused.</span>");
            }

            if(printTicketResult.hasErrors() && printReceiptResult.hasErrors()) {
                throw new Exception();
            }

            return generateSuccessResponse("Printing Completed");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            return generateExceptionResponse(ApiErrorCodes.TicketAndReceiptPrintError, "Please note down the receipt number (" + receiptNumber + ") and print your tickets at other ticketing kiosks or counters.<br /> We are sorry for any inconvenience caused.");
        }
    }

    @RequestMapping(value = "printTicketsForRecoveryFlow", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<?>> printTicketsForRecoveryFlow(@RequestBody String receiptNumber) {

        try {
            if(StringUtils.isEmpty(receiptNumber)) {
                logger.error("Empty parameter value supplied when calling API.");
                return generateExceptionResponse(ApiErrorCodes.General, "");
            }

            StoreTransaction storeTransaction = getStoreTransaction(receiptNumber);

            if(storeTransaction == null) {
                logPrintErrors("Tickets", "Invalid Transaction ID");
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "Invalid Transaction ID.");
            }

            if(storeTransaction.isRecoveryFlowTicketsPrinted()) {
                logPrintErrors("Tickets", "Receipt number has been used and no longer valid");
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "ALL tickets of the transaction have been issued.");
            }

            // Save Store Transaction to prevent further printing of the same receipt
            updateRecoveryFlowExecuted(storeTransaction);

            KioskTicketsPackage ticketPackage = retrieveTicketsFromSession();

            ResponseEntity<PrintTicketResult> printTicketResponse = printTickets(ticketPackage.getTicketXmls());

            PrintTicketResult printTicketResult = printTicketResponse.getBody();

            if(printTicketResult.hasErrors()) {
                logPrintErrors("Tickets", printTicketResult.getErrorMessages());
                throw new Exception();
            }

            return generateSuccessResponse("Printing Completed");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "Please print your tickets at counters.<br /> We are sorry for any inconvenience caused.");
        }
    }

    protected void updateRecoveryFlowExecuted(StoreTransaction storeTransaction) {

        storeTransaction.setRecoveryFlowTicketsPrinted(true);
        bookingRepository.saveStoreTransaction(getChannel(), storeTransaction);
    }

    protected StoreTransaction getStoreTransaction(String receiptNumber) {
        return bookingRepository.getStoreTransactionByReceipt(getChannel(), receiptNumber);
    }

    protected String generateQRCode(String ticketData) throws WriterException, IOException {
        return QRCodeGenerator.toBase64(ticketData, 150, 150);
    }

    protected String getReceiptXMLWithQRCode(String receiptNumber) throws WriterException, IOException {

        String qrCode = generateQRCode(receiptNumber);

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperReceiptWithQRCodePackage(getChannel().code, qrCode);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage.getReceiptXml();
    }

    protected KioskTicketsPackage retrieveTicketsFromSession() {

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperTicketsPackage(getChannel().code);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage;
    }

    protected ResponseEntity<PrintTicketResult> requestPrinterRESTAPI(String url, String requestJSONString) throws Exception {

        HttpEntity<String> requestEntity = new HttpEntity<>(requestJSONString, getJSONHttpHeaders());

        RestTemplate template = new RestTemplate();
        ResponseEntity<PrintTicketResult> printResult = template.postForEntity(url, requestEntity, PrintTicketResult.class);

        return printResult;
    }

    protected ResponseEntity<PrintTicketResult> printTickets(List<TicketXmlRecord> ticketsXml) throws Exception {

        logger.info("Ticket Printing started .....");

        logger.info("Total tickets to print: " + ticketsXml.size());

        String requestJSONString = convertToJSONString("tickets", ticketsXml);

        ResponseEntity<PrintTicketResult> printResult =requestPrinterRESTAPI(PRINT_TICKET_URL, requestJSONString);

        logger.info("Ticket Printing ended .....");

        return printResult;
    }

    protected ResponseEntity<PrintTicketResult> printReceipt(String receiptXml) throws Exception {

        logger.info("Receipt Printing started .....");

        String requestJSONString = convertToJSONString("templateXml", receiptXml);

        ResponseEntity<PrintTicketResult> printResult = requestPrinterRESTAPI(PRINT_RECEIPT_URL, requestJSONString);

        logger.info("Receipt Printing ended .....");

        return printResult;
    }

    protected String convertToJSONString(String rootName, Object content) {
        return "{ \"" + rootName + "\": " + JsonUtil.jsonify(content, false) + " }";
    }

    protected HttpHeaders getJSONHttpHeaders() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    protected ResponseEntity<ApiResult<?>> generateExceptionResponse(ApiErrorCodes errorCode, String message) {
        return new ResponseEntity<>(new ApiResult<>(errorCode, message, ""), HttpStatus.OK);
    }

    protected ResponseEntity<ApiResult<?>> generateSuccessResponse(String message) {
        return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
    }

    protected void logPrintErrors(String type, String errorMessages) {

        logger.info("Printing " + type + " encounters the following error(s):");
        logger.info(errorMessages);
    }

    protected abstract StoreApiChannels getChannel();
}