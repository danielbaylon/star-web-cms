package com.enovax.star.cms.api.store.model.partner.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class PartnerUser extends User {

    private Integer id;
    private Integer paId;
    private String  accountCode;
    private boolean isSubAccount;
    private PartnerAccount partnerAccount;

    public PartnerUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, Integer id, Integer paId, String accountCode, boolean isSubAccount) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.id = id;
        this.paId = paId;
        this.accountCode = accountCode;
        this.isSubAccount = isSubAccount;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPaId() {
        return paId;
    }
    public void setPaId(Integer paId) {
        this.paId = paId;
    }
    public String getAccountCode() {
        return accountCode;
    }
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }
    public boolean isSubAccount() {
        return isSubAccount;
    }
    public void setSubAccount(boolean subAccount) {
        isSubAccount = subAccount;
    }

    public PartnerAccount getPartnerAccount() {
        return partnerAccount;
    }

    public void setPartnerAccount(PartnerAccount partnerAccount) {
        this.partnerAccount = partnerAccount;
    }
}
