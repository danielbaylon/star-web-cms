package com.enovax.star.cms.api.store.repository.partner.security.ppmflg;

import com.enovax.star.cms.api.store.model.partner.ppmflg.PartnerUser;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;

/**
 * Created by jennylynsze on 10/17/16.
 */
@Component("PPMFLGCustomSecurityContextRepository")
public class CustomSecurityContextRepository extends HttpSessionSecurityContextRepository{

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    IPartnerAccountService accountService;


    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        SecurityContext context = super.loadContext(requestResponseHolder);
        Authentication authentication = context.getAuthentication();

        if(authentication == null) {
            return context;
        }
        if(!(authentication.getPrincipal() instanceof PartnerUser)){
            authentication.setAuthenticated(false);
            return context;
        }
        PartnerUser partnerUser = (PartnerUser) authentication.getPrincipal();
        try {
            if(accountService.isCurrentUserAccountInvalid(partnerUser.getId(), partnerUser.isSubAccount(), partnerUser.getPaId())) {
                authentication.setAuthenticated(false);
                return context;
            }
            try {
                PartnerVM partner = partnerUser.getPartnerAccount().getPartner();
                String tierIdStored = MgnlContext.getAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_TIER_ID, Context.SESSION_SCOPE);
                String tierIdNow = partner.getTierId();

                if(tierIdNow != null && !tierIdNow.equals(tierIdStored)
                        && requestResponseHolder.getRequest().getRequestURI().startsWith("/" + StoreApiChannels.PARTNER_PORTAL_MFLG.code + "/category")) {
                    MgnlContext.setAttribute(PartnerPortalConst.CACHE_PARTNER_PRODUCT_PRICE_MAP,
                            accountService.getCustomerItemPrice(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber()), Context.SESSION_SCOPE);
                }

                MgnlContext.setAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_TIER_ID, tierIdNow , Context.SESSION_SCOPE);

            }catch(Exception e) {
                logger.error("Error on changing the price group of customer", e);
            }

        } catch (Exception e) {
            return SecurityContextHolder.createEmptyContext();
        }

        return context;
    }
}
