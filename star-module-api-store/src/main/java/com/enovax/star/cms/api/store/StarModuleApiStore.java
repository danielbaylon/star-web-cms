package com.enovax.star.cms.api.store;

import com.enovax.star.cms.api.store.config.StarModuleApiStoreAppConfig;
import com.enovax.star.cms.api.store.config.StarModuleApiStoreWebConfig;
import com.enovax.star.cms.api.store.filter.SimpleCorsFilter;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * This class is optional and represents the configuration for the star-module-api-store module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/star-module-api-store</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class StarModuleApiStore implements ModuleLifecycle {

    /**
     * URL mapping for the dispatcherServlet with a default value. To configure in AdminCentral,
     * go to: Configuration > modules > kando-event-reservation-module, and create a folder called "config"
     * with a property "urlMappings" and value "whatever".
     *
     * Changing this property might require a server restart to take effect.
     */
    private String urlMappings = "/.store/*";

    private ContextLoader contextLoader;
    private DispatcherServlet defaultDispatcherServlet;

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
            //Retrieve serlvet context.
            ServletContext servletContext = getServletContext();

            //Initialize root context.

            //Initialize dispatcher servlet.

            AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
            webCtx.register(StarModuleApiStoreAppConfig.class,  StarModuleApiStoreWebConfig.class);

            registerDefaultDispatcherServlet(servletContext, webCtx);
        }
    }

    private void registerDefaultDispatcherServlet(ServletContext servletContext, AnnotationConfigWebApplicationContext webCtx) {
        defaultDispatcherServlet = new DispatcherServlet(webCtx);

        final ServletRegistration.Dynamic dispatcherRegistration = servletContext.addServlet("store-api-dispatcher", defaultDispatcherServlet);
        dispatcherRegistration.setLoadOnStartup(1);
        dispatcherRegistration.setInitParameter("dispatchOptionsRequest", "true");
        dispatcherRegistration.setAsyncSupported(true);

        final String[] urlMappingArray = urlMappings.split(",");
        dispatcherRegistration.addMapping(urlMappingArray);

        final FilterRegistration.Dynamic corsRegistration = servletContext.addFilter("storeCorsFilter", SimpleCorsFilter.class);
        corsRegistration.addMappingForServletNames(null, false, "store-api-dispatcher");
        corsRegistration.setInitParameter("dispatchOptionsRequest", "true");
        corsRegistration.setAsyncSupported(true);
    }


    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
            if (defaultDispatcherServlet != null) {
                defaultDispatcherServlet.destroy();
            }
            if (contextLoader != null) {
                contextLoader.closeWebApplicationContext(getServletContext());
            }
        }
    }

    protected ServletContext getServletContext() {
        return Components.getComponent(ServletContext.class);
    }

    /*
    Getters and Setters
     */

    public String getUrlMappings() {
        return urlMappings;
    }

    public void setUrlMappings(String urlMappings) {
        this.urlMappings = urlMappings;
    }
}
