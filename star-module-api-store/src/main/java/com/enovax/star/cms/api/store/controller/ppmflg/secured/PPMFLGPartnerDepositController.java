package com.enovax.star.cms.api.store.controller.ppmflg.secured;

import com.enovax.star.cms.api.store.controller.ppmflg.PPMFLGBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerDepositService;
import com.enovax.star.cms.api.store.service.ppmflg.IPartnerDepositReportService;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.DepositVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PagedData;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 6/13/16.
 */
@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/partner-deposit/")
public class PPMFLGPartnerDepositController extends PPMFLGBaseStoreApiController {

    @Autowired
    @Qualifier("PPMFLGIPartnerDepositService")
    private IPartnerDepositService depositService;

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService accountService;

    @Autowired
    @Qualifier("PPMFLGIPartnerDepositReportService")
    private IPartnerDepositReportService depositRptSrv;

    private static final String DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ID = "DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ID";
    private static final String DEPOSIT_TOPUP_TRANSACTION_RECEIPT_NUM = "DEPOSIT_TOPUP_TRANSACTION_RECEIPT_NUM";
    private static final String DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ST = "DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ST";

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;

    @RequestMapping(value = "get-deposit-topup-config", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<BigDecimal>> apiGetDepositTopupConfig(HttpServletRequest request) {
        log.info("Entered apiGetDepositTopupConfig...");
        try {
            float amt = paramSrv.getMinimumDepositTopupAmount();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", new BigDecimal(amt)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetDepositTopupConfig",e, BigDecimal.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-deposit", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchDeposit(HttpServletRequest request,
            @RequestParam(value="startDateStr", required = false) String startDateStr,
            @RequestParam(value="endDateStr", required = false) String endDateStr,
            @RequestParam(value="depositTxnQueryType", required = false) String depositTxnQueryType,
            @RequestParam("take") int take,
            @RequestParam("skip") int skip,
            @RequestParam("page") int page,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
    log.info("Entered apiSearchDeposit..");
    try {
        PartnerAccount account = getLoginUserAccount(request.getSession(false));
        Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDate(startDateStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY), true) : null;
        Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.populateQueryToDate(NvxDateUtils.parseDate(endDateStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY), true) : null;
        ApiResult<PagedData<List<DepositVM>>> data = depositService.getPagedViewDepositHist(account.getPartner().getAxAccountNumber(),account.getMainAccountId(), startDate, endDate, depositTxnQueryType, sortField, sortDirection, page, pageSize);
        int total = 0;
        boolean success = false;
        List<DepositVM> items = new ArrayList<DepositVM>();
        final ResultVM result = new ResultVM();
        if(data != null && data.isSuccess()){
            PagedData<List<DepositVM>> pagedData = data.getData();
            items = pagedData != null ? pagedData.getData() : new ArrayList<DepositVM>();
            total = pagedData != null ? pagedData.getSize() : 0;
            success = true;
        }
        result.setTotal(total);
        result.setStatus(success);
        result.setViewModel(items);
        return new ResponseEntity<>(new ApiResult<>(success, data != null ? data.getErrorCode() : "", data != null ? data.getMessage() : "", result), HttpStatus.OK);
    } catch (Exception e) {
            log.error("!!! System exception encountered [apiSearchDeposit] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "export-to-excel", method = {RequestMethod.POST})
    public void apiExportAsExcel(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam(value="startDateStr", required = false) String startDateStr,
                                 @RequestParam(value="endDateStr", required = false) String endDateStr,
                                 @RequestParam(value="depositTxnQueryType", required = false) String depositTxnQueryType) {
        log.info("Entered apiExportToExcel.");
        String fileName = "deposit-management-report.xlsx";
        SXSSFWorkbook workbook = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            if(depositTxnQueryType == null || depositTxnQueryType.trim().length() == 0){
                depositTxnQueryType = "All";
            }
            workbook = depositRptSrv.exportToExcel(account, startDate, endDate, depositTxnQueryType);
        } catch (Exception e) {
            fileName = "deposit-management-report-failed.xlsx";
            workbook = depositRptSrv.exportEmptyExcelWithError(e.getMessage());
        }
        try {
            response.resetBuffer();
            response.reset();
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
        } catch (Throwable e) {
        }
    }

    @RequestMapping(value = "get-deposit-balance", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<BigDecimal>> apiGetDepositBalance(HttpServletRequest request) {
        log.info("Entered apiGetDepositBalance...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            ApiResult<AxCustomerBalances> balance = depositService.getPartnerDepositBalanceWithResponse(account.getPartner().getAxAccountNumber());
            if(balance != null){
                if(balance.getData() != null){
                    return new ResponseEntity<>(new ApiResult<>(true, "", "", balance.getData().getBalance()), HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(new ApiResult<>(false, balance.getErrorCode(), balance.getMessage(), null), HttpStatus.OK);
                }
            }else{
                return new ResponseEntity<>(new ApiResult<>(false, balance.getErrorCode(), balance.getMessage(), null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetDepositBalance] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, BigDecimal.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "refresh-deposit-hist", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRefreshDepositHist(HttpServletRequest request) {
        log.info("Entered apiRefreshDepositHist...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            /***depositService.refreshPartnerTransactionHist(account.getId(), account.getPartner().getAxAccountNumber());***/
            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRefreshDepositHist] !!!");
            return new ResponseEntity<>(new ApiResult<>(false, "ERROR", e.getMessage(), null), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "pre-deposit-topup-process", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPreDepositTopUpProcess(HttpServletRequest request, @RequestBody BigDecimal amount) {
        log.info("Entered apiPreDepositBalanceProcess...");
        try {
            HttpSession session = request.getSession(false);
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            depositService.validateTopUpBalanceAmount(amount);
            PPMFLGDepositTransaction txn = depositService.createTopUpBalanceTxn(account, account.getPartner(), amount, paramSrv.getUserRequestAddress(request), session.getId());
            if(!TransactionStage.Reserved.name().equalsIgnoreCase(txn.getStatus())){
                throw new BizValidationException("Invalid deposit topup transaction status, please try again");
            }
            session.setAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ID, txn.getId());
            session.setAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_NUM, txn.getReceiptNum());
            session.setAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ST, txn.getStatus());
            return new ResponseEntity<>(new ApiResult<>(true, "","",""), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiPreDepositBalanceProcess",e, String.class), HttpStatus.OK);
        }
    }
    @RequestMapping(value = "redirect-deposit-topup-payment", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<CheckoutConfirmResult>> apiRedirectDepositTopupPayment(HttpServletRequest request, HttpServletResponse response) {
        log.info("Entered apiRedirectDepositTopupPayment...");
        try {
            HttpSession session = request.getSession(false);
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Integer txnId     = (Integer)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ID);
            String receiptNum = (String)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_NUM);
            String txnStatus  = (String)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ST);
            final CheckoutConfirmResult ccr = new CheckoutConfirmResult();
            if(paramSrv.isTMServiceEnabled()) {
                String redirectURL = depositService.populateDepositToupPaymentRedirectURL(PartnerPortalConst.Partner_Portal_Channel, account, txnId, receiptNum, txnStatus, paramSrv.getUserRequestAddress(request), session.getId());
                ccr.setRedirectUrl(redirectURL);
            }else{
                CheckoutConfirmResult ccr2 = depositService.fireLoadTestingResult(PartnerPortalConst.Partner_Portal_Channel, account, txnId, receiptNum, txnStatus, paramSrv.getUserRequestAddress(request), session.getId());
                depositService.fireLoadTestingResult(PartnerPortalConst.Partner_Portal_Channel, ccr2);
                ccr.setRedirectUrl(depositService.getDepositTopupTransactionTelemoneyReturnURL(PartnerPortalConst.Partner_Portal_Channel));
            }
            ccr.setTmRedirect(true);
            return new ResponseEntity<>(new ApiResult<>(true, "","",ccr), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiRedirectDepositTopupPayment",e, CheckoutConfirmResult.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "post-deposit-purchase-process", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPostDepositTopUpProcess(HttpServletRequest request) {
        log.info("Entered apiPostDepositTopUpProcess...");
        try {
            HttpSession session = request.getSession(false);
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Integer txnId     = (Integer)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ID);
            String receiptNum = (String)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_NUM);
            String txnStatus  = (String)session.getAttribute(DEPOSIT_TOPUP_TRANSACTION_RECEIPT_ST);
            ApiResult<String> result = depositService.populateDepositToupPaymentStatusInfo(account, txnId, receiptNum, txnStatus, paramSrv.getUserRequestAddress(request), session.getId());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiPostDepositTopUpProcess",e, String.class), HttpStatus.OK);
        }
    }
}
