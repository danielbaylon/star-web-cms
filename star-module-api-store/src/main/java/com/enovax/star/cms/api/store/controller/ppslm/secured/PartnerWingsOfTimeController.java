package com.enovax.star.cms.api.store.controller.ppslm.secured;


import com.enovax.star.cms.api.store.controller.ppslm.PPSLMBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerWoTReportService;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerWoTReservationService;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.ProductExtViewModelWrapper;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.PartnerExtAttrName;
import com.enovax.star.cms.partnershared.constant.ppslm.WingOfTimeChannel;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerProductService;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import com.enovax.star.cms.partnershared.service.ppslm.IWingsOfTimeSharedService;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/7/16.
 */
@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/partner-wot/")
public class PartnerWingsOfTimeController extends PPSLMBaseStoreApiController {

    @Autowired
    private IWingsOfTimeSharedService paWoTSrv;

    @Autowired
    private IPartnerWoTReservationService reservationService;

    @Autowired
    private IPartnerProductService paProdSrv;

    @Autowired
    private IProductService prodSrv;

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private IPartnerWoTReportService wotRptSrv;

    private static final String PROD_PRICE_ERROR_MSG = "System is retrieving price details, please try again.";

    private Map<String, BigDecimal> getProductItemPriceMap(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null){
            return null;
        }
        PartnerAccount account = getLoginUserAccount(session);
        if(account == null){
            return null;
        }

        Object obj = session.getAttribute(PartnerPortalConst.CACHE_WOT_PRODUCT_PRICE_CONFIG);
        if(obj == null || !(obj instanceof Map)){
            return null;
        }
        return (Map<String, BigDecimal>)obj;
    }

    private boolean cacheWoTProductPrice(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null){
            return false;
        }
        PartnerAccount account = getLoginUserAccount(session);
        if(account == null){
            return false;
        }

        Object obj = session.getAttribute(PartnerPortalConst.CACHE_WOT_PRODUCT_PRICE_CONFIG);
        if(obj != null && obj instanceof Map){
            return true;
        }
        final List<String> productIds = new ArrayList<String>();
        List<AxProduct> prodList = paWoTSrv.getWOTProducts();
        for (AxProduct prod : prodList) {
            productIds.add(prod.getProductListingId()+"");
        }
        if(productIds == null || productIds.size() == 0){
            return true;
        }
        ApiResult<List<ProductExtViewModel>> result = null;
        try {
            result = prodSrv.getDataForProducts(WingOfTimeChannel.CHANNEL, productIds);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if(result == null || result.getData() == null || result.getData().size() == 0){
            return true;
        }
        List<String> productIdList = new ArrayList<String>();
        List<String> productItemList = new ArrayList<String>();
        List<ProductExtViewModel> list = result.getData();
        for(ProductExtViewModel mode : list){
            if(!productIdList.contains(mode.getProductId())){
                productIdList.add(mode.getProductId());
            }
            productItemList.add(mode.getProductId()+"-"+ mode.getItemId());
        }
        Map<String, BigDecimal> productItemPriceMap = null;
        try{
            productItemPriceMap = paProdSrv.getProductItemPriceMap(WingOfTimeChannel.CHANNEL, account.getPartner().getAxAccountNumber(), productIdList, productItemList);
        }catch (Exception ex){
            return false;
        }
        if(productItemPriceMap != null && productItemPriceMap.size() > 0){
            session.setAttribute(PartnerPortalConst.CACHE_WOT_PRODUCT_PRICE_CONFIG, productItemPriceMap);
            return true;
        }
        return false;
    }

    @RequestMapping(value = "refresh-wot-pin", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiRefreshPIN(HttpServletRequest request) {
        log.info("Entered apiGetSchedule...");
        try {
            String errorMsg = "";
            try{
                if(!cacheWoTProductPrice(request)){
                    log.error("apiGetSchedule : Getting wings of time product price failed, please try again.");
                }
            }catch (Exception ex){
                errorMsg = "Getting wings of time product price failed : "+ex.getMessage();
                log.error("apiGetSchedule : Getting wings of time product price failed, please try again.", ex);
            }
            try{
                PartnerAccount account = getLoginUserAccount(request.getSession(false));
                this.reservationService.refreshPartnerReservations(account.getId(), account.getPartner().getAxAccountNumber());
            }catch (Exception ex){
                errorMsg = "Refresh wings of time pin code details failed : "+ex.getMessage();
                log.error("apiGetSchedule : Getting wings of time product price failed, please try again.", ex);
            }
            return new ResponseEntity<>(new ApiResult<>(errorMsg == null || errorMsg.trim().length() == 0, "",
                    errorMsg != null && errorMsg.trim().length() > 0 ? errorMsg.trim() : "", null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiRefreshPIN",e, ProductExtViewModelWrapper.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-schedule", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiGetSchedule(HttpServletRequest request) {
        log.info("Entered apiGetSchedule...");
        try {
            HttpSession session = request.getSession(false);
            if(session == null){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", "Invalid access, no partner login information found.", null), HttpStatus.OK);
            }
            PartnerAccount account = super.getLoginUserAccount(session);
            if(account == null || account.getPartner() == null || account.getPartner().getExtension() == null || account.getPartner().getExtension().size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", "Invalid access, no partner details found.", null), HttpStatus.OK);
            }
            boolean isWoTEnabled = false;
            List<PartnerExtVM> exts = account.getPartner().getExtension();
            if(exts != null){
                for(PartnerExtVM i : exts){
                    if(PartnerExtAttrName.WingsOfTimePurchaseEnabled.code.equalsIgnoreCase(i.getAttrName())){
                        if(i.getAttrValue() != null && i.getAttrValue().trim().length() > 0 && Boolean.TRUE.toString().equalsIgnoreCase(i.getAttrValue())){
                            isWoTEnabled = true;
                            break;
                        }
                    }
                }
            }
            if(isWoTEnabled){
                final List<String> productIds = new ArrayList<>();
                List<AxProduct> prodList = paWoTSrv.getWOTProducts();
                for (AxProduct prod : prodList) {
                    productIds.add(prod.getProductListingId() + "");
                }
                final ApiResult<List<ProductExtViewModel>> result = prodSrv.getDataForProducts(WingOfTimeChannel.CHANNEL, productIds);

                if (result.isSuccess()) {
                    return new ResponseEntity<>(new ApiResult<>(true, "", "", new ProductExtViewModelWrapper(result.getData())), HttpStatus.OK);
                }
                return new ResponseEntity<>(new ApiResult<>(false, result.getErrorCode(), result.getMessage(), null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", "Invalid Request, Access Denied.", null), HttpStatus.OK);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetSchedule",e, ProductExtViewModelWrapper.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-reservation", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchReservation(HttpServletRequest request,
            @RequestParam(value="startDateStr", required = false) String startDateStr,
            @RequestParam(value="endDateStr", required = false) String endDateStr,
            @RequestParam(value="reservationStartDateStr", required = false) String reservationStartDateStr,
            @RequestParam(value="reservationEndDateStr", required = false) String reservationEndDateStr,
            @RequestParam(value="filterStatus", required = false) String filterStatus,
            @RequestParam(value="showTime", required = false) String showTimes,
            @RequestParam("take") int take,
            @RequestParam("skip") int skip,
            @RequestParam("page") int page,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered apiSearchReservation..");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date reservationStartDate = reservationStartDateStr != null && reservationStartDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(reservationStartDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date reservationEndDate   = reservationEndDateStr != null && reservationEndDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(reservationEndDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;

            ApiResult<PagedData<List<WOTReservationVM>>> data = reservationService.getPagedWingsOfTimeReservations(account, startDate, endDate, reservationStartDate, reservationEndDate,  filterStatus, showTimes, sortField, sortDirection, page, pageSize);
            int total = 0;
            boolean success = false;
            List<WOTReservationVM> items = new ArrayList<WOTReservationVM>();
            final ResultVM result = new ResultVM();
            if(data != null && data.isSuccess()){
                PagedData<List<WOTReservationVM>> pagedData = data.getData();
                items = pagedData != null ? pagedData.getData() : new ArrayList<WOTReservationVM>();
                total = pagedData != null ? pagedData.getSize() : 0;
                success = true;
            }
            result.setTotal(total);
            result.setStatus(success);
            result.setViewModel(items);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSearchReservation",e, ResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "export-to-excel", method = {RequestMethod.POST})
    public void apiExportAsExcel(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value="startDateStr", required = false) String startDateStr,
                            @RequestParam(value="endDateStr", required = false) String endDateStr,
                            @RequestParam(value="reservationStartDateStr", required = false) String reservationStartDateStr,
                            @RequestParam(value="reservationEndDateStr", required = false) String reservationEndDateStr,
                            @RequestParam(value="filterStatus", required = false) String filterStatus,
                            @RequestParam(value="showTime", required = false) String showTimes) {
        log.info("Entered apiExportToExcel.");
        String fileName = "wings-of-time-report.xlsx";
        SXSSFWorkbook workbook = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date reservationStartDate = reservationStartDateStr != null && reservationStartDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(reservationStartDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date reservationEndDate   = reservationEndDateStr != null && reservationEndDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(reservationEndDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;

            workbook = wotRptSrv.exportAsExcel(account, startDate, endDate, reservationStartDate, reservationEndDate, filterStatus, showTimes);
        } catch (Exception e) {
            fileName = "wings-of-time-report-failed.xlsx";
            workbook = wotRptSrv.exportEmptyExcelWithError(e.getMessage());
        }
        try {
            response.resetBuffer();
            response.reset();
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
        } catch (Throwable e) {
        }
    }

    @RequestMapping(value = "get-reservation-printing-details/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiGetReservationPrintingDetails(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiGetReservation...");
        try {
            PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
            WOTReservationVM reservation = reservationService.getReservationPrintingDetailsByID(partnerAccount, reservationId);
            try{
                reservation.setBaseUrl(paramSrv.getApplicationContextPath());
                reservation = reservationService.generateWoTPinCodeBarcode(reservation);
            }catch (BizValidationException ex){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", ex.getMessage(), null), HttpStatus.OK);
            }catch (Exception ex){
                return new ResponseEntity<>(new ApiResult<>(false, "", "Generate Barcode for PIN Code failed.",  reservation), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", "",  reservation), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetReservationPrintingDetails",e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiGetReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiGetReservation...");
        try {
            PartnerAccount partnerAccount = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            WOTReservationVM reservation = reservationService.getReservationByID(partnerAccount, productItemPriceMap, reservationId);
            return new ResponseEntity<>(new ApiResult<>(true, "", "",  reservation), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetReservation",e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "validate-new-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiValidateNewReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiValidateReservation...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            reservation.setMainAccountId(account.getId());
            reservation.setCreatedByUsername(account.getUsername());
            reservation.setAxCustomerNumber(account.getMainAccount().getProfile().getAxAccountNumber());
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            ApiResult<WOTReservationCharge> result = reservationService.validateNewReservation(account, productItemPriceMap, reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiValidateNewReservation",e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "validate-update-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiValidateUpdateReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiValidateReservation...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            reservation.setMainAccountId(account.getId());
            reservation.setCreatedByUsername(account.getUsername());
            reservation.setAxCustomerNumber(account.getMainAccount().getProfile().getAxAccountNumber());
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            ApiResult<WOTReservationCharge> result = reservationService.validateUpdateReservation(account, productItemPriceMap, reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiValidateUpdateReservation",e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "validate-cancel-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiCalculateCancelReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiCalculateCancelReservation...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            ApiResult<WOTReservationCharge> result = reservationService.validateCancelReservation(account, productItemPriceMap, reservationId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCalculateCancelReservation",e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "cancel-reservation/{id}/{refund}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCancelReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId, @PathVariable("refund")Double refund) {
        log.info("Entered apiCancelReservation...");
        try {
            if(reservationId == null || reservationId.intValue() == 0 || refund == null ){
                throw new Exception("Invalid request found");
            }
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            ApiResult<String> result = reservationService.cancelReservation(account, productItemPriceMap, reservationId, refund, false);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCancelReservation",e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiSaveReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiGetReservation...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            reservation.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            ApiResult<WOTReservationVM> result = reservationService.saveReservation(account, productItemPriceMap, reservation, false);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSaveReservation",e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiUpdateReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiGetReservation...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if (productItemPriceMap == null || productItemPriceMap.size() == 0) {
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG, null), HttpStatus.OK);
            }
            reservation.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            ApiResult<WOTReservationVM> result = reservationService.updateReservation(account, productItemPriceMap, reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiUpdateReservation",e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "refresh-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRefreshPinCodeDetails(HttpServletRequest request, @PathVariable("id") Integer reservationId){
        log.info("Entered apiRefreshPinCodeDetails...");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            ApiResult<String> result = reservationService.refreshPinCodeReferenceIdAndInventTransId(account, reservationId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiRefreshPinCodeDetails",e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "verify-split-reservation/{id}/{qty}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WoTVM>> apiVerifySplitReservation(HttpServletRequest request,@PathVariable("id") Integer id, @PathVariable("qty") Integer qty){
        log.info("Entered apiVerifySplitReservation...");
        ApiResult<WoTVM> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            result = reservationService.verifyAndGetSplitReservationOrder(account, productItemPriceMap, id, qty);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifySplitReservation",e, WoTVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "cancel-split-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCancelSplitReservation(HttpServletRequest request,@PathVariable("id") Integer id){
        log.info("Entered apiCancelSplitReservation...");
        ApiResult<String> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            result = reservationService.verifyAndCancelSplitReservationOrder(account, id);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCancelSplitReservation",e, String.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "confirm-split-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WoTVM>> apiConfirmSplitReservation(HttpServletRequest request, @PathVariable("id") Integer id){
        log.info("Entered apiConfirmSplitReservation...");
        ApiResult<WoTVM> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            result = reservationService.verifyAndConfirmSplitReservationOrder(account, id);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiConfirmSplitReservation",e, WoTVM.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @RequestMapping(value = "verify-purchase-reservation/{id}/{qty}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WoTVM>> apiVerifyPurchaseReservation(HttpServletRequest request,@PathVariable("id") Integer id, @PathVariable("qty") Integer qty){
        log.info("Entered apiVerifySplitReservation...");
        ApiResult<WoTVM> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            result = reservationService.verifyAndGetPurchaseReservationOrder(account, productItemPriceMap, id, qty);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifyPurchaseReservation",e, WoTVM.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "cancel-purchase-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCancelPurchaseReservation(HttpServletRequest request,@PathVariable("id") Integer id){
        log.info("Entered apiCancelPurchaseReservation...");
        ApiResult<String> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            result = reservationService.verifyAndCancelPurchaseReservationOrder(account, id);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCancelPurchaseReservation",e, String.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "validate-purchase-reserved-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> validatePurchaseReservedReservationPrice(HttpServletRequest request, @PathVariable("id") Integer id) {
        log.info("Entered apiValidateReservation...");
        ApiResult<WOTReservationCharge> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            result = reservationService.validatePurchaseReservedReservationPrice(account, productItemPriceMap, id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("validatePurchaseReservedReservationPrice",e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "confirm-purchase-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WoTVM>> apiConfirmPurchaseReservation(HttpServletRequest request, @PathVariable("id") Integer id){
        log.info("Entered apiConfirmPurchaseReservation...");
        ApiResult<WoTVM> result = null;
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Map<String, BigDecimal> productItemPriceMap = getProductItemPriceMap(request);
            if(productItemPriceMap == null || productItemPriceMap.size() == 0){
                return new ResponseEntity<>(new ApiResult<WoTVM>(false, "", PROD_PRICE_ERROR_MSG,  null), HttpStatus.OK);
            }
            result = reservationService.verifyAndConfirmPurchaseReservationOrder(account, productItemPriceMap, id);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiConfirmPurchaseReservation",e, WoTVM.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "verifyDailyReservations", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiVerifyDailyReservations(HttpServletRequest request, @RequestParam(value = "date")String date){
        log.info("Entered apiVerifyDailyReservations...");
        ApiResult<String> result = null;
        try {
            if(date == null || date.trim().length() == 0){
                throw new BizValidationException("Please select a date to generate receipt");
            }
            Date validDate = null;
            try{
                validDate = NvxDateUtils.parseDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }catch (Exception ex){
                throw new BizValidationException("Please select a valid date to generate receipt");
            }catch(Throwable ex2){
                throw new BizValidationException("Please select a valid date to generate receipt");
            }
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            result = reservationService.verifyWoTReservationByDate(account, validDate);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifyDailyReservations",e, String.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "verifyMonthlyReservations", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> verifyMonthlyReservations(HttpServletRequest request, @RequestParam(value = "date")String date){
        log.info("Entered verifyMonthlyReservations...");
        ApiResult<String> result = null;
        try {
            if(date == null || date.trim().length() == 0){
                throw new BizValidationException("Please select a month to generate receipt");
            }
            Date validDate = null;
            try{
                validDate = NvxDateUtils.parseDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }catch (Exception ex){
                throw new BizValidationException("Please select a valid month to generate receipt");
            }catch(Throwable ex2){
                throw new BizValidationException("Please select a valid month to generate receipt");
            }
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            result = reservationService.verifyWoTReservationByMonth(account, validDate);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("verifyMonthlyReservations",e, String.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "genDailyReservations", method = {RequestMethod.POST})
    public void genDailyReservations(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "param")String date){
        log.info("Entered genDailyReservations...");
        ApiResult<String> result = null;
        try {
            if(date == null || date.trim().length() == 0){
                throw new BizValidationException("Please select a date to generate receipt");
            }
            Date validDate = null;
            try{
                validDate = NvxDateUtils.parseDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }catch (Exception ex){
                throw new BizValidationException("Please select a valid date to generate receipt");
            }catch(Throwable ex2){
                throw new BizValidationException("Please select a valid date to generate receipt");
            }
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            String filePath = reservationService.genWoTReservationByDate(account, validDate);
            downloadFile(request, response, filePath, "WOT-DAILY-RECEIPT-"+NvxDateUtils.formatDate(validDate, NvxDateUtils.WOT_TICKET_DATE_PRINT_FORMAT_DASH)+FileUtil.FILE_EXT_PDF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "genMonthlyReservations", method = {RequestMethod.POST})
    public void genMonthlyReservations(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "param")String date){
        log.info("Entered genMonthlyReservations...");
        String filePath = null;
        try {
            if(date == null || date.trim().length() == 0){
                throw new BizValidationException("Please select a month to generate receipt");
            }
            Date validDate = null;
            try{
                validDate = NvxDateUtils.parseDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }catch (Exception ex){
                throw new BizValidationException("Please select a valid month to generate receipt");
            }catch(Throwable ex2){
                throw new BizValidationException("Please select a valid month to generate receipt");
            }
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            filePath = reservationService.genWoTReservationByMonth(account, validDate);
            downloadFile(request, response, filePath, "WOT-MONTHLY-RECEIPT-"+NvxDateUtils.formatDate(validDate, NvxDateUtils.WOT_TICKET_MONTH_PRINT_FORMAT_DASH)+FileUtil.FILE_EXT_PDF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downloadFile(HttpServletRequest request, HttpServletResponse response, String fileFullPath, String fileName) {
        File f = new File(fileFullPath);
        if(f.exists() && f.canRead()){
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + fileName);
            InputStream is = null;
            OutputStream os = null;
            try{
                is = new FileInputStream(fileFullPath);
                int read=0;
                byte[] bytes = new byte[500];
                os = response.getOutputStream();
                while((read = is.read(bytes))!= -1){
                    os.write(bytes, 0, read);
                }
                os.flush();
            }catch(Exception ex){
                log.error("!!! System exception encountered wot receipt downloading !!!");
                ex.printStackTrace();
            }finally{
                if(is != null){
                    try{
                        is.close();
                        is = null;
                    }catch(Exception ex){
                    }
                }
                if(os != null){
                    os = null;
                }
            }
        }
    }
}
