package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.constant.TmLocale;
import com.enovax.payment.tm.constant.TmRequestType;
import com.enovax.payment.tm.model.TmFraudCheckPackage;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppmflg.DepositLocation;
import com.enovax.star.cms.commons.constant.ppmflg.DepositTransactionType;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTransactionDetails;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxDepositRequest;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGDepositTransactionRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerTransactionHistRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGWingsOfTimeReservationRepository;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppmflg.PPMFLGDepositTransactionSpecification;
import com.enovax.star.cms.commons.repository.specification.ppmflg.PPMFLGPartnerTransactionHistSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.service.axchannel.AxChannelDepositService;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.*;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.datatype.DatatypeConfigurationException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jennylynsze on 6/13/16.
 */
@Service("PPMFLGIPartnerDepositService")
public class DefaultPartnerDepositService implements IPartnerDepositService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerDepositService.class);

    @Autowired
    private AxChannelDepositService axDepositSrv;
    @Autowired
    private PPMFLGDepositTransactionRepository depositTxnRepo;
    @Autowired
    @Qualifier("PPMFLGIPartnerAccountService")
    private IPartnerAccountService paAccSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerAccountSharedService")
    private IPartnerAccountSharedService paAccSharedSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedService")
    private IPartnerSharedService paSharedSrv;
    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private PPMFLGWingsOfTimeReservationRepository wotRepo;
    @Autowired
    private PPMFLGPartnerTransactionHistRepository paTaHistRepo;
    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;
    @Autowired
    @Qualifier("PPMFLGITemplateService")
    private ITemplateService tplSrv;
    @Autowired
    private IMailService mailSrv;
    @Autowired
    @Qualifier("PPMFLGIEmailTemplateService")
    private IEmailTemplateService mailTplSrv;
    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private StarTemplatingFunctions starFn;
    @Autowired
    private HttpRequestUtil httpRequestUtil;

    @Override
    public ApiResult<AxCustomerBalances> getPartnerDepositBalanceWithResponse(String axAccountNumber) throws AxChannelException {
        String currencyCode    = PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE;
        return axDepositSrv.apiGetCustomerBalance(StoreApiChannels.PARTNER_PORTAL_MFLG, axAccountNumber, currencyCode);
    }

    public BigDecimal getPartnerDepositBalance(String axAccountNumber) throws BizValidationException{
        try{
            if(axAccountNumber == null || axAccountNumber.trim().length() == 0){
                throw new BizValidationException("Invalid retrieving deposit balance request.");
            }
            ApiResult<AxCustomerBalances> response = getPartnerDepositBalanceWithResponse(axAccountNumber);
            if(response != null && response.getData() != null && response.getData().getBalance() != null){
                if(response.getData().getBalance().doubleValue() <= 0){
                    // correct is must be less than zero
                    throw new BizValidationException("Please top-up your deposit balance before purchase.");
                }
                return response.getData().getBalance();
            }else{
                if(response != null){
                    throw new BizValidationException(response.getMessage());
                }else{
                    throw new BizValidationException("Retrieving deposit balance failed.");
                }
            }
        }catch (Exception ex){
            throw new BizValidationException(ex);
        }
    }

    @Override
    public void validateTopUpBalanceAmount(BigDecimal amount) throws BizValidationException {
        if(amount == null){
            throw new BizValidationException("Please enter correct amount to top-up.");
        }
        if(amount.intValue() <= 0){
            throw new BizValidationException("Please enter correct amount to top-up.");
        }
        float minimumTopupAmt = sysParamSrv.getMinimumDepositTopupAmount();
        if(amount.floatValue() < minimumTopupAmt ){
            throw new BizValidationException("Minimum top-up amount must not less than S$"+ NvxNumberUtils.formatToCurrency(new BigDecimal(minimumTopupAmt))+".");
        }
    }

    @Override
    public PPMFLGDepositTransaction getDepositTopupTransaction(String tmDataReceiptNumber, String tmDataSessionId, String transType) {
        if(tmDataReceiptNumber == null || tmDataReceiptNumber.trim().length() == 0 || tmDataSessionId == null || tmDataSessionId.trim().length() == 0 || transType == null || transType.trim().length() == 0 ){
            return null;
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findTxnByReceiptAndSessionIdAndTransType(tmDataReceiptNumber, tmDataSessionId, transType);
        if(list == null || list.size() == 0){
            return null;
        }
        if(list.size() > 1){
            return null;
        }
        if(list.size() == 1){
            return list.get(0);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public PPMFLGDepositTransaction createTopUpBalanceTxn(PartnerAccount account, PartnerVM partner, BigDecimal amount, String ipAddress, String userSessionId) throws BizValidationException {
        String tenderTypeId = sysParamSrv.getDepositTopupAxPaymentType();
        if(tenderTypeId == null || tenderTypeId.trim().length() == 0){
            log.error("Deposit top-up AX configuration not found!");
            throw new BizValidationException("Deposit top-up AX configuration not found!");
        }
        Date now = new Date(System.currentTimeMillis());
        PPMFLGDepositTransaction txn = new PPMFLGDepositTransaction();

        String receiptNum = ProjectUtils.generateReceiptNumber(StoreApiChannels.PARTNER_PORTAL_MFLG, now);

        // receipt details
        txn.setReceiptNum(receiptNum);
        txn.setTotalAmount(amount);
        txn.setCurrency(PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE);
        txn.setTransType(DepositTransactionType.DepositTopUp.name());
        txn.setStatus(TransactionStage.Reserved.name());
        txn.setLocation(DepositLocation.Online.name());
        txn.setTmMerchantId(starFn.getDefaultMerchant(PartnerPortalConst.Partner_Portal_Channel));

        // AX config
        txn.setTenderTypeId(tenderTypeId);

        // Customer Details
        txn.setIp(ipAddress);
        txn.setMainAccountId(account.getMainAccountId());
        txn.setUsername(account.getUsername());
        txn.setSubAccountTrans(account.isSubAccount());
        txn.setSessionId(userSessionId);

        // Transaction Date
        txn.setCreatedDate(new Date(System.currentTimeMillis()));

        depositTxnRepo.save(txn);
        return depositTxnRepo.findByReceiptNum(receiptNum);
    }

    @Transactional
    private PPMFLGDepositTransaction saveDepositTransactionForTMQueryProcessing(PPMFLGDepositTransaction t, TmServiceResult queryResult) throws BizValidationException {
        if (t == null || t.getId().intValue() == 0) {
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(t.getId());
        if (list == null || list.size() == 0 || list.size() > 1 || list.size() != 1) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if (txn == null) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        txn.setModifiedDate(new Date());
        txn.setTmStatus(EnovaxTmSystemStatus.TmQuerySent.name());
        txn.setTmQuerySentDate(new Date());
        if(queryResult != null && queryResult.isSuccess()){
            txn.setTmQuerySentStatus(EnovaxTmSystemStatus.Success.name());
        }else{
            txn.setTmQuerySentStatus(EnovaxTmSystemStatus.Failed.name());
        }
        txn.setTmQuerySentResponse(queryResult != null ? queryResult.getErrorMessage() : "");
        depositTxnRepo.save(txn);
        return txn;
    }

    @Transactional
    public PPMFLGDepositTransaction saveDepositTransactionForTMFailedProcessing(Integer txnId, String stageName, String statusName, String errorCode, String errorMsg, Date now, boolean forceFailed) throws BizValidationException {
        if (txnId == null || txnId.intValue() == 0) {
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(txnId);
        if (list == null || list.size() == 0 || list.size() > 1 || list.size() != 1) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if (txn == null) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        if(!forceFailed){
            if (TransactionStage.Success.name().equalsIgnoreCase(txn.getStatus())) {
                throw new BizValidationException("Invalid request, Deposit-topup status is already success.");
            }
            if ((EnovaxTmSystemStatus.Success.name().equals(txn.getTmStatus()))) {
                throw new BizValidationException("Invalid request, Deposit-topup TM status is already success.");
            }
        }
        txn.setModifiedDate(now);
        txn.setStatus(stageName);
        txn.setTmStatus(statusName);
        txn.setTmErrorCode(errorCode);
        txn.setTmErrorMessage(errorMsg);
        txn.setTmResponseDate(now);
        depositTxnRepo.save(txn);
        return txn;
    }

    @Transactional
    public PPMFLGDepositTransaction saveDepositTransactionForTMSuccessProcessing(Integer txnId, String stageName, String statusName, String maskedCc, String approvalCode, String tmPaymentType, Date now) throws BizValidationException {
        if (txnId == null || txnId.intValue() == 0) {
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(txnId);
        if (list == null || list.size() == 0 || list.size() > 1 || list.size() != 1) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if (txn == null) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        if (TransactionStage.Success.name().equalsIgnoreCase(txn.getStatus())) {
            throw new BizValidationException("Invalid request, Deposit-topup status is already success.");
        }
        if ((EnovaxTmSystemStatus.Success.name().equals(txn.getTmStatus()))) {
            throw new BizValidationException("Invalid request, Deposit-topup TM status is already success.");
        }
        txn.setStatus(stageName);
        txn.setTmStatus(statusName);
        txn.setMaskedCc(maskedCc);
        txn.setTmApprovalCode(approvalCode);
        txn.setPaymentType(tmPaymentType);
        txn.setModifiedDate(now);
        txn.setTmResponseDate(now);
        depositTxnRepo.save(txn);
        return txn;
    }

    @Transactional
    public PPMFLGDepositTransaction saveDepositTransactionForTMVoidProcessing(PPMFLGDepositTransaction mflgTxn, TmServiceResult voidResult) throws BizValidationException {
        if (mflgTxn == null || mflgTxn.getId().intValue() == 0) {
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(mflgTxn.getId());
        if (list == null || list.size() == 0 || list.size() > 1 || list.size() != 1) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if (txn == null) {
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        if(voidResult != null && voidResult.isSuccess()){
            txn.setStatus(TransactionStage.Incomplete.name());
            txn.setTmVoidStatus(EnovaxTmSystemStatus.VoidSuccess.name());
        }else{
            txn.setTmVoidStatus(EnovaxTmSystemStatus.VoidFailed.name());
        }
        txn.setTmVoidDate(new Date(System.currentTimeMillis()));
        txn.setTmVoidResponse(voidResult != null ? voidResult.getErrorMessage() : "");
        depositTxnRepo.save(txn);
        return txn;
    }

    @Override
    public ApiResult<String> populateDepositToupPaymentStatusInfo(PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException {
        if(txnId == null || txnId.intValue() == 0){
            throw new BizValidationException("Invalid request, No deposit-topup transaction id found");
        }
        if(receiptNum == null || receiptNum.trim().length() == 0){
            throw new BizValidationException("Invalid request, No deposit-topup receipt number found");
        }
        if(txnStatus == null || txnStatus.trim().length() == 0){
            throw new BizValidationException("Invalid request, No deposit-topup transaction status found");
        }
        if(ipAddress == null || ipAddress.trim().length() == 0){
            throw new BizValidationException("Invalid request, No user IP address found");
        }
        if(sessionId == null || sessionId.trim().length() == 0){
            throw new BizValidationException("Invalid request, No user session ID found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(txnId);
        if(list == null || list.size() == 0 || list.size() > 1 || list.size() != 1){
            throw new BizValidationException("Invalid request, No deposit-topup transaction found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if(txn == null){
            throw new BizValidationException("Invalid request, No deposit-topup transaction found");
        }
        if(!receiptNum.equals(txn.getReceiptNum())){
            throw new BizValidationException("Invalid request, Receipt number is not found in transaction.");
        }

        if(!ipAddress.equals(txn.getIp())){
            throw new BizValidationException("Invalid request, IP Address is not found in transaction.");
        }
        if(!sessionId.equals(txn.getSessionId())){
            throw new BizValidationException("Invalid request, Session Id is not found in transaction.");
        }
        if(!account.getUsername().equals(txn.getUsername())){
            throw new BizValidationException("Invalid request, User Name is not found in transaction.");
        }
        if(account.getMainAccount().getId().intValue() != txn.getMainAccountId().intValue()){
            throw new BizValidationException("Invalid request, Main Account is not found in transaction.");
        }
        if(TransactionStage.Reserved.name().equalsIgnoreCase(txn.getStatus())){
            return new ApiResult<>(true,"","","Deposit-topup transaction is still Pending.");
        }
        if(TransactionStage.Incomplete.name().equalsIgnoreCase(txn.getStatus())){
            return new ApiResult<>(true,"","","Deposit-topup transaction is Incomplete.");
        }
        if(TransactionStage.Failed.name().equalsIgnoreCase(txn.getStatus())){
            return new ApiResult<>(true,"","","Deposit-topup transaction is Failed.");
        }
        if((txn.getTmStatus() == null || txn.getTmStatus().trim().length() == 0)){
            return new ApiResult<>(true,"","","Deposit-topup transaction payment status not found.");
        }
        if(TransactionStage.Canceled.name().equalsIgnoreCase(txn.getStatus())){
            return new ApiResult<>(true,"","","Deposit-topup transaction is Canceled.");
        }
        if(TransactionStage.Success.name().equalsIgnoreCase(txn.getStatus())){}
        return new ApiResult<>(true,"","","Deposit Top Up Transaction Successful!");
    }

    @Transactional
    public String populateDepositToupPaymentRedirectURL(String channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException {
        Long tid = System.currentTimeMillis();
        log.info("["+tid+"]redirect-deposit-toup-payment:"+(String.format("TransId:%s, ReceiptNum:%s, TransStatus:%s, IP:%s, SessionId:%s, TaId:%s, LoginName:%s, IsSubAccount:%s  ", String.valueOf(txnId), receiptNum, txnStatus, ipAddress, sessionId, String.valueOf(account.getId()), account.getUsername(), ( account.isSubAccount() ? "Y" : "N" ))));
        if(txnId == null || txnId.intValue() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        if(receiptNum == null || receiptNum.trim().length() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup receipt number not found");
        }
        if(txnStatus == null || txnStatus.trim().length() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup transaction status not found");
        }
        if(ipAddress == null || ipAddress.trim().length() == 0){
            throw new BizValidationException("Invalid request, User IP address not found");
        }
        if(sessionId == null || sessionId.trim().length() == 0){
            throw new BizValidationException("Invalid request, User Session ID not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(txnId);
        if(list == null || list.size() == 0 || list.size() > 1 || list.size() != 1){
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if(txn == null){
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        if(!TransactionStage.Reserved.name().equalsIgnoreCase(txn.getStatus())){
            throw new BizValidationException("Invalid request, Deposit-topup status is not in Pending.");
        }
        if(!(txn.getTmStatus() == null || txn.getTmStatus().trim().length() == 0)){
            throw new BizValidationException("Invalid request, Deposit-topup TM status is not in Pending.");
        }
        if(!receiptNum.equals(txn.getReceiptNum())){
            throw new BizValidationException("Invalid request, Receipt number is not found in transaction.");
        }
        if(!txnStatus.equals(txn.getStatus())){
            throw new BizValidationException("Invalid request, Transaction status is not found in transaction.");
        }
        if(!ipAddress.equals(txn.getIp())){
            throw new BizValidationException("Invalid request, IP Address is not found in transaction.");
        }
        if(!sessionId.equals(txn.getSessionId())){
            throw new BizValidationException("Invalid request, Session Id is not found in transaction.");
        }
        if(!account.getUsername().equals(txn.getUsername())){
            throw new BizValidationException("Invalid request, User Name is not found in transaction.");
        }
        if(account.getMainAccount().getId().intValue() != txn.getMainAccountId().intValue()){
            throw new BizValidationException("Invalid request, Main Account is not found in transaction.");
        }

        final TelemoneyParamPackage tmParams;
        try {
            tmParams = tmParamRepository.getTmParams(StoreApiChannels.fromCode(channel), TmRequestType.DepositTopup);
        } catch (JcrRepositoryException e) {
            throw new BizValidationException("Invalid Config, Channel not found.");
        }

        String tmStatusUrl = tmParams.getTmStatusUrl();
        String tmReturnUrl = tmParams.getTmReturnUrl();
        if(tmReturnUrl == null || tmReturnUrl.trim().length() == 0){
            throw new BizValidationException("Invalid Config, TM Return URL not found.");
        }
        if(tmStatusUrl == null || tmStatusUrl.trim().length() == 0){
            throw new BizValidationException("Invalid Config, TM Status URL not found.");
        }

        final String receiptNumber = txn.getReceiptNum();
        final BigDecimal totalAmount = txn.getTotalAmount();
        final String merchantId = txn.getTmMerchantId();
        final TmCurrency tmCurrency = TmCurrency.SGD;
        final TmFraudCheckPackage fraudCheckPackage = new TmFraudCheckPackage();
        fraudCheckPackage.setFirstName(account.getUsername());
        fraudCheckPackage.setEmail(account.getPartner().getEmail());
        fraudCheckPackage.setIp(ipAddress);
        fraudCheckPackage.setEncoding(NvxUtil.DEFAULT_ENCODING);
        final TmLocale tmLocale = TmLocale.EnglishUs;
        final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
        merchantSignatureConfig.setSignatureEnabled(false);
        String returnUrlParamString = "";
        final TmPaymentRedirectInput input = new TmPaymentRedirectInput(tmReturnUrl, tmStatusUrl, returnUrlParamString, receiptNumber,
        totalAmount, merchantId, null, tmCurrency, null, tmLocale, merchantSignatureConfig);
        input.setUserField1(StoreApiChannels.fromCode(channel).code);
        input.setUserField2(TmRequestType.DepositTopup.name());
        input.setUserField3(sessionId);
        input.setUserField4(receiptNum);

        final String redirectUrl = tmServiceProvider.constructPaymentRedirectUrl(input);

        log.info("["+tid+"]redirect-deposit-toup-payment:"+(String.format("TransId:%s, ReceiptNum:%s, TransStatus:%s, IP:%s, SessionId:%s, TaId:%s, LoginName:%s, IsSubAccount:%s, RedirectURL:%s", String.valueOf(txnId), receiptNum, txnStatus, ipAddress, sessionId, String.valueOf(account.getId()), account.getUsername(), ( account.isSubAccount() ? "Y" : "N" ), redirectUrl)));

        txn.setTmRedirectUrl(redirectUrl);
        txn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
        txn.setTmRedirectDate(new Date(System.currentTimeMillis()));
        depositTxnRepo.save(txn);

        return redirectUrl;
    }

    @Override
    public CheckoutConfirmResult fireLoadTestingResult(String channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException {
        Long tid = System.currentTimeMillis();
        log.info("["+tid+"]redirect-deposit-toup-payment:"+(String.format("TransId:%s, ReceiptNum:%s, TransStatus:%s, IP:%s, SessionId:%s, TaId:%s, LoginName:%s, IsSubAccount:%s  ", String.valueOf(txnId), receiptNum, txnStatus, ipAddress, sessionId, String.valueOf(account.getId()), account.getUsername(), ( account.isSubAccount() ? "Y" : "N" ))));
        if(txnId == null || txnId.intValue() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup transaction id not found");
        }
        if(receiptNum == null || receiptNum.trim().length() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup receipt number not found");
        }
        if(txnStatus == null || txnStatus.trim().length() == 0){
            throw new BizValidationException("Invalid request, Deposit-topup transaction status not found");
        }
        if(ipAddress == null || ipAddress.trim().length() == 0){
            throw new BizValidationException("Invalid request, User IP address not found");
        }
        if(sessionId == null || sessionId.trim().length() == 0){
            throw new BizValidationException("Invalid request, User Session ID not found");
        }
        List<PPMFLGDepositTransaction> list = depositTxnRepo.findById(txnId);
        if(list == null || list.size() == 0 || list.size() > 1 || list.size() != 1){
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        PPMFLGDepositTransaction txn = list.get(0);
        if(txn == null){
            throw new BizValidationException("Invalid request, Deposit-topup detials not found");
        }
        if(!TransactionStage.Reserved.name().equalsIgnoreCase(txn.getStatus())){
            throw new BizValidationException("Invalid request, Deposit-topup status is not in Pending.");
        }
        if(!(txn.getTmStatus() == null || txn.getTmStatus().trim().length() == 0)){
            throw new BizValidationException("Invalid request, Deposit-topup TM status is not in Pending.");
        }
        if(!receiptNum.equals(txn.getReceiptNum())){
            throw new BizValidationException("Invalid request, Receipt number is not found in transaction.");
        }
        if(!txnStatus.equals(txn.getStatus())){
            throw new BizValidationException("Invalid request, Transaction status is not found in transaction.");
        }
        if(!ipAddress.equals(txn.getIp())){
            throw new BizValidationException("Invalid request, IP Address is not found in transaction.");
        }
        if(!sessionId.equals(txn.getSessionId())){
            throw new BizValidationException("Invalid request, Session Id is not found in transaction.");
        }
        if(!account.getUsername().equals(txn.getUsername())){
            throw new BizValidationException("Invalid request, User Name is not found in transaction.");
        }
        if(account.getMainAccount().getId().intValue() != txn.getMainAccountId().intValue()){
            throw new BizValidationException("Invalid request, Main Account is not found in transaction.");
        }

        final TelemoneyParamPackage tmParams;
        try {
            tmParams = tmParamRepository.getTmParams(StoreApiChannels.fromCode(channel), TmRequestType.DepositTopup);
        } catch (JcrRepositoryException e) {
            throw new BizValidationException("Invalid Config, Channel not found.");
        }

        String tmStatusUrl = tmParams.getTmStatusUrl();
        String tmReturnUrl = tmParams.getTmReturnUrl();
        if(tmReturnUrl == null || tmReturnUrl.trim().length() == 0){
            throw new BizValidationException("Invalid Config, TM Return URL not found.");
        }
        if(tmStatusUrl == null || tmStatusUrl.trim().length() == 0){
            throw new BizValidationException("Invalid Config, TM Status URL not found.");
        }

        final String receiptNumber = txn.getReceiptNum();
        final BigDecimal totalAmount = txn.getTotalAmount();
        final String merchantId = txn.getTmMerchantId();
        final TmCurrency tmCurrency = TmCurrency.SGD;
        final TmFraudCheckPackage fraudCheckPackage = new TmFraudCheckPackage();
        fraudCheckPackage.setFirstName(account.getUsername());
        fraudCheckPackage.setEmail(account.getPartner().getEmail());
        fraudCheckPackage.setIp(ipAddress);
        fraudCheckPackage.setEncoding(NvxUtil.DEFAULT_ENCODING);
        final TmLocale tmLocale = TmLocale.EnglishUs;
        final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
        merchantSignatureConfig.setSignatureEnabled(false);
        String returnUrlParamString = "";
        final TmPaymentRedirectInput input = new TmPaymentRedirectInput(tmReturnUrl, tmStatusUrl, returnUrlParamString, receiptNumber,
                totalAmount, merchantId, null, tmCurrency, null, tmLocale, merchantSignatureConfig);
        input.setUserField1(StoreApiChannels.fromCode(channel).code);
        input.setUserField2(TmRequestType.DepositTopup.name());
        input.setUserField3(sessionId);
        input.setUserField4(receiptNum);

        final String dataString = "TM_ApprovalCode=010203&TM_BankRespCode=00&TM_Currency=SGD&TM_DebitAmt="+NvxNumberUtils.formatToBigDecimalString(totalAmount)+"&TM_Error=&TM_ErrorMsg=&TM_ExpiryDate=2929&TM_MCode="+merchantId+"&TM_PaymentType=3&TM_RefNo="+receiptNumber+"&TM_Status=YES&TM_TrnType=sale&TM_UserField1="+channel+"&TM_UserField2=DepositTopup&TM_UserField3="+sessionId+"&TM_UserField4="+receiptNumber+"&TM_UserField5=&TM_CCNum=LOADTESTxxxxxx1111&TM_ECI=&TM_CAVV=&TM_XID=&TM_Original_RefNo=&TM_Signature=&TM_OriginalPayType=";

        log.info("["+tid+"]load testing redirect-deposit-toup-payment:"+(String.format("TransId:%s, ReceiptNum:%s, TransStatus:%s, IP:%s, SessionId:%s, TaId:%s, LoginName:%s, IsSubAccount:%s, RedirectURL:%s", String.valueOf(txnId), receiptNum, txnStatus, ipAddress, sessionId, String.valueOf(account.getId()), account.getUsername(), ( account.isSubAccount() ? "Y" : "N" ), dataString)));

        txn.setTmRedirectUrl(tmParams.getTmStatusUrl()+"?"+dataString);
        txn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
        txn.setTmRedirectDate(new Date(System.currentTimeMillis()));
        depositTxnRepo.save(txn);

        CheckoutConfirmResult ccr = new CheckoutConfirmResult();
        ccr.setTmRedirect(true);
        ccr.setRedirectUrl(dataString);
        return ccr;
    }

    public void fireLoadTestingResult(String channel, CheckoutConfirmResult ccr) {
        final TelemoneyParamPackage tmParams;
        try {
            tmParams = tmParamRepository.getTmParams(StoreApiChannels.fromCode(channel), TmRequestType.DepositTopup);
            try{
                httpRequestUtil.request(tmParams.getTmStatusUrl(), ccr.getRedirectUrl());
            }catch (Exception ex1){
                log.error("processing load testing URL string "+tmParams.getTmStatusUrl()+"?"+ccr.getRedirectUrl()+" failed : "+ ex1.getMessage(), ex1);
            }catch (Throwable ex2){
                log.error("processing load testing URL string "+tmParams.getTmStatusUrl()+"?"+ccr.getRedirectUrl()+" failed : "+ ex2.getMessage(), ex2);
            }

        } catch (JcrRepositoryException e) {
        }
    }

    @Override
    public String getDepositTopupTransactionTelemoneyReturnURL(String channel) {
        final TelemoneyParamPackage tmParams;
        try {
            tmParams = tmParamRepository.getTmParams(StoreApiChannels.fromCode(channel), TmRequestType.DepositTopup);
            if(tmParams != null){
                return tmParams.getTmReturnUrl();
            }
        } catch (JcrRepositoryException e) {
        }
        return null;
    }

    @Override
    public ApiResult<String> updateAxCustomerDepositBalance(PPMFLGDepositTransaction mflgTxn) throws Exception {
        PPMFLGPartner pa = paSharedSrv.getPartnerByMainAccountId(mflgTxn.getMainAccountId());
        return updateAxCustomerDepositBalance(pa.getAxAccountNumber(), mflgTxn.getReceiptNum());
    }

    @Override
    public ApiResult<String> updateAxCustomerDepositBalance(String axAccountNumber, String receiptNum) throws Exception {
        PPMFLGDepositTransaction txn = depositTxnRepo.findByReceiptNum(receiptNum);
        if(!TransactionStage.Success.name().equalsIgnoreCase(txn.getStatus())){
            throw new Exception("INVALID_DEPOSIT_BALANCE_REQUEST_STATUS");
        }
        AxDepositRequest request = new AxDepositRequest();
        request.setAccountNum(axAccountNumber);
        request.setCurrencyCode(txn.getCurrency());
        request.setAmount(txn.getTotalAmount());
        request.setPostingDate(txn.getCreatedDate());
        request.setDueDate(txn.getCreatedDate());
        request.setTenderTypeId(txn.getTenderTypeId());
        request.setTxt("top up "+txn.getCurrency()+""+txn.getTotalAmount()+" for customer "+axAccountNumber+" in transaction "+receiptNum);
        request.setDocumentNum(receiptNum);
        String exceptionMsg = "";
        String requestText = "";
        String responseText = "";
        try{
            requestText = JsonUtil.jsonify(request);
        }catch (Exception ex1){
        }catch (Throwable ex2){}
        ApiResult<String> axResponse = null;
        try{
            axResponse = axDepositSrv.apiTopupDeposit(StoreApiChannels.PARTNER_PORTAL_MFLG, request);
            if(axResponse != null){
                responseText = axResponse.getData();
            }
        }catch (Exception ex1){
            exceptionMsg = ex1.getMessage();
        }catch (Throwable ex2){
            exceptionMsg = ex2.getMessage();
        }finally{
            try{
                updateTopUpBalanceTxnAXRequestAndResponse(txn.getReceiptNum(), requestText, "Response Msg : "+String.valueOf(responseText)+", Exception Msg : "+String.valueOf(exceptionMsg));
            }catch (Exception ex1){
            }catch (Throwable ex2){}
        }
        return axResponse;
    }

    @Transactional(rollbackFor = Exception.class)
    public PPMFLGDepositTransaction updateTopUpBalanceTxnStatus(String receiptNum, ApiResult<String> axResponse) {
        PPMFLGDepositTransaction txn = depositTxnRepo.findByReceiptNum(receiptNum);
        if(!(TransactionStage.Reserved.name().equalsIgnoreCase(txn.getStatus()))){
            return null;
        }
        if(axResponse.isSuccess()){
            txn.setStatus(TransactionStage.Success.name());
        }else{
            txn.setStatus(TransactionStage.Failed.name());
        }
        depositTxnRepo.save(txn);
        return txn;
    }

    @Transactional(rollbackFor = Exception.class)
    public PPMFLGDepositTransaction updateTopUpBalanceTxnAXRequestAndResponse(String receiptNum, String axRequest, String axResponse) {
        PPMFLGDepositTransaction txn = depositTxnRepo.findByReceiptNum(receiptNum);
        if(!(TransactionStage.Success.name().equalsIgnoreCase(txn.getStatus()))){
            return null;
        }
        txn.setAxRequestText(axRequest);
        txn.setAxResponseText(axResponse);
        depositTxnRepo.save(txn);
        return txn;
    }

    @Transactional(readOnly = true)
    public ApiResult<PagedData<List<DepositVM>>> getPagedViewDepositHist(String axCustAccNum, Integer adminId, Date startDate, Date endDate, String depositTxnQueryType, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        /*localStorageData(axCustAccNum, adminId, startDate, endDate, sortField, sortDirection, page, pageSize);*/
        return axStorageData(axCustAccNum, adminId, startDate, endDate, depositTxnQueryType, sortField, sortDirection, page, pageSize);
    }

    private ApiResult<PagedData<List<DepositVM>>> axStorageData(String custAccNum, Integer adminId, Date startDate, Date endDate, String depositTxnQueryType, String sortField, String sortDirection, int page, int pageSize) {
        List<DepositVM> list = new ArrayList<>();
        PagedData<List<DepositVM>> paged = new PagedData<List<DepositVM>>();
        paged.setData(list);
        paged.setSize(0);
        ApiResult<PagedData<List<DepositVM>>> api = new ApiResult<PagedData<List<DepositVM>>>();
        api.setData(paged);
        api.setSuccess(true);

        if(adminId == null || adminId.intValue() == 0){
            api.setMessage("Invalid Request");
            api.setSuccess(false);
            return api;
        }

        if(custAccNum == null || custAccNum.trim().length() == 0){
            api.setMessage("Invalid Request");
            api.setSuccess(false);
            return api;
        }

        if(startDate == null || endDate == null || depositTxnQueryType == null || depositTxnQueryType.trim().length() == 0){
            return api;
        }

        String currency = PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE;
        ApiResult<List<AxCustomerTransactionDetails>> response = null;
        try {
            response = axDepositSrv.apiViewDepositTransactions(StoreApiChannels.PARTNER_PORTAL_MFLG, custAccNum, currency, startDate, endDate);
        } catch (AxChannelException e) {
            log.error("refresh partner '"+custAccNum+"' transaction history failed.Error Details : "+ e.getMessage());
            e.printStackTrace();
            api.setMessage("Calling AX API failed.");
            api.setSuccess(false);
            return api;
        } catch (DatatypeConfigurationException e) {
            log.error("refresh partner '"+custAccNum+"' transaction history failed.Error Details : "+ e.getMessage());
            e.printStackTrace();
            api.setMessage("Calling AX API failed.");
            api.setSuccess(false);
            return api;
        }
        if(response == null || (!response.isSuccess())){
            log.error("refresh partner '"+custAccNum+"' transaction history failed.Error Details : "+ JsonUtil.jsonify(response));
            api.setMessage("Calling AX API failed.");
            api.setSuccess(false);
            return api;
        }
        List<AxCustomerTransactionDetails> axList = response.getData();
        int total = 0;
        //int page, int pageSize

        if(axList != null && axList.size() > 0){
            int index = 0;
            int startIndex = (page - 1) * pageSize;
            int endIndex   = page * pageSize;
            for(AxCustomerTransactionDetails t : axList){
                if(depositTxnQueryType != null && depositTxnQueryType.trim().length() > 0 && (!"All".equalsIgnoreCase(depositTxnQueryType))){
                    if(depositTxnQueryType != null && depositTxnQueryType.trim().length() > 0 && t.getType() != null && t.getType().trim().length() > 0){
                        if(!depositTxnQueryType.equalsIgnoreCase(t.getType())){
                            continue;
                        }
                    }
                }
                Date dt = null;
                try {
                    dt = NvxDateUtils.parseDate(NvxDateUtils.formatDateForDisplay(t.getTransDate(), false), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                } catch (Exception e) {
                } catch (Throwable e) {
                }
                if(dt != null && startDate != null && endDate != null){
                    if(!(dt.compareTo(startDate) >= 0 && dt.compareTo(endDate) <= 0)){
                        continue;
                    }
                }else{
                    continue;
                }

                total += 1;

                if(index >= startIndex && index < endIndex){
                    DepositVM vm = new DepositVM();
                    vm.setDebit(new BigDecimal(0.0));
                    vm.setCredit(new BigDecimal(0.0));
                    vm.setId(t.getVoucher());
                    vm.setReceiptNo(t.getVoucher());
                    vm.setTransactionType(t.getTransType());
                    vm.setTransactionDate(NvxDateUtils.formatDateForDisplay(t.getTransDate(), false));
                    vm.setLocation(t.getPaymentReference());
                    vm.setPaymentReference(t.getPaymentReference());
                    vm.setCredit(t.getAmountCur() != null ? t.getAmountCur().abs() : new BigDecimal(0.0));
                    vm.setReceiptNo(t.getDocumentNum());
                    vm.setSalesPoolId(t.getSalesPoolId());
                    vm.setType(t.getType());
                    list.add(vm);
                }

                index ++;
            }
        }
        paged.setData(list);
        paged.setSize(total);
        api.setData(paged);
        api.setSuccess(true);
        return api;
    }

    private ApiResult<PagedData<List<DepositVM>>> localStorageData(String axCustAccNum, Integer mainAccountId, Date startDate, Date endDate, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("orderAccount", QCriteria.Operation.EQ, axCustAccNum));
        if(startDate != null){
            criterias.add(new QCriteria("transDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("transDate", QCriteria.Operation.LE, endDate));
        }
        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.DESC, "transDate");
        SpecificationBuilder builder = new SpecificationBuilder();
        Specifications querySpec = builder.build(criterias, PPMFLGPartnerTransactionHistSpecification.class);
        Page<PPMFLGPartnerTransactionHist> list = paTaHistRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        PagedData<List<DepositVM>> paged = new PagedData<List<DepositVM>>();
        paged.setData(getDepositVMsByTxnHistList(list));
        paged.setSize(Long.valueOf(list.getTotalElements()).intValue());
        ApiResult<PagedData<List<DepositVM>>> api = new ApiResult<PagedData<List<DepositVM>>>();
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    @Deprecated
    private ApiResult<PagedData<List<DepositVM>>> localStorageData_DepositTxn(Integer mainAccountId, Date startDate, Date endDate, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.EQ, mainAccountId));
        if(startDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.LE, endDate));
        }
        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.ASC, "createdDate");
        SpecificationBuilder builder = new SpecificationBuilder();
        Specifications querySpec = builder.build(criterias, PPMFLGDepositTransactionSpecification.class);
        Page<PPMFLGDepositTransaction> list = depositTxnRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        PagedData<List<DepositVM>> paged = new PagedData<List<DepositVM>>();
        paged.setData(getDepositVMs(list));
        paged.setSize(list.getNumberOfElements());
        ApiResult<PagedData<List<DepositVM>>> api = new ApiResult<PagedData<List<DepositVM>>>();
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    private List<DepositVM> getDepositVMs(List<AxCustomerTransactionDetails> txnList, int page, int pageSize) {
        List<DepositVM> list = new ArrayList<DepositVM>();
        if(txnList != null && txnList.size() > 0){
            int index = 0;
            Iterator<AxCustomerTransactionDetails> iter = txnList.iterator();
            while(iter.hasNext()){
                index ++;
                if(!(page * pageSize >= index) && ((page + 1) * pageSize < index)){
                    continue;
                }
                AxCustomerTransactionDetails txn = iter.next();
                DepositVM vm = new DepositVM();
                vm.setDebit(new BigDecimal(0.0));
                vm.setCredit(new BigDecimal(0.0));
                vm.setId(txn.getVoucher());
                vm.setTransactionType(txn.getTransType());
                vm.setLocation(txn.getPaymentReference());
                vm.setReceiptNo(txn.getVoucher());
                vm.setTransactionDate(NvxDateUtils.formatDateForDisplay(txn.getTransDate(), false));
                vm.setCredit(txn.getAmountCur() != null ? txn.getAmountCur().abs() : new BigDecimal(0.0));
                list.add(vm);
            }
        }
        return list;
    }


    public List<DepositVM> getDepositVMs(Page<PPMFLGDepositTransaction> txnList) {
        List<DepositVM> list = new ArrayList<DepositVM>();
        if(txnList != null && txnList.getSize() > 0){
            Iterator<PPMFLGDepositTransaction> iter = txnList.iterator();
            while(iter.hasNext()){
                PPMFLGDepositTransaction txn = iter.next();
                DepositVM vm = new DepositVM();
                vm.setDebit(new BigDecimal(0.0));
                vm.setCredit(new BigDecimal(0.0));
                vm.setId(String.valueOf(txn.getId()));
                vm.setLocation(txn.getLocation());
                if(txn.getTransType() != null && txn.getTransType().trim().length() > 0) {
                    String v = DepositTransactionType.valueOf(txn.getTransType()).description;
                    vm.setTransactionType(v);
                }
                vm.setMainAccountId(txn.getMainAccountId());
                vm.setReceiptNo(txn.getReceiptNum());
                vm.setTransactionDate(NvxDateUtils.formatDateForDisplay(txn.getCreatedDate(), false));
                vm.setCredit(txn.getTotalAmount());
                list.add(vm);
            }
        }
        return list;
    }

    private List<DepositVM> getDepositVMsByTxnHistList(Page<PPMFLGPartnerTransactionHist> txlist) {
        List<DepositVM> list = new ArrayList<DepositVM>();
        if(txlist != null && txlist.getSize() > 0){
            Iterator<PPMFLGPartnerTransactionHist> iter = txlist.iterator();
            while(iter.hasNext()){
                PPMFLGPartnerTransactionHist txn = iter.next();
                DepositVM vm = new DepositVM();
                vm.setDebit(new BigDecimal(0.0));
                vm.setCredit(new BigDecimal(0.0));
                vm.setId(txn.getVoucher());
                vm.setTransactionType(txn.getTransType());
                vm.setLocation(txn.getPaymentReference());
                vm.setReceiptNo(txn.getVoucher());
                vm.setTransactionDate(NvxDateUtils.formatDateForDisplay(txn.getTransDate(), false));
                vm.setCredit(txn.getAmountCur() != null ? txn.getAmountCur().abs() : new BigDecimal(0.0));
                list.add(vm);
            }
        }
        return list;
    }


    @Transactional(rollbackFor = Exception.class)
    public void refreshPartnerTransactionHist(Integer loginId, String custAccNum) throws Exception {

    }

    @Override
    public void sendDepositTopupTransactionReceiptEmail(PPMFLGDepositTransaction txn) {
        try{
            BigDecimal depositBalance = null;
            try{
                PPMFLGPartner pa = paSharedSrv.getPartnerByMainAccountId(txn.getMainAccountId());
                depositBalance = getPartnerDepositBalance(pa.getAxAccountNumber());
            }catch (Exception ex){
                log.error("Retrieve the deposit balance for sending email is failed "+txn.getId());
            }catch (Throwable ex){
                log.error("Retrieve the deposit balance for sending email is failed "+txn.getId());
            }
            sendDepositTopupTransactionReceiptEmail(txn, depositBalance);
        }catch (Exception ex1){
            log.error("Sending email for Deposit Topup is failed, due to : "+ex1.getMessage(), ex1);
        }catch (Throwable ex1){
            log.error("Sending email for Deposit Topup is failed, due to : "+ex1.getMessage(), ex1);
        }
    }

    public void sendDepositTopupTransactionReceiptEmail(PPMFLGDepositTransaction txn, BigDecimal balance) {
        String baseUrl = sysParamSrv.getApplicationContextPath();
        Map<String,String> contactDtls = sysParamSrv.getSentosaContactDetails();
        String ownerAddr = contactDtls.get(PartnerPortalConst.ADDRESS_OWN);
        String ownerCont = contactDtls.get(PartnerPortalConst.CONTACT_OWN);

        PPMFLGTAMainAccount mainAcc = paAccSharedSrv.getPartnerMainAccountByMainAccountId(txn.getMainAccountId());

        PPMFLGPartner partner = paSharedSrv.getPartnerByAccountCodeAndMainAccountId(mainAcc.getId(), mainAcc.getAccountCode());

        String address = paSharedSrv.getActiveExtensionPropertyValue("correspondenceAddress", partner.getId());
        String postalCode = paSharedSrv.getActiveExtensionPropertyValue("correspondencePostalCode", partner.getId());
        String city = paSharedSrv.getActiveExtensionPropertyValue("correspondenceCity", partner.getId());

        ReceiptVM receipt = new ReceiptVM();
        receipt.setHasTnc(false);
        receipt.setBaseUrl(baseUrl);
        receipt.setOrgName(partner.getOrgName());
        receipt.setAccountCode(partner.getAccountCode());
        receipt.setAddress(address+" " + postalCode+" " + city);
        receipt.setRecepitNum(txn.getReceiptNum());
        receipt.setCurrency(sysParamSrv.getDefaultPartnerRegistrationCurrencyCode());
        receipt.setShortCurrency(sysParamSrv.getDefaultPartnerRegistrationShortCurrencyCode());
        if(balance != null && balance.intValue() > 0){
            receipt.setDepositBalanceText(NvxNumberUtils.formatToCurrency(balance));
            receipt.setDepositBalanceAvaialble(true);
        }else{
            receipt.setDepositBalanceAvaialble(false);
            receipt.setDepositBalanceText("");
        }
        List<ReceiptItemVM> items = new ArrayList<ReceiptItemVM>();
        ReceiptItemVM item = new ReceiptItemVM();
        item.setItemTopup(false);
        item.setDisplayName("Deposit Top Up");
        item.setSubTotalStr(NvxNumberUtils.formatToCurrency(txn.getTotalAmount()));
        items.add(item);

        ReceiptProdVM prodVM = new ReceiptProdVM(txn.getReceiptNum(), "Deposit Top Up");
        prodVM.setItems(items);

        List<ReceiptProdVM> list = new ArrayList<ReceiptProdVM>();
        list.add(prodVM);

        receipt.setProds(list);
        receipt.setExclGstStr(NvxNumberUtils.formatToCurrency(txn.getTotalAmount()));
        receipt.setTotalStr(NvxNumberUtils.formatToCurrency(txn.getTotalAmount()));
        receipt.setOriginalMsg("ORIGINAL");
        receipt.setReprintedMsg("");
        receipt.setDollarsInwords(TransactionUtil.convertToWordsWithDecimal(txn.getTotalAmount()));
        receipt.setTncs(new ArrayList<>());
        receipt.setOwnAddress(ownerAddr);
        receipt.setOwnContact(ownerCont);
        receipt.setPrintedDtTimeStr(NvxDateUtils.formatDateForDisplay(new Date(System.currentTimeMillis()), true));
        receipt.setTransDtStr(NvxDateUtils.formatDateForDisplay(txn.getCreatedDate(), false));

        List<String> emails = new ArrayList<String>();
        emails.add(partner.getEmail());
        if(txn.getSubAccountTrans() != null && txn.getSubAccountTrans().booleanValue()){
            PPMFLGTASubAccount subAccount = paAccSrv.getSubAccountByUserName(txn.getUsername());
            if(subAccount != null && subAccount.getEmail() != null && subAccount.getEmail().trim().length() > 0){
                emails.add(subAccount.getEmail().trim());
            }
        }

        Long uuid = System.currentTimeMillis();

        boolean receiptPDF = false;
        String receiptName = null;
        String receiptAttchmentName = null;
        String receiptPath = null;
        String receiptRootDir = sysParamSrv.getPartnerTransactionReceiptRootDir();
        if(receiptRootDir != null && receiptRootDir.trim().length() > 0){
            receiptAttchmentName = txn.getReceiptNum()+ FileUtil.FILE_EXT_PDF;
            receiptName = "DepositTopUp-"+txn.getReceiptNum()+ "-" + (System.currentTimeMillis()) + FileUtil.FILE_EXT_PDF;
            receiptName = receiptName.toUpperCase();
            receiptPath = receiptRootDir + "/" + receiptName;
            try {
                receiptPDF = tplSrv.generateDepositTopupReceiptPDF(receipt, receiptPath);
            } catch (Exception e) {
                log.error("["+uuid+"] Deposit topup receipt not generated for deposit purchase due to exception : "+e.getMessage(), e);
            } catch (Throwable e) {
                log.error("["+uuid+"] Deposit topup receipt not generated for deposit purchase due to exception : "+e.getMessage(), e);
            }
        }else{
            log.error("["+uuid+"] Receipt not generated for deposit purchase due to PartnerTransactionReceiptRootDir not configured.");
        }

        String html = null;
        String uniqueTrackingId = "["+uuid+"] DepositTopUp-"+ (txn.getReceiptNum() != null && txn.getReceiptNum().trim().length() > 0 ? txn.getReceiptNum().trim() : "");
        try {
            html = tplSrv.generateDepositTopupReceiptHTML(receipt);
        } catch (Exception e) {
            log.error("["+uuid+"] Deposit topup email content not generated for deposit purchase due to exception : "+e.getMessage(), e);
        } catch (Throwable e) {
            log.error("["+uuid+"] Deposit topup email content not generated for deposit purchase due to exception : "+e.getMessage(), e);
        }

        boolean hasEmailTemplate = mailTplSrv.hasEmailTemplateByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_EMAIL);

        if(html != null && html.trim().length() > 0 && hasEmailTemplate){

            SysEmailTemplateVM mailTemplateVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_EMAIL);

            MailProperties props = new MailProperties();
            props.setSubject(mailTemplateVM.getSubject());
            props.setToUsers(emails.toArray(new String[emails.size()]));
            props.setBody(html);
            props.setBodyHtml(true);
            props.setTrackingId(uniqueTrackingId);
            props.setSender(mailSrv.getDefaultSender());
            if(receiptPDF){
                props.setAttachFilePaths(new String[]{ receiptPath });
                props.setAttachNames(new String[]{ receiptAttchmentName });
                props.setHasAttach(true);
            }else{
                props.setHasAttach(false);
            }
            try {
                log.debug("partner deposit service -- sending email for "+uniqueTrackingId);
                mailSrv.sendEmail(props, new MailCallback() {
                    @Override
                    public void complete(String trackingId, boolean success) {
                        log.info("Finished sending email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                    }
                });
            } catch (Exception e) {
                log.error("["+uuid+"] Sending deposit topup email "+uniqueTrackingId+" failed due to exception : "+e.getMessage(), e);
            } catch (Throwable e) {
                log.error("["+uuid+"] Sending deposit topup email "+uniqueTrackingId+" failed due to exception : "+e.getMessage(), e);
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void refreshWoTReservationListWithDateRange(String axCustNumber, Map<String, Integer>  wotList, Date startDateTime, Date endDateTime) throws BizValidationException {
        if(wotList == null || wotList.size() == 0 || startDateTime == null || endDateTime == null){
            return;
        }
        ApiResult<List<AxCustomerTransactionDetails>> apiResult = null;
        try {
            apiResult = axDepositSrv.apiViewDepositTransactions(StoreApiChannels.PARTNER_PORTAL_MFLG, axCustNumber, PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE, startDateTime, endDateTime);
        } catch (AxChannelException e) {
            e.printStackTrace();
            throw new BizValidationException("Getting reservation transaction invoice details failed, please try again later");
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new BizValidationException("Getting reservation transaction invoice details failed, please try again later");
        }
        if(apiResult == null){
            throw new BizValidationException("Getting reservation transaction invoice details failed, please try again later");
        }
        if(!apiResult.isSuccess()){
            throw new BizValidationException(apiResult.getMessage());
        }
        List<AxCustomerTransactionDetails> list = apiResult.getData();
        if(list == null || list.size() == 0){
            return;
        }

        for(AxCustomerTransactionDetails t : list){
            if(t == null){
                continue;
            }
            String ref = t.getPaymentReference();
            if(ref == null || ref.trim().length() == 0){
                continue;
            }
            if(!wotList.containsKey(ref)){
               continue;
            }
            Integer id = wotList.get(ref);
            if(id == null || id.intValue() == 0){
                continue;
            }
            PPMFLGWingsOfTimeReservation wot = wotRepo.findById(id);
            if(wot == null){
                continue;
            }
            if("INVOICED".equalsIgnoreCase(t.getType())){
                wot.setAxChargeAmount(t.getAmountCur());
                wot.setAxChargeDocumentNum(t.getDocumentNum());
                wot.setAxChargeInvoice(t.getInvoice());
                wot.setAxChargeVoucher(t.getVoucher());
                wotRepo.save(wot);
            }else if("REFUND".equalsIgnoreCase(t.getType())){
                //could have multiple refund
                BigDecimal axRefundAmount = wot.getAxRefundAmount() ==null? BigDecimal.ZERO: wot.getAxRefundAmount();
                wot.setAxRefundAmount(axRefundAmount.add(t.getAmountCur()));
                wot.setAxRefundDocumentNum(t.getDocumentNum());
                wot.setAxRefundInvoice(t.getInvoice());
                wot.setAxRefundVoucher(t.getVoucher());
                wotRepo.save(wot);
            }
        }
    }

    public void sendTmQueryForPendingTrans() {
        String merchantId = starFn.getDefaultMerchant(PartnerPortalConst.Partner_Portal_Channel);
        int lowerBoundMinutes = sysParamSrv.getDepositTopupTeleemoneyQuerySentLowerBoundMinutesAfterRequestSent();
        int upperBoundMinutes = sysParamSrv.getDepositTopupTeleemoneyQuerySentUpperBoundMinutesAfterRequestSent();

        long lowerBound = System.currentTimeMillis() - lowerBoundMinutes * 60000;
        long upperBound = System.currentTimeMillis() - upperBoundMinutes * 60000;

        Date lowerBoundDate = new Date(lowerBound);
        Date upperBoundDate = new Date(upperBound);

        List<PPMFLGDepositTransaction> list = depositTxnRepo.getPendingPaymentQueryList(lowerBoundDate, upperBoundDate);
        if(list != null && list.size() > 0){
            for(PPMFLGDepositTransaction t : list){
                if(t == null){
                    continue;
                }
                TmServiceResult queryResult = tmServiceProvider.sendQueryWithRetry(merchantId, t.getReceiptNum(), null);
                if(queryResult != null && queryResult.isSuccess()){
                    log.info("Query is sent is success for "+t.getReceiptNum()+"");
                }else{
                    log.info("Query is sent is failed for "+t.getReceiptNum()+", error : "+(queryResult != null ? queryResult.getErrorMessage() : ""));
                }
                try{
                    saveDepositTransactionForTMQueryProcessing(t, queryResult);
                }catch (Exception ex1){
                    log.error("Save transaction query response failed for "+t.getReceiptNum()+", error : "+ex1.getMessage());
                }catch (Throwable ex2){
                    log.error("Save transaction query response failed for "+t.getReceiptNum()+", error : "+ex2.getMessage());
                }
            }
        }
    }

    @Override
    public void sendVoidEmail(PPMFLGDepositTransaction txn, TmServiceResult voidResult) {
        //TODO:
    }
}
