package com.enovax.star.cms.api.kioskprint.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cornelius on 17/6/16.
 */
public class TicketStatus implements Serializable {

    private String id;
    private Boolean success;
    private String errorMessage;

    private List<String> statusCodes = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<String> getStatusCodes() {
        return statusCodes;
    }

    public void setStatusCodes(List<String> statusCodes) {
        this.statusCodes = statusCodes;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}