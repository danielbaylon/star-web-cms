package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.api.store.model.partner.ppslm.PPSLMTransactionAdapter;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTelemoneyLog;
import org.slf4j.Logger;

/**
 * Created by jennylynsze on 11/15/16.
 */

public interface ILogService {
    void logTelemoneyDetails(String title, PPSLMTelemoneyLog tm);
    void logTelemoneyDetails(Logger logger, String title, PPSLMTelemoneyLog tm);
    boolean logErrorDetails(String errorCode, String methodName, String details, PPSLMInventoryTransaction trans);
    void saveTelemoneyDetails(PPSLMTelemoneyLog tm);
    boolean logTelemoneyDetailsToDB(PPSLMTelemoneyLog tm);

    PPSLMTelemoneyLog createTelemoneyObject(Boolean isQuery, TelemoneyResponse response);

    boolean processError(String errorCode, String methodName, PPSLMInventoryTransaction trans, boolean sendEmail);

    boolean processDepositTopupError(String errorCode, String methodName, String details, PPSLMDepositTransaction trans, boolean sendEmail);

    boolean processError(String errorCode, String methodName, String details, PPSLMInventoryTransaction trans,
                         boolean sendEmail);

    void logInventoryTransactionDetails(String title, PPSLMInventoryTransaction tran);

    void logInventoryTransactionDetails(Logger logger, String title, PPSLMInventoryTransaction tran);

    boolean logErrorTransDetails(String errorCode, String methodName,
                                        String details, PPSLMTransactionAdapter trans);

    public boolean processTransError(String errorCode, String methodName, String details,
                                     PPSLMTransactionAdapter trans, boolean sendEmail);

    public void logTransDetails(Logger logger, String title, PPSLMTransactionAdapter tran);

}
