package com.enovax.star.cms.api.store.service.ppslm;


import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.DepositVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PagedData;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 6/13/16.
 */
@Service
public class DefaultPartnerDepositReportService implements IPartnerDepositReportService {

    private static final int[] columnWidth = new int[]{25, 25, 20, 15, 50};
    private static final String[] columnNames = new String[]{"Transaction Date", "Transaction Type", "Type", "Amount(S$)", "Reference Number"};
    private static final int columnCount = columnNames.length;
    private static final String emptyString = "";

    class WorkbookPropertyHolder{
        private int ri = 0;
        private int ci = 0;
        private int sn = 0;

        public int getRi() {
            return ri;
        }

        public void setRi(int ri) {
            this.ri = ri;
        }

        public int getCi() {
            return ci;
        }

        public void setCi(int ci) {
            this.ci = ci;
        }

        public int getSn() {
            return sn;
        }

        public void setSn(int sn) {
            this.sn = sn;
        }

        public int getNextRowIndex(){
            int temp = ri;
            ri += 1;
            ci = 0;
            return temp;
        }
        public int getNextColumnIndex(){
            int temp = ci;
            ci += 1;
            return temp;
        }
    }

    @Autowired
    private IPartnerDepositService paDepositSrv;

    @Override
    public SXSSFWorkbook exportEmptyExcelWithError(String message) {
        SXSSFWorkbook wb = new SXSSFWorkbook(3);
        wb.createSheet("Errors").createRow(0).createCell(0).setCellValue(String.valueOf(message));
        return wb;
    }
    
    @Override
    public SXSSFWorkbook exportToExcel(PartnerAccount account, Date startDate, Date endDate, String depositTxnQueryType) {
        int pageSize  = 100;
        ApiResult<PagedData<List<DepositVM>>> data = null;
        try {
            //String axCustAccNum, Integer adminId, Date startDate, Date endDate, String depositTxnQueryType, String sortField, String sortDirection, int page, int pageSize
            data = paDepositSrv.getPagedViewDepositHist(account.getPartner().getAxAccountNumber(), account.getMainAccountId(), startDate, endDate, depositTxnQueryType, null, null, 1, 100);
        } catch (Exception e) {
            return exportEmptyExcelWithError(e.getMessage());
        }
        DefaultPartnerDepositReportService.WorkbookPropertyHolder holder = new DefaultPartnerDepositReportService.WorkbookPropertyHolder();
        SXSSFWorkbook wb = new SXSSFWorkbook(3);

        CellStyle wrapTextStyle = wb.createCellStyle();
        wrapTextStyle.setWrapText(true);

        Sheet sheet = wb.createSheet("Deposit Management Report");
        int total = data.getData() != null ? data.getData().getSize() : 0;
        holder = writeReportHeader(holder, wb, sheet);
        holder = writeReportContent(holder, wb, sheet, data.getData() != null ? data.getData().getData() : null, wrapTextStyle);
        if(total > pageSize){
            int pages = total / pageSize;
            if((total % pageSize) > 0){
                pages += 1;
            }
            pages += 1;
            for(int i = 2 ; i< pages ; i++){
                try {
                    data = paDepositSrv.getPagedViewDepositHist(account.getPartner().getAxAccountNumber(), account.getMainAccountId(), startDate, endDate, depositTxnQueryType, null, null, i, 100);
                    holder = writeReportContent(holder, wb, sheet, data.getData() != null ? data.getData().getData() : null, wrapTextStyle);
                } catch (Exception e) {
                    try{
                        wb.close();
                        wb = null;
                    }catch (Exception ex1){
                    }catch (Throwable ex2){}
                    return exportEmptyExcelWithError(e.getMessage());
                }
            }
        }
        autoResizeColumns(wb,sheet);
        return wb;
    }

    private void autoResizeColumns(SXSSFWorkbook wb, Sheet sheet) {
        for(int i = 0  ; i < columnCount ; i++){
            sheet.setColumnWidth(i, columnWidth[i] * 256);
        }
    }

    private DefaultPartnerDepositReportService.WorkbookPropertyHolder writeReportHeader(DefaultPartnerDepositReportService.WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet) {
        Font font = wb.createFont();
        font.setBold(true);
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font);
        Row row = null;
        Cell cell = null;
        row = sheet.createRow(holder.getNextRowIndex());
        for(int i = 0 ;  i < columnCount ; i++){
            cell = row.createCell(holder.getNextColumnIndex());
            cell.setCellStyle(style);
            cell.setCellValue(columnNames[i]);
        }
        return holder;
    }

    private DefaultPartnerDepositReportService.WorkbookPropertyHolder writeReportContent(DefaultPartnerDepositReportService.WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet, List<DepositVM> data, CellStyle wrapTextStyle) {
        Row row = null;
        Cell cell = null;
        if(data != null && data.size() > 0){
            int total = data.size();
            for(int i = 0 ; i < total ; i++ ){
                DepositVM vm = data.get(i);
                row = sheet.createRow(holder.getNextRowIndex());
                holder = writeReportRowContent(holder, wb, sheet, row, cell, vm, wrapTextStyle);
            }
        }
        return holder;
    }

    private DefaultPartnerDepositReportService.WorkbookPropertyHolder writeReportRowContent(DefaultPartnerDepositReportService.WorkbookPropertyHolder holder, SXSSFWorkbook wb, Sheet sheet, Row row, Cell cell, DepositVM vm, CellStyle wrapTextStyle) {
        // Transaction Date
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(vm.getTransactionDate()));
        // Transaction Type
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(vm.getTransactionType()));
        // Type
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellStyle(wrapTextStyle);
        cell.setCellValue(getNonEmptyString(vm.getType()));
        // Amount
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(getNonEmptyString(NvxNumberUtils.formatToCurrency(vm.getCredit())));
        // Payment Reference
        cell = row.createCell(holder.getNextColumnIndex());
        cell.setCellValue(getNonEmptyString( vm.getPaymentReference()));

        cell = null;
        return holder;
    }

    public String getNonEmptyString(String str){
        if(str == null || str.trim().length() == 0){
            return emptyString;
        }
        return str.trim();
    }
}

