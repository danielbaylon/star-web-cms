package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.model.axchannel.penaltycharge.AxPenaltyCharge;

/**
 * Created by houtao on 5/9/16.
 */
public interface IWoTPenaltyChargeService {

    public AxPenaltyCharge getPenaltyCharge(String accountNum, String eventGroupId, String eventLineId) throws Exception;

}
