package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.processor.TmSaleProcessor;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.api.store.model.partner.ppmflg.PPMFLGTransactionAdapter;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTelemoneyLog;
import com.enovax.star.cms.commons.model.booking.TelemoneyProcessingDataPartnerRevalidation;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerRevalidationTransactionService;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerSharedAccountService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * Created by jennylynsze on 11/15/16.
 */

@SuppressWarnings("Duplicates")
@Component("PPMFLGDefaultTelemoneyRevalidationProcessor")
@Scope("prototype")
public class DefaultTelemoneyRevalidationProcessor extends TmSaleProcessor<TelemoneyProcessingDataPartnerRevalidation> {

    @Autowired
    @Qualifier("PPMFLGILogService")
    private ILogService logService;
    @Autowired
    @Qualifier("PPMFLGIPartnerRevalidationTransactionService")
    private IPartnerRevalidationTransactionService revalidationTransactionService;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedAccountService")
    private IPartnerSharedAccountService accSrv;
    @Autowired
    private TmServiceProvider tmServiceProvider;


    public final static String FAIL = "FAIL";
    public final static String PRTEH = "PurchaseRevalTransEventHook";
    public final static String PRTEH_REVAL_ST = PRTEH
            + " Confirm revalidateSales with ST SENT";
    public final static String PRTEH_REVALVOID_ST = PRTEH
            + " PurchaseRevalTransEventHook Error occurred while trying to voild revalidate.";
    public final static String PRTEH_REVAL_SUCCESS_ST = PRTEH
            + " Transaction {} has been confirmed. Processing continues...";
    public final static String PRTEH_REVALVOID_FAIL_ST = PRTEH
            + "ERROR: Unable to void recurring transaction at SACT!!! ";
    public final static String PRTEH_REVALVOID_SUCCESS_ST = PRTEH
            + " Recurring transaction has been voided at SACT successfully.";
    public final static String PRTEH_TRANS_NOT_CONFIRM = PRTEH
            + " Transaction cannot be confirmed! Receipt no.: ";
    public final static String PRTEH_TRANS_REDIRECT = PRTEH
            + " Customer has been redirected to an OTP gateway for payment verification."
            + " returned from Telemoney with an RDR Status. "
            + " Transaction with receipt number ";
    public final static String PRTEH_TRANS_SAVE_SUCCESS = PRTEH
            + "Successfully saved status=failed,usercancelled transacton in DB.";
    public final static String PRTEH_TRANS_SAVE_FAIL = PRTEH
            + "Error saving status=failed,usercancelled transaction in DB.";
    public final static String PRTEH_TRANS_USER_CANCEL = PRTEH
            + "User has cancelled his own transaction. Updating system.";
    public final static String PRTEH_TRANS_VOID_SUCCESS = PRTEH
            + "TicketingTmEvent :: Voided Transaction (Success)";
    public final static String PRTEH_GEN_RECEIPT_ERROR = PRTEH
            + "Error occurred during receipt generation. Performing error steps. Receipt no.: ";
    public final static String PRTEH_GEN_RECEIPT_SUCCESS = PRTEH
            + "TicketingTmEvent :: SUCCESSFUL RECEIPT SENT : ";


    @Transactional
    public void process(TelemoneyResponse tmResponse) {
        log.info("Start processing Telemoney POST for Partner Portal SLM - Inventory Purchase Flow...");
        log.info("Logging Telemoney Response...");
        log.info("[Telemoney Response] => " + JsonUtil.jsonify(tmResponse));

        boolean isFromQuery = false;

        String payment = TmPaymentType.getTitle(tmResponse.getTmPaymentType());
        log.debug("initialize payment: " + payment);

        final String tmDataReceiptNumber = tmResponse.getTmRefNo();
        final String tmDataChannel = tmResponse.getTmUserField1();
        final String tmDataSessionId = tmResponse.getTmUserField3();
        final String tmStatus = tmResponse.getTmStatus();
        final String tmMerchantId = tmResponse.getTmMerchantId();

        PPMFLGRevalidationTransaction revalTrans = revalidationTransactionService.findByReceipt(tmDataReceiptNumber);
        revalTrans.setPaymentType(payment);

        log.info("Creating Telemoney object...");
        final PPMFLGTelemoneyLog telemoneyLog = logService.createTelemoneyObject(isFromQuery, tmResponse);
        log.info("Logging TM object and transaction.");
        this.logService.logTelemoneyDetails(log, "ProcessTMStatusAction :: TM Parameters Accepted", telemoneyLog);

        final boolean tmDbSaved = this.logService.logTelemoneyDetailsToDB(telemoneyLog);
        if (!tmDbSaved) {
            log.warn("Telemoney details were not saved to the database.");
        }

        //We purposely only handle SALE type requests here. VOID type requests are handled in a different processing flow.
        final String tmTransType = tmResponse.getTmTransType();
        if (!TmTransType.Sale.tmCode.equalsIgnoreCase(tmTransType)) {
            log.info("Transaction was not of SALE type. Processing will not continue from here.");
            return;
        }

        ErrorTypes errorMsg = verifyRevalTrans(revalTrans, tmStatus,
                tmMerchantId);
        if (errorMsg != null) {
            PPMFLGTransactionAdapter trans = new PPMFLGTransactionAdapter(revalTrans,
                    revalTrans.getTransaction(), null);
            logService.processTransError(errorMsg.cd,
                    "ProcessRevalransaction: ", errorMsg.msg, trans, true);
            return;
        }

        //TODO maybe dont need to pass the channel
        StoreApiChannels channel;
        try {

            channel = StoreApiChannels.fromCode(tmDataChannel);
        } catch (Exception e) {
            log.error("Unable to find a valid channel for this response. EX: " + e.getMessage(), e);
            return;
        }

        final TelemoneyProcessingDataPartnerRevalidation data = new TelemoneyProcessingDataPartnerRevalidation();

        data.setChannel(channel);
        data.setReceiptNumber(tmDataReceiptNumber);
        data.setSlm(false);
        data.setRevalTransMflg(revalTrans);

        PPMFLGInventoryTransaction invTrans = revalTrans.getTransaction();
        data.setInvTransMflg(invTrans);
        data.setMainAccountMflg(accSrv.getMainUser(invTrans.getUsername(), false, false));
        data.setIsSubAccount(invTrans.isSubAccountTrans());
        data.setSubAccountMflg(data.isSubAccount() ? accSrv.getSubUser(invTrans.getUsername(), false, false) : null);
        data.setEmail(data.isSubAccount() ? data.getSubAccountMflg().getEmail() : data.getMainAccountMflg().getEmail());
        try {
            processTelemoneySalePost(tmResponse, data);
        } catch (Exception e) {
            log.error("General exception encountered while processing...", e);
        }
    }

    private ErrorTypes verifyRevalTrans(PPMFLGRevalidationTransaction revalTrans,
                                        String tmStatus, String tmCode) {
        log.info("Checking transaction existence.");
        if (revalTrans == null) {
            log.error("Retrieved transaction is null.");
            return ErrorTypes.TKT_INVALID_TRANS;
        }
        log.info("Checking transaction status.");
        if (!TicketStatus.Reserved.toString().equals(
                revalTrans.getStatus())) {
            log.error("Transaction status has already been set.");
            return ErrorTypes.TKT_STATUS_SET;
        }
        log.info("Checking transaction TM Merchant.");
        if (TmStatus.Success.equals(tmStatus)
                && !revalTrans.getTmMerchantId().equals(tmCode)) {
            log.error("Transaction's merchant code did not correspond to TM's code.");
            return ErrorTypes.TKT_WRONG_TMMID;
        }
        return null;
    }

    @Override
    protected void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerRevalidation data) {
        log.info("Telemoney response is SUCCESSFUL for the transaction. Processing...");
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        final BigDecimal tmDebitAmt = tmResponse.getTmDebitAmt().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        final BigDecimal transAmount = revalTrans.getTotal().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);

        if (!transAmount.equals(tmDebitAmt)) {
            final String errMsg = "TM debit amount of " + tmDebitAmt + " is different from the transaction amount of " + transAmount;
            log.error(errMsg);
            _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.Failed.toString(), "TMIncorrectAmount", errMsg, TicketStatus.Incomplete.toString());
        } else {
            log.info("Amount validated.");
            _processSuccessfulTransaction(tmResponse, data);
        }
    }

    @Override
    protected void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerRevalidation data) {
        log.info("Telemoney response is NOT FOUND for the transaction. Processing...");
        _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.NA.toString(), "TelemoneyNotFound", "Transaction not found at Telemoney.", TicketStatus.Incomplete.toString());
    }

    @Override
    protected void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerRevalidation data) {
        log.info("Telemoney response is FAILED for the transaction. Processing...");

        final String tmErr = tmResponse.getTmError();
        final String tmErrMsg = tmResponse.getTmErrorMessage();
        _processUnsuccessfulTrans(data, EnovaxTmSystemStatus.Failed.toString(),
                StringUtils.isEmpty(tmErr) ? "FAIL" : tmErr,
                StringUtils.isEmpty(tmErrMsg) ? "Telemoney returned Fail status" : tmErrMsg, TicketStatus.Failed.toString());
    }

    @Override
    protected void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerRevalidation data) {
//        log.info("Telemoney {} has indicated a REDIRECT status for the current transaction. " +
//                "Transaction {} is still pending at a third-party site.", tmResponse.getTmXid(), data.getSlmTxn().getReceiptNum());
        //Do nothing, and wait for the next response.
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        log.info(PRTEH_TRANS_REDIRECT + revalTrans.getReceiptNum());
    }

    private void _processSuccessfulTransaction(TelemoneyResponse tmResponse, TelemoneyProcessingDataPartnerRevalidation data) {
        log.info("Updating transaction with successful result.");
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        PPMFLGInventoryTransaction trans = data.getInvTransMflg();
        PPMFLGTransactionAdapter tranAdapter =  new PPMFLGTransactionAdapter(revalTrans, trans, tmResponse);

        this.logService.logTransDetails(log,
                "TicketingTmEvent :: Successful Transaction", tranAdapter);
        _completeSuccessfulTrans(tmResponse, data);
    }


    private void _processUnsuccessfulTrans(TelemoneyProcessingDataPartnerRevalidation data, String tmStatus, String errorCode, String errorMessage, String status) {
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        revalTrans.setTmStatus(tmStatus);
        revalTrans.setTmErrorMessage(errorCode + "-" + revalTrans.getReceiptNum() + ":" + errorMessage);
        revalTrans.setStatus(status);
        boolean saved = revalidationTransactionService.save(revalTrans);
        log.info(saved ? PRTEH_TRANS_SAVE_SUCCESS
                : PRTEH_TRANS_SAVE_FAIL);

    }

    private void _completeSuccessfulTrans(TelemoneyResponse response, TelemoneyProcessingDataPartnerRevalidation data) {
        log.info("Completing successful transaction...");
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        PPMFLGInventoryTransaction trans = data.getInvTransMflg();
        PPMFLGTransactionAdapter tranAdapter =  new PPMFLGTransactionAdapter(revalTrans, trans, response);

        try {
            log.info("Generating and sending receipt PDF + email.");
            boolean res = this.revalidationTransactionService.saveOnSuccessRevalidated(
                    revalTrans, trans, response.getTmApprovalCode());

            if(!res) {
                throw new Exception("Error encountered on sales order");
            }

        } catch (Exception e) {

            ErrorTypes etVoidSuccess = ErrorTypes.TKT_CONF0_VOID1;
            ErrorTypes etVoidFailed = ErrorTypes.TKT_CONF0_VOID0;
            String extraErrMsg = "Error encountered on sales order";
            _voidTransaction(data, etVoidSuccess, etVoidFailed, extraErrMsg);
        }

        //TODO evaluate if need to void or not
        try {
            this.revalidationTransactionService
                    .generateRevalReceiptAndEmail(trans.getId());

            this.logService.logTransDetails(log,
                    PRTEH_GEN_RECEIPT_SUCCESS, tranAdapter);
        }catch (Exception e) {
            log.error(
                    PRTEH_GEN_RECEIPT_ERROR + revalTrans.getReceiptNum(),
                    e);
            log.error(
                    "Error encountered for void recur during receipt gen!!", e);
        }

    }

    private boolean _voidTransaction(TelemoneyProcessingDataPartnerRevalidation data, ErrorTypes errCodeVoided, ErrorTypes errCodeUnvoided, String extraErrorMsg) {
        log.info("Voiding transaction with Telemoney.");
        PPMFLGRevalidationTransaction revalTrans = data.getRevalTransMflg();
        PPMFLGInventoryTransaction trans = revalTrans.getTransaction();

        final TmServiceResult voidResult = this.tmServiceProvider.sendVoidWithRetry(
                revalTrans.getTmMerchantId(), TmCurrency.valueOf(revalTrans.getCurrency()), revalTrans.getTotal(), revalTrans.getReceiptNum(), null);

        final TelemoneyResponse response = voidResult.getTmResponse();
        final String tmErrMsg = response==null?"FAIL":response.getTmErrorMessage();
        final String tmErr = response==null?"Null void result. Failed voiding.": response.getTmError();

        if (voidResult.isSuccess() && TmStatus.Success.equals(response.getTmStatus())) {
            log.info("Transaction successfully voided. Updating transaction in Database.");
            String errorMessage = (StringUtils.isEmpty(tmErrMsg) ? errCodeVoided.msg
                    : tmErrMsg)
                    + (StringUtils.isEmpty(extraErrorMsg) ? "" : extraErrorMsg);
            String errorCode = StringUtils.isEmpty(tmErr) ? errCodeVoided.cd
                    : tmErr;

            revalTrans.setTmStatus(EnovaxTmSystemStatus.VoidSuccess.toString());
            revalTrans
                    .setTmErrorMessage(errorCode
                            + " - " + revalTrans.getReceiptNum() + ":"
                            + (StringUtils.isEmpty(errorMessage) ? "Voided transaction, blank error message"
                            : errorMessage));

            revalTrans.setStatus(TicketStatus.Failed.toString());
            this.revalidationTransactionService.save(revalTrans);

            revalidationTransactionService.sendVoidEmail(true, revalTrans,
                    data.isSubAccount(), data.getSubAccountMflg(), data.getMainAccountMflg());

            PPMFLGTransactionAdapter tranAdapter = new PPMFLGTransactionAdapter(revalTrans, trans, response);

            this.logService.logTransDetails(log,
                    PRTEH_TRANS_VOID_SUCCESS, tranAdapter);

            this.logService.processTransError(errCodeVoided.cd,
                    "TicketingTmEvent._voidTransaction",
                    revalTrans.getReceiptNum() + ":" + errorMessage,
                    tranAdapter, true);
            return true;

        } else {

            log.info("Transaction voiding failed. Updating transaction in Database.");
            String errorCode = voidResult == null ? "FAIL" : response
                    .getTmError();
            errorCode = StringUtils.isEmpty(errorCode) ? tmErr : errorCode;
            String errorMessage = voidResult == null ? "Null void result. Failed voiding."
                    : response.getTmErrorMessage();
            errorMessage = (StringUtils.isEmpty(errorCode) ? tmErrMsg
                    : errorMessage)
                    + (StringUtils.isEmpty(extraErrorMsg) ? "" : extraErrorMsg);

            revalTrans.setTmStatus(EnovaxTmSystemStatus.VoidFailed.toString());
            revalTrans.setTmErrorMessage(errorCode + " - "+ trans.getReceiptNum()+":"+ errorMessage);
            revalTrans.setStatus(TicketStatus.Failed.toString());
            this.revalidationTransactionService.save(revalTrans);

            revalidationTransactionService.sendVoidEmail(true, revalTrans,
                    data.isSubAccount(), data.getSubAccountMflg(), data.getMainAccountMflg());

            PPMFLGTransactionAdapter tranAdapter = new PPMFLGTransactionAdapter(revalTrans, trans, response);

            this.logService.logTransDetails(log,
                    "TicketingTmEvent :: Voided Transaction (Failed)",
                    tranAdapter);

            this.logService.processTransError(errCodeVoided.cd,
                    "TicketingTmEvent._voidTransaction",
                    trans.getReceiptNum() + ":" + errorMessage,
                    tranAdapter, true);
            return false;

        }
    }
}
