package com.enovax.star.cms.api.store.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jace on 22/8/16.
 */
public class SkyDiningActionParam implements Serializable {

    protected Integer packageId;
    protected Date dateOfVisit;
    protected String jcNumber;
    protected String jcExpiry;
    protected Integer productId;
    protected Boolean getRelatedItems;
    protected String promoCode;
    protected String dateOfVisitText;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getJcNumber() {
        return jcNumber;
    }

    public void setJcNumber(String jcNumber) {
        this.jcNumber = jcNumber;
    }

    public String getJcExpiry() {
        return jcExpiry;
    }

    public void setJcExpiry(String jcExpiry) {
        this.jcExpiry = jcExpiry;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Boolean getGetRelatedItems() {
        return getRelatedItems;
    }

    public void setGetRelatedItems(Boolean getRelatedItems) {
        this.getRelatedItems = getRelatedItems;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDateOfVisitText() {
        return dateOfVisitText;
    }

    public void setDateOfVisitText(String dateOfVisitText) {
        this.dateOfVisitText = dateOfVisitText;
    }
}
