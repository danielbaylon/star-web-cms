package com.enovax.star.cms.api.store.service.ppslm;


import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppslm.DepositVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PagedData;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/13/16.
 */
public interface IPartnerDepositReportService {
    SXSSFWorkbook exportToExcel(PartnerAccount account, Date startDate, Date endDate, String depositTxnQueryType);

    SXSSFWorkbook exportEmptyExcelWithError(String message);
}
