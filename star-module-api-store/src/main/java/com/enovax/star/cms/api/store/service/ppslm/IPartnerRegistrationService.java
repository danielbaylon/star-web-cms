package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerRegistrationVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import info.magnolia.cms.beans.runtime.Document;

public interface IPartnerRegistrationService {

    public PartnerRegistrationVM getPartnerRegistrationParam();

    String registerPartner(PartnerVM partnerVM, Document doc1, Document doc2) throws Exception;

    boolean isAnExistingPartner(String accountCode, Integer paId);

    PartnerVM getPartnerDetailsByPartnerId(String accCodeId);

    PPSLMPartner getPartnerByAxAccountNumber(String custAccNum);

    String populatePartnerRegistrationFormResubmitRemarks(PartnerVM paVM);
}
