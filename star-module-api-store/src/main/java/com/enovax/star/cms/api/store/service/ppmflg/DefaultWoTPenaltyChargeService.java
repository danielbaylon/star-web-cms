package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.penaltycharge.AxPenaltyCharge;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by houtao on 5/9/16.
 */
@Service("PPMFLGIWoTPenaltyChargeService")
public class DefaultWoTPenaltyChargeService implements IWoTPenaltyChargeService {


    private static final StoreApiChannels CHANNEL = StoreApiChannels.PARTNER_PORTAL_MFLG;

    @Autowired
    private AxChannelTransactionService axTxnSrv;

    @Override
    public AxPenaltyCharge getPenaltyCharge(String accountNum, String eventGroupId, String eventLineId) throws Exception {
        validate(accountNum, eventGroupId, eventLineId);
        AxPenaltyCharge pc = null;
        pc = getCustomerSpecificPenaltyCharge(accountNum, eventGroupId, eventLineId);
        if(pc != null){
            return pc;
        }
        pc = getDefaultSpecificPenaltyCharge(eventGroupId, eventLineId);
        if(pc != null){
            return pc;
        }
        throw new Exception("No penalty charge configuration found");
    }

    private AxPenaltyCharge getDefaultSpecificPenaltyCharge(String eventGroupId, String eventLineId) throws Exception {
        ApiResult<List<AxPenaltyCharge>> apiResult = axTxnSrv.apiGetWoTCustPenaltyCharge(CHANNEL, "");
        if(!apiResult.isSuccess()){
            throw new Exception(apiResult.getMessage());
        }
        List<AxPenaltyCharge> pcs = apiResult.getData();
        if(pcs != null && pcs.size() > 0){
            for(AxPenaltyCharge pc : pcs){
                if("".equals(pc.getAccountNum()) && eventGroupId.equals(pc.getEventGroupId()) && eventLineId.equals(pc.getEventLineId())){
                    return pc;
                }
            }
            for(AxPenaltyCharge pc : pcs){
                if("".equals(pc.getAccountNum()) && eventGroupId.equals(pc.getEventGroupId()) && "".equals(pc.getEventLineId().trim())){
                    return pc;
                }
            }
            for(AxPenaltyCharge pc : pcs){
                if("".equals(pc.getAccountNum()) && "".equals(pc.getEventGroupId()) && "".equals(pc.getEventLineId().trim())){
                    return pc;
                }
            }
        }
        return null;
    }

    private AxPenaltyCharge getCustomerSpecificPenaltyCharge(String accountNum, String eventGroupId, String eventLineId) throws Exception {
        ApiResult<List<AxPenaltyCharge>> apiResult = axTxnSrv.apiGetWoTCustPenaltyCharge(CHANNEL, accountNum);
        if(!apiResult.isSuccess()){
            throw new Exception(apiResult.getMessage());
        }
        List<AxPenaltyCharge> pcs = apiResult.getData();
        if(pcs != null && pcs.size() > 0){
            for(AxPenaltyCharge pc : pcs){
                if(accountNum.equals(pc.getAccountNum()) && eventGroupId.equals(pc.getEventGroupId()) && eventLineId.equals(pc.getEventLineId())){
                    return pc;
                }
            }
            for(AxPenaltyCharge pc : pcs){
                if(accountNum.equals(pc.getAccountNum()) && eventGroupId.equals(pc.getEventGroupId()) && "".equals(pc.getEventLineId())){
                    return pc;
                }
            }
            for(AxPenaltyCharge pc : pcs){
                if(accountNum.equals(pc.getAccountNum()) && "".equals(pc.getEventGroupId()) && "".equals(pc.getEventLineId().trim())){
                    return pc;
                }
            }
        }
        return null;
    }


    private void validate(String accountNum, String eventGroupId, String eventLineId) throws Exception {
        if(accountNum == null || accountNum.trim().length() == 0){
            throw new Exception("Invalid parameter account number ["+accountNum+"] for obtaining penalty charge");
        }
        if(eventGroupId == null || eventGroupId.trim().length() == 0){
            throw new Exception("Invalid parameter event group ["+eventGroupId+"] for obtaining penalty charge");
        }
        if(eventLineId == null || eventLineId.trim().length() == 0){
            throw new Exception("Invalid parameter event line ["+eventLineId+"] for obtaining penalty charge");
        }
    }
}
