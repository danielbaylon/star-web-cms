package com.enovax.star.cms.api.store.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.SecurityContextRepository;

@Configuration
@EnableWebSecurity
public class StarModuleApiStoreSecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    @Configuration
    @Order(0)
    public static class SecurityConfigPPSLM extends WebSecurityConfigurerAdapter {
        @Autowired
        @Qualifier("PPSLMUserDetailsService")
        private UserDetailsService ppslmUserDetailsService;

        @Autowired
        @Qualifier("PPSLMDaoAuthenticationProvider")
        private DaoAuthenticationProvider ppslmAuthenticationProvider;

        @Autowired
        @Qualifier("PPSLMCustomSecurityContextRepository")
        private SecurityContextRepository ppslmSecurityContextRepository;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Override
        public void configure(WebSecurity webSecurity) throws Exception {
            // web resource will not going to this section
            webSecurity.ignoring().antMatchers("/.resources").antMatchers("/.resources/**");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            ppslmAuthenticationProvider.setUserDetailsService(ppslmUserDetailsService);
            ppslmAuthenticationProvider.setPasswordEncoder(passwordEncoder);
            http.csrf().disable().headers().frameOptions().disable()
                    .requestMatchers()
                    .antMatchers("/.store/ppslm/**","/partner-portal-slm/**")
                    .and()
                    .userDetailsService(ppslmUserDetailsService)
                    .authenticationProvider(ppslmAuthenticationProvider)
                    .authorizeRequests()
                    .antMatchers("/partner-portal-slm").permitAll()
                    .antMatchers("/partner-portal-slm/forgot-password").permitAll()
                    .antMatchers("/partner-portal-slm/partner-registration").permitAll()
                    .antMatchers("/partner-portal-slm/partner-registration-tnc").permitAll()
                    .antMatchers("/partner-portal-slm/maintenance").permitAll()
                    .antMatchers("/partner-portal-slm/data-protection-policy").permitAll()
                    .antMatchers("/partner-portal-slm/**")
                    .hasAuthority(com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst.AUTHORNIZED_USER)
                    .antMatchers("/.store/ppslm/publicity/**").permitAll()
                    .antMatchers("/.store/ppslm/secured/**")
                    .hasAuthority(com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst.AUTHORNIZED_USER)
                    .and()
                    .securityContext().securityContextRepository(ppslmSecurityContextRepository)
                    .and()
                    .formLogin()
                    .loginPage("/partner-portal-slm")
                    .loginProcessingUrl("/.store/ppslm/publicity/partner-account/login")
                    .failureUrl("/.store/ppslm/publicity/partner-account/login-failure")
                    .defaultSuccessUrl("/.store/ppslm/secured/partner-account/login-success")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .and()
                    .logout()
                    .logoutUrl("/.store/ppslm/publicity/partner-account/logout")
                    .logoutSuccessUrl("/.store/ppslm/publicity/partner-account/logout-success")
                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/partner-portal-slm")

            ;
        }
    }

    @Configuration
    @Order(1)
    public static class SecurityConfigPPMFLG extends WebSecurityConfigurerAdapter {

        @Autowired
        @Qualifier("PPMFLGUserDetailsService")
        private UserDetailsService ppmflgUserDetailsService;

        @Autowired
        @Qualifier("PPMFLGDaoAuthenticationProvider")
        private DaoAuthenticationProvider ppmflgAuthenticationProvider;

        @Autowired
        @Qualifier("PPMFLGCustomSecurityContextRepository")
        private SecurityContextRepository ppmflgCustomSecurityCtxRepo;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Override
        public void configure(WebSecurity webSecurity) throws Exception {
            // web resource will not going to this section
            webSecurity.ignoring().antMatchers("/.resources").antMatchers("/.resources/**");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            ppmflgAuthenticationProvider.setPasswordEncoder(passwordEncoder);
            ppmflgAuthenticationProvider.setUserDetailsService(ppmflgUserDetailsService);

            http.csrf().disable().headers().frameOptions().disable()
                    .requestMatchers()
                    .antMatchers("/.store/ppmflg/**","/partner-portal-mflg/**")
                    .and()
                    .userDetailsService(ppmflgUserDetailsService)
                    .authenticationProvider(ppmflgAuthenticationProvider)
                    .authorizeRequests()
                    .antMatchers("/partner-portal-mflg").permitAll()
                    .antMatchers("/partner-portal-mflg/forgot-password").permitAll()
                    .antMatchers("/partner-portal-mflg/partner-registration").permitAll()
                    .antMatchers("/partner-portal-mflg/partner-registration-tnc").permitAll()
                    .antMatchers("/partner-portal-mflg/maintenance").permitAll()
                    .antMatchers("/partner-portal-mflg/data-protection-policy").permitAll()
                    .antMatchers("/partner-portal-mflg/**")
                    .hasAuthority(com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst.AUTHORNIZED_USER)
                    .antMatchers("/.store/ppmflg/publicity/**").permitAll()
                    .antMatchers("/.store/ppmflg/secured/**")
                    .hasAuthority(com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst.AUTHORNIZED_USER)
                    .and()
                    .securityContext().securityContextRepository(ppmflgCustomSecurityCtxRepo)
                    .and()
                    .formLogin()
                    .loginPage("/partner-portal-mflg")
                    .loginProcessingUrl("/.store/ppmflg/publicity/partner-account/login")
                    .failureUrl("/.store/ppmflg/publicity/partner-account/login-failure")
                    .defaultSuccessUrl("/.store/ppmflg/secured/partner-account/login-success")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .and()
                    .logout()
                    .logoutUrl("/.store/ppmflg/publicity/partner-account/logout")
                    .logoutSuccessUrl("/.store/ppmflg/publicity/partner-account/logout-success")
                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/partner-portal-mflg")
            ;
        }
    }

}
