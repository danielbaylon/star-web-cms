package com.enovax.star.cms.api.store.controller.ppslm.secured;


import com.enovax.star.cms.api.store.controller.ppslm.PPSLMBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerWoTReservationService;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.partnershared.service.ppslm.IWingsOfTimeSharedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by houtao on 6/10/16.
 */

@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/BatchJobRunner/")
public class BatchJobRunnerController extends PPSLMBaseStoreApiController {

    @Autowired
    private IWingsOfTimeSharedService wotSrv;

    @Autowired
    private IPartnerWoTReservationService wotReservedSrv;

    @RequestMapping(value="runWoTReleaseReminderJob")
    public ResponseEntity<ApiResult<String>> runWoTReleaseReminderJob(@RequestParam(value = "days", required = false)Integer days){
        try{
            wotSrv.sendWotUnconfirmedBackendReservationReleaseReminder(days);
            return new ResponseEntity<>(new ApiResult(true, "Success", "Run WoT Release Reminder Job Success", ""), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("runWoTReleaseReminderJob", ex, String.class), HttpStatus.OK);
        }
    }
    @RequestMapping(value="runWoTReleaseJob")
    public ResponseEntity<ApiResult<String>> runWoTReleaseJob(@RequestParam(value = "days", required = false)Integer days){
        try{
            wotSrv.releaseWotUnconifrmedBackendReservation(days);
            return new ResponseEntity<>(new ApiResult(true, "Success", "Run WoT Release Job Success", ""), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("runWoTReleaseJob", ex, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value="runWoTCancelReminderJob")
    public ResponseEntity<ApiResult<String>> runWoTCancelReminderJob(@RequestParam(value = "hours", required = false)Integer hours){
        try{
            wotSrv.sendWotUnredeemedReservationCancelReminder(hours);
            return new ResponseEntity<>(new ApiResult(true, "Success", "Run WoT Cancel Reminder Job Success", ""), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("runWoTCancelReminderJob", ex, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value="runRefreshWoTPinCodeJob")
    public ResponseEntity<ApiResult<String>> runRefreshWoTPinCodeJob(){
        try{
            wotReservedSrv.refreshUnredeemedPinCodeService();
            return new ResponseEntity<>(new ApiResult(true, "Success", "Run Refresh WoT Pin Code Job Success", ""), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(handleException("runRefreshWoTPinCodeJob", ex, String.class), HttpStatus.OK);
        }
    }
}
