package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.api.store.repository.partner.IDepositRepository;
import com.enovax.star.cms.api.store.repository.partner.IWOTReservationRepository;
import com.enovax.star.cms.commons.model.partner.ppslm.WOTReservationVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMWingsOfTimeReservationRepository;
import com.enovax.star.cms.partnershared.service.ppslm.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jennylynsze on 6/7/16.
 */
@Service
public class DefaultPartnerReservationService implements IPartnerReservationService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerReservationService.class);

    @Autowired
    IWOTReservationRepository reservationRepo;

    @Autowired
    IDepositRepository depositRepo;

    @Autowired
    PPSLMWingsOfTimeReservationRepository slmReservationRepo;

    @Autowired
    private IPartnerDepositService depositSrv;

    @Autowired
    private IPartnerAccountService paAccSrv;

    @Autowired
    private IWingsOfTimeService wotSrv;

    @Autowired
    private IWingsOfTimeSharedService wotSharedSrv;

    @Autowired
    private PPSLMWingsOfTimeReservationRepository wotRepo;

    @Autowired
    private IWoTPenaltyChargeService wotPenaltyChargeSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private IPartnerWoTEmailService wotEmailSrv;

    @Autowired
    private IPartnerAccountSharedService paAccSharedSrv;

    @Autowired
    private IPartnerSharedService paSharedSrv;

    @Deprecated
    @Override
    public List<WOTReservationVM> getWOTReservations(Integer adminId, String fromDateStr, String toDateStr, String displayCancelled, String showTimes, String orderBy, String orderWith, Integer page, Integer pagesize) {
        return reservationRepo.getReservations(adminId, fromDateStr, toDateStr, displayCancelled, showTimes, orderBy, orderWith, page, pagesize);
    }

    @Deprecated
    @Override
    public int getWOTReservationsSize(Integer adminId, String fromDateStr, String toDateStr, String displayCancelled, String showTimes) {
        return reservationRepo.getReservationsSize(adminId, fromDateStr, toDateStr, displayCancelled, showTimes);
    }
}
