package com.enovax.star.cms.api.store.component;

import com.enovax.star.cms.api.store.service.ppslm.IPartnerAccountService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.partnershared.service.ppslm.IApprovalLogService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerExclProdService;

/**
 * Created by jennylynsze on 7/20/16.
 */
public class StarModuleApiBeans {


    public StarModuleApiBeans() {
    }

    public IProductService getProductService() {
        return AppContextManager.getAppContext().getBean(IProductService.class);
    }

    public IPartnerAccountService getPPSLMPartnerAccountService() {
        return AppContextManager.getAppContext().getBean(IPartnerAccountService.class);
    }


    public com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService getPPMFLGPartnerAccountService() {
        return AppContextManager.getAppContext().getBean("PPMFLGIPartnerAccountService", com.enovax.star.cms.api.store.service.ppmflg.IPartnerAccountService.class);
    }

    public IPartnerExclProdService getPPSLMPartnerExclProdService() {
        return AppContextManager.getAppContext().getBean(IPartnerExclProdService.class);
    }

    public com.enovax.star.cms.partnershared.service.ppmflg.IPartnerExclProdService getPPMFLGPartnerExclProdService() {
        return AppContextManager.getAppContext().getBean("PPMFLGIPartnerExclProdService", com.enovax.star.cms.partnershared.service.ppmflg.IPartnerExclProdService.class);
    }

    public IApprovalLogService getPPSLMApprovalLogService() {
        return AppContextManager.getAppContext().getBean(IApprovalLogService.class);
    }

    public com.enovax.star.cms.partnershared.service.ppmflg.IApprovalLogService getPPMFLGApprovalLogService() {
        return AppContextManager.getAppContext().getBean("PPMFLGIApprovalLogService", com.enovax.star.cms.partnershared.service.ppmflg.IApprovalLogService.class);
    }
}
