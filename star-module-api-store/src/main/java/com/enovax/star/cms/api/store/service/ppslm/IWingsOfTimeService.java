package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTSalesOrder;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.WOTVerifyProdQtyVM;
import com.enovax.star.cms.commons.model.partner.ppslm.WoTVM;

import java.math.BigDecimal;

/**
 * Created by houtao on 23/8/16.
 */
public interface IWingsOfTimeService {

    ApiResult<AxWOTSalesOrder> saveWingsOfTimeReservationOrder(PartnerAccount partnerAccount, ApiResult<AxWOTSalesOrder> result, PPSLMWingsOfTimeReservation reservation);

    ApiResult<String> updateWingsOfTimeReservationOrder(PartnerAccount partnerAccount, ApiResult<?> result, PPSLMWingsOfTimeReservation reservation, String markupCode, BigDecimal markupAmt) throws Exception;

    ApiResult<String> cancelWingsOfTimeReservationOrder(PartnerAccount partnerAccount, PPSLMWingsOfTimeReservation wot, String markupCode, BigDecimal markupAmt) throws Exception;

    ApiResult<String> checkProductItemAvailableQuantity(WOTVerifyProdQtyVM vm);

    ApiResult<WoTVM> updateWingsOfTimeReservationOrderDetails(String axAccountNumber, PPSLMWingsOfTimeReservation wot);

    //Map<String,String> getAllWoTPinCodeReferenceIdMapByAxAccountNumber(String axAccountNumber);

    ApiResult<AxWOTSalesOrder> saveWingsOfTimeReservationOrderFromReservedPool(PartnerAccount partnerAccount, ApiResult<AxWOTSalesOrder> checkoutResult, PPSLMWingsOfTimeReservation wot, PPSLMWingsOfTimeReservation srcWoT);
}
