package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.api.store.model.partner.ppslm.PartnerUser;
import com.enovax.star.cms.api.store.util.password.PasswordUtil;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.*;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxProductPriceMap;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PPSLMTAAccessGroupVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.partnershared.constant.PartnerExtAttrName;
import com.enovax.star.cms.partnershared.repository.ppslm.IPartnerRepository;
import com.enovax.star.cms.partnershared.service.ppslm.*;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class DefaultPartnerAccountService implements IPartnerAccountService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private PPSLMTAMainAccountRepository mainAccSrv;

    @Autowired
    private PPSLMTASubAccountRepository subAccSrv;

    @Autowired
    private PPSLMTAPrevPasswordMainRepository mainAccPrevPwdSrv;

    @Autowired
    private PPSLMTAPrevPasswordSubRepository subAccPrevPwdSrv;

    @Autowired
    private PPSLMTAAccessRightsGroupRepository accRightRepo;

    @Autowired
    private PPSLMPartnerRepository partnerSrv;

    @Autowired
    private IPartnerRepository paTierRepo;

    @Autowired
    private IPartnerAccountSharedService paAccSharedSrv;

    @Autowired
    private IMailService mailSrv;

    @Autowired
    private PPSLMPartnerExtRepository paExtRepo;

    @Autowired
    IPartnerBookingService bookingService;

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private IPartnerAXService paAxSrv;

    @Autowired
    private AxStarService axStarService;

    @Autowired
    private IPartnerEmailService paEmailSrv;

    @Autowired
    private PPSLMTAAccessRightRepository accessRightRepo;

    @Override
    public PPSLMTASubAccount getSubAccountByUserName(String username) {
        if(username != null && username.trim().length() > 0){
            List<PPSLMTASubAccount> list = subAccSrv.findByUsername(username);
            if(list != null && list.size() == 1){
                return list.get(0);
            }
        }
        return null;
    }

    @Transactional(readOnly = true)
    public Integer findMainAccountIdByAccountNumber(String axAccountNumber) throws Exception {
        if(axAccountNumber == null || axAccountNumber.trim().length()  == 0){
            throw new Exception("Invalid AX Account Number ["+axAccountNumber+"] found");
        }
        PPSLMPartner partner = partnerSrv.findByAxAccountNumber(axAccountNumber);
        if(partner == null){
            throw new Exception("No Partner Details for AX Account Number ["+axAccountNumber+"] found");
        }
        return partner.getAdminId();
    }

    @Override
    public void cachePartnerConfigs(PPSLMPartner profile) {
        if(profile != null){
            MgnlContext.setAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_ID, profile.getId(), Context.SESSION_SCOPE);
            MgnlContext.setAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_TIER_ID, profile.getTierId(), Context.SESSION_SCOPE);
            MgnlContext.setAttribute(PartnerPortalConst.CACHE_PARTNER_PRODUCT_PRICE_MAP,
                    getCustomerItemPrice(StoreApiChannels.PARTNER_PORTAL_SLM, profile.getAxAccountNumber()), Context.SESSION_SCOPE);
        }
    }

    @Override
    public AxProductPriceMap getCustomerItemPrice(StoreApiChannels channel, String axAccountNumber) {
        if(StringUtils.isNotEmpty(axAccountNumber)){
            AxStarServiceResult<AxProductPriceMap> getProductPriceMap = axStarService.apiGetItemsPriceByCustomerId(channel, axAccountNumber, new Long[0]);

            if (!getProductPriceMap.isSuccess()) {
                log.error("Error getting product price map."); //TODO Further handling
            }

            AxProductPriceMap productPriceMap = getProductPriceMap.getData();

            if(productPriceMap == null) {
                productPriceMap = new AxProductPriceMap();
            }
            return productPriceMap;
        }

        return new AxProductPriceMap();
    }

    public void logout(String id, String sessionId) {
        if(sessionId != null && sessionId.trim().length() > 0) {
            bookingService.cartClear(StoreApiChannels.PARTNER_PORTAL_SLM, sessionId); //TODO check check
        }
    }

    public String getLoginFailedErrorMessageByLoginName(String name){
        PPSLMTAAccount taAccount = null;
        if(name == null || name.trim().length() == 0){
            return MessageConst.login_err_LoginFailed;
        }
        PPSLMPartner partner = null;
        List<PPSLMTAMainAccount> mainAccList = mainAccSrv.findByUsername(name);
        if(mainAccList != null && mainAccList.size() > 0){
            if(mainAccList.size() > 1){
                return MessageConst.login_err_LoginFailed;
            }
            PPSLMTAMainAccount mainAccount = mainAccList.get(0);
            taAccount = mainAccount;
            partner = mainAccount.getProfile();
        }
        if(taAccount == null){
            List<PPSLMTASubAccount> subAccList = subAccSrv.findByUsername(name);
            if(subAccList != null && subAccList.size() > 0){
                if(subAccList.size() > 1){
                    return MessageConst.login_err_LoginFailed;
                }
                PPSLMTASubAccount subAccount = subAccList.get(0);
                taAccount = subAccount;
                partner = subAccount.getTAMainAccount().getProfile();
            }
        }
        if(taAccount == null || partner == null){
            return MessageConst.login_err_LoginFailed;
        }
        String status = taAccount.getStatus();
        if(status == null || status.trim().length() == 0 || partner.getStatus() == null || partner.getStatus().trim().length() == 0){
            return MessageConst.login_err_LoginFailed;
        }
        if(PartnerStatus.Suspended.code.equalsIgnoreCase(partner.getStatus())){
            return MessageConst.login_err_LoginSuspended;
        }
        if(UserStatus.Deleted.name().equalsIgnoreCase(status)){
            return MessageConst.login_err_LoginFailed;
        }
        if(UserStatus.Inactive.name().equalsIgnoreCase(status)){
            return MessageConst.login_err_AccountInactive;
        }
        if(UserStatus.Locked.name().equalsIgnoreCase(status)){
            return MessageConst.login_err_AccountLocked;
        }
        if(PartnerStatus.Suspended.code.equalsIgnoreCase(status)){
            return MessageConst.login_err_LoginSuspended;
        }
        if(!UserStatus.Active.name().equalsIgnoreCase(status)){
            return MessageConst.login_err_LoginFailed;
        }
        int pwdAttempts = taAccount.getPwdAttempts() != null ? taAccount.getPwdAttempts().intValue() : 0;
        if(pwdAttempts > paramSrv.getPartnerLoginMaximumAttempts()){
            return MessageConst.login_err_ExceedPasswordAttempts;
        }
        return MessageConst.login_err_LoginFailed;
    }

    @Override
    public PartnerUser getUserDetailsByUserName(String username) throws UsernameNotFoundException {
        PartnerUser user = null;
        if(username == null || username.trim().length() == 0){
            throw new UsernameNotFoundException(MessageConst.login_err_LoginFailed);
        }
        List<PPSLMTAMainAccount> mainAccList = mainAccSrv.findByUsername(username);
        if(mainAccList != null && mainAccList.size() > 0){
            if(mainAccList.size() > 1){
                throw new UsernameNotFoundException(MessageConst.login_err_LoginFailed);
            }
            if(mainAccList.get(0) != null){
                user = getActivePartnerAccountDetails(mainAccList.get(0));
            }
        }
        if(user == null){
            List<PPSLMTASubAccount> subAccList = subAccSrv.findByUsername(username);
            if(subAccList != null && subAccList.size() > 0){
                if(subAccList.size() > 1){
                    throw new UsernameNotFoundException(MessageConst.login_err_LoginFailed);
                }
                if(subAccList.get(0) != null) {
                    user = getActivePartnerAccountDetails(subAccList.get(0));
                }
            }
        }
        if(user == null){
            throw new UsernameNotFoundException(MessageConst.login_err_LoginFailed);
        }
        return user;
    }

    private PartnerUser getActivePartnerAccountDetails(PPSLMTAAccount acc) {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        if((UserStatus.Deleted.name().equalsIgnoreCase(acc.getStatus()))){
            accountNonExpired = false;
        }else if(UserStatus.Locked.name().equalsIgnoreCase(acc.getStatus())){
            accountNonLocked = false;
        }else if(UserStatus.Inactive.name().equalsIgnoreCase(acc.getStatus())){
            enabled = false;
        }else if(!UserStatus.Active.name().equalsIgnoreCase(acc.getStatus())){
            enabled = false;
        }
        if(acc.getPwdAttempts() != null && acc.getPwdAttempts().intValue() > paramSrv.getPartnerLoginMaximumAttempts()){
            credentialsNonExpired = false;
        }
        String username = acc.getUsername();
        String password = acc.getPassword();
        Integer id = acc.getId();
        Integer paId = null;
        String accountCode = null;
        PPSLMPartner partner = null;
        boolean isSubAccount = false;
        List<String> accessRights = new ArrayList<>();

        PPSLMTASubAccount subAcc = null;
        PPSLMTAMainAccount mainAcc = null;


        List<String> partnerExtAttrList = new ArrayList<>();
        partnerExtAttrList.add(PartnerExtAttrName.DepositEnabled.code);
        partnerExtAttrList.add(PartnerExtAttrName.WingsOfTimePurchaseEnabled.code);
        partnerExtAttrList.add(PartnerExtAttrName.OfflinePaymentEnabled.code);

        if(acc instanceof PPSLMTASubAccount){
            subAcc = (PPSLMTASubAccount) acc;
            if(!subAcc.getTAMainAccount().getSubAccountEnabled()){
                return new PartnerUser(username, subAcc.getPassword(), false, true, true, true, getUserAccountAuthorities(false), null, null, null, false);
            }
            isSubAccount = true;
            paId = subAcc.getTAMainAccount().getProfile().getId();
            accountCode = subAcc.getTAMainAccount().getAccountCode();
            partner = subAcc.getTAMainAccount().getProfile();

            List<PPSLMTAAccessRight> accessRightList = accessRightRepo.getSubUserAccessRights(username);
            if(accessRightList != null) {
                for(PPSLMTAAccessRight ar: accessRightList) {
                    accessRights.add(ar.getName());
                }
            }

        }else{
            isSubAccount = false;
            mainAcc = (PPSLMTAMainAccount) acc;
            paId = mainAcc.getProfile().getId();
            accountCode = mainAcc.getAccountCode();
            partner = mainAcc.getProfile();
            List<PPSLMTAAccessRight> accessRightList = accessRightRepo.getMainUserAccessRights(username);
            if(accessRightList != null) {
                for(PPSLMTAAccessRight ar: accessRightList) {
                    accessRights.add(ar.getName());
                }
            }
        }

        List<PPSLMPartnerExt> paExtOffline = paExtRepo.findByPartnerIdAndAttrNameInAndStatus(partner.getId(), partnerExtAttrList , "A");
        for(PPSLMPartnerExt paExt: paExtOffline) {
            if(PartnerExtAttrName.DepositEnabled.code.equals(paExt.getAttrName())) {
                if(!"true".equals(paExt.getAttrValue())) {
                    accessRights.remove(TAAccessRight.manageDeposits.toString());
                    accessRights.remove(TAAccessRight.topupDeposits.toString());
                }
            }else if(PartnerExtAttrName.WingsOfTimePurchaseEnabled.code.equals(paExt.getAttrName())) {
                if(!"true".equals(paExt.getAttrValue())) {
                    accessRights.remove(TAAccessRight.reserveWOT.toString());
                    accessRights.remove(TAAccessRight.viewWOTReceipts.toString());
                }
            }else if(PartnerExtAttrName.OfflinePaymentEnabled.code.equals(paExt.getAttrName())) {
                if(!"true".equals(paExt.getAttrValue())) {
                    accessRights.remove(TAAccessRight.offlinePayment.toString());
                }else {
                    accessRights.add(TAAccessRight.offlinePayment.toString());
                }
            }
        }

        //saved to session the accessRights
        MgnlContext.setAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_ACCESS_RIGHTS, accessRights, Context.SESSION_SCOPE);

        if(PartnerStatus.Suspended.code.equals(partner.getStatus())){
            accountNonExpired = false;
        }
        else if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            enabled = false;
        }
        PartnerUser partnerUser = new PartnerUser(
                username, password, enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getUserAccountAuthorities(true),
                id, paId, accountCode, isSubAccount);
        if(enabled && accountNonExpired && credentialsNonExpired && accountNonLocked){
            if(isSubAccount){
                partnerUser.setPartnerAccount(populatePartnerAccount(subAcc));
            }else{
                partnerUser.setPartnerAccount(populatePartnerAccount(mainAcc));
            }
        }
        return partnerUser;
    }

    private Collection<? extends GrantedAuthority> getUserAccountAuthorities(boolean ok) {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
        if(ok){
            GrantedAuthority auth = new SimpleGrantedAuthority(PartnerPortalConst.AUTHORNIZED_USER);
            list.add(auth);
        }
        return list;
    }

    @Override
    public PartnerAccount getPartnerAccountByLoginId(Integer adminId) throws Exception {
        if(adminId == null || adminId.intValue() == 0){
            throw new Exception("INVALID_LOGIN_USER_FOUND");
        }
        PPSLMTAAccount account = mainAccSrv.findById(adminId);
        if(account == null){
            account = subAccSrv.findById(adminId);
        }
        if(account == null){
            throw new Exception("INVALID_LOGIN_USER_FOUND");
        }
        PartnerAccount paAcc = populatePartnerAccount(account);
        return paAcc;
    }

    @Transactional
    public List<PPSLMTAMainAccount> findMainAccountListByUserName(String username) {
        return mainAccSrv.findByUsername(username);
    }

    @Transactional
    public List<PPSLMTASubAccount> getSubAccountListByUserName(String username) {
        return subAccSrv.findByUsername(username);
    }

    @Transactional
    public List<PartnerAccountVM> getPartnerSubaccountListByMainAccountId(Integer mainAccId) {
        List<PartnerAccountVM> list = new ArrayList<PartnerAccountVM>();
        if(mainAccId != null && mainAccId.intValue() > 0){
            PPSLMTAMainAccount mainAcc = mainAccSrv.findById(mainAccId);
            if(mainAcc != null){
                List<PPSLMTASubAccount> subAccounts = subAccSrv.findByMainUser(mainAcc);
                if(subAccounts != null && subAccounts.size() > 0){
                    for(PPSLMTASubAccount sc : subAccounts){
                        PartnerAccountVM vm = new PartnerAccountVM(sc);
                        list.add(vm);
                    }
                }
            }
        }
        return list;
    }

    @Transactional
    public PartnerAccountVM getPartnerSubaccountDetailsByUserId(Integer userId) {
        if(userId == null || userId.intValue() == 0){
            return null;
        }
        PPSLMTASubAccount account = subAccSrv.findById(userId);
        if(account == null){
            return null;
        }
        return new PartnerAccountVM(account);
    }

    @Transactional
    public List<PPSLMTAAccessGroupVM> getPartnerSubaccountAccessGroupList() {
        List<PPSLMTAAccessGroupVM> list = new ArrayList<PPSLMTAAccessGroupVM>();
        List<PPSLMTAAccessRightsGroup> accGrpList = accRightRepo.findByType(PartnerPortalConst.GROUPS_FOR_SUB);
        if(accGrpList != null && accGrpList.size() > 0){
            for(PPSLMTAAccessRightsGroup t : accGrpList){
                if(t != null){
                    PPSLMTAAccessGroupVM m = new PPSLMTAAccessGroupVM();
                    m.setName(t.getName());
                    m.setDescription(t.getDescription());
                    m.setId(t.getId());
                    m.setType(t.getType());
                    list.add(m);
                }
            }
        }
        return list;
    }

    @Transactional(readOnly = true)
    private PartnerAccount populatePartnerAccount(PPSLMTAAccount acc) {
        PartnerAccount pa = new PartnerAccount();
        pa.setUsername(acc.getUsername());
        pa.setName(acc.getName());
        pa.setId(acc.getId());
        pa.setEmail(acc.getEmail());
        pa.setPwdForceChange(acc.getPwdForceChange());
        if(acc instanceof PPSLMTAMainAccount){
            PPSLMTAMainAccount mainAcc = (PPSLMTAMainAccount)acc;
            pa.setAccountCode(mainAcc.getAccountCode());
            pa.setMainAccountId(mainAcc.getId());
            pa.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
            pa.setSubAccount(false);
            pa.setMainAccount(mainAcc);
        }else{
            PPSLMTASubAccount subAcc = (PPSLMTASubAccount) acc;
            PPSLMTAMainAccount mainAcc = subAcc.getTAMainAccount();
            pa.setMainAccountId(mainAcc.getId());
            pa.setAccountCode(mainAcc.getAccountCode());
            pa.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
            pa.setSubAccount(true);
            pa.setMainAccount(mainAcc);
        }
        PartnerVM partner = new PartnerVM(partnerSrv.findByAccountCode(pa.getAccountCode()));
        pa.setPartner(partner);
        return pa;
    }

    @Override
    public PPSLMTAMainAccount createTAMainAccount(PartnerVM paVm) {
        PPSLMTAMainAccount taMaAcc = new PPSLMTAMainAccount();
        taMaAcc.setAccountCode(paVm.getAccountCode().toUpperCase());
        taMaAcc.setUsername(paVm.getAccountCode().toUpperCase() + PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        taMaAcc.setEmail(paVm.getEmail().toUpperCase());
        taMaAcc.setName(paVm.getAccountCode().toUpperCase()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        taMaAcc.setSubAccountEnabled(false);
        taMaAcc.setPassword("NA");
        taMaAcc.setSalt("NA");
        taMaAcc.setPwdLastModDt(new Date());
        taMaAcc.setStatus(UserStatus.Inactive.toString());
        taMaAcc.setPwdAttempts(0);
        taMaAcc.setCreatedBy(PartnerPortalConst.SYSTEM.toString());
        taMaAcc.setCreatedDate(new Date());
        taMaAcc.setModifiedBy(PartnerPortalConst.SYSTEM.toString());
        taMaAcc.setModifiedDate(new Date());
        taMaAcc.setPwdForceChange(true);
        mainAccSrv.save(taMaAcc);
        return taMaAcc;
    }

    @Override
    public PPSLMTAMainAccount updateTAMainAccount(PPSLMPartner partner, PartnerVM paVm) {
        PPSLMTAMainAccount taMaAcc = partner.getMainAccount();
        taMaAcc.setAccountCode(paVm.getAccountCode());
        taMaAcc.setUsername(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        taMaAcc.setEmail(paVm.getEmail().toUpperCase());
        taMaAcc.setName(paVm.getAccountCode()+ PartnerPortalConst.TA_MAIN_ACC_POSTFIX);
        taMaAcc.setSubAccountEnabled(false);
        taMaAcc.setPassword("NA");
        taMaAcc.setSalt("NA");
        taMaAcc.setPwdLastModDt(new Date());
        taMaAcc.setStatus(UserStatus.Inactive.toString());
        taMaAcc.setPwdAttempts(0);
        taMaAcc.setCreatedBy(PartnerPortalConst.SYSTEM.toString());
        taMaAcc.setCreatedDate(new Date());
        taMaAcc.setModifiedBy(PartnerPortalConst.SYSTEM.toString());
        taMaAcc.setModifiedDate(new Date());
        taMaAcc.setPwdForceChange(true);
        mainAccSrv.save(taMaAcc);
        return taMaAcc;
    }

    @Override
    public String resetPartnerPassword(String un, String email) throws Exception {
        if(un == null || un.trim().length() == 0 || email == null || email.trim().length() == 0){
            return "EMPTY_USERNAME_AND_PWD";
        }
        un = un.trim();
        email = email.trim();
        if (un.toUpperCase().endsWith(PartnerPortalConst.TA_MAIN_ACC_POSTFIX)) {
            if(resetPartnerMainAccountPassword(un, email)){
                return null;
            }
        }else{
            if(resetPartnerSubAccountPassword(un, email)){
                return null;
            }
        }
        return "RESET_USERNAME_AND_PWD_FAILED";
    }

    private boolean resetPartnerMainAccountPassword(String un, String email) throws Exception {
        List<PPSLMTAMainAccount> users = mainAccSrv.findByUsername(un);
        if(users == null || users.size() != 1){
            return false;
        }
        return resetPartnerAccountPassword((PPSLMTAAccount)users.get(0), un, email, true);
    }
    private boolean resetPartnerSubAccountPassword(String un, String email) throws Exception {
        List<PPSLMTASubAccount> users = subAccSrv.findByUsername(un);
        if(users == null || users.size() != 1){
            return false;
        }
        return resetPartnerAccountPassword((PPSLMTAAccount)users.get(0), un, email, false);
    }

    @Transactional
    private boolean resetPartnerAccountPassword(PPSLMTAAccount acc, String un, String email, boolean isMain) throws Exception {
        if(UserStatus.Deleted.name().equalsIgnoreCase(acc.getStatus())){
            return false;
        }
        if(UserStatus.Inactive.name().equalsIgnoreCase(acc.getStatus())){
            return false;
        }
        if(acc.getEmail() == null || acc.getEmail().trim().length() == 0){
            return false;
        }
        if(!acc.getEmail().equalsIgnoreCase(email)){
            return false;
        }

        PPSLMPartner partner = null;
        if(isMain){
            partner = ((PPSLMTAMainAccount)acc).getProfile();
        }else{
            PPSLMTAMainAccount mainAcc = (((PPSLMTASubAccount)acc).getTAMainAccount());
            if(mainAcc == null){
                return false;
            }
            if(UserStatus.Deleted.name().equalsIgnoreCase(mainAcc.getStatus())){
                return false;
            }
            if(UserStatus.Inactive.name().equalsIgnoreCase(mainAcc.getStatus())){
                return false;
            }
            partner = mainAcc.getProfile();
        }
        if((partner == null)){
            return false;
        }
        if(!PartnerStatus.Active.code.equalsIgnoreCase(partner.getStatus())){
            return false;
        }
        if(!isAccountHavingLoginPermission(acc)){
            return false;
        }
        Date now = new Date(System.currentTimeMillis());
        if(isMain){
            savePartnerMainAccountPreviousPassword(now, (PPSLMTAMainAccount)acc);
        }else{
            savePartnerSubAccountPreviousPassword(now, (PPSLMTASubAccount)acc);
        }
        String userName = acc.getName();
        String loginName = acc.getUsername();
        String newPwd = PasswordUtil.generatePswd();
        acc.setPwdForceChange(true);
        acc.setStatus(UserStatus.Active.name());
        acc.setPwdAttempts(0);
        acc.setPassword(PasswordUtil.encode(newPwd));
        acc.setModifiedDate(now);
        acc.setModifiedBy(un);
        if(isMain){
            mainAccSrv.save((PPSLMTAMainAccount)acc);
        }else{
            subAccSrv.save((PPSLMTASubAccount)acc);
        }
        paEmailSrv.sendForgotPasswordEmail(userName, loginName, newPwd, email);
        return true;
    }

    private boolean isAccountHavingLoginPermission(PPSLMTAAccount acc) {
        //TODO: MUST CHECK USER ROELS...
        return true;
    }

    private void savePartnerSubAccountPreviousPassword(Date now, PPSLMTASubAccount acc) {
        PPSLMTAPrevPasswordSub subAccPrePwd = new PPSLMTAPrevPasswordSub();
        subAccPrePwd.setUser(acc);
        subAccPrePwd.setCreatedDate(now);
        subAccPrePwd.setPassword(acc.getPassword());
        subAccPrePwd.setSalt("NA");
        subAccPrevPwdSrv.save(subAccPrePwd);
    }

    private void savePartnerMainAccountPreviousPassword(Date now, PPSLMTAMainAccount acc) {
        PPSLMTAPrevPasswordMain mainAccPrePwd = new PPSLMTAPrevPasswordMain();
        mainAccPrePwd.setUser(acc);
        mainAccPrePwd.setCreatedDate(now);
        mainAccPrePwd.setPassword(acc.getPassword());
        mainAccPrePwd.setSalt("NA");
        mainAccPrevPwdSrv.save(mainAccPrePwd);
    }

    @Override
    public PPSLMPartner resetFailAttempts(String username) {
        if(username == null || username.trim().length() == 0){
            return null;
        }
        PPSLMTAMainAccount mainAccount = null;
        PPSLMTASubAccount  subAccount  = null;
        List<PPSLMTAMainAccount> mainAccList = mainAccSrv.findByUsername(username);
        if(mainAccList != null && mainAccList.size() > 0){
            if(mainAccList.size() > 1){
                return null;
            }
            mainAccount = mainAccList.get(0);
        }
        if(mainAccount == null){
            List<PPSLMTASubAccount> subAccList = subAccSrv.findByUsername(username);
            if(subAccList != null && subAccList.size() > 0){
                if(subAccList.size() > 1){
                    return null;
                }
                subAccount = subAccList.get(0);
            }
        }
        if(mainAccount != null){
            mainAccount.setPwdAttempts(0);
            mainAccSrv.save(mainAccount);
            return mainAccount.getProfile();
        }else if(subAccount != null){
            subAccount.setPwdAttempts(0);
            subAccSrv.save(subAccount);
            return subAccount.getTAMainAccount().getProfile();
        }
        return null;
    }

    @Override
    public void updateFailAttempts(String username, String errorCodex) {
        if(username == null || username.trim().length() == 0){
            return;
        }
        PPSLMTAMainAccount mainAccount = null;
        PPSLMTASubAccount  subAccount  = null;
        List<PPSLMTAMainAccount> mainAccList = mainAccSrv.findByUsername(username);
        if(mainAccList != null && mainAccList.size() > 0){
            if(mainAccList.size() > 1){
                return;
            }
            mainAccount = mainAccList.get(0);
        }
        if(mainAccount == null){
            List<PPSLMTASubAccount> subAccList = subAccSrv.findByUsername(username);
            if(subAccList != null && subAccList.size() > 0){
                if(subAccList.size() > 1){
                    return;
                }
                subAccount = subAccList.get(0);
            }
        }
        int maximumFailedAttemps = paramSrv.getPartnerLoginMaximumAttempts();
        if(mainAccount != null){
            if(!UserStatus.Active.name().equalsIgnoreCase(mainAccount.getStatus())){
                return;
            }
            mainAccount.setPwdAttempts(mainAccount.getPwdAttempts() + 1);
            mainAccount.setStatus(mainAccount.getPwdAttempts().intValue() > maximumFailedAttemps ? UserStatus.Locked.name() : mainAccount.getStatus());
            mainAccSrv.save(mainAccount);
        }else if(subAccount != null){
            if(!UserStatus.Active.name().equalsIgnoreCase(subAccount.getStatus())){
                return;
            }
            subAccount.setPwdAttempts(subAccount.getPwdAttempts() + 1);
            subAccount.setStatus(subAccount.getPwdAttempts().intValue() > maximumFailedAttemps ? UserStatus.Locked.name() : subAccount.getStatus());
            subAccSrv.save(subAccount);
        }
    }

    @Override
    public void renewPassword(String username, String pw, String newPw, String newPw2) throws Exception {
        if(username == null || username.trim().length() == 0 || pw == null || pw.trim().length() == 0 || newPw == null || newPw.trim().length() == 0 || newPw2 == null || newPw2.trim().length() == 0){
            throw new Exception(MessageConst.renew_pwd_AllFieldsRequired);
        }
        if(!newPw.equals(newPw2)){
            throw new Exception(MessageConst.renew_pwd_RenewPassNotMatch);
        }
        if(newPw.startsWith(" ")){
           throw new Exception(MessageConst.renew_pwd_PasswordLeadingTrailing);
        }
        if(pw.equalsIgnoreCase(newPw)){
            throw new Exception(MessageConst.renew_pwd_PasswordPolicyNotMet);
        }
        if(!PasswordUtil.isValidPswd(newPw)){
            throw new Exception(MessageConst.renew_pwd_PasswordPolicyNotMet);
        }
        PartnerUser user = null;
        try{
            user = getUserDetailsByUserName(username);
        }catch (Exception ex){
            throw new Exception(getLoginFailedErrorMessageByLoginName(username));
        }
        if(user == null){
            throw new Exception(getLoginFailedErrorMessageByLoginName(username));
        }
        if(!PasswordUtil.isMatch(pw, user.getPassword())){
            throw new Exception(MessageConst.renew_pwd_Last5Passwords);
        }
        int numHistPwds = paramSrv.getUserPasswordValidationMaximumPastPasswords();
        List<PPSLMPrevPassword> list = null;
        if(user.isSubAccount()){
            list = subAccPrevPwdSrv.findHistByAccountId(user.getId());
        }else{
            list = mainAccPrevPwdSrv.findHistByAccountId(user.getId());
        }
        if(list != null && list.size() > 0){
            int i = 0 ;
            for(PPSLMPrevPassword p : list){
                if( (i++) > numHistPwds ){
                    break;
                }
                if(PasswordUtil.isMatch(newPw, p.getPassword())){
                    throw new Exception(MessageConst.renew_pwd_Last5Passwords);
                }
            }
        }
        if(user.isSubAccount()){
            renewTASubAccountPassword(user.getId(), newPw);
        }else{
            renewTAMainAccountPassword(user.getId(), newPw);
        }
    }

    private void renewTAMainAccountPassword(Integer id, String newPw) {
        Date now = new Date(System.currentTimeMillis());
        PPSLMTAMainAccount mainAcc = mainAccSrv.findById(id);
        savePartnerMainAccountPreviousPassword(now, mainAcc);
        mainAcc.setPassword(PasswordUtil.encode(newPw));
        mainAcc.setPwdAttempts(0);
        mainAcc.setPwdForceChange(false);
        mainAcc.setStatus(UserStatus.Active.name());
        mainAcc.setModifiedDate(now);
        mainAccSrv.save(mainAcc);
    }

    private void renewTASubAccountPassword(Integer id, String newPw) {
        Date now = new Date(System.currentTimeMillis());
        PPSLMTASubAccount subAcc = subAccSrv.findById(id);
        savePartnerSubAccountPreviousPassword(now, subAcc);
        subAcc.setPassword(PasswordUtil.encode(newPw));
        subAcc.setPwdAttempts(0);
        subAcc.setPwdForceChange(false);
        subAcc.setStatus(UserStatus.Active.name());
        subAcc.setModifiedDate(now);
        subAccSrv.save(subAcc);
    }

    public void syncPartnerProfileWithAX(Integer paId) {
        try{
            paAxSrv.syncPartnerProfile(paId);
        }catch (Exception ex1){
            log.error(String.format("refreshPartnerProfile exception for  partner [%s]",paId), ex1);
        }catch (Throwable ex2){
            log.error(String.format("refreshPartnerProfile system error for partner [%s]",paId), ex2);
        }
    }

    @Transactional(readOnly = true)
    public boolean isPartnerOfflinePaymentEnabled(PartnerAccount loginUserAccount) {
        if(loginUserAccount == null || loginUserAccount.getPartner() == null || loginUserAccount.getPartner().getId() == null){
            return false;
        }
        List<PPSLMPartnerExt> list = paExtRepo.findOfflinePaymentConfigByPartnerId(loginUserAccount.getPartner().getId());
        if(list == null || list.size() == 0){
            return false;
        }

        PPSLMPartnerExt ext = list.get(0);
        if(ext == null || ext.getAttrValue() == null || ext.getAttrValue().trim().length() == 0){
            return false;
        }
        return "true".equalsIgnoreCase(ext.getAttrValue().trim());
    }

    @Transactional
    public ApiResult<String> saveTASubAccount(PPSLMPartner partner, Boolean isNew, Integer userId, String username, String name, String email, String title, String status, String access, String loginUserId, Integer loginId, boolean isSubAccount) throws BizValidationException {

        if(!paAccSharedSrv.hasSubAccountAdminPermission(partner.getMainAccount().getId(), loginId, isSubAccount)){
            throw new BizValidationException("Invalid Request, You don't have permission to administrate sub-accounts.");
        }

        if(partner == null || partner.getMainAccount() == null || partner.getMainAccount().getId() == null){
            throw new BizValidationException("Invalid request");
        }
        if(isNew){
            if(userId != null && userId.intValue() > 0){
                throw new BizValidationException("Invalid request");
            }
        }else{
            if(userId == null || userId.intValue() == 0){
                throw new BizValidationException("Invalid request");
            }
        }
        if(username == null || username.trim().length() == 0){
            throw new BizValidationException("User name is mandatory");
        }
        if(name == null || name.trim().length() == 0){
            throw new BizValidationException("Name is mandatory");
        }
        if(email == null || email.trim().length() == 0){
            throw new BizValidationException("Email is mandatory");
        }
        if(title == null || title.trim().length() == 0 ){
            throw new BizValidationException("Designation is mandatory");
        }
        if(status == null || status.trim().length() == 0){
            throw new BizValidationException("Status is mandatory");
        }
        String pwd = PasswordUtil.generatePswd();
        if(pwd == null || pwd.trim().length() == 0){
            throw new BizValidationException("Invalid Request");
        }
        String encodedPwd = PasswordUtil.encode(pwd);
        if(encodedPwd == null || encodedPwd.trim().length() == 0){
            throw new BizValidationException("Invalid Request");
        }
        try{
            return paAccSharedSrv.saveSubuser(partner.getId(), isNew, userId, username, name, email, title, status, access, loginUserId, pwd, encodedPwd);
        }
        catch (Exception ex){
            throw new BizValidationException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    public ApiResult<String> savePartnerSubAccountRightsChanges(PPSLMPartner partner, Integer mainAccId, String loginUserName, Integer loginId, boolean isSubAccount, List<PartnerAccountVM> accounts) throws BizValidationException {
        if(!paAccSharedSrv.hasSubAccountAdminPermission(mainAccId, loginId, isSubAccount)){
            throw new BizValidationException("Invalid Request, You don't have permission to administrate sub-accounts.");
        }
        try{
            return paAccSharedSrv.savePartnerSubAccountRightsChanges(mainAccId,loginUserName, accounts);
        }
        catch (Exception ex){
            throw new BizValidationException(ex.getMessage());
        }
    }

    @Override
    public boolean isCurrentUserAccountInvalid(Integer id, boolean subAccount, Integer paId) {

        if(id == null || paId == null || id.intValue() == 0 || paId.intValue() == 0){
            return true;
        }
        if(subAccount){
            int c = subAccSrv.countByIdAndStatus(id, com.enovax.star.cms.commons.constant.ppmflg.UserStatus.Active.name());
            if(c == 0){
                return true;
            }
        }else{
            int c =  mainAccSrv.countByIdAndStatus(id, com.enovax.star.cms.commons.constant.ppmflg.UserStatus.Active.name());
            if(c == 0){
                return true;
            }
        }
        int c = partnerSrv.countByIdAndStatus(paId, com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus.Active.code);
        if(c == 0){
            return true;
        }
        return false;
    }

    @Override
    public List<PartnerVM> getActivePartners() {
        List<PartnerVM> paVms = new ArrayList<PartnerVM>();
        try {

            List<PPSLMPartner> partners= partnerSrv.findByStatus(PartnerStatus.Active.toString());

            for(PPSLMPartner pa:partners) {
                PartnerVM partnerVM = new PartnerVM();
                partnerVM.setId(pa.getId());
                partnerVM.setOrgName(pa.getOrgName());
                paVms.add(partnerVM);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllPartners] !!!");
        }

        return paVms;
    }
}
