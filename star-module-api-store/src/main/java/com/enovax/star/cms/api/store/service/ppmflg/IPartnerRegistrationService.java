package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerRegistrationVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import info.magnolia.cms.beans.runtime.Document;

public interface IPartnerRegistrationService {

    public PartnerRegistrationVM getPartnerRegistrationParam();

    String registerPartner(PartnerVM partnerVM, Document doc1, Document doc2) throws Exception;

    boolean isAnExistingPartner(String accountCode, Integer paId);

    PartnerVM getPartnerDetailsByPartnerId(String accCodeId);

    PPMFLGPartner getPartnerByAxAccountNumber(String custAccNum);

    String populatePartnerRegistrationFormResubmitRemarks(PartnerVM paVM);
}
