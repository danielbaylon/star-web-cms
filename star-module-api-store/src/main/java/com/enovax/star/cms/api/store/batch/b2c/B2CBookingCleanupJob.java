package com.enovax.star.cms.api.store.batch.b2c;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.b2cshared.service.IB2CTransactionService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionExtension;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGStoreTransactionExtensionRepository;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGStoreTransactionRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionExtensionRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionRepository;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class B2CBookingCleanupJob {

    private Logger log = LoggerFactory.getLogger(B2CBookingCleanupJob.class);

    @Autowired
    private IB2CTransactionService transService;

    @Autowired
    private B2CSLMStoreTransactionRepository slmTransRepo;
    @Autowired
    private B2CSLMStoreTransactionExtensionRepository slmExtRepo;
    @Autowired
    private B2CMFLGStoreTransactionRepository mflgTransRepo;
    @Autowired
    private B2CMFLGStoreTransactionExtensionRepository mflgExtRepo;
    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private TmServiceProvider tmServiceProvider;

    private boolean isAuthorInstance = false;

    @PostConstruct
    public void init() {
        try {

            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        } catch (Exception e) {
            log.error("Issue initialising cleanup job... non-critical.");
        }
    }

    //TODO Implement some sort of flag/mechanism to dynamically stop/start performing of jobs
    //Idea: Store flags in JCR, have a batch job that observes these values and updated variable flags here.

//    @Scheduled(initialDelay = 600000, fixedDelay = 3600000)
    public void performB2CSLMGeneralCleanup() {
        //TODO
    }

//    @Scheduled(initialDelay = 1000, fixedDelay = 180000)
    public void performB2CSLMTransactionQuery() {
        try {

//            final TelemoneyParamPackage tmParams = tmParamRepository.getTmParams(StoreApiChannels.B2C_SLM);
            final Integer lowerBoundMillis = 150;//tmParams.getTransTimeoutLowerBoundMillis();
            final Integer upperBoundMillis = 20;//tmParams.getTransTimeoutUpperBoundMillis();

            long lowerBound = System.currentTimeMillis() - lowerBoundMillis * 60000;
            long upperBound = System.currentTimeMillis() - upperBoundMillis * 60000;
            Date lowerBoundDate = new Date(lowerBound);
            Date upperBoundDate = new Date(upperBound);
            System.out.println(upperBoundDate.toString());

            final TmMerchantSignatureConfig signatureConfig = new TmMerchantSignatureConfig();
            signatureConfig.setSignatureEnabled(false);

            final List<B2CSLMStoreTransaction> failedQueries = new ArrayList<>();

            //TODO Double check if I am passing in the right parameters actually.
            final List<B2CSLMStoreTransaction> txns = slmTransRepo.retrieveTransactionsWithPendingPayment(upperBoundDate);
            for (B2CSLMStoreTransaction txn : txns) {
                //final B2CSLMStoreTransactionExtension extData = slmExtRepo.findByReceiptNumber(txn.getReceiptNum());
                //W504785557
                System.out.println("Found txn: " + txn.getId());

                log.info("Setting transaction stage to Query for " + txn.getReceiptNum());
                txn.setTmStatus(EnovaxTmSystemStatus.TmQuerySent.toString());
                txn.setModifiedDate(new Date());
                transService.saveB2CSLMStoreTransaction(txn);

                log.info("Sending status update request to Telemoney for " + txn.getReceiptNum());
                TmServiceResult queryResult = tmServiceProvider.sendQueryWithRetry(txn.getTmMerchantId(), txn.getReceiptNum(), null);
                if (queryResult != null && queryResult.isSuccess()) {
                    log.info("Successfully sent query to Telemoney.");
                } else {
                    log.info("Unable to send query to Telemoney. Performing cleanup tasks.");

                    log.info("Sending void to Telemoney");
                    TmServiceResult voidResult = tmServiceProvider.sendVoidWithRetry(txn.getTmMerchantId(), TmCurrency.SGD, txn.getTotalAmount(), txn.getReceiptNum(), signatureConfig);
                    if (voidResult == null || !voidResult.isSuccess()) {
                        log.info("Unable to send void to Telemoney.");
                    } else {
                        log.info("Send void successful. Logging result.");
                        TelemoneyResponse voidResponse = voidResult.getTmResponse();
                        transService.saveTelemoneyLog(StoreApiChannels.B2C_SLM, voidResponse, true);
                    }

                    log.info("Releasing ticket.");
                    boolean released = false;
                    try {
                        // transService.processTransactionReleaseReservation(trans.getId());
                        released = true;
                    } catch (Exception e) {
                        log.error("Unable to release ticket due to an exception.", e);
                    }

                    if (released) {
                        log.info("Successfully released ticket.");
                    } else {
                        log.info("Unsuccessfully released ticket.");
                    }

                    if (!released || queryResult == null) {
                        failedQueries.add(txn);
                    }
                }
            }

            if (!failedQueries.isEmpty()) {
                try {
//                    final MailProperties mailProps = mailService.getAdminMailProps("failed_query_" + new Date(), FAILED_SUBJ,
//                        constructFailedEmail(failedQueries), false);
//                    mailService.sendEmail(mailProps, new MailCallback() {
//                        @Override
//                        public void complete(String trackingId, boolean success) {
//                            log.info("Mail status update for {}. Status: {}", trackingId, success ? "SUCCESS" : "FAILED");
//                        }
//                    });
                } catch (Exception e) {
                    log.error("Email sending failed for TM Query Task.", e);
                }
            }

        } catch (Exception e) {
            log.error("General error encountered peforming transaction query job. " + e.getMessage(), e);
        }
    }

//    @Scheduled(initialDelay = 31000, fixedDelay = 600000)
    public void performB2CSLMTransactionFinalTimeoutCleanup() {
        //TODO
    }
}
