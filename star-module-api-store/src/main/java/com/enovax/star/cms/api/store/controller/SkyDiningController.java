package com.enovax.star.cms.api.store.controller;

import com.enovax.star.cms.api.store.model.SkyDiningActionParam;
import com.enovax.star.cms.api.store.service.b2c.skydining.ISkyDiningService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.skydining.*;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.model.SessionRecord;
import com.enovax.star.cms.skydining.model.api.SkyDiningCheckPromoResult;
import com.enovax.star.cms.skydining.model.result.SkyDiningCalendarResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 16/6/16.
 */
@Controller
@RequestMapping("/sky-dining/")
public class SkyDiningController extends BaseStoreApiController {

    @Autowired
    private HttpSession session;
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ISkyDiningService skyDiningService;

    @RequestMapping(value = "add-to-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningFunCartDisplay>> apiAddToCart(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
        @RequestBody SkyDiningFunCart cart) {
        log.info("Entered apiAddToCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<SkyDiningFunCartDisplay> result = skyDiningService.cartAdd(channel, cart, sessionId);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-checkout-display", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningCheckoutDisplay>> apiGetCheckoutDisplay(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCheckoutDisplay...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final SkyDiningCheckoutDisplay checkoutDisplay = skyDiningService.constructCheckoutDisplayFromCart(channel, session);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", checkoutDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCheckoutDisplay] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningCheckoutDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "apply-promo-code", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiApplyPromoCode(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
        @RequestBody String promoCode) {
        log.info("Entered apiApplyPromoCode...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<String> result = skyDiningService.cartApplyPromoCode(channel, sessionId, promoCode);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiApplyPromoCode] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-item-from-cart/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningFunCartDisplay>> apiRemoveItemFromCart(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
        @PathVariable("id") String id) {
        log.info("Entered apiRemoveItemFromCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<SkyDiningFunCartDisplay> result = skyDiningService.cartRemoveItem(channel, sessionId, id);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveItemFromCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningFunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "clear-cart", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiClearCart(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiClearCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            skyDiningService.cartClear(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiClearCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningStoreTransaction>> apiCheckout(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
        @RequestBody CustomerDetails deets) {
        log.info("Entered apiCheckout...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<SkyDiningStoreTransaction> result = skyDiningService.doCheckout(channel, session, deets);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningStoreTransaction.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout-confirm", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<CheckoutConfirmResult>> apiCheckoutConfirm(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckoutConfirm...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final Trackd trackd = trackr.buildTrackd(session.getId(), request);

            final ApiResult<CheckoutConfirmResult> result = skyDiningService.checkoutConfirm(channel, session, trackd);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckoutConfirm] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CheckoutConfirmResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout-complete-sale", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningStoreTransaction>> apiCompleteSale(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCompleteSale...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<SkyDiningStoreTransaction> result = skyDiningService.completeSale(channel, session);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCompleteSale] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningStoreTransaction.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout-cancel", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCheckoutCancel(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckoutCancel...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<String> result = skyDiningService.cancelCheckout(channel, session);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckoutCancel] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-receipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningReceiptDisplay>> apiGetReceipt(
        @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetReceipt...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final SkyDiningReceiptDisplay receipt = skyDiningService.getReceiptForDisplayBySession(channel, session);

            if (receipt == null) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "No transaction found", null), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningReceiptDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-available-sessions", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<List<SessionRecord>>> getAvailableSessions(@RequestBody SkyDiningActionParam param) {
        log.info("Entered getAvailableSessions...");

        try {
            if (StringUtils.isNotBlank(param.getDateOfVisitText())) {
                Date dateOfVisit = NvxDateUtils.parseDate(param.getDateOfVisitText(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

                List<SessionRecord> sessions = skyDiningService.getAvailableSessions(param.getPackageId(), dateOfVisit);
                return new ResponseEntity<>(new ApiResult<>(true, "", "", sessions), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ApiResult<>(false, "", "", null), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getAvailableSessions] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, SessionRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-calendar-params", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningCalendarResult>> getCalendarParams(@RequestBody SkyDiningActionParam param) {
        log.info("Entered getCalendarParams...");

        try {
            ApiResult<SkyDiningCalendarResult> result = skyDiningService.getCalendarParams(param.getPackageId());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getCalendarParams] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningCalendarResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "check-sky-dining-jc", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningCheckPromoResult>> checkSkyDiningJc(@RequestBody SkyDiningActionParam param) {
        log.info("Entered checkSkyDiningJc...");

        try {
            SkyDiningCheckPromoResult result = skyDiningService.checkJewelCard(param.getJcNumber(), param.getJcExpiry(), param.getProductId(), true);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [checkSkyDiningJc] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningCheckPromoResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "check-sky-dining-pc", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SkyDiningCheckPromoResult>> checkSkyDiningPc(@RequestBody SkyDiningActionParam param) {
        log.info("Entered checkSkyDiningPc...");

        try {
            SkyDiningCheckPromoResult result = skyDiningService.checkPromoCode(param.getPromoCode(), param.getProductId(), param.getDateOfVisitText(), true);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [checkSkyDiningPc] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SkyDiningCheckPromoResult.class, ""), HttpStatus.OK);
        }
    }

}
