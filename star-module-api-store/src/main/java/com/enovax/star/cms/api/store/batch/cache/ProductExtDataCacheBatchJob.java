package com.enovax.star.cms.api.store.batch.cache;


import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.product.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by jennylynsze on 12/15/16.
 */

@Service
public class ProductExtDataCacheBatchJob {
    private Logger log = LoggerFactory.getLogger(ProductExtDataCacheBatchJob.class);

    @Autowired
    IProductService productService;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    //EVERY 5 mins
    @Scheduled(cron = "${cache.online.product.search.ext.job}")
    public void retrieveExtDataCache() {

        realTimeTaskExecutor.submit(new Runnable() {
            @Override
            public void run() {
                StoreApiChannels onlineChannels[] = new StoreApiChannels[] { StoreApiChannels.B2C_SLM, StoreApiChannels.B2C_MFLG, StoreApiChannels.PARTNER_PORTAL_SLM, StoreApiChannels.PARTNER_PORTAL_MFLG};
                for(StoreApiChannels channel:onlineChannels) {
                    try {
                        productService.updateProductExtDataCache(channel);
                    } catch (AxChannelException e) {
                        log.error("Error encountered on batch job retrieve of ax products...", e);
                    }
                }
            }
        });


    }
}
