package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.DepositVM;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 6/24/16.
 */
@Repository
public class JcrDepositRepository implements IDepositRepository {

    @Override
    public BigDecimal getBalance(Integer adminId) {
        try {
            Node partnerAccountNode =  JcrRepository.getParentNode(JcrWorkspace.PartnerAccounts.getWorkspaceName(), "/" + adminId);
            BigDecimal currentBalance = partnerAccountNode.getProperty("depositBalance").getDecimal();
            return currentBalance;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return BigDecimal.ZERO;
    }

    @Override
    public void updateBalance(Integer adminId, BigDecimal depositChanges) {
        try {
            Node partnerAccountNode =  JcrRepository.getParentNode(JcrWorkspace.PartnerAccounts.getWorkspaceName(), "/" + adminId);
            BigDecimal currentBalance = partnerAccountNode.getProperty("depositBalance").getDecimal();
            currentBalance = currentBalance.add(depositChanges);
            partnerAccountNode.setProperty("depositBalance", currentBalance);
            JcrRepository.updateNode(JcrWorkspace.PartnerAccounts.getWorkspaceName(), partnerAccountNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(DepositVM depositVM) {
        try {

            Node depositNode;
            if(JcrRepository.nodeExists(JcrWorkspace.PartnerDeposits.getWorkspaceName(), "/" + depositVM.getId())) {
                depositNode = JcrRepository.getParentNode(JcrWorkspace.PartnerDeposits.getWorkspaceName(), "/" + depositVM.getId());
            }else {
                depositNode = JcrRepository.createNode(JcrWorkspace.PartnerDeposits.getWorkspaceName(), "/", depositVM.getId() + "", "mgnl:content");
            }

            depositNode.setProperty("data", JsonUtil.jsonify(depositVM));
            Calendar transDate = Calendar.getInstance();
            try{
                if(depositVM.getTransactionDate() != null && depositVM.getTransactionDate().trim().length() > 0){
                    transDate.setTime(NvxDateUtils.parseDate(depositVM.getTransactionDate(),NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            depositNode.setProperty("transactionDate", transDate);
            depositNode.setProperty("mainAccountId", depositVM.getMainAccountId() + "");
            JcrRepository.updateNode(JcrWorkspace.PartnerDeposits.getWorkspaceName(), depositNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node reservationSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/deposits");
            int currId = Integer.parseInt(reservationSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            reservationSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), reservationSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    @Override
    public List<DepositVM> getDepositHistory(Integer adminId, String fromDateStr, String toDateStr, String orderBy, String orderWith, Integer page, Integer pagesize) {

        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "']";
            List<DepositVM> depositList = new ArrayList<>();
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.PartnerDeposits.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            while(nodeIterator.hasNext()) {
                Node depositNode = nodeIterator.next();
                DepositVM deposit = JsonUtil.fromJson(depositNode.getProperty("data").getString(), DepositVM.class);
                depositList.add(deposit);
            }

            //only get those with status for main transaction as "Available"


            List<DepositVM> pagedDepositList = new ArrayList<>();
            //filter pag page also
            if(page > 0 && pagesize > 0) {
                for(int i = (page - 1) * pagesize; pagedDepositList.size() < pagesize && i < depositList.size(); i++) {
                    pagedDepositList.add(depositList.get(i));
                }
            }else {
                return depositList;
            }

            return pagedDepositList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int getDepositHistorySize(Integer adminId, String fromDateStr, String toDateStr) {
        //TODO ADD MORE query here
        try {
            String query = "[@mainAccountId = '" + adminId + "']";
            Iterable<Node> nodeIterable = JcrRepository.query(JcrWorkspace.PartnerDeposits.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)" + query);
            Iterator<Node> nodeIterator = nodeIterable.iterator();

            int cnt = 0;
            while(nodeIterator.hasNext()) {
                nodeIterator.next();
                cnt++;
            }
            return cnt;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return 0;

    }
}
