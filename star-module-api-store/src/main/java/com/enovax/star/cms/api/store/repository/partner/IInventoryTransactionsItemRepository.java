package com.enovax.star.cms.api.store.repository.partner;


import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionItemVM;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 5/17/16.
 */
public interface IInventoryTransactionsItemRepository {

    List<InventoryTransactionItemVM> getInventoryTransactionsItem(
            Integer adminId, Date fromDate, Date toDate,  boolean showAvailable,
            String prodNm, String orderBy, String orderWith,
            Integer pageNumber, Integer pageSize);

    int getInventoryTransactionsItemSize(Integer adminId, Date fromDate,
                                                Date toDate,  boolean showAvailable, String prodNm);

    List<InventoryTransactionItemVM> getTransItemsByTransIds(Integer[] tranIds);

    List<InventoryTransactionItemVM> getTransItemsByTransId(Integer transId);

    InventoryTransactionItemVM getTransItemById(Integer id);

    void updateTransItem(InventoryTransactionItemVM item);

}
