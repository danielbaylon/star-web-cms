package com.enovax.star.cms.api.store.service.b2c;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.constant.TmStatus;
import com.enovax.payment.tm.constant.TmTransType;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.processor.TmSaleProcessor;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.b2cshared.repository.IB2CCartManager;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.b2cshared.service.IB2CMailService;
import com.enovax.star.cms.b2cshared.service.IB2CTransactionService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.axchannel.AxPinType;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailGeneratePinInput;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxFacility;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxInsertOnlineReference;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.ticket.Facility;
import com.enovax.star.cms.commons.model.ticket.TicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketTokenList;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@SuppressWarnings("Duplicates")
@Component
@Scope("prototype")
public class TelemoneyB2CSaleProcessor extends TmSaleProcessor<TelemoneyProcessingDataB2C> {

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;
    @Autowired
    private IProductService productService;
    @Autowired
    private AxStarService axStarService;
    @Autowired
    private IB2CCartManager cartManager;
    @Autowired
    private IB2CCartAndDisplayService cartAndDisplayService;
    @Autowired
    private IB2CTransactionService transService;
    @Autowired
    private IB2CMailService mailService;
    @Autowired
    private TmServiceProvider tmServiceProvider;

    //TODO Transaction query batch jobs

    @Transactional
    public void process(TelemoneyResponse tmResponse) {
        log.info("Start processing Telemoney POST for B2C...");
        log.info("Logging Telemoney Response...");
        log.info("[Telemoney Response] => " + JsonUtil.jsonify(tmResponse));

        //TODO Implement signature mechanism? Version 2.0 of TM.

        //We purposely only handle SALE type requests here. VOID type requests are handled in a different processing flow.
        final String tmTransType = tmResponse.getTmTransType();
        if (!TmTransType.Sale.tmCode.equalsIgnoreCase(tmTransType)) {
            log.info("Transaction was not of SALE type. Processing will not continue from here.");
            return;
        }

        final String tmDataReceiptNumber = tmResponse.getTmRefNo();
        final String tmDataChannel = tmResponse.getTmUserField1();
        final String tmDataSessionId = tmResponse.getTmUserField3();

        StoreApiChannels channel;
        try {

            channel = StoreApiChannels.fromCode(tmDataChannel);
        } catch (Exception e) {
            log.error("Unable to find a valid channel for this response. EX: " + e.getMessage(), e);
            return;
        }

        final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, tmDataReceiptNumber);

        final StoreTransaction txn = extData.getTxn();
        if (txn == null) {
            log.error("Unable to find a valid transaction for this response! " + tmDataReceiptNumber);
            return;
        }

        final FunCart cart = extData.getCart();
        if (cart == null) {
            log.error("Unable to find a valid cart for this response! " + tmDataReceiptNumber);
            return;
        }

        // Store TM object in DB
        boolean isFromQuery = false;
        if (txn != null && EnovaxTmSystemStatus.TmQuerySent.toString().equals(txn.getTmStatus())) {
            isFromQuery = true;
        }
        transService.saveTelemoneyLog(channel, tmResponse, isFromQuery);

        final TelemoneyProcessingDataB2C data = new TelemoneyProcessingDataB2C();
        data.setChannel(channel);
        data.setReceiptNumber(tmDataReceiptNumber);
        data.setSid(tmDataSessionId);
        data.setTxn(txn);
        data.setCart(cart);
        data.setExtData(extData);

        if (StoreApiChannels.B2C_SLM.equals(channel)) {
            final B2CSLMStoreTransaction slmTxn = transService.getB2CSLMStoreTransaction(txn.getReceiptNumber());
            data.setSlmTxn(slmTxn);
            data.setSlm(true);
        } else if (StoreApiChannels.B2C_MFLG.equals(channel)) {
            final B2CMFLGStoreTransaction mflgTxn = transService.getB2CMFLGtoreTransaction(txn.getReceiptNumber());
            data.setMflgTxn(mflgTxn);
            data.setSlm(false);
        }

        try {

            processTelemoneySalePost(tmResponse, data);
        } catch (Exception e) {
            log.error("General exception encountered while processing...", e);
        }
    }

    @Override
    protected void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        log.info("Telemoney response is NOT FOUND for the transaction. Processing...");
        _processFailedTransaction(TransactionStage.Incomplete, EnovaxTmSystemStatus.NA, tmResponse, data, "TelemoneyNotFound",
                "Transaction not found at Telemoney.", true);
    }

    @Override
    protected void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        log.info("Telemoney response is FAILED for the transaction. Processing...");
        _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "TelemoneyFailed",
                "Transaction failed at Telemoney.", true);
    }

    @Override
    protected void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        log.info("Telemoney {} has indicated a REDIRECT status for the current transaction. " +
                "Transaction {} is still pending at a third-party site.", tmResponse.getTmXid(), data.getTxn().getReceiptNumber());
        //Do nothing, and wait for the next response.
    }

    @Override
    protected void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        log.info("Telemoney response is SUCCESSFUL for the transaction. Processing...");
        final BigDecimal tmDebitAmt = tmResponse.getTmDebitAmt().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        final BigDecimal transAmount;
        if (data.isSlm()) {
            transAmount = data.getSlmTxn().getTotalAmount().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        } else {
            transAmount = data.getMflgTxn().getTotalAmount().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        }

        if (!transAmount.equals(tmDebitAmt)) {
            final String errMsg = "TM debit amount of " + tmDebitAmt + " is different from the transaction amount of " + transAmount;
            log.error(errMsg);
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "TMIncorrectAmount", errMsg, true);
        } else {
            log.info("Amount validated.");
            _finaliseSuccessfulTransaction(tmResponse, data);
        }
    }

    private void _finaliseSuccessfulTransaction(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        try {

            final StoreTransaction txn = data.getTxn();
            final FunCart cart = data.getCart();
            final String receiptNumber = data.getReceiptNumber();
            final StoreApiChannels channel = data.getChannel();
            final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();

            extDataForUpdate.setReceiptNumber(receiptNumber);
            extDataForUpdate.setTxn(txn);
            extDataForUpdate.setCart(cart);

            //TODO Set other txn stuff if missing
            txn.setStage(TransactionStage.Success.name());

            final Date now = new Date();

            if (data.isSlm()) {
                final B2CSLMStoreTransaction slmTxn = data.getSlmTxn();
                slmTxn.setStage(TransactionStage.Success.name());
                slmTxn.setTmStatus(EnovaxTmSystemStatus.Success.name());
                slmTxn.setModifiedDate(now);
                slmTxn.setMaskedCc(tmResponse.getTmCcNum());
                slmTxn.setTmApprovalCode(tmResponse.getTmApprovalCode());
                transService.saveB2CSLMStoreTransaction(slmTxn);
            } else {
                final B2CMFLGStoreTransaction mflgTxn = data.getMflgTxn();
                mflgTxn.setStage(TransactionStage.Success.name());
                mflgTxn.setTmStatus(EnovaxTmSystemStatus.Success.name());
                mflgTxn.setModifiedDate(now);
                mflgTxn.setMaskedCc(tmResponse.getTmCcNum());
                mflgTxn.setTmApprovalCode(tmResponse.getTmApprovalCode());
                transService.saveB2CMFLGStoreTransaction(mflgTxn);
            }

            final Map<String, StoreTransactionItem> axLineCommentMap = new HashMap<>();
            for (StoreTransactionItem sti : txn.getItems()) {
                axLineCommentMap.put(sti.getOtherDetails(), sti);
            }

            final AxStarCart axCart = cart.getAxCart();
            final AxStarCart checkoutCart = axCart.getCheckoutCart();
            final List<AxStarCartLine> coLines = checkoutCart.getCartLines();

            final Map<String, AxStarCartLine> cartLineCommentMap = new HashMap<>();
            for(AxStarCartLine cartLine : coLines) {
                cartLineCommentMap.put(cartLine.getComment(), cartLine);
            }

            AxStarInputSalesOrderCreation salesOrderCreation = new AxStarInputSalesOrderCreation();
            salesOrderCreation.setCartId(checkoutCart.getId());
            salesOrderCreation.setCustomerId(checkoutCart.getCustomerId());
            salesOrderCreation.setSalesOrderNumber(receiptNumber);
            salesOrderCreation.setEmail(txn.getCustEmail());

            final Map<String, String> soExtProps = new HashMap<>();
            soExtProps.put("SalesPoolId", "Direct"); //TODO Don't hardcode
            salesOrderCreation.setStringExtensionProperties(soExtProps);

            AxStarInputTenderDataLine tenderDataLine = new AxStarInputTenderDataLine();
            tenderDataLine.setAmount(txn.getTotalAmount());

            //TODO TEMPORARY ONLY, PLEASE REMOVE
            String tenderType = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.tenderType");
            if(StringUtils.isEmpty(tenderType)) { //TODO Don't hardcode
                tenderType = "Credit Card (B2B/B2C)"; //default
            }

            tenderDataLine.setTenderType(tenderType);
            salesOrderCreation.setTenderDataLines(Collections.singletonList(tenderDataLine));

            log.info("Creating sales order in AX: " + JsonUtil.jsonify(salesOrderCreation));
            final AxStarServiceResult<AxStarSalesOrder> axStarSalesOrderResult = axStarService.apiSalesOrderCreate(channel, salesOrderCreation) ;
            if (!axStarSalesOrderResult.isSuccess()) {
                log.info("Error encountered on AxStarCreateSalesOrder. " + JsonUtil.jsonify(axStarSalesOrderResult));
                _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                        "AxSalesOrderCreationFailed", "Failed to create AX sales order!", true);
                return;
            }

            final AxStarSalesOrder soData = axStarSalesOrderResult.getData();

            final Map<String, Boolean> relevantProductIds = new HashMap<>();
            final List<AxStarSalesLine> soLines = soData.getSalesLines();

            try {

                for (int i = 0; i < soLines.size(); i++) {
                    final AxStarSalesLine soLine = soLines.get(i);
                    final AxStarCartLine coLine = coLines.get(i);
                    if ((soLine.getComment().equals(coLine.getComment())) && (soLine.getQuantity().intValue() == coLine.getQuantity())) {
                        soLine.setLineId(coLine.getLineId());
                        relevantProductIds.put(soLine.getProductId().toString(), true);
                    } else {
                        throw new Exception("Not a perfect match of SO line and cart line. Reverting to previous approach.");
                    }
                }
            } catch (Exception e) {
                log.warn("Error encountered matching SO and cart line normally. Proceeding with backup implementation.", e);

                for(AxStarSalesLine saleLine : soLines) {
                    if(StringUtils.isEmpty(saleLine.getLineId())) {
                        AxStarCartLine cartLine = cartLineCommentMap.get(saleLine.getComment());
                        saleLine.setLineId(cartLine.getLineId()); //this one assign the line id that I need
                        relevantProductIds.put(saleLine.getProductId().toString(), true);
                    }
                }
            }

            log.info("AX Sales Order Creation result: " + JsonUtil.jsonify(axStarSalesOrderResult));
            extDataForUpdate.setAxSalesOrder(soData);

            //Mid-process save in case of error to prevent loss of data.
            transService.updateStoreTransactionExtensionData(channel, extDataForUpdate);

            //TODO Error handling is a must, bro.

            final String pinCode;
            if (txn.isPinToGenerate()) {
                final List<AxRetailGeneratePinInput> pinLines = new ArrayList<>();

                log.info("Getting extension properties to determine PIN validity start and end date...");
                final Map<String, ProductExtViewModel> prodExtPropsMap = new HashMap<>();
                Date validityStartDate = null;
                Date validityEndDate = null;
                try {
                    final ApiResult<List<ProductExtViewModel>> prodExtResult =
                            productService.getDataForProducts(channel, new ArrayList<>(relevantProductIds.keySet()));
                    if (prodExtResult.isSuccess()) {
                        for (ProductExtViewModel pevm : prodExtResult.getData()) {
                            prodExtPropsMap.put(pevm.getItemId(), pevm);
                            if (validityStartDate == null) {
                                validityStartDate = pevm.getOpenValidityStartDate();
                                validityEndDate = pevm.getOpenValidityEndDate();
                            } else {
                                final Date currentEndDateToCompare = pevm.getOpenValidityEndDate();
                                if (currentEndDateToCompare.before(validityEndDate)) {
                                    validityEndDate = currentEndDateToCompare;
                                }
                            }
                        }
                    } else {
                        String pemsg = "Error encountered calling product extension retrieval for pin validity period... " + prodExtResult.getMessage();
                        log.error(pemsg);
                        _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                                "AxProdExtRetrievalFailed", pemsg, true);
                        return;
                    }
                } catch (AxChannelException e) {
                    String pemsg = "Error encountered calling product extension retrieval for pin validity period... " + e.getMessage();
                    log.error(pemsg, e);
                    _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                            "AxProdExtRetrievalFailed", pemsg, true);
                    return;
                }

                for (AxStarSalesLine sl : soData.getSalesLines()) {
                    final StoreTransactionItem relatedItem = axLineCommentMap.get(sl.getComment());

                    final AxRetailGeneratePinInput pi = new AxRetailGeneratePinInput();
                    pi.setAllowPartialRedemption(false);
                    pi.setCombineTicket(false);
                    pi.setCustomerAccount("");
                    pi.setDescription(relatedItem.getReceiptNumber());

                    pi.setStartDateTime(validityStartDate);
                    pi.setEndDateTime(validityEndDate);

                    pi.setEventGroupId(relatedItem.getEventGroupId());
                    pi.setEventLineId(relatedItem.getEventLineId());

                    Date selectedEventDate = null;
                    try {

                        if (StringUtils.isNotEmpty(relatedItem.getSelectedEventDate())) {
                            selectedEventDate = NvxDateUtils.parseDate(relatedItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                        }
                    } catch (ParseException e) {
                        //This should (almost) never happen.
                        log.error("Error parsing event date: " + relatedItem.getSelectedEventDate(), e);
                    }
                    pi.setEventDate(selectedEventDate);

                    pi.setGroupTicket(false);
                    pi.setInventTransId(sl.getLineId());
                    pi.setInvoiceId("");
                    pi.setItemId(sl.getItemId());
                    pi.setLineNumber(BigDecimal.valueOf(sl.getLineNumber()));
                    pi.setMediaType("paper");
                    pi.setPackageName(receiptNumber);
                    pi.setQty(BigDecimal.valueOf(sl.getQuantity()));
                    pi.setQtyPerProduct(BigDecimal.valueOf(sl.getQuantity()));
                    pi.setReferenceId(receiptNumber);
                    pi.setRetailVariantId("");
                    pi.setSalesId("");
                    pi.setTransactionId(receiptNumber);
                    pi.setGuestName(txn.getCustName());
                    pi.setPinType(AxPinType.B2C);

                    String ccLast4String = tmResponse.getTmCcNum();
                    if (ccLast4String != null && ccLast4String.length() > 4) {
                        ccLast4String = ccLast4String.substring(ccLast4String.length() - 4, ccLast4String.length());
                    }
                    pi.setCcLast4Digits(ccLast4String);
                    pinLines.add(pi);
                }

                try {
                    final ApiResult<List<AxRetailPinTable>> axPinGenerationResult = axChannelTransactionService.apiGeneratePin(channel, pinLines);
                    if (!axPinGenerationResult.isSuccess()) {
                        log.error("Failed to perform PIN generation at AX Retail service.");
                        _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                                "PINGenerationFailed", "Failed to do AX PIN generation!", true);
                        return;
                    }

                    final List<AxRetailPinTable> pinTable = axPinGenerationResult.getData();
                    if (pinTable == null || pinTable.isEmpty()) {
                        log.error("Failed to perform PIN generation at AX Retail service. (Empty PIN Data)");
                        _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                                "PINGenerationEmptyData", "Generated PIN data is empty.", true);
                        return;
                    }

                    final AxRetailPinTable pinData = pinTable.get(0);
                    pinCode = pinData.getPinCode();

                    txn.setPinCode(pinCode);

                    if (data.isSlm()) {
                        final B2CSLMStoreTransaction slmTxn = data.getSlmTxn();
                        slmTxn.setPinCode(txn.getPinCode());
                        transService.saveB2CSLMStoreTransaction(slmTxn);
                    } else {
                        final B2CMFLGStoreTransaction mflgTxn = data.getMflgTxn();
                        mflgTxn.setPinCode(txn.getPinCode());
                        transService.saveB2CMFLGStoreTransaction(mflgTxn);
                    }
                } catch (AxChannelException e) {
                    log.error("Error encountered calling AX Retail service for PIN generation.", e);
                    _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                            "GeneralPINGenerationError", "System encountered general PIN Generation error.", true);
                    return;
                }
            } else {

                final List<AxRetailTicketRecord> axTickets;
                try {
                    final ApiResult<List<AxRetailTicketRecord>> axFinalCheckoutResult =
                            axChannelTransactionService.apiB2CCartCheckoutComplete(channel, receiptNumber);
                    if (!axFinalCheckoutResult.isSuccess()) {
                        log.error("Failed to perform final checkout at AX Retail service.");
                        _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                                "AXFinalCheckoutFailed", "Failed to perform AX Final eTicket Checkout.", true);
                        return;
                    }

                    axTickets = axFinalCheckoutResult.getData();
                } catch (AxChannelException e) {
                    log.error("Error encountered calling AX Retail service for sale completion.", e);
                    _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                            "AXFinalCheckoutFailedGeneral", "Failed to perform AX Final eTicket Checkout (general).", true);
                    return;
                }

                try {

                    final Map<String, StoreTransactionItem> tiMap = new HashMap<>();
                    for (StoreTransactionItem ti : txn.getItems()) {
                        tiMap.put(ti.getProductCode(), ti);
                    }

                    final List<ETicketData> eTickets = new ArrayList<>();
                    for (AxRetailTicketRecord axTicket : axTickets) {
                        final TicketData rawData = new TicketData();
                        //TODO Retrieve this information from AX

                        List<AxFacility> axFacilities = axTicket.getFacilityClassEntity();
                        List<Facility> facilities = new ArrayList<>();

                        for(AxFacility axFac: axFacilities) {
                            Facility fac = new Facility();
                            fac.setFacilityId(axFac.getFacilityId());
                            fac.setFacilityAction(axFac.getFacilityAction());
                            fac.setOperationIds(axFac.getOperationIds());
                            facilities.add(fac);
                        }

                        rawData.setFacilities(facilities);
                        rawData.setQuantity(1);
                        rawData.setStartDate(NvxDateUtils.formatDate(axTicket.getStartDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
                        rawData.setEndDate(NvxDateUtils.formatDate(axTicket.getEndDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
                        rawData.setTicketCode(axTicket.getTicketCode());

                        final ETicketData td = new ETicketData();

                        td.setRawValidationData(rawData);

                        String tcode = axTicket.getTicketCode();

                        if (axTicket.isNeedsActivation()) {
                            td.setThirdParty(false);
                            td.setCodeData(JsonUtil.jsonify(rawData));
                            td.setBarcodeBase64(QRCodeGenerator.toBase64(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)), 200, 200));
                        } else {
                            td.setThirdParty(true);
                            td.setCodeData(tcode);
                            td.setBarcodeBase64(BarcodeGenerator.toBase64(tcode, 200, 50));
                        }

                        td.setItemId(axTicket.getItemId());
                        td.setTicketNumber(tcode);
                        td.setValidityStartDate(NvxDateUtils.formatDate(axTicket.getStartDate(), "dd/MM/yyyy"));
                        td.setValidityEndDate(NvxDateUtils.formatDate(axTicket.getEndDate(), "dd/MM/yyyy"));

                        //TODO These should come from AX product attributes

//                        final String pid = axTicket.getItemId();
//                        final StoreTransactionItem ti = tiMap.get(pid);
//
//                        String productName = axTicket.getProductName();
//
//                        final String eventLineId = axTicket.getEventLineId();
//                        final Date eventDate = axTicket.getEventDate();
//                        if (StringUtils.isEmpty(eventLineId)) {
//                            td.setEventSession("");
//                            td.setEventDate("");
//                        } else {
//                            td.setEventSession(eventLineId);
//                            if (eventDate != null) {
//                                td.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
//                            } else {
//                                td.setEventDate("");
//                            }
//                        }

//                        productName = productName + (StringUtils.isEmpty(td.getEventSession()) ? "" : " - " + td.getEventSession()) +
//                                (StringUtils.isEmpty(td.getEventDate()) ? "" : " - " + td.getEventDate());
//                        td.setDisplayName(productName);
//                        td.setShortDisplayName(productName);
//
//                        if (ti == null) {
//                            td.setTicketPersonType("Standard");
//                            td.setTotalPrice("");
//                        } else {
//                            td.setTotalPrice(NvxNumberUtils.formatToCurrency(ti.getUnitPrice(), "S$ "));
//                            td.setTicketPersonType(ti.getType());
//                        }

                        td.setTicketTemplateName(axTicket.getTemplateName());

                        Map<String, String> tokenList = axTicket.getPrintTokenEntity();
                        td.setTokenData(JsonUtil.jsonify(new ETicketTokenList(tokenList)));

                        eTickets.add(td);
                    }

                    Collections.sort(eTickets);
                    txn.seteTickets(eTickets);
                } catch (Exception e) {
                    log.error("Error encountered generating eTickets.", e);
                    //TODO do we really need to faile this?
                    _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                            "ETicketGenerationFailed", "Something went wrong when trying to generate eTickets.", true);
                    return;
                }

                txn.setAxTickets(axTickets);
            }

            //safe spot for me to call confirm
            AxInsertOnlineReference onlineRef = new AxInsertOnlineReference();
            onlineRef.setTransactionId(txn.getReceiptNumber());
            onlineRef.setTransDate(new Date());
            try {
               ApiResult<String> confirmSalesOrderRes =  axChannelTransactionService.apiInsertOnlineReference(channel, onlineRef);;
               if(!confirmSalesOrderRes.isSuccess())  {
                   _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                           "ConfirmSalesOrderFailed", "Something went wrong when trying to confirm sales order. " + confirmSalesOrderRes.getMessage(), true);
                   return;
               }
            }catch (Exception e) {
                log.error(e.getMessage(), e);
                _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data,
                        "ConfirmSalesOrderFailed", "Something went wrong when trying to confirm sales order", true);
                return;
            }

            transService.updateStoreTransactionExtensionData(channel, extDataForUpdate);

            //TODO maybe need to retry if error???
            try {
                mailService.sendEmailForB2CBookingReceipt(channel, txn, cartAndDisplayService.getReceiptForDisplay(txn));
            } catch (Exception e) {
                log.error("Error sending email for txn = " + txn.getReceiptNumber(), e);
            }

            //Clean up cart + AX cart
            try {

                axStarService.apiCartDeleteEntireCart(channel, cart.getAxCart().getId(), cart.getAxCustomerId());
                cartManager.removeCart(channel, cart.getCartId()); //TODO move to the other page or what.
            } catch (Exception e) {
                log.error("Failed to clean up cart. Not a serious error. Proceeding...", e);
            }
        } catch (Exception e) {
            log.error("General error encountered during finalization of transaction.", e);
            log.info("Performing error handling steps for Transaction {}", data.getTxn().getReceiptNumber());
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "FinalisationError",
                    "Error encountered during finalisation of transaction.", true);
        }
    }

    private void _processFailedTransaction(TransactionStage stage, EnovaxTmSystemStatus tmStatus,
                                           TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data,
                                           String customErrCode, String customErrMsg, boolean requireVoid) {
        final Date now = new Date();

        final StoreTransaction txn = data.getTxn();

        txn.setStage(stage.name());
        txn.setTmStatus(tmStatus.name());
        txn.setModifiedDate(now);

        final String ec = StringUtils.isEmpty(customErrCode)
                ? StringUtils.isEmpty(tmResponse.getTmError()) ? "ERROR" : tmResponse.getTmError()
                : customErrCode;
        final String em = StringUtils.isEmpty(customErrMsg)
                ? StringUtils.isEmpty(tmResponse.getTmErrorMessage()) ? "ERROR" : tmResponse.getTmErrorMessage()
                : customErrMsg;
        log.error("Processing failed transaction due to an error. More details: " + ec + " - " + em);

        if (data.isSlm()) {
            final B2CSLMStoreTransaction slmTxn = data.getSlmTxn();
            slmTxn.setStage(stage.name());
            slmTxn.setTmStatus(tmStatus.name());
            slmTxn.setModifiedDate(now);
            slmTxn.setTmError(StringUtils.isEmpty(tmResponse.getTmError()) ? ec : tmResponse.getTmError());
            slmTxn.setTmErrorMessage(StringUtils.isEmpty(tmResponse.getTmErrorMessage()) ? em : tmResponse.getTmErrorMessage());
            slmTxn.setMaskedCc(tmResponse.getTmCcNum());
            transService.saveB2CSLMStoreTransaction(slmTxn);
        } else {
            final B2CMFLGStoreTransaction mflgTxn = data.getMflgTxn();
            mflgTxn.setStage(stage.name());
            mflgTxn.setTmStatus(tmStatus.name());
            mflgTxn.setModifiedDate(now);
            mflgTxn.setTmError(StringUtils.isEmpty(tmResponse.getTmError()) ? ec : tmResponse.getTmError());
            mflgTxn.setTmErrorMessage(StringUtils.isEmpty(tmResponse.getTmErrorMessage()) ? em : tmResponse.getTmErrorMessage());
            mflgTxn.setMaskedCc(tmResponse.getTmCcNum());
            transService.saveB2CMFLGStoreTransaction(mflgTxn);
        }

        //TODO Break this up into separate methods for reuse

        final boolean isPin = txn.isPinToGenerate();
        final FunCart cart = data.getCart();
        if (isPin) {
            try {

                final List<AxRetailCartTicket> reservedTickets = cart.getAxCart().getReservedTickets();
                for (AxRetailCartTicket ct : reservedTickets) {
                    ct.setQty(0);
                    ct.setUpdateCapacity(1);
                }

                final ApiResult<List<AxRetailTicketRecord>> revertResult = axChannelTransactionService.apiCartValidateAndReserveQuantity(
                        data.getChannel(), txn.getReceiptNumber(), reservedTickets, cart.getAxCustomerId());
                log.info("Result for reverting PIN: " + JsonUtil.jsonify(revertResult));
                if (!revertResult.isSuccess()) {
                    log.error("API error reversing AX reservations for PIN due to: " + revertResult.getMessage());
                    //TODO Handle?
                }
            } catch (Exception e) {
                log.error("General error reversing AX reservations for PIN due to: " + e.getMessage(), e);
                //TODO Handle?
            }
        } else {
            try {

                final ApiResult<List<AxRetailTicketRecord>> revertResult = axChannelTransactionService.apiB2CCartCheckoutCancel(
                        data.getChannel(), txn.getReceiptNumber());
                log.info("Result for reverting AX cart checkout for eTicket: " + JsonUtil.jsonify(revertResult));
                if (!revertResult.isSuccess()) {
                    log.error("API error reversing AX cart checkout for eTicket due to: " + revertResult.getMessage());
                    //TODO Handle?
                }
            } catch (Exception e) {
                log.error("General error reversing AX cart checkout for eTicket due to: " + e.getMessage(), e);
                //TODO Handle?
            }
        }

        final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
        extDataForUpdate.setReceiptNumber(txn.getReceiptNumber());
        extDataForUpdate.setTxn(txn);
        transService.updateStoreTransactionExtensionData(data.getChannel(), extDataForUpdate);

        //Transaction release tasks
        //TODO Adjust promo code capacity? Or part of AX call oredi leh
        //TODO Other tasks required?


        if (requireVoid) {
            _doVoidAction(tmResponse, data);
        }
    }

    private void _doVoidAction(TelemoneyResponse tmResponse, TelemoneyProcessingDataB2C data) {
        log.info("Preparing transaction for voiding.");
        final StoreTransaction txn = data.getTxn();
        final String receiptNumber = txn.getReceiptNumber();
        final BigDecimal totalAmount = tmResponse.getTmDebitAmt();
        final String merchantId = txn.getTmMerchantId();
        final TmCurrency currency = TmCurrency.SGD;

        final TmMerchantSignatureConfig signatureConfig = new TmMerchantSignatureConfig();
        signatureConfig.setSignatureEnabled(false);

        log.info("Executing telemoney void for " + receiptNumber);
        final TmServiceResult voidResult = tmServiceProvider.sendVoidWithRetry(merchantId, currency, totalAmount, receiptNumber, signatureConfig);
        final TelemoneyResponse voidResponse = voidResult.getTmResponse();
        final boolean hasVoidResponse = voidResponse != null;

        if (hasVoidResponse) {
            log.info("Void response from Telemoney: " + JsonUtil.jsonify(voidResponse));
            //TODO Log details into DB
        }

        final Date now = new Date();

        if (voidResult.isSuccess() && hasVoidResponse && TmStatus.isSuccess(voidResponse)) {
            log.info("Void action successful. Updating transaction status.");
            txn.setTmStatus(EnovaxTmSystemStatus.VoidSuccess.name());
            if (data.isSlm()) {
                final B2CSLMStoreTransaction slmTxn = data.getSlmTxn();
                slmTxn.setTmStatus(EnovaxTmSystemStatus.VoidSuccess.name());
                slmTxn.setStage(TransactionStage.Failed.name());
                slmTxn.setModifiedDate(now);
                transService.saveB2CSLMStoreTransaction(slmTxn);
            } else {
                final B2CMFLGStoreTransaction mflgTxn = data.getMflgTxn();
                mflgTxn.setTmStatus(EnovaxTmSystemStatus.VoidSuccess.name());
                mflgTxn.setStage(TransactionStage.Failed.name());
                mflgTxn.setModifiedDate(now);
                transService.saveB2CMFLGStoreTransaction(mflgTxn);
            }

            final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
            extDataForUpdate.setReceiptNumber(txn.getReceiptNumber());
            extDataForUpdate.setTxn(txn);
            transService.updateStoreTransactionExtensionData(data.getChannel(), extDataForUpdate);

            //TODO Send void email with error handling

        } else {
            log.info("Void action failed. Updating transaction status.");
            txn.setTmStatus(EnovaxTmSystemStatus.VoidFailed.name());

            if (data.isSlm()) {
                final B2CSLMStoreTransaction slmTxn = data.getSlmTxn();
                slmTxn.setTmStatus(EnovaxTmSystemStatus.VoidFailed.name());
                slmTxn.setStage(TransactionStage.Failed.name());
                slmTxn.setModifiedDate(now);
                transService.saveB2CSLMStoreTransaction(slmTxn);
            } else {
                final B2CMFLGStoreTransaction mflgTxn = data.getMflgTxn();
                mflgTxn.setTmStatus(EnovaxTmSystemStatus.VoidFailed.name());
                mflgTxn.setStage(TransactionStage.Failed.name());
                mflgTxn.setModifiedDate(now);
                transService.saveB2CMFLGStoreTransaction(mflgTxn);
            }

            final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
            extDataForUpdate.setReceiptNumber(txn.getReceiptNumber());
            extDataForUpdate.setTxn(txn);
            transService.updateStoreTransactionExtensionData(data.getChannel(), extDataForUpdate);

            //TODO Send void email with error handling
        }

        log.info("Void processing complete.");
    }
}
