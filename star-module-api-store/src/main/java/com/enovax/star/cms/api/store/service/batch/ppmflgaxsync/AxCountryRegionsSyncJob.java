package com.enovax.star.cms.api.store.service.batch.ppmflgaxsync;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCountryRegion;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.repository.ppmflg.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 22/9/16.
 */
@Service("PPMFLGAxCountryRegionsSyncJob")
public class AxCountryRegionsSyncJob {

    private static final Logger log = LoggerFactory.getLogger(AxCountryRegionsSyncJob.class);

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;


    @Autowired
    private AxChannelCustomerService customerService;

    @Autowired
    @Qualifier("PPMFLGICountryRegionsRepository")
    private ICountryRegionsRepository ctryRepo;

    private static volatile Runnable runner;
    private static volatile boolean isDone;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
        if(isAuthorInstance){
            runner = new Runnable() {
                @Override
                public void run() {
                    try{
                        isDone = false;
                        syncAxCountryRegionsIntoJCR();
                    }catch (Exception ex){
                        log.error("Ax country regions sync job completed with failure", ex);
                    }finally {
                        isDone = true;
                    }
                }
            };
            isDone = true;
        }
    }

    @Scheduled(cron = "${ppmflg.ax.country.region.sync.job}")
    public void axCountryRegionsSyncJob(){
        boolean isJobEnabled = sysParamSrv.isPPMFLGAxCountryRegionSyncJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[axCountryRegionsSyncJob] is not enabled.");
            return;
        }
        if(isAuthorInstance){
            if(isDone){
                realTimeTaskExecutor.submit(runner);
            }
        }
    }

    private void syncAxCountryRegionsIntoJCR() {
        ApiResult<List<AxCountryRegion>> list = this.customerService.getCountryRegion(StoreApiChannels.PARTNER_PORTAL_MFLG);
        if (list == null) {
            log.error("Ax country regions sync job completed with failure because of ax service response is empty.");
            return;
        }
        if (!list.isSuccess()) {
            log.error("Ax country regions sync job completed with failure because of ax service response is not success, error message : " + (list.getApiErrorCode() != null ? list.getApiErrorCode().message : "<empty>"));
            return;
        }
        List<AxCountryRegion> ctryList = list.getData();
        if (ctryList == null) {
            log.error("Ax country regions sync job completed with failure because of country list in ax service response is empty.");
            return;
        }

        List<String> nodeIdList = new ArrayList<String>();
        for (AxCountryRegion i : ctryList) {
            nodeIdList.add(i.getCountryId() + "-" + i.getRegionId());
        }

        String wsName = PartnerPortalConst.Partner_Portal_Ax_Data;
        ctryRepo.deleteCountryIfNotInList(wsName, nodeIdList);
        for (AxCountryRegion i : ctryList) {
            ctryRepo.saveCountry(wsName,i);
        }
        ctryRepo.refreshCache(wsName);
    }
}
