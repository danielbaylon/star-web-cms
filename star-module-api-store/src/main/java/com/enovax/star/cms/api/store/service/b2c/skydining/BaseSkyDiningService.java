package com.enovax.star.cms.api.store.service.b2c.skydining;

import com.enovax.star.cms.commons.jcrrepository.merchant.IMerchantRepository;
import com.enovax.star.cms.skydining.repository.db.*;
import com.enovax.star.cms.skydining.repository.jcr.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jace on 30/8/16.
 */
public abstract class BaseSkyDiningService {

  public static String MSG_FULL_SESSION_CAPACITY = "The selected session has reached maximum capacity for the chosen date. Please select a different session time or date.";
  public static String MSG_FULL_THEME_CAPACITY = "The selected theme has reached maximum capacity for the chosen session. Please select a different theme, session time or date.";
  public static String MSG_PACKAGE_CUT_OFF = "The selected package cannot be purchased too close to the chosen date. Please choose a later date.";
  public static String MSG_MENU_CUT_OFF = "The selected menu cannot be purchased too close to the chosen date. Please choose a later date.";
  public static String MSG_TOP_UP_CUT_OFF = "The selected top-up cannot be purchased too close to the chosen date. Please choose a later date.";
  public static String MSG_GENERAL_VALIDATION_ERROR = "There was an error processing your reservation. Please refresh the booking page and try again.";
  public static String MSG_CART_NOT_EMPTY = "Sky dining packages cannot be purchased with other packages. Please check out the itmes in the cart before purchasing a sky dining package.";

  public static String MSG_CUSTOMER_DETAILS_FAILED = "Customer details are invalid. Please try again.";
  public static String MSG_FAILED_RESERVE_TICKET = "We are unable to complete your reservation. Please clear your cart and try again, or contact our guest experience officers.";
  public static String MSG_FAILED_INSUFFICIENT_TICKETS = "There are insufficient tickets left for your selected booking. Please clear your cart and try again, or contact our guest experience officers.";
  public static String MSG_FAILED = "We are unable to continue your checkout. Please clear your cart and try again, or contact our guest experience officers.";

  public static final String SESS_TRANS_ID = "SESS_TRANS_ID";
  public static final String SESS_CUST_DETAILS = "SESS_CUST_DETAILS";
  public static final String SESS_BACK_TO_CART = "SESS_BACK_TO_CART";
  public static final String SESS_EXTRA_MESSAGE = "SESS_EXTRA_MESSAGE";
  public static final String SESS_REVAL_ERROR = "SESS_REVAL_ERROR";
  public static final String SESS_CHK_ERROR = "SESS_CHK_ERROR";

  @Autowired
  protected ISkyDiningSessionRepository sessionRepo;
  @Autowired
  protected ISkyDiningPackageRepository packageRepo;
  @Autowired
  protected ISkyDiningMenuRepository menuRepo;
  @Autowired
  protected ISkyDiningTopUpRepository topUpRepo;
  @Autowired
  protected ISkyDiningMainCourseRepository mainCourseRepo;
  @Autowired
  protected ISkyDiningDiscountRepository discountRepo;
  @Autowired
  protected ISkyDiningPromoCodeRepository promoCodeRepo;
  @Autowired
  protected ISkyDiningTncRepository tncRepo;
  @Autowired
  protected ISkyDiningThemeRepository themeRepo;

  @Autowired
  protected SkyDiningJewelCardRepository jcRepo;
  @Autowired
  protected SkyDiningReservationRepository reservationRepo;
  @Autowired
  protected SkyDiningCartRepository cartRepo;
  @Autowired
  protected SkyDiningSequenceLogRepository seqLogRepo;
  @Autowired
  protected SkyDiningTransactionRepository transRepo;

  @Autowired
  protected IMerchantRepository merchRepo;
}
