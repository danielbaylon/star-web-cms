package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by jennylynsze on 6/7/16.
 */
public interface IPartnerReservationService {

    /***
     * JCR, using database data
     */
    @Deprecated
    List<WOTReservationVM> getWOTReservations(
            Integer adminId, String fromDateStr, String toDateStr,
            String displayCancelled, String showTimes, String orderBy,
            String orderWith, Integer page, Integer pagesize);

    /**
     * JCR, using database data
     */
    @Deprecated
    int getWOTReservationsSize(
            Integer adminId, String fromDateStr, String toDateStr,
            String displayCancelled, String showTimes);
}