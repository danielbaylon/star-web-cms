package com.enovax.star.cms.api.store.controller;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.api.store.service.b2c.TelemoneyB2CService;
import com.enovax.star.cms.api.store.service.b2c.skydining.TelemoneySkyDiningService;
import com.enovax.star.cms.api.store.service.ppmflg.ITelemoneyPPMFLGService;
import com.enovax.star.cms.api.store.service.ppslm.ITelemoneyPPSLMService;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.tracking.Trackd;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/store-payment/tm/")
public class OnlineStoreTelemoneyController extends BaseStoreApiController {

    @Autowired
    private HttpSession session;

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected TelemoneyB2CService tmServiceB2C;

    @Autowired
    protected TelemoneySkyDiningService tmServiceSd;

    @Autowired
    private ITelemoneyPPSLMService tmServicePPSLM;

    @Autowired
    private ITelemoneyPPMFLGService tmServicePPMFLG;

    @RequestMapping(value = "process-tm-status/b2c", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ApiResult<String>> doProcessTmStatusForB2CSLM(@RequestParam Map<String, String> params) {
        log.info("[PAYMENT-TM-B2C-SLM] Entered doProcessTmStatusForB2CSLM.");
        try {

            final String sid = session.getId();

            final Trackd trackd = trackr.buildTrackd(sid, request);
            log.info("[PAYMENT-TM-B2C-SLM] " + trackd.toString());

            final TelemoneyResponse response = TelemoneyResponse.fromUrlParams(params, false);

            if(StringUtils.equals(response.getTmUserField2(),"sky-dining")) {
                tmServiceSd.processMerchantPost(response);
            } else {
                tmServiceB2C.processMerchantPost(response);
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", "YES"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("==System Exception Encountered : doProcessTmStatusForB2CSLM==");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "process-tm-status/ppslm", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ApiResult<String>> doProcessTmStatusForPartnerPortalSLM(@RequestParam Map<String, String> params) {
        log.info("[PAYMENT-TM-PP-SLM] Entered doProcessTmStatusForPartnerPortalSLM.");
        try {

            final String sid = session.getId();

            final Trackd trackd = trackr.buildTrackd(sid, request);
            log.info("[PAYMENT-TM-PP-SLM] " + trackd.toString());

            final TelemoneyResponse response = TelemoneyResponse.fromUrlParams(params, false);
            tmServicePPSLM.processMerchantPost(response);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", "YES"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("==System Exception Encountered :  doProcessTmStatusForPartnerPortalSLM==");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "process-tm-status/ppmflg", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ApiResult<String>> doProcessTmStatusForPartnerPortalMFLG(@RequestParam Map<String, String> params) {
        log.info("[PAYMENT-TM-PP-MFLG] Entered doProcessTmStatusForPartnerPortalMFLG.");
        try {

            final String sid = session.getId();

            final Trackd trackd = trackr.buildTrackd(sid, request);
            log.info("[PAYMENT-TM-PP-MFLG] " + trackd.toString());

            final TelemoneyResponse response = TelemoneyResponse.fromUrlParams(params, false);
            tmServicePPMFLG.processMerchantPost(response);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", "YES"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("==System Exception Encountered :  doProcessTmStatusForPartnerPortalMFLG==");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}
