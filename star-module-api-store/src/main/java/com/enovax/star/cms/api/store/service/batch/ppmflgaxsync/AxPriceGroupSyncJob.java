package com.enovax.star.cms.api.store.service.batch.ppmflgaxsync;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.tier.ProductTier;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxPriceGroup;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.repository.ppmflg.IProductTierRepository;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 25/9/16.
 */
@Service("PPMFLGAxPriceGroupSyncJob")
public class AxPriceGroupSyncJob {
    private static final Logger log = LoggerFactory.getLogger(AxCountryRegionsSyncJob.class);

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    @Autowired
    private AxChannelCustomerService axCustSrv;

    @Autowired
    @Qualifier("PPMFLGIProductTierRepository")
    private IProductTierRepository tierRepo;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    private static volatile Runnable runner;
    private static volatile boolean isDone;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
        if(isAuthorInstance){
            runner = new Runnable() {
                @Override
                public void run() {
                    try{
                        isDone = false;
                        syncAxPriceGroupIntoJCR();
                    }catch (Exception ex){
                        log.error("Ax price group sync job completed with failure", ex);
                    }finally {
                        isDone = true;
                    }
                }
            };
            isDone = true;
        }
    }

    @Scheduled(cron = "${ppmflg.ax.price.group.sync.job}")
    public void axPriceGroupSyncJob(){
        boolean isJobEnabled = sysParamSrv.isPPMFLGAxPriceGroupSyncJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[axPriceGroupSyncJob] is not enabled.");
            return;
        }
        if(isAuthorInstance){
            if(isDone){
                realTimeTaskExecutor.submit(runner);
            }
        }
    }

    private void syncAxPriceGroupIntoJCR() {
        ApiResult<List<AxPriceGroup>> list = this.axCustSrv.getAxPriceGroupList(StoreApiChannels.PARTNER_PORTAL_MFLG);
        if (list == null) {
            log.error("Ax price group job completed with failure because of ax service response is empty.");
            return;
        }
        if (!list.isSuccess()) {
            log.error("Ax price group sync job completed with failure because of ax service response is not success, error message : "
                    + (list.getErrorCode() != null ? list.getErrorCode() : "<empty>")
                    + " , "
                    + (list.getMessage() != null ? list.getMessage() : "<empty>")
            );
            return;
        }
            List<AxPriceGroup> axPriceGroupList = list.getData();
            if (axPriceGroupList == null) {
                log.error("Ax price group sync job completed with failure because of price group in ax service response is empty.");
                return;
            }
            String wsName = PartnerPortalConst.Partner_Portal_Channel;
            //Iterator<AxPriceGroup> axPriceGroupIterator = axPriceGroupList.iterator();
            List<ProductTier> priceGroups = tierRepo.getTiers(wsName);
            List<String> priceGroupIds = new ArrayList<String>();
            for (ProductTier p : priceGroups) {
                priceGroupIds.add(p.getPriceGroupId());
            }
            for (AxPriceGroup apg : axPriceGroupList) {
                if (priceGroupIds.contains(apg.getPriceGroupId()))
                    priceGroupIds.remove(apg.getPriceGroupId());
            }

            //remove the additional price groups from jcr that is not in ax
            for (String p : priceGroupIds) {
                tierRepo.removePriceGroup(wsName,p);
            }
            //create or update the price groups from ax
            for (AxPriceGroup i : axPriceGroupList) {
                if (i.getPriceGroupId() != null && i.getPriceGroupId().trim().length() > 0) {
                    tierRepo.createOrUpdatePriceGroup(wsName, i);
                }
            }

    }
}
