package com.enovax.star.cms.api.store.service.batch.ppmflgaxsync;

/**
 * Created by houtao on 27/9/16.
 */
public interface IPartnerProfileSyncJob {

    public void sync(Integer paId);

    public boolean check(Integer paId);

}
