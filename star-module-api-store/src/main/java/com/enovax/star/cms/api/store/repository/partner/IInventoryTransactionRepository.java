package com.enovax.star.cms.api.store.repository.partner;


import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;

/**
 * Created by jennylynsze on 5/18/16.
 */
public interface IInventoryTransactionRepository {
    InventoryTransactionVM getTransByUser(Integer transId, Integer mainAccountId);
}
