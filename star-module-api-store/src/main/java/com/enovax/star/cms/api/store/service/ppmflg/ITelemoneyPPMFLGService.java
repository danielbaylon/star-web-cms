package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.model.TelemoneyResponse;

/**
 * Created by houtao on 14/11/16.
 */
public interface ITelemoneyPPMFLGService  {
    void processMerchantPost(TelemoneyResponse response);
}
