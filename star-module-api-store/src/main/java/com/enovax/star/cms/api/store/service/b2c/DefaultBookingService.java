package com.enovax.star.cms.api.store.service.b2c;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmFraudCheckPackage;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.api.store.service.product.IProductCmsService;
import com.enovax.star.cms.b2cshared.repository.IB2CBookingRepository;
import com.enovax.star.cms.b2cshared.repository.IB2CCartManager;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.b2cshared.service.IB2CTransactionService;
import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.discountcounter.AxDiscountCounter;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.promotions.ApplyAffiliationRequest;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.merchant.Merchant;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.*;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@SuppressWarnings("Duplicates")
@Service
public class DefaultBookingService implements IBookingService {

    private Logger log = LoggerFactory.getLogger(DefaultBookingService.class);

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;
    @Autowired
    private AxStarService axStarService;
    @Autowired
    private IProductService productService;
    @Autowired
    private IProductCmsService productCmsService;
    @Autowired
    private StarTemplatingFunctions starfn;
    @Autowired
    private IB2CBookingRepository bookingRepository;
    @Autowired
    private IB2CCartManager cartManager;
    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private IB2CCartAndDisplayService cartAndDisplayService;
    @Autowired
    private IB2CTransactionService transService;
    @Autowired
    private ObjectFactory<TelemoneyB2CSaleProcessor> tmB2CSaleProcessorFactory;
    @Autowired
    private ISystemParamRepository sysParamRepo;

    private void saveTransactionInSession(StoreApiChannels channel, HttpSession session, StoreTransaction txn) {
        session.setAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN, txn);
    }
    private StoreTransaction getTransactionInSession(StoreApiChannels channel, HttpSession session) {
        return (StoreTransaction) session.getAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }
    private void clearTransactionInSession(StoreApiChannels channel, HttpSession session) {
        session.removeAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }

    //TODO Business rules validation for all steps in the booking process
    //TODO Cart is stored in session, which means if you change quantity of updated item, it'll get updated instantly.
    //TODO Get products data from server, stop using client data for cart flow.

    @Override
    public ApiResult<FunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId) {
        FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            savedCart = new FunCart();
            savedCart.setCartId(sessionId);
            savedCart.setChannelCode(channel.code);
        }

        final List<FunCartItem> itemsForAdding = cartToAdd.getItems();

        final List<FunCartItem> savedItems = savedCart.getItems();
        final Map<String, FunCartItem> savedItemsMap = new HashMap<>();
        final Map<String, FunCartItem> savedAxLineCommentMap = savedCart.getAxLineCommentMap();

        for (FunCartItem savedItem : savedItems) {
            savedItemsMap.put(savedItem.getCartItemId(), savedItem);
        }

        String discountCodeToAdd = "";
        String dcCmsProductId = "";

        final List<String> productIdsForAdd = new ArrayList<>();
        for (FunCartItem itemToAdd : itemsForAdding) {
            productIdsForAdd.add(itemToAdd.getListingId());

            final String dclala = itemToAdd.getDiscountCode();
            if (StringUtils.isNotEmpty(dclala)) {
                discountCodeToAdd = dclala;
                dcCmsProductId = itemToAdd.getCmsProductId();
            }
        }

        String dcOfferId = "";
        if (StringUtils.isNotEmpty(discountCodeToAdd)) {
            final ApiResult<String> offerIdResult = productCmsService.getOfferIdForPromoCode(channel.code, dcCmsProductId, discountCodeToAdd);
            if (!offerIdResult.isSuccess()) {
                log.error("Unable to find offer ID for promo code: " + offerIdResult.getMessage());
                return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeOfferId);
            }

            dcOfferId = offerIdResult.getData();

            //Check discount code usage
            try {

                final ApiResult<AxDiscountCounter> dcUsageResult =
                        axChannelTransactionService.apiGetDiscountCounter(channel, dcOfferId, discountCodeToAdd);
                if (!dcUsageResult.isSuccess()) {
                    log.error("Discount code usage query failed: " + dcUsageResult.getMessage());
                    return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
                }

                final AxDiscountCounter dcCounter = dcUsageResult.getData();
                final Integer dcMaxUse = dcCounter.getMaxUse();
                if (dcMaxUse > 0) {
                    final Integer dcQty = dcCounter.getQuantity();
                    if (dcQty < 1) {
                        log.error("No more usage available for discount code.");
                        return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
                    }
                }
            } catch (Exception e) {
                log.error("Unable to check promo code usage...", e);
                return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
            }
        }

        log.info("Getting extension properties for related items to be added to cart...");
        final Map<String, ProductExtViewModel> prodExtPropsMap = new HashMap<>();
        try {

            final ApiResult<List<ProductExtViewModel>> prodExtResult = productService.getDataForProducts(channel, productIdsForAdd);
            if (prodExtResult.isSuccess()) {
                for (ProductExtViewModel pevm : prodExtResult.getData()) {
                    prodExtPropsMap.put(pevm.getItemId(), pevm);
                }
            } else {
                log.error("Error encountered after calling product extension properties retrieval... " + prodExtResult.getMessage());
                return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
            }
        } catch (AxChannelException e) {
            log.error("Error encountered while trying to get product extension properties.", e);
            return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
        }

        final List<FunCartItem> updatedItems = new ArrayList<>();
        final Map<String, FunCartItem> updatedFunCartItemMap = new HashMap<>();
        final Map<String, Integer> addedQuantities = new HashMap<>();

        for (FunCartItem itemToAdd : itemsForAdding) {
            final String cartItemId = itemToAdd.generateCartItemId();

            FunCartItem theItem = savedItemsMap.get(cartItemId);

            if (theItem == null) {
                theItem = new FunCartItem();
                theItem.setCmsProductId(itemToAdd.getCmsProductId());
                theItem.setCmsProductName(itemToAdd.getCmsProductName());
                theItem.setListingId(itemToAdd.getListingId());
                theItem.setName(itemToAdd.getName());
                theItem.setPrice(itemToAdd.getPrice());
                theItem.setProductCode(itemToAdd.getProductCode());
                theItem.setQty(itemToAdd.getQty());
                theItem.setType(itemToAdd.getType());

                final ProductExtViewModel prodExt = prodExtPropsMap.get(itemToAdd.getProductCode());
                final boolean isEvent = prodExt.isEvent();
                final boolean isOpenDate = prodExt.isOpenDate();

                theItem.setOpenDate(isOpenDate);
                if (isEvent) {
                    theItem.setEventGroupId(prodExt.getEventGroupId());

                    final String defaultEventLineId = prodExt.getDefaultEventLineId();

                    if(StringUtils.isNotEmpty(defaultEventLineId)) {
                        theItem.setEventLineId(defaultEventLineId);
                    }else {
                        theItem.setEventLineId(itemToAdd.getEventLineId());
                    }

                    if(StringUtils.isNotEmpty(itemToAdd.getSelectedEventDate())) {
                        theItem.setSelectedEventDate(itemToAdd.getSelectedEventDate());
                        theItem.setEventSessionName(itemToAdd.getEventSessionName());
                    }
                }

                final String discountCode = itemToAdd.getDiscountCode();
                if (StringUtils.isNotEmpty(discountCode)) {
                    theItem.setDiscountId(dcOfferId);
                }
                theItem.setDiscountCode(discountCode);

                theItem.setCartItemId(cartItemId);

                savedItems.add(theItem);
                savedItemsMap.put(theItem.getCartItemId(), theItem);
                savedAxLineCommentMap.put(theItem.getAxLineCommentId(), theItem);
            } else {
                theItem.setQty(theItem.getQty() + itemToAdd.getQty());
            }

            addedQuantities.put(cartItemId, itemToAdd.getQty());

            updatedItems.add(theItem);

            //This assumes that the AX listing ID is unique per CMS product.
            updatedFunCartItemMap.put(theItem.getListingId(), theItem);
        }

        log.info("Validating capacity...");
        try {

            final List<AxRetailCartTicket> cartValidateTickets = new ArrayList<>();
            for (ProductExtViewModel prodExtProp : prodExtPropsMap.values()) {
                final FunCartItem item = updatedFunCartItemMap.get(prodExtProp.getProductId());

                String mediaTypeId = prodExtProp.getMediaTypeId();
                if (StringUtils.isNotEmpty(mediaTypeId)) {
                    mediaTypeId = mediaTypeId.toLowerCase();
                } else {
                    mediaTypeId = "";
                }

                item.setMediaTypePin("pin".equals(mediaTypeId)); //TODO extract hardcoding

                final AxRetailCartTicket ct = new AxRetailCartTicket();
                ct.setLineId("");
                ct.setTransactionId("");
                ct.setItemId(prodExtProp.getItemId());
                ct.setListingId(0L);
                ct.setUpdateCapacity(0);
                ct.setQty(item.getQty());

                if (StringUtils.isNotEmpty(item.getEventGroupId())) {
                    ct.setEventGroupId(prodExtProp.getEventGroupId());

                    final String defaultEventLineId = prodExtProp.getDefaultEventLineId();

                    if(StringUtils.isNotEmpty(defaultEventLineId)) {
                        ct.setEventLineId(defaultEventLineId);
                    }else {
                        ct.setEventLineId(item.getEventLineId());
                    }

                    if(StringUtils.isNotEmpty(item.getSelectedEventDate())) {
                        ct.setEventDate(NvxDateUtils.parseDate(item.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
                    }
                }

                cartValidateTickets.add(ct);
            }

            if (!cartValidateTickets.isEmpty()) {
                final ApiResult<List<AxRetailTicketRecord>> capacityValidationResult =
                        axChannelTransactionService.apiCartValidateQuantity(channel, null, cartValidateTickets);
                //TODO Evaluate with NEC if this sort of result object is good enough.
                if (!capacityValidationResult.isSuccess()) {
                    log.error("Failed to perform capacity validation at AX Retail Service... " + capacityValidationResult.getMessage());
                    return new ApiResult<>(ApiErrorCodes.InsufficientQty);
                }
            }
        } catch (Exception e) {
            log.error("Error encountered while trying to validate capacity.", e);
            return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
        }

        final AxStarCart axCart = savedCart.getAxCart();
        String axCartId = axCart == null ? "" : axCart.getId();

        final AxStarInputAddCart axc = new AxStarInputAddCart();
        final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

        //TODO Handle multiple items with same listing ID. Need to add cart multiple times. Refer: B2B update cart

        final Map<String, Integer> updatedItemQtyMap = new HashMap<>();
        for (FunCartItem updatedItem : updatedItems) {
            final String productId = updatedItem.getListingId();
            Integer updatedQty = updatedItemQtyMap.get(productId);
            if (updatedQty == null) { updatedQty = 0; }
            Integer addedQty = addedQuantities.get(updatedItem.getCartItemId());
            updatedQty += (addedQty == null ? 0 : addedQty);
            updatedItemQtyMap.put(productId, updatedQty);
        }

        for (Map.Entry<String, Integer> entry : updatedItemQtyMap.entrySet()) {
            final String productId = entry.getKey();
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();

            axcItem.setProductId(Long.parseLong(productId));
            axcItem.setQuantity(entry.getValue());

            FunCartItem cartItem = updatedFunCartItemMap.get(productId);
            axcItem.setComment(cartItem.getAxLineCommentId());

            axcItems.add(axcItem);
        }

        axc.setCartId(StringUtils.isEmpty(axCartId) ? "" : axCartId);
        axc.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        axc.setItems(axcItems);

        final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
        if (!axStarResult.isSuccess()) {
            log.info("Error encountered adding to cart. " + JsonUtil.jsonify(axStarResult));

            for (FunCartItem itemToAdd : cartToAdd.getItems()) {
                final String cartItemId = itemToAdd.generateCartItemId();
                final FunCartItem savedItem = savedItemsMap.get(cartItemId);
                final int originalQty = savedItem.getQty() - itemToAdd.getQty();
                if (originalQty > 0) {
                    savedItem.setQty(originalQty);
                } else {
                    savedItemsMap.remove(cartItemId);
                    savedItems.remove(savedItem);
                }
            }

            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddItem);
        }

        final AxStarCart updatedAxCart = axStarResult.getData().getCart();
        savedCart.setAxCustomerId(updatedAxCart.getCustomerId());
        savedCart.setAxCart(updatedAxCart);
        cartManager.saveCart(channel, savedCart);

        if (StringUtils.isNotEmpty(discountCodeToAdd)) {
            final AxStarServiceResult<String> axResult = axStarService.apiCartApplyPromoCode(channel, updatedAxCart.getId(),
                    StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId(), discountCodeToAdd);
            if (!axResult.isSuccess()) {
                log.warn("Unable to apply promo code due to error, still proceeding: " + JsonUtil.jsonify(axResult));
                //TODO decide if this warrants further handling
            } else {
                final Map<String, String> appliedPromoCodes = savedCart.getAppliedPromoCodes();
                appliedPromoCodes.put(discountCodeToAdd, dcOfferId);

                AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(
                        channel, updatedAxCart.getId(), StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
                if (!getCartResult.isSuccess()) {
                    log.warn("Unable to get updated cart... Proceeding for now: " + JsonUtil.jsonify(getCartResult));
                    //TODO decide if this warrants further handling, actually, needed
                } else {
                    savedCart.setAxCart(getCartResult.getData().getCart());
                    cartManager.saveCart(channel, savedCart);
                }
            }
        }

        final FunCartDisplay funCartDisplay = cartAndDisplayService.constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public ApiResult<FunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, FunCart topupCart) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        //TODO What if topups were event products?
        //TODO What if main items were events? This code doesn't support that yet.
        //TODO Add product extension retrieval
        //TODO Add capacity validation
        //TODO Actually have to check the cross-sell AX rules. Assumption: per product qty limit
        //TODO PIN and eTicket media type detection for top up

        final Map<String, FunCartItem> savedAxLineCommentMap = savedCart.getAxLineCommentMap();

        final Map<String, FunCartItem> savedMainItemsMap = new HashMap<>();
        final Map<String, FunCartItem> savedTopupItemsMap = new HashMap<>();
        final List<FunCartItem> savedItems = savedCart.getItems();
        for (FunCartItem savedItem : savedItems) {
            if (savedItem.isTopup()) {
                savedTopupItemsMap.put(savedItem.getCartItemId(), savedItem);
            } else {
                savedMainItemsMap.put(savedItem.getCmsProductId() + savedItem.getListingId(), savedItem);
            }
        }

        final Map<String, FunCartItem> updatedFunCartItemMap = new HashMap<>();
        final List<FunCartItem> updatedTopups = new ArrayList<>();
        final Map<String, Integer> addedQuantities = new HashMap<>();
        for (FunCartItem newTopupItem : topupCart.getItems()) {
            final String parentCmsProductId = newTopupItem.getParentCmsProductId();
            final String parentListingId = newTopupItem.getParentListingId();
            final String newCartItemId = newTopupItem.generateCartItemId();
            final Integer topupQty = newTopupItem.getQty();

            if (topupQty == 0) {
                continue;
            }

            final FunCartItem parentItem = savedMainItemsMap.get(newTopupItem.getCmsProductId() + newTopupItem.getParentListingId());
            final Integer parentQty = parentItem.getQty();

            final FunCartItem existingTopup = savedTopupItemsMap.get(newCartItemId);
            if (existingTopup != null) {
                final Integer existingQty = existingTopup.getQty();

                if (existingQty + topupQty > parentQty) {
                    return new ApiResult<>(ApiErrorCodes.CartTopupOverLimit);
                }

                existingTopup.setQty(existingQty + topupQty);

                updatedTopups.add(existingTopup);
                updatedFunCartItemMap.put(existingTopup.getListingId(), existingTopup);
            } else {
                if (topupQty > parentQty) {
                    return new ApiResult<>(ApiErrorCodes.CartTopupOverLimit);
                }

                final FunCartItem newTopup = new FunCartItem();
                newTopup.setCmsProductId(newTopupItem.getCmsProductId());
                newTopup.setCmsProductName(newTopupItem.getCmsProductName());
                newTopup.setListingId(newTopupItem.getListingId());
                newTopup.setName(newTopupItem.getName());
                newTopup.setPrice(newTopupItem.getPrice());
                newTopup.setProductCode(newTopupItem.getProductCode());
                newTopup.setQty(newTopupItem.getQty());
                newTopup.setType(newTopupItem.getType());

                newTopup.setEventGroupId("");
                newTopup.setEventLineId("");
                newTopup.setSelectedEventDate("");
                newTopup.setEventSessionName("");

                newTopup.setIsTopup(true);
                newTopup.setParentCmsProductId(parentCmsProductId);
                newTopup.setParentListingId(parentListingId);

                newTopup.setCartItemId(newCartItemId);

                savedItems.add(newTopup);
                savedTopupItemsMap.put(newCartItemId, newTopup);
                savedAxLineCommentMap.put(newTopup.getAxLineCommentId(), newTopup);

                updatedTopups.add(newTopup);
                updatedFunCartItemMap.put(newTopup.getListingId(), newTopup);
            }

            addedQuantities.put(newCartItemId, topupQty);
        }

        final AxStarCart axCart = savedCart.getAxCart();
        final String axCartId = axCart != null ? axCart.getId() : "";

        //TODO Handle what if there are 2 updated items with the same listing ID -- EDIT i think this is now handled
        //TODO When adding to cart, how is the event date different and same product added? HANDLE

        AxStarServiceResult<AxStarCartNested> axStarResult = null;

        // final Map<String, Integer> updatedItemQtyMap = new HashMap<>();
        for (FunCartItem updatedItem : updatedTopups) {
            final AxStarInputAddCart axc = new AxStarInputAddCart();
            final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();
            final String productId = updatedItem.getListingId();
//            Integer updatedQty = updatedItemQtyMap.get(productId);
//            if (updatedQty == null) { updatedQty = 0; }
//            Integer addedQty = addedQuantities.get(updatedItem.getCartItemId());
//            updatedQty += (addedQty == null ? 0 : addedQty);
//            updatedItemQtyMap.put(productId, updatedQty);

            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();
            axcItem.setProductId(Long.parseLong(productId));
            axcItem.setQuantity(addedQuantities.get(updatedItem.getCartItemId()));
            axcItem.setComment(updatedItem.getAxLineCommentId());
            axcItems.add(axcItem);

            axc.setCartId(StringUtils.isEmpty(axCartId) ? "" : axCartId);
            axc.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
            axc.setItems(axcItems);

            axStarResult = axStarService.apiCartAddItem(channel, axc);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered adding to cart. " + JsonUtil.jsonify(axStarResult));

                for (FunCartItem itemToAdd : topupCart.getItems()) {
                    final String cartItemId = itemToAdd.generateCartItemId();
                    final FunCartItem savedItem = savedTopupItemsMap.get(cartItemId);
                    final int originalQty = savedItem.getQty() - itemToAdd.getQty();
                    if (originalQty > 0) {
                        savedItem.setQty(originalQty);
                    } else {
                        savedTopupItemsMap.remove(cartItemId);
                        savedItems.remove(savedItem);
                    }
                }

                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddItem);
            }
        }

//        for (Map.Entry<String, Integer> entry : updatedItemQtyMap.entrySet()) {
//            final String productId = entry.getKey();
//            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();
//
//            axcItem.setProductId(Long.parseLong(productId));
//            axcItem.setQuantity(entry.getValue());
//
//            final FunCartItem relatedTopup = updatedFunCartItemMap.get(productId);
//            axcItem.setComment(relatedTopup.getAxLineCommentId());
//
//            axcItems.add(axcItem);
//        }

        final AxStarCart updatedAxCart = axStarResult.getData().getCart();
        savedCart.setAxCustomerId(updatedAxCart.getCustomerId());
        savedCart.setAxCart(updatedAxCart);
        cartManager.saveCart(channel, savedCart);

        final FunCartDisplay funCartDisplay = cartAndDisplayService.constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public ApiResult<FunCartDisplay> cartUpdate(StoreApiChannels channel, FunCart cartToUpdate, String sessionId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        //TODO Update AX cart logic
//        final Map<String, FunCartItem> savedItemsMap = new HashMap<>();
//        final List<FunCartItem> savedItems = savedCart.getItems();
//        for (FunCartItem savedItem : savedItems) {
//            savedItemsMap.put(savedItem.getListingId(), savedItem);
//        }
//
//        final List<FunCartItem> itemsToRemove = new ArrayList<>();
//
//        for (FunCartItem updatedItem : cartToUpdate.getItems()) {
//            final FunCartItem savedItem = savedItemsMap.get(updatedItem.getListingId());
//            if (savedItem == null) {
//                log.info("Cart Item " + updatedItem.getListingId() + " was not updated as it was not found in the saved cart.");
//                continue;
//            }
//
//            if (updatedItem.getQty() <= 0) {
//                itemsToRemove.add(savedItem);
//            } else {
//                savedItem.setQty(updatedItem.getQty());
//            }
//        }
//
//        if (!itemsToRemove.isEmpty()) {
//           savedItems.removeAll(itemsToRemove);
//        }
//
//        cartManager.saveCart(channel, savedCart);
//
//        final FunCartDisplay funCartDisplay = constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", new FunCartDisplay());
    }

    @Override
    public ApiResult<String> cartApplyAffiliation(StoreApiChannels channel, String sessionId, ApplyAffiliationRequest applyAffiliationRequest) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        final CustomerDetails passedDeets = applyAffiliationRequest.getDeets();
        final boolean hasPassedDeets = passedDeets != null;

        final String cartId = savedCart.getAxCart().getId();
        final String customerId = savedCart.getAxCustomerId();

        final boolean isRemoveAction  = applyAffiliationRequest.isRemove();
        final boolean isBankAffiliation = applyAffiliationRequest.isBankAffiliation();

        if (isRemoveAction) {
            final FunCartAffiliation relatedAffiliation = isBankAffiliation ? savedCart.getBankAffiliation() : savedCart.getPaymentModeAffiliation();
            if (relatedAffiliation == null) {
                log.info("Related affiliation for the cart is already empty, no use in removing.");
                return new ApiResult<>(true, "", "", "OK");
            }

            final AxStarServiceResult<String> removeAffResult = axStarService.apiCartRemoveAffiliation(
                    channel, cartId, customerId, Collections.singletonList(relatedAffiliation.getAffiliationId()));
            if (!removeAffResult.isSuccess()) {
                log.error("Error removing affiliation from the cart.");
                return new ApiResult<>(ApiErrorCodes.CartErrorApplyAffiliation);
            }

            if (isBankAffiliation) {
                savedCart.setBankAffiliation(null);
            } else {
                savedCart.setPaymentModeAffiliation(null);
            }

            final AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(
                    channel, savedCart.getAxCart().getId(), StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
            if (!getCartResult.isSuccess()) {
                log.error("Error getting updated cart."); //TODO Further handling
                return new ApiResult<>(ApiErrorCodes.CartErrorApplyAffiliation);
            }

            if (hasPassedDeets) {
                passedDeets.setMerchantId("");
                passedDeets.setSelectedMerchant("");
                savedCart.setSavedCustomerDeets(passedDeets);
            }

            savedCart.setAxCart(getCartResult.getData().getCart());
            cartManager.saveCart(channel, savedCart);

            log.info("Successfully removed affiliation.");
            return new ApiResult<>(true, "", "", removeAffResult.getData());
        }

        //TODO Validation for client-side input

        final FunCartAffiliation existingAffiliation = isBankAffiliation ? savedCart.getBankAffiliation() : savedCart.getPaymentModeAffiliation();
        if (existingAffiliation != null) {
            final AxStarServiceResult<String> removeAffResult = axStarService.apiCartRemoveAffiliation(
                    channel, cartId, customerId, Collections.singletonList(existingAffiliation.getAffiliationId()));
            if (!removeAffResult.isSuccess()) {
                log.error("Error removing an existing affiliation from the cart.");
                return new ApiResult<>(ApiErrorCodes.CartErrorApplyAffiliation);
            }
        }

        final FunCartAffiliation affiliationToAdd = applyAffiliationRequest.getAffiliationToAdd();
        log.info("Applying affiliation " + affiliationToAdd.getAffiliationName() + "  to the shopping cart...");

        final AxStarServiceResult<String> addAffiliationResult = axStarService.apiCartAddAffiliation(
                channel, cartId, customerId, Collections.singletonList(affiliationToAdd.getAffiliationId()));
        if (!addAffiliationResult.isSuccess()) {
            log.error("Error adding affiliation to the cart.");
            return new ApiResult<>(ApiErrorCodes.CartErrorApplyAffiliation);
        }

        //TODO Get additional affiliation information!!

        if (isBankAffiliation) {
            savedCart.setBankAffiliation(affiliationToAdd);
        } else {
            savedCart.setPaymentModeAffiliation(affiliationToAdd);
        }

        final AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(
                channel, savedCart.getAxCart().getId(), StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        if (!getCartResult.isSuccess()) {
            log.error("Error getting updated cart."); //TODO Further handling
            return new ApiResult<>(ApiErrorCodes.CartErrorApplyAffiliation);
        }

        if (hasPassedDeets) {
            passedDeets.setMerchantId(affiliationToAdd.getAffiliationType());
            savedCart.setSavedCustomerDeets(passedDeets);
        }

        savedCart.setAxCart(getCartResult.getData().getCart());
        cartManager.saveCart(channel, savedCart);

        log.info("Successfully added affiliation.");
        return new ApiResult<>(true, "", "", addAffiliationResult.getData());
    }

    @Override
    public ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        String dcOfferId = "";
        if (StringUtils.isNotEmpty(promoCode)) {
            final ApiResult<String> offerIdResult = productCmsService.getOfferIdForPromoCode(channel.code, promoCode, true);
            if (!offerIdResult.isSuccess()) {
                log.error("Unable to find offer ID for promo code: " + offerIdResult.getMessage());
                return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeOfferId);
            }

            dcOfferId = offerIdResult.getData();

            //Check discount code usage
            try {

                final ApiResult<AxDiscountCounter> dcUsageResult =
                        axChannelTransactionService.apiGetDiscountCounter(channel, dcOfferId, promoCode);
                if (!dcUsageResult.isSuccess()) {
                    log.error("Discount code usage query failed: " + dcUsageResult.getMessage());
                    return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
                }

                final AxDiscountCounter dcCounter = dcUsageResult.getData();
                final Integer dcMaxUse = dcCounter.getMaxUse();
                if (dcMaxUse > 0) {
                    final Integer dcQty = dcCounter.getQuantity();
                    if (dcQty < 1) {
                        log.error("No more usage available for discount code.");
                        return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
                    }
                }
            } catch (Exception e) {
                log.error("Unable to check promo code usage...", e);
                return new ApiResult<>(ApiErrorCodes.CartErrorPromoCodeUsage);
            }
        }

        final AxStarCart axCart = savedCart.getAxCart();
        final AxStarServiceResult<String> axResult = axStarService.apiCartApplyPromoCode(channel, axCart.getId(),
                StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId(), promoCode);
        if (!axResult.isSuccess()) {
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddPromoCode);
        }

        final Map<String, String> appliedPromoCodes = savedCart.getAppliedPromoCodes();
        appliedPromoCodes.put(promoCode, dcOfferId);

        AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(
                channel, axCart.getId(), StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        if (!getCartResult.isSuccess()) {
            log.error("Error getting updated cart."); //TODO Further handling
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddPromoCode);
        }

        savedCart.setAxCart(getCartResult.getData().getCart());
        cartManager.saveCart(channel, savedCart);

        return new ApiResult<>(true, "", "", axResult.getData());
    }

    @Override
    public ApiResult<List<AxProduct>> checkPromoCode(StoreApiChannels channel, String promoCode) {
        //TODO
        return null;
    }

    @Override
    public ApiResult<FunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        FunCartItem itemToRemove = null;
        for (FunCartItem item : savedCart.getItems()) {
            if (item.getCartItemId().equals(cartItemId)) {
                itemToRemove = item;
            }
        }

        if (itemToRemove != null) {
            final String mainListingId = itemToRemove.getListingId();

            //TODO Top ups also do not support events @ this code block

            final Map<String, FunCartItem> relatedTopupCommentMap = new HashMap<>();
            if (!itemToRemove.isTopup()) {
                for (FunCartItem item : savedCart.getItems()) {
                    if (item.isTopup() && mainListingId.equals(item.getParentListingId())) {
                        relatedTopupCommentMap.put(item.getAxLineCommentId(), item);
                    }
                }
            }

            final String cartItemCommentId = itemToRemove.getAxLineCommentId();

            final AxStarCart axCart = savedCart.getAxCart();
            final List<AxStarCartLine> axCartLines = axCart.getCartLines();

            final List<String> lineIdsToRemove = new ArrayList<>();

            for (AxStarCartLine cl : axCartLines) {
                final String clComment = cl.getComment();
                if (clComment.equals(cartItemCommentId)) {
                    //Add main item related line to removal.
                    lineIdsToRemove.add(cl.getLineId());
                } else if (relatedTopupCommentMap.get(clComment) != null) {
                    //Add top up item related line to removal.
                    lineIdsToRemove.add(cl.getLineId());
                }
            }

            final boolean noLinesToRemove = lineIdsToRemove.isEmpty();
            if (noLinesToRemove) {
                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
            }

            final AxStarInputRemoveCart cartToRemove = new AxStarInputRemoveCart();
            cartToRemove.setCartId(axCart.getId());
            cartToRemove.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
            cartToRemove.setLineIds(lineIdsToRemove);

            final AxStarServiceResult<AxStarCartNested> removeCartResult = axStarService.apiCartRemoveItem(channel, cartToRemove);
            if (!removeCartResult.isSuccess()) {
                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
            }

            savedCart.setAxCart(removeCartResult.getData().getCart());

            final List<FunCartItem> itamz = savedCart.getItems();
            itamz.remove(itemToRemove);

            for (FunCartItem relTopup : relatedTopupCommentMap.values()) {
                itamz.remove(relTopup);
            }
        } else {
            //TODO Decide whether to show error or not.
        }

        final FunCartDisplay funCartDisplay = cartAndDisplayService.constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public boolean cartClear(StoreApiChannels channel, String sessionId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return false;
        }

        final AxStarCart axCart = savedCart.getAxCart();
        final AxStarServiceResult<String> deleteCartResult = axStarService.apiCartDeleteEntireCart(channel, axCart.getId(),
                StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        if (!deleteCartResult.isSuccess()) {
            log.info("Error encountered deleting entire cart. " + JsonUtil.jsonify(deleteCartResult));
            //return false; TODO see if needed or not
        }

        cartManager.removeCart(channel, sessionId);

        return true;
    }

    @Override
    public CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session) {
        final FunCart funCart = cartManager.getCart(channel, session.getId());

        final FunCartDisplay cartVm;
        if (funCart == null) {
            cartVm = new FunCartDisplay();
            cartVm.setCartId(session.getId());
        } else {
            cartVm = cartAndDisplayService.constructCartDisplay(channel, session.getId());
        }

        final CheckoutDisplay checkout = new CheckoutDisplay();

        if (funCart != null) {
            if (funCart.getSavedCustomerDeets() != null) {
                checkout.setCustomer(funCart.getSavedCustomerDeets());
            }
        }

        checkout.setCartId(cartVm.getCartId());
        checkout.setTotalQty(cartVm.getTotalQty());
        checkout.setTotalMainQty(cartVm.getTotalMainQty());
        checkout.setCmsProducts(cartVm.getCmsProducts());
        checkout.setPayMethod(cartVm.getPayMethod());
        checkout.setPaymentTypeLabel("");
        if(StringUtils.isNotEmpty(checkout.getPayMethod())){
            checkout.setMessage("");
        }

        final boolean hasBookingFee = false;
        checkout.setHasBookingFee(hasBookingFee);
        checkout.setBookFeeMode("");

        BigDecimal grandTotal = cartVm.getTotal();

        //TODO remove this
        checkout.setWaivedTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        checkout.setTotal(grandTotal);
        checkout.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        List<Merchant> promoMerchants = productCmsService.getPromotionMerchants(channel.code, checkout);
        if (promoMerchants != null && promoMerchants.size() > 0) {
            checkout.setHasPromoMerchant(true);
            checkout.setPromoMerchants(promoMerchants);
        } else {
            checkout.setHasPromoMerchant(false);
        }

        return checkout;
    }

    @Override
    public ApiResult<StoreTransaction> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets, String locale) {

        if(MagnoliaConfigUtil.isAuthorInstance() && !MagnoliaConfigUtil.isDevMode()) {
            return new ApiResult<>(ApiErrorCodes.AuthorInstanceNotAllowedHere);
        }

        final String sessionId = session.getId();
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        final ApiResult<StoreTransaction> result = doCheckout(channel, savedCart, deets, locale);
        if (result.isSuccess()) {
            final StoreTransaction txn = result.getData();
            saveTransactionInSession(channel, session, txn);
            cartManager.removeCart(channel, sessionId); //Remove to prevent errors with AX handling of reused carts
        }

        return result;
    }

    @Transactional
    @Override
    public ApiResult<StoreTransaction> doCheckout(StoreApiChannels channel, FunCart cart, CustomerDetails deets, String locale) {
        //TODO Rollback transaction upon error (not working due to returning API Result instead of exception)

        //TODO make sure that the dev mode is set correctly on production!!!!
        if(MagnoliaConfigUtil.isAuthorInstance() && !MagnoliaConfigUtil.isDevMode()) {
            return new ApiResult<>(ApiErrorCodes.AuthorInstanceNotAllowedHere);
        }

        final Date now = new Date();
        final String receiptNumber = ProjectUtils.generateReceiptNumber(channel, now);

        final StoreTransaction txn = new StoreTransaction();
        txn.setChannel(channel.code);
        txn.setCreatedBy("SYSTEM");
        txn.setCreatedDate(now);
        txn.setModifiedBy("SYSTEM");
        txn.setModifiedDate(now);
        txn.setCurrency("SGD");
        txn.setCustDob(deets.getDob());
        txn.setCustEmail(deets.getEmail());
        txn.setCustMobile(deets.getMobile());
        txn.setCustName(deets.getName());
        txn.setCustNationality(deets.getNationality());
        txn.setCustReferSource(deets.getReferSource());
        txn.setCustSubscribe(deets.isSubscribed());
        txn.setErrorCode("");
        txn.setPaymentType(deets.getPaymentType());
        txn.setReceiptNumber(receiptNumber);
        txn.setStage(TransactionStage.Reserved.name());
        txn.setMgnlLocale(locale);

        final String defaultMerchantId = starfn.getDefaultMerchant(channel.code);
        //TODO Handle merchant ID switching for promotions
        if (StringUtils.isEmpty(deets.getMerchantId())) {
            txn.setTmMerchantId(defaultMerchantId);
        } else {
            txn.setTmMerchantId(deets.getMerchantId());
        }

        //TODO Handle affiliations saving inside items
        //TODO Handle promo code saving inside items (from shopping cart level)
        //TODO Double check other info needed to be saved inside the database.

        txn.setCustIdNo(deets.getIdNo());
        txn.setCustIdType(deets.getIdType());
        txn.setCustSalutation(deets.getSalutation());
        txn.setCustCompanyName(deets.getCompanyName());

        BigDecimal total = BigDecimal.ZERO;

        final List<AxStarCartLine> cartLines = cart.getAxCart().getCartLines();
        final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
        for (AxStarCartLine cl : cartLines) {
            final String lineCommentId = cl.getComment();
            List<AxStarCartLine> lineList = cartLineMap.get(lineCommentId);
            if (lineList == null) {
                lineList = new ArrayList<>();
                cartLineMap.put(lineCommentId, lineList);
            }
            lineList.add(cl);
        }

        boolean isMediaTypePin = false;

        final List<StoreTransactionItem> txnItems = new ArrayList<>();
        for (FunCartItem cartItem : cart.getItems()) {

            final StoreTransactionItem item = new StoreTransactionItem();
            item.setCmsProductId(cartItem.getCmsProductId());
            item.setCmsProductName(cartItem.getCmsProductName());
            item.setListingId(cartItem.getListingId());
            item.setProductCode(cartItem.getProductCode());
            item.setType(cartItem.getType());
            item.setName(cartItem.getName());
            item.setTopup(cartItem.isTopup());
            if (item.isTopup()) {
                item.setParentListingId(cartItem.getParentListingId());
                item.setParentCmsProductId(cartItem.getParentCmsProductId());
            }

            if (cartItem.isMediaTypePin()) {
                isMediaTypePin = true;
            }

            final boolean isEvent = StringUtils.isNotEmpty(cartItem.getEventGroupId());
            final boolean showEventDetails = StringUtils.isNotEmpty(cartItem.getSelectedEventDate());
            item.setDisplayDetails(
                    "Ticket Type: " + cartItem.getType() +
                            (isEvent && showEventDetails ? "<br>Event Session: " + cartItem.getEventSessionName() +
                                    "<br>Event Date: " + cartItem.getSelectedEventDate()
                                    : "")
            );

            item.setReceiptNumber(receiptNumber);
            item.setItemType(item.isTopup() ? TransItemType.Topup.name() : TransItemType.Standard.name());

            final List<AxStarCartLine> relatedLines = cartLineMap.get(cartItem.getAxLineCommentId());

            item.setEventGroupId(cartItem.getEventGroupId());
            item.setEventLineId(cartItem.getEventLineId());
            item.setSelectedEventDate(cartItem.getSelectedEventDate());
            item.setEventSessionName(cartItem.getEventSessionName());

            item.setTransactionItemId(cartItem.getCartItemId());

            BigDecimal itemUnitPrice = cartItem.getPrice();
            int itemQty = 0;
            BigDecimal itemTotalAmount = BigDecimal.ZERO;
            BigDecimal itemDiscountAmount = BigDecimal.ZERO;
            final List<AxStartCartLineDiscountLine> itemDiscountLines = new ArrayList<>();

            for (AxStarCartLine line : relatedLines) {
                final int lineQty = line.getQuantity();

                itemQty += lineQty;
                itemUnitPrice = line.getPrice(); //get from AX
                itemTotalAmount = itemTotalAmount.add(line.getTotalAmount());
                itemDiscountAmount = itemDiscountAmount.add(line.getDiscountAmount());

                for (AxStartCartLineDiscountLine discountLine : line.getDiscountLines()) {
                    itemDiscountLines.add(discountLine);
                }

            }

            if (itemQty != cartItem.getQty()) {
                itemQty = cartItem.getQty();
            }

            item.setQty(itemQty);
            item.setUnitPrice(itemUnitPrice);
            item.setSubtotal(itemTotalAmount);
            item.setDiscountTotal(itemDiscountAmount);
            item.setPromoCode(cartItem.getDiscountCode());
            item.setPromoCodeDiscountId(cartItem.getDiscountId());

            item.setOtherDetails(cartItem.getAxLineCommentId());

            StringBuilder sb = new StringBuilder("");
            Map<String, Boolean> appliedDiscountDisplayIds = new HashMap<>();
            for (AxStartCartLineDiscountLine discountLine : itemDiscountLines) {
                if (appliedDiscountDisplayIds.get(discountLine.getOfferId()) == null) {
                    appliedDiscountDisplayIds.put(discountLine.getOfferId(), true);
                    //TODO Must retrieve from promotion content in JCR
                    sb.append(discountLine.getOfferName() + "<br/>");
                }
            }
            if (sb.length() > 4) {
                sb.setLength(sb.length() - 4);
            }
            item.setDiscountLabel(sb.toString());

            total = total.add(item.getSubtotal());

            txnItems.add(item);
        }

        final String custNameCheat = txn.getCustName();
        final boolean generatePinCheat = custNameCheat.startsWith("pin");
        txn.setPinToGenerate(generatePinCheat || isMediaTypePin);

        txn.setAppliedPromoCodes(cart.getAppliedPromoCodes());
        txn.setPaymentModeAffiliation(cart.getPaymentModeAffiliation());
        txn.setBankAffiliation(cart.getBankAffiliation());

        txn.setItems(txnItems);
        txn.setTotalAmount(total);

        txn.setLanCode(locale.toUpperCase());
        txn.setIp(deets.getIp());

        bookingRepository.saveStoreTransaction(channel, txn);
        transService.saveStoreTransaction(channel, txn, cart);

        try {

            final Map<String, String> appliedPromoCodes = cart.getAppliedPromoCodes();
            for (Map.Entry<String, String> pcEntry : appliedPromoCodes.entrySet()) {
                final ApiResult<String> dcResult = this.axChannelTransactionService.apiUseDiscountCounter(
                        channel, pcEntry.getValue(), pcEntry.getKey(), receiptNumber, receiptNumber, 1, 0);
                if (!dcResult.isSuccess()) {
                    log.error("Error saving discount code usage.");
                    //TODO Proceeding for now, but need to do error handling.
                }
            }

            final AxStarCart axCart = cart.getAxCart();
            final Map<String, FunCartItem> axLineCommentMap = cart.getAxLineCommentMap();
            final List<AxRetailCartTicket> axRetailCartTickets = new ArrayList<>();

            for (AxStarCartLine cl : axCart.getCartLines()) {
                final AxRetailCartTicket ct = new AxRetailCartTicket();
                final String commentId = cl.getComment();
                final FunCartItem cartItem = axLineCommentMap.get(commentId);

                ct.setItemId(cl.getItemId());
                ct.setListingId(cl.getProductId());
                ct.setQty(cl.getQuantity());
                ct.setTransactionId(receiptNumber);
                ct.setLineId(cl.getLineId());
                ct.setUpdateCapacity(0);

                ct.setEventGroupId(cartItem.getEventGroupId());
                ct.setEventLineId(cartItem.getEventLineId());

                Date selectedEventDate = null;
                if (StringUtils.isNotEmpty(cartItem.getSelectedEventDate())) {
                    try {

                        selectedEventDate = NvxDateUtils.parseDate(cartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    } catch (ParseException e) {
                        //This should (almost) never happen.
                        log.error("Error parsing event date: " + cartItem.getSelectedEventDate(), e);
                    }
                }
                ct.setEventDate(selectedEventDate);

                axRetailCartTickets.add(ct);
            }

            axCart.setReservedTickets(axRetailCartTickets);

            //TODO Error handling must be done, bro.

            final boolean generatePin = txn.isPinToGenerate();

            if (generatePin) {
                log.info("Starting PIN generation logic.");
                final ApiResult<List<AxRetailTicketRecord>> axReserveResult =
                        axChannelTransactionService.apiCartValidateAndReserveQuantity(channel, receiptNumber, axRetailCartTickets, "");
                if (!axReserveResult.isSuccess()) {
                    log.error("Error encountered calling AX Retail service reserve and validate qty for transaction " + receiptNumber);
                    txn.setStage("Failed");
                    //TODO Error handling
                    return new ApiResult<>(false, ApiErrorCodes.General.code, axReserveResult.getMessage(), null);
                }
            } else {
                log.info("Starting eTicket generation logic.");
                final ApiResult<List<AxRetailTicketRecord>> axCheckoutResult =
                        axChannelTransactionService.apiB2CCartCheckoutStart(channel, receiptNumber, axRetailCartTickets);
                if (!axCheckoutResult.isSuccess()) {
                    log.error("Error encountered calling AX Retail service cart checkout start for transaction " + receiptNumber);
                    txn.setStage("Failed");
                    //TODO Error handling
                    return new ApiResult<>(false, ApiErrorCodes.General.code, axCheckoutResult.getMessage(), null);
                }
            }

            log.info("Doing AX Star Shopping Cart Checkout...");

            final AxStarServiceResult<AxStarCartNested> axStarCheckoutResult = axStarService.apiCheckout(channel,
                    axCart.getId(), axCart.getCustomerId(), receiptNumber);
            if (!axStarCheckoutResult.isSuccess()) {
                log.info("Error encountered on AxStarCheckout " + JsonUtil.jsonify(axStarCheckoutResult));
                //TODO Revert the AX API calls above.
                cartAndDisplayService.rebuildCart(channel, cart);
                txn.setStage("Failed");
                //TODO Error handling
                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartCheckout);
            }

            final AxStarCart checkoutCart = axStarCheckoutResult.getData().getCart();
            axCart.setCheckoutCart(checkoutCart);

        } catch (Exception e) {
            log.error("Error encountered calling AX Retail service for checkout.", e);
            txn.setStage("Failed");
            //TODO Error handling
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
        extDataForUpdate.setReceiptNumber(receiptNumber);
        extDataForUpdate.setCart(cart);
        transService.updateStoreTransactionExtensionData(channel, extDataForUpdate);

        return new ApiResult<>(true, "", "", txn);
    }

    @Transactional
    @Override
    public ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session) {
        final StoreTransaction txnInSession = getTransactionInSession(channel, session);
        if (txnInSession == null) {
            return new ApiResult<>(ApiErrorCodes.NoTransactionFoundInSession);
        }

        final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, txnInSession.getReceiptNumber());

        final StoreTransaction txn = extData.getTxn();
        if (txn == null) {
            return new ApiResult<>(ApiErrorCodes.NoTransactionFound);
        }

        if (StoreApiChannels.B2C_SLM.equals(channel)) {
            final B2CSLMStoreTransaction slmTxn = transService.getB2CSLMStoreTransaction(txn.getReceiptNumber());
            slmTxn.setStage(TransactionStage.Canceled.name());
            slmTxn.setModifiedDate(new Date());
            transService.saveB2CSLMStoreTransaction(slmTxn);
        } else if (StoreApiChannels.B2C_MFLG.equals(channel)) {
            final B2CMFLGStoreTransaction mflgTxn = transService.getB2CMFLGtoreTransaction(txn.getReceiptNumber());
            mflgTxn.setStage(TransactionStage.Canceled.name());
            mflgTxn.setModifiedDate(new Date());
            transService.saveB2CMFLGStoreTransaction(mflgTxn);
        }

        txn.setStage(TransactionStage.Canceled.name());
        //TODO Perform additional cancellation logic: release tickets, rebuild cart, API calls etc.

        try {

            StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
            extDataForUpdate.setReceiptNumber(txn.getReceiptNumber());
            extDataForUpdate.setTxn(txn);
            transService.updateStoreTransactionExtensionData(channel, extDataForUpdate);
        } catch (Exception e) {
            log.error("Error while trying to update store transaction.", e);
        }

        final FunCart savedCart = extData.getCart();
        cartAndDisplayService.rebuildCart(channel, savedCart);

        clearTransactionInSession(channel, session);

        return new ApiResult<>(true, "", "", "");
    }

    /**
     * Should deprecate this soon
     * @param channel
     * @param receiptNumber
     * @return
     */
    @Override
    public StoreTransaction getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber) {
        final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, receiptNumber);
        return extData.getTxn();
    }

    @Override
    public StoreTransaction getStoreTransactionBySession(StoreApiChannels channel, HttpSession session) {
        return getTransactionInSession(channel, session);
    }

    @Override
    public ReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session) {
        final StoreTransaction txn = getTransactionInSession(channel, session);
        if (txn == null) {
            return null;
        }

        return cartAndDisplayService.getReceiptForDisplay(txn);
    }

    @Transactional
    @Override
    public ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd) {
        //TODO This is simplified validation of checkout confirm. May need to add more complex validation.
        final StoreTransaction txn = getTransactionInSession(channel, session);
        if (txn == null) {
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final String stage = txn.getStage();

        if (TransactionStage.Success.name().equals(stage)) {
            log.error("Transaction " + txn.getReceiptNumber() + " has an invalid status at the confirm checkout stage: " + stage);
            return new ApiResult<>(ApiErrorCodes.TransactionSuccess);
        }
        if (!TransactionStage.Reserved.name().equals(stage)) {
            log.error("Transaction " + txn.getReceiptNumber() + " has an invalid status at the confirm checkout stage: " + stage);
            return new ApiResult<>(ApiErrorCodes.TransactionNotReserved);
        }

        final String tmStatus = txn.getTmStatus();

        if (StringUtils.isNotEmpty(tmStatus)) {
            //TODO Handle pa more
            log.error("Transaction " + txn.getReceiptNumber() + " has already proceeded with payment or been processed: " + tmStatus);
            return new ApiResult<>(ApiErrorCodes.TransactionNotReserved);
        }

        if (StoreApiChannels.B2C_SLM.equals(channel)) {
            final B2CSLMStoreTransaction slmTxn = transService.getB2CSLMStoreTransaction(txn.getReceiptNumber());
            slmTxn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
            slmTxn.setModifiedDate(new Date());
            transService.saveB2CSLMStoreTransaction(slmTxn);
        } else if (StoreApiChannels.B2C_MFLG.equals(channel)) {
            final B2CMFLGStoreTransaction mflgTxn = transService.getB2CMFLGtoreTransaction(txn.getReceiptNumber());
            mflgTxn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
            mflgTxn.setModifiedDate(new Date());
            transService.saveB2CMFLGStoreTransaction(mflgTxn);
        }

        txn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());

        final StoreTransactionExtensionData extDataForUpdate = new StoreTransactionExtensionData();
        extDataForUpdate.setReceiptNumber(txn.getReceiptNumber());
        extDataForUpdate.setTxn(txn);
        transService.updateStoreTransactionExtensionData(channel, extDataForUpdate);

        final CheckoutConfirmResult ccr = new CheckoutConfirmResult();
        final String custName = txn.getCustName();


        String enableTMService =  sysParamRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_TM_SERVICE_KEY);
        if(enableTMService != null && "false".equals(enableTMService)) {
            ccr.setTmRedirect(false);
        }else {
            ccr.setTmRedirect(true);
        }

        if (ccr.isTmRedirect()) {
            final String receiptNumber = txn.getReceiptNumber();
            final BigDecimal totalAmount = txn.getTotalAmount();
            final String merchantId = txn.getTmMerchantId();
            final TmCurrency tmCurrency = TmCurrency.SGD;
            final TmPaymentType tmPaymentType = TmPaymentType.fromPaymentModeString(txn.getPaymentType());

            final TmFraudCheckPackage fraudCheckPackage = new TmFraudCheckPackage();
            fraudCheckPackage.setFirstName(custName);
            fraudCheckPackage.setEmail(txn.getCustEmail());
            fraudCheckPackage.setIp(trackd.getIp()); //TODO Get IP should be getting from the transaction IP instead of this.
            fraudCheckPackage.setEncoding(NvxUtil.DEFAULT_ENCODING);

            final TmLocale tmLocale = TmLocale.EnglishUs; //TODO apply i18n

            final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
            merchantSignatureConfig.setSignatureEnabled(false);

            final TelemoneyParamPackage tmParams;
            try {
                tmParams = tmParamRepository.getTmParams(channel);
            } catch (JcrRepositoryException e) {
                //TODO Additional handling
                log.error("Error retrieving telemoney parameters", e);
                return new ApiResult<>(ApiErrorCodes.General);
            }

            String returnUrl = tmParams.getTmReturnUrl();
            String statusUrl = tmParams.getTmStatusUrl();
            String returnUrlParamString = "";

            //TODO Namespace for i18n purposes

            final TmPaymentRedirectInput input = new TmPaymentRedirectInput(returnUrl, statusUrl, returnUrlParamString, receiptNumber,
                    totalAmount, merchantId, tmPaymentType, tmCurrency, fraudCheckPackage, tmLocale, merchantSignatureConfig);
            input.setUserField1(channel.code);
            input.setUserField3(session.getId());

            final String redirectUrl = tmServiceProvider.constructPaymentRedirectUrl(input);

            ccr.setRedirectUrl(redirectUrl);

        } else {
            final TelemoneyResponse r = new TelemoneyResponse();
            r.setTmTransType(TmTransType.Sale.tmCode);
            r.setTmRefNo(txn.getReceiptNumber());
            r.setTmUserField1(channel.code);
            r.setTmUserField3(session.getId());
            r.setTmStatus(TmStatus.Success.tmCode);
            r.setTmDebitAmt(txn.getTotalAmount());
            r.setTmCcNum("411111xxxxxx1111");
            r.setTmApprovalCode("skiptm"); //I use this value to check whether to send email in my processor class.

            final TelemoneyB2CSaleProcessor saleProcessor = tmB2CSaleProcessorFactory.getObject();
            saleProcessor.process(r);

            ccr.setRedirectUrl("receipt");
            ccr.setTmRedirect(false);
        }

        return new ApiResult<>(true, "", "", ccr);
    }

    @Override
    public ApiResult<StoreTransaction> getFinalisedTransaction(StoreApiChannels channel, HttpSession session) {
        //TODO This is simplified validation of sale completion. May need to add more complex validation.
        final StoreTransaction txnFromSession = getTransactionInSession(channel, session);
        if (txnFromSession == null) {
            //TODO
            log.error("Transaction is unavailable, session may already be timed out.");
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, txnFromSession.getReceiptNumber());


        final StoreTransaction txn = extData.getTxn();
        if (txn == null) {
            //TODO
            log.error("Transaction cannot be found in the database. " + txnFromSession.getReceiptNumber());
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final String stage = txn.getStage();
        final String tmStatus = txn.getTmStatus();
        if (TransactionStage.Reserved.name().equals(stage) &&
                (EnovaxTmSystemStatus.RedirectedToTm.name().equals(tmStatus) || EnovaxTmSystemStatus.TmQuerySent.name().equals(tmStatus))) {
            //TODO
            log.warn("Transaction is at the payment side, but still pending finalisation.");
            return new ApiResult<>(true, "", "", null);
        }

        if (TransactionStage.Canceled.name().equals(stage) || TransactionStage.Failed.name().equals(stage)) {
            //TODO Give more detailed error (maybe put in error message)
            cartAndDisplayService.rebuildCart(channel, extData.getCart()); //rebuild cart
            return new ApiResult<>(ApiErrorCodes.PaymentFailed);
        }

        if (TransactionStage.Success.name().equals(stage)) {
            String cartId = extData != null? (extData.getCart() != null? extData.getCart().getCartId() : null): null;
            if(cartId == null) {
                cartId = session.getId();
            }
            cartManager.removeCart(channel, cartId);
            return new ApiResult<>(true, "", "", txn);
        }

        log.error("Error resolving transaction state. " + JsonUtil.jsonify(txn));
        return new ApiResult<>(ApiErrorCodes.General);
    }
}
