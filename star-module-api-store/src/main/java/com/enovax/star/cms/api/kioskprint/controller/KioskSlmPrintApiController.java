package com.enovax.star.cms.api.kioskprint.controller;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cornelius on 24/6/16.
 */

@RestController
@RequestMapping("/kiosk-slm/")
public class KioskSlmPrintApiController extends PrintApiController {

    protected StoreApiChannels getChannel() {
        return StoreApiChannels.KIOSK_SLM;
    }
}