package com.enovax.star.cms.api.store.service.batch.ppmflg;

import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerInventoryService;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerPkgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by jennylynsze on 10/27/16.
 */
@Service("PPMFLGInventoryTransactionStatusUpdateService")
public class InventoryTransactionStatusUpdateService {

    @Autowired
    @Qualifier("PPMFLGIPartnerInventoryService")
    IPartnerInventoryService inventoryService;

    @Autowired
    @Qualifier("PPMFLGIPartnerPkgService")
    IPartnerPkgService pkgService;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    //Copied from TransStatusTask
    //once every 12:01 to update the status
    @Scheduled(cron = "${ppmflg.inventory.status.update.job}")
    public void checkStatus(){
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    log.info("=== Starting Update Expired PPMFLG Transactions...");
                    inventoryService.updateExpiredStatus();
                    log.info("=== Update Expired PPMFLG Transactions End...");
                    log.info("=== Starting Update Forfeited PPMFLG Transactions...");
                    inventoryService.updateForfeitedStatus();
                    log.info("=== Update Forfeited PPMFLG Transactions End...");

                    log.info("=== Starting Update Expired PPMFLG Packages...");
                    pkgService.updateExpiredStatus();
                    log.info("=== Update Expired PPMFLG Packages End...");
                }
            });
        }
    }

}
