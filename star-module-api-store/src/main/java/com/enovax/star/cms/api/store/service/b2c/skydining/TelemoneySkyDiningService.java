package com.enovax.star.cms.api.store.service.b2c.skydining;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.service.TmServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jace on 15/11/16.
 */
@SuppressWarnings("Duplicates")
@Service
public class TelemoneySkyDiningService {

    private Logger log = LoggerFactory.getLogger(TelemoneySkyDiningService.class);

    @Autowired
    private TmServiceProvider tmServiceProvider;

    @Autowired
    private ObjectFactory<TelemoneySkyDiningSaleProcessor> tmSdSaleProcessorFactory;

    public void processMerchantPost(TelemoneyResponse tmResponse) {
        final TelemoneySkyDiningSaleProcessor tmProcessorSd = tmSdSaleProcessorFactory.getObject();
        tmProcessorSd.process(tmResponse);
    }
}
