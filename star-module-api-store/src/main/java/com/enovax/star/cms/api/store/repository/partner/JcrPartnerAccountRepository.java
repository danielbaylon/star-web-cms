package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.Iterator;

/**
 * Created by jennylynsze on 5/18/16.
 */
@Repository
public class JcrPartnerAccountRepository implements IPartnerAccountRepository {

    @Override
    public boolean hasUsername(String username) {
        String query =  "//element(*, mgnl:content)[@username='" + username + "']";
        try {
            Iterable<Node> userAccount = JcrRepository.query(JcrWorkspace.PartnerAccounts.getWorkspaceName(), "mgnl:content", query);
            Iterator<Node> userAccountIterator = userAccount.iterator();

            if(userAccountIterator.hasNext()) {
                return true;
            }

            return false;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public PartnerAccount getUserInfo(String username, String password) {
        String query =  "//element(*, mgnl:content)[@username='" + username + "' and @password='" + password + "']";
        try {
            Iterable<Node> userAccount = JcrRepository.query(JcrWorkspace.PartnerAccounts.getWorkspaceName(), "mgnl:content", query);
            Iterator<Node> userAccountIterator = userAccount.iterator();

            if(userAccountIterator.hasNext()) {
                Node accountNode = userAccountIterator.next();
                PartnerAccount account = new PartnerAccount();
                account.setId(Integer.parseInt(accountNode.getProperty("id").getString()));
                account.setUsername(username);
                account.setPassword(password);
                account.setAccountCode(accountNode.getProperty("accountCode").getString());

                //query for Partner
                String partnerQuery = "//element(*,mgnl:content)[@mainAccountId = '" + account.getId() + "']";
                Iterable<Node> partnerIterable = JcrRepository.query(JcrWorkspace.Partners.getWorkspaceName(), "mgnl:content", partnerQuery);
                Iterator<Node> partnerIterator = partnerIterable.iterator();

                if(partnerIterator.hasNext()) {
                    Node partnerNode = partnerIterator.next();
                    PartnerVM partner = new PartnerVM();
                    partner.setId(Integer.parseInt(partnerNode.getProperty("id").getString()));
                    partner.setAccountCode(partnerNode.getProperty("accountCode").getString());
                    partner.setOrgName(partnerNode.getProperty("orgName").getString());
                    partner.setAddress(partnerNode.getProperty("address").getString());
                    partner.setAdminId(account.getId());

                    account.setPartner(partner);
                }

                return account;
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public PartnerAccount getUserInfo(Integer id) {
        try {
           Node accountNode =  JcrRepository.getParentNode(JcrWorkspace.PartnerAccounts.getWorkspaceName(), "/" + id);

            PartnerAccount account = new PartnerAccount();
            account.setId(Integer.parseInt(accountNode.getProperty("id").getString()));
            account.setUsername(accountNode.getProperty("username").getString());
            account.setAccountCode(accountNode.getProperty("accountCode").getString());

            //query for Partner
            String partnerQuery = "//element(*,mgnl:content)[@mainAccountId = '" + account.getId() + "']";
            Iterable<Node> partnerIterable = JcrRepository.query(JcrWorkspace.Partners.getWorkspaceName(), "mgnl:content", partnerQuery);
            Iterator<Node> partnerIterator = partnerIterable.iterator();

            if(partnerIterator.hasNext()) {
                Node partnerNode = partnerIterator.next();
                PartnerVM partner = new PartnerVM();
                partner.setId(Integer.parseInt(partnerNode.getProperty("id").getString()));
                partner.setOrgName(partnerNode.getProperty("orgName").getString());
                partner.setAddress(partnerNode.getProperty("address").getString());
                partner.setAdminId(account.getId());

                account.setPartner(partner);
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }
}
