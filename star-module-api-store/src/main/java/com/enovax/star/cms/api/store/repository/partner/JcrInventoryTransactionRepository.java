package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 5/18/16.
 */
@Repository
public class JcrInventoryTransactionRepository implements IInventoryTransactionRepository {

    @Override
    public InventoryTransactionVM getTransByUser(Integer transId, Integer mainAccountId) {

        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/" + transId);
            InventoryTransactionVM transactionVM  = JsonUtil.fromJson(node.getProperty("data").getString(), InventoryTransactionVM.class);

            //make sure that the login user have access
            if(transactionVM.getMainAccountId() == mainAccountId) {
                return transactionVM;
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }
}
