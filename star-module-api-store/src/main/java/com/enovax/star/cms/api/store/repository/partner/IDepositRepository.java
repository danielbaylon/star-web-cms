package com.enovax.star.cms.api.store.repository.partner;


import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.model.partner.ppslm.DepositVM;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jennylynsze on 6/24/16.
 */
public interface IDepositRepository {

    BigDecimal getBalance(Integer adminId);

    void updateBalance(Integer adminId, BigDecimal depositChanges);

    void save(DepositVM depositVM);
    Integer getNextId();
    List<DepositVM> getDepositHistory(Integer adminId,
                                           String fromDateStr,
                                           String toDateStr,
                                           String orderBy,
                                           String orderWith,
                                           Integer page, Integer pagesize);
    int getDepositHistorySize(Integer adminId,
                            String fromDateStr,
                            String toDateStr);

}
