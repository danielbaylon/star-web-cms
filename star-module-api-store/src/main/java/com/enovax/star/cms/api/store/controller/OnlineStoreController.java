package com.enovax.star.cms.api.store.controller;

import com.enovax.star.cms.api.store.batch.b2c.B2CBookingCleanupJob;
import com.enovax.star.cms.api.store.service.b2c.search.IQuickSearch;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.api.store.service.b2c.IBookingService;
import com.enovax.star.cms.api.store.service.product.IProductCmsService;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.b2cshared.service.IB2CTransactionService;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.promotions.ApplyAffiliationRequest;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeRequest;
import com.enovax.star.cms.commons.model.booking.promotions.PromoCodeResult;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.ProductExtViewModelWrapper;
import com.enovax.star.cms.commons.model.product.ProductItemCriteria;
import com.enovax.star.cms.commons.model.product.ProductItemCriteriaWrapper;
import com.enovax.star.cms.commons.model.search.QuickSearchProductGraph;
import com.enovax.star.cms.commons.model.search.QuickSearchRequest;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.model.ticketgen.KioskTicketsPackage;
import com.enovax.star.cms.commons.model.ticketgen.TicketXmlRecord;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/store/")
@SuppressWarnings("Duplicates")
public class OnlineStoreController extends BaseStoreApiController {

  @Autowired
  private HttpSession session;
  @Autowired
  private HttpServletRequest request;

  @Autowired
  private IB2CTransactionService transService;
  @Autowired
  private IB2CCartAndDisplayService cartAndDisplayService;
  @Autowired
  private IBookingService bookingService;
  @Autowired
  private ITicketGenerationService ticketGenerationService;
  @Autowired
  private IProductService productService;
  @Autowired
  private IProductCmsService productCmsService;
  @Autowired
  private IQuickSearch quickSearch;

  @Autowired
  private StarTemplatingFunctions starfn;


  @Autowired
  private B2CBookingCleanupJob job;

  @RequestMapping(value = "test", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<String>> test() {
    log.info("Entered test...");
    try {
      this.job.performB2CSLMTransactionQuery();
      return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [test] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }



  //TODO i18n functionality

  @RequestMapping(value = "products-ext", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiGetProductsExt(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody ProductItemCriteriaWrapper data) {
    log.info("Entered apiGetProductsExt...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final List<String> productIds = new ArrayList<>();
      for (ProductItemCriteria crit : data.getProducts()) {
        productIds.add(crit.getProductId());
      }

      final ApiResult<List<ProductExtViewModel>> result = productService.getDataForProducts(channel, productIds);
      if (result.isSuccess()) {
        return new ResponseEntity<>(new ApiResult<>(true, "", "", new ProductExtViewModelWrapper(result.getData())), HttpStatus.OK);
      }

      return new ResponseEntity<>(new ApiResult<>(false, result.getErrorCode(), result.getMessage(), null), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGetProductsExt] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, ProductExtViewModelWrapper.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "add-to-cart", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<FunCartDisplay>> apiAddToCart(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody FunCart cart) {
    log.info("Entered apiAddToCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      log.info(JsonUtil.jsonify(trackr.buildTrackd(sessionId, request)));

      final ApiResult<FunCartDisplay> result = bookingService.cartAdd(channel, cart, sessionId);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiAddToCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "update-cart", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<FunCartDisplay>> apiUpdateCart(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody FunCart cart) {
    log.info("Entered apiUpdateCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final ApiResult<FunCartDisplay> result = bookingService.cartUpdate(channel, cart, sessionId);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiUpdateCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "remove-item-from-cart/{id}", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<FunCartDisplay>> apiRemoveItemFromCart(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @PathVariable("id") String id) {
    log.info("Entered apiRemoveItemFromCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final ApiResult<FunCartDisplay> result = bookingService.cartRemoveItem(channel, sessionId, id);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiRemoveItemFromCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "add-topup-to-cart", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<FunCartDisplay>> apiAddTopupToCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                                     @RequestBody FunCart cart) {
    log.info("Entered apiAddTopupToCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final ApiResult<FunCartDisplay> result = bookingService.cartAddTopup(channel, sessionId, cart);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiAddTopupToCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "apply-promo-code", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<String>> apiApplyPromoCode(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody String promoCode) {
    log.info("Entered apiApplyPromoCode...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final ApiResult<String> result = bookingService.cartApplyPromoCode(channel, sessionId, promoCode);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiApplyPromoCode] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "update-affiliation", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<String>> apiUpdateAffiliation(
          @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
          @RequestBody ApplyAffiliationRequest applyAffiliationRequest) {
    log.info("Entered apiUpdateAffiliation...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final ApiResult<String> result = bookingService.cartApplyAffiliation(channel, sessionId, applyAffiliationRequest);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiUpdateAffiliation] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "check-promo-code", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<PromoCodeResult>> apiCheckPromoCode(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody PromoCodeRequest data) {
    log.info("Entered apiCheckPromoCode...");
    try {
      final ApiResult<PromoCodeResult> result = productCmsService.checkPromoCode(data, channelCode);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiCheckPromoCode] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, PromoCodeResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "search-promo-code", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<PromoCodeResult>> apiSearchPromoCode(
      @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
      @RequestBody PromoCodeRequest data) {
    log.info("Entered apiSearchPromoCode...");
    try {
      final ApiResult<PromoCodeResult> result = productCmsService.searchPromoCode(data, channelCode);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiSearchPromoCode] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, PromoCodeResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "quick-search", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<QuickSearchProductGraph>> apiQuickSearch(
      @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
      @RequestHeader(StoreApiConstants.REQUEST_HEADER_LOCALE) String locale,
      @RequestBody QuickSearchRequest data) {
    log.info("Entered apiQuickSearch...");
    try {
      final QuickSearchProductGraph result = quickSearch.doSearch(data.getQry(), locale);

      return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiQuickSearch] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, QuickSearchProductGraph.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "clear-cart", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<String>> apiClearCart(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
    log.info("Entered apiClearCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      bookingService.cartClear(channel, sessionId);

      return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiClearCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-cart", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<FunCartDisplay>> apiGetCart(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
    log.info("Entered apiGetCart...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
      final String sessionId = session.getId();

      final FunCartDisplay funCartDisplay = cartAndDisplayService.constructCartDisplay(channel, sessionId);

      return new ResponseEntity<>(new ApiResult<>(true, "", "", funCartDisplay), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGetCart] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-cart-by-receipt-number", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<StoreTransaction>> apiGetCartByReceiptNumber(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestBody String receiptNumber) {

    //TODO Need to return a proper transaction formatted for display, or a new API should return this
    log.info("Entered apiGetCartByReceiptNumber ...");

    try {
      StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      StoreTransaction storeTransaction = bookingService.getStoreTransactionByReceipt(channel, receiptNumber);

      if (storeTransaction == null) {
        return new ResponseEntity<>(new ApiResult<>(ApiErrorCodes.NoKioskTransactionFound, "", ""), HttpStatus.OK);
      }

      return new ResponseEntity<>(new ApiResult<StoreTransaction>(true, "", "", storeTransaction), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGetCartByReceiptNumber] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, StoreTransaction.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-checkout-display", method = {RequestMethod.GET, RequestMethod.POST})
  public ResponseEntity<ApiResult<CheckoutDisplay>> apiGetCheckoutDisplay(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
    log.info("Entered apiGetCheckoutDisplay...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final CheckoutDisplay checkoutDisplay = bookingService.constructCheckoutDisplayFromCart(channel, session);

      return new ResponseEntity<>(new ApiResult<>(true, "", "", checkoutDisplay), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGetCheckoutDisplay] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, CheckoutDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "checkout", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<StoreTransaction>> apiCheckout(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_LOCALE) String locale,
    @RequestBody CustomerDetails deets) {
    log.info("Entered apiCheckout...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final ApiResult<StoreTransaction> result = bookingService.doCheckout(channel, session, deets, locale);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiCheckout] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, StoreTransaction.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "checkout-cancel", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<String>> apiCheckoutCancel(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
    log.info("Entered apiCheckoutCancel...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final ApiResult<String> result = bookingService.cancelCheckout(channel, session);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiCheckoutCancel] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "checkout-confirm", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<CheckoutConfirmResult>> apiCheckoutConfirm(
          @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
      log.info("Entered apiCheckoutConfirm...");
      try {

          final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
          final Trackd trackd = trackr.buildTrackd(session.getId(), request);

          final ApiResult<CheckoutConfirmResult> result = bookingService.checkoutConfirm(channel, session, trackd);

          return new ResponseEntity<>(result, HttpStatus.OK);
      } catch (Exception e) {
          log.error("!!! System exception encountered [apiCheckoutConfirm] !!!");
          return new ResponseEntity<>(handleUncaughtException(e, CheckoutConfirmResult.class, ""), HttpStatus.OK);
      }
  }

  @RequestMapping(value = "checkout-complete-sale", method = {RequestMethod.POST})
  @Deprecated
  public ResponseEntity<ApiResult<StoreTransaction>> apiCompleteSale(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
    log.info("Entered apiCompleteSale...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

//      final ApiResult<StoreTransaction> result = bookingService.completeSale(channel, session);
      final ApiResult<StoreTransaction> result = new ApiResult<>(ApiErrorCodes.General);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiCompleteSale] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, StoreTransaction.class, ""), HttpStatus.OK);
    }
  }

    @RequestMapping(value = "get-finalised-receipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptDisplay>> apiGetFinalisedTransaction(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_LOCALE) String locale) {
        log.info("Entered apiGetFinalisedTransaction...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<StoreTransaction> result = bookingService.getFinalisedTransaction(channel, session);
            if (!result.isSuccess()) {
                log.info("Transaction failed..."); //TODO
                return new ResponseEntity<>(new ApiResult<>(result.getApiErrorCode()), HttpStatus.OK);
            }

            if (result.isSuccess()) {
                if (result.getData() == null) {
                    log.info("Transaction waiting..."); //TODO
                    return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
                }
            }

            final ReceiptDisplay receipt = cartAndDisplayService.getReceiptForDisplayByReceipt(channel, result.getData().getReceiptNumber());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetFinalisedTransaction] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ReceiptDisplay.class, ""), HttpStatus.OK);
        }
    }

  @RequestMapping(value = "get-receipt", method = {RequestMethod.POST})
  @Deprecated
  public ResponseEntity<ApiResult<ReceiptDisplay>> apiGetReceipt(
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
    @RequestHeader(StoreApiConstants.REQUEST_HEADER_LOCALE) String locale) {
    log.info("Entered apiGetReceipt...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final ReceiptDisplay receipt = bookingService.getReceiptForDisplayBySession(channel, session);

      if (receipt == null) {
        return new ResponseEntity<>(new ApiResult<>(false, "", "No transaction found", null), HttpStatus.OK);
      }

        List<TncVM> generalTncs = starfn.getGeneralTermsAndConditions(channelCode,locale);
        if(generalTncs!=null && !generalTncs.isEmpty()){
            receipt.getTncs().addAll(generalTncs);
            receipt.setHasTnc(true);
        }
        List<String> productIds = new ArrayList<>();
        for(FunCartDisplayCmsProduct product : receipt.getCmsProducts())  {
            productIds.add(product.getId());
        }
        List<TncVM> productTncs = starfn.getProductTermsAndConditions(channelCode,productIds,locale);
        if(productTncs!=null && !productTncs.isEmpty()) {
            receipt.getTncs().addAll(productTncs);
            receipt.setHasTnc(true);
        }

      return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGetReceipt] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, ReceiptDisplay.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "generate-paper-tickets-package/{channel}", method = {RequestMethod.GET, RequestMethod.POST})
  @ResponseBody
  public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperTicketsPackage(@PathVariable("channel") String channelCode) {
    log.info("Entered apiGeneratePaperTicketsPackage...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final StoreTransaction storeTransaction = bookingService.getStoreTransactionBySession(channel, session);

      final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
      ticketDataCompiled.setTickets(storeTransaction.geteTickets());

      final Map<String, String> ticketXmlMap = ticketGenerationService.generatePaperTicketXml(ticketDataCompiled, "", "");

      final KioskTicketsPackage pkg = new KioskTicketsPackage();
      final List<TicketXmlRecord> ticketXmls = new ArrayList<>();
      for (Map.Entry<String, String> entry : ticketXmlMap.entrySet()) {
        final TicketXmlRecord rec = new TicketXmlRecord();
        rec.setId(entry.getKey());
        rec.setTemplateXml(entry.getValue());
        ticketXmls.add(rec);
      }
      pkg.setTicketXmls(ticketXmls);

      pkg.setReceiptXml(ticketGenerationService.generateReceiptXml(storeTransaction, ""));

      return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGeneratePaperTicketsPackage] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
    }
  }

  public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperReceiptWithQRCodePackage(String channelCode, String qrCode) {

    log.info("Entered apiGeneratePaperReceiptWithQRCodePackage...");

    try {
      StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      StoreTransaction storeTransaction = bookingService.getStoreTransactionBySession(channel, session);

      KioskTicketsPackage pkg = new KioskTicketsPackage();

      pkg.setReceiptXml(ticketGenerationService.generateReceiptXmlWithQRCode(storeTransaction, "", qrCode));

      return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGeneratePaperReceiptWithQRCodePackage] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "generate-ticket/{channel}", method = {RequestMethod.GET, RequestMethod.POST})
  @ResponseBody
  public ResponseEntity<ApiResult<String>> apiGenerateTicket(@PathVariable("channel") String channelCode,
                                                             final HttpServletResponse response) {
    log.info("Entered apiGenerateTicket...");
    try {

      final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

      final StoreTransaction sessionTxn = bookingService.getStoreTransactionBySession(channel, session);
      final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, sessionTxn.getReceiptNumber());
      final StoreTransaction storeTransaction = extData.getTxn();

      final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
      ticketDataCompiled.setTickets(storeTransaction.geteTickets());

      final byte[] outputBytes = ticketGenerationService.generateTickets(channel, ticketDataCompiled);

      final String fileName = "etickets.pdf";
      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
      response.setHeader("X-Frame-Options", "SAMEORIGIN");

      ServletOutputStream output = response.getOutputStream();
      output.write(outputBytes);
      output.flush();

      return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGenerateTicket] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "generate-ticket-sample", method = {RequestMethod.GET, RequestMethod.POST})
  @ResponseBody
  public ResponseEntity<ApiResult<String>> apiGenerateTicketSample(final HttpServletResponse response) {
    log.info("Entered apiGenerateTicketSample...");
    try {

      final List<ETicketData> tickets = new ArrayList<>();

      ETicketData td = new ETicketData();
      td.setBarcodeBase64(BarcodeGenerator.toBase64("helloworld", 250, 75));
      td.setDisplayName("Sample Name");
      td.setTicketNumber(ProjectUtils.generateReceiptNumber(new Date()));
      td.setTicketPersonType("Adult");
      td.setTotalPrice("S$ 10.00");
      td.setValidityStartDate("29/03/2016");
      td.setValidityEndDate("29/03/2016");
      tickets.add(td);

      td = new ETicketData();
      td.setBarcodeBase64(QRCodeGenerator.toBase64("helloworld qr", 200, 200));
      td.setDisplayName("Sample Name QR");
      td.setTicketNumber(ProjectUtils.generateReceiptNumber(new Date()));
      td.setTicketPersonType("Adult");
      td.setTotalPrice("S$ 10.00");
      td.setValidityStartDate("29/03/2016");
      td.setValidityEndDate("29/03/2016");
      tickets.add(td);

      final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
      ticketDataCompiled.setTickets(tickets);

      final byte[] outputBytes = ticketGenerationService.generateTickets(StoreApiChannels.B2C_SLM, ticketDataCompiled);

      final String fileName = "sentosa-tickets.pdf";
      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
      response.setHeader("X-Frame-Options", "SAMEORIGIN");

      ServletOutputStream output = response.getOutputStream();
      output.write(outputBytes);
      output.flush();

      return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
    } catch (Exception e) {
      log.error("!!! System exception encountered [apiGenerateTicketSample] !!!");
      return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
    }
  }

}