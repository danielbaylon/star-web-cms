package com.enovax.star.cms.api.store.controller.ppslm.secured;


import com.enovax.star.cms.api.store.controller.ppslm.PPSLMBaseStoreApiController;
import com.enovax.star.cms.api.store.service.ppslm.IPartnerBookingOfflinePayService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.tracking.Trackr;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(PartnerPortalConst.ROOT_SECURED + "/store-partner-offlinePay/")
public class PartnerOnlineStoreOfflinePayController extends PPSLMBaseStoreApiController {

    @Autowired
    protected Trackr trackr;

    @Autowired
    private IPartnerBookingOfflinePayService bookingService;

    private static final String CURRENT_OFFLINE_PAYMENT_REQUEST_ID = "CURRENT_OFFLINE_PAYMENT_REQUEST_ID";

    @RequestMapping(value = "checkout-offline", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<InventoryTransactionVM>> apiCheckout(HttpServletRequest request,
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody CustomerDetails deets) {
        log.info("Entered apiCheckout...");
        try {
            HttpSession session = request.getSession(false);
            session.removeAttribute(CURRENT_OFFLINE_PAYMENT_REQUEST_ID);
            final ApiResult<InventoryTransactionVM> loggedInResult = isLoggedInWrappedResult(session, InventoryTransactionVM.class);
            if (!loggedInResult.isSuccess()) {
                return new ResponseEntity<>(loggedInResult, HttpStatus.OK);
            }
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);

            final Trackd trackd = trackr.buildTrackd(session.getId(), request);
            String ip = "";
            if(trackd != null) {
                ip = trackd.getIp();
            }
            final ApiResult<InventoryTransactionVM> result = bookingService.doOfflineCheckout(channel, session, account, ip);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, InventoryTransactionVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-request", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<Integer>> apiViewOfflineRequest(HttpServletRequest request, @RequestParam("id")Integer id) {
        log.info("Entered apiViewOfflineRequest...");
        try {
            if(id != null && id.intValue() > 0){
                HttpSession session = request.getSession(false);
                session.removeAttribute(CURRENT_OFFLINE_PAYMENT_REQUEST_ID);
                session.setAttribute(CURRENT_OFFLINE_PAYMENT_REQUEST_ID, id);
                return new ResponseEntity<>(new ApiResult<>(true,null,null,null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(false,null,"Invalid Request",null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, Integer.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-offline-request", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<Integer>> apiSaveOfflineRequest(HttpServletRequest request, @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCheckout...");
        try {
            HttpSession session = request.getSession(false);
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            final ApiResult<Integer> result = bookingService.saveOfflineRequest(channel, session, account);
            if(result != null && result.isSuccess()){
                bookingService.cleanShoppingCart(channel, session.getId());
            }
            session.setAttribute(CURRENT_OFFLINE_PAYMENT_REQUEST_ID, result.getData());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, Integer.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-cart-display", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<OfflinePaymentResultVM>> apiGetOfflineCartDisplay(HttpServletRequest request, @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        try {
            HttpSession session = request.getSession(false);
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            Integer reqId = (Integer)session.getAttribute(CURRENT_OFFLINE_PAYMENT_REQUEST_ID);
            if(reqId == null || reqId.intValue() == 0){
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Request", null), HttpStatus.OK);
            }
            ApiResult<OfflinePaymentResultVM> apiResult = bookingService.getOfflinePaymentRequestDetailsById(channel, account, reqId);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("!!! System exception encountered [apiGetOfflineCartDisplay] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, OfflinePaymentResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "withdraw-offline-request", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<OfflinePaymentResultVM>> apiWithdrawOfflineRequest(HttpServletRequest request,
                                                                    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                                    @RequestBody OfflinePaymentRequestVM input) {
        try {
            HttpSession session = request.getSession(false);
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            final ApiResult<OfflinePaymentResultVM> result = bookingService.withdrawOfflineRequest(channel, account, input);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiWithdrawOfflineRequest] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, OfflinePaymentResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "send-partner-msg", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSendPartnerMsg(HttpServletRequest request,
                                                                                       @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                                                       @RequestBody OfflinePaymentRequestVM input) {
        try {
            HttpSession session = request.getSession(false);
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            final ApiResult<String> result = bookingService.notifyApprovers(channel, account, input);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiWithdrawOfflineRequest] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "submit-request", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<OfflinePaymentResultVM>> apiSubmitOfflinePaymentRequest(HttpServletRequest request,
                                                               @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                               @RequestBody OfflinePaymentRequestVM input) {
        try {
            HttpSession session = request.getSession(false);
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            PartnerAccount account = getLoginUserAccount(session);
            final ApiResult<OfflinePaymentResultVM> result = bookingService.submitOfflinePaymentRequest(channel, account, input);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("!!! System exception encountered [apiSubmitOfflinePaymentRequest] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, OfflinePaymentResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "list-request", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchOfflinePayment(HttpServletRequest request,
            @RequestParam(value="receiptNum", required = false) String receiptNum,
            @RequestParam(value="startDtStr", required = false) String startDateStr,
            @RequestParam(value="endDtStr", required = false) String endDateStr,
            @RequestParam(value="status", required = false) String status,
            @RequestParam(value="username", required = false) String username,
            @RequestParam("take") int take,
            @RequestParam("skip") int skip,
            @RequestParam("page") int page,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered apiSearchOfflinePayment..");
        try {
            PartnerAccount account = getLoginUserAccount(request.getSession(false));
            Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            ApiResult<PagedData<List<OfflinePaymentRequestVM>>> data = bookingService.getPagedOfflinePaymentRequest(account, receiptNum, startDate, endDate, status, username, sortField, sortDirection, page, pageSize);
            int total = 0;
            boolean success = false;
            List<OfflinePaymentRequestVM> items = new ArrayList<OfflinePaymentRequestVM>();
            final ResultVM result = new ResultVM();
            if(data != null && data.isSuccess()){
                PagedData<List<OfflinePaymentRequestVM>> pagedData = data.getData();
                items = pagedData != null ? pagedData.getData() : new ArrayList<OfflinePaymentRequestVM>();
                total = pagedData != null ? pagedData.getSize() : 0;
                success = true;
            }
            result.setTotal(total);
            result.setStatus(success);
            result.setViewModel(items);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSearchOfflinePayment] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }
}
