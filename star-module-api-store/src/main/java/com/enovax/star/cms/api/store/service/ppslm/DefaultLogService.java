package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.api.store.model.partner.ppslm.PPSLMTransactionAdapter;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTelemoneyLog;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMTelemoneyLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 11/15/16.
 */
@Service
public class DefaultLogService implements ILogService{

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    PPSLMTelemoneyLogRepository telemoneyLogRepo;

    @Override
    public void logTelemoneyDetails(String title, PPSLMTelemoneyLog tm) {
        logTelemoneyDetails(log, title, tm);
    }

    @Override
    public void logTelemoneyDetails(Logger logger, String title, PPSLMTelemoneyLog tm) {
        logger.info("= = = = = BEGIN " + title + " = = = = =");
        if (tm == null) {
            logger.warn("Telemoney object is null. No parameters to log!");
        } else {
            logger.info("TM_MCode: " + tm.getTmMerchantId());
            logger.info("TM_RefNo: " + tm.getTmRefNo());
            logger.info("TM_Currency: " + tm.getTmCurrency());
            logger.info("TM_DebitAmt: " + tm.getTmDebitAmt());
            logger.info("TM_Status: " + tm.getTmStatus());
            logger.info("TM_Error: " + tm.getTmError());
            logger.info("TM_ErrorMsg: " + tm.getTmErrorMessage());
            logger.info("TM_PaymentType: " + tm.getTmPaymentType());
            logger.info("TM_ApprovalCode: " + tm.getTmApprovalCode());
            logger.info("TM_BankRespCode: " + tm.getTmBankRespCode());
            logger.info("TM_UserField1: " + tm.getTmUserField1());
            logger.info("TM_UserField2: " + tm.getTmUserField2());
            logger.info("TM_UserField3: " + tm.getTmUserField3());
            logger.info("TM_UserField4: " + tm.getTmUserField4());
            logger.info("TM_UserField5: " + tm.getTmUserField5());
            logger.info("TM_TrnType: " + tm.getTmTransType());
            logger.info("TM_SubTrnType: " + tm.getTmSubTransType());
            logger.info("TM_ExpiryDate: " + tm.getTmExpiryDate());
            logger.info("TM_RecurrentId: " + tm.getTmRecurrentId());
            logger.info("TM_SubSequentMCode: " + tm.getTmSubsequentMid());
            logger.info("TM_CCNum: " + tm.getTmCcNum());
            logger.info("TM_ECI: " + tm.getTmEci());
            logger.info("TM_CAVV: " + tm.getTmCavv());
            logger.info("TM_XID: " + tm.getTmXid());
        }
        logger.info("= = = = = END " + title + " = = = = =");
    }

    @Override
    public boolean logErrorDetails(String errorCode, String methodName, String details, PPSLMInventoryTransaction trans) {
        return logErrorTransDetails(errorCode, methodName, details,
                new PPSLMTransactionAdapter(trans));
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void saveTelemoneyDetails(PPSLMTelemoneyLog tm) {
        telemoneyLogRepo.save(tm);
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public boolean logTelemoneyDetailsToDB(PPSLMTelemoneyLog tm) {
        try {
            saveTelemoneyDetails(tm);
            return true;
        } catch (Exception e) {
            log.error("Unable to save Telemoney details to DB.", e);
            return false;
        }
    }

    @Override
    public PPSLMTelemoneyLog createTelemoneyObject(Boolean isQuery, TelemoneyResponse response) {
        PPSLMTelemoneyLog tm = new PPSLMTelemoneyLog();

        tm.setIsQuery(isQuery);
        tm.setTmApprovalCode(response.getTmApprovalCode());
        tm.setTmBankRespCode(response.getTmBankRespCode());
        tm.setTmCurrency(response.getTmCurrency());
        tm.setTmDebitAmt(response.getTmDebitAmt());
        tm.setTmError(response.getTmError());
        tm.setTmErrorMessage(response.getTmErrorMessage());
        tm.setTmExpiryDate(response.getTmExpiryDate());
        tm.setTmMerchantId(response.getTmMerchantId());
        tm.setTmPaymentType(response.getTmPaymentType());
        tm.setTmRecurrentId(response.getTmRecurrentId());
        tm.setTmRefNo(response.getTmRefNo());
        tm.setTmStatus(response.getTmStatus());
        tm.setTmSubsequentMid(response.getTmSubsequentMid());
        tm.setTmSubTransType(response.getTmSubTransType());
        tm.setTmTransType(response.getTmTransType());
        tm.setTmUserField1(response.getTmUserField1());
        tm.setTmUserField2(response.getTmUserField2());
        tm.setTmUserField3(response.getTmUserField3());
        tm.setTmUserField4(response.getTmUserField4());
        tm.setTmUserField5(response.getTmUserField5());
        tm.setTmCcNum(response.getTmCcNum());
        tm.setTmEci(response.getTmEci());
        tm.setTmCavv(response.getTmCavv());
        tm.setTmXid(response.getTmXid());

        return tm;

    }

    @Override
    public boolean processError(String errorCode, String methodName, PPSLMInventoryTransaction trans, boolean sendEmail) {
        try {

            String errorMessage = errorCode; //TODO Error message?

            log.info("Processing error: " + errorMessage);
            return processError(errorCode, methodName, errorMessage, trans, sendEmail);
        } catch(Exception e) {
            log.error("Another error occurred while processing the error. (processError P1)", e);
            return false;
        }
    }

    @Override
    public boolean processError(String errorCode, String methodName, String details, PPSLMInventoryTransaction trans, boolean sendEmail) {
        return processTransError(errorCode, methodName, details, new PPSLMTransactionAdapter(trans), sendEmail);
    }

    @Override
    public boolean processDepositTopupError(String errorCode, String methodName, String details, PPSLMDepositTransaction trans, boolean sendEmail) {
        return processTransError(errorCode, methodName, details, new PPSLMTransactionAdapter(trans), sendEmail);
    }

    @Override
    public void logInventoryTransactionDetails(String title, PPSLMInventoryTransaction tran) {
        logInventoryTransactionDetails(log, title, tran);
    }

    @Override
    public void logInventoryTransactionDetails(Logger logger, String title, PPSLMInventoryTransaction tran) {
        logTransDetails(log, title, new PPSLMTransactionAdapter(tran));
    }

    @Override
    public boolean logErrorTransDetails(String errorCode, String methodName, String details, PPSLMTransactionAdapter trans) {
        try {
            log.info("= = = = = BEGIN Error Log Details = = = = =");

            log.info("Error Code/Details: (" + errorCode + ") " + details);
            log.info("Related Method Name: " + methodName);

            if (trans == null) {
                log.warn("Transaction is null. Nothing transaction-related can be logged!");
            } else {
                log.info(trans.getTransMsg());
                log.info("Partner Details >>");
                log.info("Main Account ID: " + trans.getMainAccountId());
                log.info("Username: " + trans.getUsername());
                log.info("IsSubAccount: " + trans.isSubAccountTrans());

                log.info("Transaction Details >>");
                log.info("Receipt Number: " + trans.getReceiptNum());
                log.info("Transaction Created Date: " + trans.getCreatedDate());
                log.info("Transaction Last Modified Date: " + trans.getModifiedDate());
                log.info("Total Amount: " + trans.getTotalAmount());
                log.info("Status: " + trans.getStatus());
                log.info("TM Status: " + trans.getTmStatus());
            }

            log.info("= = = = = END Error Log Details = = = = =");
            return true;
        } catch (Exception e) {
            log.error("Exception occurred while logging to file.", e);
            return false;
        }
    }

    @Override
    public boolean processTransError(String errorCode, String methodName, String details, PPSLMTransactionAdapter trans, boolean sendEmail) {
        try {

            final String extraDetails =
                    (trans != null) ? "(RECEIPT " + trans.getReceiptNum() + ") " + details : details;

            boolean result = true;

            //SAVE TO DATABASE
            try {
                //TODO
               // logErrorToDb(errorCode, methodName, extraDetails);
            } catch (Exception e) {
                result = false;
                log.error("Error logging error to DB, continuing steps.");
            }

            //SAVE TO log FILE
            if (logErrorTransDetails(errorCode, methodName, extraDetails, trans)) {
                log.info("An error log record was successfully saved to the log file.");
            } else {
                result = false;
                log.error("Failed to save an error log record to the log file.");
            }

            //SEND EMAIL
            if (sendEmail) {
                log.info("Sending email to admin.");
                final String deets = trans == null ?
                        "We are unable to provide customer or transaction details for this message. Please check the logs." :
                        trans.getTransMsg() + "\n" +
                                "Partner Details:\n" +
                                "Main Account ID: " + trans.getMainAccountId() + "\n" +
                                "Username: (" + trans.getUsername() + "\n" +
                                "IsSubAccount: " + trans.isSubAccountTrans() + "\n\n\n" +
                                "Transaction Details:\n" +
                                "Receipt Number: " + trans.getReceiptNum() + "\n" +
                                "Transaction Created Date: " + trans.getCreatedDate() + "\n" +
                                "Transaction Last Modified Date: " + trans.getModifiedDate() + "\n" +
                                "Total Amount: " + trans.getTotalAmount() + "\n" +
                                "Status: " + trans.getStatus() + "\n" +
                                "TM Status: " + trans.getTmStatus() + "\n";

                Map<String, String> mailParams = new HashMap<String, String>();
                mailParams.put(":ErrorCode", errorCode);
                mailParams.put(":ErrorMessage", extraDetails);
                mailParams.put(":TransDetails", deets);

                //TODO send email
//                final String body = this.mailService.constructMailTemplate("adminErrorEmail.txt", mailParams);
//
//                this.mailService.sendEmail(this.mailService.getAdminMailProps(
//                                "gen_err_" + new Date(), "B2B System Alert: " + extraDetails, body, false),
//                        new DefaultMailCallback(log));
            }

            return result;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void logTransDetails(Logger logger, String title, PPSLMTransactionAdapter tran) {
        logger.info("= = = = = BEGIN " + title + " = = = = =");
        if (tran == null) {
            logger.warn("Transaction object is null. Nothing to log!");
        } else {
            logger.info("Trans Type: " + tran.getTransMsg());
            logger.info("receiptNo: " + tran.getReceiptNum());
            logger.info("status: " + tran.getStatus());

            logger.info("mainAccountId: " + tran.getMainAccountId());
            logger.info("username: " + tran.getUsername());
            logger.info("isSubAccount: " + tran.isSubAccountTrans());
            logger.info("totalAmount: " + tran.getTotalAmount());
            logger.info("currency: " + tran.getCurrency());
            logger.info("paymentType: " + tran.getPaymentType());

            logger.info("tmmerchantId: " + tran.getTmMerchantId());
            logger.info("tmstatus: " + tran.getTmStatus());
            logger.info("tmerrorMessage: " + tran.getTmErrorMessage());
            logger.info("tmapprovalCode: " + tran.getTmApprovalCode());

            logger.info("createdDate: " + tran.getCreatedDate());
            logger.info("modifiedDate: " + tran.getModifiedDate());
            logger.info("validityEndDate: " + tran.getValidityEndDate());

            if (tran.getTransClass() == PPSLMRevalidationTransaction.class) {
                logger.info("TmTransType :"
                        + (tran.getTmPkg() == null ? "null" : tran.getTmPkg()
                        .getTmTransType()));
                logger.info("TotalMainQty :" + tran.getRevalItemCode());
                logger.info("RevalTopupItemCode " + tran.getTotalMainQty());
                logger.info("RevalTopupItemCode :"
                        + tran.getRevalTopupItemCode());
                logger.info("TotalTopupQty :" + tran.getTotalTopupQty());
            }


        }
        logger.info("= = = = = END " + title + " = = = = =");
    }
}
