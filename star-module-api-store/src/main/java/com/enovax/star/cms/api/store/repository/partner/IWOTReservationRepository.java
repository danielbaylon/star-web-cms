package com.enovax.star.cms.api.store.repository.partner;

import com.enovax.star.cms.commons.model.partner.ppslm.WOTReservationVM;

import java.util.List;

/**
 * Created by jennylynsze on 6/21/16.
 */
public interface IWOTReservationRepository {
    void save(WOTReservationVM reservation);
    List<WOTReservationVM> getReservations(Integer adminId,
                                           String fromDateStr,
                                           String toDateStr,
                                           String displayCancelled,
                                           String showTimes,
                                           String orderBy,
                                           String orderWith,
                                           Integer page, Integer pagesize);
    int getReservationsSize(Integer adminId,
                            String fromDateStr,
                            String toDateStr,
                            String displayCancelled,
                            String showTimes);

    WOTReservationVM getReservationById(int id);
    Integer getNextId();
}
