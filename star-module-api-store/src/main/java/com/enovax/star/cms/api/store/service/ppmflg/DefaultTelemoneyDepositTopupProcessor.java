package com.enovax.star.cms.api.store.service.ppmflg;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.payment.tm.processor.TmSaleProcessor;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.api.store.model.partner.ppmflg.PPMFLGTransactionAdapter;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTelemoneyLog;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.TelemoneyProcessingDataDepositTopup;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@SuppressWarnings("Duplicates")
@Component("PPMFLGDefaultTelemoneyDepositTopupProcessor")
@Scope("prototype")
public class DefaultTelemoneyDepositTopupProcessor extends TmSaleProcessor<TelemoneyProcessingDataDepositTopup> {

    @Autowired
    @Qualifier("PPMFLGILogService")
    private ILogService logService;

    @Autowired
    private StarTemplatingFunctions starFn;

    @Autowired
    private TmServiceProvider tmServiceProvider;

    @Autowired
    @Qualifier("PPMFLGIPartnerDepositService")
    private IPartnerDepositService ppmflgDepositSrv;

    public void process(TelemoneyResponse tmResponse) {
        String uniqueKeyPart1 = System.currentTimeMillis()+"";
        log.info(uniqueKeyPart1+"Start processing Telemoney POST for Partner Portal SLM - Deposit Topup Flow...");
        log.info(uniqueKeyPart1+"Logging Telemoney Response...");
        log.info(uniqueKeyPart1+"[Telemoney Response] => " + JsonUtil.jsonify(tmResponse));

        boolean isFromQuery = false;

        String payment = TmPaymentType.getTitle(tmResponse.getTmPaymentType());
        log.debug("initialize payment: " + payment);

        final String tmDataReceiptNumber = tmResponse.getTmRefNo();
        final String tmDataChannel = tmResponse.getTmUserField1();
        final String tmDataSessionId = tmResponse.getTmUserField3();
        final String tmStatus = tmResponse.getTmStatus();
        final String tmMerchantId = tmResponse.getTmMerchantId();
        String uniqueKey = uniqueKeyPart1+"-"+String.valueOf(tmDataReceiptNumber)+"-";

        PPMFLGDepositTransaction txn = ppmflgDepositSrv.getDepositTopupTransaction(tmDataReceiptNumber, tmDataSessionId, tmResponse.getTmUserField2());

        log.info(uniqueKey+"Checking transaction existence.");
        if (txn == null) {
            log.error(uniqueKey+"Retrieved transaction is null.");
            logService.processDepositTopupError(ErrorTypes.TKT_INVALID_TRANS.cd, "ProcessTMStatusAction.execute-checkTransNull", null, null, true);
            return;
        }

        log.info(uniqueKey+"Checking transaction status.");
        if (!(TransactionStage.Reserved.name().equals(txn.getStatus()) && EnovaxTmSystemStatus.RedirectedToTm.name().equals(txn.getTmStatus()))) {
            log.error(uniqueKey+"Transaction status has already been set.");
            logService.processDepositTopupError(ErrorTypes.TKT_STATUS_SET.cd, "ProcessTMStatusAction.execute-checkStatusSet", null ,txn, true);
            return;
        }

        log.info(uniqueKey+"Checking transaction TM Merchant.");
        if (TmStatus.Success.name().equals(tmStatus) && !txn.getTmMerchantId().equals(tmMerchantId)) {
            log.error(uniqueKey+"Transaction's merchant code did not correspond to TM's code.");
            logService.processDepositTopupError(ErrorTypes.TKT_WRONG_TMMID.cd, "ProcessTMStatusAction.execute-checkTMmid", null, txn, true);
            return;
        }

        this.logService.logTransDetails(log, "ProcessTMStatusAction :: Retrieved Inventory Transaction (RefNo)", new PPMFLGTransactionAdapter(txn));

        isFromQuery = isTMStatusInitiatedByQuery(txn);

        log.info(uniqueKey+"Creating Telemoney object...");

        final PPMFLGTelemoneyLog telemoneyLog = logService.createTelemoneyObject(isFromQuery, tmResponse);
        log.info(uniqueKey+"Logging TM object and transaction.");

        this.logService.logTelemoneyDetails(log, "ProcessTMStatusAction :: TM Parameters Accepted", telemoneyLog);

        final boolean tmDbSaved = this.logService.logTelemoneyDetailsToDB(telemoneyLog);

        if (!tmDbSaved) {
            log.warn(uniqueKey+"Telemoney details were not saved to the database.");
        }

        //We purposely only handle SALE type requests here. VOID type requests are handled in a different processing flow.
        final String tmTransType = tmResponse.getTmTransType();
        if (!TmTransType.Sale.tmCode.equalsIgnoreCase(tmTransType)) {
            log.info(uniqueKey+"Transaction was not of SALE type. Processing will not continue from here.");
            return;
        }

        StoreApiChannels channel;
        try {
            channel = StoreApiChannels.fromCode(tmDataChannel);
        } catch (Exception e) {
            log.error(uniqueKey+"Unable to find a valid channel for this response. EX: " + e.getMessage(), e);
            return;
        }

        try {
            TelemoneyProcessingDataDepositTopup data = new TelemoneyProcessingDataDepositTopup();
            data.setChannel(channel);
            data.setReceiptNumber(tmDataReceiptNumber);
            data.setSid(tmDataSessionId);
            data.setMflgTx(txn);
            processTelemoneySalePost(tmResponse, data);
        } catch (Exception e) {
            log.error(uniqueKey+"General exception encountered while processing...", e);
        }
    }

    private boolean isTMStatusInitiatedByQuery(PPMFLGDepositTransaction trans) {
        if (trans != null && EnovaxTmSystemStatus.TmQuerySent.toString().equals(trans.getTmStatus())) {
            return true;
        }
        return false;
    }

    @Override
    protected void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data) {
        //Do nothing, and wait for the next response.
    }

    @Override
    protected void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data) {
        log.info(data.getLogKey()+"Telemoney response is NOT FOUND for the transaction. Processing...");
        _processFailedTransaction(TransactionStage.Incomplete, EnovaxTmSystemStatus.NA, tmResponse, data, "TelemoneyNotFound",  "Transaction not found at Telemoney.", false, false);
    }

    @Override
    protected void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data) {
        log.info(data.getLogKey()+"Telemoney response is FAILED for the transaction. Processing...");
        String tmMessage = tmResponse.getTmErrorMessage();
        if(tmMessage != null && tmMessage.trim().length() > 0 && tmMessage.toLowerCase().indexOf("Cancelled by user".toLowerCase()) > -1){
            _processFailedTransaction(TransactionStage.Canceled, EnovaxTmSystemStatus.Failed, tmResponse, data, "TelemoneyFailed", "Transaction failed at Telemoney.", false, false);
        }else{
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "TelemoneyFailed", "Transaction failed at Telemoney.", false, false);
        }
    }

    @Override
    protected void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data) {
        log.info(data.getLogKey()+"Telemoney response is SUCCESSFUL for the transaction. Processing...");
        final BigDecimal tmDebitAmt = tmResponse.getTmDebitAmt().setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);
        BigDecimal tempTransAmount = data.getMflgTx().getTotalAmount();
        final BigDecimal transAmount = tempTransAmount.setScale(2, NvxNumberUtils.DEFAULT_ROUND_MODE);

        if (!transAmount.equals(tmDebitAmt)) {
            final String errMsg = "TM debit amount of " + tmDebitAmt + " is different from the transaction amount of " + transAmount;
            log.error(data.getLogKey()+""+errMsg);
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "TMIncorrectAmount", errMsg, true, true);
        } else {
            log.info(data.getLogKey()+"Amount validated.");
            _finaliseSuccessfulTransaction(tmResponse, data);
        }
    }

    private void _processFailedTransaction(TransactionStage stage, EnovaxTmSystemStatus tmStatus, TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data, String customErrCode, String customErrMsg, boolean requireVoid, boolean forceFailed) {
        final Date now = new Date();

        final String ec = StringUtils.isEmpty(customErrCode) ? StringUtils.isEmpty(tmResponse.getTmError()) ? "ERROR" : tmResponse.getTmError() : customErrCode;
        final String em = StringUtils.isEmpty(customErrMsg)  ? StringUtils.isEmpty(tmResponse.getTmErrorMessage()) ? "ERROR" : tmResponse.getTmErrorMessage() : customErrMsg;

        log.error(data.getLogKey()+""+"Processing failed transaction due to an error. More details: " + ec + " - " + em);

        PPMFLGDepositTransaction txn = data.getMflgTx();
        try {
            ppmflgDepositSrv.saveDepositTransactionForTMFailedProcessing(data.getMflgTx().getId(), stage.name(), tmStatus.name(), ec, em, now, forceFailed);
        } catch (BizValidationException e) {
            log.error(data.getLogKey()+""+"_processFailedTransaction : "+e.getMessage(), e);
        }

        if (requireVoid) {
            _doVoidAction(data);
        }
    }

    private void _finaliseSuccessfulTransaction(TelemoneyResponse tmResponse, TelemoneyProcessingDataDepositTopup data) {
        final Date now = new Date();
        PPMFLGDepositTransaction txn = data.getMflgTx();
        try {
            ppmflgDepositSrv.saveDepositTransactionForTMSuccessProcessing(txn.getId(), TransactionStage.Success.name(), EnovaxTmSystemStatus.Success.name(), tmResponse.getTmCcNum(), tmResponse.getTmApprovalCode(), tmResponse.getTmPaymentType(), now);
        } catch (BizValidationException e) {
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "SaveTxnDetailsFailed", "Save transaction details into database failed", true, true);
            log.error(data.getLogKey()+""+"_processFailedTransaction : "+e.getMessage(), e);
            return;
        }
        ApiResult<String> axResponse = null;
        try{
            axResponse = ppmflgDepositSrv.updateAxCustomerDepositBalance(data.getMflgTx());
        }catch (Exception ex){
            if(axResponse == null){
                axResponse = new ApiResult<String>();
            }
            if(!axResponse.isSuccess()){
                axResponse.setMessage(ex.getMessage());
            }
        }catch (Throwable ex){
            if(axResponse == null){
                axResponse = new ApiResult<String>();
            }
            if(!axResponse.isSuccess()){
                axResponse.setMessage(ex.getMessage());
            }
        }
        if(!(axResponse != null && axResponse.isSuccess())){
            String errMsg = axResponse != null ? axResponse.getMessage() : "NA";
            _processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "UpdateDepositBalanceFailed", "Calling API update deposit balance failed. "+ (errMsg != null && errMsg.trim().length() > 0 ? "Error:"+errMsg : ""), true, true);
            log.error(data.getLogKey()+""+"_finaliseSuccessfulTransaction : calling API and update deposit failed "+errMsg);
        }else{
            if(axResponse != null && axResponse.isSuccess()){
                ppmflgDepositSrv.sendDepositTopupTransactionReceiptEmail(txn);
            }
        }
    }

    private void _doVoidAction(TelemoneyProcessingDataDepositTopup data) {
        TmServiceResult voidResult = null;
        try{
            String merchantId = starFn.getDefaultMerchant(PartnerPortalConst.Partner_Portal_Channel);
            voidResult = tmServiceProvider.sendVoidWithRetry(merchantId, TmCurrency.SGD, data.getMflgTx().getTotalAmount(), data.getReceiptNumber(), null);
        }catch (Exception ex1){
            if(voidResult == null){
                voidResult = new TmServiceResult(false, ex1.getMessage(), "");
            }
            voidResult.setErrorMessage(ex1.getMessage());
        }catch (Throwable ex2){
            if(voidResult == null){
                voidResult = new TmServiceResult(false, ex2.getMessage(), "");
            }
            voidResult.setErrorMessage(ex2.getMessage());
        }finally {
            try{
                PPMFLGDepositTransaction txn = ppmflgDepositSrv.saveDepositTransactionForTMVoidProcessing(data.getMflgTx(), voidResult);
                ppmflgDepositSrv.sendVoidEmail(txn, voidResult);
                logService.logTransDetails(log, "TicketingTmEvent :: Voided Transaction (Failed)", new PPMFLGTransactionAdapter(txn));
                String errorCode = null;
                if(voidResult != null && voidResult.isSuccess()){
                    errorCode = ErrorTypes.TKT_RECGEN_GENERAL_VOID1.cd;
                }else{
                    errorCode = ErrorTypes.TKT_RECGEN_GENERAL_VOID0.cd;
                }
                logService.processDepositTopupError(errorCode, "TicketingTmEvent._voidTransaction", (voidResult != null&& voidResult.isSuccess() ? "Void Deposit Topup Transaction Success" : "Void Deposit Topup Transaction Failed"), txn, true);
            }catch (Exception ex1){
                log.error(data.getLogKey()+""+"save void transaction "+data.getReceiptNumber()+" failed with error : "+ex1.getMessage());
            }
        }
    }
}