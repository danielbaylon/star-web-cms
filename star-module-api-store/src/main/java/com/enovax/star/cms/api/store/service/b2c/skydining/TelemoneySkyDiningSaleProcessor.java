package com.enovax.star.cms.api.store.service.b2c.skydining;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmStatus;
import com.enovax.payment.tm.constant.TmTransType;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.processor.TmSaleProcessor;
import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.skydining.constant.PaymentMode;
import com.enovax.star.cms.skydining.constant.ReservationStatus;
import com.enovax.star.cms.skydining.constant.Stage;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTelemoneyLog;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTransaction;
import com.enovax.star.cms.skydining.model.booking.TelemoneyProcessingDataSkyDining;
import com.enovax.star.cms.skydining.repository.db.SkyDiningReservationRepository;
import com.enovax.star.cms.skydining.repository.db.SkyDiningTelemoneyLogRepository;
import com.enovax.star.cms.skydining.repository.db.SkyDiningTransactionRepository;
import com.enovax.star.cms.skydining.service.ISharedConfigService;
import com.enovax.star.cms.skydining.service.ISkyDiningReservationService;
import com.enovax.star.cms.skydining.service.ISkyDiningTemplateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by jace on 15/11/16.
 */
@SuppressWarnings("Duplicates")
@Component
@Scope("prototype")
public class TelemoneySkyDiningSaleProcessor extends TmSaleProcessor<TelemoneyProcessingDataSkyDining> {

    @Autowired
    private SkyDiningTransactionRepository transRepo;
    @Autowired
    private SkyDiningReservationRepository resRepo;
    @Autowired
    private SkyDiningTelemoneyLogRepository tmRepo;

    @Autowired
    private ISharedConfigService configService;
    @Autowired
    private ISkyDiningTemplateService templateService;

    @Autowired
    private ISkyDiningService skyDiningService;
    @Autowired
    private IMailService mailSrv;
    @Autowired
    private ISkyDiningReservationService reservationService;

    @Autowired
    private ISystemParamRepository paramRepo;

    public boolean isMailServiceEnabled() {
        String str = paramRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_MAIL_SERVICE_KEY);
        if (str != null && str.trim().length() > 0) {
            if (Boolean.TRUE.toString().equalsIgnoreCase(str.trim())) {
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void process(TelemoneyResponse tmResponse) {
        log.info("Start processing Telemoney POST for Sky Dining...");
        log.info("Logging Telemoney Response...");
        log.info("[Telemoney Response] => " + JsonUtil.jsonify(tmResponse));

        //TODO Implement signature mechanism? Version 2.0 of TM.

        //We purposely only handle SALE type requests here. VOID type requests are handled in a different processing flow.
        final String tmTransType = tmResponse.getTmTransType();
        if (!TmTransType.Sale.tmCode.equalsIgnoreCase(tmTransType)) {
            log.info("Transaction was not of SALE type. Processing will not continue from here.");
            return;
        }

        final String tmDataReceiptNumber = tmResponse.getTmRefNo();
        //final String tmDataSessionId = tmResponse.getTmUserField3();

        SkyDiningTransaction transaction = transRepo.findOneByReceiptNum(tmDataReceiptNumber);
        SkyDiningReservation reservation = transaction.getReservation();

        // Store TM object in DB
        boolean isFromQuery = false;
        if (transaction != null && EnovaxTmSystemStatus.TmQuerySent.toString().equals(transaction.getTmStatus())) {
            isFromQuery = true;
        }
        SkyDiningTelemoneyLog tmLog = new SkyDiningTelemoneyLog();

        tmLog.setIsQuery(isFromQuery);
        tmLog.setTmApprovalCode(tmResponse.getTmApprovalCode());
        tmLog.setTmBankRespCode(tmResponse.getTmBankRespCode());
        tmLog.setTmCurrency(tmResponse.getTmCurrency());
        tmLog.setTmDebitAmt(tmResponse.getTmDebitAmt());
        tmLog.setTmError(tmResponse.getTmError());
        tmLog.setTmErrorMessage(tmResponse.getTmErrorMessage());
        tmLog.setTmExpiryDate(tmResponse.getTmExpiryDate());
        tmLog.setTmMerchantId(tmResponse.getTmMerchantId());
        tmLog.setTmPaymentType(tmResponse.getTmPaymentType());
        tmLog.setTmRecurrentId(tmResponse.getTmRecurrentId());
        tmLog.setTmRefNo(tmResponse.getTmRefNo());
        tmLog.setTmStatus(tmResponse.getTmStatus());
        tmLog.setTmSubsequentMid(tmResponse.getTmSubsequentMid());
        tmLog.setTmSubTransType(tmResponse.getTmSubTransType());
        tmLog.setTmTransType(tmResponse.getTmTransType());
        tmLog.setTmUserField1(tmResponse.getTmUserField1());
        tmLog.setTmUserField2(tmResponse.getTmUserField2());
        tmLog.setTmUserField3(tmResponse.getTmUserField3());
        tmLog.setTmUserField4(tmResponse.getTmUserField4());
        tmLog.setTmUserField5(tmResponse.getTmUserField5());
        tmLog.setTmCcNum(tmResponse.getTmCcNum());
        tmLog.setTmEci(tmResponse.getTmEci());
        tmLog.setTmCavv(tmResponse.getTmCavv());
        tmLog.setTmXid(tmResponse.getTmXid());

        tmRepo.save(tmLog);

        final TelemoneyProcessingDataSkyDining data = new TelemoneyProcessingDataSkyDining();
        data.setSdRes(reservation);
        data.setSdTxn(transaction);

        try {
            processTelemoneySalePost(tmResponse, data);
        } catch (Exception e) {
            log.error("General exception encountered while processing...", e);
        }
    }

    @Override
    protected void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, TelemoneyProcessingDataSkyDining data) {
        log.info("Telemoney response is SUCCESSFUL for the transaction. Processing...");


        try {
            SkyDiningTransaction transaction = data.getSdTxn();

            Date now = new Date();
            transaction.setTmStatus(TmStatus.Success.name());
            transaction.setTmApprovalCode(tmResponse.getTmApprovalCode());
            transaction.setStage(Stage.SuccessFinalized.name());
            transaction.setModifiedDate(now);
            transaction.setModifiedBy(SysConst.SYSTEM);
            transRepo.save(transaction);

            SkyDiningReservation reservation = data.getSdRes();
            reservation.setReceiptNum(tmResponse.getTmRefNo());
            reservation.setReceivedBy(SysConst.SYSTEM);
            reservation.setPaymentDate(now);
            reservation.setStatus(ReservationStatus.PaidInsideSystem.value);
            switch (PaymentMode.fromTitle(tmResponse.getTmPaymentType())) {
                case VISA:
                    reservation.setPaymentMode(PaymentMode.VISA.value);
                    break;
                case MasterCard:
                    reservation.setPaymentMode(PaymentMode.MasterCard.value);
                    break;
                case ChinaUnionPay:
                    reservation.setPaymentMode(PaymentMode.ChinaUnionPay.value);
                    break;
                default:
                    reservation.setPaymentMode(PaymentMode.OtherCard.value);
                    break;
            }
            reservation.setPaidAmount(tmResponse.getTmDebitAmt());
            reservation.setModifiedDate(now);
            reservation.setModifiedBy(SysConst.SYSTEM);
            resRepo.save(reservation);

            if (isMailServiceEnabled()) {
                reservationService.sendReceiptEmail(transaction, reservation);
            }
        } catch (Exception e) {
            processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.NA, tmResponse, data, "SaveError",
                "Error updating sky dining reservation.", true);
        }
    }

    @Override
    protected void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, TelemoneyProcessingDataSkyDining data) {
        log.info("Telemoney response is NOT FOUND for the transaction. Processing...");
        processFailedTransaction(TransactionStage.Incomplete, EnovaxTmSystemStatus.NA, tmResponse, data, "TelemoneyNotFound",
            "Transaction not found at Telemoney.", true);
    }

    @Override
    protected void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, TelemoneyProcessingDataSkyDining data) {
        log.info("Telemoney response is FAILED for the transaction. Processing...");
        processFailedTransaction(TransactionStage.Failed, EnovaxTmSystemStatus.Failed, tmResponse, data, "TelemoneyFailed",
            "Transaction failed at Telemoney.", true);
    }

    @Override
    protected void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, TelemoneyProcessingDataSkyDining data) {
        log.info("Telemoney {} has indicated a REDIRECT status for the current transaction. " +
            "Transaction {} is still pending at a third-party site.", tmResponse.getTmXid(), data.getTxn().getReceiptNumber());
        //Do nothing, and wait for the next response.
    }

    private void processFailedTransaction(TransactionStage stage, EnovaxTmSystemStatus tmStatus,
                                          TelemoneyResponse tmResponse, TelemoneyProcessingDataSkyDining data,
                                          String customErrCode, String customErrMsg, boolean requireVoid) {
        final Date now = new Date();

        final SkyDiningTransaction txn = data.getSdTxn();

        txn.setStage(stage.name());
        txn.setTmStatus(tmStatus.name());
        txn.setModifiedDate(now);

        final String ec = StringUtils.isEmpty(customErrCode)
            ? StringUtils.isEmpty(tmResponse.getTmError()) ? "ERROR" : tmResponse.getTmError()
            : customErrCode;
        final String em = StringUtils.isEmpty(customErrMsg)
            ? StringUtils.isEmpty(tmResponse.getTmErrorMessage()) ? "ERROR" : tmResponse.getTmErrorMessage()
            : customErrMsg;
        log.error("Processing failed transaction due to an error. More details: " + ec + " - " + em);

        transRepo.save(txn);

        SkyDiningReservation res = data.getSdRes();

        res.setStatus(ReservationStatus.Cancelled.value);
        res.setModifiedDate(now);

        resRepo.save(res);

        if (requireVoid) {
            doVoidAction();
        }
    }

    private void doVoidAction() {
        //TODO
    }
}
