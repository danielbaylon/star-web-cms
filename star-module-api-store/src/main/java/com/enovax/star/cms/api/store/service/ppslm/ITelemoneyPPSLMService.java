package com.enovax.star.cms.api.store.service.ppslm;

import com.enovax.payment.tm.model.TelemoneyResponse;

/**
 * Created by houtao on 14/11/16.
 */
public interface ITelemoneyPPSLMService  {
    void processMerchantPost(TelemoneyResponse response);
}
