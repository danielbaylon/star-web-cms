package com.enovax.star.cms.api.store.controller;

import com.enovax.star.cms.api.store.service.b2c.IBookingService;
import com.enovax.star.cms.api.store.service.batch.axsync.AxCountryRegionsSyncJob;
import com.enovax.star.cms.api.store.service.batch.axsync.AxRegionSyncJob;
import com.enovax.star.cms.api.store.service.product.IProductCmsService;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;
import com.enovax.star.cms.commons.model.booking.CrossSellCart;
import com.enovax.star.cms.commons.model.booking.CrossSellCartItem;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.service.template.ITicketTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/store-data/")
public class OnlineStoreDataController extends BaseStoreApiController {

    @Autowired
    private HttpSession session;

    @Autowired
    private IB2CCartAndDisplayService cartAndDisplayService;
    @Autowired
    private IBookingService bookingService;
    @Autowired
    private IProductCmsService productCmsService;
    @Autowired
    private IProductService productService;
    @Autowired
    private ITicketTemplateService ticketTemplateService;
    @Autowired
    private AxCountryRegionsSyncJob axCountryRegionsSyncJob;

    @Autowired
    @Qualifier("PPMFLGAxCountryRegionsSyncJob")
    private com.enovax.star.cms.api.store.service.batch.ppmflgaxsync.AxCountryRegionsSyncJob mflgAxCountryRegionsSyncJob;

    @Autowired
    private AxRegionSyncJob axRegionsSyncJob;

    @Autowired
    @Qualifier("PPMFLGAxRegionsSyncJob")
    private com.enovax.star.cms.api.store.service.batch.ppmflgaxsync.AxRegionSyncJob mflgAxRegionsSyncJob;

    @RequestMapping(value = "reload-ticket-template-cache/{channel}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiReloadTicketTemplates(
            @PathVariable("channel") String channelCode) {
        log.info("Entered  apiReloadTicketTemplates...");
        try {
            ticketTemplateService.updateTicketTemplateCache(StoreApiChannels.fromCode(channelCode));
            return new ResponseEntity<>(new ApiResult<>(true, "", "", "OK"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiReloadTicketTemplates] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "reload-ax-country-region/{channel}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiReloadAxCountryRegion(
            @PathVariable("channel") String channelCode) {
        log.info("Entered  apiReloadAxCountryRegion...");
        try {
            if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channelCode)) {
                axCountryRegionsSyncJob.axCountryRegionsSyncJob();
            }else {
                mflgAxCountryRegionsSyncJob.axCountryRegionsSyncJob();
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", "OK"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiReloadAxCountryRegion] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "reload-ax-region/{channel}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiReloadAxRegion(
            @PathVariable("channel") String channelCode) {
        log.info("Entered  apiReloadAxRegion...");
        try {
            if(StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channelCode)) {
                axRegionsSyncJob.axRegionsSyncJob();
            }else {
                mflgAxRegionsSyncJob.axRegionsSyncJob();
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", "OK"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiReloadAxRegio] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-ticket-templates", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<List<AxRetailTemplateXML>>> apiGetTicketTemplates(
            @RequestBody String channelCode) {


        log.info("Entered apiGetTicketTemplates...");

        try {
            List<AxRetailTemplateXML> result = ticketTemplateService.getTicketTemplates(StoreApiChannels.fromCode(channelCode));

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTicketTemplates] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, AxRetailTemplateXML.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-products", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<AxProductRelatedLinkPackage>> apiGetProducts(
            @RequestBody String channelCode) {


        log.info("Entered apiGetCrossSellItems...");

        try {
            ApiResult<AxProductRelatedLinkPackage> result = productService.getValidRelatedAxProducts(StoreApiChannels.fromCode(channelCode));

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxProductRelatedLinkPackage.class, ""), HttpStatus.OK);
        }
    }

//    @RequestMapping(value = "get-promotions", method = {RequestMethod.GET, RequestMethod.POST})
//    public ResponseEntity<ApiResult<PromotionLinkPackage>> apiGetPromotions(
//            @RequestBody String channelCode) {
//
//
//        log.info("Entered apiGetCrossSellItems...");
//
//        try {
//            ApiResult<PromotionLinkPackage> result = productService.getFullDiscountData(StoreApiChannels.fromCode(channelCode));
//
//            return new ResponseEntity<>(result, HttpStatus.OK);
//
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
//            return new ResponseEntity<>(handleUncaughtException(e, PromotionLinkPackage.class, ""), HttpStatus.OK);
//        }
//    }

    @RequestMapping(value = "get-cs", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<CrossSellLinkPackage>> apiGetCS(
            @RequestBody String channelCode) {


        log.info("Entered apiGetCrossSellItems...");

        try {
            ApiResult<CrossSellLinkPackage> result = productService.getFullUpCrossSellData(StoreApiChannels.fromCode(channelCode));

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CrossSellLinkPackage.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-cross-sell-items/{locale}", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<CrossSellCart>> apiGetCrossSellItems(
                                                    @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
                                                    @PathVariable("locale") String locale,
                                                    @RequestBody String productNodeName) {

        log.info("Entered apiGetCrossSellItems...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            String sessionId = session.getId();

            FunCartDisplay funCartDisplay = cartAndDisplayService.constructCartDisplay(channel, sessionId);

            ApiResult<List<CrossSellCartItem>> result = productCmsService.getAllCrossSellCartItemsFor(funCartDisplay, channelCode, productNodeName, locale);

            CrossSellCart crossSellCart = new CrossSellCart();
            crossSellCart.setCrossSellCartItems(result.getData());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", crossSellCart), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CrossSellCart.class, ""), HttpStatus.OK);
        }
    }
}