package com.enovax.star.cms.api.store.service.b2c.search;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.search.QsProd;
import com.enovax.star.cms.commons.model.search.QuickSearchProductGraph;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import info.magnolia.module.site.functions.SiteFunctions;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by jonathan on 28/12/16.
 */
@Service
public class DefaultQuickSearch implements IQuickSearch {

    protected Logger log = LoggerFactory.getLogger(getClass());

    public static final String MAIN_KEY = "nvx.quick.search";
    public static final int DESC_SNIPPET_PLUS_MINUS = 20;

    private LoadingCache<String, QuickSearchProductGraph> mainCache;
    private Cache<String, QuickSearchProductGraph> termCache;

    @Autowired
    private StarTemplatingFunctions starfn;

    @PostConstruct
    protected void init() {
        this.mainCache = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.DAYS)
            .build(new CacheLoader<String, QuickSearchProductGraph>() {
                @Override
                public QuickSearchProductGraph load(String key) throws Exception {
                    if (!MAIN_KEY.equals(key)) { return new QuickSearchProductGraph(new ArrayList<QsProd>()); }
                    return loadMainGraph();
                }
            });

        this.termCache = CacheBuilder.newBuilder()
            .maximumSize(2000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .build();
    }

    @Override
    public QuickSearchProductGraph doSearch(String searchTerm, String locale) {
        QuickSearchProductGraph result = this.termCache.getIfPresent(searchTerm+locale);
        if (result != null) {
            return result;
        }

        try {
            final QuickSearchProductGraph wholeGraph = this.mainCache.get(MAIN_KEY);

            final QuickSearchProductGraph resultGraph = searchInWholeGraph(searchTerm, wholeGraph, locale);

            this.termCache.put(searchTerm+locale, resultGraph);

            return resultGraph;
        } catch (ExecutionException e) {
            log.error("Error retrieving from the main cache. Returning an empty search result", e);
            return new QuickSearchProductGraph(new ArrayList<QsProd>());
        }
    }

    @Override
    public QuickSearchProductGraph loadMainGraph() {
        SiteFunctions siteFunctions = Components.getComponent(SiteFunctions.class);
        Collection<Locale> locales = siteFunctions.site().getI18n().getLocales();

        // FIXME
        final List<QsProd> stripped = new ArrayList<>();
        final List<Node> cats = this.starfn.getCMSProductCategories(StoreApiChannels.B2C_SLM.code);
        for (Node cat: cats) {
            try {
                final List<Node> results = this.starfn.getCMSProductByCategory(StoreApiChannels.B2C_SLM.code, cat.getName());
                if (results == null) {
                    continue;
                }
                for (Node res : results) {
//                    if (ProductTypes.isEvent(res.getType())) {
//                        final EventProduct ep = res.getEventProduct();
//                        if (ep.getStartDate() == null || ep.getEndDate() == null) {
//                            continue;
//                        }
//                        if (!NvxUtil.inDateRange(ep.getStartDate(), ep.getEndDate(), new Date(), true, false)) {
//                            continue;
//                        }
//                    }

                    try {
                        final QsProd p = new QsProd();
                        p.setId(res.getUUID());
                        p.setDisplayTitle(res.getProperty("name").getString());
                        p.setDisplayTitleI18N(new HashMap<>());
                        for (Locale locale: locales) {
                            p.getDisplayTitleI18N().put(locale.toString(), starfn.getPropertyValue(res, "name", locale.toString()));
                        }
                        p.setDisplayDescription(res.getProperty("description").getString());
                        p.setDisplayDescriptionI18N(new HashMap<>());
                        for (Locale locale: locales) {
                            p.getDisplayDescriptionI18N().put(locale.toString(), starfn.getPropertyValue(res, "description", locale.toString()));
                        }
//            p.setShortDescription(res.getShortDescription());
                        p.setUrlTitle("/category/product~" + starfn.asContentMap(res).get("generatedURLPath").toString().toLowerCase() +
                            "~${LANG}~" + cat.getName() + "~" + res.getName() + "~.html?category=" +
                            cat.getProperty("generatedURLPath").getString().toLowerCase());
                        p.setCategory(cat.getProperty("name").getString());

                        stripped.add(p);
                    } catch (RepositoryException e) {
                        log.error("Error retrieving product.", e);
                    }
                }
            } catch (Exception e) {
                log.error("Error retrieving category.", e);
            }
        }

        return new QuickSearchProductGraph(stripped);
    }

    @Override
    public void refreshGraph() {
        this.mainCache.refresh(MAIN_KEY);
        this.termCache.invalidateAll();
    }

    protected QuickSearchProductGraph searchInWholeGraph(String searchTerm, QuickSearchProductGraph wholeGraph, String locale) {
        final List<QsProd> prods = wholeGraph.getProds();
        final List<QsProd> results = new ArrayList<>();
        final int termlen = searchTerm.length();
        log.debug("local --- "+locale);
        for (QsProd p : prods) {
            int titleIdx = -1;
            String title = null;
            if(Locale.ENGLISH.toString().equals(locale)){
                title = p.getDisplayTitle();
            }else{
                title = p.getDisplayTitleI18N().get(locale);
            }
            titleIdx = StringUtils.indexOfIgnoreCase(title, searchTerm);

            if (titleIdx > -1) {
                final int titleIdxEnd = titleIdx + termlen;
                final String highlighted = title.substring(0, titleIdx) + "<strong>" + title.substring(titleIdx, titleIdxEnd) +
                    "</strong>" + title.substring(titleIdxEnd, title.length());

                final QsProd res = new QsProd();
                res.setId(p.getId());
                res.setDisplayTitle(highlighted);
                res.setRawTitle(title);
                res.setCategory(p.getCategory());
                res.setUrlTitle(p.getUrlTitle().replace("${LANG}", locale));
                res.setFoundInTitle(true);
                res.setIsDefault(false);
                results.add(res);
            } else {
                int descIdx = -1;
                String desc = null;
                if(Locale.ENGLISH.toString().equals(locale)){
                    desc = Jsoup.clean(p.getDisplayDescription(), Whitelist.none());
                }else{
                    desc = Jsoup.clean(p.getDisplayDescriptionI18N().get(locale), Whitelist.none());
                }
                descIdx = StringUtils.indexOfIgnoreCase(desc, searchTerm);

                if (descIdx > -1) {
                    final int beginIndex = descIdx - DESC_SNIPPET_PLUS_MINUS < 0  ? 0 : descIdx - DESC_SNIPPET_PLUS_MINUS;
                    final int endIndex = descIdx + DESC_SNIPPET_PLUS_MINUS >= desc.length() ? desc.length() : descIdx + DESC_SNIPPET_PLUS_MINUS;

                    String snippet = (beginIndex == 0 ? "" : "...") + desc.substring(beginIndex, endIndex) + "...";
                    final int snipIdx = StringUtils.indexOfIgnoreCase(snippet, searchTerm);
                    final int snipIdxEnd = snipIdx + termlen;
                    snippet = snippet.substring(0, snipIdx) + "<strong>" + snippet.substring(snipIdx, snipIdxEnd) + "</strong>" +
                        snippet.substring(snipIdxEnd, snippet.length());

                    final QsProd res = new QsProd();
                    res.setId(p.getId());
                    res.setDisplayTitle(title);
                    res.setRawTitle(title);
                    res.setDisplayDescription(snippet);
                    res.setUrlTitle(p.getUrlTitle().replace("${LANG}", locale));
                    res.setFoundInTitle(false);
                    res.setIsDefault(false);
                    results.add(res);
                }
            }
        }

        Collections.sort(results);
        if (!results.isEmpty()) {
            results.get(0).setIsDefault(true);
        }

        return new QuickSearchProductGraph(results.isEmpty() ? QuickSearchProductGraph.QsResult.NotFound : QuickSearchProductGraph.QsResult.Found, results);
    }

}
