package com.enovax.star.cms.api.store.model.partner.ppslm;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 11/15/16.
 */
public class PPSLMTransactionAdapter {

    private String transMsg;
    private Class transClass;
    private String receiptNum;
    private Integer mainAccountId;
    private String username;
    private Boolean subAccountTrans;
    private Date createdDate;
    private Date modifiedDate;
    private BigDecimal totalAmount;
    private String status;
    private String tmStatus;
    private String currency;
    private String paymentType;
    private String tmErrorMessage;
    private String tmApprovalCode;
    private Date validityEndDate;
    private String tmMerchantId;

    private Integer totalMainQty;
    private Integer totalTopupQty;
    private String revalItemCode;
    private String revalTopupItemCode;
    private TelemoneyResponse tmPkg;

    public PPSLMTransactionAdapter(PPSLMDepositTransaction trans) {
        if (trans == null) {
            return;
        }
        this.transMsg = "DepositTransaction:" + trans.getReceiptNum();
        this.transClass = trans.getClass();
        this.receiptNum = trans.getReceiptNum();
        this.mainAccountId = trans.getMainAccountId();
        this.username = trans.getUsername();
        this.subAccountTrans = trans.getSubAccountTrans();
        this.createdDate = trans.getCreatedDate();
        this.modifiedDate = trans.getModifiedDate();
        this.totalAmount = trans.getTotalAmount();
        this.status = trans.getStatus();
        this.tmStatus = trans.getTmStatus();
        this.currency = trans.getCurrency();
        this.paymentType = trans.getPaymentType();
        this.tmErrorMessage = trans.getTmErrorMessage();
        this.tmApprovalCode = trans.getTmApprovalCode();
        this.tmMerchantId = trans.getTmMerchantId();
    }

    public PPSLMTransactionAdapter(PPSLMInventoryTransaction trans) {
        if (trans == null) {
            return;
        }
        this.transMsg = "InventoryTransaction:" + trans.getReceiptNum();
        this.transClass = trans.getClass();
        this.receiptNum = trans.getReceiptNum();
        this.mainAccountId = trans.getMainAccountId();
        this.username = trans.getUsername();
        this.subAccountTrans = trans.getSubAccountTrans();
        this.createdDate = trans.getCreatedDate();
        this.modifiedDate = trans.getModifiedDate();
        this.totalAmount = trans.getTotalAmount();
        this.status = trans.getStatus();
        this.tmStatus = trans.getTmStatus();
        this.currency = trans.getCurrency();
        this.paymentType = trans.getPaymentType();
        this.tmErrorMessage = trans.getTmErrorMessage();
        this.tmApprovalCode = trans.getTmApprovalCode();
        this.validityEndDate = trans.getValidityEndDate();
        this.tmMerchantId = trans.getTmMerchantId();
    }

    public PPSLMTransactionAdapter(PPSLMRevalidationTransaction trans,
                              PPSLMInventoryTransaction otrans, TelemoneyResponse pkg) {
        if (trans == null || otrans == null) {
            return;
        }
        this.transMsg = "RevalidationTransaction:" + trans.getReceiptNum();
        this.transClass = trans.getClass();
        this.receiptNum = trans.getReceiptNum();
        this.mainAccountId = trans.getMainAccountId();
        this.username = trans.getUsername();
        this.subAccountTrans = trans.getSubAccountTrans();
        this.createdDate = trans.getCreatedDate();
        this.modifiedDate = new Date();
        this.totalAmount = trans.getTotal();
        this.status = trans.getStatus();
        this.tmStatus = trans.getTmStatus();
        this.revalItemCode = trans.getRevalItemCode();
        this.totalMainQty = trans.getTotalMainQty();
        this.revalTopupItemCode = trans.getRevalItemCode();
        this.totalTopupQty = trans.getTotalTopupQty();
    }

    public String getTransMsg() {
        return transMsg;
    }

    public void setTransMsg(String transMsg) {
        this.transMsg = transMsg;
    }

    public Class getTransClass() {
        return transClass;
    }

    public void setTransClass(Class transClass) {
        this.transClass = transClass;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean isSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public Integer getTotalMainQty() {
        return totalMainQty;
    }

    public void setTotalMainQty(Integer totalMainQty) {
        this.totalMainQty = totalMainQty;
    }

    public Integer getTotalTopupQty() {
        return totalTopupQty;
    }

    public void setTotalTopupQty(Integer totalTopupQty) {
        this.totalTopupQty = totalTopupQty;
    }

    public String getRevalItemCode() {
        return revalItemCode;
    }

    public void setRevalItemCode(String revalItemCode) {
        this.revalItemCode = revalItemCode;
    }

    public String getRevalTopupItemCode() {
        return revalTopupItemCode;
    }

    public void setRevalTopupItemCode(String revalTopupItemCode) {
        this.revalTopupItemCode = revalTopupItemCode;
    }

    public TelemoneyResponse getTmPkg() {
        return tmPkg;
    }

    public void setTmPkg(TelemoneyResponse tmPkg) {
        this.tmPkg = tmPkg;
    }
}
