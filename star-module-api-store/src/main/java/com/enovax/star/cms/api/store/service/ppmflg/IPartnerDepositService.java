package com.enovax.star.cms.api.store.service.ppmflg;


import com.enovax.payment.tm.model.TmServiceResult;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.DepositVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PagedData;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/13/16.
 */
public interface IPartnerDepositService {

    ApiResult<AxCustomerBalances> getPartnerDepositBalanceWithResponse(String axAccountNumber) throws AxChannelException;

    BigDecimal getPartnerDepositBalance(String axAccountNumber) throws BizValidationException;

    PPMFLGDepositTransaction createTopUpBalanceTxn(PartnerAccount account, PartnerVM partner, BigDecimal amount, String ipAddress, String userSessionId) throws BizValidationException;

    ApiResult<String> updateAxCustomerDepositBalance(String axAccountNumber, String receiptNum) throws Exception;

    PPMFLGDepositTransaction updateTopUpBalanceTxnStatus(String receiptNum, ApiResult<String> axResponse);

    ApiResult<PagedData<List<DepositVM>>> getPagedViewDepositHist(String axCustAccNum, Integer mainAccountId, Date startDate, Date endDate, String sortField, String sortDirection, String depositTxnQueryType, int page, int pageSize) throws Exception;

    void validateTopUpBalanceAmount(BigDecimal amount) throws BizValidationException;

    void refreshWoTReservationListWithDateRange(String axCustNumber, Map<String, Integer> wotList, Date startDateTime, Date endDateTime) throws BizValidationException;

    String populateDepositToupPaymentRedirectURL(String channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException;

    PPMFLGDepositTransaction getDepositTopupTransaction(String tmDataReceiptNumber, String tmDataSessionId, String tmUserField2);

    PPMFLGDepositTransaction saveDepositTransactionForTMFailedProcessing(Integer id, String name, String name1, String ec, String em, Date now, boolean forceFailed) throws BizValidationException;

    PPMFLGDepositTransaction saveDepositTransactionForTMSuccessProcessing(Integer id, String name, String name1, String tmCcNum, String tmApprovalCode, String tmPaymentType, Date now) throws BizValidationException;

    PPMFLGDepositTransaction saveDepositTransactionForTMVoidProcessing(PPMFLGDepositTransaction mflgTxn, TmServiceResult voidResult) throws BizValidationException;

    ApiResult<String> updateAxCustomerDepositBalance(PPMFLGDepositTransaction mflgTxn) throws Exception;

    void sendDepositTopupTransactionReceiptEmail(PPMFLGDepositTransaction txn);

    ApiResult<String> populateDepositToupPaymentStatusInfo(PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String ipAddress, String sessionId) throws BizValidationException;

    void sendVoidEmail(PPMFLGDepositTransaction txn, TmServiceResult voidResult);

    void sendTmQueryForPendingTrans();

    public CheckoutConfirmResult fireLoadTestingResult(String partner_portal_channel, PartnerAccount account, Integer txnId, String receiptNum, String txnStatus, String userRequestAddress, String id) throws BizValidationException;

    public void fireLoadTestingResult(String channel, CheckoutConfirmResult ccr2);

    public String getDepositTopupTransactionTelemoneyReturnURL(String channel);

}
