#Generate Java JAX-WS SOAP services code for AX SOAP API WSDL files
#AX API by NEC encounters this issue for a few of the web services: http://stackoverflow.com/questions/13422253/xjc-two-declarations-cause-a-collision-in-the-objectfactory-class?rq=1

echo "Generating B2B PIN View Line Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_B2BPINViewLineService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline

echo "Generating B2B PIN View Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_B2BPINViewService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.b2bpinview

echo "Generating B2B Retail Ticket Services..."
wsimport -Xnocompile -extension http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50000/Services/SDC_B2BRetailTicketService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.b2bretailticket

echo "Generating Block PIN Redemption Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_BlockPINRedemptionService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.blockpinredemption

echo "Generating Customer Ext Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_CustomerExtService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.customerext -b SDC_CustomerExtService.svc-custom-binding.xjb

echo "Generating Discount Counter Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_DiscountCounterService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.discountcounter

echo "Generating Invent Table Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_InventTableService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.inventtable

echo "Generating Online Cart Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_OnlineCartService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.onlinecart
 -b SDC_OnlineCartService.svc-custom-binding.xjb

echo "Generating Retail Ticket Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_RetailTicketTableService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.retailticket

echo "Generating UpCrossSell Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_UpCrossSellService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.upcrosssell

echo "Generating Use Discount Counter Services..."
wsimport -Xnocompile -extension http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_UseDiscountCounterService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.usediscountcounter

echo "Generating Online Cart Services..."
wsimport -Xnocompile -extension http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50001/Services/SDC_OnlineCartService.svc?wsdl -p com.enovax.star.cms.commons.ws.axchannel.onlinecart -b SDC_OnlineCartService.svc-address-binding.xjb

echo "Generating Customer Ext Services..."
wsimport -Xnocompile -extension http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50000/services/SDC_CustomerExtService.svc?wsdl -keep -XadditionalHeaders -B-XautoNameResolution -p com.enovax.star.cms.commons.ws.axchannel.customerext -b SDC_CustomerExtService.svc-custom-binding-dev3.xjb
