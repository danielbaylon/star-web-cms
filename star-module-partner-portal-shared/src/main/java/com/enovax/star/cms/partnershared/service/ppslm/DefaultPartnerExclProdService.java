package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppslm.PartnerExclProdMapping;
import com.enovax.star.cms.commons.jcrrepository.system.ppslm.IPartnerExclProdMappingRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 23/9/16.
 */
@Service
public class DefaultPartnerExclProdService implements IPartnerExclProdService {
    @Autowired
    IPartnerExclProdMappingRepository partnerExclProdMappingRepository;

    @Autowired
    IPartnerService partnerService;

    @Autowired
    IPartnerAdminProductService prodSrv;

    @Override
    public List<String> getExclProdbyPartner(String channel,Integer partnerId) throws RepositoryException{
        return partnerExclProdMappingRepository.getExclProdbyPartner(channel, partnerId);
    }

    @Override
    public PartnerExclProdMapping getPartnerExclProdMappingbyPartner(String channel,Integer partnerId) throws RepositoryException{
       return partnerExclProdMappingRepository.getPartnerExclProdMappingbyPartner(channel, partnerId);
    }

    @Override
    public PartnerExclProdMapping savePartnerExclProdMapping(String channel,PartnerExclProdMapping partnerExclProdMapping) throws Exception{
        return partnerExclProdMappingRepository.savePartnerExclProdMapping(channel, partnerExclProdMapping);
    }

    @Override
    public PartnerExclProdMapping saveExclProdbyPartner(String channel,Integer partnerId, List<String> exclProdIds) throws Exception{
        return partnerExclProdMappingRepository.saveExclProdbyPartner(channel, partnerId, exclProdIds);
    }

    @Override
    public List<String> getPartnerByExclProd(String channel, String productId) throws RepositoryException {
        return partnerExclProdMappingRepository.getPartnerByExclProd(channel, productId);
    }

    @Override
    public ApiResult<String> processExclusive(String channel, String productId, List<String> partnerIdstoAdd, List<String> partnerIdsToRemove, String userNm) {
        for(String r: partnerIdsToRemove) {
            try {
                PartnerVM partner =  partnerService.getPartnerVmById(Integer.parseInt(r));
                List<PartnerProductVM> excluProds= partner.getExcluProds();
                List<PartnerProductVM> updExcluProds = new ArrayList<>();
                List<String> excluProdIds = new ArrayList<>();
                List<String> updExcluProdIds = new ArrayList<>();
                for(PartnerProductVM prod: excluProds) {
                    excluProdIds.add(prod.getId());
                    if(!productId.equals(prod.getId())) {
                        updExcluProds.add(prod);
                        updExcluProdIds.add(prod.getId());
                    }

                }
                partner.setExcluProds(updExcluProds);
                partner.setExcluProdIds(updExcluProdIds);
                partner.setRemarks("remove Exclusive Item");
                partner.setExcluProds(updExcluProds);
                partnerService.submitEditPa(JsonUtil.jsonify(partner), userNm);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        PartnerProductVM productVM = prodSrv.getProdVmByProdId(StoreApiChannels.fromCode(channel), productId);

        if(productVM != null) {
            for(String a: partnerIdstoAdd) {
                try {
                    PartnerVM partner =  partnerService.getPartnerVmById(Integer.parseInt(a));
                    List<PartnerProductVM> excluProds= partner.getExcluProds();
                    List<String> excluProdIds = new ArrayList<>();

                    if(excluProds != null) {
                        for(PartnerProductVM prod: excluProds) {
                            excluProdIds.add(prod.getId());
                        }
                    }else {
                        excluProds = new ArrayList<>();
                    }

                    for(PartnerProductVM prod: excluProds) {
                        excluProdIds.add(prod.getId());
                    }
                    excluProdIds.add(productId);
                    excluProds.add(productVM);

                    partner.setExcluProds(excluProds);
                    partner.setExcluProdIds(excluProdIds);
                    partner.setRemarks("add Exclusive Item");
                    partnerService.submitEditPa(JsonUtil.jsonify(partner), userNm);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        return new ApiResult<>(true, "", "", "");
    }

}
