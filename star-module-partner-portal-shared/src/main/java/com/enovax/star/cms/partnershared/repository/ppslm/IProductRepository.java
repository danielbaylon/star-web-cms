package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;

import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
public interface IProductRepository {
    public int getProdSize(ProductGridFilterVM filterVM, boolean isExclusive);

    public List<PartnerProductVM> getProdByPage(ProductGridFilterVM filterVM, boolean isExclusive);

    public PartnerProductVM getProd(String channel, String prodId);
}
