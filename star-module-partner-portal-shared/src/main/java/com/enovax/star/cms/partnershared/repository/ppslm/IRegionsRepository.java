package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.model.axchannel.customerext.AxRegion;

import java.util.List;

/**
 * Created by jennylynsze on 12/29/16.
 */
public interface IRegionsRepository {

    boolean initRegion(String channel);

    boolean saveRegion(String channel, AxRegion info);

    List<AxRegion> getRegionList(String channel);

    void deleteRegionIfNotInList(String channel, List<String> regionList);

    void refreshCache(String channel);
}
