package com.enovax.star.cms.partnershared.constant.ppslm;

/**
 * Created by jennylynsze on 5/13/16.
 */
public enum ProductLevels {
    Tiered,
    Exclusive;
}

