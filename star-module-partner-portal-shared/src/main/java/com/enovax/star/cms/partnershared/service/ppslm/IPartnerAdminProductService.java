package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;

import java.util.List;

public interface IPartnerAdminProductService {

    ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels b2cSlm, List<String> productIds) throws Exception;

    com.enovax.star.cms.commons.model.api.ApiResult<List<PartnerProductVM>> getProdVmsByPage(ProductGridFilterVM filter, boolean isExclusive);

    List<PartnerProductVM> getProdVmsByProdIds(ProductGridFilterVM filter, List<String> excluProdIds, boolean isExclusive);

    PartnerProductVM getProdVmByProdId(StoreApiChannels channel, String prodId);
}