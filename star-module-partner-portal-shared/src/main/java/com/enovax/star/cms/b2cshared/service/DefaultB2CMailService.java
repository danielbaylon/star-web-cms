package com.enovax.star.cms.b2cshared.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.jcrrepository.system.IEmailTemplateRepository;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.booking.StoreTransactionExtensionData;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DefaultB2CMailService implements IB2CMailService {

    private Logger log = LoggerFactory.getLogger(DefaultB2CMailService.class);

    public static final String B2C_SLM_RECEIPT_TEMPLATE = "purchase-receipt-b2c-slm";
    public static final String B2C_MFLG_RECEIPT_TEMPLATE = "purchase-receipt-b2c-mflg";

    @Autowired
    private IEmailTemplateRepository mailRepo;
    @Autowired
    private IMailService mailService;
    @Autowired
    private IB2CTransactionService transService;
    @Autowired
    private IB2CCartAndDisplayService cartAndDisplayService;
    @Autowired
    private ITicketGenerationService ticketGenerationService;
    @Autowired
    private IB2CSysParamService paramService;

    private Handlebars handlebars;

    public static final int CACHE_TIME_SECONDS = 60; //TODO Make this configurable?
    private Map<String, TemplateCacheObject> templateCache = new ConcurrentHashMap<>();

    private class TemplateCacheObject {
        private LocalDateTime cacheTs = null;
        private Template template = null;
        private String subject = "";

        public LocalDateTime getCacheTs() {
            return cacheTs;
        }
        public void setCacheTs(LocalDateTime cacheTs) {
            this.cacheTs = cacheTs;
        }
        public Template getTemplate() {
            return template;
        }
        public void setTemplate(Template template) {
            this.template = template;
        }
        public String getSubject() {
            return subject;
        }
        public void setSubject(String subject) {
            this.subject = subject;
        }
    }

    @PostConstruct
    public void init() {
        handlebars = new Handlebars();
    }

    //TODO Consider merging with the partner portal implementation DefaultTemplateService
    //TODO Consider unifying the caching approach here with other data that requires caching

    private TemplateCacheObject getMailTemplateData(StoreApiChannels channel, String key) throws IOException {
        final LocalDateTime now = LocalDateTime.now();

        TemplateCacheObject tco = templateCache.get(key);
        if (tco == null) {
            tco = new TemplateCacheObject();
            tco.setCacheTs(null);
            templateCache.put(key, tco);
        }

        final Template theTemplate;

        final boolean shouldUpdate = tco.getCacheTs() == null || tco.getCacheTs().plusSeconds(CACHE_TIME_SECONDS).isBefore(now);
        if (shouldUpdate) {
            final SysEmailTemplateVM templateData = mailRepo.getSystemParamByKey(channel.code, key);
            theTemplate = handlebars.compileInline(templateData.getBody());
            tco.setCacheTs(now);
            tco.setTemplate(theTemplate);
            tco.setSubject(templateData.getSubject());
        }

        return tco;
    }

    private String generateHtml(Template template, Object model) throws IOException {
        final Context ctx = Context.newContext(model);
        return template.apply(ctx);
    }

    @Override
    @Transactional
    public void sendAndUpdateEmailForB2CBookingReceipt(StoreApiChannels channel, String receiptNumber,
                                                       boolean updateEmail, String customEmailAddress) throws Exception {
        final StoreTransactionExtensionData ext = transService.getStoreTransactionExtensionData(channel, receiptNumber);
        final StoreTransaction txn = ext.getTxn();
        
        if (StringUtils.isNotEmpty(customEmailAddress)) {
            txn.setCustEmail(customEmailAddress);
            if (updateEmail) {
                switch (channel) {
                    case B2C_SLM:
                        final B2CSLMStoreTransaction slmTxn = transService.getB2CSLMStoreTransaction(receiptNumber);
                        slmTxn.setCustEmail(customEmailAddress);
                        transService.saveB2CSLMStoreTransaction(slmTxn);
                        final StoreTransactionExtensionData slmDataForUpdate = new StoreTransactionExtensionData();
                        slmDataForUpdate.setReceiptNumber(receiptNumber);
                        slmDataForUpdate.setTxn(txn);
                        transService.updateStoreTransactionExtensionData(channel, slmDataForUpdate);
                        break;
                    case B2C_MFLG:
                        final B2CMFLGStoreTransaction mflgTxn = transService.getB2CMFLGtoreTransaction(receiptNumber);
                        mflgTxn.setCustEmail(customEmailAddress);
                        transService.saveB2CMFLGStoreTransaction(mflgTxn);
                        final StoreTransactionExtensionData mflgDataForUpdate = new StoreTransactionExtensionData();
                        mflgDataForUpdate.setReceiptNumber(receiptNumber);
                        mflgDataForUpdate.setTxn(txn);
                        transService.updateStoreTransactionExtensionData(channel, mflgDataForUpdate);
                        break;
                }
            }
        }
        
        final ReceiptDisplay receiptDisplay = cartAndDisplayService.getReceiptForDisplay(txn);
        
        sendEmailForB2CBookingReceipt(channel, txn, receiptDisplay);
    }

    @Override
    public void sendEmailForB2CBookingReceipt(StoreApiChannels channel, StoreTransaction txn,
                                              ReceiptDisplay receiptDisplay) throws Exception {
        final String trackingId = channel.code + "-" + txn.getReceiptNumber() + "-" + System.currentTimeMillis();
        log.info("Preparing email sending for task: " + trackingId);

        log.info("Retrieving email template data.");
        String templateKey;
        switch (channel) {
            case B2C_SLM:
                templateKey = B2C_SLM_RECEIPT_TEMPLATE;
                break;
            case B2C_MFLG:
                templateKey = B2C_MFLG_RECEIPT_TEMPLATE;
                break;
            default:
                log.error("[CRITICAL] Unsupported channel.");
                throw new Exception("[CRITICAL] Unsupported channel.");
        }

        final TemplateCacheObject tco;
        try {

            tco = getMailTemplateData(channel, templateKey);
        } catch (Exception e) {
            log.error("[CRITICAL] Error retrieving mail template date. " + e.getMessage(), e);
            throw new Exception("[CRITICAL] Error retrieving mail template date. " + e.getMessage(), e);
        }

        final MailProperties props = new MailProperties();
        props.setTrackingId(trackingId);

        props.setSender(mailService.getDefaultSender());
        props.setSubject(tco.getSubject().replace("{{receiptNumber}}", txn.getReceiptNumber()));
        props.setToUsers(new String[]{ txn.getCustEmail() });

        if (!txn.isPinToGenerate()) {
            log.info("Generating eTicket attachment.");
            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(txn.geteTickets());

            final byte[] outputBytes = ticketGenerationService.generateTickets(channel, ticketDataCompiled);
            final String eticketsFileName =  "eticket_" + txn.getReceiptNumber() + FileUtil.FILE_EXT_PDF;
            final String outputFilePath = paramService.getSystemParamValueByKey(channel,
                    DefaultB2CSysParamService.SYS_PARAM_B2C_TRANSACTION_RECEIPT_DIR) + "/" + eticketsFileName;

            File file = new File(outputFilePath);
            FileOutputStream os = new FileOutputStream(file);
            try {
                os.write(outputBytes);
            } finally {
                os.close();
            }

            props.setAttachFilePaths(new String[]{ outputFilePath });
            props.setAttachNames(new String[]{ eticketsFileName });
            props.setHasAttach(true);
        } else {
            log.info("Generating barcode image.");
            final String barcodeFileName =  "barcode_" + receiptDisplay.getPinCode() + FileUtil.IMG_EXT_PNG;
            final String outputFilePath = paramService.getSystemParamValueByKey(channel,
                    DefaultB2CSysParamService.SYS_PARAM_B2C_TRANSACTION_RECEIPT_DIR) + "/" + barcodeFileName;
            BarcodeGenerator.toBase64(receiptDisplay.getPinCode(), 300, 75, outputFilePath);
            final String barcodeCid = "barcodeCid" + receiptDisplay.getPinCode();

            props.setAttachFilePaths(new String[]{ outputFilePath });
            props.setAttachNames(new String[]{ barcodeFileName });
            props.setAttachFileContentTypes(new String[] { "<" + barcodeCid + ">" });
            props.setHasAttach(true);

            receiptDisplay.setBarcodeImage("<img width=\"300\" height=\"75\" src=\"cid:" + barcodeCid + "\" />");
        }

        log.info("Generating email body.");
        final String emailBody;
        try {

            emailBody = generateHtml(tco.getTemplate(), receiptDisplay);
        } catch (Exception e) {
            log.error("[CRITICAL] Unable to generate receipt template. " + e.getMessage(), e);
            throw new Exception("[CRITICAL] Unable to generate receipt template. " + e.getMessage(), e);
        }

        props.setBody(emailBody);
        props.setBodyHtml(true);

        //TODO Still need to generate receipt PDF?

        mailService.sendEmail(props, new MailCallback() {
            @Override
            public void complete(String trackingId, boolean success) {
                log.info("Email job for B2C booking receipt with ID " + trackingId + " resulted in " + (success ? "SUCCESS." : "FAILURE."));
            }
        });
    }
}
