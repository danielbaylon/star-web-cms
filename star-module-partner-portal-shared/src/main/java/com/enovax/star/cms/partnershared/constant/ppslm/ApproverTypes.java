package com.enovax.star.cms.partnershared.constant.ppslm;

public enum ApproverTypes {
    General("G","General Approver"),
    GeneralBackup("G_B","General Backup Approver");
    private static final String SUFFIX = "_B";
    
    public final String code;
    public final String label;
    
    private ApproverTypes(String code, String label) {
        this.code = code;
        this.label = label;
    }
    
    public static String getBackupCode(String code){
        return code+SUFFIX;
    }
    public static String getApproverType(String code){
        if(code.endsWith(SUFFIX)){
            return code.replaceAll(SUFFIX, "");
        }else{
            return code;   
        }
    }
    public static String getLabel(String code){
        for(ApproverTypes tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
}
