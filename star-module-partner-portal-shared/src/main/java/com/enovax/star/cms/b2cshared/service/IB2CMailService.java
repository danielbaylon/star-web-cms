package com.enovax.star.cms.b2cshared.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;

/**
 * Created by jensen on 15/11/16.
 */
public interface IB2CMailService {

    void sendAndUpdateEmailForB2CBookingReceipt(StoreApiChannels channel, String receiptNumber, boolean updateEmail, String customEmailAddress) throws Exception;

    void sendEmailForB2CBookingReceipt(StoreApiChannels channel, StoreTransaction txn,
                                       ReceiptDisplay receiptDisplay) throws Exception;
}
