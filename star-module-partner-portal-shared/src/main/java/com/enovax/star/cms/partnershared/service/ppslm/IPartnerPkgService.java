package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;

import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public interface IPartnerPkgService {

    ResultVM getBundledItemsByTransItemIds(String transItemIdsStr,
                                           String pkgName, Integer pkgQtyPerProd, String pkgTktType,
                                           Integer itemIdToAdd, String pkgDesc, String pkgTktMedia);

    //Todo removed the "TAMainAccount"
    ResultVM createPkg(String transItemIdsStr, String pkgName,
                       Integer pkgQtyPerProd, String pkgTktType, PartnerAccount account, String pkgDesc, String pkgTktMedia);

    ResultVM getPkgsByPage(
            Integer adminId, String fromDateStr, String toDateStr,
            String status, String pkgTktMedia, String pkgNm, String orderField,
            String orderWith, Integer pageNumber, Integer pageSize);

    MixMatchPkgVM getPkgByID(Integer pkgId);

    //
    ResultVM verifyPkg(String transItemIdsStr, String pkgName,
                       String partnerCode, Integer pkgQtyPerProd, String pkgTktType, String pkgTktMedia);

    ResultVM genETicket(Integer pkgId, String customerId);


    List<ETicketData> getETickets(Integer pkgId);


    ResultVM genExcelTicket(Integer pkgId, String customerId);


    List<ETicketData> getExcelTickets(Integer pkgId);

    ResultVM genPinCode(Integer pkgId);

    ResultVM checkRdmStatus(Integer pkgId);

    ResultVM checkRdmStatus(Integer pkgId, boolean updLastCheckDate);

    void removeReservedPkg(List<Integer> pkgIds);

    String sendPincodeEmail(Integer pkgId);

    String sendEticketEmail(Integer pkgId);

    String sendExcelEmail(Integer pkgId);

    ApiResult<String> deactivatePkg(Integer pkgId, String userNm);
//
    void removeOverduePkgs();
//
     void updateExpiredStatus();

    void updatePinStatus();

    String resendingPkgReceiptEmailByPkgId(int pkgId) throws BizValidationException;
//
//    ResultVM getPkgsByPage(PinQueryFilterVM queryFilter, String orderField,
//                           String orderWith, Integer pageNumber, Integer pageSize);
//
//    public MixMatchPackage getPkgByPinCode(String pinCode);
//
//    public PinReconLog processPinReconFile(String processDirStr,String fileName);
//
//    public List<PinReconLog> processPinReconFiles();
//
//    public void emailPinBatchInfo(List<PinReconLog> logs) throws MessagingException;
//
//    public void archivePinFile();
//
}
