package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by lavanya on 9/11/16.
 */
@Repository("PPMFLGReportDao")
public class ReportDaoImpl implements ReportDao {

    private static final Logger log = LoggerFactory.getLogger(ReportDaoImpl.class);

    @Autowired(required = false)
    @Qualifier("sessionFactory")
    SessionFactory sessionFactory;

    public SQLQuery prepareSqlQuery(String query, Map<String, Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        for (String key : parameters.keySet()) {
            Object para = parameters.get(key);
            if (para instanceof List<?>) {
                queryObject.setParameterList(key, (List<?>)para);
            } else {
                queryObject.setParameter(key, para);
            }
        }
        return queryObject;
    }

    public SQLQuery prepareSqlQuery(String query, List<Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        int i=0;
        for (Object para : parameters) {
            queryObject.setParameter(i, para);
            i++;
        }
        return queryObject;
    }

    private Object executeSqlQueryScalar(String query, List<Object> params) {
        SQLQuery qry = prepareSqlQuery(query, params);
        return qry.uniqueResult();
    }


    private static final String SQL_TRANS_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, rt.receiptNum as receiptNum, rt.createdDate as txnDate, " +
                    "null as expiryDate, null as revalExpiryDate, 'Revalidation Admin Charge' as itemDesc, " +
                    "rt.totalMainQty as qty, rt.revalFeeInCents as priceInCents,  " +
                    "rt.totalMainQty * rt.revalFeeInCents as totalAmountCents, rt.username as purchasedBy, " +
                    "it.receiptNum as revalidateReference, rt.status as transStatus, rt.tmStatus as tmStatus, " +
                    "case  " +
                    "when rt.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when rt.status in ('Available', 'Revalidated', 'Refunded', 'Expired', 'Expiring') then 'Successful' " +
                    "when rt.status in ('Failed') then 'Failed' " +
                    "end as transStatusText, 'Revalidation Item Fee Code: ' + cast(rt.revalItemCode as varchar(10)) as remarks," +
                    "1 as reval, " +
                    "rt.paymentType, opr.status as offlinePaymentStatus, opr.approveDate as offlinePayApproveDate, " +
                    "opr.id as offlinePaymentId, opr.paymentDate, opr.referenceNum  " +
                    "from PPMFLGRevalidationTransaction rt " +
                    "inner join PPMFLGInventoryTransaction it on it.id = rt.transactionId " +
                    "inner join PPMFLGTAMainAccount acct on rt.mainAccountId = acct.id " +
                    "inner join PPMFLGPartner partner on acct.id = partner.adminId " +
                    "left  join PPMFLGOfflinePaymentRequest opr on opr.transactionId = rt.id and opr.transactionId = -1 " +
                    "where 1 = 1 " +
                    "{{revalFilterClause}} " +
                    "union all " +
                    "select partner.orgName as orgName, it.receiptNum as receiptNum, it.createdDate as txnDate,  " +
                    "isnull(rt.oldValidityEndDate, it.validityEndDate) as expiryDate, rt.newValidityEndDate as revalExpiryDate, " +
                    "item.displayName as itemDesc, item.qty as qty, item.unitPrice * 100 as priceInCents,  " +
                    "item.qty * item.unitPrice * 100 as totalAmountCents, it.username as purchasedBy,  " +
                    "isnull(rt.receiptNum, '') as revalidateReference, it.status as transStatus, it.tmStatus as tmStatus, " +
                    "case  " +
                    "when it.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when it.status in ('Available', 'Revalidated', 'Expired', 'Expiring') then 'Successful' " +
                    "when it.status in ('Refunded') then 'Refunded' " +
                    "when it.status in ('Failed') then 'Failed' " +
                    "end as transStatusText, '' as remarks," +
                    "0 as reval, " +
                    "case it.paymentType when 'OfflinePayment' then opr.paymentType else it.paymentType end paymentType, " +
                    "opr.status as offlinePaymentStatus, opr.approveDate as offlinePayApproveDate, "+
                    "opr.id offlinePaymentId, opr.paymentDate, opr.referenceNum "+
                    "from PPMFLGInventoryTransaction it " +
                    "inner join PPMFLGTAMainAccount acct on it.mainAccountId = acct.id " +
                    "inner join PPMFLGPartner partner on acct.id = partner.adminId " +
                    "inner join PPMFLGInventoryTransactionItem item on it.id = item.transactionId " +
                    "left join PPMFLGRevalidationTransaction rt on it.id = rt.transactionId " +
                    "left join PPMFLGOfflinePaymentRequest opr on opr.transactionId = it.id " +
                    "where 1 = 1 " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_INVALIDATE_CLAUSE = " and 1 = 0 ";
    private static final String SQL_TR_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_TR_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";

    private static final String SQL_TR_CLAUSE_TXN_DT_REVAL = " and rt.createdDate >= :fromDate and rt.createdDate - 1 <= :toDate ";
    private static final String SQL_TR_CLAUSE_RECEIPT_REVAL = " and rt.receiptNum = :receiptNum ";
    private static final String SQL_TR_CLAUSE_STATUS_REVAL = " and rt.status in (:transStatuses) ";

    private static final String SQL_TR_CLAUSE_TXN_DT = " and it.createdDate >= :fromDate and it.createdDate - 1 <= :toDate ";
    private static final String SQL_TR_CLAUSE_RECEIPT = " and it.receiptNum = :receiptNum ";
    private static final String SQL_TR_CLAUSE_STATUS = " and it.status in (:transStatuses) ";
    private static final String SQL_TR_CLAUSE_ITEM_NAME = " and it.id in (select distinct item2.transactionId from PPMFLGInventoryTransactionItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_TR_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    public ReportResult<TransReportModel> generateTransReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<TransReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_TR_CLAUSE_TXN_DT);
        final StringBuilder revalSb = new StringBuilder(SQL_TR_CLAUSE_TXN_DT_REVAL);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_TR_CLAUSE_ACCOUNT_ID);
            revalSb.append(SQL_TR_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_TR_CLAUSE_ORG_NAME);
            revalSb.append(SQL_TR_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            params.put("paIds", filter.getPaIds() );
        }

        if (StringUtils.isNotEmpty(filter.getReceiptNum())) {
            invSb.append(SQL_TR_CLAUSE_RECEIPT);
            revalSb.append(SQL_TR_CLAUSE_RECEIPT_REVAL);

            params.put("receiptNum",  filter.getReceiptNum());
        }

        if(filter.getPaymentType() != null && filter.getPaymentType().trim().length() > 0){
            if("Online".equalsIgnoreCase(filter.getPaymentType())){
                invSb.append("   and opr.id is null and ( it.paymentType is null or it.paymentType <> 'OfflinePayment' ) ");
                revalSb.append(" and opr.id is null and ( rt.paymentType is null or rt.paymentType <> 'OfflinePayment' ) ");
                if (filter.getTransMegaStatus() != null) {
                    final List<String> statuses = TransMegaStatus.getChildStatuses(filter.getTransMegaStatus());
                    if(statuses != null && statuses.size() > 0){
                        invSb.append(SQL_TR_CLAUSE_STATUS);
                        revalSb.append(SQL_TR_CLAUSE_STATUS_REVAL);
                        params.put("transStatuses", statuses);
                    }
                }
            }else if("Offline".equalsIgnoreCase(filter.getPaymentType())){
                invSb.append("   and opr.id is not null and ( it.paymentType is not null or it.paymentType = 'OfflinePayment' ) ");
                revalSb.append(" and opr.id is not null and ( rt.paymentType is not null or rt.paymentType = 'OfflinePayment' ) ");
                if(filter.getStatus() != null && filter.getStatus().trim().length() > 0){
                    invSb.append("   and opr.status is not null and opr.status = :oprStatus ");
                    revalSb.append(" and opr.status is not null and opr.status = :oprStatus ");
                    params.put("oprStatus", filter.getStatus().trim());
                }
            }
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_TR_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

            if (StringUtils.isEmpty(filter.getRevalItem())) {
                revalSb.setLength(0);
                revalSb.append(SQL_INVALIDATE_CLAUSE);
            }
        } else {
            if (!StringUtils.isEmpty(filter.getRevalItem())) {
                invSb.setLength(0);
                invSb.append(SQL_INVALIDATE_CLAUSE);
            }
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_TR_PA_CLAUSE);
            revalSb.append(SQL_TR_PA_CLAUSE);
        }

        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "receiptNum":
                    sqlOrderBy = " order by tbl.receiptNum "; break;
                case "txnDate":
                case "txnDateText":
                    sqlOrderBy = " order by tbl.txnDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "revalExpiryDate":
                case "revalExpiryDateText":
                    sqlOrderBy = " order by tbl.revalExpiryDate "; break;
                case "itemDesc":
                    sqlOrderBy = " order by tbl.itemDesc "; break;
                case "purchasedBy":
                    sqlOrderBy = " order by tbl.purchasedBy "; break;
                case "revalidateReference":
                    sqlOrderBy = " order by tbl.revalidateReference "; break;
                case "transStatus":
                case "transStatusText":
                    sqlOrderBy = " order by tbl.transStatusText "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_TRANS_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{revalFilterClause}}", revalSb.toString())
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("receiptNum", StandardBasicTypes.STRING)
                .addScalar("txnDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("revalExpiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("itemDesc", StandardBasicTypes.STRING)
                .addScalar("qty", StandardBasicTypes.INTEGER)
                .addScalar("priceInCents", StandardBasicTypes.INTEGER)
                .addScalar("totalAmountCents", StandardBasicTypes.INTEGER)
                .addScalar("purchasedBy", StandardBasicTypes.STRING)
                .addScalar("revalidateReference", StandardBasicTypes.STRING)
                .addScalar("transStatus", StandardBasicTypes.STRING)
                .addScalar("tmStatus", StandardBasicTypes.STRING)
                .addScalar("transStatusText", StandardBasicTypes.STRING)
                .addScalar("remarks", StandardBasicTypes.STRING)
                .addScalar("reval", StandardBasicTypes.INTEGER)
                .addScalar("paymentType", StandardBasicTypes.STRING)
                .addScalar("offlinePaymentStatus", StandardBasicTypes.STRING)
                .addScalar("offlinePaymentId", StandardBasicTypes.INTEGER)
                .addScalar("offlinePayApproveDate", StandardBasicTypes.DATE)
                .addScalar("paymentDate", StandardBasicTypes.DATE)
                .addScalar("referenceNum", StandardBasicTypes.STRING)
                .setResultTransformer(Transformers.aliasToBean(TransReportModel.class));

        final List<TransReportModel> theList = qry.list();
        for (TransReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }

    private static final String SQL_INV_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, it.receiptNum as receiptNum, it.createdDate as txnDate, " +
                    "it.validityEndDate as expiryDate, item.displayName as itemDesc, item.qty as qtyPurchased,  " +
                    "item.qty - item.unpackagedQty as qtyPackaged, item.unpackagedQty as balance,  " +
                    "isnull(rt.receiptNum, '') as revalidateReference, it.status as transStatus, it.tmStatus as tmStatus, " +
                    "case  " +
                    "when it.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when it.status in ('Available', 'Revalidated', 'Refunded', 'Expired', 'Expiring') then 'Successful' " +
                    "when it.status in ('Failed') then 'Failed' " +
                    "end as transStatusText " +
                    "from PPMFLGInventoryTransaction it " +
                    "inner join PPMFLGTAMainAccount acct on it.mainAccountId = acct.id " +
                    "inner join PPMFLGPartner partner on acct.id = partner.adminId " +
                    "inner join PPMFLGInventoryTransactionItem item on it.id = item.transactionId " +
                    "left join PPMFLGRevalidationTransaction rt on it.id = rt.transactionId " +
                    "where 1 = 1 " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_IR_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_IR_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";

    private static final String SQL_IR_CLAUSE_TXN_DT = " and it.createdDate >= :fromDate and it.createdDate - 1 <= :toDate ";
    private static final String SQL_IR_CLAUSE_RECEIPT = " and it.receiptNum = :receiptNum ";
    private static final String SQL_IR_CLAUSE_STATUS = "and it.status in (:statuses) ";
    private static final String SQL_IR_CLAUSE_ITEM_NAME = " and it.id in (select distinct item2.transactionId from PPMFLGInventoryTransactionItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_IR_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    @SuppressWarnings("unchecked")
    public ReportResult<InventoryReportModel> generateInvReport(ReportFilterVM filter, Integer skip, Integer take,
                                                             String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<InventoryReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_IR_CLAUSE_TXN_DT);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_IR_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_IR_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (StringUtils.isNotEmpty(filter.getReceiptNum())) {
            invSb.append(SQL_IR_CLAUSE_RECEIPT);

            params.put("receiptNum",  filter.getReceiptNum());
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_IR_PA_CLAUSE);
            params.put("paIds", filter.getPaIds() );
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_IR_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

        }

        invSb.append(SQL_IR_CLAUSE_STATUS);
        final List<String> statuses;
        if (StringUtils.isNotEmpty(filter.getStatus())) {
            statuses = Arrays.asList(filter.getStatus());
        } else {
            statuses = Arrays.asList("Available", "Revalidated", "Refunded", "Expired", "Expiring", "Forfeited");
        }
        params.put("statuses", statuses);

        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "receiptNum":
                    sqlOrderBy = " order by tbl.receiptNum "; break;
                case "txnDate":
                case "txnDateText":
                    sqlOrderBy = " order by tbl.txnDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "itemDesc":
                    sqlOrderBy = " order by tbl.itemDesc "; break;
                case "revalidateReference":
                    sqlOrderBy = " order by tbl.revalidateReference "; break;
                case "transStatus":
                    sqlOrderBy = " order by tbl.transStatus "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_INV_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("receiptNum", StandardBasicTypes.STRING)
                .addScalar("txnDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("itemDesc", StandardBasicTypes.STRING)
                .addScalar("qtyPurchased", StandardBasicTypes.INTEGER)
                .addScalar("qtyPackaged", StandardBasicTypes.INTEGER)
                .addScalar("balance", StandardBasicTypes.INTEGER)
                .addScalar("revalidateReference", StandardBasicTypes.STRING)
                .addScalar("transStatus", StandardBasicTypes.STRING)
                .addScalar("tmStatus", StandardBasicTypes.STRING)
                .addScalar("transStatusText", StandardBasicTypes.STRING)
                .setResultTransformer(Transformers.aliasToBean(InventoryReportModel.class));

        final List<InventoryReportModel> theList = qry.list();
        for (InventoryReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }

    private static final String SQL_PIN_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, pkg.pinCode as pinCode, pkg.ticketGeneratedDate as ticketGeneratedDate, " +
                    "pkg.ticketMedia as ticketMedia, pkg.expiryDate as expiryDate, pkg.name as packageName, pkg.description as packageDesc, pkg.qty as pinCodeQty,  " +
                    "pkg.qtyRedeemed as qtyRedeemed, pkg.lastRedemptionDate as lastRedemptionDate, pkg.id as pkgId, " +
                    "stuff((select ', ' + item.displayName  " +
                    "from PPMFLGMixMatchPackageItem item  " +
                    "where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as indivItems, " +
                    "stuff((select ', ' + item.receiptNum  " +
                    "from PPMFLGMixMatchPackageItem item  " +
                    "where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as indivReceipts, " +
                    "stuff((select ', ' + STR(item.qty) " +
                    "from PPMFLGMixMatchPackageItem item where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as itemQty," +
                    "stuff((select ', ' + STR(item.qtyRedeemed) " +
                    "from PPMFLGMixMatchPackageItem item where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as itemQtyRedeemed "+
                    "from PPMFLGMixMatchPackage pkg " +
                    "inner join PPMFLGTAMainAccount acct on pkg.mainAccountId = acct.id " +
                    "inner join PPMFLGPartner partner on acct.id = partner.adminId " +
                    "where 1 = 1" +
                    "and pkg.status in ('Available', 'Expired', 'Redeemed') " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_PIN_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_PIN_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";
    private static final String SQL_PIN_CLAUSE_TICKET_MEDIA = " and pkg.ticketMedia = :ticketMedia ";
    private static final String SQL_PIN_CLAUSE_TXN_DT = " and pkg.ticketGeneratedDate >= :fromDate and pkg.ticketGeneratedDate - 1 <= :toDate ";
    private static final String SQL_PIN_CLAUSE_ITEM_NAME = " and pkg.id in (select distinct item2.packageId from PPMFLGMixMatchPackageItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_PIN_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    @SuppressWarnings("unchecked")
    public ReportResult<PackageReportModel> generatePkgReport(ReportFilterVM filter, Integer skip, Integer take,
                                                           String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<PackageReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_PIN_CLAUSE_TXN_DT);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_PIN_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_PIN_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (StringUtils.isNotEmpty(filter.getTicketMedia())) {
            invSb.append(SQL_PIN_CLAUSE_TICKET_MEDIA);

            params.put("ticketMedia", filter.getTicketMedia());
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_PIN_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

        }
        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_PIN_PA_CLAUSE);
            params.put("paIds", filter.getPaIds() );
        }
        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "ticketGeneratedDate":
                    sqlOrderBy = " order by tbl.ticketGeneratedDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "lastRedemptionDate":
                case "lastRedemptionDateText":
                    sqlOrderBy = " order by tbl.lastRedemptionDate "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_PIN_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("pinCode", StandardBasicTypes.STRING)
                .addScalar("ticketGeneratedDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("packageName", StandardBasicTypes.STRING)
                .addScalar("packageDesc", StandardBasicTypes.STRING)
                .addScalar("pinCodeQty", StandardBasicTypes.INTEGER)
                .addScalar("qtyRedeemed", StandardBasicTypes.INTEGER)
                .addScalar("lastRedemptionDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("pkgId", StandardBasicTypes.INTEGER)
                .addScalar("indivItems", StandardBasicTypes.STRING)
                .addScalar("indivReceipts", StandardBasicTypes.STRING)
                .addScalar("ticketMedia",StandardBasicTypes.STRING)
                .addScalar("itemQty", StandardBasicTypes.STRING)
                .addScalar("itemQtyRedeemed",StandardBasicTypes.STRING)
                .setResultTransformer(Transformers.aliasToBean(PackageReportModel.class));

        final List<PackageReportModel> theList = qry.list();
        for (PackageReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }

    private static final String WHERE_CLAUSE = "##WHERE_CLAUSE##";
    @Override
    public List<ExceptionRptDetailsVM> getFinReconPage(ExceptionRptFilterVM fliter, String sortField, String sortDirection, Integer page, Integer pageSize) {
        String commonSQy = FINAL_SELECT
                +  " ( select  mmp.id pkgId, mmp.pinCode, mmp.ticketMedia,"
                + " pa.orgName,pa.accountCode "
                + " FROM  dbo.PPMFLGMixMatchPackage mmp  "
                + " left outer join dbo.PPMFLGMixMatchPackageItem mmpi on mmpi.packageId = mmp.id "
                + " left outer join dbo.PPMFLGPartner pa on pa.adminId = mmp.mainAccountId  "
                + " left outer join dbo.PPMFLGInventoryTransactionItem transItem on mmpi.transactionItemId = transItem.id  "
                + " left outer join dbo.PPMFLGInventoryTransaction itran on transItem.transactionId = itran.id  "
                + " where mmp.status != 'Failed' "
                +WHERE_CLAUSE
                + " group by mmp.id , mmp.pinCode,mmp.ticketMedia,pa.orgName,pa.accountCode) tbl";
        String tempSql = this.makeQuery(commonSQy,fliter);
        tempSql = tempSql.replace(FINAL_SELECT, FINAL_SELECT_ALL);
        tempSql = tempSql + ORDER_BY_CLAUSE;
        if (sortField != null && sortDirection != null
                && !"".equals(sortField)
                && !"".equals(sortDirection)) {
            tempSql = tempSql + " " + sortField + " "
                    + sortDirection;
        } else {
            tempSql = tempSql + " orgName";
        }
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(tempSql);
        this.groupCommonQuery(query, fliter);
        if (page != null && pageSize != null) {
            query.setFirstResult((page - 1) * pageSize);
            query.setMaxResults(pageSize);
        }

        query.addScalar("pkgId", StandardBasicTypes.INTEGER);
        query.addScalar("pinCode", StandardBasicTypes.STRING);
        query.addScalar("orgName", StandardBasicTypes.STRING);
        query.addScalar("accountCode", StandardBasicTypes.STRING);
        query.addScalar("ticketMedia",StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers
                .aliasToBean(ExceptionRptDetailsVM.class));
        return query.list();
    }

    @Override
    public int getFinReconSize(ExceptionRptFilterVM fliter) {
        String commonSQy = FINAL_SELECT
                +  " ( select  mmp.id pkgId, mmp.pinCode, mmp.ticketMedia,"
                + " pa.orgName,pa.accountCode "
                + " FROM  dbo.PPMFLGMixMatchPackage mmp  "
                + " left outer join dbo.PPMFLGMixMatchPackageItem mmpi on mmpi.packageId = mmp.id "
                + " left outer join dbo.PPMFLGPartner pa on pa.adminId = mmp.mainAccountId  "
                + " left outer join dbo.PPMFLGInventoryTransactionItem transItem on mmpi.transactionItemId = transItem.id  "
                + " left outer join dbo.PPMFLGInventoryTransaction itran on transItem.transactionId = itran.id  "
                + " where mmp.status != 'Failed' "
                +WHERE_CLAUSE
                + " group by mmp.id , mmp.pinCode,mmp.ticketMedia,pa.orgName,pa.accountCode) tbl";
        String tempSql = this.makeQuery(commonSQy,fliter);
        tempSql = tempSql.replace(FINAL_SELECT, FINAL_SELECT_COUNT);
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(tempSql);
        query = this.groupCommonQuery(query, fliter);
        return (Integer) query.uniqueResult();
    }

    @Override
    public List<ExceptionRptDetailsVM>  getFinReconBaseResult() {
        String varianceSQL = " select  mmp.id pkgId,mmpi.itemProductCode,SUM(mmpi.qty) purchasedQty, SUM(mmpi.qtyRedeemed) qtyRedeemed,"
                + " SUM(mmpi.qty) as pinCodeQty FROM [dbo].[PPMFLGMixMatchPackageItem] mmpi"
                + " left outer join dbo.PPMFLGMixMatchPackage mmp on mmpi.packageId = mmp.id  "
                + " where mmp.status != 'Failed' group by mmp.id ,mmpi.itemProductCode , mmp.qtyRedeemed, mmp.qty"
                + " having SUM(mmpi.qtyRedeemed) > SUM(mmpi.qty)";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(varianceSQL);

        query.addScalar("pkgId", StandardBasicTypes.INTEGER);
        query.addScalar("itemProductCode", StandardBasicTypes.STRING);
        query.addScalar("purchasedQty", StandardBasicTypes.INTEGER);
        query.addScalar("pinCodeQty", StandardBasicTypes.INTEGER);
        query.addScalar("qtyRedeemed", StandardBasicTypes.INTEGER);

        query.setResultTransformer(Transformers
                .aliasToBean(ExceptionRptDetailsVM.class));
        return query.list();
    }

    private SQLQuery groupCommonQuery(SQLQuery query, ExceptionRptFilterVM fliter) {
        if (fliter.getStartDate() != null) {
            query.setDate("fromDt", fliter.getStartDate());
        }
        if (fliter.getToDate() != null) {
            query.setDate("toDt", fliter.getToDate());
        }
        if (fliter.getOrgName() != null && !"".equals(fliter.getOrgName())) {
            query.setString("orgName", "%" + fliter.getOrgName() + "%");
        }
        if (fliter.getProdDesc() != null && !"".equals(fliter.getProdDesc())) {
            query.setString("prodDesc", "%" + fliter.getProdDesc() + "%");
        }
        if (fliter.getReceiptNum() != null && !"".equals(fliter.getReceiptNum())) {
            query.setString("receiptNum", "%" + fliter.getReceiptNum() + "%");
        }
        if (fliter.getPkgIds()!= null ) {
            query.setParameterList("pkgIds", fliter.getPkgIds());
        }
        if (fliter.getPaIds() != null && fliter.getPaIds().length>0) {
            query.setParameterList("paIds", fliter.getPaIds() );
        }
        return query;
    }

    private String makeQuery(String sqlStr,ExceptionRptFilterVM fliter) {
        String criteria = "";
        if (fliter.getStartDate() != null) {
            criteria = criteria + " and  itran.createdDate > :fromDt";
        }
        if (fliter.getToDate() != null) {
            criteria = criteria + " and itran.createdDate - 1 < :toDt";
        }
        if (fliter.getOrgName() != null && !"".equals(fliter.getOrgName())) {
            criteria = criteria + " and pa.orgName like :orgName";
        }
        if (fliter.getProdDesc() != null && !"".equals(fliter.getProdDesc())) {
            criteria = criteria + " and mmpi.displayName like :prodDesc";
        }
        if (fliter.getReceiptNum() != null && !"".equals(fliter.getReceiptNum())) {
            criteria = criteria + " and mmpi.receiptNum like :receiptNum";
        }
        if (fliter.getPkgIds()!= null ) {
            criteria = criteria + " and (mmp.id in ( :pkgIds ) or mmpi.qty > transItem.qty  )";
        }else{
            criteria = criteria + " and mmpi.qty > transItem.qty  ";
        }
        if (fliter.getPaIds() != null && fliter.getPaIds().length>0) {
            criteria = criteria + " and pa.id in (:paIds)";
        }
        return sqlStr.replace(WHERE_CLAUSE, criteria);
    }


    /**
     * begin: TransQuery
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    private static final String TRANS_WHERE_CLAUSE = "##TRANS_WHERE_CLAUSE##";
    private static final String RETRANS_WHERE_CLAUSE = "##RETRANS_WHERE_CLAUSE##";
    private static final String TRANS_ALIAS = "it.";
    private static final String RETRAN_ALIAS = "rt.";
    private static final String TABLE_ALIAS = "##ALIAS##";
    private static final String FINAL_SELECT = "##FINAL_SELECT_ALIAS##";
    private static final String FINAL_SELECT_ALL = "select * from ";
    private static final String FINAL_SELECT_COUNT = "select count(1) from ";
    private static final String TRANS_QUERY_SQL = FINAL_SELECT
            + " (select it.id,it.receiptNum, 'Purchase' as transType,"
            + " it.username,it.createdDate, pa.orgName,"
            + " it.status,irt.receiptNum as linkedTrans, "
            + " case it.paymentType when 'OfflinePayment' then opr.paymentType else it.paymentType end paymentType, "
            + " opr.status as offlinePaymentStatus, "
            + " opr.id offlinePaymentId "
            + " from PPMFLGInventoryTransaction it "
            + " left outer join PPMFLGPartner pa on pa.adminId = it.mainAccountId"
            + " left outer join PPMFLGRevalidationTransaction irt on irt.transactionId = it.id"
            + " left outer join PPMFLGOfflinePaymentRequest opr on opr.transactionId = it.id "
            + " where  1=1 "
            + TRANS_WHERE_CLAUSE
            + " union "
            + " select  rt.id,rt.receiptNum, 'Revalidate' as transType, rt.username,rt.createdDate, pa.orgName"
            + " ,rt.status,rit.receiptNum as linkedTrans, rt.paymentType, opr.status as offlinePaymentStatus, "
            + " opr.id as offlinePaymentId "
            + " from PPMFLGRevalidationTransaction rt "
            + " left outer join PPMFLGPartner pa on pa.adminId = rt.mainAccountId"
            + " left outer join PPMFLGInventoryTransaction rit on rt.transactionId = rit.id"
            + " left outer join PPMFLGOfflinePaymentRequest opr on opr.transactionId = rit.id and opr.transactionId = -1 "
            + " where  1=1 " + RETRANS_WHERE_CLAUSE + ") tbl";
    private static final String ORDER_BY_CLAUSE = " order by ";

    private String makeQuery(TransQueryFilterVM filter) {
        String tranSubSql;
        String reTranSubSql;
        String commonSQy = "";
        if (filter.getFromDate() != null) {
            commonSQy = commonSQy + " and " + TABLE_ALIAS + "createdDate > :fromDt";
        }
        if (filter.getToDate() != null) {
            commonSQy = commonSQy + " and " + TABLE_ALIAS + "createdDate - 1 < :toDt";
        }
        if (filter.getReceiptNum() != null && !"".equals(filter.getReceiptNum())) {
            commonSQy = commonSQy + " and " + TABLE_ALIAS + "receiptNum like :receiptNum";
        }
        if (filter.getOrgName() != null && !"".equals(filter.getOrgName())) {
            commonSQy = commonSQy + " and pa.orgName like :orgName";
        }
        if (filter.getAccountCode() != null && !"".equals(filter.getAccountCode())) {
            commonSQy = commonSQy + " and pa.accountCode like :accountCode";
        }
        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            commonSQy = commonSQy + " and pa.id in (:paIds) ";
        }
        if(filter.getPaymentType() != null && filter.getPaymentType().trim().length() > 0){
            if("Online".equalsIgnoreCase(filter.getPaymentType())){
                commonSQy = commonSQy + " and opr.id is null ";
                commonSQy = commonSQy + " and (";
                commonSQy = commonSQy + "        "+TABLE_ALIAS +"paymentType is null ";
                commonSQy = commonSQy + "        or ";
                commonSQy = commonSQy + "        "+TABLE_ALIAS +"paymentType <> 'OfflinePayment' ";
                commonSQy = commonSQy + "     )";
                if(filter.getMixedStatus() != null && filter.getMixedStatus().trim().length() > 0){
                    commonSQy = commonSQy + " and " + TABLE_ALIAS + "status in (:status) ";
                }
            }else if("Offline".equalsIgnoreCase(filter.getPaymentType())){
                commonSQy = commonSQy + " and opr.id is not null ";
                commonSQy = commonSQy + " and (";
                commonSQy = commonSQy + "        "+TABLE_ALIAS +"paymentType is not null ";
                commonSQy = commonSQy + "        or ";
                commonSQy = commonSQy + "        "+TABLE_ALIAS +"paymentType = 'OfflinePayment' ";
                commonSQy = commonSQy + "     )";
                if(filter.getMixedStatus() != null && filter.getMixedStatus().trim().length() > 0){
                    commonSQy = commonSQy + " and opr.status is not null ";
                    commonSQy = commonSQy + " and opr.status = :status ";
                }
            }
        }

        tranSubSql = commonSQy.replace(TABLE_ALIAS, TRANS_ALIAS);
        reTranSubSql = commonSQy.replace(TABLE_ALIAS, RETRAN_ALIAS);
        log.debug("tranSubSql-----" + tranSubSql);
        log.debug("reTranSubSql-----" + reTranSubSql);

        String finalSQLStr = TRANS_QUERY_SQL;
        finalSQLStr = finalSQLStr.replace(TRANS_WHERE_CLAUSE, tranSubSql);
        finalSQLStr = finalSQLStr.replace(RETRANS_WHERE_CLAUSE, reTranSubSql);
        return finalSQLStr;
    }

    @Override
    public List<TransQueryVM> getTransByPage(TransQueryFilterVM filter,
                                             String orderField, String order, Integer pageNumber,
                                             Integer pageSize) {
        String tempSql = this.makeQuery(filter);
        tempSql = tempSql.replace(FINAL_SELECT, FINAL_SELECT_ALL);
        tempSql = tempSql + ORDER_BY_CLAUSE;
        if (orderField != null && order != null && !"".equals(orderField) && !"".equals(order)) {
            orderField = orderField.trim();
            order = order.trim();
            if(!("desc".equalsIgnoreCase(order) || "asc".equalsIgnoreCase(order))){
                order = "desc";
            }
            if("createdDateStr".equalsIgnoreCase(orderField)){
                orderField = "createdDate";
            }else if("receiptNum".equalsIgnoreCase(orderField)){
                orderField = "receiptNum";
            }else{
                orderField = "createdDate";
            }
            tempSql = tempSql + " "+ orderField+" "+order;
        } else {
            tempSql = tempSql + " createdDate desc";
        }

        Session session = null;
        try{
            session = sessionFactory.openSession();
            SQLQuery query = session.createSQLQuery(tempSql);
            query = this.groupCommonQuery(query, filter);
            if (pageNumber != null && pageSize != null) {
                query.setFirstResult((pageNumber - 1) * pageSize);
                query.setMaxResults(pageSize);
            }
            query.addScalar("id", StandardBasicTypes.INTEGER);
            query.addScalar("receiptNum", StandardBasicTypes.STRING);
            query.addScalar("createdDate", StandardBasicTypes.TIMESTAMP);
            query.addScalar("orgName", StandardBasicTypes.STRING);
            query.addScalar("username", StandardBasicTypes.STRING);
            query.addScalar("status", StandardBasicTypes.STRING);
            query.addScalar("transType", StandardBasicTypes.STRING);
            query.addScalar("linkedTrans", StandardBasicTypes.STRING);
            query.addScalar("paymentType", StandardBasicTypes.STRING);
            query.addScalar("offlinePaymentStatus", StandardBasicTypes.STRING);
            query.addScalar("offlinePaymentId", StandardBasicTypes.INTEGER);
            query.setResultTransformer(Transformers.aliasToBean(TransQueryVM.class));
            return query.list();
        }catch (Exception ex){
            log.error("getTransByPage error : "+ex.getMessage(), ex);
            throw ex;
        }catch (Throwable th){
            log.error("getTransByPage error : "+th.getMessage(), th);
            throw th;
        }finally {
            if(session != null){
                try{
                    session.close();
                }catch (Exception ex){
                    log.error("getTransByPage error : "+ex.getMessage(), ex);
                }catch (Throwable th){
                    log.error("getTransByPage error : "+th.getMessage(), th);
                }
            }
        }
    }

    @Override
    public int getTransByPageSize(TransQueryFilterVM filter) {
        String tempSql = this.makeQuery(filter);
        tempSql = tempSql.replace(FINAL_SELECT, FINAL_SELECT_COUNT);
        Session session = null;
        try {
            session = sessionFactory.openSession();
            SQLQuery query = session.createSQLQuery(tempSql);
            query = this.groupCommonQuery(query, filter);
            return (Integer) query.uniqueResult();
        }catch (Exception ex){
            log.error("getTransByPageSize error : "+ex.getMessage(), ex);
            throw ex;
        }catch (Throwable th){
            log.error("getTransByPageSize error : "+th.getMessage(), th);
            throw th;
        }finally {
            if(session != null){
                try{
                    session.close();
                }catch (Exception ex){
                    log.error("getTransByPageSize error : "+ex.getMessage(), ex);
                }catch (Throwable th){
                    log.error("getTransByPageSize error : "+th.getMessage(), th);
                }
            }
        }
    }

    private SQLQuery groupCommonQuery(SQLQuery query, TransQueryFilterVM filter) {
        if (filter.getFromDate() != null) {
            query.setDate("fromDt", populateStartDateTime(filter.getFromDate()) );
        }
        if (filter.getToDate() != null) {
            query.setDate("toDt", populateEndDateTime(filter.getToDate()));
        }
        if (filter.getReceiptNum() != null && !"".equals(filter.getReceiptNum())) {
            query.setString("receiptNum", "%" + filter.getReceiptNum() + "%");
        }
        if (filter.getOrgName() != null && !"".equals(filter.getOrgName())) {
            query.setString("orgName", "%" + filter.getOrgName() + "%");
        }
        if (filter.getAccountCode() != null && !"".equals(filter.getAccountCode())) {
            query.setString("accountCode", "%" + filter.getAccountCode() + "%");
        }
        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            query.setParameterList("paIds", filter.getPaIds());
        }
        if(filter.getPaymentType() != null && filter.getPaymentType().trim().length() > 0){
            if("Online".equalsIgnoreCase(filter.getPaymentType())){
                if(filter.getMixedStatus() != null && filter.getMixedStatus().trim().length() > 0){
                    List<String> lst = com.enovax.star.cms.commons.constant.ppmflg.TransMegaStatus.getChildStatuses(com.enovax.star.cms.commons.constant.ppmflg.TransMegaStatus.valueOf(filter.getMixedStatus()));
                    query.setParameterList("status", lst);
                }
            }else if("Offline".equalsIgnoreCase(filter.getPaymentType())){
                if(filter.getMixedStatus() != null && filter.getMixedStatus().trim().length() > 0){
                    query.setString("status",  filter.getMixedStatus().trim() );
                }
            }
        }
        return query;
    }
    /**
     * end: TransQuery
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------
     */



    /**
     * begin: PinCode Query
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    private StringBuilder makePinQuery(PinQueryFilterVM ft){
        StringBuilder sql = new StringBuilder();
        sql.append(" select distinct ");
        sql.append(" t.id as id, t.name, t.ticketMedia as pkgTktMedia, t.pinCode as pinCode, t.ticketGeneratedDate as ticketGeneratedDate, pa.orgName as orgName, t.lastRedemptionDate as lastRedemptionDate, t.status as status ");
        sql.append(" FROM PPMFLGMixMatchPackage t ");
        sql.append(" left join PPMFLGTAMainAccount tma on tma.id = t.mainAccountId ");
        sql.append(" left join PPMFLGPartner pa on pa.adminId = tma.id ");
        sql.append(" left join PPMFLGMixMatchPackageItem ti on ti.packageId = t.id ");
        sql.append(" where 1=1 ");
        sql.append(" and t.ticketGeneratedDate is not null and tma.id is not null and pa.id is not null and ti.id is not null ");
        if(ft != null){
            if(ft.getTicketMediaType() != null && ft.getTicketMediaType().trim().length() > 0){
                sql.append(" and t.ticketMedia = :ticketMediaType ");
            }
            if(ft.getFromDate() != null){
                sql.append(" and t.ticketGeneratedDate >= :fromDate ");
            }
            if(ft.getToDate() != null){
                sql.append(" and t.ticketGeneratedDate <= :toDate ");
            }
            if(ft.getReceiptNum() != null && ft.getReceiptNum().trim().length() > 0){
                sql.append(" and ti.receiptNum like :receiptNum ");
            }
            if(ft.getPinCode() != null && ft.getPinCode().trim().length() > 0){
                sql.append(" and t.pinCode like :pinCode ");
            }
            if(ft.getOrgName() != null && ft.getOrgName().trim().length() > 0){
                sql.append(" and pa.orgName like :orgName ");
            }
            if(ft.getAccountCode() != null && ft.getAccountCode().trim().length() > 0){
                sql.append(" and pa.accountCode like :accountCode ");
                sql.append(" and tma.accountCode like :accountCode ");
            }
            if(ft.getPaIds() != null && ft.getPaIds().length > 0){
                sql.append(" and pa.id in (:paIds) ");
            }
        }
        return sql;
    }

    private SQLQuery groupPinCodeQuery(SQLQuery query, PinQueryFilterVM filter) {
        if(filter.getTicketMediaType() != null && filter.getTicketMediaType().trim().length() > 0){
            query.setString("ticketMediaType", filter.getTicketMediaType());
        }
        if (filter.getFromDate() != null) {
            query.setDate("fromDate", populateStartDateTime(filter.getFromDate()) );
        }
        if (filter.getToDate() != null) {
            query.setDate("toDate", populateEndDateTime(filter.getToDate()));
        }
        if(filter.getPinCode() != null && filter.getPinCode().trim().length() > 0){
            query.setString("pinCode", filter.getPinCode());
        }
        if (filter.getReceiptNum() != null && !"".equals(filter.getReceiptNum())) {
            query.setString("receiptNum", "%" + filter.getReceiptNum() + "%");
        }
        if (filter.getOrgName() != null && !"".equals(filter.getOrgName())) {
            query.setString("orgName", "%" + filter.getOrgName() + "%");
        }
        if (filter.getAccountCode() != null && !"".equals(filter.getAccountCode())) {
            query.setString("accountCode", "%" + filter.getAccountCode() + "%");
        }
        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            query.setParameterList("paIds", filter.getPaIds());
        }
        return query;
    }

    @Override
    public List<PackageViewResultVM> getPinQueryByPage(PinQueryFilterVM ft, String orderField, String order, Integer pageNumber, Integer pageSize) {
        StringBuilder tempSql = this.makePinQuery(ft);

        StringBuilder finalSql = new StringBuilder();
        finalSql.append("SELECT xt.id as id, xt.name as name, xt.pinCode as pinCode, xt.pkgTktMedia as pkgTktMedia, xt.ticketGeneratedDate as ticketGeneratedDate, xt.orgName as orgName, xt.lastRedemptionDate as lastRedemptionDate, xt.status as status FROM (");
        finalSql.append(tempSql.toString());
        finalSql.append(")xt where 1= 1");
        finalSql.append(" ");
        finalSql.append(ORDER_BY_CLAUSE);
        finalSql.append(" ");
        if (orderField != null && order != null && !"".equals(orderField) && !"".equals(order)) {
            orderField = orderField.trim();
            order = order.trim();
            if(!("desc".equalsIgnoreCase(order) || "asc".equalsIgnoreCase(order))){
                order = "desc";
            }
            if("name".equalsIgnoreCase(orderField) || "pinCode".equalsIgnoreCase(orderField) || "pkgTktMedia".equalsIgnoreCase(orderField) || "ticketGeneratedDate".equalsIgnoreCase(orderField) || "orgName".equalsIgnoreCase(orderField) || "lastRedemptionDate".equalsIgnoreCase(orderField) || "status".equalsIgnoreCase(orderField)){
                finalSql.append(" xt.").append(orderField.trim()).append(" ").append(order);
            }else{
                finalSql.append(" xt.ticketGeneratedDate desc");
            }
        } else {
            finalSql.append(" xt.ticketGeneratedDate desc");
        }

        Session session = null;
        try{
            session = sessionFactory.openSession();
            SQLQuery query = session.createSQLQuery(finalSql.toString());
            query = this.groupPinCodeQuery(query, ft);
            if (pageNumber != null && pageSize != null) {
                query.setFirstResult((pageNumber - 1) * pageSize);
                query.setMaxResults(pageSize);
            }
            query.addScalar("id", StandardBasicTypes.INTEGER);
            query.addScalar("pinCode", StandardBasicTypes.STRING);
            query.addScalar("name", StandardBasicTypes.STRING);
            query.addScalar("pkgTktMedia", StandardBasicTypes.STRING);
            query.addScalar("ticketGeneratedDate", StandardBasicTypes.TIMESTAMP);
            query.addScalar("orgName", StandardBasicTypes.STRING);
            query.addScalar("lastRedemptionDate", StandardBasicTypes.TIMESTAMP);
            query.addScalar("status", StandardBasicTypes.STRING);
            query.setResultTransformer(Transformers.aliasToBean(PackageViewResultVM.class));
            return query.list();
        }catch (Exception ex){
            log.error("getTransByPage error : "+ex.getMessage(), ex);
            ex.printStackTrace();
            throw ex;
        }catch (Throwable th){
            log.error("getTransByPage error : "+th.getMessage(), th);
            th.printStackTrace();
            throw th;
        }finally {
            if(session != null){
                try{
                    session.close();
                }catch (Exception ex){
                    log.error("getTransByPage error : "+ex.getMessage(), ex);
                }catch (Throwable th){
                    log.error("getTransByPage error : "+th.getMessage(), th);
                }
            }
        }
    }

    @Override
    public int getPinQueryByPageSize(PinQueryFilterVM filter) {
        StringBuilder tempSql = this.makePinQuery(filter);

        StringBuilder finalSql = new StringBuilder();
        finalSql.append(FINAL_SELECT_COUNT);
        finalSql.append(" ( ");
        finalSql.append(tempSql.toString());
        finalSql.append(" ) ");
        finalSql.append(" tbl ");

        Session session = null;
        try{
            session = sessionFactory.openSession();
            SQLQuery query = session.createSQLQuery(finalSql.toString());
            query = this.groupPinCodeQuery(query, filter);
            return (Integer) query.uniqueResult();
        }catch (Exception ex){
            log.error("getPinQueryByPageSize error : "+ex.getMessage(), ex);
            throw ex;
        }catch (Throwable th){
            log.error("getPinQueryByPageSize error : "+th.getMessage(), th);
            throw th;
        }finally {
            if(session != null){
                try{
                    session.close();
                }catch (Exception ex){
                    log.error("getPinQueryByPageSize error : "+ex.getMessage(), ex);
                }catch (Throwable th){
                    log.error("getPinQueryByPageSize error : "+th.getMessage(), th);
                }
            }
        }
    }

    private Date populateStartDateTime(Date date){
        if(date != null){
            String str = NvxDateUtils.formatDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            String strWithTime = str +" "+ "00:00:00";
            try {
                return NvxDateUtils.parseDate(strWithTime, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
            }catch (Exception ex){
                ex.printStackTrace();
            }catch (Throwable th){
                th.printStackTrace();
            }
        }
        return null;
    }
    private Date populateEndDateTime(Date date){
        if(date != null){
            String str = NvxDateUtils.formatDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            String strWithTime = str +" "+ "23:59:59";
            try {
                return NvxDateUtils.parseDate(strWithTime, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
            }catch (Exception ex){
                ex.printStackTrace();
            }catch (Throwable th){
                th.printStackTrace();
            }
        }
        return null;
    }
    /**
     * end: PinCode Query
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
}
