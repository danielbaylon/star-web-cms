package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPaDistributionMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTable;

import java.text.ParseException;
import java.util.List;

/**
 * Created by houtao on 12/8/16.
 */
public interface IPartnerExtensionService {

    boolean updatePartnerExtProperties(AxCustomerTable c, boolean enablePartnerPortal);

    public AxCustomerTable popuplateCustoemrTable(PPSLMPartner pa, boolean enablePartnerPortal, boolean allowOnAccount, List<PPSLMPADocMapping> docMap, List<PPSLMPaDistributionMapping> marketDistributionList, String documnetTypeId, String capacityGroupId, String lineOfBusiness) throws ParseException;

}
