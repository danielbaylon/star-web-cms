package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.constant.TmLocale;
import com.enovax.payment.tm.constant.TmRequestType;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.ReceiptVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMInventoryTransactionRepository;
import com.enovax.star.cms.commons.tracking.Trackd;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * Created by jennylynsze on 10/10/16.
 */
@Service
public class DefaultPartnerRevalidationService implements IPartnerRevalidationService {

    @Autowired
    private IPartnerInventoryService inventoryService;

    @Autowired
    private IPartnerRevalidationTransactionService revalTransService;

    @Autowired
    private IPartnerRevalidatePurchaseService revalPurchaseService;

    @Autowired
    private PPSLMInventoryTransactionRepository invTransRepo;

    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private TmServiceProvider tmServiceProvider;

    private Logger log = Logger.getLogger(getClass());

    @Override
    public ApiResult<ReceiptVM> revalidateReceipt(Integer transId, PartnerAccount account) {

        InventoryTransactionVM inventoryTransactionVM = inventoryService.getTransByUser(transId, account);

        if(inventoryTransactionVM != null) {
            if(!inventoryTransactionVM.isAllowRevalidation()) {
                return new ApiResult<>(ApiErrorCodes.RevaliationNotAllowed);
            }
        }

        try {
            ReceiptVM receiptInfo = revalTransService.getRevalidateInfoByTransId(transId);
            return new ApiResult<>(true, "", "", receiptInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ApiResult<>(ApiErrorCodes.ErrorInGettingRevalidationInfo);
        }
    }

    @Override
    @Transactional
    public ApiResult<CheckoutConfirmResult> finaliseRevalTransAndRedirect(Integer transId, PartnerAccount account, HttpSession session, Trackd trackd) {
        try {
            PPSLMRevalidationTransaction revalTrans = revalPurchaseService
                    .getRevalInfoBeforePayment(transId, account.getMainAccountId(), account.getUsername(),
                            !account.isSubAccount());

            if(revalTrans == null) {
                return new ApiResult<>(false, "", "", null);
            }

            if(revalTrans.getTotal().compareTo(BigDecimal.ZERO) == 0) {
                //TODO dunno what to do lol wut
            }

            final CheckoutConfirmResult ccr = new CheckoutConfirmResult();
            final String receiptNumber = revalTrans.getReceiptNum();
            final BigDecimal totalAmount = revalTrans.getTotal();
            final String merchantId = revalTrans.getTmMerchantId();
            final TmCurrency tmCurrency = TmCurrency.SGD;

            final TmLocale tmLocale = TmLocale.EnglishUs; //TODO apply i18n

            final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
            merchantSignatureConfig.setSignatureEnabled(false);

            final TelemoneyParamPackage tmParams;
            final StoreApiChannels channel = StoreApiChannels.PARTNER_PORTAL_SLM;
            try {
                tmParams = tmParamRepository.getTmParams(channel, TmRequestType.Revalidation);
            } catch (JcrRepositoryException e) {
                //TODO Additional handling
                log.error("Error retrieving telemoney parameters", e);
                return new ApiResult<>(ApiErrorCodes.General);
            }

            String returnUrl = tmParams.getTmReturnUrl();
            String statusUrl = tmParams.getTmStatusUrl();
            String returnUrlParamString = "";

            //TODO Namespace for i18n purposes
            final TmPaymentRedirectInput input = new TmPaymentRedirectInput(returnUrl, statusUrl, returnUrlParamString, receiptNumber,
                    totalAmount, merchantId, null, tmCurrency, null, tmLocale, merchantSignatureConfig);
            input.setUserField1(channel.code);
            input.setUserField2(TmRequestType.Revalidation.toString());
            input.setUserField3(session.getId());

            final String redirectUrl = tmServiceProvider.constructPaymentRedirectUrl(input);

            ccr.setTmRedirect(true);
            ccr.setRedirectUrl(redirectUrl);

            session.setAttribute(channel.code + account.getUsername() + StoreApiConstants.SESS_ATTR_REVAL_TXN_RECEIPT_NUM, revalTrans.getReceiptNum());

            return new ApiResult<>(true, "", "", ccr);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return new ApiResult<>(false, "", "", null);
    }

    @Override
    public ApiResult<String> checkStatus(String receiptNo) {
        PPSLMRevalidationTransaction revalTrans = revalTransService.findByReceipt(receiptNo);

        final String stage = revalTrans.getStatus();
        final String tmStatus = revalTrans.getTmStatus();
        if (TicketStatus.Reserved.name().equals(stage) &&
                (EnovaxTmSystemStatus.RedirectedToTm.name().equals(tmStatus) || EnovaxTmSystemStatus.TmQuerySent.name().equals(tmStatus))) {
            //TODO
            return new ApiResult<>(true, "", "", null);
        }

        if (TicketStatus.Incomplete.name().equals(stage) || TicketStatus.Failed.name().equals(stage) || EnovaxTmSystemStatus.NA.name().equals(tmStatus)) {
            //TODO Give more detailed error
            return new ApiResult<>(false, ApiErrorCodes.PaymentFailed.name(), ApiErrorCodes.PaymentFailed.message, revalTrans.getTransactionId() + "");
        }


        if (TicketStatus.Revalidated.toString().equals(stage)) {
            return new ApiResult<>(true, "", "",  receiptNo);
        }

        return new ApiResult<>(ApiErrorCodes.General);
    }

    @Override
    @Transactional(readOnly = true)
    public ApiResult<ReceiptVM> getRevalidatedReceiptByReceiptNum(String receiptNum, PartnerAccount account) {
        try {
            //TODO check the status etc, throws error if not yet ok, return the transId so can do reval again.
            PPSLMRevalidationTransaction revalTrans = revalTransService.findByReceipt(receiptNum);
            ReceiptVM receiptInfo = new ReceiptVM(revalTrans.getTransaction(), revalTrans, "", "", "");
            return new ApiResult<>(true, "", "Transaction is successful!", receiptInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ApiResult<>(ApiErrorCodes.ErrorInGettingRevalidationInfo);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ApiResult<ReceiptVM> getRevalidatedReceiptByTransId(Integer transId, PartnerAccount account) {
        try {
            //TODO check the status etc, throws error if not yet ok, return the transId so can do reval again.
            return new ApiResult<>(true, "", "", revalTransService.getRevalidateTransByTransId(transId));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ApiResult<>(ApiErrorCodes.ErrorInGettingRevalidationInfo);
        }
    }
}
