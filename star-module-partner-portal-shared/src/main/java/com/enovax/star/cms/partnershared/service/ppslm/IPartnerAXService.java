package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.axstar.AxStarCustomer;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;

import java.text.ParseException;

/**
 * Created by houtao on 28/7/16.
 */
public interface IPartnerAXService {

    public AxStarServiceResult<AxStarCustomer> registerPartner(PPSLMPartner partner, String tierId, String custGroup, String correspondenceAddr, String correspondencePostCode, String correspondenceCity);

    void syncPartnerProfile(Integer partnerId);

    void syncSelfUpdatedPartnerProfileToAX(Integer id) throws Exception;
}
