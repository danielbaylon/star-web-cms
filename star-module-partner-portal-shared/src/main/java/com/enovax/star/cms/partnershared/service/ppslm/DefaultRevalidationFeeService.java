package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppslm.RevalFeeItemVM;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.PublishingUtil;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class DefaultRevalidationFeeService implements IRevalidationFeeService {

    @Autowired
    private IPartnerProductService prodSrv;
    @Autowired
    StarTemplatingFunctions starTemplatingFunctions;

    @Override
    public BigDecimal getRevalidationFeeInBigDecimalByRevalidationFeeId(StoreApiChannels channel, String axAccNum, String revalFeeItemId) {
       //TODO evaluate the correct way of getting the price for this, I think there shouldn't be specific price for the partner.... I think...
        try {
            if(revalFeeItemId == null || revalFeeItemId.trim().length() == 0){
                return null;
            }
            Long prodId = null;
            Long itemId = null;
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+ PartnerPortalConst.Partner_Portal_Ax_Data + "/revalidation-products");
            if(productNode != null) {
                NodeIterator iter = productNode.getNodes();
                while (iter.hasNext()) {
                    Node node = iter.nextNode();
                    if (node != null) {
                        if (node.hasProperty("productListingId") || node.hasProperty("productNumber")) {
                            itemId = PropertyUtil.getLong(node, "productNumber");
                            prodId = PropertyUtil.getLong(node, "productListingId");
                            if(itemId == null || prodId == null){
                                continue;
                            }
                            if(revalFeeItemId.equals(itemId.toString())){
                                break;
                            }
                        }
                    }
                }
            }
            BigDecimal price = getItemPriceByProductIdAndItemId(channel, axAccNum, prodId, itemId);
            return price;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getRevalidationFeeByRevalidationFeeId(StoreApiChannels channel, String axAccNum, String revalFeeItemId) {
        BigDecimal price = getRevalidationFeeInBigDecimalByRevalidationFeeId(channel, axAccNum, revalFeeItemId);
        return price != null ? NvxNumberUtils.formatToCurrency(price) : "";
    }

    @Override
    public List<RevalFeeItemVM> populdateActiveRevalidationFeeItemVMs(StoreApiChannels channel, String axAccNum) {
        axAccNum = axAccNum != null && axAccNum.trim().length() > 0 ? axAccNum.trim() : null;
        List<RevalFeeItemVM> items = new ArrayList<RevalFeeItemVM>();
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+ PartnerPortalConst.Partner_Portal_Ax_Data + "/revalidation-products");
            if(productNode != null){
                NodeIterator iter = productNode.getNodes();
                while(iter.hasNext()){
                    Node node = iter.nextNode();
                    if(node != null){
                        if(node.hasProperty("productListingId") || node.hasProperty("productNumber")){
                            RevalFeeItemVM axProduct = new RevalFeeItemVM();
                            axProduct.setProductId(PropertyUtil.getLong(node, "productListingId"));
                            Long productNumber = PropertyUtil.getLong(node, "productNumber");
                            axProduct.setItemId(productNumber);
                            axProduct.setUuid(node.getIdentifier());
                            Node axProductNode = starTemplatingFunctions.getAxProductByProductCode(PartnerPortalConst.Partner_Portal_Channel,productNumber.toString());
                            if(axProductNode != null) {
                                String desc = PropertyUtil.getString(axProductNode,"itemName");
                                String status = PropertyUtil.getString(axProductNode,"status");
                                axProduct.setDescription(desc);
                                axProduct.setStatus(status);
                            }
                            BigDecimal price = getItemPriceByProductIdAndItemId(channel, axAccNum, axProduct.getProductId(), axProduct.getItemId());
                            axProduct.setPriceStr(price != null ? NvxNumberUtils.formatToCurrency(price) : "");
                            items.add(axProduct);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    private BigDecimal getItemPriceByProductIdAndItemId(StoreApiChannels channel, String axAccNum, Long productId, Long itemId) {
        axAccNum = axAccNum != null && axAccNum.trim().length() > 0 ? axAccNum.trim() : null;
        if(productId == null || itemId == null || productId.intValue() == 0 || itemId.intValue() == 0){
            return null;
        }
        List<String> prodIdList = new ArrayList<String>();
        prodIdList.add(productId.toString());

        List<String> itemIdList = new ArrayList<String>();
        itemIdList.add(productId.longValue()+"-"+itemId.longValue());

        Map<String,BigDecimal> map = prodSrv.getProductItemPriceMap(channel, axAccNum, prodIdList, itemIdList);
        if (map == null) {
            return null;
        }
        String key = productId+"-"+itemId;
        if(!map.containsKey(key)){
            return null;
        }
        return map.get(productId+"-"+itemId);
    }

    @Override
    public ResultVM saveRevalidationFeeItem(StoreApiChannels channel, RevalFeeItemVM revalFeeItemVM) {
        ResultVM resultVM = new ResultVM();
        boolean toDelete = false;
        try {
            String uuid = revalFeeItemVM.getUuid();
            Node oldNode=null;
            if (uuid != null && !uuid.equalsIgnoreCase("")) {

                oldNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSConfig.getWorkspaceName(),uuid);
                if (oldNode!= null) {
                    toDelete = true;
                }
            }
            Node newNode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.code + "/revalidation-products/", revalFeeItemVM.getProductId().toString(), JcrWorkspace.CMSConfig.getNodeType());
            if(newNode!= null) {
                if(toDelete) {
                    PublishingUtil.deleteNodeAndPublish(oldNode, JcrWorkspace.CMSConfig.getWorkspaceName());
                }
                newNode.setProperty("productListingId", revalFeeItemVM.getProductId().toString());
                if (revalFeeItemVM.getDescription() != null)
                    newNode.setProperty("productName", revalFeeItemVM.getDescription());
                else
                    newNode.setProperty("productName", "");
                newNode.setProperty("productNumber", revalFeeItemVM.getItemId().toString());
                JcrRepository.updateNode((JcrWorkspace.CMSConfig.getWorkspaceName()), newNode);
                PublishingUtil.publishNodes(newNode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
            }
            else{
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.RevalItemAlreadyExists.message);
            }



        } catch (Exception e) {
            e.printStackTrace();
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.RevalItemNotFound.message);
        }
        resultVM.setViewModel(revalFeeItemVM);
        return resultVM;
    }

    @Override
    public ResultVM removeRevalidationFeeItem(StoreApiChannels channel,String feeItemIds) {
        ResultVM resultVM = new ResultVM();
        List<String> uuidList=new ArrayList<>();
        if(!feeItemIds.equals("")) {
            uuidList = Arrays.asList(feeItemIds.split("\\s*,\\s*"));
        }
        try {
            for (String uuid : uuidList) {
                Node node = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSConfig.getWorkspaceName(), uuid);
                PublishingUtil.deleteNodeAndPublish(node,JcrWorkspace.CMSConfig.getWorkspaceName());
            }
        }catch(Exception e) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.RevalItemNotFound.message);
            e.printStackTrace();
        }
        return resultVM;
    }
}
