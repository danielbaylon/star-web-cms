package com.enovax.star.cms.partnershared.service.ppslm;


import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackage;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

public interface IPartnerEmailService {

    public void sendSignupAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    public void sendSignupEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    void sendSignupUpdateEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    void sendSignupUpdateAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;


    void sendingPartnerProfileRejectEmail(PartnerVM vm) throws Exception;

    void sendingPartnerProfileResubmitEmail(PartnerVM vm) throws Exception;

    void sendingPartnerProfileVerifingEmail(PPSLMApprovalLog vm) throws Exception;

    void sendPartnerRegistrationRejectionEmail(ApprovingPartnerVM vm) throws Exception;

    void sendPartnerRegistrationApprovedEmail(ApprovingPartnerVM vm) throws Exception;

    void sendPartnerSubaccountCreationEmail(PartnerAccountVM vm) throws Exception;

    void sendPartnerSubaccountPasswordResetEmail(PartnerAccountVM vm) throws Exception;

    void sendTransactionReceiptEmail(PPSLMInventoryTransaction trans, ReceiptVM receipt, String receiptName, String genpdfPath) throws Exception;

    void sendRevalTransactionReceiptEmail(PPSLMInventoryTransaction trans,
                                          PPSLMRevalidationTransaction revalTrans, ReceiptVM receipt,
                                          String receiptName, String genpdfpath) throws Exception;

    void sendPincodeEmail(PPSLMMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendEticketEmail(PPSLMMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendExcelEmail(PPSLMMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendForgotPasswordEmail(String userName, String loginName, String newPwd, String email);
}
