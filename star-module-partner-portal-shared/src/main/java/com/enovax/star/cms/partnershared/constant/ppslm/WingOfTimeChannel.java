package com.enovax.star.cms.partnershared.constant.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

/**
 * Created by houtao on 2/10/16.
 */
public class WingOfTimeChannel {

    public static final StoreApiChannels CHANNEL = StoreApiChannels.PARTNER_PORTAL_SLM;
}
