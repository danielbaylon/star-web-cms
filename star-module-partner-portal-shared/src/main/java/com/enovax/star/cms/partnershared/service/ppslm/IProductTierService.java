package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.tier.ProductTier;
import com.enovax.star.cms.commons.model.partner.ppslm.ProductTierVM;

import java.util.List;

/**
 * Created by jennylynsze on 5/12/16.
 */
public interface IProductTierService {
    List<ProductTierVM> getAllTierVms(String channel);

    public ProductTierVM getTierVmById(String channel, String tierId);

    public ProductTierVM saveTier(String channel, ProductTierVM tierVm, String userNm);

    //public void removeTier(String channel, List<Integer> rcmdIds, String userNm);

    //public void processTierPartner(String tierId, Integer partnerId) throws Exception;

    public ProductTier getProductTier(String channel,String tierId);

    String getTierIdByPartnerId(String partner_portal_channel, Integer id);
}
