package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.ApproverVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;

import java.util.List;

/**
 * Created by lavanya on 28/9/16.
 */
public interface IApproverService {
    public ApproverVM getCurrentApprover();
    public List<ApproverVM> getCandidates();
    public List<ApproverVM> getGeneralBackupApproverbyPage(String orderBy, String orderWith, Integer page, Integer pagesize);
    public int getGeneralBackupApproverSize();
    public ResultVM saveApprover(String approverAdminId, String userNm);
    public ResultVM saveBackupApprover(ApproverVM backupApprover, String userNm);
    public void removeBackupApprovers(List<Integer> rcmdIds, String userNm);

    boolean isGeneralApproverUser(String loginAdminId);
    boolean isGeneralBackupApproverUser(String loginAdminId);
}
