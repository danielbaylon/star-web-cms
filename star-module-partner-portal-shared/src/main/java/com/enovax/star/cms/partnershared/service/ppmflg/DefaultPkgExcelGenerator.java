package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.MixMatchPkgVM;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jennylynsze on 11/7/16.
 */
@Service("PPMFLGIPkgExcelGenerator")
public class DefaultPkgExcelGenerator implements IPkgExcelGenerator {

    @Override
    public Workbook generatePkgExcel(List<MixMatchPkgVM> pkgvms, String startDateStr, String endDateStr, String status, String ticketMedia, String pkgName) {
        Workbook wb = new XSSFWorkbook();
        Sheet isheet = wb.createSheet("Package Listing");
        Row irow = null;
        Cell icell = null;
        int rowNum = 0;
        irow = isheet.createRow(rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Ticket Generated Date Range: From "
                + (StringUtils.isEmpty(startDateStr) ? "-" : startDateStr) + " To "
                + (StringUtils.isEmpty(endDateStr) ? "-" : endDateStr));

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Package Status: "
                + (StringUtils.isEmpty(status) ? "-" : status));

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Ticket Media: "
                + (StringUtils.isEmpty(ticketMedia) ? "-" : ticketMedia));

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Package Name: "
                + (StringUtils.isEmpty(pkgName) ? "-" : pkgName));

        rowNum++; //blank row
        irow = isheet.createRow(++rowNum);

        irow = isheet.createRow(++rowNum);
        int cnum = writeReportHeader(irow);
        int rowno = 1;
        for (MixMatchPkgVM pkgvm : pkgvms) {
            irow = isheet.createRow(++rowNum);
            writePkg(pkgvm, irow, rowno++);
        }
        for (int i = 1; i <= cnum; i++) {
            isheet.autoSizeColumn(i);
        }
        return wb;
    }

    private int writeReportHeader(Row irow) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("No.");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Package Name");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Description");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Media Type");
        icell = irow.createCell(++cnum);
        icell.setCellValue("PIN Code");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Quantity");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Ticket Generated Date Time");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Expiry Date");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Package Status");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Last Redemption Date Time");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Action By");
        return cnum;
    }

    private int writePkg(MixMatchPkgVM pkgvm, Row irow, int rowNum) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue(rowNum);
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getName());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getDescription());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getPkgTktMedia());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getPinCode());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getQty());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getTicketGeneratedDateStr());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getExpiryDateStr());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getStatus());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getLastRedemptionDateStr());
        icell = irow.createCell(++cnum);
        icell.setCellValue(pkgvm.getUsername());
        return cnum;
    }
}
