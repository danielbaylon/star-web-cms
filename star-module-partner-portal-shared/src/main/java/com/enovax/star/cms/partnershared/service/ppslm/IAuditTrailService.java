package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.partnershared.constant.AuditAction;

/**
 * Created by lavanya on 5/9/16.
 */
public interface IAuditTrailService {
    public boolean log(boolean includeDb, AuditAction action, String details, String relatedEntities,
                       String relatedEntityKeys, String performedBy);
}
