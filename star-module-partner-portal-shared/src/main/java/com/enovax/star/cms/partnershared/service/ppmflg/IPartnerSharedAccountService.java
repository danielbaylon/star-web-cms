package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccessRightsGroup;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.UserResult;

import java.util.List;

/**
 * Created by lavanya on 26/8/16.
 */
public interface IPartnerSharedAccountService {
    public List<PPMFLGTASubAccount> getSubUsers(PPMFLGTAMainAccount mainAccount, boolean wipeSensitive);
    public PPMFLGTASubAccount getSubUser(Integer subUserId, boolean wipeSensitive);
    public List<PPMFLGTAAccessRightsGroup> getAccessRightsGroup();
    public PPMFLGTAAccount getUser(String username, boolean wipeSensitive);
    public PPMFLGTASubAccount getSubUser(String username, boolean wipeSensitive, boolean withAccessRights);
    public PPMFLGTAMainAccount getMainUser(String username, boolean wipeSensitive, boolean withAccessRights);
    public boolean exceedSubUsers(Integer mainAccountId, Integer userId);
    public UserResult saveSubUser(boolean isNew, Integer userId, String username, String pwd, String encodedPwd, String email, String[] access,
                                  String status, String createdBy, String name, Integer mainAccountId, String title) throws Exception;

    public ResultVM resetPassword(Integer userId, String newPassword, String encodedPassword, String updatedBy) throws Exception;
    public void savePreviousPassword(PPMFLGTAAccount user);
}
