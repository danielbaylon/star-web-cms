package com.enovax.star.cms.b2cshared.repository;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class B2CSessionBookingRepository implements IB2CBookingRepository {

    private Map<String, StoreTransaction> txns = new ConcurrentHashMap<>(50, 0.75f);

    @Override
    @Deprecated
    public void saveStoreTransaction(StoreApiChannels channel, StoreTransaction txn) {
        txns.put(txn.getReceiptNumber(), txn);
    }

    @Override
    @Deprecated
    public StoreTransaction getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber) {
        return txns.get(receiptNumber);
    }
}
