package com.enovax.star.cms.partnershared.service.ppslm;


import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionItemVM;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public interface IPartnerInventoryService {
    List<InventoryTransactionItemVM> getInventoryTransactionsItem(
            Integer adminId, String fromDateStr, String toDateStr,
            String availableOnly, String prodNm, String orderBy,
            String orderWith, Integer page, Integer pagesize);

    long getInventoryTransactionsItemSize(Integer adminId,
                                                String fromDateStr, String toDateStr, String availableOnly,
                                                String prodNm);

    InventoryTransactionVM getTransByUser(Integer transId, PartnerAccount account);

    InventoryTransactionVM getTransById(Integer transId, String transType);

    InventoryTransactionVM getTransDetails(PPSLMInventoryTransaction transaction, PPSLMRevalidationTransaction revalTrans, PartnerAccount account, String transType);

    String generateReceiptAndEmail(Integer transId);

    String generateReceiptAndEmail(Integer transId, PPSLMOfflinePaymentRequest req);

    String generateReceiptAndEmail(PPSLMInventoryTransaction trans, PPSLMOfflinePaymentRequest req);

    ApiResult<String> refundTrans(Integer transId, String userName);

    void emailAlertForExpiringTicket();

    void updateExpiredStatus();

    void updateForfeitedStatus();

    BigDecimal getUsedTransAmountToday(Integer mainAccountId);

    List<PPSLMInventoryTransaction> getTransForVoidReconciliation(int minsPast);

}
