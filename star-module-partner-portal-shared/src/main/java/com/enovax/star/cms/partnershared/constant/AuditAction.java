package com.enovax.star.cms.partnershared.constant;

/**
 * Created by lavanya on 5/9/16.
 */
public enum AuditAction {
    //User actions
    UserLogin,
    UserLogout,
    UserForgotPassword,
    UserChangePassword,

    //Account actions
    AccountCreate,
    AccountUpdate,
    ResetPassword,

    //Configuration actions
    ConfigSiteMode,
    ConfigCartItems,
    ConfigBookingFee,
    ConfigMaintMessage,
    ConfigBookingFeeWaiver,
    ConfigMailParam,

    //Ads actions
    AdsPageAdUpdate,
    AdsBannerCreate,
    AdsBannerUpdate,
    AdsBannerDelete,
    AdsBannerReorder,

    //Announcement actions
    NewsCreate,
    NewsUpdate,
    NewsDelete,

    //Decide
    AttrCreate,
    AttrUpdate,
    AttrDelete,

    //Catalog update actions
    CatalogUpdateGeneral,
    CatalogUpdateEvent,
    CatalogUpdateAll,

    //Category actions
    CategoryUpdateDetails,
    CategoryAddProducts,
    CategoryToggleActive,
    CategoryDeleteSubcategories,
    CategoryDeleteProductOrders,
    CategoryUpdateSubcategory,

    //Shared configuration actions
    TncUpdate,
    TncCreate,
    TncDelete,
    MerchantUpdate,
    MerchantCreate,
    MerchantDelete,

    //Product actions
    ProductToggleActive,
    ProductChangeRecommendedImage,
    ProductReorderImages,
    ProductDeleteImages,
    ProductUploadImage,
    ProductUpdateEventDetails,
    ProductUpdateDetails,
    ProductAddRecommendedProducts,
    ProductDeleteRecommendedProducts,
    ProductUpdateMainItem,

    ProductUpdateOtherItem,
    ProductUpdateEventItem,

    ProductDeleteDiscountItemMap,
    ProductCreateDiscountItemMap,
    ProductDeleteDiscountEventItemMap,
    ProductCreateDiscountEventItemMap,

    ProductUpdatePromo,
    ProductUpdatePromoItem,
    ProductDeletePromoCode,
    ProductUpdatePromoCode,
    ProductCreatePromoCode,
    ProductDeletePromos,

    ProductCreateTopup,
    ProductUpdateTopup,
    ProductDeleteTopup,

    ProductMassGeneratePromoCode,

    EventScheduleToggleActive,

    //Capacity
    CapacityUpdate,

    //GST
    GSTUpdate,

    //Tkt
    RefundTkt,
    //Pkg
    DeactivePkg
}
