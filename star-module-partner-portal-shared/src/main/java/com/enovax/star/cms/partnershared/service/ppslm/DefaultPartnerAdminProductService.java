package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.commons.model.product.ProductExtDataCacheObject;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.service.axchannel.AxChannelProductsService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import com.enovax.star.cms.partnershared.repository.ppslm.IProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jennylynsze on 5/12/16.
 */
@Service
public class DefaultPartnerAdminProductService implements IPartnerAdminProductService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerAdminProductService.class);

    private static final long CACHE_MINS = 30L;
    private Map<StoreApiChannels, ProductExtDataCacheObject> productExtDataCache = new ConcurrentHashMap<>();

    @Autowired
    IProductRepository productRepo;

    @Autowired
    private AxChannelProductsService axChannelProductsService;

    @PostConstruct
    public void init() {
        productExtDataCache.put(StoreApiChannels.B2C_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.B2C_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new ProductExtDataCacheObject());
    }

    @Override
    public ApiResult<List<PartnerProductVM>> getProdVmsByPage(ProductGridFilterVM filter, boolean isExclusive) {

        ApiResult<List<PartnerProductVM>> res = new ApiResult<>();
        List<PartnerProductVM> prodList = productRepo.getProdByPage(filter, isExclusive);
        int total = productRepo.getProdSize(filter, isExclusive);
        res.setData(prodList);
        res.setTotal(total);
        return res;
    }

    @Override
    public List<PartnerProductVM> getProdVmsByProdIds(ProductGridFilterVM filter, List<String> prodIds, boolean isExclusive) {
        if(prodIds == null || prodIds.size() == 0){
            return null;
        }
        List<PartnerProductVM> lst = productRepo.getProdByPage(filter, isExclusive);
        if(lst == null || lst.size() == 0){
            return null;
        }
        List<PartnerProductVM> res = new ArrayList<PartnerProductVM>();

        for(PartnerProductVM prd : lst){
            if(prodIds.contains(prd.getId())){
               res.add(prd);
            }

        }
        lst = null;
        return res;
    }

    @Override
    public PartnerProductVM getProdVmByProdId(StoreApiChannels channel, String prodId) {
        return productRepo.getProd(channel.code, prodId);
    }


    @Override
    public ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels channel, List<String> productIds) throws Exception {
        final boolean updateSuccessful = updateProductExtDataCache(channel);
        if (!updateSuccessful) {
            return new ApiResult<>(false, "ERR_PROD_EXT_RETRIEVAL", "Error retrieving products data.", null);
        }
        final ProductExtDataCacheObject channelCacheObject = productExtDataCache.get(channel);
        final Map<String, ProductExtViewModel> vmMap = channelCacheObject.getViewModelMap();
        final List<ProductExtViewModel> viewModels = new ArrayList<>();
        for (String productId : productIds) {
            final ProductExtViewModel vm = vmMap.get(productId);
            if (vm != null) {
                viewModels.add(vm);
            }
        }
        return new ApiResult<>(true, "", "", viewModels);
    }

    public boolean updateProductExtDataCache(StoreApiChannels channel) throws Exception {
        final LocalDateTime now = LocalDateTime.now();
        final ProductExtDataCacheObject channelCacheObject = productExtDataCache.get(channel);
        final LocalDateTime cacheTs = channelCacheObject.getCacheTs();
        final boolean shouldUpdate = cacheTs == null || cacheTs.plusMinutes(CACHE_MINS).isBefore(now);

        if (shouldUpdate) {
            final com.enovax.star.cms.commons.model.api.ApiResult<List<AxRetailProductExtData>> result = axChannelProductsService.apiSearchProductExt(channel, true, "", 0L);
            if (!result.isSuccess()) {
                log.error("Error updating product ext data cache for " + channel + ". Result: " + JsonUtil.jsonify(result));
                return false;
            }
            final Map<String, ProductExtViewModel> vmMap = channelCacheObject.getViewModelMap();
            vmMap.clear();
            final Map<String, AxRetailProductExtData> dataMap = channelCacheObject.getDataMap();
            dataMap.clear();
            for (AxRetailProductExtData data : result.getData()) {
                dataMap.put(data.getProductId(), data);
                vmMap.put(data.getProductId(), ProductExtViewModel.fromExtData(data));
            }
            channelCacheObject.setCacheTs(LocalDateTime.now());

            log.info("Updated product ext data cache.");
            return true;
        }

        log.info("No update required.");
        return true;
    }
}
