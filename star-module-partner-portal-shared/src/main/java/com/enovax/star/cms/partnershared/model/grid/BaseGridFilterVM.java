package com.enovax.star.cms.partnershared.model.grid;

import java.util.List;

public class BaseGridFilterVM {
    private int take;
    private int skip;
    private int page;
    private int pageSize;
    private List<Sort> sort;


    public int getTake() {
        return take;
    }

    public void setTake(int take) {
        this.take = take;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<Sort> getSort() {
        return sort;
    }

    public void setSort(List<Sort> sort) {
        this.sort = sort;
    }

    public String getSortField(String defName){
        if(sort != null && sort.size() > 0 && sort.get(0).getField() != null && sort.get(0).getField().trim().length() > 0){
            return sort.get(0).getField().trim();
        }
        return defName;
    }

    public boolean isSortDesc(){
        if(sort != null && sort.size() > 0 && sort.get(0).getDir() != null && "asc".equalsIgnoreCase(sort.get(0).getDir().trim().toLowerCase())){
            return true;
        }
        return false;
    }

}
