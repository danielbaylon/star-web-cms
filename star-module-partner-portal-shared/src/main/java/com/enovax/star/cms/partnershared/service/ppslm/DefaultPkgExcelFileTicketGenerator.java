package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jennylynsze on 10/1/16.
 */
@Service
public class DefaultPkgExcelFileTicketGenerator implements IPkgExcelFileTicketGenerator {

    @Override
    public Workbook generatePkgExcel(MixMatchPkgVM mixMatchPkgVM, List<ETicketData> tickets) {

        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Mix and Match Package");
//        // DataFormat money = wb.createDataFormat();
//        // short dataFormatMoney = money.getFormat("0.00");
        Row r = null;
        Cell c = null;
//
        int row = 0;
//
        r = s.createRow(row);
        c = r.createCell(0);
        c.setCellValue("Package Name: " + mixMatchPkgVM.getName());

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Ticket Generated Date: " + mixMatchPkgVM.getTicketGeneratedDateStr());

        row++; //blank row

        r = s.createRow(++row);

        int cnum = writeReportHeader(r);

        for(ETicketData ticket: tickets) {
            r = s.createRow(++row);
            writePkg(ticket, r);
        }

        for (int i = 0; i <= cnum; i++) {
            s.autoSizeColumn(i);
        }

        return wb;
    }


    private int writeReportHeader(Row irow) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("Ticket No.");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Display Name");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Validity Period");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Price");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Barcode/QR Code");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Code Data");
        return cnum;
    }

    private int writePkg(ETicketData ticketData, Row irow) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue(ticketData.getTicketNumber());
        icell = irow.createCell(++cnum);
        icell.setCellValue(ticketData.getDisplayName());
        icell = irow.createCell(++cnum);
        icell.setCellValue("Valid from " + ticketData.getValidityStartDate() + " to " + ticketData.getValidityEndDate());
        icell = irow.createCell(++cnum);
        icell.setCellValue(ticketData.getTotalPrice());
        icell = irow.createCell(++cnum);
        icell.setCellValue(ticketData.isThirdParty()?"Barcode": "QR Code");
        icell = irow.createCell(++cnum);
        icell.setCellValue(ticketData.getCodeData());
        return cnum;
    }
}
