package com.enovax.star.cms.partnershared.service.ppmflg;


import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionItemVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public interface IPartnerInventoryService {
    List<InventoryTransactionItemVM> getInventoryTransactionsItem(
            Integer adminId, String fromDateStr, String toDateStr,
            String availableOnly, String prodNm, String orderBy,
            String orderWith, Integer page, Integer pagesize);

    long getInventoryTransactionsItemSize(Integer adminId,
                                                String fromDateStr, String toDateStr, String availableOnly,
                                                String prodNm);

    InventoryTransactionVM getTransByUser(Integer transId, PartnerAccount account);

    InventoryTransactionVM getTransById(Integer transId, String transType);

    InventoryTransactionVM getTransDetails(PPMFLGInventoryTransaction transaction, PPMFLGRevalidationTransaction revalTrans, PartnerAccount account, String transType);

    String generateReceiptAndEmail(Integer transId);

    String generateReceiptAndEmail(Integer transId, PPMFLGOfflinePaymentRequest req);

    String generateReceiptAndEmail(PPMFLGInventoryTransaction trans, PPMFLGOfflinePaymentRequest req);

    ApiResult<String> refundTrans(Integer transId, String userName);

    void emailAlertForExpiringTicket();

    void updateExpiredStatus();

    void updateForfeitedStatus();

    BigDecimal getUsedTransAmountToday(Integer mainAccountId);

    List<PPMFLGInventoryTransaction> getTransForVoidReconciliation(int minsPast);

}
