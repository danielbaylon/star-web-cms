package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccessRightsGroup;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppslm.UserResult;

import java.util.List;

/**
 * Created by lavanya on 26/8/16.
 */
public interface IPartnerSharedAccountService {
    public List<PPSLMTASubAccount> getSubUsers(PPSLMTAMainAccount mainAccount, boolean wipeSensitive);
    public PPSLMTASubAccount getSubUser(Integer subUserId, boolean wipeSensitive);
    public List<PPSLMTAAccessRightsGroup> getAccessRightsGroup();
    public PPSLMTAAccount getUser(String username, boolean wipeSensitive);
    public PPSLMTASubAccount getSubUser(String username, boolean wipeSensitive, boolean withAccessRights);
    public PPSLMTAMainAccount getMainUser(String username, boolean wipeSensitive, boolean withAccessRights);
    public boolean exceedSubUsers(Integer mainAccountId, Integer userId);
    public UserResult saveSubUser(boolean isNew, Integer userId, String username, String pwd, String encodedPwd, String email, String[] access,
                                  String status, String createdBy, String name, Integer mainAccountId, String title) throws Exception;

    public ResultVM resetPassword(Integer userId, String newPassword, String encodedPassword, String updatedBy) throws Exception;
    public void savePreviousPassword(PPSLMTAAccount user);
}
