package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.ReceiptVM;

/**
 * Created by jennylynsze on 10/10/16.
 */
public interface IPartnerRevalidationTransactionService {

//    List<PPSLMRevalidationTransaction> getTransactionsForStatusUpdate(int lowerBoundMins, int upperBoundMins);

    ReceiptVM getRevalidateInfoByTransId(Integer transId) throws Exception;

    ReceiptVM getRevalidateTransByTransId(Integer transId);

    public String generateRevalReceiptAndEmailByRevalidationId(Integer transId);

//    ReceiptVM getRevalidateTransByReceiptNum(String receiptNumber);

    String generateRevalReceiptAndEmail(PPSLMInventoryTransaction trans);

    String generateRevalReceiptAndEmail(Integer transId);

    PPSLMRevalidationTransaction findByReceipt(String receiptNum);

    PPSLMInventoryTransaction findInvenTransByReceipt(String receiptNum);

    boolean save(PPSLMRevalidationTransaction trans);
//
    boolean saveOnSuccessRevalidated(PPSLMRevalidationTransaction trans,
                                            PPSLMInventoryTransaction invTrans, String tmApvcd);
//
    public boolean sendVoidEmail(boolean success, PPSLMRevalidationTransaction revaltrans,
                                 boolean isSubAccount, PPSLMTASubAccount subAccount,
                                 PPSLMTAMainAccount mainAccount);
//
//    public PPSLMRevalidationTransaction initRevalTrans(PPSLMRevalidationTransaction revalTrans);
//
    boolean saveOnSuccessRevalidated(PPSLMRevalidationTransaction trans,
                                            PPSLMInventoryTransaction invTrans, String tmApvcd, boolean isfree);
//
//    public List<RevalidationTransaction> getTransForVoidReconciliation(int minsPast);
}
