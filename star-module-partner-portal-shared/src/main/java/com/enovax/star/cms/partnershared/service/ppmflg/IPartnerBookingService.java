package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxStarCart;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartDisplay;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerReceiptDisplay;
import com.enovax.star.cms.commons.tracking.Trackd;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface IPartnerBookingService {

//    String getMessageFromCheckoutResult(ApiResult<PPMFLGInventoryTransaction> result, boolean isOnlinePurchase);

    ApiResult<PartnerFunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId, String customerId);

    ApiResult<PartnerFunCartDisplay> cartUpdate(StoreApiChannels channel, FunCart cartToUpdate, String sessionId);

    ApiResult<PartnerFunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId);

    ApiResult<String> cartRecreate(StoreApiChannels channel, FunCart cart);

    boolean cartClear(StoreApiChannels channel, String sessionId);

    Integer getCartSize(StoreApiChannels channel, String sessionId);

    FunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId);

    FunCartDisplay constructCartDisplay(FunCart cart);

    CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session);

    ApiResult<InventoryTransactionVM> doCheckout(StoreApiChannels channel, HttpSession session, PartnerAccount partnerAccount, String ip);

    ApiResult<InventoryTransactionVM> doCheckout(StoreApiChannels channel, FunCart cart, PartnerAccount partnerAccount, String ip);

    ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session,  PartnerAccount partnerAccount);

    InventoryTransactionVM getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber);

    InventoryTransactionVM getStoreTransactionBySession(StoreApiChannels channel, HttpSession session);

    PartnerReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session, PartnerAccount account);

//    ApiResult<InventoryTransactionVM> completeSale(StoreApiChannels channel, String emailAddress, HttpSession session);

    PartnerReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber);

    PartnerReceiptDisplay getReceiptForDisplay(PPMFLGInventoryTransaction txn, PartnerAccount account);
//
//    ApiResult<InventoryTransactionVM> completeSale(StoreApiChannels channel, String receiptNumber, String emailAddress, AxStarCart checkoutCart);

    ApiResult<PartnerFunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, FunCart topupCart);

    public void saveTransactionInSession(StoreApiChannels channel, HttpSession session, InventoryTransactionVM txn);
    public InventoryTransactionVM getTransactionInSession(StoreApiChannels channel, HttpSession session);
    public void clearTransactionInSession(StoreApiChannels channel, HttpSession session);

    ApiResult<InventoryTransactionVM> makeTransaction(StoreApiChannels channel, String receiptNumber, PartnerAccount partnerAccount, FunCart cart, String ip, boolean b);

    ApiResult<InventoryTransactionVM> validateCheckOut(PartnerAccount partnerAccount, String ip, List<FunCartItem> cartItems);

    ApiResult<InventoryTransactionVM> finishAxReserveAndCheckout(StoreApiChannels channel, String receiptNumber, FunCart savedCart, String accountNumber);

    ApiResult<InventoryTransactionVM> createSalesOrder(StoreApiChannels channel, PPMFLGInventoryTransaction txn, String emailAddress, AxStarCart checkoutCart);

    ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd);

    ApiResult<InventoryTransactionVM> getFinalisedTransaction(StoreApiChannels channel, HttpSession session);
}
