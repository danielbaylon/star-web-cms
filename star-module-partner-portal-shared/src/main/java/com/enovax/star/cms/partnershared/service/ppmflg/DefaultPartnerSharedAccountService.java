package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.UserResult;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.partnershared.constant.AuditAction;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by lavanya on 26/8/16.
 */
@Service("PPMFLGIPartnerSharedAccountService")
public class DefaultPartnerSharedAccountService implements IPartnerSharedAccountService {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private PPMFLGTAMainAccountRepository mainAccRepo;

    @Autowired
    private PPMFLGTASubAccountRepository subAccRepo;

    @Autowired
    private PPMFLGPartnerRepository partnerRepo;

    @Autowired
    private PPMFLGTAAccessRightsGroupRepository accessRightsGroupRepo;

    @Autowired
    private PPMFLGTARightsMappingRepository rightsMappingRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerEmailService")
    private IPartnerEmailService partnerEmailService;

    @Autowired
    @Qualifier("PPMFLGIAuditTrailService")
    private IAuditTrailService auditTrailService;

    @Autowired
    private PPMFLGTAPrevPasswordMainRepository prevPasswordMainRepo;

    @Autowired
    private PPMFLGTAPrevPasswordSubRepository prevPasswordSubRepo;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamService;

    @Override
    @Transactional(readOnly = true)
    public List<PPMFLGTASubAccount> getSubUsers(PPMFLGTAMainAccount mainAccount,boolean wipeSensitive) {
        List<PPMFLGTASubAccount> users = this.subAccRepo.findByMainUser(mainAccount);
//        for(PPMFLGTASubAccount user: users){
//            _wipeSensitive(user);
//        }
        return users;
    }

    private void _wipeSensitive(PPMFLGTAMainAccount user) {
        user.setPassword("");
        user.setSalt("");
        user.setPrevPws(null);
    }

    private void _wipeSensitive(PPMFLGTASubAccount user) {
        user.setPassword("");
        user.setSalt("");
        user.setPrevPws(null);
    }

    @Override
    @Transactional(readOnly = true)
    public PPMFLGTASubAccount getSubUser(Integer subUserId, boolean wipeSensitive) {
        log.debug("TAAcountServiceImpl, findByUsername START->" + new Date());
        long lStartTime;
        long lEndTime;
        long difference;
        lStartTime = System.currentTimeMillis();
        final PPMFLGTASubAccount user = this.subAccRepo.findById(subUserId);
        lEndTime = System.currentTimeMillis();
        difference = lEndTime - lStartTime;
        log.debug("TAAcountServiceImpl, findByUsername, find by username->" + difference);
//        if (user != null && wipeSensitive) {
//            _wipeSensitive(user);
//        }
        lStartTime = System.currentTimeMillis();
        List<PPMFLGTARightsMapping> rightsMappingList = user.getRightsMapping();
        List<String> rightsMappingId = new ArrayList<>();
        for (PPMFLGTARightsMapping temp : rightsMappingList) {
            PPMFLGTAAccessRightsGroup group = temp.getAccessRightsGroup();
            rightsMappingId.add(group.getId().toString());
        }
        lEndTime = System.currentTimeMillis();
        difference = lEndTime - lStartTime;
        log.debug("TAAcountServiceImpl, findByUsername, access rights->" + difference);
        user.setRightsMappingId(rightsMappingId);
        log.debug("TAAcountServiceImpl, findByUsername END->" + new Date());

        return user;
    }

    @Override
    @Transactional
    public void savePreviousPassword(PPMFLGTAAccount user) {
        final Date now = new Date();
        if (user instanceof PPMFLGTAMainAccount) {
            final PPMFLGTAPrevPasswordMain ppw = new PPMFLGTAPrevPasswordMain();
            ppw.setCreatedDate(now);
            ppw.setUser((PPMFLGTAMainAccount) user);
            ppw.setPassword(user.getPassword());
            ppw.setSalt(user.getSalt());
            this.prevPasswordMainRepo.save(ppw);
        } else {
            final PPMFLGTAPrevPasswordSub ppw = new PPMFLGTAPrevPasswordSub();
            ppw.setCreatedDate(now);
            ppw.setUser((PPMFLGTASubAccount) user);
            ppw.setPassword(user.getPassword());
            ppw.setSalt(user.getSalt());
            this.prevPasswordSubRepo.save(ppw);
        }
    }

    @Override
    @Transactional
    public ResultVM resetPassword(Integer userId, String newPassword, String encodedPassword, String updatedBy) throws Exception{
        ResultVM resultVM = new ResultVM();
        final Date now = new Date();
        final PPMFLGTASubAccount user = this.subAccRepo.findById(userId);
        if(user == null) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.PartnerNotFound.message);
        }
        savePreviousPassword(user);


        user.setPassword(encodedPassword);
        user.setSalt("NA");
        user.setPwdLastModDt(now);
        user.setPwdAttempts(0);
        user.setPwdForceChange(true);

        user.setModifiedBy(updatedBy);
        user.setModifiedDate(now);

        this.subAccRepo.save(user);

       /* final Map<String, String> params = new HashMap<String, String>();
        params.put(":AccountHolderName", user.getName());
        params.put(":AccountLoginName", user.getUsername());
        params.put(":Password", password);
        params.put(":FaqLink", emailFaqLink);

        String mailBody = this.mailService.constructMailTemplate("ResetSubAccountPassword.html", params);
        final String email = user.getEmail();

        final MailProperties mailProps = new MailProperties("passwordReset-" + user.getUsername() + "-" + now.getTime(),
                new String[] { email }, null, null, emailSubjResetSubPassword, mailBody, true,
                this.mailService.getDefaultSender());
        try {
            this.mailService.sendEmail(mailProps, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    if (success) {
                        log.info("Successfully sent Reset Password email with ID: " + trackingId);
                    } else {
                        log.error("Failed sending Reset Password email with ID: " + trackingId);
                    }
                }
            });
        } catch (MessagingException me) {
            throw new RuntimeException("Error with email sending", me);
        }*/
        //send email
        try {
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(user);
            PPMFLGTAMainAccount mainAcc = user.getMainUser();
            if(mainAcc!=null) {
                partnerAccountVM.setMainAccountLoginName(mainAcc.getUsername());
            }
            partnerAccountVM.setPassword(newPassword);
   ///         partnerAccountVM.setFaqLink(para);
            partnerEmailService.sendPartnerSubaccountPasswordResetEmail(partnerAccountVM);
        }catch (Exception e) {
            throw new RuntimeException("Error with email sending", e);
        }
        final AuditAction action = AuditAction.ResetPassword;
        String auditDetails = String.format("User {%d:%s} password reset by %s.", user.getId(), user.getUsername(),
                updatedBy);

        this.auditTrailService.log(true, action, auditDetails, "SysUser", String.valueOf(user.getId()), updatedBy);
        return resultVM;
    }

    @Override
    @Transactional(readOnly=true)
    public List<PPMFLGTAAccessRightsGroup> getAccessRightsGroup() {
        return accessRightsGroupRepo.findByType(PartnerPortalConst.GROUPS_FOR_SUB);
    }

    @Override
    @Transactional(readOnly = true)
    public PPMFLGTAAccount getUser(String username, boolean wipeSensitive) {
        if (username.endsWith(PartnerPortalConst.TA_MAIN_ACC_POSTFIX)) {
            return getMainUser(username, wipeSensitive, false);
        } else {
            return getSubUser(username, wipeSensitive, false);
        }

    }

    @Override
    @Transactional(readOnly = true)
    public PPMFLGTASubAccount getSubUser(String username, boolean wipeSensitive, boolean withAccessRights) {
        final PPMFLGTASubAccount user = this.subAccRepo.findFirstByUsername(username);
        if(withAccessRights) {
            final Set<String> permissions = new HashSet<>();
            List<PPMFLGTARightsMapping> rightsMappingList = user.getRightsMapping();
            for(int i=0;i<rightsMappingList.size();i++) {
                PPMFLGTAAccessRightsGroup accessRightsGroup = rightsMappingList.get(i).getAccessRightsGroup();
                List<PPMFLGTAAccessRightsGroupMapping> accessRightsGroupMappingList = accessRightsGroup.getAccessRightsGroupMapping();
                for(int j=0;j<accessRightsGroupMappingList.size();j++) {
                    PPMFLGTAAccessRight accessRight = accessRightsGroupMappingList.get(j).getAccessRight();
                    permissions.add(accessRight.getName());
                    if(StringUtils.isNotEmpty(accessRight.getTopNavName())){
                        permissions.add(accessRight.getTopNavName());
                    }
                }
            }
            user.setAccessRights(permissions);
        }
        /*if (user != null && wipeSensitive) {
            _wipeSensitive(user);
        }*/
        return user;

    }

    @Override
    @Transactional(readOnly = true)
    public PPMFLGTAMainAccount getMainUser(String username, boolean wipeSensitive, boolean withAccessRights) {

        final PPMFLGTAMainAccount user = this.mainAccRepo.findFirstByUsername(username);
        if(withAccessRights) {
            final Set<String> permissions = new HashSet<>();
            List<PPMFLGTAMainRightsMapping> rightsMappingList = user.getRightsMapping();
            for(int i=0;i<rightsMappingList.size();i++) {
                PPMFLGTAAccessRightsGroup accessRightsGroup = rightsMappingList.get(i).getAccessRightsGroup();
                List<PPMFLGTAAccessRightsGroupMapping> accessRightsGroupMappingList = accessRightsGroup.getAccessRightsGroupMapping();
                for(int j=0;j<accessRightsGroupMappingList.size();j++) {
                    PPMFLGTAAccessRight accessRight = accessRightsGroupMappingList.get(j).getAccessRight();
                    permissions.add(accessRight.getName());
                    if(StringUtils.isNotEmpty(accessRight.getTopNavName())){
                        permissions.add(accessRight.getTopNavName());
                    }
                }
            }
            user.setAccessRights(permissions);
        }
        /*if (user != null && wipeSensitive) {
            _wipeSensitive(user);
        }*/
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean exceedSubUsers(Integer mainAccountId, Integer userId) {

        final int activeSubAccount = sysParamService.getMaxNoOfSubUsers();
        PPMFLGTAMainAccount mainAccount = mainAccRepo.findById(mainAccountId);
        int count = subAccRepo.find(mainAccount,userId).size();

        if (count >= activeSubAccount) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public UserResult saveSubUser(boolean isNew, Integer userId, String username, String pwd, String encodedPwd, String email, String[] access,
                                  String status, String createdBy, String name, Integer mainAccountId, String title) throws Exception {
        final Date now = new Date();
        final PPMFLGTASubAccount user = isNew ? new PPMFLGTASubAccount() : this.subAccRepo.findById(userId);

        String auditDetails = "";

        if (isNew) {
            user.setUsername(username);
            user.setPassword(encodedPwd);
            user.setPwdLastModDt(now);
            user.setCreatedBy(createdBy);
            user.setCreatedDate(now);
            user.setPwdForceChange(true);

            PPMFLGTAMainAccount mainAcc = new PPMFLGTAMainAccount();
            mainAcc.setId(mainAccountId);
            user.setMainUser(mainAcc);

        } else {
            List<PPMFLGTARightsMapping> rightsMapping = user.getRightsMapping();
            StringBuffer sb = new StringBuffer();
            for (PPMFLGTARightsMapping right : rightsMapping) {
                PPMFLGTAAccessRightsGroup accessRightsGroup = right.getAccessRightsGroup();
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(accessRightsGroup.getId());

            }

            auditDetails = String.format(
                    "[Previous Values] {accessRightsGroup:%s}, {email:%s}, {pwdAttempts:%d}, {status:%s}",
                    sb.toString(), user.getEmail(), user.getPwdAttempts(), user.getStatus());

            // delete the old RightsMapping
            for (PPMFLGTARightsMapping right : rightsMapping) {
                rightsMappingRepo.delete(right);
                //rightsMappingRepo.flush();
            }

            // add the new RightsMapping
            for (int i = 0; i < access.length; i++) {
                PPMFLGTARightsMapping right = new PPMFLGTARightsMapping();
                PPMFLGTAAccessRightsGroup accessRightsGroup = accessRightsGroupRepo.findById(new Integer(access[i]));
                right.setSubUser(user);
                right.setAccessRightsGroup(accessRightsGroup);
                rightsMappingRepo.save(right);
                //rightsMappingRepo.flush();
            }
            user.setPwdForceChange(false);
//            user.setRightsMapping(null);
//            user.setRightsMappingId(null);
        }

        user.setEmail(email);
        user.setPwdAttempts(0);
        user.setStatus(status);
        user.setName(name);
        user.setTitle(title);
        user.setSalt("NA");
        user.setModifiedBy(createdBy);
        user.setModifiedDate(now);

        subAccRepo.save(user);
        //subAccRepo.flush();
        if (isNew) {
            // add the new RightsMapping
            for (int i = 0; i < access.length; i++) {
                PPMFLGTARightsMapping right = new PPMFLGTARightsMapping();
                PPMFLGTAAccessRightsGroup accessRightsGroup = new PPMFLGTAAccessRightsGroup();
                accessRightsGroup.setId(new Integer(access[i]));
                right.setSubUser(user);
                right.setAccessRightsGroup(accessRightsGroup);
                rightsMappingRepo.save(right);
                //rightsMappingRepo.flush();
            }

            // send email
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(user);
            PPMFLGTAMainAccount mainAcc = mainAccRepo.findById(mainAccountId);
            if(mainAcc!=null) {
                partnerAccountVM.setMainAccountLoginName(mainAcc.getUsername());
            }
            partnerAccountVM.setPassword(pwd);
           /// partnerAccountVM.setFaqLink(PartnerPortalConst.FAQ_LINK);
            partnerEmailService.sendPartnerSubaccountCreationEmail(partnerAccountVM);


        }

        final AuditAction action = isNew ? AuditAction.AccountCreate : AuditAction.AccountUpdate;
        if (isNew) {
            auditDetails = String.format("User {%d:%s} created.", user.getId(), user.getUsername());
        } else {
            auditDetails = String.format("User {%d:%s} updated. " + auditDetails, user.getId(), user.getUsername());
        }
        this.auditTrailService.log(true, action, auditDetails, "SysUser", String.valueOf(user.getId()), createdBy);

        return new UserResult(UserResult.Result.Success, user);
    }


}