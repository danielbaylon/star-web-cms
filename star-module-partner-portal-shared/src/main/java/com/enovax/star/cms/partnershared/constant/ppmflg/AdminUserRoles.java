package com.enovax.star.cms.partnershared.constant.ppmflg;

/**
 * Created by houtao on 26/7/16.
 */
public enum AdminUserRoles {

    PartnerVerifyRole("pp-mflg-user-accounts"),
    OfflinePaymentViewRole("pp-mflg-view-offline-payment"),
    OfflinePaymentApproverRole("pp-mflg-approve-offline-payment"),
    ViewInventoryRole("pp-mflg-view-inventory"),
    ViewPackageRole("pp-mflg-view-package"),
    DeactivatePincodeRole("pp-mflg-deactivate-pincode"),
    RefundTransactionRole("pp-mflg-refund-transaction"),
    PartnerPortalSlmAdmin("pp-mflg-admin"),
    WOTBackendReservation("pp-mflg-wot-backend-reservation"),
    GeneralApproverRole("dynamic-pp-mflg-general-approver"),
    GeneralApproverBackupRole("dynamic-pp-mflg-general-backup-approver"),
    TransactionQuery("pp-mflg-transaction-query"),
    PincodeQuery("pp-mflg-pincode-query"),
    PartnerProfileReport("pp-mflg-partner-profile-report"),
    MarketShareReport("pp-mflg-market-share-report"),
    TransactionReport("pp-mflg-transaction-report"),
    InventoryReport("pp-mflg-inventory-report"),
    PackageReport("pp-mflg-package-report"),
    ExceptionReport("pp-mflg-exception-report")
    ;

    public final String code;

    private AdminUserRoles(String code) {
        this.code = code;
    }
}
