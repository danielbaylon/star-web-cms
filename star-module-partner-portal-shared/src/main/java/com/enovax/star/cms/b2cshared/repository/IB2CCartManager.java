package com.enovax.star.cms.b2cshared.repository;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.FunCart;

public interface IB2CCartManager {

    void saveCart(StoreApiChannels channel, FunCart cartToSave);

    FunCart getCart(StoreApiChannels channel, String cartId);

    void removeCart(StoreApiChannels channel, String cartId);
}
