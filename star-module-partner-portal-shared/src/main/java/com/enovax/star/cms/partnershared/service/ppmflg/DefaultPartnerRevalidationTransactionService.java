package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxInsertOnlineReference;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReceiptVM;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jennylynsze on 10/10/16.
 */
@Service("PPMFLGIPartnerRevalidationTransactionService")
public class DefaultPartnerRevalidationTransactionService implements  IPartnerRevalidationTransactionService {


    @Autowired
    PPMFLGInventoryTransactionRepository inventoryTransRepo;

    @Autowired
    private PPMFLGPartnerRepository partnerRepo;

    @Autowired
    @Qualifier("PPMFLGIRevalidationFeeService")
    private IRevalidationFeeService revalidationFeeService;

    @Autowired
    private PPMFLGRevalidationTransactionRepository revalidationTransRepo;

    @Autowired
    private PPMFLGAxSalesOrderRepository axSalesOrderRepo;
    @Autowired
    private PPMFLGAxCheckoutCartRepository axCheckoutCartRepo;

    @Autowired
    private PPMFLGTAMainAccountRepository mainAccountRepo;

    @Autowired
    private PPMFLGTASubAccountRepository subAccountRepo;

    @Autowired
    private AxStarService axStarService;

    @Autowired
    @Qualifier("PPMFLGIPartnerTncService")
    private IPartnerTncService partnerTncService;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;

    @Autowired
    @Qualifier("PPMFLGITemplateService")
    private ITemplateService templateSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerEmailService")
    private IPartnerEmailService partnerEmailSrv;

    @Autowired
    @Qualifier("PPMFLGIGSTRateService")
    private IGSTRateService gstRateService;

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;

    private Logger log = LoggerFactory.getLogger(this.getClass());


    //    @Override
//    public List<PPMFLGRevalidationTransaction> getTransactionsForStatusUpdate(int lowerBoundMins, int upperBoundMins) {
//        return null;
//    }

    @Override
    @Transactional(readOnly = true)
    public ReceiptVM getRevalidateInfoByTransId(Integer transId) throws Exception {
        PPMFLGInventoryTransaction trans = inventoryTransRepo.findOne(transId);
        PPMFLGPartner partner = partnerRepo.findByAccountCode(trans.getMainAccount().getAccountCode());
        //TODO get from jcr the Revalidation Item
        BigDecimal revalFee = revalidationFeeService.getRevalidationFeeInBigDecimalByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber(), partner.getRevalFeeItemId());
        BigDecimal normalRelPrice = revalFee;
        BigDecimal topupRelPrice = BigDecimal.ZERO; //TODO check if need to be from the APP, or no need since it's forever ZERO as per SRS

        Integer revalMonth = partner.getRevalPeriodMonths();
        BigDecimal gstRate = gstRateService.getCurrentGST();

        ReceiptVM receipt = new ReceiptVM(trans,gstRate ,normalRelPrice, topupRelPrice,
                revalMonth);
        return receipt;
    }

    @Override
    @Transactional(readOnly = true)
    public ReceiptVM getRevalidateTransByTransId(Integer transId) {
        PPMFLGInventoryTransaction trans = inventoryTransRepo.findOne(transId);
        Hibernate.initialize(trans.getInventoryItems());

        if (!Hibernate.isInitialized(trans.getRevalTransList())) {
            Hibernate.initialize(trans.getRevalTransList());
        }

        PPMFLGRevalidationTransaction revalTrans = trans.getRevalidateTrans();

        ReceiptVM receipt = new ReceiptVM(trans, revalTrans, "", "", ""); //TODO check what is the paratmer to be passed
        return receipt;
    }

    @Override
    public String generateRevalReceiptAndEmail(PPMFLGInventoryTransaction trans) {
        Hibernate.initialize(trans.getInventoryItems());

        if (!Hibernate.isInitialized(trans.getRevalTransList())) {
            Hibernate.initialize(trans.getRevalTransList());
        }
        PPMFLGRevalidationTransaction revalTrans = trans.getRevalidateTrans();

        Set<String> prodIds = new LinkedHashSet<>();
        for (PPMFLGInventoryTransactionItem item : trans.getInventoryItems()) {
            prodIds.add(item.getProductId());
        }
        for (PPMFLGInventoryTransactionItem item : revalTrans.getRevalItems()) {
            log.info("item.getProductId()" + item.getProductId());
        }

        final List<TncVM> tncs = this.partnerTncService.getProductTncs(StoreApiChannels.PARTNER_PORTAL_MFLG,new ArrayList<>(
                prodIds));


        String receiptName = revalTrans.getReceiptNum() + (revalTrans.getReprintCount() == 0 ? "" : "_" + revalTrans.getReprintCount());

        String genpdfpath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + receiptName
                + FileUtil.FILE_EXT_PDF;

        Map<String,String> map = paramSrv.getSentosaContactDetails();
        ReceiptVM receipt = new ReceiptVM(trans, revalTrans, paramSrv.getApplicationContextPath(), map.get(PartnerPortalConst.ADDRESS_OWN), map.get(PartnerPortalConst.CONTACT_OWN));

        receipt.setHasTnc(!tncs.isEmpty());
        receipt.setTncs(tncs);

        log.info("Step 2: Generate PDF to :" + genpdfpath);
        try {
            templateSrv.generatePartnerRevalTransactionReceiptPdf(receipt, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed to Generate Receipt , please contact system admin!";
        }
        Boolean isSuccess = sendReceiptGenerateEmail(trans, revalTrans,
                receipt, receiptName, genpdfpath);
        if (isSuccess) {
            revalTrans.setReprintCount(revalTrans.getReprintCount() + 1);
            revalidationTransRepo.save(revalTrans);
            return "Regenerate Receipt Successfully, please verify your email. ";
        } else {
            return "Failed to Send Email, please contact system admin!";
        }
    }

//    @Override
//    public ReceiptVM getRevalidateTransByReceiptNum(String receiptNumber) {
//       PPMFLGRevalidationTransaction revalTrans = revalidationTransRepo.findByReceiptNum(receiptNumber);
//       PPMFLGInventoryTransaction invTrans = revalTrans.getTransaction();
//
//       ReceiptVM receipt = new ReceiptVM(invTrans, revalTrans, "", "", "");
//       return receipt;
//    }

    @Override
    @Transactional
    public String generateRevalReceiptAndEmail(Integer transId) {
        PPMFLGInventoryTransaction trans = inventoryTransRepo.findOne(transId);
        return generateRevalReceiptAndEmail(trans);
    }


    @Override
    @Transactional
    public String generateRevalReceiptAndEmailByRevalidationId(Integer transId) {
        PPMFLGRevalidationTransaction trans = revalidationTransRepo.findById(transId);
        PPMFLGInventoryTransaction invTxn = inventoryTransRepo.findOne(trans.getTransactionId());
        return generateRevalReceiptAndEmail(invTxn);
    }

    private Boolean sendReceiptGenerateEmail(PPMFLGInventoryTransaction trans,
                                             PPMFLGRevalidationTransaction revalTrans, ReceiptVM receipt,
                                             String receiptName, String genpdfpath)  {
        try {
            partnerEmailSrv.sendRevalTransactionReceiptEmail(trans, revalTrans, receipt, receiptName, genpdfpath);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return false;
    }


    @Override
    @Transactional(readOnly = true)
    public PPMFLGRevalidationTransaction findByReceipt(String receiptNum) {
        PPMFLGRevalidationTransaction revalTrans = revalidationTransRepo.findByReceiptNum(receiptNum);
        return revalTrans;
    }

    @Override
    public PPMFLGInventoryTransaction findInvenTransByReceipt(String receiptNum) {
        return null;
    }

    @Override
    @Transactional
    public boolean save(PPMFLGRevalidationTransaction trans) {
        revalidationTransRepo.save(trans);
        return true;
    }

    @Override
    @Transactional
    public boolean saveOnSuccessRevalidated(PPMFLGRevalidationTransaction trans, PPMFLGInventoryTransaction invTrans, String tmApvcd) {
        return this.saveOnSuccessRevalidated(trans, invTrans, tmApvcd, false);
    }

    @Override
    public boolean sendVoidEmail(boolean success, PPMFLGRevalidationTransaction revaltrans, boolean isSubAccount, PPMFLGTASubAccount subAccount, PPMFLGTAMainAccount mainAccount) {
        //TODO
        return false;
    }

    @Override
    @Transactional
    public boolean saveOnSuccessRevalidated(PPMFLGRevalidationTransaction trans, PPMFLGInventoryTransaction invTrans, String tmApvcd, boolean isfree) {
        trans.setStatus(TicketStatus.Revalidated.toString());
        if(isfree){
            trans.setTmStatus(EnovaxTmSystemStatus.NA.toString());
        }else{
            trans.setTmStatus(EnovaxTmSystemStatus.Success.toString());
        }
        trans.setTmApprovalCode(tmApvcd);
        trans.setTmStatusDate(new Date());

        revalidationTransRepo.save(trans);

        //create sales order?
        boolean salesOrderRes =  createSalesOrder(trans);
        if(!salesOrderRes) {
            return false;
        }

        invTrans.setRevalidated(true);
        invTrans.setStatus(TicketStatus.Revalidated.toString());
        PPMFLGPartner partner = partnerRepo.findByAccountCode(invTrans.getMainAccount().getAccountCode());
        Integer revalMonth = partner.getRevalPeriodMonths();
        Date newValiEndDate = TransactionUtil.getNewValidateEndDate(
                invTrans.getValidityEndDate(), revalMonth);
        invTrans.setValidityEndDate(newValiEndDate);
        invTrans.setModifiedDate(new Date());
        inventoryTransRepo.save(invTrans);

        return true;
    }

    @Transactional
    private boolean createSalesOrder(PPMFLGRevalidationTransaction trans) {

        //TODO evaluate how to refactor correctly
        PPMFLGAxCheckoutCart checkoutCartFromDB = axCheckoutCartRepo.findByReceiptNum(trans.getReceiptNum());
        AxStarCart checkoutCart = JsonUtil.fromJson(checkoutCartFromDB.getCheckoutCartJson(), AxStarCart.class);

        AxStarInputSalesOrderCreation salesOrderCreation = new AxStarInputSalesOrderCreation();
        salesOrderCreation.setCartId(checkoutCart.getId());
        salesOrderCreation.setCustomerId(checkoutCart.getCustomerId());
        salesOrderCreation.setSalesOrderNumber(trans.getReceiptNum());

        String email = "";
        if(trans.getSubAccountTrans()) {
            List<PPMFLGTASubAccount> subAccountList = subAccountRepo.findByUsername(trans.getUsername());
            if(subAccountList != null && subAccountList.size() > 0) {
                email = subAccountList.get(0).getEmail();
            }
        }else {
            List<PPMFLGTAMainAccount> mainAccountList = mainAccountRepo.findByUsername(trans.getUsername());
            if(mainAccountList != null && mainAccountList.size() > 0) {
                email = mainAccountList.get(0).getEmail();
            }
        }

        salesOrderCreation.setEmail(email);

        final Map<String, String> soExtProps = new HashMap<>();
        soExtProps.put("SalesPoolId", "Direct");
        salesOrderCreation.setStringExtensionProperties(soExtProps);

        AxStarInputTenderDataLine tenderDataLine = new AxStarInputTenderDataLine();
        tenderDataLine.setAmount(trans.getTotal());

        //TODO TEMPORARY ONLY, PLEASE REMOVE
        String tenderType = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.tenderType");

        if(StringUtils.isEmpty(tenderType)) {
            tenderType = "Credit Card (B2B/B2C)"; //default
            //tenderDataLine.setTenderType("Credit Card (B2B/B2C)"); //TODO put to JCR better
            //tenderDataLine.setTenderType("VMDBS");
        }

        tenderDataLine.setTenderType(tenderType);
        salesOrderCreation.setTenderDataLines(Collections.singletonList(tenderDataLine));

        final AxStarServiceResult<AxStarSalesOrder> axStarSalesOrderResult = axStarService.apiSalesOrderCreate(StoreApiChannels.PARTNER_PORTAL_MFLG, salesOrderCreation) ;
        if (!axStarSalesOrderResult.isSuccess()) {
            log.info("Error encountered on AxStarCreateSalesOrder. " + JsonUtil.jsonify(axStarSalesOrderResult));
            //TODO errHandleng HOW AH HOW
            return false;
        }


        //safe spot for me to call confirm
        AxInsertOnlineReference onlineRef = new AxInsertOnlineReference();
        onlineRef.setTransactionId(trans.getReceiptNum());
        onlineRef.setTransDate(new Date());
        try {
            ApiResult<String> confirmSalesOrderRes =  axChannelTransactionService.apiInsertOnlineReference(StoreApiChannels.PARTNER_PORTAL_MFLG, onlineRef);;
            if(!confirmSalesOrderRes.isSuccess())  {
                log.info("Error encountered on Confirm Sales Order. " + confirmSalesOrderRes.getMessage());
                return false;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }


        //delete the AxCartCheckout as it is no longer needed
        try {
            PPMFLGAxCheckoutCart axCheckoutCartFromDB = axCheckoutCartRepo.findByReceiptNum(trans.getReceiptNum());
            axCheckoutCartRepo.delete(axCheckoutCartFromDB);
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return true;
    }
}
