package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprovalLog;
import com.enovax.star.cms.commons.model.partner.ppmflg.ApprovalLogVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReasonVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.RequestApprovalVM;
import com.enovax.star.cms.commons.model.system.SysParamVM;

import java.util.List;

public interface IApprovalLogService {

    RequestApprovalVM populatePendingApprovalRequestVM(String loginAdminId);

    String approvePendingRequest(Integer appId, String loginAdminId) throws Exception;

    String rejectPendingRequest(Integer appId, ReasonVM reasonVM, String loginAdminId) throws Exception;

    public boolean hasPendingRequest(String key, String cate) throws Exception;

    List<ApprovalLogVM> getHistoryLog(Integer paId);

    PPMFLGApprovalLog getLogById(Integer appId);

    PartnerVM getPartnerFromHist(String hist) throws Exception;

    public PPMFLGApprovalLog logSystemParamRequest(SysParamVM sysParam, String label, String newValue,
                                                  String userNm);

    int getPendingRequestForProdCnt(String prodId);

}
