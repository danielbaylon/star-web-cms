package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;

import java.util.Map;

/**
 * Created by houtao on 9/9/16.
 */
public interface IPartnerSharedService {
    PPMFLGPartner getPartnerById(Integer partnerId);

    PPMFLGPartner getPartnerByMainAccountId(Integer mainAccountId);

    String getPartnerNameByMainAccountIdWithCache(Map<String, String> paNameMapping, Integer mainAccountId);

    Integer getPartnerIdByMainAccountIdWithCache(Map<Integer, Integer> paNameMapping, Integer mainAccountId);

    Integer getWoTReservationCap(Integer id) throws Exception;

    public boolean resetAllPartnerReservationCap(String adminId) throws Exception;

    void selfUpdatePartnerProfile(PartnerAccount partnerAccount, PartnerVM partnerVM) throws BizValidationException;

    void syncSelfUpdatedProfileDetailsToAX(PartnerAccount partnerAccount) throws Exception;

    void selfUpdateProfileAndSyncProfileChangesToAX(PartnerAccount partnerAccount, PartnerVM partnerVM) throws Exception;

    String getActiveExtensionPropertyValue(String correspondenceAddress, Integer id);

    PPMFLGPartner getPartnerByAccountCodeAndMainAccountId(Integer id, String accountCode);
}
