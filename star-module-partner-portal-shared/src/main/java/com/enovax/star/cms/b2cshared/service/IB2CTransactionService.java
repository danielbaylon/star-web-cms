package com.enovax.star.cms.b2cshared.service;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionExtension;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionItem;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionExtension;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionItem;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.booking.StoreTransactionExtensionData;
import com.enovax.star.cms.commons.model.booking.StoreTransactionItem;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jensen on 12/11/16.
 */
public interface IB2CTransactionService {

    @Transactional
    void saveTelemoneyLog(StoreApiChannels channel, TelemoneyResponse response, boolean isFromQuery);

    @Transactional
    void saveStoreTransaction(StoreApiChannels channel, StoreTransaction txn, FunCart cart);

    @Transactional
    StoreTransactionExtensionData getStoreTransactionExtensionData(StoreApiChannels channel, String receiptNumber);

    @Transactional
    void updateStoreTransactionExtensionData(StoreApiChannels channel, StoreTransactionExtensionData dataPackage);

    @Transactional
    B2CSLMStoreTransaction getB2CSLMStoreTransaction(String receiptNumber);

    @Transactional
    void saveB2CSLMStoreTransaction(B2CSLMStoreTransaction txn);

    @Transactional
    B2CMFLGStoreTransaction getB2CMFLGtoreTransaction(String receiptNumber);

    @Transactional
    void saveB2CMFLGStoreTransaction(B2CMFLGStoreTransaction txn);

    B2CSLMStoreTransaction constructB2CSLMTransaction(StoreTransaction st);

    B2CMFLGStoreTransaction constructB2CMFLGTransaction(StoreTransaction st);

    B2CSLMStoreTransactionItem constructB2CSLMTransactionItem(StoreTransaction st, StoreTransactionItem sti,
                                                              B2CSLMStoreTransaction txn);

    B2CMFLGStoreTransactionItem constructB2CMFLGTransactionItem(StoreTransaction st, StoreTransactionItem sti,
                                                                B2CMFLGStoreTransaction txn);

    StoreTransactionExtensionData constructExtDataFromDB(B2CSLMStoreTransactionExtension slmExt, B2CMFLGStoreTransactionExtension mflgExt);
}
