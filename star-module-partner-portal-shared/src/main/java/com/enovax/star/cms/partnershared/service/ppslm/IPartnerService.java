package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.partnershared.model.grid.PartnerGridFilterVM;
import info.magnolia.cms.beans.runtime.Document;
import org.springframework.data.domain.Page;

import javax.jcr.RepositoryException;
import java.util.List;


public interface IPartnerService {

    ApiResult getPartnersByPage(PartnerGridFilterVM filterVM);

    List<PartnerVM> getPartnerVMsByStatus(String status);

    Page<PPSLMPartner> getPartnersByParams(PartnerGridFilterVM gridFilterVM) throws Exception;

    List<PartnerVM> getPartnerVMs(Page<PPSLMPartner> result);

    PartnerVerificationDetailsVM getPartnerVerificationDetailsViewModel(StoreApiChannels channel, Integer paId) throws BizValidationException;

    List<AdminAccountVM> getAssignedSalesAdminByCountryId(String ctyId);

    String revertPartnerProfileResubmit(Integer id, String reasonVmJson, String userId) throws Exception;

    String apiRevertPartnerProfileReject(Integer id, String reasonVmJson, String userId) throws Exception;

    String apiSubmitPartnerProfile(String paVmJson, String userId) throws Exception;

    ApprovingPartnerVM getPendingApprovalPartnerDetailsByApprovalIdAndUserId(StoreApiChannels channel, String loginAdminId, Integer paId) throws Exception;

    String apiApprovePartnerRegistrationRequest(Integer appId, PPSLMApprovalLog request, String adminLoginId) throws Exception;

    String apiRejectPartnerRegistrationRequest(StoreApiChannels channel, Integer appId, PPSLMApprovalLog request, ReasonVM reasonVM, String adminLoginId) throws Exception;

    ApiResult<List<PartnerVM>> getAllWingsOfTimePartners() throws Exception;

    com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM viewPartnerDetails(Integer paId, String adminLoginId) throws Exception;

    PartnerMaintenanceVM getPartnerMaintenanceViewModel(StoreApiChannels channel, Integer paId, String adminId) throws Exception;

    PartnerVM getPartnerVmById(Integer paId) throws Exception;

    public com.enovax.star.cms.commons.model.api.ApiResult<SubuserVM> getSubuser(Integer paId);

    public com.enovax.star.cms.commons.model.api.ApiResult<String> getPartnerHist(Integer paId);

    public com.enovax.star.cms.commons.model.api.ApiResult<String> getPreviousPartnerHist(Integer appId) throws Exception;

    PPSLMPartner getPartnerById(Integer paId);

    ResultVM changePartnerStatus(Integer paId, String userNm, String status) throws RepositoryException;

    void changeOrgType(Integer paId, String userNm, String typeId) throws RepositoryException;

    void saveGeneralLog(Integer paId, String userNm, String desc,
                        String prePaXml) throws RepositoryException;

    public ResultVM changePartnerEmail(Integer id, String userNm, String email) throws RepositoryException;

    ResultVM changePaUEN(Integer paId, String userNm, String uen) throws RepositoryException;

    ResultVM changePaLicense(Integer paId, String userNm, String licenseNum) throws RepositoryException;

    PartnerDocumentVM savePartnerDocument(int paId, Document doc, String paFileType, String fileName, String userName) throws Exception;

    public com.enovax.star.cms.commons.model.api.ApiResult<String> saveSubuser(int paId, boolean isNew, Integer userId, String userName, String name, String email, String title, String status, String access, String createdBy) throws Exception;

    public ResultVM resetPassword(int userId, String updatedBy) throws Exception;

    public String submitEditPa(String paVmJson, String userId) throws Exception;

    public void apiReUpdatePartnerDetailsAfterRegistration(Integer paId);

    public String addExclProd(String paVmJson);

    public String apiApprovePartnerUpdateRequest(Integer appId, PPSLMApprovalLog request, String adminLoginId) throws Exception;

    public String apiRejectPartnerUpdateRequest(StoreApiChannels channel, Integer appId, PPSLMApprovalLog request, ReasonVM reasonVM, String adminLoginId);

    List<PartnerVM> getAllPartnersByStatus(String code);
}
