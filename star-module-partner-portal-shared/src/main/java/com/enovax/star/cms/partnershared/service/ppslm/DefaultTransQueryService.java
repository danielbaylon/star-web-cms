package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.partnershared.repository.ppslm.ReportDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 7/11/16.
 */
@Service
public class DefaultTransQueryService implements ITransQueryService {

    private static final Logger log = LoggerFactory.getLogger(DefaultTransQueryService.class);

    public static final String RECEIPT_MSG_SUCCESS = "Regenerate Receipt Successfully, please verify your email.";
    public static final String RECEIPT_MSG_FAILED = "Failed to Generate Receipt , please contact system admin!";

    @Autowired
    private ReportDao rptDAO;

    @Autowired
    private PPSLMMixMatchPackageItemRepository pkgItemRepo;

    @Autowired
    private PPSLMInventoryTransactionRepository txnRepo;

    @Autowired
    private IPartnerSharedAccountService accSrv;

    @Autowired
    private IPartnerSharedService paSrv;

    @Autowired
    private PPSLMPartnerRepository paRepo;

    @Autowired
    private PPSLMTAMainAccountRepository taMainAccRepo;

    @Autowired
    private PPSLMTASubAccountRepository taSubAccRepo;

    @Autowired
    private PPSLMInventoryTransactionItemRepository txnItemRepo;

    @Autowired
    private PPSLMOfflinePaymentRequestReqpository offlinePayReqRepo;

    @Autowired
    private PPSLMRevalidationTransactionRepository revalTxnRepo;

    @Autowired
    private IPartnerInventoryService paInvtTxnSrv;

    @Autowired
    private IPartnerRevalidationTransactionService paRevalTxnSrv;

    @Autowired
    private IPartnerPkgService paPkgSrv;

    @Override
    public ApiResult<List<TransQueryVM>> getTransQueryResults(TransQueryFilterVM ft, int take, int skip, int page, int pageSize, String sortField, String sortDirection) {
        List<TransQueryVM> list = rptDAO.getTransByPage(ft, sortField, sortDirection, page, pageSize);
        int total = rptDAO.getTransByPageSize(ft);
        return new ApiResult<List<TransQueryVM>>(true, "","", total, list);
    }

    @Override
    public ApiResult<List<PackageViewResultVM>> getPinCodeQueryResults(PinQueryFilterVM ft, int take, int skip, int page, int pageSize, String sortField, String sortDirection) {
        List<PackageViewResultVM> list = rptDAO.getPinQueryByPage(ft, sortField, sortDirection, page, pageSize);
        int total = rptDAO.getPinQueryByPageSize(ft);
        return new ApiResult<List<PackageViewResultVM>>(true, "","", total, list);
    }

    @Override
    public ApiResult<List<PackageItemViewResultVM>> getPackageItemListByPackageId(Integer packageId) {
        List<PackageItemViewResultVM> list = new ArrayList<>();
        List<PPSLMMixMatchPackageItem> items = pkgItemRepo.findByPackageId(packageId);
        if(items != null && items.size() > 0){
            for(PPSLMMixMatchPackageItem i : items){
                PackageItemViewResultVM t = new PackageItemViewResultVM();
                t.setId(i.getId());
                t.setPackageId(i.getPackageId());
                t.setReceiptNum(i.getReceiptNum());
                t.setDisplayName(i.getDisplayName());
                t.setQty(i.getQty());
                t.setTransItemType(i.getTransItemType());
                t.setTicketType(i.getTicketType());
                t.setItemType(i.getItemType());
                if(i.getQtyRedeemed() != null){
                    t.setQtyRedeemed(i.getQtyRedeemed());
                }else{
                    t.setQtyRedeemed(0);
                }
                list.add(t);
            }
        }
        return new ApiResult<List<PackageItemViewResultVM>>(true, "", "", list.size(), list);
    }

    @Transactional(readOnly = true)
    public ApiResult<TransDetailVM> getTransDetails(String id, String transType) throws BizValidationException {
        if(id == null || id.trim().length() == 0 || Integer.parseInt(id.trim()) == 0){
            throw new BizValidationException("Invalid Request");
        }
        if(transType == null || transType.trim().length() == 0){
            throw new BizValidationException("Invalid Request");
        }
        if(!(TransDetailVM.PURCHASE.equalsIgnoreCase(transType) || TransDetailVM.REVALIDATE.equalsIgnoreCase(transType))){
            throw new BizValidationException("Invalid Request");
        }
        Integer txnId = Integer.parseInt(id.trim());
        TransDetailVM vm = new TransDetailVM();
        PPSLMInventoryTransaction txn = null;
        List<PPSLMInventoryTransactionItem> items = null;
        PPSLMPartner pa = null;
        if(TransDetailVM.PURCHASE.equalsIgnoreCase(transType)){
            txn = txnRepo.findById(txnId);
            if(txn == null){
                throw new BizValidationException("Invalid Request, Transaction not found.");
            }
            items = txnItemRepo.findByTransactionId(txn.getId());
            if(items == null || items.size() == 0){
                throw new BizValidationException("Invalid Request, Transaction items not found.");
            }
            Integer taMainAccountId = txn.getMainAccountId();
            if(taMainAccountId == null || taMainAccountId.intValue() == 0){
                throw new BizValidationException("Invalid Request, Main-account of this transaction not found.");
            }
            PPSLMTAMainAccount mainAcc = taMainAccRepo.findById(taMainAccountId);
            if(mainAcc == null){
                throw new BizValidationException("Invalid Request, Main-account of this transaction not found.");
            }
            pa = paRepo.findFirstByAdminId(taMainAccountId);
            if(pa == null){
                throw new BizValidationException("Invalid Request, Partner profile of this transaction not found.");
            }
            String email  = pa.getEmail();
            PPSLMTASubAccount subAcc = null;
            if(txn.isSubAccountTrans()){
                subAcc = taSubAccRepo.findFirstByUsername(txn.getUsername());
                if(subAcc == null){
                    throw new BizValidationException("Invalid Request, Sub-account of this transaction not found.");
                }
                if(subAcc != null){
                    if(email != null && email.trim().length() > 0){
                        email += ", ";
                    }
                    email += subAcc.getEmail();
                }
            }
            vm.setEmail(email);
            PPSLMOfflinePaymentRequest offlinePayReq = offlinePayReqRepo.findFirstByTransactionId(txnId);
            vm.populatePurchaseTransactionDetails(txn, items, pa, mainAcc, subAcc, offlinePayReq);
        }else{
            PPSLMRevalidationTransaction revalTxn = revalTxnRepo.findById(txnId);
            if(revalTxn == null){
                throw new BizValidationException("Invalid Request, Revalidation transaction not found.");
            }
            Integer purchaseTxnId = revalTxn.getTransactionId();
            if(purchaseTxnId == null || purchaseTxnId.intValue() == 0){
                throw new BizValidationException("Invalid Request, Purchase transaction not found.");
            }
            txn = txnRepo.findById(purchaseTxnId);
            if(txn == null){
                throw new BizValidationException("Invalid Request, Purchase transaction not found.");
            }
            if(revalTxn.getRevalDetail() == null){
                items = txnItemRepo.findByTransactionId(purchaseTxnId);
            }else{
                items = revalTxn.getRevalItems();
            }
            if(items == null || items.size() == 0){
                throw new BizValidationException("Invalid Request, Revalidation transaction items not found.");
            }
            Integer taMainAccountId = revalTxn.getMainAccountId();
            if(taMainAccountId == null || taMainAccountId.intValue() == 0){
                throw new BizValidationException("Invalid Request, Main-account of this transaction not found.");
            }
            PPSLMTAMainAccount mainAcc = taMainAccRepo.findById(taMainAccountId);
            if(mainAcc == null){
                throw new BizValidationException("Invalid Request, Main-account of this transaction not found.");
            }
            pa = paRepo.findFirstByAdminId(taMainAccountId);
            if(pa == null){
                throw new BizValidationException("Invalid Request, Partner profile of this transaction not found.");
            }
            String email  = pa.getEmail();

            PPSLMTASubAccount subAcc = null;
            if(txn.isSubAccountTrans()){
                subAcc = taSubAccRepo.findFirstByUsername(txn.getUsername());
                if(subAcc != null){
                    if(email != null && email.trim().length() > 0){
                        email += ", ";
                    }
                    email += subAcc.getEmail();
                }
            }
            vm.setEmail(email);
            vm.populateRevalidationTransactionDetails(revalTxn, txn, items, pa, mainAcc, subAcc);
        }
        if(txn != null && txn.getId() != null && txn.getId().intValue() > 0){
            PPSLMOfflinePaymentRequest offlinePayReq = offlinePayReqRepo.findFirstByTransactionId(txn.getId());
            if(offlinePayReq != null){
                OfflinePaymentRequestVM offPayVM = OfflinePaymentRequestVM.buildPaymentRequest(offlinePayReq, txn, pa.getOrgName(), pa.getAccountCode());
                if(offPayVM != null){
                    vm.setOfflinePaymentRequestVM(offPayVM);
                }
            }
        }
        return new ApiResult<>(true, null, null, vm);
    }

    public String resendingTransReceipt(String id, String transType) throws BizValidationException {
        if(id == null || id.trim().length() == 0 || Integer.parseInt(id.trim()) == 0){
            throw new BizValidationException("Invalid Request");
        }
        if(transType == null || transType.trim().length() == 0){
            throw new BizValidationException("Invalid Request");
        }
        if(!(TransDetailVM.PURCHASE.equalsIgnoreCase(transType) || TransDetailVM.REVALIDATE.equalsIgnoreCase(transType))){
            throw new BizValidationException("Invalid Request");
        }
        try{
            if(TransDetailVM.PURCHASE.equalsIgnoreCase(transType)){
                return paInvtTxnSrv.generateReceiptAndEmail(Integer.parseInt(id.trim()));
            }else{
                return paRevalTxnSrv.generateRevalReceiptAndEmailByRevalidationId(Integer.parseInt(id.trim()));
            }
        }catch (Exception ex){
            log.error("resendingTransReceipt : "+ex.getMessage(), ex);
        }catch (Throwable th){
            log.error("resendingTransReceipt : "+th.getMessage(), th);
        }
        return RECEIPT_MSG_FAILED;
    }

    @Override
    public String resendingPkgReceipt(String id) throws BizValidationException {
        if(id == null || id.trim().length() == 0 || Integer.parseInt(id.trim()) == 0){
            throw new BizValidationException("Invalid Request");
        }
        return paPkgSrv.resendingPkgReceiptEmailByPkgId(Integer.parseInt(id.trim()));
    }
}
