package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGApproverRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGTAMainAccountRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGTASubAccountRepository;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.partnershared.constant.ppmflg.ApproverTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("PPMFLGIPartnerEmailService")
public class DefaultPartnerEmailService implements IPartnerEmailService {

    public static final Logger log = LoggerFactory.getLogger(DefaultPartnerEmailService.class);

    public static final String BASE_URL = "##BASE_URL##";
    public static final String ORG_NAME = "##ORG_NAME##";

    @Autowired
    private IMailService mailSrv;
    @Autowired
    @Qualifier("PPMFLGIEmailTemplateService")
    private IEmailTemplateService mailTplSrv;
    @Autowired
    @Qualifier("PPMFLGITemplateService")
    private ITemplateService tplSrv;
    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;
    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService adminSrv;
    @Autowired
    private PPMFLGApproverRepository approverRepo;
    @Autowired
    private PPMFLGTASubAccountRepository subAccountRepo;
    @Autowired
    private PPMFLGTAMainAccountRepository mainAccountRepo;

    public void sendSignupEmail(PartnerVM paVM, PartnerHist paHist) throws Exception {
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_SIGNUP_EMAIL);
        SysEmailTemplateVM footerVM  = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, paVM.getOrgName());
        String content = contentVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String footer  = footerVM.getBody();
        String processedFooterContent = processEmailParams(footer, paramMap);
        paVM.setBaseUrl(paramSrv.getApplicationContextPath());
        paVM.setPreDefinedBody(processedContent);
        paVM.setPreDefinedFooter(processedFooterContent);
        String mailBody = tplSrv.generatePartnerRegistrationSignupEmailHtml(paVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ paVM.getEmail() });
        props.setBody(mailBody);
        props.setBodyHtml(true);
        props.setTrackingId("REGISTER-"+paVM.getOrgName()+"-PROFILE-"+System.currentTimeMillis());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendSignupUpdateEmail(PartnerVM paVM, PartnerHist paHist) throws Exception {
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_SIGNUP_UPDATE_EMAIL);
        SysEmailTemplateVM footerVM  = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, paVM.getOrgName());
        String content = contentVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String footer  = footerVM.getBody();
        String processedFooterContent = processEmailParams(footer, paramMap);
        paVM.setBaseUrl(paramSrv.getApplicationContextPath());
        paVM.setPreDefinedBody(processedContent);
        paVM.setPreDefinedFooter(processedFooterContent);
        String mailBody = tplSrv.generatePartnerRegistrationSignupUpdateEmailHtml(paVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ paVM.getEmail() });
        props.setBody(mailBody);
        props.setBodyHtml(true);
        props.setTrackingId("UPDATE-"+paVM.getOrgName()+"-PROFILE-"+System.currentTimeMillis());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendSignupAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception {
        paVM.setBaseUrl(paramSrv.getAdminApplicationContextPath());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_ALERT);
        String mailContent = tplSrv.generatePartnerRegistrationAlertEmailHtml(paVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ paramSrv.getDefaultPartnerRegistrationEmailReceiver() });
        props.setBody(mailContent);
        props.setBodyHtml(true);
        props.setTrackingId("ADMIN-REGISTER-"+paVM.getOrgName()+"-PROFILE-"+System.currentTimeMillis());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  alert email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendSignupUpdateAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception {
        paVM.setBaseUrl(paramSrv.getAdminApplicationContextPath());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_UPDATE_ALERT);
        String mailContent = tplSrv.generatePartnerRegistrationUpdateAlertEmailHtml(paVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ paramSrv.getDefaultPartnerRegistrationEmailReceiver() });
        props.setBody(mailContent);
        props.setBodyHtml(true);
        props.setTrackingId("ADMIN-UPDATE-"+paVM.getOrgName()+"-PROFILE-"+System.currentTimeMillis());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  alert email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendingPartnerProfileVerifingEmail(PPMFLGApprovalLog appLog) throws Exception {
        List<String> approverTypes = new ArrayList<String>();
        approverTypes.add(appLog.getApproverType());
        approverTypes.add(ApproverTypes.getBackupCode(appLog.getApproverType()));
        List<PPMFLGApprover> approvers = approverRepo.findApproverByType(approverTypes, new Date(System.currentTimeMillis()));
        if(approvers == null || approvers.size() == 0){
            throw new Exception("No approvers found!");
        }
        List<String> emails = new ArrayList<String>();
        for(PPMFLGApprover i : approvers){
            String e = adminSrv.getAdminUserEmailById(i.getAdminId());
            if(e != null && e.trim().length() > 0){
                emails.add(e);
            }
        }
        if(emails.size() == 0){
            throw new Exception("No approvers found!");
        }

        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(
                PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_APPROVER_ALERT);
        ApprovalEamilVM vm = new ApprovalEamilVM(appLog, paramSrv.getAdminApplicationContextPath());
        String content = tplSrv.generateApproverAlertEmailHtml(vm);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(emails.toArray(new String[emails.size()]));
        props.setBody(content);
        props.setBodyHtml(true);
        props.setTrackingId("Approving-Request-"+appLog.getId()+"-"+System.currentTimeMillis());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendingPartnerProfileResubmitEmail(PartnerVM partnerVM) throws Exception {
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, partnerVM.getOrgName());
        paramMap.put(PartnerPortalConst.ORG_ACCOUNT_CODE, partnerVM.getAccountCode());
        paramMap.put(PartnerPortalConst.BASE_URL_KEY,paramSrv.getApplicationContextPath());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_RESUBMIT_EMAIL);
        SysEmailTemplateVM footerVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        String content = contentVM.getBody();
        String footer  = footerVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String processedFooter  = processEmailParams(footer,  paramMap);
        partnerVM.setPreDefinedBody(processedContent);
        partnerVM.setPreDefinedFooter(processedFooter);
        String mailContentHtml = tplSrv.generatePartnerRegistrationResubmitEmailHtml(partnerVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ partnerVM.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(partnerVM.getOrgName());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendingPartnerProfileRejectEmail(PartnerVM partnerVM) throws Exception {
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, partnerVM.getOrgName());
        paramMap.put(PartnerPortalConst.ORG_ACCOUNT_CODE, partnerVM.getAccountCode());
        paramMap.put(PartnerPortalConst.BASE_URL_KEY,paramSrv.getApplicationContextPath());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_REJECT_EMAIL);
        SysEmailTemplateVM footerVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        String content = contentVM.getBody();
        String footer  = footerVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String processedFooter  = processEmailParams(footer,  paramMap);
        partnerVM.setPreDefinedBody(processedContent);
        partnerVM.setPreDefinedFooter(processedFooter);
        String mailContentHtml = tplSrv.generatePartnerRegistrationRejectEmailHtml(partnerVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ partnerVM.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(partnerVM.getOrgName());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendPartnerRegistrationApprovedEmail(ApprovingPartnerVM vm) throws Exception {
        PartnerVM partnerVM = vm.getPartnerVM();
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        partnerVM.setBaseUrl(paramSrv.getApplicationContextPath());
        paramMap.put(PartnerPortalConst.ORG_NAME, partnerVM.getOrgName());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_APPROVED_EMAIL);
        SysEmailTemplateVM footerVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        String content = contentVM.getBody();
        String footer  = footerVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String processedFooter  = processEmailParams(footer,  paramMap);
        partnerVM.setUsername(vm.getPartnerAccountVM().getUsername());
        partnerVM.setPassword(vm.getPartnerAccountVM().getPassword());
        partnerVM.setPreDefinedBody(processedContent);
        partnerVM.setPreDefinedFooter(processedFooter);
        String mailContentHtml = tplSrv.generatePartnerRegistrationApprovedEmailHtml(partnerVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ partnerVM.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(partnerVM.getOrgName());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendPartnerRegistrationRejectionEmail(ApprovingPartnerVM vm) throws Exception {
        PartnerVM partnerVM = vm.getPartnerVM();
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        partnerVM.setBaseUrl(paramSrv.getApplicationContextPath());
        paramMap.put(PartnerPortalConst.ORG_NAME, partnerVM.getOrgName());
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REGISTRATION_REJECT_EMAIL);
        SysEmailTemplateVM footerVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_GENERAL_FOOTER);
        String content = contentVM.getBody();
        String footer  = footerVM.getBody();
        String processedContent = processEmailParams(content, paramMap);
        String processedFooter  = processEmailParams(footer,  paramMap);
        partnerVM.setPreDefinedBody(processedContent);
        partnerVM.setPreDefinedFooter(processedFooter);
        String mailContentHtml = tplSrv.generatePartnerRegistrationRejectEmailHtml(partnerVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ partnerVM.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(partnerVM.getOrgName());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending sign up  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendPartnerSubaccountCreationEmail(PartnerAccountVM vm) throws Exception {

        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_SUBACCOUNT_CREATION_EMAIL);
        String content = contentVM.getBody();
        vm.setPredefinedBody(content);

        String processedContent = tplSrv.generatePartnerSubaccountCreationEmailHtml(vm);
        String mailContentHtml = processEmailParams(processedContent, paramMap);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ vm.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId("newSubAccount-" + vm.getUsername() + "-" + new Date().getTime());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending subaccount creation  email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendPartnerSubaccountPasswordResetEmail(PartnerAccountVM vm) throws Exception {

        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_SUBACCOUNT_PASSWORD_RESET_EMAIL);
        String content = contentVM.getBody();
        vm.setPredefinedBody(content);

        String processedContent = tplSrv.generatePartnerSubaccountPasswordResetEmailHtml(vm);
        String mailContentHtml = processEmailParams(processedContent, paramMap);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        props.setToUsers(new String[]{ vm.getEmail() });
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId("passwordReset-" + vm.getUsername() + "-" + new Date().getTime());
        props.setSender(mailSrv.getDefaultSender());
        props.setHasAttach(false);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending password reset email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendTransactionReceiptEmail(PPMFLGInventoryTransaction trans, ReceiptVM receipt, String receiptName, String genpdfPath) throws Exception {
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, receipt.getOrgName());
        paramMap.put(PartnerPortalConst.BASE_URL_KEY,paramSrv.getApplicationContextPath() + "/" + StoreApiChannels.PARTNER_PORTAL_MFLG);
        String mainUserEmail = trans.getMainAccount().getProfile().getEmail();

        //TODO preEmailBody = EmailLabel.replaceBaseUrl(preEmailBody);



        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_TRANSACTION_RECEIPT_EMAIL);
        String content = contentVM.getBody();
        receipt.setPreDefinedBody(content);

        String processedContent = tplSrv.generatePartnerTransactionReceiptEmailHtml(receipt);
        String mailContentHtml = processEmailParams(processedContent, paramMap);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        if(trans.isSubAccountTrans()) {
            PPMFLGTASubAccount subAccount = subAccountRepo.findFirstByUsername(trans.getUsername());
            log.debug("subAccount.getEmail()  at ... " + subAccount.getEmail());
            props.setToUsers(new String[] { mainUserEmail,
                    (subAccount == null ? "" : subAccount.getEmail()) });
        } else {
            props.setToUsers(new String[] { mainUserEmail });
        }
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(receiptName);
        props.setSender(mailSrv.getDefaultSender());
        props.setAttachNames(new String[] { receiptName
                + FileUtil.FILE_EXT_PDF });
        props.setAttachFilePaths(new String[] { genpdfPath });
        props.setHasAttach(true);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending receipt generation email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendRevalTransactionReceiptEmail(PPMFLGInventoryTransaction trans, PPMFLGRevalidationTransaction revalTrans, ReceiptVM receipt, String receiptName, String genpdfpath) throws Exception {
        Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
        if(paramMap == null){
            paramMap = new HashMap<String, String>();
        }
        paramMap.put(PartnerPortalConst.ORG_NAME, receipt.getOrgName());
        paramMap.put(PartnerPortalConst.BASE_URL_KEY,paramSrv.getApplicationContextPath() + "/" + StoreApiChannels.PARTNER_PORTAL_MFLG);
        String mainUserEmail = trans.getMainAccount().getProfile().getEmail();

        //TODO preEmailBody = EmailLabel.replaceBaseUrl(preEmailBody);

        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_REVAL_RECEIPT_EMAIL);
        String content = contentVM.getBody();
        content.replaceAll(ORG_NAME, receipt.getOrgName());
        receipt.setPreDefinedBody(content);


        String processedContent = tplSrv.generatePartnerRevalTransactionReceiptEmailHtml(receipt);
        String mailContentHtml = processEmailParams(processedContent, paramMap);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        if(trans.isSubAccountTrans()) {
            PPMFLGTASubAccount subAccount = subAccountRepo.findFirstByUsername(trans.getUsername());
            log.debug("subAccount.getEmail()  at ... " + subAccount.getEmail());
            props.setToUsers(new String[] { mainUserEmail,
                    (subAccount == null ? "" : subAccount.getEmail()) });
        } else {
            props.setToUsers(new String[] { mainUserEmail });
        }
        props.setBody(mailContentHtml);
        props.setBodyHtml(true);
        props.setTrackingId(receiptName);
        props.setSender(mailSrv.getDefaultSender());
        props.setAttachNames(new String[] { receiptName
                + FileUtil.FILE_EXT_PDF });
        props.setAttachFilePaths(new String[] { genpdfpath });
        props.setHasAttach(true);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending receipt generation email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendPincodeEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception {
        String mainUserEmail = mainAccountRepo.findById(immpkg.getMainAccountId()).getProfile().getEmail();
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_PINCODE_BODY);


        String processedContent = tplSrv.generatePincodeEmailHtml(packageVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        if (!immpkg.getSubAccountTrans()) {
            PPMFLGTASubAccount subAcc = subAccountRepo.findFirstByUsername(
                    immpkg.getUsername());
            log.debug("subAcc.getEmail()  at ... " + subAcc.getEmail());
            props.setToUsers(new String[] { mainUserEmail,
                    (subAcc == null ? "" : subAcc.getEmail()) });
        } else {
            props.setToUsers(new String[] { mainUserEmail });
        }

        String barcodeFileName = null;
        String barcodeFileAttachmentName = null;
        String barcodeFilePath = null;
        barcodeFileName = "PinCode-"+receiptName.trim()+ "-" + (System.currentTimeMillis()) + FileUtil.IMG_EXT_PNG;
        barcodeFileName = barcodeFileName.toUpperCase();
        barcodeFileAttachmentName = receiptName.trim().toUpperCase() + FileUtil.IMG_EXT_PNG;
        barcodeFilePath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + barcodeFileName;
        try{
             BarcodeGenerator.toBase64(packageVM.getPinCode(), 300, 75, barcodeFilePath);
        }catch (Exception ex){
            log.error("Generating barcode failed : "+ex.getMessage() +" for "+immpkg.getName(), ex);
        }catch (Throwable ex){
            log.error("Generating barcode failed : "+ex.getMessage() +" for "+immpkg.getName(), ex);
        }
        String[] fileNameArray = null;
        String[] filePathArray = null;
        String[] fileContentId = null;
        fileNameArray = new String[]{ receiptName
                + FileUtil.FILE_EXT_PDF , barcodeFileAttachmentName };
        filePathArray = new String[]{ genpdfPath, barcodeFilePath };
        fileContentId = new String[]{ "", "<barcodeImageFileId>"};
        props.setBody(processedContent);
        props.setBodyHtml(true);
        props.setTrackingId(immpkg.getName());
        props.setSender(mailSrv.getDefaultSender());
        props.setAttachNames(fileNameArray);
        props.setAttachFilePaths(filePathArray);
        props.setAttachFileContentTypes(fileContentId);
        props.setHasAttach(true);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending receipt generation email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendEticketEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception {
        String mainUserEmail = mainAccountRepo.findById(immpkg.getMainAccountId()).getProfile().getEmail();
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_ETICKET);


        String processedContent = tplSrv.generateEticketEmailHtml(packageVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        if (!immpkg.getSubAccountTrans()) {
            PPMFLGTASubAccount subAcc = subAccountRepo.findFirstByUsername(
                    immpkg.getUsername());
            log.debug("subAcc.getEmail()  at ... " + subAcc.getEmail());
            props.setToUsers(new String[] { mainUserEmail,
                    (subAcc == null ? "" : subAcc.getEmail()) });
        } else {
            props.setToUsers(new String[] { mainUserEmail });
        }

        props.setBody(processedContent);
        props.setBodyHtml(true);
        props.setTrackingId(immpkg.getName());
        props.setSender(mailSrv.getDefaultSender());
        props.setAttachNames(new String[] { receiptName
                + FileUtil.FILE_EXT_PDF });
        props.setAttachFilePaths(new String[] { genpdfPath });
        props.setHasAttach(true);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending receipt generation email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void sendExcelEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception {
        String mainUserEmail = mainAccountRepo.findById(immpkg.getMainAccountId()).getProfile().getEmail();
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.HANDLEBARS_EXCEL);

        String processedContent = tplSrv.generateEticketEmailHtml(packageVM);
        MailProperties props = new MailProperties();
        props.setSubject(contentVM.getSubject());
        if (!immpkg.getSubAccountTrans()) {
            PPMFLGTASubAccount subAcc = subAccountRepo.findFirstByUsername(
                    immpkg.getUsername());
            log.debug("subAcc.getEmail()  at ... " + subAcc.getEmail());
            props.setToUsers(new String[] { mainUserEmail,
                    (subAcc == null ? "" : subAcc.getEmail()) });
        } else {
            props.setToUsers(new String[] { mainUserEmail });
        }

        props.setBody(processedContent);
        props.setBodyHtml(true);
        props.setTrackingId(immpkg.getName());
        props.setSender(mailSrv.getDefaultSender());
        props.setAttachNames(new String[] { receiptName
                + FileUtil.FILE_EXT_XLS });
        props.setAttachFilePaths(new String[] { genpdfPath });
        props.setHasAttach(true);
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending receipt generation email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendForgotPasswordEmail(String userName, String loginName, String newPwd, String email) {
        String ownOrgName = paramSrv.getPartnerOwnerOrgName();
        SysEmailTemplateVM contentVM = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_FORGOT_PWD);
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("AccountHolderName", getNonEmptyStringValue(userName));
        paramMap.put("AccountLoginName", getNonEmptyStringValue(loginName));
        paramMap.put("Password", getNonEmptyStringValue(newPwd));
        paramMap.put("ownOrgName", getNonEmptyStringValue(ownOrgName));
        String processedContent = processEmailParams(contentVM.getBody(), paramMap);

        String trackingId = "forgot-pwd-email-"+loginName+"-"+System.currentTimeMillis();
        MailProperties props = new MailProperties();
        props.setToUsers(new String[] { email });
        props.setSubject(contentVM.getSubject());
        props.setBody(processedContent);
        props.setBodyHtml(true);
        props.setTrackingId(trackingId);
        props.setSender(mailSrv.getDefaultSender());
        try {
            log.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    log.info("Finished sending forgot password email for {} with result {}", userName, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            log.error("Finished sending forgot password email for "+userName+" with result FAILED ", e);
        }
    }

    private String processEmailParams(String content, Map<String, String> paramMap) {
        if(content == null || content.trim().length() == 0 || paramMap == null || paramMap.size() == 0){
            return content;
        }
        Iterator<Map.Entry<String, String>> iter = paramMap.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry<String, String> entry = iter.next();
            content = content.replace(getNonEmptyStringKey(entry.getKey()), getNonEmptyStringValue(entry.getValue()));
        }
        return content;
    }

    private String getNonEmptyStringKey(String key) {
        if(key != null && key.trim().length() > 0){
            return "##"+(key.trim())+"##";
        }
        return "####";
    }

    private String getNonEmptyStringValue(String val){
        if(val != null && val.trim().length() > 0){
            return val.trim();
        }
        return "";
    }
}
