package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.tier.ProductTier;
import com.enovax.star.cms.commons.datamodel.ppmflg.tier.TierProductMapping;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerProductVM;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
@Repository("PPMFLGITierProductMappingRepository")
public class JcrTierProductMappingRepository implements ITierProductMappingRepository {
    @Override
    public List<TierProductMapping> getProductList(String channel, String tierId) {
        List<TierProductMapping> tierProductMappings = new ArrayList<>();
        String query = "/jcr:root/"+ channel+"//element(*, mgnl:tier-product-mapping)[@tierId = '" + tierId + "']";
        try {
            Iterable<Node> tierProductMappingNodes = JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), JcrWorkspace.TierProductMappings.getNodeType(), query);
            Iterator<Node> tierProductMappingNodesIterator = tierProductMappingNodes.iterator();

            while(tierProductMappingNodesIterator.hasNext()) {
                Node mappingNode = tierProductMappingNodesIterator.next();
                TierProductMapping mapping = new TierProductMapping(mappingNode);

                Iterable<Node> tierNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), "//element(*, mgnl:product-tier)[@priceGroupId = '" + tierId + "']");
                if(tierNodes != null) {
                    Iterator<Node> tierNodesIterator = tierNodes.iterator();
                    while (tierNodesIterator.hasNext()) {
                        Node tierNode = tierNodesIterator.next();
                        int id = Integer.parseInt(tierNode.getName());
                        String priceGroupId = tierNode.getProperty("priceGroupId").getString();
                        String name = tierNode.getProperty("name").getString();
                        int isB2B = tierNode.getProperty("isB2B").getDecimal().intValue();
                        mapping.setProductTier(new ProductTier(id,priceGroupId, name, isB2B));
                    }
                }

                Node prodNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + mapping.getProductId());
                mapping.setProductVM(new PartnerProductVM(prodNode));

                tierProductMappings.add(mapping);
            }

            return tierProductMappings;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<TierProductMapping> getTierList(String channel, String productId) {
        List<TierProductMapping> tierProductMappings = new ArrayList<>();
        String query = "/jcr:root/"+ channel+"//element(*, mgnl:tier-product-mapping)[@productId = '" + productId+ "']";
        try {
            Iterable<Node> tierProductMappingNodes = JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), JcrWorkspace.TierProductMappings.getNodeType(), query);
            Iterator<Node> tierProductMappingNodesIterator = tierProductMappingNodes.iterator();

            while(tierProductMappingNodesIterator.hasNext()) {
                Node mappingNode = tierProductMappingNodesIterator.next();
                TierProductMapping mapping = new TierProductMapping(mappingNode);

                Iterable<Node> tierNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), "//element(*, mgnl:product-tier)[@priceGroupId = '" + mapping.getTierId() + "']");
                if(tierNodes != null) {
                    Iterator<Node> tierNodesIterator = tierNodes.iterator();
                    while (tierNodesIterator.hasNext()) {
                        Node tierNode = tierNodesIterator.next();
                        int id = Integer.parseInt(tierNode.getName());
                        String priceGroupId = tierNode.getProperty("priceGroupId").getString();
                        String name = tierNode.getProperty("name").getString();
                        int isB2B = tierNode.getProperty("isB2B").getDecimal().intValue();
                        mapping.setProductTier(new ProductTier(id,priceGroupId, name, isB2B));
                    }
                }

                Node prodNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/"+ channel +"/" + mapping.getProductId());
                mapping.setProductVM(new PartnerProductVM(prodNode));

                tierProductMappings.add(mapping);
            }

            return tierProductMappings;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/tier-product-mappings");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }
}
