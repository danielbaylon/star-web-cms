package com.enovax.star.cms.partnershared.model.grid;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class ProductGridFilterVM extends BaseGridFilterVM {
    private ProdFilter prodFilter;
    private String prodfilterJson;

    public String getProdfilterJson() {
        return prodfilterJson;
    }

    public void setProdfilterJson(String prodfilterJson) {
        this.prodfilterJson = prodfilterJson;
    }

    public ProdFilter getProdFilter() {
        return prodFilter;
    }

    public void setProdFilter(ProdFilter prodFilter) {
        this.prodFilter = prodFilter;
    }
}
