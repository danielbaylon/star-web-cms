package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.ActionType;
import com.enovax.star.cms.commons.constant.ppslm.ApproverCategory;
import com.enovax.star.cms.commons.constant.ppslm.FlowStatus;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprover;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PartnerExclProdMapping;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCapacityGroup;
import com.enovax.star.cms.commons.model.axstar.AxCustomerGroup;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.system.SysParamVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMApprovalLogRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMApproverRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPADocMappingRepository;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.partnershared.constant.ppslm.ApproverTypes;
import com.enovax.star.cms.partnershared.constant.ppslm.ProductLevels;
import com.enovax.star.cms.partnershared.model.grid.ProdFilter;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import com.enovax.star.cms.partnershared.repository.ppslm.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.IPartnerRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.JcrCountryRegionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DefaultApprovalLogService implements IApprovalLogService {

    @Autowired
    private IReasonService reasonSrv;

    @Autowired
    private IAdminService adminSrv;

    @Autowired
    private PPSLMApprovalLogRepository approvalLogRepo;

    @Autowired
    private IPartnerService partnerSrv;

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private JcrCountryRegionsRepository countryRepo;

    @Autowired
    private PPSLMPADocMappingRepository paDocMappingRepo;

    @Autowired
    private PPSLMApproverRepository approverRepository;

    @Autowired
    private IAdminService adminService;

    @Autowired
    private ILineOfBusinessRepository partnerTypeRepo;

    @Autowired
    private IRevalidationFeeService revalidationFeeService;

    @Autowired
    private IPartnerRepository paTierRepo;

    @Autowired
    private IProductTierService productTierService;

    @Autowired
    private IPartnerExclProdService partnerExclProdService;

    @Autowired
    private IPartnerAdminProductService partnerAdminProductService;

    @Autowired
    private AxStarService axSrv;

    @Autowired
    private AxChannelCustomerService axCustSrv;

    @Transactional(readOnly = true)
    public RequestApprovalVM populatePendingApprovalRequestVM(String loginAdminId) {
        RequestApprovalVM vm = new RequestApprovalVM();
        vm.setRequests(getPendingApprovalRequestListByUserId(loginAdminId));
        vm.setReasons(reasonSrv.populateActiveReasonVms());
        return vm;
    }

    private List<ApprovalLogVM> getPendingApprovalRequestListByUserId(String loginAdminId) {
        List<ApprovalLogVM> retList = new ArrayList<ApprovalLogVM>();

        if(!isAuthorizedApprover(loginAdminId)){
            return retList;
        }

        String approverType = ApproverTypes.General.code;

        List<PPSLMApprovalLog> list = approvalLogRepo.findAllByApproverTypeAndStatus(ApproverTypes.General.code, FlowStatus.Pending.code);

        if(list != null && list.size() > 0){
            for(PPSLMApprovalLog log : list){
                retList.add( new ApprovalLogVM(log) );
            }
        }
        return retList;
    }

    private boolean isAuthorizedApprover(String loginAdminId){
        if(loginAdminId == null || loginAdminId.trim().length() == 0 ){
            return false;
        }

        List<PPSLMApprover> approverList = approverRepository.findByAdminId(loginAdminId);
        if(approverList == null || approverList.size() == 0){
            return false;
        }
        return true;
    }

    @Override
    public String approvePendingRequest(Integer appId, String loginAdminId) throws Exception {
        if(appId == null || appId.intValue() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        PPSLMApprovalLog request = approvalLogRepo.findById(appId);
        if(request == null){
            return "INVALID_PENDING_REQUEST_ID";
        }
        if(!FlowStatus.Pending.code.equalsIgnoreCase(request.getStatus())){
            return "REQUEST_NOT_EXISTS_WITH_PROCESSED";
        }
        if(!isAuthorizedApprover(loginAdminId)){
            return "NOT_AUTHORIZED_APPROVER";
        }
        if(request.getCategory() == null || request.getCategory().trim().length() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        ApproverCategory category = ApproverCategory.fromCode(request.getCategory());
        if(category == null || category.code == null || category.code.trim().length() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        switch (category){
            case Partner:
                if(ActionType.Approve.code.equalsIgnoreCase(request.getActionType())){
                    return partnerSrv.apiApprovePartnerRegistrationRequest(appId, request, loginAdminId);
                }else if(ActionType.Update.code.equalsIgnoreCase(request.getActionType())){
                    // approval of updating partner profile
                    return partnerSrv.apiApprovePartnerUpdateRequest(appId, request, loginAdminId);
                }else{
                    return "INVALID_APPROVAL_REQUEST";
                }
            case SystemParam:
                return "NOT_DONE_YET";
            default:
                return "INVALID_APPROVAL_REQUEST";
        }
    }

    @Override
    public boolean hasPendingRequest(String key, String cate) throws Exception {
        List<PPSLMApprovalLog> logs = approvalLogRepo.findPendingRequest(key,cate);
        if(logs!=null&&logs.size()>0){
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public PPSLMApprovalLog logSystemParamRequest(SysParamVM sysParam, String label, String newValue,
                                                  String userNm) {
        PPSLMApprovalLog appLog = new PPSLMApprovalLog();
        appLog.setRelatedKey(sysParam.getName());
        //SysParamHist preHist = new SysParamHist(sysParam);
        String preHistStr = NvxUtil.getXmlfromObj(sysParam);
        appLog.setPreviousValue(preHistStr);
        sysParam.setValue(newValue);
        String newHistStr = NvxUtil.getXmlfromObj(sysParam);
        appLog.setCurrentValue(newHistStr);
        appLog.setActionType(ActionType.Update.code);
        appLog.setStatus(FlowStatus.Pending.code);
        appLog.setApproverType(ApproverTypes.General.code);
        appLog.setCategory(ApproverCategory.SystemParam.code);
        appLog.setApprover(userNm);
        appLog.setReviewDate(new Date());
        appLog.setCreatedBy(userNm);
        appLog.setCreatedDate(new Date());
        appLog.setDescription("changed " +label + " to " + newValue);

        this.approvalLogRepo.save(appLog);
        return appLog;
    }

    @Override
    @Transactional(readOnly = true)
    public int getPendingRequestForProdCnt(String prodId) {
        return this.approvalLogRepo.getPendingRequestForProdCnt(prodId);
    }

    @Override
    public String rejectPendingRequest(Integer appId, ReasonVM reasonVM, String loginAdminId) throws Exception {
        if(appId == null || appId.intValue() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        PPSLMApprovalLog request = approvalLogRepo.findById(appId);
        if(request == null){
            return "INVALID_PENDING_REQUEST_ID";
        }
        if(!FlowStatus.Pending.code.equalsIgnoreCase(request.getStatus())){
            return "REQUEST_NOT_EXISTS_WITH_PROCESSED";
        }
        if(!isAuthorizedApprover(loginAdminId)){
            return "NOT_AUTHORIZED_APPROVER";
        }
        if(request.getCategory() == null || request.getCategory().trim().length() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        ApproverCategory category = ApproverCategory.fromCode(request.getCategory());
        if(category == null || category.code == null || category.code.trim().length() == 0){
            return "INVALID_PENDING_REQUEST_ID";
        }
        switch (category){
            case Partner:
                if(ActionType.Approve.code.equalsIgnoreCase(request.getActionType())){
                    return partnerSrv.apiRejectPartnerRegistrationRequest(StoreApiChannels.PARTNER_PORTAL_SLM, appId, request, reasonVM, loginAdminId);
                }else if(ActionType.Update.code.equalsIgnoreCase(request.getActionType())){
                    // reject of updating partner profile
                    return partnerSrv.apiRejectPartnerUpdateRequest(StoreApiChannels.PARTNER_PORTAL_SLM, appId, request, reasonVM, loginAdminId);
                }else{
                    return "INVALID_APPROVAL_REQUEST";
                }
            case SystemParam:
                return "NOT_DONE_YET";
            default:
                return "INVALID_APPROVAL_REQUEST";
        }
    }

    @Override
    public List<ApprovalLogVM> getHistoryLog(Integer paId) {
       List<PPSLMApprovalLog> appLogs = approvalLogRepo.findByCategoryAndRelatedKeyOrderByCreatedDateDesc(ApproverCategory.Partner.code,paId.toString());
        List<ApprovalLogVM> applogVms = new  ArrayList<>();
        for(PPSLMApprovalLog appLog:appLogs){
            applogVms.add(new ApprovalLogVM(appLog));
        }
        return applogVms;
    }

    @Override
    public PPSLMApprovalLog getLogById(Integer appId) {
        return approvalLogRepo.findById(appId);
    }

    private List<AxCustomerGroup> getCustomerGroups(){
        AxStarServiceResult<List<AxCustomerGroup>> api = axSrv.apiCustomerGroupGet(StoreApiChannels.PARTNER_PORTAL_SLM);
        if(api.isSuccess()){
            return api.getData();
        }
        return new ArrayList<AxCustomerGroup>();
    }


    private List<AxCapacityGroup> getCapacityGroups(){
        ApiResult<List<AxCapacityGroup>> api = axCustSrv.getAxCapacityGroupList(StoreApiChannels.PARTNER_PORTAL_SLM);
        if(api.isSuccess()){
            return api.getData();
        }
        return new ArrayList<AxCapacityGroup>();
    }
    @Override
    public PartnerVM getPartnerFromHist(String hist) throws Exception{
        String paXml = hist;
        PartnerHist paHist = NvxUtil.getObjFromXml(paXml, PartnerHist.class);
        PartnerVM paVm = new PartnerVM(paHist,paramSrv.getApplicationContextPath());
        paVm.populateCustomerGroupName(getCustomerGroups());
        paVm.populateCapacityGroupName(getCapacityGroups());
        if(paVm.getDistributionMapping()!=null){
            for(PaDistributionMappingVM tmpObj:paVm.getDistributionMapping()){
                String ctyName = this.countryRepo.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data,tmpObj.getCountryId());
                tmpObj.setAdminNm(adminService.getAdminUserNameByAdminId(tmpObj.getAdminId()));
                tmpObj.setCountryNm(ctyName);
            }
        }

        List<PPSLMPADocMapping> paDocMaps = paDocMappingRepo.findByPartnerId(paHist.getId());
        List<PartnerDocumentVM> paDocs = new ArrayList<PartnerDocumentVM>();
        if(paDocs!=null){
            for(PPSLMPADocMapping tmpObj:paDocMaps){
                paDocs.add(new PartnerDocumentVM(tmpObj.getPaDoc()));
            }
        }
        paVm.setPaDocs(paDocs);

        if(paHist.getAccountManagerId()!=null){
            paVm.setAccountManager(adminService.getAdminUserNameByAdminId(paHist.getAccountManagerId()));
        }

        paVm.setCountryName(countryRepo.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data,paHist.getCountryCode()));
        paVm.setOrgTypeName(partnerTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data,paHist.getOrgTypeCode()));

        String revalFeeStr = revalidationFeeService.getRevalidationFeeByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_SLM,paHist.getAxAccountNumber(),paHist.getRevalFeeItemId());
        paVm.setRevalFeeItemId(paHist.getRevalFeeItemId());
        paVm.setRevalFeeStr(revalFeeStr);

        String tierId = paHist.getTierId();
        if(tierId!=null && !tierId.equals("")) {
            ProductTierVM productTierVM = productTierService.getTierVmById(PartnerPortalConst.Partner_Portal_Channel, tierId);
            paVm.setTierLabel(productTierVM.getName());
            paVm.setTierId(tierId);
        }

        PartnerExclProdMapping prodMap = partnerExclProdService.getPartnerExclProdMappingbyPartner(PartnerPortalConst.Partner_Portal_Channel,paHist.getId());

        if(prodMap!=null) {
            ProductGridFilterVM gridFilterVM = new ProductGridFilterVM();
            ProdFilter prodFilter = new ProdFilter();
            prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
            prodFilter.setProductLevel(ProductLevels.Exclusive.toString());
            gridFilterVM.setProdFilter(prodFilter);
            List<PartnerProductVM> productVMs = partnerAdminProductService.getProdVmsByProdIds(gridFilterVM, prodMap.getExclProdIds(), true);
            paVm.setExcluProds(productVMs);
        }


            paVm.setSubAccountEnabled(paHist.getSubAccountEnabled());
        return paVm;
    }
}
