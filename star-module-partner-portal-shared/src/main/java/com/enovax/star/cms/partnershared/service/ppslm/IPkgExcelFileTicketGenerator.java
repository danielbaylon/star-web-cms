package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by jennylynsze on 9/30/16.
 */
public interface IPkgExcelFileTicketGenerator {
    Workbook generatePkgExcel(MixMatchPkgVM mixMatchPkgVM, List<ETicketData> tickets);
}
