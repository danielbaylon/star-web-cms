package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.model.axchannel.customerext.AxLineOfBusiness;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerTypeVM;

import java.util.List;

/**
 * Created by houtao on 22/9/16.
 */
public interface ILineOfBusinessRepository {

    public boolean deleteLineOfBusinessById(String channel, String id);

    public boolean createLineOfBusiness(String channel, AxLineOfBusiness info);

    public AxLineOfBusiness getLineOfBusinessById(String channel, String id);

    public List<AxLineOfBusiness> getLineOfBusinessList(String channel);

    public List<PartnerTypeVM> getPartnerTypeList(String channel);

    public boolean existsLineOfBusinessId(String channel, String id);

    void deleteLineOfBusinessIfNotInList(String channel, List<String> idlist);

    String getLineOfBusinessNameById(String channel, String id);

    public void refreshCache(String channel);

}
