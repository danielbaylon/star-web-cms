package com.enovax.star.cms.partnershared.service.ppslm;


import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.axchannel.AxPinType;
import com.enovax.star.cms.commons.constant.ppslm.PkgStatus;
import com.enovax.star.cms.commons.constant.ppslm.TicketMediaType;
import com.enovax.star.cms.commons.constant.ticket.OnlineAxTicketToken;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailGeneratePinInput;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.model.axchannel.b2bupdatepinstatus.AxB2BUpdatePinStatus;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTPinViewLine;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTPinViewTable;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxFacility;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.AxStarCartLineComment;
import com.enovax.star.cms.commons.model.axstar.AxStarSalesLine;
import com.enovax.star.cms.commons.model.axstar.AxStarSalesOrder;
import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PkgVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.ticket.Facility;
import com.enovax.star.cms.commons.model.ticket.TicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.model.ticketgen.ETicketTokenList;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.commons.util.partner.ppslm.PkgUtil;
import com.enovax.star.cms.commons.util.partner.ppslm.PkgValidator;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import com.enovax.star.cms.partnershared.constant.AuditAction;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by jennylynsze on 5/10/16.
 */
@Service
public class DefaultPartnerPkgService implements IPartnerPkgService {

    private Logger log = LoggerFactory.getLogger(getClass());

    public static final String PINCODE_EMAIL_SUCCESS = "PIN Code Email Send Successful.";
    public static final String PINCODE_PDF_FAILED = "Generate PIN code success, but PDF generation Failed, Please contact system admin.";
    public static final String PINCODE_EMAIL_FAILED = "Generate PIN code success, but send Email Failed, Please contact system admin.";
    public static final String ETICKET_EMAIL_SUCCESS = "E-Ticket Email Send Successful.";
    public static final String ETICKET_PDF_FAILED = "Generate E-Ticket success, but PDF generation Failed, Please contact system admin.";
    public static final String ETICKET_EMAIL_FAILED = "Generate E-Ticket success, but send Email Failed, Please contact system admin.";
    public static final String EXCEL_EMAIL_SUCCESS = "Excel File Ticket Email Send Successful.";
    public static final String EXCEL_XLS_FAILED = "Generate Excel File Ticket Failed. Please contact system admin.";
    public static final String EXCEL_EMAIL_FAILED = "Generate Excel File Ticket success, but send Email Failed, Please contact system admin.";

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;

    @Autowired
    private PPSLMInventoryTransactionRepository transRepo;

    @Autowired
    private PPSLMInventoryTransactionItemRepository itemRepo;

    @Autowired
    private PPSLMMixMatchPackageRepository mmPkgRepo;

    @Autowired
    private PPSLMMixMatchPackageItemRepository mmPkgItemRepo;

    @Autowired
    private PPSLMETicketRepository eTicketRepo;

    @Autowired
    private PPSLMTASubAccountRepository taSubAccountRepo;

    @Autowired
    private PPSLMAxCheckoutCartRepository checkoutCartRepo;

    @Autowired
    private PPSLMAxSalesOrderRepository salesOrderRepo;

    @Autowired
    private PPSLMAxSalesOrderLineNumberQuantityRepository salesOrderLineNumberQuantityRepo;

    @Autowired
    private PPSLMTAMainAccountRepository mainAccRepo;

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private ITemplateService templateSrv;

    @Autowired
    private IPartnerEmailService partnerEmailSrv;

    @Autowired
    private ITicketGenerationService ticketGenerationSrv;

    @Autowired
    private IAuditTrailService auditService;

    @Autowired
    private IPkgExcelFileTicketGenerator excelFileTicketGenerator;

    @Autowired
    private IProductService productService;

    @Autowired
    private IPartnerTncService partnerTncService;

    @Autowired
    private StarTemplatingFunctions starfn;

    @Autowired
    private PPSLMTAMainAccountRepository taMainAccRepo;

    @Autowired
    private PPSLMTASubAccountRepository taSubAccRepo;

    @Autowired
    private PPSLMPartnerRepository paRepo;

    @Autowired
    private PPSLMMixMatchPackagePinLineRepository itemPinLineRepo;


    private final StoreApiChannels channel = StoreApiChannels.PARTNER_PORTAL_SLM;

    @Override
    public ResultVM getBundledItemsByTransItemIds(String transItemIdsStr, String pkgName, Integer pkgQtyPerProd, String pkgTktType, Integer itemIdToAdd, String pkgDesc, String pkgTktMedia) {

        ResultVM resultVm = new ResultVM();
        List<Integer> tranIdsInt =  PkgUtil.getTransIdsFromStr(transItemIdsStr);

        List<PPSLMInventoryTransactionItem> transItems = itemRepo.findByIdIn(tranIdsInt);
        PPSLMInventoryTransactionItem itemToAdd = null;

        //TODO check the ticketMeida of each items, if one of the item is PINCODE, the ticket media should be forced to PINCODE
        List<String> listingIds = new ArrayList<>();
        List<String> itemToAddListingIds = new ArrayList<>();

        Map<String, Node> productMap = new HashMap<>();

        for (PPSLMInventoryTransactionItem item : transItems) {
            List<PPSLMInventoryTransactionItem> topupItems = itemRepo
                    .getTaggedTopItem(item.getTransactionId(), item.getProductId(), item.getUnpackagedQty(), item.getProductId(), item.getItemListingId(), null, null, null);

            item.setTopupItems(topupItems);

            //add the top up listing id also to check the media type and stuff
            for(PPSLMInventoryTransactionItem topupItem: topupItems) {
                listingIds.add(topupItem.getItemListingId());
                if (itemIdToAdd != null && itemIdToAdd.equals(item.getId())) {
                    itemToAddListingIds.add(item.getItemListingId());
                }
            }

            //the new one
            if (itemIdToAdd != null && itemIdToAdd.equals(item.getId())) {
                itemToAdd = item;
                itemToAddListingIds.add(item.getItemListingId());

            }

            listingIds.add(item.getItemListingId()); //for later use

            productMap.put(item.getProductId(), starfn.getCMSProduct(channel.code, item.getProductId()));
        }

        // validate part started, TODO add more validation
        //TODO need to get by transaction ID instead
        List<PPSLMMixMatchPackageItem> pkgItemWithSameItem = mmPkgItemRepo.findByTransactionItemId(itemIdToAdd);
        List<ProductExtViewModel> productExtViewModelList = getProductExt(listingIds);

        ResultVM valiVm = PkgValidator.verifyNewAddedItem(itemToAdd,
                transItems, pkgTktType, pkgItemWithSameItem, pkgTktMedia, productExtViewModelList, productMap);

        //check if the item transaction is reserved.
//        boolean hasSameTransactionReserved = mmPkgItemRepo.noOfItemWithSameTransactionReserved(itemToAdd.getInventoryTrans().getReceiptNum()) > 0;
//        if(hasSameTransactionReserved) {
//            valiVm.setStatus(false);
//            valiVm.setMessage((valiVm.getMessage() == null ? ""
//                    : valiVm.getMessage()+ PkgValidator.Break)
//                    + "Same Transaction has been reserved");
//            //TODO see message.properties
//            // + textProvider.getText(PkgValidator.MSG_SameItemWithDiffTopup));
//        }

        resultVm.setStatus(valiVm.isStatus());
        resultVm.setMessage(valiVm.getMessage());
        if (resultVm.isStatus()) {
            ResultVM verifyResult = getCheckItemsLimitsBySystemParam(transItems);
            resultVm.setStatus(verifyResult.isStatus());
            resultVm.setMessage(verifyResult.getMessage());
        }
        if (!resultVm.isStatus()) {
            transItems = removeInvalidItem(transItems, itemIdToAdd);
        }
        // positive check
        if (itemIdToAdd == null) {
            ResultVM pAllRes = new ResultVM();
            ResultVM pRes1 = checkMinQtyBySystemParam(pkgQtyPerProd);
            ResultVM pRes2 = PkgValidator.checkIfItemNeedRemoveDueToQty(
                    transItems, pkgQtyPerProd, pkgTktType);
            ResultVM pRes3 = checkMaxQtyBySystemParam(pkgQtyPerProd);
            pAllRes = ResultVM.processResult(pRes1, pRes2, pRes3);
            if (!pAllRes.isStatus()) {
                resultVm.setMessage(pAllRes.getMessage());
            }
        }

        // validate part end
        // Set tkt type when first select
        if (tranIdsInt != null && tranIdsInt.size() == 1 && itemToAdd != null) {
            pkgTktType = itemToAdd.getTicketType();
        }

        if (resultVm.isStatus()) {
           itemToAddListingIds = null; //meaning the newly added one is ok, nothing to exclude loh.
        }

        String pkgTktMediaTypeAllowed = PkgUtil.getTicketMediaTypeAllowed(productExtViewModelList, itemToAddListingIds);

        PkgVM pkgvm = new PkgVM(transItems, pkgName, pkgQtyPerProd, pkgTktType, pkgDesc, pkgTktMedia, pkgTktMediaTypeAllowed);

        resultVm.setTotal(1); //TODO is this needed?
        resultVm.setViewModel(pkgvm);

        return resultVm;
    }

    //TODO ask HOUTAO for the sysparam!!!!!!!!
    private ResultVM getCheckItemsLimitsBySystemParam(
            List<PPSLMInventoryTransactionItem> transItems) {
//        systemParamService.getSys
//        Integer maxItemCodeNum = sysService.getObjByKey(
//                SysParamConst.PkgAllowedItemsCodes.toString(), Integer.class,
//                true);
//        Integer maxMSitesNum = sysService.getObjByKey(
//                SysParamConst.PkgAllowedMultipleSites.toString(),
//                Integer.class, true);
//        Integer maxSSitesNum = sysService.getObjByKey(
//                SysParamConst.PkgAllowedSingleSites.toString(), Integer.class,
//                true);
//        ResultVM verifyResult = PkgValidator.checkItemsMaxAllowed(transItems,
//                maxItemCodeNum, maxMSitesNum, maxSSitesNum);
//        return verifyResult;
          return new ResultVM();
    }


    private List<ProductExtViewModel> getProductExt(List<String> listingIds) {
        ApiResult<List<ProductExtViewModel>> productExtRes = null;
        try {
            productExtRes = productService.getDataForProducts(channel, listingIds);
            if(productExtRes.isSuccess()) {
                List<ProductExtViewModel> extViewModels = productExtRes.getData();
                return extViewModels;
            }
        } catch (AxChannelException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    //TODO ask HOUTAO for the sysparam!!!!!!!!
    private ResultVM checkMinQtyBySystemParam(Integer inQty) {
//        Integer minPkgQty = sysService.getObjByKey(
//                SysParamConst.PkgAllowedMinQty.toString(), Integer.class, true);
//        ResultVM verifyResult = PkgValidator.checkMinQtyAllowed(inQty,
//                minPkgQty);
//        return verifyResult;
        return new ResultVM();
    }

    //TODO ask HOUTAO for the sysparam!!!!!!!!
    private ResultVM checkMaxQtyBySystemParam(Integer inQty) {
//        Integer maxPkgQty = sysService.getObjByKey(
//                SysParamConst.PkgAllowedMaxQty.toString(), Integer.class, true);
//        ResultVM verifyResult = PkgValidator.checkMaxQtyAllowed(inQty,
//                maxPkgQty);
//        return verifyResult;
        return new ResultVM();
    }

    private List<PPSLMInventoryTransactionItem> removeInvalidItem(
            List<PPSLMInventoryTransactionItem> transItems, Integer itemIdToAdd) {
        List<PPSLMInventoryTransactionItem> ntransItems = new ArrayList<PPSLMInventoryTransactionItem>();
        for (PPSLMInventoryTransactionItem item : transItems) {
            if (!item.getId().equals(itemIdToAdd)) {
                ntransItems.add(item);
            }
        }
        return ntransItems;
    }

    @Override
    @Transactional
    public ResultVM createPkg(String transItemIdsStr, String pkgName, Integer pkgQtyPerProd, String pkgTktType,
                              PartnerAccount account, String pkgDesc, String pkgTktMedia) {
        ResultVM resultVm = new ResultVM();
        List<Integer> tranIdsInt =  PkgUtil.getTransIdsFromStr(transItemIdsStr);
        List<PPSLMInventoryTransactionItem> transItems = itemRepo.findByIdIn(tranIdsInt);
        for (PPSLMInventoryTransactionItem item : transItems) {
            List<PPSLMInventoryTransactionItem> topupItems = itemRepo
                    .getTaggedTopItem(item.getTransactionId(), item.getProductId(), item.getUnpackagedQty(), item.getProductId(), item.getItemListingId(),
                            null, null, null);

            item.setTopupItems(topupItems);
        }

        List<PPSLMInventoryTransactionItem> oitems = PkgUtil
                .getAvailableListUnderPkg(transItems, pkgQtyPerProd, pkgTktType);
        Date expDate = PkgUtil.getLatestExpiringDate(oitems);
        String newPkgName = checkPkgNameInUse(pkgName, account.getAccountCode()); //todo pass from session
        if (!pkgName.equals(newPkgName)) {
            pkgName = newPkgName;
        }

        PPSLMMixMatchPackage immpkg = createMMPkg(pkgName, pkgQtyPerProd,
                pkgTktType, account.getId(), account.isSubAccountEnabled(), account.getUsername(), pkgDesc, expDate, pkgTktMedia);
        mmPkgRepo.save(immpkg);

        int totalQty = 0;
        for (PPSLMInventoryTransactionItem item : oitems) {
            PPSLMMixMatchPackageItem immpkgItem = createMMPkgItemByTransItem(immpkg,
                    item);
            for (PPSLMInventoryTransactionItem topup : item.getTopupItems()) {
                PPSLMMixMatchPackageItem topupMMItem = createMMPkgItemByTopupItem(
                        immpkg, topup, item);
                totalQty = totalQty + topupMMItem.getQty();
                mmPkgItemRepo.save(topupMMItem);
            }
            totalQty = totalQty + immpkgItem.getQty();
            mmPkgItemRepo.save(immpkgItem);
        }

        immpkg.setTotalQty(totalQty);
        immpkg.setTotalQtyRedeemed(0);
        mmPkgRepo.save(immpkg);

        MixMatchPkgVM pkgvm = new MixMatchPkgVM(immpkg, oitems);
        pkgvm.setPkgTktType(pkgTktType);
        resultVm.setStatus(true);
        resultVm.setViewModel(pkgvm);
        return resultVm;


////
////        immpkg.setExpiryDateStr(NvxDateUtils.formatDate(immpkg.getExpiryDate(),
////                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
//
//
//        List<MixMatchPkgItemVM> immpkgItemList = new ArrayList<>();
//        for (InventoryTransactionItemVM item : oitems) {
//            MixMatchPkgItemVM immpkgItem = createMMPkgItemByTransItem(immpkg,
//                    item);
//            immpkgItemList.add(immpkgItem);
//            //TODO topup
////            for (InventoryTransactionItemVM topup : item.getTopupItems()) {
////                MixMatchPackageItem topupMMItem = createMMPkgItemByTopupItem(
////                        immpkg, topup, item);
////                mmPkgItemDao.save(topupMMItem);
////            }
//        }
//        // add group view model
//
//        immpkg.setTransItems(immpkgItemList);
//        immpkg.setPkgTktType(pkgTktType);
//        mixMatchPkgRepo.save(immpkg);
//
//        retartultVM.setStatus(true);
//        resultVM.setViewModel(immpkg);
//



//
//        resultVM.setViewModel(immpkg);
        //return resultVM;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultVM getPkgsByPage(Integer adminId, String fromDateStr, String toDateStr, String status, String pkgTktMedia, String pkgNm, String orderField, String orderWith, Integer pageNumber, Integer pageSize) {
        Date fromDate = null;
        Date toDate = null;
        try {
            if (fromDateStr != null && !"".equals(fromDateStr)) {
                fromDate = NvxDateUtils.parseDate(fromDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if (toDateStr != null && !"".equals(toDateStr)) {
                toDate = NvxDateUtils.parseDate(toDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (orderField != null && orderWith != null && !"".equals(orderField)
                && !"".equals(orderWith)) {
            if ("pkgTktMedia".equalsIgnoreCase(orderField)) {
                orderField = "ticketMedia";
            } else if ("ticketGeneratedDateStr".equalsIgnoreCase(orderField)) {
                orderField = "ticketGeneratedDate";
            } else if ("expiryDateStr".equalsIgnoreCase(orderField)) {
                orderField = "expiryDate";
            } else if ("qtyRedeemedStr".equalsIgnoreCase(orderField)) {
                orderField = "qtyRedeemed";
            } else if ("lastRedemptionDateStr".equalsIgnoreCase(orderField)) {
                orderField = "lastRedemptionDate";
            }
        }else {
            orderWith = "DESC";
            orderField = "id";
        }


        if("-".equals(status) || StringUtils.isBlank(status)) {
            status = null;
        }

        if(StringUtils.isBlank(pkgTktMedia)) {
            pkgTktMedia = null;
        }

        if(StringUtils.isNotBlank(pkgNm)) {
            pkgNm = "%" + pkgNm + "%";
        }


        List<PPSLMMixMatchPackage> mmPkgList = new ArrayList<>();
        Integer packagesCnt = 0;
        if(pageNumber == null) {
            mmPkgList = mmPkgRepo.getPackages(adminId, fromDate, toDate, status, pkgTktMedia, pkgNm);
            packagesCnt = mmPkgList.size();
        }else {
            PageRequest pageRequest = new PageRequest( pageNumber - 1, pageSize,
                    "ASC".equalsIgnoreCase(orderWith) ? Sort.Direction.ASC : Sort.Direction.DESC,
                    orderField
            );
            Page packagesPaged = mmPkgRepo.getPackages(adminId, fromDate, toDate, status,  pkgTktMedia, pkgNm, pageRequest);
            Iterator<PPSLMMixMatchPackage> pkgIterator = packagesPaged.iterator();
            mmPkgList = Lists.newArrayList(pkgIterator);
            packagesCnt = mmPkgRepo.countPackages(adminId, fromDate, toDate, status, pkgTktMedia, pkgNm);
        }

        List<MixMatchPkgVM> pkgList = new ArrayList<>();
        for(PPSLMMixMatchPackage pkg: mmPkgList) {
            MixMatchPkgVM item = new MixMatchPkgVM(pkg);
            pkgList.add(item);
        }

        ResultVM res = new ResultVM();
        res.setViewModel(pkgList);
        res.setTotal(packagesCnt);
        return res;
    }

    @Override
    @Transactional(readOnly = true)
    public MixMatchPkgVM getPkgByID(Integer pkgId) {
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        log.debug("getPkgByID...");
        List<PPSLMInventoryTransactionItem> transItems = groupTransItemsWithPkgQty(pkg);
        MixMatchPkgVM pkgvm = new MixMatchPkgVM(pkg, transItems);
        Integer taMainAccountId = pkg.getMainAccountId();
        PPSLMPartner pa = paRepo.findFirstByAdminId(taMainAccountId);
        pkgvm.populatePartnerDetails(paramSrv.getAdminApplicationContextPath(), pa);
        String email  = pa.getEmail();
        PPSLMTASubAccount subAcc = null;
        if(pkg.isSubAccountTrans()){
            subAcc = taSubAccRepo.findFirstByUsername(pkg.getUsername());
            if(subAcc != null){
                if(email != null && email.trim().length() > 0){
                    email += ", ";
                }
                email += subAcc.getEmail();
            }
        }
        pkgvm.setEmail(email);
        return pkgvm;
    }

    @Override
    @Transactional(readOnly = true)
    public String sendPincodeEmail(Integer pkgId) {
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        List<PPSLMMixMatchPackageItem> pkgItems = pkg.getPkgItems();
        List<PPSLMInventoryTransactionItem> transItems = groupTransItemsWithPkgQty(pkg);
        MixMatchPkgVM pkgVm = new MixMatchPkgVM(pkg, transItems);
        Set<String> prodIds = new LinkedHashSet<>();
        for (PPSLMMixMatchPackageItem item : pkgItems) {
            prodIds.add(item.getTransItem().getProductId());
        }

        final List<TncVM> tncs = this.partnerTncService.getProductTncs(StoreApiChannels.PARTNER_PORTAL_SLM, new ArrayList<>(prodIds));
        pkgVm.setHasTnc(!tncs.isEmpty());
        pkgVm.setTncs(tncs);
        PPSLMPartner partner = mainAccRepo.findById(pkg.getMainAccountId()).getProfile();
        pkgVm.setOrgName(partner.getOrgName());
        pkgVm.setPinRequestId(pkgId.toString());
        pkgVm.setBaseUrl(paramSrv.getApplicationContextPath());


        String receiptName = pkg.getId() + "nip" + pkg.getPinCode()
                + NvxUtil.generateRandomAlphaNumeric(6, false);
        String genpdfpath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + receiptName
                + FileUtil.FILE_EXT_PDF;


        try {
            Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
            pkgVm.setBarCodeImage(getBarcodePdfString(BarcodeGenerator.toBase64(pkg.getPinCode(), 300, 75)));
            pkgVm.setTicketBarcodeImageString(getBarcodeEmailString(pkgVm.getBarCodeImage())); //TODO
            String body = templateSrv.generatePincodeBody(pkgVm);
            String footer = templateSrv.generatePincodeFooter(pkgVm);
            String processedFooter = templateSrv.processEmailParams(footer,paramMap);
            pkgVm.setPreDefinedBody(body);
            pkgVm.setPreDefinedFooter(processedFooter);


            templateSrv.generatePincodePdf(pkgVm, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return PINCODE_PDF_FAILED;
        }

        try {
            pkgVm.setBarCodeImage(getBarcodeEmailString(BarcodeGenerator.toBase64(pkg.getPinCode(), 300, 75)));
            partnerEmailSrv.sendPincodeEmail(pkg, pkgVm, receiptName, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return PINCODE_EMAIL_FAILED;
        }
        return PINCODE_EMAIL_SUCCESS;
    }

    //TODO
    private String getBarcodeEmailString(String barcodeImageString) {
        if(barcodeImageString != null && barcodeImageString.trim().length() > 0){
            return "<img width=\"300\" height=\"75\" src=\"cid:barcodeImageFileId\" />";
        }
        return "";
    }


    private String getBarcodePdfString(String barcodeImageString) {
        if(barcodeImageString != null && barcodeImageString.trim().length() > 0){
            return "<img width=\"300\" height=\"75\" src=\"data:image/png;base64,"+ barcodeImageString.trim() +"\" />";
        }
        return "";
    }

    @Override
    @Transactional(readOnly = true)
    public String sendEticketEmail(Integer pkgId) {
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        List<PPSLMMixMatchPackageItem> pkgItems = pkg.getPkgItems();
        List<PPSLMInventoryTransactionItem> transItems = groupTransItemsWithPkgQty(pkg);
        MixMatchPkgVM pkgVm = new MixMatchPkgVM(pkg, transItems);
        Set<String> prodIds = new LinkedHashSet<>();
        for (PPSLMMixMatchPackageItem item : pkgItems) {
            prodIds.add(item.getTransItem().getProductId());
        }
        final List<TncVM> tncs = this.partnerTncService.getProductTncs(StoreApiChannels.PARTNER_PORTAL_SLM, new ArrayList<>(prodIds));
        pkgVm.setHasTnc(!tncs.isEmpty());
        pkgVm.setTncs(tncs);
        PPSLMPartner partner = mainAccRepo.findById(pkg.getMainAccountId()).getProfile();
        pkgVm.setOrgName(partner.getOrgName());
        pkgVm.setPinRequestId(pkgId.toString());
        pkgVm.setBaseUrl(paramSrv.getApplicationContextPath());
        String receiptName = pkg.getId() + "ET" + pkg.getName();
        String genpdfpath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + receiptName
                + FileUtil.FILE_EXT_PDF;

        try {
            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(getETickets(pkgId));
            final byte[] outputBytes = ticketGenerationSrv.generateTickets(StoreApiChannels.PARTNER_PORTAL_SLM, ticketDataCompiled);
            File file = new File(genpdfpath);
            FileOutputStream os = new FileOutputStream(file);
            os.write(outputBytes);
            os.close();
            Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
            String body = templateSrv.generatePincodeBody(pkgVm);
            String footer = templateSrv.generatePincodeFooter(pkgVm);
            String processedFooter = templateSrv.processEmailParams(footer,paramMap);
            pkgVm.setPreDefinedBody(body);
            pkgVm.setPreDefinedFooter(processedFooter);
//            pkgVm.setBarCodeImage(BarcodeGenerator.toBase64(pkg.getPinCode(),300,75));

//            templateSrv.generatePincodePdf(pkgVm, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return ETICKET_PDF_FAILED;
        }

        try {
            partnerEmailSrv.sendEticketEmail(pkg, pkgVm, receiptName, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return ETICKET_EMAIL_FAILED;
        }
        return ETICKET_EMAIL_SUCCESS;
    }

    @Override
    @Transactional(readOnly = true)
    public String sendExcelEmail(Integer pkgId) {
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        List<PPSLMMixMatchPackageItem> pkgItems = pkg.getPkgItems();
        List<PPSLMInventoryTransactionItem> transItems = groupTransItemsWithPkgQty(pkg);
        MixMatchPkgVM pkgVm = new MixMatchPkgVM(pkg, transItems);
        Set<String> prodIds = new LinkedHashSet<>();
        for (PPSLMMixMatchPackageItem item : pkgItems) {
            prodIds.add(item.getTransItem().getProductId());
        }
        final List<TncVM> tncs = this.partnerTncService.getProductTncs(StoreApiChannels.PARTNER_PORTAL_SLM, new ArrayList<>(prodIds));
        pkgVm.setHasTnc(!tncs.isEmpty());
        pkgVm.setTncs(tncs);
        PPSLMPartner partner = mainAccRepo.findById(pkg.getMainAccountId()).getProfile();
        pkgVm.setOrgName(partner.getOrgName());
        pkgVm.setPinRequestId(pkgId.toString());
        pkgVm.setBaseUrl(paramSrv.getApplicationContextPath());
        String receiptName = pkg.getId() + "EXCEL" + pkg.getName();
        String genpdfpath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + receiptName
                + FileUtil.FILE_EXT_XLS;

        try {
            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(getETickets(pkgId));

            Workbook wb = excelFileTicketGenerator.generatePkgExcel(pkgVm, getETickets(pkgId));
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);

            File file = new File(genpdfpath);
            FileOutputStream os = new FileOutputStream(file);
            os.write(outByteStream.toByteArray());
            os.close();

            Map<String, String> paramMap = paramSrv.getSentosaContactDetails();
            String body = templateSrv.generatePincodeBody(pkgVm);
            String footer = templateSrv.generatePincodeFooter(pkgVm);
            String processedFooter = templateSrv.processEmailParams(footer,paramMap);
            pkgVm.setPreDefinedBody(body);
            pkgVm.setPreDefinedFooter(processedFooter);
//            pkgVm.setBarCodeImage(BarcodeGenerator.toBase64(pkg.getPinCode(),300,75));

//            templateSrv.generatePincodePdf(pkgVm, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return EXCEL_XLS_FAILED;
        }

        try {
            partnerEmailSrv.sendExcelEmail(pkg, pkgVm, receiptName, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return EXCEL_EMAIL_FAILED;
        }
        return EXCEL_EMAIL_SUCCESS;
    }

    @Override
    @Transactional
    public ApiResult<String> deactivatePkg(Integer pkgId, String userNm) {
        ResultVM res = new ResultVM();
        ResultVM resultVM = checkRdmStatus(pkgId, false);
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        if(resultVM.isStatus() && !PkgStatus.Redeemed.toString().equals(pkg.getStatus()) && !PkgStatus.Deactivated.toString().equals(pkg.getStatus())) {

            try {
                AxB2BUpdatePinStatus updatePinStatusModel = new AxB2BUpdatePinStatus();
                updatePinStatusModel.setPinCode(pkg.getPinCode());
                updatePinStatusModel.setPinStatus(1);
                updatePinStatusModel.setReasonCode("CardReject");
                updatePinStatusModel.setProcessedBy(userNm);
                ApiResult<String> apiResult = axChannelTransactionService.apiB2BUpdatePinStatus(StoreApiChannels.PARTNER_PORTAL_SLM, updatePinStatusModel);
                if (apiResult.isSuccess()) {
                    pkg.setStatus(PkgStatus.Deactivated.toString());
                    mmPkgRepo.save(pkg);
                    this.auditService.log(true, AuditAction.DeactivePkg,
                            userNm + " deactivate the Pin code  for " + pkg.getPinCode(), "", "", userNm);
                }
                return apiResult;
            } catch (Exception e) {
                e.printStackTrace();
                return new ApiResult<String>(ApiErrorCodes.PincodeDeactivateError);
            }
        }else {
            return new ApiResult<String>(ApiErrorCodes.PincodeDeactivateError);
        }
    }

    @Override
    @Transactional
    public void removeOverduePkgs() {
        int pkgCleanTime = paramSrv.getPackageOverdueCleanTime();
        log.info("removeOverduePkgs..Current session out time..."
                + pkgCleanTime);
        Date now = new Date();
        Date sessionOutTime = TransactionUtil.addToDate(now, Calendar.MINUTE,
                -pkgCleanTime);
        List<PPSLMMixMatchPackage> overduePackages = mmPkgRepo.findByStatusAndCreatedDateLessThan(PkgStatus.Reserved.toString(), sessionOutTime);

        if(overduePackages != null) {
            for(PPSLMMixMatchPackage oPackage: overduePackages) {
                oPackage.setStatus(PkgStatus.Failed.toString());
                mmPkgRepo.save(oPackage);
            }
        }
    }

    @Transactional
    @Override
    public void updateExpiredStatus() {
        log.info("Pkg updateExpiredStatus .....start");

        List<String> status = new ArrayList<>();
        status.add(PkgStatus.Redeemed.toString());
        status.add(PkgStatus.Available.toString());
        List<PPSLMMixMatchPackage> mmPkgs = mmPkgRepo.findByStatusInAndExpiryDateLessThan(status, NvxDateUtils.clearTime(new Date()));

        for(PPSLMMixMatchPackage mmPkg: mmPkgs) {
            log.info(mmPkg.getName() + " is Expired.");
            mmPkg.setStatus(PkgStatus.Expired.toString());
            mmPkgRepo.save(mmPkg);
        }

        log.info("Pkg updateExpiredStatus .....end");
    }

    @Override
    @Transactional
    public void updatePinStatus() {
        List<PPSLMMixMatchPackage> mmPkgs = mmPkgRepo.findByStatus(PkgStatus.Available.toString());
        for(PPSLMMixMatchPackage mmPkg: mmPkgs) {
            checkRdmStatus(mmPkg.getId(), false);
        }
    }


    @Transactional(readOnly = true)
    private List<PPSLMInventoryTransactionItem> groupTransItemsWithPkgQty(
            PPSLMMixMatchPackage pkg) {
        List<PPSLMMixMatchPackageItem> pkgItems = pkg.getPkgItems();

        List<Integer> itemIds =  mmPkgItemRepo.getPkgTransactionItemsIdByPkgId(pkg.getId());
        List<PPSLMInventoryTransactionItem> transItems = itemRepo.findByIdIn(itemIds);
        for (PPSLMInventoryTransactionItem item : transItems) {
            List<PPSLMInventoryTransactionItem> topupItems = itemRepo.getTaggedTopItem(item.getTransactionId(), item.getProductId(), item.getUnpackagedQty(), item.getProductId(), item.getItemListingId(), null, null, null);
            item.setTopupItems(topupItems);
        }

        for (PPSLMMixMatchPackageItem pkgItem : pkgItems) {
            for (PPSLMInventoryTransactionItem item : transItems) {
                if (item.getId().equals(pkgItem.getTransactionItemId())) {
                    item.setPkgQty(pkgItem.getQty());
                }
                for (PPSLMInventoryTransactionItem topup : item.getTopupItems()) {
                    if (topup.getId().equals(pkgItem.getTransactionItemId())) {
                        topup.setPkgQty(pkgItem.getQty());
                    }
                }
            }
        }
        return transItems;
    }

    private PPSLMMixMatchPackage createMMPkg(String pkgName, Integer pkgQtyPerProd,
                                        String pkgTktType, Integer mainAccId, Boolean isSub,
                                        String username, String pkgDesc, Date expDate, String pkgTktMedia) {
        PPSLMMixMatchPackage immpkg = new PPSLMMixMatchPackage();
        immpkg.setMainAccountId(mainAccId);
        immpkg.setUsername(username);
        immpkg.setSubAccountTrans(isSub);
        immpkg.setName(pkgName);
        immpkg.setDescription(pkgDesc);
        immpkg.setQty(pkgQtyPerProd);
        immpkg.setExpiryDate(expDate);
        immpkg.setStatus(PkgStatus.Reserved.toString());
        immpkg.setQtyRedeemed(0);
        immpkg.setCreatedDate(new Date());
        immpkg.setTicketMedia(pkgTktMedia);

        return immpkg;
    }


    private PPSLMMixMatchPackageItem createMMPkgItemByTransItem(
            PPSLMMixMatchPackage immpkg, PPSLMInventoryTransactionItem item) {
        PPSLMMixMatchPackageItem immpkgItem = new PPSLMMixMatchPackageItem();
        immpkgItem.setPackageId(immpkg.getId());
        immpkgItem.setReceiptNum(item.getInventoryTrans().getReceiptNum());
        immpkgItem.setTransactionItemId(item.getId());
        immpkgItem.setDisplayName(item.getDisplayName());
        immpkgItem.setDisplayDetails(item.getDisplayDetails());
        immpkgItem.setItemProductCode(item.getItemProductCode());
        immpkgItem.setItemListingId(item.getItemListingId());
        immpkgItem.setQty(item.getPkgQty());
        immpkgItem.setTransItemType(item.getTransItemType()); //TODO dunno
        immpkgItem.setTicketType(item.getTicketType());
        immpkgItem.setItemType(item.getItemType()); //TODO DUNNO
        immpkgItem.setQtyRedeemed(0);
        immpkgItem.setDateOfVisit(item.getDateOfVisit());
        immpkgItem.setEventGroupId(item.getEventGroupId());
        immpkgItem.setEventLineId(item.getEventLineId());
        immpkgItem.setEventName(item.getEventName());
        return immpkgItem;
    }

    private PPSLMMixMatchPackageItem createMMPkgItemByTopupItem(
            PPSLMMixMatchPackage immpkg, PPSLMInventoryTransactionItem topupItem,
            PPSLMInventoryTransactionItem praItem) {
        PPSLMMixMatchPackageItem immpkgItem = createMMPkgItemByTransItem(immpkg,
                topupItem);
        immpkgItem.setQty(praItem.getPkgQty());
        immpkgItem.setQtyRedeemed(0);
        return immpkgItem;

    }

    private String checkPkgNameInUse(String pkgName, String partnerCode) {
        PPSLMMixMatchPackage sameNamePkg = mmPkgRepo.findByName(pkgName);
        while (sameNamePkg != null) {
            pkgName = PkgUtil.generatePkgName(partnerCode);
            sameNamePkg = mmPkgRepo.findByName(pkgName);
        }

        return pkgName;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultVM verifyPkg(String transItemIdsStr, String pkgName, String partnerCode, Integer pkgQtyPerProd, String pkgTktType, String pkgTktMedia) {
        ResultVM result = new ResultVM();
        List<Integer> tranIdsInt = PkgUtil.getTransIdsFromStr(transItemIdsStr);
        List<PPSLMInventoryTransactionItem> transItems = itemRepo.findByIdIn(tranIdsInt);
        List<String> listingIds = new ArrayList<>();

        Map<String, Node> productMap = new HashMap<>();
        for (PPSLMInventoryTransactionItem item : transItems) {
            List<PPSLMInventoryTransactionItem> topupItems = itemRepo
                    .getTaggedTopItem(item.getTransactionId(), item.getProductId(), item.getUnpackagedQty(), item.getProductId(), item.getItemListingId(), null, null, null);
            item.setTopupItems(topupItems);
            listingIds.add(item.getItemListingId()); //TODO need to check the TOPUP?
            productMap.put(item.getProductId(), starfn.getCMSProduct(channel.code, item.getProductId()));
        }


//        List<ProductExtViewModel> productExtRes = getProductExt(listingIds);
//        boolean hasPinCode = false;
//        for(ProductExtViewModel productExt:productExtRes) {
//            if("PIN".equalsIgnoreCase(productExt.getMediaTypeId())) {
//                hasPinCode = true;
//                break;
//            }
//        }
//
//        if(hasPinCode && !TicketMediaType.Pincode.name().equalsIgnoreCase(pkgTktMedia)) {
//            result.setStatus(false);
//            result.setMessage(ApiErrorCodes.PkgValidatePincodeOnly.message);
//            return result;
//        }

        ResultVM verifyResult = PkgValidator.verifyPackageItems(transItems, productMap);
        ResultVM verifyResult2 = PkgValidator.checkIfItemNeedRemoveDueToQty(transItems, pkgQtyPerProd, pkgTktType);
        ResultVM verifyResult3 = getCheckItemsLimitsBySystemParam(transItems);
        // verify Item id servered
        List<PPSLMMixMatchPackageItem> pkgitems = mmPkgItemRepo.findByTransactionItemIdIn(tranIdsInt);
        ResultVM verifyResult4 = PkgValidator.checkIfAnyItemReserved(pkgitems, pkgName);
        ResultVM verifyResult5 = PkgValidator.checkMinQtyAllowed(pkgQtyPerProd, PkgValidator.MIN_QTY_ALLOWED);
        ResultVM verifyResult6 = PkgValidator.checkMaxQtyAllowed(pkgQtyPerProd, PkgValidator.MAX_QTY_ALLOWED);
        ResultVM verifyAllRes = ResultVM.processResult(verifyResult,
                verifyResult2, verifyResult4, verifyResult5,
                verifyResult6);
//
        if (verifyAllRes.isStatus()) {
            log.debug("Verify Pkg name when all validation passed");
            result.setStatus(true);
            String newPkgName = checkPkgNameInUse(pkgName, partnerCode);
            if (!pkgName.equals(newPkgName)) {
                result.setStatus(false);
                result.setMessage(ApiErrorCodes.PkgNameAlreadyInUsed.message);
            }
        } else {
            result.setStatus(false);
            result.setMessage(verifyAllRes.getMessage());
        }

        return result;
    }


    public ResultVM genTicket(Integer pkgId, String customerId, TicketMediaType mediaType) {
        //TODO need to call api to generate some ticketing stuff
        ResultVM resultVM = new ResultVM();
        PPSLMMixMatchPackage mmp = mmPkgRepo.findOne(pkgId);
        List<PPSLMMixMatchPackageItem> mmpiList = mmPkgItemRepo.findByPackageId(pkgId);
        List<PPSLMInventoryTransactionItem> itemList = new ArrayList<>();

        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();
        final List<String> transIdList = new ArrayList<>();

        boolean singleProduct = true;
        AxRetailCartTicket retailCartTicket = null; //store a copy so to check if single or not

        Map<Integer, Integer> salesOrderLineNumberQtyMap = new HashMap<>();

        for (PPSLMMixMatchPackageItem pkgItem :  mmpiList) {
            int remainingQtyToBeApplied = pkgItem.getQty(); //this is the qty i needed to pass to checkout.

            PPSLMInventoryTransactionItem txnItem =  itemRepo.findOne(pkgItem.getTransactionItemId());
            txnItem.setPkgQty(pkgItem.getQty());

            itemList.add(txnItem);
//            if(TransItemType.Standard.name().equals(txnItem.getTransItemType())) {
//
//                List<PPSLMInventoryTransactionItem> topupItems = itemRepo
//                        .getTaggedTopItem(txnItem.getTransactionId(), txnItem.getProductId(), txnItem.getUnpackagedQty(), txnItem.getProductId(), txnItem.getItemListingId(),
//                                null, null, null);
//
//                txnItem.setTopupItems(topupItems);
//
//                standardItemList.add(txnItem);  //standard only
//            }

            PPSLMInventoryTransaction txn = txnItem.getInventoryTrans();
            String receiptNumber = txn.getReceiptNum();

            List<PPSLMAxSalesOrder> salesOrdersList = salesOrderRepo.findByTransactionId(txn.getId());
            PPSLMAxSalesOrder salesOrder;

            if(salesOrdersList.size() > 0) {
                salesOrder = salesOrdersList.get(0);

                //get the sales order line numbers qyt
                List<PPSLMAxSalesOrderLineNumberQuantity> salesOrderLineNumberQuantities = salesOrderLineNumberQuantityRepo.findBySalesOrderId(salesOrder.getId());
                Map<Integer, PPSLMAxSalesOrderLineNumberQuantity> salesOrderLineNumberQuantityMap = new HashMap<>();

                for(PPSLMAxSalesOrderLineNumberQuantity lineNumberQuantity: salesOrderLineNumberQuantities) {
                    salesOrderLineNumberQuantityMap.put(lineNumberQuantity.getLineNumber(), lineNumberQuantity);
                }
                // finalCheckoutCartList.add(checkoutCart); //remember all teh checkoutcart

                //check the salesOrder
                AxStarSalesOrder axSalesOrder = JsonUtil.fromJson(salesOrder.getSalesOrderJson(), AxStarSalesOrder.class);

                for(AxStarSalesLine axStarSaleLine: axSalesOrder.getSalesLines()) {
                    final AxRetailCartTicket ct = new AxRetailCartTicket();
//                    ct.setItemId(axStarCartLine.getItemId());
//                    ct.setListingId(axStarCartLine.getProductId());

                    String comment = axStarSaleLine.getComment();

                    //The comment should never be null, because i'm always setting the value
                    if(StringUtils.isEmpty(comment)) {
                        log.info("No comment indicated. Will not proceed with the mix and match...");
                        resultVM.setMessage("No comment indicated. Will not proceed with the mix and match...");
                        resultVM.setStatus(false);
                        return resultVM;
                    }

                    AxStarCartLineComment cartLineComment =  JsonUtil.fromJson(comment, AxStarCartLineComment.class);

                    //check if the cartline is equal the transItem details
                    if(txnItem.getProductId().equals(cartLineComment.getCmsProductId())
                            && txnItem.getItemProductCode().equals(axStarSaleLine.getItemId())
                            && txnItem.getItemListingId().equals(axStarSaleLine.getProductId().toString())
                            && remainingQtyToBeApplied > 0) {

                        //well there should be!
                        if(salesOrderLineNumberQuantityMap.containsKey(axStarSaleLine.getLineNumber())) {
                            PPSLMAxSalesOrderLineNumberQuantity salesOrderLineNumberQuantity = salesOrderLineNumberQuantityMap.get(axStarSaleLine.getLineNumber());
                            if(salesOrderLineNumberQuantity.getQty().longValue() >  0) {

                                ct.setItemId(axStarSaleLine.getItemId());
                                ct.setListingId(axStarSaleLine.getProductId());

                                if(salesOrderLineNumberQuantity.getQty().longValue() <= remainingQtyToBeApplied) {
                                    ct.setQty(salesOrderLineNumberQuantity.getQty());
                                    int qty = salesOrderLineNumberQuantity.getQty();
                                    remainingQtyToBeApplied = remainingQtyToBeApplied - qty;
                                    salesOrderLineNumberQuantity.setQty(0);
                                    salesOrderLineNumberQuantityRepo.save(salesOrderLineNumberQuantity);

                                    //remember
                                    if(salesOrderLineNumberQtyMap.containsKey(salesOrderLineNumberQuantity.getId())) {
                                       qty = qty + salesOrderLineNumberQtyMap.get(salesOrderLineNumberQuantity.getId());
                                    }

                                    salesOrderLineNumberQtyMap.put(salesOrderLineNumberQuantity.getId(), qty);

                                }else {
                                    int qty = remainingQtyToBeApplied;

                                    ct.setQty(remainingQtyToBeApplied);
                                    salesOrderLineNumberQuantity.setQty(salesOrderLineNumberQuantity.getQty() - remainingQtyToBeApplied);
                                    salesOrderLineNumberQuantityRepo.save(salesOrderLineNumberQuantity);

                                    //remember
                                    if(salesOrderLineNumberQtyMap.containsKey(salesOrderLineNumberQuantity.getId())) {
                                        qty = qty + salesOrderLineNumberQtyMap.get(salesOrderLineNumberQuantity.getId());
                                    }

                                    salesOrderLineNumberQtyMap.put(salesOrderLineNumberQuantity.getId(), qty);

                                    remainingQtyToBeApplied = 0;
                                }

                                ct.setTransactionId(receiptNumber);  //receipt number
                                ct.setLineId(salesOrderLineNumberQuantity.getLineId());
                                ct.setLineNumber(salesOrderLineNumberQuantity.getLineNumber() == null ? "" : salesOrderLineNumberQuantity.getLineNumber() + "");

                                if(StringUtils.isNotBlank(cartLineComment.getEventGroupId())) {
                                    ct.setEventGroupId(cartLineComment.getEventGroupId());
                                    ct.setEventLineId(cartLineComment.getEventLineId());
                                    ct.setEventDate(cartLineComment.getEventDate());
                                }else {
                                    ct.setEventDate(null); //need to set to null
                                }

                                ct.setUpdateCapacity(0);
                                cartTickets.add(ct);

                                if(retailCartTicket == null) {
                                    retailCartTicket = new AxRetailCartTicket();
                                    retailCartTicket.setItemId(ct.getItemId());
                                    retailCartTicket.setListingId(ct.getListingId());
                                }else {
                                    //compare the ct with retailTicket stored
                                    if(!retailCartTicket.getListingId().equals(ct.getListingId()) ||
                                            !retailCartTicket.getItemId().equals(ct.getItemId())) {
                                        singleProduct = false;
                                    }
                                }

                            }
                        }
                    }

                    if(remainingQtyToBeApplied == 0) {
                        break; //break the loop if already get the enough qty
                    }

                }

            }else {
                resultVM.setStatus(false);
                resultVM.setMessage("Failed..");
                return resultVM;
            }

            if(!transIdList.contains(receiptNumber)) {
                transIdList.add(receiptNumber);
            }

        }



        if(cartTickets.size() > 0) {
            if(singleProduct) {
                try {
                    final ApiResult<List<AxRetailTicketRecord>> result = axChannelTransactionService.apiB2BCartCheckoutStartSingle(channel, cartTickets, customerId);
                    if (!result.isSuccess()) {
                        onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                        log.error("Failed to perform cart checkout start single at AX B2B Retail service.");
                        resultVM.setStatus(false);
                        resultVM.setMessage(result.getMessage());
                        return resultVM;
                    }
                } catch (AxChannelException e) {
                    onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                    log.error("Error encountered calling AX B2B Retail service for cart checkout start single.", e);
                    resultVM.setStatus(false);
                    resultVM.setMessage(ApiErrorCodes.General.message);
                    return resultVM;
                }
            }else {
                try {
                    final ApiResult<List<AxRetailTicketRecord>> result = axChannelTransactionService.apiB2BCartCheckoutStartCombined(channel, cartTickets, customerId);
                    if (!result.isSuccess()) {
                        onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                        log.error("Failed to perform cart checkout start combined at AX B2B Retail service.");
                        resultVM.setStatus(false);
                        resultVM.setMessage(result.getMessage());
                        return resultVM;
                    }

                    List<AxRetailTicketRecord> ticketDatas = result.getData();

                    for(AxRetailTicketRecord ticketData: ticketDatas) {
                        transIdList.clear();
                        transIdList.add(ticketData.getTransactionId());
                        break;
                    }

                } catch (AxChannelException e) {
                    onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                    log.error("Error encountered calling AX B2B Retail service for cart checkout start combined.", e);
                    resultVM.setStatus(false);
                    resultVM.setMessage(ApiErrorCodes.General.message);
                    return resultVM;
                }
            }




            List<AxRetailTicketRecord> axTickets = new ArrayList<>();

            for(String transId: transIdList) {

                try {
                    final ApiResult<List<AxRetailTicketRecord>> result = axChannelTransactionService.apiB2BCartCheckoutComplete(channel, transId);
                    if (!result.isSuccess()) {
                        try {
                            final ApiResult<List<AxRetailTicketRecord>> cancelCheckoutRes = axChannelTransactionService.apiB2BCartCheckoutCancel(channel, transId);
                            if(!cancelCheckoutRes.isSuccess()) {
                                log.error("Failed to perform cart checkout cancel at AX Retail service.");
                                log.error(cancelCheckoutRes.getMessage());
                            }
                        }catch(Exception e) {
                            log.error(e.getMessage(), e);
                        }
                        onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                        log.error("Failed to perform cart checkout finalise at AX Retail service.");
                        log.error(result.getMessage());
                        resultVM.setStatus(false);
                        resultVM.setMessage(ApiErrorCodes.General.message);
                        return resultVM;
                    }
                    axTickets.addAll(result.getData());
                } catch (AxChannelException e) {
                    try {
                        final ApiResult<List<AxRetailTicketRecord>> cancelCheckoutRes = axChannelTransactionService.apiB2BCartCheckoutCancel(channel, transId);
                        if(!cancelCheckoutRes.isSuccess()) {
                            log.error("Failed to perform cart checkout cancel at AX Retail service.");
                            log.error(cancelCheckoutRes.getMessage());
                        }
                    }catch(Exception e2) {
                        log.error(e2.getMessage(), e2);
                    }
                    onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                    log.error("Error encountered calling api cart checkout finalise.", e);
                    resultVM.setStatus(false);
                    resultVM.setMessage(ApiErrorCodes.General.message);
                    return resultVM;
                }

            }

            try {

                final Map<String, PPSLMInventoryTransactionItem> tiMap = new HashMap<>();
                String ticketType = "";
                for (PPSLMInventoryTransactionItem ti : itemList) {
                    tiMap.put(ti.getItemProductCode(), ti);
                    ticketType = ti.getTicketType();
                }

                final List<ETicketData> eTickets = new ArrayList<>();
                for (AxRetailTicketRecord axTicket : axTickets) {
                    final TicketData rawData = new TicketData();

                    List<AxFacility> axFacilities = axTicket.getFacilityClassEntity();
                    List<Facility> facilities = new ArrayList<>();

                    for(AxFacility axFac: axFacilities) {
                        Facility fac = new Facility();
                        fac.setFacilityId(axFac.getFacilityId());
                        fac.setFacilityAction(axFac.getFacilityAction());
                        fac.setOperationIds(axFac.getOperationIds());
                        facilities.add(fac);
                    }

                    rawData.setFacilities(facilities);
                    rawData.setQuantity(1);
                    rawData.setStartDate(NvxDateUtils.formatDate(axTicket.getStartDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
                    rawData.setEndDate(NvxDateUtils.formatDate(axTicket.getEndDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
                    rawData.setTicketCode(axTicket.getTicketCode());

                    final ETicketData td = new ETicketData();

                    td.setRawValidationData(rawData);

                    if (axTicket.isNeedsActivation()) {
                        td.setThirdParty(false);
                        td.setCodeData(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)));
                        td.setBarcodeBase64(QRCodeGenerator.toBase64(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)), 200, 200));
                    } else {
                        td.setThirdParty(true);
                        td.setCodeData(axTicket.getTicketCode());
                        td.setBarcodeBase64(BarcodeGenerator.toBase64(axTicket.getTicketCode(), 300, 75));
                    }

//                    td.setDisplayName(axTicket.getPrintTokenEntity().get("DisplayName"));

//                    final PPSLMInventoryTransactionItem ti = tiMap.get(axTicket.getItemId());
//                    if (ti != null) {
//                        td.setTotalPrice(NvxNumberUtils.formatToCurrency(ti.getUnitPrice(), "S$ "));
//                        td.setTicketPersonType(ti.getTicketType());
//                    }else {
//                        td.setTotalPrice("");
//                        td.setTicketPersonType(ticketType);
//                    }

                    td.setTicketPersonType(ticketType);
                    td.setValidityStartDate(NvxDateUtils.formatDate(axTicket.getStartDate(), "dd/MM/yyyy"));
                    td.setValidityEndDate(NvxDateUtils.formatDate(axTicket.getEndDate(), "dd/MM/yyyy"));
                    td.setTicketNumber(axTicket.getTicketCode());
                    td.setTicketTemplateName(axTicket.getTemplateName());

                    Map<String, String> tokenList = axTicket.getPrintTokenEntity();
                    tokenList.put(OnlineAxTicketToken.MixMatchPackageName.name(), mmp.getName());
                    tokenList.put(OnlineAxTicketToken.MixMatchDescription.name(), mmp.getDescription());
                    //tokenList.put(OnlineAxTicketToken.TicketType.name(), ticketType); //SHOULD I DO THIS?

                    td.setTokenData(JsonUtil.jsonify(new ETicketTokenList(tokenList)));

                    eTickets.add(td);
                }

                //SAVE TO DB
                //successful, then apply changes
                onGenTicketSuccess(mmp, itemList);

                //save the Etickets to DB
                for(ETicketData ticketData: eTickets) {
                    PPSLMETicket eTicket = new PPSLMETicket();
                    eTicket.setPackageId(mmp.getId());
                    eTicket.setDisplayName(ticketData.getDisplayName());
                    eTicket.setTicketNumber(ticketData.getTicketNumber());
                    eTicket.setTicketPersonType(ticketData.getTicketPersonType());
                    eTicket.setTotalPrice(ticketData.getTotalPrice());
                    eTicket.setEventDate(ticketData.getEventDate());
                    eTicket.setEventSession(ticketData.getEventSession());
                    eTicket.setValidityStartDate(ticketData.getValidityStartDate());
                    eTicket.setValidityEndDate(ticketData.getValidityEndDate());
                    eTicket.setShortDisplayName(ticketData.getShortDisplayName());
                    eTicket.seteTicketData(JsonUtil.jsonify(ticketData));
                    eTicketRepo.save(eTicket); //ERROR here
                }


                MixMatchPkgVM mmpVm = getPkgByID(pkgId);
                mmpVm.setEtickets(eTickets);
                resultVM.setViewModel(mmpVm);


                if(TicketMediaType.ETicket.equals(mediaType)) {
                    try {
                        sendEticketEmail(pkgId);
                    }catch (Exception e) {
                        resultVM.setStatus(false);
                        resultVM.setMessage("Error in sending email...");
                    }
                }else if(TicketMediaType.ExcelFile.equals(mediaType)) {
                    try {
                        sendExcelEmail(pkgId);
                    }catch (Exception e) {
                        resultVM.setStatus(false);
                        resultVM.setMessage("Error in sending email...");
                    }
                }

                return resultVM;

            } catch (Exception e) {
                log.error("Error encountered generating eTickets.", e);
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.General.message);

                return resultVM;
            }
        }else {
            resultVM.setStatus(false);
            log.error("No cart tickets found.. so will make this as an error... but why???");
            resultVM.setMessage(ApiErrorCodes.General.message);
        }

        return resultVM;

    }

    @Override
    @Transactional
    public ResultVM genETicket(Integer pkgId, String customerId) {
        return genTicket(pkgId, customerId, TicketMediaType.ETicket);
    }


    @Override
    @Transactional
    public ResultVM genPinCode(Integer pkgId) {
        ResultVM resultVM = new ResultVM();
        PPSLMMixMatchPackage mmp = mmPkgRepo.findOne(pkgId);
        log.info("Generate PIN Code for Pkg Name" + mmp.getName());

        List<PPSLMMixMatchPackageItem> mmpiList = mmPkgItemRepo.findByPackageId(pkgId);
        List<PPSLMInventoryTransactionItem> itemList = new ArrayList<>();

        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();
        final List<String> transIdList = new ArrayList<>();

        final List<AxRetailGeneratePinInput> pinInputs = new ArrayList<>();

        Map<Integer, Integer> salesOrderLineNumberQtyMap = new HashMap<>();

        boolean singleProduct = true;
        String itemId = null;


        final List<String> listingIds = new ArrayList<>();

        List<PPSLMMixMatchPackagePinLine> packagePinLines = new ArrayList<>();

        for (PPSLMMixMatchPackageItem pkgItem :  mmpiList) {

            int remainingQtyToBeApplied = pkgItem.getQty(); //this is the qty i needed to pass to checkout.

            PPSLMInventoryTransactionItem txnItem = itemRepo.findOne(pkgItem.getTransactionItemId());
            txnItem.setPkgQty(pkgItem.getQty());

            listingIds.add(txnItem.getItemListingId());

            itemList.add(txnItem);

            PPSLMInventoryTransaction txn = txnItem.getInventoryTrans();
            String receiptNumber = txn.getReceiptNum();

            if (!transIdList.contains(receiptNumber)) {
                transIdList.add(receiptNumber);
            }


            List<PPSLMAxSalesOrder> salesOrdersList = salesOrderRepo.findByTransactionId(txn.getId());

            PPSLMAxSalesOrder salesOrder;

            if (salesOrdersList.size() > 0) {
                salesOrder = salesOrdersList.get(0);


                //get the sales order line numbers qyt
                List<PPSLMAxSalesOrderLineNumberQuantity> salesOrderLineNumberQuantities = salesOrderLineNumberQuantityRepo.findBySalesOrderId(salesOrder.getId());
                Map<Integer, PPSLMAxSalesOrderLineNumberQuantity> salesOrderLineNumberQuantityMap = new HashMap<>();

                for (PPSLMAxSalesOrderLineNumberQuantity lineNumberQuantity : salesOrderLineNumberQuantities) {
                    salesOrderLineNumberQuantityMap.put(lineNumberQuantity.getLineNumber(), lineNumberQuantity);
                }

                AxStarSalesOrder axSalesOrder = JsonUtil.fromJson(salesOrder.getSalesOrderJson(), AxStarSalesOrder.class);


                for (AxStarSalesLine axStarSaleLine : axSalesOrder.getSalesLines()) {
                    final AxRetailGeneratePinInput pi = new AxRetailGeneratePinInput();

                    String comment = axStarSaleLine.getComment();

                    //The comment should never be null, because i'm always setting the value
                    if (StringUtils.isEmpty(comment)) {
                        log.info("No comment indicated. Will not proceed with the mix and match...");
                        resultVM.setMessage("No comment indicated. Will not proceed with the mix and match...");
                        resultVM.setStatus(false);
                        return resultVM;
                    }

                    AxStarCartLineComment cartLineComment = JsonUtil.fromJson(comment, AxStarCartLineComment.class);

                    //check if the cartline is equal the transItem details
                    if (txnItem.getItemProductCode().equals(axStarSaleLine.getItemId())
                            && txnItem.getItemListingId().equals(axStarSaleLine.getProductId().toString())
                            && txnItem.getProductId().equals(cartLineComment.getCmsProductId()) && remainingQtyToBeApplied > 0) {

                        //well there should be!
                        if (salesOrderLineNumberQuantityMap.containsKey(axStarSaleLine.getLineNumber())) {
                            PPSLMAxSalesOrderLineNumberQuantity salesOrderLineNumberQuantity = salesOrderLineNumberQuantityMap.get(axStarSaleLine.getLineNumber());
                            if (salesOrderLineNumberQuantity.getQty().longValue() > 0) {

                                pi.setAllowPartialRedemption(true);
                                pi.setCombineTicket(false);
                                pi.setCustomerAccount(axSalesOrder.getCustomerId());
                                pi.setDescription(mmp.getDescription());

                                Calendar cal = Calendar.getInstance();
                                cal.setTime(mmp.getExpiryDate());
                                cal.add(Calendar.DATE, 1);
                                pi.setEndDateTime(cal.getTime()); //purposely add one more day

                                //Event date logic
                                if (StringUtils.isNotEmpty(cartLineComment.getEventGroupId())) {
                                    pi.setEventGroupId(cartLineComment.getEventGroupId());
                                    pi.setEventLineId(cartLineComment.getEventLineId());
                                    pi.setEventDate(cartLineComment.getEventDate());
                                }

                                pi.setGroupTicket(false);
                                pi.setInventTransId(axStarSaleLine.getLineId());
                                pi.setInvoiceId(axStarSaleLine.getInvoiceId());
                                pi.setItemId(axStarSaleLine.getItemId());
                                pi.setLineNumber(BigDecimal.valueOf(axStarSaleLine.getLineNumber()));
                                pi.setMediaType("paper");
                                pi.setPackageName(mmp.getName());


                                if (salesOrderLineNumberQuantity.getQty().longValue() <= remainingQtyToBeApplied) {

                                    int qty = salesOrderLineNumberQuantity.getQty();
                                    pi.setQty(BigDecimal.valueOf(salesOrderLineNumberQuantity.getQty()));
                                    pi.setQtyPerProduct(BigDecimal.valueOf(salesOrderLineNumberQuantity.getQty()));
                                    remainingQtyToBeApplied = remainingQtyToBeApplied - qty;
                                    salesOrderLineNumberQuantity.setQty(0);
                                    salesOrderLineNumberQuantityRepo.save(salesOrderLineNumberQuantity);

                                    //remember
                                    if(salesOrderLineNumberQtyMap.containsKey(salesOrderLineNumberQuantity.getId())) {
                                        qty = qty + salesOrderLineNumberQtyMap.get(salesOrderLineNumberQuantity.getId());
                                    }

                                    salesOrderLineNumberQtyMap.put(salesOrderLineNumberQuantity.getId(), qty);
                                } else {
                                    int qty = remainingQtyToBeApplied;

                                    pi.setQty(BigDecimal.valueOf(remainingQtyToBeApplied));
                                    pi.setQtyPerProduct(BigDecimal.valueOf(remainingQtyToBeApplied));
                                    salesOrderLineNumberQuantity.setQty(salesOrderLineNumberQuantity.getQty() - qty);
                                    salesOrderLineNumberQuantityRepo.save(salesOrderLineNumberQuantity);
                                    remainingQtyToBeApplied = 0;

                                    //remember
                                    if(salesOrderLineNumberQtyMap.containsKey(salesOrderLineNumberQuantity.getId())) {
                                        qty = qty + salesOrderLineNumberQtyMap.get(salesOrderLineNumberQuantity.getId());
                                    }

                                    salesOrderLineNumberQtyMap.put(salesOrderLineNumberQuantity.getId(), qty);
                                }

                                pi.setReferenceId(mmp.getId() + ""); //TODO double check what is the correct value to be passed.
                                pi.setRetailVariantId("");
                                pi.setSalesId(axSalesOrder.getSalesId());
                                pi.setStartDateTime(mmp.getCreatedDate());
                                pi.setTransactionId(axSalesOrder.getId()); //from the sales order
                                pi.setGuestName(mmp.getUsername());

                                //pi.setCcLast4Digits(0);

                                pi.setPinType(AxPinType.B2B);

                                if(itemId == null) {
                                    itemId = pi.getItemId();
                                }else if(!itemId.equals(pi.getItemId())) {
                                    singleProduct = false;
                                }



                                PPSLMMixMatchPackagePinLine pinLine = new PPSLMMixMatchPackagePinLine();
                                pinLine.setPackageItemId(pkgItem.getId());
                                DecimalFormat df = new DecimalFormat("#");
                                pinLine.setLineNum(df.format(pi.getLineNumber()));
                                pinLine.setReceiptNum(pi.getTransactionId());

                                packagePinLines.add(pinLine);


                                pinInputs.add(pi);

                            }
                        }
                    }

                    if (remainingQtyToBeApplied == 0) {
                        break; //break the loop if already get the enough qty
                    }

                }

            } else {
                onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                resultVM.setStatus(false);
                resultVM.setMessage("Failed..");
                return resultVM;
            }
        }

        if(!singleProduct) {
            //check which will be combined or not
            List<ProductExtViewModel> productExt = getProductExt(listingIds);

            int noOfPax = 0;
            boolean noCombined = false;
            List<String> itemIds = new ArrayList<>();
            //TODO whats the handling for no. of pax ah???? ALL SINGLE ah?
            for(ProductExtViewModel prod: productExt) {

                if(noOfPax == 0) {
                    noOfPax = prod.getNoOfPax();
                }

                if(noOfPax != prod.getNoOfPax()) {
                    noCombined = true;
                }

                if(prod.getPrinting() == 3) {
                    noCombined = true;
                }

                if(prod.isEvent()) {
                    noCombined = true;
                }

                if(itemIds.contains(prod.getItemId())) {
                    noCombined = true;
                }else {
                    itemIds.add(prod.getItemId());
                }
            }

            if(!noCombined) {
                for(AxRetailGeneratePinInput pi: pinInputs) {
                    pi.setCombineTicket(true);
                }
            }
        }

        try {
            final ApiResult<List<AxRetailPinTable>> generatePinResult = axChannelTransactionService.apiGeneratePin(channel, pinInputs);
            log.info("GeneratePin RESULT: " + JsonUtil.jsonify(generatePinResult));

            if(!generatePinResult.isSuccess()) {
                onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                log.error("Error in generate pin code");
                resultVM.setStatus(false);
                resultVM.setMessage("Failed in generating pin code");

                return resultVM;
            }

            List<AxRetailPinTable> pinTableList = generatePinResult.getData();
            String pincode = null;
            for(AxRetailPinTable pinTable: pinTableList) {
                log.info(pinTable.getPinCode());
                pincode = pinTable.getPinCode();
            }

            if(pincode != null) {
                mmp.setPinCode(pincode);
            }else {
                onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
                resultVM.setStatus(false);
                resultVM.setMessage("Error in generating the pin code");

                return resultVM;
            }


            //save the pin line
            for(PPSLMMixMatchPackagePinLine pinLine:packagePinLines) {
                itemPinLineRepo.save(pinLine);
            }

            onGenTicketSuccess(mmp, itemList);

            MixMatchPkgVM mmpVm = getPkgByID(pkgId);
            resultVM.setViewModel(mmpVm);

            try {
                sendPincodeEmail(pkgId);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                resultVM.setStatus(false);
                resultVM.setMessage("Failed in sending email for pin code");

                return resultVM;
            }

            return resultVM;

        } catch (AxChannelException e) {
            onGenTicketFail(mmp, salesOrderLineNumberQtyMap);
            log.error("Error encountered generating pincode.", e);
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.General.message);

            return resultVM;
        }

        //TODO
//        List<RedeemItem> items = getRedeemItems(pkgItems);
////        String expDateStr = NvxUtil.utcFmt.print(pkg.getExpiryDate().getTime());
//        int encodeType = 1;
//        Set<Integer> itemCode = PkgUtil.getAllItemCodeByPkgItem(pkgItems);
//        log.debug("itemCode size " + itemCode.size());
//        // if pkg has more than 1 item code, need set encodeType as 2 to ST
//        if (itemCode.size() > 1) {
//            encodeType = 2;
//        }
//        // Check if item being used with user doing transaction
//        if (!beforeGenPinCodeCheckIfItemUsed(pkgItems)) {
//            MMPkgVM pkgVm = getPkgByID(pkgId);
//            result.setViewModel(pkgVm);
//            result.setStatus(false);
//            result.setMessage(textProvider.getText(MSG_GenPINCodeFailedItemUsed));
//            return result;
//        }
//        Calendar ca = Calendar.getInstance();
//        ca.setTime(pkg.getExpiryDate());
//        Integer dateDiff = sysService.getObjByKey(
//                SysParamConst.PkgVildDateDiffToSACT.toString(), Integer.class, true);
//        if(dateDiff!=null){
//            ca.add(Calendar.DATE, dateDiff);
//        }
//        SactResult sacRes = sactService.getPinCode(encodeType,
//                ca, pkg.getName(), items);
//
//        log.info("PIN Code encodeType:" + encodeType);
//        log.info("PIN Code expDate:" + ca);
//        log.info("PIN Code pinReservedId:" + pkg.getName());
//        log.info("PIN Code SACT Date Diff:" + dateDiff);
//
//        if (sacRes.getSuccess()) {
//            // sometimes ST will return erro msg, even it is "success"
//            log.info("sacRes.getStatusMessage()" + sacRes.getStatusMessage());
//            // deduct qty from inventrans item
//            String pinCode = sacRes.getOutPinRequest().getPinCode();
//            PinRequestOutput pinRequest = sacRes.getOutPinRequest();
//            log.info("pinCode is generated " + pinCode);
//            // check pin code successful generated before this submit
//            MixMatchPackage tmpPkg = this.mmPkgDao.findUniqueByProperty(
//                    "pinCode", pinCode);
//            if (tmpPkg != null) {
//                log.info("pin code successful generated before this submit....double submited");
//                MMPkgVM pkgVm = getPkgByID(pkgId);
//                result.setViewModel(pkgVm);
//                result.setStatus(true);
//                return result;//if pin code already created
//            }
//            onGenPinCodeSuccess(pkg, pkgItems, pinRequest);
//            MMPkgVM pkgVm = getPkgByID(pkgId);
//            result.setViewModel(pkgVm);
//            result.setStatus(true);
//            try {
//                sendPinCodeEmail(pkgId);
//            } catch (Exception e) {
//                result.setStatus(false);
//                result.setMessage(textProvider.getText(MSG_SendEmailFailed));
//                e.printStackTrace();
//            }
//        } else {
//            MixMatchPackage mmpkg = mmPkgDao.findUniqueByProperty("id",pkgId);
//            log.info("pin code:"+mmpkg.getPinCode());
//            if(mmpkg!=null&&!StringUtils.isEmpty(mmpkg.getPinCode())){
//                log.info("pin code successful generated before this submit.. error in webservice....double submited");
//                MMPkgVM pkgVm = getPkgByID(pkgId);
//                result.setViewModel(pkgVm);
//                result.setStatus(true);
//                return result;//if pin code already created
//            }
//
//            String message = " Generate PIN code error "
//                    + sacRes.getResponseCode().getStatus().getValue() + " : "
//                    + sacRes.getStatusMessage();
//            onGenPinCodeFail(pkg);
//            MMPkgVM pkgVm = getPkgByID(pkgId);
//            result.setViewModel(pkgVm);
//            result.setStatus(false);
//            result.setMessage(message);
//            log.error(message);
//        }
    }

    @Override
    @Transactional
    public ResultVM checkRdmStatus(Integer pkgId) {
        return checkRdmStatus(pkgId, true);
    }

    @Override
    @Transactional
    public ResultVM checkRdmStatus(Integer pkgId, boolean updLastCheckDate) {
        ResultVM result = new ResultVM();
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        try {
            ApiResult<AxWOTPinViewTable> pinDetail = axChannelTransactionService.apiGetB2BPINTableInquiry(StoreApiChannels.PARTNER_PORTAL_SLM, pkg.getPinCode());
            if(pinDetail.isSuccess()) {
                AxWOTPinViewTable pincodeRes = pinDetail.getData();
                if(pincodeRes != null) {
                    onGetPinStatusSuccess(pkg, pincodeRes, updLastCheckDate);
                }else {
                    result.setStatus(false);
                    result.setMessage("Cannot find the pincode.");
                }
            }else {
                result.setStatus(false);
                result.setMessage("Error encountered on getting the pincode status");
            }
        }catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(false);
            result.setMessage("Error encountered on getting the pincode status");
        }

        return result;
    }

    @Transactional
    private void onGetPinStatusSuccess(PPSLMMixMatchPackage pkg, AxWOTPinViewTable pinDetail, boolean updLastCheckDate) {

        HashMap<String, Integer> pinLineRedeemedQtyMap = new HashMap<>();
        HashMap<String, Integer> pinLineRedeemedQtyMapNoLineNum = new HashMap<>();

        int totalQtyRedeemed = 0;

        /**
         * Have to handle the old cases and the migrated one
         */
        for(AxWOTPinViewLine pinLine: pinDetail.getPinViewLineCollection()) {

            int qtyRedeemed = 0;
            int qtyRedeemedNoLineNum = 0;

            String key = pinLine.getItemId() + "" + pinLine.getTransactionId();
            String newKey =  key + "" + pinLine.getTransactionLineNum();

            if(pinLineRedeemedQtyMap.containsKey(newKey)) {
                qtyRedeemed = pinLineRedeemedQtyMap.get(newKey);//TODO evaluate if this is correct.. coz what if there's main item and there's topup also.. how to know which one is which
            }
            qtyRedeemed += pinLine.getQtyRedeemed();

            if(pinLineRedeemedQtyMapNoLineNum.containsKey(key)) {
                qtyRedeemedNoLineNum = pinLineRedeemedQtyMapNoLineNum.get(key);
            }
            qtyRedeemedNoLineNum += pinLine.getQtyRedeemed();

            pinLineRedeemedQtyMap.put(newKey, qtyRedeemed);
            pinLineRedeemedQtyMapNoLineNum.put(key, qtyRedeemedNoLineNum);

            totalQtyRedeemed = totalQtyRedeemed + pinLine.getQtyRedeemed(); //keep adding
        }

        //saved correctly
        for(PPSLMMixMatchPackageItem item: pkg.getPkgItems()) {
            String receiptNumber = item.getTransItem().getInventoryTrans().getReceiptNum();
            List<PPSLMMixMatchPackagePinLine> pinLines = itemPinLineRepo.findByPackageItemId(item.getId());
            String key = item.getItemProductCode() + "" + receiptNumber;
            if(pinLines != null && pinLines.size() > 0) {
                int qtyRedeemed = 0;
                for(PPSLMMixMatchPackagePinLine pinLine: pinLines) {
                    String newKey = key + pinLine;
                    if(pinLineRedeemedQtyMap.containsKey(newKey)) {
                        qtyRedeemed += pinLineRedeemedQtyMap.get(newKey);
                    }
                }
                item.setQtyRedeemed(qtyRedeemed);
                mmPkgItemRepo.save(item);
            }else {
                if(pinLineRedeemedQtyMap.containsKey(key)) {
                    item.setQtyRedeemed(pinLineRedeemedQtyMapNoLineNum.get(key));
                    mmPkgItemRepo.save(item);
                }
            }

        }

        if(PkgStatus.Expired.toString().equals(pkg.getStatus())) {
            //TODO skip?
        }

        pkg.setStatus(PkgStatus.getSTPkgStatus(pinDetail.getPinStatus()));

        if(pinDetail.getLastRedeemDateTime() != null) {
            pkg.setLastRedemptionDate(pinDetail.getLastRedeemDateTime());
        }

        pkg.setTotalQtyRedeemed(totalQtyRedeemed);

        if(updLastCheckDate) {
            pkg.setLastCheckDate(new Date()); //I will only update this when it is actually triggered by the user... if it's striggered by the system then no need la
        }

        mmPkgRepo.save(pkg);
    }

    @Transactional
    private void onGenTicketSuccess(PPSLMMixMatchPackage pkg,
                                     List<PPSLMInventoryTransactionItem> transactionItemList) {
        pkg.setStatus(PkgStatus.Available.toString());
        //pkg.setPinCode(pinRequest.getPinCode());
        pkg.setTicketGeneratedDate(new Date());
        //pkg.setPinRequestId(new Long(pinRequest.getPinRequestID()).toString());
        mmPkgRepo.save(pkg);

        for (PPSLMInventoryTransactionItem item: transactionItemList) {
            item.setUnpackagedQty(item.getUnpackagedQty()
                    - pkg.getQty());
            itemRepo.save(item);
            PPSLMInventoryTransaction trans = item.getInventoryTrans();
            trans.setTicketGenerated(true);
            transRepo.save(trans);
        }
    }

    @Transactional
    private void onGenTicketFail(PPSLMMixMatchPackage pkg, Map<Integer, Integer> salesOrderLineNumberQtyMap) {
        pkg.setStatus(PkgStatus.Failed.toString());
        mmPkgRepo.save(pkg);

        //revert all please
        for(Integer so: salesOrderLineNumberQtyMap.keySet()) {
            PPSLMAxSalesOrderLineNumberQuantity salesOrderLineNumberQuantity = salesOrderLineNumberQuantityRepo.findOne(so);
            salesOrderLineNumberQuantity.setQty(salesOrderLineNumberQuantity.getQty() + salesOrderLineNumberQtyMap.get(so));
            salesOrderLineNumberQuantityRepo.save(salesOrderLineNumberQuantity);
        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<ETicketData> getETickets(Integer pkgId) {
        List<PPSLMETicket> eTicketList = eTicketRepo.findByPackageId(pkgId);
        List<ETicketData> eTicketDataList = new ArrayList<>();

        for(PPSLMETicket  eTicket: eTicketList) {
            ETicketData eTicketData = JsonUtil.fromJson(eTicket.geteTicketData(), ETicketData.class);
            eTicketDataList.add(eTicketData);
        }

        return eTicketDataList;
    }

    //TODO evaluate if has diff with eticket
    @Override
    @Transactional
    public ResultVM genExcelTicket(Integer pkgId, String customerId) {
        return genTicket(pkgId, customerId, TicketMediaType.ExcelFile);
    }

    //TODO evaluate if has diff with eticket
    @Override
    @Transactional(readOnly = true)
    public List<ETicketData> getExcelTickets(Integer pkgId) {
        return getETickets(pkgId);
    }

    @Override
    @Transactional
    public void removeReservedPkg(List<Integer> pkgIds) {
        List<PPSLMMixMatchPackage> mmpList = mmPkgRepo.findByIdInAndStatus(pkgIds, PkgStatus.Reserved.toString());
        for(PPSLMMixMatchPackage mmp: mmpList) {
            mmp.setStatus(PkgStatus.Failed.toString());
            mmPkgRepo.save(mmp);
        }
    }

    @Override
    @Transactional
    public String resendingPkgReceiptEmailByPkgId(int pkgId) throws BizValidationException {
        PPSLMMixMatchPackage pkg = mmPkgRepo.findOne(pkgId);
        if(pkg == null){
            throw new BizValidationException("Invalid Request, Package not found");
        }
        String ticketMedia = pkg.getTicketMedia();
        if(TicketMediaType.ETicket.name().equalsIgnoreCase(ticketMedia)){
            return sendEticketEmail(pkgId);
        }else if(TicketMediaType.Pincode.name().equalsIgnoreCase(ticketMedia)){
            return sendPincodeEmail(pkgId);
        }else if(TicketMediaType.ExcelFile.name().equalsIgnoreCase(ticketMedia)){
            return sendExcelEmail(pkgId);
        }else{
            throw new BizValidationException("Invalid Request, Invalid ticket media not found");
        }
    }
}
