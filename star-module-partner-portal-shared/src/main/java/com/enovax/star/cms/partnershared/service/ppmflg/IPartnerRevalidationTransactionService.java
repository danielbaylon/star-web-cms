package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReceiptVM;

/**
 * Created by jennylynsze on 10/10/16.
 */
public interface IPartnerRevalidationTransactionService {

    //    List<PPMFLGRevalidationTransaction> getTransactionsForStatusUpdate(int lowerBoundMins, int upperBoundMins);

    ReceiptVM getRevalidateInfoByTransId(Integer transId) throws Exception;

    ReceiptVM getRevalidateTransByTransId(Integer transId);

    public String generateRevalReceiptAndEmailByRevalidationId(Integer transId);

//    ReceiptVM getRevalidateTransByReceiptNum(String receiptNumber);

    String generateRevalReceiptAndEmail(PPMFLGInventoryTransaction trans);

    String generateRevalReceiptAndEmail(Integer transId);

    PPMFLGRevalidationTransaction findByReceipt(String receiptNum);

    PPMFLGInventoryTransaction findInvenTransByReceipt(String receiptNum);

    boolean save(PPMFLGRevalidationTransaction trans);
    //
    boolean saveOnSuccessRevalidated(PPMFLGRevalidationTransaction trans,
                                     PPMFLGInventoryTransaction invTrans, String tmApvcd);
    //
    public boolean sendVoidEmail(boolean success, PPMFLGRevalidationTransaction revaltrans,
                                 boolean isSubAccount, PPMFLGTASubAccount subAccount,
                                 PPMFLGTAMainAccount mainAccount);
    //
//    public PPMFLGRevalidationTransaction initRevalTrans(PPMFLGRevalidationTransaction revalTrans);
//
    boolean saveOnSuccessRevalidated(PPMFLGRevalidationTransaction trans,
                                     PPMFLGInventoryTransaction invTrans, String tmApvcd, boolean isfree);
//
//    public List<RevalidationTransaction> getTransForVoidReconciliation(int minsPast);
}
