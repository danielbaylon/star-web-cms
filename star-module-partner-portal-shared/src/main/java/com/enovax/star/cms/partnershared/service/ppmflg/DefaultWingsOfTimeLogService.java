package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservation;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservationLog;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGWingsOfTimeReservationLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by houtao on 26/8/16.
 */
@Service("PPMFLGIWingsOfTimeLogService")
public class DefaultWingsOfTimeLogService implements IWingsOfTimeLogService {

    @Autowired
    private PPMFLGWingsOfTimeReservationLogRepository wotLogRepo;

    @Transactional
    public void saveWingsOfTimeReservationLog(PPMFLGWingsOfTimeReservation wot, String channel, String serviceName, boolean success, String errorCode, String errorMsg, String request, String response) {
        PPMFLGWingsOfTimeReservationLog log = null;
        try{
            log = new PPMFLGWingsOfTimeReservationLog();
            log.setWotReserveId(wot.getId());
            log.setCreatedDateTime(new Date(System.currentTimeMillis()));
            log.setCreatedBy(wot.getCreatedBy());
            log.setSubAccountTrans(wot.isSubAccountTrans());
            log.setServiceName(serviceName);
            log.setChannel(channel);
            log.setErrorCode(errorCode);
            log.setErrorMsg(errorMsg);
            log.setRequestText(request);
            log.setResponseText(response);
            wotLogRepo.save(log);
        }catch (Exception ex){
            try{
                wotLogRepo.save(log);
            }catch (Exception ex2){}
        }
    }
}
