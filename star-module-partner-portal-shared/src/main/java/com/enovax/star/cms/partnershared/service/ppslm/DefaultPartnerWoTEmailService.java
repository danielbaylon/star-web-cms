package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.WOTReservationStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReceiptLog;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMWingsOfTimeReceiptLogRepository;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 7/9/16.
 */
@Service
public class DefaultPartnerWoTEmailService implements IPartnerWoTEmailService {

    private Logger logger = LoggerFactory.getLogger(DefaultPartnerWoTEmailService.class);

    @Autowired
    private IMailService mailSrv;
    @Autowired
    private IEmailTemplateService mailTplSrv;
    @Autowired
    private ITemplateService tplSrv;
    @Autowired
    private ISystemParamService paramSrv;
    @Autowired
    private IAdminService adminSrv;
    @Autowired
    private IGSTRateService gstSrv;
    @Autowired
    private PPSLMWingsOfTimeReceiptLogRepository wotReceiptLogRepo;

    public String generateWoTReservationDailyReceipts(PartnerAccount account, List<PPSLMWingsOfTimeReservation> list, Date date) {
        return genWoTReceipt(account, list, date, false);
    }

    public String generateWoTReservationMonthlyReceipts(PartnerAccount account, List<PPSLMWingsOfTimeReservation> list, Date date) {
        return genWoTReceipt(account, list, date, true);
    }

    @Transactional(rollbackFor = Exception.class)
    private String genWoTReceipt(PartnerAccount account, List<PPSLMWingsOfTimeReservation> wotlist, Date date, boolean isMonthly){
        int count = 0;
        PPSLMWingsOfTimeReceiptLog log =null;
        List<PPSLMWingsOfTimeReceiptLog> logs = wotReceiptLogRepo.findWoTReceiptGenerationLogByTypeAndDate(isMonthly ? "Monthly" : "Daily", date, account.getMainAccount().getId());
        if(logs != null && logs.size() > 0){
            log = logs.get(0);
            if(log != null){
                count = log.getCount() + 1;
            }
        }

        PartnerVM partner = account.getPartner();

        String baseUrl = paramSrv.getApplicationContextPath();
        Map<String,String> contactDtls = paramSrv.getSentosaContactDetails();
        String ownerAddr = contactDtls.get(PartnerPortalConst.ADDRESS_OWN);
        String ownerCont = contactDtls.get(PartnerPortalConst.CONTACT_OWN);

        String address = partner.getExtensionPropertyValue("correspondenceAddress");
        String postalCode = partner.getExtensionPropertyValue("correspondencePostalCode");
        String city = partner.getExtensionPropertyValue("correspondenceCity");

        ReceiptVM receipt = new ReceiptVM();
        receipt.setHasTnc(false);
        receipt.setBaseUrl(baseUrl);
        receipt.setOrgName(partner.getOrgName());
        receipt.setAccountCode(partner.getAccountCode());
        receipt.setAddress(address+" " + postalCode+" " + city);
        receipt.setRecepitNum("");
        receipt.setCurrency(paramSrv.getDefaultPartnerRegistrationCurrencyCode());
        receipt.setShortCurrency(paramSrv.getDefaultPartnerRegistrationShortCurrencyCode());
        receipt.setDepositBalanceAvaialble(false);
        receipt.setDepositBalanceText("");

        String displayTitle = "";
        if(isMonthly){
            if(date != null){
                displayTitle = NvxDateUtils.formatDate(date, NvxDateUtils.WOT_RECEIPT_MONTH_PRINT_FORMAT);
            }
        }else{
            if(date != null){
                displayTitle = NvxDateUtils.formatDate(date, NvxDateUtils.WOT_RECEIPT_DATE_PRINT_FORMAT);
            }
        }

        BigDecimal gstRate = gstSrv.getCurrentGST();

        ReceiptProdVM prodVM = new ReceiptProdVM(displayTitle, displayTitle);
        List<ReceiptItemVM> items = new ArrayList<ReceiptItemVM>();
        BigDecimal totalAmt = new BigDecimal(0.0);
        BigDecimal totalGst = new BigDecimal(0.0);
        if(wotlist != null && wotlist.size() > 0){
            int qty = 0;
            for(PPSLMWingsOfTimeReservation wot : wotlist){

                BigDecimal wotBalanceAmount = BigDecimal.ZERO;

                ReceiptItemVM item = new ReceiptItemVM();
                item.setItemTopup(false);
                if(wot.getEventName() != null && wot.getEventName().trim().length() > 0){
                    item.setDisplayName(wot.getEventName());
                }else{
                    item.setDisplayName("");
                }
                if(wot.getReceiptNum() != null && wot.getReceiptNum().trim().length() > 0){
                    item.setReceiptNum(wot.getReceiptNum());
                }else{
                    item.setReceiptNum("");
                }
                qty = 0;
                if(wot.getQty() != null && wot.getQty().intValue() >= 0){
                    qty = wot.getQty().intValue();
                }
                if(wot.getWasheddown() != null && wot.getWasheddown().intValue() == 1){
                    if(wot.getQtyWashedDown() != null && wot.getQtyWashedDown().intValue() > 0){
                        qty = qty - wot.getQtyWashedDown().intValue();
                    }
                }

                if(com.enovax.star.cms.commons.constant.ppmflg.WOTReservationStatus.Cancelled.name().equals(wot.getStatus())) {
                    qty = 0; //TODO dunno if ok to just set it here
                }

                item.setQty(qty);
                if(wot.getAxChargeAmount() != null){
                    BigDecimal refundAmount = wot.getAxRefundAmount()==null?BigDecimal.ZERO:wot.getAxRefundAmount();
                    wotBalanceAmount = wot.getAxChargeAmount().add(refundAmount);
                    BigDecimal gstRatePercent = gstRate.multiply(new BigDecimal(100));
                    BigDecimal gst = gstRatePercent.multiply(wotBalanceAmount).divide((new BigDecimal(100).add(gstRatePercent)), 2, NvxNumberUtils.DEFAULT_ROUND_MODE);
                    item.setSubTotalStr(NvxNumberUtils.formatToCurrency(wotBalanceAmount));
                    item.setGstStr(NvxNumberUtils.formatToCurrency(gst));
                    totalAmt = totalAmt.add(wotBalanceAmount);
                    totalGst = totalGst.add(gst);
                }else{
                    item.setSubTotalStr("");
                    item.setGstStr("");
                }
                items.add(item);
            }
        }
        prodVM.setItems(items);

        List<ReceiptProdVM> list = new ArrayList<ReceiptProdVM>();
        list.add(prodVM);
        receipt.setProds(list);

        receipt.setTotalStr(NvxNumberUtils.formatToCurrency(totalAmt));
        receipt.setGstStr(NvxNumberUtils.formatToCurrency(totalGst));
        if(count > 0){
            receipt.setOriginalMsg("DUPLICATE");
            receipt.setReprintedMsg("Reprint Count : "+count);
        }else{
            receipt.setOriginalMsg("ORIGINAL");
            receipt.setReprintedMsg("");
        }
        receipt.setDollarsInwords(TransactionUtil.convertToWordsWithDecimal(totalAmt));
        receipt.setTncs(new ArrayList<>());
        receipt.setOwnAddress(ownerAddr);
        receipt.setOwnContact(ownerCont);
        receipt.setPrintedDtTimeStr(NvxDateUtils.formatDateForDisplay(new Date(System.currentTimeMillis()), true));

        Long uuid = System.currentTimeMillis();

        boolean receiptPDF = false;
        String receiptName = null;
        String receiptPath = null;
        String receiptRootDir = paramSrv.getPartnerTransactionReceiptRootDir();
        if(receiptRootDir != null && receiptRootDir.trim().length() > 0){
            receiptName = "WOT-DAILY-RECEIPT-"+NvxDateUtils.formatDate(date, NvxDateUtils.WOT_TICKET_DATE_PRINT_FORMAT_DASH)+ "-" + (System.currentTimeMillis()) + FileUtil.FILE_EXT_PDF;
            receiptName = receiptName.toUpperCase();
            receiptPath = receiptRootDir + "/" + receiptName;
            try {
                if(isMonthly){
                    receiptPDF = tplSrv.generateWoTMonthlyReceiptReceiptPDF(receipt, receiptPath);
                }else{
                    receiptPDF = tplSrv.generateWoTDailyReceiptReceiptPDF(receipt, receiptPath);
                }
            } catch (Exception e) {
                if(isMonthly){
                    logger.error("[" + uuid + "] WoT monthly receipt not generated for deposit purchase due to exception : " + e.getMessage(), e);
                }else{
                    logger.error("[" + uuid + "] WoT daily receipt not generated for deposit purchase due to exception : " + e.getMessage(), e);
                }
            } catch (Throwable e) {
                if(isMonthly){
                    logger.error("["+uuid+"] WoT monthly receipt not generated for deposit purchase due to exception : "+e.getMessage(), e);
                }else{
                    logger.error("["+uuid+"] WoT daily receipt not generated for deposit purchase due to exception : "+e.getMessage(), e);
                }
            }
        }else{
            if(isMonthly){
                logger.error("["+uuid+"] Receipt not generated for WoT monthly due to PartnerTransactionReceiptRootDir not configured.");
            }else{
                logger.error("["+uuid+"] Receipt not generated for WoT daily due to PartnerTransactionReceiptRootDir not configured.");
            }
        }
        if(receiptPDF){
            if(log != null){
                log.setCount(log.getCount()+1);
                wotReceiptLogRepo.save(log);
            }else{
                log = new PPSLMWingsOfTimeReceiptLog();
                log.setCount(0);
                log.setDate(date);
                log.setType(isMonthly ? "Monthly" : "Daily");
                log.setMainAccountId(account.getMainAccount().getId());
                wotReceiptLogRepo.save(log);
            }
            return receiptPath;
        }else{
            return null;
        }
    }

    @Override
    public void sendingAdminWoTConfirmationEmail(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId) {
        try{
            WoTEmailVM vm = getWoTEmailVM(account.getProfile().getOrgName(), wot, "");
            vm = populateWoTEmailGeneralInfo(vm);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_ADMIN_WOT_CONFIRM);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null, subject, null, content, populateBackendReservationEmails(account, adminUserId), "ADMIN-WOT-CONFIRM-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    @Override
    public void sendingAdminWoTCancellationEmail(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId) {
        try{
            WoTEmailVM vm = getWoTEmailVM(account.getProfile().getOrgName(), wot, "");
            vm = populateWoTEmailGeneralInfo(vm);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_ADMIN_WOT_CANCEL);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null, subject, null, content, populateBackendReservationEmails(account, adminUserId), "ADMIN-WOT-CANCEL-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    @Override
    public void sendingAdminWoTAmendmentEmail(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId) {
        try{
            WoTEmailVM vm = getWoTEmailVM(account.getProfile().getOrgName(), wot, "");
            vm = populateWoTEmailGeneralInfo(vm);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_ADMIN_WOT_UPDATE);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null, subject, null, content, populateBackendReservationEmails(account, adminUserId), "ADMIN-WOT-UPDATE-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    @Override
    public void sendingAdminWoTReleaseEmail(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId, String releaseDate) {
        try{
            WoTEmailVM vm = getWoTEmailVM(account.getProfile().getOrgName(), wot, "");
            vm = populateWoTEmailGeneralInfo(vm);
            if(!(releaseDate != null && releaseDate.trim().length() > 0)){
                releaseDate = "&nbsp;";
            }
            vm.setBackendReservationReleaseDate(releaseDate);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_ADMIN_WOT_RELEASE);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null, subject, null, content, populateBackendReservationEmails(account, adminUserId), "ADMIN-WOT-RELEASE-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    @Override
    public void sendingAdminWoTPostReleaseEmailNotification(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId, String releaseDate) {
        try{
            WoTEmailVM vm = getWoTEmailVM(account.getProfile().getOrgName(), wot, "");
            vm = populateWoTEmailGeneralInfo(vm);
            if(!(releaseDate != null && releaseDate.trim().length() > 0)){
                releaseDate = "&nbsp;";
            }
            vm.setBackendReservationReleaseDate(releaseDate);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_ADMIN_WOT_POST_RELEASE_NOTIFICATION);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null, subject, null, content, populateBackendReservationEmails(account, adminUserId), "ADMIN-WOT-POST-RELEASE-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    private String[] populateBackendReservationEmails(PPSLMTAMainAccount account, String adminUserId) {
        List<String> emails = new ArrayList<String>();
        String partnerEmail = account.getProfile().getEmail();
        String mainAccEmail = account.getEmail();
        if(partnerEmail != null && partnerEmail.trim().length() > 0 && (!emails.contains(partnerEmail.trim()))){
            emails.add(partnerEmail.trim());
        }
        if(mainAccEmail != null && mainAccEmail.trim().length() > 0 && (!emails.contains(mainAccEmail.trim()))){
            emails.add(mainAccEmail.trim());
        }
        String accMgrId = account.getProfile().getAccountManagerId();
        if(accMgrId != null && accMgrId.trim().length() > 0){
            String email = adminSrv.getAdminUserEmailById(accMgrId);
            if(email != null && email.trim().length() > 0 && (!emails.contains(email.trim()))){
                emails.add(email.trim());
            }
        }
        if(adminUserId != null && adminUserId.trim().length() > 0){
            String email = adminSrv.getAdminUserEmailById(adminUserId);
            if(email != null && email.trim().length() > 0 && (!emails.contains(email.trim()))){
                emails.add(email.trim());
            }
        }
        return emails.toArray(new String[]{});
    }

    public void sendingWoTConfirmationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        try{
            WoTEmailVM vm = getWoTEmailVMWithGernalInfo(account, wot, barcodeImageString);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_WOT_CONFIRM);
            String subject = mail.getSubject();
            String content = mail.getBody();
            vm.setTicketBarcodeImageString(getBarcodePdfString(barcodeImageString));
            String pdfHtml = resolveEmailParameters(content, vm);
            vm.setTicketBarcodeImageString(getBarcodeEmailString(barcodeImageString));
            String emailHtml = resolveEmailParameters(content, vm);
            sendingEmail(true, wot.getReceiptNum(), wot.getPinCode(),subject, pdfHtml, emailHtml, populatePartnerEmails(account), "PARTNER-WOT-CONFIRM-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    public void sendingWoTAmendmentEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        try{
            WoTEmailVM vm = getWoTEmailVMWithGernalInfo(account, wot, barcodeImageString);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_WOT_AMENDMENT);
            String subject = mail.getSubject();
            String content = mail.getBody();
            vm.setTicketBarcodeImageString(getBarcodePdfString(barcodeImageString));
            String pdfHtml = resolveEmailParameters(content, vm);
            vm.setTicketBarcodeImageString(getBarcodeEmailString(barcodeImageString));
            String emailHtml = resolveEmailParameters(content, vm);
            sendingEmail(true, wot.getReceiptNum(), wot.getPinCode(),subject, pdfHtml, emailHtml, populatePartnerEmails(account), "PARTNER-WOT-AMENDMENT-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Amendment Email is failed due to "+ex.getMessage(), ex);
        }
    }

    public void sendingWoTSplitConfirmationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        try{
            WoTEmailVM vm = getWoTEmailVMWithGernalInfo(account, wot, barcodeImageString);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_WOT_SPLIT_CONFIRM);
            String subject = mail.getSubject();
            String content = mail.getBody();
            vm.setTicketBarcodeImageString(getBarcodePdfString(barcodeImageString));
            String pdfHtml = resolveEmailParameters(content, vm);
            vm.setTicketBarcodeImageString(getBarcodeEmailString(barcodeImageString));
            String emailHtml = resolveEmailParameters(content, vm);
            sendingEmail(true, wot.getReceiptNum(), wot.getPinCode(),subject, pdfHtml, emailHtml, populatePartnerEmails(account), "PARTNER-WOT-SPLIT-CONFIRM-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Split Confirmation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    public void sendingWoTCancellationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        try{
            WoTEmailVM vm = getWoTEmailVMWithGernalInfo(account, wot, barcodeImageString);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_WOT_CANCEL);
            String subject = mail.getSubject();
            String content = mail.getBody();
            vm.setTicketBarcodeImageString(getBarcodeEmailString(barcodeImageString));
            String emailHtml = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null,subject, null, emailHtml, populatePartnerEmails(account), "PARTNER-WOT-CANCEL-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Cancellation Email is failed due to "+ex.getMessage(), ex);
        }
    }

    private String getBarcodeEmailString(String barcodeImageString) {
        if(barcodeImageString != null && barcodeImageString.trim().length() > 0){
            return "<img width=\"300\" height=\"75\" src=\"cid:barcodeImageFileId\" />";
        }
        return "";
    }

    private String getBarcodePdfString(String barcodeImageString) {
        if(barcodeImageString != null && barcodeImageString.trim().length() > 0){
            return "<img width=\"300\" height=\"75\" src=\"data:image/png;base64,"+ barcodeImageString.trim() +"\" />";
        }
        return "";
    }

    public void sendingWoTCancellationReminderEmail(String orgName, String mainAccEmail, PPSLMWingsOfTimeReservation wot) {
        try{
            WoTEmailVM vm = getWoTEmailVM(orgName, wot, null);
            vm = populateWoTEmailGeneralInfo(vm);
            SysEmailTemplateVM mail = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, DefaultEmailTemplateService.CONTENT_PARTNER_WOT_CANCEL_REMINDER);
            String subject = mail.getSubject();
            String content = mail.getBody();
            content = resolveEmailParameters(content, vm);
            sendingEmail(false, null, null,subject, null, content, new String[]{mainAccEmail}, "PARTNER-WOT-CANCEL-REMINDER-EMAIL-"+wot.getItemId()+"-"+wot.getPinCode()+"-"+System.currentTimeMillis());
        }catch (Exception ex){
            logger.error("Sending WoT Cancellation Reminder Email is failed due to "+ex.getMessage(), ex);
        }
    }

    private void sendingEmail(boolean requiredPDF, String receiptNumber, String pinCode, String subject, String receiptHtmlContent, String emailContent, String[] emails, String trackingId) throws Exception {
        String receiptName = null;
        String receiptAttachmentName = null;
        String receiptPath = null;
        boolean receiptPDF = false;
        String uniqueTrackingId = (receiptNumber != null && receiptNumber.trim().length() > 0 ? receiptNumber.trim() : "") + "-"+trackingId;
        String receiptRootDir = paramSrv.getPartnerTransactionReceiptRootDir();
        if(requiredPDF && receiptRootDir != null && receiptRootDir.trim().length() > 0 && receiptNumber != null && receiptNumber.trim().length() > 0 && receiptHtmlContent != null && receiptHtmlContent.trim().length() > 0){
            String pdfHtml = tplSrv.generatePartnerWoTEmailHtml(WoTEmailHtmlGeneratorVM.getHtmlContentObject(getNonEmptyString(subject), getNonEmptyString(receiptHtmlContent)));
            receiptName = "WoT-"+receiptNumber.trim()+ "-" + (System.currentTimeMillis()) + FileUtil.FILE_EXT_PDF;
            receiptName = receiptName.toUpperCase();
            receiptAttachmentName = receiptNumber.trim().toUpperCase()+FileUtil.FILE_EXT_PDF;
            receiptPath = receiptRootDir + "/" + receiptName;
            receiptPDF = tplSrv.generatePdfByHtml(uniqueTrackingId, pdfHtml, receiptPath);
        }
        String barcodeFileName = null;
        String barcodeFileAttachmentName = null;
        String barcodeFilePath = null;
        boolean barcodeGenerated = false;
        if(pinCode != null && pinCode.trim().length() > 0){
            barcodeFileName = "WoTPinCode-"+receiptNumber.trim()+ "-" + (System.currentTimeMillis()) + FileUtil.IMG_EXT_PNG;
            barcodeFileName = barcodeFileName.toUpperCase();
            barcodeFileAttachmentName = receiptNumber.trim().toUpperCase() + FileUtil.IMG_EXT_PNG;
            barcodeFilePath = receiptRootDir + "/" + barcodeFileName;
            try{
                barcodeGenerated = BarcodeGenerator.toBase64(pinCode, 300, 75, barcodeFilePath);
            }catch (Exception ex){
                logger.error("Generating barcode failed : "+ex.getMessage() +" for "+trackingId, ex);
            }catch (Throwable ex){
                logger.error("Generating barcode failed : "+ex.getMessage() +" for "+trackingId, ex);
            }
        }

        MailProperties props = new MailProperties();
        props.setSubject(subject);
        props.setToUsers(emails);
        props.setBody(emailContent);
        props.setBodyHtml(true);
        props.setTrackingId(uniqueTrackingId);
        props.setSender(mailSrv.getDefaultSender());

        String[] fileNameArray = null;
        String[] filePathArray = null;
        String[] fileContentId = null;
        if(barcodeGenerated && receiptPDF){
            fileNameArray = new String[]{ receiptAttachmentName, barcodeFileAttachmentName };
            filePathArray = new String[]{ receiptPath, barcodeFilePath };
            fileContentId = new String[]{ "", "<barcodeImageFileId>"};
        }else if(barcodeGenerated){
            fileNameArray = new String[]{ barcodeFileAttachmentName };
            filePathArray = new String[]{ barcodeFilePath };
            fileContentId = new String[]{ "<barcodeImageFileId>"};
        }else if(receiptPDF){
            fileNameArray = new String[]{ receiptAttachmentName };
            filePathArray = new String[]{ receiptPath };
            fileContentId = new String[]{ "" };
        }

        if(requiredPDF || receiptPDF){
            props.setAttachFilePaths(filePathArray);
            props.setAttachNames(fileNameArray);
            props.setAttachFileContentTypes(fileContentId);
            props.setHasAttach(true);
        }else{
            props.setHasAttach(false);
        }
        try {
            logger.debug("mailService sending Email");
            mailSrv.sendEmail(props, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    logger.info("Finished sending email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private String[] populatePartnerEmails(PartnerAccount account) {
        String email1 = account.getEmail();
        String email2 = account.getMainAccount().getEmail();
        if(email1 != null && email2 != null){
            if(email1.equalsIgnoreCase(email2)){
                return new String[]{ email1 };
            }
            return new String[]{ email1, email2 };
        }
        if(email1 != null){
            return new String[]{ email1 };
        }else{
            return new String[]{ email2 };
        }
    }

    private WoTEmailVM getWoTEmailVMWithGernalInfo(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        WoTEmailVM vm = getWoTEmailVM(account.getPartner().getOrgName(), wot, barcodeImageString);
        vm = populateWoTEmailGeneralInfo(vm);
        return vm;
    }

    private WoTEmailVM populateWoTEmailGeneralInfo(WoTEmailVM vm) {
        SysEmailTemplateVM template = mailTplSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,DefaultEmailTemplateService.CONTENT_PARTNER_WOT_GENERAL_INFO);
        if(template != null && template.getBody() != null && template.getBody().trim().length() > 0){
            vm.setWotGeneralInfo(resolveEmailParameters(template.getBody(), vm));
        }else{
            vm.setWotGeneralInfo("&nbsp;");
        }
        return vm;
    }

    private String resolveEmailParameters(String html, WoTEmailVM vm) {
        String body = html;
        if(body == null || body.trim().length() == 0 || vm == null){
            return "";
        }
        if(isNonEmptyString(vm.getBaseUrl())){
            body = body.replace("{{baseUrl}}", vm.getBaseUrl());
        }
        if(isNonEmptyString(vm.getBaseAppUrl())){
            body = body.replace("{{baseAppUrl}}", vm.getBaseAppUrl());
        }
        if(isNonEmptyString(vm.getBaseAppLogoUrl())){
            body = body.replace("{{baseAppLogoUrl}}", vm.getBaseAppLogoUrl());
        }
        if(isNonEmptyString(vm.getPartnerName())){
            body = body.replace("{{partnerName}}", vm.getPartnerName());
        }
        if(isNonEmptyString(vm.getTicketEventPinCode())){
            body = body.replace("{{ticketEventPinCode}}", vm.getTicketEventPinCode());
        }
        if(isNonEmptyString(vm.getTicketBarcodeImageString())){
            body = body.replace("{{ticketBarcodeImageString}}", vm.getTicketBarcodeImageString());
        }
        if(isNonEmptyString(vm.getTicketEventDateForPrint())){
            body = body.replace("{{ticketEventDateForPrint}}", vm.getTicketEventDateForPrint());
        }
        if(isNonEmptyString(vm.getTicketEventCutoffTimeForPrint())){
            body = body.replace("{{ticketEventCutoffTimeForPrint}}", vm.getTicketEventCutoffTimeForPrint());
        }
        if(isNonEmptyString(vm.getTicketEventStartTime())){
            body = body.replace("{{ticketEventStartTime}}", vm.getTicketEventStartTime());
        }
        if(isNonEmptyString(vm.getTicketEventEndTime())){
            body = body.replace("{{ticketEventEndTime}}", vm.getTicketEventEndTime());
        }
        if(isNonEmptyString(vm.getTicketEventName())){
            body = body.replace("{{ticketEventName}}", vm.getTicketEventName());
        }
        if(isNonEmptyString(vm.getTicketQty())){
            body = body.replace("{{ticketQty}}", vm.getTicketQty());
        }
        if(isNonEmptyString(vm.getTicketStatus())){
            body = body.replace("{{ticketStatus}}", vm.getTicketStatus());
        }
        if(isNonEmptyString(vm.getTicketRemarks())){
            body = body.replace("{{ticketRemarks}}", vm.getTicketRemarks());
        }
        if(isNonEmptyString(vm.getWotGeneralInfo())){
            body = body.replace("{{wotGeneralInfo}}", vm.getWotGeneralInfo());
        }
        if(isNonEmptyString(vm.getBackendReservationReleaseDate())){
            body = body.replace("{{backendReservationReleaseDate}}", vm.getBackendReservationReleaseDate());
        }
        if(isNonEmptyString(vm.getTicketEventStartDateAndTime())){
            body = body.replace("{{ticketEventStartDateAndTime}}", vm.getTicketEventStartDateAndTime());
        }
        return body;
    }


    private WoTEmailVM getWoTEmailVM(String orgName, PPSLMWingsOfTimeReservation wot, String barcodeImageString) {
        WoTEmailVM vm = new WoTEmailVM();
        vm.setBaseUrl(paramSrv.getApplicationContextPath());
        vm.setBaseAppLogoUrl(paramSrv.getPartnerPortalSLMLogoPath());
        vm.setBaseAppUrl(paramSrv.getPartnerPortalSLMRootPath());
        vm.setPartnerName(getNonEmptyString(orgName));
        vm.setTicketEventPinCode(getNonEmptyString(wot.getPinCode()));
        vm.setTicketBarcodeImageString(getNonEmptyString(barcodeImageString));
        try{
            vm.setTicketEventDateForPrint(wot.getEventDate() != null ? NvxDateUtils.formatDate(wot.getEventDate(), NvxDateUtils.WOT_TICKET_DATE_PRINT_FORMAT) : "");
        }catch (Exception ex){
            vm.setTicketEventDateForPrint("");
            logger.error("- event date : "+ex.getMessage(), ex);
        }
        try{
            vm.setTicketEventCutoffTimeForPrint(wot.getEventCutOffTime() != null ? NvxDateUtils.formatDate(wot.getEventCutOffTime(), NvxDateUtils.WOT_TICKET_TIME_PRINT_FORMAT) : "");
        }catch (Exception ex){
            vm.setTicketEventStartTime("");
            logger.error("- event start time : "+ex.getMessage(), ex);
        }
        try{
            vm.setTicketEventStartDateAndTime(wot.getEventStartTime() != null ? NvxDateUtils.formatDate(new Date(wot.getEventDate().getTime() + wot.getEventStartTime() * 1000), NvxDateUtils.WOT_TICKET_DATE_TIME_PRINT_FORMAT) : "");
        }catch (Exception ex){
            vm.setTicketEventStartDateAndTime("");
            logger.error("- event start time : "+ex.getMessage(), ex);
        }
        try{
            vm.setTicketEventStartTime(wot.getEventStartTime() != null ? NvxDateUtils.formatDate(new Date(wot.getEventDate().getTime() + wot.getEventStartTime() * 1000), NvxDateUtils.WOT_TICKET_TIME_PRINT_FORMAT) : "");
        }catch (Exception ex){
            vm.setTicketEventStartTime("");
            logger.error("- event start time : "+ex.getMessage(), ex);
        }
        try{
            vm.setTicketEventEndTime(wot.getEventEndTime() != null ? NvxDateUtils.formatDate(new Date(wot.getEventDate().getTime() + wot.getEventEndTime() * 1000), NvxDateUtils.WOT_TICKET_TIME_PRINT_FORMAT) : "");
        }catch (Exception ex){
            vm.setTicketEventEndTime("");
            logger.error("- event end time : "+ex.getMessage(), ex);
        }
        vm.setTicketEventName(getNonEmptyString(wot.getEventName()));
        int wotQty = 0;
        if(wot.getQty() != null){
            wotQty = wot.getQty();
        }
        int wotRedeemed = 0;
        if(wot.getQtyRedeemed() != null){
            wotRedeemed = 0;
        }
        vm.setTicketQty(String.valueOf( wotQty - wotRedeemed >= 0 ? wotQty - wotRedeemed : 0));
        vm.setTicketStatus(WOTReservationStatus.valueOf(wot.getStatus()).displayName);
        vm.setTicketRemarks(getNonEmptyString(wot.getRemarks()));
        vm.setWotGeneralInfo("&nbsp;");
        if(wot.isAdminRequest()){
            vm.setBackendReservationReleaseDate("&nbsp;");
        }
        return vm;
    }

    private String getNonEmptyString(String orgName) {
        if(orgName != null && orgName.trim().length() > 0){
            return orgName;
        }
        return "";
    }

    private boolean isNonEmptyString(String str) {
        return str != null;
    }

}

class WoTEmailHtmlGeneratorVM{
    private String title;
    private String email;

    public static WoTEmailHtmlGeneratorVM getHtmlContentObject(String title, String email){
        WoTEmailHtmlGeneratorVM vm = new WoTEmailHtmlGeneratorVM();
        vm.setTitle(title);
        vm.setEmail(email);
        return vm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
