package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by lavanya on 8/11/16.
 */
public interface IReportService {
    public TransactionReportInitVM initTransPage(boolean isSysAdmin, Integer mainAccountId);

    public ReportResult<TransReportModel> generateTransReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception;

    public Workbook generateTransReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception;

    public TransactionReportInitVM initInventoryPage(boolean isSysAdmin, Integer mainAccountId);

    public ReportResult<InventoryReportModel> generateInventoryReport(ReportFilterVM filter, Integer skip, Integer take,
                                                                      String orderBy, boolean asc, boolean isCount) throws Exception;

    public Workbook generateInventoryReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception;

    public TransactionReportInitVM initPackagePage(boolean isSysAdmin, Integer mainAccountId);

    public ReportResult<PackageReportModel> generatePackageReport(ReportFilterVM filter, Integer skip, Integer take,
                                                                    String orderBy, boolean asc, boolean isCount) throws Exception;

    public Workbook generatePackageReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception;

    public List<PartnerVM> initExceptionPage();

    public ResultVM getExceptionReportPage(ExceptionRptFilterVM fliter,String sortField, String sortDirection, int page, int pageSize);

    public List<ExceptionRptDetailsVM> getExceptionReportList(ExceptionRptFilterVM fliter);
}
