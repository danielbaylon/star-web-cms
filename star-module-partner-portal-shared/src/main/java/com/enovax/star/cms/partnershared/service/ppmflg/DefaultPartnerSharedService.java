package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerExtVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerExtRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerRepository;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.PartnerExtAttrName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 9/9/16.
 */
@Service("PPMFLGIPartnerSharedService")
public class DefaultPartnerSharedService implements IPartnerSharedService{

    @Autowired
    private PPMFLGPartnerRepository paRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerAccountSharedService")
    private IPartnerAccountSharedService paAccSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;

    @Autowired
    private PPMFLGPartnerExtRepository paExtRepo;

    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService admSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerAXService")
    private IPartnerAXService paAxSrv;

    @Transactional(readOnly = true)
    public PPMFLGPartner getPartnerById(Integer partnerId) {
        if(partnerId == null || partnerId.intValue() == 0){
            return null;
        }
        return paRepo.findById(partnerId);
    }

    @Transactional
    public PPMFLGPartner getPartnerByMainAccountId(Integer mainAccountId) {
        PPMFLGTAMainAccount mainAccount = paAccSrv.getActivePartnerByMainAccountId(mainAccountId);
        if(mainAccount == null){
            return null;
        }
        return mainAccount.getProfile();
    }

    @Transactional(readOnly = true)
    public String getPartnerNameByMainAccountIdWithCache(Map<String, String> paNameMapping, Integer mainAccountId) {
        if(mainAccountId == null || mainAccountId.intValue() == 0){
            return null;
        }
        if(paNameMapping != null && paNameMapping.containsKey(mainAccountId.intValue()+"")){
            return paNameMapping.get(mainAccountId.intValue()+"");
        }
        PPMFLGTAMainAccount mainAccount = paAccSrv.getActivePartnerByMainAccountId(mainAccountId);
        if(mainAccount == null){
            return null;
        }
        if(paNameMapping != null){
            paNameMapping.put(mainAccountId.intValue()+"", mainAccount.getProfile().getOrgName());
            return paNameMapping.get(mainAccountId.intValue()+"");
        }else{
            return mainAccount.getProfile().getOrgName();
        }
    }

    @Override
    public PPMFLGPartner getPartnerByAccountCodeAndMainAccountId(Integer id, String accountCode) {
        return paRepo.findFirstByAdminIdAndAccountCode(id, accountCode);
    }

    @Transactional(readOnly = true)
    public Integer getPartnerIdByMainAccountIdWithCache(Map<Integer, Integer> paNameMapping, Integer mainAccountId) {
        if(mainAccountId == null || mainAccountId.intValue() == 0){
            return null;
        }
        if(paNameMapping != null && paNameMapping.containsKey(mainAccountId.intValue())){
            return paNameMapping.get(mainAccountId.intValue());
        }
        PPMFLGTAMainAccount mainAccount = paAccSrv.getActivePartnerByMainAccountId(mainAccountId);
        if(mainAccount == null){
            return null;
        }
        if(paNameMapping != null){
            paNameMapping.put(mainAccountId.intValue(), mainAccount.getProfile().getId());
            return paNameMapping.get(mainAccountId.intValue());
        }else{
            return mainAccount.getProfile().getId();
        }
    }

    @Transactional(readOnly = true)
    public Integer getWoTReservationCap(Integer id) throws Exception {
        if(id == null || id.intValue() == 0){
            return 0;
        }
        PPMFLGPartnerExt reservationCap = this.paExtRepo.findFirstByPartnerIdAndAttrNameAndStatus(id, PartnerExtAttrName.WoTReservationCap.code, GeneralStatus.Active.code);
        if(reservationCap != null && reservationCap.getAttrValue() != null && reservationCap.getAttrValue().trim().length() > 0){
            return Integer.parseInt(reservationCap.getAttrValue().trim());
        }
        throw new BizValidationException("Wings of Time reservation capacity configuration not found");
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public boolean resetAllPartnerReservationCap(String adminId){
        if(adminId == null || adminId.trim().length() == 0){
            return false;
        }
        List<String> roles = this.admSrv.getLoginAdminUserAssignedRights(adminId);
        if(roles == null || roles.size() == 0){
            return false;
        }
        List<PPMFLGPartnerExt> capList = paExtRepo.finAllWingsOfTimeReservationCapList();
        if(capList != null && capList.size() > 0){
            for(PPMFLGPartnerExt i : capList){
                i.setAttrValue(paramSrv.getPartnerWoTReservationCap().toString());
                paExtRepo.save(i);
            }
        }
        return true;
    }

    @Override
    public void selfUpdatePartnerProfile(PartnerAccount partnerAccount, PartnerVM partnerVM) throws BizValidationException {
        if(partnerAccount == null || partnerAccount.getId() == null || partnerAccount.getId().intValue() == 0){
            throw new BizValidationException("Partner Login details not found.");
        }
        PPMFLGPartner partner = paRepo.findById(partnerAccount.getId());
        partner.setContactPerson(partnerVM.getContactPerson());
        partner.setContactDesignation(partnerVM.getContactDesignation());
        partner.setMainDestinations(partnerVM.getMainDestinations());
        partner.setWebsite(partnerVM.getWebsite());
        partner.setMobileNum(partnerVM.getMobileNum());
        partner.setTelNum(partnerVM.getTelNum());
        partner.setFaxNum(partnerVM.getFaxNum());
        partner.setAddress(partnerVM.getAddress());
        partner.setPostalCode(partnerVM.getPostalCode());
        partner.setCity(partnerVM.getCity());
        paRepo.save(partner);

        List<PartnerExtVM> exts = partnerVM.getExtension();
        List<PPMFLGPartnerExt> extList = partner.getPartnerExtList();
        if(exts != null && exts.size() > 0 && extList != null && extList.size() > 0){
            for(PartnerExtVM e : exts){
                for(PPMFLGPartnerExt i : extList){
                    if(e != null && i != null && e.getAttrName() != null && i.getAttrName() != null && e.getAttrName().trim().equalsIgnoreCase(i.getAttrName().trim())){
                        i.setAttrValue(e.getAttrValue());
                        i.setStatus(GeneralStatus.Active.code);
                        paExtRepo.save(i);
                    }
                }
            }
        }
    }

    @Override
    public void syncSelfUpdatedProfileDetailsToAX(PartnerAccount partnerAccount) throws Exception {
        paAxSrv.syncSelfUpdatedPartnerProfileToAX(partnerAccount.getId());
    }

    @Transactional(rollbackFor = {BizValidationException.class, Exception.class, Throwable.class})
    public void selfUpdateProfileAndSyncProfileChangesToAX(PartnerAccount partnerAccount, PartnerVM partnerVM) throws Exception {
        this.selfUpdatePartnerProfile(partnerAccount, partnerVM);
        this.syncSelfUpdatedProfileDetailsToAX(partnerAccount);
    }

    @Override
    public String getActiveExtensionPropertyValue(String name, Integer id) {
        PPMFLGPartnerExt ext = paExtRepo.findFirstByPartnerIdAndAttrNameAndStatus(id, name, GeneralStatus.Active.code);
        if(ext != null){
            return ext.getAttrValue();
        }
        return null;
    }
}