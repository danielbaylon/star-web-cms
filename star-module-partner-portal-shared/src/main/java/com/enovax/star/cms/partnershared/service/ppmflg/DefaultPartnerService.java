package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.*;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.datamodel.ppmflg.tier.ProductTier;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ppmflg.IUserRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCapacityGroup;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTable;
import com.enovax.star.cms.commons.model.axstar.AxCustomerGroup;
import com.enovax.star.cms.commons.model.axstar.AxStarCustomer;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppmflg.PPMFLGPartnerExtSpecification;
import com.enovax.star.cms.commons.repository.specification.ppmflg.PPMFLGPartnerSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;
import com.enovax.star.cms.partnershared.constant.ppmflg.AdminUserRoles;
import com.enovax.star.cms.partnershared.constant.ppmflg.ApproverTypes;
import com.enovax.star.cms.partnershared.constant.ppmflg.ProductLevels;
import com.enovax.star.cms.partnershared.model.grid.PartnerFilter;
import com.enovax.star.cms.partnershared.model.grid.PartnerGridFilterVM;
import com.enovax.star.cms.partnershared.model.grid.ProdFilter;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import com.enovax.star.cms.partnershared.repository.ppmflg.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.repository.ppmflg.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.repository.ppmflg.IPartnerRepository;
import com.enovax.star.cms.partnershared.util.password.PasswordUtil;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.security.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@Service("PPMFLGIPartnerService")
public class DefaultPartnerService implements IPartnerService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerService.class);
    @Autowired
    @Qualifier("PPMFLGIPartnerRepository")
    IPartnerRepository paTierSrv;
    @Autowired
    PPMFLGPartnerRepository paRepo;
    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService adminService;
    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerDocumentService")
    private IPartnerDocumentService paDocSrv;
    @Autowired
    private PPMFLGPADocMappingRepository paDocMapRepo;
    @Autowired
    @Qualifier("PPMFLGICountryRegionsRepository")
    private ICountryRegionsRepository ctryRepo;
    @Autowired
    private PPMFLGPADocumentRepository paDocRepo;
    @Autowired
    private PPMFLGPaDistributionMappingRepository paDisMapRepo;
    @Autowired
    private PPMFLGApprovalLogRepository apprLogRepo;
    @Autowired
    @Qualifier("PPMFLGIPartnerEmailService")
    private IPartnerEmailService paEmailSrv;
    @Autowired
    private PPMFLGTAMainAccountRepository mainAccRepo;
    @Autowired
    @Qualifier("PPMFLGIReasonService")
    private IReasonService reasonSrv;
    @Autowired
    @Qualifier("PPMFLGIRevalidationFeeService")
    private IRevalidationFeeService revalidFeeSrv;
    @Autowired
    @Qualifier("PPMFLGIProductTierService")
    private IProductTierService tierService;
    @Autowired
    @Qualifier("PPMFLGIPartnerAdminProductService")
    private IPartnerAdminProductService prodSrv;
    @Autowired
    @Qualifier("PPMFLGIApprovalLogService")
    private IApprovalLogService appLogSrv;
    @Autowired
    private PPMFLGPPMFLGReasonLogMapRepository reasonLogRepo;
    @Autowired
    private PPMFLGTAAccessRightsGroupRepository taAccRightsGrpRepo;
    @Autowired
    private PPMFLGTAMainRightsMappingRepository taMainRightMapRepo;
    @Autowired
    private PPMFLGTARightsMappingRepository taRightMapRepo;
    @Autowired
    private PPMFLGPartnerExtRepository paExtRepo;
    @Autowired
    @Qualifier("PPMFLGIPartnerExclProdService")
    private IPartnerExclProdService paExclProdSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedAccountService")
    private IPartnerSharedAccountService partnerSharedAccSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerAXService")
    private IPartnerAXService paAxSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerAxDocService")
    private IPartnerAxDocService paAxDocSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerExtensionService")
    private IPartnerExtensionService paExtSrv;
    @Autowired
    private PPMFLGTASubAccountRepository subAccRepo;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedAccountService")
    private IPartnerSharedAccountService paAccSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerInventoryService")
    private IPartnerInventoryService inventoryService;
    @Autowired
    @Qualifier("PPMFLGILineOfBusinessRepository")
    private ILineOfBusinessRepository paTypeRepo;
    @Autowired
    private AxStarService axSrv;
    @Autowired
    @Qualifier("PPMFLGIUserRepository")
    private IUserRepository userRepo;
    @Autowired
    private AxChannelCustomerService axCustSrv;
    @Autowired
    private AxStarService axStarSrv;

    private String VERIFY_SUCCESS_LABEL = " verified for new registered partner ";
    private String VERIFY_REJECTED_LABEL = " rejected for new registered partner ";
    private String VERIFY_RESUBMIT_LABEL = " required resubmit for new registered partner ";
    private String APPROVED_SUCCESS_LABEL = " approved the register of ";
    private String APPROVED_PA_EDIT_LABEL = "approved the edit of ";
    private String REJECT_PA_EDIT_LABEL = "rejected the edit of ";

    private List<AxCustomerGroup> getCustomerGroups(){
        AxStarServiceResult<List<AxCustomerGroup>>  api = axSrv.apiCustomerGroupGet(StoreApiChannels.PARTNER_PORTAL_MFLG);
        if(api.isSuccess()){
            return api.getData();
        }
        return new ArrayList<AxCustomerGroup>();
    }

    private List<AxCapacityGroup> getCapacityGroups() {
        ApiResult<List<AxCapacityGroup>>  api = axCustSrv.getAxCapacityGroupList(StoreApiChannels.PARTNER_PORTAL_MFLG);
        if(api.isSuccess()){
            return api.getData();
        }
        return new ArrayList<AxCapacityGroup>();
    }

    @Override
    public ApiResult<List<PartnerVM>> getPartnersByPage(PartnerGridFilterVM filterVM) {
        //get all the partners
        // List<Partner> partners = partnerRepo.getPartnerbyPage(filterVM);
        //       int total = partnerRepo.getPartnerSize(filterVM);

        //     List<PartnerVM> partnerVms = new ArrayList<>();
        //   for(Partner partner:partners){
        //     partnerVms.add(new PartnerVM(partner));
        //}

        ApiResult<List<PartnerVM>> result = new ApiResult<>();
        // result.setData(partnerVms);
        //result.setTotal(total);
        return result;
    }

    private String getTierNameByTierId(String tierId) {
        if(tierId == null || tierId.trim().length() == 0){
            return null;
        }
        ProductTierVM vm = tierService.getTierVmById( PartnerPortalConst.Partner_Portal_Channel,tierId);
        if(vm == null){
            return null;
        }
        return vm.getName();
    }

    private void populatePartnerDistributionMapping(PartnerVM paVM) throws Exception {
        if(paVM != null && paVM.getDistributionMapping() != null){
            List<CountryVM> ctryVMs = ctryRepo.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
            for(PaDistributionMappingVM vm : paVM.getDistributionMapping()){
                if(vm != null && ctryVMs != null){
                    populatePartnerDistributionMapping(vm, ctryVMs);
                }
            }
        }
    }

    private void populatePartnerDistributionMapping(PaDistributionMappingVM vm, List<CountryVM> ctryVMs) throws Exception {
        if(vm == null){
            return;
        }
        vm.setAdminNm(adminService.getAdminUserNameByAdminId(vm.getAdminId()));
        if(ctryVMs != null && ctryVMs.size() != 0 && vm.getCountryId() != null && vm.getCountryId().trim().length() > 0){
            for(CountryVM i : ctryVMs){
                if(i != null && i.getCtyCode().equals(vm.getCountryId())){
                    vm.setCountryNm(i.getCtyName());
                    break;
                }
            }
        }
    }


    @Transactional(readOnly = true)
    public ApprovingPartnerVM getPendingApprovalPartnerDetailsByApprovalIdAndUserId(StoreApiChannels channel, String loginAdminId, Integer appId) throws Exception {
        if(appId == null || appId.intValue() == 0){
            return null;
        }
        ApprovingPartnerVM vm = getApprovalLogDetailsByApprovalLogId(channel, loginAdminId, appId);
        return vm;
    }

    @Override
    public String addExclProd(String paVmJson) {
        try {
            PartnerVM paVm = JsonUtil.fromJson(paVmJson, PartnerVM.class);
            PartnerExclProdMapping paExclProd = paExclProdSrv.saveExclProdbyPartner(PartnerPortalConst.Partner_Portal_Channel,paVm.getId(), paVm.getExcluProdIds());
        }catch(Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    private ApprovingPartnerVM getApprovalLogDetailsByApprovalLogId(StoreApiChannels channel, String adminId, Integer appId) throws Exception {
        PPMFLGApprovalLog approvalLog = apprLogRepo.findById(appId);
        if(approvalLog == null){
            return null;
        }
        PartnerVM partnerVM = getPartnerVMFromApprovalLog(channel, approvalLog);
        ApprovingPartnerVM vm = new ApprovingPartnerVM();
        vm.setPartnerVM(partnerVM);
        vm.setAppLogVM(new ApprovalLogVM(approvalLog));
        List<ReasonVM> reasonList = reasonSrv.populateActiveReasonVms();
        vm.setReasonVM(reasonList);
        List<ReasonVM> reasonVMs = populateApprovalLogReasonVMs(approvalLog, reasonList);
        partnerVM.setReasons(reasonVMs);
        partnerVM.setRemarks(approvalLog.getRemarks());
        if(approvalLog != null && approvalLog.getRemarks() != null && approvalLog.getRemarks().trim().length() > 0){
            partnerVM.setUserRemarks(approvalLog.getRemarks().trim());
            partnerVM.setHasUserRemarks(true);
        }
        vm.setAppLogReasonVM(reasonVMs);
        vm.setShowRemarks(true);
        vm.setCanApprove(false);
        List<String> list = adminService.getLoginAdminUserAssignedRights(adminId);
        if(list != null && (list.contains(AdminUserRoles.GeneralApproverRole.code) || list.contains(AdminUserRoles.GeneralApproverBackupRole.code))){
            vm.setCanApprove(true);
        }
        return vm;
    }

    private List<ReasonVM> populateApprovalLogReasonVMs(PPMFLGApprovalLog approvalLog, List<ReasonVM> reasonList) {
        List<ReasonVM> lst = null;
        if(approvalLog != null && reasonList != null && reasonList.size() > 0){
            List<PPMFLGReasonLogMap> iter = reasonLogRepo.findByLogId(approvalLog.getId());
            if(iter != null){
                lst = new ArrayList<ReasonVM>();
                for(PPMFLGReasonLogMap i : iter){
                    for(ReasonVM rvm : reasonList){
                        if(rvm.getId().intValue() == i.getReasonId().intValue()){
                            lst.add(rvm);
                            break;
                        }
                    }
                }
            }
        }
        return lst;
    }

    private PartnerVM getPartnerVMFromApprovalLog(StoreApiChannels channel, PPMFLGApprovalLog approvalLog) throws Exception {
        if(approvalLog == null || approvalLog.getCurrentValue() == null){
            return null;
        }
        PartnerHist paHist = NvxUtil.getObjFromXml(approvalLog.getCurrentValue(), PartnerHist.class);
        PartnerVM paVM = new PartnerVM(paHist, sysParamSrv.getApplicationContextPath());
        paVM.setAccountManager(adminService.getAdminUserNameByAdminId(paVM.getAccountManagerId()));
        paVM.setPaDocs(paDocSrv.getPartnerDocumentsByPartnerId(paVM.getId()));
        paVM.setCountryName(ctryRepo.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data, paVM.getCountryCode()));
        paVM.setOrgTypeName(paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data, paVM.getOrgTypeCode()));
        paVM.setRevalFeeStr(revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(channel, paVM.getAxAccountNumber(),paVM.getRevalFeeItemId()));
        ProductTier tier = tierService.getProductTier(PartnerPortalConst.Partner_Portal_Channel, paVM.getTierId());
        paVM.setTierLabel(tier != null ? tier.getName() : null);
        paVM.populateCustomerGroupName(getCustomerGroups());
        paVM.populateCapacityGroupName(getCapacityGroups());
        populatePartnerDistributionMapping(paVM);
        ProductGridFilterVM filter = new ProductGridFilterVM();
        ProdFilter prodFilter = new ProdFilter();
        prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
        filter.setProdFilter(prodFilter);
        paVM.setExcluProds(prodSrv.getProdVmsByProdIds(filter, paVM.getExcluProdIds(),true));
        return paVM;
    }

    @Transactional(readOnly = true)
    public PartnerVerificationDetailsVM getPartnerVerificationDetailsViewModel(StoreApiChannels channel, Integer paId) throws BizValidationException {
        PPMFLGPartner partner = paRepo.findById(paId);
        if(partner == null){
            return null;
        }
        if(!com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus.PendingVerify.code.equals(partner.getStatus())){
            throw new BizValidationException("Partner profile is not in Pending Verify status.");
        }

        try{
            paAxDocSrv.downloadPartnerDocuments(partner, paDocMapRepo.findByPartnerId(paId));
        }catch (Exception ex){
            throw new BizValidationException("Downloading partner documents from AX FTP server is failed.");
        }

        List<CountryVM> ctyVMs = ctryRepo.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        List<PartnerTypeVM> paTypes = paTypeRepo.getPartnerTypeList(PartnerPortalConst.Partner_Portal_Ax_Data);
        PartnerVM partnerVM = new PartnerVM(partner);
        partnerVM.setTierId(tierService.getTierIdByPartnerId(PartnerPortalConst.Partner_Portal_Channel, partner.getId()));
        partnerVM.populateProeprtyName(ctyVMs, paTypes);
        partnerVM.setPaDocs(paDocSrv.getPartnerDocumentsByPartnerId(paId));
        partnerVM.populateCustomerGroupName(getCustomerGroups());
        partnerVM.populateCapacityGroupName(getCapacityGroups());
        List<AdminAccountVM> adminAccountVms = adminService.getPartnerMaintenanceAdminAccountList();
        List<RevalFeeItemVM> revalFeeItemVMs = revalidFeeSrv.populdateActiveRevalidationFeeItemVMs(channel, partnerVM.getAxAccountNumber());
        List<ReasonVM> reasonVms = reasonSrv.populateActiveReasonVms();
        List<ProductTierVM> tierVMs = tierService.getAllTierVms(PartnerPortalConst.Partner_Portal_Channel);
        List<AxCustomerGroup> custGrpList = getCustomerGroups();
        List<AxCapacityGroup> capacityGrpList = getCapacityGroups();

        int ticketRevalidatePeriod = sysParamSrv.getTicketRevalidationPeriod();

        PartnerVerificationDetailsVM vm = new PartnerVerificationDetailsVM(
                partnerVM, adminAccountVms, revalFeeItemVMs, tierVMs, reasonVms, ctyVMs, tierVMs, ticketRevalidatePeriod, custGrpList, capacityGrpList);
        return vm;
    }

    @Transactional(readOnly = true)
    public PartnerMaintenanceVM getPartnerMaintenanceViewModel(StoreApiChannels channel, Integer paId, String adminId) throws Exception {
        PartnerVM partnerVM = getPartnerVmById(paId);
        List<PartnerTypeVM> partnerTypeVMs = paTypeRepo.getPartnerTypeList(PartnerPortalConst.Partner_Portal_Ax_Data);
        List<AdminAccountVM> adminAccountVMs = adminService.getPartnerMaintenanceAdminAccountList();
        List<RevalFeeItemVM> revalFeeItemVMs = revalidFeeSrv.populdateActiveRevalidationFeeItemVMs(channel, partnerVM.getAxAccountNumber());
        List<ProductTierVM> tierVMs = tierService.getAllTierVms(PartnerPortalConst.Partner_Portal_Channel);
        List<CountryVM> ctyVMs = ctryRepo.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        partnerVM.populateProeprtyName(ctyVMs,partnerTypeVMs);
        partnerVM.populateCustomerGroupName(getCustomerGroups());
        partnerVM.populateCapacityGroupName(getCapacityGroups());
        List<DropDownVM> partnerDocTypes = PartnerDocType.getInUseDocTypes();
        boolean isPending = appLogSrv.hasPendingRequest(paId.toString(),ApproverCategory.Partner.code);
        List<AxCustomerGroup> custGrpList = getCustomerGroups();
        List<AxCapacityGroup> capacityGroupList = getCapacityGroups();
        int ticketRevalidatePeriod = sysParamSrv.getTicketRevalidationPeriod();

        PartnerMaintenanceVM vm = new PartnerMaintenanceVM(partnerVM, partnerTypeVMs,
                adminAccountVMs, revalFeeItemVMs, ctyVMs,
                ticketRevalidatePeriod, isPending, partnerDocTypes, tierVMs, custGrpList, capacityGroupList);
        return vm;
    }

    private List<PartnerExtVM> populateCompleteExtensionPropertiesSet(PartnerVM paVM, PPMFLGPartner pa) {
        List<PartnerExtVM> paExtVMList = paVM.getExtension();
        List<PPMFLGPartnerExt> paExtList = pa.getPartnerExtList();
        boolean found;
        for(PPMFLGPartnerExt paExt : paExtList) {
            found = false;
            for(PartnerExtVM paExtVM : paExtVMList) {
                if(paExt.getAttrName().equalsIgnoreCase(paExtVM.getAttrName())) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                PartnerExtVM tempPartnerExtVM = new PartnerExtVM(paExt);
                paExtVMList.add(tempPartnerExtVM);
            }
        }
        return paExtVMList;
    }

    @Override
    @Transactional
    public String submitEditPa(String paVmJson, String userId) throws Exception {
        if(paVmJson == null || paVmJson.trim().length() == 0){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        PartnerVM paVm = JsonUtil.fromJson(paVmJson, PartnerVM.class);
        if(paVm.getId() == null || paVm.getId().intValue() == 0){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        PPMFLGPartner partner = paRepo.findById(paVm.getId());
        if(partner == null){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        if(partner.getStatus() == null || partner.getStatus().trim().length() == 0){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        List<PartnerExtVM> partnerExtVMList = populateCompleteExtensionPropertiesSet(paVm,partner);
        paVm.setExtension(partnerExtVMList);
        PartnerHist paHist = this.preparePartnerHistWithExclProdandTier(paVm.getId());
        String compareRes = this.comparePaHist(paHist, paVm);
        log.debug("compareRes:"+compareRes);
        if(compareRes == null){
            return "No change was detected.";
        }

        String prePaXml = NvxUtil.getXmlfromObj(paHist);
        PartnerHist curPaHist = paHist;
        curPaHist.setAccountManagerId(paVm.getAccountManagerId());
        curPaHist.setSubAccountEnabled(paVm.getSubAccountEnabled());
        curPaHist.setRevalPeriodMonths(paVm.getRevalPeriodMonths());
        log.debug("paVm.getRevalFeeItemId():"+paVm.getRevalFeeItemId());
        curPaHist.setRevalFeeItemId(paVm.getRevalFeeItemId());
        final DecimalFormat df = new DecimalFormat(NvxNumberUtils.DEFAULT_SYSPARAM_DECIMAL_FORMAT);
        String capStr= paVm.getDailyTransCapStr().replaceAll(",", "").trim();
        Number newCap = df.parse(capStr);
        curPaHist.setDailyTransCap(new BigDecimal(newCap.floatValue()));
        curPaHist.setTierId(paVm.getTierId());
        curPaHist.setExcluProdIds(paVm.getExcluProdIds());
        curPaHist.setRemarks(paVm.getRemarks());
        curPaHist.setExtension(paVm.getExtension());
        List<PaDistributionMappingVM> distributionMapping = paVm.getDistributionMapping();
        List<PaDistributionMappingHist> pdMaps = new ArrayList<PaDistributionMappingHist>();
        for(PaDistributionMappingVM tmpObj:distributionMapping){
            pdMaps.add(new PaDistributionMappingHist(tmpObj));
        }
        curPaHist.setDistributionMapping(pdMaps);
        String curPaXml = NvxUtil.getXmlfromObj(curPaHist);

        this.savePaEidtPendingHist(paHist, curPaXml, prePaXml, compareRes, userId);
        return null;
    }

    private void savePaEidtPendingHist(PartnerHist paHist, String curPaXml,
                                       String prePaXml, String compareRes, String userNm) throws Exception {
        PPMFLGApprovalLog appLog = this.savePaEidtPendingHistNoEmail(paHist, curPaXml, prePaXml, compareRes, userNm);
        paEmailSrv.sendingPartnerProfileVerifingEmail(appLog);
    }

    private PPMFLGApprovalLog savePaEidtPendingHistNoEmail(PartnerHist paHist, String curPaXml,
                                                          String prePaXml, String compareRes, String userNm) throws Exception {
        PPMFLGApprovalLog appLog = new PPMFLGApprovalLog();
        appLog.setRelatedKey(paHist.getId().toString());
        appLog.setCurrentValue(curPaXml);
        appLog.setPreviousValue(prePaXml);
        appLog.setActionType(ActionType.Update.code);
        appLog.setStatus(FlowStatus.Pending.code);
        appLog.setCategory(ApproverCategory.Partner.code);
        appLog.setApproverType(ApproverTypes.General.code);
        appLog.setCreatedBy(userNm);
        appLog.setCreatedDate(new Date());
        appLog.setDescription(compareRes);
        appLog.setRemarks(paHist.getRemarks());
        this.apprLogRepo.save(appLog);
        return appLog;
    }

    private String SEPERATOR = "; ";
    private String FULLSTOP = ".";
    private String COMMA = ", ";
    private String comparePaHist(PartnerHist paHist, PartnerVM paVm){
        String compareRes = "";
        if(paVm.getAccountManagerId()!=null&&!paVm.getAccountManagerId().equals(paHist.getAccountManagerId())){
            User acc = this.userRepo.getUserById(paVm.getAccountManagerId());
            compareRes = compareRes + " changed AccountManager to "+acc.getName();
        }

        if(!paVm.getSubAccountEnabled().equals(paHist.getSubAccountEnabled())){
            if(paVm.getSubAccountEnabled()){
                if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
                compareRes = compareRes + " enabled SubAccount ";
            }else{
                if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
                compareRes = compareRes + " disabled SubAccount ";
            }
        }
        if(paVm.getRevalPeriodMonths()!=null&&!paVm.getRevalPeriodMonths().equals(paHist.getRevalPeriodMonths())){
            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
            compareRes = compareRes + " changed Revalidation Period to "+paVm.getRevalPeriodMonths()+" month(s)";
        }
        if(paVm.getRevalFeeItemId()!=null&&!paVm.getRevalFeeItemId().equals(paHist.getRevalFeeItemId())){
            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
            String revalFeeStr  = this.revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG,paVm.getAxAccountNumber(),paVm.getRevalFeeItemId());
            compareRes = compareRes + " changed Revalidation Fee to "+revalFeeStr;
        }
        String cap = TransactionUtil.priceWithDecimal(paHist.getDailyTransCap(), "");
        if(paVm.getDailyTransCapStr()!=null&&!paVm.getDailyTransCapStr().equals(cap)){
            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
            compareRes = compareRes + " changed daily Transaction to "+paVm.getDailyTransCapStr();
        }
        if(paVm.getTierId()!=null&&!paVm.getTierId().equals(paHist.getTierId())){
            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}

            ProductTier tier = this.tierService.getProductTier(PartnerPortalConst.Partner_Portal_Channel,paVm.getTierId());
            compareRes = compareRes + " changed Tier Level to "+tier.getName();
        }

        List<String> l1 = paVm.getExcluProdIds();
        List<String> l2 =  paHist.getExcluProdIds();
        if(!NvxUtil.compareList(l1,l2)){
            if(paVm.getExcluProdIds()== null || paVm.getExcluProdIds().size() == 0){
                if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
                compareRes = compareRes + " removed all Exclusive Item";
            }else{
                if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
                compareRes = compareRes + " changed Exclusive Item to ";
                ProductGridFilterVM gridFilterVM = new ProductGridFilterVM();
                ProdFilter prodFilter = new ProdFilter();
                prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
                prodFilter.setProductLevel(ProductLevels.Exclusive.toString());
                gridFilterVM.setProdFilter(prodFilter);
                List<PartnerProductVM> productVMs = prodSrv.getProdVmsByProdIds(gridFilterVM, paVm.getExcluProdIds(), true);
                for(PartnerProductVM prodVM:productVMs){
                    compareRes = compareRes + " " + prodVM.getName() + COMMA;
                }
            }
        }
        List<PaDistributionMappingHist> newDistributionMappingHist = new ArrayList<PaDistributionMappingHist>();
        List<PaDistributionMappingVM> newDistributionMapping = paVm.getDistributionMapping();
        for(PaDistributionMappingVM pdMap:newDistributionMapping){
            newDistributionMappingHist.add(new PaDistributionMappingHist(pdMap));
        }
        List<PaDistributionMappingHist> oldDistributionMappingHist = paHist.getDistributionMapping();
        if(!NvxUtil.compareList(newDistributionMappingHist,oldDistributionMappingHist)){
            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
            compareRes = compareRes + " changed Market Distribution Country";
        }

        List<PartnerExtVM> newExtList = paVm.getExtension();
        List<PartnerExtVM> oldExtList = paHist.getExtension();
        if(!NvxUtil.compareList(newExtList,oldExtList)){
//            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
            for(PartnerExtVM oldExt:oldExtList) {
                for(PartnerExtVM newExt:newExtList) {
                    if(oldExt.getAttrName().equalsIgnoreCase(newExt.getAttrName())) {
                        if(!oldExt.getAttrValue().equals(newExt.getAttrValue())) {
                            if(!"".equals(compareRes)){compareRes=compareRes+SEPERATOR;}
                            if(newExt.getAttrValue()!= null && !newExt.getAttrValue().equalsIgnoreCase("")) {
                                compareRes = compareRes + " changed " + oldExt.getAttrName() + " to " + newExt.getAttrValue();
                            }
                            else {
                                compareRes = compareRes + " removed " + oldExt.getAttrName();
                            }
                        }
                        break;
                    }
                }
            }
            //compareRes = compareRes + " changed Extension Property";
        }
        if(!"".equals(compareRes)){return compareRes+ " for "+paHist.getOrgName()+FULLSTOP;}
        return null;
    }


    @Override
    public List<AdminAccountVM> getAssignedSalesAdminByCountryId(String ctyId) {
        List<AdminAccountVM> lst = null;
        if(ctyId != null && ctyId != ""){
            lst = adminService.getAssignedSalesAdminByCountryId(ctyId);
        }
        if(lst == null){
            lst = new ArrayList<AdminAccountVM>();
        }
        return lst;
    }

    @Override
    public String revertPartnerProfileResubmit(Integer id, String reasonVmJson, String userId) throws Exception {
        return updateRevertPartnerProfileReject(id, reasonVmJson, PartnerStatus.PendingReSubmit.code, ActionType.Resubmit, userId, VERIFY_RESUBMIT_LABEL);
    }

    @Override
    public String apiRevertPartnerProfileReject(Integer id, String reasonVmJson, String userId) throws Exception {
        return updateRevertPartnerProfileReject(id, reasonVmJson, PartnerStatus.Rejected.code, ActionType.Reject, userId, VERIFY_REJECTED_LABEL);
    }

    @Transactional(rollbackFor = Exception.class)
    public String apiSubmitPartnerProfile(String paVmJson, String userId) throws Exception {
        if(paVmJson == null || paVmJson.trim().length() == 0){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        PartnerVM paVm = JsonUtil.fromJson(paVmJson, PartnerVM.class);
        if(paVm.getId() == null || paVm.getId().intValue() == 0){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        PPMFLGPartner partner = paRepo.findById(paVm.getId());
        if(partner == null){
            return "INVALID_REQUEST_INVALID_PA_PROFILE";
        }
        if(partner.getStatus() == null || partner.getStatus().trim().length() == 0){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        if(!PartnerStatus.PendingVerify.code.equalsIgnoreCase(partner.getStatus().trim())){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        this.saveVerifiedPartnerProfileDetails(partner, paVm, userId);
        this.savePartnerDistributionMapping(partner, paVm, userId);
        this.savePartnerSubAccountCfg(partner, paVm, userId);
        PartnerHist paHist = preparePartnerHist(partner.getId());
        paHist.setTierId(paVm.getTierId());
        paHist.setExcluProdIds(paVm.getExcluProdIds());
        paHist.setRemarks(paVm.getRemarks());
        PPMFLGApprovalLog appLog = this.saveApprovalLog(partner, paHist, userId, ActionType.Approve, ApproverCategory.Partner, ApproverTypes.General, paVm.getRemarks(), FlowStatus.Pending.code, VERIFY_SUCCESS_LABEL);
        this.paEmailSrv.sendingPartnerProfileVerifingEmail(appLog);
        return null;
    }

    String UPLOAD_DOC = " uploaded a new document ";
    @Override
    @Transactional(rollbackFor = Exception.class)
    public PartnerDocumentVM savePartnerDocument(int paId, Document doc, String paFileType, String fileName, String userName) throws  Exception{
        PartnerHist paHist = this.preparePartnerHist(paId);
        String prePaXml = NvxUtil.getXmlfromObj(paHist);
        PPMFLGPartner partner = getPartnerById(paId);
        PPMFLGPADocument paDoc = paDocSrv.savePartnerDocuments(partner.getMainAccount(),partner,doc,paFileType);
        PartnerDocumentVM paDocVM=null;
        PPMFLGPADocMapping paDocMap = paDocMapRepo.findByPartnerIdAndDocId(paId,paDoc.getId());
        List<PPMFLGPADocMapping> docMapList = new ArrayList<>();
        docMapList.add(paDocMap);
        if(paDoc != null) {
            paAxDocSrv.syncPartnerDocuments(partner, docMapList);
            updatePartnerDocInAX(partner,paHist);
            saveGeneralLog(paId, userName, UPLOAD_DOC + fileName, prePaXml);
            paDocVM = new PartnerDocumentVM(paDoc);
        }
        return paDocVM;
    }

    private void updatePartnerDocInAX(PPMFLGPartner partner, PartnerHist paHist) throws Exception{
        ApiResult<AxCustomerTable> axCustomerTableGetApiResult = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG,partner.getAxAccountNumber());
        if(!axCustomerTableGetApiResult.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axCustomerTableGetApiResult));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }
        String capacityGroupId = "";
        List<PartnerExtVM> exts = paHist.getExtension();
        if(exts != null && exts.size() > 0){
            for(PartnerExtVM i : exts){
                if(i == null || i.getAttrName() == null){
                    continue;
                }
                if("capacityGroupId".equals(i.getAttrName())){
                    capacityGroupId = i.getAttrValue();
                }
            }
        }
        AxCustomerTable axCustomerTable = axCustomerTableGetApiResult.getData();
        boolean allowOnAccount = sysParamSrv.getPartnerRegistrationAllowOnAccount();
        axCustomerTable.setAccountNum(partner.getAxAccountNumber());
        axCustomerTable.setAccountMgrCMS(paHist.getAccountManagerId());
        axCustomerTable.setCountryCode(paHist.getCountryCode());
        axCustomerTable.setAllowOnAccount(allowOnAccount?1:0);
        axCustomerTable.setDesignation(partner.getContactDesignation());
        List<PPMFLGPADocMapping> docMap = paDocMapRepo.findByPartnerId(partner.getId());
        StringBuilder documentLink = new StringBuilder();
        String documnetTypeId = sysParamSrv.getPartnerRegistrationAxDocumentTypeId();
        if(docMap != null && docMap.size() > 0){
            for(PPMFLGPADocMapping dm : docMap){
                PPMFLGPADocument d = dm.getPaDoc();
                if(d != null){
                    if(documentLink.length() > 0){
                        documentLink.append("|");
                    }
                    documentLink.append(d.getFileName()).append(";").append(documnetTypeId).append(";").append("ftp://example.host.com/partner-portal-mflg/"+d.getFilePath());
                }
            }
        }
        if(documentLink != null && documentLink.length() > 0){
            axCustomerTable.setDocumentLinkDelimited(documentLink.toString());
        }
        axCustomerTable.setFax(partner.getFaxNum());
        List<PaDistributionMappingHist> marketDistributionList = paHist.getDistributionMapping();
        StringBuilder market = new StringBuilder();
        if(marketDistributionList != null && marketDistributionList.size() > 0){
            for(PaDistributionMappingHist i : marketDistributionList){
                if(market.length() > 0){
                    market.append("|");
                }
                market.append(i.getCountryId()).append(";").append(i.getPercentage()).append(";").append(i.getAdminId());
            }
        }
        if(market.length() > 0) {
            axCustomerTable.setMarketDistributionDelimited(market.toString());
        }
        String revalFeeStr = revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG,"",paHist.getRevalFeeItemId());
        Double d = Double.parseDouble(revalFeeStr);
        axCustomerTable.setRevalidationFee(paHist.getRevalFeeItemId());
        axCustomerTable.setRevalidationPeriod(paHist.getRevalPeriodMonths());
        axCustomerTable.setDailyTransCapValue(paHist.getDailyTransCap().doubleValue());
        axCustomerTable.setRegisteredDate(partner.getCreatedDate());

        if(partner.getUen() != null && partner.getUen().trim().length() > 0){
            axCustomerTable.setUenNumber(partner.getUen());
        }
        if(partner.getLicenseNum() != null){
            axCustomerTable.setTaLicenseNumber(partner.getLicenseNum());
        }
        if(partner.getLicenseExpDate() != null){
            axCustomerTable.setTaExpiryDate(NvxDateUtils.parseDate(partner.getLicenseExpDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        if(partner.getBranchName() != null && partner.getBranchName().trim().length() > 0){
            axCustomerTable.setBranchName(partner.getBranchName());
        }

        if(capacityGroupId != null && capacityGroupId.trim().length() > 0){
            axCustomerTable.setCapacityGroupId(capacityGroupId);
        }

        if(partner.getTelNum() != null && partner.getTelNum().trim().length() > 0){
            axCustomerTable.setOfficeNo(partner.getTelNum());
        }

        axCustomerTable.setEnablePartnerPortal(axCustomerTable.getEnablePartnerPortal());
        PPMFLGTAMainAccount mainAccount = partner.getMainAccount();

        axCustomerTable.setMainAccLoginName(mainAccount.getUsername());
        axCustomerTable.setReservationAmendment(0);
        ApiResult<Boolean> axUpdateApi = axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG,axCustomerTable,axCustomerTable.getEnablePartnerPortal()==1);
        if(!axUpdateApi.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axUpdateApi));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }
    }

    @Override
    @Transactional
    public com.enovax.star.cms.commons.model.api.ApiResult<String> saveSubuser(int paId, boolean isNew, Integer userId, String userName, String name, String email, String title, String status, String access, String createdBy) throws Exception{
        PPMFLGPartner pa = paRepo.findById(paId);
        Integer mainAccId = pa.getMainAccount().getId();
        if(isNew) {
            userName = pa.getAccountCode() + "_" + userName;
        }
        if(isNew && paAccSrv.getUser(userName, false)!= null) {
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.SubAccountAlreadyExists);
        }
        String[] s;
        if(access==null || access.isEmpty()){
            s = new String[0];
        }else{
            s = access.split(",");
        }
        if(paAccSrv.exceedSubUsers(mainAccId, userId)){
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.MaxSubAccountExceeded);
        }
        String pwd = PasswordUtil.generatePswd();
        String encodedPwd = PasswordUtil.encode(pwd);
        final UserResult result = paAccSrv.saveSubUser(isNew, userId, userName, pwd, encodedPwd, email, s, status, createdBy, name, mainAccId, title);

        List<PPMFLGTASubAccount> accounts =  paAccSrv.getSubUsers(pa.getMainAccount(),true);

        List<PartnerAccountVM> accVms = new ArrayList<PartnerAccountVM>();
        for(PPMFLGTASubAccount acc:accounts){
            //Hibernate.initialize(acc.getRightsMapping());
            PPMFLGTASubAccount tmpObj = paAccSrv.getSubUser(acc.getId(), true);
            ArrayList<Integer> rightsIds = new ArrayList<Integer>();
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(tmpObj);
            List<PPMFLGTARightsMapping> rightsMappingList = taRightMapRepo.findBySubUser(tmpObj);
            if(rightsMappingList!=null){
                for(PPMFLGTARightsMapping temp: rightsMappingList){
                    PPMFLGTAAccessRightsGroup group = temp.getAccessRightsGroup();
                    rightsIds.add(group.getId());
                }
            }
            partnerAccountVM.setRightsIds(rightsIds);
            accVms.add(partnerAccountVM);
        }
        String accountsJson = JsonUtil.jsonify(accVms);
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", accountsJson);
    }

    private void savePartnerSubAccountCfg(PPMFLGPartner partner, PartnerVM paVm, String userId) {
        if(partner == null){
            return;
        }
        PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
        if(mainAcc!=null){
            mainAcc.setSubAccountEnabled(paVm.getSubAccountEnabled());
            mainAccRepo.save(mainAcc);
        }
    }


    private void savePartnerDistributionMapping(PPMFLGPartner partner, PartnerVM paVm, String userId) {
        if(paVm.getDistributionMapping() !=null){
            for( PaDistributionMappingVM tmpObj:paVm.getDistributionMapping()){
                PPMFLGPaDistributionMapping pdMap = new PPMFLGPaDistributionMapping();
                pdMap.setAdminId(tmpObj.getAdminId());
                pdMap.setCountryId(tmpObj.getCountryId());
                pdMap.setPartnerId(partner.getId());
                pdMap.setPercentage(tmpObj.getPercentage());
                paDisMapRepo.save(pdMap);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    private void saveVerifiedPartnerProfileDetails(PPMFLGPartner partner, PartnerVM paVm, String userId) throws Exception {
        partner.setStatus(PartnerStatus.Pending.code);
        partner.setAccountManagerId(paVm.getAccountManagerId());
        partner.setRevalFeeItemId(paVm.getRevalFeeItemId());
        partner.setRevalPeriodMonths(paVm.getRevalPeriodMonths());
        partner.setDailyTransCap(getDailyTransCap(paVm.getDailyTransCapStr()));
        partner.setModifiedBy(userId);
        partner.setModifiedDate(new Date(System.currentTimeMillis()));
        partner.setTierId(paVm.getTierId());
        paRepo.save(partner);
        List<PartnerExtVM> extVMs = paVm.getExtension();
        if(extVMs != null && extVMs.size() > 0){
            PPMFLGPartnerExt paExt = null;
            for(PartnerExtVM i : extVMs){
                if(i == null || i.getAttrName() == null || i.getAttrName().trim().length() == 0){
                    throw new Exception("Invalid partner details found");
                }
                paExt = paExtRepo.findFirstByPartnerIdAndAttrName(partner.getId(), i.getAttrName());
                if(paExt == null){
                    paExt = new PPMFLGPartnerExt();
                }
                paExt.setPartner(partner);
                paExt.setPartnerId(partner.getId());
                paExt.setDescription(i.getDescription());
                paExt.setMandatoryInd(i.getMandatoryInd());
                paExt.setAttrValueType(i.getAttrValueType());
                paExt.setAttrValueFormat(i.getAttrValueFormat());
                paExt.setApprovalRequired(i.getApprovalRequired());
                paExt.setStatus(GeneralStatus.Active.code);
                paExt.setAttrName(i.getAttrName());
                paExt.setAttrValue(i.getAttrValue() != null && i.getAttrValue().trim().length() > 0 ? i.getAttrValue().trim() : "");
                partner.getPartnerExtList().add(paExt);
                paExtRepo.save(paExt);
            }
            paRepo.save(partner);
        }
    }

    private BigDecimal getDailyTransCap(String dailyTransCapStr) throws ParseException {
        if(dailyTransCapStr == null || dailyTransCapStr.trim().length() == 0){
            return null;
        }
        return NvxNumberUtils.parseBigDecimal(dailyTransCapStr, NvxNumberUtils.DEFAULT_SYSPARAM_DECIMAL_FORMAT);
    }

    @Transactional
    private String updateRevertPartnerProfileReject(Integer id, String reasonVMJson, String status,
                                                    ActionType actionType, String userId, String label) throws Exception {
        if(id == null || id.intValue() == 0){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        if(reasonVMJson == null || reasonVMJson.trim().length() == 0) {
            return "INVALID_REQUEST_NO_REASON";
        }
        if(status == null || status.trim().length() == 0){
            return "INVALID_REQUEST_STATUS";
        }
        ReasonVM reasonVm = JsonUtil.fromJson(reasonVMJson, ReasonVM.class);
        if(reasonVm == null || reasonVm.getReasonIds() == null || reasonVm.getReasonIds().size() == 0 ||
                reasonVm.getRemarks() == null || reasonVm.getRemarks().trim().length() == 0){
            return "INVALID_REQUEST_NO_REASON";
        }
        PPMFLGPartner partner = this.paRepo.findById(id);
        if(partner == null){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        if(partner.getStatus() == null || partner.getStatus().trim().length() == 0){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        if(!PartnerStatus.PendingVerify.code.equalsIgnoreCase(partner.getStatus().trim())){
            return "INVALID_REQUEST_INVALID_PA_ID";
        }
        // update partner status
        partner.setStatus(status.trim());
        partner.setModifiedBy(userId);
        partner.setModifiedDate(new Date(System.currentTimeMillis()));
        paRepo.save(partner);


        // save partner history status
        PartnerHist paHist = this.preparePartnerHist(id);
        PPMFLGApprovalLog log = this.saveApprovalLog(partner, paHist, userId, actionType, ApproverCategory.Partner, null, reasonVm.getRemarks(), null, VERIFY_SUCCESS_LABEL);
        if(log != null && reasonVm.getReasonIds() != null && reasonVm.getReasonIds().size() > 0){
            PPMFLGReasonLogMap map = new PPMFLGReasonLogMap();
            map.setReasonId(reasonVm.getReasonIds().get(0));
            map.setLogId(log.getId());
            reasonLogRepo.save(map);
        }
        PartnerVM partnerVM =
                new PartnerVM(paHist,
                        sysParamSrv.getApplicationContextPath());
        List<ReasonVM> reasonVMs = reasonSrv.populateReasonsByIds(reasonVm.getReasonIds());
        partnerVM.setReasons(reasonVMs);
        if(reasonVm != null && reasonVm.getRemarks() != null && reasonVm.getRemarks().trim().length() > 0){
            partnerVM.setHasUserRemarks(true);
            partnerVM.setUserRemarks(reasonVm.getRemarks());
        }
        if(ActionType.Resubmit.code.equalsIgnoreCase(actionType.code)){
            this.paEmailSrv.sendingPartnerProfileResubmitEmail(partnerVM);
        }else{
            // sent email by action type
            this.paEmailSrv.sendingPartnerProfileRejectEmail(partnerVM);
        }
        return null;
    }

    private void changeAccountStatus(PPMFLGPartner partner,String status){
        if(status.equals(PartnerStatus.Suspended.code)||status.equals(PartnerStatus.Ceased.code)){
            PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
            mainAcc.setStatus(UserStatus.Inactive.toString());
            this.mainAccRepo.save(mainAcc);
            List<PPMFLGTASubAccount> subAccs = mainAcc.getSubAccs();
            for(PPMFLGTASubAccount subAcc:subAccs){
                subAcc.setStatus(UserStatus.Inactive.toString());
                this.subAccRepo.save(subAcc);
            }
        }
        if(status.equals(PartnerStatus.Active.code)){
            PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
            mainAcc.setStatus(UserStatus.Active.toString());
            this.mainAccRepo.save(mainAcc);
        }
    }

    String CHANGE_ORG_TYPE_LABEL = "changed the partner type to ";
    @Override
    @Transactional
    public void changeOrgType(Integer paId,String userNm,String typeId) throws RepositoryException{
        PartnerHist paHist = this.preparePartnerHist(paId);
        String prePaXml = NvxUtil.getXmlfromObj(paHist);
        PPMFLGPartner partner = this.paRepo.findById(paId);
        partner.setOrgTypeCode(typeId);
        paRepo.save(partner);
        paRepo.flush();
        String paTypeLabel = paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data,typeId);
        PPMFLGPartnerType paType = null;
        this.saveGeneralLog(paId, userNm, CHANGE_ORG_TYPE_LABEL+paTypeLabel, prePaXml);
    }

    String CHANGE_STATUS_LABEL = "changed the status to ";
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVM changePartnerStatus(Integer paId,String userNm,String status) throws RepositoryException{
        ResultVM resultVM = new ResultVM();
        PPMFLGPartner partner = this.paRepo.findById(paId);
        try {
            ApiResult<AxCustomerTable> axCustomerResult = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber());
            if (!axCustomerResult.isSuccess()) {
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
                return resultVM;
            }
            PartnerHist paHist = this.preparePartnerHist(paId);
            String prePaXml = NvxUtil.getXmlfromObj(paHist);

            partner.setStatus(status);
            paRepo.save(partner);
            changeAccountStatus(partner, status);
            paRepo.flush();

            boolean axStatus = false;
            if (status.equals(PartnerStatus.Suspended.code) || status.equals(PartnerStatus.Ceased.code)) {
                axStatus = false;
            } else {
                axStatus = true;
            }
            AxCustomerTable axCustomer = axCustomerResult.getData();
            axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, axCustomer, axStatus);
            this.saveGeneralLog(paId, userNm, CHANGE_STATUS_LABEL + status, prePaXml);
        } catch (Exception e) {
            e.printStackTrace();
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
        }

        return resultVM;
    }

    String CHANGE_EMAIL_LABEL = "changed the email to ";
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVM changePartnerEmail(Integer paId, String userNm, String email) throws RepositoryException {
        ResultVM resultVM = new ResultVM();
        PPMFLGPartner partner = this.paRepo.findById(paId);
        AxStarServiceResult<AxStarCustomer> axCustomerResult = axStarSrv.apiCustomerGet(StoreApiChannels.PARTNER_PORTAL_MFLG,partner.getAxAccountNumber());
        if (!axCustomerResult.isSuccess()) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
            return resultVM;
        }
        PartnerHist paHist = this.preparePartnerHist(paId);
        String prePaXml = NvxUtil.getXmlfromObj(paHist);
        partner.setEmail(email);
        paRepo.save(partner);
        PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
        mainAcc.setEmail(email);
        this.mainAccRepo.save(mainAcc);
        paRepo.flush();
        AxStarCustomer axCustomer = axCustomerResult.getData();
        axCustomer.setEmail(email);
        AxStarServiceResult<AxStarCustomer> axCustomerUpdateResult = axStarSrv.apiCustomerUpdate(StoreApiChannels.PARTNER_PORTAL_MFLG,axCustomer);
        if (!axCustomerUpdateResult.isSuccess()) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
            return resultVM;
        }
        this.saveGeneralLog(paId, userNm, CHANGE_EMAIL_LABEL+email, prePaXml);
        return resultVM;
    }

    String CHANGE_UEN_LABEL = "changed the UEN to ";
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVM changePaUEN(Integer paId, String userNm, String uen) throws RepositoryException{
        PPMFLGPartner partner = this.paRepo.findById(paId);
        ResultVM resultVM = new ResultVM();
        try {
            ApiResult<AxCustomerTable> axCustomerResult = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber());
            if (!axCustomerResult.isSuccess()) {
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
                return resultVM;
            }
            PartnerHist paHist = this.preparePartnerHist(paId);
            String prePaXml = NvxUtil.getXmlfromObj(paHist);

            partner.setUen(uen);
            paRepo.save(partner);
            paRepo.flush();

            AxCustomerTable axCustomer = axCustomerResult.getData();
            axCustomer.setUenNumber(uen);
            axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, axCustomer, axCustomer.getEnablePartnerPortal()==1);
            this.saveGeneralLog(paId, userNm, CHANGE_UEN_LABEL + uen, prePaXml);
        } catch (Exception e) {
            e.printStackTrace();
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
        }
        return resultVM;
    }

    String CHANGE_LICENSE_LABEL = "changed the Travel Agent License No to ";
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVM changePaLicense(Integer paId, String userNm, String licenseNum) {
        PPMFLGPartner partner = this.paRepo.findById(paId);
        ResultVM resultVM = new ResultVM();
        try {
            ApiResult<AxCustomerTable> axCustomerResult = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber());
            if (!axCustomerResult.isSuccess()) {
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
                return resultVM;
            }
            PartnerHist paHist = this.preparePartnerHist(paId);
            String prePaXml = NvxUtil.getXmlfromObj(paHist);

            partner.setLicenseNum(licenseNum);
            paRepo.save(partner);
            paRepo.flush();
            AxCustomerTable axCustomer = axCustomerResult.getData();
            axCustomer.setTaLicenseNumber(licenseNum);
            ApiResult<Boolean> axUpdateCustomerResult = axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, axCustomer, axCustomer.getEnablePartnerPortal()==1);

            this.saveGeneralLog(paId, userNm, CHANGE_LICENSE_LABEL + licenseNum, prePaXml);
        } catch (Exception e) {
            e.printStackTrace();
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.AxCustomerNotFound.message);
        }
        return resultVM;
    }

    @Override
    public void saveGeneralLog(Integer paId,String userNm,String desc,String prePaXml) throws RepositoryException{
        PartnerHist curPaHist = this.preparePartnerHist(paId);
        String curPaXml = NvxUtil.getXmlfromObj(curPaHist);
        PPMFLGApprovalLog appLog = new PPMFLGApprovalLog();
        appLog.setRelatedKey(paId.toString());
        appLog.setCurrentValue(curPaXml);
        appLog.setPreviousValue(prePaXml);
        appLog.setActionType(ActionType.Update.code);
        appLog.setStatus(FlowStatus.Completed.code);
        appLog.setCategory(ApproverCategory.Partner.code);
        appLog.setCreatedBy(userNm);
        appLog.setCreatedDate(new Date());
        appLog.setDescription(desc);
        this.apprLogRepo.save(appLog);
    }
    @Override
    @Transactional(readOnly = true)
    public PPMFLGPartner getPartnerById(Integer paId){
        return paRepo.findById(paId);
    }

    @Override
    public PartnerVM viewPartnerDetails(Integer paId, String adminLoginId) throws Exception {
        PPMFLGPartner pa = paRepo.findById(paId);
        PartnerVM paVM = getPartnerVmById(paId);
        paVM.populateCustomerGroupName(getCustomerGroups());
        paVM.populateCapacityGroupName(getCapacityGroups());
        //PartnerVM paVM = new PartnerVM(pa);
        log.debug("getDocMapByPaId...");
//        List<PPMFLGPADocMapping> paDocMaps = paDocMapRepo.findByPartnerId(pa.getId());
//        List<PartnerDocumentVM> paDocs = new ArrayList<PartnerDocumentVM>();
//        for(PPMFLGPADocMapping tmpObj:paDocMaps){
//            paDocs.add(new PartnerDocumentVM(tmpObj.getPaDoc()));
//        }
//        paVM.setPaDocs(paDocs);

        //paVM.setAccountManager(adminService.getAdminUserNameByAdminId(paVM.getAccountManagerId()));
        //paVM.setPaDocs(paDocSrv.getPartnerDocumentsByPartnerId(paVM.getId()));
        //paVM.setCountryName(ctySrv.getCountryNameByCountryId(paVM.getCountryId()));
        //paVM.setOrgType(paTypeSrv.getPartnerTypeByPartnerTypeId(paVM.getOrgTypeId()));
        //populatePartnerDistributionMapping(paVM);
        // paVM.setRevalFeeStr(revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(paVM.getRevalFeeItemId()));
        // productTierSrv.processTierPartner(paVM.getTierId(),paVM.getId());

        //prodSrv.processProductPartnerMapping(paVM.getId(), paVM.getExcluProdIds());
        // paVM.setTierLabel(getTierNameByTierId(paVM.getTierId()));
//        ProductGridFilterVM filter = new ProductGridFilterVM();
//        ProdFilter prodFilter = new ProdFilter();
//        prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
//        filter.setProdFilter(prodFilter);
//        paVM.setExcluProds(prodSrv.getProdVmsByProdIds(filter, paVM.getExcluProdIds(),true));
        /*if(pa.getAccountManagerId()!=null){
            AdminAccount accMg = adminAccountDao.find(pa.getAccountManagerId());
            paVM.setAccountManager(accMg.getUsername());
        }
        if(pa.getTierId()!=null){
            ProductTier tier = productTierDao.find(pa.getTierId());
            paVM.setTierLabel(tier.getName());
        }
        if(pa.getRevalFeeItemId()!=null){
            RevalidationFeeItem feeItem = this.revalFeeItemDao.find(pa.getRevalFeeItemId());
            String revalFee = TransactionUtil.priceWithDecimal(feeItem.getPrice(), "");
            paVM.setRevalFeeStr(revalFee);
        }

        List<PartnerExclusiveMapping> prodMaps = partnerExclusiveMappingDao.getMappingExclusiveProds(pa.getId());
        if(prodMaps!=null){
            List<ProductViewModel> prodVms = new ArrayList<ProductViewModel>();
            for(PartnerExclusiveMapping tmpObj:prodMaps){
                prodVms.add(new ProductViewModel(tmpObj.getProduct()));
            }
            paVM.setExcluProds(prodVms);
        }
        TAMainAccount mainAcc = pa.getMainAccount();
        if(mainAcc!=null){
            paVM.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
        }
        List<PaDistributionMapping> distributionMapping =  paDistributionMappingDao.getPaDistributionMapping(pa.getId());
        if(distributionMapping!=null){
            List<PaDistributionMappingVM> pdmVms = new ArrayList<PaDistributionMappingVM>();
            for(PaDistributionMapping tmpObj:distributionMapping){
                pdmVms.add(new PaDistributionMappingVM(tmpObj));
            }
            paVM.setDistributionMapping(pdmVms);
        }*/
        return paVM;

    }
    @Override
    @Transactional(readOnly = true)
    public com.enovax.star.cms.commons.model.api.ApiResult<String> getPartnerHist(Integer paId) {
        List<ApprovalLogVM> vms = appLogSrv.getHistoryLog(paId);
        String result = JsonUtil.jsonify(vms);
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result);
    }

    @Override
    @Transactional(readOnly = true)
    public com.enovax.star.cms.commons.model.api.ApiResult<String> getPreviousPartnerHist(Integer appId) throws Exception{
        PPMFLGApprovalLog appLog = appLogSrv.getLogById(appId);
        ApprovalLogVM appLogVM = new ApprovalLogVM(appLog);
        PartnerVM paVm = appLogSrv.getPartnerFromHist(appLog.getPreviousValue());
        paVm.setCreatedBy(appLog.getCreatedBy());
        paVm.setRemarks(appLog.getRemarks());
        PartnerDetailsVM partnerDetailsVM = new PartnerDetailsVM(paVm,appLogVM,false,true);
        String result = JsonUtil.jsonify(partnerDetailsVM);
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result);
    }

    @Override
    @Transactional(readOnly = true)
    public com.enovax.star.cms.commons.model.api.ApiResult<SubuserVM> getSubuser(Integer paId) {
        //PartnerVM paVM = getPartnerVmById(paId);
        PPMFLGPartner pa = paRepo.findById(paId);
        if(pa == null) {
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.PartnerNotFound);
        }
        PPMFLGTAMainAccount mainAcc = mainAccRepo.findById(pa.getAdminId());
        List<PPMFLGTASubAccount> subAccs = mainAcc.getSubAccs();
        List<PartnerAccountVM> accVms = new ArrayList<>();
        for(PPMFLGTASubAccount acc:subAccs){
            PPMFLGTASubAccount tmpObj = partnerSharedAccSrv.getSubUser(acc.getId(), true);
            PartnerAccountVM vm=new PartnerAccountVM(tmpObj);
            vm.setPassword("");
            accVms.add(vm);
        }
        List<PPMFLGTAAccessGroupVM> accessGroupVms = new ArrayList<>();
        List<PPMFLGTAAccessRightsGroup> accessRightsGroup = partnerSharedAccSrv.getAccessRightsGroup();
        for(PPMFLGTAAccessRightsGroup group : accessRightsGroup){
            accessGroupVms.add(new PPMFLGTAAccessGroupVM(group));
        }

        SubuserVM subuserVM = new SubuserVM(accVms,accessGroupVms,accVms.size());
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", subuserVM);
    }
/*
    @Override
    @Transactional(readOnly = true)
    public com.enovax.star.cms.commons.model.api.ApiResult<SubuserVM> getTransDetails(Integer transId, String transType) {


        SubuserVM subuserVM = null;
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", subuserVM);
    }
*/


    private PPMFLGApprovalLog saveApprovalLog(PPMFLGPartner pa, PartnerHist paHist, String userId, ActionType actionType, ApproverCategory category, ApproverTypes approverType,
                                             String remarks, String status, String label) {
        PPMFLGApprovalLog appLog = new PPMFLGApprovalLog();
        appLog.setRelatedKey(pa.getId().toString());
        appLog.setCurrentValue(NvxUtil.getXmlfromObj(paHist));
        appLog.setActionType(actionType != null ? actionType.code : null);
        appLog.setApproverType(approverType != null ? approverType.code : null);
        appLog.setCategory(category != null ? category.code : null);
        appLog.setDescription(label + pa.getOrgName());
        appLog.setCategory(ApproverCategory.Partner.code);
        if(status != null){
            appLog.setStatus(status);
        }
        appLog.setCreatedBy(userId);
        appLog.setRemarks(remarks);
        appLog.setCreatedDate(new Date());
        apprLogRepo.save(appLog);
        return appLog;

    }

    private PartnerHist preparePartnerHist(Integer id) throws  RepositoryException{
        log.debug("preparePartnerHist......"+id);
        PPMFLGPartner pa = paRepo.findOne(id);
        List<Integer> paDocIds = new ArrayList<Integer>();
        log.debug("getDocMapByPaId...");
        List<PPMFLGPADocMapping> paDocMaps = paDocMapRepo.findByPartnerId(pa.getId());
        if(paDocMaps!=null){
            for(PPMFLGPADocMapping tmpObj : paDocMaps){
                if(tmpObj!=null){
                    paDocIds.add(tmpObj.getDocId());
                }
            }
        }
        List<PPMFLGPaDistributionMapping> distributionMapping =  paDisMapRepo.findByPartnerId(pa.getId());
        List<PaDistributionMappingHist> pdmHist = new ArrayList<PaDistributionMappingHist>();
        if(distributionMapping!=null){
            for(PPMFLGPaDistributionMapping tmpObj:distributionMapping){
                pdmHist.add(new PaDistributionMappingHist(tmpObj));
            }
        }
        PartnerHist paHist = new PartnerHist(pa);
        paHist.setPaDocIds(paDocIds);
        paHist.setDistributionMapping(pdmHist);
        String tierId = pa.getTierId();
        paHist.setTierId(tierId);
        List<String> exclProdIds = paExclProdSrv.getExclProdbyPartner(PartnerPortalConst.Partner_Portal_Channel, id);
        paHist.setExcluProdIds(exclProdIds);
        PPMFLGTAMainAccount mainAcc = pa.getMainAccount();
        if(mainAcc!=null){
            paHist.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
        }
        return paHist;
    }


    private PartnerHist preparePartnerHistWithExclProdandTier(Integer id) throws RepositoryException{
        log.debug("preparePartnerHist......"+id);
        PPMFLGPartner pa = paRepo.findOne(id);
        List<Integer> paDocIds = new ArrayList<Integer>();
        log.debug("getDocMapByPaId...");
        List<PPMFLGPADocMapping> paDocMaps = paDocMapRepo.findByPartnerId(pa.getId());
        if(paDocMaps!=null){
            for(PPMFLGPADocMapping tmpObj : paDocMaps){
                if(tmpObj!=null){
                    paDocIds.add(tmpObj.getDocId());
                }
            }
        }
        List<PPMFLGPaDistributionMapping> distributionMapping =  paDisMapRepo.findByPartnerId(pa.getId());
        List<PaDistributionMappingHist> pdmHist = new ArrayList<PaDistributionMappingHist>();
        if(distributionMapping!=null){
            for(PPMFLGPaDistributionMapping tmpObj:distributionMapping){
                pdmHist.add(new PaDistributionMappingHist(tmpObj));
            }
        }
        PartnerHist paHist = new PartnerHist(pa);
        paHist.setPaDocIds(paDocIds);
        paHist.setDistributionMapping(pdmHist);
        String tierId = pa.getTierId();
        paHist.setTierId(tierId);
        List<String> exclProdIds = paExclProdSrv.getExclProdbyPartner(PartnerPortalConst.Partner_Portal_Channel, id);
        paHist.setExcluProdIds(exclProdIds);
//        List<PPMFLGPartnerExt> partnerExtList = pa.getPartnerExtList();
//        List<PartnerExtVM> partnerExtVMList = new ArrayList<>();
//        List<String> attrNameList = new ArrayList<>();
//        attrNameList.add("customerGroup");
//        attrNameList.add("onlinePaymentEnabled");
//        attrNameList.add("offlinePaymentEnabled");
//        attrNameList.add("depoistEnabled");
//        attrNameList.add("wotReservationEnabled");
//        attrNameList.add("wotReservationCap");
//        attrNameList.add("wotReservationValidUntil");
//        for (PPMFLGPartnerExt partnerExt : partnerExtList) {
//            PartnerExtVM partnerExtVM = new PartnerExtVM(partnerExt);
//            if (attrNameList.contains(partnerExtVM.getAttrName())) {
//                partnerExtVM.setId(null);
//                partnerExtVM.setPartnerId(null);
//                partnerExtVM.setDescription(null);
//                partnerExtVM.setAttrValueLabel(null);
//                partnerExtVM.setAttrValueFormat(null);
//                partnerExtVMList.add(partnerExtVM);
//            }
//        }
//        paHist.setExtension(partnerExtVMList);
        PPMFLGTAMainAccount mainAcc = pa.getMainAccount();
        if(mainAcc!=null){
            paHist.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
        }
        return paHist;
    }

    @Override
    public ResultVM resetPassword(int userId, String updatedBy) throws Exception{
        String password = PasswordUtil.generatePswd();
        String encodedPassword = PasswordUtil.encode(password);
        return paAccSrv.resetPassword(userId, password, encodedPassword, updatedBy);
    }

    @Transactional(readOnly = true)
    public Page<PPMFLGPartner> getPartnersByParams(PartnerGridFilterVM filter) throws Exception {
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        PartnerFilter partnerFilter = filter != null ? filter.getPartnerFilter() : null;
        if(partnerFilter != null) {
            if (partnerFilter.getStatus() != null && partnerFilter.getStatus().trim().length() > 0) {
                criterias.add(new QCriteria("status", QCriteria.Operation.EQ, partnerFilter.getStatus()));
            }
            if (partnerFilter.getPaCode() != null && partnerFilter.getPaCode().trim().length() > 0) {
                criterias.add(new QCriteria("accountCode", QCriteria.Operation.EQ, partnerFilter.getPaCode()));
            }
            if (partnerFilter.getOrgName() != null && partnerFilter.getOrgName().trim().length() > 0){
                criterias.add(new QCriteria("orgName", QCriteria.Operation.LIKE, partnerFilter.getOrgName()));
            }
            if(partnerFilter.getContactPerson() != null && partnerFilter.getContactPerson().trim().length() > 0) {
                criterias.add(new QCriteria("contactPerson", QCriteria.Operation.LIKE, partnerFilter.getContactPerson()));
            }
        }
        partnerFilter = null;
        PageRequest pageRequest = new PageRequest( filter.getPage() - 1, filter.getPageSize(),
                filter.isSortDesc() ? Sort.Direction.ASC : Sort.Direction.DESC,
                filter.getSortField("orgName")
        );
        SpecificationBuilder specBuilder = new SpecificationBuilder();
        Specifications querySpec = specBuilder.build(criterias, PPMFLGPartnerSpecification.class);
        Page<PPMFLGPartner> list = paRepo.findAll(querySpec, pageRequest);
        partnerFilter = null;
        specBuilder = null;
        querySpec = null;
        criterias = null;
        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PartnerVM> getPartnerVMsByStatus(String status) {
        List<PPMFLGPartner> partners = paRepo.findByStatus(status);
        List<PartnerVM> paVms = new ArrayList<PartnerVM>();
        for(PPMFLGPartner pa:partners) {
            PartnerVM partnerVM = new PartnerVM();
            partnerVM.setId(pa.getId());
            partnerVM.setOrgName(pa.getOrgName());
            paVms.add(partnerVM);
        }
        return paVms;
    }

    @Override
    public List<PartnerVM> getPartnerVMs(Page<PPMFLGPartner> page) {
        List<PartnerVM> vms = new ArrayList<PartnerVM>();
        if(page != null){
            Iterator<PPMFLGPartner> iter = page.iterator();
            while(iter.hasNext()){
                PPMFLGPartner pp = iter.next();
                PartnerVM pv = new PartnerVM();
                pv.setId(pp.getId());
                pv.setOrgName(pp.getOrgName());
                pv.setAccountCode(pp.getAccountCode());
                pv.setEmail(pp.getEmail());
                pv.setStatus(pp.getStatus());
                pv.setStatusLabel(PartnerStatus.getLabel(pp.getStatus()));
                ///  pv.setCountryId(pp.getCountryId());
                pv.setContactPerson(pp.getContactPerson());
                //... add more...
                vms.add(pv);
            }
        }
        return vms;
    }

    @Transactional
    public String apiRejectPartnerRegistrationRequest(StoreApiChannels channel, Integer appId, PPMFLGApprovalLog request, ReasonVM reasonVM, String adminLoginId) throws Exception {
        if(request == null || request.getCurrentValue() == null || request.getCurrentValue().trim().length() == 0 ||
                reasonVM == null || reasonVM.getReasonIds() == null || reasonVM.getReasonIds().size() == 0){
            return "INVALID_APPROVAL_REQUEST";
        }
        Date now = new Date(System.currentTimeMillis());
        request.setStatus(FlowStatus.Completed.code);
        request.setRemarks(reasonVM.getRemarks());
        request.setApprover(adminLoginId);
        request.setReviewDate(now);
        apprLogRepo.save(request);
        String xml = request.getCurrentValue();
        PartnerHist paHist = NvxUtil.getObjFromXml(xml, PartnerHist.class);
        PPMFLGPartner partner = paRepo.findById(paHist.getId());
        partner.setStatus(PartnerStatus.Rejected.code);
        partner.setModifiedDate(now);
        partner.setModifiedBy(adminLoginId);
        paRepo.save(partner);
        PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
        if(mainAcc != null){
            mainAcc.setStatus(UserStatus.Deleted.toString());
            mainAcc.setModifiedBy(adminLoginId);
            mainAcc.setModifiedDate(now);
            mainAccRepo.save(mainAcc);
        }

        PPMFLGApprovalLog approvalLog = new PPMFLGApprovalLog();
        approvalLog.setParentLogId(request.getId());
        approvalLog.setRelatedKey(request.getRelatedKey());
        approvalLog.setRemarks(reasonVM.getRemarks());
        approvalLog.setCurrentValue(request.getCurrentValue());
        approvalLog.setActionType(ActionType.Reject.code);
        approvalLog.setStatus(FlowStatus.Rejected.code);
        approvalLog.setApproverType(request.getApproverType());
        approvalLog.setCategory(ApproverCategory.Partner.code);
        approvalLog.setApprover(adminLoginId);
        approvalLog.setReviewDate(now);
        approvalLog.setCreatedBy(adminLoginId);
        approvalLog.setCreatedDate(now);
        approvalLog.setDescription(PartnerPortalConst.REJECT_PA_REGISTER_LABEL + partner.getOrgName());
        apprLogRepo.save(approvalLog);

        savePartnerRejectReasonList(approvalLog.getId(), reasonVM.getReasonIds());

        ApprovingPartnerVM vm = getApprovalLogDetailsByApprovalLogId(channel, adminLoginId, approvalLog.getId());
        paEmailSrv.sendPartnerRegistrationRejectionEmail(vm);
        return null;
    }

    @Override
    @Transactional
    public String apiRejectPartnerUpdateRequest(StoreApiChannels channel, Integer appId, PPMFLGApprovalLog request, ReasonVM reasonVM, String adminLoginId) {
        if(request == null || request.getCurrentValue() == null || request.getCurrentValue().trim().length() == 0 ||
                reasonVM == null || reasonVM.getReasonIds() == null || reasonVM.getReasonIds().size() == 0){
            return "INVALID_APPROVAL_REQUEST";
        }
        request.setStatus(FlowStatus.Completed.code);
        apprLogRepo.save(request);
        PartnerHist paHist = NvxUtil.getObjFromXml(request.getCurrentValue(), PartnerHist.class);
        PPMFLGApprovalLog appLog = new PPMFLGApprovalLog();
        appLog.setParentLogId(request.getId());
        appLog.setRelatedKey(request.getRelatedKey());
        appLog.setCurrentValue(request.getCurrentValue());
        appLog.setActionType(ActionType.Reject.code);
        appLog.setStatus(FlowStatus.Rejected.code);
        appLog.setApproverType(request.getApproverType());
        appLog.setRemarks(reasonVM.getRemarks());
        appLog.setCategory(request.getCategory());
        appLog.setApprover(adminLoginId);
        appLog.setReviewDate(new Date());
        appLog.setCreatedBy(adminLoginId);
        appLog.setCreatedDate(new Date());
        appLog.setDescription(REJECT_PA_EDIT_LABEL+paHist.getOrgName());
        this.apprLogRepo.save(appLog);
        this.savePartnerRejectReasonList(appLog.getId(), reasonVM.getReasonIds());
        return null;
    }

    private void savePartnerRejectReasonList(Integer id, List<Integer> reasonIds) {
        if(id == null || id.intValue() == 0 || reasonIds == null || reasonIds.size() == 0){
            return;
        }
        for(Integer reasonId : reasonIds){
            PPMFLGReasonLogMap map = new PPMFLGReasonLogMap();
            map.setLogId(id);
            map.setReasonId(reasonId);
            reasonLogRepo.save(map);
        }
    }

    @Transactional(rollbackFor = { ParseException.class, Exception.class, Throwable.class, NullPointerException.class, RepositoryException.class })
    public String apiApprovePartnerRegistrationRequest( Integer appId, PPMFLGApprovalLog request, String adminLoginId) throws Exception {
        if(request == null || request.getCurrentValue() == null || request.getCurrentValue().trim().length() == 0){
            return "INVALID_APPROVAL_REQUEST";
        }

        boolean enablePartnerPortal = true;
        boolean allowOnAccount = sysParamSrv.getPartnerRegistrationAllowOnAccount();
        String documnetTypeId = sysParamSrv.getPartnerRegistrationAxDocumentTypeId();
        List<PPMFLGPADocMapping> docMap = null;
        List<PPMFLGPaDistributionMapping> marketDistributionList = null;

        Date now = new Date(System.currentTimeMillis());

        String xml = request.getCurrentValue();
        PartnerHist paHist = NvxUtil.getObjFromXml(xml, PartnerHist.class);

        String custGroup  = null;
        String corrspAddr = null;
        String corrspCity = null;
        String corrspPostCode = null;
        String capacityGroupId = null;
        List<PartnerExtVM> exts = paHist.getExtension();
        if(exts != null && exts.size() > 0){
            for(PartnerExtVM i : exts){
                if(i == null || i.getAttrName() == null){
                    continue;
                }
                if("customerGroup".equals(i.getAttrName())){
                    custGroup = i.getAttrValue();
                }
                if("correspondenceAddress".equals(i.getAttrName())){
                    corrspAddr = i.getAttrValue();
                }
                if("correspondenceCity".equals(i.getAttrName())){
                    corrspCity = i.getAttrValue();
                }
                if("correspondencePostalCode".equals(i.getAttrName())){
                    corrspPostCode = i.getAttrValue();
                }
                if("capacityGroupId".equals(i.getAttrName())){
                    capacityGroupId = i.getAttrValue();
                }
            }
        }
        if(custGroup == null || custGroup.trim().length() == 0){
            throw new BizValidationException("Customer Group is not found");
        }
        if(corrspAddr == null || corrspAddr.trim().length() == 0){
            throw new BizValidationException("Correspondence Address is not found");
        }
        if(corrspPostCode == null || corrspPostCode.trim().length() == 0){
            throw new BizValidationException("Correspondence Postal Code is not found");
        }
        if(capacityGroupId == null || capacityGroupId.trim().length() == 0){
            throw new BizValidationException("Capacity Group is not found");
        }
        PPMFLGPartner partner = paRepo.findById(paHist.getId());
        docMap = paDocMapRepo.findByPartnerId(partner.getId());
        marketDistributionList = paDisMapRepo.findByPartnerId(partner.getId());

        AxCustomerTable customerTable = paExtSrv.popuplateCustoemrTable(partner, enablePartnerPortal, allowOnAccount, docMap, marketDistributionList, documnetTypeId, capacityGroupId, partner.getOrgTypeCode());

        // save partner tier mapping
        String tierId = paHist.getTierId();

        // save partner exclusive product mapping
        if(paHist.getExcluProdIds() != null && paHist.getExcluProdIds().size() > 0){
            paExclProdSrv.saveExclProdbyPartner(PartnerPortalConst.Partner_Portal_Channel,paHist.getId(), paHist.getExcluProdIds());
        }
        // update partner status
        partner.setTierId(tierId);
        partner.setStatus(PartnerStatus.Active.code);
        partner.setModifiedDate(now);
        partner.setModifiedBy(adminLoginId);
        paRepo.save(partner);

        // update partner account status
        PartnerAccountVM partnerAccountVM = null;
        PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
        if(mainAcc != null){
            mainAcc.setStatus(UserStatus.Active.toString());
            mainAcc.setModifiedBy(adminLoginId);
            mainAcc.setModifiedDate(now);
            assignRegisteredTAMainAccountDefaultRoles(mainAcc,adminLoginId);
            partnerAccountVM = assignRegisteredTAMainAccountDefaultPassword(mainAcc, adminLoginId);
            mainAccRepo.save(mainAcc);
        }

        ApprovingPartnerVM vm = getApprovalLogDetailsByApprovalLogId(StoreApiChannels.PARTNER_PORTAL_MFLG, adminLoginId, appId);
        vm.setPartnerAccountVM(partnerAccountVM);

        // sync partners
        AxStarServiceResult<AxStarCustomer> result = paAxSrv.registerPartner(partner, tierId, custGroup, corrspAddr, corrspPostCode, corrspCity);
        if(result.isSuccess()){
            partner.setAxAccountNumber(result.getData().getAccountNumber());
            this.paRepo.save(partner);
            customerTable.setAccountNum(partner.getAxAccountNumber());
            log.info("customer ["+partner.getId()+"] registration successfully done");
        }else{
            log.error("customer ["+partner.getId()+"] registration failed : "+JsonUtil.jsonify(result));
            throw new Exception("REGISTER-PARTNER-IN-AX-FAILED");
        }

        request.setStatus(FlowStatus.Completed.code);
        request.setApprover(adminLoginId);
        request.setReviewDate(now);
        apprLogRepo.save(request);

        PPMFLGApprovalLog approvalLog = new PPMFLGApprovalLog();
        approvalLog.setParentLogId(request.getId());
        approvalLog.setRelatedKey(request.getRelatedKey());
        approvalLog.setCurrentValue(request.getCurrentValue());
        approvalLog.setActionType(ActionType.Approve.code);
        approvalLog.setStatus(FlowStatus.Approved.code);
        approvalLog.setApproverType(request.getApproverType());
        approvalLog.setCategory(ApproverCategory.Partner.code);
        approvalLog.setApprover(adminLoginId);
        approvalLog.setReviewDate(now);
        approvalLog.setDescription(APPROVED_SUCCESS_LABEL+partner.getOrgName());
        approvalLog.setCreatedBy(adminLoginId);
        approvalLog.setCreatedDate(now);
        apprLogRepo.save(approvalLog);
        try{
            boolean success = paExtSrv.updatePartnerExtProperties(customerTable, enablePartnerPortal);
            updateExtensionsFailed(partner, success);
        }catch(Exception ex){
            updateExtensionsFailed(partner, false);
            log.error("Update Partner Extension Properties Failed, and failure is ignore during registering customers.",ex);
        }catch(Throwable ex){
            updateExtensionsFailed(partner, false);
            log.error("Update Partner Extension Properties Failed, and failure is ignore during registering customers.",ex);
        }
        paEmailSrv.sendPartnerRegistrationApprovedEmail(vm);
        return null;
    }

    @Transactional(rollbackFor = { ParseException.class, Exception.class, Throwable.class, NullPointerException.class, RepositoryException.class })
    public void apiReUpdatePartnerDetailsAfterRegistration(Integer paId){
        PPMFLGPartner partner = null;
        try{
            partner = paRepo.findById(paId);
            boolean success = apiReUpdatePartnerDetailsAfterRegistrationWithException(partner);
            updateExtensionsFailed(partner, success);
        }catch (Exception ex){
            ex.printStackTrace();
            log.error("Update Partner Extension Properties Failed, and failure is ignore during registering customers.",ex);
            if(partner != null){
                updateExtensionsFailed(partner, false);
            }
        }
    }

    @Transactional(rollbackFor = { ParseException.class, Exception.class, Throwable.class, NullPointerException.class, RepositoryException.class })
    public boolean apiReUpdatePartnerDetailsAfterRegistrationWithException(PPMFLGPartner partner) throws Exception {
        boolean enablePartnerPortal = true;
        boolean allowOnAccount = sysParamSrv.getPartnerRegistrationAllowOnAccount();
        String documnetTypeId = sysParamSrv.getPartnerRegistrationAxDocumentTypeId();
        List<PPMFLGPADocMapping> docMap = null;
        List<PPMFLGPaDistributionMapping> marketDistributionList = null;

        docMap = paDocMapRepo.findByPartnerId(partner.getId());
        marketDistributionList = paDisMapRepo.findByPartnerId(partner.getId());

        String capacityGroupId = null;
        List<PPMFLGPartnerExt> exts = partner.getPartnerExtList();
        if(exts != null && exts.size() > 0){
            for(PPMFLGPartnerExt ext : exts){
                if("capacityGroupId".equals(ext.getAttrName())){
                    capacityGroupId = ext.getAttrValue();
                    break;
                }
            }
        }

        AxCustomerTable customerTable = paExtSrv.popuplateCustoemrTable(partner, enablePartnerPortal, allowOnAccount, docMap, marketDistributionList, documnetTypeId, capacityGroupId, partner.getOrgTypeCode());
        return paExtSrv.updatePartnerExtProperties(customerTable, enablePartnerPortal);
    }

    @Transactional(rollbackFor = Exception.class)
    private void updateExtensionsFailed(PPMFLGPartner partner, boolean isSuccess) {
        List<PPMFLGPartnerExt> list1 = paExtRepo.findReUpdateFailedExtensionByPartnerId(partner.getId());
        PPMFLGPartnerExt paExt = null;
        if(list1 != null && list1.size() > 0){
            paExt = list1.get(0);
        }
        if(paExt == null){
            paExt = new PPMFLGPartnerExt();
        }
        paExt.setPartner(partner);
        paExt.setPartnerId(partner.getId());
        paExt.setDescription("updateExtensionFailed");
        paExt.setMandatoryInd(false);
        paExt.setAttrValueType("STRING");
        paExt.setAttrValueFormat("");
        paExt.setApprovalRequired(false);
        paExt.setStatus(GeneralStatus.Active.code);
        paExt.setAttrName("updateExtensionFailed");
        paExt.setAttrValue(isSuccess ? "N" : "Y");
        paExt.setStatus(isSuccess ? "I" : "A");
        paExtRepo.save(paExt);
        // empty it first
        paExt = null;
        List<PPMFLGPartnerExt> list2 = paExtRepo.findReUpdateFailedTimesExtensionByPartnerId(partner.getId());
        if(list2 != null && list2.size() > 0){
            paExt = list2.get(0);
        }
        if(paExt == null){
            paExt = new PPMFLGPartnerExt();
        }
        paExt.setPartner(partner);
        paExt.setPartnerId(partner.getId());
        paExt.setDescription("updateExtensionFailedTimes");
        paExt.setMandatoryInd(false);
        paExt.setAttrValueType("INT");
        paExt.setAttrValueFormat("");
        paExt.setApprovalRequired(false);
        paExt.setStatus(GeneralStatus.Active.code);
        paExt.setAttrName("updateExtensionFailedTimes");
        Integer currentCount = 0;
        String currentVal = paExt.getAttrValue();
        if(currentVal != null && currentVal.trim().length() > 0){
            currentCount += Integer.parseInt(currentVal.trim());
        }
        if(!isSuccess){
            currentCount += 1;
        }
        paExt.setStatus(isSuccess ? "I" : "A");
        paExt.setAttrValue(String.valueOf(currentCount));
        paExtRepo.save(paExt);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String apiApprovePartnerUpdateRequest(Integer appId, PPMFLGApprovalLog request, String adminLoginId) throws Exception{

        if(request == null || request.getCurrentValue() == null || request.getCurrentValue().trim().length() == 0){
            return "INVALID_APPROVAL_REQUEST";
        }
        request.setStatus(FlowStatus.Completed.code);
        apprLogRepo.save(request);
        PartnerHist paHist = NvxUtil.getObjFromXml(request.getCurrentValue(), PartnerHist.class);
        PPMFLGPartner partner = paRepo.findById(paHist.getId());
        partner.setAccountManagerId(paHist.getAccountManagerId());
        PPMFLGTAMainAccount mainAcc = partner.getMainAccount();
        mainAcc.setSubAccountEnabled(paHist.getSubAccountEnabled());
        List<PartnerExtVM> partnerExtVMList = paHist.getExtension();

        for(PartnerExtVM partnerExtVM : partnerExtVMList) {
            PPMFLGPartnerExt partnerExt = paExtRepo.findFirstByPartnerIdAndAttrName(paHist.getId(),partnerExtVM.getAttrName());
            if(partnerExt!= null) {
                partnerExt.setAttrValue(partnerExtVM.getAttrValue());
                paExtRepo.save(partnerExt);
            }
        }

        this.mainAccRepo.save(mainAcc);
        partner.setRevalPeriodMonths(paHist.getRevalPeriodMonths());
        partner.setRevalFeeItemId(paHist.getRevalFeeItemId());
        partner.setDailyTransCap(paHist.getDailyTransCap());
        partner.setTierId(paHist.getTierId());

        // save partner exclusive product mapping
        paExclProdSrv.saveExclProdbyPartner(PartnerPortalConst.Partner_Portal_Channel,paHist.getId(), paHist.getExcluProdIds());

        List<PPMFLGPaDistributionMapping> oldDistributionMapping = this.paDisMapRepo.findByPartnerId(partner.getId());
        if (oldDistributionMapping != null  && oldDistributionMapping.size() > 0){
            for (PPMFLGPaDistributionMapping tmpOldMap : oldDistributionMapping) {
                paDisMapRepo.delete(tmpOldMap);
            }
        }
        List<PaDistributionMappingHist> newDMapHist = paHist.getDistributionMapping();
        if (newDMapHist != null && newDMapHist.size()> 0) {
            for (PaDistributionMappingHist tmpObj : newDMapHist) {
                PPMFLGPaDistributionMapping pdMap = new PPMFLGPaDistributionMapping();
                pdMap.setAdminId(tmpObj.getAdminId());
                pdMap.setCountryId(tmpObj.getCountryId());
                pdMap.setPartnerId(partner.getId());
                pdMap.setPercentage(tmpObj.getPercentage());
                paDisMapRepo.save(pdMap);
            }
        }

        PPMFLGApprovalLog appLog = new PPMFLGApprovalLog();
        appLog.setParentLogId(request.getId());
        appLog.setRelatedKey(request.getRelatedKey());
        appLog.setCurrentValue(request.getCurrentValue());
        appLog.setActionType(ActionType.Approve.code);
        appLog.setStatus(FlowStatus.Approved.code);
        appLog.setApproverType(request.getApproverType());
        appLog.setCategory(request.getCategory());
        appLog.setApprover(adminLoginId);
        appLog.setReviewDate(new Date());
        appLog.setCreatedBy(adminLoginId);
        appLog.setCreatedDate(new Date());
        appLog.setDescription(APPROVED_PA_EDIT_LABEL+partner.getOrgName());
        this.apprLogRepo.save(appLog);

        //sync with AX
        AxStarServiceResult<AxStarCustomer> axStarGetServiceResult = axStarSrv.apiCustomerGet(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber());
        String custGrp = "";
        String capacityGrpId ="";

        if (!axStarGetServiceResult.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axStarGetServiceResult));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }

        AxStarCustomer axStarCustomer = axStarGetServiceResult.getData();
        axStarCustomer.setPriceGroup(paHist.getTierId());
        for (PartnerExtVM paExtVM : partnerExtVMList) {
            if (paExtVM.getAttrName().equalsIgnoreCase("customerGroup")) {
                custGrp = paExtVM.getAttrValue();
            }
            if (paExtVM.getAttrName().equalsIgnoreCase("capacityGroupId")) {
                capacityGrpId = paExtVM.getAttrValue();
            }
        }
        if (!custGrp.equalsIgnoreCase("")) {
            axStarCustomer.setCustomerGroup(custGrp);
        }
        AxStarServiceResult<AxStarCustomer> axStarUpdateServiceResult = axStarSrv.apiCustomerUpdate(StoreApiChannels.PARTNER_PORTAL_MFLG, axStarCustomer);
        if (!axStarUpdateServiceResult.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axStarUpdateServiceResult));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }

        ApiResult<AxCustomerTable> axCustomerTableGetApiResult = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG,partner.getAxAccountNumber());
        if(!axCustomerTableGetApiResult.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axCustomerTableGetApiResult));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }

        AxCustomerTable axCustomerTable = axCustomerTableGetApiResult.getData();
        boolean allowOnAccount = sysParamSrv.getPartnerRegistrationAllowOnAccount();
        axCustomerTable.setAccountNum(partner.getAxAccountNumber());
        axCustomerTable.setAccountMgrCMS(paHist.getAccountManagerId());
        axCustomerTable.setAllowOnAccount(allowOnAccount?1:0);
        axCustomerTable.setDesignation(partner.getContactDesignation());
        axCustomerTable.setCountryCode(partner.getCountryCode());
        List<PPMFLGPADocMapping> docMap = paDocMapRepo.findByPartnerId(partner.getId());
        StringBuilder documentLink = new StringBuilder();
        String documnetTypeId = sysParamSrv.getPartnerRegistrationAxDocumentTypeId();
        if(docMap != null && docMap.size() > 0){
            for(PPMFLGPADocMapping dm : docMap){
                PPMFLGPADocument d = dm.getPaDoc();
                if(d != null){
                    if(documentLink.length() > 0){
                        documentLink.append("|");
                    }
                    documentLink.append(d.getFileName()).append(";").append(documnetTypeId).append(";").append("ftp://example.host.com/partner-portal-mflg/"+d.getFilePath());
                }
            }
        }
        if(documentLink != null && documentLink.length() > 0){
            axCustomerTable.setDocumentLinkDelimited(documentLink.toString());
        }
        axCustomerTable.setFax(partner.getFaxNum());
        List<PaDistributionMappingHist> marketDistributionList = paHist.getDistributionMapping();
        StringBuilder market = new StringBuilder();
        if(marketDistributionList != null && marketDistributionList.size() > 0){
            for(PaDistributionMappingHist i : marketDistributionList){
                if(market.length() > 0){
                    market.append("|");
                }
                market.append(i.getCountryId()).append(";").append(i.getPercentage()).append(";").append(i.getAdminId());
            }
        }
        if(market.length() > 0) {
            axCustomerTable.setMarketDistributionDelimited(market.toString());
        }
        String revalFeeStr = revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG,"",paHist.getRevalFeeItemId());
        Double d = Double.parseDouble(revalFeeStr);
        axCustomerTable.setRevalidationFee(paHist.getRevalFeeItemId());
        axCustomerTable.setRevalidationPeriod(paHist.getRevalPeriodMonths());
        axCustomerTable.setDailyTransCapValue(paHist.getDailyTransCap().doubleValue());
        axCustomerTable.setRegisteredDate(partner.getCreatedDate());

        if(partner.getUen() != null && partner.getUen().trim().length() > 0){
            axCustomerTable.setUenNumber(partner.getUen());
        }
        if(partner.getLicenseNum() != null){
            axCustomerTable.setTaLicenseNumber(partner.getLicenseNum());
        }
        if(partner.getLicenseExpDate() != null){
            axCustomerTable.setTaExpiryDate(NvxDateUtils.parseDate(partner.getLicenseExpDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        if(partner.getBranchName() != null && partner.getBranchName().trim().length() > 0){
            axCustomerTable.setBranchName(partner.getBranchName());
        }

        if(capacityGrpId != null && capacityGrpId.trim().length() > 0){
            axCustomerTable.setCapacityGroupId(capacityGrpId);
        }

        if(partner.getTelNum() != null && partner.getTelNum().trim().length() > 0){
            axCustomerTable.setOfficeNo(partner.getTelNum());
        }

        axCustomerTable.setEnablePartnerPortal(axCustomerTable.getEnablePartnerPortal());
        PPMFLGTAMainAccount mainAccount = partner.getMainAccount();

        axCustomerTable.setMainAccLoginName(mainAccount.getUsername());
        axCustomerTable.setReservationAmendment(0);
        ApiResult<Boolean> axUpdateApi = axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG,axCustomerTable,axCustomerTable.getEnablePartnerPortal()==1);
        if(!axUpdateApi.isSuccess()) {
            log.error("customer ["+partner.getId()+"] update failed : "+JsonUtil.jsonify(axUpdateApi));
            throw new Exception("UPDATE-PARTNER-IN-AX-FAILED");
        }
        return null;
    }

    private PartnerAccountVM assignRegisteredTAMainAccountDefaultPassword(PPMFLGTAMainAccount mainAcc, String loginAdminId) {
        if(mainAcc == null){
            return null;
        }
        String pwd = PasswordUtil.generatePswd();
        String encodedPwd = PasswordUtil.encode(pwd);
        mainAcc.setPwdForceChange(true);
        mainAcc.setStatus(UserStatus.Active.name());
        mainAcc.setPwdAttempts(0);
        mainAcc.setPassword(encodedPwd);
        mainAcc.setModifiedDate(new Date(System.currentTimeMillis()));
        mainAcc.setModifiedBy(loginAdminId);

        PartnerAccountVM accVM = new PartnerAccountVM();
        accVM.setPassword(pwd);
        accVM.setAccountCode(mainAcc.getAccountCode());
        accVM.setEmail(mainAcc.getEmail());
        accVM.setId(mainAcc.getId());
        accVM.setName(mainAcc.getName());
        accVM.setUsername(mainAcc.getUsername());

        return accVM;
    }

    private void assignRegisteredTAMainAccountDefaultRoles(PPMFLGTAMainAccount mainAcc, String loginAdminId) {
        if(mainAcc == null){
            return;
        }
        List<PPMFLGTAAccessRightsGroup> list = taAccRightsGrpRepo.findAll();
        if(list == null || list.size() == 0){
            return;
        }
        for(PPMFLGTAAccessRightsGroup grp : list){
            PPMFLGTAMainRightsMapping map = new PPMFLGTAMainRightsMapping();
            map.setUser(mainAcc);
            map.setAccessRightsGroup(grp);
            taMainRightMapRepo.save(map);
        }
    }

    @Transactional(readOnly = true)
    public ApiResult<List<PartnerVM>> getAllWingsOfTimePartners() throws Exception {
        List<PartnerVM> list = new ArrayList<>();
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("attrName", QCriteria.Operation.EQ, PPMFLGPartnerExtType.WingsOfTimePurchaseEnabled.code));
        criterias.add(new QCriteria("attrValue", QCriteria.Operation.EQ, PPMFLGPartnerExtValueType.TRUE.code));
        criterias.add(new QCriteria("status", QCriteria.Operation.EQ, GeneralStatus.Active.code ));
        criterias.add(new QCriteria("partnerId", QCriteria.Operation.NOTNULL, ""));
        SpecificationBuilder specBuilder = new SpecificationBuilder();
        Specifications querySpec = specBuilder.build(criterias, PPMFLGPartnerExtSpecification.class);
        List<PPMFLGPartnerExt> exts = paExtRepo.findAll(querySpec);
        if(exts != null && exts.size() > 0){
            for(PPMFLGPartnerExt i : exts){
                PPMFLGPartner pa = i.getPartner();
                if(pa != null && com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus.Active.code.equalsIgnoreCase(pa.getStatus())){
                    PartnerVM vm = new PartnerVM();
                    vm.setId(pa.getId());
                    vm.setOrgName(pa.getOrgName());
                    list.add(vm);
                }
            }
        }
        ApiResult<List<PartnerVM>> api = new ApiResult<>();
        api.setData(list);
        api.setSuccess(true);
        api.setTotal(list.size());
        return api;
    }

    @Transactional(readOnly = true)
    public PartnerVM getPartnerVmById(Integer paId) throws Exception {
        PPMFLGPartner pa = paRepo.findById(paId);
        PartnerVM paVm = new PartnerVM(pa);
        log.debug("getDocMapByPaId...");
        List<PartnerDocumentVM> paDocs = paDocSrv.getPartnerDocumentsByPartnerId(pa.getId());

        paVm.setCountryName(ctryRepo.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data,pa.getCountryCode()));
        paVm.setOrgTypeName(paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data,pa.getOrgTypeCode()));
        paVm.setPaDocs(paDocs);
        if(pa.getAccountManagerId()!=null){
            paVm.setAccountManager(adminService.getAdminUserNameByAdminId(pa.getAccountManagerId()));
        }
        String revalFeeStr = revalidFeeSrv.getRevalidationFeeByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG,pa.getAxAccountNumber(),pa.getRevalFeeItemId());
        paVm.setRevalFeeItemId(pa.getRevalFeeItemId());
        paVm.setRevalFeeStr(revalFeeStr);


        String tierId = pa.getTierId();
        if(tierId!=null && !tierId.equals("")) {
            ProductTierVM productTierVM = tierService.getTierVmById(PartnerPortalConst.Partner_Portal_Channel, tierId);
            paVm.setTierLabel(productTierVM.getName());
            paVm.setTierId(tierId);
        }

        PartnerExclProdMapping prodMap = paExclProdSrv.getPartnerExclProdMappingbyPartner(PartnerPortalConst.Partner_Portal_Channel,pa.getId());

        if(prodMap!=null) {
            ProductGridFilterVM gridFilterVM = new ProductGridFilterVM();
            ProdFilter prodFilter = new ProdFilter();
            prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
            prodFilter.setProductLevel(ProductLevels.Exclusive.toString());
            gridFilterVM.setProdFilter(prodFilter);
            List<PartnerProductVM> productVMs = prodSrv.getProdVmsByProdIds(gridFilterVM, prodMap.getExclProdIds(), true);
            paVm.setExcluProds(productVMs);
        }
        PPMFLGTAMainAccount mainAcc = pa.getMainAccount();
        if(mainAcc!=null){
            paVm.setSubAccountEnabled(mainAcc.getSubAccountEnabled());
        }

        List<PPMFLGPaDistributionMapping> distributionMapping =  paDisMapRepo.findByPartnerId(pa.getId());
        if(distributionMapping!=null){
            List<PaDistributionMappingVM> pdmVms = new ArrayList<PaDistributionMappingVM>();
            for(PPMFLGPaDistributionMapping tmpObj:distributionMapping){
                pdmVms.add(new PaDistributionMappingVM(tmpObj));
            }
            paVm.setDistributionMapping(pdmVms);
        }
        populatePartnerDistributionMapping(paVm);
        return paVm;
    }

    @Override
    public List<PartnerVM> getAllPartnersByStatus(String code) {
        List<PartnerVM> vms = new ArrayList<PartnerVM>();
        List<PPMFLGPartner> list = paRepo.findByStatus(code);
        if(list != null && list.size() > 0){
            for(PPMFLGPartner pa : list){
                PartnerVM vm = new PartnerVM();
                vm.setId(pa.getId());
                vm.setOrgName(pa.getOrgName());
                vms.add(vm);
            }
        }
        return vms;
    }
}
