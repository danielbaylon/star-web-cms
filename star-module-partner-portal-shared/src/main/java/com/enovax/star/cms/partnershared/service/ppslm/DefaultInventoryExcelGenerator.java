package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionItemVM;
import com.enovax.star.cms.commons.model.partner.ppslm.TopupItemVM;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jennylynsze on 11/7/16.
 */
@Service
public class DefaultInventoryExcelGenerator implements IInventoryExcelGenerator {

    @Override
    public Workbook generateInventoryeExcel(List<InventoryTransactionItemVM> transItems, String startDateStr, String endDateStr, String availableOnly, String productName) {
        Workbook wb = new XSSFWorkbook();
        Sheet isheet = wb.createSheet("Inventory Item Listing");
        Row irow = null;
        Cell icell = null;

        int rowNum = 0;
        irow = isheet.createRow(rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Tickets Validity Date Range: From "
                + (StringUtils.isEmpty(startDateStr) ? "-" : startDateStr) + " To "
                + (StringUtils.isEmpty(endDateStr) ? "-" : endDateStr));

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Product Name: "
                + (StringUtils.isEmpty(productName) ? "-" : productName));

        if ("on".equals(availableOnly)) {
            irow = isheet.createRow(++rowNum);
            icell = irow.createCell(0);
            icell.setCellValue("Show Available Items Only: Yes");
        }else{
            irow = isheet.createRow(++rowNum);
            icell = irow.createCell(0);
            icell.setCellValue("Show Available Items Only: No");
        }

        rowNum++; //blank row

        irow = isheet.createRow(++rowNum);

        irow = isheet.createRow(++rowNum);
        int cnum = writeReportHeader(irow);
        int rowno = 1;
        for (InventoryTransactionItemVM item : transItems) {
            irow = isheet.createRow(++rowNum);
            writeTransItem(item, wb, irow, rowno++);
            for (TopupItemVM topup : item.getTopupItems()) {
                irow = isheet.createRow(++rowNum);
                writeTopup(topup, irow);
            }

        }
        for (int i = 1; i <= cnum; i++) {
            isheet.autoSizeColumn(i);
        }
        return wb;
    }

    private int writeReportHeader(Row irow) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("No.");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Receipt Number");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Product");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Ticket Type");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Validity Start Date");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Validity End Date");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Original Quantity");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Remaining Quantity");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Amt(S$)");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Status");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Purchased By");
        return cnum;
    }

    private void writeTransItem(InventoryTransactionItemVM item, Workbook wb, Row irow,
                                int rowNum) {
        Cell icell = null;
        int innerCnum = 0;
        icell = irow.createCell(innerCnum);
        icell.setCellValue(rowNum);
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getReceiptNumber());
        icell = irow.createCell(++innerCnum);

        CellStyle cs = wb.createCellStyle();
        cs.setWrapText(true);
        icell.setCellValue(item.getName() + item.getDisplayDetails().replace("<br/>", "\n"));
        icell.setCellStyle(cs);

        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getType());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getValidityStartDateStr());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getValidityEndDateStr());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getQty());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getUnpackagedQty());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(TransactionUtil.priceWithDecimal(item.getSubtotal(),
                null));
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getStatus());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(item.getUsername());
    }

    private void writeTopup(TopupItemVM topup, Row irow) {
        int innerCnum = 0;
        Cell icell = null;
        icell = irow.createCell(innerCnum);
        icell.setCellValue("");
        icell = irow.createCell(++innerCnum);
        icell.setCellValue("");
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getDisplayName());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getTicketType());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getValidateStartDateStr());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getValidityEndDateStr());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getQty());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getUnpackagedQty());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(TransactionUtil.priceWithDecimal(
                topup.getSubTotal(), null));
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getStatus());
        icell = irow.createCell(++innerCnum);
        icell.setCellValue(topup.getUsername());
    }

}
