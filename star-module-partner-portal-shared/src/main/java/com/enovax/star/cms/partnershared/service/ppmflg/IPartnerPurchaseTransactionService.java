package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.model.api.ApiResult;

import java.util.List;

/**
 * Created by jennylynsze on 9/24/16.
 */
public interface IPartnerPurchaseTransactionService {
    void processTransactionReleaseReservation(PPMFLGInventoryTransaction trans);

    void processTransactionReleaseReservation(PPMFLGInventoryTransaction trans, boolean updateTransIncomplete);

    void processTransactionReleaseReservation(int transId);

    void processTransactionReleaseReservation(int transId, boolean updateTransIncomplete);

    List<PPMFLGInventoryTransaction> getTransactionsForStatusUpdate(int timeoutLowerBoundMins,
                                                              int timeoutUpperBoundMins);

    PPMFLGInventoryTransaction confirmTransaction(Integer transId);

    PPMFLGInventoryTransaction getTransaction(int id, boolean includeItems);

    PPMFLGInventoryTransaction getTransaction(String receiptNum, boolean includeItems);

    boolean saveTransaction(PPMFLGInventoryTransaction trans);

    boolean generateAndEmailPurchaseReceipt(PPMFLGInventoryTransaction transParam);

    boolean generateOfflinePaymentReceiptAndEmail(PPMFLGInventoryTransaction transParam, PPMFLGOfflinePaymentRequest req);


    boolean sendVoidEmail(boolean b, PPMFLGInventoryTransaction trans, String email, boolean isSubAccount,
                          PPMFLGTASubAccount subAccount, PPMFLGTAMainAccount mainAccount);

    ApiResult<String> confirmOfflineTransaction(Integer transId,String un);
}
