package com.enovax.star.cms.b2cshared.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;

/**
 * Created by jensen on 12/11/16.
 */
public interface IB2CCartAndDisplayService {

    ApiResult<FunCartDisplay> rebuildCart(StoreApiChannels channel, String sessionId);

    ApiResult<FunCartDisplay> rebuildCart(StoreApiChannels channel, FunCart theCart);

    FunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId);

    FunCartDisplay constructCartDisplay(FunCart cart);

    ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber, boolean isAdmin);

    ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber);

    ReceiptDisplay getReceiptForDisplay(StoreTransaction txn);
}
