package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGOfflinePaymentRequest;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentNotifyVM;

/**
 * Created by houtao on 7/9/16.
 */
public interface IOfflinePaymentEmailService {

    void sendingOfflinePaymentRequestSubmissionEmail(OfflinePaymentNotifyVM vm, PPMFLGOfflinePaymentRequest request, PPMFLGInventoryTransaction txn);

    void sendingOfflinePaymentRequestApprovedEmail(OfflinePaymentNotifyVM vm, PPMFLGOfflinePaymentRequest request, PPMFLGInventoryTransaction txn);

    void sendingOfflinePaymentRequestRejectedEmail(OfflinePaymentNotifyVM vm, PPMFLGOfflinePaymentRequest request, PPMFLGInventoryTransaction txn);

    void sendingOfflinePaymentCustomerNotification(OfflinePaymentNotifyVM vm, PPMFLGOfflinePaymentRequest request, PPMFLGInventoryTransaction txn);

    void sendingOfflinePaymentPartnerToAdminNotification(OfflinePaymentNotifyVM vm, PPMFLGOfflinePaymentRequest request, PPMFLGInventoryTransaction txn);
}
