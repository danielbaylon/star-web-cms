package com.enovax.star.cms.b2cshared.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jensen on 15/11/16.
 */
public interface IB2CSysParamService {
    @Transactional(readOnly = true)
    String getSystemParamValueByKey(StoreApiChannels channel, String key);

    @Transactional(readOnly = true)
    String getSystemParamValueByKey(String channelCode, String key);
}
