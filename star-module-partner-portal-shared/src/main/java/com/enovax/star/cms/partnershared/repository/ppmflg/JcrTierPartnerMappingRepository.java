package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 5/25/16.
 */
@Repository("PPMFLGITierPartnerMappingRepository")
public class JcrTierPartnerMappingRepository implements ITierPartnerMappingRepository {

    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node partnerTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/tier-partner-mappings");
            int currId = Integer.parseInt(partnerTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            partnerTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), partnerTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    /*@Override
    public List<TierPartnerMapping> getPartnerList(Integer tierId) {
        List<TierPartnerMapping> tierPartnerMappings = new ArrayList<>();
        String query = "//element(*, mgnl:content)[@tierId = '" + tierId + "']";
        try {
            Iterable<Node> tierPartnerMappingNodes = JcrRepository.query(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), JcrWorkspace.TierPartnerMappings.getNodeType(), query);
            Iterator<Node> tierPartnerMappingNodesIterator = tierPartnerMappingNodes.iterator();

            while(tierPartnerMappingNodesIterator.hasNext()) {
                Node mappingNode = tierPartnerMappingNodesIterator.next();
                TierPartnerMapping mapping = new TierPartnerMapping(mappingNode);

                Node tierNode = JcrRepository.getParentNode(JcrWorkspace.ProductTiers.getWorkspaceName(), "/" + mapping.getTierId());
                mapping.setProductTier(new ProductTier(tierNode));

                Node partnerNode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/" + mapping.getPartnerId());
                //mapping.setPartner(new PartnerVM(new Partner(partnerNode)));

                tierPartnerMappings.add(mapping);
            }

            return tierPartnerMappings;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }*/

}
