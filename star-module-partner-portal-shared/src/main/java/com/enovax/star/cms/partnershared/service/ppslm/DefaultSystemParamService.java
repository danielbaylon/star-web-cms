package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.model.system.SysParamVM;
import com.enovax.star.cms.commons.util.IpAddressParamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

@Service
public class DefaultSystemParamService implements ISystemParamService {

    @Autowired
    private ISystemParamRepository sysParamRepo;

    public static final String SYS_PARAM_RECAPTCHA_ENABLED = "sys.param.recapcha.enabled";
    public static final String SYS_PARAM_RECAPTCHA_PRIVATE_KEY = "sys.param.recapcha.private.key";
    public static final String SYS_PARAM_RECAPTCHA_PUBLIC_KEY = "sys.param.recapcha.public.key";
    public static final String SYS_PARAM_RECAPTCHA_REQUEST_IP = "sys.param.recapcha.request.ip";
    public static final String SYS_PARAM_PARTNER_DOCUMENT_DIR = "sys.param.partner.document.repo.dir";
    public static final String SYS_PARAM_PARTNER_DOCUMENT_AX_DIR = "sys.param.partner.document.ax.dir";
    public static final String SYS_PARAM_MAIL_SERVER_CFG = "sys.param.mail.server.config";
    public static final String SYS_PARAM_PARTNER_PORTAL_ADMIN_EMAILS_CFG = "sys.param.admin.emails.cfg";
    public static final String SYS_PARAM_PARTNER_PORTAL_WOT_CUTOFF_TIME = "sys.param.partner.wot.cutoff.time";
    public static final String SYS_PARAM_PARTNER_PORTAL_WOT_RESERVATION_TYPE = "sys.param.partner.wot.reservation.type";
    public static final String SYS_PARAM_PARTNER_DOCUMENT_MAXIMUM_SIZE = "sys.param.partner.document.maximum.size";
    //public static final String SYS_PARAM_PARTNER_AX_CUSTOMER_GROUP = "sys.param.partner.ax.customer.group";
    public static final String SYS_PARAM_PARTNER_AX_CUSTOMER_OWNER_SENTOSA_ORG_NAME = "sys.param.partner.owner.sentosa.organization.name";
    public static final String SYS_PARAM_PARTNER_DOCUMENT_SUPPORTED_DOC_TYPES = "sys.param.partner.document.supported.filetype";
    public static final String SYS_PARAM_TICKET_REVLIDATION_PERIOD = "sys.param.ticket.revalidation.period";
    public static final String SYS_PARAM_TICKET_EXPIRING_ALERT_PERIOD = "sys.param.ticket.expiring.alert.period";
    public static final String SYS_PARAM_APP_CONTEXT_ROOT = "sys.param.app.context.root";
    public static final String SYS_PARAM_ADMIN_APP_CONTEXT_ROOT = "sys.param.admin.app.context.root";
    public static final String SYS_PARAM_APP_CONTEXT_PATH = "sys.param.app.context.path";
    public static final String SYS_PARAM_APP_LOGO_PATH = "sys.param.app.logo.path";

    public static final String SYS_PARAM_AX_PARTNER_REGISTRATION_REQUIRED = "sys.param.ax.partner.registration.config";
    public static final String SYS_PARAM_AX_PARTNER_REGISTRATION_SERVICE_AVAILABLE = "sys.param.ax.partner.registration.service.available";
    public static final String SYS_PARAM_PARTNER_REGISTRATION_MAIL_RECEIVER = "sys.param.partner.registration.notification.email.receiver";
    public static final String SYS_PARAM_SENTOSA_CONTACT_DETAILS = "sys.param.partner.registration.sentosa.contact.details";
    public static final String SYS_PARAM_SENTOSA_AX_FTP_CONFIG = "sys.param.ax.ftp.configs";
    public static final String SYS_PARAM_PARTNER_TRANSACTION_RECEIPT_DIR = "sys.param.partner.transaction.receipt.dir";
    public static final String SYS_PARAM_PARTNER_DEPOSIT_MINIMUM_TOPUP_AMT = "sys.param.partner.deposit.topup.minimum.amount";
    public static final String SYS_PARAM_PARTNER_DEPOSIT_AX_TENDER_TYPE_ID = "sys.param.partner.deposit.topup.ax.tender.type.id";
    public static final String SYS_PARAM_MAX_SUBUSERS = "sys.param.max.subusers";
    public static final String SYS_PARAM_MIN_NO_OF_TICKETS_PURCHASE_PER_TXN = "sys.param.min.tickets.purchase.per.transaction";
    public static final String SYS_PARAM_MAX_LIMIT_PER_TRANSACTION = "sys.param.max.limit.per.transaction";
    public static final String SYS_PARAM_MAX_TOTAL_DAILY_TRANSACTION = "sys.param.max.total.daily.transaction";
    public static final String SYS_PARAM_MAX_TYPE_OF_ITEMS_IN_PACKAGE = "sys.param.max.type.of.items.in.package";
    public static final String SYS_PARAM_TICKET_ALLOW_REVALIDATE_PERIOD = "sys.param.ticket.allow.revalidate.period";
    public static final String SYS_PARAM_WOT_ADMIN_RESERVATION_CAP = "sys.param.admin.wot.reservation.cap";

    public static final String SYS_PARAM_WOT_ADMIN_RESERVATION_PERIOD_LIMIT = "sys.param.admin.wot.reservation.period.limit";
    public static final String SYS_PARAM_WOT_ENABLE_ETICKET = "sys.param.partner.wot.enable.eticket";
    public static final String SYS_PARAM_WOT_ADVANCED_DAYS_RELEASE_BACKEND_RESERVED_TICKETS = "sys.param.partner.wot.advanced.days.release.backend.reservation";
    public static final String SYS_PARAM_WOT_ADVANCED_HOURS_CANCEL_RESERVATION_EMAIL_REMINDER = "sys.param.partner.wot.advanced.no.of.hours.cancel.reservation.email.reminder";

    public static final String SYS_PARAM_WOT_PARTNER_RESERVATION_CAP = "sys.param.partner.reservation.cap";

    public static final String SYS_PARAM_PAYMENT_TIME = "sys.param.partner.no.of.minutes.reservation.druration.of.payment";
    public static final String SYS_PARAM_TICKET_ONHOLD_TIME = "sys.param.partner.no.of.minutes.reservation.druration.of.tickets";

    //Purchase
    public static final String SYS_PARAM_PURCHASE_MIN_QTY_PER_TXN = "sys.param.purchase.min.qty.per.txn";
    public static final String SYS_PARAM_PURCHASE_MAX_DAILY_TXN_LIMIT ="sys.param.purchase.max.daily.txn.limit";
    public static final String SYS_PARAM_PURCHASE_MAX_AMOUNT_PER_TXN = "sys.param.purchase.max.amount.per.txn";

    public static final String sys_param_batch_ppslm_auto_reupdate_customer_extensions_job_enabled="sys.param.batch.ppslm.auto.reupdate.customer.extensions.job.enabled";
    public static final String sys_param_batch_wot_refresh_unredeemed_pin_code_job_enabled="sys.param.batch.wot.refresh.unredeemed.pincode.job.enabled";
    public static final String sys_param_batch_wot_backend_reservation_reminder_and_release_job_enabled="sys.param.batch.wot.backend.reservation.reminder.and.release.job.enabled";
    public static final String sys_param_batch_wot_unredeemed_pincode_cancel_job_enabled="sys.param.batch.wot.unredeemed.pincode.cancel.job.enabled";
    public static final String sys_param_batch_ax_price_group_job_enabled="sys.param.batch.ax.price.group.job.enabled";
    public static final String sys_param_batch_ax_line_of_business_job_enabled="sys.param.batch.ax.line.of.business.job.enabled";
    public static final String sys_param_batch_ax_country_region_sync_job_enabled="sys.param.batch.ax.country.region.sync.job.enabled";
    public static final String sys_param_batch_deopit_topup_telemoney_query_job_enabled = "sys.param.batch.deposit.topup.telemoney.query.job.enabled";
    public static final String sys_param_batch_deopit_topup_telemoney_query_lowerBoundMin_after_request_sent = "sys.param.batch.deposit.topup.telemoney.query.lowerBoundMins.after.telemoney.request.sent";
    public static final String sys_param_batch_deopit_topup_telemoney_query_upperBoundMin_after_request_sent = "sys.param.batch.deposit.topup.telemoney.query.upperBoundMins.after.telemoney.request.sent";

    public static final String SYS_PARAM_RECS_PER_PAGE = "sys.param.recs.per.page";

    public static final String SYS_PARAM_PKG_OVERDUE_CLEAN_TIME = "sys.param.pkg.overdue.clean.time";

    @Override
    public boolean isRecaptchaEnabled(){
        String value = getSystemParamValueByKey(SYS_PARAM_RECAPTCHA_ENABLED);
        return value != null && value.trim().length() > 0 && "Y".equalsIgnoreCase(value.trim());
    }
    @Override
    public String getRecaptchaPrivateKey() {
        return getSystemParamValueByKey(SYS_PARAM_RECAPTCHA_PRIVATE_KEY);
    }
    @Override
    public String getRecaptchaPublicKey() {
        return getSystemParamValueByKey(SYS_PARAM_RECAPTCHA_PUBLIC_KEY);
    }

    @Override
    public String getUserRequestAddress(HttpServletRequest request) {
        if (request != null) {
            String ip = null;
            String xForwardFor = request.getHeader("X-FORWARDED-FOR");
            if(xForwardFor != null && xForwardFor.trim().length() > 0){
                ip = IpAddressParamUtil.getValidCustIp(xForwardFor);
            }
            if(ip != null && ip.trim().length() > 0){
                return ip.trim();
            }
            String remoteAddr = request.getRemoteAddr();
            if (!"0:0:0:0:0:0:0:1".equalsIgnoreCase(remoteAddr)) {
                return remoteAddr;
            }
        }
        return getSystemParamValueByKey(SYS_PARAM_RECAPTCHA_REQUEST_IP);
    }

    @Override
    public String getUserRequestHeaderAddress(HttpServletRequest request) {
        String ip = null;
        String xForwardFor = request.getHeader("X-FORWARDED-FOR");
        if(xForwardFor != null && xForwardFor.trim().length() > 0){
            ip = IpAddressParamUtil.getValidCustIp(xForwardFor);
        }
        if(ip != null && ip.trim().length() > 0){
            return ip.trim();
        }
        return request.getRemoteAddr();
    }

    @Override
    public String isAXPartnerRegistrationServiceAvailable() {
        return getSystemParamValueByKey(SYS_PARAM_AX_PARTNER_REGISTRATION_SERVICE_AVAILABLE);
    }

    @Override
    public String getPartnerDocumentRootDir(){
        return getSystemParamValueByKey(SYS_PARAM_PARTNER_DOCUMENT_DIR);
    }

    @Override
    public String getPartnerDocumentAxRootDir() {
        return getSystemParamValueByKey(SYS_PARAM_PARTNER_DOCUMENT_AX_DIR);
    }

    @Override
    public Map<String, String> getEmailConfigs() {
        return getSystemParamAsMapByKey(SYS_PARAM_MAIL_SERVER_CFG);
    }

    @Override
    public Map<String, String> getAdminEmailCfgs() {
        return getSystemParamAsMapByKey(SYS_PARAM_PARTNER_PORTAL_ADMIN_EMAILS_CFG);
    }

    @Override
    public long getPartnerDocumentMaximumSizeLimit() {
        return Long.valueOf(getSystemParamValueByKey(SYS_PARAM_PARTNER_DOCUMENT_MAXIMUM_SIZE));
    }

    @Override
    public String getPartnerDocumentSupportedFileType() {
        return getSystemParamValueByKey(SYS_PARAM_PARTNER_DOCUMENT_SUPPORTED_DOC_TYPES);
    }

    @Override
    public int getTicketRevalidationPeriod() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_TICKET_REVLIDATION_PERIOD));
    }

    @Override
    public SysParamVM getTicketRevalidationPeriodVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_TICKET_REVLIDATION_PERIOD);
    }

    @Override
    public int getPurchaseMinQtyPerTxn() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_PURCHASE_MIN_QTY_PER_TXN));
    }

    @Override
    public BigDecimal getPurchaseMaxDailyTxnLimit() {
        return new BigDecimal(getSystemParamValueByKey(SYS_PARAM_PURCHASE_MAX_DAILY_TXN_LIMIT));
    }

    @Override
    public BigDecimal getPurchaseMaxAmountPerTxn() {
        return new BigDecimal(getSystemParamValueByKey(SYS_PARAM_PURCHASE_MAX_AMOUNT_PER_TXN));
    }

    @Override
    public int getTicketAllowRevalidatePeriod() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_TICKET_ALLOW_REVALIDATE_PERIOD));
    }

    @Override
    public SysParamVM getTicketAllowRevalidatePeriodVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_TICKET_ALLOW_REVALIDATE_PERIOD);
    }

    @Override
    public String getApplicationContextPath() {
        return getSystemParamValueByKey(SYS_PARAM_APP_CONTEXT_ROOT);
    }

    @Override
    public String getAdminApplicationContextPath() {
        return getSystemParamValueByKey(SYS_PARAM_ADMIN_APP_CONTEXT_ROOT);
    }

    @Override
    public String getPartnerPortalSLMRootPath() {
        return getSystemParamValueByKey(SYS_PARAM_APP_CONTEXT_PATH);
    }

    @Override
    public String getPartnerPortalSLMLogoPath() {
        return getSystemParamValueByKey(SYS_PARAM_APP_LOGO_PATH);
    }

    @Override
    public int getRecsPerPage() {
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_RECS_PER_PAGE));
    }


    public String getSystemParamValueByKey(String key) {
        return sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, key);
    }

    public boolean getSystemParamBooleanValueByKey(String key){
        String val = getSystemParamValueByKey(key);
        if(val != null && val.trim().length() > 0){
            if("Y".equalsIgnoreCase(val.trim())){
                return true;
            }
        }
        return false;
    }

    private Map<String,String> getSystemParamAsMapByKey(String key) {
        return sysParamRepo.getSystemParamAsMapByKey(PartnerPortalConst.Partner_Portal_Channel, key);
    }

    private SysParamVM getSystemParamAsVMByKey(String key) {
        return sysParamRepo.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, key);
    }

    @Override
    public String getDefaultPartnerRegistrationEmailReceiver() {
        return sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_PARTNER_REGISTRATION_MAIL_RECEIVER);
    }

    public Map<String, String> getSentosaContactDetails(){
        return sysParamRepo.getSystemParamAsMapByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_SENTOSA_CONTACT_DETAILS);
    }

    @Override
    public Map<String, String> getAxFtpConfigs() {
        return sysParamRepo.getSystemParamAsMapByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_SENTOSA_AX_FTP_CONFIG);
    }

    @Override
    public int getPartnerLoginMaximumAttempts() {
        return 3;
    }

    @Override
    public int getUserPasswordValidationMaximumPastPasswords() {
        return 5;
    }


    @Override
    public int getWingsOfTimeShowCutoffInMinutes() {
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_PARTNER_PORTAL_WOT_CUTOFF_TIME));
    }

    @Override
    public SysParamVM getWingsOfTimeShowCutoffInMinutesVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_PARTNER_PORTAL_WOT_CUTOFF_TIME);
    }

    @Override
    public String getPartnerOwnerOrgName() {
        return sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_PARTNER_AX_CUSTOMER_OWNER_SENTOSA_ORG_NAME);
    }

    @Override
    public Integer getWingsOfTimeReservationType() {
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_PARTNER_PORTAL_WOT_RESERVATION_TYPE));
    }

    /*
    public Integer getWoTAdminReservationCap(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_WOT_ADMIN_RESERVATION_CAP));
    }
    */

    @Override
    public SysParamVM getWoTAdminReservationCapVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_ADMIN_RESERVATION_CAP);
    }

    public Integer getWotAdminReservationPeriodLimit(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_WOT_ADMIN_RESERVATION_PERIOD_LIMIT));
    }

    @Override
    public SysParamVM getWotAdminReservationPeriodLimitVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_ADMIN_RESERVATION_PERIOD_LIMIT);
    }

    public boolean getWoTeTicketEnabled(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_WOT_ENABLE_ETICKET)) == 1;
    }

    @Override
    public SysParamVM getWoTeTicketEnabledVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_ENABLE_ETICKET);
    }

    public Integer getPartnerWoTAdvancedDaysReleaseBackendReservedTickets(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_WOT_ADVANCED_DAYS_RELEASE_BACKEND_RESERVED_TICKETS));
    }

    @Override
    public SysParamVM getPartnerWoTAdvancedDaysReleaseBackendReservedTicketsVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_ADVANCED_DAYS_RELEASE_BACKEND_RESERVED_TICKETS);
    }

    public Integer getPartnerWoTReservationCap(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_WOT_PARTNER_RESERVATION_CAP));
    }

    @Override
    public SysParamVM getPartnerWoTReservationCapVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_PARTNER_RESERVATION_CAP);
    }

    public Integer getPartnerOnlinePaymentTime(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_PAYMENT_TIME));
    }

    @Override
    public SysParamVM getPartnerOnlinePaymentTimeVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_PAYMENT_TIME);
    }

    public Integer getPartnerTicketonHoldTime(){
        return Integer.valueOf(sysParamRepo.getSystemParamValueByKey(PartnerPortalConst.Partner_Portal_Channel, SYS_PARAM_TICKET_ONHOLD_TIME));
    }

    @Override
    public SysParamVM getPartnerTicketonHoldTimeVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_TICKET_ONHOLD_TIME);
    }

    @Override
    public String getPartnerTransactionReceiptRootDir() {
        return getSystemParamValueByKey(SYS_PARAM_PARTNER_TRANSACTION_RECEIPT_DIR);
    }

    @Override
    public int getTicketExpiringAlertPeriod() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_TICKET_EXPIRING_ALERT_PERIOD));
    }

    @Override
    public SysParamVM getTicketExpiringAlertPeriodVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_TICKET_EXPIRING_ALERT_PERIOD);
    }

    @Override
    public int getMaxNoOfSubUsers() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_MAX_SUBUSERS));
    }

    @Override
    public SysParamVM getMaxNoOfSubUsersVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_MAX_SUBUSERS);
    }

    @Override
    public int getMinimumNoOfTicketsPurchasePerTxn() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_MIN_NO_OF_TICKETS_PURCHASE_PER_TXN));
    }

    @Override
    public SysParamVM getMinimumNoOfTicketsPurchasePerTxnVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_MIN_NO_OF_TICKETS_PURCHASE_PER_TXN);
    }

    @Override
    public int getMaxTypeOfItemsinPackage() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_MAX_TYPE_OF_ITEMS_IN_PACKAGE));
    }

    @Override
    public SysParamVM getMaxTypeOfItemsinPackageVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_MAX_TYPE_OF_ITEMS_IN_PACKAGE);
    }

    @Override
    public int getMaxLimitPerTransaction() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_MAX_LIMIT_PER_TRANSACTION));
    }

    @Override
    public SysParamVM getMaxLimitPerTransactionVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_MAX_LIMIT_PER_TRANSACTION);
    }

    @Override
    public int getMaxTotalDailyTransaction() {
        return Integer.valueOf(getSystemParamValueByKey(SYS_PARAM_MAX_TOTAL_DAILY_TRANSACTION));
    }

    @Override
    public SysParamVM getMaxTotalDailyTransactionVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_MAX_TOTAL_DAILY_TRANSACTION);
    }

    @Override
    public Long getDefaultPartnerRegistrationCustomerType() {
        return Long.parseLong(getSystemParamValueByKey("ax.crt.api.partner.registration.customer.type.value"));
    }

    @Override
    public String getDefaultPartnerRegistrationCurrencyCode() {
        return getSystemParamValueByKey("ax.crt.api.partner.registration.currency.code");
    }

    @Override
    public String getDefaultPartnerRegistrationShortCurrencyCode() {
        return getSystemParamValueByKey("ax.crt.api.partner.registration.short.currency.code");
    }

    @Override
    public Integer getPartnerRegistrationBizAddrType() {
        return Integer.parseInt(getSystemParamValueByKey("ax.crt.api.partner.registration.business.address.type"));
    }

    @Override
    public Integer getPartnerRegistrationInvoiceAddressType() {
        return Integer.parseInt(getSystemParamValueByKey("ax.crt.api.partner.registration.correspondence.address.type"));
    }

    @Override
    public boolean getPartnerRegistrationAllowOnAccount() {
        return 1  == Integer.parseInt(getSystemParamValueByKey("ax.crt.api.partner.registration.allowOnAccount"));
    }

    @Override
    public String getPartnerRegistrationAxDocumentTypeId() {
        return getSystemParamValueByKey("ax.crt.api.partner.registration.documentTypeId");
    }

    @Override
    public Integer getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminder() {
        String str = getSystemParamValueByKey(SYS_PARAM_WOT_ADVANCED_HOURS_CANCEL_RESERVATION_EMAIL_REMINDER);
        if(str != null && str.trim().length() > 0){
            return Integer.parseInt(str);
        }
        return null;
    }

    @Override
    public SysParamVM getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminderVM() {
        return getSystemParamAsVMByKey(SYS_PARAM_WOT_ADVANCED_HOURS_CANCEL_RESERVATION_EMAIL_REMINDER);
    }


    @Override
    public float getMinimumDepositTopupAmount() throws BizValidationException {
        String str = getSystemParamValueByKey(SYS_PARAM_PARTNER_DEPOSIT_MINIMUM_TOPUP_AMT);
        if(str == null || str.trim().length() == 0){
            throw new BizValidationException("Deposit top-up configuration not found.");
        }
        return Float.parseFloat(str.trim());
    }

    @Override
    public String getDepositTopupAxPaymentType() {
        return getSystemParamValueByKey(SYS_PARAM_PARTNER_DEPOSIT_AX_TENDER_TYPE_ID);
    }


    @Override
    public boolean isPPSLMWoTRefreshUnredeemedPinCodeJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_wot_refresh_unredeemed_pin_code_job_enabled);
    }

    @Override
    public boolean isPPSLMWoTBackendReservationReminderAndReleaseJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_wot_backend_reservation_reminder_and_release_job_enabled);
    }

    @Override
    public boolean isPPSLMAxPriceGroupSyncJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_ax_price_group_job_enabled);
    }

    @Override
    public boolean isPPSLMAxLineOfBusinessJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_ax_line_of_business_job_enabled);
    }

    @Override
    public boolean isPPSLMAxCountryRegionSyncJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_ax_country_region_sync_job_enabled);
    }

    @Override
    public boolean isPPSLMReUpdateCustomerExtensionsJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_ppslm_auto_reupdate_customer_extensions_job_enabled);
    }

    @Override
    public boolean isPPSLMWoTUnredeemedPinCodeCanncelReminderJob() {
        return getSystemParamBooleanValueByKey(sys_param_batch_wot_unredeemed_pincode_cancel_job_enabled);
    }

    @Override
    public boolean isPPSLMDepositTopupTelemoneyQueryJobEnabled() {
        return getSystemParamBooleanValueByKey(sys_param_batch_deopit_topup_telemoney_query_job_enabled);
    }

    @Override
    public int getDepositTopupTeleemoneyQuerySentLowerBoundMinutesAfterRequestSent() {
        return Integer.parseInt(getSystemParamValueByKey(sys_param_batch_deopit_topup_telemoney_query_lowerBoundMin_after_request_sent));
    }
    @Override
    public int getDepositTopupTeleemoneyQuerySentUpperBoundMinutesAfterRequestSent() {
        return Integer.parseInt(getSystemParamValueByKey(sys_param_batch_deopit_topup_telemoney_query_upperBoundMin_after_request_sent));
    }

    @Override
    public boolean isTMServiceEnabled() {
        String str = sysParamRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_TM_SERVICE_KEY);
        if(str != null && str.trim().length() > 0){
            if(Boolean.TRUE.toString().equalsIgnoreCase(str.trim())){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPackageOverdueCleanTime() {
        return Integer.parseInt(getSystemParamValueByKey(SYS_PARAM_PKG_OVERDUE_CLEAN_TIME));
    }
}