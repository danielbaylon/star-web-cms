package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMReason;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMReasonLogMap;
import com.enovax.star.cms.commons.model.partner.ppslm.ReasonVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPPSLMReasonLogMapRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMReasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class DefaultReasonService implements IReasonService {

    @Autowired
    private PPSLMReasonRepository reasonRepo;

    @Autowired
    private PPSLMPPSLMReasonLogMapRepository reasonLogMapRepo;

    @Transactional(readOnly = true)
    public List<ReasonVM> populateActiveReasonVms() {
        List<ReasonVM> vms = new ArrayList<ReasonVM>();
        List<PPSLMReason> lst = reasonRepo.findAllReasonByStatus(GeneralStatus.Active.code);
        if(lst != null && lst.size() > 0){
            for(PPSLMReason i : lst){
                vms.add(new ReasonVM(i));
            }
        }
        return vms;
    }

    @Transactional(readOnly = true)
    public List<ReasonVM> populateReasonsByIds(List<Integer> reasonIds) {
        List<ReasonVM> reasonVMs = new ArrayList<ReasonVM>();
        if(reasonIds != null && reasonIds.size() > 0){
            for(Integer id :reasonIds){
                PPSLMReason reason  = reasonRepo.findById(id);
                if(reason != null) {
                    reasonVMs.add(new ReasonVM(reason));
                }
            }
        }
        return reasonVMs;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReasonVM> getReasonsByLogId(Integer logId){
        List<PPSLMReasonLogMap> reasonMaps = reasonLogMapRepo.findByLogId(logId);
        List<ReasonVM> reasonVms = new ArrayList<ReasonVM>();
        for(PPSLMReasonLogMap reasonMap:reasonMaps){
            reasonVms.add(new ReasonVM(reasonMap.getReason()));
        }
        return reasonVms;
    }


    @Override
    @Transactional(readOnly = true)
    public ResultVM getReasonsbyPage(String orderBy, String orderWith, Integer page, Integer pagesize){
        ResultVM res = new ResultVM();
        if(orderWith == null) {
            orderWith = "DESC";
            orderBy = "id";
        }
        PageRequest pageRequest = new PageRequest( page - 1, pagesize,
                "ASC".equalsIgnoreCase(orderWith) ? Sort.Direction.ASC : Sort.Direction.DESC,
                orderBy);
        Page<PPSLMReason> reasonByPage = reasonRepo.getReasonByPage(pageRequest);
        Iterator<PPSLMReason> reasonIterator = reasonByPage.iterator();
        List<ReasonVM> reasonVMs=new ArrayList<>();
        while(reasonIterator.hasNext()) {
            PPSLMReason reason = reasonIterator.next();
            ReasonVM reasonVM = new ReasonVM(reason);
            reasonVMs.add(reasonVM);
        }
        int total = reasonRepo.getReasonSize();
        res.setViewModel(reasonVMs);
        res.setTotal(total);
        return res;
    }

    @Override
    @Transactional
    public ReasonVM saveReason(ReasonVM reasonVm, String userNm) {
        PPSLMReason reason = null;
        if(reasonVm.getId()!=null&&reasonVm.getId()>0){
            reason = reasonRepo.findById(reasonVm.getId());
        }else{
            reason = new PPSLMReason();
            reason.setCreatedBy(userNm);
            reason.setCreatedDate(new Date());
        }
        reason.setTitle(reasonVm.getTitle());
        reason.setContent(reasonVm.getContent());
        reason.setStatus(reasonVm.getStatus());
        reason.setModifiedBy(userNm);
        reason.setModifiedDate(new Date());
        reasonRepo.save(reason);
        return new ReasonVM(reason);
    }

    @Override
    @Transactional
    public void removeReasons(List<Integer> rcmdIds, String userNm) {
        for(Integer id:rcmdIds){
            PPSLMReason reason = reasonRepo.findById(id);
            reason.setStatus(GeneralStatus.Deleted.code);
            reason.setModifiedBy(userNm);
            reason.setModifiedDate(new Date());
            reasonRepo.save(reason);
        }
    }

}
