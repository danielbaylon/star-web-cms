package com.enovax.star.cms.b2cshared.repository;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;

public interface IB2CBookingRepository {

    void saveStoreTransaction(StoreApiChannels channel, StoreTransaction txn);

    StoreTransaction getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber);
}
