package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;

/**
 * Created by jennylynsze on 10/10/16.
 */
public interface IPartnerRevalidatePurchaseService {

   PPSLMRevalidationTransaction getRevalInfoBeforePayment(Integer transId,
                                                             Integer adminId, String accname, boolean isAdmin) throws Exception;
    ResultVM revalTransWithFreeCharge(Integer revalTransId);

}
