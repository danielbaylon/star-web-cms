package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccountVM;

import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 8/9/16.
 */
public interface IPartnerAccountSharedService {

    String getPartnerAccountNameByIdWithCache(Map<String, String> nameMapping, boolean subAccountTrans, String createdBy);

    PPMFLGTAMainAccount getActivePartnerMainAccountByPartnerId(Integer partnerId);

    PPMFLGTAMainAccount getActivePartnerByMainAccountId(Integer mainAccountId);

    PPMFLGTAMainAccount getPartnerMainAccountByMainAccountId(Integer mainAccId);

    public com.enovax.star.cms.commons.model.api.ApiResult<String> saveSubuser(int paId, boolean isNew, Integer userId, String userName, String name, String email, String title, String status, String access, String createdBy, String pwd, String encodedPwd) throws Exception;

    public boolean hasSubAccountAdminPermission(Integer mainAccId, Integer loginId, boolean isSubAccount);

    ApiResult<String> savePartnerSubAccountRightsChanges(Integer mainAccId, String loginUserName, List<PartnerAccountVM> accounts) throws BizValidationException;
}
