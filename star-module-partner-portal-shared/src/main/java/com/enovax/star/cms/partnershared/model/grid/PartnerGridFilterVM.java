package com.enovax.star.cms.partnershared.model.grid;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class PartnerGridFilterVM extends BaseGridFilterVM {
    private String ptfilterJson;
    private PartnerFilter partnerFilter;

    public String getPtfilterJson() {
        return ptfilterJson;
    }

    public void setPtfilterJson(String ptfilterJson) {
        this.ptfilterJson = ptfilterJson;
    }


    public PartnerFilter getPartnerFilter() {
        return partnerFilter;
    }

    public void setPartnerFilter(PartnerFilter partnerFilter) {
        this.partnerFilter = partnerFilter;
    }
}
