package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;

/**
 * Created by jennylynsze on 10/10/16.
 */
public interface IPartnerRevalidatePurchaseService {

   PPMFLGRevalidationTransaction getRevalInfoBeforePayment(Integer transId,
                                                             Integer adminId, String accname, boolean isAdmin) throws Exception;
    ResultVM revalTransWithFreeCharge(Integer revalTransId);

}
