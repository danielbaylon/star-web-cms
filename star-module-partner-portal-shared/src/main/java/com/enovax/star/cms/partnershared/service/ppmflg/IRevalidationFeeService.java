package com.enovax.star.cms.partnershared.service.ppmflg;


import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.RevalFeeItemVM;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by houtao on 26/7/16.
 */
public interface IRevalidationFeeService {

    BigDecimal getRevalidationFeeInBigDecimalByRevalidationFeeId(StoreApiChannels channel, String axAccNum, String revalFeeItemId);

    String getRevalidationFeeByRevalidationFeeId(StoreApiChannels channel, String axAccNum, String revalFeeItemId);

    public List<RevalFeeItemVM> populdateActiveRevalidationFeeItemVMs(StoreApiChannels channel, String axAccNum);

    public ResultVM saveRevalidationFeeItem(StoreApiChannels channel,RevalFeeItemVM revalFeeItemVM);

    public ResultVM removeRevalidationFeeItem(StoreApiChannels channel,String feeItemIds);

}
