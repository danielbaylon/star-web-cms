package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionItemVM;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by jennylynsze on 11/7/16.
 */
public interface IInventoryExcelGenerator {
    Workbook generateInventoryeExcel(
            List<InventoryTransactionItemVM> transItems,
            String startDateStr, String endDateStr, String availableOnly,
            String productName);
}
