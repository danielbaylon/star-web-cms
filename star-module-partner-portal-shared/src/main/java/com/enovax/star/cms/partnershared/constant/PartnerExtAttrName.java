package com.enovax.star.cms.partnershared.constant;

/**
 * Created by houtao on 20/9/16.
 */
public enum PartnerExtAttrName {

    WingsOfTimePurchaseEnabled("wotReservationEnabled"),
    WoTReservationCap("wotReservationCap"),
    WoTReservationCapValidUntil("wotReservationValidUntil"),
    DepositEnabled("depoistEnabled"),
    OfflinePaymentEnabled("offlinePaymentEnabled");

    public final String code;

    private PartnerExtAttrName(String code){
        this.code = code;
    }

}
