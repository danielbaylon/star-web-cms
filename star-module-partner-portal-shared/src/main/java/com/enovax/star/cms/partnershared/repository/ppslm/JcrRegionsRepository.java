package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxRegion;
import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jennylynsze on 12/29/16.
 */
@Service
public class JcrRegionsRepository implements IRegionsRepository {

    private static final String REGIONS = "regions";

    private static volatile int list_refresh_count = 0;
    private static volatile List<AxRegion> list = new ArrayList<>();

    @Override
    public boolean initRegion(String channel) {
        try {
            if(!JcrRepository.nodeExists(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase())) {
                Node node = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/", channel.toLowerCase(), JcrWorkspace.CMSConfig.getNodeType());
                try {
                    PublishingUtil.publishNodes(node.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if(!JcrRepository.nodeExists(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase() + "/" + REGIONS)) {
                Node node = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase() + "/", REGIONS, JcrWorkspace.CMSConfig.getNodeType());
                try {
                    PublishingUtil.publishNodes(node.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return true;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean saveRegion(String channel, AxRegion info) {
        Node subnode = null;

        try {
            subnode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase() + "/" + REGIONS + "/" + info.getRegionId());
        }catch (RepositoryException e)  {
            //create the subnode
            try {
                subnode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + REGIONS + "/",  info.getRegionId(), "mgnl:content");
            } catch (RepositoryException e1) {
                e1.printStackTrace();
            }
        }

        if(subnode== null) {
            return false;
        }

        try {
            subnode.setProperty("regionId", info.getRegionId());
            subnode.setProperty("description", info.getDescription());

            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), subnode);

            try{
                PublishingUtil.publishNodes(subnode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
                return true;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    public List<AxRegion> getRegionList(String channel) {
        List<AxRegion> list = new ArrayList<AxRegion>();
        try {
            AxRegion i = null;
            Node root = null;
            root = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase() + "/" + REGIONS);
            if (root != null) {
                NodeIterator iter = root.getNodes();
                if (iter != null) {
                    while (iter.hasNext()) {
                        Node node = iter.nextNode();
                        i = new AxRegion();
                        i.setRegionId(PropertyUtil.getString(node, "regiondId"));
                        i.setDescription(PropertyUtil.getString(node, "description"));
                        list.add(i);
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void deleteRegionIfNotInList(String channel, List<String> regionList) {
        if (list == null || list.size() == 0) {
            return;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+"/" + REGIONS);
            NodeIterator iter = node.getNodes();
            while (iter.hasNext()) {
                Node subNode = iter.nextNode();
                if (!list.contains(subNode.getName())) {
                    try{
                        PublishingUtil.deleteNodeAndPublish(subNode,JcrWorkspace.CMSConfig.getWorkspaceName());
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshCache(String channel) {
        list_refresh_count = 1;
        List<AxRegion> tempList = getRegionList(channel);
        list.clear();
        list.addAll(tempList);
    }
}
