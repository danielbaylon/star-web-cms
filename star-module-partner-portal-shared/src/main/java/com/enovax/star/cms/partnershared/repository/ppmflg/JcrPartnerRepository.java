package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
@Repository("PPMFLGIPartnerRepository")
public class JcrPartnerRepository implements IPartnerRepository {

    public List<Integer> getPartnerIdsByTierId(String channel, String tierName) {
        List<Integer> paIdList = new ArrayList<Integer>();
        try {
            Node subnode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/"+ channel );
            if(subnode != null){
                NodeIterator iter = subnode.getNodes();
                while(iter.hasNext()){
                    Node node = iter.nextNode();
                    if(node != null && node.hasProperty("partnerId") && node.hasProperty("name")){
                        paIdList.add(Integer.parseInt(node.getProperty("partnerId").getValue().getString()));
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return paIdList;
    }

    public boolean savePartnerTierMapping(String channel, Integer partnerId, String tierId, String status) {
        Node subnode = null;
        try{
            subnode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/"+channel + "/" + partnerId.toString());
        }catch (Exception ex){
            if(!ex.getMessage().contains("Not an absolute path: "+channel)){
                ex.printStackTrace();
            }
        }
        try {
            if(subnode == null){
                subnode = JcrRepository.createNode(JcrWorkspace.Partners.getWorkspaceName(), "/"+channel + "/", partnerId.toString(), "mgnl:partner");
            }
            if(subnode == null){
                return false;
            }
            if(subnode.hasProperty("name")){
                subnode.getProperty("name").setValue(partnerId);
            }else{
                subnode.setProperty("name", partnerId);
            }
            if(subnode.hasProperty("status")){
                subnode.getProperty("status").setValue(status);
            }else{
                subnode.setProperty("status", status);
            }
            if(subnode.hasProperty("partnerId")){
                subnode.getProperty("partnerId").setValue(partnerId);
            }else{
                subnode.setProperty("partnerId", partnerId);
            }
            if(subnode.hasProperty("tierId")){
                subnode.getProperty("tierId").setValue(tierId);
            }else{
                subnode.setProperty("tierId", tierId);
            }
            JcrRepository.updateNode(JcrWorkspace.Partners.getWorkspaceName(), subnode);
            return true;
        } catch (RepositoryException ex) {
            if(!ex.getMessage().contains("Not an absolute path: "+channel)){
                ex.printStackTrace();
            }
        }
        return false;
    }

    public String getTierIdByPartnerId(String channel, Integer partnerId) {
        try{
            Node subnode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/"+channel + "/" + partnerId.toString());
            if(subnode != null){
                if(subnode.hasProperty("partnerId") && subnode.hasProperty("name")){
                    return subnode.getProperty("tierId").getValue().getString();
                }
            }
        }catch (Exception ex){
            if(!ex.getMessage().contains("Not an absolute path: "+channel)){
                ex.printStackTrace();
            }
        }
        return null;
    }

    public String getActiveTierIdByPartnerId(String channel, Integer partnerId) {
        try{
            Node subnode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/"+channel + "/" + partnerId.toString());
            if(subnode != null){
                if(subnode.hasProperty("partnerId") && subnode.hasProperty("name") && subnode.hasProperty("status")){
                    if(GeneralStatus.Active.code.equals(subnode.getProperty("status").getValue().getString())){
                        return subnode.getProperty("tierId").getValue().getString();
                    }
                }
            }
        }catch (Exception ex){
            if(!ex.getMessage().contains("Not an absolute path: "+channel)){
                ex.printStackTrace();
            }
        }
        return null;
    }

    /*@Override
    public List<Partner> getPartnerbyPage(PartnerGridFilterVM gridFilterVM) {
        List<Partner> partners = new ArrayList<>();
        PartnerFilter filter = gridFilterVM.getPartnerFilter();
        filter.setHasTier(false);
        filter.setStatus("Active");
        gridFilterVM.setPartnerFilter(filter);
        List<String> partnerIdListString = new ArrayList<>();
        if(filter.getPartnerIds() != null && filter.getPartnerIds().length > 0) {
            for(Integer pi: filter.getPartnerIds()) {
                partnerIdListString.add(pi.toString());
            }
        }

        try {
            Iterable<Node> partnerNodes = JcrRepository.query(JcrWorkspace.Partners.getWorkspaceName(), JcrWorkspace.Partners.getNodeType(), getQuery(filter));
            Iterator<Node> partnerNodesIterator = partnerNodes.iterator();

            while(partnerNodesIterator.hasNext()) {
                Node partnerNode = partnerNodesIterator.next();

                //only add those that are not in the list
                if(!partnerIdListString.contains(partnerNode.getName())) {
                    partners.add(new Partner(partnerNode));
                }
            }

            List<Partner> pagedPartners = new ArrayList<>();
            //filter pag page also
            if(gridFilterVM.getPage() > 0 && gridFilterVM.getPageSize() > 0) {
                for(int i = (gridFilterVM.getPage() - 1) * gridFilterVM.getPageSize(); pagedPartners.size() < gridFilterVM.getPageSize() && i < partners.size(); i++) {
                    pagedPartners.add(partners.get(i));
                }
            }else {
                return partners;
            }

            return pagedPartners;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int getPartnerSize(PartnerGridFilterVM gridFilterVM) {
        List<Partner> partners = new ArrayList<>();
        PartnerFilter filter = gridFilterVM.getPartnerFilter();
        filter.setHasTier(false);
        filter.setStatus("Active");
        gridFilterVM.setPartnerFilter(filter);
        List<String> partnerIdListString = new ArrayList<>();
        if(filter.getPartnerIds() != null && filter.getPartnerIds().length > 0) {
            for(Integer pi: filter.getPartnerIds()) {
                partnerIdListString.add(pi.toString());
            }
        }

        try {
            Iterable<Node> partnerNodes = JcrRepository.query(JcrWorkspace.Partners.getWorkspaceName(), JcrWorkspace.Partners.getNodeType(), getQuery(filter));
            Iterator<Node> partnerNodesIterator = partnerNodes.iterator();

            while(partnerNodesIterator.hasNext()) {
                Node partnerNode = partnerNodesIterator.next();

                //only add those that are not in the list
                if(!partnerIdListString.contains(partnerNode.getName())) {
                    partners.add(new Partner(partnerNode));
                }
            }

            return partners.size();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public List<Partner> getPartnerByTierId(Integer tierId) {
        List<Partner> partners = new ArrayList<>();
        String query = "//element(*, mgnl:content)[@tierId= '" + tierId + "']";
        try {
            Iterable<Node> tierPartnerNodes = JcrRepository.query(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), JcrWorkspace.TierPartnerMappings.getNodeType(), query);
            Iterator<Node> tierPartnerNodesIterator = tierPartnerNodes.iterator();

            while(tierPartnerNodesIterator.hasNext()) {
                Node tierPartnerNode = tierPartnerNodesIterator.next();
                String partnerId  = tierPartnerNode.getProperty("partnerId").getString();

                Node partnerNode = JcrRepository.getParentNode(JcrWorkspace.Partners.getWorkspaceName(), "/" + partnerId);
                partners.add(new Partner(partnerNode));
            }

            return partners;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getQuery(PartnerFilter filter) {
        String baseQuery = "//element(*, mgnl:partner)";
        String criteria = null;

        if(StringUtils.isNotBlank(filter.getOrgName())) {
            if(criteria == null) {
                criteria = "[";
            }
            criteria = criteria + "jcr:like(fn:lower-case(@orgName), '%" + filter.getOrgName().toLowerCase() + "%')";
        }

        if(StringUtils.isNotBlank(filter.getStatus())) {
            if(criteria == null) {
                criteria = "[";
            }else {
                criteria = criteria + " and ";
            }

            criteria = criteria + "@status = 'Active'";
        }

//        if (filter.getHasTier() != null && !filter.getHasTier()) {
//            if(criteria == null) {
//                criteria = "[";
//            }else {
//                criteria = criteria + " and ";
//            }
//
//            if(filter.getTierId() != null && !filter.getTierId().equals(-1)){
//                criteria = criteria + "(@tierId = " + filter.getTierId() + "  or @tierId = '')";
//            }else{
//                criteria = criteria + "@tierId = ''";
//            }
//        }

        if(StringUtils.isNotBlank(criteria)) {
            criteria = criteria + "]";
        }

        return baseQuery + criteria;
    }*/
}
