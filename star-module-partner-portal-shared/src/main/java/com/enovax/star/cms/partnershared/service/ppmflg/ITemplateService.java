package com.enovax.star.cms.partnershared.service.ppmflg;


import com.enovax.star.cms.commons.model.partner.ppmflg.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentNotifyVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReceiptVM;

import java.util.Map;

public interface ITemplateService {

    public String generateApproverAlertEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationAlertEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationSignupEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationRejectEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationApprovedEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationResubmitEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationSignupUpdateEmailHtml(Object model) throws Exception;

    public String generatePartnerRegistrationUpdateAlertEmailHtml(Object model) throws Exception;

    public String generatePartnerWoTEmailHtml(Object model) throws Exception;

    public void generatePincodePdf(MixMatchPkgVM receipt, String outputFilePath) throws Exception;

    public String generatePartnerSubaccountCreationEmailHtml(Object model) throws Exception;

    public String generatePartnerSubaccountPasswordResetEmailHtml(Object model) throws Exception;

    public void generatePartnerTransactionReceiptPdf(ReceiptVM receipt, String outputFilePath) throws Exception;

    public String generatePartnerTransactionReceiptEmailHtml(Object model) throws Exception;

    public void generatePartnerRevalTransactionReceiptPdf(ReceiptVM receipt, String outputFilePath) throws Exception;

    public String generatePartnerRevalTransactionReceiptEmailHtml(Object model) throws Exception;

    public String processEmailParams(String content, Map<String, String> paramMap);

    public String generatePincodeFooter(Object model) throws Exception;

    public String generatePincodeBody(Object model) throws Exception;

    public String generatePincodeEmailHtml(Object model) throws Exception;

    public String generateEticketEmailHtml(Object model) throws Exception;

    public String generateExcelEmailHtml(Object model) throws Exception;

    String generateOfflinePaymentNotificationEmailHtml(OfflinePaymentNotifyVM opnvm) throws Exception;

    public boolean generatePdfByHtml(String key, String html, String outputFilePath);

    public boolean generateDepositTopupReceiptPDF(ReceiptVM receiptVM, String outputFilePath) throws Exception;

    public String generateDepositTopupReceiptHTML(Object mode) throws Exception;

    boolean generateWoTMonthlyReceiptReceiptPDF(ReceiptVM receipt, String receiptPath) throws Exception;

    boolean generateWoTDailyReceiptReceiptPDF(ReceiptVM receipt, String receiptPath) throws Exception;
}
