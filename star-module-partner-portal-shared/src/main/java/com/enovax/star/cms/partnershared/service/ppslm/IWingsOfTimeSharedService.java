package com.enovax.star.cms.partnershared.service.ppslm;


import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/7/16.
 */
public interface IWingsOfTimeSharedService {

    public List<AxProduct> getWOTProducts();

    public Date calculateWingsOfTimeShowCutOffTimeByShowTime(final PPSLMWingsOfTimeReservation wot, long cutoffmins) throws Exception;

    ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(Integer partnerId, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String displayCancelled, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception;

    ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(boolean isAdmin, Integer mainAccountId, Date startDate, Date endDate,  Date reservationStartDate, Date reservationEndDate, String displayCancelled, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception;

    ApiResult<WOTReservationCharge> validateAdminNewReservation(WOTReservationVM reservation);

    ApiResult<WOTReservationVM> saveAdminReservation(WOTReservationVM reservation, boolean isSendUpdateEmail) throws Exception;

    WOTReservationVM getAdminReservationByID(Integer reservationId) throws Exception;

    ApiResult<WOTReservationCharge> validateAdminUpdateReservation(WOTReservationVM reservation);

    ApiResult<WOTReservationCharge> validateAdminCancelReservation(Integer reservationId);

    ApiResult<String> cancelAdminReservation(WOTReservationVM reservation, boolean hasEmailNotification) throws BizValidationException;

    ApiResult<WOTReservationVM> updateAdminReservation(WOTReservationVM reservation) throws BizValidationException, Exception;

    ApiResult<String> refreshAdminReservationPinCodeById(Integer reservationId);


    public void saveOrUpdatePreservationOrder(ApiResult<List<AxRetailTicketRecord>> retResponse, String custId, PPSLMWingsOfTimeReservation wot, boolean update);

    void sendWotUnconfirmedBackendReservationReleaseReminder(Integer paramDays);

    void releaseWotUnconifrmedBackendReservation(Integer paramDays);

    void sendWotUnredeemedReservationCancelReminder(Integer paramHours);

    public List<WOTProductVM> getWOTProductsVM(StoreApiChannels channel);

    public ResultVM saveWOTProduct(StoreApiChannels channel, WOTProductVM wotProductVM);

    public ResultVM removeWOTProduct(StoreApiChannels channel,String wotProdIds);

}