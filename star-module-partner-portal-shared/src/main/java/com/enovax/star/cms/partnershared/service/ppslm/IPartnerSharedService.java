package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import java.util.Map;

/**
 * Created by houtao on 9/9/16.
 */
public interface IPartnerSharedService {
    PPSLMPartner getPartnerById(Integer partnerId);

    PPSLMPartner getPartnerByMainAccountId(Integer mainAccountId);

    String getPartnerNameByMainAccountIdWithCache(Map<String, String> paNameMapping, Integer mainAccountId);

    Integer getPartnerIdByMainAccountIdWithCache(Map<Integer, Integer> paNameMapping, Integer mainAccountId);

    Integer getWoTReservationCap(Integer id) throws Exception;

    public boolean resetAllPartnerReservationCap(String adminId) throws Exception;

    void selfUpdatePartnerProfile(PartnerAccount partnerAccount, PartnerVM partnerVM) throws BizValidationException;

    void syncSelfUpdatedProfileDetailsToAX(PartnerAccount partnerAccount) throws Exception;

    void selfUpdateProfileAndSyncProfileChangesToAX(PartnerAccount partnerAccount, PartnerVM partnerVM) throws Exception;

    String getActiveExtensionPropertyValue(String correspondenceAddress, Integer id);

    PPSLMPartner getPartnerByAccountCodeAndMainAccountId(Integer id, String accountCode);
}
