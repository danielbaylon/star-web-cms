package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.OfflinePaymentStatus;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ppslm.IUserRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.commons.model.booking.FunCartItem;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartEmailDisplay;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMInventoryTransactionItemRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMInventoryTransactionRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMOfflinePaymentRequestReqpository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPartnerRepository;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppslm.PPSLMOfflinePaymentRequestSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.partnershared.constant.ppslm.AdminUserRoles;
import com.enovax.star.cms.partnershared.repository.ppslm.cart.IPartnerCartManager;
import info.magnolia.cms.security.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by houtao on 30/9/16.
 */
@Service
public class DefaultOfflinePaymentService implements IOfflinePaymentService {

    private static final Logger log = LoggerFactory.getLogger(DefaultOfflinePaymentService.class);

    @Autowired
    private IUserRepository userSrv;
    @Autowired
    private IAdminService adminSrv;
    @Autowired
    private ISystemParamService paramSrv;
    @Autowired
    private PPSLMInventoryTransactionRepository transRepo;
    @Autowired
    private PPSLMPartnerRepository paRepo;
    @Autowired
    private PPSLMOfflinePaymentRequestReqpository offlinePayRepo;
    @Autowired
    private PPSLMInventoryTransactionItemRepository txnItemRepo;
    @Autowired
    private IPartnerAccountSharedService accSrv;
    @Autowired
    private IOfflinePaymentEmailService offlineReqEmailSrv;
    @Autowired
    private IPartnerPurchaseTransactionService purchaseTxnSrv;
    @Autowired
    private IPartnerCartManager cartManager;

    @Autowired
    private IPartnerBookingService bookingService;

    @Autowired
    private IProductService productService;

    private final StoreApiChannels channel = StoreApiChannels.PARTNER_PORTAL_SLM;

    public PartnerFunCartEmailDisplay constcutOfflinePaymentCart(PPSLMInventoryTransaction txn, boolean includePartner){
        PartnerFunCartEmailDisplay cart = constcutOfflinePaymentCart(txn);
        if(cart != null && includePartner){
            cart.setPartner(constructPartnerProfile(txn.getMainAccountId()));
        }
        return cart;
    }

    private PartnerVM constructPartnerProfile(Integer mainAccountId) {
        if(mainAccountId != null && mainAccountId.intValue() > 0){
            PPSLMTAMainAccount mainAcc = accSrv.getActivePartnerByMainAccountId(mainAccountId);
            if(mainAcc != null){
                PPSLMPartner partner = paRepo.findById(mainAcc.getProfile().getId());
                if(partner != null){
                    PartnerVM vm = new PartnerVM(partner);
                    return vm;
                }
            }
        }
        return null;
    }

    public PartnerFunCartEmailDisplay constcutOfflinePaymentCart(PPSLMInventoryTransaction txn){
        FunCartDisplayCmsProduct cmsProduct = null;
        List<FunCartDisplayItem> cartItems = null;
        List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();

        PartnerFunCartEmailDisplay cart = new PartnerFunCartEmailDisplay();
        cart.setCartId(txn.getReceiptNum());
        cart.setGstRate(txn.getGstRate());
        cart.setTotal(txn.getTotalAmount());
        cart.setGstRateStr(txn.getGstRateStr());
        cart.setGstText(NvxNumberUtils.formatToCurrency(cart.getTotal().multiply(txn.getGstRate())));
        cart.setTotalNoGstText(NvxNumberUtils.formatToCurrency(cart.getTotal().subtract(cart.getTotal().multiply(txn.getGstRate()))));
        cart.setTotalText(NvxNumberUtils.formatToCurrency(txn.getTotalAmount()));
        cart.setTotalQty(txn.getTransQty());
        cart.setPayMethod(txn.getPaymentType());

        List<PPSLMInventoryTransactionItem> items = txnItemRepo.findByTransactionId(txn.getId());

        List<String> productIds = new ArrayList<String>();
        if(items != null){
            for(PPSLMInventoryTransactionItem item : items){
                if(!productIds.contains(item.getProductId())){
                    productIds.add(item.getProductId());
                }
            }
        }

        BigDecimal subtotal = BigDecimal.ZERO;
        for(String productId : productIds){
            cmsProduct = new FunCartDisplayCmsProduct();
            cartItems = new ArrayList<FunCartDisplayItem>();
            cmsProduct.setId(productId);
            subtotal = BigDecimal.ZERO;

            for(PPSLMInventoryTransactionItem item : items){
                if(!productId.equals(item.getProductId())){
                    continue;
                }
                subtotal = subtotal.add(item.getSubTotal());
                cmsProduct.setName(item.getProductName());
                FunCartDisplayItem cartItem = new FunCartDisplayItem();
                cartItem.setCartItemId(item.getId().toString());
                cartItem.setListingId(item.getItemListingId());
                cartItem.setProductCode(item.getItemProductCode());
                cartItem.setType(item.getTicketType());
                cartItem.setEventGroupId(item.getEventGroupId());
                cartItem.setEventLineId(item.getEventLineId());
                cartItem.setSelectedEventDate(item.getEventLineId());
                cartItem.setEventSessionName(item.getEventName());
                cartItem.setName(item.getDisplayName());
                cartItem.setDescription(item.getDisplayDetails());
                cartItem.setQty(item.getQty());
                cartItem.setPrice(item.getUnitPrice());
                cartItem.setPriceText(NvxNumberUtils.formatToCurrency(item.getUnitPrice()));
                cartItem.setTotal(item.getSubTotal());
                cartItem.setTotalText(NvxNumberUtils.formatToCurrency(item.getSubTotal()));
                cartItem.setDiscountTotal(BigDecimal.ZERO);// TODO: pending
                cartItem.setDiscountLabel(NvxNumberUtils.formatToCurrency(cartItem.getDiscountTotal()));//TODO: pending
                cartItem.setDiscountTotalText(NvxNumberUtils.formatToCurrency(cartItem.getDiscountTotal()));//TODO: pending
                if(item.getMainItemListingId() != null && item.getMainItemListingId().trim().length() > 0 && item.getMainItemProductId() != null && item.getMainItemProductId().trim().length() > 0) {
                    cartItem.setIsTopup(true);
                    cartItem.setParentListingId(item.getMainItemListingId());
                }
                cartItems.add(cartItem);
            }
            cmsProduct.setSubtotal(subtotal);
            cmsProduct.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal));
            cmsProduct.setItems(cartItems);
            cmsProducts.add(cmsProduct);
        }

        cart.setCmsProducts(cmsProducts);

        return cart;
    }

    @Override
    public ApiResult<OfflinePaymentResultVM> viewOfflinePaymentDetails(String adminId, Integer id, boolean isPreview) {
        ApiResult<OfflinePaymentResultVM> api = new ApiResult<OfflinePaymentResultVM>();
        api.setSuccess(false);
        api.setMessage("Invalid Request");
        if(adminId == null || adminId.trim().length() == 0){
            return api;
        }
        User user = userSrv.getUserById(adminId);
        if(user == null){
            return api;
        }
        List<String> roles = adminSrv.getLoginAdminUserAssignedRights(adminId);
        if(roles == null || roles.size() == 0){
            return api;
        }
        if(isPreview){
            if(!(roles.contains(AdminUserRoles.OfflinePaymentApproverRole.code) || roles.contains(AdminUserRoles.OfflinePaymentViewRole.code))){
                return api;
            }
        }
        PPSLMOfflinePaymentRequest request = offlinePayRepo.findById(id);
        if(request == null){
            return api;
        }

        if(isPreview){
            if(!OfflinePaymentStatus.Pending_Approval.code.equals(request.getStatus())){
                api.setMessage("The offline payment request is not allowed for approval");
                return api;
            }
        }
        Integer txnId = request.getTransactionId();
        if(txnId == null){
            return api;
        }
        PPSLMInventoryTransaction txn = transRepo.findById(txnId);
        if(txn == null){
            return api;
        }
        if(isPreview){
            if(!TicketStatus.Pending_Approval.name().equals(txn.getStatus())){
                api.setMessage("The offline payment transaction is not allowed for approval");
                return api;
            }
        }
        boolean isApprover = roles.contains(AdminUserRoles.OfflinePaymentApproverRole.code);
        boolean canManage = roles.contains(AdminUserRoles.OfflinePaymentViewRole.code);
        boolean isAccMgr  = false;

        PPSLMTAMainAccount mainAcc = accSrv.getActivePartnerByMainAccountId(request.getMainAccountId());
        PPSLMPartner pa = mainAcc.getProfile();
        if(isPreview){
            if(pa != null && pa.getAccountManagerId() != null && pa.getAccountManagerId().equalsIgnoreCase(adminId)){
                isAccMgr = true;
            }
        }

        OfflinePaymentResultVM vm = new OfflinePaymentResultVM();
        OfflinePaymentRequestVM requestVM = OfflinePaymentRequestVM.buildPaymentRequest(request, txn, mainAcc.getProfile().getOrgName(), mainAcc.getProfile().getAccountCode());
        requestVM.setIsApprover(isApprover);
        vm.setPaymentDetail(requestVM);
        vm.setCart(constcutOfflinePaymentCart(txn, true));
        vm.setCanApprove(isApprover);
        vm.setCanManage(canManage && isAccMgr || isApprover);
        api.setData(vm);
        api.setSuccess(true);
        api.setMessage(null);
        return api;
    }

    @Override
    public ApiResult<PagedData<List<OfflinePaymentRequestVM>>> getPagedOfflinePaymentRequest(String isPartnerOnly, String paId, String adminId, Date startDate, Date endDate, String status, String username, String orgName, String receiptNo, String referenceNum, String accountCode, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        PagedData<List<OfflinePaymentRequestVM>> paged = new PagedData<List<OfflinePaymentRequestVM>>();
        paged.setData(new ArrayList<>());
        paged.setSize(0);
        ApiResult<PagedData<List<OfflinePaymentRequestVM>>> api = new ApiResult<PagedData<List<OfflinePaymentRequestVM>>>();
        api.setData(paged);
        api.setSuccess(true);

        if(adminId == null || adminId.trim().length() == 0){
            return api;
        }
        User user = userSrv.getUserById(adminId);
        if(user == null){
            return api;
        }
        List<String> roles = adminSrv.getLoginAdminUserAssignedRights(adminId);
        if(roles == null || roles.size() == 0){
            return api;
        }
        Integer intPaId = null;
        if(isPartnerOnly != null && isPartnerOnly.trim().length() > 0 && "Y".equalsIgnoreCase(isPartnerOnly)){
            if(!(paId != null && paId.trim().length() > 0)){
                return api;
            }
            try{
                intPaId = Integer.parseInt(paId.trim());
            }catch (Exception ex){
                return api;
            }
            if(intPaId == null || intPaId.intValue() == 0){
                return api;
            }
        }else{
            status = OfflinePaymentStatus.Pending_Approval.code;
            if(!(roles.contains(AdminUserRoles.OfflinePaymentApproverRole.code) || roles.contains(AdminUserRoles.OfflinePaymentViewRole.code))){
                return api;
            }
        }

        Integer txnId = null;
        if(receiptNo != null && receiptNo.trim().length() > 0){
            PPSLMInventoryTransaction txn = transRepo.findByReceiptNum(receiptNo);
            if(txn != null && txn.getId() != null && txn.getId().doubleValue() != 0){
                txnId = txn.getId();
            }
            if(txnId == null){
                return api;
            }
        }

        List<Integer> txnList = new ArrayList<Integer>();

        List<Integer> mainAccIdList1 = new ArrayList<Integer>();
        if(orgName != null && orgName.trim().length() > 0){
            List<PPSLMPartner> partners = paRepo.findByOrgNameLike(orgName);
            if(partners != null && partners.size() > 0){
                for(PPSLMPartner pa : partners){
                    mainAccIdList1.add(pa.getMainAccount().getId());
                }
            }
            if(mainAccIdList1.size() == 0){
                mainAccIdList1.add(-9876);
            }
        }

        List<Integer> mainAccIdList2 = new ArrayList<Integer>();
        if(accountCode != null && accountCode.trim().length() > 0){
            PPSLMPartner partners = paRepo.findByAccountCode(accountCode);
            if(partners != null){
                mainAccIdList2.add(partners.getMainAccount().getId());
            }
            if(mainAccIdList2.size() == 0){
                mainAccIdList2.add(-9876);
            }
        }
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        if(isPartnerOnly != null && isPartnerOnly.trim().length() > 0 && "Y".equalsIgnoreCase(isPartnerOnly)){
            boolean isValid = false;
            if(intPaId != null && intPaId.intValue() > 0){
                PPSLMTAMainAccount mainAcc = accSrv.getActivePartnerMainAccountByPartnerId(intPaId);
                if(mainAcc != null && mainAcc.getId() != null){
                    criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.EQ, mainAcc.getId()));
                    isValid = true;
                }
            }
            if(!isValid){
                return api;
            }
        }
        if(startDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.LE, endDate));
        }
        if(username != null && username.trim().length() > 0){
            criterias.add(new QCriteria("createdBy", QCriteria.Operation.LIKE, username));
        }
        if(status != null && status.trim().length() > 0){
            criterias.add(new QCriteria("status", QCriteria.Operation.EQ, status));
        }
        if(referenceNum != null && referenceNum.trim().length() > 0){
            criterias.add(new QCriteria("referenceNum", QCriteria.Operation.LIKE, referenceNum));
        }
        if(orgName != null && orgName.trim().length() > 0){
            criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.IN, mainAccIdList1, QCriteria.ValueType.Integer));
        }
        if(accountCode != null && accountCode.trim().length() > 0){
            criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.IN, mainAccIdList2, QCriteria.ValueType.Integer));
        }

        if(receiptNo != null && receiptNo.trim().length() > 0){
            criterias.add(new QCriteria("transactionId", QCriteria.Operation.EQ, txnId));
        }

        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.DESC, "createdDate");
        SpecificationBuilder builder = new SpecificationBuilder();
        Specifications querySpec = builder.build(criterias, PPSLMOfflinePaymentRequestSpecification.class);
        Page<PPSLMOfflinePaymentRequest> list = offlinePayRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        List<OfflinePaymentRequestVM> ret = new ArrayList<OfflinePaymentRequestVM>();
        if(list != null && list.getSize() > 0){
            PPSLMInventoryTransaction tTxn = null;
            PPSLMTAMainAccount mainAccount = null;
            for(PPSLMOfflinePaymentRequest i : list){
                tTxn = transRepo.findById(i.getTransactionId());
                mainAccount = accSrv.getActivePartnerByMainAccountId(i.getMainAccountId());
                ret.add(OfflinePaymentRequestVM.buildPaymentRequestBaisc(i, tTxn, mainAccount != null ? mainAccount.getProfile().getOrgName() : null, mainAccount != null ? mainAccount.getProfile().getAccountCode() : null ));
            }
        }
        paged.setData(ret);
        paged.setSize(ret.size());
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    @Override
    public ApiResult<String> reviewOfflinePaymentDetails(String adminId, Integer requestId, String action, String remarks) throws BizValidationException {
        ApiResult<String> api = validateRequestParameter(adminId, requestId, action, remarks);
        if(!api.isSuccess()){
            return api;
        }
        if("Reject".equalsIgnoreCase(action)){
            return rejectOfflinePaymentRequest(adminId, requestId, action, remarks);
        }else if("Approve".equalsIgnoreCase(action)){
            return approveOfflinePaymentRequest(adminId, requestId, action, remarks);
        }else if("Notify".equalsIgnoreCase(action)){
            return notifyPartnerOfflinePaymentRequestComments(adminId, requestId, action, remarks);
        }else{
            return new ApiResult<>(false, "", "Invalid review offline payment request.", "");
        }
    }

    private ApiResult<String> validateRequestParameter(String adminId, Integer requestId, String action, String remarks){
        ApiResult<String> api = new ApiResult<>();
        api.setSuccess(false);
        api.setMessage("Invalid review offline payment request.");
        if(!("Reject".equalsIgnoreCase(action) || "Approve".equalsIgnoreCase(action) || "Notify".equalsIgnoreCase(action))){
            return api;
        }
        if(adminId == null || adminId.trim().length() == 0 || requestId == null || requestId.intValue() == 0 || action == null || action.trim().length() == 0){
            return api;
        }
        if(("Notify".equalsIgnoreCase(action) || "Reject".equalsIgnoreCase(action)) && (remarks == null || remarks.trim().length() == 0)){
            return api;
        }
        List<String> rights = adminSrv.getLoginAdminUserAssignedRights(adminId);
        if(rights == null || rights.size() == 0){
            api.setMessage("You are not authorized to review/approve this request.");
            return api;
        }
        PPSLMOfflinePaymentRequest request = offlinePayRepo.findById(requestId);
        if(request == null){
            return api;
        }
        if(!OfflinePaymentStatus.Pending_Approval.code.equalsIgnoreCase(request.getStatus())){
            api.setMessage("The request is not in approval status.");
            return api;
        }

        boolean isApprover = rights.contains(AdminUserRoles.OfflinePaymentApproverRole.code);
        boolean isViewer   = rights.contains(AdminUserRoles.OfflinePaymentViewRole.code);

        if(!( isViewer || isApprover )){
            api.setMessage("You are not authorized to perform this action.");
            return api;
        }

        if("Approve".equalsIgnoreCase(action) && (!isApprover)){
            api.setMessage("You are not authorized to perform this action.");
            return api;
        }

        if(isViewer && (!isApprover)) {
            PPSLMTAMainAccount mainAcc = accSrv.getPartnerMainAccountByMainAccountId(request.getMainAccountId());
            if (mainAcc == null) {
                api.setMessage("You are not authorized to perform this action.");
                return api;
            }
            PPSLMPartner pa = mainAcc.getProfile();
            if (!(pa != null && pa.getAccountManagerId() != null && pa.getAccountManagerId().equalsIgnoreCase(adminId))) {
                api.setMessage("You are not authorized to perform this action.");
                return api;
            }
        }
        api.setMessage(null);
        api.setSuccess(true);
        return api;
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    private ApiResult<String> notifyPartnerOfflinePaymentRequestComments(String adminId, Integer requestId, String action, String remarks) {
        PPSLMOfflinePaymentRequest request = offlinePayRepo.findById(requestId);
        PPSLMTAMainAccount mainAcc = accSrv.getPartnerMainAccountByMainAccountId(request.getMainAccountId());
        PPSLMPartner profile = mainAcc.getProfile();
        String ofRemarksStr = this.makeChatHists(request.getApproverRemarks(), remarks, adminId);
        request.setApproverRemarks(ofRemarksStr);
        offlinePayRepo.save(request);
        PPSLMInventoryTransaction trans = transRepo.findById(request.getTransactionId());
        try {
            OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(request, trans, profile.getOrgName(), profile.getAxAccountNumber());
            OfflinePaymentNotifyVM vm = new OfflinePaymentNotifyVM();
            vm.setPaymentDetail(offReqVM);
            vm.setCart(constcutOfflinePaymentCart(trans));
            vm.setMsg(remarks);
            vm.setBaseUrl(paramSrv.getApplicationContextPath());
            offlineReqEmailSrv.sendingOfflinePaymentCustomerNotification(vm, request, trans);
        } catch (Exception e) {
            log.error("sending offline payment customer notification email failed for offline payment request "+requestId, e);
            throw e;
        }
        return new ApiResult<>(true, "", "Notified Client Successfully.", "");
    }

    private ApiResult<String> rejectOfflinePaymentRequest(String adminId, Integer requestId, String action, String remarks) {
        PPSLMOfflinePaymentRequest request = offlinePayRepo.findById(requestId);
        if(OfflinePaymentStatus.Approved.code.equalsIgnoreCase(request.getStatus())){
            return new ApiResult<>(true, "", "Offline payment request were approved.", "");
        }
        if(OfflinePaymentStatus.Rejected.code.equalsIgnoreCase(request.getStatus())){
            return new ApiResult<>(true, "", "Offline payment request were rejected.", "");
        }
        PPSLMInventoryTransaction trans = transRepo.findById(request.getTransactionId());
        trans.setStatus(TicketStatus.Incomplete.toString());
        transRepo.save(trans);
        request.setApprover(adminId);
        request.setStatus(OfflinePaymentStatus.Rejected.code);
        request.setApproveDate(new Date(System.currentTimeMillis()));
        request.setFinalApprovalRemarks(this.makeChatHists(null, remarks, adminId));
        offlinePayRepo.save(request);
        try {
            PPSLMTAMainAccount mainAcc = accSrv.getPartnerMainAccountByMainAccountId(request.getMainAccountId());
            PPSLMPartner profile = mainAcc.getProfile();
            OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(request, trans, profile.getOrgName(), profile.getAxAccountNumber());
            OfflinePaymentNotifyVM vm = new OfflinePaymentNotifyVM();
            vm.setPaymentDetail(offReqVM);
            vm.setCart(constcutOfflinePaymentCart(trans));
            vm.setMsg(remarks);
            vm.setBaseUrl(paramSrv.getApplicationContextPath());
            offlineReqEmailSrv.sendingOfflinePaymentRequestRejectedEmail(vm, request, trans);
        } catch (Exception e) {
            log.error("sending offline payment request rejection email failed for offline payment request "+requestId, e);
            throw e;
        }
        return new ApiResult<>(true, "", "Transaction Rejected Successfully.", "");
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    private ApiResult<String> approveOfflinePaymentRequest(String adminId, Integer requestId, String action, String remarks) throws BizValidationException {
        PPSLMOfflinePaymentRequest request = offlinePayRepo.findById(requestId);
        if(OfflinePaymentStatus.Approved.code.equalsIgnoreCase(request.getStatus())){
            return new ApiResult<>(true, "", "Offline payment request were approved.", "");
        }
        if(OfflinePaymentStatus.Rejected.code.equalsIgnoreCase(request.getStatus())){
            return new ApiResult<>(true, "", "Offline payment request were rejected.", "");
        }
        PPSLMTAMainAccount mainAcc = accSrv.getPartnerMainAccountByMainAccountId(request.getMainAccountId());
        PPSLMPartner profile = mainAcc.getProfile();

        ApiResult<String> css = this.confirmOfflineTransaction(request.getTransactionId(), profile.getAxAccountNumber(), mainAcc.getEmail());
        if(css == null){
            return new ApiResult<>(true, "", "Approving offline payment transaction failed.", "");
        }
        if(!css.isSuccess()){
            return new ApiResult<>(true, "", css.getMessage() != null && css.getMessage().trim().length() > 0 ? css.getMessage() : "Approving offline payment transaction failed." , "");
        }
        request.setApprover(adminId);
        request.setStatus(OfflinePaymentStatus.Approved.code);
        request.setApproveDate(new Date(System.currentTimeMillis()));
        request.setFinalApprovalRemarks(this.makeChatHists(null, remarks, adminId));
        offlinePayRepo.save(request);
        PPSLMInventoryTransaction trans = transRepo.findById(request.getTransactionId());
        try{
            boolean receiptEmail = purchaseTxnSrv.generateOfflinePaymentReceiptAndEmail(trans, request); //check if this is working??
            if(!receiptEmail){
                log.error("sending offline payment request [receipt] email failed for offline payment request "+requestId+".");
            }
        }catch (Exception ex){
            log.error("sending offline payment request [approval] email failed for offline payment request "+requestId+", exception message : "+ex.getMessage(), ex);
        }
        try {
            OfflinePaymentRequestVM offReqVM = OfflinePaymentRequestVM.buildPaymentRequest(request, trans, profile.getOrgName(), profile.getAxAccountNumber());
            OfflinePaymentNotifyVM vm = new OfflinePaymentNotifyVM();
            vm.setPaymentDetail(offReqVM);
            vm.setCart(constcutOfflinePaymentCart(trans));
            vm.setMsg(remarks);
            vm.setBaseUrl(paramSrv.getApplicationContextPath());
            offlineReqEmailSrv.sendingOfflinePaymentRequestApprovedEmail(vm, request, trans);
            return new ApiResult<>(true, "", "Transaction Approved Successfully.", "");
        } catch (Exception e) {
            log.error("sending offline payment request approval email failed for offline payment request "+requestId+", exception message : "+e.getMessage(), e);
            throw e;
        }
    }

    public String makeChatHists(String oriRemarks,String remarks,String username){
        if(remarks == null || remarks.trim().length() == 0){
            return oriRemarks;
        }

        List<ChatHistVM> chatHists = null;
        if(StringUtils.isNotEmpty(oriRemarks)){
            chatHists =  new ArrayList<ChatHistVM>(Arrays.asList(JsonUtil.fromJson(oriRemarks.trim(), ChatHistVM[].class)));
        }else{
            chatHists = new ArrayList<ChatHistVM>();
        }

        ChatHistVM tmpChat = new ChatHistVM();
        tmpChat.setContent(remarks);
        tmpChat.setCreateDtStr(NvxDateUtils.formatDate(new Date(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        tmpChat.setUsername(username);
        chatHists.add(tmpChat);
        String ofRemarksStr = JsonUtil.jsonify(chatHists);
        return ofRemarksStr;
    }

    public ChatHistVM getLatestChatHistVM(StringBuilder lastSubmitCountAfterApprovalChat, String remarks, Date lastApprovalRemarksDate, boolean calcuateLastSubmitCountAfterApprovalChat) {
        if(remarks == null || remarks.trim().length() == 0){
            return null;
        }
        Integer tempCount = 0;
        List<ChatHistVM> chatHists = null;
        if(StringUtils.isNotEmpty(remarks)){
            chatHists =  new ArrayList<ChatHistVM>(Arrays.asList(JsonUtil.fromJson(remarks.trim(), ChatHistVM[].class)));
        }else{
            chatHists = new ArrayList<ChatHistVM>();
        }
        ChatHistVM vm = null;
        if(chatHists != null && chatHists.size() > 0){
            for(ChatHistVM t : chatHists){
                if(calcuateLastSubmitCountAfterApprovalChat && lastApprovalRemarksDate != null){
                    if(NvxDateUtils.parseDateFromDisplay(t.getCreateDtStr(), true).after(lastApprovalRemarksDate)){
                        tempCount += 1;
                    }
                }else{
                    tempCount += 1;
                }
                if(vm != null){
                    if(NvxDateUtils.parseDateFromDisplay(vm.getCreateDtStr(), true).before(NvxDateUtils.parseDateFromDisplay(t.getCreateDtStr(), true))){
                        vm = t;
                    }
                }else{
                    vm = t;
                }
            }
        }
        if(calcuateLastSubmitCountAfterApprovalChat && lastSubmitCountAfterApprovalChat != null){
            lastSubmitCountAfterApprovalChat.append(tempCount.intValue());
        }
        return vm;
    }

    /*
    Update the transaction other details also with the new fun cart item loh
     */
    @Transactional
    private FunCart getCartFromTrans(PPSLMInventoryTransaction trans) throws BizValidationException {
        FunCart cart = new FunCart();
        List<FunCartItem> cartItems = new ArrayList<>();
        cart.setItems(cartItems);

        Map<String, FunCartItem> axLineCommentMap = new HashMap<>();
        cart.setAxLineCommentMap(axLineCommentMap);

        List<String> productIds = new ArrayList<>();
        List<PPSLMInventoryTransactionItem> items = txnItemRepo.findByTransactionId(trans.getId());
        if(items == null || items.size() == 0){
            throw new BizValidationException("No items found, approval offline payment failed.");
        }

        for(PPSLMInventoryTransactionItem item: items) {
            FunCartItem cartItem = new FunCartItem();
            cartItem.setCmsProductId(item.getProductId());
            cartItem.setCmsProductName(item.getProductName());
            cartItem.setListingId(item.getItemListingId());
            cartItem.setProductCode(item.getItemProductCode());
            cartItem.setName(item.getDisplayName());
            cartItem.setIsTopup(TransItemType.Topup.toString().equals(item.getTransItemType()));

            if(cartItem.isTopup()) {
                cartItem.setParentCmsProductId(item.getMainItemProductId());
                cartItem.setParentListingId(item.getMainItemListingId());

            }
            cartItem.setType(item.getTicketType());
            cartItem.setQty(item.getQty());
            cartItem.setEventLineId(item.getEventLineId());
            cartItem.setEventGroupId(item.getEventGroupId());
            if(item.getDateOfVisit() != null){
                cartItem.setSelectedEventDate(NvxDateUtils.formatDate(item.getDateOfVisit(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            }

            cartItem.setPrice(item.getUnitPrice());

            axLineCommentMap.put(cartItem.getAxLineCommentId(), cartItem);

            productIds.add(item.getItemListingId());

            item.setOtherDetails(cartItem.getAxLineCommentId()); //store the new ax line comment id
            txnItemRepo.save(item);

            cartItems.add(cartItem);
        }

        ApiResult<List<ProductExtViewModel>> productExtRes = null;
        Map<String, ProductExtViewModel> productExtViewModelMap = new HashMap<>();

        try {
            productExtRes = productService.getDataForProducts(channel, productIds);

            if(productExtRes.isSuccess()) {
                List<ProductExtViewModel> extViewModels = productExtRes.getData();
                for(ProductExtViewModel extViewModel: extViewModels) {
                    productExtViewModelMap.put(extViewModel.getProductId(), extViewModel);
                }
            }
        }catch (AxChannelException e) {
            throw new BizValidationException(e);
        }

        for(FunCartItem cartItem: cartItems) {
            ProductExtViewModel productExtViewModel = productExtViewModelMap.get(cartItem.getListingId());
            if(productExtViewModel!= null) {
                cartItem.setPrinting(productExtViewModel.getPrinting());
            }
        }

        return cart;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public ApiResult<String> confirmOfflineTransaction(Integer transactionId, String axAccountNumber, String mainAccountEmail) throws BizValidationException {

        PPSLMInventoryTransaction trans = transRepo.findById(transactionId);

        FunCart cart = getCartFromTrans(trans);
        cart.setCartId("InventoryTransactionId=" + trans.getId()); //just invent anyhow la
        cart.setAxCustomerId(axAccountNumber);
        cartManager.saveCart(channel, cart);


        //ADD to Cart
        ApiResult<String> cartRecreateRes = bookingService.cartRecreate(channel, cart);  //this one will recreate everything
        if(!cartRecreateRes.isSuccess()) {
            cartManager.removeCart(channel, cart.getCartId());
            return new ApiResult<>(cartRecreateRes.getApiErrorCode());
        }

        //Checkout & RESERVE ITEMS
        ApiResult<InventoryTransactionVM> res = bookingService.finishAxReserveAndCheckout(channel, trans.getReceiptNum(), cart, axAccountNumber);
        if(!res.isSuccess()) {
            cartManager.removeCart(channel, cart.getCartId());
            return new ApiResult<>(res.getApiErrorCode());
        }

        //COMPLETE Sales
        ApiResult<InventoryTransactionVM> createSalesOrderRes = bookingService.createSalesOrder(channel, trans, mainAccountEmail, cart.getAxCart().getCheckoutCart());

        if(!createSalesOrderRes.isSuccess()) {
            cartManager.removeCart(channel, cart.getCartId());
            return new ApiResult<>(createSalesOrderRes.getApiErrorCode());
        }

        cartManager.removeCart(channel, cart.getCartId());

        List<PPSLMInventoryTransactionItem> txnItems = txnItemRepo.findByTransactionId(trans.getId());
        if(txnItems != null && txnItems.size() > 0){
            for(PPSLMInventoryTransactionItem item: txnItems) {
                item.setOtherDetails("");
                txnItemRepo.save(item);
            }
        }

        trans.setCreatedDate(new Date());
        trans.setStatus(TicketStatus.Available.toString());
        transRepo.save(trans);

        return new ApiResult<>(true, "", "", "");
    }
}
