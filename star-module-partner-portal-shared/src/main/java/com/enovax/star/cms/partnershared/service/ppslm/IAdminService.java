package com.enovax.star.cms.partnershared.service.ppslm;


import com.enovax.star.cms.commons.model.partner.ppslm.AdminAccountVM;

import java.util.List;

public interface IAdminService {

    List<AdminAccountVM> getPartnerMaintenanceAdminAccountList();

    List<AdminAccountVM> getAssignedSalesAdminByCountryId(String ctyId);

    List<String> getAdminUserAssignedRights(String loginAdminId);

    List<String> getApprovalRightsWithAssignedRightsByAdminId(String loginAdminId);

    boolean isPartnerProfileApprover(String loginAdminId);

    String getDefaultPartnerApproverEmailAddress();

    String getAdminUserEmailById(String adminUserId);

    String getAdminUserNameByAdminId(String accountManagerId);

    List<String> getAllAdminUsersEmailByRoleId(String offlinePayApproverRight);

    List<String> getLoginAdminUserAssignedRights(String loginAdminId);
}
