package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutConfirmResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.ReceiptVM;
import com.enovax.star.cms.commons.tracking.Trackd;

import javax.servlet.http.HttpSession;

/**
 * Created by jennylynsze on 10/10/16.
 */
public interface IPartnerRevalidationService {
    ApiResult<ReceiptVM> revalidateReceipt(Integer transId, PartnerAccount account);
    ApiResult<CheckoutConfirmResult> finaliseRevalTransAndRedirect(Integer transId, PartnerAccount account,  HttpSession session, Trackd trackd);
    ApiResult<String> checkStatus(String receiptNo);
    ApiResult<ReceiptVM> getRevalidatedReceiptByReceiptNum(String receiptNum, PartnerAccount account);
    ApiResult<ReceiptVM> getRevalidatedReceiptByTransId(Integer transId, PartnerAccount account);
}
