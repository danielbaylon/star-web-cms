package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTable;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 12/8/16.
 */
@Service("PPMFLGIPartnerExtensionService")
public class DefaultPartnerExtensionService implements IPartnerExtensionService {

    private static final String EMPTY = "";

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerExtensionService.class);

    @Autowired
    private AxChannelCustomerService axCustSrv;

    @Override
    public boolean updatePartnerExtProperties(AxCustomerTable c, boolean enablePartnerPortal) {
        try{
            ApiResult<Boolean> result = internalUpdatePartnerExtProperties(c, enablePartnerPortal);
            if(result == null){
                return false;
            }else{
                if(result.isSuccess()){
                    return true;
                }else{
                    log.info("Update partner extension properties failed; Data["+(JsonUtil.jsonify(c))+"], Error: "+result.getMessage());
                }
            }
            return false;
        }catch (Exception ex1){
            ex1.printStackTrace();
            log.info("Update partner extension properties failed; Data["+(JsonUtil.jsonify(c))+"], Error: "+ex1.getMessage(), ex1);
            return false;
        }catch (Throwable ex2) {
            ex2.printStackTrace();
            log.info("Update partner extension properties failed; Data[" + (JsonUtil.jsonify(c)) + "], Error: " + ex2.getMessage(), ex2);
            return false;
        }
    }

    @Override
    public AxCustomerTable popuplateCustoemrTable(PPMFLGPartner pa, boolean enablePartnerPortal, boolean allowOnAccount, List<PPMFLGPADocMapping> docMap, List<PPMFLGPaDistributionMapping> marketDistributionList, String documnetTypeId, String capacityGroupId, String lineOfBusiness) throws ParseException {
        AxCustomerTable c = new AxCustomerTable();
        c.setAccountMgrCMS(EMPTY);
        c.setAccountNum(EMPTY);
        c.setAllowOnAccount(0);
        c.setContactPersonStr(EMPTY);
        c.setDesignation(EMPTY);
        c.setDocumentLinkDelimited(EMPTY);
        c.setFax(EMPTY);
        c.setFaxExtension(EMPTY);
        c.setMarketDistributionDelimited(EMPTY);
        c.setReservationAmendment(0);
        c.setUenNumber(EMPTY);
        c.setTaLicenseNumber(EMPTY);
        c.setBranchName(EMPTY);
        c.setCapacityGroupId(EMPTY);
        c.setCountriesInterestedIn(EMPTY);
        c.setLanguageId(EMPTY);
        c.setMainAccLoginName(EMPTY);
        c.setOfficeNo(EMPTY);
        c.setLineOfBusiness(EMPTY);
        c.setCountryCode(EMPTY);

        c.setAccountMgrCMS(pa.getAccountManagerId());
        c.setCountryCode(pa.getCountryCode());
        c.setAccountNum(pa.getAxAccountNumber());
        c.setAllowOnAccount(allowOnAccount ? 1 : 0);
        c.setContactPersonStr(pa.getContactPerson());
        c.setDesignation(pa.getContactDesignation());
        c.setLineOfBusiness(lineOfBusiness);
        c.setCountriesInterestedIn(pa.getMainDestinations());
        StringBuilder documentLink = new StringBuilder();
        if(docMap != null && docMap.size() > 0){
            for(PPMFLGPADocMapping dm : docMap){
                PPMFLGPADocument d = dm.getPaDoc();
                if(d != null){
                    if(documentLink.length() > 0){
                        documentLink.append("|");
                    }
                    documentLink.append(d.getFileName()).append(";").append(documnetTypeId).append(";").append(d.getAxFileViewPath());
                }
            }
        }
        if(documentLink != null && documentLink.length() > 0){
            c.setDocumentLinkDelimited(documentLink.toString());
        }
        c.setFax(pa.getFaxNum());
        StringBuilder market = new StringBuilder();
        if(marketDistributionList != null && marketDistributionList.size() > 0){
            for(PPMFLGPaDistributionMapping i : marketDistributionList){
                if(market.length() > 0){
                    market.append("|");
                }
                market.append(i.getCountryId()).append(";").append(i.getPercentage()).append(";").append(i.getAdminId());
            }
        }
        if(market != null && market.length() > 0) {
            c.setMarketDistributionDelimited(market.toString());
        }
        c.setRegisteredDate(pa.getCreatedDate());

        if(pa.getUen() != null && pa.getUen().trim().length() > 0){
            c.setUenNumber(pa.getUen());
        }
        if(pa.getLicenseNum() != null){
            c.setTaLicenseNumber(pa.getLicenseNum());
        }
        if(pa.getLicenseExpDate() != null){
            c.setTaExpiryDate(NvxDateUtils.parseDate(pa.getLicenseExpDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        if(pa.getBranchName() != null && pa.getBranchName().trim().length() > 0){
            c.setBranchName(pa.getBranchName());
        }

        if(pa.getDailyTransCap() != null){
            c.setDailyTransCapValue(pa.getDailyTransCap().doubleValue());
        }
        if(pa.getTelNum() != null && pa.getTelNum().trim().length() > 0){
            c.setOfficeNo(pa.getTelNum());
        }

        if(capacityGroupId != null && capacityGroupId.trim().length() > 0){
            c.setCapacityGroupId(capacityGroupId);
        }

        c.setRevalidationPeriod(pa.getRevalPeriodMonths());

        c.setEnablePartnerPortal(enablePartnerPortal ? 1 : 0);
        PPMFLGTAMainAccount mainAccount = pa.getMainAccount();

        c.setMainAccLoginName(mainAccount.getUsername());
        c.setRevalidationFee(pa.getRevalFeeItemId());
        c.setReservationAmendment(0);
        return c;
    }

    private ApiResult<Boolean> internalUpdatePartnerExtProperties(AxCustomerTable c, boolean enablePartnerPortal) throws Exception {
        ApiResult<Boolean> booleanApiResult = axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, c, enablePartnerPortal);
        return booleanApiResult;
    }

    private String getNonEmptyStringValue(String v) {
        if (v != null && v.trim().length() > 0) {
            return v.trim();
        }
        return "";
    }

    private String getFormatedDate(Date licenseExpDt) {
        if(licenseExpDt == null){
            return "";
        }
        return NvxDateUtils.formatDateForDisplay(licenseExpDt, false);
    }
}
