package com.enovax.star.cms.partnershared.constant;

/**
 * Created by houtao on 20/9/16.
 */
public enum PartnerExtAttrNameFormat {

    DATE_ddMMyyyy("dd/MM/yyyy");

    public final String code;

    private PartnerExtAttrNameFormat(String code) {
        this.code = code;
    }


}
