package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGReason;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGReasonLogMap;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReasonVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPPMFLGReasonLogMapRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGReasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service("PPMFLGIReasonService")
public class DefaultReasonService implements IReasonService {

    @Autowired
    private PPMFLGReasonRepository reasonRepo;

    @Autowired
    private PPMFLGPPMFLGReasonLogMapRepository reasonLogMapRepo;

    @Transactional(readOnly = true)
    public List<ReasonVM> populateActiveReasonVms() {
        List<ReasonVM> vms = new ArrayList<ReasonVM>();
        List<PPMFLGReason> lst = reasonRepo.findAllReasonByStatus(GeneralStatus.Active.code);
        if(lst != null && lst.size() > 0){
            for(PPMFLGReason i : lst){
                vms.add(new ReasonVM(i));
            }
        }
        return vms;
    }

    @Transactional(readOnly = true)
    public List<ReasonVM> populateReasonsByIds(List<Integer> reasonIds) {
        List<ReasonVM> reasonVMs = new ArrayList<ReasonVM>();
        if(reasonIds != null && reasonIds.size() > 0){
            for(Integer id :reasonIds){
                PPMFLGReason reason  = reasonRepo.findById(id);
                if(reason != null) {
                    reasonVMs.add(new ReasonVM(reason));
                }
            }
        }
        return reasonVMs;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReasonVM> getReasonsByLogId(Integer logId){
        List<PPMFLGReasonLogMap> reasonMaps = reasonLogMapRepo.findByLogId(logId);
        List<ReasonVM> reasonVms = new ArrayList<ReasonVM>();
        for(PPMFLGReasonLogMap reasonMap:reasonMaps){
            reasonVms.add(new ReasonVM(reasonMap.getReason()));
        }
        return reasonVms;
    }


    @Override
    @Transactional(readOnly = true)
    public ResultVM getReasonsbyPage(String orderBy, String orderWith, Integer page, Integer pagesize){
        ResultVM res = new ResultVM();
        if(orderWith == null) {
            orderWith = "DESC";
            orderBy = "id";
        }
        PageRequest pageRequest = new PageRequest( page - 1, pagesize,
                "ASC".equalsIgnoreCase(orderWith) ? Sort.Direction.ASC : Sort.Direction.DESC,
                orderBy);
        Page<PPMFLGReason> reasonByPage = reasonRepo.getReasonByPage(pageRequest);
        Iterator<PPMFLGReason> reasonIterator = reasonByPage.iterator();
        List<ReasonVM> reasonVMs=new ArrayList<>();
        while(reasonIterator.hasNext()) {
            PPMFLGReason reason = reasonIterator.next();
            ReasonVM reasonVM = new ReasonVM(reason);
            reasonVMs.add(reasonVM);
        }
        int total = reasonRepo.getReasonSize();
        res.setViewModel(reasonVMs);
        res.setTotal(total);
        return res;
    }

    @Override
    @Transactional
    public ReasonVM saveReason(ReasonVM reasonVm, String userNm) {
        PPMFLGReason reason = null;
        if(reasonVm.getId()!=null&&reasonVm.getId()>0){
            reason = reasonRepo.findById(reasonVm.getId());
        }else{
            reason = new PPMFLGReason();
            reason.setCreatedBy(userNm);
            reason.setCreatedDate(new Date());
        }
        reason.setTitle(reasonVm.getTitle());
        reason.setContent(reasonVm.getContent());
        reason.setStatus(reasonVm.getStatus());
        reason.setModifiedBy(userNm);
        reason.setModifiedDate(new Date());
        reasonRepo.save(reason);
        return new ReasonVM(reason);
    }

    @Override
    @Transactional
    public void removeReasons(List<Integer> rcmdIds, String userNm) {
        for(Integer id:rcmdIds){
            PPMFLGReason reason = reasonRepo.findById(id);
            reason.setStatus(GeneralStatus.Deleted.code);
            reason.setModifiedBy(userNm);
            reason.setModifiedDate(new Date());
            reasonRepo.save(reason);
        }
    }

}
