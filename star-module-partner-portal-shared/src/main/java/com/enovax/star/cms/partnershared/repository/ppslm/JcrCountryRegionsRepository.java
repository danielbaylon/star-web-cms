package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCountryRegion;
import com.enovax.star.cms.commons.model.partner.ppslm.CountryVM;
import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.*;

/**
 * Created by houtao on 22/9/16.
 */
@Service
public class JcrCountryRegionsRepository implements ICountryRegionsRepository {

    private static final String COUNTRY_REGIONS = "countries";

    private static volatile int list_refresh_count = 0;
    private static volatile List<AxCountryRegion> list = new ArrayList<AxCountryRegion>();

//    @Override
//    public boolean deleteCountryById(String channel, String ctryId) {
//        if (ctryId == null || ctryId.trim().length() == 0) {
//            return false;
//        }
//        ctryId = ctryId.trim();
//        try {
//            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+ channel.toLowerCase() + "/" + COUNTRY_REGIONS + "/" + ctryId);
//            if(node != null && node.getName() != null && node.getName().trim().endsWith(COUNTRY_REGIONS)){
//                throw new Exception("Deleting root node of "+COUNTRY_REGIONS+", deleting failed.");
//            }
//            //node.remove();
//            PublishingUtil.deleteNodeAndPublish(node,JcrWorkspace.CMSConfig.getWorkspaceName());
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

//    @Override
//    public boolean createCountry(String channel, AxCountryRegion info) {
//        try {
//            Node subnode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS + "/", info.getCountryId() + "-" + info.getRegionId(), "mgnl:content");
//
//            if (subnode == null) {
//                subnode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS + "/" + info.getCountryId() + "-" + info.getRegionId());
//            }
//            if (subnode == null) {
//                return false;
//            }
//
//            subnode.setProperty("countryRegionId", info.getCountryId());
//            subnode.setProperty("shortName", info.getCountryName());
//            subnode.setProperty("regionId", info.getRegionId());
//
//            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), subnode);
//            try{
//                PublishingUtil.publishNodes(subnode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
//                return true;
//            }catch (Exception ex){
//                ex.printStackTrace();
//            }
//            return true;
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

    @Override
    public boolean saveCountry(String channel, AxCountryRegion info) {

        Node subnode = null;

        try {
            subnode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS + "/" + info.getCountryId() + "-" + info.getRegionId());
        }catch (RepositoryException e)  {
            //create the subnode
            try {
                subnode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS + "/", info.getCountryId() + "-" + info.getRegionId(), "mgnl:content");
            } catch (RepositoryException e1) {
                e1.printStackTrace();
            }
        }

        if(subnode== null) {
            return false;
        }

        try {
            subnode.setProperty("countryRegionId", info.getCountryId());
            subnode.setProperty("shortName", info.getCountryName());
            subnode.setProperty("regionId", info.getRegionId());
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), subnode);
            try{
                PublishingUtil.publishNodes(subnode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
                return true;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }


        return false;
    }

//    @Override
//    public AxCountryRegion getCountryById(String channel, String ctryId) {
//        if (ctryId == null || ctryId.trim().length() == 0) {
//            return null;
//        }
//        ctryId = ctryId.trim();
//        try {
//            AxCountryRegion i = null;
//            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+"/" + COUNTRY_REGIONS + "/" + ctryId);
//            if (node.hasProperty("countryRegionId")) {
//                i = new AxCountryRegion();
//                i.setCountryRegionId(node.getProperty("countryRegionId").getValue().getString());
//                if (node.hasProperty("shortName")) {
//                    i.setShortName(node.getProperty("shortName").getValue().getString());
//                }
//                if (node.hasProperty("longName")) {
//                    i.setLongName(node.getProperty("longName").getValue().getString());
//                }
//                if (node.hasProperty("isoCode")) {
//                    i.setISOCode(node.getProperty("isoCode").getValue().getString());
//                }
//                if (node.hasProperty("timeZone")) {
//                    i.setTimeZone(node.getProperty("timeZone").getValue().getString());
//                }
//                if (node.hasProperty("languageId")) {
//                    i.setLanguageId(node.getProperty("languageId").getValue().getString());
//                }
//                return i;
//            }
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    @Override
//    public List<CountryVM> getAllCountryVmList(String channel) {
//        List<CountryVM> list = new ArrayList<CountryVM>();
//        List<String> countryIdList = new ArrayList<>();
//        try {
//            CountryVM i = null;
//            Node root = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS);
//            if (root != null) {
//                NodeIterator iter = root.getNodes();
//                if (iter != null) {
//                    while (iter.hasNext()) {
//
//                        Node node = iter.nextNode();
//                        String countryId = PropertyUtil.getString(node, "countryId");
//
//                        //get unique country only
//                        if(countryIdList.contains(countryId)) {
//                            continue;
//                        }
//
//                        i = new CountryVM();
//                        i.setCtyCode(countryId);
//                        i.setCtyName(PropertyUtil.getString(node, "countryName"));
//                        i.setRegionId(PropertyUtil.getString(node, "regiondId"));
//                        list.add(i);
//                        countryIdList.add(countryId);
//                    }
//                }
//            }
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
//        return list;
//    }

    @Override
    public List<AxCountryRegion> getCountryList(String channel) {
        List<AxCountryRegion> list = new ArrayList<AxCountryRegion>();
        List<String> countryIdList = new ArrayList<>();
        try {
            AxCountryRegion i = null;
            Node root = null;
            root = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.toLowerCase() + "/" + COUNTRY_REGIONS);

            if (root != null) {
                NodeIterator iter = root.getNodes();
                if (iter != null) {
                    while (iter.hasNext()) {
                        Node node = iter.nextNode();
                        String countryId = PropertyUtil.getString(node, "countryRegionId");

                        //get unique country only
                        if(countryIdList.contains(countryId)) {
                            continue;
                        }

                        i = new AxCountryRegion();
                        i.setCountryId(countryId);
                        i.setCountryName(PropertyUtil.getString(node, "shortName"));
                        i.setRegionId(PropertyUtil.getString(node, "regiondId"));
                        list.add(i);
                        countryIdList.add(countryId);

                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<CountryVM> getCountryVmList(String channel){
        if(list_refresh_count == 0){
            refreshCache(channel);
        }

        List<CountryVM> tempList = new ArrayList<CountryVM>();
        for(AxCountryRegion i : list){
            CountryVM v = new CountryVM();
            v.setCtyCode(i.getCountryId());
            v.setCtyName(i.getCountryName());
            v.setRegionId(i.getRegionId());
            tempList.add(v);
        }

        Collections.sort(tempList, new Comparator<CountryVM>() {
            @Override
            public int compare(CountryVM o1, CountryVM o2) {
                return o1.getCtyName().compareTo(o2.getCtyName());
            }
        });
        return tempList;
    }

    @Override
    public boolean existsCountryId(String channel, String ctryId, String regionId) {
        if (ctryId == null || ctryId.trim().length() == 0) {
            return false;
        }
        ctryId = ctryId.trim();
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+channel.toLowerCase()+ "/" + COUNTRY_REGIONS + "/" + ctryId + "-" + regionId);
            String country = PropertyUtil.getString(node, "countryRegionId");

            if(country != null && country.equals(ctryId)) {
                return true;
            }

            return false;
        } catch (RepositoryException e) {
        }
        return false;
    }

    @Override
    public void deleteCountryIfNotInList(String channel, List<String> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase()+"/" + COUNTRY_REGIONS);
            NodeIterator iter = node.getNodes();
            while (iter.hasNext()) {
                Node subNode = iter.nextNode();
                if (!list.contains(subNode.getName())) {
                    try{
                        PublishingUtil.deleteNodeAndPublish(subNode,JcrWorkspace.CMSConfig.getWorkspaceName());
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getCountryNameByCountryId(String channel, String id) {
        if(id == null || id.trim().length() == 0){
            return null;
        }
        try {
            Iterable<Node> countryRegionList = JcrRepository.query(JcrWorkspace.CMSConfig.getWorkspaceName(), "mgnl:content", "/jcr:root/" + channel.toLowerCase()  + "/countries//element(*, mgnl:content)[@countryRegionId= '" + id + "']");
            Iterator<Node> countryRegionIterator = countryRegionList.iterator();

            if(countryRegionIterator.hasNext()) {
                Node country = countryRegionIterator.next();
                return PropertyUtil.getString(country, "shortName");
            }
        } catch (RepositoryException e) {
        }
        return id; //return the ID if not found
    }

    @Override
    public void refreshCache(String channel) {
        list_refresh_count = 1;
        List<AxCountryRegion> tempList = getCountryList(channel);
        list.clear();
        list.addAll(tempList);
    }
}
