package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservation;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 7/9/16.
 */
public interface IPartnerWoTEmailService {


    public void sendingWoTCancellationReminderEmail(String orgName, String mainAccEmail, PPMFLGWingsOfTimeReservation wot);

    public void sendingWoTConfirmationEmail(PartnerAccount account, PPMFLGWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTAmendmentEmail(PartnerAccount account, PPMFLGWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTSplitConfirmationEmail(PartnerAccount account, PPMFLGWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTCancellationEmail(PartnerAccount account, PPMFLGWingsOfTimeReservation wot, String barcodeImageString);

    void sendingAdminWoTConfirmationEmail(PPMFLGTAMainAccount paMainAcc, PPMFLGWingsOfTimeReservation reservation, String createdByUsername);

    void sendingAdminWoTCancellationEmail(PPMFLGTAMainAccount mainAccount, PPMFLGWingsOfTimeReservation reservation, String createdByUsername);

    void sendingAdminWoTAmendmentEmail(PPMFLGTAMainAccount mainAccount, PPMFLGWingsOfTimeReservation wot, String createdByUsername);

    void sendingAdminWoTReleaseEmail(PPMFLGTAMainAccount account, PPMFLGWingsOfTimeReservation wot, String adminUserId, String releaseDate);

    void sendingAdminWoTPostReleaseEmailNotification(PPMFLGTAMainAccount mainAccount, PPMFLGWingsOfTimeReservation wot, String createdBy, String releaseDateText);

    String generateWoTReservationDailyReceipts(PartnerAccount account, List<PPMFLGWingsOfTimeReservation> list, Date date);

    String generateWoTReservationMonthlyReceipts(PartnerAccount account, List<PPMFLGWingsOfTimeReservation> list, Date date);
}
