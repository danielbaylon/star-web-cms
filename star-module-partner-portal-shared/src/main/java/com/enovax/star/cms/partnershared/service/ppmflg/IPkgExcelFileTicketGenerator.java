package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by jennylynsze on 9/30/16.
 */
public interface IPkgExcelFileTicketGenerator {
    Workbook generatePkgExcel(MixMatchPkgVM mixMatchPkgVM, List<ETicketData> tickets);
}
