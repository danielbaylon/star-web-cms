package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.system.SysParamVM;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

public interface ISystemParamService {

    boolean isRecaptchaEnabled();

    String getRecaptchaPrivateKey();

    String getRecaptchaPublicKey();

    String getUserRequestAddress(HttpServletRequest request);

    public String getUserRequestHeaderAddress(HttpServletRequest request);

    String getPartnerDocumentRootDir();

    Map<String,String> getEmailConfigs();

    Map<String,String> getAdminEmailCfgs();

    long getPartnerDocumentMaximumSizeLimit();

    String getPartnerDocumentSupportedFileType();

    public int getTicketRevalidationPeriod();

    public String getApplicationContextPath();

    public String getAdminApplicationContextPath();

    public String getPartnerPortalSLMRootPath();

    public String getPartnerPortalSLMLogoPath();

    public String isAXPartnerRegistrationServiceAvailable();

    String getDefaultPartnerRegistrationEmailReceiver();

    public Map<String, String> getSentosaContactDetails();

    public Map<String, String> getAxFtpConfigs();

    int getPartnerLoginMaximumAttempts();

    int getUserPasswordValidationMaximumPastPasswords();

    int getWingsOfTimeShowCutoffInMinutes();

    Integer getWingsOfTimeReservationType();

    /*public Integer getWoTAdminReservationCap();*/

    public Integer getWotAdminReservationPeriodLimit();

    public boolean getWoTeTicketEnabled();

    public Integer getPartnerWoTAdvancedDaysReleaseBackendReservedTickets();

    public Integer getPartnerWoTReservationCap();

    public Integer getPartnerOnlinePaymentTime();

    public Integer getPartnerTicketonHoldTime();

    String getPartnerTransactionReceiptRootDir();

    int getTicketExpiringAlertPeriod();

    public int getTicketAllowRevalidatePeriod();

    public int getPurchaseMinQtyPerTxn();

    public BigDecimal getPurchaseMaxDailyTxnLimit();

    public BigDecimal getPurchaseMaxAmountPerTxn();

    int getMaxNoOfSubUsers();

    int getMaxTypeOfItemsinPackage();

    int getMaxLimitPerTransaction();

    int getMaxTotalDailyTransaction();

    Long getDefaultPartnerRegistrationCustomerType();

    String getDefaultPartnerRegistrationCurrencyCode();

    Integer getPartnerRegistrationBizAddrType();

    Integer getPartnerRegistrationInvoiceAddressType();

    boolean getPartnerRegistrationAllowOnAccount();

    public int getRecsPerPage();

    String getPartnerRegistrationAxDocumentTypeId();

    int getMinimumNoOfTicketsPurchasePerTxn();

    String getPartnerOwnerOrgName();

    public String getSystemParamValueByKey(String key);

    public SysParamVM getTicketRevalidationPeriodVM();

    public SysParamVM getTicketAllowRevalidatePeriodVM();

    public SysParamVM getTicketExpiringAlertPeriodVM();

    public SysParamVM getMaxNoOfSubUsersVM();

    public SysParamVM getMinimumNoOfTicketsPurchasePerTxnVM();

    public SysParamVM getMaxTypeOfItemsinPackageVM();

    public SysParamVM getMaxLimitPerTransactionVM();

    public SysParamVM getMaxTotalDailyTransactionVM();

    Integer getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminder();

    public SysParamVM getWingsOfTimeShowCutoffInMinutesVM();

    public SysParamVM getWoTAdminReservationCapVM();


    public SysParamVM getWotAdminReservationPeriodLimitVM();

    public SysParamVM getWoTeTicketEnabledVM();

    public SysParamVM getPartnerWoTAdvancedDaysReleaseBackendReservedTicketsVM();

    public SysParamVM getPartnerWoTReservationCapVM();

    public SysParamVM getPartnerOnlinePaymentTimeVM();

    public SysParamVM getPartnerTicketonHoldTimeVM();

    public SysParamVM getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminderVM();

    float getMinimumDepositTopupAmount() throws BizValidationException;

    String getDepositTopupAxPaymentType();

    String getPartnerDocumentAxRootDir();

    String getDefaultPartnerRegistrationShortCurrencyCode();

    boolean isPPMFLGWoTRefreshUnredeemedPinCodeJobEnabled();

    boolean isPPMFLGWoTBackendReservationReminderAndReleaseJobEnabled();

    boolean isPPMFLGAxPriceGroupSyncJobEnabled();

    boolean isPPMFLGAxLineOfBusinessJobEnabled();

    boolean isPPMFLGAxCountryRegionSyncJobEnabled();

    boolean isPPMFLGReUpdateCustomerExtensionsJobEnabled();

    boolean isPPMFLGWoTUnredeemedPinCodeCanncelReminderJob();

    boolean isPPMFLGDepositTopupTelemoneyQueryJobEnabled();

    public int getDepositTopupTeleemoneyQuerySentLowerBoundMinutesAfterRequestSent();

    public int getDepositTopupTeleemoneyQuerySentUpperBoundMinutesAfterRequestSent();

    boolean isTMServiceEnabled();

    int getPackageOverdueCleanTime();
}
