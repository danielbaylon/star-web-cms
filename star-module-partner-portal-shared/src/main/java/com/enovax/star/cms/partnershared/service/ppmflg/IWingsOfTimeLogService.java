package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservation;

/**
 * Created by houtao on 26/8/16.
 */
public interface IWingsOfTimeLogService {
    void saveWingsOfTimeReservationLog(PPMFLGWingsOfTimeReservation wot, String channel, String serviceName, boolean success, String errorCode, String errorMsg, String request, String response);
}
