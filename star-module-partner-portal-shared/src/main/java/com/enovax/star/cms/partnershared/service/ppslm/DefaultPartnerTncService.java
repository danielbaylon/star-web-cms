package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by jennylynsze on 9/14/16.
 */
@Service
public class DefaultPartnerTncService implements  IPartnerTncService {

    @Autowired
    private
    StarTemplatingFunctions starfn;

    @Override
    public List<TncVM> getProductTncs(StoreApiChannels channel, List<String> prodIds) {

        final List<TncVM> tncs = new ArrayList<>();

        List<TncVM> gtncs = starfn.getGeneralTermsAndConditions(channel.code, Locale.ENGLISH.toString());
        for (TncVM tmpObj : gtncs) {
            tmpObj.setTitle("");
        }

        tncs.addAll(gtncs);


        if(prodIds != null) {
            final Set<String> noDupesProductIds= new LinkedHashSet<>(prodIds);
            List<String> noDupesProductIdsList = new ArrayList<>();
            noDupesProductIdsList.addAll(noDupesProductIds);

            List<TncVM> ptncs = starfn.getProductTermsAndConditions(channel.code, noDupesProductIdsList, Locale.ENGLISH.toString());
            if(ptncs != null){
                tncs.addAll(ptncs);
            }
        }
        return tncs;
    }

}
