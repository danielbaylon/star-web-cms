package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionItemVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/16/16.
 */
@Repository("PPMFLGIPartnerBookingRepository")
public class JcrPartnerBookingRepository implements IPartnerBookingRepository {
    @Override
    public void saveStoreTransaction(InventoryTransactionVM txn) {
        //save the inventory
        int txnId = getNexInventoryTransactiontId();
        txn.setId(txnId);

        try {
            //create the inventory txn
           Node txnNode =  JcrRepository.createNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/", txnId + "", "mgnl:content");
           txnNode.setProperty("data", JsonUtil.jsonify(txn)) ;
           txnNode.setProperty("receiptNum", txn.getReceiptNumber());
           txnNode.setProperty("status", txn.getStatus());

           JcrRepository.updateNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), txnNode);

            //create the inventory txn item
            List<InventoryTransactionItemVM> items = txn.getItems();
            for(InventoryTransactionItemVM item: items) {
                int txnItemId = getNextInventoryTransactionItemId();
                item.setId(txnItemId);
                item.setTransId(txnId);
                Node txnItemNode = JcrRepository.createNode(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), "/", txnItemId + "", "mgnl:content");
                txnItemNode.setProperty("data", JsonUtil.jsonify(item));
                txnItemNode.setProperty("transId", txnId + "");
                txnItemNode.setProperty("unpackagedQty", item.getUnpackagedQty() + "");
                txnItemNode.setProperty("tmStatusDate", txn.getTmStatusDate().getTime() + "");
                txnItemNode.setProperty("validityEndDate", txn.getValidityEndDate().getTime() + "");
                txnItemNode.setProperty("mainAccountId", txn.getMainAccountId() + "");
                txnItemNode.setProperty("displayName", item.getName());
                txnItemNode.setProperty("id", txnItemId + "");
                JcrRepository.updateNode(JcrWorkspace.InventoryTransactionItems.getWorkspaceName(), txnItemNode);
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTransaction(InventoryTransactionVM txn) {
        try {
            Node txnNode = JcrRepository.getParentNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "/" + txn.getId());
            txnNode.setProperty("data", JsonUtil.jsonify(txn));
            txnNode.setProperty("status", txn.getStatus());
            JcrRepository.updateNode(JcrWorkspace.InventoryTransactions.getWorkspaceName(), txnNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public InventoryTransactionVM getStoreTransactionByReceipt(String receiptNumber) {
        try {
            Iterable<Node> txnIterable = JcrRepository.query(JcrWorkspace.InventoryTransactions.getWorkspaceName(), "mgnl:content", "//element(*, mgnl:content)[@receiptNum = '" + receiptNumber + "']");
            Iterator<Node> txnIterator = txnIterable.iterator();

            if(txnIterator.hasNext()) {
                Node txnNode = txnIterator.next();
                return JsonUtil.fromJson(txnNode.getProperty("data").getString(), InventoryTransactionVM.class);
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getNexInventoryTransactiontId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/inventory-transactions");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    @Override
    public Integer getNextInventoryTransactionItemId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/inventory-transaction-items");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }
}
