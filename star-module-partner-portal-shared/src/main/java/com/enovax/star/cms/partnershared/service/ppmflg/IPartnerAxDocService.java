package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;

import java.io.IOException;
import java.util.List;

/**
 * Created by houtao on 18/10/16.
 */
public interface IPartnerAxDocService {

    void syncPartnerDocuments(PPMFLGPartner partner, List<PPMFLGPADocMapping> docMap) throws BizValidationException, IOException;

    void downloadPartnerDocuments(PPMFLGPartner partner, List<PPMFLGPADocMapping> byPartnerId) throws BizValidationException, IOException;
}
