package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.WOTReservationVM;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 7/9/16.
 */
public interface IPartnerWoTEmailService {


    public void sendingWoTCancellationReminderEmail(String orgName, String mainAccEmail, PPSLMWingsOfTimeReservation wot);

    public void sendingWoTConfirmationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTAmendmentEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTSplitConfirmationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString);

    public void sendingWoTCancellationEmail(PartnerAccount account, PPSLMWingsOfTimeReservation wot, String barcodeImageString);

    void sendingAdminWoTConfirmationEmail(PPSLMTAMainAccount paMainAcc, PPSLMWingsOfTimeReservation reservation, String createdByUsername);

    void sendingAdminWoTCancellationEmail(PPSLMTAMainAccount mainAccount, PPSLMWingsOfTimeReservation reservation, String createdByUsername);

    void sendingAdminWoTAmendmentEmail(PPSLMTAMainAccount mainAccount, PPSLMWingsOfTimeReservation wot, String createdByUsername);

    void sendingAdminWoTReleaseEmail(PPSLMTAMainAccount account, PPSLMWingsOfTimeReservation wot, String adminUserId, String releaseDate);

    void sendingAdminWoTPostReleaseEmailNotification(PPSLMTAMainAccount mainAccount, PPSLMWingsOfTimeReservation wot, String createdBy, String releaseDateText);

    String generateWoTReservationDailyReceipts(PartnerAccount account, List<PPSLMWingsOfTimeReservation> list, Date date);

    String generateWoTReservationMonthlyReceipts(PartnerAccount account, List<PPSLMWingsOfTimeReservation> list, Date date);
}
