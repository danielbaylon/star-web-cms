package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservationLog;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMWingsOfTimeReservationLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by houtao on 26/8/16.
 */
@Service
public class DefaultWingsOfTimeLogService implements IWingsOfTimeLogService {

    @Autowired
    private PPSLMWingsOfTimeReservationLogRepository wotLogRepo;

    @Transactional
    public void saveWingsOfTimeReservationLog(PPSLMWingsOfTimeReservation wot, String channel, String serviceName, boolean success, String errorCode, String errorMsg, String request, String response) {
        PPSLMWingsOfTimeReservationLog log = null;
        try{
            log = new PPSLMWingsOfTimeReservationLog();
            log.setWotReserveId(wot.getId());
            log.setCreatedDateTime(new Date(System.currentTimeMillis()));
            log.setCreatedBy(wot.getCreatedBy());
            log.setSubAccountTrans(wot.isSubAccountTrans());
            log.setServiceName(serviceName);
            log.setChannel(channel);
            log.setErrorCode(errorCode);
            log.setErrorMsg(errorMsg);
            log.setRequestText(request);
            log.setResponseText(response);
            wotLogRepo.save(log);
        }catch (Exception ex){
            try{
                wotLogRepo.save(log);
            }catch (Exception ex2){}
        }
    }
}
