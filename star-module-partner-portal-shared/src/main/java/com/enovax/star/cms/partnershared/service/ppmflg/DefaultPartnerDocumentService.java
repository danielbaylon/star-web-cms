package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocument;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerDocumentVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPADocMappingRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPADocumentRepository;
import com.enovax.star.cms.commons.util.FileUtil;
import info.magnolia.cms.beans.runtime.Document;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service("PPMFLGIPartnerDocumentService")
public class DefaultPartnerDocumentService implements IPartnerDocumentService {

    public static final Logger log = LoggerFactory.getLogger(DefaultPartnerDocumentService.class);

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    private PPMFLGPADocumentRepository paDocRepo;

    @Autowired
    private PPMFLGPADocMappingRepository paDocMapRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerAxDocService")
    private IPartnerAxDocService axDocSrv;

    @Override
    public PPMFLGPADocument savePartnerDocuments(PPMFLGTAMainAccount taMainAcc, PPMFLGPartner pa, Document doc, String fileType) throws Exception {
        if(taMainAcc == null || pa == null || doc == null){
            return null;
        }
        String fileUploadPath = sysParamSrv.getPartnerDocumentRootDir();
        String partnerDocDir = getPartnerDocumentRootDir(taMainAcc.getAccountCode(), fileUploadPath);
        File file = doc.getFile();
        String hashedName = FileUtil.hashFile(doc.getFile());
        Random rand = new Random();
        hashedName = hashedName+"_"+ rand.nextInt(100)+1;
        log.debug("[saveFile] fileuploadPath." + partnerDocDir + file.getName());
        PPMFLGPADocument paDoc = savePartnerDocuments(
                taMainAcc.getAccountCode() + PartnerPortalConst.TA_MAIN_ACC_POSTFIX,
                taMainAcc.getAccountCode(), hashedName, file.getName(),fileType);

        savePartnerDocumentMapping(paDoc, pa);

        if(paDoc != null){
            FileUtils.copyFile(file, new File(partnerDocDir + hashedName));
        }
        return paDoc;
    }

    @Transactional(readOnly =  true)
    public PPMFLGPADocument getPartnerDocumentById(Integer id) {
        return paDocRepo.findById(id);
    }

    private void savePartnerDocumentMapping(PPMFLGPADocument paDoc, PPMFLGPartner pa) {
        PPMFLGPADocMapping map = new PPMFLGPADocMapping();
        map.setDocId(paDoc.getId());
        map.setPartnerId(pa.getId());
        paDocMapRepo.save(map);
    }

    private PPMFLGPADocument savePartnerDocuments(String username, String docDir, String hashedName, String name, String type) {
        PPMFLGPADocument doc = new PPMFLGPADocument();
        doc.setFileName(name);
        doc.setFilePath(docDir + File.separator + hashedName);
        doc.setFileType(type);
        doc.setStatus(GeneralStatus.Active.code);
        doc.setCreatedBy(username);
        doc.setCreatedDate(new Date(System.currentTimeMillis()));
        doc.setModifiedBy(username);
        doc.setModifiedDate(new Date(System.currentTimeMillis()));
        paDocRepo.save(doc);
        return doc;
    }

    private String getPartnerDocumentRootDir(String accountCode, String fileUploadPath) throws Exception {
        if(accountCode == null || accountCode.trim().length() == 0){
            throw new Exception("ERROR_EMPTY_ACC_CODE");
        }
        return fileUploadPath + File.separator + accountCode.trim() + File.separator;
    }

    @Transactional(readOnly =  true)
    public List<PartnerDocumentVM> getPartnerDocumentsByPartnerId(Integer paId) {
        List<PartnerDocumentVM> docs = new ArrayList<PartnerDocumentVM>();
        if(paId != null) {
            List<PPMFLGPADocMapping> maps = paDocMapRepo.findByPartnerId(paId);
            if (maps != null) {
                for (PPMFLGPADocMapping map : maps) {
                    PPMFLGPADocument doc = map.getPaDoc();
                    if(doc != null){
                        docs.add(new PartnerDocumentVM(doc));
                    }
                }
            }
        }
        return docs;
    }

    @Override
    public boolean isValidPartnerDocument(Document doc) {
        if(doc == null){
            return true;
        }
        if(!(doc.getLength() > 0 && doc.getLength() < sysParamSrv.getPartnerDocumentMaximumSizeLimit())){
            return false;
        }
        String ext = doc.getExtension();
        if(ext == null || ext.trim().length() == 0){
            return false;
        }
        String supportedType = sysParamSrv.getPartnerDocumentSupportedFileType();
        if(supportedType == null || supportedType.trim().length() == 0){
            return false;
        }
        if(!(supportedType.trim().toUpperCase().indexOf("#"+ext.trim().toUpperCase()+"#") >= 0)){
            return false;
        }
        return true;
    }
}
