package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppslm.UserResult;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 8/9/16.
 */
@Service
public class DefaultPartnerAccountSharedService implements IPartnerAccountSharedService {

    @Autowired
    private PPSLMTAMainAccountRepository mainAccRepo;

    @Autowired
    private PPSLMTASubAccountRepository subAccRepo;

    @Autowired
    private IPartnerSharedService paSharedSrv;

    @Autowired
    private PPSLMPartnerRepository paRepo;

    @Autowired
    private IPartnerSharedAccountService paSharedAccSrv;

    @Autowired
    private PPSLMTARightsMappingRepository taRightMapRepo;

    @Autowired
    private PPSLMTAAccessRightsGroupRepository acgRepo;

    @Override
    @Transactional
    public ApiResult<String> savePartnerSubAccountRightsChanges(Integer mainAccId, String loginUserName, List<PartnerAccountVM> accounts) throws BizValidationException {
        if(accounts != null && accounts.size() > 0){
            for(PartnerAccountVM a : accounts){
                PPSLMTASubAccount acc = subAccRepo.findById(a.getId());
                if(acc == null){
                    throw new BizValidationException("Invalid request, sub-account not found.");
                }
                if(acc.getMainUser().getId().intValue() != mainAccId.intValue()){
                    throw new BizValidationException("Invalid request, main-account and sub-account are not match.");
                }
                List<PPSLMTARightsMapping> mappings = acc.getRightsMapping();
                List<Integer> rightIds = a.getRightsIds();
                if(rightIds == null || rightIds.size() == 0){
                    if(mappings != null){
                        for(PPSLMTARightsMapping m : mappings){
                            taRightMapRepo.delete(m);
                        }
                    }
                }else{
                    // remove rights which not found in list
                    if(mappings != null){
                        for(PPSLMTARightsMapping m : mappings){
                            if(rightIds.contains(m.getId())){
                                rightIds.remove(m.getId());
                            }else{
                                taRightMapRepo.delete(m);
                            }
                        }
                    }
                    for(Integer rightId : rightIds){
                        PPSLMTAAccessRightsGroup acg = acgRepo.findById(rightId);
                        if(acg == null || (!"S".equalsIgnoreCase(acg.getType()))){
                            throw new BizValidationException("Invalid request, access configuration not found");
                        }
                        PPSLMTARightsMapping mapping = new PPSLMTARightsMapping();
                        mapping.setAccessRightsGroup(acg);
                        mapping.setSubUser(acc);
                        taRightMapRepo.save(mapping);
                    }
                }
            }
        }
        return new ApiResult<>(true, "", "", "");
    }

    @Transactional(readOnly = true)
    public String getPartnerAccountNameByIdWithCache(Map<String, String> nameMapping, boolean subAccountTrans, String createdBy) {
        if(createdBy == null || createdBy.trim().length() == 0 | nameMapping == null){
            return null;
        }
        int accountId = -1;
        try{
            accountId = Integer.parseInt(createdBy.trim());
        }catch (Exception ex){
            return null;
        }
        String key = (subAccountTrans ? "S" : "M")+createdBy;
        if(nameMapping.containsKey(key)){
            return nameMapping.get(key);
        }

        PPSLMTAAccount account = null;
        if(subAccountTrans){
            account = subAccRepo.findById(accountId);
        }else{
            account = mainAccRepo.findById(accountId);
        }
        if(account != null){
            nameMapping.put(key, account.getName());
        }
        return nameMapping.get(key);
    }

    @Transactional(readOnly = true)
    public PPSLMTAMainAccount getActivePartnerMainAccountByPartnerId(Integer partnerId) {
        if(partnerId == null || partnerId.intValue() == 0){
            return null;
        }
        PPSLMPartner partner = paSharedSrv.getPartnerById(partnerId);
        if(partner == null){
            return null;
        }
        if(!PartnerStatus.Active.code.equals(partner.getStatus())){
            return null;
        }
        return partner.getMainAccount();
    }

    @Transactional(readOnly = true)
    public PPSLMTAMainAccount getActivePartnerByMainAccountId(Integer mainAccountId) {
        if(mainAccountId == null || mainAccountId.intValue() == 0){
            return null;
        }
        return mainAccRepo.findById(mainAccountId);
    }

    @Transactional(readOnly = true)
    public PPSLMTAMainAccount getPartnerMainAccountByMainAccountId(Integer mainAccId) {
        if(mainAccId == null || mainAccId.intValue() == 0){
            return null;
        }
        return mainAccRepo.findById(mainAccId);
    }



    @Override
    @Transactional
    public com.enovax.star.cms.commons.model.api.ApiResult<String> saveSubuser(int paId, boolean isNew, Integer userId, String userName, String name, String email, String title, String status, String access, String createdBy, String pwd, String encodedPwd) throws Exception{
        PPSLMPartner pa = paRepo.findById(paId);
        Integer mainAccId = pa.getMainAccount().getId();
        if(isNew) {
            userName = pa.getAccountCode() + "_" + userName;
        }
        if(isNew && paSharedAccSrv.getUser(userName, false)!= null) {
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.SubAccountAlreadyExists);
        }
        String[] s;
        if(access==null || access.isEmpty()){
            s = new String[0];
        }else{
            s = access.split(",");
        }
        if(paSharedAccSrv.exceedSubUsers(mainAccId, userId)){
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.MaxSubAccountExceeded);
        }
        final UserResult result = paSharedAccSrv.saveSubUser(isNew, userId, userName, pwd, encodedPwd, email, s, status, createdBy, name, mainAccId, title);

        List<PPSLMTASubAccount> accounts =  paSharedAccSrv.getSubUsers(pa.getMainAccount(),true);

        List<PartnerAccountVM> accVms = new ArrayList<PartnerAccountVM>();
        for(PPSLMTASubAccount acc:accounts){
            //Hibernate.initialize(acc.getRightsMapping());
            PPSLMTASubAccount tmpObj = paSharedAccSrv.getSubUser(acc.getId(), true);
            ArrayList<Integer> rightsIds = new ArrayList<Integer>();
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(tmpObj);
            List<PPSLMTARightsMapping> rightsMappingList = taRightMapRepo.findBySubUser(tmpObj);
            if(rightsMappingList!=null){
                for(PPSLMTARightsMapping temp: rightsMappingList){
                    PPSLMTAAccessRightsGroup group = temp.getAccessRightsGroup();
                    rightsIds.add(group.getId());
                }
            }
            partnerAccountVM.setRightsIds(rightsIds);
            accVms.add(partnerAccountVM);
        }
        String accountsJson = JsonUtil.jsonify(accVms);
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", accountsJson);
    }

    @Transactional
    public boolean hasSubAccountAdminPermission(Integer mainAccId, Integer loginId, boolean isSubAccount){
        if(mainAccId == null){
            return false;
        }
        if(isSubAccount){
            PPSLMTASubAccount subAccount = subAccRepo.findById(loginId);
            if(subAccount == null){
                return false;
            }
            if(subAccount.getMainUser().getId().intValue() != mainAccId.intValue()){
                return false;
            }
            List<PPSLMTARightsMapping> mappings = subAccount.getRightsMapping();
            if(mappings != null && mappings.size() > 0){
                for(PPSLMTARightsMapping m : mappings){
                    if(m != null){
                        PPSLMTAAccessRightsGroup acg = m.getAccessRightsGroup();
                        if("G5".equalsIgnoreCase(acg.getName())){
                            return true;
                        }
                    }
                }
            }
        }else{
            PPSLMTAMainAccount mainAccount = mainAccRepo.findById(loginId);
            if(mainAccount == null){
                return false;
            }
            if(mainAccount.getId().intValue() != mainAccId.intValue()){
                return false;
            }
            List<PPSLMTAMainRightsMapping> mappings = mainAccount.getRightsMapping();
            if(mappings != null && mappings.size() > 0){
                for(PPSLMTAMainRightsMapping m : mappings){
                    if(m != null){
                        PPSLMTAAccessRightsGroup acg = m.getAccessRightsGroup();
                        if(acg != null){
                            if("G5".equalsIgnoreCase(acg.getName())){
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
