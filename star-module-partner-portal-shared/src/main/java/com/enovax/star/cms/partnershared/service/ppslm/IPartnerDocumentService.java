package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerDocumentVM;
import info.magnolia.cms.beans.runtime.Document;

import java.util.List;

public interface IPartnerDocumentService {

    PPSLMPADocument savePartnerDocuments(PPSLMTAMainAccount taMainAcc, PPSLMPartner pa, Document doc, String fileType) throws Exception;

    PPSLMPADocument getPartnerDocumentById(Integer id);

    List<PartnerDocumentVM> getPartnerDocumentsByPartnerId(Integer paId);

    boolean isValidPartnerDocument(Document doc);

}
