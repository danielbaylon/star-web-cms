package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppslm.PPSLMInventoryTransactionItemSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import com.enovax.star.cms.partnershared.constant.AuditAction;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jennylynsze on 5/10/16.
 */
@Service
public class DefaultPartnerInventoryService implements IPartnerInventoryService {

//    @Autowired
//    IInventoryTransactionsItemRepository itemRepo;
//    @Autowired
//    IInventoryTransactionRepository transRepo;
    public static final String RECEIPT_MSG_SUCCESS = "Regenerate Receipt Successfully, please verify your email.";
    public static final String RECEIPT_MSG_FAILED = "Failed to Generate Receipt , please contact system admin!";
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PPSLMInventoryTransactionRepository transRepo;
    @Autowired
    private PPSLMInventoryTransactionItemRepository itemRepo;
    @Autowired
    private PPSLMPartnerRepository partnerRepo;
    @Autowired
    private ISystemParamService paramSrv;
    @Autowired
    private ITemplateService templateSrv;
    @Autowired
    private IPartnerEmailService partnerEmailSrv;
    @Autowired
    private IAuditTrailService auditService;
    @Autowired
    private PPSLMRevalidationTransactionRepository revalidationTransRepo;
    @Autowired
    private PPSLMMixMatchPackageRepository mixmatchPkgRepo;
    @Autowired
    private IPartnerTncService partnerTncService;
    @Autowired
    private PPSLMOfflinePaymentRequestReqpository offlinePayReqRepo;

    //TODO check with InventoryServiceImpl
    @Override
    @Transactional(readOnly = true)
    public List<InventoryTransactionItemVM> getInventoryTransactionsItem(Integer adminId, String fromDateStr, String toDateStr, String availableOnly, String prodNm, String orderBy, String orderWith, Integer page, Integer pagesize) {
        int tktepweeks = paramSrv.getTicketExpiringAlertPeriod();

        Date fromDate = null;
        Date toDate = null;
        boolean showAvailable = false;
        try {
            if (fromDateStr != null&&!"".equals(fromDateStr)) {
                fromDate = NvxDateUtils.parseDate(fromDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if (toDateStr != null&&!"".equals(toDateStr)) {
                toDate = NvxDateUtils.parseDate(toDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if ("on".equals(availableOnly)) {
            showAvailable = true;
        }

        if (orderBy != null && orderWith != null && !"".equals(orderBy)
                && !"".equals(orderWith)) {
            if ("receiptNumber".equalsIgnoreCase(orderBy)) {
                orderBy = "inventoryTrans.receiptNum";
            } else if ("status".equalsIgnoreCase(orderBy)) {
                orderBy = "inventoryTrans.status";
            } else if ("validityStartDateStr".equalsIgnoreCase(orderBy)) {
                orderBy = "inventoryTrans.validityStartDate";
            } else if ("validityEndDateStr".equalsIgnoreCase(orderBy)) {
                orderBy = "inventoryTrans.validityEndDate";
            } else if ("username".equalsIgnoreCase(orderBy)) {
                orderBy = "inventoryTrans.username";
            } else if ("type".equalsIgnoreCase(orderBy)) {
                orderBy = "ticketType";
            } else if ("name".equalsIgnoreCase(orderBy)) {
                orderBy = "displayName";
            } else if ("subtotal".equalsIgnoreCase(orderBy)) {
                orderBy = "subTotal";
            }
        }else {
            orderWith = "DESC";
            orderBy = "id";
        }


        if(prodNm != null && StringUtils.isNotEmpty(prodNm)) {
            prodNm = "%" + prodNm + "%";
        }else {
            prodNm = null;
        }

        List<String> strList= new ArrayList<>();
        strList.add(TicketStatus.Available.toString());
        strList.add(TicketStatus.Expiring.toString());
        strList.add(TicketStatus.Revalidated.toString());
        String strStatus = "('"+ TicketStatus.Available.toString() + "','" + TicketStatus.Expiring.toString() + "','" + TicketStatus.Available.toString()+"')";

        List<PPSLMInventoryTransactionItem> itemList = new ArrayList<>();
        if(page == null) {
            itemList = itemRepo
                    .getInventoryTransactionsItem(adminId, fromDate, toDate, showAvailable, prodNm);
        }else {
            PageRequest pageRequest = new PageRequest( page - 1, pagesize,
                    "ASC".equalsIgnoreCase(orderWith) ? Sort.Direction.ASC : Sort.Direction.DESC,
                    orderBy);

            Page<PPSLMInventoryTransactionItem> itemPaged = itemRepo
                    .getInventoryTransactionsItem(adminId, fromDate, toDate, showAvailable, prodNm, pageRequest);
            Iterator<PPSLMInventoryTransactionItem> itemIterator = itemPaged.iterator();
            itemList = Lists.newArrayList(itemIterator);
        }

        for(PPSLMInventoryTransactionItem item: itemList) {
            List<PPSLMInventoryTransactionItem> topupItems = itemRepo
                    .getTaggedTopItem(item.getTransactionId(), item.getProductId(), item.getUnpackagedQty(), item.getProductId(), item.getItemListingId(), null, null, null);
            item.setTopupItems(topupItems);
        }

        return InventoryTransactionItemVM.convertInventoryItemVM(itemList, tktepweeks);
    }

    @Override
    @Transactional(readOnly = true)
    public long getInventoryTransactionsItemSize(Integer adminId, String fromDateStr, String toDateStr, String availableOnly, String prodNm) {
        /*Specifications querySpec = groupInventorySearchCriteria(adminId, fromDateStr, toDateStr, availableOnly, prodNm);
        if(querySpec != null) {
            return itemRepo.count(querySpec);
        }
        return 0;*/
        Date fromDate = null;
        Date toDate = null;
        boolean showAvailable = false;
        try {
            if (fromDateStr != null&&!"".equals(fromDateStr)) {
                fromDate = NvxDateUtils.parseDate(fromDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if (toDateStr != null&&!"".equals(toDateStr)) {
                toDate = NvxDateUtils.parseDate(toDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if ("on".equals(availableOnly)) {
            showAvailable = true;
        }

        if(prodNm != null && StringUtils.isNotEmpty(prodNm)) {
            prodNm = "%" + prodNm + "%";
        }else {
            prodNm = null;
        }

        return itemRepo.countInventoryTransactionsItem(adminId, fromDate, toDate, showAvailable, prodNm);
    }


    private Specifications groupInventorySearchCriteria(
            Integer adminId, String fromDateStr, String toDateStr, String availableOnly, String prodNm) {

        Date fromDate = null;
        Date toDate = null;
        boolean showAvailable = false;

        try {
            if (fromDateStr != null&&!"".equals(fromDateStr)) {
                fromDate = NvxDateUtils.parseDate(fromDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if (toDateStr != null&&!"".equals(toDateStr)) {
                toDate = NvxDateUtils.parseDate(toDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("on".equals(availableOnly)) {
            showAvailable = true;
        }


        List<QCriteria> criterias = new ArrayList<QCriteria>();

        if (toDate != null) {
            criterias.add(new QCriteria("validityStartDate", QCriteria.Operation.LE, toDate));
        }

        if(fromDate != null) {
            criterias.add(new QCriteria("validityEndDate", QCriteria.Operation.GE, fromDate));
        }

        if(StringUtils.isNotBlank(prodNm)) {
            criterias.add(new QCriteria("displayName", QCriteria.Operation.LIKE, prodNm));
        }
        List<String> statusList= new ArrayList<>();

        if(showAvailable) {
            statusList.add(TicketStatus.Available.toString());
            statusList.add(TicketStatus.Expiring.toString());
            statusList.add(TicketStatus.Revalidated.toString());
            criterias.add(new QCriteria("status", QCriteria.Operation.IN, statusList, QCriteria.ValueType.String));
            criterias.add(new QCriteria("unpackagedQty", QCriteria.Operation.GT, 0));
        }else{
            statusList.add(TicketStatus.Available.toString());
            statusList.add(TicketStatus.Expiring.toString());
            statusList.add(TicketStatus.Revalidated.toString());
            statusList.add(TicketStatus.Expired.toString());
            statusList.add(TicketStatus.Refunded.toString());
            statusList.add(TicketStatus.Forfeited.toString());
            criterias.add(new QCriteria("status", QCriteria.Operation.IN, statusList, QCriteria.ValueType.String));
        }

        //criterias.add(new QCriteria("transactionId", QCriteria.Operation.EQ, adminId.toString()));

        //TODO this is to get only those main product one,s not the topup
        criterias.add(new QCriteria("transItemType", QCriteria.Operation.EQ, TransItemType.Standard.toString()));

        /*Specification<PPSLMInventoryTransactionItem> spec = new Specification<PPSLMInventoryTransactionItem>() {
            @Override
            public Predicate toPredicate(Root<PPSLMInventoryTransactionItem> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                final Subquery<PPSLMInventoryTransactionItem> itemQuery = criteriaQuery.subquery(PPSLMInventoryTransactionItem.class);
                final Root<PPSLMInventoryTransactionItem> item = itemQuery.from(PPSLMInventoryTransactionItem.class);
                final Join<PPSLMInventoryTransactionItem, PPSLMInventoryTransaction> trans = item.join("inventoryTrans");
                itemQuery.select(item);
                itemQuery.where(criteriaBuilder.equal(trans.<Integer> get("mainAccountId"), adminId));

                return criteriaBuilder.in(root.get("id")).value(itemQuery);
            }
        };*/
        SpecificationBuilder specBuilder = new SpecificationBuilder();
        Specifications querySpec = null;

        try {
            querySpec = specBuilder.build(criterias, PPSLMInventoryTransactionItemSpecification.class);
            //querySpec.and(spec);
            return querySpec;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    @Override
    @Transactional(readOnly = true)
    public InventoryTransactionVM getTransByUser(Integer transId, PartnerAccount account) {
        InventoryTransactionVM trans = this.getTransById(transId, InventoryTransactionVM.PURCHASE);
        //TODO check if this transaction is really for the partner
        trans.setCanRevalidateByAcc(true); //TODO get the rigths correctly
//        Set<String> accessSet = null;
//        if (accName.endsWith(SysConst.TA_MAIN_ACC_POSTFIX)) {
//            TAMainAccount omainUser = this.taas
//                    .getMainUser(accName, true, true);
//            accessSet = omainUser.getAccessRights();
//        } else {
//            TASubAccount osubUser = this.taas.getSubUser(accName, true, true);
//            accessSet = osubUser.getAccessRights();
//        }
//        if (accessSet.contains(NavCodes.revalidateTickets.code)) {
//            trans.setCanRevalidateByAcc(true);
//        } else {
//            trans.setCanRevalidateByAcc(false);
//        }
        return trans;
    }


    @Override
    @Transactional(readOnly = true)
    public InventoryTransactionVM getTransById(Integer transId, String transType) {
        PPSLMInventoryTransaction currentTransaction;
        PPSLMRevalidationTransaction revalTrans = null;

        if(InventoryTransactionVM.PURCHASE.equalsIgnoreCase(transType)) {
            currentTransaction = transRepo.findOne(transId);
            revalTrans = currentTransaction.getRevalidateTrans();
        }else {
            revalTrans = revalidationTransRepo.findById(transId);
            currentTransaction = transRepo.findOne(revalTrans.getTransactionId());
        }

        PartnerAccount account = new PartnerAccount();
        account.setUsername(currentTransaction.getUsername());
        account.setPartner(new PartnerVM(partnerRepo.findByAccountCode(currentTransaction.getMainAccount().getAccountCode())));

        int tktepweeks = paramSrv.getTicketExpiringAlertPeriod();
        int tktepmonth = paramSrv.getTicketRevalidationPeriod();
        int allowRevalPeriod = paramSrv.getTicketAllowRevalidatePeriod();
        List<PPSLMMixMatchPackage> mmpkgs = mixmatchPkgRepo.getPackage(currentTransaction.getReceiptNum());

        currentTransaction.setRevalidatePeriod(tktepmonth);
        currentTransaction.setExpiringAlertPeriod(tktepweeks);
        currentTransaction.setMmpkgs(mmpkgs);
        currentTransaction.setAllowRevalPeriod(allowRevalPeriod);

        return getTransDetails(currentTransaction, revalTrans, account, transType);
    }

    @Override
    @Transactional(readOnly = true)
    public InventoryTransactionVM getTransDetails(PPSLMInventoryTransaction transaction, PPSLMRevalidationTransaction revalTrans, PartnerAccount account, String transType) {

        InventoryTransactionVM currentTransactionVm = InventoryTransactionVM.fromDataModel(transaction);
        List<PPSLMInventoryTransactionItem> items;
        if(InventoryTransactionVM.PURCHASE.equalsIgnoreCase(transType)) {
            items = transaction.getInventoryItems();
        }else{
            items = revalTrans.getRevalItems();
        }
        List<InventoryTransactionItemVM> itemVms = new ArrayList<>();

//        Collections.sort(items);

        //add the product list for easier display
        List<FunCartDisplayCmsProduct> productList = new ArrayList<>();
        List<String> productIdList = new ArrayList<>();

        Map<String, FunCartDisplayCmsProduct> productMap = new HashMap<>();

        FunCartDisplayCmsProduct product;
        for(PPSLMInventoryTransactionItem item: items) {
            if(productMap.containsKey(item.getProductId())) {
                product = productMap.get(item.getProductId());
                product.setSubtotal(product.getSubtotal().add(item.getSubTotal()));
            }else {
                product = new FunCartDisplayCmsProduct();
                product.setName(item.getProductName());
                product.setId(item.getProductId());
                product.setSubtotal(item.getSubTotal());
                productMap.put(product.getId(), product);
                productIdList.add(product.getId());
            }
            InventoryTransactionItemVM itemVm = new InventoryTransactionItemVM(item);
            if("Topup".equalsIgnoreCase(item.getTransItemType())) {
                itemVm.setTopup(true);
            }
                itemVms.add(itemVm);
        }

        //to have a sorted one
        for(String prodId: productIdList) {
            productList.add(productMap.get(prodId));
        }

        if(revalTrans != null) {
            currentTransactionVm.setRevalTransaction(new RevalTransactionVM(revalTrans));
        }

        currentTransactionVm.setProductList(productList);
        currentTransactionVm.setItems(itemVms);
//        currentTransactionVm.setUsername(account.getUsername());
        currentTransactionVm.setPartner(account.getPartner());
//        currentTransactionVm.setCreatedDateStr(NvxDateUtils.formatDate(transaction.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
//        currentTransactionVm.setValidityStartDateStr(NvxDateUtils.formatDate(transaction.getTmStatusDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
//        currentTransactionVm.setValidityEndDateStr((NvxDateUtils.formatDate(transaction.getValidityEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY))); //TODO should be from AX
//        currentTransactionVm.setExpiringStatus(transaction.getDisplayStatus());

        currentTransactionVm.setAllowRevalidation(transaction.revalidateAvailable());
        return currentTransactionVm;
    }


    //TODO need to revaluate the receiptVM, its different from the B2B one... need to double check how.
    @Override
    @Transactional
    public String generateReceiptAndEmail(Integer transId) {
        PPSLMInventoryTransaction trans = transRepo.findOne(transId);
        if(PartnerPortalConst.OfflinePayment.equals(trans.getPaymentType())) {
            PPSLMOfflinePaymentRequest req = offlinePayReqRepo.findFirstByTransactionId(transId);
            return generateReceiptAndEmail(trans, req);
        }
        return generateReceiptAndEmail(trans, null);
    }

    @Override
    @Transactional
    public String generateReceiptAndEmail(Integer transId, PPSLMOfflinePaymentRequest req) {
        PPSLMInventoryTransaction trans = transRepo.findOne(transId);
        return generateReceiptAndEmail(trans, req);

    }

    @Override
    @Transactional
    public String generateReceiptAndEmail(PPSLMInventoryTransaction trans, PPSLMOfflinePaymentRequest req) {
        log.info("Step 1: Loading transaction info from database.");

        Hibernate.initialize(trans.getInventoryItems());
        // Hibernate.initialize(trans.get);
        Set<String> prodIds = new LinkedHashSet<>();
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            prodIds.add(item.getProductId());
        }

        final List<TncVM> tncs = this.partnerTncService.getProductTncs(StoreApiChannels.PARTNER_PORTAL_SLM,new ArrayList<>(
                prodIds));

        String receiptName = trans.getReceiptNum()
                + (trans.getReprintCount() == 0 ? "" : "_"
                + trans.getReprintCount());
        String genpdfpath = paramSrv.getPartnerTransactionReceiptRootDir() + "/" + receiptName
                + FileUtil.FILE_EXT_PDF;
//        String genpdfpath = FileUtil.getSystemPDFPath() + receiptName
//                + FileUtil.FILE_EXT_PDF;
        Map<String,String> map = paramSrv.getSentosaContactDetails();

        ReceiptVM receipt = null;

        if(req != null) {
            receipt = new ReceiptVM(trans, req, paramSrv.getApplicationContextPath(), map.get(PartnerPortalConst.ADDRESS_OWN), map.get(PartnerPortalConst.CONTACT_OWN));
        }else {
            receipt = new ReceiptVM(trans,paramSrv.getApplicationContextPath(), map.get(PartnerPortalConst.ADDRESS_OWN), map.get(PartnerPortalConst.CONTACT_OWN));
        }

        receipt.setHasTnc(!tncs.isEmpty());
        receipt.setTncs(tncs);
        log.info("Step 2: Generate PDF to :"+genpdfpath);
        try {
            templateSrv.generatePartnerTransactionReceiptPdf(receipt, genpdfpath);
        } catch (Exception e) {
            e.printStackTrace();
            return RECEIPT_MSG_FAILED;
        }
        log.info("Step 3: Generate And Send Email");
        Boolean isSuccess = sendReceiptGenerateEmail(trans, receipt,
                receiptName, genpdfpath);
        if (isSuccess) {
            trans.setReprintCount(trans.getReprintCount() + 1);
            transRepo.save(trans);
            return RECEIPT_MSG_SUCCESS;
        } else {
            return RECEIPT_MSG_FAILED;
        }
    }

    private Boolean sendReceiptGenerateEmail(PPSLMInventoryTransaction trans,
                                             ReceiptVM receipt, String receiptName, String genpdfpath) {
        try {
            partnerEmailSrv.sendTransactionReceiptEmail(trans, receipt, receiptName, genpdfpath);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public ApiResult<String> refundTrans(Integer transId, String userName) {
        PPSLMInventoryTransaction trans = transRepo.findOne(transId);
        List<PPSLMInventoryTransactionItem> items = trans.getInventoryItems();
        if(trans.isTicketGenerated()){
            return new ApiResult<>(ApiErrorCodes.RefundErrorWithPin);
        }
        for(PPSLMInventoryTransactionItem item:items){
            if(!item.getQty().equals(item.getUnpackagedQty())){
                return new ApiResult<>(ApiErrorCodes.RefundErrorWithPin);
            }
        }
        trans.setStatus(TicketStatus.Refunded.toString());
        transRepo.save(trans);
        PPSLMPartner pa = trans.getMainAccount().getProfile();
        //this.partnerSevice.saveGeneralLog(pa.getId(), userNm, REFUND_TRANSACTION_LABEL, prePaXml);
        this.auditService.log(true, AuditAction.RefundTkt,
                userName + " refund the transaction "+trans.getReceiptNum()+" for "+pa.getOrgName(), "", "", userName);
        return new ApiResult(true,"","",null);
    }

    @Override
    public void emailAlertForExpiringTicket() {

    }

    @Transactional
    @Override
    public void updateExpiredStatus() {
        log.info("updateExpiredStatus .....start");
        List<String> status = new ArrayList<>();
        status.add(TicketStatus.Available.toString());
        status.add(TicketStatus.Revalidated.toString());

        List<PPSLMInventoryTransaction> trans = transRepo.findByStatusInAndValidityEndDateLessThan(status, NvxDateUtils.clearTime(new Date()));
        updateStatus(trans, TicketStatus.Expired.toString());

        log.info("updateExpiredStatus .....end");
    }

    @Transactional
    @Override
    public void updateForfeitedStatus() {
        Integer tktepmonth =  paramSrv.getTicketRevalidationPeriod();
        Date forfeitDt = TransactionUtil.addToDate(new Date(),
                Calendar.MONTH,
                -tktepmonth);
        List<PPSLMInventoryTransaction> noRevalTrans = transRepo.retrieveForfeitedForNoRevalidatedTrans(forfeitDt);
        updateStatus(noRevalTrans, TicketStatus.Forfeited.toString());
        List<PPSLMInventoryTransaction> expiredTrans = transRepo.findByStatusAndRevalidated(TicketStatus.Expired.toString(), true);
        updateStatus(expiredTrans, TicketStatus.Forfeited.toString());

    }

    @Transactional
    private void updateStatus(List<PPSLMInventoryTransaction> trans, String status) {
        if(trans != null) {
            for(PPSLMInventoryTransaction t: trans) {
                log.info(t.getReceiptNum() +" is Expired.");
                t.setStatus(status);
                transRepo.save(t);
            }
        }
    }

    //TODO

    @Override
    public BigDecimal getUsedTransAmountToday(Integer mainAccountId) {
        Date today = TransactionUtil.getDateWithoutTime();
        Date eod = TransactionUtil.addDaysToDate(today, 1);
//        return inventoryTransactionDao.getUsedTransAmountByDate(mainAccountId,
//                today, eod);
        return null;
    }


    //TODO
    @Override
    public List<PPSLMInventoryTransaction> getTransForVoidReconciliation(int minsPast) {
//        final List<InventoryTransaction> transList = this.inventoryTransactionDao
//                .getTransForVoidReconciliation(minsPast);
//        return transList;
        return null;
    }
}
