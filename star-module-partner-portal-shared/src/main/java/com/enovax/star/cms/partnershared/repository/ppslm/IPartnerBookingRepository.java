package com.enovax.star.cms.partnershared.repository.ppslm;


import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;

/**
 * Created by jennylynsze on 5/16/16.
 */
public interface IPartnerBookingRepository {
    void saveStoreTransaction(InventoryTransactionVM txn);

    void updateTransaction(InventoryTransactionVM txn);

    InventoryTransactionVM getStoreTransactionByReceipt(String receiptNumber);

    Integer getNexInventoryTransactiontId();

    Integer getNextInventoryTransactionItemId();
}
