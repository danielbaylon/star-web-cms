package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartEmailDisplay;
import com.enovax.star.cms.commons.model.partner.ppmflg.ChatHistVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentRequestVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentResultVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PagedData;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 30/9/16.
 */
public interface IOfflinePaymentService {

    PartnerFunCartEmailDisplay constcutOfflinePaymentCart(PPMFLGInventoryTransaction txn, boolean includePartner);

    PartnerFunCartEmailDisplay constcutOfflinePaymentCart(PPMFLGInventoryTransaction txn);

    ApiResult<PagedData<List<OfflinePaymentRequestVM>>> getPagedOfflinePaymentRequest(String isPartnerOnly, String paId, String adminId, Date startDate, Date endDate, String status, String username, String orgName, String receiptNum, String referenceNum, String accountCode, String sortField, String sortDirection, int page, int pageSize) throws Exception;

    ApiResult<OfflinePaymentResultVM> viewOfflinePaymentDetails(String adminId, Integer id, boolean isPreview);

    ApiResult<String> reviewOfflinePaymentDetails(String loginAdminId, Integer id, String action, String approverRemarks) throws BizValidationException;

    public ApiResult<String> confirmOfflineTransaction(Integer transactionId, String axAccountNumber, String mainAccountEmail) throws BizValidationException;

    public String makeChatHists(String oriRemarks,String remarks,String username);

    public ChatHistVM getLatestChatHistVM(StringBuilder lastSubmitCountAfterApprovalChat, String remarks, Date lastApprovalRemarksDate, boolean calcuateLastSubmitCountAfterApprovalChat);

}
