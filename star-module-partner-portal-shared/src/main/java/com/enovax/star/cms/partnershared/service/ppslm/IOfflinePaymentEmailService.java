package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.model.partner.ppslm.OfflinePaymentNotifyVM;
import com.enovax.star.cms.commons.model.partner.ppslm.OfflinePaymentResultVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccount;

/**
 * Created by houtao on 7/9/16.
 */
public interface IOfflinePaymentEmailService {

    void sendingOfflinePaymentRequestSubmissionEmail(OfflinePaymentNotifyVM vm, PPSLMOfflinePaymentRequest request, PPSLMInventoryTransaction txn);

    void sendingOfflinePaymentRequestApprovedEmail(OfflinePaymentNotifyVM vm, PPSLMOfflinePaymentRequest request, PPSLMInventoryTransaction txn);

    void sendingOfflinePaymentRequestRejectedEmail(OfflinePaymentNotifyVM vm, PPSLMOfflinePaymentRequest request, PPSLMInventoryTransaction txn);

    void sendingOfflinePaymentCustomerNotification(OfflinePaymentNotifyVM vm, PPSLMOfflinePaymentRequest request, PPSLMInventoryTransaction txn);

    void sendingOfflinePaymentPartnerToAdminNotification(OfflinePaymentNotifyVM vm, PPSLMOfflinePaymentRequest request, PPSLMInventoryTransaction txn);
}
