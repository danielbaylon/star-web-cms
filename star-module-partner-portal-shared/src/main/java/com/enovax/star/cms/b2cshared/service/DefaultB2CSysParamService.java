package com.enovax.star.cms.b2cshared.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jensen on 15/11/16.
 */
@Service
public class DefaultB2CSysParamService implements IB2CSysParamService {

    public static final String SYS_PARAM_B2C_TRANSACTION_RECEIPT_DIR = "sys.param.b2c.transaction.receipt.dir";
    public static final String SYS_PARAM_B2C_APP_CONTEXT_ROOT = "sys.param.app.context.root";

    @Autowired
    private ISystemParamRepository paramRepo;

    @Override
    @Transactional(readOnly = true)
    public String getSystemParamValueByKey(StoreApiChannels channel, String key) {
        return paramRepo.getSystemParamValueByKey(channel.code, key);
    }

    @Override
    @Transactional(readOnly = true)
    public String getSystemParamValueByKey(String channelCode, String key) {
        return paramRepo.getSystemParamValueByKey(channelCode, key);
    }
}
