package com.enovax.star.cms.partnershared.repository.ppslm;


import com.enovax.star.cms.commons.datamodel.ppslm.tier.TierProductMapping;

import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
public interface ITierProductMappingRepository {
    List<TierProductMapping> getProductList(String channel, String tierId);
    List<TierProductMapping> getTierList(String channel, String productId);
    Integer getNextId();
}
