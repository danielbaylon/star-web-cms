package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerProductVM;
import com.enovax.star.cms.partnershared.model.grid.ProdFilter;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
@Repository("PPMFLGIProductRepository")
public class JcrProductRepository implements IProductRepository {

    @Autowired
    @Qualifier("PPMFLGITierProductMappingRepository")
    ITierProductMappingRepository tierProductMappingRepo;

    @Override
    public int getProdSize(ProductGridFilterVM gridFilterVM, boolean isExclusive) {
        List<PartnerProductVM> productVms = new ArrayList<>();

        ProdFilter filter = gridFilterVM.getProdFilter();

        List<String> prodIdListString = new ArrayList<>();
        if(filter.getProdIds() != null && filter.getProdIds().length > 0) {
            for(String pi: filter.getProdIds()) {
                prodIdListString.add(pi);
            }
        }

        try {
            Iterable<Node> productNodes = JcrRepository.query(JcrWorkspace.CMSProducts.getWorkspaceName(), JcrWorkspace.CMSProducts.getNodeType(), getQuery(filter,isExclusive));
            Iterator<Node> productNodesIterator = productNodes.iterator();

            while(productNodesIterator.hasNext()) {
                Node productNode = productNodesIterator.next();

                //only add those that are not in the list
                if(!prodIdListString.contains(productNode.getName())) {
                    PartnerProductVM productVm = new PartnerProductVM(productNode);
                    if(!isExclusive) {
                        productVm.setTiers(productVm.groupTierStr(tierProductMappingRepo.getTierList(filter.getChannel(), productVm.getId())));
                    }
                    productVms.add(productVm);
                }
            }

            return productVms.size();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public List<PartnerProductVM> getProdByPage(ProductGridFilterVM gridFilterVM, boolean isExclusive) {
        List<PartnerProductVM> productVms = new ArrayList<>();

        ProdFilter filter = gridFilterVM.getProdFilter();

        List<String> prodIdListString = new ArrayList<>();
        if(filter.getProdIds() != null && filter.getProdIds().length > 0) {
            for(String pi: filter.getProdIds()) {
                prodIdListString.add(pi);
            }
        }
        try {
            Iterable<Node> productNodes = JcrRepository.query(JcrWorkspace.CMSProducts.getWorkspaceName(), JcrWorkspace.CMSProducts.getNodeType(), getQuery(filter,isExclusive));
            Iterator<Node> productNodesIterator = productNodes.iterator();

            while(productNodesIterator.hasNext()) {
                Node productNode = productNodesIterator.next();

                //only add those that are not in the list
                if(!prodIdListString.contains(productNode.getName())) {
                    PartnerProductVM productVm = new PartnerProductVM(productNode);
                    if(!isExclusive) {
                        productVm.setTiers(productVm.groupTierStr(tierProductMappingRepo.getTierList(filter.getChannel(), productVm.getId())));
                    }
                    productVms.add(productVm);
                }
            }


            List<PartnerProductVM> pagedProducts = new ArrayList<>();
            //filter pag page also
            if(gridFilterVM.getPage() > 0 && gridFilterVM.getPageSize() > 0) {
                for(int i = (gridFilterVM.getPage() - 1) * gridFilterVM.getPageSize(); pagedProducts.size() < gridFilterVM.getPageSize() && i < productVms.size(); i++) {
                    pagedProducts.add(productVms.get(i));
                }
            }else {
                return productVms;
            }

            return pagedProducts;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public PartnerProductVM getProd(String channel, String prodId) {
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + prodId);
            PartnerProductVM productVm = new PartnerProductVM(productNode);
            return productVm;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getQuery(ProdFilter filter, boolean isExclusive) {
        String baseQuery = "/jcr:root/cms-products/" + filter.getChannel() + "//element(*, mgnl:cms-product)";
        String criteria = "";

        if(StringUtils.isNotBlank(filter.getProdName())) {
            if(StringUtils.isBlank(criteria)) {
                criteria = "[";
            }
            criteria = criteria + "jcr:like(fn:lower-case(@name), '%" + filter.getProdName().toLowerCase() + "%')";
        }

        if(filter.getStatus() != null) {
            if(StringUtils.isBlank(criteria)) {
                criteria = "[";
            }else {
                criteria = criteria + " and ";
            }

            criteria = criteria + "@status = 'Active'";
        }

        //TODO product level
        if(isExclusive) {
            if(StringUtils.isBlank(criteria)) {
                criteria = "[";
            }else {
                criteria = criteria + " and ";
            }

            criteria = criteria + "@productLevel='Exclusive'";
        }else{
            if(StringUtils.isBlank(criteria)) {
                criteria = "[";
            }else {
                criteria = criteria + " and ";
            }
            criteria = criteria + "@productLevel='Tiered'";
        }

        if(StringUtils.isNotBlank(criteria)) {
            criteria = criteria + "]";
        }

        return baseQuery + criteria;
    }



    /*@Override
    public List<PartnerProductVM> getExclProdByPage(ProductGridFilterVM gridFilterVM) {
        List<PartnerProductVM> productVms = new ArrayList<>();

        ProdFilter filter = gridFilterVM.getProdFilter();

        List<String> prodIdListString = new ArrayList<>();
        if(filter.getProdIds() != null && filter.getProdIds().length > 0) {
            for(String pi: filter.getProdIds()) {
                prodIdListString.add(pi);
            }
        }
        try {
            Iterable<Node> productNodes = JcrRepository.query(JcrWorkspace.CMSProducts.getWorkspaceName(), JcrWorkspace.CMSProducts.getNodeType(), getQuery(filter,true));
            Iterator<Node> productNodesIterator = productNodes.iterator();

            while(productNodesIterator.hasNext()) {
                Node productNode = productNodesIterator.next();

                //only add those that are not in the list
                if(!prodIdListString.contains(productNode.getName())) {
                    PartnerProductVM productVm = new PartnerProductVM(productNode);
                    //productVm.setTiers(productVm.groupTierStr(tierProductMappingRepo.getTierList(filter.getChannel(), productVm.getId())));
                    productVms.add(productVm);
                }
            }


            List<PartnerProductVM> pagedProducts = new ArrayList<>();
            //filter pag page also
            if(gridFilterVM.getPage() > 0 && gridFilterVM.getPageSize() > 0) {
                for(int i = (gridFilterVM.getPage() - 1) * gridFilterVM.getPageSize(); pagedProducts.size() < gridFilterVM.getPageSize() && i < productVms.size(); i++) {
                    pagedProducts.add(productVms.get(i));
                }
            }else {
                return productVms;
            }

            return pagedProducts;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }*/
}
