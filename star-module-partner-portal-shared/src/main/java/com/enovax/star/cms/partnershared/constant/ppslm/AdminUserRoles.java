package com.enovax.star.cms.partnershared.constant.ppslm;

/**
 * Created by houtao on 26/7/16.
 */
public enum AdminUserRoles {

    PartnerVerifyRole("pp-slm-user-accounts"),
    OfflinePaymentViewRole("pp-slm-view-offline-payment"),
    OfflinePaymentApproverRole("pp-slm-approve-offline-payment"),
    ViewInventoryRole("pp-slm-view-inventory"),
    ViewPackageRole("pp-slm-view-package"),
    DeactivatePincodeRole("pp-slm-deactivate-pincode"),
    RefundTransactionRole("pp-slm-refund-transaction"),
    PartnerPortalSlmAdmin("pp-slm-admin"),
    WOTBackendReservation("pp-slm-wot-backend-reservation"),
    GeneralApproverRole("dynamic-pp-slm-general-approver"),
    GeneralApproverBackupRole("dynamic-pp-slm-general-backup-approver"),
    TransactionQuery("pp-slm-transaction-query"),
    PincodeQuery("pp-slm-pincode-query"),
    PartnerProfileReport("pp-slm-partner-profile-report"),
    MarketShareReport("pp-slm-market-share-report"),
    TransactionReport("pp-slm-transaction-report"),
    InventoryReport("pp-slm-inventory-report"),
    PackageReport("pp-slm-package-report"),
    ExceptionReport("pp-slm-exception-report")
    ;

    public final String code;

    private AdminUserRoles(String code) {
        this.code = code;
    }
}
