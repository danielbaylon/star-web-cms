package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMInventoryTransactionRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMOfflinePaymentRequestReqpository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 9/24/16.
 */
@Service
public class DefaultPartnerPurchaseTransactionService implements IPartnerPurchaseTransactionService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private PPSLMInventoryTransactionRepository transRepo;

    @Autowired
    private IPartnerInventoryService invService;

    @Autowired
    private PPSLMOfflinePaymentRequestReqpository offlinePayReqRepo;

    @Override
    public void processTransactionReleaseReservation(PPSLMInventoryTransaction trans) {
        processTransactionReleaseReservation(trans, false);
    }

    @Override
    @Transactional
    public void processTransactionReleaseReservation(PPSLMInventoryTransaction trans, boolean updateTransIncomplete) {
        //TODO revert the reservation capacity, get from the DB? Maybe I should store the things I needed for telemoney later on :(
        if (updateTransIncomplete) {
            trans.setStatus(TicketStatus.Incomplete.toString());
            trans.setModifiedDate(new Date());
            transRepo.save(trans);
        }
    }

    @Override
    @Transactional
    public void processTransactionReleaseReservation(int transId) {
        processTransactionReleaseReservation(transRepo.findOne(transId), false);
    }

    @Override
    @Transactional
    public void processTransactionReleaseReservation(int transId, boolean updateTransIncomplete) {
        PPSLMInventoryTransaction tran = transRepo.findOne(transId);
        if(tran.getStatus().equals(TicketStatus.Pending_Submit.toString())&&updateTransIncomplete){
            tran.setStatus(TicketStatus.Incomplete.toString());
            tran.setModifiedDate(new Date());
            transRepo.save(tran);
        }else{
            processTransactionReleaseReservation(tran, updateTransIncomplete);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<PPSLMInventoryTransaction> getTransactionsForStatusUpdate(int timeoutLowerBoundMins, int timeoutUpperBoundMins) {
        Calendar upperBound = Calendar.getInstance();
        upperBound.add(Calendar.MINUTE, -1 * timeoutUpperBoundMins);

        Calendar lowerBound = Calendar.getInstance();
        lowerBound.add(Calendar.MINUTE, -1 * timeoutLowerBoundMins);

        List<PPSLMInventoryTransaction> trans = transRepo.getTransactionsForStatusUpdate(upperBound.getTime(), lowerBound.getTime());

        return trans;
    }

    @Override
    @Transactional
    public PPSLMInventoryTransaction confirmTransaction(Integer transId) {
        final PPSLMInventoryTransaction trans = transRepo.findOne(transId);

        trans.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.toString());
        trans.setModifiedDate(new Date());

        transRepo.save(trans);

        return trans;

    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMInventoryTransaction getTransaction(int id, boolean includeItems) {
        PPSLMInventoryTransaction trans = transRepo.findOne(id);
        if (includeItems && trans != null) {
            trans.getInventoryItems().size();
        }
        return trans;
    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMInventoryTransaction getTransaction(String receiptNum, boolean includeItems) {
        PPSLMInventoryTransaction trans =transRepo.findByReceiptNum(receiptNum);
        if (includeItems && trans != null) {
            trans.getInventoryItems().size();
        }
        return trans;
    }

    @Override
    @Transactional
    public boolean saveTransaction(PPSLMInventoryTransaction trans) {
        transRepo.save(trans);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean generateAndEmailPurchaseReceipt(PPSLMInventoryTransaction transParam) {
        String result;

        if(PartnerPortalConst.OfflinePayment.equals(transParam.getPaymentType())) {
            PPSLMOfflinePaymentRequest req = offlinePayReqRepo.findFirstByTransactionId(transParam.getId());
            result = this.invService.generateReceiptAndEmail(transParam, req);
        }else {
            result = this.invService.generateReceiptAndEmail(transParam, null);
        }
        return DefaultPartnerInventoryService.RECEIPT_MSG_SUCCESS.equals(result);
    }

    @Override
    public boolean generateOfflinePaymentReceiptAndEmail(PPSLMInventoryTransaction transParam, PPSLMOfflinePaymentRequest req) {
        String result = this.invService.generateReceiptAndEmail(transParam.getId(), req);
        return DefaultPartnerInventoryService.RECEIPT_MSG_SUCCESS.equals(result);
    }

    @Override
    public boolean sendVoidEmail(boolean b, PPSLMInventoryTransaction trans, String email, boolean isSubAccount, PPSLMTASubAccount subAccount, PPSLMTAMainAccount mainAccount) {
        //TODO
        log.info("Send Void Email...");
        return true;
    }

    @Override
    public ApiResult<String> confirmOfflineTransaction(Integer transId, String un) {
        return null;
    }
}
