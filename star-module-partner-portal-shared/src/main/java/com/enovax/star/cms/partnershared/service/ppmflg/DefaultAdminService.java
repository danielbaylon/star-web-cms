package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprover;
import com.enovax.star.cms.commons.jcrrepository.system.ppmflg.IUserRepository;
import com.enovax.star.cms.commons.model.partner.ppmflg.AdminAccountVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGApproverRepository;
import com.enovax.star.cms.partnershared.constant.ppmflg.AdminUserRoles;
import info.magnolia.cms.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("PPMFLGIAdminService")
public class DefaultAdminService implements IAdminService {

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;

    @Autowired
    @Qualifier("PPMFLGIUserRepository")
    private IUserRepository userRepository;

    @Autowired
    private PPMFLGApproverRepository approverRepository;

    @Autowired
    @Qualifier("PPMFLGIApproverService")
    private IApproverService approverSrv;


    @Override
    public List<String> getLoginAdminUserAssignedRights(String loginAdminId) {
        List<String> list = new ArrayList<String>();
        if(loginAdminId == null || loginAdminId.trim().length() == 0){
            return list;
        }
        List<String> assignedRoles = getApprovalRightsWithAssignedRightsByAdminId(loginAdminId);
        if(assignedRoles != null && assignedRoles.size() > 0){
            for(String str : assignedRoles){
                if(str != null && str.trim().length() > 0){
                    if(!list.contains(str.trim())){
                        list.add(str.trim());
                    }
                }
            }
        }
        boolean approver = approverSrv.isGeneralApproverUser(loginAdminId);
        if(approver){
            if(!list.contains(AdminUserRoles.GeneralApproverRole.code)){
                list.add(AdminUserRoles.GeneralApproverRole.code);
            }
        }
        boolean backupApprover = approverSrv.isGeneralBackupApproverUser(loginAdminId);
        if(backupApprover){
            if(!list.contains(AdminUserRoles.GeneralApproverBackupRole.code)){
                list.add(AdminUserRoles.GeneralApproverBackupRole.code);
            }
        }
        return list;
    }

    @Override
    public String getAdminUserNameByAdminId(String adminUserId) {
        if(adminUserId == null || adminUserId.trim().length() == 0){
            return null;
        }
        User user = userRepository.getUserById(adminUserId);
        if(user != null){
            return user.getName();
        }
        return null;
    }

    @Override
    public String getAdminUserEmailById(String adminUserId) {
        if(adminUserId == null || adminUserId.trim().length() == 0){
            return null;
        }
        User user = userRepository.getUserById(adminUserId);
        if(user != null){
            return user.getProperty("email");
        }
        return null;
    }

    @Override
    public String getDefaultPartnerApproverEmailAddress() {
        return paramSrv.getDefaultPartnerRegistrationEmailReceiver();
    }

    public List<String> getAdminUserAssignedRights(String loginAdminId) {
        return userRepository.getAdminUserAssignedRights(loginAdminId);
    }

    public List<String> getApprovalRightsWithAssignedRightsByAdminId(String loginAdminId) {
        List<String> lst = new ArrayList<String>();
        List<String> assignedRights = getAdminUserAssignedRights(loginAdminId);
        if(assignedRights != null && assignedRights.size() > 0){
            lst.addAll(assignedRights);
        }
        return lst;
    }

    @Override
    public boolean isPartnerProfileApprover(String loginAdminId) {
        List<PPMFLGApprover> approverList = approverRepository.findByAdminId(loginAdminId);
        if(approverList == null || approverList.size() == 0){
            return false;
        }
        return true;
    }

    @Override
    public List<AdminAccountVM> getPartnerMaintenanceAdminAccountList() {
        List<AdminAccountVM> vms = userRepository.getAdminAccountVMbyRole(AdminUserRoles.PartnerVerifyRole.code);
        return vms;
    }

    @Override
    public List<AdminAccountVM> getAssignedSalesAdminByCountryId(String ctyId) {
        List<AdminAccountVM> vms = new ArrayList<AdminAccountVM>();
        vms = userRepository.getAdminAccountVMByCountry(ctyId);
        return vms;
    }

    @Override
    public List<String> getAllAdminUsersEmailByRoleId(String offlinePayApproverRight) {
        List<String> emails = new ArrayList<String>();
        List<AdminAccountVM> vms = userRepository.getAdminAccountVMbyRole(offlinePayApproverRight);
        if(vms != null && vms.size() > 0){
            for(AdminAccountVM i : vms){
                if(i.getEmail() != null && i.getEmail().trim().length() > 0 && (!emails.contains(i.getEmail().trim()))){
                    emails.add(i.getEmail().trim());
                }
            }
        }
        return emails;
    }
}
