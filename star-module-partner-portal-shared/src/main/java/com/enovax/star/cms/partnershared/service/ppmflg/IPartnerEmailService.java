package com.enovax.star.cms.partnershared.service.ppmflg;


import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprovalLog;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackage;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;

public interface IPartnerEmailService {

    public void sendSignupAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    public void sendSignupEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    void sendSignupUpdateEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;

    void sendSignupUpdateAlertEmail(PartnerVM paVM, PartnerHist paHist) throws Exception;


    void sendingPartnerProfileRejectEmail(PartnerVM vm) throws Exception;

    void sendingPartnerProfileResubmitEmail(PartnerVM vm) throws Exception;

    void sendingPartnerProfileVerifingEmail(PPMFLGApprovalLog vm) throws Exception;

    void sendPartnerRegistrationRejectionEmail(ApprovingPartnerVM vm) throws Exception;

    void sendPartnerRegistrationApprovedEmail(ApprovingPartnerVM vm) throws Exception;

    void sendPartnerSubaccountCreationEmail(PartnerAccountVM vm) throws Exception;

    void sendPartnerSubaccountPasswordResetEmail(PartnerAccountVM vm) throws Exception;

    void sendTransactionReceiptEmail(PPMFLGInventoryTransaction trans, ReceiptVM receipt, String receiptName, String genpdfPath) throws Exception;

    void sendRevalTransactionReceiptEmail(PPMFLGInventoryTransaction trans,
                                          PPMFLGRevalidationTransaction revalTrans, ReceiptVM receipt,
                                          String receiptName, String genpdfpath) throws Exception;

    void sendPincodeEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendEticketEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendExcelEmail(PPMFLGMixMatchPackage immpkg, MixMatchPkgVM packageVM, String receiptName, String genpdfPath) throws Exception;

    void sendForgotPasswordEmail(String userName, String loginName, String newPwd, String email);
}
