package com.enovax.star.cms.b2cshared.service;

import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionExtension;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionItem;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGTelemoneyLog;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionExtension;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionItem;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMTelemoneyLog;
import com.enovax.star.cms.commons.model.axstar.AxStarSalesOrder;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.booking.StoreTransactionExtensionData;
import com.enovax.star.cms.commons.model.booking.StoreTransactionItem;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGStoreTransactionExtensionRepository;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGStoreTransactionItemRepository;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGStoreTransactionRepository;
import com.enovax.star.cms.commons.repository.b2cmflg.B2CMFLGTelemoneyLogRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionExtensionRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionItemRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionRepository;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMTelemoneyLogRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("Duplicates")
@Service
public class DefaultB2CTransactionService implements IB2CTransactionService {

    private Logger log = LoggerFactory.getLogger(DefaultB2CTransactionService.class);

    @Autowired
    private B2CSLMStoreTransactionRepository slmTransRepo;
    @Autowired
    private B2CSLMStoreTransactionItemRepository slmTransItemRepo;
    @Autowired
    private B2CSLMStoreTransactionExtensionRepository slmTransExtRepo;
    @Autowired
    private B2CMFLGStoreTransactionRepository mflgTransRepo;
    @Autowired
    private B2CMFLGStoreTransactionItemRepository mflgTransItemRepo;
    @Autowired
    private B2CMFLGStoreTransactionExtensionRepository mflgTransExtRepo;
    @Autowired
    private B2CSLMTelemoneyLogRepository slmTmRepo;
    @Autowired
    private B2CMFLGTelemoneyLogRepository mflgTmRepo;

    @Transactional
    public void performTransactionRelease() {

    }

    @Override
    @Transactional
    public void saveTelemoneyLog(StoreApiChannels channel, TelemoneyResponse response, boolean isFromQuery) {
        switch (channel) {
            case B2C_SLM:
                B2CSLMTelemoneyLog slmTmLog = new B2CSLMTelemoneyLog();

                slmTmLog.setIsQuery(isFromQuery);
                slmTmLog.setTmApprovalCode(response.getTmApprovalCode());
                slmTmLog.setTmBankRespCode(response.getTmBankRespCode());
                slmTmLog.setTmCurrency(response.getTmCurrency());
                slmTmLog.setTmDebitAmt(response.getTmDebitAmt());
                slmTmLog.setTmError(response.getTmError());
                slmTmLog.setTmErrorMessage(response.getTmErrorMessage());
                slmTmLog.setTmExpiryDate(response.getTmExpiryDate());
                slmTmLog.setTmMerchantId(response.getTmMerchantId());
                slmTmLog.setTmPaymentType(response.getTmPaymentType());
                slmTmLog.setTmRecurrentId(response.getTmRecurrentId());
                slmTmLog.setTmRefNo(response.getTmRefNo());
                slmTmLog.setTmStatus(response.getTmStatus());
                slmTmLog.setTmSubsequentMid(response.getTmSubsequentMid());
                slmTmLog.setTmSubTransType(response.getTmSubTransType());
                slmTmLog.setTmTransType(response.getTmTransType());
                slmTmLog.setTmUserField1(response.getTmUserField1());
                slmTmLog.setTmUserField2(response.getTmUserField2());
                slmTmLog.setTmUserField3(response.getTmUserField3());
                slmTmLog.setTmUserField4(response.getTmUserField4());
                slmTmLog.setTmUserField5(response.getTmUserField5());
                slmTmLog.setTmCcNum(response.getTmCcNum());
                slmTmLog.setTmEci(response.getTmEci());
                slmTmLog.setTmCavv(response.getTmCavv());
                slmTmLog.setTmXid(response.getTmXid());

                slmTmRepo.save(slmTmLog);
                break;
            case B2C_MFLG:
                B2CMFLGTelemoneyLog mflgTmLog = new B2CMFLGTelemoneyLog();

                mflgTmLog.setIsQuery(isFromQuery);
                mflgTmLog.setTmApprovalCode(response.getTmApprovalCode());
                mflgTmLog.setTmBankRespCode(response.getTmBankRespCode());
                mflgTmLog.setTmCurrency(response.getTmCurrency());
                mflgTmLog.setTmDebitAmt(response.getTmDebitAmt());
                mflgTmLog.setTmError(response.getTmError());
                mflgTmLog.setTmErrorMessage(response.getTmErrorMessage());
                mflgTmLog.setTmExpiryDate(response.getTmExpiryDate());
                mflgTmLog.setTmMerchantId(response.getTmMerchantId());
                mflgTmLog.setTmPaymentType(response.getTmPaymentType());
                mflgTmLog.setTmRecurrentId(response.getTmRecurrentId());
                mflgTmLog.setTmRefNo(response.getTmRefNo());
                mflgTmLog.setTmStatus(response.getTmStatus());
                mflgTmLog.setTmSubsequentMid(response.getTmSubsequentMid());
                mflgTmLog.setTmSubTransType(response.getTmSubTransType());
                mflgTmLog.setTmTransType(response.getTmTransType());
                mflgTmLog.setTmUserField1(response.getTmUserField1());
                mflgTmLog.setTmUserField2(response.getTmUserField2());
                mflgTmLog.setTmUserField3(response.getTmUserField3());
                mflgTmLog.setTmUserField4(response.getTmUserField4());
                mflgTmLog.setTmUserField5(response.getTmUserField5());
                mflgTmLog.setTmCcNum(response.getTmCcNum());
                mflgTmLog.setTmEci(response.getTmEci());
                mflgTmLog.setTmCavv(response.getTmCavv());
                mflgTmLog.setTmXid(response.getTmXid());

                mflgTmRepo.save(mflgTmLog);
                break;
            default:
                log.warn("Unsupported channel");
        }
    }

    @Override
    @Transactional
    public void saveStoreTransaction(StoreApiChannels channel, StoreTransaction txn, FunCart cart) {
        switch (channel) {
            case B2C_SLM:
                final B2CSLMStoreTransaction unsavedSlmTxn = constructB2CSLMTransaction(txn);
                final B2CSLMStoreTransaction slmTxn = slmTransRepo.save(unsavedSlmTxn);
                final List<B2CSLMStoreTransactionItem> unsavedSlmItems = new ArrayList<>();
                for (StoreTransactionItem sti : txn.getItems()) {
                    unsavedSlmItems.add(constructB2CSLMTransactionItem(txn, sti, slmTxn));
                }
                slmTransItemRepo.save(unsavedSlmItems);
                final B2CSLMStoreTransactionExtension slmExt = new B2CSLMStoreTransactionExtension();
                slmExt.setReceiptNumber(slmTxn.getReceiptNum());
                slmExt.setTransactionId(slmTxn.getId());
                slmExt.setCartJson(JsonUtil.jsonify(cart));
                slmExt.setStoreTransactionJson(JsonUtil.jsonify(txn));
//                slmExt.setSalesOrderJson(null);
//                slmExt.setTicketsJson(null);
                slmTransExtRepo.save(slmExt);
                break;
            case B2C_MFLG:
                final B2CMFLGStoreTransaction unsavedMflgTxn = constructB2CMFLGTransaction(txn);
                final B2CMFLGStoreTransaction mflgTxn = mflgTransRepo.save(unsavedMflgTxn);
                final List<B2CMFLGStoreTransactionItem> unsavedMflgItems = new ArrayList<>();
                for (StoreTransactionItem sti : txn.getItems()) {
                    unsavedMflgItems.add(constructB2CMFLGTransactionItem(txn, sti, mflgTxn));
                }
                mflgTransItemRepo.save(unsavedMflgItems);
                final B2CMFLGStoreTransactionExtension mflgExt = new B2CMFLGStoreTransactionExtension();
                mflgExt.setReceiptNumber(mflgTxn.getReceiptNum());
                mflgExt.setTransactionId(mflgTxn.getId());
                mflgExt.setCartJson(JsonUtil.jsonify(cart));
                mflgExt.setStoreTransactionJson(JsonUtil.jsonify(txn));
//                mflgExt.setSalesOrderJson(null);
//                mflgExt.setTicketsJson(null);
                mflgTransExtRepo.save(mflgExt);
                break;
            default:
                log.warn("Unsupported channel");
        }
    }

    @Override
    @Transactional
    public StoreTransactionExtensionData getStoreTransactionExtensionData(StoreApiChannels channel, String receiptNumber) {
        switch (channel) {
            case B2C_SLM:
                final B2CSLMStoreTransactionExtension slmTxnExt = slmTransExtRepo.findByReceiptNumber(receiptNumber);
                return constructExtDataFromDB(slmTxnExt, null);
            case B2C_MFLG:
                final B2CMFLGStoreTransactionExtension mflgTxnExt = mflgTransExtRepo.findByReceiptNumber(receiptNumber);
                return constructExtDataFromDB(null, mflgTxnExt);
            default:
                log.warn("Unsupported channel");
                return null;
        }
    }

    @Override
    @Transactional
    public void updateStoreTransactionExtensionData(StoreApiChannels channel, StoreTransactionExtensionData dataPackage) {
        switch (channel) {
            case B2C_SLM:
                final B2CSLMStoreTransactionExtension slmTxnExt = slmTransExtRepo.findByReceiptNumber(dataPackage.getReceiptNumber());
                if (dataPackage.getTxn() != null) {
                    slmTxnExt.setStoreTransactionJson(JsonUtil.jsonify(dataPackage.getTxn()));
                }
                if (dataPackage.getAxSalesOrder() != null) {
                    slmTxnExt.setSalesOrderJson(JsonUtil.jsonify(dataPackage.getAxSalesOrder()));
                }
                if (dataPackage.getCart() != null) {
                    slmTxnExt.setCartJson(JsonUtil.jsonify(dataPackage.getCart()));
                }
                slmTransExtRepo.save(slmTxnExt);
                break;
            case B2C_MFLG:
                final B2CMFLGStoreTransactionExtension mflgTxnExt = mflgTransExtRepo.findByReceiptNumber(dataPackage.getReceiptNumber());
                if (dataPackage.getTxn() != null) {
                    mflgTxnExt.setStoreTransactionJson(JsonUtil.jsonify(dataPackage.getTxn()));
                }
                if (dataPackage.getAxSalesOrder() != null) {
                    mflgTxnExt.setSalesOrderJson(JsonUtil.jsonify(dataPackage.getAxSalesOrder()));
                }
                if (dataPackage.getCart() != null) {
                    mflgTxnExt.setCartJson(JsonUtil.jsonify(dataPackage.getCart()));
                }
                mflgTransExtRepo.save(mflgTxnExt);
                break;
            default:
                log.warn("Unsupported channel");
                return;
        }
    }

    @Override
    @Transactional
    public B2CSLMStoreTransaction getB2CSLMStoreTransaction(String receiptNumber) {
        return slmTransRepo.findByReceiptNum(receiptNumber);
    }

    @Override
    @Transactional
    public void saveB2CSLMStoreTransaction(B2CSLMStoreTransaction txn) {
        slmTransRepo.save(txn);
    }

    @Override
    @Transactional
    public B2CMFLGStoreTransaction getB2CMFLGtoreTransaction(String receiptNumber) {
        return mflgTransRepo.findByReceiptNum(receiptNumber);
    }

    @Override
    @Transactional
    public void saveB2CMFLGStoreTransaction(B2CMFLGStoreTransaction txn) {
        mflgTransRepo.save(txn);
    }

    @Override
    public B2CSLMStoreTransaction constructB2CSLMTransaction(StoreTransaction st) {
        final Date now = new Date();

        final B2CSLMStoreTransaction t = new B2CSLMStoreTransaction();
        t.setReceiptNum(st.getReceiptNumber());
        t.setStage(st.getStage());

        t.setCreatedDate(now);
        t.setModifiedDate(now);
        t.setStatusDate(now);

        t.setTmMerchantId(st.getTmMerchantId());
        t.setTmError("");
        t.setTmErrorMessage("");
        t.setTmApprovalCode("");
        t.setTmStatus("");

        t.setCustCompanyName(st.getCustCompanyName());
        t.setCustDob(st.getCustDob());
        t.setCustEmail(st.getCustEmail());
        t.setCustIdNo(st.getCustIdNo());
        t.setCustIdType(st.getCustIdType());
        t.setCustMobile(st.getCustMobile());
        t.setCustName(st.getCustName());
        t.setCustNationality(st.getCustNationality());
        t.setCustReferSource(st.getCustReferSource());
        t.setCustSalutation(st.getCustSalutation());
        t.setCustSubscribe(st.isCustSubscribe());

        t.setPaymentType(st.getPaymentType());
        t.setTotalAmount(st.getTotalAmount());
        t.setTrafficSource(st.getTrafficSource());
        t.setCurrency(st.getCurrency());
        t.setIp(st.getIp());
        t.setLangCode(st.getLanCode());
        t.setMaskedCc("");
        t.setRemarks("");
        t.setCustJewelCard("");

        t.setPinCode("");
        t.setTicketMedia(st.isPinToGenerate() ? "pin" : "eticket"); //TODO don't hardcode?

        StringBuilder sb = new StringBuilder("");

        for (String pc : st.getAppliedPromoCodes().values()) {
            sb.append(pc).append(",");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        t.setPromoCodes(sb.toString());

        sb.setLength(0);
        if (st.getPaymentModeAffiliation() != null) {
            sb.append(st.getPaymentModeAffiliation().getAffiliationId());
        }
        if (st.getBankAffiliation() != null) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(st.getBankAffiliation().getAffiliationId());
        }
        t.setAffiliations(sb.length() > 0 ? sb.toString() : "");

        return t;
    }

    @Override
    public B2CMFLGStoreTransaction constructB2CMFLGTransaction(StoreTransaction st) {
        final Date now = new Date();

        final B2CMFLGStoreTransaction t = new B2CMFLGStoreTransaction();
        t.setReceiptNum(st.getReceiptNumber());
        t.setStage(st.getStage());

        t.setCreatedDate(now);
        t.setModifiedDate(now);
        t.setStatusDate(now);

        t.setTmMerchantId(st.getTmMerchantId());
        t.setTmError("");
        t.setTmErrorMessage("");
        t.setTmApprovalCode("");
        t.setTmStatus("");

        t.setCustCompanyName(st.getCustCompanyName());
        t.setCustDob(st.getCustDob());
        t.setCustEmail(st.getCustEmail());
        t.setCustIdNo(st.getCustIdNo());
        t.setCustIdType(st.getCustIdType());
        t.setCustMobile(st.getCustMobile());
        t.setCustName(st.getCustName());
        t.setCustNationality(st.getCustNationality());
        t.setCustReferSource(st.getCustReferSource());
        t.setCustSalutation(st.getCustSalutation());
        t.setCustSubscribe(st.isCustSubscribe());

        t.setPaymentType(st.getPaymentType());
        t.setTotalAmount(st.getTotalAmount());
        t.setTrafficSource(st.getTrafficSource());
        t.setCurrency(st.getCurrency());
        t.setIp(st.getIp());
        t.setLangCode(st.getLanCode());
        t.setMaskedCc("");
        t.setRemarks("");
        t.setCustJewelCard("");

        t.setPinCode("");
        t.setTicketMedia(st.isPinToGenerate() ? "pin" : "eticket"); //TODO don't hardcode?

        StringBuilder sb = new StringBuilder("");

        for (String pc : st.getAppliedPromoCodes().values()) {
            sb.append(pc).append(",");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        t.setPromoCodes(sb.toString());

        sb.setLength(0);
        if (st.getPaymentModeAffiliation() != null) {
            sb.append(st.getPaymentModeAffiliation().getAffiliationName());
        }
        if (st.getBankAffiliation() != null) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(st.getBankAffiliation().getAffiliationName());
        }
        t.setAffiliations(sb.toString());

        return t;
    }

    @Override
    public B2CSLMStoreTransactionItem constructB2CSLMTransactionItem(StoreTransaction st, StoreTransactionItem sti,
                                                                     B2CSLMStoreTransaction txn) {
        final B2CSLMStoreTransactionItem item = new B2CSLMStoreTransactionItem();
        item.setCmsProductId(sti.getCmsProductId());
        item.setCmsProductName(sti.getCmsProductName());
        item.setCmsProductNameByLang(sti.getCmsProductName());
        item.setDisplayDetails(sti.getDisplayDetails());
        item.setDisplayDetailsByLang(sti.getDisplayDetails());
        item.setDisplayName(sti.getName());
        item.setDisplayNameByLang(sti.getName());

        item.setBaseQty(sti.getQty());
        item.setQty(sti.getQty());
        item.setActualUnitPrice(sti.getSubtotal().divide(new BigDecimal(sti.getQty()), 2, BigDecimal.ROUND_HALF_UP));
        item.setBaseUnitPrice(sti.getUnitPrice());
        item.setSubTotal(sti.getSubtotal());
        item.setTicketType(sti.getType());
        item.setTransItemType(sti.getItemType());

        item.setEventGroupId(sti.getEventGroupId());
        item.setEventLineId(sti.getEventLineId());
        if (StringUtils.isNotEmpty(sti.getSelectedEventDate())) {
            Date ed;
            try {
                ed = NvxDateUtils.parseDate(sti.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing event date, should not happend.", e);
                ed = null;
            }
            item.setDateOfVisit(ed);
        }

        item.setItemListingId(sti.getListingId());
        item.setItemProductCode(sti.getProductCode());
        item.setMainItemCmsProductId(sti.getParentCmsProductId());
        item.setMainItemListingId(sti.getParentListingId());

        item.setOtherDetails(sti.getOtherDetails());

        item.setTransaction(txn);
        item.setTransactionId(txn.getId());

        item.setPromoCodes(StringUtils.isEmpty(sti.getPromoCode()) ? "" : sti.getPromoCode());
        item.setPromoCodeDiscountId(StringUtils.isEmpty(sti.getPromoCodeDiscountId()) ? "" : sti.getPromoCodeDiscountId());
        item.setAffiliations(""); //TODO fill?

        return item;
    }

    @Override
    public B2CMFLGStoreTransactionItem constructB2CMFLGTransactionItem(StoreTransaction st, StoreTransactionItem sti,
                                                                       B2CMFLGStoreTransaction txn) {
        final B2CMFLGStoreTransactionItem item = new B2CMFLGStoreTransactionItem();
        item.setCmsProductId(sti.getCmsProductId());
        item.setCmsProductName(sti.getCmsProductName());
        item.setCmsProductNameByLang(sti.getCmsProductName());
        item.setDisplayDetails(sti.getDisplayDetails());
        item.setDisplayDetailsByLang(sti.getDisplayDetails());
        item.setDisplayName(sti.getName());
        item.setDisplayNameByLang(sti.getName());

        item.setBaseQty(sti.getQty());
        item.setQty(sti.getQty());
        item.setActualUnitPrice(sti.getSubtotal().divide(new BigDecimal(sti.getQty()), 2, BigDecimal.ROUND_HALF_UP));
        item.setBaseUnitPrice(sti.getUnitPrice());
        item.setSubTotal(sti.getSubtotal());
        item.setTicketType(sti.getType());
        item.setTransItemType(sti.getItemType());

        item.setEventGroupId(sti.getEventGroupId());
        item.setEventLineId(sti.getEventLineId());
        if (sti.getSelectedEventDate() != null) {
            Date ed;
            try {
                ed = NvxDateUtils.parseDate(sti.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing event date, should not happend.", e);
                ed = null;
            }
            item.setDateOfVisit(ed);
        }

        item.setItemListingId(sti.getListingId());
        item.setItemProductCode(sti.getProductCode());
        item.setMainItemCmsProductId(sti.getParentCmsProductId());
        item.setMainItemListingId(sti.getParentListingId());

        item.setOtherDetails(sti.getOtherDetails());

        item.setTransaction(txn);
        item.setTransactionId(txn.getId());

        item.setPromoCodes(StringUtils.isEmpty(sti.getPromoCode()) ? "" : sti.getPromoCode());
        item.setPromoCodeDiscountId(StringUtils.isEmpty(sti.getPromoCodeDiscountId()) ? "" : sti.getPromoCodeDiscountId());
        item.setAffiliations(""); //TODO fill?

        return item;
    }
    
    @Override
    public StoreTransactionExtensionData constructExtDataFromDB(B2CSLMStoreTransactionExtension slmExt, B2CMFLGStoreTransactionExtension mflgExt) {
        final StoreTransactionExtensionData data = new StoreTransactionExtensionData();
        if (slmExt != null) {
            data.setId(slmExt.getId());
            data.setReceiptNumber(slmExt.getReceiptNumber());
            data.setTransactionId(slmExt.getTransactionId());
            data.setAxSalesOrder(StringUtils.isEmpty(slmExt.getSalesOrderJson()) ? null :
                    JsonUtil.fromJson(slmExt.getSalesOrderJson(), AxStarSalesOrder.class));
            data.setCart(StringUtils.isEmpty(slmExt.getCartJson()) ? null :
                    JsonUtil.fromJson(slmExt.getCartJson(), FunCart.class));
            data.setTicketsJson(slmExt.getTicketsJson());
            data.setTxn(StringUtils.isEmpty(slmExt.getStoreTransactionJson()) ? null :
                    JsonUtil.fromJson(slmExt.getStoreTransactionJson(), StoreTransaction.class));
        } else {
            data.setId(mflgExt.getId());
            data.setReceiptNumber(mflgExt.getReceiptNumber());
            data.setTransactionId(mflgExt.getTransactionId());
            data.setAxSalesOrder(StringUtils.isEmpty(mflgExt.getSalesOrderJson()) ? null :
                    JsonUtil.fromJson(mflgExt.getSalesOrderJson(), AxStarSalesOrder.class));
            data.setCart(StringUtils.isEmpty(mflgExt.getCartJson()) ? null :
                    JsonUtil.fromJson(mflgExt.getCartJson(), FunCart.class));
            data.setTicketsJson(mflgExt.getTicketsJson());
            data.setTxn(StringUtils.isEmpty(mflgExt.getStoreTransactionJson()) ? null :
                    JsonUtil.fromJson(mflgExt.getStoreTransactionJson(), StoreTransaction.class));
        }

        return data;
    }
}
