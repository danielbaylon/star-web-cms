package com.enovax.star.cms.partnershared.model.grid;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class PartnerFilter {
    private String orgName;
    private Integer prodId;
    private Integer[] partnerIds;
    private Boolean hasTier;
    private Integer tierId;
    private String paCode;
    private String contactPerson;
    private String status;
    private String[] statuses;
    private String channel;

    public PartnerFilter(String orgName, Integer prodId, Integer[] partnerIds) {
        super();
        this.orgName = orgName;
        this.prodId = prodId;
        this.partnerIds = partnerIds;
    }
    public PartnerFilter() {
        super();
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getProdId() {
        return prodId;
    }

    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    public Integer[] getPartnerIds() {
        return partnerIds;
    }

    public void setPartnerIds(Integer[] partnerIds) {
        this.partnerIds = partnerIds;
    }
    public Boolean getHasTier() {
        return hasTier;
    }
    public void setHasTier(Boolean hasTier) {
        this.hasTier = hasTier;
    }
    public Integer getTierId() {
        return tierId;
    }
    public void setTierId(Integer tierId) {
        this.tierId = tierId;
    }
    public String getPaCode() {
        return paCode;
    }
    public void setPaCode(String paCode) {
        this.paCode = paCode;
    }
    public String getContactPerson() {
        return contactPerson;
    }
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String[] getStatuses() {
        return statuses;
    }
    public void setStatuses(String[] statuses) {
        this.statuses = statuses;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
