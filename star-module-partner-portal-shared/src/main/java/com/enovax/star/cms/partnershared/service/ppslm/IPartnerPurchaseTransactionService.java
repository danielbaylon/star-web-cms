package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.model.api.ApiResult;

import java.util.List;

/**
 * Created by jennylynsze on 9/24/16.
 */
public interface IPartnerPurchaseTransactionService {
    void processTransactionReleaseReservation(PPSLMInventoryTransaction trans);

    void processTransactionReleaseReservation(PPSLMInventoryTransaction trans, boolean updateTransIncomplete);

    void processTransactionReleaseReservation(int transId);

    void processTransactionReleaseReservation(int transId, boolean updateTransIncomplete);

    List<PPSLMInventoryTransaction> getTransactionsForStatusUpdate(int timeoutLowerBoundMins,
                                                              int timeoutUpperBoundMins);

    PPSLMInventoryTransaction confirmTransaction(Integer transId);

    PPSLMInventoryTransaction getTransaction(int id, boolean includeItems);

    PPSLMInventoryTransaction getTransaction(String receiptNum, boolean includeItems);

    boolean saveTransaction(PPSLMInventoryTransaction trans);

    boolean generateAndEmailPurchaseReceipt(PPSLMInventoryTransaction transParam);

    boolean generateOfflinePaymentReceiptAndEmail(PPSLMInventoryTransaction transParam, PPSLMOfflinePaymentRequest req);

    boolean sendVoidEmail(boolean b, PPSLMInventoryTransaction trans, String email, boolean isSubAccount,
                          PPSLMTASubAccount subAccount, PPSLMTAMainAccount mainAccount);

    ApiResult<String> confirmOfflineTransaction(Integer transId,String un);
}
