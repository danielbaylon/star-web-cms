package com.enovax.star.cms.partnershared.repository.ppslm;


import com.enovax.star.cms.commons.model.axchannel.customerext.AxCountryRegion;
import com.enovax.star.cms.commons.model.partner.ppslm.CountryVM;

import java.util.List;

/**
 * Created by houtao on 22/9/16.
 */
public interface ICountryRegionsRepository {

//    public boolean deleteCountryById(String channel, String ctryId);

//    public boolean createCountry(String channel, AxCountryRegion info);

    boolean saveCountry(String channel, AxCountryRegion info);

//    public AxCountryRegion getCountryById(String channel, String ctryId);

    public List<AxCountryRegion> getCountryList(String channel);

    public boolean existsCountryId(String channel, String ctryId, String regionId);

    void deleteCountryIfNotInList(String channel, List<String> isoCountryCodeList);

    String getCountryNameByCountryId(String channel, String countryCode);

    public void refreshCache(String channel);

    public List<CountryVM> getCountryVmList(String channel);

//    List<CountryVM> getAllCountryVmList(String code);
}
