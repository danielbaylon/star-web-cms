package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.MixMatchPkgVM;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by jennylynsze on 11/7/16.
 */
public interface IPkgExcelGenerator {
    Workbook generatePkgExcel(List<MixMatchPkgVM> pkgvms, String startDateStr,
                     String endDateStr, String status, String ticketMedia, String pkgName);
}
