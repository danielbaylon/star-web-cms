package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPADocumentRepository;
import com.enovax.star.cms.commons.service.ftp.FTPService;
import com.enovax.star.cms.commons.service.ftp.FTPServiceImpl;
import com.enovax.star.cms.commons.service.ftp.FTPFileActionService;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 18/10/16.
 */
@Service
public class DefaultPartnerAxDocService implements IPartnerAxDocService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerAxDocService.class);

    @Autowired
    private PPSLMPADocumentRepository paDocRepo;

    @Autowired
    private ISystemParamService paramSrv;

    @Autowired
    private ISystemParamRepository paramRepo;

    @Override
    public void downloadPartnerDocuments(PPSLMPartner partner, List<PPSLMPADocMapping> docMap) throws BizValidationException, IOException {
        Map<String, String> ftpCfgs = paramSrv.getAxFtpConfigs();
        if(ftpCfgs == null || ftpCfgs.size() == 0){
            throw new BizValidationException("AX FTP Configs not found.");
        }
        String url  = ftpCfgs.get("ax.ftp.host");
        if(url == null || url.trim().length() == 0){
            throw new BizValidationException("AX FTP Host not found.");
        }
        String port = ftpCfgs.get("ax.ftp.port");
        if(port == null || port.trim().length() == 0){
            throw new BizValidationException("AX FTP Port not found.");
        }
        String user = ftpCfgs.get("ax.ftp.user");
        if(user == null || user.trim().length() == 0){
            throw new BizValidationException("AX FTP User not found.");
        }
        String pass = ftpCfgs.get("ax.ftp.pass");
        if(pass == null || pass.trim().length() == 0){
            throw new BizValidationException("AX FTP Pass not found.");
        }
        int ftpPort = -1;
        try{
            ftpPort = Integer.parseInt(port.trim());
        }catch (Exception ex){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        if(ftpPort == -1){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        String paDocFtpDir = paramSrv.getPartnerDocumentAxRootDir();
        if(paDocFtpDir == null || paDocFtpDir.trim().length() == 0){
            throw new BizValidationException("AX FTP Partner Document Directory not found.");
        }
        if(partner == null || docMap == null || docMap.size() == 0){
            return;
        }
        if(!(paDocFtpDir.trim().endsWith("/") || paDocFtpDir.trim().endsWith("\\"))){
            paDocFtpDir = paDocFtpDir + "/";
        }
        String paDocLocalDir = paramSrv.getPartnerDocumentRootDir();
        if(!(paDocLocalDir.trim().endsWith("/") || paDocLocalDir.trim().endsWith("\\"))){
            paDocLocalDir = paDocLocalDir + "/";
        }

        FTPService ftpService = new FTPServiceImpl();
        try {
            ftpService.init(paramRepo, PartnerPortalConst.Partner_Portal_Channel, DefaultSystemParamService.SYS_PARAM_SENTOSA_AX_FTP_CONFIG);
        } catch (Exception e) {
            throw new BizValidationException(e.getMessage());
        }

        FTPFileActionService ftpSrv = new FTPFileActionService();
        String trackingId = "Download-Pa-Docs-to-AX-"+partner.getId()+"";

        Iterator<PPSLMPADocMapping> iter = docMap.iterator();
        FTPClient client = null;
        while(iter.hasNext()){
            PPSLMPADocMapping doc = iter.next();
            if(doc != null){
                PPSLMPADocument paDoc = paDocRepo.findById(doc.getDocId());
                if(paDoc != null){
                    if(client == null){
                        client = ftpService.connect();
                        ftpService.login(client);
                    }
                    if(client == null){
                        throw new BizValidationException("Connecting to AX FTP server is failed");
                    }
                    processDocDownloading(trackingId, client, ftpSrv, paDoc, url, ftpPort, user, pass, paDocFtpDir, paDocLocalDir);
                }
            }
        }
        if(client != null){
            try{
                ftpService.logout(client);
            }catch (Exception ex){
            }catch (Throwable ex){
            }
        }
    }

    @Override
    public void syncPartnerDocuments(PPSLMPartner partner, List<PPSLMPADocMapping> docMap) throws BizValidationException, IOException {
        Map<String, String> ftpCfgs = paramSrv.getAxFtpConfigs();
        if(ftpCfgs == null || ftpCfgs.size() == 0){
            throw new BizValidationException("AX FTP Configs not found.");
        }
        String url  = ftpCfgs.get("ax.ftp.host");
        if(url == null || url.trim().length() == 0){
            throw new BizValidationException("AX FTP Host not found.");
        }
        String port = ftpCfgs.get("ax.ftp.port");
        if(port == null || port.trim().length() == 0){
            throw new BizValidationException("AX FTP Port not found.");
        }
        String user = ftpCfgs.get("ax.ftp.user");
        if(user == null || user.trim().length() == 0){
            throw new BizValidationException("AX FTP User not found.");
        }
        String pass = ftpCfgs.get("ax.ftp.pass");
        if(pass == null || pass.trim().length() == 0){
            throw new BizValidationException("AX FTP Pass not found.");
        }
        int ftpPort = -1;
        try{
            ftpPort = Integer.parseInt(port.trim());
        }catch (Exception ex){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        if(ftpPort == -1){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        String paDocFtpDir = paramSrv.getPartnerDocumentAxRootDir();
        if(paDocFtpDir == null || paDocFtpDir.trim().length() == 0){
            throw new BizValidationException("AX FTP Partner Document Directory not found.");
        }
        if(partner == null || docMap == null || docMap.size() == 0){
            return;
        }
        if(!(paDocFtpDir.trim().endsWith("/") || paDocFtpDir.trim().endsWith("\\"))){
            paDocFtpDir = paDocFtpDir + "/";
        }
        String paDocLocalDir = paramSrv.getPartnerDocumentRootDir();
        if(!(paDocLocalDir.trim().endsWith("/") || paDocLocalDir.trim().endsWith("\\"))){
            paDocLocalDir = paDocLocalDir + "/";
        }

        FTPService ftpService = new FTPServiceImpl();
        try {
            ftpService.init(paramRepo, PartnerPortalConst.Partner_Portal_Channel, DefaultSystemParamService.SYS_PARAM_SENTOSA_AX_FTP_CONFIG);
        } catch (Exception e) {
            throw new BizValidationException(e.getMessage());
        }

        FTPFileActionService ftpSrv = new FTPFileActionService();
        String trackingId = "Uploading-Pa-Docs-to-AX-"+partner.getId()+"";
        Iterator<PPSLMPADocMapping> iter = docMap.iterator();
        FTPClient client = null;
        while(iter.hasNext()){
            PPSLMPADocMapping doc = iter.next();
            if(doc != null){
                PPSLMPADocument paDoc = paDocRepo.findById(doc.getDocId());
                if(paDoc != null){
                    if(client == null){
                        client = ftpService.connect();
                        ftpService.login(client);
                    }
                    if(client == null){
                        throw new BizValidationException("Connecting to AX FTP server is failed");
                    }
                    processDocUploading(trackingId, client, ftpSrv, paDoc, url, ftpPort, user, pass, paDocFtpDir, paDocLocalDir);
                }
            }
        }
        if(client != null){
            try{
                ftpService.logout(client);
            }catch (Exception ex){
            }catch (Throwable ex){
            }
        }
    }

    private void processDocUploading(String trackingId, FTPClient client, FTPFileActionService ftpSrv, PPSLMPADocument paDoc, String url, int ftpPort, String user, String pass, String paDocFtpDir, String paDocLocalDir) throws BizValidationException {
        Integer docId = paDoc.getId();
        String  filePath = paDoc.getFilePath();
        String  fileName = paDoc.getFileName();
        String  fileType = paDoc.getFileType();
        if(docId != null && docId.intValue() > 0 && filePath != null && filePath.trim().length() > 0 && fileName != null && fileName.trim().length() > 0 && fileType != null && fileType.trim().length() > 0){
            String fileExt = getFileExt(fileName);
            String remoteFileDir = paDocFtpDir;
            String remoteFileName = docId+fileExt;
            paDoc.setRemoteFileDir(remoteFileDir);
            paDoc.setRemoteFileName(remoteFileName);
            paDoc.setAxFileViewPath("ftp://"+user+":"+pass+"@"+url+""+getCorrectDir(remoteFileDir)+remoteFileName);
            saveDocumentRemotePath(paDoc);
            syncDocUploading(trackingId, client, ftpSrv, paDoc, url, ftpPort, user, pass, paDocFtpDir, paDocLocalDir);
        }
    }

    private void processDocDownloading(String trackingId, FTPClient client, FTPFileActionService ftpSrv, PPSLMPADocument paDoc, String url, int ftpPort, String user, String pass, String paDocFtpDir, String paDocLocalDir) throws BizValidationException {
        Integer docId = paDoc.getId();
        String  filePath = paDoc.getFilePath();
        String  fileName = paDoc.getFileName();
        String  fileType = paDoc.getFileType();
        if(docId != null && docId.intValue() > 0 && filePath != null && filePath.trim().length() > 0 && fileName != null && fileName.trim().length() > 0 && fileType != null && fileType.trim().length() > 0){
            syncDocDownloading(trackingId, client, ftpSrv, paDoc, url, ftpPort, user, pass, paDocFtpDir, paDocLocalDir);
        }
    }

    private void syncDocUploading(String trackingId, FTPClient client, FTPFileActionService ftpSrv, PPSLMPADocument paDoc, String url, int ftpPort, String user, String pass, String paDocFtpDir, String paDocLocalDir) throws BizValidationException {
        String filePath = paDocLocalDir + paDoc.getFilePath();
        String remotePath = paDocFtpDir + getCorrectDir(paDoc.getRemoteFileDir())+ paDoc.getRemoteFileName();
        int maxTried = 0;
        boolean isSuccess = false;
        do{
            try {
                ftpSrv.storeFile(client, filePath, remotePath, trackingId+"-Doc"+paDoc.getId()+"-"+paDoc.getFilePath());
                isSuccess = true;
            } catch (FileNotFoundException e) {
                log.error("Upload file "+filePath+" ["+paDoc.getId()+"] to remote ftp "+remotePath + " is failed : "+e.getMessage(), e);
                isSuccess = false;
                e.printStackTrace();
            } catch (BizValidationException e) {
                log.error("Upload file "+filePath+" ["+paDoc.getId()+"] to remote ftp "+remotePath + " is failed : "+e.getMessage(), e);
                isSuccess = false;
                e.printStackTrace();
            }
            maxTried += 1;
        }while(maxTried < 3 && isSuccess == false);
        if(!isSuccess){
            throw new BizValidationException("Upload file to AX FTP server is failed");
        }
    }

    private void syncDocDownloading(String trackingId, FTPClient client, FTPFileActionService ftpSrv, PPSLMPADocument paDoc, String url, int ftpPort, String user, String pass, String paDocFtpDir, String paDocLocalDir) throws BizValidationException {
        String filePath = paDocLocalDir + paDoc.getFilePath();
        File localFile = new File(filePath);
        if(localFile.exists() && localFile.canRead()){
           return;
        }
        String parentFileDir = localFile.getParent();
        if(parentFileDir != null && parentFileDir.trim().length() > 0){
            File parentFileDirObj = new File(parentFileDir);
            if(!(parentFileDirObj.exists() && parentFileDirObj.isDirectory())){
                parentFileDirObj.mkdirs();
            }
        }
        String remotePath = paDocFtpDir + getCorrectDir(paDoc.getRemoteFileDir())+ paDoc.getRemoteFileName();
        int maxTried = 0;
        boolean isSuccess = false;
        do{
            try {
                ftpSrv.downloadFile(client, filePath, remotePath, trackingId+"-Doc"+paDoc.getId()+"-"+paDoc.getFilePath());
                isSuccess = true;
            } catch (FileNotFoundException e) {
                log.error("Downloading file "+filePath+" ["+paDoc.getId()+"] from remote ftp "+remotePath + " is failed : "+e.getMessage(), e);
                isSuccess = false;
                e.printStackTrace();
            } catch (BizValidationException e) {
                log.error("Downloading file "+filePath+" ["+paDoc.getId()+"] from remote ftp "+remotePath + " is failed : "+e.getMessage(), e);
                isSuccess = false;
                e.printStackTrace();
            }
            maxTried += 1;
        }while(maxTried < 3 && isSuccess == false);
        if(!isSuccess){
            throw new BizValidationException("Downloading file from AX FTP server is failed");
        }
    }

    private String getCorrectDir(String remoteFileDir) {
        if(!(remoteFileDir.trim().endsWith("/") || remoteFileDir.trim().endsWith("\\"))){
            remoteFileDir = remoteFileDir + "/";
        }
        return remoteFileDir;
    }

    @Transactional(rollbackFor = Exception.class)
    private void saveDocumentRemotePath(PPSLMPADocument paDoc) {
        paDocRepo.save(paDoc);
    }

    private String getFileExt(String fileName) {
        String ext = "";
        if(fileName != null && fileName.trim().length() > 0 && fileName.lastIndexOf(".") > 0){
            ext = fileName.substring(fileName.lastIndexOf("."));
        }
        if(ext == null || ext.trim().length() == 0){
            ext = "";
        }else{
            ext = ext.trim();
        }
        return ext;
    }
}
