package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.GSTRateVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lavanya on 24/10/16.
 */
public interface IGSTRateService {
    public BigDecimal getCurrentGST();

    List<GSTRateVM> listGstVms();

    public ResultVM saveGst(GSTRateVM gstVm, String userNm);

    public void removeGst(List<Integer> rcmdIds, String userNm);
}
