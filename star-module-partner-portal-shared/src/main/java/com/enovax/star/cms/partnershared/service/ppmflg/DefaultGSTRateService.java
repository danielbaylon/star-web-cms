package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGGSTRate;
import com.enovax.star.cms.commons.model.partner.ppmflg.GSTRateVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGGSTRateRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGReasonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lavanya on 24/10/16.
 */
@Service("PPMFLGIGSTRateService")
public class DefaultGSTRateService implements IGSTRateService{

    private Logger log = LoggerFactory.getLogger(DefaultGSTRateService.class);
    @Autowired
    PPMFLGReasonRepository reasonRepository;
    @Autowired
    PPMFLGGSTRateRepository gstRateRepository;

    @Override
    @Transactional(readOnly = true)
    public BigDecimal getCurrentGST() {
        PPMFLGGSTRate gstRate = gstRateRepository.getGSTRateByDate(new Date());
        if (gstRate != null) {
            log.debug("Current GST is " + gstRate.getRate());
            return gstRate.getRate();
        } else {
            log.debug("Current GST is null");
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<GSTRateVM> listGstVms() {
        List<String> status = new ArrayList<>();
        status.add(GeneralStatus.Active.code);
        List<PPMFLGGSTRate> gsts = gstRateRepository
                .listGSTRate(status);
        List<GSTRateVM> gstVms = new ArrayList<>();
        for (PPMFLGGSTRate gst : gsts) {
            gstVms.add(new GSTRateVM(gst));
        }
        return gstVms;
    }


    @Override
    @Transactional
    public ResultVM saveGst(GSTRateVM gstVm, String userNm) {
        ResultVM res = new ResultVM();
        PPMFLGGSTRate gst = null;
        if(gstVm.getId()!=null&&gstVm.getId()>0){
            gst = gstRateRepository.findOne(gstVm.getId());
        }else{
            gst = new PPMFLGGSTRate();
            gst.setCreatedBy(userNm);
            gst.setCreatedDate(new Date());
        }
        gst.setRate(new BigDecimal((gstVm.getRateInInt().intValue())).divide(
                new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
        gst.setStartDate(gstVm.getStartDate());
        gst.setEndDate(gstVm.getEndDate());
        gst.setStatus(GeneralStatus.Active.code );
        gst.setModifiedBy(userNm);
        gst.setModifiedDate(new Date());
        List<PPMFLGGSTRate> gsts = null;
        if(gst.getEndDate() == null) {
            gsts = gstRateRepository.getOverlappedGstWOEndDate(gst.getId(),gst.getStartDate());
        }
        else {
            gsts = gstRateRepository.getOverlappedGst(gst.getId(),gst.getStartDate(),gst.getEndDate());
        }
        if(gsts!=null && gsts.size()>0){
            res.setMessage(ApiErrorCodes.GSTOverlapError.message);
            res.setStatus(false);
            return res;
        }
        gstRateRepository.save(gst);
        GSTRateVM newGstVm = new GSTRateVM(gst);
        res.setViewModel(newGstVm);
        return res;
    }

    @Override
    @Transactional
    public void removeGst(List<Integer> rcmdIds, String userNm) {
        for(Integer id:rcmdIds){
            PPMFLGGSTRate gst = gstRateRepository.findOne(id);
            gst.setStatus(GeneralStatus.Deleted.code);
            gst.setModifiedBy(userNm);
            gst.setModifiedDate(new Date());
            gstRateRepository.save(gst);
        }
    }
}
