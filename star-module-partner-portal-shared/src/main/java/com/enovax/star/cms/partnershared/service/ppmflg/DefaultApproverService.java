package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprover;
import com.enovax.star.cms.commons.jcrrepository.system.ppmflg.DefaultJcrUserRepository;
import com.enovax.star.cms.commons.model.partner.ppmflg.AdminAccountVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ApproverVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGApproverRepository;
import com.enovax.star.cms.partnershared.constant.ppmflg.ApproverTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lavanya on 28/9/16.
 */
@Service("PPMFLGIApproverService")
public class DefaultApproverService implements IApproverService {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private PPMFLGApproverRepository approverRepo;

    @Autowired
    @Qualifier("PPMFLGIUserRepository")
    private DefaultJcrUserRepository adminUserRepo;

    @Override
    public boolean isGeneralApproverUser(String loginAdminId) {
        if(loginAdminId == null || loginAdminId.trim().length() == 0){
            return false;
        }
        int c = approverRepo.countGeneralApproverByAdminId(loginAdminId, new Date(System.currentTimeMillis()));
        return c > 0;
    }

    @Override
    public boolean isGeneralBackupApproverUser(String loginAdminId) {
        if(loginAdminId == null || loginAdminId.trim().length() == 0){
            return false;
        }
        int c = approverRepo.countGeneralBackupApproverByAdminId(loginAdminId, new Date(System.currentTimeMillis()));
        return c > 0;
    }

    @Override
    @Transactional(readOnly = true)
    public ApproverVM getCurrentApprover() {
        PPMFLGApprover approver = approverRepo.findCurrentApprover(ApproverTypes.General.code);
        if (approver == null) {
            return null;
        }
        return new ApproverVM(approver);
    }


    @Override
    @Transactional(readOnly = true)
    public List<ApproverVM> getCandidates() {
        List<AdminAccountVM> admins = adminUserRepo.getAllPPMFLGUsers();
        List<ApproverVM> approvers = new ArrayList<>();
        for (AdminAccountVM admin : admins) {
            approvers.add(new ApproverVM(admin));
        }
        return approvers;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApproverVM> getGeneralBackupApproverbyPage(String orderBy, String orderWith, Integer page, Integer pagesize) {
        if (orderWith == null) {
            orderWith = "DESC";
            orderBy = "id";
        }

        PageRequest pageRequest = new PageRequest(page - 1, pagesize,
                "ASC".equalsIgnoreCase(orderWith) ? Sort.Direction.ASC : Sort.Direction.DESC,
                orderBy);

        Page<PPMFLGApprover> backupApproverPaged = approverRepo.getBackupApprover(pageRequest);
        Iterator<PPMFLGApprover> backupApproverIterator = backupApproverPaged.iterator();
        List<ApproverVM> backupApproverList = new ArrayList<>();
        while (backupApproverIterator.hasNext()) {
            PPMFLGApprover backupApprover = backupApproverIterator.next();
            ApproverVM backupApproverVm = new ApproverVM(backupApprover);
            backupApproverList.add(backupApproverVm);
        }
        return backupApproverList;
    }

    @Override
    @Transactional(readOnly = true)
    public int getGeneralBackupApproverSize() {
        int backupApproverCount = approverRepo.countBackupApprover();
        return backupApproverCount;
    }


    @Override
    @Transactional
    public ResultVM saveApprover(String approverAdminId, String userNm) {
        ResultVM res = new ResultVM();
        PPMFLGApprover approver = approverRepo.findCurrentApprover(ApproverTypes.General.code);
        if(approver != null&&approver.getAdminId().equals(approverAdminId)){
            res.setMessage(ApiErrorCodes.BackupAlreadyAdmin.message);
            res.setStatus(false);
            return res;
        }
        List<PPMFLGApprover> apps = approverRepo.findByAdminId(approverAdminId);
        if(apps!=null && apps.size()>0){
            res.setMessage(ApiErrorCodes.BackupAlreadyCovering.message);
            res.setStatus(false);
            return res;
        }

        if (approver != null) {
            approver.setStatus(GeneralStatus.Inactive.code);
            approver.setModifiedBy(userNm);
            approver.setModifiedDate(new Date());
            approverRepo.save(approver);
        }
        PPMFLGApprover newApprover = new PPMFLGApprover();
        newApprover.setAdminId(approverAdminId);
        newApprover.setModifiedBy(userNm);
        newApprover.setModifiedDate(new Date());
        newApprover.setCreatedBy(userNm);
        newApprover.setCreatedDate(new Date());
        newApprover.setCategory(ApproverTypes.General.code);
        newApprover.setStatus(GeneralStatus.Active.code);
        approverRepo.save(newApprover);
        ApproverVM appVM = new ApproverVM(newApprover);
        res.setViewModel(appVM);
        return res;
    }


    @Override
    @Transactional
    public ResultVM saveBackupApprover(ApproverVM backupApprover, String userNm) {
        ResultVM res = new ResultVM();

        PPMFLGApprover approver = null;
        if (backupApprover.getId() != null && backupApprover.getId() != -1) {
            approver = approverRepo.findById(backupApprover.getId());
            approver.setModifiedBy(userNm);
            approver.setModifiedDate(new Date());
            approver.setStatus(backupApprover.getStatus());
            approver.setStartDate(backupApprover.getStartDate());
            approver.setEndDate(backupApprover.getEndDate());
        } else {
            approver = new PPMFLGApprover();
            approver.setAdminId(backupApprover.getAdminId());
            approver.setStartDate(backupApprover.getStartDate());
            approver.setEndDate(backupApprover.getEndDate());
            approver.setModifiedBy(userNm);
            approver.setModifiedDate(new Date());
            approver.setCreatedBy(userNm);
            approver.setCreatedDate(new Date());
            approver.setCategory(ApproverTypes.GeneralBackup.code);
            approver.setStatus(GeneralStatus.Active.code);
        }
        res = this.verifyBackupOfficer(backupApprover);
        if(!res.isStatus()){
            return res;
        }
        approverRepo.save(approver);
        return res;
    }

    @Override
    @Transactional
    public void removeBackupApprovers(List<Integer> rcmdIds, String userNm) {
        for(Integer id:rcmdIds){
            PPMFLGApprover approver = approverRepo.findById(id);
            approver.setStatus(GeneralStatus.Deleted.code);
            approver.setModifiedBy(userNm);
            approver.setModifiedDate(new Date());
            approverRepo.save(approver);
        }
    }

    private ResultVM verifyBackupOfficer(ApproverVM backupApprover){
        ResultVM res = new ResultVM();
        ApproverVM currentApp = this.getCurrentApprover();
        if(currentApp!=null){
            if(currentApp.getAdminId().equals(backupApprover
                    .getAdminId())){
                res.setMessage(ApiErrorCodes.BackupSelfBackup.message);
                res.setStatus(false);
                return res;
            }

        }
        List<PPMFLGApprover> apps = approverRepo.getOverlappedBackupOfficers(backupApprover.getAdminId(),backupApprover.getStartDate(),backupApprover.getEndDate(),backupApprover.getId());
        if(apps!=null && apps.size()>0){
            res.setMessage(ApiErrorCodes.BackupOverlap.message);
            res.setStatus(false);
            return res;
        }
        return res;
    }


}
