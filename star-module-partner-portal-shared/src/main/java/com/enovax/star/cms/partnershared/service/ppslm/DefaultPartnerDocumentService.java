package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.GeneralStatus;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerDocumentVM;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPADocMappingRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPADocumentRepository;
import com.enovax.star.cms.commons.util.FileUtil;
import info.magnolia.cms.beans.runtime.Document;
import org.apache.commons.betwixt.io.id.RandomIDGenerator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class DefaultPartnerDocumentService implements IPartnerDocumentService {

    public static final Logger log = LoggerFactory.getLogger(DefaultPartnerDocumentService.class);

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private PPSLMPADocumentRepository paDocRepo;

    @Autowired
    private PPSLMPADocMappingRepository paDocMapRepo;

    @Autowired
    private IPartnerAxDocService axDocSrv;

    @Override
    public PPSLMPADocument savePartnerDocuments(PPSLMTAMainAccount taMainAcc, PPSLMPartner pa, Document doc, String fileType) throws Exception {
        if(taMainAcc == null || pa == null || doc == null){
            return null;
        }
        String fileUploadPath = sysParamSrv.getPartnerDocumentRootDir();
        String partnerDocDir = getPartnerDocumentRootDir(taMainAcc.getAccountCode(), fileUploadPath);
        File file = doc.getFile();
        String hashedName = FileUtil.hashFile(doc.getFile());
        Random rand = new Random();
        hashedName = hashedName+"_"+ rand.nextInt(100)+1;
        log.debug("[saveFile] fileuploadPath." + partnerDocDir + file.getName());
        PPSLMPADocument paDoc = savePartnerDocuments(
                taMainAcc.getAccountCode() + PartnerPortalConst.TA_MAIN_ACC_POSTFIX,
                taMainAcc.getAccountCode(), hashedName, file.getName(),fileType);

        savePartnerDocumentMapping(paDoc, pa);

        if(paDoc != null){
            FileUtils.copyFile(file, new File(partnerDocDir + hashedName));
        }
        return paDoc;
    }

    @Transactional(readOnly =  true)
    public PPSLMPADocument getPartnerDocumentById(Integer id) {
        return paDocRepo.findById(id);
    }

    private void savePartnerDocumentMapping(PPSLMPADocument paDoc, PPSLMPartner pa) {
        PPSLMPADocMapping map = new PPSLMPADocMapping();
        map.setDocId(paDoc.getId());
        map.setPartnerId(pa.getId());
        paDocMapRepo.save(map);
    }

    private PPSLMPADocument savePartnerDocuments(String username, String docDir, String hashedName, String name, String type) {
        PPSLMPADocument doc = new PPSLMPADocument();
        doc.setFileName(name);
        doc.setFilePath(docDir + File.separator + hashedName);
        doc.setFileType(type);
        doc.setStatus(GeneralStatus.Active.code);
        doc.setCreatedBy(username);
        doc.setCreatedDate(new Date(System.currentTimeMillis()));
        doc.setModifiedBy(username);
        doc.setModifiedDate(new Date(System.currentTimeMillis()));
        paDocRepo.save(doc);
        return doc;
    }

    private String getPartnerDocumentRootDir(String accountCode, String fileUploadPath) throws Exception {
        if(accountCode == null || accountCode.trim().length() == 0){
            throw new Exception("ERROR_EMPTY_ACC_CODE");
        }
        return fileUploadPath + File.separator + accountCode.trim() + File.separator;
    }

    @Transactional(readOnly =  true)
    public List<PartnerDocumentVM> getPartnerDocumentsByPartnerId(Integer paId) {
        List<PartnerDocumentVM> docs = new ArrayList<PartnerDocumentVM>();
        if(paId != null) {
            List<PPSLMPADocMapping> maps = paDocMapRepo.findByPartnerId(paId);
            if (maps != null) {
                for (PPSLMPADocMapping map : maps) {
                    PPSLMPADocument doc = map.getPaDoc();
                    if(doc != null){
                        docs.add(new PartnerDocumentVM(doc));
                    }
                }
            }
        }
        return docs;
    }

    @Override
    public boolean isValidPartnerDocument(Document doc) {
        if(doc == null){
            return true;
        }
        if(!(doc.getLength() > 0 && doc.getLength() < sysParamSrv.getPartnerDocumentMaximumSizeLimit())){
            return false;
        }
        String ext = doc.getExtension();
        if(ext == null || ext.trim().length() == 0){
            return false;
        }
        String supportedType = sysParamSrv.getPartnerDocumentSupportedFileType();
        if(supportedType == null || supportedType.trim().length() == 0){
            return false;
        }
        if(!(supportedType.trim().toUpperCase().indexOf("#"+ext.trim().toUpperCase()+"#") >= 0)){
            return false;
        }
        return true;
    }
}
