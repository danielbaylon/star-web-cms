package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.partner.ppmflg.MixMatchPkgVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentNotifyVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ReceiptVM;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.StringTemplateSource;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.github.jknack.handlebars.io.TemplateSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

@Service("PPMFLGITemplateService")
public class DefaultTemplateService implements ITemplateService {

    private Logger logger = LoggerFactory.getLogger(DefaultTemplateService.class);

    private static final String CHANNEL = PartnerPortalConst.Partner_Portal_Channel;

    @Autowired
    @Qualifier("PPMFLGIEmailTemplateService")
    private IEmailTemplateService tplSrv;

    private Handlebars hbars = null;
    private Template approverAlertTemplate = null;
    private Template partnerRegistrationAlertTemplate = null;
    private Template partnerRegistrationUpdateAlertTemplate = null;
    private Template partnerRegistrationSignupTemplate = null;
    private Template partnerRegistrationSignupUpdateTemplate = null;
    private Template partnerRegistrationRejectTemplate = null;
    private Template partnerRegistrationApprovedTemplate = null;
    private Template partnerRegistrationResubmitTemplate = null;
    private Template partnerWoTEmailTemplate = null;
    private Template partnerSubaccountCreationTemplate = null;
    private Template partnerSubaccountPasswordResetTemplate = null;
    private Template partnerTransactionReceiptPdfTemplate = null;
    private Template partnerTransactionReceiptEmailTemplate = null;
    private Template partnerRevalTransactionReceiptPdfTemplate = null;
    private Template partnerRevalTransactionReceiptEmailTemplate = null;

    private Template partnerOfflinePaymentEmailTemplate = null;
    private Template partnerDepositTopupPdfTemplate = null;
    private Template partnerDepositTopupEmailTemplate = null;
    private Template pincodePdfTemplate = null;
    private Template pincodeTemplateBody = null;
    private Template pincodeTemplateFooter = null;
    private Template eticketEmailTemplate = null;
    private Template excelEmailTemplate = null;

    private Template wotDailyReceiptTemplate = null;
    private Template wotMonthlyReceiptTemplate = null;

    @PostConstruct
    public void init() throws IOException {
        final TemplateLoader loader = new TemplateLoader() {
            public TemplateSource sourceAt(String s) throws IOException {
                SysEmailTemplateVM vm = tplSrv.getSystemParamByKey(CHANNEL, s);
                final String fileName = s;
                final String content = vm.getBody();
                return new StringTemplateSource(fileName, content);
            }
            public String resolve(String s) {
                return "";
            }
            public String getPrefix() {
                return  "";
            }
            public String getSuffix() {
                return "";
            }
            public void setPrefix(String s) {}
            public void setSuffix(String s) {}
        };
        this.hbars = new Handlebars(loader);
        initTemplates();
    }

    private void initTemplates() {
        this.approverAlertTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_APPROVER_ALERT);
        this.partnerRegistrationAlertTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_ALERT);
        this.partnerRegistrationUpdateAlertTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_UPDATE_ALERT);
        this.partnerRegistrationSignupTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_SIGNUP);
        this.partnerRegistrationSignupUpdateTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_SIGNUP_UPDATE);
        this.partnerRegistrationRejectTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_REJECTED);
        this.partnerRegistrationApprovedTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_APPROVED);
        this.partnerRegistrationResubmitTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_REGISTRATION_RESUBMIT);
        this.partnerWoTEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_WOT);
        this.partnerSubaccountCreationTemplate = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_SUBACCOUNT_CREATION_EMAIL);
        this.partnerSubaccountPasswordResetTemplate = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_SUBACCOUNT_PASSWORD_RESET_EMAIL);
        this.partnerTransactionReceiptPdfTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_TRANSACTION_RECEIPT_PDF);
        this.partnerTransactionReceiptEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_TRANSACTION_RECEIPT_EMAIL);
        this.partnerDepositTopupPdfTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_PDF);
        this.partnerDepositTopupEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_EMAIL);
        this.pincodePdfTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PINCODE_PDF);
        this.pincodeTemplateBody = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_PINCODE_BODY);
        this.pincodeTemplateFooter = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_PINCODE_FOOTER);
        this.eticketEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_ETICKET);
        this.excelEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_EXCEL);
        this.partnerOfflinePaymentEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_PARTNER_OFFLINE_PAYMENT);
        this.partnerRevalTransactionReceiptPdfTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_REVAL_RECEIPT_PDF);
        this.partnerRevalTransactionReceiptEmailTemplate = initTemplateByEmailId(DefaultEmailTemplateService.CONTENT_PARTNER_REVAL_RECEIPT_EMAIL);
        this.wotDailyReceiptTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_WOT_DAILY_RECEIPT_PDF);
        this.wotMonthlyReceiptTemplate = initTemplateByEmailId(DefaultEmailTemplateService.HANDLEBARS_WOT_MONTHLY_RECEIPT_PDF);
    }

    private Template initTemplateByEmailId(String key) {
        if(key != null && key.trim().length() > 0 && tplSrv.hasEmailTemplateByKey(CHANNEL, key)){
            try{
                return hbars.compile(key);
            }catch (Exception ex){
                logger.error("[Critical] - init email template failed for "+key +", error message : "+ex.getMessage(), ex);
            }
        }else{
            logger.error("[Critical] - No email template found in JCR for "+key);
        }
        return null;
    }

    @Override
    public boolean generateWoTMonthlyReceiptReceiptPDF(ReceiptVM receipt, String receiptPath) throws Exception {
        return generatePdf(wotMonthlyReceiptTemplate,receipt,receiptPath);
    }

    @Override
    public boolean generateWoTDailyReceiptReceiptPDF(ReceiptVM receipt, String receiptPath) throws Exception {
        return generatePdf(wotDailyReceiptTemplate,receipt,receiptPath);
    }

    public boolean generateDepositTopupReceiptPDF(ReceiptVM receiptVM, String outputFilePath) throws Exception {
        return generatePdf(partnerDepositTopupPdfTemplate,receiptVM,outputFilePath);
    }

    public String generateDepositTopupReceiptHTML(Object mode) throws Exception {
        return generateHtml(partnerDepositTopupEmailTemplate, mode);
    }

    @Override
    public void generatePartnerTransactionReceiptPdf(ReceiptVM receipt, String outputFilePath) throws Exception {
        generatePdf(this.partnerTransactionReceiptPdfTemplate,receipt,outputFilePath);
    }

    @Override
    public void generatePincodePdf(MixMatchPkgVM receipt, String outputFilePath) throws Exception {
        generatePdf(this.pincodePdfTemplate,receipt,outputFilePath);
    }

    @Override
    public String generatePincodeEmailHtml(Object model) throws Exception {
        return generateHtml(pincodePdfTemplate, model);
    }

    @Override
    public String generateEticketEmailHtml(Object model) throws Exception {
        return generateHtml(eticketEmailTemplate, model);
    }

    @Override
    public String generateExcelEmailHtml(Object model) throws Exception {
        return generateHtml(eticketEmailTemplate, model);
    }

    @Override
    public String generatePincodeBody(Object model) throws Exception {
        return generateHtml(pincodeTemplateBody, model);
    }

    @Override
    public String generatePincodeFooter(Object model) throws Exception {
        return generateHtml(pincodeTemplateFooter, model);
    }

    public String generateApproverAlertEmailHtml(Object model) throws Exception {
        return generateHtml(approverAlertTemplate, model);
    }

    public String generatePartnerRegistrationAlertEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationAlertTemplate, model);
    }

    public String generatePartnerRegistrationUpdateAlertEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationUpdateAlertTemplate, model);
    }

    public String generatePartnerRegistrationSignupEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationSignupTemplate, model);
    }

    public String generatePartnerRegistrationSignupUpdateEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationSignupUpdateTemplate, model);
    }

    public String generatePartnerRegistrationRejectEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationRejectTemplate, model);
    };

    public String generatePartnerRegistrationApprovedEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationApprovedTemplate, model);
    }

    public String generatePartnerRegistrationResubmitEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRegistrationResubmitTemplate, model);
    }

    public String generatePartnerSubaccountCreationEmailHtml(Object model) throws Exception {
        return generateHtml(partnerSubaccountCreationTemplate, model);
    }

    public String generatePartnerSubaccountPasswordResetEmailHtml(Object model) throws Exception {
        return generateHtml(partnerSubaccountPasswordResetTemplate, model);
    }

    public String generatePartnerWoTEmailHtml(Object model) throws Exception {
        return generateHtml(partnerWoTEmailTemplate, model);
    }

    public String generatePartnerTransactionReceiptEmailHtml(Object model) throws Exception {
        return generateHtml(partnerTransactionReceiptEmailTemplate, model);
    }

    @Override
    public void generatePartnerRevalTransactionReceiptPdf(ReceiptVM receipt, String outputFilePath) throws Exception {
        generatePdf(this.partnerRevalTransactionReceiptPdfTemplate, receipt,outputFilePath);
    }

    @Override
    public String generatePartnerRevalTransactionReceiptEmailHtml(Object model) throws Exception {
        return generateHtml(partnerRevalTransactionReceiptEmailTemplate, model);
    }

    @Override
    public String generateOfflinePaymentNotificationEmailHtml(OfflinePaymentNotifyVM model) throws Exception {
        return generateHtml(partnerOfflinePaymentEmailTemplate, model);
    }

    private String generateHtml(Template template, Object model) throws Exception {
        final Context ctx = Context.newContext(model);
        String htmlStr = template.apply(ctx);
        return htmlStr;
    }

    private boolean generatePdf(Template template, Object viewModel,
                             String outputFilePath) throws Exception {
        final ITextRenderer renderer = new ITextRenderer();
        String html = generateHtml(template, viewModel);
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(new FileOutputStream(new File(outputFilePath)), true);
        return true;
    }

    public boolean generatePdfByHtml(String key, String html, String outputFilePath) {
        final ITextRenderer renderer = new ITextRenderer();
        File file = null;

        FileOutputStream fos = null;
        try{
            renderer.setDocumentFromString(html);
            renderer.layout();
            file = new File(outputFilePath);
            fos = new FileOutputStream(file);
            renderer.createPDF(fos, true);
            fos.flush();
        }catch (Exception ex){
            logger.error("generatePdfByHtml - Exception during generate pdf for " + key + " is failed, error message : "+ex.getMessage(), ex);
        }catch (Throwable tb){
            logger.error("generatePdfByHtml - Throwable during generate pdf for " + key + " is failed, error message : "+tb.getMessage(), tb);
        }finally{
            if(fos != null){
                try{
                    fos.close();
                }catch (Exception ex){
                    logger.error("generatePdfByHtml - close FileOutputStream during generate pdf for "+key+" is failed");
                }catch (Throwable tb2) {
                    logger.error("generatePdfByHtml - close FileOutputStream during generate pdf for " + key + " is failed");
                }
            }
        }
        //System.out.println(html);
        if(file != null && file.isFile() && file.exists() && file.canRead()){
            return true;
        }
        logger.error("close FileOutputStream during generate pdf for " + key + " is failed");
        return false;

    }

    public String processEmailParams(String content, Map<String, String> paramMap) {
        if(content == null || content.trim().length() == 0 || paramMap == null || paramMap.size() == 0){
            return content;
        }
        Iterator<Map.Entry<String, String>> iter = paramMap.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry<String, String> entry = iter.next();
            content = content.replace(getNonEmptyStringKey(entry.getKey()), getNonEmptyStringValue(entry.getValue()));
        }
        return content;
    }

    private String getNonEmptyStringKey(String key) {
        if(key != null && key.trim().length() > 0){
            return "##"+(key.trim())+"##";
        }
        return "####";
    }

    private String getNonEmptyStringValue(String val){
        if(val != null && val.trim().length() > 0){
            return val.trim();
        }
        return "";
    }

}
