package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.ReasonVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;

import java.util.List;


public interface IReasonService {

    public List<ReasonVM> populateActiveReasonVms();

    public List<ReasonVM> populateReasonsByIds(List<Integer> reasonIds);

    public List<ReasonVM> getReasonsByLogId(Integer logId);

    public ResultVM getReasonsbyPage(String orderBy,
                                     String orderWith, Integer page, Integer pagesize);

    public ReasonVM saveReason(ReasonVM reasonVm, String userNm);

    public void removeReasons(List<Integer> rcmdIds, String userNm);

}
