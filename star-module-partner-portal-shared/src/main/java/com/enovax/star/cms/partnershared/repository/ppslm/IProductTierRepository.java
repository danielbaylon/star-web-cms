package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.tier.ProductTier;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxPriceGroup;

import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
public interface IProductTierRepository {
    List<ProductTier> getTiers(String channel);
    ProductTier getTier(String channel,String tierId);
    Integer getNextId();
    boolean hasPriceGroup(String channel,String priceGroupId);
    public void removePriceGroup(String channel, String priceGroupId);
    public void createOrUpdatePriceGroup(String channel, AxPriceGroup priceGroup);
    //void saveTier(ProductTierVM tierVM);
}
