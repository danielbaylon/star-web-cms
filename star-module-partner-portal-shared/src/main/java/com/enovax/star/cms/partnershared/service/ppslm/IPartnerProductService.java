package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.product.CmsMainProduct;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 5/20/16.
 */
public interface IPartnerProductService {

    List<CmsMainProduct> getCurrentPartnerCMSProductByCategory(String categoryNodeName, Integer partnerId);

    CmsMainProduct getProductDetail(String prodId);

    Map<String,BigDecimal> getProductItemPriceMap(StoreApiChannels channel, String axAccountNumber, List<String> productIdList, List<String> productItemList);

}
