package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.product.CmsMainProduct;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jennylynsze on 5/20/16.
 */
@Service
public class DefaultPartnerProductService implements IPartnerProductService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerProductService.class);

    @Autowired
    private AxStarService axStarService;

    @Autowired
    private StarTemplatingFunctions starfn;

    @Override
    public List<CmsMainProduct> getCurrentPartnerCMSProductByCategory(String categoryNodeName, Integer partnerId) {
        try {
            //get tierIds of the partner
            String partnerQuery = "//element(*,mgnl:content)[@partnerId = '" + partnerId + "']";
            Iterable<Node> tierPartnerMappingIterable = JcrRepository.query(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), "mgnl:content", partnerQuery);
            Iterator<Node> tierPartnerMappingIterator = tierPartnerMappingIterable.iterator();
            List<CmsMainProduct> productList = new ArrayList<>();
            Map<String, CmsMainProduct> productMap = new HashMap<>();

            //add all the tiers
            while(tierPartnerMappingIterator.hasNext()) {
                Node tierPartnerMappingNode = tierPartnerMappingIterator.next();
                String tierId = tierPartnerMappingNode.getProperty("tierId").getString();

                //for each tier, get all the products under the mapping

                //check if the tier is still active
                Node tierNode = JcrRepository.getParentNode(JcrWorkspace.ProductTiers.getWorkspaceName(), "/" + tierId);

                boolean isExlusiveDeal = false;

                if(tierNode.hasProperty("exclusiveDeal")) {
                    isExlusiveDeal = "Yes".equals(tierNode.getProperty("exclusiveDeal").getString());
                }

                if("A".equals(tierNode.getProperty("status").getString())) {
                    //gora active naman
                    //get the products in the tier mappings
                    String query = "//element(*,mgnl:content)[@tierId = '" + tierId + "']";
                    Iterable<Node> tierMappingIterable = JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), "mgnl:content", query);
                    Iterator<Node> tierMappingIterator = tierMappingIterable.iterator();

                    while(tierMappingIterator.hasNext()) {
                        Node tierMappingNode = tierMappingIterator.next();
                        String productId = tierMappingNode.getProperty("productId").getString();
                        Node prodNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/partner-portal-slm/" + productId);
                        if(productMap.containsKey(prodNode.getName())) {
                            CmsMainProduct prod = productMap.get(prodNode.getName());

                            if(isExlusiveDeal) {
                                prod.setIsExclusiveDeal(true);
                            }

                            productMap.put(prodNode.getName(), prod);
                        }else {
                            CmsMainProduct prod = new CmsMainProduct();
                            prod.setId(prodNode.getName());
                            prod.setIsExclusiveDeal(isExlusiveDeal);
                            productMap.put(prodNode.getName(), prod);
                        }
                    }
                }

            }

            for(String key: productMap.keySet()) {
                productList.add(productMap.get(key));
            }

            return productList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public CmsMainProduct getProductDetail(String prodId) {
        Node productNode = starfn.getCMSProduct(StoreApiChannels.PARTNER_PORTAL_SLM.code, prodId);

        if(productNode != null) {
            CmsMainProduct mainProduct = new CmsMainProduct();
            mainProduct.setId(prodId);
            mainProduct.setProdLevel(PropertyUtil.getString(productNode, CMSProductProperties.ProductLevel.getPropertyName()));

            if(PartnerPortalConst.Exclusive.toString().equals(mainProduct.getProdLevel())) {
                mainProduct.setMinQty(PropertyUtil.getLong(productNode, CMSProductProperties.MinQty.getPropertyName()));
            }
            return mainProduct;
        }

        return new CmsMainProduct();
    }


    @Override
    public Map<String, BigDecimal> getProductItemPriceMap(StoreApiChannels channel, String axAccountNumber, List<String> productIdList, List<String> productItemList){
        Map<String, BigDecimal> productItemPriceMap = new HashMap<String, BigDecimal>();
        if(productIdList == null || productIdList.size() == 0 || productItemList == null || productItemList.size() == 0){
            return null;
        }
        int index = 0;
        Long[] lProductIdList = new Long[productIdList.size()];
        for(String prodId : productIdList){
            lProductIdList[index++] = Long.parseLong(prodId.trim());
        }

        AxStarServiceResult<String> apiResult = null;
        if(productItemList != null && productItemList.size() > 0){
            for(String prodId : productIdList){
                if(axAccountNumber == null || axAccountNumber.trim().length() == 0){
                    apiResult = axStarService.apiGetSingleItemPrice(channel, prodId);
                }else{
                    apiResult = axStarService.apiGetSingleItemPriceByCustomerId(channel, axAccountNumber, prodId);
                }
                if(apiResult != null){
                    if(!apiResult.isSuccess()){
                        popuplateProductItemPriceMapErrorLogs(apiResult);
                        return null;
                    }
                    productItemPriceMap = populateAxProductPriceItemMap(prodId, productIdList, productItemList, productItemPriceMap, apiResult.getData() != null && apiResult.getData() != null ? apiResult.getData() : null);
                }
            }

        }
        return productItemPriceMap;
    }

    private void popuplateProductItemPriceMapErrorLogs(AxStarServiceResult<String> apiResult) {
        if(apiResult.getError() != null){
            if(apiResult.getError().getMessage() != null && apiResult.getError().getMessage().trim().length() > 0) {
                log.error("popuplateProductItemPriceMap failed : "+apiResult.getError().getMessage());
            }
            log.error("popuplateProductItemPriceMap failed : "+apiResult.getError().getExceptionMessage());
        }
    }

    private Map<String, BigDecimal> populateAxProductPriceItemMap(String prodId, List<String> productIdList, List<String> productItemList, Map<String, BigDecimal> productItemPriceMap, String priceJsonString){
        if(productIdList == null || productIdList.size() == 0 || productItemList == null || productItemList.size() == 0){
            return productItemPriceMap;
        }
        if(priceJsonString == null || priceJsonString.trim().length() == 0){
            return productItemPriceMap;
        }
        JsonParser parser = new JsonParser();
        JsonArray jPrice = null;
        try{
            jPrice = parser.parse(priceJsonString.trim()).getAsJsonArray();
        }catch (Exception ex){
            return productItemPriceMap;
        }
        Iterator<JsonElement> iter = jPrice.iterator();
        while(iter.hasNext()){
            JsonElement ele = iter.next();
            if(ele != null && ele.isJsonObject()){
                JsonObject element = ele.getAsJsonObject();
                if(getProductItemPriceDetailsFromJsonObject(element, prodId, productItemList, productItemPriceMap)){
                    productItemList.remove(prodId+"-"+element.get("ItemId").getAsString());
                }
            }
        }
        return productItemPriceMap;
    }

    private boolean getProductItemPriceDetailsFromJsonObject(JsonObject jsonObj, String productId, List<String> productItemList, Map<String, BigDecimal> productItemPriceMap) {
        if(jsonObj.has("ItemId") && jsonObj.has("BasePrice") && jsonObj.has("CustomerContextualPrice")){
            String itemId = jsonObj.get("ItemId").getAsString();
            String key = productId + "-"+ itemId;
            if(!productItemList.contains(key)){
                return false;
            }
            Double ccpprice = jsonObj.get("CustomerContextualPrice").getAsDouble();
            double price = jsonObj.get("BasePrice").getAsDouble();
            productItemPriceMap.put(key, ccpprice != null && ccpprice.doubleValue() != 0.0 ? new BigDecimal(ccpprice) : new BigDecimal(price));
            return true;
        }
        return false;
    }
}
