package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPaDistributionMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTable;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxMarketDistribution;
import com.enovax.star.cms.commons.model.axstar.AxStarAddress;
import com.enovax.star.cms.commons.model.axstar.AxStarCustomer;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPADocMappingRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPaDistributionMappingRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerExtRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerRepository;
import com.enovax.star.cms.commons.service.axchannel.AxChannelCustomerService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.repository.ppmflg.IPartnerRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("PPMFLGIPartnerAXService")
public class DefaultPartnerAXService implements IPartnerAXService {

    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerAXService.class);

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;
    @Autowired
    private AxStarService axStarSrv;
    @Autowired
    private PPMFLGPartnerRepository paRepo;
    @Autowired
    private PPMFLGPartnerExtRepository paExtRepo;
    @Autowired
    private PPMFLGPaDistributionMappingRepository paDistRepo;
    @Autowired
    @Qualifier("PPMFLGIPartnerRepository")
    private IPartnerRepository paTierSrv;
    @Autowired
    private AxChannelCustomerService axCustSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerExtensionService")
    private IPartnerExtensionService paExtSrv;
    @Autowired
    private PPMFLGPADocMappingRepository paDocMapRepo;
    @Autowired
    private PPMFLGPaDistributionMappingRepository paDisMapRepo;

    @Override
    public AxStarServiceResult<AxStarCustomer> registerPartner(PPMFLGPartner partner, String tierId, String custGroup, String correspondenceAddr, String correspondencePostCode, String correspondenceCity) {
        AxStarCustomer c = populateAXCustomer(partner,tierId, custGroup, correspondenceAddr, correspondencePostCode, correspondenceCity);
        String uid = Thread.currentThread().getId() + " - " + System.currentTimeMillis();
        log.info("[PA-REG-"+uid+"] Partner AX Registration Request JSON : "+ JsonUtil.jsonify(c));
        System.out.println("[PA-REG-"+uid+"] Partner AX Registration Request JSON : "+ JsonUtil.jsonify(c));
        AxStarServiceResult<AxStarCustomer> result = axStarSrv.apiCustomerCreate(StoreApiChannels.PARTNER_PORTAL_MFLG, c);
        if(result.isSuccess()){
            log.info("[PA-REG-"+uid+"] Partner AX Registration Response JSON : "+ JsonUtil.jsonify(result));
            System.out.println("[PA-REG-"+uid+"] Partner AX Registration Response JSON : "+ JsonUtil.jsonify(result));
            AxStarServiceResult<AxStarCustomer> updateResult = this.updateAXCustomerBizAddr(result, partner);
            if(updateResult != null){
                if(updateResult != null && updateResult.isSuccess()){
                    log.info("[PA-REG-UPDATE-"+uid+"] Partner AX Registration Update Response JSON : "+ JsonUtil.jsonify(updateResult));
                    System.out.println("[PA-REG-UPDATE-"+uid+"] Partner AX Registration Update Response JSON : "+ JsonUtil.jsonify(updateResult));
                    return updateResult;
                }else{
                    log.info("[PA-REG-UPDATE-"+uid+"] Partner AX Registration Failed Update Response JSON : "+ JsonUtil.jsonify(updateResult));
                    System.out.println("[PA-REG-UPDATE-"+uid+"] Partner AX Registration Failed Update Response JSON : "+ JsonUtil.jsonify(updateResult));
                }
            }
        }else{
            log.error("[PA-REG-"+uid+"] Partner AX Registration Response JSON : "+ JsonUtil.jsonify(result));
            System.out.println("[PA-REG-"+uid+"] Partner AX Registration Response JSON : "+ JsonUtil.jsonify(result));
        }
        return result;
    }

    private AxStarCustomer populateAXCustomer(PPMFLGPartner partner, String tierId, String customerGroup, String correspondenceAddr, String correspondencePostCode, String correspondenceCity) {
        String orgName        = getNonEmptyStringValue(partner.getOrgName());
        String countryCode    = getNonEmptyStringValue(partner.getCountryCode());
        String mobileNo       = getNonEmptyStringValue(partner.getMobileNum());
        String email          = getNonEmptyStringValue(partner.getEmail());
        String website        = getNonEmptyStringValue(partner.getWebsite());

        AxStarCustomer c = new AxStarCustomer();
        c.setBlocked(false);
        c.setCustomerTaxInclusive(true);
        c.setCustomerTypeValue(sysParamSrv.getDefaultPartnerRegistrationCustomerType());//ax.crt.api.partner.registration.customer.type.value
        c.setName(orgName);
        c.setUrl(website);
        c.setPriceGroup(tierId);
        c.setEmail(email);
        c.setCellphone(mobileNo);
        c.setCustomerGroup(customerGroup);
        c.setLanguage("en-us");
        c.setCreatedDateTime(new Date(System.currentTimeMillis()));
        c.setCurrencyCode(sysParamSrv.getDefaultPartnerRegistrationCurrencyCode()); // ax.crt.api.partner.registration.currency.code

        AxStarAddress primaryAddr = new AxStarAddress();
        primaryAddr.setAddressTypeValue(sysParamSrv.getPartnerRegistrationInvoiceAddressType());
        primaryAddr.setPrimary(true);
        primaryAddr.setPrivate(false);
        primaryAddr.setDeactivate(false);
        primaryAddr.setCity(correspondenceCity);
        primaryAddr.setName(correspondenceAddr);
        primaryAddr.setFullAddress(correspondenceAddr);
        primaryAddr.setZipCode(correspondencePostCode);
        primaryAddr.setAttentionTo("");
        primaryAddr.setBuildingCompliment("");
        primaryAddr.setUrl("");
        primaryAddr.setStreet("");
        primaryAddr.setStreetNumber("");
        primaryAddr.setState("");
        primaryAddr.setStateName("");
        primaryAddr.setPhone("");
        primaryAddr.setCounty("");
        primaryAddr.setCountyName("");
        primaryAddr.setPhoneExt("");
        primaryAddr.setTaxGroup("");
        primaryAddr.setPostbox("");
        primaryAddr.setDistrictName("");
        primaryAddr.setTwoLetterISORegionName("");
        primaryAddr.setThreeLetterISORegionName(countryCode);
        primaryAddr.setExtensionProperties(new ArrayList<>());

        List<AxStarAddress> addrs = new ArrayList<AxStarAddress>();
        addrs.add(primaryAddr);
        c.setAddresses(addrs);

        return c;
    }

    private AxStarServiceResult<AxStarCustomer> updateAXCustomerBizAddr(AxStarServiceResult<AxStarCustomer> response,PPMFLGPartner partner){
        String countryCode    = getNonEmptyStringValue(partner.getCountryCode());
        String address        = getNonEmptyStringValue(partner.getAddress());
        String postalCode     = getNonEmptyStringValue(partner.getPostalCode());
        String city           = getNonEmptyStringValue(partner.getCity());

        AxStarAddress bizAddr = new AxStarAddress();
        bizAddr.setAddressTypeValue(sysParamSrv.getPartnerRegistrationBizAddrType());
        bizAddr.setPrimary(false);
        bizAddr.setPrivate(false);
        bizAddr.setDeactivate(false);
        bizAddr.setCity(city);
        bizAddr.setName(address);
        bizAddr.setFullAddress(address);
        bizAddr.setZipCode(postalCode);
        bizAddr.setAttentionTo("");
        bizAddr.setBuildingCompliment("");
        bizAddr.setUrl("");
        bizAddr.setStreet("");
        bizAddr.setStreetNumber("");
        bizAddr.setState("");
        bizAddr.setStateName("");
        bizAddr.setPhone("");
        bizAddr.setCounty("");
        bizAddr.setCountyName("");
        bizAddr.setPhoneExt("");
        bizAddr.setTaxGroup("");
        bizAddr.setPostbox("");
        bizAddr.setDistrictName("");
        bizAddr.setTwoLetterISORegionName("");
        bizAddr.setThreeLetterISORegionName(countryCode);
        bizAddr.setExtensionProperties(new ArrayList<>());

        AxStarCustomer axCustomer = response.getData();

        AxStarCustomer clonedCustomer = null;
        AxStarServiceResult<AxStarCustomer> updatedAxCustomer = null;
        try {
            clonedCustomer = (AxStarCustomer) BeanUtils.cloneBean(axCustomer);
            clonedCustomer.getAddresses().add(bizAddr);
            updatedAxCustomer = axStarSrv.apiCustomerUpdate(StoreApiChannels.PARTNER_PORTAL_MFLG, clonedCustomer);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return updatedAxCustomer;
    }

    private String getNonEmptyStringValue(String v) {
        if(v != null && v.trim().length() > 0){
            return v.trim();
        }
        return "";
    }

    private String getFormatedDate(Date licenseExpDt) {
        if(licenseExpDt == null){
            return "";
        }
        return NvxDateUtils.formatDateForDisplay(licenseExpDt, false);
    }

    @Transactional(rollbackFor = Exception.class)
    public void syncPartnerProfile(Integer partnerId) {
        if(partnerId == null || partnerId.intValue() == 0){
            return;
        }
        PPMFLGPartner pa = paRepo.findById(partnerId);
        if(pa == null){
            return;
        }
        if(!(PartnerStatus.Active.code.equals(pa.getStatus()) || PartnerStatus.Suspended.code.equals(pa.getStatus()))){
            return;
        }

        String axAccountNumber = pa.getAxAccountNumber();
        if(axAccountNumber == null || axAccountNumber.trim().length() == 0){
            throw new RuntimeException("AX Account Number is Empty");
        }
        AxStarServiceResult result = axStarSrv.apiCustomerGet(StoreApiChannels.PARTNER_PORTAL_MFLG, axAccountNumber);
        if(!result.isSuccess()){
            log.error("Sync partner profile failed; [ERROR]:"+String.valueOf(result.getError())+"[DATA]:"+result.getRawData());
        }else{
            AxStarCustomer customer = (AxStarCustomer) result.getData();
            try{
                updatePartnerProfileByAxStarCustomer(pa, customer);
            }catch (Exception ex){
                log.error("Update partner profile failed; [ERROR]:"+ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    private void updatePartnerProfileByAxStarCustomer(PPMFLGPartner pa, AxStarCustomer customer) throws Exception {
        if(customer == null || pa == null || pa.getId() == null){
            return;
        }
        String orgName      = customer.getName();
        String website      = customer.getUrl();
        String priceGroupId = customer.getPriceGroup();
        String email        = customer.getEmail();
        String mobileNo     = customer.getCellphone();
        String orgId        = customer.getOrganizationId();
        String customerGrp  = customer.getCustomerGroup();

        pa.setOrgName(orgName);
        pa.setWebsite(website);
        pa.setEmail(email);
        pa.setTierId(priceGroupId);
        pa.setMobileNum(mobileNo);
        pa.setUen(orgId);
        paRepo.save(pa);

        List<PPMFLGPartnerExt> exts = pa.getPartnerExtList();
        for(PPMFLGPartnerExt i : exts){
            if("customerGroup".equals(i.getAttrName())){
                i.setAttrValue(customerGrp);
                paExtRepo.save(i);
                break;
            }
        }

        int bizAddrType = sysParamSrv.getPartnerRegistrationBizAddrType();
        int invoiceAddrType = sysParamSrv.getPartnerRegistrationInvoiceAddressType();

        List<AxStarAddress> addresses = customer.getAddresses();
        if(addresses != null && addresses.size() > 0){
            for(AxStarAddress a : addresses){
                if(a == null){
                    continue;
                }
                if(a.getAddressTypeValue() == null){
                    continue;
                }
                if(a.getAddressTypeValue().intValue() == bizAddrType){
                    pa.setAddress(a.getName());
                    pa.setCity(a.getCity());
                    pa.setPostalCode(a.getZipCode());
                }
                else if(a.getAddressTypeValue().intValue() == invoiceAddrType)
                {
                    pa.setCountryCode(a.getThreeLetterISORegionName());
                    boolean addr = false, city = false, postcode = false;
                    for(PPMFLGPartnerExt i : exts){
                        if("correspondenceAddress".equals(i.getAttrName())){
                            i.setAttrValue(a.getName());
                            paExtRepo.save(i);
                            addr = true;
                        }
                        if("correspondenceCity".equals(i.getAttrName())){
                            i.setAttrValue(a.getCity());
                            paExtRepo.save(i);
                            city = true;
                        }
                        if("correspondencePostalCode".equals(i.getAttrName())){
                            i.setAttrValue(a.getZipCode());
                            paExtRepo.save(i);
                            postcode = true;
                        }
                        if(addr && city && postcode){
                            break;
                        }
                    }
                }
            }
        }

        paRepo.save(pa);

        ApiResult<AxCustomerTable> custApi = axCustSrv.apiGetCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, pa.getAxAccountNumber());
        if(custApi == null){
            return;
        }
        if(!custApi.isSuccess()){
            log.error("updatePartnerProfileByAxStarCustomer for partner "+pa.getId()+"["+pa.getAxAccountNumber()+"] is failed : "+custApi.getErrorCode()+" | "+custApi.getMessage());
            return;
        }
        AxCustomerTable i = custApi.getData();
        if(i == null){
            return;
        }

        if(i.getAccountMgrCMS() != null && i.getAccountMgrCMS().trim().length() > 0){
            pa.setAccountManagerId(i.getAccountMgrCMS());
        }
        if(i.getContactPersonStr() != null && i.getContactPersonStr().trim().length() > 0){
            pa.setContactPerson(i.getContactPersonStr());
        }
        if(i.getDesignation() != null && i.getDesignation().trim().length() > 0){
            pa.setContactDesignation(i.getDesignation());
        }
        if(i.getFax() != null && i.getFax().trim().length() > 0){
            pa.setFaxNum(i.getFax());
        }
        if(i.getCountriesInterestedIn() != null && i.getCountriesInterestedIn().trim().length() > 0){
            pa.setMainDestinations(i.getCountriesInterestedIn());
        }
        if(i.getUenNumber() != null && i.getUenNumber().trim().length() > 0){
            pa.setUen(i.getUenNumber());
        }
        if(i.getTaLicenseNumber() != null && i.getTaLicenseNumber().trim().length() > 0){
            pa.setLicenseNum(i.getTaLicenseNumber());
        }
        if(i.getTaExpiryDate() != null){
            pa.setLicenseExpDate(NvxDateUtils.formatDate(i.getTaExpiryDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        if(i.getDailyTransCapValue() != null){
            pa.setDailyTransCap(new BigDecimal(i.getDailyTransCapValue()));
        }
        if(i.getLineOfBusiness() != null && i.getLineOfBusiness().trim().length() > 0){
            pa.setOrgTypeCode(i.getLineOfBusiness());
        }
        if(i.getEnablePartnerPortal() != null && i.getEnablePartnerPortal().intValue() == 1){
            if(PartnerStatus.Active.code.equals(pa.getStatus()) || PartnerStatus.Suspended.code.equals(pa.getStatus())){
                pa.setStatus(PartnerStatus.Active.code);
            }
        }else{
            pa.setStatus(PartnerStatus.Suspended.code);
        }
        if(i.getRevalidationFee() != null){
            pa.setRevalFeeItemId(i.getRevalidationFee().toString());
        }
        if(i.getRevalidationPeriod() != null){
            pa.setRevalPeriodMonths(i.getRevalidationPeriod());
        }

        if(i.getCountryCode() != null && i.getCountryCode().trim().length() > 0){
            pa.setCountryCode(i.getCountryCode());
        }
        this.paRepo.save(pa);
        
        List<PPMFLGPartnerExt> capacityGroupIds = paExtRepo.findCapacityGroupIdByPartnerId(pa.getId());
        if(capacityGroupIds != null && i.getCapacityGroupId() != null && i.getCapacityGroupId().trim().length() > 0){
            for(PPMFLGPartnerExt e : capacityGroupIds){
                if(e != null && e.getPartnerId() != null && e.getPartnerId().equals(pa.getId()) && e.getAttrName() != null && "capacityGroupId".equalsIgnoreCase(e.getAttrName())){
                    e.setAttrValue(i.getCapacityGroupId());
                }
            }
        }

        List<PPMFLGPaDistributionMapping> paDistMapList = paDistRepo.findByPartnerId(pa.getId());
        if(paDistMapList != null && paDistMapList.size() > 0) {
            for (PPMFLGPaDistributionMapping t : paDistMapList) {
                t.setFound(false);
            }
        }
        List<AxMarketDistribution> dislist = i.getMarketDistributions();
        if(dislist != null && dislist.size() > 0){
            for(AxMarketDistribution amd : dislist){
                boolean found = false;
                if(paDistMapList != null && paDistMapList.size() > 0){
                    for(PPMFLGPaDistributionMapping pdm : paDistMapList){
                        if(pdm.getAdminId() != null && amd.getSalesManager() != null && pdm.getAdminId().equals(amd.getSalesManager()) &&
                            pdm.getCountryId() != null && amd.getCountryRegion() != null && pdm.getCountryId().equals(amd.getCountryRegion()))
                        {
                            pdm.setPercentage(Integer.parseInt(amd.getPercent().trim()));
                            pdm.setFound(true);
                            found = true;
                            break;
                        }
                    }
                }
                if(!found){
                    PPMFLGPaDistributionMapping pdm = new PPMFLGPaDistributionMapping();
                    pdm.setPercentage(Integer.parseInt(amd.getPercent().trim()));
                    pdm.setPartnerId(pa.getId());
                    pdm.setPartner(pa);
                    pdm.setAdminId(amd.getSalesManager());
                    pdm.setCountryId(amd.getCountryRegion());
                    paDistRepo.save(pdm);
                }
            }
        }
        if(paDistMapList != null && paDistMapList.size() > 0){
            for (PPMFLGPaDistributionMapping t : paDistMapList) {
                if(!t.isFound()){
                    paDistRepo.delete(t);
                }
            }
        }
    }

    @Override
    public void syncSelfUpdatedPartnerProfileToAX(Integer id) throws Exception {
        if(id == null || id.intValue() == 0){
            throw  new BizValidationException("Invalid partner details found");
        }
        PPMFLGPartner pa = paRepo.findById(id);
        if(pa == null){
            throw  new BizValidationException("Invalid partner details found");
        }
        AxStarServiceResult<AxStarCustomer> cust = axStarSrv.apiCustomerGet(StoreApiChannels.PARTNER_PORTAL_MFLG, pa.getAxAccountNumber());
        if(cust == null){
            throw  new BizValidationException("Invalid partner details found");
        }
        if(!cust.isSuccess()){
            throw  new BizValidationException("Retrieving partner details from AX is failed");
        }
        String mobileNo       = getNonEmptyStringValue(pa.getMobileNum());
        String website        = getNonEmptyStringValue(pa.getWebsite());


        int invoiceAddrType = sysParamSrv.getPartnerRegistrationInvoiceAddressType();
        int bizAddrType = sysParamSrv.getPartnerRegistrationBizAddrType();

        List<PPMFLGPartnerExt> exts = pa.getPartnerExtList();
        String capacityGroupId = null;
        if(exts != null && exts.size() > 0){
            for(PPMFLGPartnerExt e : exts){
                if("capacityGroupId".equalsIgnoreCase(e.getAttrName())){
                    capacityGroupId = e.getAttrValue();
                }
            }
        }

        boolean allowOnAccount = sysParamSrv.getPartnerRegistrationAllowOnAccount();
        String documnetTypeId = sysParamSrv.getPartnerRegistrationAxDocumentTypeId();

        List<PPMFLGPADocMapping> docMap = paDocMapRepo.findByPartnerId(pa.getId());
        List<PPMFLGPaDistributionMapping> marketDistributionList = paDisMapRepo.findByPartnerId(pa.getId());

        AxCustomerTable customerTable = paExtSrv.popuplateCustoemrTable(pa, PartnerStatus.Active.code.equalsIgnoreCase(pa.getStatus()), allowOnAccount, docMap, marketDistributionList, documnetTypeId, capacityGroupId, pa.getOrgTypeCode());
        if(customerTable == null){
            throw  new BizValidationException("Populating customer details failed.");
        }

        AxStarCustomer c = cust.getData();
        c.setUrl(website);
        c.setCellphone(mobileNo);
        List<AxStarAddress> addrs = c.getAddresses();
        if(addrs != null && addrs.size() > 0){
            for(AxStarAddress addr : addrs){
                //
                if(addr.getAddressTypeValue() != null && addr.getAddressTypeValue().intValue() == invoiceAddrType){
                    if(exts != null && exts.size() > 0){
                        for(PPMFLGPartnerExt e : exts){
                            if("correspondencePostalCode".equalsIgnoreCase(e.getAttrName())){
                                addr.setZipCode(getNonEmptyStringValue(e.getAttrValue()));
                            }
                            if("correspondenceAddress".equalsIgnoreCase(e.getAttrName())){
                                addr.setName(getNonEmptyStringValue(e.getAttrValue()));
                            }
                            if("correspondenceCity".equalsIgnoreCase(e.getAttrName())){
                                addr.setCity(getNonEmptyStringValue(e.getAttrValue()));
                            }
                        }
                    }
                }
                //
                if(addr.getAddressTypeValue() != null && addr.getAddressTypeValue().intValue() == bizAddrType){
                    addr.setCity(getNonEmptyStringValue(pa.getCity()));
                    addr.setFullAddress(getNonEmptyStringValue(pa.getAddress()));
                    addr.setZipCode(getNonEmptyStringValue(pa.getPostalCode()));
                }
            }
        }


        AxStarServiceResult<AxStarCustomer> updatedCust = axStarSrv.apiCustomerUpdate(StoreApiChannels.PARTNER_PORTAL_MFLG, c);
        if(updatedCust == null){
            throw  new BizValidationException("Update partner details is failed");
        }
        if(!updatedCust.isSuccess()){
            throw  new BizValidationException("Sync partner details to AX is failed");
        }

        ApiResult<Boolean> apiUpdateResult = axCustSrv.apiUpdateCreatedCustomer(StoreApiChannels.PARTNER_PORTAL_MFLG, customerTable, PartnerStatus.Active.code.equalsIgnoreCase(pa.getStatus()));
        if(apiUpdateResult == null){
            throw  new BizValidationException("Update partner details is failed");
        }
        if(!apiUpdateResult.isSuccess()){
            throw  new BizValidationException("Sync partner details to AX is failed");
        }
    }
}
