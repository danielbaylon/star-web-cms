package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.payment.tm.constant.*;
import com.enovax.payment.tm.model.TelemoneyResponse;
import com.enovax.payment.tm.model.TmFraudCheckPackage;
import com.enovax.payment.tm.model.TmMerchantSignatureConfig;
import com.enovax.payment.tm.model.TmPaymentRedirectInput;
import com.enovax.payment.tm.service.TmServiceProvider;
import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.constant.api.TransactionStage;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.TicketStatus;
import com.enovax.star.cms.commons.constant.ppmflg.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.jcrrepository.system.ITmParamRepository;
import com.enovax.star.cms.commons.mgnl.definition.AXProductCrossSellProperties;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxInsertOnlineReference;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartDisplay;
import com.enovax.star.cms.commons.model.partner.ppmflg.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerReceiptDisplay;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.tracking.Trackd;
import com.enovax.star.cms.commons.util.*;
import com.enovax.star.cms.partnershared.constant.ppmflg.ProductLevels;
import com.enovax.star.cms.partnershared.repository.ppmflg.IPartnerBookingRepository;
import com.enovax.star.cms.partnershared.repository.ppmflg.cart.IPartnerCartManager;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Service("PPMFLGIPartnerBookingService")
public class DefaultPartnerBookingService implements IPartnerBookingService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;
    @Autowired
    private AxStarService axStarService;
    @Autowired
    @Qualifier("PPMFLGIPartnerBookingRepository")
    private IPartnerBookingRepository bookingRepository;
    @Autowired
    @Qualifier("PPMFLGIPartnerCartManager")
    private IPartnerCartManager cartManager;
    @Autowired
    private PPMFLGInventoryTransactionRepository transRepo;
    @Autowired
    private PPMFLGInventoryTransactionItemRepository itemRepo;
    @Autowired
    private PPMFLGAxSalesOrderRepository axSalesOrderRepo;
    @Autowired
    private PPMFLGAxCheckoutCartRepository axCheckoutCartRepo;
    @Autowired
    private PPMFLGAxSalesOrderLineNumberQuantityRepository salesOrderLineNumberQuantityRepo;
    @Autowired
    private StarTemplatingFunctions starfn;
    @Autowired
    private IProductService productService;
    @Autowired
    @Qualifier("PPMFLGIPartnerPurchaseTransactionService")
    private IPartnerPurchaseTransactionService purchaseTransactionService;
    @Autowired
    private ITmParamRepository tmParamRepository;
    @Autowired
    private TmServiceProvider tmServiceProvider;
    @Autowired
    private ISystemParamRepository sysParamRepo;
    @Autowired
    @Qualifier("PPMFLGIGSTRateService")
    private IGSTRateService gstSrv;

    public void saveTransactionInSession(StoreApiChannels channel, HttpSession session, InventoryTransactionVM txn) {
        session.setAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN, txn);
    }
    public InventoryTransactionVM getTransactionInSession(StoreApiChannels channel, HttpSession session) {
        return (InventoryTransactionVM) session.getAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }
    public void clearTransactionInSession(StoreApiChannels channel, HttpSession session) {
        session.removeAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }


    //TODO this getting of message should be done in the js would be better for easy change later
//    @Override
//    public String getMessageFromCheckoutResult(ApiResult<PPMFLGInventoryTransaction> result, boolean isOnlinePurchase) {
//        String msg = "";
//        switch (result.getApiErrorCode()) {
//            case CheckoutResultSuccess:
//                return "success";
//            case CheckoutResultCustomerDetailsFailed:
//                msg = "Customer details are invalid. Please try again.";
//                break;
//            case CheckoutResultFailedReserveTicket:
//                if(isOnlinePurchase){
//                    msg = "We are unable to complete your reservation. Please clear your cart and try again, or contact our guest experience officers.";
//                }else{
//                    msg = "Ticket reservation is not successful , please try again later.";
//                }
//                break;
//            case CheckoutResultFailedInsufficientTickets:
//                if(isOnlinePurchase){
//                    msg = "There are insufficient tickets left for your selected booking. Please clear your cart and try again, or contact our guest experience officers.";
//                }else{
//                    msg = "There are insufficient tickets left";
//                }
//                break;
//            case CheckoutResultFailedPurchaseMaxAmountPerTxn:
//                if(isOnlinePurchase){
//                    msg = "Your current transaction has exceeded the maximum dollar value limit per transaction. Please reduce the total value in your cart.";
//                }else{
//                    msg = "Current transaction has exceeded the maximum dollar value limit per transaction.";
//                }
//                break;
//            case CheckoutResultFailedPurchaseMaxDailyTxnLimit:
//                if(isOnlinePurchase){
//                    msg = "The maximum dollar value limit per day has been reached. Please reduce the total value in your cart or try again tomorrow.";
//                }else{
//                    msg = "The maximum dollar value limit per day has been reached. Please try again tomorrow.";
//                }
//                break;
//            case CheckoutResultFailedPurchaseMinQtyPerTxn:
//                if(isOnlinePurchase){
//                    msg = "Your current transaction does not meet the minimum quantity of items per transaction. Please add more items in your cart.";
//                }else{
//                    msg = "This transaction does not meet the minimum quantity of items per transaction.";
//                }
//                break;
//            case CheckoutResultFailedPurchaseMinQtySetting:
//                if(isOnlinePurchase){
//                    msg = "Some items does not meet the minimum quantity, please check your cart and add it back.";
//                }else{
//                    msg = "Some items does not meet the minimum quantity.";
//                }
//                break;
//            case CheckoutResultFailedConfirmSale:
//                if(isOnlinePurchase){
//                    msg = "Can not confirm sales, please contact admin.";
//                }else{
//                    msg = "Can not confirm sales, please contact admin.";
//                }
//                break;
//            case CheckoutResultFailedMixEventInOffline:
//                if(isOnlinePurchase){
//                    msg = "Event item can not be payed offline.";
//                }else{
//                    msg = "Event item can not be payed offline.";
//                }
//                break;
//            case CheckoutResultFailedGetBookingFee:
//            case CheckoutResultFailedReceiptSequence:
//            case CheckoutResultFailed:
//                if(isOnlinePurchase){
//                    msg = "We are unable to continue your checkout. Please clear your cart and try again, or contact our guest experience officers.";
//                }else{
//                    msg = "We are unable to continue this checkout.";
//                }
//                break;
//            case CheckoutResultFailedItemExpiredInCatalog:
//                if(isOnlinePurchase){
//                    msg = "Some of the items in this transaction is expired. Please clear your cart and try again, or contact our guest experience officers.";
//                }else{
//                    msg = "Some of the items in this transaction is expired.";
//                }
//                break;
//            case CheckoutResultFailedItemDifferentValidityCfg:
////                DefaultTextProvider textProvider = new DefaultTextProvider();
////                if(isOnlinePurchase){
////                    msg = textProvider.getText("booking.addToCart.err.DifferentExiprationItem");
////                }else{
////                    msg = textProvider.getText("offline.different.item.expiration.cfg");
////                }
////                textProvider = null;
//                break;
//        }
//        return msg;
//    }

    @Override
    public ApiResult<PartnerFunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId, String customerId) {

        final List<FunCartItem> cartItemsToAdd = cartToAdd.getItems();

        //Validation: Empty cart
        if (cartItemsToAdd.isEmpty()) {
            return new ApiResult<>(ApiErrorCodes.AddCartResultValidationFailed);
        }

        //Get the existing item
        FunCart savedCart = cartManager.getCart(channel, sessionId);
        FunCart topupCart = new FunCart(); //for topup
        topupCart.setAxCustomerId(customerId);
        topupCart.setChannelCode(channel.code);
        topupCart.setItems(new ArrayList<>());
        List<FunCartItem> topupToAddList = topupCart.getItems();

        //new Cart
        if (savedCart == null) {
            savedCart = new FunCart();
            savedCart.setCartId(sessionId);
            savedCart.setAxCustomerId(customerId);
            savedCart.setChannelCode(channel.code);
        }

        final Map<String, FunCartItem> savedItemsMap = new HashMap<>();
        final Map<String, List<FunCartItem>> savedTopupItemsMap = new HashMap<>(); //TODO lol what if topups were event products
        FunCartItem savedEventItem = null;

        final List<String> productListingId = savedCart.getProductListingIdList();
        final Map<String, FunCartItem> axLineCommentMap = savedCart.getAxLineCommentMap();
        final List<FunCartItem> savedItems = savedCart.getItems();
        boolean hasGeneralTicket = false;

        for (FunCartItem savedItem : savedItems) {
            if (savedItem.isTopup()) {
                //get all the topups from the cart
                List<FunCartItem> topups = savedTopupItemsMap.get(savedItem.getParentCmsProductId() + savedItem.getParentListingId());
                if(topups == null) {
                    topups = new ArrayList<>();
                }
                topups.add(savedItem);
                savedTopupItemsMap.put(savedItem.getParentCmsProductId() + savedItem.getParentListingId(), topups);
            } else {
                savedItemsMap.put(savedItem.generateCartItemId(), savedItem);
            }

            if(StringUtils.isNotEmpty(savedItem.getEventGroupId())) {
                savedEventItem = savedItem; //remember the savedEventItem
            }else {
                hasGeneralTicket = true;
            }
        }


        List<String> productIds = new ArrayList<>();
        Map<String, FunCartItem> funCartItemMap = new HashMap<>();

        // if an event product is added, need to check if there's already an existing event product with a different event date
        // if a general product is added, make sure all are general items
        // if an event is added, make suare all are event items with the same event date

        String cmsProductId = "";

        for (FunCartItem itemToAdd : cartToAdd.getItems()) {

            //there's already an existing event, can't add event and non event on the same
            if(savedEventItem != null && StringUtils.isEmpty(itemToAdd.getEventGroupId())) {
                return new ApiResult<>(ApiErrorCodes.AddCartResultDifferentEventSchedules);
            }

            //if the item is an event, validate events
            if(StringUtils.isNotEmpty(itemToAdd.getEventGroupId())) {
                //compare if same event
                if(savedEventItem != null) {
                    if(!savedEventItem.generateEventSched().equals(itemToAdd.generateEventSched())) {
                        return new ApiResult<>(ApiErrorCodes.AddCartResultDifferentEventSchedules);
                    }
                    //check if there's already a general ticket
                }else if(hasGeneralTicket) {
                    return new ApiResult<>(ApiErrorCodes.AddCartResultDifferentEventSchedules);
                }else {
                    savedEventItem = itemToAdd;
                }
            }

            productIds.add(itemToAdd.getListingId()); //store the listingId to get the data needed
            funCartItemMap.put(itemToAdd.getListingId(), itemToAdd); //store the funcart mapping with the listingId also

            cmsProductId = itemToAdd.getCmsProductId();
        }

        log.info("Getting extension properties for related items to be added to cart...");
        final Map<String, ProductExtViewModel> prodExtPropsMap = new HashMap<>();
        try {

            final ApiResult<List<ProductExtViewModel>> prodExtResult = productService.getDataForProducts(channel, productIds);
            if (prodExtResult.isSuccess()) {
                for (ProductExtViewModel pevm : prodExtResult.getData()) {
                    prodExtPropsMap.put(pevm.getItemId(), pevm);
                }
            } else {
                log.error("Error encountered after calling product extension properties retrieval... " + prodExtResult.getMessage());
                return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
            }
        } catch (AxChannelException e) {
            log.error("Error encountered while trying to get product extension properties.", e);
            return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
        }


        //TODO Check If item meets min qty.
        try {
            List<AxRetailCartTicket> cartTickets =  new ArrayList<>();

            if(productIds.size() > 0) {
                log.info("Getting the data for products: " + StringUtils.join(productIds, ",") + "...");
            }

            //check if capacity

            for(String itemId: prodExtPropsMap.keySet()) {
                ProductExtViewModel extViewModel = prodExtPropsMap.get(itemId);

                AxRetailCartTicket ct = new AxRetailCartTicket();
                FunCartItem funCartItem = funCartItemMap.get(extViewModel.getProductId()); //now get the funcart itme loh
                FunCartItem savedCartItem = savedItemsMap.get(funCartItem.generateCartItemId()); //check if there's an existing product, so later we can just add the quantity


                //default
                ct.setLineId("");
                ct.setAccountNum(customerId);
                ct.setTransactionId("");
                ct.setItemId(extViewModel.getItemId());
                ct.setListingId(0L); //no need for the listing id, just the item id will do

                int savedQty = savedCartItem ==null?0: savedItemsMap.get(funCartItem.generateCartItemId()).getQty();

                ct.setQty(funCartItem.getQty() + savedQty);

                //if event check capacity
                if(extViewModel.isEvent()) {
                    //check the capacity
                    ct.setEventGroupId(extViewModel.getEventGroupId());

                    //if open dated, doest have any date or anything.
                    if (StringUtils.isNotEmpty(extViewModel.getDefaultEventLineId())) {
                        ct.setEventLineId(extViewModel.getDefaultEventLineId());
                    }else {
                        ct.setEventLineId(funCartItem.getEventLineId());
                    }

                    try {
                        if(funCartItem.getSelectedEventDate() != null) {
                            ct.setEventDate(NvxDateUtils.parseDate(funCartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
                        }
                    } catch (ParseException e) {
                        ct.setEventDate(null);  //thou this shoudlnt happen la
                        log.error(e.getMessage(), e);
                    }

                    ct.setUpdateCapacity(0);
                }
                cartTickets.add(ct);
            }


            //TODO removed topps if not enough

            if(cartTickets.size() > 0) {
                final ApiResult<List<AxRetailTicketRecord>> result =
                        axChannelTransactionService.apiCartValidateQuantity(channel, null, cartTickets);
                if (!result.isSuccess()) {
                    log.error("Failed to perform validate quantity at AX Retail service.");
                    return new ApiResult<>(ApiErrorCodes.InsufficientQty);
                }
            }

        } catch (AxChannelException e) {
            e.printStackTrace();
        }

        //TODO validate min qty
        Node cmsProduct = starfn.getCMSProduct(channel.code, cmsProductId);
        if(ProductLevels.Exclusive.name().equals(PropertyUtil.getString(cmsProduct, CMSProductProperties.ProductLevel.getPropertyName()))) {
            //get the min qty
            Long minQty = PropertyUtil.getLong(cmsProduct, CMSProductProperties.MinQty.getPropertyName());
            if(minQty != null) {
                boolean minQtyRes = this. isItemMeetMinQtySetting(savedItems, cartToAdd.getItems(), minQty);
                if (!minQtyRes) {
                    return new ApiResult<>(false, ApiErrorCodes.AddCartResultFailedItemMinQty.code,
                            ApiErrorCodes.AddCartResultFailedItemMinQty.message.replace("{0}", minQty.toString()), null);
                }
            }

        }

        //CmsMainProduct prod = partnerProductService.getProductDetail();

        //TODO validate expiry dates

        //TODO validate capacity

        //TODO remove topups if needed

//            PurchaseServiceImpl,
//            if event, validate quantity
//            if absoluteCapacityId, validate quantity
//            if printing = 3, validate quantity
//            else no need

        //TODO There is an issue here. Cart is stored in session, which means if you change this quantity of updated item, it'll get updated instantly.
        //TODO Implement fix for this issue, it is critical for error handling.
        final List<FunCartItem> updatedItems = new ArrayList<>();
        final Map<String, Integer> addedQuantities = new HashMap<>();
        for (FunCartItem itemToAdd : cartToAdd.getItems()) {

            ProductExtViewModel extViewModel= prodExtPropsMap.get(itemToAdd.getProductCode());
            // do the validation
            final boolean isEvent = extViewModel.isEvent();
            final String cartItemId = itemToAdd.generateCartItemId();

            FunCartItem theItem = savedItemsMap.get(cartItemId);

            if (theItem == null) {

                theItem = new FunCartItem();
                theItem.setCmsProductId(itemToAdd.getCmsProductId());
                theItem.setCmsProductName(itemToAdd.getCmsProductName());
                theItem.setListingId(itemToAdd.getListingId());
                theItem.setName(itemToAdd.getName());
                theItem.setPrice(itemToAdd.getPrice());
                theItem.setProductCode(itemToAdd.getProductCode());
                theItem.setQty(itemToAdd.getQty());
                theItem.setType(itemToAdd.getType());
                theItem.setPrinting(extViewModel.getPrinting());
                theItem.setOpenDate(extViewModel.isOpenDate());

                if (isEvent) {
                    theItem.setEventGroupId(itemToAdd.getEventGroupId());

                    if(StringUtils.isNotEmpty(extViewModel.getDefaultEventLineId())) {
                        theItem.setEventLineId(extViewModel.getDefaultEventLineId());
                    }else {
                        theItem.setEventLineId(itemToAdd.getEventLineId());
                    }

                    if(StringUtils.isNotEmpty(itemToAdd.getSelectedEventDate())) {
                        theItem.setSelectedEventDate(itemToAdd.getSelectedEventDate());
                        theItem.setEventSessionName(itemToAdd.getEventSessionName());
                    }
                }

                theItem.setCartItemId(cartItemId);

                savedItems.add(theItem);
                savedItemsMap.put(theItem.getCartItemId(), theItem);
                axLineCommentMap.put(theItem.getAxLineCommentId(), theItem); //save the ax line comment
                productListingId.add(theItem.getListingId());


            } else {
                theItem.setQty(theItem.getQty() + itemToAdd.getQty());
                String key = theItem.getCartItemId(); //the KEY now for topup is the cartItemId of the parent
                //check if has topups, if so, then would need to add topup as well
                //for b2b, the no. of topups should be same with the no. of items, so that's why this is diff from b2c
                if(savedTopupItemsMap.containsKey(key)) {
                    List<FunCartItem> savedTopupItems = savedTopupItemsMap.get(key);
                    for(FunCartItem stItems: savedTopupItems) {
                        stItems.setQty(theItem.getQty()); //TODO evaluate if needed to put here
                        topupToAddList.add(stItems);
//                        FunCartItem topupCartItem = new FunCartItem();
//                        topupCartItem.setCartItemId(stItems.generateCartItemId());
//                        topupCartItem.setCmsProductId(stItems.getCmsProductId());
//                        topupCartItem.setCmsProductName(stItems.getCmsProductName());
//                        topupCartItem.setListingId(stItems.getListingId());
//                        topupCartItem.setIsTopup(stItems.isTopup());
//                        topupCartItem.setType(stItems.getType());
//                        topupCartItem.setParentCmsProductId(stItems.getParentCmsProductId());
//                        topupCartItem.setParentListingId(stItems.getParentListingId());
//                        topupCartItem.setProductCode(stItems.getProductCode());
//                        topupCartItem.setPrice(stItems.getPrice());
//                        topupCartItem.setName(stItems.getName());
//                        topupCartItem.setSelectedEventDate(stItems.getSelectedEventDate());
//                        topupCartItem.setEventGroupId(stItems.getEventGroupId());
//                        topupCartItem.setEventLineId(stItems.getEventLineId());
//                        topupCartItem.setEventSessionName(stItems.getEventSessionName());
//                        topupToAddList.add(topupCartItem);
                    }
                }
            }

            addedQuantities.put(cartItemId, itemToAdd.getQty());
            updatedItems.add(theItem);
        }

        final AxStarCart axCart = savedCart.getAxCart();
        String axCartId = "";

        if (axCart != null) {
            axCartId = axCart.getId();
        }

        //TODO evaluate the purpose of below
//        final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
//        final Map<String, Integer> cartLineQtyMap = new HashMap<>();
//
//        if (axCart != null) {
//            axCartId = axCart.getId();
//
//            for (AxStarCartLine cl : axCart.getCartLines()) {
//                final String productId = cl.getProductId().toString();
//                List<AxStarCartLine> lineList = cartLineMap.get(productId);
//                Integer currQty = cartLineQtyMap.get(productId);
//                if (lineList == null) {
//                    lineList = new ArrayList<>();
//                    cartLineMap.put(productId, lineList);
//                }
//                cartLineQtyMap.put(productId, (currQty == null ? 0 : currQty) + cl.getQuantity());
//                lineList.add(cl);
//            }
//        }

        final AxStarInputAddCart axc = new AxStarInputAddCart();
        final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

        //Handle what if there are 2 updated items with the same listing ID
        final Map<String, Integer> updatedItemQtyMap = new HashMap<>();
        final Map<String, FunCartItem> updatedFunCartItemMap = new HashMap<>();

        //updatedItems = cartToAdd Items, can assume that the ax product listing id in one cms product is always unique. so add to cart is only called once always.
        for (FunCartItem updatedItem : updatedItems) {
            final String productId = updatedItem.getListingId();
            Integer updatedQty = updatedItemQtyMap.get(productId);
            if (updatedQty == null) { updatedQty = 0; }
            Integer addedQty = addedQuantities.get(updatedItem.getCartItemId());
            updatedQty += (addedQty == null ? 0 : addedQty);
            updatedItemQtyMap.put(productId, updatedQty);
            updatedFunCartItemMap.put(productId, updatedItem); //put the item here
        }


        //TODO When adding to cart, how is the event date different and same product added? HANDLE
        for (Map.Entry<String, Integer> entry : updatedItemQtyMap.entrySet()) {
            final String productId = entry.getKey();
//            Integer currAxQty = cartLineQtyMap.get(productId);
//            if (currAxQty == null) { currAxQty = 0; }
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();
            axcItem.setProductId(Long.parseLong(productId));
            axcItem.setQuantity(entry.getValue());

            FunCartItem cartItem = updatedFunCartItemMap.get(productId);
            axcItem.setComment(cartItem.getAxLineCommentId());
            axcItems.add(axcItem);
        }

        axc.setCartId(StringUtils.isEmpty(axCartId) ? "" : axCartId);
        axc.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? customerId : savedCart.getAxCustomerId()); //set as customerId
        axc.setItems(axcItems);

        final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
        if (!axStarResult.isSuccess()) {
            log.info("Error encountered adding to cart. " + JsonUtil.jsonify(axStarResult));
            //rollback the added items...
            for (FunCartItem itemToAdd : cartToAdd.getItems()) {
                final String cartItemId = itemToAdd.generateCartItemId();
                final FunCartItem savedItem = savedItemsMap.get(cartItemId);
                final int originalQty = savedItem.getQty() - itemToAdd.getQty();
                if (originalQty > 0) {
                    savedItem.setQty(originalQty);
                } else {
                    savedItemsMap.remove(cartItemId);
                    savedItems.remove(savedItem);
                }
            }

            return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
        }


        savedCart.setAxCart(axStarResult.getData().getCart());
        cartManager.saveCart(channel, savedCart);


        //if have topup then need to update the topup as well
        if(topupCart.getItems().size() > 0) {
            ApiResult<List<FunCartItem>> removedTopups = updateRelatedTopups(channel, sessionId, topupCart);
            if(removedTopups.isSuccess()) {
                //formulate the topups
            }
        }

        final PartnerFunCartDisplay funCartDisplay = constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }


    private boolean isItemMeetMinQtySetting(List<FunCartItem> savedItems, List<FunCartItem> itemsToAdd, Long minQty){
        for (FunCartItem tempItem : itemsToAdd) {
            if (minQty != null && minQty > 0) {
                int sameItemQty = tempItem.getQty();
                for (FunCartItem savedItem : savedItems) {
                    if (savedItem.getListingId().equals(tempItem.getListingId())) {
                        sameItemQty = sameItemQty + savedItem.getQty();
                    }
                }
                if (sameItemQty < minQty) {
                    return false;
                }
            }
        }
        return true;
    }



    @Override
    public ApiResult<PartnerFunCartDisplay> cartUpdate(StoreApiChannels channel, FunCart cartToUpdate, String sessionId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        //DONE
        //group all the items by List<Map> items to be added, call the api for add the cart
        List<FunCartItem> cartItems = cartToUpdate.getItems();
        List<AxRetailCartTicket> cartTickets =  new ArrayList<>();
        Map<String, Integer> mainItemQtyMap = new HashMap<>(); //key = productid + listingId
        Map<String, Integer> generalItemQtyMap = new HashMap<>();
        Map<String, FunCartItem> eventItemQtyMap = new HashMap<>();
        Map<String, List<FunCartItem>> allItemQtyMap = new HashMap<>();

        //TODO might not need anymore if handled correclty on the front end size
        //update the qty of the topup to be the same as the main one.
        for(FunCartItem cartItem: cartItems) {
            if(!cartItem.isTopup()) {
                mainItemQtyMap.put(cartItem.getCmsProductId() + cartItem.getListingId(), cartItem.getQty());
            }else {
                if(mainItemQtyMap.containsKey(cartItem.getParentCmsProductId() + cartItem.getParentListingId()))  {
                    cartItem.setQty(mainItemQtyMap.get(cartItem.getParentCmsProductId() + cartItem.getParentListingId())); //TOPUP qty should be same as parent...
                }else {
                    cartItem.setQty(0); //TODO check maybe this one wont happen
                    //remove this item?????
                }
            }

            if(StringUtils.isNotEmpty(cartItem.getEventGroupId())) {
                eventItemQtyMap.put(cartItem.getProductCode(), cartItem);
            }else {
                int totalQty = cartItem.getQty();
                if(generalItemQtyMap.containsKey(cartItem.getProductCode())) {
                    totalQty = totalQty + generalItemQtyMap.get(cartItem.getProductCode());
                }
                generalItemQtyMap.put(cartItem.getProductCode(), totalQty);
            }

            if(allItemQtyMap.containsKey(cartItem.getListingId()))  {
                allItemQtyMap.get(cartItem.getListingId()).add(cartItem);
            }else {
                List<FunCartItem> cartItemList = new ArrayList<>();
                cartItemList.add(cartItem);
                allItemQtyMap.put(cartItem.getListingId(), cartItemList);
            }
        }

        //get the productext
        List<String> listingIds = new ArrayList<>();
        listingIds.addAll(allItemQtyMap.keySet());

        ApiResult<List<ProductExtViewModel>> productExtRes = null;
        try {
            productExtRes = productService.getDataForProducts(channel, listingIds);
            if(productExtRes.isSuccess()) {
                List<ProductExtViewModel> productExtList = productExtRes.getData();
                for(ProductExtViewModel productExt: productExtList) {
                    for(FunCartItem item: allItemQtyMap.get(productExt.getProductId())) {
                        item.setPrinting(productExt.getPrinting()); //assign the product ext printing here :)
                    }
                }
            }
        } catch (AxChannelException e) {
            //TODO dunno what to do
            log.error("Error in productService.getDataForProducts... ");
            log.error(e.getMessage(), e);
        }

        //generate those non event ticket
        for(String productCode: generalItemQtyMap.keySet()) {
            AxRetailCartTicket ct = new AxRetailCartTicket();
            ct.setAccountNum(savedCart.getAxCustomerId());
            ct.setTransactionId("");
            ct.setItemId(productCode);
            ct.setListingId(0L);
            ct.setQty(generalItemQtyMap.get(productCode));
            ct.setUpdateCapacity(0);
            cartTickets.add(ct);
        }

        //generate those event ticket
        for(String productCode: eventItemQtyMap.keySet()) {
            AxRetailCartTicket ct = new AxRetailCartTicket();
            FunCartItem eventItem = eventItemQtyMap.get(productCode);
            ct.setAccountNum(savedCart.getAxCustomerId());
            ct.setTransactionId("");
            ct.setItemId(productCode);
            ct.setListingId(0L);
            ct.setQty(eventItem.getQty());
            ct.setEventGroupId(eventItem.getEventGroupId());
            ct.setEventLineId(eventItem.getEventLineId());

            Date selectedEventDate = null;
            if(StringUtils.isNotEmpty(eventItem.getSelectedEventDate())) {
                try {
                    selectedEventDate = NvxDateUtils.parseDate(eventItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                } catch (ParseException e) {
                    log.error(e.getMessage(), e);
                }
            }
            ct.setEventDate(selectedEventDate);

            ct.setUpdateCapacity(0);
            cartTickets.add(ct);
        }

        //validate all the cartItems
        if(cartTickets.size() > 0) {
            final ApiResult<List<AxRetailTicketRecord>> result;
            try {
                result = axChannelTransactionService.apiCartValidateQuantity(channel, null, cartTickets);
                if (!result.isSuccess()) {
                    log.error("Failed to perform validate quantity at AX Retail service.");
                    return new ApiResult<>(ApiErrorCodes.AddCartResultInsufficientQty);
                }
            } catch (AxChannelException e) {
                log.error("Error in validating the quantity...");
                return new ApiResult<>(ApiErrorCodes.General);
            }
        }

        //delete all the cartItems lines
        AxStarCart axCart = savedCart.getAxCart();
        List<String> lineIds = new ArrayList<>();
        for(AxStarCartLine lines: axCart.getCartLines()) {
            lineIds.add(lines.getLineId());
        }

        if(lineIds.size() > 0) {
            AxStarInputRemoveCart itemsToRemove = new AxStarInputRemoveCart();
            itemsToRemove.setLineIds(lineIds);
            itemsToRemove.setCartId(axCart.getId());
            itemsToRemove.setCustomerId(axCart.getCustomerId());

            final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartRemoveItem(channel, itemsToRemove);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered deleting all items in the cart. " + JsonUtil.jsonify(axStarResult));
                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
            }
        }

        //add all of them again
        //This one is needed to handle multiple cart items with the same product ID.
        //Note that AX add to cart doesn't support multiple lines with the same product ID to be added in 1 transaction.
        final Map<Long, List<AxStarInputAddCartItem>> axcItemMap = new HashMap<>();
        for (FunCartItem funCartItem : cartItems) {
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();

            axcItem.setProductId(Long.parseLong(funCartItem.getListingId()));
            axcItem.setQuantity(funCartItem.getQty());
            axcItem.setComment(funCartItem.getAxLineCommentId());

            List<AxStarInputAddCartItem> axcItems = axcItemMap.get(axcItem.getProductId());
            if (axcItems == null) {
                axcItems = new ArrayList<>();
                axcItemMap.put(axcItem.getProductId(), axcItems);
            }
            axcItems.add(axcItem);
        }

        while (!axcItemMap.isEmpty()) {
            final AxStarInputAddCart axc = new AxStarInputAddCart();
            axc.setCartId(savedCart.getAxCart().getId());
            axc.setCustomerId(savedCart.getAxCustomerId());

            final List<Long> keysToRemove = new ArrayList<>();

            final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

            for (Map.Entry<Long, List<AxStarInputAddCartItem>> axcItemMapEntry : axcItemMap.entrySet()) {
                final List<AxStarInputAddCartItem> itams = axcItemMapEntry.getValue();

                final AxStarInputAddCartItem axcItem = itams.get(0);
                axcItems.add(axcItem);

                itams.remove(0);
                if (itams.isEmpty()) {
                    keysToRemove.add(axcItemMapEntry.getKey());
                }
            }

            for (Long itemKey : keysToRemove) {
                axcItemMap.remove(itemKey);
            }
            keysToRemove.clear();

            axc.setItems(axcItems);

            final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered rebuilding cart due to API add cart failure: " + JsonUtil.jsonify(axStarResult));
                log.info("Clearing cart contents...");
                //TODO Clear cart contents
                //TODO clear the saved cart also

                return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
            }

            final AxStarCart updatedAxCart = axStarResult.getData().getCart();
            if (axcItemMap.isEmpty()) {
                savedCart.setAxCart(updatedAxCart);
            }
        }


        //after adding everything, update also the local copy
        Map<String, FunCartItem> axLineCommentMap = new HashMap<>();
        for(FunCartItem cartItem: cartItems) {
            cartItem.setCartItemId(cartItem.generateCartItemId());
            axLineCommentMap.put(cartItem.getAxLineCommentId(), cartItem);
        }

        savedCart.setAxLineCommentMap(axLineCommentMap);
        savedCart.setItems(cartItems);

        final PartnerFunCartDisplay funCartDisplay = constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public ApiResult<PartnerFunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        FunCartItem itemToRemove = null;
        for (FunCartItem item : savedCart.getItems()) {
            if (item.getCartItemId().equals(cartItemId)) {
                itemToRemove = item;
            }
        }

        //TODO Update quantity properly for multi cart line items

        if (itemToRemove != null) {
            final String mainListingId = itemToRemove.getListingId();

            final Map<String, FunCartItem> relatedTopupMap = new HashMap<>();
            final List<FunCartItem> relatedTopups = new ArrayList<>();
            if (!itemToRemove.isTopup()) {
                for (FunCartItem item : savedCart.getItems()) {

                    //check the related topups of the item to be removed
                    if (item.isTopup() && mainListingId.equals(item.getParentListingId())) {
                        relatedTopupMap.put(mainListingId + item.getListingId(), item);
                        relatedTopups.add(item);
                    }
                }
            }

            final Integer qtyToRemove = itemToRemove.getQty();
            int runningQty = qtyToRemove;

            final AxStarCart axCart = savedCart.getAxCart(); //geting the ax cart
            final List<AxStarCartLine> axCartLines = axCart.getCartLines();
            final Map<String, List<AxStarCartLine>> lineMap = new HashMap<>();
            final List<String> lineIdsToRemove = new ArrayList<>();
            final Map<String, Integer> relatedTopupLinesToUpdate = new HashMap<>();
            String lineIdToUpdate = "";
            Integer qtyToUpdate = 0;

            //this one checking the car item, and to know the lineIDs of the item to be removed
            for (AxStarCartLine cl : axCartLines) {
                final String clProductId = cl.getProductId().toString();
                if (clProductId.equals(mainListingId)) {
                    final int lineQty = cl.getQuantity();
                    if (lineQty == runningQty) {
                        lineIdsToRemove.add(cl.getLineId()); //if the top up belongs only to the main item, remove this top up
                        break;
                    } else if (lineQty > runningQty) {
                        qtyToUpdate = lineQty - runningQty;    //if the top up belongs to multiple main item, then just update the qty of the topup
                        lineIdToUpdate = cl.getLineId();
                        break;
                    } else {
                        lineIdsToRemove.add(cl.getLineId());  //this one maybe happen when 2 cart lines have same productId loh
                        runningQty -= lineQty;
                        if (runningQty <= 0) {
                            break;
                        }
                    }
                }
            }

            //map of productId with the list of cartlines
            for (AxStarCartLine cl : axCartLines) {
                final String clProductId = cl.getProductId().toString();
                //Place other non main product lines inside map for future reference
                List<AxStarCartLine> relatedLines = lineMap.get(clProductId);
                if (relatedLines == null) {
                    relatedLines = new ArrayList<>();
                    lineMap.put(clProductId, relatedLines);  //is it like puting a empty arraylist to a map of productID??????????????
                }
                relatedLines.add(cl);
            }

            //TODO Think of a better performing algorithm to do this. Nested loops are expensive.
            //get all the relatedtopup of the deleted item
            for (FunCartItem relatedTopup : relatedTopups) {
                final String topupListingId = relatedTopup.getListingId();
                int topupRunningQty = relatedTopup.getQty();

                final List<AxStarCartLine> theLines = lineMap.get(topupListingId); //O_O
                for (AxStarCartLine cl : theLines) {
                    final String clProductId = cl.getProductId().toString();
                    if (clProductId.equals(topupListingId)) {
                        final int lineQty = cl.getQuantity();
                        if (lineQty == topupRunningQty) {
                            lineIdsToRemove.add(cl.getLineId());
                            break;
                        } else if (lineQty > topupRunningQty) {
                            relatedTopupLinesToUpdate.put(cl.getLineId(), lineQty - topupRunningQty);
                            lineIdToUpdate = cl.getLineId();
                            break;
                        } else {
                            lineIdsToRemove.add(cl.getLineId());
                            topupRunningQty -= lineQty;
                            if (topupRunningQty <= 0) {
                                break;
                            }
                        }
                    }
                }
            }

            final boolean noLinesToRemove = lineIdsToRemove.isEmpty();
            if (StringUtils.isEmpty(lineIdToUpdate) && noLinesToRemove &&
                    relatedTopupLinesToUpdate.isEmpty()) {
                return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
            }

            final AxStarInputUpdateCart cartToUpdate = new AxStarInputUpdateCart();
            cartToUpdate.setCartId(axCart.getId());
            cartToUpdate.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
            cartToUpdate.setItems(new ArrayList<>());

            //add the item to remove in the cartToUpdate
            if (StringUtils.isNotEmpty(lineIdToUpdate)) {
                final AxStarInputUpdateCartItem item = new AxStarInputUpdateCartItem();
                item.setLineId(lineIdToUpdate);
                item.setQuantity(qtyToUpdate);
                cartToUpdate.getItems().add(item);
            }

            for (Map.Entry<String, Integer> entry : relatedTopupLinesToUpdate.entrySet()) {
                final AxStarInputUpdateCartItem item = new AxStarInputUpdateCartItem();
                item.setLineId(entry.getKey());
                item.setQuantity(entry.getValue());
                cartToUpdate.getItems().add(item);
            }

            if (!cartToUpdate.getItems().isEmpty()) {
                AxStarServiceResult<AxStarCartNested> heller = axStarService.apiCartUpdateItem(channel, cartToUpdate);//TODO complete this lah
                if (!heller.isSuccess()) {
                    log.error("Here got error.");
                    return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
                }

                if (noLinesToRemove) {
                    savedCart.setAxCart(heller.getData().getCart());
                }
            }

            if (!noLinesToRemove) {
                final AxStarInputRemoveCart cartToRemove = new AxStarInputRemoveCart();
                cartToRemove.setCartId(axCart.getId());
                cartToRemove.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
                cartToRemove.setLineIds(lineIdsToRemove);

                final AxStarServiceResult<AxStarCartNested> removeCartResult = axStarService.apiCartRemoveItem(channel, cartToRemove);
                if (!removeCartResult.isSuccess()) {
                    return new ApiResult<>(ApiErrorCodes.AxStarErrorCartRemoveItem);
                }

                savedCart.setAxCart(removeCartResult.getData().getCart());
            }

            final List<FunCartItem> itamz = savedCart.getItems();
            itamz.remove(itemToRemove);
            for (FunCartItem relTopup : relatedTopups) {
                itamz.remove(relTopup);
            }
        }

        final PartnerFunCartDisplay funCartDisplay = constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }


    @Override
    public ApiResult<String> cartRecreate(StoreApiChannels channel, FunCart savedCart) {

        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        cartManager.saveCart(channel, savedCart);
        savedCart.setAxCart(null);

        //This one is needed to handle multiple cart items with the same product ID.
        //Note that AX add to cart doesn't support multiple lines with the same product ID to be added in 1 transaction.
        final Map<Long, List<AxStarInputAddCartItem>> axcItemMap = new HashMap<>();

        final List<FunCartItem> funCartItems = savedCart.getItems();
        for (FunCartItem funCartItem : funCartItems) {
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();

            axcItem.setProductId(Long.parseLong(funCartItem.getListingId()));
            axcItem.setQuantity(funCartItem.getQty());
            axcItem.setComment(funCartItem.getAxLineCommentId());

            List<AxStarInputAddCartItem> axcItems = axcItemMap.get(axcItem.getProductId());
            if (axcItems == null) {
                axcItems = new ArrayList<>();
                axcItemMap.put(axcItem.getProductId(), axcItems);
            }
            axcItems.add(axcItem);
        }

        String axCartId = "";

        while (!axcItemMap.isEmpty()) {
            final AxStarInputAddCart axc = new AxStarInputAddCart();
            axc.setCartId(axCartId);
            axc.setCustomerId(savedCart.getAxCustomerId());

            final List<Long> keysToRemove = new ArrayList<>();

            final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

            for (Map.Entry<Long, List<AxStarInputAddCartItem>> axcItemMapEntry : axcItemMap.entrySet()) {
                final List<AxStarInputAddCartItem> itams = axcItemMapEntry.getValue();

                final AxStarInputAddCartItem axcItem = itams.get(0);
                axcItems.add(axcItem);

                itams.remove(0);
                if (itams.isEmpty()) {
                    keysToRemove.add(axcItemMapEntry.getKey());
                }
            }

            for (Long itemKey : keysToRemove) {
                axcItemMap.remove(itemKey);
            }
            keysToRemove.clear();

            axc.setItems(axcItems);

            final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered rebuilding cart due to API add cart failure: " + JsonUtil.jsonify(axStarResult));
                log.info("Clearing cart contents...");
                //TODO Clear cart contents

                return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
            }

            final AxStarCart updatedAxCart = axStarResult.getData().getCart();

            axCartId = updatedAxCart.getId();

            if (axcItemMap.isEmpty()) {
                savedCart.setAxCart(updatedAxCart);
            }
        }

        return new ApiResult<>(true, "", "", null);
    }

    /*
    TODO: evaluate if needed
     */
    @Override
    public boolean cartClear(StoreApiChannels channel, String sessionId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return false;
        }

        final AxStarCart axCart = savedCart.getAxCart();
        final AxStarServiceResult<String> deleteCartResult = axStarService.apiCartDeleteEntireCart(channel, axCart.getId(),
                StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        if (!deleteCartResult.isSuccess()) {
            log.info("Error encountered deleting entire cart. " + JsonUtil.jsonify(deleteCartResult));
            return false;
        }

        cartManager.removeCart(channel, sessionId);

        return true;
    }

    @Override
    public Integer getCartSize(StoreApiChannels channel, String sessionId) {
        final FunCart funCart = cartManager.getCart(channel, sessionId);
        if (funCart != null && funCart.getItems() != null) {
            List<FunCartItem> cartItems = funCart.getItems();
            int cartSize = 0;
            for(FunCartItem cartItem: cartItems) {
                cartSize += cartItem.getQty();
            }
            return cartSize;
        }
        return 0;
    }

    @Override
    public PartnerFunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId) {
        final FunCart funCart = cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            PartnerFunCartDisplay emptyCart = new PartnerFunCartDisplay();
            emptyCart.setCartId(sessionId);
            return emptyCart;
        }

        return constructCartDisplay(funCart);
    }

    //TODO reavaluate the price thingy
    @Override
    public PartnerFunCartDisplay constructCartDisplay(FunCart cart) {
        final PartnerFunCartDisplay cd = new PartnerFunCartDisplay();

        cd.setCartId(cart.getCartId());

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;

        final List<AxStarCartLine> cartLines = cart.getAxCart().getCartLines();
        final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
        for (AxStarCartLine cl : cartLines) {
            final String lineComment = cl.getComment();
            List<AxStarCartLine> lineList = cartLineMap.get(lineComment);
            if (lineList == null) {
                lineList = new ArrayList<>();
                cartLineMap.put(lineComment, lineList);
            }
            lineList.add(cl);
        }

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();

        for (FunCartItem item : cart.getItems()) {
            final boolean isTopup = item.isTopup();
            final String productId = isTopup ? item.getParentCmsProductId() : item.getCmsProductId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getCmsProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            final String axCommentId = item.getAxLineCommentId();
            final List<AxStarCartLine> relatedLines = cartLineMap.get(axCommentId);

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();
            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();
            itemDisplay.setListingId(item.getListingId());
            itemDisplay.setProductCode(item.getProductCode());
            itemDisplay.setIsTopup(isTopup);

            if (itemDisplay.isTopup()) {
                itemDisplay.setParentListingId(item.getParentListingId());
            }else {
                //TODO get the topups and add it to the additonalTopups if it is not yet been added
                //get all the topups and save it
                //TODO CHECK IF THIS IS CORreCt
//                List<ContentMap> topups = starfn.asContentMapList(starfn.getAXProductCrossSellsByListingId(cart.getChannelCode(), itemDisplay.getListingId()));
//                for(ContentMap topup:topups) {
//                    System.out.println(topup.get(AXProductCrossSellProperties.ListingId.getPropertyName()));
//                }
            }

            itemDisplay.setType(item.getType());
            itemDisplay.setName(item.getName());


            itemDisplay.setCartItemId(item.getCartItemId());

            BigDecimal itemUnitPrice = item.getPrice();
            final int qtyForThisLine = item.getQty();
            int itemQty = 0;
            BigDecimal itemTotalAmount = BigDecimal.ZERO;
            BigDecimal itemDiscountAmount = BigDecimal.ZERO;


            final List<AxStartCartLineDiscountLine> itemDiscountLines = new ArrayList<>();
            for (AxStarCartLine line : relatedLines) {
                final int lineQty = line.getQuantity();

                itemQty += lineQty;
                itemTotalAmount = itemTotalAmount.add(line.getTotalAmount());
                itemDiscountAmount = itemDiscountAmount.add(line.getDiscountAmount());

                for (AxStartCartLineDiscountLine discountLine : line.getDiscountLines()) {
                    itemDiscountLines.add(discountLine);
                }
            }

//            final Map<String, AxStartCartLineDiscountLine> dcMap = new HashMap<>();
//
//            for (AxStarCartLine line : relatedLines) {
//                int qtyToAdd = 0;
//                if (itemQty + line.getQuantity() > qtyForThisLine) {
//                    qtyToAdd = line.getQuantity() - ((itemQty + line.getQuantity()) - qtyForThisLine);
//                } else {
//                    qtyToAdd = line.getQuantity();
//                }
//                itemQty += qtyToAdd;
//
//                BigDecimal qtyToAddBd = BigDecimal.valueOf(qtyToAdd);
//                BigDecimal lineQuantityBd = BigDecimal.valueOf(line.getQuantity());
//
//                itemTotalAmount = itemTotalAmount.add(line.getTotalAmount().divide(lineQuantityBd).multiply(qtyToAddBd));
//                itemDiscountAmount = itemDiscountAmount.add(line.getDiscountAmount().divide(lineQuantityBd).multiply(qtyToAddBd));
//
//                for (AxStartCartLineDiscountLine discountLine : line.getDiscountLines()) {
//                    final String dcCode = discountLine.getDiscountCode();
//                    AxStartCartLineDiscountLine dcLine = dcMap.get(dcCode);
//                    if (dcLine == null) {
//                        dcLine = discountLine;
//                        dcMap.put(dcCode, dcLine);
//                    }
//                }
//            }

//            if (itemQty != item.getQty()) {
//                itemQty = item.getQty();
//            }

            itemDisplay.setQty(itemQty);
            itemDisplay.setPrice(itemUnitPrice);
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(itemUnitPrice, "S$ "));
            itemDisplay.setTotal(itemTotalAmount);
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemTotalAmount, "S$ "));
            itemDisplay.setDiscountTotal(itemDiscountAmount);
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDiscountAmount, "S$ "));

            final boolean isEvent = StringUtils.isNotEmpty(item.getEventGroupId());
            final boolean showEventDetails = StringUtils.isNotEmpty(item.getSelectedEventDate());

            //TODO check with the B2B

            itemDisplay.setDescription(
                    (isEvent && showEventDetails? "<br/>Event Session: " + item.getEventSessionName() +
                            "<br/>Event Date: " + item.getSelectedEventDate()
                            : ""));

            itemDisplay.setEventGroupId(item.getEventGroupId());
            itemDisplay.setEventLineId(item.getEventLineId());
            itemDisplay.setSelectedEventDate(item.getSelectedEventDate());
            itemDisplay.setEventSessionName(item.getEventSessionName());

            StringBuilder sb = new StringBuilder("");
            Map<String, Boolean> appliedDiscountDisplayIds = new HashMap<>();
            for (AxStartCartLineDiscountLine discountLine : itemDiscountLines) {
                if (appliedDiscountDisplayIds.get(discountLine.getOfferId()) == null) {
                    appliedDiscountDisplayIds.put(discountLine.getOfferId(), true);
                    //TODO Must retrieve from promotion content in JCR
                    sb.append(discountLine.getOfferName() + "<br/>");
                }
            }
            if (sb.length() > 4) {
                sb.setLength(sb.length() - 4);
            }

            itemDisplay.setDiscountLabel(sb.toString());

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            productDisplay.reorganiseItems();
            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));
        }

        cd.setCmsProducts(cmsProducts);
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));


        //get the topups of the producsts
//        final Map<String, List<FunCartDisplayItem>> cmsProductTopupsMap = new HashMap<>();
//        for(String productId: cmsProductMap.keySet()) {
//            List<FunCartDisplayItem> axProductTopupsMap = cmsProductTopupsMap.get(productId);
//            if(axProductTopupsMap == null) {
//                axProductTopupsMap = new ArrayList<>();
//            }
//            FunCartDisplayCmsProduct cmsProduct = cmsProductMap.get(productId);
//            List<FunCartDisplayItem> cmsProductItems =  cmsProduct.getItems();
//            for(FunCartDisplayItem item: cmsProductItems) {
//                if(!item.isTopup()) {
//                    List<ContentMap> relatedTopups = starfn.asContentMapList(starfn.getAXProductCrossSellsByListingId(cart.getChannelCode(), item.getListingId()));
//
//                    if(relatedTopups != null && relatedTopups.size() > 0) {
//                        for(ContentMap relatedTopupMap: relatedTopups) {
//                            FunCartDisplayItem topupItem = new FunCartDisplayItem();
//
//                            topupItem.setProductCode(relatedTopupMap.get(AXProductCrossSellProperties.ProductCode.getPropertyName()));
//
//                            topupItem.setIsTopup(true);
//                        }
//
//
//                    }
//                   // axProductTopupsMap.put(item.getParentListingId() + "-" + item.getListingId(), item);
//                }
//            }
//            //cmsProductTopupsMap.put(productId, axProductTopupsMap);
//        }

        //construct the topup here for easy access later in the front end
//        final Map<String, Map<String, FunCartDisplayItem>> cmsProductTopupsMap = new HashMap<>();
//        for(String productId: cmsProductMap.keySet()) {
//
//            Map<String, FunCartDisplayItem> axProductTopupsMap = cmsProductTopupsMap.get(productId);
//
////            if(axProductTopupsMap == null) {
////                axProductTopupsMap = new HashMap<>();
////            }
//
//            FunCartDisplayCmsProduct cmsProduct = cmsProductMap.get(productId);
//            List<FunCartDisplayItem> cmsProductItems =  cmsProduct.getItems();
//            for(FunCartDisplayItem item: cmsProductItems) {
//                if(!item.isTopup()) {
//                    List<ContentMap> relatedTopups = starfn.asContentMapList(starfn.getAXProductCrossSellsByListingId(cart.getChannelCode(), item.getListingId()));
//
//                    if(relatedTopups != null && relatedTopups.size() > 0) {
//                        //relatedTopups here
//
//                    }
//                   // axProductTopupsMap.put(item.getParentListingId() + "-" + item.getListingId(), item);
//                }
//            }
//            //cmsProductTopupsMap.put(productId, axProductTopupsMap);
//        }

        //cd.setCartTopupMap(cmsProductTopupsMap);


        //construct the topup here for easy access later in the front end
        //TODO only construct the additionalTopups when needed
        final Map<String, Map<String, FunCartDisplayItem>> additionalTopups = new HashMap<>(); //TODO get the list of additional top ups so user can add it in
        final Map<String, Map<String, FunCartDisplayItem>> cmsProductTopupsMap = new HashMap<>();
        final Map<String, FunCartDisplayItem> standardFuncartItemMap = new HashMap<>();
        final Map<String, List<String>> topupFuncartItemMap = new HashMap<>();

        for(String productId: cmsProductMap.keySet()) {
            Map<String, FunCartDisplayItem> axProductTopupsMap = cmsProductTopupsMap.get(productId);

            if(axProductTopupsMap == null) {
                axProductTopupsMap = new HashMap<>();
            }

            FunCartDisplayCmsProduct cmsProduct = cmsProductMap.get(productId);
            List<FunCartDisplayItem> cmsProductItems =  cmsProduct.getItems();
            for(FunCartDisplayItem item: cmsProductItems) {
                if(item.isTopup()) {
                    axProductTopupsMap.put(item.getParentListingId() + "-" + item.getListingId(), item);
                    List<String> topupListingIds = topupFuncartItemMap.get(item.getParentListingId());
                    if(topupListingIds == null) {
                        topupListingIds = new ArrayList<>();
                    }
                    topupListingIds.add(item.getListingId());
                    topupFuncartItemMap.put(item.getParentListingId(), topupListingIds);

                }else {
                    standardFuncartItemMap.put(item.getListingId(), item);  //saving the main item, so we can get if they still have topups...
                }
            }

            Map<String, FunCartDisplayItem> availTopupMap = cmsProductTopupsMap.get(productId);
            if(availTopupMap == null) {
                availTopupMap = new HashMap<>();
            }

            //check the additional topups here
            for(String listingId: standardFuncartItemMap.keySet()) {
                List<String> existingTopupListingIds = topupFuncartItemMap.get(listingId);


                List<FunCartDisplayItem> availTopupList = new ArrayList<>();
                //get the topups based on the listingId
                AxProductPriceMap priceMap = MgnlContext.getAttribute(PartnerPortalConst.CACHE_PARTNER_PRODUCT_PRICE_MAP, Context.SESSION_SCOPE);
                Map<String, List<AxProductPrice>> productPrices = new HashMap<>();

                if(priceMap != null) {
                    productPrices = priceMap.getProductPriceMap();
                }
                List<Node> crossSells = starfn.getAXProductCrossSellsByListingId(cart.getChannelCode(), listingId);
                for(Node cs:crossSells) {
                    FunCartDisplayItem topupItem = new FunCartDisplayItem();
                    topupItem.setListingId(PropertyUtil.getString(cs, AXProductCrossSellProperties.ListingId.getPropertyName()));

                    if(existingTopupListingIds != null && existingTopupListingIds.contains(topupItem.getListingId())) {
                        continue;
                    }
                    topupItem.setProductCode(PropertyUtil.getString(cs, AXProductCrossSellProperties.ProductCode.getPropertyName()));
                    topupItem.setIsTopup(true);
                    topupItem.setQty(standardFuncartItemMap.get(listingId).getQty()); //assign the qty of the main item
                    topupItem.setName(PropertyUtil.getString(cs, AXProductCrossSellProperties.ItemName.getPropertyName()));

                    Node axProductNode = starfn.getAxProductByProductCode(cart.getChannelCode(), topupItem.getProductCode());

                    BigDecimal price = BigDecimal.ZERO;
                    if(productPrices.containsKey(topupItem.getListingId())) {
                        price = productPrices.get(topupItem.getListingId()).get(0).getCustomerContextualPrice();
                    }else {
                        String priceStr = PropertyUtil.getString(axProductNode, AXProductProperties.ProductPrice.getPropertyName());
                        if(StringUtils.isNotEmpty(priceStr)) {
                            price = new BigDecimal(priceStr);
                        }
                    }

                    topupItem.setPrice(price); //TODO should be from customer prize list O_O
                    topupItem.setType(PropertyUtil.getString(axProductNode, AXProductProperties.TicketType.getPropertyName()));
                    topupItem.setTotal(BigDecimal.ZERO);
                    topupItem.setParentListingId(listingId);
                    availTopupMap.put(listingId + "-" + topupItem.getListingId(), topupItem);
                }

            }
            additionalTopups.put(productId, availTopupMap);
            cmsProductTopupsMap.put(productId, axProductTopupsMap);
        }

        cd.setCartTopupMap(cmsProductTopupsMap);
        cd.setAdditionalTopups(additionalTopups);
        cd.setGstRate(gstSrv.getCurrentGST());

        return cd;
    }

    @Override
    public CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session) {


        final InventoryTransactionVM txnInSession = getTransactionInSession(channel, session);
        if (txnInSession == null) {
            return null;
        }


        FunCart funCart;
        if(cartManager.getCart(channel, session.getId()) == null) {
            PPMFLGAxCheckoutCart checkoutCart = axCheckoutCartRepo.findByReceiptNum(txnInSession.getReceiptNumber());
            funCart= JsonUtil.fromJson(checkoutCart.getSessionCartJson(), FunCart.class);
        }else {
            funCart = cartManager.getCart(channel, session.getId());
        }

        final PartnerFunCartDisplay cartVm;
        if (funCart == null) {
            cartVm = new PartnerFunCartDisplay();
            cartVm.setCartId(session.getId());
        } else {
            cartVm = constructCartDisplay(funCart);
        }

        final CheckoutDisplay checkout = new CheckoutDisplay();
        checkout.setCartId(cartVm.getCartId());
        checkout.setTotalQty(cartVm.getTotalQty());
        checkout.setTotalMainQty(cartVm.getTotalMainQty());
        checkout.setCmsProducts(cartVm.getCmsProducts());
        checkout.setPayMethod(cartVm.getPayMethod());
        checkout.setPaymentTypeLabel("");
        if(StringUtils.isNotEmpty(checkout.getPayMethod())){
            checkout.setMessage("");
        }

        final boolean hasBookingFee = false;
        checkout.setHasBookingFee(hasBookingFee);
        checkout.setBookFeeMode("");

        BigDecimal grandTotal = cartVm.getTotal();

        //TODO remove this
        checkout.setWaivedTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        checkout.setTotal(grandTotal);
        checkout.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        checkout.setGstRate(gstSrv.getCurrentGST());

        return checkout;
    }

    @Override
    public ApiResult<InventoryTransactionVM> doCheckout(StoreApiChannels channel, HttpSession session, PartnerAccount partnerAccount, String ip) {
        final String sessionId = session.getId();
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        final ApiResult<InventoryTransactionVM> result = doCheckout(channel, savedCart, partnerAccount, ip);
        if (result.isSuccess()) {
            final InventoryTransactionVM txn = result.getData();
            saveTransactionInSession(channel, session, txn);
            cartManager.removeCart(channel, savedCart.getCartId()); //YES Delete it!!!!!
        }

        return result;
    }


    @Override
    @Transactional
    public ApiResult<InventoryTransactionVM> doCheckout(StoreApiChannels channel, FunCart cart, PartnerAccount partnerAccount, String ip) {
        final Date now = new Date();
        final List<FunCartItem> cartItems =cart.getItems();

        //validate
        ApiResult<InventoryTransactionVM> validateCheckoutRes = this.validateCheckOut(partnerAccount, ip, cartItems); //TODO ok this is weird, it should check the status

        if(!validateCheckoutRes.isSuccess()) {
            return validateCheckoutRes;
        }

        //TODO refer to PurchaseServiceImpl.java doCheckout method
        final String receiptNumber = ProjectUtils.generateReceiptNumber(channel, now); //TODO follow the B2B receipt format

        if(receiptNumber == null) {
            return new ApiResult<>(ApiErrorCodes.CheckoutResultFailedReceiptSequence);
        }

        //RESERVE ITEMS
        ApiResult<InventoryTransactionVM> res = this.finishAxReserveAndCheckout(channel, receiptNumber, cart, partnerAccount.getPartner().getAxAccountNumber());
        if(!res.isSuccess()) {
            return new ApiResult<>(res.getApiErrorCode());
        }
        //delete the cart since we saved ia lready

        return this.makeTransaction(channel, receiptNumber, partnerAccount, cart, ip, false);
    }

    private DateTime getValidateEndDate(){
        Date now = new Date();
        //TODO call from SysParam
//        final SysParam expiringParam = this.sysService.getParamByKey(SysParamConst.TicketExpiringPeriod.toString());TicketExpiringPeriod
        final Integer expiryMonths = 6;
        final DateTime validityEndDate = new DateTime(now).plusMonths(expiryMonths);
        return validityEndDate;
    }



    //TODO refer to check PurchaseServiceImpl makeTransaction
    @Transactional
    public ApiResult<InventoryTransactionVM> makeTransaction(StoreApiChannels channel, String receiptNumber, PartnerAccount partnerAccount,
                                                                                                             FunCart cart, String ip, boolean offline){
        try {
            final Date now = new Date();
            final PPMFLGInventoryTransaction trans = new PPMFLGInventoryTransaction();
            trans.setReceiptNum(receiptNumber);
            trans.setCurrency("SGD");
            trans.setCreatedDate(now);
            trans.setModifiedDate(now);
            trans.setMainAccountId(partnerAccount.getMainAccountId());
            trans.setUsername(partnerAccount.getUsername());
            trans.setSubAccountTrans(partnerAccount.isSubAccount());

            BigDecimal gstRate = gstSrv.getCurrentGST();
            trans.setGstRate(gstRate);

            trans.setIp(ip);

            String tmMerchantId = starfn.getDefaultMerchant(channel.code);
            trans.setTmMerchantId(tmMerchantId);

            trans.setPaymentType("");
            trans.setTicketGenerated(false);

            trans.setRevalidated(false);
            trans.setReprintCount(0);

            if (offline) {
                trans.setStatus(TicketStatus.Pending_Submit.toString());
                trans.setPaymentType(PartnerPortalConst.OfflinePayment);
            } else {
                trans.setStatus(TicketStatus.Reserved.toString());
            }

            trans.setTmStatus("");

            BigDecimal total = BigDecimal.ZERO;

            Date defValidityEndDate = getValidateEndDate().toDate(); //TODO evaluate if still applicable to STAR
            Date txnValidityStartDate = null;
            Date txnValidityEndDate = null;

            final List<AxStarCartLine> cartLines = cart.getAxCart().getCartLines();
            final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
            for (AxStarCartLine cl : cartLines) {
                final String productId = cl.getProductId().toString();
                List<AxStarCartLine> lineList = cartLineMap.get(productId);
                if (lineList == null) {
                    lineList = new ArrayList<>();
                    cartLineMap.put(productId, lineList);
                }
                lineList.add(cl);
            }

            final List<PPMFLGInventoryTransactionItem> txnItems = new ArrayList<>();
            List<String> listingIds = new ArrayList<>();
            for (FunCartItem cartItem : cart.getItems()) {
                final PPMFLGInventoryTransactionItem item = new PPMFLGInventoryTransactionItem();

                item.setProductId(cartItem.getCmsProductId());
                item.setProductName(cartItem.getCmsProductName());
                item.setItemListingId(cartItem.getListingId());
                item.setItemProductCode(cartItem.getProductCode());
                item.setItemType("S"); //TODO might not need this one anymore, coz this one came from SACT, suggest to remove this
                item.setDisplayName(cartItem.getName());
                item.setTicketType(cartItem.getType());
                item.setStatus(TicketStatus.Available.toString());

                if (cartItem.isTopup()) {
                    item.setTransItemType(TransItemType.Topup.toString());
                    item.setMainItemListingId(cartItem.getParentListingId());
                    item.setMainItemProductId(cartItem.getParentCmsProductId());
                } else {
                    item.setTransItemType(TransItemType.Standard.toString());
                }

                final boolean isEvent = StringUtils.isNotEmpty(cartItem.getEventGroupId());
                final boolean showEventDetails = StringUtils.isNotEmpty(cartItem.getSelectedEventDate());
                item.setDisplayDetails(
                        isEvent && showEventDetails? "<br/>Event Session: " + cartItem.getEventSessionName() + "<br/>Event Date: " + cartItem.getSelectedEventDate()
                                : "");

                final List<AxStarCartLine> relatedLines = cartLineMap.get(item.getItemListingId());

                item.setEventGroupId(cartItem.getEventGroupId());
                item.setEventLineId(cartItem.getEventLineId());
                item.setEventName(cartItem.getEventSessionName());

                item.setOtherDetails(cartItem.getAxLineCommentId());


                BigDecimal itemUnitPrice = cartItem.getPrice();
                final int qtyForThisLine = cartItem.getQty();
                int itemQty = 0;
                BigDecimal itemTotalAmount = BigDecimal.ZERO;
                BigDecimal itemDiscountAmount = BigDecimal.ZERO;
                final Map<String, AxStartCartLineDiscountLine> dcMap = new HashMap<>();

                for (AxStarCartLine line : relatedLines) {
                    int qtyToAdd = 0;
                    if (itemQty + line.getQuantity() > qtyForThisLine) {
                        qtyToAdd = line.getQuantity() - ((itemQty + line.getQuantity()) - qtyForThisLine);
                    } else {
                        qtyToAdd = line.getQuantity();
                    }
                    itemQty += qtyToAdd;

                    BigDecimal qtyToAddBd = BigDecimal.valueOf(qtyToAdd);
                    BigDecimal lineQuantityBd = BigDecimal.valueOf(line.getQuantity());

                    itemTotalAmount = itemTotalAmount.add(line.getTotalAmount().divide(lineQuantityBd).multiply(qtyToAddBd));
                    itemDiscountAmount = itemDiscountAmount.add(line.getDiscountAmount().divide(lineQuantityBd).multiply(qtyToAddBd));

                    for (AxStartCartLineDiscountLine discountLine : line.getDiscountLines()) {
                        final String dcCode = discountLine.getDiscountCode();
                        AxStartCartLineDiscountLine dcLine = dcMap.get(dcCode);
                        if (dcLine == null) {
                            dcLine = discountLine;
                            dcMap.put(dcCode, dcLine);
                        }
                    }

                    itemUnitPrice = line.getPrice();
                }

                if (itemQty != cartItem.getQty()) {
                    itemQty = cartItem.getQty();
                }


                item.setQty(itemQty);
                item.setBaseQty(itemQty);
                item.setUnpackagedQty(itemQty);
                item.setUnitPrice(itemUnitPrice);
                item.setSubTotal(itemTotalAmount);
                //item.setDiscountTotal(itemDiscountAmount); //TODO no need i think

                StringBuilder sb = new StringBuilder("");
                for (AxStartCartLineDiscountLine discountLine : dcMap.values()) {
                    sb.append(discountLine.getOfferName() + ", ");
                }

                if (sb.length() > 2) {
                    sb.setLength(sb.length() - 2);
                }
                // item.setDiscountLabel(sb.toString());

                total = total.add(item.getSubTotal());

                if (isEvent &&  StringUtils.isNotEmpty(cartItem.getSelectedEventDate())) {
                    try {
                        item.setDateOfVisit(NvxDateUtils.parseDate(cartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));

                        if (txnValidityEndDate == null) {
                            txnValidityEndDate = item.getDateOfVisit();
                        }

                        trans.setValidityEndDate(item.getDateOfVisit()); //TODO although this should be the same date for all so should be safe
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    listingIds.add(item.getItemListingId());
                }

                txnItems.add(item);
            }

            //get the product ext, and check the validity thingy
            try {
                ApiResult<List<ProductExtViewModel>> productExtRes = productService.getDataForProducts(channel, listingIds);
                if(productExtRes.isSuccess()) {
                    List<ProductExtViewModel> extViewModels = productExtRes.getData();
                    for (ProductExtViewModel extViewModel : extViewModels) {
                        Date endDate = extViewModel.getOpenValidityEndDate();
                        Date strDate = extViewModel.getOpenValidityStartDate();
                        if(endDate != null && strDate != null) {
                            if (txnValidityEndDate != null) {
                                if (txnValidityEndDate.compareTo(endDate) >= 0) {
                                    txnValidityEndDate = endDate;
                                }
                            } else {
                                txnValidityEndDate = endDate;
                            }
                            if (txnValidityStartDate != null) {
                                if (txnValidityStartDate.compareTo(strDate) <= 0) {
                                    txnValidityStartDate = strDate;
                                }
                            } else {
                                txnValidityStartDate = strDate;
                            }
                        }
                    }
                }
            } catch (AxChannelException e){
                log.error(e.getMessage(), e);
            }
            //TODO get the validity date from AX

            trans.setTotalAmount(total);
            trans.setValidityStartDate(txnValidityStartDate != null ? txnValidityStartDate : now);
            trans.setValidityEndDate(txnValidityEndDate != null ? txnValidityEndDate : defValidityEndDate);

            transRepo.save(trans);

            for (PPMFLGInventoryTransactionItem item : txnItems) {
                item.setTransactionId(trans.getId());
                item.setValidityStartDate(trans.getValidityStartDate()); //update the validity date based on trans
                item.setValidityEndDate(trans.getValidityEndDate()); //update the validity date based on trans
                itemRepo.save(item);
            }

            return new ApiResult<>(true, "", "", InventoryTransactionVM.fromDataModel(trans));
        }catch (Exception e) {
            if(!offline) {
                revertAxReservation(channel, receiptNumber, cart.getAxCart(), partnerAccount.getPartner().getAxAccountNumber());
            }
            return new ApiResult<>(ApiErrorCodes.General);
        }
    }

    private ApiResult<InventoryTransactionVM> revertAxReservation(StoreApiChannels channel, String receiptNumber, AxStarCart axCart, String accountNumber) {
        final List<AxRetailCartTicket> cartTickets =  axCart.getReservedTickets();

        if(cartTickets.size() > 0) {
            for(AxRetailCartTicket axRetailCartTicket: cartTickets) {
                axRetailCartTicket.setQty(0);
                axRetailCartTicket.setUpdateCapacity(1);
            }

            final ApiResult<List<AxRetailTicketRecord>> result;
            try {
                result = axChannelTransactionService.apiCartValidateAndReserveQuantity(channel, receiptNumber, cartTickets, accountNumber);

                if (!result.isSuccess()) {
                    log.error("Failed to perform validate and reserve quantity at AX Retail service. (qty to set to 0)");
                }
            } catch (AxChannelException e) {
                e.printStackTrace();
            }

        }
        return new ApiResult<>(true, "", "", null);
    }

    @Override
    @Transactional
    public ApiResult<InventoryTransactionVM> finishAxReserveAndCheckout(StoreApiChannels channel, String receiptNumber, FunCart savedCart, String accountNumber){

        //TOOD double check if needs the daily capacity checking ek ek :(

        /*
        Ax Validate and Reserve
         */
        final AxStarCart savedAxCart = savedCart.getAxCart();
        final Map<String, FunCartItem> axLineCommentMap = savedCart.getAxLineCommentMap();
        //traverse all the checkout cart and create the cartickets to pass to AX
        final List<AxRetailCartTicket> cartTickets = new ArrayList<>();
        for(AxStarCartLine axStarCartLine: savedAxCart.getCartLines()) {
            final AxRetailCartTicket ct = new AxRetailCartTicket();

            String comment = axStarCartLine.getComment(); //always have comment if i'm not wrong...
            FunCartItem cartItem = axLineCommentMap.get(comment);
//
//            //exlucde the barcoe
//            if(cartItem.getPrinting() == 3) {
//                continue;
//            }
            ct.setItemId(axStarCartLine.getItemId());
            ct.setListingId(axStarCartLine.getProductId());
            ct.setQty(axStarCartLine.getQuantity());
            ct.setTransactionId(receiptNumber);
            ct.setLineId(axStarCartLine.getLineId()); //copy over the line id also to he knows which one la


            if(StringUtils.isNotBlank(cartItem.getEventGroupId())) {
                ct.setEventGroupId(cartItem.getEventGroupId());
                ct.setEventLineId(cartItem.getEventLineId());

                Date selectedEventDate = null;
                try {
                    selectedEventDate = NvxDateUtils.parseDate(cartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                } catch (ParseException e) {
                    log.error(e.getMessage(), e);
                }
                ct.setEventDate(selectedEventDate);
            }

            ct.setUpdateCapacity(0);
            cartTickets.add(ct);
        }

        savedAxCart.setReservedTickets(cartTickets);

        if(cartTickets.size() > 0) {
            try {
                final ApiResult<List<AxRetailTicketRecord>> result = axChannelTransactionService.apiCartValidateAndReserveQuantity(channel, receiptNumber, cartTickets, accountNumber);
                if (!result.isSuccess()) {
                    log.error("Failed to perform validate and reserve quantity at AX Retail service.");
                    return new ApiResult<>(ApiErrorCodes.AxStarErrorCartValidateAndReserveQuantity);
                }
            } catch (AxChannelException e) {
                e.printStackTrace();
                return new ApiResult<>(ApiErrorCodes.CheckoutResultFailedInsufficientTickets);
            }
        }


        /*
        AX Checkout
         */
        final AxStarServiceResult<AxStarCartNested> axStarCheckoutResult = axStarService.apiCheckout(channel,
                savedAxCart.getId(), savedAxCart.getCustomerId(), receiptNumber);

        if (!axStarCheckoutResult.isSuccess()) {
            //revert the reservation...

            revertAxReservation(channel, receiptNumber, savedAxCart, accountNumber);

            log.info("Error encountered on AxStarCheckout " + JsonUtil.jsonify(axStarCheckoutResult));
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartCheckout);
        }

        //set the checkout cart now
        AxStarCart checkoutCart = axStarCheckoutResult.getData().getCart();
        savedAxCart.setCheckoutCart(checkoutCart);

        //save to the DB for complete sales
        PPMFLGAxCheckoutCart axCheckoutCart = new PPMFLGAxCheckoutCart();
        axCheckoutCart.setCheckoutCartJson(JsonUtil.jsonify(checkoutCart));
        axCheckoutCart.setReceiptNum(receiptNumber);
        axCheckoutCart.setSessionCartJson(JsonUtil.jsonify(savedCart));
        axCheckoutCartRepo.save(axCheckoutCart);

        return new ApiResult<>(true, "", "", null);
    }


    /**
     * Todo:ok
     *   Uncomment the code, then update it based on what we have right now.
     *   For this to work, you need the already have a min qty specified for each partner
     *   For the SysParam, I already added in the code in the DefaultSystemParamService.java
     * @param account
     * @param ip
     * @param cartItems
     * @return
     */
    public ApiResult<InventoryTransactionVM> validateCheckOut(PartnerAccount account, String ip, List<FunCartItem> cartItems){
        final Date now = new Date();
        BigDecimal cartTotalAmount = BigDecimal.ZERO;
        Integer cartMainQty = 0;
        String tempScheId = null;

//        boolean allEventItem = true;
        boolean allEventSameSchedId = true;
        for (FunCartItem cart : cartItems) {
            final int qty = cart.getQty();
            final BigDecimal price = cart.getPrice().multiply(new BigDecimal(qty));

            cartTotalAmount = cartTotalAmount.add(price);
            if (!cart.isTopup()) {
                cartMainQty += qty;
            }
            if(tempScheId == null){
                tempScheId = cart.generateEventSched();
            }

            if(tempScheId!=null&&!tempScheId.equals(cart.generateEventSched())){
                allEventSameSchedId = false;
            }

            Node cmsProductNode = starfn.getCMSProduct(StoreApiChannels.PARTNER_PORTAL_MFLG.code, cart.getCmsProductId());
            if (ProductLevels.Exclusive.toString().equals(
                    PropertyUtil.getString(cmsProductNode, CMSProductProperties.ProductLevel.getPropertyName()))) {

                Long minQty =  PropertyUtil.getLong(cmsProductNode, CMSProductProperties.MinQty.getPropertyName());

                if(minQty != null && minQty > qty) {
                    return new ApiResult<>(false, ApiErrorCodes.AddCartResultFailedItemMinQty.code,
                            ApiErrorCodes.AddCartResultFailedItemMinQty.message.replace("{0}", minQty.toString()), null);
                }
            }
        }
//
//        //VALIDATE: allow only One Event Item per transaction, validate in cart already
//        if(!allEventSameSchedId){
//            return new CheckoutResult(CheckoutResult.Result.Failed);
//        }
//
//
//        //VALIDATE: Total B2B System Per day Transaction Limit
//        final String maxDailyTxnLimitText = purchaseSysParams.get(SysParamConst.PurchaseMaxDailyTxnLimit.toString());
//        if (StringUtils.isNotEmpty(maxDailyTxnLimitText)) {
//            try {
//                final BigDecimal maxDailyTxnLimit = NvxUtil.parseBigDecimal(maxDailyTxnLimitText, NvxUtil.BIG_DECIMAL_SYSPARAM_FORMAT);
//                if (!maxDailyTxnLimit.equals(BigDecimal.ZERO)) {
//                    final BigDecimal totalAmtSiteWideToday = this.transDao.calculateTotalAmountTransactedForDate(now);
//                    if (totalAmtSiteWideToday.add(cartTotalAmount).compareTo(maxDailyTxnLimit) > 0) {
//                        return new CheckoutResult(CheckoutResult.Result.FailedPurchaseMaxDailyTxnLimit);
//                    }
//                }
//            } catch (ParseException e) {
//                log.error("maxDailyTxnLimitText cannot be parsed whoa. Skipping.");
//            }
//        }
//
//        //VALIDATE: Total Partner Per day Transaction Limit
//        TAMainAccount mainAcc = null;
//        if (isAdmin) {
//            mainAcc = (TAMainAccount) user;
//        } else {
//            mainAcc = ((TASubAccount) user).getMainUser();
//        }
//        BigDecimal dayTransCap = mainAcc.getProfile().getDailyTransCap();
//        BigDecimal usedAmount = inventoryTransactionService
//                .getUsedTransAmountToday(mainAcc.getId());
//        if (dayTransCap != null
//                && usedAmount.add(cartTotalAmount).compareTo(dayTransCap) > 0) {
//            log.info("Partner reached max daily transaction volume: "+mainAcc.getId());
//            return new CheckoutResult(
//                    CheckoutResult.Result.FailedPurchaseMaxDailyTxnLimit);
//        }
//
//        //VALIDATE: System shall be able to limit the maximum amount per transaction.
//        final String maxAmountPerTxnText = purchaseSysParams.get(SysParamConst.PurchaseMaxAmountPerTxn.toString());
//        if (StringUtils.isNotEmpty(maxAmountPerTxnText)) {
//            try {
//                final BigDecimal maxAmountPerTxn = NvxUtil.parseBigDecimal(maxAmountPerTxnText, NvxUtil.BIG_DECIMAL_SYSPARAM_FORMAT);
//                if (!maxAmountPerTxn.equals(BigDecimal.ZERO)) {
//                    if (cartTotalAmount.compareTo(maxAmountPerTxn) > 0) {
//                        return new CheckoutResult(CheckoutResult.Result.FailedPurchaseMaxAmountPerTxn);
//                    }
//                }
//            } catch (ParseException e) {
//                log.error("maxAmountPerTxnText cannot be parsed whoa. Skipping.");
//            }
//        }
//
//        //VALIDATE: System shall be able to limit the maximum amount per transaction.
//        final String minQtyPerTxnText = purchaseSysParams.get(SysParamConst.PurchaseMinQtyPerTxn.toString());
//        if (StringUtils.isNotEmpty(minQtyPerTxnText)) {
//            try {
//                final Integer minQtyPerTxn = new Integer(minQtyPerTxnText);
//                if (minQtyPerTxn != 0) {
//                    if (cartMainQty < minQtyPerTxn) {
//                        return new CheckoutResult(CheckoutResult.Result.FailedPurchaseMinQtyPerTxn);
//                    }
//                }
//            } catch (NumberFormatException e) {
//                log.error("minQtyPerTxnText cannot be parsed whoa. Skipping.");
//            }
//        }
//        return new CheckoutResult(Result.Success, null);
        return new ApiResult<>(true, "", "", null);
    }

    @Override
    @Transactional
    public ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession session,  PartnerAccount partnerAccount) {
        String accountNumber = partnerAccount.getPartner().getAxAccountNumber();
        final InventoryTransactionVM txnInSession = getTransactionInSession(channel, session);
        if (txnInSession == null) {
            return new ApiResult<>(ApiErrorCodes.NoTransactionFoundInSession);
        }

        clearTransactionInSession(channel, session);
        purchaseTransactionService.processTransactionReleaseReservation(txnInSession.getId(), true);

        FunCart savedCart;

        if(cartManager.getCart(channel, session.getId()) == null) {
            PPMFLGAxCheckoutCart checkoutCart = axCheckoutCartRepo.findByReceiptNum(txnInSession.getReceiptNumber());
            savedCart = JsonUtil.fromJson(checkoutCart.getSessionCartJson(), FunCart.class);
        }else {
            savedCart = cartManager.getCart(channel, session.getId());
        }

        AxStarCart axStarCart = savedCart.getAxCart();

        //revert the reservation again;
        //TODO should only revert for non offline, payment, need to add this code in the processTransactionReleaseReservation
        revertAxReservation(channel, txnInSession.getReceiptNumber(), axStarCart, accountNumber);

        ApiResult<String> cartRecreateResult = cartRecreate(channel, savedCart);

        return cartRecreateResult;
    }


    @Override
    public InventoryTransactionVM getStoreTransactionByReceipt(StoreApiChannels channel, String receiptNumber) {
        return bookingRepository.getStoreTransactionByReceipt(receiptNumber);
    }

    @Override
    public InventoryTransactionVM getStoreTransactionBySession(StoreApiChannels channel, HttpSession session) {
        return getTransactionInSession(channel, session);
    }

    @Override
    @Transactional(readOnly = true)
    public PartnerReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session, PartnerAccount account) {
        final InventoryTransactionVM txn = getTransactionInSession(channel, session);
        if (txn == null) {
            return null;
        }

        final PPMFLGInventoryTransaction transaction = transRepo.findByReceiptNum(txn.getReceiptNumber());

        return getReceiptForDisplay(transaction, account);
    }


    @Override
    public PartnerReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber) {
        final PPMFLGInventoryTransaction txn = transRepo.findByReceiptNum(receiptNumber);
        if (txn == null) {
            return null;
        }

        System.out.println("Transaction txn.getMainAccountId = " + txn.getMainAccountId());

        //TODO get the correct PartnerAccount
        PartnerAccount account = new PartnerAccount();

        System.out.println("account = " + account);
        return getReceiptForDisplay(txn, account);
    }

    @Override
    @Transactional(readOnly = true)
    public PartnerReceiptDisplay getReceiptForDisplay(PPMFLGInventoryTransaction txn, PartnerAccount account) {
        final PartnerReceiptDisplay rd = new PartnerReceiptDisplay();

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();
        for (PPMFLGInventoryTransactionItem item : txn.getInventoryItems()) {
            final String productId = item.getProductId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();
            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();
            itemDisplay.setListingId(item.getItemListingId());
            itemDisplay.setProductCode(item.getItemProductCode());
            itemDisplay.setIsTopup(TransItemType.Topup.toString().equals(item.getTransItemType()));
            itemDisplay.setParentListingId(item.getMainItemListingId());
            itemDisplay.setType(item.getTicketType());
            itemDisplay.setName(item.getDisplayName());
            itemDisplay.setDescription(item.getDisplayDetails());
            itemDisplay.setQty(item.getQty());
            itemDisplay.setPrice(item.getUnitPrice());
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(item.getUnitPrice(), "S$ "));
            itemDisplay.setTotal(item.getSubTotal());
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getTotal(), "S$ "));
            itemDisplay.setDiscountTotal(BigDecimal.ZERO);
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getDiscountTotal(), "S$ "));
            itemDisplay.setDiscountLabel("");

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            productDisplay.reorganiseItems();
            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));
        }

        //TODO get from session
        PartnerVM partner = account.getPartner();
        partner.setOrgName(partner.getOrgName());
        partner.setAccountCode(partner.getAccountCode());
        partner.setAddress(partner.getAddress());

        rd.setPartner(partner);

        rd.setCmsProducts(cmsProducts);
        rd.setTotalMainQty(totalMainQty);
        rd.setTotalQty(totalQty);
        rd.setTotal(total);
        rd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        //       final CustomerDetails customer = new CustomerDetails();
//        customer.setDob(txn.getCustDob());
//        customer.setEmail(txn.getCustEmail());
//        customer.setMobile(txn.getCustMobile());
//        customer.setName(txn.getCustName());
//        customer.setNationality(txn.getCustNationality());
//        customer.setPaymentType(txn.getPaymentType());
//        customer.setReferSource(txn.getCustReferSource());
//        customer.setSubscribed(txn.isCustSubscribe());
//
//        customer.setIdType(txn.getCustIdType());
//        customer.setIdNo(txn.getCustIdNo());
//        customer.setSalutation(txn.getCustSalutation());
//        customer.setCompanyName(txn.getCustCompanyName());


        rd.setDateOfPurchase(NvxDateUtils.formatDate(txn.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        rd.setReceiptNumber(txn.getReceiptNum());
        rd.setPaymentType(txn.getPaymentType());
        rd.setTransStatus(txn.getTmStatus());
        rd.setTotalTotal(rd.getTotal());
        rd.setTotalTotalText(rd.getTotalText());
        rd.setValidityStartDate(NvxDateUtils.formatDate(txn.getValidityStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        rd.setValidityEndDate(NvxDateUtils.formatDate(txn.getValidityEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY)); //TODO should be from AX
        rd.setUsername(account.getUsername()); //TODO get from session;

        return rd;
    }


//    @Override
//    @Transactional
//    public ApiResult<InventoryTransactionVM> completeSale(StoreApiChannels channel, String emailAddress, HttpSession session) {
//
//        //TODO should be getting from the DB or session BUT BUT hmm anyways to check with Jensen
//        //TODO This is simplified validation of sale completion. May need to add more complex validation.
//        final InventoryTransactionVM txn = getTransactionInSession(channel, session);
//        if (txn == null) {
//            return new ApiResult<>(ApiErrorCodes.General);
//        }
//
//        final FunCart savedCart = cartManager.getCart(channel, session.getId());
////        String axCartId = savedCart.getAxCart().getId();
//
//        final ApiResult<InventoryTransactionVM> result = completeSale(channel, txn.getReceiptNumber(), emailAddress, savedCart.getAxCart().getCheckoutCart());
//        if (result.isSuccess()) {
//            final String sid = session.getId();
//            //final FunCart cart = cartManager.getCart(channel, sid);
//            //axStarService.apiCartDeleteEntireCart(channel, cart.getAxCart().getId(), cart.getAxCustomerId());
//            cartManager.removeCart(channel, sid);
//
//            saveTransactionInSession(channel, session, result.getData()); //save it
//        }
//
//        return result;
//    }


    @Transactional
    @Override
    public ApiResult<InventoryTransactionVM> createSalesOrder(StoreApiChannels channel, PPMFLGInventoryTransaction txn, String emailAddress, AxStarCart checkoutCart) {

        final Map<String, PPMFLGInventoryTransactionItem> axLineCommentMap = new HashMap<>();

        List<PPMFLGInventoryTransactionItem> itemList = itemRepo.findByTransactionId(txn.getId());

        for(PPMFLGInventoryTransactionItem item: itemList) {
            axLineCommentMap.put(item.getOtherDetails(), item);
        }


        List<AxStarCartLine> cartLines = checkoutCart.getCartLines();
        final Map<String, AxStarCartLine> cartLineCommentMap = new HashMap<>();
        for(AxStarCartLine cartLine : cartLines) {
            cartLineCommentMap.put(cartLine.getComment(), cartLine);
        }

        AxStarInputSalesOrderCreation salesOrderCreation = new AxStarInputSalesOrderCreation();
        salesOrderCreation.setCartId(checkoutCart.getId());
        salesOrderCreation.setCustomerId(checkoutCart.getCustomerId());
        salesOrderCreation.setSalesOrderNumber(txn.getReceiptNum());
        salesOrderCreation.setEmail(emailAddress);

        final Map<String, String> soExtProps = new HashMap<>();
        soExtProps.put("SalesPoolId", "Direct");
        salesOrderCreation.setStringExtensionProperties(soExtProps);

        AxStarInputTenderDataLine tenderDataLine = new AxStarInputTenderDataLine();
        tenderDataLine.setAmount(txn.getTotalAmount());

        //TODO TEMPORARY ONLY, PLEASE REMOVE
        String tenderType = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.tenderType");

        if(StringUtils.isEmpty(tenderType)) {
            tenderType = "Credit Card (B2B/B2C)"; //default
            //tenderDataLine.setTenderType("Credit Card (B2B/B2C)"); //TODO put to JCR better
            //tenderDataLine.setTenderType("VMDBS");
        }


        tenderDataLine.setTenderType(tenderType);
        salesOrderCreation.setTenderDataLines(Collections.singletonList(tenderDataLine));

        final AxStarServiceResult<AxStarSalesOrder> axStarSalesOrderResult = axStarService.apiSalesOrderCreate(channel, salesOrderCreation) ;
        if (!axStarSalesOrderResult.isSuccess()) {
            log.info("Error encountered on AxStarCreateSalesOrder. " + JsonUtil.jsonify(axStarSalesOrderResult));
            //TODO errHandleng
            txn.setStatus(TicketStatus.Failed.toString());
            return new ApiResult<>(ApiErrorCodes.AxStarErrorSalesOrderCreate);
        }

        //now confirm sales order
        AxInsertOnlineReference onlineRef = new AxInsertOnlineReference();
        onlineRef.setTransactionId(txn.getReceiptNum());
        onlineRef.setTransDate(new Date());
        try {
            ApiResult<String> confirmSalesOrderRes =  axChannelTransactionService.apiInsertOnlineReference(channel, onlineRef);;
            if(!confirmSalesOrderRes.isSuccess())  {
                txn.setStatus(TicketStatus.Failed.toString());
                return new ApiResult<>(ApiErrorCodes.AxStarErrorConfirmSalesOrder);
            }
        }catch (Exception e) {
            log.error(e.getMessage(), e);
            txn.setStatus(TicketStatus.Failed.toString());
            return new ApiResult<>(ApiErrorCodes.AxStarErrorConfirmSalesOrder);
        }

        //TODO use AX sales order to populate next few calls
        //If ax star sales order is successful, save the info to the database
        AxStarSalesOrder axStarSalesOrder = axStarSalesOrderResult.getData();


        final List<AxStarSalesLine> soLines = axStarSalesOrder.getSalesLines();

        try {
            for (int i = 0; i < soLines.size(); i++) {
                final AxStarSalesLine soLine = soLines.get(i);
                final AxStarCartLine coLine = cartLines.get(i);
                if ((soLine.getComment().equals(coLine.getComment())) && (soLine.getQuantity().intValue() == coLine.getQuantity())) {
                    soLine.setLineId(coLine.getLineId());
                } else {
                    throw new Exception("Not a perfect match of SO line and cart line. Reverting to previous approach.");
                }
            }
        } catch (Exception e) {
            log.warn("Error encountered matching SO and cart line normally. Proceeding with backup implementation.", e);

            for(AxStarSalesLine saleLine : soLines) {
                if(StringUtils.isEmpty(saleLine.getLineId())) {
                    AxStarCartLine cartLine = cartLineCommentMap.get(saleLine.getComment());
                    saleLine.setLineId(cartLine.getLineId()); //this one assign the line id that I need
                }
            }
        }

        //ADD in the COMMENT again
        for(AxStarSalesLine saleLine: soLines) {
            AxStarCartLineComment cartLineComment = new AxStarCartLineComment();
            PPMFLGInventoryTransactionItem item = axLineCommentMap.get(saleLine.getComment());
            cartLineComment.setCmsProductId(item.getProductId());
            //cartLineComment.setItemId(item.getOtherDetails()); //TODO evaluate if needed

            if(StringUtils.isNotEmpty(item.getEventGroupId())) {
                cartLineComment.setEventLineId(item.getEventLineId());
                cartLineComment.setEventGroupId(item.getEventGroupId());
                cartLineComment.setEventDate(item.getDateOfVisit());
            }

            saleLine.setComment(JsonUtil.jsonify(cartLineComment)); //replacing
        }

        PPMFLGAxSalesOrder salesOrder = new PPMFLGAxSalesOrder();
        salesOrder.setCustomerId(axStarSalesOrder.getCustomerId());
        salesOrder.setTransactionId(txn.getId());
        salesOrder.setSalesOrderId(axStarSalesOrder.getId());
        salesOrder.setSalesOrderJson(JsonUtil.jsonify(axStarSalesOrder));
        axSalesOrderRepo.save(salesOrder);

        //Store also the lineIds
        for(AxStarSalesLine salesLine: soLines) {
            PPMFLGAxSalesOrderLineNumberQuantity orderLineNumberQuantity = new PPMFLGAxSalesOrderLineNumberQuantity();
            orderLineNumberQuantity.setSalesOrderId(salesOrder.getId());
            orderLineNumberQuantity.setLineNumber(salesLine.getLineNumber());
            orderLineNumberQuantity.setLineId(salesLine.getLineId());
            orderLineNumberQuantity.setQty(salesLine.getQuantity());
            salesOrderLineNumberQuantityRepo.save(orderLineNumberQuantity);
        }

        //delete the column that is not needed anymore
        try {
            PPMFLGAxCheckoutCart axCheckoutCartFromDB = axCheckoutCartRepo.findByReceiptNum(txn.getReceiptNum());
            axCheckoutCartFromDB.setSessionCartJson(null);
            axCheckoutCartRepo.save(axCheckoutCartFromDB);
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return new ApiResult<>(true, "", "", null);
    }

    @Override
    @Transactional
    public ApiResult<CheckoutConfirmResult> checkoutConfirm(StoreApiChannels channel, HttpSession session, Trackd trackd) {
        //TODO This is simplified validation of checkout confirm. May need to add more complex validation.
        final InventoryTransactionVM txn = getTransactionInSession(channel, session);
        if (txn == null) {
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final PPMFLGInventoryTransaction invTxn = transRepo.findByReceiptNum(txn.getReceiptNumber());

        final String status = invTxn.getStatus();

        if (TicketStatus.Available.name().equals(status)) {
            log.error("Transaction " + txn.getReceiptNumber() + " has an invalid status at the confirm checkout stage: " + status);
            return new ApiResult<>(ApiErrorCodes.TransactionSuccess);
        }

        if (!TransactionStage.Reserved.name().equals(status)) {
            log.error("Transaction " + txn.getReceiptNumber() + " has an invalid status at the confirm checkout stage: " + status);
            return new ApiResult<>(ApiErrorCodes.TransactionNotReserved);
        }

        final String tmStatus = invTxn.getTmStatus();

        if (StringUtils.isNotEmpty(tmStatus)) {
            //TODO Handle pa more
            log.error("Transaction " + txn.getReceiptNumber() + " has already proceeded with payment or been processed: " + tmStatus);
            return new ApiResult<>(ApiErrorCodes.TransactionNotReserved);
        }

        final CheckoutConfirmResult ccr = new CheckoutConfirmResult();

        ccr.setTmRedirect(true);
        String enableTMService =  sysParamRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_TM_SERVICE_KEY);
        if(enableTMService != null && "false".equals(enableTMService)) {
            ccr.setTmRedirect(false);
        }


        if(ccr.isTmRedirect()) {
            final String receiptNumber = invTxn.getReceiptNum();
            final BigDecimal totalAmount = invTxn.getTotalAmount();
            final String merchantId = invTxn.getTmMerchantId();
            final TmCurrency tmCurrency = TmCurrency.SGD;


            //TODO ask Jensen what is fraud check ah?
            final TmFraudCheckPackage fraudCheckPackage = new TmFraudCheckPackage();
            fraudCheckPackage.setIp(invTxn.getIp());
            fraudCheckPackage.setEncoding(NvxUtil.DEFAULT_ENCODING);

            final TmLocale tmLocale = TmLocale.EnglishUs; //TODO apply i18n

            final TmMerchantSignatureConfig merchantSignatureConfig = new TmMerchantSignatureConfig();
            merchantSignatureConfig.setSignatureEnabled(false);

            final TelemoneyParamPackage tmParams;
            try {
                tmParams = tmParamRepository.getTmParams(channel, TmRequestType.InventoryPurchase);
            } catch (JcrRepositoryException e) {
                //TODO Additional handling
                log.error("Error retrieving telemoney parameters", e);
                return new ApiResult<>(ApiErrorCodes.General);
            }

            String returnUrl = tmParams.getTmReturnUrl();
            String statusUrl = tmParams.getTmStatusUrl();
            String returnUrlParamString = "";

            //TODO Namespace for i18n purposes
            final TmPaymentRedirectInput input = new TmPaymentRedirectInput(returnUrl, statusUrl, returnUrlParamString, receiptNumber,
                    totalAmount, merchantId, null, tmCurrency, null, tmLocale, merchantSignatureConfig);
            input.setUserField1(channel.code);
            input.setUserField2(TmRequestType.InventoryPurchase.toString());
            input.setUserField3(session.getId());

            final String redirectUrl = tmServiceProvider.constructPaymentRedirectUrl(input);

            ccr.setTmRedirect(true);
            ccr.setRedirectUrl(redirectUrl);

            //update the status
            invTxn.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.name());
            invTxn.setModifiedDate(new Date());
            transRepo.save(invTxn);
        }else {
            final TelemoneyResponse r = new TelemoneyResponse();
            r.setTmTransType(TmTransType.Sale.tmCode);
            r.setTmRefNo(invTxn.getReceiptNum());
            r.setTmUserField1(channel.code);
            r.setTmUserField2(TmRequestType.InventoryPurchase.toString());
            r.setTmUserField3(session.getId());
            r.setTmStatus(TmStatus.Success.tmCode);
            r.setTmDebitAmt(invTxn.getTotalAmount());
            r.setTmCcNum("411111xxxxxx1111");
            r.setTmPaymentType(TmPaymentType.Visa.name());
            r.setTmApprovalCode("skiptm"); //I use this value to check whether to send email in my processor class.

            session.setAttribute(PartnerPortalConst.FAKE_TELEMONEY_RESPONSE, r);
        }


        return new ApiResult<>(true, "", "", ccr);
    }

    @Override
    public ApiResult<InventoryTransactionVM> getFinalisedTransaction(StoreApiChannels channel, HttpSession session) {
        //TODO This is simplified validation of sale completion. May need to add more complex validation.
        final InventoryTransactionVM txnFromSession = getTransactionInSession(channel, session);

        if (txnFromSession == null) {
            //TODO
            return new ApiResult<>(ApiErrorCodes.General);
        }

        PPMFLGInventoryTransaction txn =  purchaseTransactionService.getTransaction(txnFromSession.getReceiptNumber(), true);

        final String stage = txn.getStatus();
        final String tmStatus = txn.getTmStatus();
        if (TicketStatus.Reserved.name().equals(stage) &&
                (EnovaxTmSystemStatus.RedirectedToTm.name().equals(tmStatus) || EnovaxTmSystemStatus.TmQuerySent.name().equals(tmStatus))) {
            //TODO
            return new ApiResult<>(true, "", "", null);
        }

        PPMFLGAxCheckoutCart axCheckoutCart = axCheckoutCartRepo.findByReceiptNum(txnFromSession.getReceiptNumber());
        FunCart cart = null;
        try {
            cart = JsonUtil.fromJson(axCheckoutCart.getSessionCartJson(), FunCart.class);
        }catch(Exception e) {
            cart = cartManager.getCart(channel, session.getId());
        }

        if (TicketStatus.Incomplete.name().equals(stage) || TicketStatus.Failed.name().equals(stage)) {
            //TODO Give more detailed error
            try {
                ApiResult<String> rebuildCartResult = cartRecreate(channel, cart);
                if(!rebuildCartResult.isSuccess()) {
                    log.error("Failed to perform rebuild cart...");
                }
            }catch(Exception e) {
                log.error(e.getMessage(), e);
            }
            return new ApiResult<>(ApiErrorCodes.PaymentFailed);
        }


        if (TicketStatus.Available.name().equals(stage)) {
            return new ApiResult<>(true, "", "",  InventoryTransactionVM.fromDataModel(txn));
        }

        //TODO
        return new ApiResult<>(ApiErrorCodes.General);
    }

//    @Override
//    @Transactional
//    public ApiResult<InventoryTransactionVM> completeSale(StoreApiChannels channel, String receiptNumber, String emailAddress, AxStarCart checkoutCart) {
//
//        //TODO should be getting from the DB or session BUT BUT hmm anyways to check with Jensen
//
//        final PPMFLGInventoryTransaction txn = transRepo.findByReceiptNum(receiptNumber);
//        if (txn == null) {
//            return new ApiResult<>(ApiErrorCodes.General);
//        }
//
//        txn.setPaymentType("Visa");
//
//        ApiResult<InventoryTransactionVM> createSalesOrderRes = createSalesOrder(channel, txn, emailAddress, checkoutCart);
//
//        if(!createSalesOrderRes.isSuccess()) {
//            return new ApiResult<>(ApiErrorCodes.AxStarErrorSalesOrderCreate); //TODO dunno what error to be sent
//        }
//
//        for(PPMFLGInventoryTransactionItem item: txn.getInventoryItems()) {
//            item.setOtherDetails("");
//            itemRepo.save(item);
//        }
//
//        txn.setTmStatus(EnovaxTmSystemStatus.Success.toString());
//        txn.setStatus(TicketStatus.Available.toString());
//        txn.setReprintCount(0);  //PurchaseTmEventHook
//
//        transRepo.save(txn);
//
//        //PurchaseTMEventHook
//        purchaseTransactionService.generateAndEmailPurchaseReceipt(txn); //check if this is working??
//
//        InventoryTransactionVM result = InventoryTransactionVM.fromDataModel(txn);
//
//        return new ApiResult<>(true, "", "", result);
//    }


    private ApiResult<List<FunCartItem>> updateRelatedTopups(StoreApiChannels channel, String sessionId, FunCart topupCart) {

        //get the savedCart, here all the topups are stored
        //get the mainItems, check what is the topups currently stored.
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        final List<FunCartItem> topupCartItems = topupCart.getItems();

        //added topups
        List<AxRetailCartTicket> cartTickets =  new ArrayList<>();
        AxStarInputUpdateCart axUpdateCart = new AxStarInputUpdateCart();
        List<AxStarInputUpdateCartItem> axUpdateCartItems = new ArrayList<>();
        axUpdateCart.setItems(axUpdateCartItems);
        axUpdateCart.setCartId(savedCart.getAxCart().getId());
        axUpdateCart.setCustomerId(savedCart.getAxCustomerId());

        AxStarInputRemoveCart axStarInputRemoveCart = new AxStarInputRemoveCart();
        axStarInputRemoveCart.setCartId(savedCart.getAxCart().getId());
        axStarInputRemoveCart.setCustomerId(savedCart.getAxCustomerId());

        List<FunCartItem> cartItemsToRemove = new ArrayList<>();
        List<FunCartItem> cartItemsToUpdate = new ArrayList<>();
        Map<String, FunCartItem> cartItemsToUpdateLineMap = new HashMap<>();

        List<String> lineIdsToRemove = new ArrayList<>();

        for(FunCartItem topupCartItem: topupCartItems) {
            //check the capacity
            AxRetailCartTicket ct = new AxRetailCartTicket();
            //default
            ct.setLineId("");
            ct.setAccountNum(topupCart.getAxCustomerId());
            ct.setTransactionId("");
            ct.setItemId(topupCartItem.getProductCode());
            ct.setListingId(0L); //no need for the listing id, just the item id will do
            ct.setQty(topupCartItem.getQty());

            if (StringUtils.isNotEmpty(topupCartItem.getEventGroupId())) {
                ct.setEventGroupId(topupCartItem.getEventGroupId());
                ct.setEventLineId(topupCartItem.getEventLineId());
                Date selectedEventDate = null;
                try {
                    selectedEventDate = NvxDateUtils.parseDate(topupCartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                } catch (ParseException e) {
                    log.error(e.getMessage(), e);
                }
                ct.setEventDate(selectedEventDate);
                ct.setUpdateCapacity(0);
            }
            cartTickets.add(ct);

            try {
                final ApiResult<List<AxRetailTicketRecord>> result = axChannelTransactionService.apiCartValidateQuantity(channel, null, cartTickets);
                AxStarCart axStarCart = savedCart.getAxCart();
                String lineId = "";
                for(AxStarCartLine cartLine: axStarCart.getCartLines()) {
                    if(topupCartItem.getAxLineCommentId().equals(cartLine.getComment())) {
                        lineId = cartLine.getLineId();
                        break;
                    }
                }

                //if not success, remove the cart item
                if (!result.isSuccess()) {
                    lineIdsToRemove.add(lineId);
                    cartItemsToRemove.add(topupCartItem);
                    savedCart.getAxLineCommentMap().remove(topupCartItem.getAxLineCommentId()); //delete the mapping of the ax line comment
                }else {
                    cartItemsToUpdateLineMap.put(lineId, topupCartItem);
                    cartItemsToUpdate.add(topupCartItem);
                }

            } catch (AxChannelException e1) {
                log.error(e1.getMessage(), e1);
            }

            cartTickets.remove(0); //remove again
        }

        //perform all the detele topupItem


        if(lineIdsToRemove.size() > 0) {
            axStarInputRemoveCart.setLineIds(lineIdsToRemove);
            AxStarServiceResult<AxStarCartNested> removeResult = axStarService.apiCartRemoveItem(channel, axStarInputRemoveCart);
            if(!removeResult.isSuccess()) {
                log.error("Failed to perform remove item at AX Retail service.");
                //TODO i dont know what is the supposed handling for this.
            }else {
                savedCart.setAxCart(removeResult.getData().getCart());
            }
            savedCart.getItems().removeAll(cartItemsToRemove); //TODO check if this is ok???
        }

        //perform all the update topupItem
        for(String lineId: cartItemsToUpdateLineMap.keySet()) {
            AxStarInputUpdateCartItem updateCartItem = new AxStarInputUpdateCartItem();
            updateCartItem.setLineId(lineId);
            updateCartItem.setQuantity(cartItemsToUpdateLineMap.get(lineId).getQty());
            axUpdateCartItems.add(updateCartItem);
        }

        AxStarServiceResult<AxStarCartNested> updateResult = axStarService.apiCartUpdateItem(channel, axUpdateCart);
        if(!updateResult.isSuccess()) {
            log.error("Failed to perform remove item at AX Retail service.");
        }else {
            savedCart.setAxCart(updateResult.getData().getCart());
        }

        cartManager.saveCart(channel, savedCart);

        return new ApiResult<>(true, "", "", cartItemsToRemove);
    }

    @Override
    public ApiResult<PartnerFunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, FunCart topupCart) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);

        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        Map<String, FunCartItem> axLineCommentMap = savedCart.getAxLineCommentMap();

        final Map<String, FunCartItem> savedMainItemsMap = new HashMap<>();
        final Map<String, FunCartItem> savedTopupItemsMap = new HashMap<>(); //TODO lol what if topups were event products
        final List<FunCartItem> savedItems = savedCart.getItems();
        for (FunCartItem savedItem : savedItems) {
            if (savedItem.isTopup()) {
                savedTopupItemsMap.put(savedItem.getCartItemId(), savedItem);
            } else {
                savedMainItemsMap.put(savedItem.getCmsProductId() + savedItem.getListingId(), savedItem);
            }
        }


        /*
        TODO: topup validation:
        //capacity check
        //ticket validity
        //no special handling for topup that is event, seems like the current b2b dont have events that is topup
         */

        /**
         * Modified code to cater for the partner specific logic
         */
        final List<FunCartItem> updatedTopups = new ArrayList<>();
        final Map<String, Integer> addedQuantities = new HashMap<>();
        for (FunCartItem newTopupItem : topupCart.getItems()) {
            final String parentCmsProductId = newTopupItem.getParentCmsProductId();
            final String parentListingId = newTopupItem.getParentListingId();
            final String newCartItemId = newTopupItem.generateCartItemId();

            final FunCartItem parentItem = savedMainItemsMap.get(newTopupItem.getCmsProductId() + newTopupItem.getParentListingId());
            final Integer parentQty = parentItem.getQty(); //This is the quantity of the parent one, so need to copy this one
            Integer topupQty = 0;
            //TODO Actually have to check the cross-sell AX rules for this.

            final FunCartItem existingTopup = savedTopupItemsMap.get(newCartItemId);
            if (existingTopup != null) {
                topupQty = parentQty - existingTopup.getQty(); //get the newly added one
                existingTopup.setQty(parentQty);
                updatedTopups.add(existingTopup);
            } else {
                topupQty = parentQty;
                final FunCartItem newTopup = new FunCartItem();
                newTopup.setCmsProductId(newTopupItem.getCmsProductId());
                newTopup.setCmsProductName(newTopupItem.getCmsProductName());
                newTopup.setListingId(newTopupItem.getListingId());
                newTopup.setName(newTopupItem.getName());
                newTopup.setPrice(newTopupItem.getPrice());
                newTopup.setProductCode(newTopupItem.getProductCode());
                newTopup.setQty(parentQty); //should be same as parent one always
                newTopup.setType(newTopupItem.getType());

                newTopup.setEventGroupId("");
                newTopup.setEventLineId("");
                newTopup.setSelectedEventDate("");
                newTopup.setEventSessionName("");

                newTopup.setIsTopup(true);
                newTopup.setParentCmsProductId(parentCmsProductId);
                newTopup.setParentListingId(parentListingId);

                newTopup.setCartItemId(newCartItemId);

                savedItems.add(newTopup);
                savedTopupItemsMap.put(newCartItemId, newTopup);

                axLineCommentMap.put(newTopup.getAxLineCommentId(), newTopup);

                updatedTopups.add(newTopup);
            }

            if(topupQty > 0) {
                addedQuantities.put(newCartItemId, topupQty);
            }
        }

        final AxStarCart axCart = savedCart.getAxCart();
        String axCartId = "";
        final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
        final Map<String, Integer> cartLineQtyMap = new HashMap<>();
        if (axCart != null) {
            axCartId = axCart.getId();

            for (AxStarCartLine cl : axCart.getCartLines()) {
                final String productId = cl.getProductId().toString();
                List<AxStarCartLine> lineList = cartLineMap.get(productId);
                Integer currQty = cartLineQtyMap.get(productId);
                if (lineList == null) {
                    lineList = new ArrayList<>();
                    cartLineMap.put(productId, lineList);
                }
                cartLineQtyMap.put(productId, (currQty == null ? 0 : currQty) + cl.getQuantity());
                lineList.add(cl);
            }
        }


        //Handle what if there are 2 updated items with the same listing ID
        final Map<String, Integer> updatedItemQtyMap = new HashMap<>();
        final Map<String, FunCartItem> updatedFunCartItemMap = new HashMap<>();
        for (FunCartItem updatedItem : updatedTopups) {
            final String cartItemId = updatedItem.getCartItemId();
            Integer updatedQty = updatedItemQtyMap.get(cartItemId);
            if (updatedQty == null) { updatedQty = 0; }
            Integer addedQty = addedQuantities.get(updatedItem.getCartItemId());
            updatedQty += (addedQty == null ? 0 : addedQty);
            updatedItemQtyMap.put(updatedItem.getCartItemId(), updatedQty);
            updatedFunCartItemMap.put(updatedItem.getCartItemId(), updatedItem); //put the item here
        }


        //This one is needed to handle multiple cart items with the same product ID.
        //Note that AX add to cart doesn't support multiple lines with the same product ID to be added in 1 transaction.
        final Map<Long, List<AxStarInputAddCartItem>> axcItemMap = new HashMap<>();

        for (Map.Entry<String, Integer> entry : updatedItemQtyMap.entrySet()) {
            final String cartItemId = entry.getKey();
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();

            FunCartItem cartItem = updatedFunCartItemMap.get(cartItemId);
            axcItem.setComment(cartItem.getAxLineCommentId());
            axcItem.setProductId(Long.parseLong(cartItem.getListingId()));
            axcItem.setQuantity(entry.getValue());

            List<AxStarInputAddCartItem> axcItems = axcItemMap.get(axcItem.getProductId());
            if (axcItems == null) {
                axcItems = new ArrayList<>();
                axcItemMap.put(axcItem.getProductId(), axcItems);
            }

            axcItems.add(axcItem);
        }

        while (!axcItemMap.isEmpty()) {
            final AxStarInputAddCart axc = new AxStarInputAddCart();
            axc.setCartId(StringUtils.isEmpty(axCartId) ? "" : axCartId);
            axc.setCustomerId(StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());

            final List<Long> keysToRemove = new ArrayList<>();

            final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

            for (Map.Entry<Long, List<AxStarInputAddCartItem>> axcItemMapEntry : axcItemMap.entrySet()) {
                final List<AxStarInputAddCartItem> itams = axcItemMapEntry.getValue();

                final AxStarInputAddCartItem axcItem = itams.get(0);
                axcItems.add(axcItem);

                itams.remove(0);
                if (itams.isEmpty()) {
                    keysToRemove.add(axcItemMapEntry.getKey());
                }
            }

            for (Long itemKey : keysToRemove) {
                axcItemMap.remove(itemKey);
            }
            keysToRemove.clear();

            axc.setItems(axcItems);

            final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered rebuilding cart due to API add cart failure: " + JsonUtil.jsonify(axStarResult));
                log.info("Clearing cart contents...");
                //TODO Clear cart contents

                //TODO check this
                for (FunCartItem itemToAdd : topupCart.getItems()) {
                    final String cartItemId = itemToAdd.generateCartItemId();
                    final FunCartItem savedItem = savedTopupItemsMap.get(cartItemId);
                    final int originalQty = savedItem.getQty() - itemToAdd.getQty();
                    if (originalQty > 0) {
                        savedItem.setQty(originalQty);
                    } else {
                        savedTopupItemsMap.remove(cartItemId);
                        savedItems.remove(savedItem);
                    }
                }

                return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
            }

            final AxStarCart updatedAxCart = axStarResult.getData().getCart();

            axCartId = updatedAxCart.getId();

            if (axcItemMap.isEmpty()) {
                savedCart.setAxCart(updatedAxCart);
            }
        }


        cartManager.saveCart(channel, savedCart);

        final PartnerFunCartDisplay funCartDisplay = constructCartDisplay(savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }


}
