package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.SysParamConst;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;
import com.enovax.star.cms.commons.model.partner.ppslm.UserResult;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.commons.util.NvxUtil;
import com.enovax.star.cms.partnershared.constant.AuditAction;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by lavanya on 26/8/16.
 */
@Service
public class DefaultPartnerSharedAccountService implements IPartnerSharedAccountService {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private PPSLMTAMainAccountRepository mainAccRepo;

    @Autowired
    private PPSLMTASubAccountRepository subAccRepo;

    @Autowired
    private PPSLMPartnerRepository partnerRepo;

    @Autowired
    private PPSLMTAAccessRightsGroupRepository accessRightsGroupRepo;

    @Autowired
    private PPSLMTARightsMappingRepository rightsMappingRepo;

    @Autowired
    private IPartnerEmailService partnerEmailService;

    @Autowired
    private IAuditTrailService auditTrailService;

    @Autowired
    private PPSLMTAPrevPasswordMainRepository prevPasswordMainRepo;

    @Autowired
    private PPSLMTAPrevPasswordSubRepository prevPasswordSubRepo;

    @Autowired
    private ISystemParamService sysParamService;

    @Override
    @Transactional(readOnly = true)
    public List<PPSLMTASubAccount> getSubUsers(PPSLMTAMainAccount mainAccount,boolean wipeSensitive) {
        List<PPSLMTASubAccount> users = this.subAccRepo.findByMainUser(mainAccount);
//        for(PPSLMTASubAccount user: users){
//            _wipeSensitive(user);
//        }
        return users;
    }

    private void _wipeSensitive(PPSLMTAMainAccount user) {
        user.setPassword("");
        user.setSalt("");
        user.setPrevPws(null);
    }

    private void _wipeSensitive(PPSLMTASubAccount user) {
        user.setPassword("");
        user.setSalt("");
        user.setPrevPws(null);
    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMTASubAccount getSubUser(Integer subUserId, boolean wipeSensitive) {
        log.debug("TAAcountServiceImpl, findByUsername START->" + new Date());
        long lStartTime;
        long lEndTime;
        long difference;
        lStartTime = System.currentTimeMillis();
        final PPSLMTASubAccount user = this.subAccRepo.findById(subUserId);
        lEndTime = System.currentTimeMillis();
        difference = lEndTime - lStartTime;
        log.debug("TAAcountServiceImpl, findByUsername, find by username->" + difference);
//        if (user != null && wipeSensitive) {
//            _wipeSensitive(user);
//        }
        lStartTime = System.currentTimeMillis();
        List<PPSLMTARightsMapping> rightsMappingList = user.getRightsMapping();
        List<String> rightsMappingId = new ArrayList<>();
        for (PPSLMTARightsMapping temp : rightsMappingList) {
            PPSLMTAAccessRightsGroup group = temp.getAccessRightsGroup();
            rightsMappingId.add(group.getId().toString());
        }
        lEndTime = System.currentTimeMillis();
        difference = lEndTime - lStartTime;
        log.debug("TAAcountServiceImpl, findByUsername, access rights->" + difference);
        user.setRightsMappingId(rightsMappingId);
        log.debug("TAAcountServiceImpl, findByUsername END->" + new Date());

        return user;
    }

    @Override
    @Transactional
    public void savePreviousPassword(PPSLMTAAccount user) {
        final Date now = new Date();
        if (user instanceof PPSLMTAMainAccount) {
            final PPSLMTAPrevPasswordMain ppw = new PPSLMTAPrevPasswordMain();
            ppw.setCreatedDate(now);
            ppw.setUser((PPSLMTAMainAccount) user);
            ppw.setPassword(user.getPassword());
            ppw.setSalt(user.getSalt());
            this.prevPasswordMainRepo.save(ppw);
        } else {
            final PPSLMTAPrevPasswordSub ppw = new PPSLMTAPrevPasswordSub();
            ppw.setCreatedDate(now);
            ppw.setUser((PPSLMTASubAccount) user);
            ppw.setPassword(user.getPassword());
            ppw.setSalt(user.getSalt());
            this.prevPasswordSubRepo.save(ppw);
        }
    }

    @Override
    @Transactional
    public ResultVM resetPassword(Integer userId, String newPassword, String encodedPassword, String updatedBy) throws Exception{
        ResultVM resultVM = new ResultVM();
        final Date now = new Date();
        final PPSLMTASubAccount user = this.subAccRepo.findById(userId);
        if(user == null) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.PartnerNotFound.message);
        }
        savePreviousPassword(user);


        user.setPassword(encodedPassword);
        user.setSalt("NA");
        user.setPwdLastModDt(now);
        user.setPwdAttempts(0);
        user.setPwdForceChange(true);

        user.setModifiedBy(updatedBy);
        user.setModifiedDate(now);

        this.subAccRepo.save(user);

       /* final Map<String, String> params = new HashMap<String, String>();
        params.put(":AccountHolderName", user.getName());
        params.put(":AccountLoginName", user.getUsername());
        params.put(":Password", password);
        params.put(":FaqLink", emailFaqLink);

        String mailBody = this.mailService.constructMailTemplate("ResetSubAccountPassword.html", params);
        final String email = user.getEmail();

        final MailProperties mailProps = new MailProperties("passwordReset-" + user.getUsername() + "-" + now.getTime(),
                new String[] { email }, null, null, emailSubjResetSubPassword, mailBody, true,
                this.mailService.getDefaultSender());
        try {
            this.mailService.sendEmail(mailProps, new MailCallback() {
                @Override
                public void complete(String trackingId, boolean success) {
                    if (success) {
                        log.info("Successfully sent Reset Password email with ID: " + trackingId);
                    } else {
                        log.error("Failed sending Reset Password email with ID: " + trackingId);
                    }
                }
            });
        } catch (MessagingException me) {
            throw new RuntimeException("Error with email sending", me);
        }*/
        //send email
        try {
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(user);
            PPSLMTAMainAccount mainAcc = user.getMainUser();
            if(mainAcc!=null) {
                partnerAccountVM.setMainAccountLoginName(mainAcc.getUsername());
            }
            partnerAccountVM.setPassword(newPassword);
   ///         partnerAccountVM.setFaqLink(para);
            partnerEmailService.sendPartnerSubaccountPasswordResetEmail(partnerAccountVM);
        }catch (Exception e) {
            throw new RuntimeException("Error with email sending", e);
        }
        final AuditAction action = AuditAction.ResetPassword;
        String auditDetails = String.format("User {%d:%s} password reset by %s.", user.getId(), user.getUsername(),
                updatedBy);

        this.auditTrailService.log(true, action, auditDetails, "SysUser", String.valueOf(user.getId()), updatedBy);
        return resultVM;
    }

    @Override
    @Transactional(readOnly=true)
    public List<PPSLMTAAccessRightsGroup> getAccessRightsGroup() {
        return accessRightsGroupRepo.findByType(PartnerPortalConst.GROUPS_FOR_SUB);
    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMTAAccount getUser(String username, boolean wipeSensitive) {
        if (username.endsWith(PartnerPortalConst.TA_MAIN_ACC_POSTFIX)) {
            return getMainUser(username, wipeSensitive, false);
        } else {
            return getSubUser(username, wipeSensitive, false);
        }

    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMTASubAccount getSubUser(String username, boolean wipeSensitive, boolean withAccessRights) {
        final PPSLMTASubAccount user = this.subAccRepo.findFirstByUsername(username);
        if(withAccessRights) {
            final Set<String> permissions = new HashSet<>();
            List<PPSLMTARightsMapping> rightsMappingList = user.getRightsMapping();
            for(int i=0;i<rightsMappingList.size();i++) {
                PPSLMTAAccessRightsGroup accessRightsGroup = rightsMappingList.get(i).getAccessRightsGroup();
                List<PPSLMTAAccessRightsGroupMapping> accessRightsGroupMappingList = accessRightsGroup.getAccessRightsGroupMapping();
                for(int j=0;j<accessRightsGroupMappingList.size();j++) {
                    PPSLMTAAccessRight accessRight = accessRightsGroupMappingList.get(j).getAccessRight();
                    permissions.add(accessRight.getName());
                    if(StringUtils.isNotEmpty(accessRight.getTopNavName())){
                        permissions.add(accessRight.getTopNavName());
                    }
                }
            }
            user.setAccessRights(permissions);
        }
        /*if (user != null && wipeSensitive) {
            _wipeSensitive(user);
        }*/
        return user;

    }

    @Override
    @Transactional(readOnly = true)
    public PPSLMTAMainAccount getMainUser(String username, boolean wipeSensitive, boolean withAccessRights) {

        final PPSLMTAMainAccount user = this.mainAccRepo.findFirstByUsername(username);
        if(withAccessRights) {
            final Set<String> permissions = new HashSet<>();
            List<PPSLMTAMainRightsMapping> rightsMappingList = user.getRightsMapping();
            for(int i=0;i<rightsMappingList.size();i++) {
                PPSLMTAAccessRightsGroup accessRightsGroup = rightsMappingList.get(i).getAccessRightsGroup();
                List<PPSLMTAAccessRightsGroupMapping> accessRightsGroupMappingList = accessRightsGroup.getAccessRightsGroupMapping();
                for(int j=0;j<accessRightsGroupMappingList.size();j++) {
                    PPSLMTAAccessRight accessRight = accessRightsGroupMappingList.get(j).getAccessRight();
                    permissions.add(accessRight.getName());
                    if(StringUtils.isNotEmpty(accessRight.getTopNavName())){
                        permissions.add(accessRight.getTopNavName());
                    }
                }
            }
            user.setAccessRights(permissions);
        }
        /*if (user != null && wipeSensitive) {
            _wipeSensitive(user);
        }*/
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean exceedSubUsers(Integer mainAccountId, Integer userId) {

        final int activeSubAccount = sysParamService.getMaxNoOfSubUsers();
        PPSLMTAMainAccount mainAccount = mainAccRepo.findById(mainAccountId);
        int count = subAccRepo.find(mainAccount,userId).size();

        if (count >= activeSubAccount) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public UserResult saveSubUser(boolean isNew, Integer userId, String username, String pwd, String encodedPwd, String email, String[] access,
                                  String status, String createdBy, String name, Integer mainAccountId, String title) throws Exception {
        final Date now = new Date();
        final PPSLMTASubAccount user = isNew ? new PPSLMTASubAccount() : this.subAccRepo.findById(userId);

        String auditDetails = "";

        if (isNew) {
            user.setUsername(username);
            user.setPassword(encodedPwd);
            user.setPwdLastModDt(now);
            user.setCreatedBy(createdBy);
            user.setCreatedDate(now);
            user.setPwdForceChange(true);

            PPSLMTAMainAccount mainAcc = new PPSLMTAMainAccount();
            mainAcc.setId(mainAccountId);
            user.setMainUser(mainAcc);

        } else {
            List<PPSLMTARightsMapping> rightsMapping = user.getRightsMapping();
            StringBuffer sb = new StringBuffer();
            for (PPSLMTARightsMapping right : rightsMapping) {
                PPSLMTAAccessRightsGroup accessRightsGroup = right.getAccessRightsGroup();
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(accessRightsGroup.getId());

            }

            auditDetails = String.format(
                    "[Previous Values] {accessRightsGroup:%s}, {email:%s}, {pwdAttempts:%d}, {status:%s}",
                    sb.toString(), user.getEmail(), user.getPwdAttempts(), user.getStatus());

            // delete the old RightsMapping
            for (PPSLMTARightsMapping right : rightsMapping) {
                rightsMappingRepo.delete(right);
                //rightsMappingRepo.flush();
            }

            // add the new RightsMapping
            for (int i = 0; i < access.length; i++) {
                PPSLMTARightsMapping right = new PPSLMTARightsMapping();
                PPSLMTAAccessRightsGroup accessRightsGroup = accessRightsGroupRepo.findById(new Integer(access[i]));
                right.setSubUser(user);
                right.setAccessRightsGroup(accessRightsGroup);
                rightsMappingRepo.save(right);
                //rightsMappingRepo.flush();
            }
            user.setPwdForceChange(false);
//            user.setRightsMapping(null);
//            user.setRightsMappingId(null);
        }

        user.setEmail(email);
        user.setPwdAttempts(0);
        user.setStatus(status);
        user.setName(name);
        user.setTitle(title);
        user.setSalt("NA");
        user.setModifiedBy(createdBy);
        user.setModifiedDate(now);

        subAccRepo.save(user);
        //subAccRepo.flush();
        if (isNew) {
            // add the new RightsMapping
            for (int i = 0; i < access.length; i++) {
                PPSLMTARightsMapping right = new PPSLMTARightsMapping();
                PPSLMTAAccessRightsGroup accessRightsGroup = new PPSLMTAAccessRightsGroup();
                accessRightsGroup.setId(new Integer(access[i]));
                right.setSubUser(user);
                right.setAccessRightsGroup(accessRightsGroup);
                rightsMappingRepo.save(right);
                //rightsMappingRepo.flush();
            }

            // send email
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(user);
            PPSLMTAMainAccount mainAcc = mainAccRepo.findById(mainAccountId);
            if(mainAcc!=null) {
                partnerAccountVM.setMainAccountLoginName(mainAcc.getUsername());
            }
            partnerAccountVM.setPassword(pwd);
           /// partnerAccountVM.setFaqLink(PartnerPortalConst.FAQ_LINK);
            partnerEmailService.sendPartnerSubaccountCreationEmail(partnerAccountVM);


        }

        final AuditAction action = isNew ? AuditAction.AccountCreate : AuditAction.AccountUpdate;
        if (isNew) {
            auditDetails = String.format("User {%d:%s} created.", user.getId(), user.getUsername());
        } else {
            auditDetails = String.format("User {%d:%s} updated. " + auditDetails, user.getId(), user.getUsername());
        }
        this.auditTrailService.log(true, action, auditDetails, "SysUser", String.valueOf(user.getId()), createdBy);

        return new UserResult(UserResult.Result.Success, user);
    }


}