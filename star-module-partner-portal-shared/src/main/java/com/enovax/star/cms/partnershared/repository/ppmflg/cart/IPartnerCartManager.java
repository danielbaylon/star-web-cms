package com.enovax.star.cms.partnershared.repository.ppmflg.cart;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.FunCart;

/**
 * Created by jennylynsze on 9/30/16.
 */
public interface IPartnerCartManager {
    void saveCart(StoreApiChannels channel, FunCart cartToSave);

    FunCart getCart(StoreApiChannels channel, String cartId);

    void removeCart(StoreApiChannels channel, String cartId);
}
