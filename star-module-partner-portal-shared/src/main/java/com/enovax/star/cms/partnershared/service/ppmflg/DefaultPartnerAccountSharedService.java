package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerAccountVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.UserResult;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 8/9/16.
 */
@Service("PPMFLGIPartnerAccountSharedService")
public class DefaultPartnerAccountSharedService implements IPartnerAccountSharedService {

    @Autowired
    private PPMFLGTAMainAccountRepository mainAccRepo;

    @Autowired
    private PPMFLGTASubAccountRepository subAccRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerSharedService")
    private IPartnerSharedService paSharedSrv;

    @Autowired
    private PPMFLGPartnerRepository paRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerSharedAccountService")
    private IPartnerSharedAccountService paSharedAccSrv;

    @Autowired
    private PPMFLGTARightsMappingRepository taRightMapRepo;

    @Autowired
    private PPMFLGTAAccessRightsGroupRepository acgRepo;

    @Override
    @Transactional
    public ApiResult<String> savePartnerSubAccountRightsChanges(Integer mainAccId, String loginUserName, List<PartnerAccountVM> accounts) throws BizValidationException {
        if(accounts != null && accounts.size() > 0){
            for(PartnerAccountVM a : accounts){
                PPMFLGTASubAccount acc = subAccRepo.findById(a.getId());
                if(acc == null){
                    throw new BizValidationException("Invalid request, sub-account not found.");
                }
                if(acc.getMainUser().getId().intValue() != mainAccId.intValue()){
                    throw new BizValidationException("Invalid request, main-account and sub-account are not match.");
                }
                List<PPMFLGTARightsMapping> mappings = acc.getRightsMapping();
                List<Integer> rightIds = a.getRightsIds();
                if(rightIds == null || rightIds.size() == 0){
                    if(mappings != null){
                        for(PPMFLGTARightsMapping m : mappings){
                            taRightMapRepo.delete(m);
                        }
                    }
                }else{
                    // remove rights which not found in list
                    if(mappings != null){
                        for(PPMFLGTARightsMapping m : mappings){
                            if(rightIds.contains(m.getId())){
                                rightIds.remove(m.getId());
                            }else{
                                taRightMapRepo.delete(m);
                            }
                        }
                    }
                    for(Integer rightId : rightIds){
                        PPMFLGTAAccessRightsGroup acg = acgRepo.findById(rightId);
                        if(acg == null || (!"S".equalsIgnoreCase(acg.getType()))){
                            throw new BizValidationException("Invalid request, access configuration not found");
                        }
                        PPMFLGTARightsMapping mapping = new PPMFLGTARightsMapping();
                        mapping.setAccessRightsGroup(acg);
                        mapping.setSubUser(acc);
                        taRightMapRepo.save(mapping);
                    }
                }
            }
        }
        return new ApiResult<>(true, "", "", "");
    }

    @Transactional(readOnly = true)
    public String getPartnerAccountNameByIdWithCache(Map<String, String> nameMapping, boolean subAccountTrans, String createdBy) {
        if(createdBy == null || createdBy.trim().length() == 0 | nameMapping == null){
            return null;
        }
        int accountId = -1;
        try{
            accountId = Integer.parseInt(createdBy.trim());
        }catch (Exception ex){
            return null;
        }
        String key = (subAccountTrans ? "S" : "M")+createdBy;
        if(nameMapping.containsKey(key)){
            return nameMapping.get(key);
        }

        PPMFLGTAAccount account = null;
        if(subAccountTrans){
            account = subAccRepo.findById(accountId);
        }else{
            account = mainAccRepo.findById(accountId);
        }
        if(account != null){
            nameMapping.put(key, account.getName());
        }
        return nameMapping.get(key);
    }

    @Transactional(readOnly = true)
    public PPMFLGTAMainAccount getActivePartnerMainAccountByPartnerId(Integer partnerId) {
        if(partnerId == null || partnerId.intValue() == 0){
            return null;
        }
        PPMFLGPartner partner = paSharedSrv.getPartnerById(partnerId);
        if(partner == null){
            return null;
        }
        if(!PartnerStatus.Active.code.equals(partner.getStatus())){
            return null;
        }
        return partner.getMainAccount();
    }

    @Transactional(readOnly = true)
    public PPMFLGTAMainAccount getActivePartnerByMainAccountId(Integer mainAccountId) {
        if(mainAccountId == null || mainAccountId.intValue() == 0){
            return null;
        }
        return mainAccRepo.findById(mainAccountId);
    }

    @Transactional(readOnly = true)
    public PPMFLGTAMainAccount getPartnerMainAccountByMainAccountId(Integer mainAccId) {
        if(mainAccId == null || mainAccId.intValue() == 0){
            return null;
        }
        return mainAccRepo.findById(mainAccId);
    }



    @Override
    @Transactional
    public com.enovax.star.cms.commons.model.api.ApiResult<String> saveSubuser(int paId, boolean isNew, Integer userId, String userName, String name, String email, String title, String status, String access, String createdBy, String pwd, String encodedPwd) throws Exception{
        PPMFLGPartner pa = paRepo.findById(paId);
        Integer mainAccId = pa.getMainAccount().getId();
        if(isNew) {
            userName = pa.getAccountCode() + "_" + userName;
        }
        if(isNew && paSharedAccSrv.getUser(userName, false)!= null) {
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.SubAccountAlreadyExists);
        }
        String[] s;
        if(access==null || access.isEmpty()){
            s = new String[0];
        }else{
            s = access.split(",");
        }
        if(paSharedAccSrv.exceedSubUsers(mainAccId, userId)){
            return new com.enovax.star.cms.commons.model.api.ApiResult<>(ApiErrorCodes.MaxSubAccountExceeded);
        }
        final UserResult result = paSharedAccSrv.saveSubUser(isNew, userId, userName, pwd, encodedPwd, email, s, status, createdBy, name, mainAccId, title);

        List<PPMFLGTASubAccount> accounts =  paSharedAccSrv.getSubUsers(pa.getMainAccount(),true);

        List<PartnerAccountVM> accVms = new ArrayList<PartnerAccountVM>();
        for(PPMFLGTASubAccount acc:accounts){
            //Hibernate.initialize(acc.getRightsMapping());
            PPMFLGTASubAccount tmpObj = paSharedAccSrv.getSubUser(acc.getId(), true);
            ArrayList<Integer> rightsIds = new ArrayList<Integer>();
            PartnerAccountVM partnerAccountVM = new PartnerAccountVM(tmpObj);
            List<PPMFLGTARightsMapping> rightsMappingList = taRightMapRepo.findBySubUser(tmpObj);
            if(rightsMappingList!=null){
                for(PPMFLGTARightsMapping temp: rightsMappingList){
                    PPMFLGTAAccessRightsGroup group = temp.getAccessRightsGroup();
                    rightsIds.add(group.getId());
                }
            }
            partnerAccountVM.setRightsIds(rightsIds);
            accVms.add(partnerAccountVM);
        }
        String accountsJson = JsonUtil.jsonify(accVms);
        return new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", accountsJson);
    }

    @Transactional
    public boolean hasSubAccountAdminPermission(Integer mainAccId, Integer loginId, boolean isSubAccount){
        if(mainAccId == null){
            return false;
        }
        if(isSubAccount){
            PPMFLGTASubAccount subAccount = subAccRepo.findById(loginId);
            if(subAccount == null){
                return false;
            }
            if(subAccount.getMainUser().getId().intValue() != mainAccId.intValue()){
                return false;
            }
            List<PPMFLGTARightsMapping> mappings = subAccount.getRightsMapping();
            if(mappings != null && mappings.size() > 0){
                for(PPMFLGTARightsMapping m : mappings){
                    if(m != null){
                        PPMFLGTAAccessRightsGroup acg = m.getAccessRightsGroup();
                        if("G5".equalsIgnoreCase(acg.getName())){
                            return true;
                        }
                    }
                }
            }
        }else{
            PPMFLGTAMainAccount mainAccount = mainAccRepo.findById(loginId);
            if(mainAccount == null){
                return false;
            }
            if(mainAccount.getId().intValue() != mainAccId.intValue()){
                return false;
            }
            List<PPMFLGTAMainRightsMapping> mappings = mainAccount.getRightsMapping();
            if(mappings != null && mappings.size() > 0){
                for(PPMFLGTAMainRightsMapping m : mappings){
                    if(m != null){
                        PPMFLGTAAccessRightsGroup acg = m.getAccessRightsGroup();
                        if(acg != null){
                            if("G5".equalsIgnoreCase(acg.getName())){
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
