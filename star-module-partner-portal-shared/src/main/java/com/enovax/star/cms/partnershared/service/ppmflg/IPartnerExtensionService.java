package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPaDistributionMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTable;

import java.text.ParseException;
import java.util.List;

/**
 * Created by houtao on 12/8/16.
 */
public interface IPartnerExtensionService {

    boolean updatePartnerExtProperties(AxCustomerTable c, boolean enablePartnerPortal);

    public AxCustomerTable popuplateCustoemrTable(PPMFLGPartner pa, boolean enablePartnerPortal, boolean allowOnAccount, List<PPMFLGPADocMapping> docMap, List<PPMFLGPaDistributionMapping> marketDistributionList, String documnetTypeId, String capacityGroupId, String lineOfBusiness) throws ParseException;

}
