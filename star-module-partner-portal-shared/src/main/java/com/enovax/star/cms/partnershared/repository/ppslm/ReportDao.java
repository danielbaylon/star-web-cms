package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.*;

import java.util.List;

/**
 * Created by lavanya on 9/11/16.
 */
public interface ReportDao {

    public List<TransQueryVM> getTransByPage(TransQueryFilterVM filter, String orderField, String order, Integer pageNumber, Integer pageSize);
    public int getTransByPageSize(TransQueryFilterVM filter);

    public List<PackageViewResultVM> getPinQueryByPage(PinQueryFilterVM ft, String orderField, String order, Integer pageNumber, Integer pageSize);
    public int getPinQueryByPageSize(PinQueryFilterVM filter);

    public ReportResult<TransReportModel> generateTransReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception;
    public ReportResult<InventoryReportModel> generateInvReport(ReportFilterVM filter, Integer skip, Integer take,
                                                                String orderBy, boolean asc, boolean isCount) throws Exception;
    public ReportResult<PackageReportModel> generatePkgReport(ReportFilterVM filter, Integer skip, Integer take,
                                                              String orderBy, boolean asc, boolean isCount) throws Exception;

    public List<ExceptionRptDetailsVM> getFinReconPage(ExceptionRptFilterVM fliter, String sortField, String sortDirection, Integer page, Integer pageSize);

    public int getFinReconSize(ExceptionRptFilterVM fliter);

    public List<ExceptionRptDetailsVM>  getFinReconBaseResult();
}
