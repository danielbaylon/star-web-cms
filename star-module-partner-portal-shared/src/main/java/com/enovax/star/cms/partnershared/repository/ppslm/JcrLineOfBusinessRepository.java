package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.GeneralStatus;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxLineOfBusiness;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerTypeVM;
import com.enovax.star.cms.commons.util.PublishingUtil;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 22/9/16.
 */

@Service
public class JcrLineOfBusinessRepository implements ILineOfBusinessRepository {

    private static final String PARTNER_TYPE = "ax-all-partner-types";

    private static volatile int list_refresh_count = 0;
    private static volatile List<AxLineOfBusiness> list = new ArrayList<AxLineOfBusiness>();

    @Override
    public boolean deleteLineOfBusinessById(String channel, String id) {
        if(id == null || id.trim().length() == 0){
            return false;
        }
        id = id.trim();
        try {
            Node node =  JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+ channel.toLowerCase() + "/" + PARTNER_TYPE + "/" + id);
            if(node != null && node.getName() != null && node.getName().trim().endsWith(PARTNER_TYPE)){
                throw new Exception("Deleting root node of "+PARTNER_TYPE+", deleting failed.");
            }
            PublishingUtil.deleteNodeAndPublish(node,JcrWorkspace.CMSConfig.getWorkspaceName());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean createLineOfBusiness(String channel, AxLineOfBusiness info) {
        try {
            Node subnode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase() + "/"+PARTNER_TYPE+"/", info.getLineOfBusinessId(), "mgnl:content");
            if(subnode == null){
                subnode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+ channel.toLowerCase() + "/" + PARTNER_TYPE + "/" + info.getLineOfBusinessId());
            }
            if(subnode == null){
                return false;
            }
            if(subnode.hasProperty("lineOfBusinessId")){
                subnode.getProperty("lineOfBusinessId").setValue(info.getLineOfBusinessId());
            }else{
                subnode.setProperty("lineOfBusinessId", info.getLineOfBusinessId());
            }
            if(subnode.hasProperty("description")){
                subnode.getProperty("description").setValue(info.getDescription());
            }else{
                subnode.setProperty("description", info.getDescription());
            }
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), subnode);
            try{
                PublishingUtil.publishNodes(subnode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
                return true;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public AxLineOfBusiness getLineOfBusinessById(String channel, String id) {
        if(id == null || id.trim().length() == 0){
            return null;
        }
        id = id.trim();
        try {
            AxLineOfBusiness i = null;
            Node node =  JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+ channel.toLowerCase() + "/" + PARTNER_TYPE + "/" + id);
            if(node.hasProperty("lineOfBusinessId")){
                i = new AxLineOfBusiness();
                i.setLineOfBusinessId(node.getProperty("lineOfBusinessId").getValue().getString());
                if(node.hasProperty("description")){
                    i.setDescription(node.getProperty("description").getValue().getString());
                }
                return i;
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AxLineOfBusiness> getLineOfBusinessList(String channel) {
        List<AxLineOfBusiness> list = new ArrayList<AxLineOfBusiness>();
        try {
            AxLineOfBusiness i = null;
            Node root =  JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase() + "/" + PARTNER_TYPE);
            if(root != null) {
                NodeIterator iter = root.getNodes();
                if (iter != null) {
                    while(iter.hasNext()){
                        Node node = iter.nextNode();
                        if(node.hasProperty("lineOfBusinessId")){
                            i = new AxLineOfBusiness();
                            i.setLineOfBusinessId(node.getProperty("lineOfBusinessId").getValue().getString());
                            if(node.hasProperty("description")){
                                i.setDescription(node.getProperty("description").getValue().getString());
                            }
                            list.add(i);
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean existsLineOfBusinessId(String channel, String id) {
        if(id == null || id.trim().length() == 0){
            return false;
        }
        id = id.trim();
        try {
            Node node =  JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),"/"+ channel.toLowerCase() + "/" + PARTNER_TYPE + "/" + id);
            if(node.hasProperty("lineOfBusinessId") && node.getProperty("lineOfBusinessId").getValue() != null){
                if(id.equals(node.getProperty("lineOfBusinessId").getValue().getString())){
                    return true;
                }
            }
            return false;
        } catch (RepositoryException e) {}
        return false;
    }

    @Override
    public void deleteLineOfBusinessIfNotInList(String channel, List<String> idlist) {
        if(idlist == null || idlist.size() == 0){
            return;
        }
        try {
            Node node =  JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/"+channel.toLowerCase() + "/" + PARTNER_TYPE);
            NodeIterator iter = node.getNodes();
            while(iter.hasNext()){
                Node subNode = iter.nextNode();
                if(subNode.hasProperty("lineOfBusinessId")){
                    if(!idlist.contains(subNode.getProperty("lineOfBusinessId").getValue().getString())){
                        try{
                            PublishingUtil.deleteNodeAndPublish(subNode,JcrWorkspace.CMSConfig.getWorkspaceName());
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLineOfBusinessNameById(String channel, String id) {
        if (list != null && list.size() == 0) {
            if(list_refresh_count == 0) {
                refreshCache(channel);
            }
        }
        if(list != null && id != null && id.trim().length() > 0){
            for(AxLineOfBusiness i : list){
                if(id.equals(i.getLineOfBusinessId())){
                    return i.getDescription();
                }
            }
        }
        return null;
    }

    @Override
    public void refreshCache(String channel) {
        list_refresh_count = 1;
        List<AxLineOfBusiness> tempList = getLineOfBusinessList(channel);
        list.clear();
        list.addAll(tempList);
    }

    @Override
    public List<PartnerTypeVM> getPartnerTypeList(String channel) {
        if(list_refresh_count == 0){
            refreshCache(channel);
        }
        List<PartnerTypeVM> tempList = new ArrayList<PartnerTypeVM>();
        for(AxLineOfBusiness i : list){
            PartnerTypeVM v = new PartnerTypeVM();
            v.setId(i.getLineOfBusinessId());
            v.setLabel(i.getDescription());
            v.setDescription(i.getDescription());
            v.setStatus(GeneralStatus.Active.code);
            tempList.add(v);
        }
        return tempList;
    }
}
