package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.model.axstar.AxStarCustomer;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;

/**
 * Created by houtao on 28/7/16.
 */
public interface IPartnerAXService {

    public AxStarServiceResult<AxStarCustomer> registerPartner(PPMFLGPartner partner, String tierId, String custGroup, String correspondenceAddr, String correspondencePostCode, String correspondenceCity);

    void syncPartnerProfile(Integer partnerId);

    void syncSelfUpdatedPartnerProfileToAX(Integer id) throws Exception;
}
