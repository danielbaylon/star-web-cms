package com.enovax.star.cms.partnershared.constant;

/**
 * Created by houtao on 20/9/16.
 */
public enum PartnerExtAttrNameType {

    STRING,
    INT,
    DATE

}
