package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.model.partner.ppslm.ApprovalLogVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReasonVM;
import com.enovax.star.cms.commons.model.partner.ppslm.RequestApprovalVM;
import com.enovax.star.cms.commons.model.system.SysParamVM;

import java.util.List;

public interface IApprovalLogService {

    RequestApprovalVM populatePendingApprovalRequestVM(String loginAdminId);

    String approvePendingRequest(Integer appId, String loginAdminId) throws Exception;

    String rejectPendingRequest(Integer appId, ReasonVM reasonVM, String loginAdminId) throws Exception;

    public boolean hasPendingRequest(String key, String cate) throws Exception;

    List<ApprovalLogVM> getHistoryLog(Integer paId);

    PPSLMApprovalLog getLogById(Integer appId);

    PartnerVM getPartnerFromHist(String hist) throws Exception;

    public PPSLMApprovalLog logSystemParamRequest(SysParamVM sysParam, String label, String newValue,
                                                  String userNm);

    int getPendingRequestForProdCnt(String prodId);
}
