package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.WOTReservationStatus;
import com.enovax.star.cms.commons.constant.ppslm.WOTReservationType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMWingsOfTimeReservationRepository;
import com.enovax.star.cms.commons.repository.specification.builder.SpecificationBuilder;
import com.enovax.star.cms.commons.repository.specification.ppslm.PPSLMWingsOfTimeReservationSpecification;
import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.*;
import com.enovax.star.cms.partnershared.constant.ppslm.WingOfTimeChannel;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by houtao on 8/9/16.
 */
@Service
public class DefaultWingsOfTimeSharedService implements IWingsOfTimeSharedService {

    private static final Logger log = LoggerFactory.getLogger(DefaultWingsOfTimeSharedService.class);

    @Autowired
    PPSLMWingsOfTimeReservationRepository slmReservationRepo;

    @Autowired
    private PPSLMWingsOfTimeReservationRepository wotRepo;

    @Autowired
    private IPartnerAccountSharedService paAccSharedSrv;

    @Autowired
    private AxStarService axStarSrv;

    @Autowired
    private AxChannelTransactionService txnSrv;

    @Autowired
    private IWingsOfTimeLogService wotLogSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    private IPartnerSharedService paSharedSrv;

    @Autowired
    private IPartnerWoTEmailService wotMailSrv;

    @Autowired
    private IPartnerProductService prodSrv;

    @Autowired
    StarTemplatingFunctions starTemplatingFunctions;

    @Transactional
    public void sendWotUnconfirmedBackendReservationReleaseReminder(Integer paramDays) {
        try{
            log.info("WoT unconfirmed backend reservation release reminder job started");
            int days = sysParamSrv.getPartnerWoTAdvancedDaysReleaseBackendReservedTickets();
            if(paramDays != null && paramDays.intValue() > 0){
                days = paramDays;
            }
            Date now = new Date(System.currentTimeMillis());
            List<Integer> list = wotRepo.findAllUnconfirmedBackendReservationWithNonNotifyDate();
            if(list != null && list.size() > 0){
                for(Integer id : list){
                    if(id == null || id.intValue() == 0){
                        continue;
                    }
                    PPSLMWingsOfTimeReservation wot = wotRepo.findById(id);
                    if(wot == null){
                        continue;
                    }
                    if(!wot.isAdminRequest()){
                        continue;
                    }
                    if(!(wot.getQty() > wot.getQtySold())){
                        continue;
                    }
                    if(!(WOTReservationStatus.Reserved.name().equalsIgnoreCase(wot.getStatus()) || WOTReservationStatus.PartiallyPurchased.name().equalsIgnoreCase(wot.getStatus()))){
                        continue;
                    }
                    if(!(wot.getReleaseNotifiedDate() == null && wot.getReleasedDate() == null)){
                        continue;
                    }
                    if(wot.getEventDate() == null){
                        continue;
                    }

                    Date eventDate = null;
                    try {
                        String dtStr = NvxDateUtils.formatDateForDisplay(wot.getEventDate(), false);
                        eventDate = NvxDateUtils.parseDate(dtStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    }catch (Exception ex1){
                    }catch (Throwable ex2){}
                    if(eventDate == null){
                        continue;
                    }
                    Date emailDate = new Date(eventDate.getTime() - (days * 86400000l));
                    if(emailDate.compareTo(now) > 0){
                        continue;
                    }
                    PPSLMTAMainAccount mainAccount = paAccSharedSrv.getActivePartnerByMainAccountId(wot.getMainAccountId());
                    if(mainAccount == null){
                        log.error("WoT unconfirmed backend reservation release reminder job - main account not found for backend reservation id ["+wot.getId()+"]");
                    }
                    try{
                        Date releaseDate = new Date(eventDate.getTime() - ((days - 1) * 86400000l));
                        String releaseDateText = NvxDateUtils.formatDate(releaseDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                        wotMailSrv.sendingAdminWoTReleaseEmail(mainAccount, wot, wot.getCreatedBy(), releaseDateText);
                        wot = wotRepo.findById(wot.getId());
                        wot.setReleaseNotifiedDate(now);
                        updateWotUnconfirmedBackendReservation(wot);
                    }catch (Exception ex){
                        log.error("WoT unconfirmed backend reservation release reminder job - sending email/update notify date for backend reservation id ["+wot.getId()+"] failed", ex);
                    }
                }
            }
            log.info("WoT unconfirmed backend reservation release reminder job completed with success");
        }catch (Exception ex){
            log.error("WoT unconfirmed backend reservation release reminder job completed with failure", ex);
        }
    }
    @Transactional
    private void updateWotUnconfirmedBackendReservation(PPSLMWingsOfTimeReservation wot) {
        wotRepo.save(wot);
    }

    @Transactional
    public void releaseWotUnconifrmedBackendReservation(Integer paramDays) {
        try{
            log.info("WoT unconfirmed backend reservation release job started");

            int days = sysParamSrv.getPartnerWoTAdvancedDaysReleaseBackendReservedTickets();
            if(paramDays != null && paramDays.intValue() > 0){
                days = paramDays.intValue();
            }
            Date now = new Date(System.currentTimeMillis());
            List<Integer> list = wotRepo.findAllUnconfirmedBackendReservationWithNotifyDate();
            if(list != null && list.size() > 0){
                for(Integer id : list){
                    if(id == null || id.intValue() == 0){
                        continue;
                    }
                    PPSLMWingsOfTimeReservation wot = wotRepo.findById(id);
                    if(wot == null){
                        continue;
                    }
                    if(!wot.isAdminRequest()){
                        continue;
                    }
                    if(!(wot.getQty() > wot.getQtySold())){
                        continue;
                    }
                    if(!(WOTReservationStatus.Reserved.name().equalsIgnoreCase(wot.getStatus()) || WOTReservationStatus.PartiallyPurchased.name().equalsIgnoreCase(wot.getStatus()))){
                        continue;
                    }
                    if(!(wot.getReleaseNotifiedDate() != null && wot.getReleasedDate() == null)){
                        continue;
                    }
                    Date eventDate = null;
                    try {
                        String dtStr = NvxDateUtils.formatDateForDisplay(wot.getEventDate(), false);
                        eventDate = NvxDateUtils.parseDate(dtStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    }catch (Exception ex1){
                    }catch (Throwable ex2){}
                    if(eventDate == null){
                        continue;
                    }
                    Date releaseDate = new Date(eventDate.getTime() - ((days - 1) * 86400000l));
                    if(releaseDate.compareTo(now) > 0){
                        continue;
                    }
                    PPSLMTAMainAccount mainAccount = paAccSharedSrv.getActivePartnerByMainAccountId(wot.getMainAccountId());
                    if(mainAccount == null){
                        log.error("WoT unconfirmed backend reservation release job - main account not found for backend reservation id ["+wot.getId()+"]");
                    }
                    try{
                        PPSLMWingsOfTimeReservation temp = new PPSLMWingsOfTimeReservation();
                        temp.setId(wot.getId());
                        temp.setQty(0);
                        temp.setItemId(wot.getItemId());
                        temp.setReceiptNum(wot.getReceiptNum());
                        temp.setEventLineId(wot.getEventLineId());
                        temp.setEventGroupId(wot.getEventGroupId());
                        //temp.setAxCheckoutLineId(wot.getAxCheckoutLineId());
                        temp.setProductId(wot.getProductId());
                        temp.setEventDate(wot.getEventDate());
                        ApiResult<List<AxRetailTicketRecord>> retResponse = new ApiResult<List<AxRetailTicketRecord>>();
                        saveOrUpdatePreservationOrder(retResponse, mainAccount.getProfile().getAxAccountNumber(), temp, true);
                        if(retResponse.isSuccess()){
                            wot = wotRepo.findById(wot.getId());
                            wot.setStatus(WOTReservationStatus.Released.name());
                            wot.setReleasedDate(now);
                            this.updateWotUnconfirmedBackendReservation(wot);
                            String releaseDateText = NvxDateUtils.formatDate(now, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                            wotMailSrv.sendingAdminWoTPostReleaseEmailNotification(mainAccount, wot, wot.getCreatedBy(), releaseDateText);
                        }else{
                            log.error("WoT unconfirmed backend reservation release job - release backend reservation ["+wot.getId()+"] failed, response error : "+retResponse.getMessage());
                        }
                        temp = null;
                    }catch (Exception ex){
                        log.error("WoT unconfirmed backend reservation release job - sending email/update notify date for backend reservation id ["+wot.getId()+"] failed", ex);
                    }
                }
            }
            log.info("WoT unconfirmed backend reservation release job completed with success");
        }catch (Exception ex){
            log.error("WoT unconfirmed backend reservation release job completed with failure", ex);
        }
    }

    @Override
    public List<AxProduct> getWOTProducts() {
        //get all the products
        List<AxProduct> wotProducts = new ArrayList<>();
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + PartnerPortalConst.Partner_Portal_Ax_Data + "/wot-products");
            //get the children, which is the list of products
            Iterable<Node> productNodeIterable = NodeUtil.getNodes(productNode, JcrWorkspace.CMSConfig.getNodeType());
            Iterator<Node> productNodeIterator = productNodeIterable.iterator();
            while(productNodeIterator.hasNext()) {
                //do this
                Node productSubNode = productNodeIterator.next();
                AxProduct axProduct = new AxProduct();
                axProduct.setProductListingId(PropertyUtil.getLong(productSubNode, "productListingId"));
                axProduct.setDisplayProductNumber(PropertyUtil.getLong(productSubNode, "productNumber"));
                axProduct.setProductName(PropertyUtil.getString(productSubNode, "productName"));
                wotProducts.add(axProduct);
            }
            return wotProducts;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<WOTProductVM> getWOTProductsVM(StoreApiChannels channel) {
        //get all the products
        List<WOTProductVM> wotProducts = new ArrayList<>();
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + PartnerPortalConst.Partner_Portal_Ax_Data + "/wot-products");
            //get the children, which is the list of products
            Iterable<Node> productNodeIterable = NodeUtil.getNodes(productNode, JcrWorkspace.CMSConfig.getNodeType());
            Iterator<Node> productNodeIterator = productNodeIterable.iterator();
            while(productNodeIterator.hasNext()) {
                //do this
                Node productSubNode = productNodeIterator.next();
                WOTProductVM axProduct = new WOTProductVM();
                axProduct.setProductId(PropertyUtil.getLong(productSubNode, "productListingId"));
                Long productNumber = PropertyUtil.getLong(productSubNode, "productNumber");
                axProduct.setItemId(productNumber);
                axProduct.setDescription(PropertyUtil.getString(productSubNode, "productName"));
                axProduct.setUuid(productSubNode.getIdentifier());
                Node axProductNode = starTemplatingFunctions.getAxProductByProductCode(PartnerPortalConst.Partner_Portal_Channel,productNumber.toString());
                if(axProductNode != null) {
                    String desc = PropertyUtil.getString(axProductNode,"itemName");
                    String status = PropertyUtil.getString(axProductNode,"status");
                    axProduct.setDescription(desc);
                    axProduct.setStatus(status);
                }
                BigDecimal price = getItemPriceByProductIdAndItemId(channel, "", axProduct.getProductId(), axProduct.getItemId());
                axProduct.setPriceStr(price != null ? NvxNumberUtils.formatToCurrency(price) : "");
                wotProducts.add(axProduct);
            }
            return wotProducts;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultVM saveWOTProduct(StoreApiChannels channel, WOTProductVM wotProductVM) {
        ResultVM resultVM = new ResultVM();
        boolean toDelete = false;
        try {
            String uuid = wotProductVM.getUuid();
            Node oldNode=null;
            if (uuid != null && !uuid.equalsIgnoreCase("")) {

                oldNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSConfig.getWorkspaceName(),uuid);
                if (oldNode!= null) {
                    toDelete = true;
                }
            }
            Node newNode = JcrRepository.createNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/" + channel.code + "/wot-products/", wotProductVM.getProductId().toString(), JcrWorkspace.CMSConfig.getNodeType());
            if(newNode!= null) {
                if(toDelete) {
                    PublishingUtil.deleteNodeAndPublish(oldNode, JcrWorkspace.CMSConfig.getWorkspaceName());
                }
                newNode.setProperty("productListingId", wotProductVM.getProductId().toString());
                if (wotProductVM.getDescription() != null)
                    newNode.setProperty("productName", wotProductVM.getDescription());
                else
                    newNode.setProperty("productName", "");
                newNode.setProperty("productNumber", wotProductVM.getItemId().toString());
                JcrRepository.updateNode((JcrWorkspace.CMSConfig.getWorkspaceName()), newNode);
                PublishingUtil.publishNodes(newNode.getIdentifier(), JcrWorkspace.CMSConfig.getWorkspaceName());
            }
            else{
                resultVM.setStatus(false);
                resultVM.setMessage(ApiErrorCodes.WotProductAlreadyExists.message);
            }



        } catch (Exception e) {
            e.printStackTrace();
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.WotProductNotFound.message);
        }
        resultVM.setViewModel(wotProductVM);
        return resultVM;
    }

    @Override
    public ResultVM removeWOTProduct(StoreApiChannels channel,String wotProdIds) {
        ResultVM resultVM = new ResultVM();
        List<String> uuidList=new ArrayList<>();
        if(!wotProdIds.equals("")) {
            uuidList = Arrays.asList(wotProdIds.split("\\s*,\\s*"));
        }
        try {
            for (String uuid : uuidList) {
                Node node = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSConfig.getWorkspaceName(), uuid);
                PublishingUtil.deleteNodeAndPublish(node,JcrWorkspace.CMSConfig.getWorkspaceName());
            }
        }catch(Exception e) {
            resultVM.setStatus(false);
            resultVM.setMessage(ApiErrorCodes.WotProductNotFound.message);
            e.printStackTrace();
        }
        return resultVM;
    }

    private BigDecimal getItemPriceByProductIdAndItemId(StoreApiChannels channel, String axAccNum, Long productId, Long itemId) {
        axAccNum = axAccNum != null && axAccNum.trim().length() > 0 ? axAccNum.trim() : null;
        if(productId == null || itemId == null || productId.intValue() == 0 || itemId.intValue() == 0){
            return null;
        }
        List<String> prodIdList = new ArrayList<String>();
        prodIdList.add(productId.toString());

        List<String> itemIdList = new ArrayList<String>();
        itemIdList.add(productId.longValue()+"-"+itemId.longValue());

        Map<String,BigDecimal> map = prodSrv.getProductItemPriceMap(channel, axAccNum, prodIdList, itemIdList);
        if (map == null) {
            return null;
        }
        String key = productId+"-"+itemId;
        if(!map.containsKey(key)){
            return null;
        }
        return map.get(productId+"-"+itemId);
    }

    @Override
    public ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(Integer partnerId, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String filterStatus, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        PPSLMTAMainAccount mainAccount = null;
        try{
            if(partnerId != null && partnerId.intValue() > 0){
                mainAccount = paAccSharedSrv.getActivePartnerMainAccountByPartnerId(partnerId);
                if(mainAccount == null){
                    return null;
                }
            }
        }catch (Exception ex){
            return null;
        }
        return getPagedWingsOfTimeReservations(true, mainAccount != null ? mainAccount.getId() : null, startDate, endDate, reservationStartDate, reservationEndDate ,filterStatus, showTimes, sortField, sortDirection, page, pageSize);
    }

    public ApiResult<PagedData<List<WOTReservationVM>>> getPagedWingsOfTimeReservations(boolean isAdmin, Integer mainAccountId, Date startDate, Date endDate, Date reservationStartDate, Date reservationEndDate, String filterStatus, String showTimes, String sortField, String sortDirection, int page, int pageSize) throws Exception {
        Date now = new Date(System.currentTimeMillis());
        List<QCriteria> criterias = new ArrayList<QCriteria>();
        criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.NOTNULL, ""));
        if(isAdmin){
            criterias.add(new QCriteria("reservationType", QCriteria.Operation.EQ, WOTReservationType.Reserve.name()));
        }else{
            criterias.add(new QCriteria("reservationType", QCriteria.Operation.EQ, WOTReservationType.Purchase.name()));
        }
        if(mainAccountId != null && mainAccountId.intValue() > 0 ){
            criterias.add(new QCriteria("mainAccountId", QCriteria.Operation.EQ, mainAccountId));
        }
        if(startDate != null){
            criterias.add(new QCriteria("eventDate", QCriteria.Operation.GE, startDate));
        }
        if(endDate != null){
            criterias.add(new QCriteria("eventDate", QCriteria.Operation.LE, endDate));
        }
        if(reservationStartDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.GE, reservationStartDate));
        }
        if(reservationEndDate != null){
            criterias.add(new QCriteria("createdDate", QCriteria.Operation.LE, reservationEndDate));
        }

        if(showTimes != null && showTimes.trim().length() > 0){
            criterias.add(new QCriteria("eventLineId", QCriteria.Operation.EQ, showTimes));
        }

        List<String> status = new ArrayList<String>();
        if(filterStatus == null || filterStatus.trim().length() == 0 || "All".equalsIgnoreCase(filterStatus)){
            status.add("Reserved");
            status.add("PartiallyPurchased");
            status.add("FullyPurchased");
            status.add("Released");
            status.add("Cancelled");
        }else if("Reserved".equalsIgnoreCase(filterStatus)){
            status.add("Reserved");
            status.add("PartiallyPurchased");
            status.add("FullyPurchased");
        }else{
            status.add(filterStatus.trim());
        }
        
        criterias.add(new QCriteria("status", QCriteria.Operation.IN, status, QCriteria.ValueType.String));

        PageRequest pageRequest = new PageRequest(page - 1, pageSize, Sort.Direction.DESC, "eventDate");
        SpecificationBuilder builder = new SpecificationBuilder();

        Specifications querySpec = builder.build(criterias, PPSLMWingsOfTimeReservationSpecification.class);

        Page<PPSLMWingsOfTimeReservation> list = wotRepo.findAll(querySpec, pageRequest);
        pageRequest = null;
        criterias = null;
        querySpec = null;
        criterias = null;
        PagedData<List<WOTReservationVM>> paged = new PagedData<List<WOTReservationVM>>();
        paged.setData(getWOTReservationVMs(list, now, sysParamSrv.getWingsOfTimeShowCutoffInMinutes()));
        paged.setSize(Long.valueOf(list.getTotalElements()).intValue());
        ApiResult<PagedData<List<WOTReservationVM>>> api = new ApiResult<PagedData<List<WOTReservationVM>>>();
        api.setData(paged);
        api.setSuccess(true);
        list = null;
        return api;
    }

    @Transactional(readOnly = true)
    public WOTReservationVM getAdminReservationByID(Integer reservationId) throws Exception {
        if(reservationId == null || reservationId.intValue() == 0){
            throw new BizValidationException("Invalid reservation id found.");
        }
        PPSLMWingsOfTimeReservation wot = this.wotRepo.findById(reservationId);
        if(wot == null){
            throw new BizValidationException("Invalid reservation id found.");
        }
        if(!(WOTReservationType.Reserve.name().equals(wot.getReservationType()) && wot.isAdminRequest())){
            throw new BizValidationException("Invalid reservation id found.");
        }
        return getWOTReservationVM(null, null, null, wot, new Date(System.currentTimeMillis()), sysParamSrv.getWingsOfTimeShowCutoffInMinutes());
    }

    private List<WOTReservationVM> getWOTReservationVMs(Page<PPSLMWingsOfTimeReservation> data, Date now, long cutoffmins) throws Exception {
        if(data == null || data.getContent() == null || data.getContent().size() == 0){
            return null;
        }
        List<WOTReservationVM> wots = new ArrayList<WOTReservationVM>();
        List<PPSLMWingsOfTimeReservation> list = data.getContent();
        Map<Integer, Integer> paIdMapping = new HashMap<Integer, Integer>();
        Map<String, String> paNameMapping = new HashMap<String, String>();
        Map<String, String> nameMapping = new HashMap<String, String>();
        for(PPSLMWingsOfTimeReservation i : list){
            wots.add(getWOTReservationVM(paIdMapping, paNameMapping, nameMapping, i, now, cutoffmins));
        }
        return wots;
    }

    private WOTReservationVM getWOTReservationVM(PPSLMWingsOfTimeReservation i, Date now, long cutoffmins) throws Exception {
        return getWOTReservationVM(null, null, null, i, now, cutoffmins);
    }

    private WOTReservationVM getWOTReservationVM(Map<Integer, Integer> paIdMapping, Map<String, String> paNameMapping, Map<String, String> nameMapping, PPSLMWingsOfTimeReservation i, Date now, long cutoffmins) throws Exception {
        if(nameMapping == null){
            nameMapping = new HashMap<String, String>();
        }
        WOTReservationVM vm = new WOTReservationVM();
        vm.setId(i.getId());
        vm.setPartnerId(paSharedSrv.getPartnerIdByMainAccountIdWithCache(paIdMapping, i.getMainAccountId()));
        vm.setPartnerName(paSharedSrv.getPartnerNameByMainAccountIdWithCache(paNameMapping, i.getMainAccountId()));
        vm.setMainAccountId(i.getMainAccountId());
        vm.setShowDate(NvxDateUtils.formatDateForDisplay(i.getEventDate(), false));
        vm.setShowTime(i.getEventLineId());
        vm.setShowTimeDisplay(i.getEventName());
        vm.setEventLineId(i.getEventLineId());
        vm.setProductId(i.getProductId());
        vm.setEventDate(i.getEventDate());
        vm.setNow(now);
        vm.setQty(i.getQty());
        vm.setReservationType(i.getReservationType());
        vm.setQtySold(i.getQtySold() != null ? i.getQtySold().intValue() : 0);
        vm.setPurchaseSrcId(i.getPurchaseSrcId() != null ? i.getPurchaseSrcId() : 0);
        vm.setPurchaseSrcQty(i.getPurchaseSrcQty() != null ? i.getPurchaseSrcQty() : 0);
        vm.setSplitSrcId(i.getSplitSrcId() != null ? i.getSplitSrcId().intValue() : 0);
        vm.setSplitSrcQty(i.getSplitSrcQty() != null ? i.getSplitSrcQty().intValue() : 0);
        vm.setRdm(i.getQtyRedeemed());
        vm.setUnr(i.getQtyUnredeemed());
        vm.setStatus(i.getStatus());
        vm.setRemarks(i.getRemarks());
        vm.setReceiptNo(i.getReceiptNum());
        vm.setPinCode(i.getPinCode());
        vm.setSystemRemarks(i.getRemarks());
        vm.setTicketPrice(i.getTicketPrice());
        vm.setItemId(i.getAxrtItemId());
        vm.setMediaTypeId(i.getMediaTypeId());
        vm.setEventGroupId(i.getEventGroupId());
        vm.setEventStartTime(i.getEventStartTime());
        vm.setEventEndTime(i.getEventEndTime());
        vm.setEventCapacityId(i.getEventCapacityId());
        vm.setCutOffDateTime(calculateWingsOfTimeShowCutOffTimeByShowTime(i, cutoffmins));
        if(i.getOpenValidityStartDate() != null){
            vm.setOpenValidityStartDate(NvxDateUtils.formatDateForDisplay(i.getOpenValidityStartDate(), false));
        }
        if(i.getOpenValidityEndDate() != null){
            vm.setOpenValidityEndDate(NvxDateUtils.formatDateForDisplay(i.getOpenValidityEndDate(), false));
        }
        if(i.getCancelNotifiedDate() != null){
            vm.setCancelNotifiedDate(NvxDateUtils.formatDateForDisplay(i.getCancelNotifiedDate(), true));
        }
        if(i.getReleasedDate() != null){
            vm.setReleasedDate(NvxDateUtils.formatDateForDisplay(i.getReleasedDate(), true));
        }
        if(i.getReleaseNotifiedDate() != null){
            vm.setReleaseNotifiedDate(NvxDateUtils.formatDateForDisplay(i.getReleaseNotifiedDate(), true));
        }
        return vm;
    }

    @Override
    public Date calculateWingsOfTimeShowCutOffTimeByShowTime(final PPSLMWingsOfTimeReservation wot, long cutoffmins) throws Exception {
        if(wot.getEventTime() != null){
            return new Date(wot.getEventDate().getTime() + wot.getEventTime() * 1000 - cutoffmins * 60000);
        }else{
            return new Date(wot.getEventDate().getTime() + wot.getEventStartTime() * 1000 - cutoffmins * 60000);
        }
    }

    @Override
    public ApiResult<WOTReservationCharge> validateAdminNewReservation(WOTReservationVM reservation) {
        WOTReservationCharge wotPC = new WOTReservationCharge();
        wotPC.setMarkupCode("");
        wotPC.setTicketPrice(BigDecimal.ZERO);
        wotPC.setPenaltyCharge(BigDecimal.ZERO);
        wotPC.setDepositToBeCharged(BigDecimal.ZERO);
        wotPC.setDepositToBeRefunded(BigDecimal.ZERO);
        wotPC.setTicketPurchased(BigDecimal.ZERO);
        wotPC.setRefundedAmount(BigDecimal.ZERO);
        ApiResult<WOTReservationCharge> api = new ApiResult<>();
        api.setData(wotPC);
        api.setSuccess(true);
        return api;
    }

    public void sendWotUnredeemedReservationCancelReminder(Integer paramHours) {
        Integer hours = sysParamSrv.getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminder();
        if(paramHours != null && paramHours.intValue() > 0){
            hours = paramHours.intValue();
        }
        if(hours != null && hours.intValue() > 0){
            Date now = new Date(System.currentTimeMillis());
            Date eventStartDate = new Date(now.getTime() + hours.intValue() * 3600000 + 86400000l);
            List<Integer> ids = wotRepo.findAllActiveUnRedeemdedReservations(eventStartDate);
            if(ids != null && ids.size() > 0){
                for(Integer id : ids){
                    if(id != null && id.intValue() > 0){
                        try{
                            sendWotUnredeemedReservationCancelReminderById(id, hours, now);
                        }catch (Exception ex){
                            log.error("WoT unredeemed reservation cancel reminder email failed for ["+id+"], Error : "+ex.getMessage(), ex);
                        }catch (Throwable ex1){
                            log.error("WoT unredeemed reservation cancel reminder email failed for ["+id+"], Error : "+ex1.getMessage(), ex1);
                        }
                    }
                }
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    private void sendWotUnredeemedReservationCancelReminderById(Integer id, Integer hours, Date now) {
        if(id != null && id.intValue() > 0){
            PPSLMWingsOfTimeReservation wot = wotRepo.findById(id);
            if(wot != null && wot.getEventDate() != null && wot.getEventTime() != null && hours != null){
                Date eventdttm = new Date((wot.getEventDate().getTime()) + (wot.getEventTime() * 1000) - (hours.intValue() * 3600000) );
                if(eventdttm.compareTo(now) <= 0){
                    Integer mainAccId = wot.getMainAccountId();
                    PPSLMTAMainAccount mainAcc = paAccSharedSrv.getPartnerMainAccountByMainAccountId(mainAccId);
                    if(mainAcc != null){
                        if(mainAcc.getEmail() != null && mainAcc.getEmail().trim().length() > 0){
                            wotMailSrv.sendingWoTCancellationReminderEmail(mainAcc.getProfile().getOrgName(), mainAcc.getEmail(), wot);
                            wot.setCancelNotifiedDate(new Date(System.currentTimeMillis()));
                            wotRepo.save(wot);
                        }
                    }
                }
            }
        }
    }

    private void verifyWoTReservationQty(Date requestDate, Integer paId, Integer mainAccId, String prodId, String itemId, String eventGroupId, String eventLineId, Integer newQty) throws BizValidationException, Exception {
        /*Integer reservationCap = sysParamSrv.getWoTAdminReservationCap();*/
        /*if(reservationCap == null){
            throw new BizValidationException("No reservation capacity configuration found!");
        }
        if(reservationCap.intValue() < (newQty)){
            throw new BizValidationException("Maximum "+reservationCap+" Wings of Time tickets are allowed to purchase for each show!");
        }
        Integer noOfTicketReserved = this.getTotalTicketReservedPerShow(mainAccId, prodId, itemId, eventGroupId, eventLineId, requestDate);
        if(noOfTicketReserved != null && noOfTicketReserved.intValue() >= reservationCap.intValue()){
            throw new BizValidationException("You have 0 Wings of Time tickets are allowed to purchase for this show!");
        }
        if(noOfTicketReserved != null && (noOfTicketReserved.intValue() + newQty) > reservationCap.intValue()){
            int diff = reservationCap.intValue() - noOfTicketReserved.intValue();
            if(diff < 0){
                diff = 0;
            }
            throw new BizValidationException("You have "+(diff)+" Wings of Time tickets are allowed to purchase for this show!");
        }*/
        int noOfDays = sysParamSrv.getWotAdminReservationPeriodLimit();

        Date maximuDate = new Date(System.currentTimeMillis());
        maximuDate = NvxDateUtils.parseDate(NvxDateUtils.formatDate(maximuDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY),NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        maximuDate = new Date( maximuDate.getTime() + noOfDays * 86400000l);
        if(requestDate.after(maximuDate)){
            throw new BizValidationException("Maximum can reserve Wings of Time tickets in "+noOfDays+" advance for each show!");
        }
    }

    @Override
    public ApiResult<WOTReservationCharge> validateAdminUpdateReservation(WOTReservationVM reservation) {
        return validateAdminNewReservation(null);
    }

    @Override
    public ApiResult<WOTReservationCharge> validateAdminCancelReservation(Integer reservationId) {
        return validateAdminNewReservation(null);
    }

    @Override
    public ApiResult<WOTReservationVM> saveAdminReservation(WOTReservationVM request, boolean isSendUpdateEmail) throws Exception {
        Date now = new Date();
        ApiResult<WOTReservationCharge> validateResult = validateAdminNewReservation(request);
        if(!validateResult.isSuccess()) {
            return new ApiResult<>(false, validateResult.getErrorCode(), validateResult.getMessage(), null);
        }
        if(!(request.getPartnerId() != null && request.getPartnerId().intValue() > 0)){
            return new ApiResult<>(false, "", "Invalid partner found.", null);
        }
        PPSLMTAMainAccount paMainAcc = paAccSharedSrv.getActivePartnerMainAccountByPartnerId(request.getPartnerId());
        if(paMainAcc == null){
            return new ApiResult<>(false, "", "Invalid partner found.", null);
        }
        verifyWoTReservationQty(request.getEventDate(), request.getPartnerId(), paMainAcc.getId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), request.getQty());
        String receiptNum = ProjectUtils.generateReceiptNumber(StoreApiChannels.PARTNER_PORTAL_SLM, now);
        PPSLMWingsOfTimeReservation reservation;
        reservation = new PPSLMWingsOfTimeReservation();
        reservation.setReceiptNum(receiptNum);
        reservation.setPinCode("");
        reservation.setReservationType(WOTReservationType.Reserve.name());
        reservation.setAdminRequest(true);
        reservation.setMainAccountId(paMainAcc.getId());
        reservation.setIsSubAccountTrans(false);
        reservation.setUsername(paMainAcc.getUsername());
        reservation.setModifiedDate(now);
        reservation.setCreatedDate(now);
        reservation.setCreatedBy(request.getCreatedByUsername());
        reservation.setModifiedBy(request.getCreatedByUsername());
        if(request.getShowDate() != null && request.getShowDate().trim().length() > 0){
            reservation.setEventDate(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        reservation.setEventLineId(request.getEventLineId());
        reservation.setProductId(request.getProductId());
        reservation.setEventName(request.getShowTimeDisplay());
        reservation.setRemarks(request.getRemarks());
        reservation.setQty(request.getQty());
        reservation.setQtySold(0);
        reservation.setQtyRedeemed(0);
        reservation.setQtyUnredeemed(0);
        reservation.setStatus(WOTReservationStatus.Pending.name());
        reservation.setTicketPrice(validateResult.getData().getTicketPrice());
        reservation.setItemId(request.getItemId());
        reservation.setMediaTypeId(request.getMediaTypeId());
        reservation.setEventGroupId(request.getEventGroupId());
        reservation.setReceiptEmail(paMainAcc.getEmail());
        reservation.setEventStartTime(request.getEventStartTime());
        reservation.setEventTime(request.getEventTime());
        reservation.setEventEndTime(request.getEventEndTime());
        reservation.setEventCapacityId(request.getEventCapacityId());
        try{
            reservation.setOpenValidityStartDate(new Date(Long.valueOf(request.getOpenValidityStartDate())));
        }catch (Exception ex){}
        try{
            reservation.setOpenValidityEndDate(new Date(Long.valueOf(request.getOpenValidityEndDate())));
        }catch (Exception ex){}
        slmReservationRepo.save(reservation);
        ApiResult<List<AxRetailTicketRecord>> result = new ApiResult<List<AxRetailTicketRecord>>();
        try{
            result = processWotTicketPreservation(result, paMainAcc.getProfile().getAxAccountNumber(), reservation);
        }catch (Exception ex){
            log.error("saveReservation failed ", ex);
            if(ex != null && ex instanceof BizValidationException){
                result.setSuccess(false);
                result.setMessage(ex.getMessage());
            }
        }
        if(result != null && result.isSuccess()){
            reservation.setStatus(WOTReservationStatus.Reserved.name());
        }else{
            reservation.setStatus(WOTReservationStatus.Failed.name());
        }
        slmReservationRepo.save(reservation);
        request.setId(reservation.getId());
        request.setStatus(reservation.getStatus());
        if(result != null && result.isSuccess()){
            reservation.setEventCutOffTime(calculateWingsOfTimeShowCutOffTimeByShowTime(reservation, sysParamSrv.getWingsOfTimeShowCutoffInMinutes()));
            if(isSendUpdateEmail){
                try{
                    this.wotMailSrv.sendingAdminWoTAmendmentEmail(paMainAcc, reservation, request.getCreatedByUsername());
                }catch (Exception ex){
                    log.error("saveAdminReservation send update email failed "+ex.getMessage());
                }catch (Throwable ex2){
                    log.error("saveAdminReservation send update email failed "+ex2.getMessage());
                }
            }else{
                try{
                    this.wotMailSrv.sendingAdminWoTConfirmationEmail(paMainAcc, reservation, request.getCreatedByUsername());
                }catch (Exception ex){
                    log.error("saveAdminReservation send update email failed "+ex.getMessage());
                }catch (Throwable ex2){
                    log.error("saveAdminReservation send update email failed "+ex2.getMessage());
                }
            }
            return new ApiResult<>(true, "", "", request);
        }else{
            return new ApiResult<>(false, result.getErrorCode(), result.getMessage(), request);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ApiResult<String> cancelAdminReservation(WOTReservationVM reservation, boolean hasEmailNotification) throws BizValidationException {
        if(reservation == null || reservation.getId() == 0 || reservation.getCreatedByUsername() == null){
            throw new BizValidationException("Invalid request found.");
        }
        ApiResult<WOTReservationCharge> calculatedAmount = validateAdminCancelReservation(reservation.getId());
        if(!calculatedAmount.isSuccess()) {
            return new ApiResult<>(false, calculatedAmount.getErrorCode(), calculatedAmount.getMessage(), "");
        }
        //update reservation status
        PPSLMWingsOfTimeReservation wot = wotRepo.findById(reservation.getId());
        if(wot == null){
            throw new BizValidationException("Invalid request found.");
        }
        if(!(wot.isAdminRequest() && WOTReservationType.Reserve.name().equals(wot.getReservationType()))){
            throw new BizValidationException("Invalid request found.");
        }
        if(!(WOTReservationStatus.Reserved.name().equals(wot.getStatus()))){
            throw new BizValidationException("Cancel is only allowed PIN code in Reserved status.");
        }
        Long purchasedtimes = wotRepo.countReservationPurchasedTimes(wot.getId());
        if(purchasedtimes != null && purchasedtimes.intValue() > 0){
            throw new BizValidationException("Cancel is not allowed after tickets purchased by partner.");
        }
        PPSLMPartner pa = paSharedSrv.getPartnerByMainAccountId(wot.getMainAccountId());
        if(pa == null){
            return new ApiResult<>(false, "", "Invalid request found.", "");
        }
        int tempQty = wot.getQty();
        wot.setQty(0);
        ApiResult<List<AxRetailTicketRecord>> retResponse = new ApiResult<List<AxRetailTicketRecord>>();
        this.saveOrUpdatePreservationOrder(retResponse, pa.getAxAccountNumber(), wot, true);
        if(retResponse.isSuccess()){
            wot.setStatus(WOTReservationStatus.Cancelled.name());
            wot.setModifiedBy(reservation.getCreatedByUsername());
            wot.setModifiedDate(new Date(System.currentTimeMillis()));
            wot.setQty(tempQty);
            wotRepo.save(wot);
            if(hasEmailNotification) {
                this.wotMailSrv.sendingAdminWoTCancellationEmail(pa.getMainAccount(), wot, reservation.getCreatedByUsername());
            }
            return new ApiResult<>(true, "", "", "");
        }else{
            log.error("Cancel Wings of Time admin reservation ["+reservation.getId()+"] failed, "+JsonUtil.jsonify(retResponse));
            throw new BizValidationException("Cancel Wings of Time failed, Please try again.");
        }
    }

    @Override
    public ApiResult<WOTReservationVM> updateAdminReservation(WOTReservationVM request) throws BizValidationException, Exception {
        if(request == null || request.getId() == 0 || request.getCreatedByUsername() == null){
            throw new BizValidationException("Invalid request found.");
        }
        boolean success = false;
        ApiResult<WOTReservationCharge> calculatedAmount = validateAdminUpdateReservation(request);
        if(calculatedAmount.isSuccess()) {
            Date now = new Date(System.currentTimeMillis());
            //update reservation status
            PPSLMWingsOfTimeReservation wot = wotRepo.findById(request.getId());
            if(wot == null){
                throw new BizValidationException("Invalid request found.");
            }
            if(!(wot.isAdminRequest() && WOTReservationType.Reserve.name().equals(wot.getReservationType()))){
                throw new BizValidationException("Invalid request found.");
            }
            PPSLMPartner pa = paSharedSrv.getPartnerByMainAccountId(wot.getMainAccountId());
            if(pa == null){
                throw new BizValidationException("Invalid request found.");
            }
            if(!(WOTReservationStatus.Reserved.name().equals(wot.getStatus()))){
                throw new BizValidationException("Update is only allowed PIN code in Reserved status.");
            }
            Long purchasedtimes = wotRepo.countReservationPurchasedTimes(wot.getId());
            if(purchasedtimes != null && purchasedtimes.intValue() > 0){
                throw new BizValidationException("Update is not allowed after tickets purchased by partner.");
            }
            if(request.getShowDate() != null && request.getShowDate().trim().length() > 0){
                try{
                    wot.setEventDate(NvxDateUtils.parseDate(request.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
                }catch (Exception ex){
                    throw new BizValidationException("Show Date must be in \""+NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY+"\" format.");
                }
            }

            boolean isCancelRequired = checkIsCanncelReservationRequied(wot, request);
            if(isCancelRequired){
                verifyWoTReservationQty(request.getEventDate(), pa.getId(), wot.getMainAccountId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), request.getQty());
            }else{
                if(wot.getQty() < request.getQty()){
                    verifyWoTReservationQty(request.getEventDate(), pa.getId(), wot.getMainAccountId(), request.getProductId(), request.getItemId(), request.getEventGroupId(), request.getEventLineId(), request.getQty() - wot.getQty());
                }
            }
            if(isCancelRequired){
                success = cancelAndCreateReservation(request, wot, pa);
            }else{
                success = updateFullReservation(request, wot, pa);
            }
        }
        return new ApiResult<>(success, calculatedAmount.getErrorCode(), calculatedAmount.getMessage(), request);
    }

    private boolean cancelAndCreateReservation( WOTReservationVM request, PPSLMWingsOfTimeReservation wot, PPSLMPartner pa) throws Exception {
        WOTReservationVM vm = new WOTReservationVM();
        vm.setId(wot.getId());
        vm.setCreatedByUsername(request.getCreatedByUsername());
        ApiResult<String> cancelResult = this.cancelAdminReservation(vm, false);
        if(!cancelResult.isSuccess()){
            throw new BizValidationException(cancelResult.getMessage());
        }
        ApiResult<WOTReservationVM> createResponse = this.saveAdminReservation(request, true);
        if(!createResponse.isSuccess()){
            throw new BizValidationException(createResponse.getMessage());
        }
        return true;
    }

    private boolean updateFullReservation(WOTReservationVM request, PPSLMWingsOfTimeReservation wot, PPSLMPartner pa) throws BizValidationException {
        PPSLMWingsOfTimeReservation copy = wot.getDataCopy();
        copy.setEventLineId(request.getEventLineId());
        copy.setProductId(request.getProductId());
        copy.setEventName(request.getShowTimeDisplay());
        copy.setRemarks(request.getRemarks() != null && request.getRemarks().trim().length() > 0 ? request.getRemarks() : request.getRemarks());
        copy.setQty(request.getQty());
        copy.setTicketPrice(new BigDecimal(0.0f));
        copy.setItemId(request.getItemId());
        copy.setMediaTypeId(request.getMediaTypeId());
        copy.setEventGroupId(request.getEventGroupId());
        copy.setEventTime(request.getEventTime());
        ApiResult<List<AxRetailTicketRecord>> retResponse = new ApiResult<List<AxRetailTicketRecord>>();
        try{
            saveOrUpdatePreservationOrder(retResponse, pa.getAxAccountNumber(), copy, true);
        }catch (Exception ex){
            log.error("updateFullReservation failed ", ex);
            ex.printStackTrace();
            throw new BizValidationException("Update Wings of Time failed, Please try again.");
        }
        if(retResponse.isSuccess()){
            wot.setEventLineId(request.getEventLineId());
            wot.setProductId(request.getProductId());
            wot.setEventName(request.getShowTimeDisplay());
            wot.setRemarks(request.getRemarks() != null && request.getRemarks().trim().length() > 0 ? request.getRemarks() : request.getRemarks());
            wot.setQty(request.getQty());
            wot.setTicketPrice(new BigDecimal(0.0f));
            wot.setItemId(request.getItemId());
            wot.setMediaTypeId(request.getMediaTypeId());
            wot.setEventGroupId(request.getEventGroupId());

            wot.setModifiedBy(request.getCreatedByUsername());
            wot.setModifiedDate(new Date(System.currentTimeMillis()));
            wotRepo.save(wot);
            request.setStatus(wot.getStatus());
            try{
                this.wotMailSrv.sendingAdminWoTAmendmentEmail(pa.getMainAccount(), wot, request.getCreatedByUsername());
            }catch (Exception ex){
                log.error("sendingAdminWoTAmendmentEmail failed "+ex.getMessage());
            }catch (Throwable ex2){
                log.error("sendingAdminWoTAmendmentEmail failed "+ex2.getMessage());
            }
            return true;
        }else{
            request.setStatus(WOTReservationStatus.Failed.name());
            log.error("Update Wings of Time admin reservation ["+wot.getId()+"] failed, WOT : " + JsonUtil.jsonify(wot) + " Response : " +JsonUtil.jsonify(retResponse));
            String msg = retResponse.getMessage();
            if(msg != null && msg.trim().length() > 0){
                throw new BizValidationException(msg.trim());
            }else{
                throw new BizValidationException("Update Wings of Time failed, Please try again.");
            }
        }
    }

    private boolean checkIsCanncelReservationRequied(PPSLMWingsOfTimeReservation c, WOTReservationVM r) throws BizValidationException {
        if(!(c != null && r != null && c.getItemId() != null && r.getItemId() != null && c.getItemId().trim().length() > 0 && r.getItemId().trim().length() > 0)){
            throw new BizValidationException("Invalid Request Found!");
        }
        if(c.getItemId().trim().equalsIgnoreCase(r.getItemId().trim())){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public ApiResult<String> refreshAdminReservationPinCodeById(Integer reservationId) {
        return new ApiResult<>(true, "","","");
    }

    private ApiResult<List<AxRetailTicketRecord>> processWotTicketPreservation(ApiResult<List<AxRetailTicketRecord>> retResponse, String custId, PPSLMWingsOfTimeReservation wot) throws Exception {
        try{
            addPresesrvationToShoppingCart(custId, wot);
            checkoutPresesrvationShoppingCart(custId, wot);
            saveOrUpdatePreservationOrder(retResponse, custId, wot, false);
        }catch (Exception ex){
            throw ex;
        }catch (Throwable ex){
            throw new Exception(ex);
        }
        return retResponse;
    }

    public void saveOrUpdatePreservationOrder(ApiResult<List<AxRetailTicketRecord>> retResponse, String custId, PPSLMWingsOfTimeReservation wot, boolean update) {
        ApiResult<List<AxRetailTicketRecord>> response = null;
        response = finalizePreservationShoppingCart(custId, wot, update);
        processPreservationFinalizeResponse(response, retResponse, wot);
    }

    private String populateShoppingCartComment(PPSLMWingsOfTimeReservation r){
        WingsOfTimeRequestCommentModel vm = new WingsOfTimeRequestCommentModel();
        vm.setReceiptNum(r.getReceiptNum());
        vm.setProductId(r.getProductId());
        vm.setEventLineId(r.getEventLineId());
        vm.setShowDateTime(NvxDateUtils.formatDate(r.getEventDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
        vm.setQty(r.getQty());
        vm.setTicketPrice(r.getTicketPrice().toString());
        vm.setCreatedDate(NvxDateUtils.formatDate(r.getCreatedDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
        vm.setItemId(r.getItemId());
        vm.setMediaTypeId(r.getMediaTypeId());
        vm.setEventGroupId(r.getEventGroupId());
        vm.setRemarks(r.getRemarks());
        return JsonUtil.jsonify(vm);
    }

    private AxStarInputAddCart populateShoppingCart(String custId, PPSLMWingsOfTimeReservation wot) throws Exception {
        if(custId == null || custId.trim().length() == 0){
            throw new Exception("No customer account number found!");
        }

        Map<String, String> extPropsMap = new HashMap<String, String>();
        extPropsMap.put("EventLineId", wot.getEventLineId());
        extPropsMap.put("EventGroupId", wot.getEventGroupId());
        extPropsMap.put("EventDate", NvxDateUtils.formatDate(wot.getEventDate(), NvxDateUtils.AX_ONLINE_CAT_SERVICE_WOT_RESERVE_DATE_FORMAT));

        AxStarInputAddCartItem item = new AxStarInputAddCartItem();
        item.setProductId(Long.valueOf(wot.getProductId()));
        item.setQuantity(wot.getQty());
        item.setComment(populateShoppingCartComment(wot));
        item.setStringExtensionProperties(extPropsMap);

        List<AxStarInputAddCartItem> items = new ArrayList<AxStarInputAddCartItem>();
        items.add(item);

        AxStarInputAddCart cart = new AxStarInputAddCart();
        cart.setItems(items);
        cart.setCustomerId(custId);
        cart.setCartId("");
        cart.setSalesOrderNumber(wot.getReceiptNum());
        return cart;
    }

    private void addPresesrvationToShoppingCart(String custId, PPSLMWingsOfTimeReservation wot) throws Exception {
        AxStarInputAddCart request = populateShoppingCart(custId, wot);
        AxStarServiceResult<AxStarCartNested> response = axStarSrv.apiCartAddItem(WingOfTimeChannel.CHANNEL, request);
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                "add-wot-to-cart",
                response.isSuccess(),
                null,
                JsonUtil.jsonify(response.getError()),
                JsonUtil.jsonify(request),
                JsonUtil.jsonify(response.getData())
        );
        verifyCartResponse(custId, wot, response);
        storeAdd2CartResponse(wot, response);
    }

    private void verifyCartResponse(String custId, PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) throws Exception  {
        if(response == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        if(!response.isSuccess()){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        AxStarCartNested cartWrap = response.getData();
        if(cartWrap == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        AxStarCart cart = cartWrap.getCart();
        if(cart == null){
            throw new BizValidationException("Add to wings of time shopping card failed!");
        }
        if(!custId.equals(cart.getCustomerId())){
            throw new BizValidationException("Verify customer no. failed for add to wings of time shopping cart!");
        }
        if(!(cart.getCartLines() != null && cart.getCartLines().size() == 1 && cart.getCartLines().get(0) != null)){
            throw new BizValidationException("Verify product/items failed for add to wings of time shopping cart!");
        }
        AxStarCartLine line = cart.getCartLines().get(0);
        if(!wot.getProductId().equalsIgnoreCase(line.getProductId().toString())){
            throw new BizValidationException("Verify product id failed for add to wings of time shopping cart!");
        }
        if(!wot.getItemId().equalsIgnoreCase(line.getItemId())){
            throw new BizValidationException("Verify item id failed for add to wings of time shopping cart!");
        }
        if(!(wot.getQty().intValue() == line.getQuantity())){
            throw new BizValidationException("Verify item quantity failed for add to wings of time shopping cart!");
        }
    }

    private void storeAdd2CartResponse(PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) {
        AxStarCartNested cartWrap = response.getData();
        AxStarCart cart = cartWrap.getCart();
        AxStarCartLine line = cart.getCartLines().get(0);
        wot.setAxCartId(cart.getId());
        //wot.setAxLineId(line.getLineId());
        //wot.setAxSubtotalAmt(cart.getSubtotalAmount());
        //wot.setAxTotalAmt(cart.getTotalAmount());
        //wot.setAxPrice(line.getPrice());
        //wot.setAxExternalPrice(line.getExtendedPrice());
        //wot.setAxTaxAmt(line.getTaxAmount());
        //wot.setAxItemTaxGroupId(line.getItemTaxGroupId());
        //wot.setAxNetAmtWoTax(line.getNetAmountWithoutTax());
        //wot.setAxDiscountAmt(line.getDiscountAmount());
    }

    private void checkoutPresesrvationShoppingCart(String custId, PPSLMWingsOfTimeReservation wot) throws Exception {
        AxStarServiceResult<AxStarCartNested> response = axStarSrv.apiCheckout(
                WingOfTimeChannel.CHANNEL,
                wot.getAxCartId(),
                custId,
                wot.getReceiptNum()
        );
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                "checkout-wot-cart",
                response != null ? response.isSuccess() : false, null,
                response != null ? JsonUtil.jsonify(response.getError()) : null,
                String.format("{ \"cartId\" : \"%s\", \"customerId\" : \"%s\", \"salesOrderNumber\" : \"%s\"",
                        String.valueOf(wot.getAxCartId()),
                        custId,
                        String.valueOf(wot.getReceiptNum())
                ),
                response != null && response.getData() != null ? JsonUtil.jsonify(response.getData()) : null
        );
        if(response == null){
            throw new BizValidationException("Checkout wings of time shopping card failed!");
        }
        if(!response.isSuccess()){
            throw new BizValidationException("Checkout wings of time shopping card failed!");
        }
        verifyCartResponse(custId, wot, response);
        storeCheckoutCartResponse(wot, response);
    }

    private void storeCheckoutCartResponse(PPSLMWingsOfTimeReservation wot, AxStarServiceResult<AxStarCartNested> response) {
        AxStarCartNested cartWrap = response.getData();
        AxStarCart cart = cartWrap.getCart();
        AxStarCartLine line = cart.getCartLines().get(0);
        wot.setAxCheckoutCartId(cart.getId());
        wot.setAxCheckoutLineId(line.getLineId());
        wot.setReceiptEmail(cart.getReceiptEmail());
    }

    private ApiResult<List<AxRetailTicketRecord>> finalizePreservationShoppingCart(String cutId, PPSLMWingsOfTimeReservation wot, boolean update) {
        List<AxRetailCartTicket> items = new ArrayList<AxRetailCartTicket>();
        AxRetailCartTicket item = new AxRetailCartTicket();
        item.setAccountNum(cutId);
        item.setQty(wot.getQty());
        item.setItemId(wot.getItemId());
        item.setTransactionId(wot.getReceiptNum());
        item.setEventLineId(wot.getEventLineId());
        item.setEventGroupId(wot.getEventGroupId());
        item.setLineId(wot.getAxCheckoutLineId());
        item.setListingId(Long.parseLong(wot.getProductId().trim()));
        item.setEventDate(wot.getEventDate());
        item.setUpdateCapacity(update ? 1 : 0);
        items.add(item);
        String exceptionMsg  = null;
        Object exception = null;
        ApiResult<List<AxRetailTicketRecord>> orders = null;
        try{
            orders = txnSrv.apiCartValidateAndReserveQuantity(WingOfTimeChannel.CHANNEL, wot.getReceiptNum(), items, cutId);
        }catch (Exception ex){
            ex.printStackTrace();
            exceptionMsg = ex.getMessage();
        }
        wotLogSrv.saveWingsOfTimeReservationLog(
                wot,
                WingOfTimeChannel.CHANNEL.code,
                update ? "update-reserved-tickets" : "reserve-tickets",
                orders != null ? orders.isSuccess() : false,
                orders != null ? orders.getErrorCode() : null,
                orders != null ? orders.getMessage() : exceptionMsg,
                orders != null ? JsonUtil.jsonify(orders) : null,
                orders != null && orders.getData() != null ? JsonUtil.jsonify(orders.getData()) : null
        );
        return orders;
    }

    private ApiResult<List<AxRetailTicketRecord>> processPreservationFinalizeResponse(
            ApiResult<List<AxRetailTicketRecord>> response, ApiResult<List<AxRetailTicketRecord>> retResponse,
            PPSLMWingsOfTimeReservation wot) {
        if(response != null && response.isSuccess()){
            savePreservationFinalizeResponse(wot, response);
            retResponse.setSuccess(response.isSuccess());
            retResponse.setData(response.getData());
            retResponse.setMessage(response.getMessage());
            retResponse.setErrorCode(response.getErrorCode());
        }else{
            if(response != null){
                retResponse.setSuccess(response.isSuccess());
                retResponse.setData(response.getData());
                retResponse.setMessage(response.getMessage());
                retResponse.setErrorCode(response.getErrorCode());
            }else{
                retResponse.setSuccess(false);
            }
        }
        return retResponse;
    }

    private void savePreservationFinalizeResponse(PPSLMWingsOfTimeReservation wot,
                                          ApiResult<List<AxRetailTicketRecord>> tempResult) {
        if(wot == null || tempResult == null || tempResult.getData() == null ){
            return;
        }
        List<AxRetailTicketRecord> tickets = tempResult.getData();
        if(tickets == null || tickets.size() == 0){
            return;
        }
        AxRetailTicketRecord ticket = null;
        for(AxRetailTicketRecord i : tickets){
            if(i.getEventGroupId() != null &&  wot.getEventGroupId().equals(i.getEventGroupId()) &&
               i.getItemId() != null &&  wot.getItemId().equals(i.getItemId()) &&
               i.getEventLineId() != null && wot.getEventLineId().equals(i.getEventLineId())
            ){
                ticket = i; break;
            }
        }
        if(ticket != null){
            wot.setPinCode(ticket.getPinCode());
            wot.setReserveNeedsActivation(ticket.isNeedsActivation());
            wot.setReserveOriginalTicketCode(ticket.getOriginalTicketCode());
            wot.setReserveQtyGroup(ticket.getQtyGroup());
            wot.setReserveRecId(ticket.getRecId());
            wot.setReserveStartDate(ticket.getStartDate());
            wot.setReserveEndDate(ticket.getEndDate());
            wot.setReserveTicketCode(ticket.getTicketCode());
            wot.setReserveTransactionId(ticket.getTransactionId());
            wot.setReserveTicketStatus(ticket.getTicketStatus());
            wot.setModifiedDate(new Date(System.currentTimeMillis()));
        }
    }
}
