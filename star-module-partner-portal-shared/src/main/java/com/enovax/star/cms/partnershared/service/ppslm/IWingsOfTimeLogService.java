package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;

/**
 * Created by houtao on 26/8/16.
 */
public interface IWingsOfTimeLogService {
    void saveWingsOfTimeReservationLog(PPSLMWingsOfTimeReservation wot, String channel, String serviceName, boolean success, String errorCode, String errorMsg, String request, String response);
}
