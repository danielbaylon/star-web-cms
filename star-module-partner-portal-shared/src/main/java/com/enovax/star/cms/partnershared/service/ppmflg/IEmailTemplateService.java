package com.enovax.star.cms.partnershared.service.ppmflg;


import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;

public interface IEmailTemplateService {
    public SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey);

    boolean hasEmailTemplateByKey(String appKey, String handlebarsApproverAlert);
}
