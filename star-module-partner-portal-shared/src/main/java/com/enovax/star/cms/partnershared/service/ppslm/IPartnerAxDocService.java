package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;

import java.io.IOException;
import java.util.List;

/**
 * Created by houtao on 18/10/16.
 */
public interface IPartnerAxDocService {

    void syncPartnerDocuments(PPSLMPartner partner, List<PPSLMPADocMapping> docMap) throws BizValidationException, IOException;

    void downloadPartnerDocuments(PPSLMPartner partner, List<PPSLMPADocMapping> byPartnerId) throws BizValidationException, IOException;
}
