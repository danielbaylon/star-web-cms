package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PartnerExclProdMapping;
import com.enovax.star.cms.commons.model.api.ApiResult;

import javax.jcr.RepositoryException;
import java.util.List;

/**
 * Created by lavanya on 23/9/16.
 */
public interface IPartnerExclProdService {
    public List<String> getExclProdbyPartner(String channel,Integer partnerId) throws RepositoryException;

    public PartnerExclProdMapping getPartnerExclProdMappingbyPartner(String channel,Integer partnerId) throws RepositoryException;

    public PartnerExclProdMapping savePartnerExclProdMapping(String channel,PartnerExclProdMapping partnerExclProdMapping) throws Exception;

    public PartnerExclProdMapping saveExclProdbyPartner(String channel,Integer partnerId, List<String> exclProdIds) throws Exception;

    List<String> getPartnerByExclProd(String channel, String productId) throws RepositoryException;

    ApiResult<String> processExclusive(String channel, String productId,
                                       List<String> partnerIdstoAdd, List<String> partnerIdsToRemove, String userNm);
}
