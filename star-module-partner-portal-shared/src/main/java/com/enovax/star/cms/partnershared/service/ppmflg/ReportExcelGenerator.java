package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

/**
 * Created by lavanya on 8/11/16.
 */
@Service("PPMFLGReportExcelGenerator")
public class ReportExcelGenerator {

    public Workbook generateTransReportExcel(ReportFilterVM filter, ReportResult<TransReportModel> result) {
        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Transaction Report");
        Row r = null;
        Cell c = null;

        int row = 0;

        r = s.createRow(row);
        c = r.createCell(0);
        c.setCellValue("Transaction Date Range: From " + filter.getFromDate() + " To " + filter.getToDate());

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Payment Type: " + (filter.getPaymentType() == null || filter.getPaymentType().trim().length() == 0 ? "All" : filter.getPaymentType().toString()));

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Receipt Number: " + (StringUtils.isEmpty(filter.getReceiptNum()) ? "-" : filter.getReceiptNum()));

        if (filter.getIsSysad()) {
            r = s.createRow(++row);
            c = r.createCell(0);
            c.setCellValue("Organization Name: " + (StringUtils.isEmpty(filter.getOrgName()) ? "-" : filter.getOrgName()));
        }

        r = s.createRow(++row);
        c = r.createCell(0);
        String headerTextTxnStatus = "All";
        if (filter.getPaymentType() != null && filter.getPaymentType().trim().length() > 0) {
            if ("Online".equalsIgnoreCase(filter.getPaymentType())) {
                if (filter.getTransMegaStatus() != null) {
                    headerTextTxnStatus = filter.getTransMegaStatus().name().toString();
                }
            } else if ("Offline".equalsIgnoreCase(filter.getPaymentType())) {
                if (filter.getStatus() != null && filter.getStatus().trim().length() > 0) {
                    headerTextTxnStatus = filter.getStatus().toString();
                    if (headerTextTxnStatus != null && headerTextTxnStatus.trim().length() > 0) {
                        headerTextTxnStatus = headerTextTxnStatus.replace("_", " ");
                    }
                }
            }
        }
        c.setCellValue("Transaction Status: " + headerTextTxnStatus);

        row++; //blank row

        r = s.createRow(++row);

        int cnum = 0;
        if (filter.getIsSysad()) {
            c = r.createCell(cnum);
            c.setCellValue("Organization Name");
        } else {
            cnum = -1;
        }
        c = r.createCell(++cnum);
        c.setCellValue("Receipt No.");
        c = r.createCell(++cnum);
        c.setCellValue("Transaction Date");
        c = r.createCell(++cnum);
        c.setCellValue("Transaction Status");
        c = r.createCell(++cnum);
        c.setCellValue("Payment Type");
        c = r.createCell(++cnum);
        c.setCellValue("Approval Date (Offline)");
        c = r.createCell(++cnum);
        c.setCellValue("Expiry Date");
        c = r.createCell(++cnum);
        c.setCellValue("Revalidated Expiry Date");
        c = r.createCell(++cnum);
        c.setCellValue("Item Description");
        c = r.createCell(++cnum);
        c.setCellValue("Qty. Purchased");
        c = r.createCell(++cnum);
        c.setCellValue("Unit Price ($S)");
        c = r.createCell(++cnum);
        c.setCellValue("Total Amount ($S)");
        c = r.createCell(++cnum);
        c.setCellValue("Purchased By");
        c = r.createCell(++cnum);
        c.setCellValue("Revalidation Ref. No.");
        c = r.createCell(++cnum);
        c.setCellValue("Remarks");

        for (TransReportModel theRow : result.getRows()) {
            r = s.createRow(++row);

            int innerCnum = 0;
            if (filter.getIsSysad()) {
                c = r.createCell(innerCnum);
                c.setCellValue(theRow.getOrgName());
            } else {
                innerCnum = -1;
            }
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getReceiptNum());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTxnDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTransStatusText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getPaymentType());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getOfflinePayApproveDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getExpiryDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getRevalExpiryDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getItemDesc());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getQty());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getPriceText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTotalAmountText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getPurchasedBy());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getRevalidateReference());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getRemarks());
        }

        for (int i = 0; i <= cnum; i++) {
            s.autoSizeColumn(i);
        }

        return wb;
    }

    public Workbook generateInventoryReportExcel(ReportFilterVM filter, ReportResult<InventoryReportModel> result) {
        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Inventory Report");
        Row r = null;
        Cell c = null;

        int row = 0;

        r = s.createRow(row);
        c = r.createCell(0);
        c.setCellValue("Transaction Date Range: From " + filter.getFromDate() + " To " + filter.getToDate());

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Receipt Number: " + (StringUtils.isEmpty(filter.getReceiptNum()) ? "-" : filter.getReceiptNum()));

        if (filter.getIsSysad()) {
            r = s.createRow(++row);
            c = r.createCell(0);
            c.setCellValue("Organization Name: " + (StringUtils.isEmpty(filter.getOrgName()) ? "-" : filter.getOrgName()));
        }

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Transaction Status: " + (StringUtils.isEmpty(filter.getStatus()) ? "All" : filter.getStatus()));

        row++; //blank row

        r = s.createRow(++row);

        int cnum = 0;
        if (filter.getIsSysad()) {
            c = r.createCell(cnum);
            c.setCellValue("Organization Name");
        } else {
            cnum = -1;
        }
        c = r.createCell(++cnum);
        c.setCellValue("Receipt No.");
        c = r.createCell(++cnum);
        c.setCellValue("Transaction Date");
        c = r.createCell(++cnum);
        c.setCellValue("Expiry Date");
        c = r.createCell(++cnum);
        c.setCellValue("Item Description");
        c = r.createCell(++cnum);
        c.setCellValue("Qty. Purchased");
        c = r.createCell(++cnum);
        c.setCellValue("Total Qty. To Package");
        c = r.createCell(++cnum);
        c.setCellValue("Balance");
        c = r.createCell(++cnum);
        c.setCellValue("Transaction Status");
        c = r.createCell(++cnum);
        c.setCellValue("Revalidation Ref. No.");
        c = r.createCell(++cnum);
        c.setCellValue("Remarks");

        for (InventoryReportModel theRow : result.getRows()) {
            r = s.createRow(++row);

            int innerCnum = 0;
            if (filter.getIsSysad()) {
                c = r.createCell(innerCnum);
                c.setCellValue(theRow.getOrgName());
            } else {
                innerCnum = -1;
            }
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getReceiptNum());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTxnDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getExpiryDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getItemDesc());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getQtyPurchased());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getQtyPackaged());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getBalance());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTransStatus());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getRevalidateReference());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getRemarks());
        }

        for (int i = 0; i <= cnum; i++) {
            s.autoSizeColumn(i);
        }

        return wb;
    }

    public Workbook generatePinReportExcel(ReportFilterVM filter, ReportResult<PackageReportModel> result) {
        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Package Report");
        Row r = null;
        Cell c = null;

        int row = 0;

        r = s.createRow(row);
        c = r.createCell(0);
        c.setCellValue("Transaction Date Range: From " + filter.getFromDate() + " To " + filter.getToDate());

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Receipt Number: " + (StringUtils.isEmpty(filter.getReceiptNum()) ? "-" : filter.getReceiptNum()));

        if (filter.getIsSysad()) {
            r = s.createRow(++row);
            c = r.createCell(0);
            c.setCellValue("Organization Name: " + (StringUtils.isEmpty(filter.getOrgName()) ? "-" : filter.getOrgName()));
        }

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Transaction Status: " + (StringUtils.isEmpty(filter.getStatus()) ? "All" : filter.getStatus()));

        r = s.createRow(++row);
        c = r.createCell(0);
        c.setCellValue("Ticket Media: " + (StringUtils.isEmpty(filter.getTicketMedia()) ? "All" : filter.getTicketMedia()));

        row++; //blank row

        r = s.createRow(++row);

        int cnum = 0;
        if (filter.getIsSysad()) {
            c = r.createCell(cnum);
            c.setCellValue("Organization Name");
        } else {
            cnum = -1;
        }
        c = r.createCell(++cnum);
        c.setCellValue("Ticket Media");
        c = r.createCell(++cnum);
        c.setCellValue("Date of Ticket Generation");
        c = r.createCell(++cnum);
        c.setCellValue("Expiry Date");
        c = r.createCell(++cnum);
        c.setCellValue("Package Name");
        c = r.createCell(++cnum);
        c.setCellValue("Package Description");
        c = r.createCell(++cnum);
        c.setCellValue("Individual Product(s)");
        c = r.createCell(++cnum);
        c.setCellValue("Sales Receipt No(s).");
        c = r.createCell(++cnum);
        c.setCellValue("Package Quantity");
        c = r.createCell(++cnum);
        c.setCellValue("Quantity Redeemed");
        c = r.createCell(++cnum);
        c.setCellValue("Last Redeemed Date");

        for (PackageReportModel theRow : result.getRows()) {
            r = s.createRow(++row);

            int innerCnum = 0;
            if (filter.getIsSysad()) {
                c = r.createCell(innerCnum);
                c.setCellValue(theRow.getOrgName());
            } else {
                innerCnum = -1;
            }
            c = r.createCell(++innerCnum);
            String ticketMediaStr = theRow.getTicketMedia();
            if("Pincode".equalsIgnoreCase(ticketMediaStr)) {
                ticketMediaStr = "PINCODE\n"+theRow.getPinCode();
            }else if("ETicket".equalsIgnoreCase(ticketMediaStr)) {
                ticketMediaStr = "E-Ticket";
            }else if("ExcelFile".equalsIgnoreCase(ticketMediaStr)) {
                ticketMediaStr = "Excel File";
            }
            c.setCellValue(ticketMediaStr);
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getTicketGeneratedDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getExpiryDateText());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getPackageName());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getIndivItems());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getIndivReceipts());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getItemQty());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getItemQtyRedeemed());
            c = r.createCell(++innerCnum);
            c.setCellValue(theRow.getLastRedemptionDateText());
        }

        for (int i = 0; i <= cnum; i++) {
            s.autoSizeColumn(i);
        }

        return wb;
    }

}
