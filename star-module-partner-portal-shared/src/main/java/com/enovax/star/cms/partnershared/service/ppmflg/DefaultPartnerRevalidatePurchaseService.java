package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGAxCheckoutCart;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.repository.ppmflg.*;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.jcr.util.PropertyUtil;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 10/10/16.
 */
@Service("PPMFLGIPartnerRevalidatePurchaseService")
public class DefaultPartnerRevalidatePurchaseService implements  IPartnerRevalidatePurchaseService {

    @Autowired
    PPMFLGInventoryTransactionRepository transRepo;

    @Autowired
    PPMFLGPartnerRepository partnerRepo;

    @Autowired
    @Qualifier("PPMFLGIRevalidationFeeService")
    IRevalidationFeeService revalidationFeeService;

    @Autowired
    StarTemplatingFunctions starfn;

    @Autowired
    private AxStarService axStarService;

    @Autowired
    PPMFLGRevalidationTransactionRepository revalTransRepo;

    @Autowired
    private PPMFLGAxSalesOrderRepository axSalesOrderRepo;

    @Autowired
    private PPMFLGAxCheckoutCartRepository axCheckoutCartRepo;

    @Autowired
    @Qualifier("PPMFLGIGSTRateService")
    IGSTRateService gstRateService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    @Transactional
    public PPMFLGRevalidationTransaction getRevalInfoBeforePayment(Integer transId, Integer adminId, String accname, boolean isAdmin) throws Exception {
        Date now = new Date();

        PPMFLGInventoryTransaction trans = transRepo.findOne(transId);
        Hibernate.initialize(trans.getRevalTransList());
        PPMFLGRevalidationTransaction irevalTrans = trans.getRevalidateTrans();
        if (irevalTrans != null
                && EnovaxTmSystemStatus.Success.toString().equals(
                irevalTrans.getTmStatus())) {
            throw new Exception("A Success Revalidation Receipt was found: "
                    + trans.getRevalidateTrans().getReceiptNum());
        }
        Hibernate.initialize(trans.getInventoryItems());

        PPMFLGPartner partner = partnerRepo.findByAccountCode(trans.getMainAccount().getAccountCode());
        BigDecimal revalFee = revalidationFeeService.getRevalidationFeeInBigDecimalByRevalidationFeeId(StoreApiChannels.PARTNER_PORTAL_MFLG, partner.getAxAccountNumber(), partner.getRevalFeeItemId());
        BigDecimal normalRelPrice = revalFee;
        BigDecimal topupRelPrice = BigDecimal.ZERO;
//        Integer topupRelItemCode = sysService.getObjByKey(
//                SysParamConst.TopupRevalidateItemCode.toString(),
//                Integer.class, true);

        String normalRelItemCode = partner.getRevalFeeItemId();
        String tmMerchantId = starfn.getDefaultMerchant(StoreApiChannels.PARTNER_PORTAL_MFLG.code);
        trans.setTmMerchantId(tmMerchantId);
        Integer revalMonth = partner.getRevalPeriodMonths();

        log.info("Loading Revalidate Parameters- normalRelPrice: "
                + normalRelPrice + " topupRelPrice: " + topupRelPrice
                + " revalMonth: " + revalMonth);


        //do the add to cart
        String receiptNum = ProjectUtils.generateReceiptNumber(StoreApiChannels.PARTNER_PORTAL_MFLG, now);

        ApiResult<String> axResult = addToCartAndCheckout(trans, partner, receiptNum);

        if(axResult.isSuccess())
        {
            PPMFLGRevalidationTransaction revelTrans = groupRevalidateTrans(irevalTrans,
                    trans, adminId, accname, isAdmin, normalRelItemCode, normalRelPrice, topupRelPrice, revalMonth,
                    receiptNum, tmMerchantId);
            revalTransRepo.save(revelTrans);
            return revelTrans;
        }


        return null;
    }

    @Override
    public ResultVM revalTransWithFreeCharge(Integer revalTransId) {
        return null;
    }


    @Transactional
    private ApiResult<String> addToCartAndCheckout(PPMFLGInventoryTransaction trans, PPMFLGPartner partner, String receiptNumber) {
        Node axProductNode = starfn.getAxProductByProductCode(StoreApiChannels.PARTNER_PORTAL_MFLG.code, partner.getRevalFeeItemId());
        String listingId = PropertyUtil.getString(axProductNode, AXProductProperties.ProductListingId.getPropertyName());

        //Add to Cart
        final AxStarInputAddCart axc = new AxStarInputAddCart();
        final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

        AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();
        axcItem.setProductId(Long.parseLong(listingId));
        axcItem.setQuantity(TransactionUtil.getTransRevalidateQty(trans, false));
        axcItems.add(axcItem);
        axc.setItems(axcItems);

        axc.setCartId("RevalTrans" + receiptNumber);
        axc.setCustomerId(partner.getAxAccountNumber());

        final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(StoreApiChannels.PARTNER_PORTAL_MFLG, axc);
        AxStarCart axStarCart;
        if (!axStarResult.isSuccess()) {
            log.info("Error encountered adding to cart. " + JsonUtil.jsonify(axStarResult));
            return new ApiResult<>(ApiErrorCodes.ErrorAddingToCart);
        }

        axStarCart = axStarResult.getData().getCart();

        //do checkout
        final AxStarServiceResult<AxStarCartNested> axStarCheckoutResult = axStarService.apiCheckout(StoreApiChannels.PARTNER_PORTAL_MFLG,
                axStarCart.getId(), axStarCart.getCustomerId(), receiptNumber);

        if (!axStarCheckoutResult.isSuccess()) {
            //revert the reservation...
            log.info("Error encountered on AxStarCheckout " + JsonUtil.jsonify(axStarCheckoutResult));
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartCheckout);
        }

        //set the checkout cart now
        AxStarCart checkoutCart = axStarCheckoutResult.getData().getCart();

        //save to the DB for complete sales
        PPMFLGAxCheckoutCart axCheckoutCart = new PPMFLGAxCheckoutCart();
        axCheckoutCart.setCheckoutCartJson(JsonUtil.jsonify(checkoutCart));
        axCheckoutCart.setReceiptNum(receiptNumber);
        axCheckoutCartRepo.save(axCheckoutCart);

        return new ApiResult<>(true, "", "", "");
    }


    private PPMFLGRevalidationTransaction groupRevalidateTrans(
            PPMFLGRevalidationTransaction irevalTrans, PPMFLGInventoryTransaction trans,
            Integer adminId, String accname, boolean isAdmin,
            String normalRelItemCode, BigDecimal normalRelPrice, BigDecimal topupRelPrice,
            Integer revalMonth, String receiptNum, String tmMerchantId) {
        Date validateEndDt = trans.getValidityEndDate();
        Date newValidityEndDate = TransactionUtil.getNewValidateEndDate(
                validateEndDt, revalMonth);
        PPMFLGRevalidationTransaction revalTrans = (irevalTrans != null ? irevalTrans
                : new PPMFLGRevalidationTransaction());

        revalTrans.setTransactionId(trans.getId());
        revalTrans.setMainAccountId(adminId);
        revalTrans.setUsername(accname);
        revalTrans.setSubAccountTrans(!isAdmin);
        revalTrans.setOldValidityEndDate(validateEndDt);
        revalTrans.setNewValidityEndDate(newValidityEndDate);
        int totalMainQty = TransactionUtil.getTransRevalidateQty(trans, false);
        int totalTopupQty = TransactionUtil.getTransRevalidateQty(trans, true);
        revalTrans.setTotalMainQty(totalMainQty);
        revalTrans.setTotalTopupQty(totalTopupQty);
        revalTrans.setRevalItemCode(normalRelItemCode);
        revalTrans.setRevalFeeInCents((normalRelPrice.multiply(new BigDecimal(
                100))).intValue());
        revalTrans.setRevalTopupFeeInCents(0);
        revalTrans.setReprintCount(0);
        revalTrans.setReceiptNum(receiptNum);
        revalTrans.setStatus(TicketStatus.Reserved.toString());
        revalTrans.setTmStatus(EnovaxTmSystemStatus.RedirectedToTm.toString());
        revalTrans.setTmMerchantId(tmMerchantId);
        revalTrans.setCurrency(PartnerPortalConst.DEFAULT_SYSTEM_CURRENCY_CODE);
        revalTrans.setCreatedDate(new Date());
        BigDecimal total = TransactionUtil.getTransRevalidateFee(trans,
                normalRelPrice);

        revalTrans.setTotal(total);

        String revalDetail = TransactionUtil.getRevalDetailInXml(trans);
        log.debug("revalDetail:"+revalDetail);
        revalTrans.setRevalDetail(revalDetail);

        BigDecimal gstRate = gstRateService.getCurrentGST();
        revalTrans.setGstRate(gstRate);

        return revalTrans;
    }
}
