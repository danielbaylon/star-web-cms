package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.*;
import com.enovax.star.cms.commons.jcrrepository.system.ppmflg.IUserRepository;
import com.enovax.star.cms.commons.model.partner.ppmflg.ChatHistVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentNotifyVM;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.service.email.DefaultMailCallback;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.constant.ppmflg.AdminUserRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by houtao on 29/9/16.
 */
@Service("PPMFLGIOfflinePaymentEmailService")
public class DefaultOfflinePaymentEmailService implements IOfflinePaymentEmailService {

    private static final Logger log = LoggerFactory.getLogger(DefaultOfflinePaymentEmailService.class);
    
    public static final String BASE_URL = "##BASE_URL##";
    public static final String ORG_NAME = "##ORG_NAME##";
    public static final String OWN_ORG_NAME = "##OWN_ORG_NAME##";
    public static final String OWN_CONTACT = "##OWN_CONTACT##";
    public static final String OWN_OPERATE_TIME = "##OWN_OPERATE_TIME##";
    public static final String MAILER_NAME = "##MAILER_NAME##";

    private static final String REQ_SUBMIT_CLIENT = "OFFLINE_PAY_SUBMIT_NOTIFY_CLIENT";
    private static final String REQ_SUBMIT_ADMIN = "OFFLINE_PAY_SUBMIT_NOTIFY_ADMIN";

    private static final String REQ_APPROVE_CLIENT = "OFFLINE_PAY_APPROVE_NOTIFY_CLIENT";
    private static final String REQ_APPROVE_ADMIN = "OFFLINE_PAY_APPROVE_NOTIFY_ADMIN";

    private static final String REQ_REJECT_CLIENT = "OFFLINE_PAY_REJECT_NOTIFY_CLIENT";
    private static final String REQ_REJECT_ADMIN = "OFFLINE_PAY_REJECT_NOTIFY_ADMIN";

    private static final String REQ_NOTIFY_CLIENT = "OFFLINE_PAY_NOTIFY_CLIENT";
    private static final String REQ_NOTIFY_ADMIN = "OFFLINE_PAY_NOTIFY_ADMIN";

    private static final String REQ_PARTNER_NOTIFY_CLIENT = "OFFLINE_PAY_PARTNER_NOTIFY_CLIENT";
    private static final String REQ_PARTNER_NOTIFY_ADMIN = "OFFLINE_PAY_PARTNER_NOTIFY_ADMIN";

    private static final String OFFLINE_PAY_APPROVER_RIGHT = AdminUserRoles.OfflinePaymentApproverRole.code;

    @Autowired
    private IMailService mailSrv;
    @Autowired
    @Qualifier("PPMFLGIEmailTemplateService")
    private IEmailTemplateService templateSrv;
    @Autowired
    @Qualifier("PPMFLGITemplateService")
    private ITemplateService tplSrv;
    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService paramSrv;
    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService adminSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedAccountService")
    private IPartnerSharedAccountService accSrv;
    @Autowired
    @Qualifier("PPMFLGIPartnerAccountSharedService")
    private IPartnerAccountSharedService accSharedSrv;
    @Autowired
    @Qualifier("PPMFLGIUserRepository")
    private IUserRepository adminUserRepo;

    private void populateOfflinePaymentApproverEmail(List<String> list){
        if(list == null){
            return;
        }
        List<String> emails = adminSrv.getAllAdminUsersEmailByRoleId(OFFLINE_PAY_APPROVER_RIGHT);
        if(emails != null){
            for(String i : emails){
                if(!list.contains(i)){
                    if(i.contains("@")){
                        list.add(i);
                    }
                }
            }
        }
    }

    private void populatePartnerEmail(PPMFLGInventoryTransaction trans, List<String> list){
        if(trans == null || list == null){
            return;
        }
        Integer mainAccId = trans.getMainAccountId();

        if(mainAccId == null || mainAccId.intValue() == 0){
            return;
        }
        PPMFLGTAMainAccount mainAcc = accSharedSrv.getActivePartnerByMainAccountId(mainAccId);
        if(mainAcc == null || mainAcc.getEmail() == null || mainAcc.getEmail().trim().length() == 0){
            return;
        }
        if(!list.contains(mainAcc.getEmail().trim())){
            list.add(mainAcc.getEmail().trim());
        }
    }

    private void populateRequestSubmitEmail(String username, List<String> list) {
        if(username == null || username.trim().length() == 0){
            return;
        }
        boolean isSub = false;
        PPMFLGTAAccount mac = accSrv.getMainUser(username, false, false);
        PPMFLGTASubAccount sac = null;
        if(mac == null){
            sac = accSrv.getSubUser(username, false, false);
        }
        if(mac == null && sac != null){
            mac = sac.getMainUser();
        }
        String mainEmail = mac != null ? mac.getEmail() : null;
        String subEmail =  sac != null ? sac.getEmail() : null;

        if(mainEmail != null && mainEmail.trim().length() > 0){
            if(!list.contains(mainEmail)){
                list.add(mainEmail);
            }
        }

        if(subEmail != null && subEmail.trim().length() > 0){
            if(!list.contains(subEmail)){
                list.add(subEmail);
            }
        }
    }

    private void populateOfflinePaymentAccountManagerEmail(PPMFLGInventoryTransaction trans, List<String> list){
        if(trans == null || list == null){
            return;
        }
        if(trans.getMainAccount() == null || trans.getMainAccount().getProfile() == null){
            return;
        }
        String accMgrId = trans.getMainAccount().getProfile().getAccountManagerId();
        if(accMgrId == null || accMgrId.trim().length() == 0){
            return;
        }
        String email = adminUserRepo.getUserEmailById(accMgrId);
        if(email == null || email.trim().length() == 0){
            return;
        }
        email = email.trim();
        if(!list.contains(email)){
            list.add(email);
        }
    }

    private String getPartnerName(PPMFLGInventoryTransaction trans){
        if(trans == null){
            return null;
        }
        if(trans.getMainAccount() == null || trans.getMainAccount().getProfile() == null){
            return null;
        }
        String orgName = trans.getMainAccount().getProfile().getOrgName();
        if(orgName == null || orgName.trim().length() == 0){
            return null;
        }
        return orgName.trim();
    }

    private String getReceiptNumber(PPMFLGInventoryTransaction trans){
        if(trans == null){
            return null;
        }
        return trans.getReceiptNum();
    }

    private String getOfflinePaymentRequestComments(PPMFLGOfflinePaymentRequest opr, boolean comments, boolean rejectreason, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks) {
        if(opr == null){
            return null;
        }
        String remarks = null;
        if(comments){
            if(isRemarksFromAdmin){
                remarks = opr.getApproverRemarks();
            }else{
                remarks = opr.getRemarks();
            }
        }
        if(rejectreason){
            remarks = opr.getApproverRemarks();
        }
        if(isFinalApprovalRemarks){
            remarks = opr.getFinalApprovalRemarks();
        }
        if(remarks == null || remarks.trim().length() == 0){
            return null;
        }
        ChatHistVM[] chatList = JsonUtil.fromJson(remarks.trim(), ChatHistVM[].class);
        if(chatList == null || chatList.length == 0){
            return null;
        }
        ChatHistVM vm = chatList[chatList.length -1];
        if(vm == null){
            return null;
        }
        return vm.getContent();
    }

    private String getNonString(String str){
        if(str != null && str.trim().length() > 0){
            return str.trim();
        }
        return "";
    }

    private void populateTargetEmails(PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans, List<String> partnerEmails, List<String> adminEmails){
        populatePartnerEmail(trans, partnerEmails);
        if(opr != null){
            populateRequestSubmitEmail(opr.getSubmitBy(), partnerEmails);
        }
        populateOfflinePaymentAccountManagerEmail(trans, adminEmails);
        populateOfflinePaymentApproverEmail(adminEmails);
    }

    private void populateDataMap(HashMap<String, String> dataMap, String name, String value) {
        if(dataMap == null || name == null || name.trim().length() == 0){
            return;
        }
        dataMap.put("##"+name+"##", value);
    }

    private HashMap<String, String> populdateEmailDataMap(PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans, boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks){
        HashMap<String, String> dataMap = new HashMap<String,String>();
        if(opr == null || trans == null){
            return dataMap;
        }
        String orgName    = getNonString(getPartnerName(trans));
        final String serverUrl = paramSrv.getApplicationContextPath();
        populateDataMap(dataMap, "ORG_NAME", orgName);
        if(isAdmin){
            populateDataMap(dataMap, "BASE_URL", serverUrl+"/");
        }else{
            populateDataMap(dataMap, "BASE_URL", serverUrl+"/"+ PartnerPortalConst.Partner_Portal_Channel);
        }
        if(hasComments){
            String comments = getNonString(getOfflinePaymentRequestComments(opr, true, false, isRemarksFromAdmin, isFinalApprovalRemarks));
            populateDataMap(dataMap, "OFFLINE_PAY_COMMENT", comments);
        }
        if(hasRejectReason){
            String comments = getNonString(getOfflinePaymentRequestComments(opr, false, true, isRemarksFromAdmin, isFinalApprovalRemarks));
            populateDataMap(dataMap, "REJECT_REASON", comments);
        }
        return dataMap;
    }
    private OfflinePaymentNotifyVM populateOfflinePaymentNotifyVM(OfflinePaymentNotifyVM opnvm, SysEmailTemplateVM template, HashMap<String, String> dataMap){
        String mailText = null;
        if(template != null && dataMap != null){
            mailText = getNonString(replaceMailParams(template.getBody(), dataMap));
        }
        opnvm.setMsg(mailText);
        return opnvm;
    }

    private String replaceMailParams(String body, HashMap<String, String> paramsMap) {
        if(paramsMap == null){
            paramsMap = new HashMap<String, String>();
        }
        Locale locale = new Locale("en");
        paramsMap.put(OWN_ORG_NAME, getNonString(paramSrv.getSystemParamValueByKey("sys.param.partner.owner.sentosa.organization.name")));
        paramsMap.put(OWN_CONTACT, getNonString(paramSrv.getSystemParamValueByKey("sys.param.partner.owner.sentosa.operating.info")));
        paramsMap.put(OWN_OPERATE_TIME, getNonString(paramSrv.getSystemParamValueByKey("sys.param.partner.owner.sentosa.contact.info")));
        Map<String,String> cfg = paramSrv.getAdminEmailCfgs();
        paramsMap.put(MAILER_NAME, cfg != null ? getNonString(cfg.get("MailUsername")) : "");
        return replaceParams(body, paramsMap);
    }
    public String replaceParams(String template, Map<String, String> params) {
        for (Map.Entry<String, String> param : params.entrySet()) {
            template = template.replace(param.getKey(), param.getValue());
        }
        return template;
    }

    private void broadcast(String trackingId, SysEmailTemplateVM template, List<String> emails, HashMap<String, String> dataMap, OfflinePaymentNotifyVM opnvm) {
        try {
            if(trackingId == null || template == null || emails == null || emails.size() == 0 || dataMap == null || opnvm == null){
                return;
            }
            opnvm = populateOfflinePaymentNotifyVM(opnvm, template, dataMap);
            String mailHtml = tplSrv.generateOfflinePaymentNotificationEmailHtml(opnvm);
            MailProperties prop = mailSrv.prepareMailProps(
                    trackingId,
                    template.getSubject(),
                    mailHtml,
                    true,
                    emails.toArray(new String[emails.size()]));
            mailSrv.sendEmail(prop, new DefaultMailCallback(log));
        } catch (Exception e) {
            log.error("sending offline payment email failed for "+trackingId, e);
            throw new RuntimeException("sending offline payment email failed for "+trackingId, e);
        }
    }

    public void sendingOfflinePaymentRequestSubmissionEmail(OfflinePaymentNotifyVM opnvm, PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans) {
        if(opr == null || trans == null){
            return;
        }
        // email to client
        // email to account manager & financial approver
        List<String> partnerEmails = new ArrayList<String>();
        List<String> adminEmails = new ArrayList<String>();

        populateTargetEmails(opr, trans, partnerEmails, adminEmails);

        SysEmailTemplateVM cltTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, REQ_SUBMIT_CLIENT);
        SysEmailTemplateVM admTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, REQ_SUBMIT_ADMIN);

        //boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks
        HashMap<String, String> taDataMap = populdateEmailDataMap(opr, trans, false, false, false, false, false);
        HashMap<String, String> adDataMap = populdateEmailDataMap(opr, trans, false, false, true, false, false);
        String receiptNum = getNonString(getReceiptNumber(trans));

        broadcast(REQ_SUBMIT_CLIENT + "_" + receiptNum +"_"+System.currentTimeMillis(), cltTmp, partnerEmails, taDataMap, opnvm);
        broadcast(REQ_SUBMIT_ADMIN + "_" + receiptNum +"_"+System.currentTimeMillis(), admTmp, adminEmails, adDataMap, opnvm);
    }


    @Override
    public void sendingOfflinePaymentRequestApprovedEmail(OfflinePaymentNotifyVM opnvm, PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans) {
        if(opr == null || trans == null){
            return;
        }
        // email to client pin code generation email
        // email to account manager & financial approver
        List<String> partnerEmails = new ArrayList<String>();
        List<String> adminEmails = new ArrayList<String>();

        populateTargetEmails(opr, trans, partnerEmails, adminEmails);

        SysEmailTemplateVM cltTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_APPROVE_CLIENT);
        SysEmailTemplateVM admTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_APPROVE_ADMIN);

        //boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks
        HashMap<String, String> taDataMap = populdateEmailDataMap(opr, trans, false, false, false, true, true);
        HashMap<String, String> adDataMap = populdateEmailDataMap(opr, trans, false, false, true, true, true);
        String receiptNum = getNonString(getReceiptNumber(trans));

        broadcast(REQ_APPROVE_CLIENT + "_" + receiptNum +"_"+System.currentTimeMillis(), cltTmp, partnerEmails, taDataMap, opnvm);
        broadcast(REQ_APPROVE_ADMIN + "_" + receiptNum +"_"+System.currentTimeMillis(), admTmp, adminEmails, adDataMap, opnvm);
    }

    @Override
    public void sendingOfflinePaymentRequestRejectedEmail(OfflinePaymentNotifyVM opnvm, PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans) {
        if(opr == null || trans == null){
            return;
        }
        // email to client
        // email to account manager & financial approver
        List<String> partnerEmails = new ArrayList<String>();
        List<String> adminEmails = new ArrayList<String>();

        populateTargetEmails(opr, trans, partnerEmails, adminEmails);

        SysEmailTemplateVM cltTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_REJECT_CLIENT);
        SysEmailTemplateVM admTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_REJECT_ADMIN);

        //boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks
        HashMap<String, String> taDataMap = populdateEmailDataMap(opr, trans, true, false, false, true, true);
        HashMap<String, String> adDataMap = populdateEmailDataMap(opr, trans, true, false, true, true, true);
        String receiptNum = getNonString(getReceiptNumber(trans));

        broadcast(REQ_REJECT_CLIENT + "_" + receiptNum +"_"+System.currentTimeMillis(), cltTmp, partnerEmails, taDataMap, opnvm);
        broadcast(REQ_REJECT_ADMIN + "_" + receiptNum +"_"+System.currentTimeMillis(), admTmp, adminEmails, adDataMap, opnvm);
    }

    @Override
    public void sendingOfflinePaymentCustomerNotification(OfflinePaymentNotifyVM opnvm, PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans) {
        if(opr == null || trans == null){
            return;
        }
        // email to client
        List<String> partnerEmails = new ArrayList<String>();
        List<String> adminEmails = new ArrayList<String>();

        populateTargetEmails(opr, trans, partnerEmails, adminEmails);

        SysEmailTemplateVM cltTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_NOTIFY_CLIENT);
        SysEmailTemplateVM admTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_NOTIFY_ADMIN);

        //boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks
        HashMap<String, String> taDataMap = populdateEmailDataMap(opr, trans, false, true, false, true, false);
        HashMap<String, String> adDataMap = populdateEmailDataMap(opr, trans, false, true, true, true, false);
        String receiptNum = getNonString(getReceiptNumber(trans));

        broadcast(REQ_NOTIFY_CLIENT + "_" + receiptNum +"_"+System.currentTimeMillis(), cltTmp, partnerEmails, taDataMap, opnvm);
        broadcast(REQ_NOTIFY_ADMIN + "_" + receiptNum +"_"+System.currentTimeMillis(), admTmp, adminEmails, adDataMap, opnvm);
    }

    @Override
    public void sendingOfflinePaymentPartnerToAdminNotification(OfflinePaymentNotifyVM opnvm, PPMFLGOfflinePaymentRequest opr, PPMFLGInventoryTransaction trans) {
        if(opr == null || trans == null){
            return;
        }
        // email to client
        List<String> partnerEmails = new ArrayList<String>();
        List<String> adminEmails = new ArrayList<String>();

        populateTargetEmails(opr, trans, partnerEmails, adminEmails);

        SysEmailTemplateVM cltTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_PARTNER_NOTIFY_CLIENT);
        SysEmailTemplateVM admTmp = templateSrv.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,REQ_PARTNER_NOTIFY_ADMIN);

        //boolean hasRejectReason, boolean hasComments, boolean isAdmin, boolean isRemarksFromAdmin, boolean isFinalApprovalRemarks
        HashMap<String, String> taDataMap = populdateEmailDataMap(opr, trans, false, true, false, false, false);
        HashMap<String, String> adDataMap = populdateEmailDataMap(opr, trans, false, true, true, false, false);
        String receiptNum = getNonString(getReceiptNumber(trans));

        broadcast(REQ_PARTNER_NOTIFY_CLIENT + "_" + receiptNum +"_"+System.currentTimeMillis(), cltTmp, partnerEmails, taDataMap, opnvm);
        broadcast(REQ_PARTNER_NOTIFY_ADMIN + "_" + receiptNum +"_"+System.currentTimeMillis(), admTmp, adminEmails, adDataMap, opnvm);
    }
}
