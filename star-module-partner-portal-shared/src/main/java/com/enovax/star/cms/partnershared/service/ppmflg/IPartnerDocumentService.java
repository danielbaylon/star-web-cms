package com.enovax.star.cms.partnershared.service.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocument;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerDocumentVM;
import info.magnolia.cms.beans.runtime.Document;

import java.util.List;

public interface IPartnerDocumentService {

    PPMFLGPADocument savePartnerDocuments(PPMFLGTAMainAccount taMainAcc, PPMFLGPartner pa, Document doc, String fileType) throws Exception;

    PPMFLGPADocument getPartnerDocumentById(Integer id);

    List<PartnerDocumentVM> getPartnerDocumentsByPartnerId(Integer paId);

    boolean isValidPartnerDocument(Document doc);

}
