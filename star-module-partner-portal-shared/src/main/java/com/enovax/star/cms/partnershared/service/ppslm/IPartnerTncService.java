package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.tnc.TncVM;

import java.util.List;

/**
 * Created by jennylynsze on 9/14/16.
 */
public interface IPartnerTncService {
    List<TncVM> getProductTncs(StoreApiChannels channel, List<String> prodIds);
}
