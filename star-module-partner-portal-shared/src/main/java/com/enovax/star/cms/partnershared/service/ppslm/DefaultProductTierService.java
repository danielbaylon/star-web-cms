package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.tier.ProductTier;
import com.enovax.star.cms.commons.datamodel.ppslm.tier.TierProductMapping;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ProductTierVM;
import com.enovax.star.cms.commons.util.PublishingUtil;
import com.enovax.star.cms.partnershared.repository.ppslm.IProductRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.IProductTierRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.ITierPartnerMappingRepository;
import com.enovax.star.cms.partnershared.repository.ppslm.ITierProductMappingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/12/16.
 */
@Service
public class DefaultProductTierService implements IProductTierService {

    @Autowired
    IProductTierRepository tierRepo;

    @Autowired
    IProductRepository productRepo;

    @Autowired
    ITierProductMappingRepository tierProductMappingRepo;

    @Autowired
    ITierPartnerMappingRepository tierPartnerMappingRepo;

    @Override
    public String getTierIdByPartnerId(String channel, Integer id) {
        return null;
    }

    /* @Override
    public List<ProductTierVM> getActiveTierVms() {
        List<ProductTier> tiers = tierRepo.getTiers(null);
        List<ProductTierVM> tierVms = new ArrayList<>();
        for (ProductTier tier : tiers) {
            ProductTierVM vm = new ProductTierVM(tier);
            if(!"A".equalsIgnoreCase(vm.getStatus())){
                continue;
            }
            tierVms.add(vm);
        }
        return tierVms;
    }*/

    @Override
    public List<ProductTierVM> getAllTierVms(String channel) {
        List<ProductTier> tiers = tierRepo.getTiers(channel);
        List<ProductTierVM> tierVms = new ArrayList<>();
        for (ProductTier tier : tiers) {
            tierVms.add(new ProductTierVM(tier));
        }
        return tierVms;
    }

    @Override
    public ProductTierVM getTierVmById(String channel, String tierId) {
        ProductTier productTier = tierRepo.getTier(channel,tierId);
        List<PPSLMPartner> partners = null;//partnerRepo.findByTierId(tierId);
        List<TierProductMapping> tierProdMaps = tierProductMappingRepo.getProductList(channel,tierId);
        ProductTierVM tierVm = new ProductTierVM(productTier,tierProdMaps);
        return tierVm;
    }

    @Override
    public ProductTier getProductTier(String channel, String tierId) {
        return tierRepo.getTier(channel,tierId);
    }

    @Override
    public ProductTierVM saveTier(String channel, ProductTierVM tierVm, String userNm) {
        ProductTier productTier = null;
        boolean isNew = false;
        if(tierVm.getPriceGroupId() !=null&&!tierVm.getPriceGroupId().equals("")){
            productTier = tierRepo.getTier(channel,tierVm.getPriceGroupId());
            productTier.setPriceGroupId(productTier.getPriceGroupId());

//            productTier.setModifiedBy(userNm);
//            productTier.setModifiedDate(new Date());
        }else{
            productTier = new ProductTier();
            productTier.setId(tierRepo.getNextId());
//            productTier.setCreatedBy(userNm);
//            productTier.setCreatedDate(new Date());
//            productTier.setModifiedBy(userNm);
//            productTier.setModifiedDate(new Date());
            isNew = true;
        }

        tierVm.setPriceGroupId(productTier.getPriceGroupId());
        //tierRepo.saveTier(tierVm);
        //this.processTierPartner(tierVm, tierVm.getPartnerVms(), isNew);
        this.proccessTierProd(channel,tierVm, tierVm.getProdVms(), isNew);
        return tierVm;
    }


    /*public void processTierPartner(Integer tierId, Integer newPartnerId) throws Exception {
        if(tierId == null || newPartnerId == null || tierId.intValue() == 0 || newPartnerId.intValue() == 0){
            return;
        }
        //all the partner that is no longer in the tier should be deleted
        String query = "//element(*, mgnl:content)[@tierId = '" + tierId + "']";

        boolean found = false;
        Iterable<Node> tierPartnerMappingNodes = null;
        try {
            tierPartnerMappingNodes = JcrRepository.query(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), JcrWorkspace.TierPartnerMappings.getNodeType(), query);
            Iterator<Node> tierPartnerMappingNodesIterator = tierPartnerMappingNodes.iterator();

            //update the tierId to the correct one now
            while(tierPartnerMappingNodesIterator.hasNext()) {
                Node mappingNode = tierPartnerMappingNodesIterator.next();
                String partnerId = mappingNode.getProperty("partnerId").getString();
                if(partnerId != null && partnerId.trim().length() > 0){
                    try{

                        Integer paId = Integer.parseInt(partnerId.trim());
                        if(paId != null && paId.intValue() == newPartnerId.intValue()){
                            found = true;
                            break;
                        }
                    }catch (Exception ex){}
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        if(!found){
            try {
                Integer nextId = tierPartnerMappingRepo.getNextId();
                Node newMappingNode = JcrRepository.createNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), "/", nextId.toString(), JcrWorkspace.TierPartnerMappings.getNodeType());
                newMappingNode.setProperty("tierId", tierId + "");
                newMappingNode.setProperty("partnerId", newPartnerId + "");
                JcrRepository.updateNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), newMappingNode);
                try {
                    PublishingUtil.publishNodes(newMappingNode.getIdentifier(), JcrWorkspace.TierPartnerMappings.getWorkspaceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
                throw new Exception(e);
            }
        }
    }

    private void processTierPartner(ProductTierVM productTierVM, List<PartnerVM> partnerVMs, boolean isNew) {
       List<Integer> partnerIds = new ArrayList<>();

        for(PartnerVM p: partnerVMs) {
            partnerIds.add(p.getId());
        }

        //all the partner that is no longer in the tier should be deleted
        String query = "//element(*, mgnl:content)[@tierId = '" + productTierVM.getId() + "']";

        Iterable<Node> tierPartnerMappingNodes = null;
        try {
            tierPartnerMappingNodes = JcrRepository.query(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), JcrWorkspace.TierPartnerMappings.getNodeType(), query);
            Iterator<Node> tierPartnerMappingNodesIterator = tierPartnerMappingNodes.iterator();

            //update the tierId to the correct one now
            while(tierPartnerMappingNodesIterator.hasNext()) {
                Node mappingNode = tierPartnerMappingNodesIterator.next();
                String partnerId = mappingNode.getProperty("partnerId").getString();
                if(!partnerIds.contains(partnerId)) {
                    mappingNode.setProperty("tierId", ""); //TODO dunno how to delete node
                    mappingNode.setProperty("partnerId", "");
                    JcrRepository.updateNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), mappingNode);
                    try {
                        PublishingUtil.publishNodes(mappingNode.getIdentifier(), JcrWorkspace.TierPartnerMappings.getWorkspaceName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    partnerIds.remove(partnerId); //remove those are ok na
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        //all the product that is in the tier but not yet save, should be added
        for(Integer p: partnerIds) {
            Integer nextId = tierPartnerMappingRepo.getNextId();
            try {
                Node newMappingNode = JcrRepository.createNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), "/", nextId.toString(), JcrWorkspace.TierPartnerMappings.getNodeType());
                newMappingNode.setProperty("tierId", productTierVM.getId() + "");
                newMappingNode.setProperty("partnerId", p + "");
                JcrRepository.updateNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), newMappingNode);
                try {
                    PublishingUtil.publishNodes(newMappingNode.getIdentifier(), JcrWorkspace.TierPartnerMappings.getWorkspaceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
    }*/


    private void proccessTierProd(String channel, ProductTierVM productTierVM, List<PartnerProductVM> productVMs, boolean isNew) {
        List<String> productIds = new ArrayList<>();

        //all the product that has mapping should update the related tier
        for(PartnerProductVM p: productVMs) {
            productIds.add(p.getId());
        }


        //all the product that is no longer in the tier should be deleted
        String query = "/jcr:root/"+ channel+ "//element(*, mgnl:tier-product-mapping)[@tierId = '" + productTierVM.getPriceGroupId() + "']";

        Iterable<Node> tierProductMappingNodes = null;
        try {
            tierProductMappingNodes = JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), JcrWorkspace.TierProductMappings.getNodeType(), query);
            Iterator<Node> tierProductMappingNodesIterator = tierProductMappingNodes.iterator();

            //update the tierId to the correct one now
            while(tierProductMappingNodesIterator.hasNext()) {
                Node mappingNode = tierProductMappingNodesIterator.next();
                String productId = mappingNode.getProperty("productId").getString();
                if(!productIds.contains(productId)) {

                    mappingNode.setProperty("tierId", ""); //TODO dunno how to delete node
                    mappingNode.setProperty("productId", "");
                    Node parent = mappingNode.getParent();
                    PublishingUtil.deleteNodeAndPublish(mappingNode,JcrWorkspace.TierProductMappings.getWorkspaceName());
//                    JcrRepository.updateNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), parent);
//                    try {
//                        PublishingUtil.publishNodes(mappingNode.getIdentifier(), JcrWorkspace.TierProductMappings.getWorkspaceName());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }else {
                    productIds.remove(productId); //remove those are ok na
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //all the product that is in the tier but not yet save, should be added
        for(String p: productIds) {
            Integer nextId = tierProductMappingRepo.getNextId();
            try {
                Node newMappingNode = JcrRepository.createNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), "/"+channel+"/", nextId.toString(), JcrWorkspace.TierProductMappings.getNodeType());
                newMappingNode.setProperty("tierId", productTierVM.getPriceGroupId());
                newMappingNode.setProperty("productId", p);
                JcrRepository.updateNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), newMappingNode);
                try {
                    PublishingUtil.publishNodes(newMappingNode.getIdentifier(), JcrWorkspace.TierProductMappings.getWorkspaceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }

    }

//    @Override
//    public void removeTier(String channel, List<Integer> rcmdIds, String userNm) {
//        for (Integer tmpId : rcmdIds) {
//            ProductTier productTier = tierRepo.getTier(tmpId);
//            Node productTierNode = productTier.getProductTier();
//            try {
//                productTierNode.setProperty("status", "D");
//                JcrRepository.updateNode(JcrWorkspace.ProductTiers.getWorkspaceName(), productTierNode);
//                try {
//                    PublishingUtil.publishNodes(productTierNode.getIdentifier(), JcrWorkspace.ProductTiers.getWorkspaceName());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } catch (RepositoryException e) {
//                e.printStackTrace();
//            }
//
//            /*TODO List<PPSLMPartner> partners = partnerRepo.findByTierId(tmpId);
//            for (PPSLMPartner opa : partners) {
//                Node partnerNode = opa.getPartner();
//                try {
//                    partnerNode.setProperty("tierId", "");
//                    JcrRepository.updateNode(JcrWorkspace.Partners.getWorkspaceName(), partnerNode);
//                    try {
//                        PublishingUtil.publishNodes(partnerNode.getIdentifier(), JcrWorkspace.Partners.getWorkspaceName());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } catch (RepositoryException e) {
//                    e.printStackTrace();
//                }
//
//            }*/
//
//            List<TierPartnerMapping> tierPartnerMaps = tierPartnerMappingRepo.getPartnerList(productTier.getId());
//            for (TierPartnerMapping otmp : tierPartnerMaps) {
//                Node mappingNode = otmp.getMappingNode();
//                try {
//                    mappingNode.setProperty("tierId", "");
//                    mappingNode.setProperty("partnerId", "");
//                    JcrRepository.updateNode(JcrWorkspace.TierPartnerMappings.getWorkspaceName(), mappingNode);
//                    try {
//                        PublishingUtil.publishNodes(mappingNode.getIdentifier(), JcrWorkspace.TierPartnerMappings.getWorkspaceName());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } catch (RepositoryException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            List<TierProductMapping> tierProdMaps = tierProductMappingRepo.getProductList(channel, productTier.getId());
//            for (TierProductMapping otmp : tierProdMaps) {
//                Node mappingNode = otmp.getMappingNode();
//                try {
//                    mappingNode.setProperty("tierId", "");
//                    mappingNode.setProperty("productId", "");
//                    JcrRepository.updateNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), mappingNode);
//                    try {
//                        PublishingUtil.publishNodes(mappingNode.getIdentifier(), JcrWorkspace.TierProductMappings.getWorkspaceName());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } catch (RepositoryException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}
