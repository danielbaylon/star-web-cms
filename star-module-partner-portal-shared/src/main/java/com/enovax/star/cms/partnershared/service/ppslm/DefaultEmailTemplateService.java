package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.jcrrepository.system.IEmailTemplateRepository;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultEmailTemplateService implements IEmailTemplateService {

    @Autowired
    private IEmailTemplateRepository mailRepo;

    @Override
    public SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey) {
        return mailRepo.getSystemParamByKey(appKey, paramKey);
    }

    @Override
    public boolean hasEmailTemplateByKey(String appKey, String paramKey) {
        return mailRepo.hasSystemParamByKey(appKey, paramKey);
    }

    public static final String CONTENT_PARTNER_FORGOT_PWD = "content-partner-forgot-pwd";

    public static final String CONTENT_GENERAL_FOOTER = "content-general-footer";
    public static final String CONTENT_PARTNER_REGISTRATION_RESUBMIT_EMAIL = "content-partner-registration-resubmit-email";
    public static final String CONTENT_PARTNER_REGISTRATION_APPROVED_EMAIL = "content-partner-registration-approved-email";
    public static final String CONTENT_PARTNER_REGISTRATION_REJECT_EMAIL = "content-partner-registration-reject-email";
    public static final String CONTENT_PARTNER_REGISTRATION_SIGNUP_EMAIL = "content-partner-registration-signup-email";
    public static final String CONTENT_PARTNER_REGISTRATION_SIGNUP_UPDATE_EMAIL = "content-partner-registration-signup-update-email";
    public static final String CONTENT_PARTNER_SUBACCOUNT_CREATION_EMAIL = "content-partner-subaccount-creation-email";
    public static final String CONTENT_PARTNER_SUBACCOUNT_PASSWORD_RESET_EMAIL = "content-partner-subaccount-password-reset-email";
    public static final String CONTENT_PARTNER_TRANSACTION_RECEIPT_EMAIL = "content-partner-transaction-receipt-email";
    public static final String CONTENT_PARTNER_REVAL_RECEIPT_EMAIL = "content-partner-reval-receipt-email";
    public static final String CONTENT_PARTNER_PINCODE_BODY = "content-partner-pincode-body";
    public static final String CONTENT_PARTNER_PINCODE_FOOTER = "content-partner-pincode-footer";
    public static final String CONTENT_PARTNER_WOT_CONFIRM = "content-partner-wot-confirm";
    public static final String CONTENT_PARTNER_WOT_SPLIT_CONFIRM = "content-partner-wot-split-confirm";
    public static final String CONTENT_PARTNER_WOT_AMENDMENT = "content-partner-wot-amendment";
    public static final String CONTENT_PARTNER_WOT_CANCEL = "content-partner-wot-cancel";
    public static final String CONTENT_PARTNER_WOT_GENERAL_INFO = "content-partner-wot-general-info";
    public static final String CONTENT_PARTNER_WOT_CANCEL_REMINDER = "content-admin-partner-wot-cancel-reminder";

    public static final String CONTENT_ADMIN_WOT_CONFIRM = "content-admin-partner-wot-confirm";
    public static final String CONTENT_ADMIN_WOT_UPDATE = "content-admin-partner-wot-update";
    public static final String CONTENT_ADMIN_WOT_RELEASE = "content-admin-partner-wot-release";
    public static final String CONTENT_ADMIN_WOT_POST_RELEASE_NOTIFICATION = "content-admin-partner-wot-post-release-notification";
    public static final String CONTENT_ADMIN_WOT_CANCEL = "content-admin-partner-wot-cancel";

    public static final String HANDLEBARS_APPROVER_ALERT = "handlebars-approver-alert";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_ALERT = "handlebars-partner-registration-alert";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_UPDATE_ALERT = "handlebars-partner-registration-update-alert";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_SIGNUP = "handlebars-partner-registration-signup";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_SIGNUP_UPDATE = "handlebars-partner-registration-signup-update";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_REJECTED = "handlebars-partner-registration-rejected";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_APPROVED = "handlebars-partner-registration-approved";
    public static final String HANDLEBARS_PARTNER_REGISTRATION_RESUBMIT = "handlebars-partner-registration-resubmit";

    public static final String HANDLEBARS_PARTNER_OFFLINE_PAYMENT = "handlebars-partner-offline-payment";

    public static final String HANDLEBARS_REVAL_RECEIPT_PDF = "handlebars-partner-reval-receipt-pdf";
    public static final String HANDLEBARS_TRANSACTION_RECEIPT_PDF = "handlebars-partner-transaction-receipt-pdf";
    public static final String HANDLEBARS_PINCODE_PDF = "handlebars-pincode-pdf";
    public static final String HANDLEBARS_ETICKET = "handlebars-eticket";
    public static final String HANDLEBARS_EXCEL = "handlebars-excel";

    public static final String HANDLEBARS_PARTNER_WOT = "handlebars-partner-wot";

    public static final String HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_PDF = "handlebars-partner-deposit-topup-receipt-pdf";
    public static final String HANDLEBARS_DEPOSIT_TOPUP_RECEIPT_EMAIL = "handlebars-partner-deposit-topup-receipt-email";

    public static final String HANDLEBARS_WOT_DAILY_RECEIPT_PDF = "handlebars-partner-wot-daily-receipt-template";
    public static final String HANDLEBARS_WOT_MONTHLY_RECEIPT_PDF = "handlebars-partner-wot-monthly-receipt-template";

}
