package com.enovax.star.cms.partnershared.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.tier.ProductTier;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxPriceGroup;
import com.enovax.star.cms.commons.util.PublishingUtil;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
@Repository
public class JcrProductTierRepository implements IProductTierRepository {

    @Override
    public List<ProductTier> getTiers(String channel) {
        try {
            List<ProductTier> tiers = new ArrayList<>();
            String baseQuery = "/jcr:root/"+ channel+ "//element(*, mgnl:product-tier)";
            Iterable<Node> tiersNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), baseQuery );
            if(tiersNodes != null)
            {
                Iterator<Node> tierNodesIterator = tiersNodes.iterator();
                while(tierNodesIterator.hasNext()) {
                    Node tierNode = tierNodesIterator.next();
                    int id = Integer.parseInt(tierNode.getName());
                    String priceGroupId = tierNode.getProperty("priceGroupId").getString();
                    String name = tierNode.getProperty("name").getString();
                    int isB2B = tierNode.getProperty("isB2B").getDecimal().intValue();
                    tiers.add(new ProductTier(id, priceGroupId,name,isB2B));
                }
            }
            return tiers;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ProductTier getTier(String channel, String tierId) {
        if(tierId != null && tierId != "") {
            try {
                ProductTier tier=null;
                String baseQuery = "/jcr:root/"+ channel+ "//element(*, mgnl:product-tier)[@priceGroupId='"+tierId+"']";
                Iterable<Node> tiersNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(),baseQuery);
                if(tiersNodes != null)
                {
                    Iterator<Node> tierNodesIterator = tiersNodes.iterator();
                    while(tierNodesIterator.hasNext()) {
                        Node tierNode = tierNodesIterator.next();
                        int id = Integer.parseInt(tierNode.getName());
                        String priceGroupId = tierNode.getProperty("priceGroupId").getString();
                        String name = tierNode.getProperty("name").getString();
                        int isB2B = tierNode.getProperty("isB2B").getDecimal().intValue();
                        tier = new ProductTier(id,priceGroupId,name,isB2B);
                    }
                }
                return tier;
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Integer getNextId() {
        Integer nextId = null;
        try {
            Node productTierSeqNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/sequences/product-tiers");
            int currId = Integer.parseInt(productTierSeqNode.getProperty("currentId").getString());
            nextId = currId + 1;
            productTierSeqNode.setProperty("currentId", currId + 1);
            JcrRepository.updateNode(JcrWorkspace.CMSConfig.getWorkspaceName(), productTierSeqNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return nextId ;
    }

    /*@Override
    public void saveTier(ProductTierVM tierVM) {
        try {
            Iterable<Node> tiersNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), "//element(*, mgnl:product-tier)[@priceGroupId='"+tierVM.getPriceGroupId()+"']");
            Iterator<Node> iterNodes = tiersNodes.iterator();
            boolean isExists = iterNodes.hasNext();
           //boolean isExists = JcrRepository.nodeExists(JcrWorkspace.ProductTiers.getWorkspaceName(), "/" + tierVM.getId());

            Node productTierNode  = null;

            if(isExists) {
               productTierNode = iterNodes.next();
            }else {
               productTierNode = JcrRepository.createNode(JcrWorkspace.ProductTiers.getWorkspaceName(), "/", tierVM.getId().toString(), JcrWorkspace.ProductTiers.getNodeType());
            }

            productTierNode.setProperty("name", tierVM.getName());
            productTierNode.setProperty("status", tierVM.getStatus());
            productTierNode.setProperty("description", tierVM.getDescription()==null?"":tierVM.getDescription());
            productTierNode.setProperty("exclusiveDeal", tierVM.isExclusiveDeal()?"Yes":"No");
            JcrRepository.updateNode(JcrWorkspace.ProductTiers.getWorkspaceName(), productTierNode);

            //TODO need handling for failed publish
            PublishingUtil.publishNodes(productTierNode.getIdentifier(), JcrWorkspace.ProductTiers.getWorkspaceName());

        } catch (RepositoryException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    public boolean hasPriceGroup(String channel,String priceGroupId) {
        List<ProductTier> tierList = getTiers(channel);
        for(ProductTier tier : tierList) {
            if (tier.getPriceGroupId().equals(priceGroupId))
                return true;
        }
        return false;
    }

    @Override
    public void createOrUpdatePriceGroup(String channel, AxPriceGroup priceGroup) {
        try {
            if(hasPriceGroup(channel,priceGroup.getPriceGroupId())) {
                String baseQuery = "/jcr:root/" + channel + "//element(*, mgnl:product-tier)[@priceGroupId='" + priceGroup.getPriceGroupId() + "']";
                Iterable<Node> tiersNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), baseQuery);
                Iterator<Node> tierIterator = tiersNodes.iterator();
                if (tierIterator.hasNext()) {
                    Node tierNode = tierIterator.next();
                    tierNode.setProperty("isB2B", priceGroup.isB2B() ? 1 : 0);
                    tierNode.setProperty("name", priceGroup.getName());
                    tierNode.getSession().save();
                    PublishingUtil.publishNodes(tierNode.getIdentifier(),JcrWorkspace.ProductTiers.getWorkspaceName());
                }
            }
            else {
                    Node tierNode = JcrRepository.createNode(JcrWorkspace.ProductTiers.getWorkspaceName(),"/"+channel,getNextId().toString(),JcrWorkspace.ProductTiers.getNodeType());
                    tierNode.setProperty("priceGroupId",priceGroup.getPriceGroupId());
                    tierNode.setProperty("name",priceGroup.getName());
                    tierNode.setProperty("isB2B",priceGroup.isB2B()?1:0);
                    tierNode.getSession().save();
                    PublishingUtil.publishNodes(tierNode.getIdentifier(),JcrWorkspace.ProductTiers.getWorkspaceName());
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removePriceGroup(String channel, String priceGroupId) {
        try {
            String baseQuery = "/jcr:root/" + channel + "//element(*, mgnl:product-tier)[@priceGroupId='" + priceGroupId + "']";
            Iterable<Node> tiersNodes = JcrRepository.query(JcrWorkspace.ProductTiers.getWorkspaceName(), JcrWorkspace.ProductTiers.getNodeType(), baseQuery);
            Iterator<Node> tierIterator = tiersNodes.iterator();
            if(tierIterator.hasNext()) {
                Node node = tierIterator.next();
                PublishingUtil.deleteNodeAndPublish(node,JcrWorkspace.ProductTiers.getWorkspaceName());
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}
