package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 7/11/16.
 */
public interface ITransQueryService {

    ApiResult<List<TransQueryVM>> getTransQueryResults(TransQueryFilterVM ft, int take, int skip, int page, int pageSize, String sortField, String sortDirection);

    ApiResult<List<PackageViewResultVM>> getPinCodeQueryResults(PinQueryFilterVM ft, int take, int skip, int page, int pageSize, String sortField, String sortDirection);

    ApiResult<List<PackageItemViewResultVM>> getPackageItemListByPackageId(Integer packageId);

    ApiResult<TransDetailVM> getTransDetails(String id, String transType) throws BizValidationException;

    String resendingTransReceipt(String id, String transType) throws BizValidationException;

    String resendingPkgReceipt(String id) throws BizValidationException;
}
