package com.enovax.star.cms.b2cshared.service;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.b2cshared.repository.IB2CCartManager;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.booking.CustomerIdType;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.booking.*;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@Service
public class DefaultB2CCartAndDisplayService implements IB2CCartAndDisplayService {

    private Logger log = LoggerFactory.getLogger(DefaultB2CCartAndDisplayService.class);

    @Autowired
    private AxStarService axStarService;
    @Autowired
    private IB2CCartManager cartManager;
    @Autowired
    private IB2CTransactionService transService;
    @Autowired
    private IB2CSysParamService paramService;

    @Autowired
    private StarTemplatingFunctions starfn;

    @Override
    public ApiResult<FunCartDisplay> rebuildCart(StoreApiChannels channel, String sessionId) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        return rebuildCart(channel, savedCart);
    }

    @Override
    public ApiResult<FunCartDisplay> rebuildCart(StoreApiChannels channel, FunCart theCart) {
        //TODO Evaluate whether this needs to be done still.
//        final AxStarCart oldAxCart = theCart.getAxCart();
//        final AxStarServiceResult<String> deleteOldCartResult =
//                axStarService.apiCartDeleteEntireCart(channel, oldAxCart.getId(), oldAxCart.getCustomerId());

        theCart.setAxCart(null);
        theCart.setAxCustomerId("");

        //This one is needed to handle multiple cart items with the same product ID.
        //Note that AX add to cart doesn't support multiple lines with the same product ID to be added in 1 transaction.
        final Map<Long, List<AxStarInputAddCartItem>> axcItemMap = new HashMap<>();

        final List<FunCartItem> funCartItems = theCart.getItems();
        for (FunCartItem funCartItem : funCartItems) {
            final AxStarInputAddCartItem axcItem = new AxStarInputAddCartItem();

            axcItem.setProductId(Long.parseLong(funCartItem.getListingId()));
            axcItem.setQuantity(funCartItem.getQty());
            axcItem.setComment(funCartItem.getAxLineCommentId());

            List<AxStarInputAddCartItem> axcItems = axcItemMap.get(axcItem.getProductId());
            if (axcItems == null) {
                axcItems = new ArrayList<>();
                axcItemMap.put(axcItem.getProductId(), axcItems);
            }
            axcItems.add(axcItem);
        }

        String axCartId = "";
        String axCustomerId = "";

        while (!axcItemMap.isEmpty()) {
            final AxStarInputAddCart axc = new AxStarInputAddCart();
            axc.setCartId(axCartId);
            axc.setCustomerId(axCustomerId);

            final List<Long> keysToRemove = new ArrayList<>();

            final List<AxStarInputAddCartItem> axcItems = new ArrayList<>();

            for (Map.Entry<Long, List<AxStarInputAddCartItem>> axcItemMapEntry : axcItemMap.entrySet()) {
                final List<AxStarInputAddCartItem> itams = axcItemMapEntry.getValue();

                final AxStarInputAddCartItem axcItem = itams.get(0);
                axcItems.add(axcItem);

                itams.remove(0);
                if (itams.isEmpty()) {
                    keysToRemove.add(axcItemMapEntry.getKey());
                }
            }

            for (Long itemKey : keysToRemove) {
                axcItemMap.remove(itemKey);
            }
            keysToRemove.clear();

            axc.setItems(axcItems);

            final AxStarServiceResult<AxStarCartNested> axStarResult = axStarService.apiCartAddItem(channel, axc);
            if (!axStarResult.isSuccess()) {
                log.info("Error encountered rebuilding cart due to API add cart failure: " + JsonUtil.jsonify(axStarResult));
                log.info("Clearing cart contents...");
                //TODO Clear cart contents

                return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
            }

            final AxStarCart updatedAxCart = axStarResult.getData().getCart();

            axCartId = updatedAxCart.getId();
            axCustomerId = updatedAxCart.getCustomerId();

            if (axcItemMap.isEmpty()) {
                theCart.setAxCustomerId(updatedAxCart.getCustomerId());
                theCart.setAxCart(updatedAxCart);
            }
        }

        //TODO During error handling, evaluate if need to clear ax cart again.
        //TODO Check whether promo code validation (and other validation) is also needed.

        final Map<String, String> appliedPromoCodes = theCart.getAppliedPromoCodes();
        if (!appliedPromoCodes.isEmpty()) {
            for (String pc : appliedPromoCodes.keySet()) {
                final AxStarServiceResult<String> pcResult = axStarService.apiCartApplyPromoCode(channel, axCartId, axCustomerId, pc);
                if (!pcResult.isSuccess()) {
                    log.info("Error encountered rebuilding cart due to API add promo code failure: " + JsonUtil.jsonify(pcResult));
                    log.info("Clearing cart contents...");
                    //TODO Clear cart contents

                    return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
                }
            }
        }

        final List<Long> affIds = new ArrayList<>();

        final FunCartAffiliation bankAffiliation = theCart.getBankAffiliation();
        if (bankAffiliation != null) {
            affIds.add(bankAffiliation.getAffiliationId());
        }

        final FunCartAffiliation payModeAffiliation = theCart.getPaymentModeAffiliation();
        if (payModeAffiliation != null) {
            affIds.add(payModeAffiliation.getAffiliationId());
        }

        if (!affIds.isEmpty()) {
            final AxStarServiceResult<String> addAffResult = axStarService.apiCartAddAffiliation(channel, axCartId, axCustomerId, affIds);
            if (!addAffResult.isSuccess()) {
                log.info("Error encountered rebuilding cart due to API add affiliations failure: " + JsonUtil.jsonify(addAffResult));
                log.info("Clearing cart contents...");
                //TODO Clear cart contents

                return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
            }
        }

        final AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(channel, axCartId, axCustomerId);
        if (!getCartResult.isSuccess()) {
            log.info("Error encountered rebuilding cart due to API get updated cart failure: " + JsonUtil.jsonify(getCartResult));
            log.info("Clearing cart contents...");
            //TODO Clear cart contents

            return new ApiResult<>(ApiErrorCodes.CartErrorRebuildCart);
        }

        final AxStarCart latestAxCart = getCartResult.getData().getCart();
        theCart.setAxCart(latestAxCart);
        cartManager.saveCart(channel, theCart);

        final FunCartDisplay funCartDisplay = constructCartDisplay(theCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public FunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId) {
        final FunCart funCart = cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            FunCartDisplay emptyCart = new FunCartDisplay();
            emptyCart.setCartId(sessionId);
            return emptyCart;
        }

        return constructCartDisplay(funCart);
    }

    @Override
    public FunCartDisplay constructCartDisplay(FunCart cart) {
        final FunCartDisplay cd = new FunCartDisplay();
        cd.setCartId(cart.getCartId());

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;

        final List<AxStarCartLine> cartLines = cart.getAxCart().getCartLines();
        final Map<String, List<AxStarCartLine>> cartLineMap = new HashMap<>();
        for (AxStarCartLine cl : cartLines) {
            final String lineComment = cl.getComment();
            List<AxStarCartLine> lineList = cartLineMap.get(lineComment);
            if (lineList == null) {
                lineList = new ArrayList<>();
                cartLineMap.put(lineComment, lineList);
            }
            lineList.add(cl);
        }

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();
        for (FunCartItem item : cart.getItems()) {
            final boolean isTopup = item.isTopup();
            final String productId = isTopup ? item.getParentCmsProductId() : item.getCmsProductId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getCmsProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            final String axCommentId = item.getAxLineCommentId();
            final List<AxStarCartLine> relatedLines = cartLineMap.get(axCommentId);

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();

            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();
            itemDisplay.setListingId(item.getListingId());
            itemDisplay.setProductCode(item.getProductCode());
            itemDisplay.setIsTopup(isTopup);
            if (itemDisplay.isTopup()) {
                itemDisplay.setParentListingId(item.getParentListingId());
            }
            itemDisplay.setType(item.getType());
            itemDisplay.setName(item.getName());

            final boolean isEvent = StringUtils.isNotEmpty(item.getEventGroupId());

            itemDisplay.setDescription(
                    "Ticket Type: " + item.getType() +
                            (isEvent && item.isOpenDate() ? "<br>Event Session: " + item.getEventSessionName() +
                                    "<br>Event Date: " + item.getSelectedEventDate()
                                    : "")
            );

            itemDisplay.setEventGroupId(item.getEventGroupId());
            itemDisplay.setEventLineId(item.getEventLineId());
            itemDisplay.setSelectedEventDate(item.getSelectedEventDate());
            itemDisplay.setEventSessionName(item.getEventSessionName());

            itemDisplay.setCartItemId(item.getCartItemId());

            int itemQty = 0;
            BigDecimal itemUnitPrice = item.getPrice();
            BigDecimal itemTotalAmount = BigDecimal.ZERO;
            BigDecimal itemDiscountAmount = BigDecimal.ZERO;

            final List<AxStartCartLineDiscountLine> itemDiscountLines = new ArrayList<>();

            for (AxStarCartLine line : relatedLines) {
                final int lineQty = line.getQuantity();

                itemQty += lineQty;
                itemTotalAmount = itemTotalAmount.add(line.getTotalAmount());
                itemDiscountAmount = itemDiscountAmount.add(line.getDiscountAmount());

                for (AxStartCartLineDiscountLine discountLine : line.getDiscountLines()) {
                    itemDiscountLines.add(discountLine);
                }
            }

            itemDisplay.setQty(itemQty);
            itemDisplay.setPrice(itemUnitPrice);
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(itemUnitPrice, "S$ "));
            itemDisplay.setTotal(itemTotalAmount);
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemTotalAmount, "S$ "));
            itemDisplay.setDiscountTotal(itemDiscountAmount);
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDiscountAmount, "S$ "));

            StringBuilder sb = new StringBuilder("");
            Map<String, Boolean> appliedDiscountDisplayIds = new HashMap<>();
            for (AxStartCartLineDiscountLine discountLine : itemDiscountLines) {
                if (appliedDiscountDisplayIds.get(discountLine.getOfferId()) == null) {
                    appliedDiscountDisplayIds.put(discountLine.getOfferId(), true);
                    //TODO Must retrieve from promotion content in JCR
                    sb.append(discountLine.getOfferName() + "<br/>");
                    itemDisplay.getOfferIds().add(discountLine.getOfferId());
                }
            }
            if (sb.length() > 4) {
                sb.setLength(sb.length() - 4);
            }
            itemDisplay.setDiscountLabel(sb.toString());

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            productDisplay.reorganiseItems();

            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));
        }

        cd.setCmsProducts(cmsProducts);
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        return cd;
    }


    @Override
    public ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber, boolean isAdmin) {
        final StoreTransactionExtensionData extData = transService.getStoreTransactionExtensionData(channel, receiptNumber);
        final StoreTransaction txn = extData.getTxn();
        if (txn == null) {
            return null;
        }

        ReceiptDisplay receiptDisplay = getReceiptForDisplay(txn);

        if(isAdmin) {
            if(StoreApiChannels.B2C_SLM.equals(channel)) {
                B2CSLMStoreTransaction storeTxn = transService.getB2CSLMStoreTransaction(receiptNumber);

                final String transStatus = storeTxn.getTmStatus();
                receiptDisplay.setTransStatus(StringUtils.isEmpty(transStatus) ? "Incomplete"
                        : transStatus);
                receiptDisplay.setIsError(!StringUtils.isEmpty(transStatus) && !EnovaxTmSystemStatus.Success.name().equalsIgnoreCase(transStatus));

                if(receiptDisplay.isError()) {
                    receiptDisplay.setErrorCode(storeTxn.getTmError());
                    receiptDisplay.setErrorMessage(storeTxn.getTmErrorMessage());

                }

                receiptDisplay.setCcDigits(storeTxn.getMaskedCc());

            }else if(StoreApiChannels.B2C_MFLG.equals(channel)) {
                B2CMFLGStoreTransaction storeTxn = transService.getB2CMFLGtoreTransaction(receiptNumber);

                final String transStatus = storeTxn.getTmStatus();
                receiptDisplay.setTransStatus(StringUtils.isEmpty(transStatus) ? "Incomplete"
                        : transStatus);
                receiptDisplay.setIsError(!StringUtils.isEmpty(transStatus) && !EnovaxTmSystemStatus.Success.name().equalsIgnoreCase(transStatus));

                if(receiptDisplay.isError()) {
                    receiptDisplay.setErrorCode(storeTxn.getTmError());
                    receiptDisplay.setErrorMessage(storeTxn.getTmErrorMessage());

                }

                receiptDisplay.setCcDigits(storeTxn.getMaskedCc());

            }


        }

        return receiptDisplay;

    }



    @Override
    public ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber) {
        return getReceiptForDisplayByReceipt(channel, receiptNumber, false);
    }

    //TODO add an admin function pls, so that the credit card info will be displyaed only if admin function
    @Override
    public ReceiptDisplay getReceiptForDisplay(StoreTransaction txn) {
        final ReceiptDisplay rd = new ReceiptDisplay();

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();
        for (StoreTransactionItem item : txn.getItems()) {
            final String productId = item.getCmsProductId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getCmsProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();
            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();
            itemDisplay.setListingId(item.getListingId());
            itemDisplay.setProductCode(item.getProductCode());

            Node axProduct = starfn.getAxProductByProductCode(txn.getChannel(), item.getProductCode());
            if(axProduct != null) {
                itemDisplay.setProductDescription(PropertyUtil.getString(axProduct, AXProductProperties.Description.getPropertyName()));
            }

            itemDisplay.setIsTopup(item.isTopup());
            itemDisplay.setParentListingId(item.getParentListingId());
            itemDisplay.setType(item.getType());
            itemDisplay.setName(item.getName());
            itemDisplay.setDescription(item.getDisplayDetails());
            itemDisplay.setQty(item.getQty());
            itemDisplay.setPrice(item.getUnitPrice());
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(item.getUnitPrice(), "S$ "));
            itemDisplay.setTotal(item.getSubtotal());
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getTotal(), "S$ "));
            itemDisplay.setDiscountTotal(item.getDiscountTotal());
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getDiscountTotal(), "S$ "));
            itemDisplay.setDiscountLabel(item.getDiscountLabel());

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            productDisplay.reorganiseItems();
            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));
        }

        rd.setCmsProducts(cmsProducts);
        rd.setTotalMainQty(totalMainQty);
        rd.setTotalQty(totalQty);
        rd.setTotal(total);
        rd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        final CustomerDetails customer = new CustomerDetails();
        customer.setDob(txn.getCustDob());
        customer.setEmail(txn.getCustEmail());
        customer.setMobile(txn.getCustMobile());
        customer.setName(txn.getCustName());
        customer.setNationality(txn.getCustNationality());
        customer.setPaymentType(txn.getPaymentType());
        customer.setReferSource(txn.getCustReferSource());
        customer.setSubscribed(txn.isCustSubscribe());

        customer.setIdType(txn.getCustIdType());
        customer.setIdNo(txn.getCustIdNo());
        customer.setSalutation(txn.getCustSalutation());
        customer.setCompanyName(txn.getCustCompanyName());
        customer.setSelectedMerchant(txn.getTmSelectedMerchant());
        customer.setMerchantId(txn.getTmMerchantId());
        customer.setIdText(String.format("(%s) %s", CustomerIdType.NricFin
                .toString().equals(txn.getCustIdType()) ? "NRIC/FIN"
                : "Passport", txn.getCustIdNo()));

        rd.setCustomer(customer);

        rd.setDateOfPurchase(NvxDateUtils.formatDate(txn.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        rd.setReceiptNumber(txn.getReceiptNumber());

        rd.setPaymentType(txn.getPaymentType());
        rd.setTransStatus(txn.getStage());
        rd.setTotalTotal(rd.getTotal());
        rd.setTotalTotalText(rd.getTotalText());
        rd.setPinCode(txn.getPinCode());
        rd.setHasPinCode(txn.isPinToGenerate());

        String locale = txn.getMgnlLocale();
        if (StringUtils.isEmpty(locale)) {
            locale = "en"; //default english
        }

        final List<TncVM> generalTncs = starfn.getGeneralTermsAndConditions(txn.getChannel(), locale);
        if(generalTncs!=null && !generalTncs.isEmpty()){
            rd.getTncs().addAll(generalTncs);
            rd.setHasTnc(true);
        }

        final List<String> productIds = new ArrayList<>();
        for(FunCartDisplayCmsProduct product : rd.getCmsProducts())  {
            productIds.add(product.getId());
        }

        final List<TncVM> productTncs = starfn.getProductTermsAndConditions(txn.getChannel(), productIds, locale);
        if(productTncs!=null && !productTncs.isEmpty()) {
            rd.getTncs().addAll(productTncs);
            rd.setHasTnc(true);
        }

        final String appContextRoot = paramService.getSystemParamValueByKey(txn.getChannel(), DefaultB2CSysParamService.SYS_PARAM_B2C_APP_CONTEXT_ROOT);
        rd.setBaseUrl(appContextRoot);

        return rd;
    }
}
