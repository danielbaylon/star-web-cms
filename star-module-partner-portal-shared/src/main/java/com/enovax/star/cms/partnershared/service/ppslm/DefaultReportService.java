package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackage;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackageItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.repository.ppslm.*;
import com.enovax.star.cms.partnershared.repository.ppslm.ReportDao;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 8/11/16.
 */
@Service
public class DefaultReportService implements IReportService {
    Logger log = LoggerFactory.getLogger(DefaultReportService.class);
    @Autowired
    PPSLMPartnerRepository partnerRepository;
    @Autowired
    private ISystemParamService systemParamService;
    @Autowired
    private PPSLMInventoryTransactionItemRepository inventoryTransactionItemRepository;
    @Autowired
    private ReportDao reportDao;
    @Autowired
    private ReportExcelGenerator excelGen;
    @Autowired
    private PPSLMMixMatchPackageItemRepository mmPkgItemRepo;

    @Override
    public TransactionReportInitVM initTransPage(boolean isSysAdmin, Integer mainAccountId) {
        List<PartnerVM> partnerVMs = getPartnerVMsByStatus(PartnerStatus.Active.code);
        int recsPerPage = systemParamService.getRecsPerPage();
        List<FilterProductTopLevelVM> filterProductTopLevelVMs = getTransItemsFilter(isSysAdmin,mainAccountId);
        TransactionReportInitVM transactionReportInitVM = new TransactionReportInitVM(partnerVMs,recsPerPage,filterProductTopLevelVMs);
        return transactionReportInitVM;
    }

    private List<FilterProductTopLevelVM> getTransItemsFilter(boolean isSysAdmin, Integer mainAccountId) {
        if(isSysAdmin) {
            List<FilterProductItemRowVM> filterProductItemRowVMs = inventoryTransactionItemRepository.getItemsFilter();
            return FilterProductTopLevelVM.convert(filterProductItemRowVMs);
        }else {
            List<FilterProductItemRowVM> filterProductItemRowVMs = inventoryTransactionItemRepository.getItemsFilterByMainAccount(mainAccountId);
            return FilterProductTopLevelVM.convert(filterProductItemRowVMs);
        }
    }

    private List<PartnerVM> getPartnerVMsByStatus(String status) {
        List<PPSLMPartner> partners = partnerRepository.findByStatus(status);
        List<PartnerVM> paVms = new ArrayList<PartnerVM>();
        for(PPSLMPartner pa:partners) {
            PartnerVM partnerVM = new PartnerVM();
            partnerVM.setId(pa.getId());
            partnerVM.setOrgName(pa.getOrgName());
            paVms.add(partnerVM);
        }
        return paVms;
    }

    @Override
    @Transactional(readOnly = true)
    public ReportResult<TransReportModel> generateTransReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception {
        return reportDao.generateTransReport(filter,skip,take,orderBy,asc,isCount);
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generateTransReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception {
        final ReportResult<TransReportModel> result = this.reportDao.generateTransReport(filter, -1, -1, orderBy, asc, false);
        final Workbook wb = this.excelGen.generateTransReportExcel(filter, result);
        return wb;
    }

    @Override
    public TransactionReportInitVM initInventoryPage(boolean isSysAdmin, Integer mainAccountId) {
        List<PartnerVM> partnerVMs = getPartnerVMsByStatus(PartnerStatus.Active.code);
        int recsPerPage = systemParamService.getRecsPerPage();
        List<FilterProductTopLevelVM> filterProductTopLevelVMs = getTransItemsFilter(isSysAdmin,mainAccountId);
        TransactionReportInitVM transactionReportInitVM = new TransactionReportInitVM(partnerVMs,recsPerPage,filterProductTopLevelVMs);
        return transactionReportInitVM;
    }

    @Override
    @Transactional(readOnly = true)
    public ReportResult<InventoryReportModel> generateInventoryReport(ReportFilterVM filter, Integer skip, Integer take,
                                                              String orderBy, boolean asc, boolean isCount) throws Exception {
        return reportDao.generateInvReport(filter,skip,take,orderBy,asc,isCount);
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generateInventoryReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception {
        final ReportResult<InventoryReportModel> result = this.reportDao.generateInvReport(filter, -1, -1, orderBy, asc, false);
        final Workbook wb = this.excelGen.generateInventoryReportExcel(filter, result);
        return wb;
    }

    @Override
    public TransactionReportInitVM initPackagePage(boolean isSysAdmin, Integer mainAccountId) {
        List<PartnerVM> partnerVMs = getPartnerVMsByStatus(PartnerStatus.Active.code);
        int recsPerPage = systemParamService.getRecsPerPage();
        List<FilterProductTopLevelVM> filterProductTopLevelVMs = getTransItemsFilter(true,mainAccountId);
        TransactionReportInitVM transactionReportInitVM = new TransactionReportInitVM(partnerVMs,recsPerPage,filterProductTopLevelVMs);
        return transactionReportInitVM;
    }

    @Override
    @Transactional(readOnly = true)
    public ReportResult<PackageReportModel> generatePackageReport(ReportFilterVM filter, Integer skip, Integer take,
                                                                      String orderBy, boolean asc, boolean isCount) throws Exception {
        return reportDao.generatePkgReport(filter,skip,take,orderBy,asc,isCount);
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generatePackageReportExcel(ReportFilterVM filter, String orderBy, boolean asc) throws Exception {
        final ReportResult<PackageReportModel> result = this.reportDao.generatePkgReport(filter, -1, -1, orderBy, asc, false);
        final Workbook wb = this.excelGen.generatePinReportExcel(filter, result);
        return wb;
    }

    @Override
    public List<PartnerVM> initExceptionPage() {
        List<PartnerVM> partnerVMs = getPartnerVMsByStatus(PartnerStatus.Active.code);
        return partnerVMs;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultVM getExceptionReportPage(ExceptionRptFilterVM fliter,String sortField, String sortDirection, int page, int pageSize) {
        log.info("getFinanceReconDetail:");
        ResultVM res = new ResultVM();
        List<ExceptionRptDetailsVM> baseResults = reportDao.getFinReconBaseResult();
        if(baseResults==null||baseResults.size()==0){
            return res;
        }
        fliter= prepareFilter(fliter,baseResults);
        List<ExceptionRptDetailsVM> finReconVms = reportDao.getFinReconPage(fliter, sortField, sortDirection, page, pageSize);
        this.getTransactionDetail(finReconVms, baseResults);
        int total = reportDao.getFinReconSize(fliter);
        res.setViewModel(finReconVms);
        res.setTotal(total);
        return res;
    }

    private void getTransactionDetail(List<ExceptionRptDetailsVM> finReconVms,
                                      List<ExceptionRptDetailsVM> baseResults) {
        for (ExceptionRptDetailsVM reconVm : finReconVms) {
            for (ExceptionRptDetailsVM rest : baseResults) {
                if (reconVm.getPkgId().equals(rest.getPkgId())) {
                    reconVm.setPurchasedQty(rest.getPurchasedQty());
                    reconVm.setPinCodeQty(rest.getPinCodeQty());
                    reconVm.setQtyRedeemed(rest.getQtyRedeemed());

                    reconVm.setV1(rest.getQtyRedeemed()-rest.getPurchasedQty());
                    reconVm.setV2(rest.getQtyRedeemed()-rest.getPinCodeQty());
                    int v3 = 0;
                    List<PPSLMMixMatchPackageItem> items = mmPkgItemRepo
                            .getPkgItemsByItemCode(rest.getPkgId(),
                                    rest.getItemProductCode());
                    for (PPSLMMixMatchPackageItem item : items) {
                        reconVm.getPkgItems().add(new ExceptionRptPkgItemVM(item));
                        if(item.getQty()>item.getTransItem().getQty()){
                            v3 = v3 + item.getQty() - item.getTransItem().getQty();
                        }
                    }
                    reconVm.setV3(v3);
                }
            }
        }
    }

    private ExceptionRptFilterVM prepareFilter(ExceptionRptFilterVM fliter,List<ExceptionRptDetailsVM> baseResults ){

        if(baseResults==null||baseResults.size()==0){
            return null;
        }
        Integer[] pkgIds = new Integer[baseResults.size()];
        for(int i = 0;i<baseResults.size();i++){
            pkgIds[i] = baseResults.get(i).getPkgId();
        }
        fliter.setPkgIds(pkgIds);
        return fliter;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExceptionRptDetailsVM> getExceptionReportList(ExceptionRptFilterVM fliter) {
        List<ExceptionRptDetailsVM> baseResults = reportDao.getFinReconBaseResult();
        if(baseResults==null||baseResults.size()==0){
            return new ArrayList<ExceptionRptDetailsVM>();
        }
        fliter= prepareFilter(fliter,baseResults);
        List<ExceptionRptDetailsVM> finReconVms = reportDao.getFinReconPage(fliter, null, null, null, null);
        this.getTransactionDetail(finReconVms, baseResults);
        return finReconVms;
    }
}
