package com.enovax.star.cms.partnershared.service.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAuditTrail;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMAuditTrailRepository;
import com.enovax.star.cms.partnershared.constant.AuditAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by lavanya on 5/9/16.
 */
@Service
public class DefaultAuditTrailService implements IAuditTrailService {
    protected Logger log = LoggerFactory.getLogger(DefaultAuditTrailService.class);

    @Autowired
    private PPSLMAuditTrailRepository auditTrailRepository;

    @Override
    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public boolean log(boolean includeDb, AuditAction action, String details, String relatedEntities,
                       String relatedEntityKeys, String performedBy) {

        log.info("[Audit] Action: {} | Performed By: {} \nDetails: {} \nRelated Entities: {} | Related Entity Keys: {}",
                new Object[]{ action.toString(), performedBy, details, relatedEntities, relatedEntityKeys });

        if (includeDb) {
            try {
                final Date now = new Date();
                final PPSLMAuditTrail rec = new PPSLMAuditTrail();
                rec.setAction(action.toString());
                rec.setDetails(details);
                rec.setRelatedEntities(relatedEntities);
                rec.setRelatedEntityKeys(relatedEntityKeys);
                rec.setPerformedBy(performedBy);
                rec.setCreatedDate(now);

                this.auditTrailRepository.save(rec);
            } catch (Exception e) {
                log.error("Error saving audit trail record.", e);
                return false;
            }
        }

        return true;
    }

}
