package com.enovax.star.cms.partnershared.model.grid;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class ProdFilter {
    private String prodName;
    private Integer cateId;
    private Boolean status;
    private String[] prodIds;
    private String productLevel;
    private String channel;

    public ProdFilter(String prodName, Integer cateId, Boolean status) {
        this.prodName = prodName;
        this.cateId = cateId;
        this.status = status;
    }
    public ProdFilter(){
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean isStatus() {
        return status;
    }

    public String[] getProdIds() {
        return prodIds;
    }

    public void setProdIds(String[] prodIds) {
        this.prodIds = prodIds;
    }

    public String getProductLevel() {
        return productLevel;
    }
    public void setProductLevel(String productLevel) {
        this.productLevel = productLevel;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
