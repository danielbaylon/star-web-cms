package com.enovax.star.cms.partnershared.repository.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerProductVM;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;

import java.util.List;

/**
 * Created by jennylynsze on 5/13/16.
 */
public interface IProductRepository {
    public int getProdSize(ProductGridFilterVM filterVM, boolean isExclusive);

    public List<PartnerProductVM> getProdByPage(ProductGridFilterVM filterVM, boolean isExclusive);

    public PartnerProductVM getProd(String channel, String prodId);
}
