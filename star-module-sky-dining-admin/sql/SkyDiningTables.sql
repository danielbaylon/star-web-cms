USE [STAR_DB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SkyDiningCart] (
  [id]              [int] IDENTITY (1, 1) NOT NULL,
  [guestName]       [nvarchar](300)       NULL,
  [contactNumber]   [nvarchar](100)       NULL,
  [emailAddress]    [nvarchar](200)       NULL,
  [date]            [datetime]            NOT NULL,
  [time]            [time](7)             NULL,
  [pax]             [int]                 NULL,
  [packageId]       [int]                 NOT NULL,
  [sessionId]       [int]                 NOT NULL,
  [themeId]         [int]                 NULL,
  [menuId]          [int]                 NOT NULL,
  [specialRequest]  [nvarchar](4000)      NULL,
  [type]            [int]                 NOT NULL,
  [status]          [int]                 NOT NULL,
  [webSessionId]    [nvarchar](100)       NOT NULL,
  [createdDate]     [datetime]            NOT NULL,
  [modifiedDate]    [datetime]            NOT NULL,
  [discountId]      [int]                 NULL,
  [jewelCard]       [nvarchar](100)       NULL,
  [jewelCardExpiry] [nvarchar](50)        NULL,
  [promoCode]       [nvarchar](100)       NULL,
  [merchantId] [nvarchar](100) NULL,
  [priceType]       [int]                 NULL,
  [discountPrice]   [int]                 NULL,
  [originalPrice]   [int]                 NULL,
  CONSTRAINT [PK_SkyDiningCart] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SkyDiningCartMainCourse] (
  [id]           [int] IDENTITY (1, 1) NOT NULL,
  [cartId]       [int]                 NOT NULL,
  [mainCourseId] [int]                 NOT NULL,
  [quantity]     [int]                 NOT NULL,
  CONSTRAINT [PK_SkyDiningCartMCMapping] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SkyDiningCartMainCourse]  WITH CHECK ADD CONSTRAINT [FK_SkyDiningCartMainCourse_SkyDiningCart] FOREIGN KEY ([cartId])
REFERENCES [dbo].[SkyDiningCart] ([id])
GO

ALTER TABLE [dbo].[SkyDiningCartMainCourse] CHECK CONSTRAINT [FK_SkyDiningCartMainCourse_SkyDiningCart]
GO

CREATE TABLE [dbo].[SkyDiningCartTopUp] (
  [id]       [int] IDENTITY (1, 1) NOT NULL,
  [cartId]   [int]                 NOT NULL,
  [topUpId]  [int]                 NOT NULL,
  [quantity] [int]                 NOT NULL,
  CONSTRAINT [PK_SkyDiningCartTUMapping] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SkyDiningCartTopUp]  WITH CHECK ADD CONSTRAINT [FK_SkyDiningCartTopUp_SkyDiningCart] FOREIGN KEY ([cartId])
REFERENCES [dbo].[SkyDiningCart] ([id])
GO

ALTER TABLE [dbo].[SkyDiningCartTopUp] CHECK CONSTRAINT [FK_SkyDiningCartTopUp_SkyDiningCart]
GO

CREATE TABLE [dbo].[SkyDiningReservation] (
  [id]              [int] IDENTITY (1, 1) NOT NULL,
  [guestName]       [nvarchar](300)       NOT NULL,
  [contactNumber]   [nvarchar](100)       NOT NULL,
  [emailAddress]    [nvarchar](200)       NOT NULL,
  [date]            [datetime]            NOT NULL,
  [time]            [time](7)             NOT NULL,
  [pax]             [int]                 NOT NULL,
  [packageId]       [int]                 NOT NULL,
  [sessionId]       [int]                 NOT NULL,
  [themeId]         [int]                 NULL,
  [menuId]          [int]                 NOT NULL,
  [specialRequest]  [nvarchar](4000)      NOT NULL,
  [type]            [int]                 NOT NULL,
  [status]          [int]                 NOT NULL,
  [createdBy]       [nvarchar](100)       NOT NULL,
  [createdDate]     [datetime]            NOT NULL,
  [modifiedBy]      [nvarchar](100)       NOT NULL,
  [modifiedDate]    [datetime]            NOT NULL,
  [discountId]      [int]                 NULL,
  [jewelCard]       [nvarchar](100)       NULL,
  [jewelCardExpiry] [nvarchar](50)        NULL,
  [promoCode]       [nvarchar](100)       NULL,
  [merchantId] [nvarchar](100) NULL,
  [receiptNum]      [nvarchar](100)       NULL,
  [receivedBy]      [nvarchar](100)       NULL,
  [paymentDate]     [datetime]            NULL,
  [paymentMode]     [int]                 NULL,
  [paidAmount]      [money]               NULL,
  [priceType]       [int]                 NULL,
  [discountPrice]   [int]                 NULL,
  [originalPrice]   [int]                 NULL,
  CONSTRAINT [PK_SkyDiningReservation] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SkyDiningResMainCourse] (
  [id]            [int] IDENTITY (1, 1) NOT NULL,
  [reservationId] [int]                 NOT NULL,
  [mainCourseId]  [int]                 NOT NULL,
  [quantity]      [int]                 NOT NULL,
  [createdBy]     [nvarchar](100)       NOT NULL,
  [createdDate]   [datetime]            NOT NULL,
  [modifiedBy]    [nvarchar](100)       NOT NULL,
  [modifiedDate]  [datetime]            NOT NULL,
  CONSTRAINT [PK_SkyDiningResMCMapping] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SkyDiningResMainCourse]  WITH CHECK ADD CONSTRAINT [FK_SkyDiningResMCMapping_SkyDiningReservation] FOREIGN KEY ([reservationId])
REFERENCES [dbo].[SkyDiningReservation] ([id])
GO

ALTER TABLE [dbo].[SkyDiningResMainCourse] CHECK CONSTRAINT [FK_SkyDiningResMCMapping_SkyDiningReservation]
GO

CREATE TABLE [dbo].[SkyDiningResTopUp] (
  [id]            [int] IDENTITY (1, 1) NOT NULL,
  [reservationId] [int]                 NOT NULL,
  [topUpId]       [int]                 NOT NULL,
  [quantity]      [int]                 NOT NULL,
  [createdBy]     [nvarchar](100)       NOT NULL,
  [createdDate]   [datetime]            NOT NULL,
  [modifiedBy]    [nvarchar](100)       NOT NULL,
  [modifiedDate]  [datetime]            NOT NULL,
  CONSTRAINT [PK_SkyDiningResTUMapping] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SkyDiningResTopUp]  WITH CHECK ADD CONSTRAINT [FK_SkyDiningResTUMapping_SkyDiningReservation] FOREIGN KEY ([reservationId])
REFERENCES [dbo].[SkyDiningReservation] ([id])
GO

ALTER TABLE [dbo].[SkyDiningResTopUp] CHECK CONSTRAINT [FK_SkyDiningResTUMapping_SkyDiningReservation]
GO

CREATE TABLE [dbo].[SkyDiningTransaction] (
  [id]               [int] IDENTITY (1, 1) NOT NULL,
  [receiptNum]       [nvarchar](50)        NOT NULL,
  [custName]         [nvarchar](500)       NOT NULL,
  [custSalutation]   [nvarchar](20)        NULL,
  [custCompany]      [nvarchar](500)       NULL,
  [custEmail]        [nvarchar](500)       NOT NULL,
  [custIdType]       [nvarchar](50)        NOT NULL,
  [custIdNo]         [nvarchar](100)       NOT NULL,
  [custMobile]       [nvarchar](100)       NULL,
  [custSubscribe]    [bit]                 NOT NULL,
  [custJewelCard]    [varchar](50)         NULL,
  [paymentType]      [varchar](50)         NOT NULL,
  [totalAmount]      [money]               NOT NULL,
  [currency]         [varchar](3)          NOT NULL,
  [bookFeeType]      [varchar](20)         NOT NULL,
  [bookFeeCents]     [int]                 NULL,
  [bookFeeItemCode]  [int]                 NULL,
  [bookFeeWaived]    [bit]                 NULL,
  [bookFeePromoCode] [varchar](50)         NULL,
  [tmMerchantId]     [varchar](50)         NOT NULL,
  [statusDate]       [datetime]            NULL,
  [errorCode]        [varchar](50)         NULL,
  [stage]            [varchar](50)         NOT NULL,
  [sactResId]        [varchar](50)         NULL,
  [sactPin]          [varchar](50)         NULL,
  [tmStatus]         [varchar](50)         NULL,
  [tmErrorMessage]   [nvarchar](1000)      NULL,
  [tmApprovalCode]   [varchar](50)         NULL,
  [reservationId]    [int]                 NULL,
  [createdBy]        [nvarchar](100)       NOT NULL,
  [createdDate]      [datetime]            NOT NULL,
  [modifiedBy]       [nvarchar](100)       NOT NULL,
  [modifiedDate]     [datetime]            NOT NULL,
  CONSTRAINT [PK_SkyDiningTransaction] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY],
  CONSTRAINT [IX_SkyDiningTransaction] UNIQUE NONCLUSTERED
    (
      [receiptNum] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SkyDiningSequenceLog] (
  [seqId]        [varchar](50) NOT NULL,
  [modifiedDate] [datetime]    NOT NULL,
  CONSTRAINT [PK_SkyDiningSequenceLog] PRIMARY KEY CLUSTERED
    (
      [seqId] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SkyDiningJewelCard] (
  [id]           [int] IDENTITY (1, 1) NOT NULL,
  [cardNumber]   [varchar](50)         NOT NULL,
  [expiryDate]   [datetime]            NOT NULL,
  [createdBy]    [nvarchar](100)       NOT NULL,
  [createdDate]  [datetime]            NOT NULL,
  [modifiedBy]   [nvarchar](100)       NOT NULL,
  [modifiedDate] [datetime]            NOT NULL,
  CONSTRAINT [PK_SkyDiningJewelCard] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO