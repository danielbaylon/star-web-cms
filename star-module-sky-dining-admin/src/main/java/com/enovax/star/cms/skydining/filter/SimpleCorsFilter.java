package com.enovax.star.cms.skydining.filter;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class SimpleCorsFilter extends OncePerRequestFilter {

  private static void setCorsHeaders(final HttpServletRequest request, final HttpServletResponse response) {
    String origin = request.getHeader("Origin");
    if (origin == null) {
      origin = "*";
    }
    response.addHeader("Access-Control-Allow-Origin", origin);
    response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
    response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X_Developer");
    response.addHeader("Access-Control-Allow-Credentials", "true");
    response.addHeader("Access-Control-Max-Age", "1728000");
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    //TODO Disable this for production environments as it is assumed that the requests will always originate on the same server.
    setCorsHeaders(request, response);
    if ("OPTIONS".equals(request.getMethod())) {
      return;
    }

    filterChain.doFilter(request, response);
  }
}

