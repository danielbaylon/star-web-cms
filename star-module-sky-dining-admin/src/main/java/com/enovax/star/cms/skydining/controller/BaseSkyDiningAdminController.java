package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.skydining.constant.ApiErrorCodes;
import com.enovax.star.cms.skydining.constant.ImageFileTypes;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.BaseSkyDiningActionParam;
import com.enovax.star.cms.skydining.util.JcrFileUtil;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.context.MgnlContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class BaseSkyDiningAdminController {
  public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";
  protected final Logger log = LoggerFactory.getLogger(getClass());

  protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
    final String logId = ProjectUtils.generateUniqueLogId();
    log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
    return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
      StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
  }

  protected <K> ApiResult<List<K>> handleUncaughtExceptionForList(Throwable t, Class<K> innerListClass, String extraMessage) {
    final String logId = ProjectUtils.generateUniqueLogId();
    log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
    return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
      StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
  }

  protected <K, V> ApiResult<Map<K, V>> handleUncaughtExceptionForMap(
    Throwable t, Class<K> innerMapKey, Class<V> innerMapValue, String extraMessage) {
    final String logId = ProjectUtils.generateUniqueLogId();
    log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
    return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
      StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
  }

  protected void processSaveResult(SaveResult result, ApiResult apiResult) {
    switch (result.getResult()) {
      case Failed:
        apiResult.setSuccess(false);
        if (StringUtils.isEmpty(result.getMessage())) {
          apiResult.setMessage("A system error has occurred. Please contact an administrator or try again.");
        } else {
          apiResult.setMessage(result.getMessage());
        }
        break;
      case StaleData:
        apiResult.setSuccess(false);
        apiResult.setMessage("The record you are trying to update has already been updated. Please refresh the page to view the latest data.");
        break;
      case ForeignKey:
        apiResult.setSuccess(false);
        apiResult.setMessage("The record you are trying to delete is being used by another record and cannot be deleted. Please consider deactivating the record instead.");
        break;
      default:
        apiResult.setSuccess(true);
        apiResult.setData(result);
        if (result.getTimestamp() != null) {
          result.setTimestampText(NvxDateUtils.formatModDt(result.getTimestamp()));
        } else {
          result.setTimestampText(null);
        }
        result.setImageHash(result.getImageHash());
        break;
    }
  }

  /**
   * Used to interface with Kendo UI Grid logic
   *
   * @param defaultSortCol
   * @param defaultAsc
   */
  protected void initGridOrdering(BaseSkyDiningActionParam param, String defaultSortCol, boolean defaultAsc) {
    if (param.getSort() == null || param.getSort().size() == 0) {
      log.info("No ordering specified. Going with default ordering.");
      param.setOrderBy(defaultSortCol);
      param.setAsc(defaultAsc);
    } else {
      log.info("Ordering specified.");
      param.setOrderBy(param.getSort().get(0).getField());
      if (param.getOrderBy().endsWith("Text")) {
        param.setOrderBy(param.getOrderBy().replace("Text", ""));
      }
      param.setAsc("asc".equals(param.getSort().get(0).getDir()) ? true : false);
    }
  }

  protected List<Integer> getIds(BaseSkyDiningActionParam param) {
    final List<Integer> ids = new ArrayList<Integer>();
    if (StringUtils.isNotBlank(param.getIdList())) {
      final String[] idStrings = param.getIdList().split(",");
      for (final String idString : idStrings) {
        if (StringUtils.isNumeric(idString)) {
          final int id = Integer.parseInt(idString);
          ids.add(id);
        }
      }
    }
    return ids;
  }

  protected String validateImage(Document image, int height, int width) throws IOException {
    if (StringUtils.isNotEmpty(image.getType())
      && !ImageFileTypes.isImage(image.getType())) {
      return "Uploaded file is not an image. Please upload a PNG, GIF, or JPG image.";
    }

    if ((image.getFile() != null)
      && !JcrFileUtil.checkImgDimensions(image.getFile(), height, width)) {
      return "Uploaded image has exceeded max image dimensions. Please choose a smaller image.";
    }
    return null;
  }

  protected final List<Integer> parseIntegerList(String listString) {
    final List<Integer> intList = new ArrayList<Integer>();
    if (StringUtils.isNotBlank(listString)) {
      final String[] intStrings = listString.split(",");
      for (final String intString : intStrings) {
        if (StringUtils.isNumeric(intString)) {
          final int intValue = Integer.parseInt(intString);
          intList.add(intValue);
        }
      }
    }
    return intList;
  }

  protected String getAdminUn() {
    return MgnlContext.getUser().getName();
  }
}
