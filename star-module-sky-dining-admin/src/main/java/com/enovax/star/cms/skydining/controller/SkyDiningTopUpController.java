package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.TopUpModel;
import com.enovax.star.cms.skydining.model.TopUpRecord;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SkyDiningTopUpActionParam;
import com.enovax.star.cms.skydining.service.ISkyDiningTopUpService;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * Created by jace on 30/7/16.
 */
@Controller
@RequestMapping("/top-up/")
public class SkyDiningTopUpController extends BaseSkyDiningAdminController {

  @Autowired
  protected ISkyDiningTopUpService topUpService;

  @RequestMapping(value = "view-top-up", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<TopUpModel>> viewTopUp(@ModelAttribute SkyDiningTopUpActionParam param) {
    try {
      TopUpModel topUpModel;
      if (param.getId() == null || param.getId() < 1) {
        topUpModel = new TopUpModel(new SkyDiningTopUp());
      } else {
        topUpModel = topUpService.getToUp(param.getId());
      }
      return new ResponseEntity<>(new ApiResult<>(true, "", "", topUpModel), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, TopUpModel.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-top-ups", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<TopUpRecord>>> getTopUpRecords(@ModelAttribute SkyDiningTopUpActionParam param) {
    try {
      initGridOrdering(param, "name", true);

      String name = null;
      if (StringUtils.isNotBlank(param.getNameFilter())) {
        name = param.getNameFilter();
      }

      Boolean active = null;
      if (param.getActiveFilter() != null) {
        if (param.getActiveFilter() == 0) {
          active = false;
        } else if (param.getActiveFilter() == 1) {
          active = true;
        }
      }

      List<TopUpRecord> topUpData = topUpService.getTopUpRecords(name, active, param.getSkip(), param.getTake(),
        param.getOrderBy(), param.getAsc());
      int total = topUpService.countTopUpRecords(name, active);

      ApiResult<List<TopUpRecord>> apiResult = new ApiResult<>();
      apiResult.setData(topUpData);
      apiResult.setTotal(total);
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, TopUpRecord.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-top-up", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveTopUp(@ModelAttribute SkyDiningTopUpActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = topUpService.saveTopUp(param.getId(), param.getNewName(),
        param.getNewType(), param.getNewDescription(), param.getNewDisplayOrder(),
        param.getNewEarlyBirdEndDate(), param.getNewStartDate(), param.getNewEndDate(),
        param.getNewSalesCutOffDays(), param.getNewPrice(), param.getNewImageHash(), param.getAdminUn(),
        param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-display-image", method = {RequestMethod.POST}, headers = "content-type=multipart/form-data")
  public ResponseEntity<ApiResult<SaveResult>> saveDisplayImage(HttpServletRequest request, @ModelAttribute SkyDiningTopUpActionParam param) {
    MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
    Document imageDocument = mpf.getDocument("image");

    try {
      String validationErrorMessage = validateImage(imageDocument, 277, 370);
      if (StringUtils.isBlank(validationErrorMessage)) {
        File imageFile = imageDocument.getFile();

        SaveResult result = topUpService.saveDisplayImage(param.getId(), imageFile, imageDocument.getFileNameWithExtension(), param.getAdminUn(), param.getTimestamp());
        ApiResult<SaveResult> apiResult = new ApiResult<>();
        processSaveResult(result, apiResult);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
      } else {
        ApiResult<SaveResult> apiResult = new ApiResult<>();
        apiResult.setSuccess(false);
        apiResult.setMessage(validationErrorMessage);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }
}
