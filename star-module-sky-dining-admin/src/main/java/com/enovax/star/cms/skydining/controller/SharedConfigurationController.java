package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningJewelCard;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SharedConfigurationActionParam;
import com.enovax.star.cms.skydining.service.ISharedConfigService;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 28/9/16.
 */
@Controller
@RequestMapping("/shared-config/")
public class SharedConfigurationController extends BaseSkyDiningAdminController {

    @Autowired
    protected ISharedConfigService configService;


    @RequestMapping(value = "get-jc-member-grid", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<SkyDiningJewelCard>>> getJCMemberGrid(@ModelAttribute SharedConfigurationActionParam param) {
        try {
            initGridOrdering(param, "cardNumber", true);
            List<SkyDiningJewelCard> data = configService.getJCMemberRecords(param.getSearchMemberCard(), param.getSearchJcFrom(),
                param.getSearchJcTo(), param.getSkip(), param.getTake(), param.getOrderBy(), param.getAsc());
            int total = configService.getJCMemberRecordCount(param.getSearchMemberCard(), param.getSearchJcFrom(),
                param.getSearchJcTo());

            ApiResult<List<SkyDiningJewelCard>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, SkyDiningJewelCard.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "delete-jc-filtered", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> deleteJcFiltered(@ModelAttribute SharedConfigurationActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();

            if (StringUtils.isEmpty(param.getSearchJcFrom()) && StringUtils.isEmpty(param.getSearchJcTo())) {
                apiResult.setSuccess(false);
                apiResult.setMessage("Please filter by expiry date first before trying to remove filtered records.");
            } else {
                configService.deleteJcFiltered(param.getSearchMemberCard(), param.getSearchJcFrom(), param.getSearchJcTo());
                apiResult.setSuccess(true);
            }

            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "upload-jc-member-excel", method = {RequestMethod.POST}, headers = "content-type=multipart/form-data")
    public ResponseEntity<ApiResult<List<SkyDiningJewelCard>>> uploadJCMemberExcel(HttpServletRequest request,
                                                                                   @ModelAttribute SharedConfigurationActionParam param) {
        try {
            MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
            Document excelDocument = mpf.getDocument("jcMemberUploader");


            InputStream iStream = new FileInputStream(excelDocument.getFile());
            List<SkyDiningJewelCard> jcs = null;
            if ("xls".equals(excelDocument.getExtension())) {
                jcs = configService.processCardXLS(iStream);
            } else if ("xlsx".equals(excelDocument.getExtension())) {
                jcs = configService.processCardXLSX(iStream);
            }

            if (jcs != null && jcs.size() > 0) {
                configService.updateJCMembers(jcs, param.getAdminUn());
            }

            ApiResult<List<SkyDiningJewelCard>> apiResult = new ApiResult<>();
            apiResult.setData(jcs);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, SkyDiningJewelCard.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-jc-member", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> updateJCMember(@ModelAttribute SharedConfigurationActionParam param) {
        try {
            SkyDiningJewelCard jc = new SkyDiningJewelCard();
            jc.setId(param.getJcId());
            jc.setCardNumber(param.getJcCardNumber());
            jc.setExpiryDate(NvxDateUtils.parseDate(param.getJcExpiry(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));

            ApiResult<SaveResult> apiResult = new ApiResult<>();
            if (configService.jcMemberExist(jc)) {
                apiResult.setSuccess(false);
                apiResult.setMessage("Membership no. " + jc.getCardNumber() + " has existed.");
            } else {
                configService.updateJCMember(jc, param.getAdminUn());
                apiResult.setSuccess(true);
            }

            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "delete-jc-member", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> deleteJCMember(@ModelAttribute SharedConfigurationActionParam param) {
        try {
            List<Integer> dIds = new ArrayList<Integer>();
            String[] splittedDeleteIds = param.getDeleteJcIds().split(",");

            for (String iString : splittedDeleteIds) {
                dIds.add(Integer.parseInt(iString));
            }

            configService.deleteJCMember(dIds);

            ApiResult<SaveResult> apiResult = new ApiResult<>();
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }


}
