package com.enovax.star.cms.skydining;

import com.enovax.star.cms.skydining.config.StarModuleSkyDiningAdminAppConfig;
import com.enovax.star.cms.skydining.config.StarModuleSkyDiningAdminWebConfig;
import com.enovax.star.cms.skydining.filter.SimpleCorsFilter;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * This class is optional and represents the configuration for the star-module-api-store module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/star-module-sky-dining-admin</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class StarModuleSkyDiningAdmin implements ModuleLifecycle {

  /**
   * URL mapping for the dispatcherServlet with a default value. To configure in AdminCentral,
   * go to: Configuration > modules > kando-event-reservation-module, and create a folder called "config"
   * with a property "urlMappings" and value "whatever".
   * <p>
   * Changing this property might require a server restart to take effect.
   */
  private String urlMappings = "/.sky-dining-admin/*";

  private ContextLoader contextLoader;
  private DispatcherServlet dispatcherServlet;

  @Override
  public void start(ModuleLifecycleContext moduleLifecycleContext) {
    if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
      //Retrieve serlvet context.
      ServletContext servletContext = getServletContext();

      //Initialize root context.

      //Initialize dispatcher servlet.

      AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
      webCtx.register(StarModuleSkyDiningAdminAppConfig.class, StarModuleSkyDiningAdminWebConfig.class);

      dispatcherServlet = new DispatcherServlet(webCtx);

      final ServletRegistration.Dynamic dispatcherRegistration = servletContext.addServlet("sky-dining-admin-dispatcher", dispatcherServlet);
      dispatcherRegistration.setLoadOnStartup(1);
      dispatcherRegistration.setInitParameter("dispatchOptionsRequest", "true");
      dispatcherRegistration.setAsyncSupported(true);

      final String[] urlMappingArray = urlMappings.split(",");
      dispatcherRegistration.addMapping(urlMappingArray);

      final FilterRegistration.Dynamic corsRegistration = servletContext.addFilter("skyDiningAdminCorsFilter", SimpleCorsFilter.class);
      corsRegistration.addMappingForServletNames(null, false, "sky-dining-admin-dispatcher");
      corsRegistration.setInitParameter("dispatchOptionsRequest", "true");
      corsRegistration.setAsyncSupported(true);
    }
  }

  @Override
  public void stop(ModuleLifecycleContext moduleLifecycleContext) {
    if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
      if (dispatcherServlet != null) {
        dispatcherServlet.destroy();
      }
      if (contextLoader != null) {
        contextLoader.closeWebApplicationContext(getServletContext());
      }
    }
  }

  protected ServletContext getServletContext() {
    return Components.getComponent(ServletContext.class);
  }

    /*
    Getters and Setters
     */

  public String getUrlMappings() {
    return urlMappings;
  }

  public void setUrlMappings(String urlMappings) {
    this.urlMappings = urlMappings;
  }
}

