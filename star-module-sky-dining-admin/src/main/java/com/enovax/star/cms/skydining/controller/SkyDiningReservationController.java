package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import com.enovax.star.cms.skydining.model.*;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SkyDiningReservationActionParam;
import com.enovax.star.cms.skydining.service.ISkyDiningReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.ParseException;
import java.util.List;

/**
 * Created by jace on 25/8/16.
 */
@Controller
@RequestMapping("/reservation/")
public class SkyDiningReservationController extends BaseSkyDiningAdminController {

    @Autowired
    protected ISkyDiningReservationService reservationService;

    @RequestMapping(value = "view-reservation", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ReservationModel>> viewReservation(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            ReservationModel model;
            if (param.getId() == null || param.getId() == 0) {
                model = new ReservationModel(new SkyDiningReservation());
            } else {
                model = reservationService.getReservation(param.getId());
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", model), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, ReservationModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-packages", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<PackageRecord>>> getSelectPackageOptions(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<PackageRecord> data = reservationService.getPackages(param.getDate());

            ApiResult<List<PackageRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, PackageRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-sessions", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<SessionRecord>>> getSelectSessionOptions(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<SessionRecord> data = reservationService.getSessions(param.getPackageId(), param.getDate(), param.getTime());

            ApiResult<List<SessionRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, SessionRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-themes", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ThemeModel>>> getSelectThemeOptions(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<ThemeModel> data = reservationService.getThemes(param.getPackageId());

            ApiResult<List<ThemeModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ThemeModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-menus", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MenuRecord>>> getSelectMenuOptions(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<MenuRecord> data = reservationService.getMenus(param.getPackageId());

            ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-edit-main-courses", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ResMainCourseModel>>> getEditMainCourses(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<ResMainCourseModel> data = reservationService.getResMainCourses(param.getParentId(), param.getMenuId(), true);

            ApiResult<List<ResMainCourseModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ResMainCourseModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-edit-top-ups", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ResTopUpModel>>> getEditTopUps(@ModelAttribute SkyDiningReservationActionParam param) {
        try {

            List<ResTopUpModel> data = reservationService.getResTopUps(param.getParentId(), param.getPackageId(), true);

            ApiResult<List<ResTopUpModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(data.size());
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ResTopUpModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-reservations", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ReservationRecord>>> getReservationRecords(@ModelAttribute SkyDiningReservationActionParam param) {
        try {
            initGridOrdering(param, "createdDate", false);

            final ReservationFilterCriteria criteria = new ReservationFilterCriteria();
            criteria.setGuestName(param.getGuestName());
            criteria.setTransactionStartDate(param.getTransactionStartDate());
            criteria.setTransactionEndDate(param.getTransactionEndDate());
            criteria.setTransactionStatus(param.getTransactionStatus());
            criteria.setReceiptNumber(param.getReceiptNumber());
            criteria.setPackageName(param.getPackageName());
            criteria.setDateOfVisit(param.getDateOfVisit());
            criteria.setReservationType(param.getReservationType());
            criteria.setGuestName(param.getGuestName());
            criteria.setReservationStatus(param.getReservationStatus());
            criteria.setStart(param.getSkip());
            criteria.setPageSize(param.getTake());
            criteria.setOrderBy(param.getOrderBy());
            criteria.setAsc(param.getAsc());

            List<ReservationRecord> data = reservationService.getReservationRecords(criteria);
            int total = reservationService.countReservationRecords(criteria);

            ApiResult<List<ReservationRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ReservationRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "resend-receipt", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> resendReceipt(@ModelAttribute SkyDiningReservationActionParam param) {
        try {
            reservationService.sendReceiptEmail(param.getReceiptNumber());

            ApiResult<String> apiResult = new ApiResult<>();
            apiResult.setSuccess(true);
            apiResult.setMessage("OK");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> saveReservation(@ModelAttribute SkyDiningReservationActionParam param) {
        try {


            final SaveResult result = reservationService.saveReservation(
                getReservationModel(param), getAdminUn(), param.getTimestamp());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    protected ReservationModel getReservationModel(SkyDiningReservationActionParam param) {
        final ReservationModel model = new ReservationModel();

        model.setId(param.getId());

        if (param.getNewGuestName() != null) {
            model.setGuestName(param.getNewGuestName());
        }
        if (param.getNewContactNumber() != null) {
            model.setContactNumber(param.getNewContactNumber());
        }
        if (param.getNewEmailAddress() != null) {
            model.setEmailAddress(param.getNewEmailAddress());
        }
        if (param.getNewDate() != null) {
            try {
                model.setDate(NvxDateUtils.parseDate(param.getNewDate(),
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Error parsing date: " + param.getNewDate());
                model.setDate(null);
            }
        }
        if (param.getNewTime() != null) {
            try {
                model.setTime(NvxDateUtils.parseTime(param.getNewTime(),
                    SysConst.DEFAULT_TIME_FORMAT));
            } catch (final ParseException e) {
                log.error("Error parsing time: " + param.getNewTime());
                model.setTime(null);
            }
        }
        if (param.getNewPax() != null) {
            model.setPax(param.getNewPax());
        }
        if (param.getNewPackageId() != null) {
            model.setPackageId(param.getNewPackageId());
        }
        if (param.getNewSessionId() != null) {
            model.setSessionId(param.getNewSessionId());
        }
        if (param.getNewThemeId() != null) {
            model.setThemeId(param.getNewThemeId());
        }
        if (param.getNewMenuId() != null) {
            model.setMenuId(param.getNewMenuId());
        }
        if (param.getNewSpecialRequest() != null) {
            model.setSpecialRequest(param.getNewSpecialRequest());
        }
        if (param.getNewType() != null) {
            model.setType(param.getNewType());
        }
        if (param.getNewStatus() != null) {
            model.setStatus(param.getNewStatus());
        }
        if (param.getNewDiscountId() != null) {
            model.setDiscountId(param.getNewDiscountId());
        }
        if (param.getNewJewelCard() != null) {
            model.setJewelCard(param.getNewJewelCard());
        }
        if (param.getNewJewelCardExpiry() != null) {
            model.setJewelCardExpiry(param.getNewJewelCardExpiry());
        }
        if (param.getNewPromoCode() != null) {
            model.setPromoCode(param.getNewPromoCode());
        }
        if (param.getNewMerchantId() != null) {
            model.setMerchantId(param.getNewMerchantId());
        }
        if (param.getNewReceiptNum() != null) {
            model.setReceiptNum(param.getNewReceiptNum());
        }
        if (param.getNewReceivedBy() != null) {
            model.setReceivedBy(param.getNewReceivedBy());
        }
        if (param.getNewPaymentDate() != null) {
            try {
                model.setPaymentDate(NvxDateUtils.parseDate(param.getNewPaymentDate(),
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Error parsing date: " + param.getNewPaymentDate());
                model.setPaymentDate(null);
            }
        }
        if (param.getNewPaymentMode() != null) {
            model.setPaymentMode(param.getNewPaymentMode());
        }
        if (param.getNewPaidAmount() != null) {
            model.setPaidAmount(param.getNewPaidAmount());
        }

        if (param.getNewMainCourseIds() != null) {
            model.setMainCourseIds(parseIntegerList(param.getNewMainCourseIds()));
        }
        if (param.getNewMainCourseQuantities() != null) {
            model.setMainCourseQuantities(parseIntegerList(param.getNewMainCourseQuantities()));
        }

        if (param.getNewTopUpIds() != null) {
            model.setTopUpIds(parseIntegerList(param.getNewTopUpIds()));
        }
        if (param.getNewTopUpQuantities() != null) {
            model.setTopUpQuantities(parseIntegerList(param.getNewTopUpQuantities()));
        }

        return model;
    }

}
