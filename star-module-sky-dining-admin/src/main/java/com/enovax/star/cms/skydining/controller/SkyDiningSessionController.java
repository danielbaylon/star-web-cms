package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;
import com.enovax.star.cms.skydining.model.*;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SkyDiningSessionActionParam;
import com.enovax.star.cms.skydining.service.ISkyDiningSessionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
@Controller
@RequestMapping("/schedule/")
public class SkyDiningSessionController extends BaseSkyDiningAdminController {

  @Autowired
  protected ISkyDiningSessionService sessionService;

  @RequestMapping(value = "view-session", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<SessionModel>> viewSession(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      final SessionModel session;
      if (param.getId() == null || param.getId() < 1) {
        session = new SessionModel(new SkyDiningSession());
      } else {
        session = sessionService.getSession(param.getId());
      }
      return new ResponseEntity<>(new ApiResult<>(true, "", "", session), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SessionModel.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-session", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveSession(@ModelAttribute SkyDiningSessionActionParam param) {
    log.info("[ACTION-AJAX] Processing saveSession.");
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService
        .saveSession(param.getId(), param.getNewName(), param.getNewSessionNumber(), param.getNewSessionType(),
          param.getNewEffectiveDays(), param.getNewStartDate(), param.getNewEndDate(),
          param.getNewTime(), param.getNewSalesStartDate(), param.getNewSalesEndDate(),
          param.getNewCapacity(), param.getNewActive(), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-schedule", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<SessionRecord>>> getScheduleRecords(@ModelAttribute SkyDiningSessionActionParam param) {
    log.info("[ACTION-AJAX] Processing getScheduleRecords.");
    try {
      initGridOrdering(param, "name", true);
      if ("schedule".equals(param.getOrderBy())) {
        param.setOrderBy("SessionType");
      } else if ("status".equals(param.getOrderBy())) {
        param.setOrderBy("Active");
      }
      ApiResult<List<SessionRecord>> apiResult = new ApiResult<>();
      List<SessionRecord> scheduleData = sessionService.getSessionRecords(param.getSkip(), param.getTake(),
        param.getOrderBy(), param.getAsc());
      apiResult.setData(scheduleData);
      apiResult.setTotal(sessionService.countScheduleRecords());
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, SessionRecord.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "remove-sessions", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> removeSessions(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService.removeSessions(getIds(param));
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "toggle-status", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> toggleStatus(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService.toggleStatus(param.getId(), param.getAdminUn(),
        param.getTimestamp());

      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-blockout", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveBlockOutDate(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService.saveBlockOutDate(
        param.getParentId(), param.getId(), param.getNewStartDate(), param.getNewEndDate(), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-blockouts", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<BlockOutDateModel>>> getBlockOutDates(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<List<BlockOutDateModel>> apiResult = new ApiResult<>();
      List<BlockOutDateModel> blockOutDateData = sessionService.getBlockOutDates(param.getParentId());
      apiResult.setData(blockOutDateData);
      apiResult.setTotal(blockOutDateData.size());
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, BlockOutDateModel.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "remove-blockouts", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> removeBlockOutDates(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService.removeBlockOutDates(
        param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "add-packages", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> addPackages(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService.addPackagesToSession(
        param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-packages", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<PackageRecord>>> getPackageRecords(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<List<PackageRecord>> apiResult = new ApiResult<>();
      List<PackageRecord> packageData = sessionService.getPackageRecords(param.getParentId());
      apiResult.setData(packageData);
      apiResult.setTotal(packageData.size());
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, PackageRecord.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-select-packages", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<PackageRecord>>> getSelectPackageRecords(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<List<PackageRecord>> apiResult = new ApiResult<>();
      initGridOrdering(param, "name", true);

      String name = null;
      if (StringUtils.isNotBlank(param.getNameFilter())) {
        name = param.getNameFilter();
      }

      Boolean active = null;
      if (param.getActiveFilter() != null) {
        if (param.getActiveFilter() == 0) {
          active = false;
        } else if (param.getActiveFilter() == 1) {
          active = true;
        }
      }

      List<PackageRecord> packageData = sessionService.getSelectPackageRecords(param.getParentId(),
        name, active, param.getSkip(), param.getTake(), param.getOrderBy(), param.getAsc());
      int total = sessionService.countSelectPackageRecords(param.getId(), name,
        active);

      apiResult.setData(packageData);
      apiResult.setTotal(total);
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, PackageRecord.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "remove-packages", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> removePackages(@ModelAttribute SkyDiningSessionActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = sessionService
        .removePackagesFromSession(param.getParentId(), getIds(param), param.getAdminUn(),
          param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

}
