package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.service.ISkyDiningReportService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * Created by jace on 10/11/16.
 */
@Controller
@RequestMapping("/report/")
public class SkyDiningReportController extends BaseSkyDiningAdminController {

    @Autowired
    private ISkyDiningReportService reportService;


    @RequestMapping(value = "daily-booking", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> doDailyBookingExcel(@RequestParam(name = "startDate", required = false) String startDate,
                                                                 @RequestParam(name = "endDate", required = false) String endDate,
                                                                 HttpServletResponse response) {
        try {
            Workbook wb = reportService.generateDailyBookingExcel(startDate, endDate);

            final String fileName = "DailyBookingReport.xlsx";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "daily-order", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> doDailyOrderExcel(@RequestParam(name = "startDate", required = false) String startDate,
                                                               @RequestParam(name = "endDate", required = false) String endDate,
                                                               HttpServletResponse response) {
        try {
            Workbook wb = reportService.generateDailyOrderExcel(startDate, endDate);

            final String fileName = "DailyOrderReport.xlsx";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "revenue", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> doRevenuegeExcel(@RequestParam(name = "startDate", required = false) String startDate,
                                                              @RequestParam(name = "endDate", required = false) String endDate,
                                                              HttpServletResponse response) {
        try {
            Workbook wb = reportService.generateRevenueExcel(startDate, endDate);

            final String fileName = "RevenueReport.xlsx";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


}
