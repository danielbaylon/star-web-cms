package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;
import com.enovax.star.cms.skydining.model.*;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SkyDiningPackageActionParam;
import com.enovax.star.cms.skydining.model.params.SkyDiningTopUpActionParam;
import com.enovax.star.cms.skydining.service.ISkyDiningPackageService;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * Created by jace on 30/7/16.
 */
@Controller
@RequestMapping("/package/")
public class SkyDiningPackageController extends BaseSkyDiningAdminController {

    @Autowired
    protected ISkyDiningPackageService packageService;

    @RequestMapping(value = "view-package", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<PackageModel>> viewPackage(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            final PackageModel packageModel;
            if (param.getId() == null || param.getId() < 1) {
                packageModel = new PackageModel(new SkyDiningPackage());
            } else {
                packageModel = packageService.getPackage(param.getId());
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", "", packageModel), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, PackageModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-packages", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<PackageRecord>>> getPackageRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            initGridOrdering(param, "name", true);

            String name = null;
            if (StringUtils.isNotBlank(param.getNameFilter())) {
                name = param.getNameFilter();
            }

            Boolean active = null;
            if (param.getActiveFilter() != null) {
                if (param.getActiveFilter() == 0) {
                    active = false;
                } else if (param.getActiveFilter() == 1) {
                    active = true;
                }
            }

            List<PackageRecord> data = packageService.getPackageRecords(name, active, param.getSkip(),
                param.getTake(), param.getOrderBy(), param.getAsc());
            int total = packageService.countPackageRecords(name, active);

            ApiResult<List<PackageRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, PackageRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-package", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> savePackage(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.savePackage(param.getId(), param.getNewName(),
                param.getNewEarlyBirdEndDate(), param.getNewStartDate(), param.getNewEndDate(),
                param.getNewSalesCutOffDays(), param.getNewCapacity(), param.getNewDescription(), param.getNewLink(),
                param.getNewDisplayOrder(), param.getNewMinPax(), param.getNewMaxPax(), param.getNewImageHash(),
                param.getNewNotes(), param.getNewSelectedTncId(), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-notes-tnc", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> saveNotesAndTnc(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.savePackage(param.getId(), null,
                null, null, null, null, null, null, null, null, null, null,
                null, param.getNewNotes(), param.getNewSelectedTncId(), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-package-info", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> savePackageInfo(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.savePackage(param.getId(), param.getNewName(),
                param.getNewEarlyBirdEndDate(), param.getNewStartDate(), param.getNewEndDate(),
                param.getNewSalesCutOffDays(), param.getNewCapacity(), null, null, null, null,
                null, null, null, null, param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-display-info", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> saveDisplayInfo(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.savePackage(param.getId(), param.getNewName(),
                null, null, null, null, null, param.getNewDescription(), param.getNewLink(),
                param.getNewDisplayOrder(), param.getNewMinPax(), param.getNewMaxPax(), null, null, null,
                param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-display-image", method = {RequestMethod.POST}, headers = "content-type=multipart/form-data")
    public ResponseEntity<ApiResult<SaveResult>> saveDisplayImage(HttpServletRequest request, @ModelAttribute SkyDiningTopUpActionParam param) {
        MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
        Document imageDocument = mpf.getDocument("image");

        try {
            String validationErrorMessage = validateImage(imageDocument, 277, 370);
            if (StringUtils.isBlank(validationErrorMessage)) {
                File imageFile = imageDocument.getFile();

                SaveResult result = packageService.saveDisplayImage(param.getId(), imageFile, imageDocument.getFileNameWithExtension(), param.getAdminUn(), param.getTimestamp());
                ApiResult<SaveResult> apiResult = new ApiResult<>();
                processSaveResult(result, apiResult);
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            } else {
                ApiResult<SaveResult> apiResult = new ApiResult<>();
                apiResult.setSuccess(false);
                apiResult.setMessage(validationErrorMessage);
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-tnc", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> saveTnc(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.saveTnc(param.getParentId(), param.getId(),
                param.getNewTitle(), param.getNewTncContent(), param.getNewSelected(), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-tncs", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<TncModel>>> getTncs(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<TncModel> data = packageService.getTncs(param.getParentId());
            int total = data.size();

            ApiResult<List<TncModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, TncModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-tncs", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeTncs(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeTncs(param.getParentId(),
                getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-theme", method = {RequestMethod.POST}, headers = "content-type=multipart/form-data")
    public ResponseEntity<ApiResult<SaveResult>> saveTheme(HttpServletRequest request, @ModelAttribute SkyDiningPackageActionParam param) {
        MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
        Document imageDocument = mpf.getDocument("image");


        try {
            String validationErrorMessage = validateImage(imageDocument, 277, 370);
            if (StringUtils.isBlank(validationErrorMessage)) {
                File imageFile = imageDocument.getFile();

                final SaveResult result = packageService.saveTheme(param.getParentId(), param.getId(),
                    imageFile, imageDocument.getFileNameWithExtension(), param.getNewDescription(), param.getNewCapacity(), param.getAdminUn(),
                    param.getTimestamp());
                ApiResult<SaveResult> apiResult = new ApiResult<>();
                processSaveResult(result, apiResult);
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            } else {
                ApiResult<SaveResult> apiResult = new ApiResult<>();
                apiResult.setSuccess(false);
                apiResult.setMessage(validationErrorMessage);
                return new ResponseEntity<>(apiResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-themes", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ThemeModel>>> getThemes(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<ThemeModel> data = packageService.getThemes(param.getParentId());
            int total = data.size();

            ApiResult<List<ThemeModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ThemeModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-themes", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeThemes(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeThemes(param.getParentId(),
                getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-menus", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> addMenus(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.addMenusToPackage(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-menus", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MenuRecord>>> getMenuRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<MenuRecord> data = packageService.getMenuRecords(param.getParentId());
            int total = data.size();

            ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-menus", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MenuRecord>>> getSelectMenusRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            initGridOrdering(param, "name", true);

            String name = null;
            if (StringUtils.isNotBlank(param.getNameFilter())) {
                name = param.getNameFilter();
            }

            Boolean active = null;
            if (param.getActiveFilter() != null) {
                if (param.getActiveFilter() == 0) {
                    active = false;
                } else if (param.getActiveFilter() == 1) {
                    active = true;
                }
            }

            List<MenuRecord> data = packageService.getSelectMenuRecords(param.getParentId(), name,
                active, parseIntegerList(param.getMenuTypeFilter()), param.getSkip(), param.getTake(),
                param.getOrderBy(), param.getAsc());
            int total = packageService.countSelectMenuRecords(param.getParentId(), name,
                active, parseIntegerList(param.getMenuTypeFilter()));

            ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-menus", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeMenus(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeMenusFromPackage(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-topups", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> addTopUps(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.addTopUpsToPackage(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-topups", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<TopUpRecord>>> getTopUpRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<TopUpRecord> data = packageService.getTopUpRecords(param.getParentId());
            int total = data.size();

            ApiResult<List<TopUpRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, TopUpRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-topups", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<TopUpRecord>>> getSelectToUpRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            initGridOrdering(param, "name", true);

            String name = null;
            if (StringUtils.isNotBlank(param.getNameFilter())) {
                name = param.getNameFilter();
            }

            Boolean active = null;
            if (param.getActiveFilter() != null) {
                if (param.getActiveFilter() == 0) {
                    active = false;
                } else if (param.getActiveFilter() == 1) {
                    active = true;
                }
            }

            List<TopUpRecord> data = packageService.getSelectTopUpRecords(param.getParentId(), name,
                active, param.getSkip(), param.getTake(), param.getOrderBy(), param.getAsc());
            int total = packageService.countSelectTopUpRecords(param.getParentId(), name,
                active);

            ApiResult<List<TopUpRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, TopUpRecord.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "remove-topups", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeTopUps(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeTopUpsFromPackage(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "save-promo", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> saveDiscount(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.saveDiscount(param.getParentId(),
                param.getId(), param.getNewName(), param.getNewActive(), param.getNewType(), param.getNewMerchantId(), param.getAdminUn(),
                param.getTimestamp(), param.getNewPriceType(), param.getNewDiscountPrice(), param.getNewValidFrom(),
                param.getNewValidTo(), param.getNewTnc());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-promos", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<DiscountModel>>> getDiscounts(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<DiscountModel> data = packageService.getDiscounts(param.getParentId());
            int total = data.size();

            ApiResult<List<DiscountModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, DiscountModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-promos", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeDiscounts(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeDiscounts(param.getParentId(),
                getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-promo-menus", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> addDiscountMenus(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.addMenusToDiscount(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-promo-menus", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MenuRecord>>> getDiscountMenuRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<MenuRecord> data = packageService.getDiscountMenuRecords(param.getParentId());
            int total = data.size();

            ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-select-promo-menus", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MenuRecord>>> getSelectDiscoutnMenuRecords(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            initGridOrdering(param, "name", true);

            String name = null;
            if (StringUtils.isNotBlank(param.getNameFilter())) {
                name = param.getNameFilter();
            }

            Boolean active = null;
            if (param.getActiveFilter() != null) {
                if (param.getActiveFilter() == 0) {
                    active = false;
                } else if (param.getActiveFilter() == 1) {
                    active = true;
                }
            }

            List<MenuRecord> data = packageService.getSelectDiscountMenuRecords(param.getParentId(),
                name, active, parseIntegerList(param.getMenuTypeFilter()), param.getSkip(), param.getTake(),
                param.getOrderBy(), param.getAsc());
            int total = packageService.countSelectDiscountMenuRecords(param.getParentId(),
                name, active, parseIntegerList(param.getMenuTypeFilter()));

            ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-promo-menus", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removeDiscountMenus(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removeMenusFromDiscount(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-promo-code", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> savePromoCode(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.savePromoCode(param.getParentId(),
                param.getId(), param.getNewCode(), param.getNewHasLimit(), param.getNewLimit(), param.getNewTimesUsed(),
                param.getNewActive(), param.getNewType(), param.getNewStartDate(), param.getNewEndDate(), param.getAdminUn(),
                param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-promo-codes", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<PromoCodeModel>>> getPromoCodes(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<PromoCodeModel> data = packageService.getPromoCodes(param.getParentId());
            int total = data.size();

            ApiResult<List<PromoCodeModel>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, PromoCodeModel.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-promo-codes", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SaveResult>> removePromoCodes(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            ApiResult<SaveResult> apiResult = new ApiResult<>();
            final SaveResult result = packageService.removePromoCodes(
                param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
            processSaveResult(result, apiResult);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-merchants", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<MerchRow>>> getMerchants(@ModelAttribute SkyDiningPackageActionParam param) {
        try {
            List<MerchRow> data = packageService.doGetMerchants(true);
            int total = data.size();

            ApiResult<List<MerchRow>> apiResult = new ApiResult<>();
            apiResult.setData(data);
            apiResult.setTotal(total);
            apiResult.setSuccess(true);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, MerchRow.class, ""), HttpStatus.OK);
        }
    }
}
