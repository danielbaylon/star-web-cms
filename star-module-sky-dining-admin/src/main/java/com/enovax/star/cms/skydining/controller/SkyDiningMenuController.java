package com.enovax.star.cms.skydining.controller;

import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;
import com.enovax.star.cms.skydining.model.MainCourseModel;
import com.enovax.star.cms.skydining.model.MenuModel;
import com.enovax.star.cms.skydining.model.MenuRecord;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.api.ApiResult;
import com.enovax.star.cms.skydining.model.params.SkyDiningMenuActionParam;
import com.enovax.star.cms.skydining.service.ISkyDiningMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by jace on 30/7/16.
 */
@Controller
@RequestMapping("/menu/")
public class SkyDiningMenuController extends BaseSkyDiningAdminController {

  @Autowired
  protected ISkyDiningMenuService menuService;

  @RequestMapping(value = "view-menu", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<MenuModel>> viewMenu(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      final MenuModel menuModel;
      if (param.getId() == null || param.getId() < 1) {
        menuModel = new MenuModel(new SkyDiningMenu());
      } else {
        menuModel = menuService.getMenu(param.getId());
      }
      return new ResponseEntity<>(new ApiResult<>(true, "", "", menuModel), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, MenuModel.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-menus", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<MenuRecord>>> getMenuRecords(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      initGridOrdering(param, "name", true);

      String name = null;
      if (StringUtils.isNotBlank(param.getNameFilter())) {
        name = param.getNameFilter();
      }

      Boolean active = null;
      if (param.getActiveFilter() != null) {
        if (param.getActiveFilter() == 0) {
          active = false;
        } else if (param.getActiveFilter() == 1) {
          active = true;
        }
      }

      List<MenuRecord> menuData = menuService.getMenuRecords(name, active, param.getSkip(), param.getTake(),
        param.getOrderBy(), param.getAsc());
      int total = menuService.countMenuRecords(name, active);

      ApiResult<List<MenuRecord>> apiResult = new ApiResult<>();
      apiResult.setData(menuData);
      apiResult.setTotal(total);
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, MenuRecord.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-menu", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveMenu(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.saveMenu(param.getId(), param.getNewName(),
        param.getNewDescription(), param.getNewEarlyBirdEndDate(), param.getNewStartDate(),
        param.getNewEndDate(), param.getNewSalesCutOffDays(), param.getNewMenuType(), param.getNewPrice(),
        param.getNewServiceCharge(), param.getNewGst(), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "save-menu-info", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveMenuInfo(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.saveMenu(param.getId(), param.getNewName(), null,
        param.getNewEarlyBirdEndDate(), param.getNewStartDate(), param.getNewEndDate(),
        param.getNewSalesCutOffDays(), param.getNewMenuType(), param.getNewPrice(),
        param.getNewServiceCharge(), param.getNewGst(), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "save-display-info", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveDisplayInfo(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.saveMenu(param.getId(), param.getNewName(),
        param.getNewDescription(), null, null, null, null, null, null, null,
        null, param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "add-main-courses", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> addMainCourses(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.addMainCoursesToMenu(
        param.getParentId(), getIds(param), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "save-main-course", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> saveMainCourse(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.saveMainCourse(param.getParentId(), param.getId(),
        param.getNewCode(), param.getNewType(), param.getNewDescription(), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

  @RequestMapping(value = "get-main-courses", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<MainCourseModel>>> getMainCourses(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      List<MainCourseModel> data = menuService.getMainCourses(param.getParentId());
      int total = data.size();

      ApiResult<List<MainCourseModel>> apiResult = new ApiResult<>();
      apiResult.setData(data);
      apiResult.setTotal(total);
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, MainCourseModel.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "get-select-main-courses", method = {RequestMethod.GET})
  public ResponseEntity<ApiResult<List<MainCourseModel>>> getSelectMainCourses(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      initGridOrdering(param, "description", true);

      List<MainCourseModel> data = menuService.getSelectMainCourseRecords(param.getParentId(),
        param.getSkip(), param.getTake(), param.getOrderBy(), param.getAsc());
      int total = menuService.countSelectMainCourseRecords(param.getParentId());

      ApiResult<List<MainCourseModel>> apiResult = new ApiResult<>();
      apiResult.setData(data);
      apiResult.setTotal(total);
      apiResult.setSuccess(true);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtExceptionForList(e, MainCourseModel.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "remove-main-courses", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> removeMainCourses(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.removeMainCourses(param.getParentId(),
        getIds(param), param.getAdminUn(), param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }


  @RequestMapping(value = "delete-main-course", method = {RequestMethod.POST})
  public ResponseEntity<ApiResult<SaveResult>> deleteMainCourse(@ModelAttribute SkyDiningMenuActionParam param) {
    try {
      ApiResult<SaveResult> apiResult = new ApiResult<>();
      final SaveResult result = menuService.deleteMainCourse(param.getId(), param.getAdminUn(),
        param.getTimestamp());
      processSaveResult(result, apiResult);
      return new ResponseEntity<>(apiResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(handleUncaughtException(e, SaveResult.class, ""), HttpStatus.OK);
    }
  }

//  @RequestMapping(value = "", method = {RequestMethod.GET})
//    public ResponseEntity<ApiResult<>> (@ModelAttribute SkyDiningMenuActionParam param) {
//    try {
//
//      return new ResponseEntity<>(new ApiResult<>(true, "", "", ), HttpStatus.OK);
//    } catch (Exception e) {
//      return new ResponseEntity<>(handleUncaughtException(e, .class, ""), HttpStatus.OK);
//    }
//  }
}
