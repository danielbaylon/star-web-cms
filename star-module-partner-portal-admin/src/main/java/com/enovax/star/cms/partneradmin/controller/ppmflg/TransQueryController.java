package com.enovax.star.cms.partneradmin.controller.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus;
import com.enovax.star.cms.commons.constant.ppmflg.TicketMediaType;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 7/11/16.
 */
@Controller("PPMFLGTransQueryController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/trans-query/")
public class TransQueryController extends BasePartnerAdminController {

    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService paSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerProductService")
    private IPartnerProductService paProdSrv;

    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeSharedService")
    private IWingsOfTimeSharedService paWoTSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerAdminProductService")
    private IPartnerAdminProductService paProdAdminSrv;

    @Autowired
    @Qualifier("PPMFLGITransQueryService")
    private ITransQueryService transQuerySrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerPkgService")
    private IPartnerPkgService paPkgSrv;

    private static final String ERROR_LOGIN_MSG = "Invalid request, Access denied";

    @RequestMapping(value = "is-correct-login-user", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> apiIsCorrectLoginUser(HttpServletRequest request) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<Boolean>(false, "ERROR", ERROR_LOGIN_MSG, false), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ApiResult<Boolean>(true, "", "", true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetAllPartners", e, Boolean.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-partner-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult> apiGetAllPartners(HttpServletRequest request) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            List<PartnerVM> partners = paSrv.getAllPartnersByStatus(PartnerStatus.Active.code);
            return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(true, "", "", partners), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetAllPartners", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-ticket-media-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult> apiGetTicketMediaList(HttpServletRequest request) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            List<TicketMediaTypeVM> list = new ArrayList<TicketMediaTypeVM>();
            TicketMediaType[] tas = TicketMediaType.values();
            if(tas != null){
                for(TicketMediaType t : tas){
                    TicketMediaTypeVM vm = new TicketMediaTypeVM();
                    vm.setName(t.name());
                    vm.setDescription(t.label);
                    list.add(vm);
                }
            }
            return new ResponseEntity<>(new ApiResult<List<TicketMediaTypeVM>>(true, "", "", list), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetAllPartners", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-txn-query-results", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult> apiViewTxnQueryResults(HttpServletRequest request,
                                                            @RequestParam(value="startDateStr", required = false) String startDateStr,
                                                            @RequestParam(value="endDateStr", required = false) String endDateStr,
                                                            @RequestParam(value="paymentType", required = false) String paymentType,
                                                            @RequestParam(value="receiptNum", required = false) String receiptNum,
                                                            @RequestParam(value="orgName", required = false) String orgName,
                                                            @RequestParam(value="status", required = false) String status,
                                                            @RequestParam(value="accountCode", required = false) String accountCode,
                                                            @RequestParam(value="partnerIds", required = false) String partnerIds,
                                                            @RequestParam("take") int take,
                                                            @RequestParam("skip") int skip,
                                                            @RequestParam("page") int page,
                                                            @RequestParam("pageSize") int pageSize,
                                                            @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }

            TransQueryFilterVM ft = new TransQueryFilterVM();
            ft.setFromDtStr(startDateStr);
            ft.setToDtStr(endDateStr);
            ft.setReceiptNum(receiptNum);
            ft.setOrgName(orgName);
            ft.setMixedStatus(status);
            ft.setAccountCode(accountCode);
            ft.setPaymentType(paymentType);
            Date startDate = null;
            Date toDate = null;
            if(startDateStr != null && startDateStr.trim().length() > 0){
                startDate = NvxDateUtils.parseDate(startDateStr.trim(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if(endDateStr != null && endDateStr.trim().length() > 0){
                toDate = NvxDateUtils.parseDate(endDateStr.trim(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            List<Integer> partnerIdList = null;
            if(partnerIds != null && partnerIds.trim().length() > 0){
                partnerIdList = new ArrayList<Integer>();
                String[] tempIds = partnerIds.split(",");
                if(tempIds != null && tempIds.length > 0){
                    for(String tempId : tempIds){
                        if(tempId != null && tempId.trim().length() > 0){
                            partnerIdList.add(Integer.parseInt(tempId));
                        }
                    }
                }
                ft.setPaIds(partnerIdList.toArray(new Integer[partnerIdList.size()]));
            }
            ft.setFromDate(startDate);
            ft.setToDate(toDate);
            ApiResult<List<TransQueryVM>> rvm = transQuerySrv.getTransQueryResults(ft, take, skip, page, pageSize, sortField, sortDirection);
            return new ResponseEntity<>(rvm, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewTxnQueryResults", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-txn-details-results", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult<TransDetailVM>> apiViewTxnQueryResults(HttpServletRequest request,
                                                            @RequestParam(value="id", required = false) String id,
                                                            @RequestParam(value="transType", required = false) String transType) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<TransDetailVM>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            ApiResult<TransDetailVM> rvm = transQuerySrv.getTransDetails(id, transType);
            return new ResponseEntity<>(rvm, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewTxnQueryResults", e, TransDetailVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "resend-txn-receipt", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiResendingTransReceipt(HttpServletRequest request,
                                                                           @RequestParam(value="id", required = false) String id,
                                                                           @RequestParam(value="transType", required = false) String transType) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<String>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            String msg = transQuerySrv.resendingTransReceipt(id, transType);
            return new ResponseEntity<>(new ApiResult<>(true, "", msg, msg), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiResendingTransReceipt", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-package-item-list", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult> apiViewPinCodeQueryResults(HttpServletRequest request, @RequestParam(value="packageId", required = false) Integer packageId){
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            ApiResult<List<PackageItemViewResultVM>> rvm = transQuerySrv.getPackageItemListByPackageId(packageId);
            return new ResponseEntity<>(rvm, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewTxnQueryResults", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-pincode-query-results", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult> apiViewPinCodeQueryResults(HttpServletRequest request,
                                                            @RequestParam(value="startDateStr", required = false) String startDateStr,
                                                            @RequestParam(value="endDateStr", required = false) String endDateStr,
                                                            @RequestParam(value="ticketMediaType", required = false) String ticketMediaType,
                                                            @RequestParam(value="pincode", required = false) String pincode,
                                                            @RequestParam(value="receiptNum", required = false) String receiptNum,
                                                            @RequestParam(value="orgName", required = false) String orgName,
                                                            @RequestParam(value="accountCode", required = false) String accountCode,
                                                            @RequestParam(value="partnerIds", required = false) String partnerIds,
                                                            @RequestParam("take") int take,
                                                            @RequestParam("skip") int skip,
                                                            @RequestParam("page") int page,
                                                            @RequestParam("pageSize") int pageSize,
                                                            @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<List<PartnerVM>>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }

            PinQueryFilterVM ft = new PinQueryFilterVM();
            ft.setFromDtStr(startDateStr);
            ft.setToDtStr(endDateStr);
            ft.setReceiptNum(receiptNum);
            ft.setOrgName(orgName);
            ft.setAccountCode(accountCode);
            ft.setPinCode(pincode);
            ft.setTicketMediaType(ticketMediaType);
            Date startDate = null;
            Date toDate = null;
            if(startDateStr != null && startDateStr.trim().length() > 0){
                startDate = NvxDateUtils.parseDate(startDateStr.trim(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            if(endDateStr != null && endDateStr.trim().length() > 0){
                toDate = NvxDateUtils.parseDate(endDateStr.trim(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            }
            List<Integer> partnerIdList = null;
            if(partnerIds != null && partnerIds.trim().length() > 0){
                partnerIdList = new ArrayList<Integer>();
                String[] tempIds = partnerIds.split(",");
                if(tempIds != null && tempIds.length > 0){
                    for(String tempId : tempIds){
                        if(tempId != null && tempId.trim().length() > 0){
                            partnerIdList.add(Integer.parseInt(tempId));
                        }
                    }
                }
                ft.setPaIds(partnerIdList.toArray(new Integer[partnerIdList.size()]));
            }
            ft.setFromDate(startDate);
            ft.setToDate(toDate);
            ApiResult<List<PackageViewResultVM>> rvm = transQuerySrv.getPinCodeQueryResults(ft, take, skip, page, pageSize, sortField, sortDirection);
            return new ResponseEntity<>(rvm, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewTxnQueryResults", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "resend-pkg-receipt", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiResendingPkgReceipt(HttpServletRequest request, @RequestParam(value="id", required = false) String id) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<String>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            String msg = transQuerySrv.resendingPkgReceipt(id);
            return new ResponseEntity<>(new ApiResult<>(true, "", msg, msg), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiResendingPkgReceipt", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-package-details", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<ApiResult<MixMatchPkgVM>> apiViewTxnQueryResults(HttpServletRequest request, @RequestParam(value="id", required = false) String id) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<MixMatchPkgVM>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            MixMatchPkgVM rvm = paPkgSrv.getPkgByID(Integer.parseInt(id.trim()));
            return new ResponseEntity<>(new ApiResult<>(true, null, null, rvm), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewTxnQueryResults", e, MixMatchPkgVM.class), HttpStatus.OK);
        }
    }

}
