package com.enovax.star.cms.partneradmin.controller.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.ReasonVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.service.ppslm.IAdminService;
import com.enovax.star.cms.partnershared.service.ppslm.IApprovalLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/admin-func")
public class AdminController extends BasePartnerAdminController {

    @Autowired
    private IAdminService adminSrv;
    @Autowired
    private IApprovalLogService approvalLogSrv;

    @RequestMapping(value = "get-admin-access-rights", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAdminUserAccessRight(HttpServletRequest request){
        try{
            List<String> list = adminSrv.getLoginAdminUserAssignedRights(getLoginAdminId());
            return new ResponseEntity<>(new ApiResult<String>(true, "", "", JsonUtil.jsonify(list)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetAdminUserAccessRight", e, String.class), HttpStatus.OK);
        }
    }
    @RequestMapping(value = "get-admin-access-and-approval-rights", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAdminUserApprovalAccessRight(HttpServletRequest request){
        try{
            List<String> list = adminSrv.getLoginAdminUserAssignedRights(getLoginAdminId());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", JsonUtil.jsonify(list)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetAdminUserApprovalAccessRight", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "verify-request/reject", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiVerifyRequestWithReject(@RequestParam(name = "id")Integer appId, @RequestParam(name="reasonVmJson")String reasonVmJson){
        try{
            ReasonVM reasonVM = JsonUtil.fromJson(reasonVmJson, ReasonVM.class);
            String errorKey = approvalLogSrv.rejectPendingRequest(appId, reasonVM, getLoginAdminId() );
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<String>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<String>(true, "", "", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifyRequestWithReject", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "verify-request/approve", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiVerifyRequestWithApprove(@RequestParam(name = "id")Integer appId){
        try{
            String errorKey = approvalLogSrv.approvePendingRequest(appId, getLoginAdminId() );
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<String>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<String>(true, "", "", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifyRequestWithApprove", e, String.class), HttpStatus.OK);
        }
    }

}
