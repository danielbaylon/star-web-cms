package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.jcrrepository.system.ppmflg.IUserRepository;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerRepository;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGTASubAccountRepository;
import com.enovax.star.cms.partnershared.repository.ppmflg.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.repository.ppmflg.ILineOfBusinessRepository;
import com.enovax.star.cms.partnershared.service.ppmflg.IAdminService;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lavanya on 25/10/16.
 */
@Service("PPMFLGIPartnerProfileReportService")
public class DefaultPartnerProfileReportService implements IPartnerProfileReportService {
    private static final Logger log = LoggerFactory.getLogger(DefaultPartnerProfileReportService.class);
    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService adminService;
    @Autowired
    @Qualifier("PPMFLGIUserRepository")
    private IUserRepository userRepository;
    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService partnerService;
    @Autowired
    private PPMFLGPartnerRepository partnerRepository;
    @Autowired
    private PPMFLGTASubAccountRepository subAccountRepository;
    @Autowired
    @Qualifier("PPMFLGILineOfBusinessRepository")
    private ILineOfBusinessRepository paTypeRepo;
    @Autowired
    @Qualifier("PPMFLGICountryRegionsRepository")
    private ICountryRegionsRepository countryRegionsRepo;

    public PartnerProfileReportInitVM initPage() {
        List<AdminAccountVM> adminAccountVMs = userRepository.getAllPPMFLGUsers();
        List<PartnerVM> partnerVMs = partnerService.getPartnerVMsByStatus(PartnerStatus.Active.code);
        PartnerProfileReportInitVM partnerProfileReportInitVM = new PartnerProfileReportInitVM(adminAccountVMs,partnerVMs);
        return partnerProfileReportInitVM;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultVM getPaProfilePage(PaProfileRptFilterVM filterVM, String sortField, String sortDirection, int page, int pageSize) {
        ResultVM resultVM = new ResultVM();
        if(sortField!=null && sortDirection!=null && !"".equals(sortField) && !"".equals(sortDirection)) {
            if("accountManager".equals(sortField)) {
                sortField = "accountManagerId";
            }
            if("subAccCount".equals(sortField)) {
                sortField = "SIZE(mainAccount.subAccs)";
            }
            if("orgTypeName".equals(sortField)) {
                sortField = "orgTypeCode";
            }
        }else {
            sortDirection = "DESC";
            sortField = "id";
        }
        PageRequest pageRequest = new PageRequest( page - 1, pageSize,
                "ASC".equalsIgnoreCase(sortDirection) ? Sort.Direction.ASC : Sort.Direction.DESC,
                sortField);

        log.info("AccMgr"+filterVM.getAccMgrId());
        log.info("Org"+filterVM.getOrgName());
        log.info("Status"+filterVM.getPaStatus());
        log.info("Pa"+filterVM.getPaIds().toString());
        if(filterVM.getOrgName()!=null && !filterVM.getOrgName().equals("")) {
            filterVM.setOrgName("%"+filterVM.getOrgName()+"%");
        }
        List<Integer> paIdList = Arrays.asList(filterVM.getPaIds());
        Page<PPMFLGPartner> partnerPage;
        int total;
        if(paIdList.size() == 0) {
            partnerPage = partnerRepository.getPartnerProfileWithoutId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus(), pageRequest);
            total = partnerRepository.getPartnerProfileSizeWithoutId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus());
        }
        else {
            partnerPage = partnerRepository.getPartnerProfileWithId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus(), paIdList, pageRequest);
            total = partnerRepository.getPartnerProfileSizeWithId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus(), paIdList);
        }
        List<PartnerVM> partnerVMs = new ArrayList<>();
        Iterator<PPMFLGPartner> iter = partnerPage.iterator();
        while (iter.hasNext()) {
            PPMFLGPartner partner = iter.next();
            PartnerVM partnerVM = new PartnerVM(partner);
            String orgTypeName = paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data,partnerVM.getOrgTypeCode());
            partnerVM.setOrgTypeName(orgTypeName);
            if(partnerVM.getAccountManagerId()!=null && !partnerVM.equals("")) {
                String name = adminService.getAdminUserNameByAdminId(partnerVM.getAccountManagerId());
                partnerVM.setAccountManager(name);
            }
            int subAccCount = partner.getMainAccount().getSubAccs().size();
            partnerVM.setSubAccCount(subAccCount);
            partnerVMs.add(partnerVM);
        }

        resultVM.setViewModel(partnerVMs);
        resultVM.setTotal(total);
        return resultVM;
    }


    @Override
    @Transactional(readOnly = true)
    public List<PartnerVM> getPaProfileList(PaProfileRptFilterVM filterVM) {


        log.info("AccMgr"+filterVM.getAccMgrId());
        log.info("Org"+filterVM.getOrgName());
        log.info("Status"+filterVM.getPaStatus());
        log.info("Pa"+filterVM.getPaIds().toString());
        if(filterVM.getOrgName()!=null && !filterVM.getOrgName().equals("")) {
            filterVM.setOrgName("%"+filterVM.getOrgName()+"%");
        }
        List<Integer> paIdList = Arrays.asList(filterVM.getPaIds());
        List<PPMFLGPartner> partnerList;

        if(paIdList.size() == 0) {
            partnerList = partnerRepository.getPartnerProfileWithoutId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus());
        }
        else {
            partnerList = partnerRepository.getPartnerProfileWithId(filterVM.getAccMgrId(), filterVM.getOrgName(), filterVM.getPaStatus(), paIdList);
        }
        List<PartnerVM> partnerVMs = new ArrayList<>();
        Iterator<PPMFLGPartner> iter = partnerList.iterator();
        while (iter.hasNext()) {
            PPMFLGPartner partner = iter.next();
            PartnerVM partnerVM = new PartnerVM(partner);
            String orgTypeName = paTypeRepo.getLineOfBusinessNameById(PartnerPortalConst.Partner_Portal_Ax_Data,partnerVM.getOrgTypeCode());
            partnerVM.setOrgTypeName(orgTypeName);
            if(partnerVM.getAccountManagerId()!=null && !partnerVM.equals("")) {
                String name = adminService.getAdminUserNameByAdminId(partnerVM.getAccountManagerId());
                partnerVM.setAccountManager(name);
            }
            int subAccCount = partner.getMainAccount().getSubAccs().size();
            partnerVM.setSubAccCount(subAccCount);
            partnerVMs.add(partnerVM);
        }
        return partnerVMs;
    }
}

