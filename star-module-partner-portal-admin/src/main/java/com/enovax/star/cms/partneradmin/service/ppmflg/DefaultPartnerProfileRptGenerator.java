package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.PaProfileRptFilterVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lavanya on 26/10/16.
 */
@Service("PPMFLGIPartnerProfileRptGenerator")
public class DefaultPartnerProfileRptGenerator implements IPartnerProfileRptGenerator {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    @Qualifier("PPMFLGIPartnerProfileReportService")
    private IPartnerProfileReportService  paProfileReportService;

    public Workbook generateExcel(PaProfileRptFilterVM filterVM){
        Workbook wb = new XSSFWorkbook();
        Sheet isheet = wb.createSheet("Partner Information");
        Row irow = null;
        Cell icell = null;
        int rowNum = this.writeFilter(wb,isheet, irow, icell, filterVM);

        rowNum++; //blank row
        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        int cnum = writeReportHeader(wb,irow);

        List<PartnerVM> paVms = paProfileReportService.getPaProfileList(filterVM);

        for(PartnerVM paVm:paVms){
            irow = isheet.createRow(++rowNum);
            this.writeReportContent(wb, irow, paVm);
        }

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);
        for (int i = 0; i <= cnum; i++) {
            isheet.autoSizeColumn(i);
        }
        return wb;
    }

    private void writeReportContent(Workbook wb, Row irow, PartnerVM paVm) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue(paVm.getOrgName());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getOrgTypeName());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getStatus());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getUen());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getLicenseNum());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getLicenseExpDate());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getContactDesignation()+" "+paVm.getContactPerson());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getAddress());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getPostalCode());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getCity());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getTelNum());

        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getFaxNum());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getEmail());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getWebsite());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getAccountManager());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paVm.getSubAccCount());
    }

    private int writeReportHeader(Workbook wb, Row irow) {
        XSSFCellStyle istyle = (XSSFCellStyle) wb.createCellStyle();
        istyle.setFillPattern(XSSFCellStyle.FINE_DOTS );
        istyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        istyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("Organization Name");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Nature of Business");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Account Status");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("UEN");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("License Number");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("License Expiry Date");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Contact Person (Designation)");
        icell.setCellStyle(istyle);

        icell = irow.createCell(++cnum);
        icell.setCellValue("Address");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Postal Code");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("City");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Telephone");
        icell.setCellStyle(istyle);

        icell = irow.createCell(++cnum);
        icell.setCellValue("Fax");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Email");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Website");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Account Manager");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Number Of Sub Account");
        icell.setCellStyle(istyle);
        return cnum;
    }

    private int writeFilter(Workbook wb, Sheet isheet, Row irow, Cell icell,
                            PaProfileRptFilterVM fliter) {
        int rowNum = 0;

        irow = isheet.createRow(rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("PARTNER INFORMATION REPORT");
        isheet.addMergedRegion(new CellRangeAddress(0,0,0,5));
        CellUtil.setAlignment(icell, wb, CellStyle.ALIGN_CENTER);
        XSSFFont ifont = (XSSFFont) wb.createFont();
        ifont.setFontHeight(18);
        ifont.setBold(true);

        CellUtil.setFont(icell, wb, ifont);

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Organization Name");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getOrgName());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Account Status");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getPaStatus());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Account Manager");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getAccMgrName());
        return rowNum;
    }

}
