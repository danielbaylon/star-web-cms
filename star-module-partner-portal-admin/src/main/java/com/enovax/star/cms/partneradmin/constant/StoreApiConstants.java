package com.enovax.star.cms.partneradmin.constant;

public final class StoreApiConstants {

    public static final String REQUEST_HEADER_CHANNEL = "Store-Api-Channel";

    public static final String SESSION_ATTR_STORE_TXN = "StoreTransaction";

    public static final String REQUEST_HEADER_LOCALE = "Store-Api-Locale";

}
