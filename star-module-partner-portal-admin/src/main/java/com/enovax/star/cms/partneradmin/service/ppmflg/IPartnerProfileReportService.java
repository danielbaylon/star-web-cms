package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.PaProfileRptFilterVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerProfileReportInitVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;

import java.util.List;

/**
 * Created by lavanya on 25/10/16.
 */
public interface IPartnerProfileReportService {
    public PartnerProfileReportInitVM initPage();

    public ResultVM getPaProfilePage(PaProfileRptFilterVM filterVM,String sortField, String sortDirection, int page, int pageSize);

    public List<PartnerVM> getPaProfileList(PaProfileRptFilterVM filterVM);
}
