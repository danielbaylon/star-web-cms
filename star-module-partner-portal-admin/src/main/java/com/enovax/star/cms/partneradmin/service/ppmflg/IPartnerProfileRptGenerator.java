package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.PaProfileRptFilterVM;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by lavanya on 26/10/16.
 */
public interface IPartnerProfileRptGenerator {
    public Workbook generateExcel(PaProfileRptFilterVM filterVM);
}
