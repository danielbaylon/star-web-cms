package com.enovax.star.cms.partneradmin.controller.ppmflg;


import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerProductVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerAdminProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller("PPMFLGProductManagementController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/product-management/")
public class ProductManagementController extends  BasePartnerAdminController {

    private static final Logger log = LoggerFactory.getLogger(ProductManagementController.class);

    @Autowired
    @Qualifier("PPMFLGIPartnerAdminProductService")
    IPartnerAdminProductService productService;

    @RequestMapping(value = "get-exclusive-products", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetExclusiveProducts(@ModelAttribute ProductGridFilterVM productGridFilterVM) {
        log.info("Entered apiGetAllActiveProducts...");
        try {
            productGridFilterVM = initFilterChannel(productGridFilterVM);
            ApiResult<List<PartnerProductVM>> productList = productService.getProdVmsByPage(productGridFilterVM, true);
            ApiResult<String> result = new ApiResult<>();
            result.setData(JsonUtil.jsonify(productList.getData()));
            result.setTotal(productList.getTotal());
            result.setSuccess(true);
            result.setMessage("");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllProducts] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-all-active-products", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAllActiveProducts(@ModelAttribute ProductGridFilterVM productGridFilterVM) {
        log.info("Entered apiGetAllActiveProducts...");
        try {
            productGridFilterVM = initFilterChannel(productGridFilterVM);
            ApiResult<List<PartnerProductVM>> productList = productService.getProdVmsByPage(productGridFilterVM, false);
            ApiResult<String> result = new ApiResult<>();
            result.setData(JsonUtil.jsonify(productList.getData()));
            result.setTotal(productList.getTotal());
            result.setSuccess(true);
            result.setMessage("");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllProducts] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}
