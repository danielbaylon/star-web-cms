package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.PaProfileRptFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProfileReportInitVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ResultVM;

import java.util.List;

/**
 * Created by lavanya on 25/10/16.
 */
public interface IPartnerProfileReportService {
    public PartnerProfileReportInitVM initPage();

    public ResultVM getPaProfilePage(PaProfileRptFilterVM filterVM,String sortField, String sortDirection, int page, int pageSize);

    public List<PartnerVM> getPaProfileList(PaProfileRptFilterVM filterVM);
}
