package com.enovax.star.cms.partneradmin.controller.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partneradmin.service.ppmflg.ISystemConfigurationService;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 28/9/16.
 */
@Controller("PPMFLGSystemConfigurationController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/sys-config/")
public class SystemConfigurationController extends BasePartnerAdminController {
    @Autowired
    @Qualifier("PPMFLGISystemConfigurationService")
    ISystemConfigurationService systemConfigurationService;
    @Autowired
    @Qualifier("PPMFLGIApproverService")
    IApproverService approverService;
    @Autowired
    @Qualifier("PPMFLGIRevalidationFeeService")
    IRevalidationFeeService revalidationFeeService;
    @Autowired
    @Qualifier("PPMFLGIPartnerSharedService")
    IPartnerSharedService partnerSharedService;
    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeSharedService")
    IWingsOfTimeSharedService wingsOfTimeSharedService;
    @Autowired
    @Qualifier("PPMFLGIReasonService")
    IReasonService reasonService;
    @Autowired
    @Qualifier("PPMFLGIGSTRateService")
    IGSTRateService gstRateService;

    @RequestMapping(value = "get-general-config", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetSysConfig() {
        try {

            GeneralConfigVM generalConfigVm = systemConfigurationService.getGeneralConfigurationViewModel();
            String generalConfigJson = JsonUtil.jsonify(generalConfigVm);
            if(generalConfigVm != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", generalConfigJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.ConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetSysConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-backup-approver", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiGetBackupApprover(@RequestParam("take") int take,
                                                                    @RequestParam("skip") int skip,
                                                                    @RequestParam("page") int page,
                                                                    @RequestParam("pageSize") int pageSize,
                                                                    @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                    @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {

            ResultVM result = systemConfigurationService.listBackupApprovers(sortField,sortDirection,page,pageSize);
            if(result != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", result), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.ConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetBackupApprover] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-approver", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiSaveApprover(@RequestParam("adminId") String adminId) {
        try {

            ResultVM resultVM = approverService.saveApprover(adminId,getLoginAdminId());
            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveApprover] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-backup-approver", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiSaveBackupApprover(@RequestParam(name = "approverJson")String approverJson) {
        try {
            ApproverVM approverVm = JsonUtil.fromJson(approverJson,ApproverVM.class);
            ResultVM resultVM = approverService.saveBackupApprover(approverVm,getLoginAdminId());
            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveBackupApprover] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-backup-approver", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRemoveBackupApprover(@RequestParam(name = "backupIds")String backupIds) {
        try {

            ResultVM resultVM = systemConfigurationService.removeBackupApprovers(backupIds,getLoginAdminId());
            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveBackupApprover] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-system-param", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiSaveSystemParam(@RequestParam(name = "paramKey")String paramKey,
                                                                  @RequestParam(name = "paramValue")String paramValue,
                                                                  @RequestParam(name = "paramLabel")String paramLabel) {
        try {

            ResultVM resultVM = systemConfigurationService.saveParam(paramKey,paramValue,paramLabel,getLoginAdminId());
            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSystemParam] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-ticket-config", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetTicketConfig() {
        try {

            Map<String,Object> ticketConfigMap = systemConfigurationService.getTicketConfigMap();
            String ticketConfigJson = JsonUtil.jsonify(ticketConfigMap);
            if(ticketConfigMap != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", ticketConfigJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.TicketConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTicketConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-reval-item-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiGetRevalItemList() {
        try {
            ResultVM resultVm = systemConfigurationService.getRevalItemList(StoreApiChannels.PARTNER_PORTAL_MFLG);
            if(resultVm != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.RevalItemRetrievalError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetRevalItemList] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-wot-products-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiGetWotProductsList() {
        try {
            ResultVM resultVm = systemConfigurationService.getWotProducts(StoreApiChannels.PARTNER_PORTAL_MFLG);
            if(resultVm != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.WotProductsRetrievalError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetWotProductsList] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-ax-products-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiGetAxProductsList(@RequestParam(name = "productName", required = false)String productName) {
        try {
            ResultVM resultVm = systemConfigurationService.getAxProductsList(productName, false);
            if(resultVm != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.TicketConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAxProductsList] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-wot-ax-products-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiGetWotAxProductsList(@RequestParam(name = "productName", required = false)String productName) {
        try {
            ResultVM resultVm = systemConfigurationService.getAxProductsList(productName, true);
            if(resultVm != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.TicketConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetWotAxProductsList] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-revalidation-item", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiSaveRevalidtionFeeItem(@RequestParam(name = "feeItemVmJson")String feeItemVmJson) {
        try {
            RevalFeeItemVM feeItemVm = JsonUtil.fromJson(feeItemVmJson,RevalFeeItemVM.class);
            ResultVM resultVM   = revalidationFeeService.saveRevalidationFeeItem(StoreApiChannels.PARTNER_PORTAL_MFLG,feeItemVm);

            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveRevalidtionFeeItem] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-revalidation-item", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRemoveRevalidtionFeeItem(@RequestParam(name = "feeItemIds")String feeItemIds) {
        try {


            ResultVM resultVM   = revalidationFeeService.removeRevalidationFeeItem(StoreApiChannels.PARTNER_PORTAL_MFLG,feeItemIds);

            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveRevalidtionFeeItem] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-wot-product", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiSaveWotProduct(@RequestParam(name = "wotProductVmJson")String wotProductVmJson) {
        try {
            WOTProductVM wotProductVm = JsonUtil.fromJson(wotProductVmJson,WOTProductVM.class);
            ResultVM resultVM   = wingsOfTimeSharedService.saveWOTProduct(StoreApiChannels.PARTNER_PORTAL_MFLG,wotProductVm);

            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveWotProduct] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-wot-product", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRemoveWotProduct(@RequestParam(name = "wotProdIds")String wotProdIds) {
        try {


            ResultVM resultVM   = wingsOfTimeSharedService.removeWOTProduct(StoreApiChannels.PARTNER_PORTAL_MFLG,wotProdIds);

            if(resultVM.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", resultVM.getMessage(), resultVM), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveWotProduct] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-wot-config", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetWotConfig() {
        try {

            Map<String,Object> wotConfigMap = systemConfigurationService.getWotConfigMap();
            String wotConfigJson = JsonUtil.jsonify(wotConfigMap);
            if(wotConfigMap != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", wotConfigJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.WotConfigNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetWotConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "reset-wot", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiResetWot() {
        try {
            boolean status = partnerSharedService.resetAllPartnerReservationCap(getLoginAdminId());

            if(status){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", null), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.WotResetError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiResetWot] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-reason-list", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiGetReason(@RequestParam("skip") int skip,
                                                                      @RequestParam("page") int page,
                                                                      @RequestParam("pageSize") int pageSize,
                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection){
        try {
            ResultVM resultVM =reasonService.getReasonsbyPage(sortField,sortDirection,page,pageSize);

            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetReason] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-gst-list", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiGetGst(){
        try {
//            ResultVM resultVM =reasonService.getReasonsbyPage(sortField,sortDirection,page,pageSize);
            List<GSTRateVM> gstList = gstRateService.listGstVms();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(gstList);
            resultVM.setTotal(gstList.size());
            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetGst] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-reason", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiSaveReason(@RequestParam(name="reasonVmJson")String reasonJson){
        try {
            ReasonVM reasonVM = JsonUtil.fromJson(reasonJson,ReasonVM.class);
            final ReasonVM result= reasonService.saveReason(reasonVM,getLoginAdminId());
            String resultJson = JsonUtil.jsonify(result);
            return new ResponseEntity<>(new ApiResult<>(true,"","",resultJson), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveReason] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-reason", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRemoveReason(@RequestParam(name = "reasonIds")String reasonIds) {
        try {
            final List<Integer> rcmdIds = new ArrayList<>();
            for (String s : reasonIds.split(",")) {
                rcmdIds.add(Integer.parseInt(s));
            }
            reasonService.removeReasons(rcmdIds,getLoginAdminId());
            return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveReason] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-gst", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<ResultVM>> apiSaveGstRate(@RequestParam(name="gstVmJson")String gstJson){
        try {
            GSTRateVM gstRateVM = JsonUtil.fromJson(gstJson,GSTRateVM.class);
            final ResultVM result= gstRateService.saveGst(gstRateVM,getLoginAdminId());
            if(result.isStatus())
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", result), HttpStatus.OK);
            else
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "", result.getMessage(), result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveGstRate] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-gst", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRemoveGstRate(@RequestParam(name = "gstIds")String gstIds) {
        try {
            final List<Integer> gstIdList = new ArrayList<>();
            for (String s : gstIds.split(",")) {
                gstIdList.add(Integer.parseInt(s));
            }
            gstRateService.removeGst(gstIdList,getLoginAdminId());
            return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveGstRate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }
}