package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPaDistributionMapping;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMInventoryTransactionRepository;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPaDistributionMappingRepository;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import com.enovax.star.cms.partnershared.repository.ppslm.ICountryRegionsRepository;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by lavanya on 28/10/16.
 */
@Service
public class DefaultCountryMktShareReportService implements ICountryMktShareReportService {
    private static final Logger log = LoggerFactory.getLogger(DefaultCountryMktShareReportService.class);
    @Autowired
    IPartnerService partnerService;
    @Autowired
    ICountryRegionsRepository countryRegionsRepository;
    @Autowired
    PPSLMInventoryTransactionRepository inventoryTransactionRepository;
    @Autowired
    PPSLMPaDistributionMappingRepository paDistributionMappingRepository;

    public CountryMktShareReportInitVM initPage() {
        List<PartnerVM> partnerVMs = partnerService.getPartnerVMsByStatus(PartnerStatus.Active.code);
        List<CountryVM> countryVMs = countryRegionsRepository.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        CountryMktShareReportInitVM countryMktShareReportInitVM = new CountryMktShareReportInitVM(partnerVMs,countryVMs);
        return countryMktShareReportInitVM;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultVM getCountryMktShareReportDetail(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, int page, int pageSize) {
        log.info("getMktShareDetail:");
        ResultVM res = new ResultVM();

        sortDirection = "DESC";
        sortField = "mainAccount.profile.id";
        PageRequest pageRequest = new PageRequest( page - 1, pageSize,
                "ASC".equalsIgnoreCase(sortDirection) ? Sort.Direction.ASC : Sort.Direction.DESC,
                sortField);
        List<Integer> paIdList = Arrays.asList(filterVM.getPaIds());
        CountryMktShareRptDetailsVM paMkts = null;
        int total;

        if(paIdList.size() == 0) {
            paMkts = this.manipulatePartnerMktWithoutID(filterVM, sortField, sortDirection, pageRequest);
            total = inventoryTransactionRepository.getCountryMktShareSizeWithoutID(filterVM.getStartDate(),filterVM.getToDate(),filterVM.getOrgName());
        }
        else {
            paMkts = this.manipulatePartnerMktWithID(filterVM, sortField, sortDirection, pageRequest);
            total = inventoryTransactionRepository.getCountryMktShareSizeWithID(filterVM.getStartDate(),filterVM.getToDate(),filterVM.getOrgName(),paIdList);
        }
        res.setViewModel(paMkts);
        res.setTotal(total);
        return res;
    }

    @Override
    @Transactional(readOnly = true)
    public CountryMktShareRptDetailsVM manipulatePartnerMktWithoutID(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, PageRequest pageRequest){

        Page<MktSharePartnerVM> paMktsPaged = inventoryTransactionRepository.getCountryMktShareDetailsWithoutID(filterVM.getStartDate(),filterVM.getToDate(),filterVM.getOrgName(),pageRequest);
        Iterator<MktSharePartnerVM> iter = paMktsPaged.iterator();
        List<MktSharePartnerVM> paMkts= new ArrayList<>();
        CountryMktShareRptDetailsVM countryMktShareRptDetailsVM=null;
        while(iter.hasNext()) {
            MktSharePartnerVM mktSharePartnerVM = iter.next();
            paMkts.add(mktSharePartnerVM);
        }
//        for (MktSharePartnerVM paMkt : mktSharePartnerVMList) {
//            paMkt.setTotalStr(TransactionUtil.priceWithDecimal(paMkt.getTotal(), ""));
//        }
        List<CountryVM> countryVms = countryRegionsRepository.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        if (paMkts != null && paMkts.size() > 0) {
            List<Integer> paIds = new ArrayList<>();
            for (int i = 0; i < paMkts.size(); i++) {
                paIds.add(paMkts.get(i).getId());
            }
            List<PPSLMPaDistributionMapping> paDistributionMappings =  paDistributionMappingRepository.findMappingbyPartnerIds(paIds);
            Set<String> usedCountryIDs = new HashSet<>();
            List<MktShareRegionVM> mktRegionDetails = new ArrayList<>();
            for(PPSLMPaDistributionMapping paDistributionMapping : paDistributionMappings) {
                MktShareRegionVM mktShareRegionVM = new MktShareRegionVM();
                mktShareRegionVM.setPaId(paDistributionMapping.getPartnerId());
                mktShareRegionVM.setCtyId(paDistributionMapping.getCountryId());
                mktShareRegionVM.setPercentage(paDistributionMapping.getPercentage());
                mktRegionDetails.add(mktShareRegionVM);
                usedCountryIDs.add(paDistributionMapping.getCountryId());
            }
            List usedCountryVMs = new ArrayList();
            for(String countryId : usedCountryIDs) {
                String countryName = countryRegionsRepository.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data, countryId);
                CountryVM countryVM = new CountryVM();
                countryVM.setCtyCode(countryId);
                countryVM.setCtyName(countryName);
                usedCountryVMs.add(countryVM);
            }
            for (MktSharePartnerVM paMkt : paMkts) {
                paMkt.setTotalStr(TransactionUtil.priceWithDecimal(paMkt.getTotal(), ""));
                this.manipulateMktShare(paMkt, countryVms, mktRegionDetails);
            }
            countryMktShareRptDetailsVM = new CountryMktShareRptDetailsVM(usedCountryVMs,paMkts);
        }

        return countryMktShareRptDetailsVM;
    }

    @Override
    @Transactional(readOnly = true)
    public CountryMktShareRptDetailsVM manipulatePartnerMktWithID(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, PageRequest pageRequest){
        List<Integer> paIdList = Arrays.asList(filterVM.getPaIds());
        Page<MktSharePartnerVM> paMktsPaged = inventoryTransactionRepository.getCountryMktShareDetailsWithID(filterVM.getStartDate(),filterVM.getToDate(),filterVM.getOrgName(),paIdList,pageRequest);
        Iterator<MktSharePartnerVM> iter = paMktsPaged.iterator();
        List<MktSharePartnerVM> paMkts= new ArrayList<>();
        CountryMktShareRptDetailsVM countryMktShareRptDetailsVM=null;
        while(iter.hasNext()) {
            MktSharePartnerVM mktSharePartnerVM = iter.next();
            paMkts.add(mktSharePartnerVM);
        }
//        for (MktSharePartnerVM paMkt : mktSharePartnerVMList) {
//            paMkt.setTotalStr(TransactionUtil.priceWithDecimal(paMkt.getTotal(), ""));
//        }
        List<CountryVM> countryVms = countryRegionsRepository.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        if (paMkts != null && paMkts.size() > 0) {
            List<Integer> paIds = new ArrayList<>();
            for (int i = 0; i < paMkts.size(); i++) {
                paIds.add(paMkts.get(i).getId());
            }
            List<PPSLMPaDistributionMapping> paDistributionMappings =  paDistributionMappingRepository.findMappingbyPartnerIds(paIds);
            Set<String> usedCountryIDs = new HashSet<>();
            List<MktShareRegionVM> mktRegionDetails = new ArrayList<>();
            for(PPSLMPaDistributionMapping paDistributionMapping : paDistributionMappings) {
                MktShareRegionVM mktShareRegionVM = new MktShareRegionVM();
                mktShareRegionVM.setPaId(paDistributionMapping.getPartnerId());
                mktShareRegionVM.setCtyId(paDistributionMapping.getCountryId());
                mktShareRegionVM.setPercentage(paDistributionMapping.getPercentage());
                mktRegionDetails.add(mktShareRegionVM);
                usedCountryIDs.add(paDistributionMapping.getCountryId());
            }
            List usedCountryVMs = new ArrayList();
            for(String countryId : usedCountryIDs) {
                String countryName = countryRegionsRepository.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data, countryId);
                CountryVM countryVM = new CountryVM();
                countryVM.setCtyCode(countryId);
                countryVM.setCtyName(countryName);
                usedCountryVMs.add(countryVM);
            }
            for (MktSharePartnerVM paMkt : paMkts) {
                paMkt.setTotalStr(TransactionUtil.priceWithDecimal(paMkt.getTotal(), ""));
                this.manipulateMktShare(paMkt, countryVms, mktRegionDetails);
            }
            countryMktShareRptDetailsVM = new CountryMktShareRptDetailsVM(usedCountryVMs,paMkts);
        }

        return countryMktShareRptDetailsVM;
    }

    @Override
    @Transactional(readOnly = true)
    public CountryMktShareRptDetailsVM manipulatePartnerMkt(CountryMktShareRptFilterVM filterVM){

        List<Integer> paIdList = Arrays.asList(filterVM.getPaIds());
        List<MktSharePartnerVM> paMkts = null;
        if(paIdList.size() == 0) {
            paMkts = inventoryTransactionRepository.getCountryMktShareDetailsWithoutID(filterVM.getStartDate(), filterVM.getToDate(), filterVM.getOrgName());
        }else {
            paMkts = inventoryTransactionRepository.getCountryMktShareDetailsWithID(filterVM.getStartDate(), filterVM.getToDate(), filterVM.getOrgName(), paIdList);
        }
        CountryMktShareRptDetailsVM countryMktShareRptDetailsVM=null;

        List<CountryVM> countryVms = countryRegionsRepository.getCountryVmList(PartnerPortalConst.Partner_Portal_Ax_Data);
        if (paMkts != null && paMkts.size() > 0) {
            List<Integer> paIds = new ArrayList<>();
            for (int i = 0; i < paMkts.size(); i++) {
                paIds.add(paMkts.get(i).getId());
            }
            List<PPSLMPaDistributionMapping> paDistributionMappings =  paDistributionMappingRepository.findMappingbyPartnerIds(paIds);
            Set<String> usedCountryIDs = new HashSet<>();
            List<MktShareRegionVM> mktRegionDetails = new ArrayList<>();
            for(PPSLMPaDistributionMapping paDistributionMapping : paDistributionMappings) {
                MktShareRegionVM mktShareRegionVM = new MktShareRegionVM();
                mktShareRegionVM.setPaId(paDistributionMapping.getPartnerId());
                mktShareRegionVM.setCtyId(paDistributionMapping.getCountryId());
                mktShareRegionVM.setPercentage(paDistributionMapping.getPercentage());
                mktRegionDetails.add(mktShareRegionVM);
                usedCountryIDs.add(paDistributionMapping.getCountryId());
            }
            List usedCountryVMs = new ArrayList();
            for(String countryId : usedCountryIDs) {
                String countryName = countryRegionsRepository.getCountryNameByCountryId(PartnerPortalConst.Partner_Portal_Ax_Data, countryId);
                CountryVM countryVM = new CountryVM();
                countryVM.setCtyCode(countryId);
                countryVM.setCtyName(countryName);
                usedCountryVMs.add(countryVM);
            }
            for (MktSharePartnerVM paMkt : paMkts) {
                paMkt.setTotalStr(TransactionUtil.priceWithDecimal(paMkt.getTotal(), ""));
                this.manipulateMktShare(paMkt, countryVms, mktRegionDetails);
            }
            countryMktShareRptDetailsVM = new CountryMktShareRptDetailsVM(usedCountryVMs,paMkts);
        }

        return countryMktShareRptDetailsVM;
    }

    private void manipulateMktShare(MktSharePartnerVM paMkt,
                                    List<CountryVM> countryVms, List<MktShareRegionVM> mktRegionDetails) {
        List<MktShareRegionVM> clone = new ArrayList<MktShareRegionVM>(
                mktRegionDetails.size());
        for (CountryVM cty : countryVms) {
            for (MktShareRegionVM regionDetail : mktRegionDetails) {
                if (paMkt.getId() != null
                        && paMkt.getId().equals(regionDetail.getPaId())
                        && cty.getCtyCode().equals(regionDetail.getCtyId())) {
                    MktShareRegionVM tmpObj = null;
                    try {
                        tmpObj = regionDetail.clone();
                        tmpObj.setCtyName(cty.getCtyName());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    BigDecimal ammount = paMkt.getTotal().multiply(new BigDecimal(regionDetail.getPercentage() * 1.0 / 100));
                    tmpObj.setAmmountStr(TransactionUtil.priceWithDecimal(ammount, ""));
                    tmpObj.setAmmount(ammount);
                    clone.add(tmpObj);
                }
            }
        }
        paMkt.setMktRegionDetails(clone);
    }
}
