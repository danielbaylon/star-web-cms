package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;
import com.enovax.star.cms.partnershared.repository.ppmflg.ICountryRegionsRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 5/11/16.
 */
@Service("PPMFLGICountryMktShareRptGenerator")
public class DefaultCountryMktShareRptGenerator implements ICountryMktShareRptGenerator {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    @Qualifier("PPMFLGICountryMktShareReportService")
    private ICountryMktShareReportService mktShareReportService;
    //    @Autowired
//    private RegionService regionService;
    @Autowired
    @Qualifier("PPMFLGICountryRegionsRepository")
    private ICountryRegionsRepository countryRegionsRepository;

    public Workbook generateExcel(CountryMktShareRptFilterVM fliter){
        Workbook wb = new XSSFWorkbook();
        Sheet isheet = wb.createSheet("TA Market Share");
        Row irow = null;
        Cell icell = null;
        int rowNum = this.writeFilter(wb,isheet, irow, icell, fliter);

        rowNum++; //blank row
        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        CountryMktShareRptDetailsVM countryMktShareRptDetailsVM = mktShareReportService.manipulatePartnerMkt(fliter);

        List<CountryVM> countryVms = countryMktShareRptDetailsVM.getAllCountryVMs();
        int cnum = writeReportHeader(wb,irow,countryVms);
        BigDecimal grantTotal = new BigDecimal(0);
        Map<String,BigDecimal> grandTotalMap = new HashMap<>();
        for(MktSharePartnerVM paMkt:countryMktShareRptDetailsVM.getMktSharePartnerVMs()){
            irow = isheet.createRow(++rowNum);
            this.writeReportContent(wb, paMkt,countryVms, irow);
            grantTotal.add(paMkt.getTotal());
//            for(MktShareTransVM dailyTrans:paMkt.getDailyTrans()){
//                irow = isheet.createRow(++rowNum);
//                this.writeSubTrans(wb,irow,regionVms,dailyTrans);
//            }
            for(MktShareRegionVM regionDetail:paMkt.getMktRegionDetails()){
                BigDecimal ammount = grandTotalMap.get(regionDetail.getCtyId());
                if(ammount==null||ammount.equals(BigDecimal.ZERO)){
                    grandTotalMap.put(regionDetail.getCtyId(), regionDetail.getAmmount());
                }else{
                    if(regionDetail.getAmmount()!=null){
                        ammount = ammount.add(regionDetail.getAmmount());
                        grandTotalMap.put(regionDetail.getCtyId(),ammount);
                    }
                }
            }
            grantTotal = grantTotal.add(paMkt.getTotal());
        }
        irow = isheet.createRow(++rowNum);
        this.writeGrantTotal(wb,irow, countryVms, grantTotal, grandTotalMap);
        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);
        for (int i = 0; i <= cnum; i++) {
            isheet.autoSizeColumn(i);
        }
        return wb;
    }

    private int writeFilter(Workbook wb,Sheet isheet, Row irow, Cell icell, CountryMktShareRptFilterVM fliter){
        int rowNum = 0;

        irow = isheet.createRow(rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Market Performance From "+fliter.getStartDateStr()+ " to "+fliter.getEndDateStr());
        isheet.addMergedRegion(new CellRangeAddress(0,0,0,5));
        CellUtil.setAlignment(icell, wb, CellStyle.ALIGN_CENTER);
        XSSFFont ifont = (XSSFFont) wb.createFont();
        ifont.setBold(true);
        ifont.setFontHeight(18);

        CellUtil.setFont(icell, wb, ifont);

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Transaction Start Date");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getStartDateStr());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Transaction End Date");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getEndDateStr());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Travel Agent Name");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getOrgName());
        return rowNum;
    }

    private int writeReportHeader(Workbook wb,Row irow,List<CountryVM> countryVms) {
        XSSFCellStyle istyle = (XSSFCellStyle) wb.createCellStyle();
        istyle.setFillPattern(XSSFCellStyle.FINE_DOTS );
        istyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        istyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("UEN");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Agent Status (Current)");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Travel Agents");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Total");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Date");
        icell.setCellStyle(istyle);

//        XSSFCellStyle regionStyle = (XSSFCellStyle) wb.createCellStyle();
//        regionStyle.setFillPattern(XSSFCellStyle.FINE_DOTS );
//        regionStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
//        regionStyle.setFillBackgroundColor(IndexedColors.RED.getIndex());

        for(CountryVM ctyVm:countryVms){
            icell = irow.createCell(++cnum);
            icell.setCellValue(ctyVm.getCtyName());
            icell.setCellStyle(istyle);
        }
        return cnum;
    }

    private void writeReportContent(Workbook wb, MktSharePartnerVM paMkt, List<CountryVM> countryVms, Row irow) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue(paMkt.getUen());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paMkt.getStatus());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paMkt.getOrgName());
        icell = irow.createCell(++cnum);
        icell.setCellValue(paMkt.getTotalStr());
        icell = irow.createCell(++cnum);
        icell.setCellValue("-");

        for(CountryVM ctyVm:countryVms){
            icell = irow.createCell(++cnum);
            for(MktShareRegionVM detail:paMkt.getMktRegionDetails()){
                if(ctyVm.getCtyCode().equals(detail.getCtyId())){
                    icell.setCellValue(detail.getAmmountStr());
                }
            }
        }
    }

    private void writeGrantTotal(Workbook wb, Row irow, List<CountryVM> countryVms,
                                 BigDecimal grantTotal, Map<String,BigDecimal> grandTotalMap) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("");
        icell = irow.createCell(++cnum);
        icell.setCellValue("");
        icell = irow.createCell(++cnum);
        icell.setCellValue("Grand Total");
        XSSFFont ifont = (XSSFFont) wb.createFont();
        ifont.setBold(true);
        CellUtil.setFont(icell, wb, ifont);
        icell = irow.createCell(++cnum);
        icell.setCellValue(TransactionUtil.priceWithDecimal(grantTotal, ""));
        icell = irow.createCell(++cnum);
        for(CountryVM ctyVm:countryVms){
            icell = irow.createCell(++cnum);
            BigDecimal amount = grandTotalMap.get(ctyVm.getCtyCode());
            icell.setCellValue(TransactionUtil.priceWithDecimal(amount, ""));
        }
    }

}
