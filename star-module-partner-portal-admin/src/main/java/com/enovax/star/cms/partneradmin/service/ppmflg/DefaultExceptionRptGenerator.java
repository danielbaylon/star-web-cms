package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.ExceptionRptDetailsVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ExceptionRptFilterVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ExceptionRptPkgItemVM;
import com.enovax.star.cms.partnershared.service.ppmflg.IReportService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lavanya on 10/11/16.
 */
@Service("PPMFLGIExceptionRptGenerator")
public class DefaultExceptionRptGenerator implements IExceptionRptGenerator {


    @Autowired
    @Qualifier("PPMFLGIReportService")
    IReportService reportService;

    public Workbook generateExcel(ExceptionRptFilterVM fliter){

        Workbook wb = new XSSFWorkbook();
        Sheet isheet = wb.createSheet("Exception Report");
        Row irow = null;
        Cell icell = null;
        int rowNum = this.writeFilter(wb,isheet, irow, icell, fliter);

        rowNum++; //blank row
        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        int cnum = writeReportHeader(wb,irow);

        List<ExceptionRptDetailsVM> finReconVms = reportService.getExceptionReportList(fliter);

        for(ExceptionRptDetailsVM finReconVm:finReconVms){
            irow = isheet.createRow(++rowNum);
            this.writeReportContent(wb, irow, finReconVm);
        }

        rowNum = this.writeReportFooter(wb, isheet, rowNum, irow);

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);


        for (int i = 0; i <= cnum; i++) {
            isheet.autoSizeColumn(i);
        }
        return wb;
    }

    private void writeReportContent(Workbook wb, Row irow, ExceptionRptDetailsVM finReconVm) {
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        String ticketMediaStr = finReconVm.getTicketMedia();
        if("Pincode".equalsIgnoreCase(ticketMediaStr)) {
            ticketMediaStr = "PINCODE\n"+finReconVm.getPinCode();
        }else if("ETicket".equalsIgnoreCase(ticketMediaStr)) {
            ticketMediaStr = "E-Ticket";
        }else if("ExcelFile".equalsIgnoreCase(ticketMediaStr)) {
            ticketMediaStr = "Excel File";
        }
        icell.setCellValue(ticketMediaStr);
        icell = irow.createCell(++cnum);
        String tmsDateStrs = "";
        String receiptNums = "";
        for(ExceptionRptPkgItemVM pkgItem:finReconVm.getPkgItems()){
            tmsDateStrs = tmsDateStrs + pkgItem.getTmStatusDateStr() + "\n\t";
            receiptNums = receiptNums + pkgItem.getReceiptNum() + "\n\t";
        }

        icell.setCellValue(tmsDateStrs);
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getOrgName()+"("+finReconVm.getAccountCode()+")");
        icell = irow.createCell(++cnum);
        icell.setCellValue(receiptNums);
        icell = irow.createCell(++cnum);
        String displayNameStr = "";
        for(ExceptionRptPkgItemVM pkgItem:finReconVm.getPkgItems()){
            displayNameStr = displayNameStr + pkgItem.getDisplayName() + "\n\t";
        }

        icell.setCellValue(displayNameStr);
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getPurchasedQty());
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getPinCodeQty());
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getQtyRedeemed());
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getV1()<0?0:finReconVm.getV1());

        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getV2()<0?0:finReconVm.getV2());
        icell = irow.createCell(++cnum);
        icell.setCellValue(finReconVm.getV3()<0?0:finReconVm.getV3());
    }

    private int writeReportHeader(Workbook wb, Row irow) {
        XSSFCellStyle istyle = (XSSFCellStyle) wb.createCellStyle();
        istyle.setFillPattern(XSSFCellStyle.FINE_DOTS );
        istyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        istyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        int cnum = 0;
        Cell icell = null;
        icell = irow.createCell(cnum);
        icell.setCellValue("Ticket Media");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Purchase Date");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Partner Name");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Receipt No.");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Product Description");
        icell.setCellStyle(istyle);

        icell = irow.createCell(++cnum);
        icell.setCellValue("Quantity Purchased");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("PINCODE Quantity ( # of Ticket)");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Quantity Redeem");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Variance 1");
        icell.setCellStyle(istyle);

        icell = irow.createCell(++cnum);
        icell.setCellValue("Variance 2");
        icell.setCellStyle(istyle);
        icell = irow.createCell(++cnum);
        icell.setCellValue("Variance 3");
        icell.setCellStyle(istyle);

        return cnum;
    }

    private int writeFilter(Workbook wb, Sheet isheet, Row irow, Cell icell,
                            ExceptionRptFilterVM fliter) {
        int rowNum = 0;

        irow = isheet.createRow(rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Exception Report");
        isheet.addMergedRegion(new CellRangeAddress(0,0,0,5));
        CellUtil.setAlignment(icell, wb, CellStyle.ALIGN_CENTER);
        XSSFFont ifont = (XSSFFont) wb.createFont();
        ifont.setFontHeight(18);
        ifont.setBold(true);

        CellUtil.setFont(icell, wb, ifont);

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);


        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Transaction Start Date");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getStartDateStr());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Transaction End Date");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getEndDateStr());


        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Organization Name");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getOrgName());

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Product Description");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getProdDesc());


        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Receipt No.");
        icell = irow.createCell(1);
        icell.setCellValue(fliter.getReceiptNum());

        return rowNum;
    }

    private int writeReportFooter(Workbook wb,Sheet isheet,int rowNum,Row irow) {
        XSSFFont ifont = (XSSFFont) wb.createFont();
        ifont.setBold(true);
        ifont.setColor(IndexedColors.RED.getIndex());

        irow = isheet.createRow(++rowNum);
        irow = isheet.createRow(++rowNum);

        Cell icell = null;
        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Report Legend:");
        CellUtil.setFont(icell, wb, ifont);

        ifont.setBold(false);

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Variance 1) Quantity Redeemed > Quantity Purchased");
        CellUtil.setFont(icell, wb, ifont);


        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Variance 2) Quantity Redeemed > PINCODE Quantity");
        CellUtil.setFont(icell, wb, ifont);

        irow = isheet.createRow(++rowNum);
        icell = irow.createCell(0);
        icell.setCellValue("Variance 3) PINCODE Quantity > Quantity Purchased");
        CellUtil.setFont(icell, wb, ifont);

        return rowNum;

    }

}
