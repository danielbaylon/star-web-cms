package com.enovax.star.cms.partneradmin.controller.ppmflg;


import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocument;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPADocMappingRepository;
import com.enovax.star.cms.commons.service.ticketgen.ITicketGenerationService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partnershared.model.grid.PartnerFilter;
import com.enovax.star.cms.partnershared.model.grid.PartnerGridFilterVM;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

@Controller("PPMFLGPartnerManagementController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/partner-management/")
public class PartnerManagementController extends BasePartnerAdminController {

    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService paSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerDocumentService")
    private IPartnerDocumentService partnerDocSrv;

    @Autowired
    private PPMFLGPADocMappingRepository paDocMapRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerAdminProductService")
    IPartnerAdminProductService productService;

    @Autowired
    @Qualifier("PPMFLGIPartnerInventoryService")
    IPartnerInventoryService paInvSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerPkgService")
    IPartnerPkgService pkgService;

    @Autowired
    @Qualifier("PPMFLGIPartnerInventoryService")
    IPartnerInventoryService inventoryService;

    @Autowired
    private ITicketGenerationService ticketGenerationService;

    @Autowired
    @Qualifier("PPMFLGIPkgExcelFileTicketGenerator")
    private IPkgExcelFileTicketGenerator excelFileTicketGenerator;

    @Autowired
    @Qualifier("PPMFLGIAdminService")
    private IAdminService adminService;

    @Autowired
    @Qualifier("PPMFLGIReasonService")
    private IReasonService reasonService;

    @Autowired
    @Qualifier("PPMFLGIPartnerAxDocService")
    private IPartnerAxDocService paAxDocSrv;

    private static final String ERROR_LOGIN_MSG = "Invalid request, Access denied";


    @RequestMapping(value = "get-partner-list", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAllPartners(HttpServletRequest request, @ModelAttribute PartnerGridFilterVM gridFilterVM) {
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<String>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            if(gridFilterVM != null && gridFilterVM.getPtfilterJson() != null){
                gridFilterVM.setPartnerFilter(JsonUtil.fromJson(gridFilterVM.getPtfilterJson(), PartnerFilter.class));
            }
            Page page = paSrv.getPartnersByParams(gridFilterVM);
            List<PartnerVM> partners = paSrv.getPartnerVMs(page);
            String partnerVmsJson = JsonUtil.jsonify(partners);
            return new ResponseEntity<>(new ApiResult<String>(true, "", "", Long.valueOf(page.getTotalElements()).intValue(), partnerVmsJson), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllPartners] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-partner-details", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerVerificationDetailsVM>> apiGetPartnerDetails(@RequestParam(name="id")Integer paId){
        try {
            if (!isLoginAdminUser()) {
                return new ResponseEntity<>(new ApiResult<PartnerVerificationDetailsVM>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            PartnerVerificationDetailsVM vm = paSrv.getPartnerVerificationDetailsViewModel(StoreApiChannels.PARTNER_PORTAL_MFLG, paId);
            if (vm != null) {
                return new ResponseEntity<ApiResult<PartnerVerificationDetailsVM>>(new ApiResult<PartnerVerificationDetailsVM>(true, "", "", vm), HttpStatus.OK);
            } else {
                return new ResponseEntity<ApiResult<PartnerVerificationDetailsVM>>(new ApiResult<PartnerVerificationDetailsVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        }catch (BizValidationException ex){
            return new ResponseEntity<>(new ApiResult<PartnerVerificationDetailsVM>(false, "ERROR",ex.getMessage(), null), HttpStatus.OK);
        } catch (Exception e) {
            if(e instanceof BizValidationException){
                return new ResponseEntity<>(new ApiResult<PartnerVerificationDetailsVM>(false, "ERROR",e.getMessage(), null), HttpStatus.OK);
            }
            log.error("!!! System exception encountered [apiGetPartnerDetails] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerVerificationDetailsVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-partner-details", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ApprovingPartnerVM>> apiGetPendingApprovalPartnerDetails(@RequestParam(name="id")Integer appId){
        try {
            if(!isLoginAdminUser()){
                return new ResponseEntity<>(new ApiResult<ApprovingPartnerVM>(false, "ERROR", ERROR_LOGIN_MSG, null), HttpStatus.OK);
            }
            ApprovingPartnerVM vm = paSrv.getPendingApprovalPartnerDetailsByApprovalIdAndUserId(StoreApiChannels.PARTNER_PORTAL_MFLG, getLoginAdminId(), appId);
            if(vm != null){
                return new ResponseEntity<ApiResult<ApprovingPartnerVM>>( new ApiResult<ApprovingPartnerVM>(true, "", "", vm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ApprovingPartnerVM>>( new ApiResult<ApprovingPartnerVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPendingApprovalPartnerDetails] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ApprovingPartnerVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "view-p-details", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM>> apiViewPartnerDetails(@RequestParam(name="id")Integer paId){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM vm = paSrv.viewPartnerDetails(paId, getLoginAdminId());
            if(vm != null){
                return new ResponseEntity<ApiResult<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM>>( new ApiResult<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM>(true, "", "", vm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM>>( new ApiResult<com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiViewPartnerDetails] !!!");
            return new ResponseEntity<>(handleUncaughtException(e,com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-partner-maintenance-details", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<PartnerMaintenanceVM>> apiGetPartnerDetailsForMaintenance(@RequestParam(name="id")Integer paId){
        try {
            PartnerMaintenanceVM vm = paSrv.getPartnerMaintenanceViewModel(StoreApiChannels.PARTNER_PORTAL_MFLG, paId,getLoginAdminId());
            if(vm != null){
                return new ResponseEntity<ApiResult<PartnerMaintenanceVM>>( new ApiResult<PartnerMaintenanceVM>(true, "", "", vm), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<PartnerMaintenanceVM>>( new ApiResult<PartnerMaintenanceVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPartnerDetailsForMaintenance] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, PartnerMaintenanceVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="save-partner-details", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apisavePartnerProfile(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            String errorKey =  paSrv.submitEditPa(paVmJson, getLoginAdminId());
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apisavePartnerProfile] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="add-excl", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiaddExclProd(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            String errorKey =  paSrv.addExclProd(paVmJson);
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiaddExclProd] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-partner-history", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiGetPartnerHistory(@RequestParam(name="id")Integer paId){
        try {
            final com.enovax.star.cms.commons.model.api.ApiResult<String> result = paSrv.getPartnerHist(paId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPartnerHistory] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-previous-partner-history", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiGetPreviousPartnerHistory(@RequestParam(name="appId")Integer appId){
        try {
            final com.enovax.star.cms.commons.model.api.ApiResult<String> result = paSrv.getPreviousPartnerHist(appId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPreviousPartnerHistory] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-reasons", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiGetReasons(@RequestParam(name="logId")Integer logId){
        try {
            List<ReasonVM> reasonVMs = reasonService.getReasonsByLogId(logId);
            String reasonVmJson = JsonUtil.jsonify(reasonVMs);
            if(reasonVMs != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", reasonVmJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.RemarksNotFound), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetReasons] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-subusers", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<SubuserVM>> apiGetSubusers(@RequestParam(name="id")Integer paId){
        try {
            final com.enovax.star.cms.commons.model.api.ApiResult<SubuserVM> result = paSrv.getSubuser(paId);
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetSubusers] !!!");
            return new ResponseEntity(handleUncaughtException(e, SubuserVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-subuser", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiSaveSubUser(@RequestParam(name="id")Integer paId,
                                                                                                  @RequestParam(name="isNew") boolean isNew,
                                                                                                  @RequestParam(name="userId")Integer userId,
                                                                                                  @RequestParam(name="userName") String userName,
                                                                                                  @RequestParam(name="name") String name,
                                                                                                  @RequestParam(name="email") String email,
                                                                                                  @RequestParam(name="title") String title,
                                                                                                  @RequestParam(name="status") String status,
                                                                                                  @RequestParam(name="") String access){
        try {
            final com.enovax.star.cms.commons.model.api.ApiResult<String> result= paSrv.saveSubuser(paId,isNew,userId,userName,name,email,title,status,access,getLoginAdminId());
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSubUser] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "reset-password", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<ResultVM>> apiResetPassword(@RequestParam(name="userId")Integer userId){
        try {
            final ResultVM result = paSrv.resetPassword(userId,getLoginAdminId());
            if(result != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", result), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.ConfigNotFound), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiResetPassword] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-inventory", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<InventoryResult>> apiGetInventory(@RequestParam(name="id")Integer paId,
                                                             @RequestParam(value="startDateStr", required = false) String startDateStr,
                                                             @RequestParam(value="endDateStr", required = false) String endDateStr,
                                                             @RequestParam(value="productName", required = false) String productName,
                                                             @RequestParam(value="availableOnly", required = false) String availableOnly,
                                                             @RequestParam("take") int take,
                                                             @RequestParam("skip") int skip,
                                                             @RequestParam("page") int page,
                                                             @RequestParam("pageSize") int pageSize,
                                                             @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                             @RequestParam(value = "sort[0][dir]", required = false) String sortDirection){
        try {
            PPMFLGPartner pa = paSrv.getPartnerById(paId);
            int adminId = pa.getMainAccount().getId();
            List<InventoryTransactionItemVM> transItemList = paInvSrv.getInventoryTransactionsItem(adminId, startDateStr, endDateStr, availableOnly, productName, sortField, sortDirection, page, pageSize);
            //TODO
            /*Integer tktepweeks = sysService.getObjByKey(
                    SysParamConst.TicketExpiringAlertPeriod.toString(), Integer.class,
                    true);
            List<InventoryTransactionItemVM> transItemVms = ViewModelUtil.convertInventoryItemVM(transItemList,tktepweeks);*/

            long total = paInvSrv.getInventoryTransactionsItemSize(adminId, startDateStr, endDateStr, availableOnly, productName);

            final InventoryResult result = new InventoryResult();
            result.setTotal(total);
            result.setTransItemVms(transItemList);

            if(result != null){
                return new ResponseEntity<ApiResult<InventoryResult>>( new ApiResult<InventoryResult>(true, "", "", result), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<InventoryResult>>( new ApiResult<InventoryResult>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetInventory] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, InventoryResult.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-trans-details", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<InventoryTransactionVM>> apiGetTransDetails(@RequestParam(name="transId")Integer transId){
        try {
            InventoryTransactionVM result = inventoryService.getTransById(transId,"Purchase");
            if(result != null){
                return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<InventoryTransactionVM>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result), HttpStatus.OK);
            }else{
                return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<InventoryTransactionVM>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTransDetails] !!!");
            return new ResponseEntity(handleUncaughtException(e, InventoryTransactionVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "regenerate-receipt", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiRegenerateReceipt(@RequestParam(name="transId")Integer transId){
        try {
            String result = inventoryService.generateReceiptAndEmail(transId);
            return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegenerateReceipt] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "refund-trans", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiRefundTrans(@RequestParam(name="transId")Integer transId){
        try {
            final com.enovax.star.cms.commons.model.api.ApiResult<String> result = inventoryService.refundTrans(transId,getLoginAdminId());
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRefundTrans] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-package", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiGetPackage(@RequestParam(name="id")Integer paId,
                                                                      @RequestParam(value="startDateStr", required = false) String startDateStr,
                                                                      @RequestParam(value="endDateStr", required = false) String endDateStr,
                                                                      @RequestParam(value="status", required = false) String status,
                                                                      @RequestParam(value="ticketMedia", required = false) String ticketMedia,
                                                                      @RequestParam(value="pkgName", required = false) String pkgName,
                                                                      @RequestParam("take") int take,
                                                                      @RequestParam("skip") int skip,
                                                                      @RequestParam("page") int page,
                                                                      @RequestParam("pageSize") int pageSize,
                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection){
        log.info("Entered apiSearchPackage...");
        try {
            PPMFLGPartner pa = paSrv.getPartnerById(paId);
            int adminId = pa.getMainAccount().getId();


            ResultVM result = pkgService.getPkgsByPage(adminId, startDateStr, endDateStr, status, ticketMedia, pkgName, sortField, sortDirection, page, pageSize);

            if(result != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", result), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-package", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<MixMatchPkgVM>> apiGetPackage(@RequestParam(name="pkgId")Integer pkgId) {
        log.info("Entered apiGetPackage...");
        try {
            MixMatchPkgVM pkgVM = pkgService.getPkgByID(pkgId);
            return new ResponseEntity<>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", pkgVM), HttpStatus.OK);
        }catch (Exception e) {
            log.error("!!! System exception encountered [apiGetPackage] !!!");
            return new ResponseEntity(handleUncaughtException(e, MixMatchPkgVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "download-ticket", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiDownloadTicket(@RequestParam(name="pkgId")Integer pkgId, HttpServletResponse response) {
        log.info("Entered apiDownloadTicket...");
        try {

            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(pkgService.getETickets(pkgId));

            final byte[] outputBytes = ticketGenerationService.generateTickets(StoreApiChannels.PARTNER_PORTAL_MFLG, ticketDataCompiled);

            final String fileName = "etickets.pdf";
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            //response.setHeader("X-Frame-Options", "SAMEORIGIN");
            OutputStream output = response.getOutputStream();
            output.write(outputBytes);
            output.flush();

            return new ResponseEntity<>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiDownloadTicket] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "download-excel-file", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiDownloadExcelFile(@RequestParam(name="pkgId")Integer pkgId, HttpServletResponse response) {
        log.info("Entered apiDownloadTicket...");
        try {
            List<ETicketData> ticketDataList = pkgService.getExcelTickets(pkgId);
            MixMatchPkgVM mixMatchPkgVM = pkgService.getPkgByID(pkgId);

            Workbook wb = excelFileTicketGenerator.generatePkgExcel(mixMatchPkgVM, ticketDataList);
            final String fileName = "partner-excelTicket-pkg-" + pkgId + ".xls";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();

            return new ResponseEntity<>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiDownloadExcelFile] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "email-pincode", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiEmailPincode(@RequestParam(name="pkgId")Integer pkgId){
        try {
            String result = pkgService.sendPincodeEmail(pkgId);
            return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiEmailPincode] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "email-eticket", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiEmailEticket(@RequestParam(name="pkgId")Integer pkgId){
        try {
            String result = pkgService.sendEticketEmail(pkgId);
            return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiEmailEticket] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "email-excel", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiEmailExcel(@RequestParam(name="pkgId")Integer pkgId){
        try {
            String result = pkgService.sendExcelEmail(pkgId);
            return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", result), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiEmailExcel] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "deactivate-pincode", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiDeactivatePincode(@RequestParam(name="pkgId")Integer pkgId){
        try {
            com.enovax.star.cms.commons.model.api.ApiResult<String> result = pkgService.deactivatePkg(pkgId,getLoginAdminId());
            return new ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>>(result,HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiDeactivatePincode] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "chk-redemption-status", method = {RequestMethod.POST})
    public ResponseEntity<com.enovax.star.cms.commons.model.api.ApiResult<String>> apiCheckRedemptionStatus(@RequestParam(name="pkgId")Integer pkgId){
        try {
            ResultVM result = pkgService.checkRdmStatus(pkgId);
            if(result.isStatus()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(new ApiResult<>(false, "", result.getMessage(), ""), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckRedemptionStatus] !!!");
            return new ResponseEntity(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="get-login-user-roles", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiGetUserRoles(){
        try {
            String responseText = "";
            List<String> roles = adminService.getAdminUserAssignedRights(getLoginAdminId());

            if(roles != null && roles.size() > 0){
                responseText = JsonUtil.jsonify(roles);
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", "", 0, responseText), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetUserRoles] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="get-sales-admin", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiGetSalesAdminByCountry(@RequestParam(name = "ctyId") String ctyId){
        try {
            String responseText = "";
            List<AdminAccountVM> vm = paSrv.getAssignedSalesAdminByCountryId(ctyId);
            if(vm != null && vm.size() > 0){
                responseText = JsonUtil.jsonify(vm);
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", "", 0, responseText), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetSalesAdminByCountry] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="update/email", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiChangeEmail(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM paVm = JsonUtil.fromJson(paVmJson, com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class);
            //TODO Check if ChangeLog has admin Id or Name
            ResultVM resultVM = paSrv.changePartnerEmail(paVm.getId(),getLoginAdminId(), paVm.getEmail());
            return new ResponseEntity<>(new ApiResult<>(resultVM.isStatus(), null, resultVM.getMessage(), null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiChangeEmail] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"fileUpload"},
            method = {RequestMethod.POST},
            headers = {"content-type=multipart/form-data"}
    )
    public ResponseEntity<ApiResult<PartnerDocumentVM>> apiFileUpload(HttpServletRequest request,
                                                           @RequestParam List<MultipartFile> files) {
        try {

                int partnerId  = Integer.parseInt(request.getParameter("id"));
                String paFileType = request.getParameter("pafileType");
                String fileName = request.getParameter("fileName");
                MultipartForm mpf = (MultipartForm)request.getAttribute("multipartform");
                Document doc = mpf.getDocument("uploadfile");
                if(!partnerDocSrv.isValidPartnerDocument(doc)){
                    return new ResponseEntity(new ApiResult(false, "ERROR", "MAXIMUM_PARTNER_DOC_SIZE", (Object)null),HttpStatus.OK);
                }
                PartnerDocumentVM paDocVM = paSrv.savePartnerDocument(partnerId, doc, paFileType, fileName, getLoginAdminId());
            if(paDocVM != null){
                return new ResponseEntity<ApiResult<PartnerDocumentVM>>( new ApiResult<PartnerDocumentVM>(true, "", "", paDocVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<PartnerDocumentVM>>( new ApiResult<PartnerDocumentVM>(false, "ERROR", "PARTNER-NOT-FOUND", null), HttpStatus.OK);
            }
        } catch (Exception var9) {
            var9.printStackTrace();
            this.log.error("!!! System exception encountered [apiFileUpload] !!!");
            return new ResponseEntity(new ApiResult(false, "ERROR", var9.getMessage(), (Object)null), HttpStatus.OK);
        }
    }



    @RequestMapping(value="update/status", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiChangeStatus(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM paVm = JsonUtil.fromJson(paVmJson, com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class);
            //TODO Check if ChangeLog has admin Id or Name
            ResultVM resultVM = paSrv.changePartnerStatus(paVm.getId(),getLoginAdminId(), paVm.getStatus());
            return new ResponseEntity<>(new ApiResult<>(resultVM.isStatus(), null, resultVM.getMessage(), null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiChangeStatus] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="update/UEN", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiChangeUEN(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM paVm = JsonUtil.fromJson(paVmJson, com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class);
            //TODO Check if ChangeLog has admin Id or Name
            ResultVM resultVM = paSrv.changePaUEN(paVm.getId(),getLoginAdminId(), paVm.getUen());
            return new ResponseEntity<>(new ApiResult<>(resultVM.isStatus(), null, resultVM.getMessage(), null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiChangeUEN] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="update/license", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiChangeLicense(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM paVm = JsonUtil.fromJson(paVmJson, com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class);
            //TODO Check if ChangeLog has admin Id or Name
            ResultVM resultVM = paSrv.changePaLicense(paVm.getId(),getLoginAdminId(), paVm.getLicenseNum());
            return new ResponseEntity<>(new ApiResult<>(resultVM.isStatus(), null, resultVM.getMessage(), null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiChangeLicense] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="update/OrgType", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiChangeOrgType(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM paVm = JsonUtil.fromJson(paVmJson, com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM.class);
            paSrv.changeOrgType(paVm.getId(),getLoginAdminId(), paVm.getOrgTypeCode());
            return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiChangeOrgType] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="verify/resubmit", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiRevertPartnerProfileResubmit(@RequestParam(name = "id") Integer id, @RequestParam(name = "reasonVmJson")String reasonVmJson){
        try {
            String errorKey =  paSrv.revertPartnerProfileResubmit(id, reasonVmJson, getLoginAdminId());
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRevertPartnerProfileResubmit] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="verify/reject", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiRevertPartnerProfileReject(@RequestParam(name = "id") Integer id, @RequestParam(name = "reasonVmJson")String reasonVmJson){
        try {
            String errorKey =  paSrv.apiRevertPartnerProfileReject(id, reasonVmJson, getLoginAdminId());
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRevertPartnerProfileReject] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value="verify/submit", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apisubmitPartnerProfile(@RequestParam(name = "paVmJson")String paVmJson){
        try {
            String errorKey =  paSrv.apiSubmitPartnerProfile(paVmJson, getLoginAdminId());
            if(errorKey != null && errorKey.trim().length() > 0){
                return new ResponseEntity<>(new ApiResult<>(false, "ERROR", errorKey, null), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(true, null, null, null), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apisubmitPartnerProfile] !!!");
            return new ResponseEntity<>(new ApiResult<>(false, "ERROR", e.getMessage(), null), HttpStatus.OK);
        }
    }

    @RequestMapping(value="attachment/downloadFile")
    public void apiDownloadPartnerFileFromFtp(HttpServletRequest request, HttpServletResponse response, @RequestParam(name="id")Integer id){
        try{
            PPMFLGPADocMapping doc = paDocMapRepo.findByDocId(id);
            PPMFLGPartner partner = paSrv.getPartnerById(doc.getPartnerId());
            paAxDocSrv.downloadPartnerDocuments(partner, paDocMapRepo.findByPartnerId(partner.getId()));
            apiDownloadPartnerFile(request,response, id);
        }catch (Exception ex){
            log.error("Downloading partner documents from AX FTP server is failed.");
        }

    }

    @RequestMapping(value="attachment/download")
    public void apiDownloadPartnerFile(HttpServletRequest request, HttpServletResponse response, @RequestParam(name="id")Integer id){
        if(id != null && id.intValue() > 0){
            PPMFLGPADocument doc = partnerDocSrv.getPartnerDocumentById(id);
            if(doc != null){
                String filePath = doc.getFilePath();
                if(filePath != null && filePath.trim().length() > 0){
                    String docRootDir = sysParamSrv.getPartnerDocumentRootDir();
                    String fileFullPath = docRootDir + File.separator + filePath;
                    File f = new File(fileFullPath);
                    if(f.exists() && f.canRead()){
                        f = null;
                    }
                    response.setContentType("application/octet-stream");
                    try {
                        response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(doc.getFileName(),"UTF-8"));
                    } catch (Exception e) {
                        response.setHeader("Content-Disposition","attachment;filename=" + doc.getFileName().replace(" ","_"));
                    }
                    InputStream is = null;
                    OutputStream os = null;
                    try{
                        is = new FileInputStream(fileFullPath);
                        int read=0;
                        byte[] bytes = new byte[500];
                        os = response.getOutputStream();
                        while((read = is.read(bytes))!= -1){
                            os.write(bytes, 0, read);
                        }
                        os.flush();
                    }catch(Exception ex){
                        log.error("!!! System exception encountered [apiDownloadPartnerFile] !!!");
                        ex.printStackTrace();
                    }finally{
                        if(is != null){
                            try{
                                is.close();
                                is = null;
                            }catch(Exception ex){
                            }
                        }
                        if(os != null){
                            os = null;
                        }
                    }
                }
            }
        }
    }

}
