package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.ExceptionRptFilterVM;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by lavanya on 10/11/16.
 */
public interface IExceptionRptGenerator {
    public Workbook generateExcel(ExceptionRptFilterVM fliter);
}
