package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.ExceptionRptFilterVM;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by lavanya on 10/11/16.
 */
public interface IExceptionRptGenerator {
    public Workbook generateExcel(ExceptionRptFilterVM fliter);
}
