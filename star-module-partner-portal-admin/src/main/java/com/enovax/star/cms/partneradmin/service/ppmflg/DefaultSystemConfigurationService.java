package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppmflg.ApproverCategory;
import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.constant.ppmflg.SysParamConst;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.system.SysParamVM;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.jcr.util.PropertyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import java.util.*;

/**
 * Created by lavanya on 28/9/16.
 */
@Service("PPMFLGISystemConfigurationService")
public class DefaultSystemConfigurationService implements ISystemConfigurationService{
    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    ISystemParamService systemParamService;
    @Autowired
    @Qualifier("PPMFLGIApproverService")
    IApproverService approverService;
    @Autowired
    ISystemParamRepository systemParamRepo;
    @Autowired
    @Qualifier("PPMFLGIApprovalLogService")
    IApprovalLogService approvalLogService;
    @Autowired
    @Qualifier("PPMFLGIRevalidationFeeService")
    IRevalidationFeeService revalidationFeeService;
    @Autowired
    StarTemplatingFunctions starTemplatingFunctions;
    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeSharedService")
    IWingsOfTimeSharedService wingsOfTimeSharedService;
    @Autowired
    IProductService productService;

    @Override
    public GeneralConfigVM getGeneralConfigurationViewModel() {
        Map<String, Object> sysMap = new HashMap<>();

        SysParamVM paramVM1 = systemParamService.getMaxNoOfSubUsersVM();
        sysMap.put(SysParamConst.NumSubAccount.toString(), paramVM1);

        SysParamVM paramVM2 = systemParamService.getMinimumNoOfTicketsPurchasePerTxnVM();
        sysMap.put(SysParamConst.PurchaseMinQtyPerTxn.toString(), paramVM2);

        SysParamVM paramVM3 = systemParamService.getMaxLimitPerTransactionVM();
        sysMap.put(SysParamConst.PurchaseMaxAmountPerTxn.toString(), paramVM3);

        SysParamVM paramVM4 = systemParamService.getMaxTotalDailyTransactionVM();
        sysMap.put(SysParamConst.PurchaseMaxDailyTxnLimit.toString(), paramVM4);

        SysParamVM paramVM5 = systemParamService.getMaxTypeOfItemsinPackageVM();
        sysMap.put(SysParamConst.PkgAllowedItemsCodes.toString(), paramVM5);

        ApproverVM approverVm = approverService.getCurrentApprover();
        List<ApproverVM> candidatesList = approverService.getCandidates();
        GeneralConfigVM generalConfigVM = new GeneralConfigVM(sysMap,approverVm,candidatesList);
        return  generalConfigVM;
    }

    @Override
    public Map<String, Object> getTicketConfigMap() {
        Map<String, Object> sysMap = new HashMap<>();

        SysParamVM paramVM1 = systemParamService.getTicketExpiringAlertPeriodVM();
        sysMap.put(SysParamConst.TicketExpiringAlertPeriod.toString(), paramVM1);


        SysParamVM paramVM2 = systemParamService.getTicketRevalidationPeriodVM();
        sysMap.put(SysParamConst.TicketRevalidatePeriod.toString(), paramVM2);


        SysParamVM paramVM3 = systemParamService.getTicketAllowRevalidatePeriodVM();
        sysMap.put(SysParamConst.TicketAllowRevalidatePeriod.toString(), paramVM3);

        return  sysMap;
    }

    @Override
    public Map<String, Object> getWotConfigMap() {
        Map<String, Object> sysMap = new HashMap<>();

        SysParamVM paramVM1 = systemParamService.getPartnerWoTAdvancedDaysReleaseBackendReservedTicketsVM();
        sysMap.put(SysParamConst.PartnerWoTAdvancedDaysReleaseBackendReservedTickets.toString(), paramVM1);

        SysParamVM paramVM2 = systemParamService.getWotAdminReservationPeriodLimitVM();
        sysMap.put(SysParamConst.WotAdminReservationPeriodLimit.toString(), paramVM2);

        SysParamVM paramVM3 = systemParamService.getPartnerTicketonHoldTimeVM();
        sysMap.put(SysParamConst.PartnerTicketonHoldTime.toString(), paramVM3);

        SysParamVM paramVM4 = systemParamService.getPartnerWoTReservationCapVM();
        sysMap.put(SysParamConst.PartnerWoTReservationCap.toString(), paramVM4);

        SysParamVM paramVM5 = systemParamService.getPartnerOnlinePaymentTimeVM();
        sysMap.put(SysParamConst.PartnerOnlinePaymentTime.toString(), paramVM5);

        SysParamVM paramVM6 = systemParamService.getWoTeTicketEnabledVM();
        sysMap.put(SysParamConst.WoTeTicketEnabled.toString(), paramVM6);

        SysParamVM paramVM7 = systemParamService.getWoTAdminReservationCapVM();
        sysMap.put(SysParamConst.WoTAdminReservationCap.toString(), paramVM7);

        SysParamVM paramVM8 = systemParamService.getPartnerWoTAdvancedHoursUnredeemedReservationCancelReminderVM();
        sysMap.put(SysParamConst.WoTAdvancedHoursUnredeemedReservationCancelReminder.toString(), paramVM8);

        SysParamVM paramVM9 = systemParamService.getWingsOfTimeShowCutoffInMinutesVM();
        sysMap.put(SysParamConst.WotReservationCutoffTime.toString(), paramVM9);

        return  sysMap;
    }

    @Override
    public ResultVM listBackupApprovers(String orderBy, String orderWith, Integer page, Integer pagesize) {
        ResultVM res = new ResultVM();
        List<ApproverVM> appVms = approverService.getGeneralBackupApproverbyPage(orderBy,orderWith,page,pagesize);
        int total = approverService.getGeneralBackupApproverSize();
        res.setViewModel(appVms);
        res.setTotal(total);
        return res;
    }

    @Override
    public ResultVM removeBackupApprovers(String backupIds, String adminId) {
        ResultVM resultVM = new ResultVM();
        final List<Integer> rcmdIds = new ArrayList<>();
        for (String s : backupIds.split(",")) {
            rcmdIds.add(Integer.parseInt(s));
        }
        approverService.removeBackupApprovers(rcmdIds,adminId);
        resultVM.setStatus(true);
        return resultVM;
    }

    @Override
    public ResultVM saveParam(String paramKey, String paramValue, String paramLabel, String adminId) {
        ResultVM resultVM = new ResultVM();
        SysParamVM sysParamVM = systemParamRepo.getSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel,paramKey);
        try {

            if (sysParamVM.isRequireApprove()) {
                Boolean applogs = approvalLogService.hasPendingRequest(sysParamVM.getName(), ApproverCategory.SystemParam.code);
                if (!applogs) {
                    approvalLogService.logSystemParamRequest(sysParamVM, paramLabel, paramValue, adminId);
                    resultVM.setStatus(true);
                   resultVM.setMessage(ApiErrorCodes.SysParamUpdateApproveSuccess.message);
                } else {
                    resultVM.setStatus(false);
                    resultVM.setMessage(ApiErrorCodes.SysParamUpdateErrorPending.message);
                }
            } else {
                systemParamRepo.saveSystemParamByKey(PartnerPortalConst.Partner_Portal_Channel, paramKey, paramValue);
                resultVM.setStatus(true);
                resultVM.setMessage(ApiErrorCodes.SysParamUpdateNoApproveSuccess.message);
            }
        }catch(Exception e) {
            resultVM.setStatus(false);
            resultVM.setMessage(e.getMessage());
            e.printStackTrace();
            return resultVM;
        }
        return resultVM;
    }

    @Override
    public ResultVM getRevalItemList(StoreApiChannels channel) {
        ResultVM resultVm = new ResultVM();
        List<RevalFeeItemVM> revalFeeItemVMs = revalidationFeeService.populdateActiveRevalidationFeeItemVMs(channel, null);
        resultVm.setViewModel(revalFeeItemVMs);
        resultVm.setTotal(revalFeeItemVMs.size());
        return resultVm;
    }

    @Override
    public ResultVM getAxProductsList(String productName, boolean filterEventProduct) {
        ResultVM resultVm = new ResultVM();
        List<Node> axNodes = null;
        if(productName!= null && !productName.equals(""))
           axNodes = starTemplatingFunctions.getAXProductsByProductName(PartnerPortalConst.Partner_Portal_Channel,productName);
        else
           axNodes = starTemplatingFunctions.getAllAXProducts(PartnerPortalConst.Partner_Portal_Channel);
        List<RevalFeeItemVM> activeProductVMs;
        if(filterEventProduct) {
            activeProductVMs = convertNodeToVMAndFilterEventProducts(axNodes);
        }else {
            activeProductVMs = convertNodeToVM(axNodes);
        }
        resultVm.setViewModel(activeProductVMs);
        resultVm.setTotal(activeProductVMs.size());
        return resultVm;
    }


    private List<RevalFeeItemVM> convertNodeToVMAndFilterEventProducts(List<Node> axNodes) {
        List<RevalFeeItemVM> activeProducts  = new ArrayList<>();
        List<String> productIds=new ArrayList<>();
        for(Node node : axNodes) {
            RevalFeeItemVM activeProduct = new RevalFeeItemVM();
            activeProduct.setProductId(PropertyUtil.getLong(node, "productListingId"));
            activeProduct.setItemId(PropertyUtil.getLong(node, "displayProductNumber"));
            activeProduct.setPriceStr(PropertyUtil.getString(node, "productPrice"));
            activeProduct.setDescription(PropertyUtil.getString(node, "itemName"));
            activeProduct.setStatus(PropertyUtil.getString(node, "status"));
            activeProducts.add(activeProduct);
            productIds.add(activeProduct.getProductId().toString());
        }
        try {
            ApiResult<List<ProductExtViewModel>> result = productService.getDataForProducts(StoreApiChannels.PARTNER_PORTAL_MFLG, productIds);
            List<ProductExtViewModel> productExtViewModels = result.getData();
            for (ProductExtViewModel prod:productExtViewModels) {
                if(!prod.isCapacity() || prod.isOpenDate()) {
                    productIds.remove(prod.getProductId());
                }
            }
            Iterator<RevalFeeItemVM> iter = activeProducts.iterator();
            while(iter.hasNext()) {
                RevalFeeItemVM revalFeeItem = iter.next();
                if(!productIds.contains(revalFeeItem.getProductId().toString())) {
                    iter.remove();
                }
            }
        }catch(Exception e) {

        }
        return activeProducts;
    }

    private List<RevalFeeItemVM> convertNodeToVM(List<Node> axNodes) {
        List<RevalFeeItemVM> activeProducts  = new ArrayList<>();

        for(Node node : axNodes) {
            RevalFeeItemVM activeProduct = new RevalFeeItemVM();
            activeProduct.setProductId(PropertyUtil.getLong(node, "productListingId"));
            activeProduct.setItemId(PropertyUtil.getLong(node, "displayProductNumber"));
            activeProduct.setPriceStr(PropertyUtil.getString(node, "productPrice"));
            activeProduct.setDescription(PropertyUtil.getString(node, "itemName"));
            activeProduct.setStatus(PropertyUtil.getString(node, "status"));
            activeProducts.add(activeProduct);
        }

        return activeProducts;
    }

    @Override
    public ResultVM getWotProducts(StoreApiChannels channel) {
        ResultVM resultVm = new ResultVM();
        List<WOTProductVM> wotProductVMs = wingsOfTimeSharedService.getWOTProductsVM(channel);
        resultVm.setViewModel(wotProductVMs);
        resultVm.setTotal(wotProductVMs.size());
        return resultVm;
    }

}
