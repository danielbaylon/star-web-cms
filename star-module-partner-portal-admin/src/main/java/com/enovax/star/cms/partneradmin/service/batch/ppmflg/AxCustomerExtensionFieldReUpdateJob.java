package com.enovax.star.cms.partneradmin.service.batch.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerExtRepository;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerService;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by houtao on 26/9/16.
 */
@Service("PPMFLGAxCustomerExtensionFieldReUpdateJob")
public class AxCustomerExtensionFieldReUpdateJob {

    private static Logger log = LoggerFactory.getLogger(AxCustomerExtensionFieldReUpdateJob.class);

    @Autowired
    private PPMFLGPartnerExtRepository paExtRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService paSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppmflg.auto.reupdate.customer.extensions.job}")
    public void autoReUpdateCustomerExtensionsJob(){
        boolean isJobEnabled = sysParamSrv.isPPMFLGReUpdateCustomerExtensionsJobEnabled();
        if(!isJobEnabled){
            log.warn("Job[autoReUpdateCustomerExtensionsJob] is not enabled.");
            return;
        }
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    List<PPMFLGPartnerExt> exts = paExtRepo.getAllUpdateFailedCustomer();
                    if(exts != null && exts.size() > 0){
                        for(PPMFLGPartnerExt e : exts){
                            List<PPMFLGPartnerExt> times = paExtRepo.findByPartnerExtensionUpdateFailedTimes(e.getPartnerId());
                            PPMFLGPartnerExt time = null;
                            if(times != null && times.size() > 0){
                                time = times.get(0);
                                if(time != null && Integer.parseInt(time.getAttrValue()) > 3){ /* ignore updated time > 3*/
                                    continue;
                                }
                            }
                            try{
                                paSrv.apiReUpdatePartnerDetailsAfterRegistration(e.getPartnerId());
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    }
}
