package com.enovax.star.cms.partneradmin.controller.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ProductTierVM;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partneradmin.model.SavedProductTierResponseVM;
import com.enovax.star.cms.partnershared.constant.ppslm.ProductLevels;
import com.enovax.star.cms.partnershared.model.grid.PartnerGridFilterVM;
import com.enovax.star.cms.partnershared.model.grid.ProdFilter;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerAdminProductService;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerService;
import com.enovax.star.cms.partnershared.service.ppslm.IProductTierService;
import info.magnolia.context.MgnlContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jennylynsze on 5/12/16.
 */
@Controller
@RequestMapping("/tier/")
public class TierManagementController extends BasePartnerAdminController {

    @Autowired
    IProductTierService tierService;

    @Autowired
    IPartnerAdminProductService productService;

    @Autowired
    IPartnerService partnerService;

    @RequestMapping(value = "get-all-tiers", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<ProductTierVM>>> apiGetAllTiers() {
        log.info("Entered apiGetAllTiers...");
        try {
            List<ProductTierVM> tierVms = tierService.getAllTierVms(PartnerPortalConst.Partner_Portal_Channel);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", tierVms), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllTiers] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ProductTierVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-tier/{id}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiGetTiers(@PathVariable("id") String tierId) {
        log.info("Entered apiGetTiers...");
        try {
            ProductTierVM tierVm = tierService.getTierVmById(getCurrentChannel(), tierId);
            String tierVmJson = JsonUtil.jsonify(tierVm);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", tierVmJson), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTiers] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-all-partners", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiAddAllPartners(@RequestBody PartnerGridFilterVM gridFilterVM) {
        log.info("Entered apiAddAllPartners...");
        try {

            gridFilterVM = initFilterChannel(gridFilterVM);

            ApiResult<List<PartnerVM>> result = partnerService.getPartnersByPage(gridFilterVM);

            List<PartnerVM> partnerVMs = result.getData();

            String partnerVmsJson = JsonUtil.jsonify(partnerVMs);

            return new ResponseEntity<>(new ApiResult<>(true, "", "",  result.getTotal(), partnerVmsJson), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddAllPartners] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-all-products", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiAddAllProducts() {
        log.info("Entered apiAddAllProducts...");
        try {

            ProdFilter prodFilter = new ProdFilter();
            prodFilter.setChannel(PartnerPortalConst.Partner_Portal_Channel);
            prodFilter.setProductLevel(ProductLevels.Tiered.toString());

            ProductGridFilterVM gridFilter = new ProductGridFilterVM();
            gridFilter.setProdFilter(prodFilter);

            ApiResult<List<PartnerProductVM>> result = productService.getProdVmsByPage(gridFilter,false);

            List<PartnerProductVM> productVMs = result.getData();

            String productVmsJson = JsonUtil.jsonify(productVMs);

            return new ResponseEntity<>(new ApiResult<>(true, "", "",  result.getTotal(), productVmsJson), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddAllProducts] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    /*@RequestMapping(value = "remove-tier", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRemoveTiers(@RequestBody String tierIdsParam) {
        log.info("Entered apiRemoveTiers...");
        try {
            String userNm = MgnlContext.getUser().getName();
            String tierIds = "";
            if(tierIdsParam.indexOf("=") > 0) {
                tierIds = tierIdsParam.substring(tierIdsParam.indexOf("=") + 1);
            }else {
                tierIds = tierIdsParam;
            }

            final List<Integer> rcmdIds = new ArrayList<>();
            for (String s : tierIds.split(",")) {
                rcmdIds.add(Integer.parseInt(s));
            }
            tierService.removeTier(getCurrentChannel(), rcmdIds, userNm);

            List<ProductTierVM> tierVms = tierService.getAllTierVms();
            String tierVmsJson = JsonUtil.jsonify(tierVms);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", tierVmsJson), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveTiers] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }*/

    @RequestMapping(value = "get-all-products", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAllProducts(@ModelAttribute ProductGridFilterVM productGridFilterVM) {
        log.info("Entered apiGetAllProducts...");
        try {

            productGridFilterVM = initFilterChannel(productGridFilterVM);

            ApiResult<List<PartnerProductVM>> productList = productService.getProdVmsByPage(productGridFilterVM,false);

            ApiResult<String> result = new ApiResult<>();
            result.setData(JsonUtil.jsonify(productList.getData()));
            result.setTotal(productList.getTotal());
            result.setSuccess(true);
            result.setMessage("");

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllProducts] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-all-partners", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiGetAllPartners(@ModelAttribute PartnerGridFilterVM partnerGridFilterVM) {
        log.info("Entered apiGetAllPartners...");
        try {

            partnerGridFilterVM = initFilterChannel(partnerGridFilterVM);

            ApiResult<List<PartnerVM>> partnerList = partnerService.getPartnersByPage(partnerGridFilterVM);

            ApiResult<String> result = new ApiResult<>();
            result.setData(JsonUtil.jsonify(partnerList.getData()));
            result.setTotal(partnerList.getTotal());
            result.setSuccess(true);
            result.setMessage("");

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllPartners] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-tier", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<SavedProductTierResponseVM>> apiSaveProductTier(@RequestBody ProductTierVM productTierInput){
        log.info("Entered apiGetAllPartners...");
        try {

            String userNm = MgnlContext.getUser().getName();
            ProductTierVM tierVm = tierService.saveTier(PartnerPortalConst.Partner_Portal_Channel,productTierInput, userNm);

            List<ProductTierVM> tierVms = tierService.getAllTierVms(PartnerPortalConst.Partner_Portal_Channel);
            String tierVmJson = JsonUtil.jsonify(tierVm);
            String tierVmsJson = JsonUtil.jsonify(tierVms);

            SavedProductTierResponseVM result = new SavedProductTierResponseVM();
            result.setTierVmJson(tierVmJson);
            result.setTierVmsJson(tierVmsJson);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllPartners] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, SavedProductTierResponseVM.class, ""), HttpStatus.OK);
        }
    }
}
