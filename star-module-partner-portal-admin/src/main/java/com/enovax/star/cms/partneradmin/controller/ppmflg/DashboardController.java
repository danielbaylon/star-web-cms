package com.enovax.star.cms.partneradmin.controller.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.RequestApprovalVM;
import com.enovax.star.cms.partnershared.service.ppmflg.IApprovalLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller("PPMFLGDashboardController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/dashboard/")
public class DashboardController extends BasePartnerAdminController {

    @Autowired
    @Qualifier("PPMFLGIApprovalLogService")
    private IApprovalLogService approvalLogSrv;

    @RequestMapping(value = "get-pending-approval-requests", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<RequestApprovalVM>> apiGetPendingApprovalRequests(HttpServletRequest request){
        try{
            RequestApprovalVM vm = approvalLogSrv.populatePendingApprovalRequestVM(getLoginAdminId());
            return new ResponseEntity<>(new ApiResult<>(true, "", "", vm), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddAllPartners] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, RequestApprovalVM.class, ""), HttpStatus.OK);
        }
    }

}
