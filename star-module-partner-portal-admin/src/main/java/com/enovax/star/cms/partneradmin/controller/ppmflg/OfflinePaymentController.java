package com.enovax.star.cms.partneradmin.controller.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentRequestVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.OfflinePaymentResultVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.PagedData;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.service.ppmflg.IOfflinePaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 30/9/16.
 */
@Controller("PPMFLGOfflinePaymentController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/offlinePay/")
public class OfflinePaymentController extends BasePartnerAdminController {

    @Autowired
    @Qualifier("PPMFLGIOfflinePaymentService")
    private IOfflinePaymentService offlinePaySrv;


    @RequestMapping(value = "list", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchReservation(HttpServletRequest request,
                                                                    @RequestParam(value="isPartnerOnly", required = false) String isPartnerOnly,
                                                                    @RequestParam(value="paId", required = false) String paId,
                                                                    @RequestParam(value="startDtStr", required = false) String startDateStr,
                                                                    @RequestParam(value="endDtStr", required = false) String endDateStr,
                                                                    @RequestParam(value="status", required = false) String status,
                                                                    @RequestParam(value="username", required = false) String username,
                                                                    @RequestParam(value="orgName", required = false) String orgName,
                                                                    @RequestParam(value="receiptNum", required = false) String receiptNum,
                                                                    @RequestParam(value="referenceNum", required = false) String referenceNum,
                                                                    @RequestParam(value="accountCode", required = false) String accountCode,
                                                                    @RequestParam("take") int take,
                                                                    @RequestParam("skip") int skip,
                                                                    @RequestParam("page") int page,
                                                                    @RequestParam("pageSize") int pageSize,
                                                                    @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                    @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            String adminId = super.getLoginAdminId();
            Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
            ApiResult<PagedData<List<OfflinePaymentRequestVM>>> data = offlinePaySrv.getPagedOfflinePaymentRequest(isPartnerOnly, paId, adminId, startDate, endDate, status, username, orgName, receiptNum, referenceNum, accountCode, sortField, sortDirection, page, pageSize);
            int total = 0;
            boolean success = false;
            List<OfflinePaymentRequestVM> items = new ArrayList<OfflinePaymentRequestVM>();
            final ResultVM result = new ResultVM();
            if(data != null && data.isSuccess()){
                PagedData<List<OfflinePaymentRequestVM>> pagedData = data.getData();
                items = pagedData != null ? pagedData.getData() : new ArrayList<OfflinePaymentRequestVM>();
                total = pagedData != null ? pagedData.getSize() : 0;
                success = true;
            }
            result.setTotal(total);
            result.setStatus(success);
            result.setViewModel(items);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSearchReservation", e, ResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "detail", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<OfflinePaymentResultVM>> apiViewOfflineRequest(HttpServletRequest request, @RequestParam("id")Integer id) {
        log.info("Entered apiViewOfflineRequest...");
        try {
            if(id != null && id.intValue() > 0){
                return new ResponseEntity<ApiResult<OfflinePaymentResultVM>>(offlinePaySrv.viewOfflinePaymentDetails(super.getLoginAdminId(), id, true), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(false,null,"Invalid Request",null), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiViewOfflineRequest", e, OfflinePaymentResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "preview", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<OfflinePaymentResultVM>> apiPreviewOfflineRequest(HttpServletRequest request, @RequestParam("id")Integer id) {
        log.info("Entered apiViewOfflineRequest...");
        try {
            if(id != null && id.intValue() > 0){
                return new ResponseEntity<ApiResult<OfflinePaymentResultVM>>(offlinePaySrv.viewOfflinePaymentDetails(super.getLoginAdminId(), id, false), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(false,null,"Invalid Request",null), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiPreviewOfflineRequest", e, OfflinePaymentResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "review", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiVerifyOfflinePaymentRequest(HttpServletRequest request,
                                        @RequestParam(value = "id", required = false)Integer id,
                                        @RequestParam(value = "action", required = false)String action,
                                        @RequestParam(value = "remarks", required = false)String approverRemarks) {
        log.info("Entered apiVerifyOfflinePaymentRequest...");
        try {
            if(id != null && id.intValue() > 0){
                return new ResponseEntity<ApiResult<String>>(offlinePaySrv.reviewOfflinePaymentDetails(super.getLoginAdminId(), id, action, approverRemarks), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ApiResult<>(false,null,"Invalid Request",null), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiVerifyOfflinePaymentRequest", e, String.class), HttpStatus.OK);
        }
    }
}
