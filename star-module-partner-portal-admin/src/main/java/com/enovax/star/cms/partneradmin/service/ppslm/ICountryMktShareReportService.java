package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.*;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by lavanya on 28/10/16.
 */
public interface ICountryMktShareReportService {
    public CountryMktShareReportInitVM initPage();
    public ResultVM getCountryMktShareReportDetail(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, int page, int pageSize);
    public CountryMktShareRptDetailsVM manipulatePartnerMktWithoutID(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, PageRequest pageRequest);
    public CountryMktShareRptDetailsVM manipulatePartnerMktWithID(CountryMktShareRptFilterVM filterVM, String sortField, String sortDirection, PageRequest pageRequest);
    public CountryMktShareRptDetailsVM manipulatePartnerMkt(CountryMktShareRptFilterVM filterVM);
}
