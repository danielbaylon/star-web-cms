package com.enovax.star.cms.partneradmin.service.batch.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import com.enovax.star.cms.commons.repository.ppmflg.PPMFLGPartnerExtRepository;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.PartnerExtAttrName;
import com.enovax.star.cms.partnershared.service.ppmflg.IPartnerService;
import com.enovax.star.cms.partnershared.service.ppmflg.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * Created by lavanya on 16/12/16.
 */
@Service("PPMFLGWoTResetReservationCapJob")
public class WoTResetReservationCapJob {
    private static Logger log = LoggerFactory.getLogger(com.enovax.star.cms.partneradmin.service.batch.ppslm.WoTResetReservationCapJob.class);

    @Autowired
    private PPMFLGPartnerExtRepository paExtRepo;

    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService paSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppmflg.wot.reset.reservation.cap}")
    public void resetWoTReservationCap(){
        log.info("======== RESET WOT PARTNER RESERVATION CAP JOB STARTED =========");
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {

                    List<PPMFLGPartnerExt> exts = paExtRepo.finAllWingsOfTimeReservationValidUntilList();
                    if(exts != null && exts.size() > 0){
                        try{
                            for(PPMFLGPartnerExt e : exts){
                                Date now = new Date(System.currentTimeMillis());
                                Date validUntil = null;
                                String value = e.getAttrValue();
                                String fmt   = e.getAttrValueFormat();
                                if(fmt != null && fmt.trim().length() > 0 && value != null && value.trim().length() > 0){
                                    validUntil = NvxDateUtils.parseDate(value.trim(), fmt.trim());
                                    if(validUntil != null && validUntil.before(now)){
                                        PPMFLGPartnerExt reservationCap = paExtRepo.findFirstByPartnerIdAndAttrNameAndStatus(e.getPartnerId(), PartnerExtAttrName.WoTReservationCap.code, GeneralStatus.Active.code);
                                        reservationCap.setAttrValue(sysParamSrv.getPartnerWoTReservationCap().toString());
                                        paExtRepo.save(reservationCap);
                                        e.setAttrValue("");
                                        paExtRepo.save(e);
                                    }
                                }
                            }


                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
            });
        }
        log.info("======== RESET WOT PARTNER RESERVATION CAP JOB ENDED =========");
    }
}
