package com.enovax.star.cms.partneradmin.service.batch.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPartnerExtRepository;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.PartnerExtAttrName;
import com.enovax.star.cms.partnershared.service.ppslm.IPartnerService;
import com.enovax.star.cms.partnershared.service.ppslm.ISystemParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * Created by lavanya on 14/12/16.
 */
@Service
public class WoTResetReservationCapJob {
    private static Logger log = LoggerFactory.getLogger(WoTResetReservationCapJob.class);

    @Autowired
    private PPSLMPartnerExtRepository paExtRepo;

    @Autowired
    private IPartnerService paSrv;

    @Autowired
    private ISystemParamService sysParamSrv;

    @Autowired
    @Qualifier("realTimeTaskExecutor")
    private ThreadPoolTaskExecutor realTimeTaskExecutor;

    private static boolean isAuthorInstance = false;

    @PostConstruct
    public void init(){
        try{
            isAuthorInstance = MagnoliaConfigUtil.isAuthorInstance();
        }catch (Exception ex){
            log.error("check is author instance failed "+ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Scheduled(cron = "${ppslm.wot.reset.reservation.cap}")
    public void resetWoTReservationCap(){
        log.info("======== RESET WOT PARTNER RESERVATION CAP JOB STARTED =========");
        if(isAuthorInstance){
            realTimeTaskExecutor.submit(new Runnable() {
                @Override
                public void run() {

                    List<PPSLMPartnerExt> exts = paExtRepo.finAllWingsOfTimeReservationValidUntilList();
                    if(exts != null && exts.size() > 0){
                        try{
                        for(PPSLMPartnerExt e : exts){
                            Date now = new Date(System.currentTimeMillis());
                            Date validUntil = null;
                                String value = e.getAttrValue();
                                String fmt   = e.getAttrValueFormat();
                                if(fmt != null && fmt.trim().length() > 0 && value != null && value.trim().length() > 0){
                                    validUntil = NvxDateUtils.parseDate(value.trim(), fmt.trim());
                                    if(validUntil != null && validUntil.before(now)){
                                        PPSLMPartnerExt reservationCap = paExtRepo.findFirstByPartnerIdAndAttrNameAndStatus(e.getPartnerId(), PartnerExtAttrName.WoTReservationCap.code, GeneralStatus.Active.code);
                                        reservationCap.setAttrValue(sysParamSrv.getPartnerWoTReservationCap().toString());
                                        paExtRepo.save(reservationCap);
                                        e.setAttrValue("");
                                        paExtRepo.save(e);
                                    }
                                }
                            }


                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }
            });
        }
        log.info("======== RESET WOT PARTNER RESERVATION CAP JOB ENDED =========");
    }
}
