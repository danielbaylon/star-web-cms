package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.PaProfileRptFilterVM;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by lavanya on 26/10/16.
 */
public interface IPartnerProfileRptGenerator {
    public Workbook generateExcel(PaProfileRptFilterVM filterVM);
}
