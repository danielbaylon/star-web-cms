package com.enovax.star.cms.partneradmin.controller.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.partnershared.constant.ppslm.AdminUserRoles;
import com.enovax.star.cms.partnershared.model.grid.PartnerFilter;
import com.enovax.star.cms.partnershared.model.grid.PartnerGridFilterVM;
import com.enovax.star.cms.partnershared.model.grid.ProdFilter;
import com.enovax.star.cms.partnershared.model.grid.ProductGridFilterVM;
import info.magnolia.cms.security.User;
import info.magnolia.context.MgnlContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class BasePartnerAdminController {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";

    protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<List<K>> handleUncaughtExceptionForList(Throwable t, Class<K> innerListClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K, V> ApiResult<Map<K, V>> handleUncaughtExceptionForMap(
            Throwable t, Class<K> innerMapKey, Class<V> innerMapValue, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected String getLoginAdminId(){
        // TODO: REQUIRED B2B ADMIN ROLES ::::
        return MgnlContext.getUser().getName();
    }

    public boolean hasRequiredUserRole(String code) {
        if(code == null || code.trim().length() == 0){
            return false;
        }
        boolean verified = MgnlContext.getUser().getAllRoles().contains(code);
        return verified;
    }

    protected boolean isLoginAdminUser(){
        User user = MgnlContext.getUser();
        if(user != null){
            if(user.getAllRoles() != null && user.getAllRoles().contains(AdminUserRoles.PartnerPortalSlmAdmin.code)){
                return true;
            }
        }
        return false;
    }

    protected String getCurrentChannel(){
        return PartnerPortalConst.Partner_Portal_Channel;
    }

    protected ProductGridFilterVM initFilterChannel(ProductGridFilterVM vm){
        if(vm == null){
            vm = new ProductGridFilterVM();
        }
        ProdFilter prodFilter = vm.getProdFilter();
        if(prodFilter == null){
            if(vm.getProdfilterJson() != null && vm.getProdfilterJson().trim().length() > 0){
                prodFilter = JsonUtil.fromJson(vm.getProdfilterJson().trim(), ProdFilter.class);
            }
            if(prodFilter == null){
                prodFilter = new ProdFilter();
            }
        }
        prodFilter.setChannel(getCurrentChannel());
        vm.setProdFilter(prodFilter);
        return vm;
    }

    protected PartnerGridFilterVM initFilterChannel(PartnerGridFilterVM vm) {
        if(vm == null){
            vm = new PartnerGridFilterVM();
        }
        PartnerFilter partnerFilter = vm.getPartnerFilter();
        if(partnerFilter == null){
            if(vm.getPtfilterJson() != null && vm.getPtfilterJson().trim().length() > 0){
                partnerFilter = JsonUtil.fromJson(vm.getPtfilterJson().trim(), PartnerFilter.class);
            }
            if(partnerFilter == null){
                partnerFilter = new PartnerFilter();
            }
            vm.setPartnerFilter(partnerFilter);
        }
        partnerFilter.setChannel(getCurrentChannel());
        vm.setPartnerFilter(partnerFilter);
        return vm;
    }

    protected <K> ApiResult<K> handleException(String methodName, Throwable t, Class<K> resultClass) {
        if(t != null){
            log.error("!!! System exception encountered ["+methodName+"] !!!", t);
        }else{
            log.error("!!! System exception encountered ["+methodName+"] !!!");
        }
        if(t instanceof BizValidationException){
            BizValidationException be = (BizValidationException) t;
            if(be.getErrorMsg() != null && be.getErrorMsg().trim().length() > 0){
                return new ApiResult<>(false, "", be.getErrorMsg(), null);
            }
        }
        return handleUncaughtException(t, resultClass, null);
    }
}
