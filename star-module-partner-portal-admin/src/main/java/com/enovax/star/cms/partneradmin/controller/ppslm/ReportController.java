package com.enovax.star.cms.partneradmin.controller.ppslm;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.partner.ppslm.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.partneradmin.service.ppslm.*;
import com.enovax.star.cms.partnershared.service.ppslm.IReportService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 25/10/16.
 */
@Controller
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/report/")
public class ReportController extends BasePartnerAdminController {

    @Autowired
    IPartnerProfileReportService partnerProfileReportService;
    @Autowired
    IPartnerProfileRptGenerator partnerProfileRptGenerator;
    @Autowired
    ICountryMktShareReportService countryMktShareReportService;
    @Autowired
    ICountryMktShareRptGenerator countryMktShareRptGenerator;
    @Autowired
    IExceptionRptGenerator exceptionReportGenerator;
    @Autowired
    IReportService reportService;
    boolean asc;
    public static final String SESS_FILTER_INV = "sess-filter-inv";
    public static final String SESS_FILTER_PIN = "sess-filter-pin";
    public static final String SESS_FILTER_TR = "sess-filter-tr";
    protected Map<String, Object> session = new HashMap<>();

    @RequestMapping(value = "partner-profile-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitPartnerProfileRptPage() {
        try {

            PartnerProfileReportInitVM partnerProfileInitVM = partnerProfileReportService.initPage();
            String partnerProfileInitJson = JsonUtil.jsonify(partnerProfileInitVM);
            if(partnerProfileInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", partnerProfileInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitPartnerProfileRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "partner-profile-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiPartnerProfileRptSearch(@RequestParam(name = "filterJson")String filterJson,@RequestParam("skip") int skip,
                                                                          @RequestParam("page") int page,
                                                                          @RequestParam("pageSize") int pageSize,
                                                                          @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                          @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            PaProfileRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,PaProfileRptFilterVM.class);
            }else{
                filter = new PaProfileRptFilterVM();
            }
            ResultVM resultVM = partnerProfileReportService.getPaProfilePage(filter,sortField,sortDirection,page,pageSize);
            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfileRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "partner-profile-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPartnerProfielRptExport(@RequestParam(name = "filterJson") String filterJson, HttpServletResponse response) {
        try {
            PaProfileRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,PaProfileRptFilterVM.class);
            }else{
                filter = new PaProfileRptFilterVM();
            }
            Workbook wb = partnerProfileRptGenerator.generateExcel(filter);
            final String fileName = "partner-profile-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
            outByteStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPartnerProfielRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "country-mktshare-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitCountryMktShareRptPage() {
        try {

            CountryMktShareReportInitVM countryMktShareReportInitVM = countryMktShareReportService.initPage();
            String countryMktShareReportInitJson = JsonUtil.jsonify(countryMktShareReportInitVM);
            if(countryMktShareReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", countryMktShareReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitCountryMktShareRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "country-mktshare-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiCountryMktShareRptSearch(@RequestParam(name = "filterJson")String filterJson,@RequestParam("skip") int skip,
                                                                          @RequestParam("page") int page,
                                                                          @RequestParam("pageSize") int pageSize,
                                                                          @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                          @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            CountryMktShareRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,CountryMktShareRptFilterVM.class);
            }else{
                filter = new CountryMktShareRptFilterVM();
            }
            //ResultVM resultVM = null;

            ResultVM resultVM = countryMktShareReportService.getCountryMktShareReportDetail(filter,sortField,sortDirection,page,pageSize);
            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCountryMktShareRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "country-mktshare-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCountryMktShareRptExport(@RequestParam(name = "filterJson") String filterJson, HttpServletResponse response) {
        try {
            CountryMktShareRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,CountryMktShareRptFilterVM.class);
            }else{
                filter = new CountryMktShareRptFilterVM();
            }
            Workbook wb = countryMktShareRptGenerator.generateExcel(filter);
            final String fileName = "country-market-share-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            outByteStream.close();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCountryMktShareRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "region-mktshare-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitRegionMktShareRptPage() {
        try {

            CountryMktShareReportInitVM countryMktShareReportInitVM = countryMktShareReportService.initPage();
            String countryMktShareReportInitJson = JsonUtil.jsonify(countryMktShareReportInitVM);
            if(countryMktShareReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", countryMktShareReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitRegionMktShareRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "region-mktshare-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiRegionMktShareRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                          @RequestParam("take") int take,
                                                                          @RequestParam("skip") int skip,
                                                                          @RequestParam("page") int page,
                                                                          @RequestParam("pageSize") int pageSize,
                                                                          @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                          @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            PaProfileRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,PaProfileRptFilterVM.class);
            }else{
                filter = new PaProfileRptFilterVM();
            }
            ResultVM resultVM = partnerProfileReportService.getPaProfilePage(filter,sortField,sortDirection,page,pageSize);
            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegionMktShareRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "region-mktshare-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRegionMktShareRptExport(@RequestParam(name = "filterJson") String filterJson, HttpServletResponse response) {
        try {
            PaProfileRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,PaProfileRptFilterVM.class);
            }else{
                filter = new PaProfileRptFilterVM();
            }
            Workbook wb = partnerProfileRptGenerator.generateExcel(filter);
            final String fileName = "partner-profile-report.xls";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return new ResponseEntity<>(new com.enovax.star.cms.commons.model.api.ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRegionMktShareRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitTransactionRptPage() {
        try {

            TransactionReportInitVM transactionReportInitVM = reportService.initTransPage(true,null);
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTransactionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiTransactionRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                       @RequestParam("take") int take,
                                                                       @RequestParam("skip") int skip,
                                                                       @RequestParam("page") int page,
                                                                       @RequestParam("pageSize") int pageSize,
                                                                       @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                       @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(true);
            session.put(SESS_FILTER_TR,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "txnDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<TransReportModel> reportResult = reportService.generateTransReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generateTransReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiTransactionRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "transaction-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiTransactionRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)session.get(SESS_FILTER_TR);
            Workbook wb = reportService.generateTransReportExcel(filter,"txnDate",asc);
            final String fileName = "transaction-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiTransactionRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "inventory-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitInventoryRptPage() {
        try {
            TransactionReportInitVM transactionReportInitVM = reportService.initInventoryPage(true,null);
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTransactionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "inventory-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiInventoryRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                       @RequestParam("take") int take,
                                                                       @RequestParam("skip") int skip,
                                                                       @RequestParam("page") int page,
                                                                       @RequestParam("pageSize") int pageSize,
                                                                       @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                       @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(true);
            session.put(SESS_FILTER_INV,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "txnDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<InventoryReportModel> reportResult = reportService.generateInventoryReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generateInventoryReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInventoryRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "inventory-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiInventoryRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)session.get(SESS_FILTER_INV);
            Workbook wb = reportService.generateInventoryReportExcel(filter,"txnDate",asc);
            final String fileName = "inventory-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInventoryRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "package-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitPackageRptPage() {
        try {
            TransactionReportInitVM transactionReportInitVM = reportService.initPackagePage(true,null);
            String transactionReportInitJson = JsonUtil.jsonify(transactionReportInitVM);
            if(transactionReportInitVM != null){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", transactionReportInitJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitPackageRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "package-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiPackageRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                     @RequestParam("take") int take,
                                                                     @RequestParam("skip") int skip,
                                                                     @RequestParam("page") int page,
                                                                     @RequestParam("pageSize") int pageSize,
                                                                     @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                     @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ReportFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ReportFilterVM.class);
            }else{
                filter = new ReportFilterVM();
            }
            filter.setIsSysad(true);
            session.put(SESS_FILTER_PIN,filter);
            //initialize default ordering
            if (sortField == null || "".equalsIgnoreCase(sortField)) {
                log.info("No ordering specified. Going with default ordering.");
                orderBy = "ticketGeneratedDate";
                asc = true;
            } else {
                log.info("Ordering specified.");
                orderBy = sortField;
                if("pinGeneratedDateText".equalsIgnoreCase(orderBy)) {
                    orderBy = "ticketGeneratedDate";
                }
                if (orderBy.endsWith("Text")) {
                    orderBy = orderBy.replace("Text", "");
                }
                asc = "asc".equals(sortDirection) ? true : false;
            }
            ReportResult<PackageReportModel> reportResult = reportService.generatePackageReport(filter,skip,take,orderBy,asc,false);
            int total = reportService.generatePackageReport(filter,-1,-1,"",true,true).getTotalCount();
            ResultVM resultVM = new ResultVM();
            resultVM.setViewModel(reportResult.getRows());
            resultVM.setTotal(total);

            if(resultVM != null) {
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPackageRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "package-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiPackageRptExport(HttpServletResponse response) {
        try {
            ReportFilterVM filter = (ReportFilterVM)session.get(SESS_FILTER_PIN);
            Workbook wb = reportService.generatePackageReportExcel(filter,"ticketGeneratedDate",asc);
            final String fileName = "package-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiPackageRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "exception-rpt/init-page", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiInitExceptionRptPage() {
        try {
            List<PartnerVM> partnerVMs = reportService.initExceptionPage();
            String partnerVMsJson = JsonUtil.jsonify(partnerVMs);
            if(partnerVMs != null && partnerVMs.size()!=0){
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(true, "", "", partnerVMsJson), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<String>>( new ApiResult<String>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitExceptionRptPage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "exception-rpt/search", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<ResultVM>> apiExceptionRptSearch(@RequestParam(name = "filterJson")String filterJson,
                                                                   @RequestParam("take") int take,
                                                                   @RequestParam("skip") int skip,
                                                                   @RequestParam("page") int page,
                                                                   @RequestParam("pageSize") int pageSize,
                                                                   @RequestParam(value = "sort[0].field", required = false) String sortField,
                                                                   @RequestParam(value = "sort[0].dir", required = false) String sortDirection) {
        try {
            ExceptionRptFilterVM filter = null;
            String orderBy;

            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ExceptionRptFilterVM.class);
            }else{
                filter = new ExceptionRptFilterVM();
            }

            ResultVM resultVM = reportService.getExceptionReportPage(filter,sortField,sortDirection,page,pageSize);
            if(resultVM != null){
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(true, "", "", resultVM), HttpStatus.OK);
            }else{
                return new ResponseEntity<ApiResult<ResultVM>>( new ApiResult<ResultVM>(ApiErrorCodes.PartnerProfileRptInitError), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiExceptionRptSearch] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ResultVM.class, ""), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "exception-rpt/export", method = {RequestMethod.GET,RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiExceptionRptExport(@RequestParam(name = "filterJson") String filterJson, HttpServletResponse response) {
        try {
            ExceptionRptFilterVM filter = null;
            if(filterJson != null){
                filter = JsonUtil.fromJson(filterJson,ExceptionRptFilterVM.class);
            }else{
                filter = new ExceptionRptFilterVM();
            }
            Workbook wb = exceptionReportGenerator.generateExcel(filter);
            final String fileName = "exception-report.xlsx";
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            wb.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
            outByteStream.close();

            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiExceptionRptExport] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}
