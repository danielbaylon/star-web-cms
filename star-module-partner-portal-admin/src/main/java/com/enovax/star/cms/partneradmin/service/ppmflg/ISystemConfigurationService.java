package com.enovax.star.cms.partneradmin.service.ppmflg;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.partner.ppmflg.GeneralConfigVM;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;

import java.util.Map;

/**
 * Created by lavanya on 28/9/16.
 */
public interface ISystemConfigurationService {
    public GeneralConfigVM getGeneralConfigurationViewModel();
    public ResultVM listBackupApprovers(String orderBy, String orderWith, Integer page, Integer pagesize);
    public ResultVM removeBackupApprovers(String backupIds, String adminId);
    public ResultVM saveParam(String paramKey, String paramValue, String paramLabel, String adminId);
    public Map<String, Object> getTicketConfigMap();
    public Map<String, Object> getWotConfigMap();
    public ResultVM getRevalItemList(StoreApiChannels channel);
    public ResultVM getAxProductsList(String productName, boolean filterEventProduct);
    public ResultVM getWotProducts(StoreApiChannels channel);
}
