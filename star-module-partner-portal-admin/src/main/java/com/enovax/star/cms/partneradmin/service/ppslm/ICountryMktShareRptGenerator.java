package com.enovax.star.cms.partneradmin.service.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.CountryMktShareRptFilterVM;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by lavanya on 5/11/16.
 */
public interface ICountryMktShareRptGenerator {
    public Workbook generateExcel(CountryMktShareRptFilterVM filterVM);
}
