package com.enovax.star.cms.partneradmin.model;

/**
 * Created by jennylynsze on 5/15/16.
 */
public class SavedProductTierResponseVM {
    private String tierVmJson;
    private String tierVmsJson;

    public String getTierVmJson() {
        return tierVmJson;
    }

    public void setTierVmJson(String tierVmJson) {
        this.tierVmJson = tierVmJson;
    }

    public String getTierVmsJson() {
        return tierVmsJson;
    }

    public void setTierVmsJson(String tierVmsJson) {
        this.tierVmsJson = tierVmsJson;
    }
}
