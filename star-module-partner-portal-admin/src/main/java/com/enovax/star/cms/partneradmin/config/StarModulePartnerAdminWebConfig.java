package com.enovax.star.cms.partneradmin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by jennylynsze on 5/12/16.
 */

@Configuration
@ComponentScan(
        basePackages = {"com.enovax.star.cms.partneradmin"},
        includeFilters = {
                @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION)
        }
)
@EnableWebMvc
public class StarModulePartnerAdminWebConfig {

    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver bean = new CommonsMultipartResolver();
        bean.setMaxUploadSize(10000000);
        return bean;
    }
}
