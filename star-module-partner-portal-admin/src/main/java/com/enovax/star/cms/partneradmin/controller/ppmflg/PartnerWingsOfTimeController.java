package com.enovax.star.cms.partneradmin.controller.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PartnerPortalConst;
import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;
import com.enovax.star.cms.commons.model.partner.ppmflg.*;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.ProductExtViewModelWrapper;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.partnershared.constant.ppmflg.AdminUserRoles;
import com.enovax.star.cms.partnershared.constant.ppmflg.WingOfTimeChannel;
import com.enovax.star.cms.partnershared.service.ppmflg.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 6/7/16.
 */
@Controller("PPMFLGPartnerWingsOfTimeController")
@RequestMapping(PartnerPortalConst.ADMIN_ROOT + "/partner-wot/")
public class PartnerWingsOfTimeController extends BasePartnerAdminController {

    @Autowired
    @Qualifier("PPMFLGIPartnerService")
    private IPartnerService paSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerProductService")
    private IPartnerProductService paProdSrv;

    @Autowired
    @Qualifier("PPMFLGIWingsOfTimeSharedService")
    private IWingsOfTimeSharedService paWoTSrv;

    @Autowired
    @Qualifier("PPMFLGIPartnerAdminProductService")
    private IPartnerAdminProductService paProdAdminSrv;

    @Autowired
    @Qualifier("PPMFLGISystemParamService")
    private ISystemParamService sysParamSrv;

    private static final String ERROR_LOGIN_MSG = "Invalid request, Access denied";

    private boolean validateLogin(){
        String id = null;
        try{
            id = super.getLoginAdminId();
            return super.hasRequiredUserRole(AdminUserRoles.WOTBackendReservation.code);
        }catch (Exception ex){
        }
        return id != null && id.trim().length() > 0;
    }

    private boolean validateLoginWithErrorMessage() throws BizValidationException {
        if(!validateLogin()){
            throw new BizValidationException(ERROR_LOGIN_MSG);
        }
        return true;
    }

    @RequestMapping(value = "get-partners", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<List<PartnerVM>>> apiGetPartners(HttpServletRequest request) {
        ApiResult<List<PartnerVM>> api = new ApiResult<List<PartnerVM>>();
        List<PartnerVM> pvm = new ArrayList<PartnerVM>();
        if(validateLogin()){
            try {
                final ApiResult<List<PartnerVM>> result = paSrv.getAllWingsOfTimePartners();
                api.setSuccess(result.isSuccess());
                if(result.isSuccess()){
                    if(result.getData() != null){
                        pvm = result.getData();
                    }
                }else{
                    api.setMessage(result.getMessage());
                }
            } catch (Exception e) {
                log.error("!!! System exception encountered [apiGetSchedule] !!!");
            }
            api.setData(pvm);
            api.setTotal(pvm != null && pvm.size() > 0 ? pvm.size() : 0);
        }
        return new ResponseEntity<>(api, HttpStatus.OK);
    }

    @RequestMapping(value = "get-advanced-reservation-release-day", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Integer>> apiAdvancedReservationReleaseDays(HttpServletRequest request) {
        log.info("Entered apiAdvancedReservationReleaseDays...");
        try {
            validateLoginWithErrorMessage();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", sysParamSrv.getPartnerWoTAdvancedDaysReleaseBackendReservedTickets()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiAdvancedReservationReleaseDays", e, Integer.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-advanced-reservation-days-allowed", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Integer>> apiAdvancedReservationDaysAllowed(HttpServletRequest request) {
        log.info("Entered apiAdvancedReservationDaysAllowed...");
        try {
            validateLoginWithErrorMessage();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", sysParamSrv.getWotAdminReservationPeriodLimit()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiAdvancedReservationDaysAllowed", e, Integer.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-schedule", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiGetSchedule(HttpServletRequest request) {
        log.info("Entered apiGetSchedule...");
        try {
            validateLoginWithErrorMessage();
            final List<String> productIds = new ArrayList<>();
            List<AxProduct> prodList = paWoTSrv.getWOTProducts();
            for (AxProduct prod : prodList) {
                productIds.add(prod.getProductListingId() + "");
            }

            final ApiResult<List<ProductExtViewModel>> result = paProdAdminSrv.getDataForProducts(WingOfTimeChannel.CHANNEL, productIds);
            if (result.isSuccess()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", new ProductExtViewModelWrapper(result.getData())), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ApiResult<>(false, result.getErrorCode(), result.getMessage(), null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetSchedule", e, ProductExtViewModelWrapper.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-reservation", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ResultVM>> apiSearchReservation(HttpServletRequest request,
                                                                    @RequestParam(value="partnerId", required = false) Integer partnerId,
                                                                    @RequestParam(value="startDateStr", required = false) String startDateStr,
                                                                    @RequestParam(value="endDateStr", required = false) String endDateStr,
                                                                    @RequestParam(value="filterStatus", required = false) String filterStatus,
                                                                    @RequestParam(value="showTime", required = false) String showTimes,
                                                                    @RequestParam("take") int take,
                                                                    @RequestParam("skip") int skip,
                                                                    @RequestParam("page") int page,
                                                                    @RequestParam("pageSize") int pageSize,
                                                                    @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                    @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered apiSearchReservation..");
        try {
            final ResultVM result = new ResultVM();
            if(validateLogin()){
                Date startDate = startDateStr != null && startDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(startDateStr + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
                Date endDate   = endDateStr != null && endDateStr.trim().length() > 0 ? NvxDateUtils.parseDate(endDateStr + " 23:59:59", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) : null;
                com.enovax.star.cms.commons.model.api.ApiResult<PagedData<List<WOTReservationVM>>> data = paWoTSrv.getPagedWingsOfTimeReservations(partnerId, startDate, endDate, filterStatus, showTimes, sortField, sortDirection, page, pageSize);
                int total = 0;
                boolean success = false;
                List<WOTReservationVM> items = new ArrayList<WOTReservationVM>();
                if(data != null && data.isSuccess()){
                    PagedData<List<WOTReservationVM>> pagedData = data.getData();
                    items = pagedData != null ? pagedData.getData() : new ArrayList<WOTReservationVM>();
                    total = pagedData != null ? pagedData.getSize() : 0;
                    success = true;
                }
                result.setTotal(total);
                result.setStatus(success);
                result.setViewModel(items);
            }
            return new ResponseEntity<>(new ApiResult<ResultVM>(true, "", "", result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSearchReservation", e, ResultVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "validate-new-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiValidateNewReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiValidateReservation...");
        try {
            validateLoginWithErrorMessage();
            reservation.setCreatedByUsername(super.getLoginAdminId());
            ApiResult<WOTReservationCharge> result = paWoTSrv.validateAdminNewReservation(reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiValidateNewReservation", e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiSaveReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiGetReservation...");
        try {
            validateLoginWithErrorMessage();
            reservation.setCreatedByUsername(super.getLoginAdminId());
            reservation.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            ApiResult<WOTReservationVM> result = paWoTSrv.saveAdminReservation(reservation, false);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiSaveReservation", e, WOTReservationVM.class), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "get-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiGetReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiGetReservation...");
        try {
            validateLoginWithErrorMessage();
            WOTReservationVM reservation = paWoTSrv.getAdminReservationByID(reservationId);
            return new ResponseEntity<>(new ApiResult<>(true, "", "",  reservation), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiGetReservation", e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "validate-update-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiValidateUpdateReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiValidateUpdateReservation...");
        try {
            validateLoginWithErrorMessage();
            ApiResult<WOTReservationCharge> result = paWoTSrv.validateAdminUpdateReservation(reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiValidateUpdateReservation", e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }
    @RequestMapping(value = "validate-cancel-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationCharge>> apiCalculateCancelReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiCalculateCancelReservation...");
        try {
            validateLoginWithErrorMessage();
            ApiResult<WOTReservationCharge> result = paWoTSrv.validateAdminCancelReservation(reservationId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCalculateCancelReservation", e, WOTReservationCharge.class), HttpStatus.OK);
        }
    }
    @RequestMapping(value = "cancel-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiCancelReservation(HttpServletRequest request, @PathVariable("id") Integer reservationId) {
        log.info("Entered apiCancelReservation...");
        try {
            validateLoginWithErrorMessage();
            if (reservationId == null || reservationId.intValue() == 0) {
                throw new BizValidationException("Invalid request found");
            }
            WOTReservationVM vm = new WOTReservationVM();
            vm.setId(reservationId);
            vm.setCreatedByUsername(super.getLoginAdminId());
            ApiResult<String> result = paWoTSrv.cancelAdminReservation(vm, true);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiCancelReservation", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-reservation", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<WOTReservationVM>> apiUpdateReservation(HttpServletRequest request, @RequestBody WOTReservationVM reservation) {
        log.info("Entered apiUpdateReservation...");
        try {
            validateLoginWithErrorMessage();
            reservation.setCreatedByUsername(super.getLoginAdminId());
            reservation.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            ApiResult<WOTReservationVM> result = paWoTSrv.updateAdminReservation(reservation);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiUpdateReservation", e, WOTReservationVM.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "refresh-reservation/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRefreshPinCodeDetails(HttpServletRequest request, @PathVariable("id") Integer reservationId){
        log.info("Entered apiRefreshPinCodeDetails...");
        try {
            validateLoginWithErrorMessage();
            ApiResult<String> result = paWoTSrv.refreshAdminReservationPinCodeById(reservationId);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiRefreshPinCodeDetails", e, String.class), HttpStatus.OK);
        }
    }
}
