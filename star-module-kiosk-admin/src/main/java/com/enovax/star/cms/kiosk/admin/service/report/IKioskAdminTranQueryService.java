package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminTranReceipt;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportPaymentDetailResult;

public interface IKioskAdminTranQueryService {

    List<KioskAdminTranQueryResult> getTranQueryResult(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskAdminTranQueryResult> getTranQueryResult(KioskAdminTranQueryFilter filter);

    KioskAdminTranReceipt getReceiptByAxCartId(String channel, String axCartId);

    List<KioskAdminZReportPaymentDetailResult> getPaymentDetails(String channel, String tranDate, String kioskId);

}
