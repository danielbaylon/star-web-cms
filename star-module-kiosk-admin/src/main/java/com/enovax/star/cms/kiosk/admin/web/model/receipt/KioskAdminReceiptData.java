package com.enovax.star.cms.kiosk.admin.web.model.receipt;

import java.util.ArrayList;
import java.util.List;

public class KioskAdminReceiptData {

    private String location;

    private String date;

    private String kioskId;

    private String transId;

    private String axReceiptNo;

    private String axReceiptId;

    private String totalAmount;

    private String totalGstAmount;

    private Boolean allTicketsPrinted;

    private String qrCodeData;

    private List<KioskAdminReceiptCmsData> cmsList = new ArrayList<>();

    private KioskAdminReceiptPaymentData payment = new KioskAdminReceiptPaymentData();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKioskId() {
        return kioskId;
    }

    public void setKioskId(String kioskId) {
        this.kioskId = kioskId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getAxReceiptNo() {
        return axReceiptNo;
    }

    public void setAxReceiptNo(String axReceiptNo) {
        this.axReceiptNo = axReceiptNo;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalGstAmount() {
        return totalGstAmount;
    }

    public void setTotalGstAmount(String totalGstAmount) {
        this.totalGstAmount = totalGstAmount;
    }

    public String getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(String qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

    public List<KioskAdminReceiptCmsData> getCmsList() {
        return cmsList;
    }

    public void setCmsList(List<KioskAdminReceiptCmsData> cmsList) {
        this.cmsList = cmsList;
    }

    public KioskAdminReceiptPaymentData getPayment() {
        return payment;
    }

    public void setPayment(KioskAdminReceiptPaymentData payment) {
        this.payment = payment;
    }

    public Boolean getAllTicketsPrinted() {
        return allTicketsPrinted;
    }

    public void setAllTicketsPrinted(Boolean allTicketsPrinted) {
        this.allTicketsPrinted = allTicketsPrinted;
    }

    public String getAxReceiptId() {
        return axReceiptId;
    }

    public void setAxReceiptId(String axReceiptId) {
        this.axReceiptId = axReceiptId;
    }

}
