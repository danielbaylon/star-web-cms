package com.enovax.star.cms.kiosk.admin.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;

/**
 * Created by jennylynsze on 5/12/16.
 */
@Configuration
@ComponentScan(basePackages = { "com.enovax.star.cms.commons", "com.enovax.star.cms.kiosk.admin", "com.enovax.star.cms.kiosk.api.store.jcr",
        "com.enovax.star.cms.kiosk.api.store" }, excludeFilters = { @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
                @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION) })
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.enovax.star.cms.kiosk.admin", "com.enovax.star.cms.kiosk.api.store.persistence.repository" })
@EnableScheduling
public class StarModuleKioskAdminAppConfig {

    protected Logger log = LoggerFactory.getLogger(StarModuleKioskAdminAppConfig.class);

    public static final String[] PROP_FILE_NAMES = new String[] { Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.db.data.source.location"), };

    @Bean
    public static PropertySourcesPlaceholderConfigurer propsConfigurer() {
        PropertySourcesPlaceholderConfigurer bean = new PropertySourcesPlaceholderConfigurer();

        final int propCount = PROP_FILE_NAMES.length;
        final Resource[] resources = new FileSystemResource[propCount];
        for (int i = 0; i < propCount; i++) {
            resources[i] = new FileSystemResource(PROP_FILE_NAMES[i]);
        }

        bean.setLocations(resources);
        bean.setIgnoreUnresolvablePlaceholders(true);

        return bean;
    }

    private @Value("${ds.driverClass}") String driverClass;
    private @Value("${ds.kiosk.jdbcUrl}") String jdbcUrl;
    private @Value("${ds.kiosk.jdbcUser}") String jdbcUser;
    private @Value("${ds.kiosk.jdbcPassword}") String jdbcPassword;

    private @Value("${c3p0.acquireIncrement}") Integer c3p0acquireIncrement;
    private @Value("${c3p0.idleConnectionTestPeriod}") Integer c3p0idleConnectionTestPeriod;
    private @Value("${c3p0.maxIdleTime}") Integer c3p0maxIdleTime;
    private @Value("${c3p0.maxPoolSize}") Integer c3p0maxPoolSize;
    private @Value("${c3p0.minPoolSize}") Integer c3p0minPoolSize;
    private @Value("${c3p0.maxStatementsPerConnection}") Integer c3p0maxStatementsPerConnection;
    private @Value("${c3p0.numHelperThreads}") Integer c3p0numHelperThreads;

    private @Value("${hibernate.bytecode.use_reflection_optimizer}") String hByteCodeUseReflectionOptimizer;
    private @Value("${hibernate.dialect}") String hDialect;
    private @Value("${hibernate.search.autoregister_listeners}") String hSearchAutoRegisterListeners;
    private @Value("${hibernate.show_sql}") String hShowSql;
    private @Value("${hibernate.format_sql}") String hFormatSql;

    public static final String[] PACKAGES_TO_SCAN_ENT_MGR = new String[] { "com.enovax.star.cms.kiosk.api.store.persistence" };

    @Bean
    public DataSource dataSource() {
        try {
            ComboPooledDataSource ds = new ComboPooledDataSource();

            ds.setDriverClass(driverClass);
            ds.setJdbcUrl(jdbcUrl);
            ds.setUser(jdbcUser);
            ds.setPassword(jdbcPassword);

            ds.setAcquireIncrement(c3p0acquireIncrement);
            ds.setIdleConnectionTestPeriod(c3p0idleConnectionTestPeriod);
            ds.setMaxIdleTime(c3p0maxIdleTime);
            ds.setMaxPoolSize(c3p0maxPoolSize);
            ds.setMinPoolSize(c3p0minPoolSize);
            ds.setMaxStatementsPerConnection(c3p0maxStatementsPerConnection);
            ds.setNumHelperThreads(c3p0numHelperThreads);

            return ds;
        } catch (PropertyVetoException pve) {
            log.error("Error with data source initialization.", pve);
            throw new RuntimeException(pve);
        }
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();

        bean.setDataSource(dataSource());
        bean.setPackagesToScan(PACKAGES_TO_SCAN_ENT_MGR);
        bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        final Properties jpaProps = new Properties();
        jpaProps.put("hibernate.bytecode.use_reflection_optimizer", hByteCodeUseReflectionOptimizer);
        jpaProps.put("hibernate.dialect", hDialect);
        jpaProps.put("hibernate.search.autoregister_listeners", hSearchAutoRegisterListeners);
        jpaProps.put("hibernate.show_sql", hShowSql);
        jpaProps.put("hibernate.format_sql", hFormatSql);
        bean.setJpaProperties(jpaProps);

        return bean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager mgr = new JpaTransactionManager();
        mgr.setEntityManagerFactory(entityManagerFactory);

        return mgr;
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        HibernateExceptionTranslator bean = new HibernateExceptionTranslator();
        return bean;
    }

    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory() throws Exception {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource());
        sessionBuilder.scanPackages(PACKAGES_TO_SCAN_ENT_MGR);
        return sessionBuilder.buildSessionFactory();
    }

    @Bean
    public StarTemplatingFunctions starfn() {
        return Components.getComponent(StarTemplatingFunctions.class);
    }

    @Bean
    public StarKioskTemplatingFunctions kioskfn() {
        return Components.getComponent(StarKioskTemplatingFunctions.class);
    }

    @Bean
    public DamTemplatingFunctions damfn() {
        return Components.getComponent(DamTemplatingFunctions.class);
    }

    @Bean
    public RestTemplate template() {
        return new RestTemplate();
    }

}
