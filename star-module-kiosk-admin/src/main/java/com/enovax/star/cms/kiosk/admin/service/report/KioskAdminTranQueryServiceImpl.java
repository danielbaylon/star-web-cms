package com.enovax.star.cms.kiosk.admin.service.report;

import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.commons.mgnl.definition.KioskReceiptTemplate;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminTranDao;
import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminReceiptAxData;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminReceiptCmsData;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminReceiptData;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminReceiptParam;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminTranReceipt;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportPaymentDetailResult;
import com.enovax.star.cms.kiosk.api.store.param.NetsContactlessSale;
import com.enovax.star.cms.kiosk.api.store.param.NetsSale;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType;
import com.enovax.star.cms.kiosk.api.store.service.transaction.IKioskTransactionService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.google.zxing.WriterException;

@Service
public class KioskAdminTranQueryServiceImpl implements IKioskAdminTranQueryService {

    private final static Logger log = Logger.getLogger(KioskAdminTranQueryServiceImpl.class);

    @Autowired
    private IKioskTransactionService tranService;

    @Autowired
    private IKioskAdminTranDao kioskAdminTranDao;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;

    private void setReportTranDate(KioskAdminTranQueryResult result, KioskTransactionEntity tran) {
        result.setTranDate("");
        result.setTranStartDateTime("");
        result.setTranEndDateTime("");

        if (tran.getAxCartCheckoutDateTime() != null) {
            result.setTranDate(DF_DD_MM_YYYY_HH_MM_AM_PM.format(tran.getAxCartCheckoutDateTime()));
        }
        if (tran.getBeginDateTime() != null) {
            result.setTranStartDateTime(DF_DD_MM_YYYY_HH_MM_AM_PM.format(tran.getBeginDateTime()));
        }
        if (tran.getEndDateTime() != null) {
            result.setTranEndDateTime(DF_DD_MM_YYYY_HH_MM_AM_PM.format(tran.getEndDateTime()));
        }

    }

    private void setPaymentData(KioskTransactionEntity tran, KioskAdminTranQueryResult result) {
        KioskTransactionPaymentEntity payment = tran.getPayments().stream().filter(p -> "00".equals(p.getResponseCode())).findAny().orElse(null);
        if (payment == null) {
            payment = tran.getPayments().stream().filter(p -> true).findAny().orElse(null);
        }

        if (payment != null) {
            result.setBankApprovalCode(payment.getApprovalCode());
            result.setCardNo(payment.getCardNumber());

            if (KioskCardEntryMode.CONTACTLESS.getValue().equals(payment.getEntryMode()) && KioskPaymentCardType.MASTERCARD.getValue().equals(payment.getPaymentMode())) {
                result.setPaymentType("MASTERCARD Paypass");
            } else if (KioskCardEntryMode.CONTACTLESS.getValue().equals(payment.getEntryMode()) && KioskPaymentCardType.VISA.getValue().equals(payment.getPaymentMode())) {
                result.setPaymentType("VISA Paywave");
            } else {
                result.setPaymentType(payment.getCardTypeDescription());
            }
            result.setPaymentStatus("00".equals(payment.getResponseCode()) ? "Payment Succeed" : "Payment Failed");
        } else {
            result.setBankApprovalCode("");
            result.setCardNo("");
            result.setPaymentType("");
            result.setPaymentStatus("Payment Failed");
        }
    }

    private String getDevice(KioskTransactionEntity tran) {
        String device = "";
        if (tran.getKiosk() != null) {
            device = "(" + tran.getKiosk().getDeviceNumber() + ") " + tran.getKiosk().getJcrNodeName();
        }
        return device;
    }

    @Override
    @Transactional
    public List<KioskAdminTranQueryResult> getTranQueryResult(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize) {
        List<KioskAdminTranQueryResult> queryResultList = new ArrayList<>();

        if (filter.getSortField() == null) {
            filter.setSortField("axCartCheckoutDateTime");
        }

        if (filter.getSortDirection() == null) {
            filter.setSortDirection("asc");
        }
        List<KioskTransactionEntity> tranList = kioskAdminTranDao.getTranList(filter, pageNumber, pageSize);

        for (KioskTransactionEntity tran : tranList) {
            KioskAdminTranQueryResult result = new KioskAdminTranQueryResult();

            result.setAmount(tran.getTotalAmount().toString());
            result.setAxReceiptNo(tran.getAxReceiptNumber());
            result.setAxReceiptSeq(tran.getAxReceiptId().toString());
            result.setAxCartId(tran.getAxCartId());
            result.setBrowsingPrintedDateTime("");
            this.setPaymentData(tran, result);
            result.setStarTranId(tran.getReceiptNumber());
            result.setStatus(tran.getStatus());
            result.setTicketingKioskId(this.getDevice(tran));
            result.setTrafficSource("");
            this.setReportTranDate(result, tran);
            queryResultList.add(result);
        }

        return queryResultList;
    }

    private KioskAdminReceiptParam getReceiptParam(String channel) {
        KioskAdminReceiptParam param = new KioskAdminReceiptParam();
        Node node = kioskfn.getReceiptTemplateParams(channel);
        try {
            if (node != null) {
                param.setLogo(node.getProperty(KioskReceiptTemplate.Logo.getPropertyName()).getString());
                param.setCompanyName(node.getProperty(KioskReceiptTemplate.CompanyName.getPropertyName()).getString());
                param.setAddress(node.getProperty(KioskReceiptTemplate.Address.getPropertyName()).getString());
                param.setPostalCode(node.getProperty(KioskReceiptTemplate.PostalCode.getPropertyName()).getString());
                param.setContactDetails(node.getProperty(KioskReceiptTemplate.ContactDetails.getPropertyName()).getString());
                param.setGstDetails(node.getProperty(KioskReceiptTemplate.GSTDetails.getPropertyName()).getString());
                param.setChannel(channel);
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return param;
    }

    private void setPaymentData(KioskAdminTranReceipt receipt, KioskTransactionEntity tran) {
        KioskAdminReceiptData data = receipt.getData();
        if (tran.getPayments() != null && tran.getPayments().size() > 0) {
            KioskTransactionPaymentEntity payment = tran.getPayments().stream().filter(p -> "00".equals(p.getResponseCode())).findAny().orElse(null);
            if (payment == null) {
                payment = tran.getPayments().stream().filter(p -> true).findAny().orElse(new KioskTransactionPaymentEntity());
            }
            data.getPayment().setAid(payment.getApplicationIdentifier());
            data.getPayment().setAppLabel(payment.getAppLabel());
            data.getPayment().setApprovalCode(payment.getApprovalCode());
            data.getPayment().setBatchNo(payment.getBatchNumber());
            data.getPayment().setCardHolderName(payment.getCardHolderName());
            data.getPayment().setCardLabel(payment.getCardLabel());
            data.getPayment().setCardNo(payment.getCardNumber());
            data.getPayment().setEntryMode(KioskCardEntryMode.toEnum(payment.getEntryMode()).getDescription());
            data.getPayment().setInvoicNo(payment.getInvoiceNumber());
            data.getPayment().setMerchant(tran.getKiosk().getTerminalMerchantNameAndLocation());
            data.getPayment().setMid(payment.getMerchantId());
            data.getPayment().setPaymentMode(KioskPaymentCardType.toEnum(payment.getPaymentMode()).getDescription());
            data.getPayment().setRrn(payment.getRetrivalReferenceData());
            data.getPayment().setSignatureData(payment.getSignature());
            data.getPayment().setSignatureRequired(payment.getSignatureRequired());
            data.getPayment().setTc(payment.getTerminalVerificationResponse());
            data.getPayment().setTid(payment.getTerminalId());
            data.getPayment().setTvr(payment.getTerminalVerificationResponse());
            data.getPayment().setRemainingBalance(payment.getRemainingBalance() == null ? "0.00" : NumberFormat.getCurrencyInstance().format(payment.getRemainingBalance()));
            data.getPayment().setOriginalBalance(payment.getOriginalBalance() == null ? "0.00" : NumberFormat.getCurrencyInstance().format(payment.getOriginalBalance()));
            data.getPayment().setNetsTrace(payment.getNetsTraceNumber());
            data.getPayment().setNetsReceipt(payment.getNetsReceiptNumber());

            if (KioskCardEntryMode.NETS.getValue().equals(payment.getEntryMode())) {
                data.getPayment().setNetsIssuer(JsonUtil.fromJson(payment.getPaymentData(), NetsSale.class).getNetsIssuerName());
                data.getPayment().setDateTime(JsonUtil.fromJson(payment.getPaymentData(), NetsSale.class).getDateTime());
            } else if (KioskCardEntryMode.NETS_FLASHPAY.getValue().equals(payment.getEntryMode())) {
                data.getPayment().setDateTime(JsonUtil.fromJson(payment.getPaymentData(), NetsContactlessSale.class).getDateTime());
            }
        }
    }

    private void setProductData(KioskAdminTranReceipt receipt, KioskTransactionEntity tran) {

        Map<String, List<KioskTransactionItemEntity>> topupItemMap = new HashMap<>();

        for (KioskTransactionItemEntity item : tran.getItems()) {
            String parent = item.getParentProductId();
            if (StringUtils.isNotBlank(parent)) {
                if (topupItemMap.get(parent) == null) {
                    topupItemMap.put(parent, new ArrayList<>());
                }
                topupItemMap.get(parent).add(item);
            }
        }

        Map<String, List<KioskTransactionItemEntity>> itemGroup = new HashMap<>();

        itemGroup = tran.getItems().stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getProductName));

        for (Map.Entry<String, List<KioskTransactionItemEntity>> entry : itemGroup.entrySet()) {
            String cmsProductName = entry.getKey();
            List<KioskTransactionItemEntity> items = entry.getValue();
            KioskAdminReceiptCmsData cms = new KioskAdminReceiptCmsData();
            cms.setName(cmsProductName);
            receipt.getData().getCmsList().add(cms);
            for (KioskTransactionItemEntity item : items) {
                KioskAdminReceiptAxData ax = new KioskAdminReceiptAxData();
                ax.setAmount(NumberFormat.getCurrencyInstance().format(item.getAmount()));
                ax.setName(item.getAxProductName());
                ax.setQty(item.getQuantity().toString());
                ax.setSubTotalAmount(NumberFormat.getCurrencyInstance().format(item.getSubTotal()));

                cms.getAxDataList().add(ax);
                List<KioskTransactionItemEntity> topups = topupItemMap.get(item.getListingId());
                if (topups != null) {
                    for (KioskTransactionItemEntity topup : topups) {
                        KioskAdminReceiptAxData topupAx = new KioskAdminReceiptAxData();
                        topupAx.setAmount(NumberFormat.getCurrencyInstance().format(topup.getAmount()));
                        topupAx.setName(topup.getAxProductName());
                        topupAx.setQty(topup.getQuantity().toString());
                        topupAx.setSubTotalAmount(NumberFormat.getCurrencyInstance().format(topup.getSubTotal()));

                        cms.getAxDataList().add(topupAx);
                    }
                }

            }
        }

    }

    @Override
    @Transactional
    public KioskAdminTranReceipt getReceiptByAxCartId(String channel, String axCartId) {
        KioskAdminTranReceipt receipt = new KioskAdminTranReceipt();
        KioskAdminReceiptData data = receipt.getData();

        receipt.setParam(getReceiptParam(channel));
        KioskTransactionEntity tran = tranService.getTransactionByAxCartId(axCartId);

        data.setDate(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.format(tran.getAxCartCheckoutDateTime()));
        data.setKioskId(tran.getKiosk().getJcrNodeName());
        data.setLocation(tran.getKiosk().getLocation());
        data.setAxReceiptId(tran.getAxReceiptId().toString());
        data.setAxReceiptNo(tran.getAxReceiptNumber());
        data.setTotalAmount(NumberFormat.getCurrencyInstance().format(tran.getTotalAmount()));
        data.setTotalGstAmount(NumberFormat.getCurrencyInstance().format(tran.getGstAmount()));
        data.setTransId(tran.getReceiptNumber());
        data.setQrCodeData("");
        setPaymentData(receipt, tran);
        setProductData(receipt, tran);
        receipt.getData().setAllTicketsPrinted(Boolean.TRUE);
        if (!tran.getAllTicketsPrinted()) {
            try {
                receipt.getData().setAllTicketsPrinted(Boolean.FALSE);
                receipt.getData().setQrCodeData(QRCodeGenerator.toBase64(tran.getAxReceiptNumber(), 150, 150));
            } catch (WriterException | IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return receipt;
    }

    private String getNextDay(String date) {
        Date end = new Date();
        try {
            end = KioskAdminDateUtil.DF_DD_MM_YYYY.parse(date);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        DateTime nextDay = new DateTime(end.getTime()).plusDays(1);
        return KioskAdminDateUtil.DF_DD_MM_YYYY.format(nextDay.toDate());
    }

    private KioskAdminTranQueryFilter getQueryFilter(String channel, String date, String kioskId) {
        KioskAdminTranQueryFilter filter = new KioskAdminTranQueryFilter();
        filter.setChannel(channel);
        filter.setPaymentStatus("ALL");
        filter.setPaymentType("ALL");
        filter.setTicketingKioskId(kioskId);
        filter.setTranEndDate(this.getNextDay(date));
        filter.setTranEndTime("00:00");
        filter.setTranStartDate(date);
        filter.setTranStartTime("00:00");
        return filter;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportPaymentDetailResult> getPaymentDetails(String channel, String tranDate, String kioskId) {

        List<KioskAdminZReportPaymentDetailResult> details = new ArrayList<>();
        KioskAdminTranQueryFilter filter = this.getQueryFilter(channel, tranDate, kioskId);
        filter.setPaymentStatus("S");
        List<KioskTransactionEntity> trans = this.kioskAdminTranDao.getTranList(filter);
        List<KioskTransactionPaymentEntity> payments = new ArrayList<>();

        for (KioskTransactionEntity tran : trans) {
            KioskTransactionPaymentEntity payment = tran.getPayments().stream().filter(p -> p.getStatus()).findAny().orElse(null);
            if (payment != null) {
                payments.add(payment);
            }
        }

        Map<String, List<KioskTransactionPaymentEntity>> paymentModeGroup = payments.stream().collect(Collectors.groupingBy(KioskTransactionPaymentEntity::getPaymentMode));

        KioskAdminZReportPaymentDetailResult detail = null;
        for (Map.Entry<String, List<KioskTransactionPaymentEntity>> entry : paymentModeGroup.entrySet()) {
            List<KioskTransactionPaymentEntity> groupedPayments = entry.getValue();

            if (KioskPaymentCardType.VISA.getValue().equals(entry.getKey()) || KioskPaymentCardType.MASTERCARD.getValue().equals(entry.getKey())) {
                Double sumContactless = groupedPayments.stream().filter(p -> KioskCardEntryMode.CONTACTLESS.getValue().equals(p.getEntryMode()))
                        .mapToDouble(KioskTransactionPaymentEntity::getAmountInDouble).sum();
                Double sumNoneContactless = groupedPayments.stream().filter(p -> !KioskCardEntryMode.CONTACTLESS.getValue().equals(p.getEntryMode()))
                        .mapToDouble(KioskTransactionPaymentEntity::getAmountInDouble).sum();

                if (sumNoneContactless != 0) {
                    detail = new KioskAdminZReportPaymentDetailResult();
                    detail.setMode(KioskPaymentCardType.toEnum(entry.getKey()).getDescription());
                    detail.setAmount(sumNoneContactless.toString());
                    details.add(detail);
                }

                if (sumContactless != 0) {
                    detail = new KioskAdminZReportPaymentDetailResult();
                    detail.setMode(KioskPaymentCardType.toEnum(entry.getKey()).getDescription() + " Paywave");
                    if (KioskPaymentCardType.MASTERCARD.getValue().equals(entry.getKey())) {
                        detail.setMode(KioskPaymentCardType.toEnum(entry.getKey()).getDescription() + " Paypass");
                    }
                    detail.setAmount(sumContactless.toString());
                    details.add(detail);
                }

            } else {
                Double sum = groupedPayments.stream().mapToDouble(KioskTransactionPaymentEntity::getAmountInDouble).sum();
                detail = new KioskAdminZReportPaymentDetailResult();
                detail.setMode(KioskPaymentCardType.toEnum(entry.getKey()).getDescription());
                detail.setAmount(sum.toString());
                details.add(detail);
            }
        }

        return details;
    }

    @Override
    @Transactional
    public List<KioskAdminTranQueryResult> getTranQueryResult(KioskAdminTranQueryFilter filter) {
        return this.getTranQueryResult(filter, null, null);
    }

}
