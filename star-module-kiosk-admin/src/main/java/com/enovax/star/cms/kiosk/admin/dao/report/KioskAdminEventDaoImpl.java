package com.enovax.star.cms.kiosk.admin.dao.report;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminEventQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;

@Repository
public class KioskAdminEventDaoImpl implements IKioskAdminEventDao {

    private static final Logger log = LoggerFactory.getLogger(KioskAdminEventDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<KioskEventEntity> getEventList(String channel, KioskAdminEventQueryFilter filter, Integer pageNumber, Integer pageSize) {
        log.info("pageNumber : " + pageNumber + ", pageSize : " + pageSize);

        String statement = " select distinct e from KioskEventEntity e ";

        statement += " where e.apiChannel = :channelCode ";
        statement += " and e.kioskName = :kioskId ";
        statement += " and e.createts between :startDate and :endDate ";
        statement += " and e.eventType = :eventType ";
        statement += " and e.eventName = :eventName ";

        if (filter.getSortField() != null && filter.getSortDirection() != null) {
            String field = "logonuser";
            if ("username".equals(filter.getSortField())) {
                field = "logonuser";
            } else if ("datetime".equals(filter.getSortField())) {
                field = "createts";
            }
            statement += " order by t." + field + " " + filter.getSortDirection();
        }

        Query query = sessionFactory.getCurrentSession().createQuery(statement);
        query.setParameter("channelCode", filter.getChannel());
        query.setParameter("kioskId", filter.getKioskId());
        query.setDate("startDate", filter.getStartDate());
        query.setDate("endDate", filter.getEndDate());
        query.setParameter("eventType", filter.getEventType());
        query.setParameter("eventName", filter.getEventName());

        if (pageNumber != null && pageSize != null) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
        }

        return query.list();
    }

    @Override
    public List<KioskEventEntity> getEventList(String channel, KioskAdminEventQueryFilter filter) {
        return this.getEventList(channel, filter, null, null);
    }

}
