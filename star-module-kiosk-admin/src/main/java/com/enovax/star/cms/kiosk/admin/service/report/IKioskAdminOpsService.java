package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportLoginHistoryResult;

public interface IKioskAdminOpsService {
    
    List<KioskAdminZReportLoginHistoryResult> getOpsUserLoginHistory(String channel, String tranDate, String kioskId);

    List<KioskAdminZReportLoginHistoryResult> getOpsUserLoginHistory(String channel, String tranDate, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection);

}
