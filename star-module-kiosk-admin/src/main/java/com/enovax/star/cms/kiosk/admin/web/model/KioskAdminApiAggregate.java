package com.enovax.star.cms.kiosk.admin.web.model;

public class KioskAdminApiAggregate {

    private KioskAdminApiAggTranTotalAmount amount;

    public KioskAdminApiAggTranTotalAmount getAmount() {
        return amount;
    }

    public void setAmount(KioskAdminApiAggTranTotalAmount amount) {
        this.amount = amount;
    }

}
