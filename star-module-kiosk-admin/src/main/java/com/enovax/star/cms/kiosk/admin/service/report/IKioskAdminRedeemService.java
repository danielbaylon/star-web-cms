package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRedeemHistoryResult;

public interface IKioskAdminRedeemService {

    List<KioskAdminZReportRedeemHistoryResult> getRedeemHistory(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection);

    List<KioskAdminZReportRedeemHistoryResult> getRedeemHistory(String channel, String date, String kioskId);
}
