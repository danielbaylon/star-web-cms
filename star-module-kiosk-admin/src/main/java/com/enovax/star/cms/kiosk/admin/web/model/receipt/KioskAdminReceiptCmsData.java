package com.enovax.star.cms.kiosk.admin.web.model.receipt;

import java.util.ArrayList;
import java.util.List;

public class KioskAdminReceiptCmsData {

    private String name;

    private List<KioskAdminReceiptAxData> axDataList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<KioskAdminReceiptAxData> getAxDataList() {
        return axDataList;
    }

    public void setAxDataList(List<KioskAdminReceiptAxData> axDataList) {
        this.axDataList = axDataList;
    }

}
