package com.enovax.star.cms.kiosk.admin.web.model.report;

import java.util.ArrayList;
import java.util.List;

public class KioskAdminZReportRequestFilter {

    private String channel;

    private String tranDate;

    private String kioskId;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getKioskId() {
        return kioskId;
    }

    public void setKioskId(String kioskId) {
        this.kioskId = kioskId;
    }

}
