package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminZReportRedeemHistoryResult {

    private String starTranId;

    private String tranDate;

    private String pinCode;

    private String itemCode;

    private String itemName;

    private String bookedQty;

    private String issuedQty;

    public String getStarTranId() {
        return starTranId;
    }

    public void setStarTranId(String starTranId) {
        this.starTranId = starTranId;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getBookedQty() {
        return bookedQty;
    }

    public void setBookedQty(String bookedQty) {
        this.bookedQty = bookedQty;
    }

    public String getIssuedQty() {
        return issuedQty;
    }

    public void setIssuedQty(String issuedQty) {
        this.issuedQty = issuedQty;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}