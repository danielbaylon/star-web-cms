package com.enovax.star.cms.kiosk.admin.web.model;

import com.enovax.star.cms.commons.model.api.ApiResult;

public class KioskAdminApiResult<T> extends ApiResult {

    private KioskAdminApiAggregate aggregate = new KioskAdminApiAggregate();

    public KioskAdminApiAggregate getAggregate() {
        return aggregate;
    }

    public void setAggregate(KioskAdminApiAggregate aggregate) {
        this.aggregate = aggregate;
    }

}
