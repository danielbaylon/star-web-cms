package com.enovax.star.cms.kiosk.admin.service.report;

import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY;
import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM;
import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS;
import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_HH_MM_SS;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminLoadTicketEventDao;
import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminTicketDao;
import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminTranDao;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminEventQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTicketQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketMgtEventResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSoldResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSummaryResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketTokenList;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTicketPrintStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Service
public class KioskAdminTicketServiceImpl implements IKioskAdminTicketService {

    private static final Logger log = Logger.getLogger(KioskAdminTicketServiceImpl.class);

    @Autowired
    private IKioskAdminTranDao tranDao;

    @Autowired
    private IKioskAdminLoadTicketEventDao eventDao;

    @Autowired
    private IKioskAdminTicketDao ticketDao;

    private String getNextDay(String date) {
        Date end = new Date();
        try {
            end = DF_DD_MM_YYYY.parse(date);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        DateTime nextDay = new DateTime(end.getTime()).plusDays(1);
        return DF_DD_MM_YYYY.format(nextDay.toDate());
    }

    private KioskAdminTranQueryFilter getQueryFilter(String channel, String date, String kioskId) {
        KioskAdminTranQueryFilter filter = new KioskAdminTranQueryFilter();
        filter.setChannel(channel);
        filter.setPaymentStatus("ALL");
        filter.setPaymentType("ALL");
        filter.setTicketingKioskId(StringUtils.isBlank(kioskId) ? "ALL" : kioskId);
        filter.setTranEndDate(this.getNextDay(date));
        filter.setTranEndTime("00:00");
        filter.setTranStartDate(date);
        filter.setTranStartTime("00:00");
        filter.getTranTypeList().add(KioskTransactionType.PURCHASE.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2B_REDEMPTION.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2C_REDEMPTION.getCode());
        return filter;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportTicketSummaryResult> getTicketIssuanceSummary(String channel, String date, String kioskId) {
        log.info("channel : " + channel + ", date : " + date + ", kiosk id : " + kioskId);

        List<KioskTransactionEntity> tranList = tranDao.getTranList(getQueryFilter(channel, date, kioskId));
        log.info("transactio list size" + tranList.size());

        List<KioskTransactionEntity> allPrinted = tranList.stream().filter(t -> t.getAllTicketsPrinted()).collect(Collectors.toList());
        Integer issuedQty = allPrinted.stream().map(KioskTransactionEntity::getTickets).filter(rs -> rs != null).mapToInt(Set::size).sum();

        List<KioskTransactionEntity> nonePrinted = tranList.stream().filter(t -> !t.getAllTicketsPrinted()).collect(Collectors.toList());

        List<KioskTicketEntity> tickets = new ArrayList<>();

        for (KioskTransactionEntity tran : nonePrinted) {
            tickets.addAll(tran.getTickets());
        }

        Integer rejectedQty = tickets.stream().filter(t -> KioskTicketPrintStatus.NOTPRINTED.getCode().equals(t.getStatus())).collect(Collectors.toList()).size();

        Integer voidedQty = tickets.stream().filter(t -> KioskTicketPrintStatus.BLACKLISTED.getCode().equals(t.getStatus())).collect(Collectors.toList()).size();

        List<KioskAdminZReportTicketSummaryResult> result = new ArrayList<>();

        KioskAdminZReportTicketSummaryResult issued = new KioskAdminZReportTicketSummaryResult();
        issued.setLabel("Qty of Issued Paper Ticket : ");
        issued.setContent(issuedQty.toString());
        result.add(issued);

        KioskAdminZReportTicketSummaryResult rejected = new KioskAdminZReportTicketSummaryResult();
        rejected.setLabel("Qty of Rejected Paper Ticket : ");
        rejected.setContent(rejectedQty.toString());
        result.add(rejected);

        KioskAdminZReportTicketSummaryResult voided = new KioskAdminZReportTicketSummaryResult();
        voided.setLabel("Qty of Voided Paper Ticket : ");
        voided.setContent(voidedQty.toString());
        result.add(voided);

        return result;
    }

    private Date getBeginDate(String date) {
        Date beginDate = null;
        try {
            beginDate = DF_DD_MM_YYYY_HH_MM_SS.parse(date + " 00:00:00");
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return beginDate;

    }

    private Date getEndDate(String date) {
        Date endDate = null;
        try {
            endDate = DF_DD_MM_YYYY_HH_MM_SS.parse(date + " 00:00:00");
            endDate = new DateTime(endDate).plusDays(1).toDate();
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return endDate;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportTicketMgtEventResult> getTicketMgtEvent(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection) {
        // load paper ticket info
        log.info("channel : " + channel + ", date : " + date + ", kiosk id : " + kioskId);
        List<KioskAdminZReportTicketMgtEventResult> events = new ArrayList<>();

        if (this.getBeginDate(date) != null && this.getEndDate(date) != null) {
            KioskAdminEventQueryFilter filter = new KioskAdminEventQueryFilter();
            filter.setChannel(channel);
            filter.setEventName("OPS");
            filter.setEventName("load paper ticket info");
            filter.setKioskId(kioskId);
            filter.setStartDate(this.getBeginDate(date));
            filter.setEndDate(this.getEndDate(date));
            filter.setSortDirection(sortDirection);
            filter.setSortField(sortField);
            List<KioskLoadPaperTicketInfoEntity> tickets = eventDao.getEventList(channel, filter, pageNumber, pageSize);

            KioskAdminZReportTicketMgtEventResult event = null;
            for (KioskLoadPaperTicketInfoEntity ticket : tickets) {

                if (StringUtils.isNotBlank(ticket.getQtyLoaded()) && !"0".equals(ticket.getQtyLoaded())) {
                    event = new KioskAdminZReportTicketMgtEventResult();
                    event.setDate(DF_DD_MM_YYYY_HH_MM_SS.format(new DateTime(ticket.getCreatets()).toDate()));
                    event.setEvent("LOAD TICKET EVENT");
                    event.setQty(ticket.getQtyLoaded());
                    event.setUser(ticket.getLogonuser());
                    events.add(event);
                }
                if (StringUtils.isNotBlank(ticket.getQtyUnloaded()) && !"0".equals(ticket.getQtyUnloaded())) {
                    event = new KioskAdminZReportTicketMgtEventResult();
                    event.setDate(DF_DD_MM_YYYY_HH_MM_SS.format(new DateTime(ticket.getCreatets()).toDate()));
                    event.setEvent("UNLOAD TICKET EVENT");
                    event.setQty(ticket.getQtyUnloaded());
                    event.setUser(ticket.getLogonuser());
                    events.add(event);
                }
            }
        }
        return events;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportTicketMgtEventResult> getTicketMgtEvent(String channel, String date, String kioskId) {
        return this.getTicketMgtEvent(channel, date, kioskId, null, null, null, null);
    }

    @Override
    @Transactional
    public List<KioskAdminZReportTicketSoldResult> getTicketSold(String channel, String date, String kioskId) {

        log.info("channel : " + channel + ", date : " + date + ", kiosk id : " + kioskId);
        List<KioskAdminZReportTicketSoldResult> resultList = new ArrayList<>();

        KioskAdminTranQueryFilter filter = getQueryFilter(channel, date, kioskId);
        filter.getTranTypeList().add(KioskTransactionType.PURCHASE.getCode());

        List<KioskTransactionEntity> tranList = tranDao.getTranList(filter);
        log.info("transactio list size" + tranList.size());
        List<KioskTicketEntity> soldTickets = new ArrayList<>();
        List<KioskTransactionItemEntity> soldItems = new ArrayList<>();
        for (KioskTransactionEntity t : tranList) {
            KioskTransactionPaymentEntity payment = t.getPayments().stream().filter(p -> p.getStatus()).findAny().orElse(null);
            if (payment != null) {
                soldTickets.addAll(t.getTickets());
                soldItems.addAll(t.getItems());
            }
        }

        Map<String, List<KioskTicketEntity>> ticketGroup = soldTickets.stream().collect(Collectors.groupingBy(KioskTicketEntity::getItemId));
        Map<String, List<KioskTransactionItemEntity>> itemGroup = soldItems.stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getItemId));
        if (ticketGroup.size() > 0 && itemGroup.size() > 0) {
            for (Map.Entry<String, List<KioskTicketEntity>> entry : ticketGroup.entrySet()) {
                List<KioskTicketEntity> tickets = entry.getValue();

                KioskAdminZReportTicketSoldResult result = new KioskAdminZReportTicketSoldResult();
                result.setItemCode(entry.getKey());
                result.setItemName(tickets.stream().findAny().get().getAxProductName());
                result.setQty(String.valueOf(tickets.size()));
                result.setTotalAmount("0.00");
                if (itemGroup.get(entry.getKey()) != null) {
                    result.setTotalAmount(String.valueOf(itemGroup.get(entry.getKey()).stream().mapToDouble(i -> i.getSubTotal().doubleValue()).sum()));
                }
                resultList.add(result);
            }
        }

        return resultList;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportTicketSoldResult> getTicketSold(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection) {

        List<KioskAdminZReportTicketSoldResult> allList = this.getTicketSold(channel, date, kioskId);
        List<KioskAdminZReportTicketSoldResult> pagedList = new ArrayList<>();
        int start = (pageNumber - 1) * pageSize;

        for (int i = start; i < (start + pageSize); i++) {
            if (allList.size() - i > 0) {
                pagedList.add(allList.get(i));
            }
        }
        return pagedList;
    }

    @Override
    @Transactional
    public List<String> getTicketNoList(String channel, String startDate, String endDate, String kioskId) {
        List<String> ticketNoList = new ArrayList<>();
        try {

            KioskAdminTranQueryFilter filter = this.getQueryFilter(channel, DF_DD_MM_YYYY.format(new Date()), kioskId);

            Date start = DF_DD_MM_YYYY_HH_MM_AM_PM.parse(startDate);
            Date end = DF_DD_MM_YYYY_HH_MM_AM_PM.parse(endDate);

            filter.setTranStartDate(DF_DD_MM_YYYY.format(start));
            filter.setTranStartTime(DF_HH_MM_SS.format(start));
            filter.setTranEndDate(DF_DD_MM_YYYY.format(end));
            filter.setTranEndTime(DF_HH_MM_SS.format(end));

            List<KioskTransactionEntity> tranList = tranDao.getTranList(filter);

            for (KioskTransactionEntity tran : tranList) {
                for (KioskTicketEntity ticket : tran.getTickets()) {
                    ticketNoList.add(ticket.getTicketNumber());
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ticketNoList;
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @Override
    @Transactional
    public List<KioskAdminTicketQueryResult> getAllTickets(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize) {
        List<KioskAdminTicketQueryResult> data = new ArrayList<>();
        try {

            List<KioskTicketEntity> ticketList = ticketDao.getTicket(filter, pageNumber, pageSize);
            ticketList = ticketList.stream().filter(distinctByKey(KioskTicketEntity::getId)).collect(Collectors.toList());
            for (KioskTicketEntity ticket : ticketList) {
                KioskTicketTokenList token = JsonUtil.fromJson(ticket.getTokenData(), KioskTicketTokenList.class);
                KioskAdminTicketQueryResult result = new KioskAdminTicketQueryResult();
                result.setActivationDate(token.getValidityStartDate());
                result.setExpiryDate(token.getValidityEndDate());
                result.setIssued(KioskTicketPrintStatus.PRINTED.getCode().equals(ticket.getStatus()) ? "Yes" : "No");
                result.setItemName(token.getDisplayName());
                result.setLocation(ticket.getTran().getKiosk().getLocation());
                result.setPinCode(ticket.getTran().getRedemptionPinCode());
                // can't get price from JCR as for redemption we may not
                // have the AX product at all
                if (token.getPublishPrice().indexOf("SGD") != -1) {
                    result.setPrice(token.getPublishPrice().substring(0, token.getPublishPrice().indexOf("SGD")));
                } else {
                    result.setPrice(token.getPublishPrice());
                }

                result.setPrinted((KioskTicketPrintStatus.BLACKLISTED.getCode().equals(ticket.getStatus()) || KioskTicketPrintStatus.PRINTED.getCode().equals(ticket.getStatus()))
                        ? "Yes" : "No");
                result.setRemarks("");
                result.setReceiptSeq(ticket.getTran().getAxReceiptId().toString());
                result.setStarTranId(ticket.getTran().getReceiptNumber());
                result.setTicketDescription(token.getTicketType());
                result.setTicketNo(ticket.getTicketNumber());
                result.setTicketPrintedKioskId(ticket.getTran().getKiosk().getJcrNodeName());
                result.setTimeTicketPrinted(DF_DD_MM_YYYY_HH_MM_AM_PM.format(ticket.getCreatedDateTime()));
                result.setValidityPeriod(token.getTicketValidity());
                data.add(result);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return data;
    }

    @Override
    @Transactional
    public List<KioskAdminTicketQueryResult> getAllTickets(KioskAdminTranQueryFilter filter) {
        return this.getAllTickets(filter, null, null);
    }

}
