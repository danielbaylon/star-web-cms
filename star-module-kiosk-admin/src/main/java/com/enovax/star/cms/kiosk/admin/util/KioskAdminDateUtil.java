package com.enovax.star.cms.kiosk.admin.util;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

@Component
public class KioskAdminDateUtil {

    public static final SimpleDateFormat DF_DD_MM_YYYY_HH_MM_AM_PM = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

    public static final SimpleDateFormat DF_DD_MM_YYYY_HH_MM_SS = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static final SimpleDateFormat DF_HH_MM_SS = new SimpleDateFormat("HH:mm:ss");

    public static final SimpleDateFormat DF_DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");

}
