package com.enovax.star.cms.kiosk.admin.service.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTicketQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportBasicInfoResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportPaymentDetailResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRedeemHistoryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRequestFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketMgtEventResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSoldResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSummaryResult;

@Service
public class KioskAdminExportServiceImpl implements IKioskAdminExportService {

    private final static Logger log = Logger.getLogger(KioskAdminExportServiceImpl.class);

    @Autowired
    private IKioskAdminTranQueryService tranQueryService;

    @Autowired
    private IKioskAdminReceiptService receiptService;

    @Autowired
    private IKioskAdminTicketService ticketService;

    @Autowired
    private IKioskAdminRedeemService redeemService;

    @Override
    public OutputStream generateTranQueryExcelReport(KioskAdminTranQueryFilter filter) {

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        Workbook wb = new XSSFWorkbook();
        try {

            Sheet s = wb.createSheet("Transaction Query");

            Integer row = 0;
            Row r = s.createRow(row);

            Integer cell = 0;
            Cell c = r.createCell(cell);

            c.setCellValue("Kiosk Transaction Query Report");

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Start Date : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTranStartDate());
            c = r.createCell(++cell);
            c.setCellValue("Start Time : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTranStartTime());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("End Date : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTranEndDate());
            c = r.createCell(++cell);
            c.setCellValue("End Time : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTranEndTime());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Trans ID: ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getStarTranId());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Payment Status: ");
            c = r.createCell(++cell);
            c.setCellValue("ALL".equals(filter.getPaymentStatus()) ? "[All Payment Status]" : ("S".equals(filter.getPaymentStatus())) ? "Payment Succeed" : "Payment Failed");

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Kiosk ID: ");
            c = r.createCell(++cell);
            c.setCellValue("ALL".equals(filter.getTicketingKioskId()) ? "[All Hosts]" : filter.getTicketingKioskId());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Payment Type: ");
            c = r.createCell(++cell);
            c.setCellValue("ALL".equals(filter.getPaymentType()) ? "[All Payment Modes]"
                    : ("V".equals(filter.getPaymentType())) ? "VISA"
                            : ("M".equals(filter.getPaymentType())) ? "MASTERCARD" : ("S".equals(filter.getPaymentType())) ? "NETS PIN DEBIT" : "NETS FLASHPAY(NFP)");

            row++; // blank row

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Transaction Date");
            c = r.createCell(++cell);
            c.setCellValue("Receipt ID");
            c = r.createCell(++cell);
            c.setCellValue("Transaction ID");
            c = r.createCell(++cell);
            c.setCellValue("Receipt NO");
            c = r.createCell(++cell);
            c.setCellValue("Payment Status");
            c = r.createCell(++cell);
            c.setCellValue("Amount");
            c = r.createCell(++cell);
            c.setCellValue("Payment Type");
            c = r.createCell(++cell);
            c.setCellValue("Card NO");
            c = r.createCell(++cell);
            c.setCellValue("Bank Approval Code");
            c = r.createCell(++cell);
            c.setCellValue("Kiosk ID");
            c = r.createCell(++cell);
            c.setCellValue("Transactioin Start Date/Time");
            c = r.createCell(++cell);
            c.setCellValue("Transaction End Date/Time");

            List<KioskAdminTranQueryResult> result = tranQueryService.getTranQueryResult(filter);
            for (KioskAdminTranQueryResult tran : result) {
                r = s.createRow(++row);
                cell = -1;
                c = r.createCell(++cell);
                c.setCellValue(tran.getTranDate());
                c = r.createCell(++cell);
                c.setCellValue(tran.getAxReceiptNo());
                c = r.createCell(++cell);
                c.setCellValue(tran.getStarTranId());
                c = r.createCell(++cell);
                c.setCellValue(tran.getAxReceiptSeq());
                c = r.createCell(++cell);
                c.setCellValue(tran.getPaymentStatus());
                c = r.createCell(++cell);
                c.setCellValue(NumberFormat.getCurrencyInstance().format(new BigDecimal(tran.getAmount())));
                c = r.createCell(++cell);
                c.setCellValue(tran.getPaymentType());
                c = r.createCell(++cell);
                c.setCellValue(tran.getCardNo());
                c = r.createCell(++cell);
                c.setCellValue(tran.getBankApprovalCode());
                c = r.createCell(++cell);
                c.setCellValue(tran.getTicketingKioskId());
                c = r.createCell(++cell);
                c.setCellValue(tran.getTranStartDateTime());
                c = r.createCell(++cell);
                c.setCellValue(tran.getTranEndDateTime());

            }

            for (int i = 0; i <= cell; i++) {
                s.autoSizeColumn(i);
            }

            wb.write(outByteStream);
            wb.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return outByteStream;
    }

    @Override
    public OutputStream generateZReportExcelReport(KioskAdminZReportRequestFilter filter) {
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        Workbook wb = new XSSFWorkbook();
        try {

            String channel = filter.getChannel();
            String tranDate = filter.getTranDate();
            String kioskId = filter.getKioskId();

            Sheet s = wb.createSheet("Z Report");

            Integer row = 0;
            Row r = s.createRow(row);

            Integer cell = 0;
            Cell c = r.createCell(cell);

            c.setCellValue("Kiosk Z Report");

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Date : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTranDate());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(++cell);
            c.setCellValue("Kiosk ID : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getKioskId());

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Basic Information: ");

            List<KioskAdminZReportBasicInfoResult> basicInfoResult = receiptService.getStartEndReceiptNumber(channel, tranDate, kioskId);

            for (KioskAdminZReportBasicInfoResult basicInfo : basicInfoResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(basicInfo.getLabel());
                c = r.createCell(++cell);
                c.setCellValue(basicInfo.getContent());
            }

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Ticket Issuance Summary");
            List<KioskAdminZReportTicketSummaryResult> ticketSummaryResult = ticketService.getTicketIssuanceSummary(channel, tranDate, kioskId);
            for (KioskAdminZReportTicketSummaryResult ticketSummary : ticketSummaryResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(ticketSummary.getLabel());
                c = r.createCell(++cell);
                c.setCellValue(ticketSummary.getContent());
            }

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Ticket Management Event");

            r = s.createRow(++row);
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Event");
            c = r.createCell(++cell);
            c.setCellValue("Qty of Ticket");
            c = r.createCell(++cell);
            c.setCellValue("Done On");
            c = r.createCell(++cell);
            c.setCellValue("Done By");

            List<KioskAdminZReportTicketMgtEventResult> ticketMgtEventResult = ticketService.getTicketMgtEvent(channel, tranDate, kioskId);
            for (KioskAdminZReportTicketMgtEventResult ticketMgtEvent : ticketMgtEventResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(ticketMgtEvent.getEvent());
                c = r.createCell(++cell);
                c.setCellValue(ticketMgtEvent.getQty());
                c = r.createCell(++cell);
                c.setCellValue(ticketMgtEvent.getDate());
                c = r.createCell(++cell);
                c.setCellValue(ticketMgtEvent.getUser());
            }

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Payment Detail");
            r = s.createRow(++row);
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Payment Mode");
            c = r.createCell(++cell);
            c.setCellValue("Amount");

            List<KioskAdminZReportPaymentDetailResult> paymentDetailResult = this.tranQueryService.getPaymentDetails(channel, tranDate, kioskId);
            for (KioskAdminZReportPaymentDetailResult paymentDetail : paymentDetailResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(paymentDetail.getMode());
                c = r.createCell(++cell);
                c.setCellValue(NumberFormat.getCurrencyInstance().format(new BigDecimal(paymentDetail.getAmount())));
            }
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Total Collection:");
            c = r.createCell(++cell);
            c.setCellValue(NumberFormat.getCurrencyInstance().format(paymentDetailResult.stream().mapToDouble(p -> new Double(p.getAmount())).sum()));

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Transaction History");

            r = s.createRow(++row);
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Transaction ID");
            c = r.createCell(++cell);
            c.setCellValue("Receipt#");
            c = r.createCell(++cell);
            c.setCellValue("Date/Time");
            c = r.createCell(++cell);
            c.setCellValue("Transaction Amount");

            DateTime searchDate = DateTime.now();
            try {
                searchDate = new DateTime(KioskAdminDateUtil.DF_DD_MM_YYYY.parse(tranDate));
            } catch (ParseException e) {
                log.error(e.getMessage(), e);
            }
            KioskAdminTranQueryFilter requestFilter = new KioskAdminTranQueryFilter();
            requestFilter.setChannel(channel);
            requestFilter.setAxReceiptNo("");
            requestFilter.setPaymentStatus("S");
            requestFilter.setPaymentType("ALL");
            requestFilter.setStarTranId("");
            requestFilter.setTicketingKioskId(kioskId);
            requestFilter.setTranEndDate(KioskAdminDateUtil.DF_DD_MM_YYYY.format(searchDate.plusDays(1).toDate()));
            requestFilter.setTranEndTime("00:00");
            requestFilter.setTranStartDate(tranDate);
            requestFilter.setTranStartTime("00:00");

            List<KioskAdminTranQueryResult> tranResult = tranQueryService.getTranQueryResult(requestFilter);
            for (KioskAdminTranQueryResult tran : tranResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(tran.getStarTranId());
                c = r.createCell(++cell);
                c.setCellValue(tran.getAxReceiptSeq());
                c = r.createCell(++cell);
                c.setCellValue(tran.getTranDate());
                c = r.createCell(++cell);
                c.setCellValue(NumberFormat.getCurrencyInstance().format(new BigDecimal(tran.getAmount())));

            }

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Redemption History");

            r = s.createRow(++row);
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Transaction ID");
            c = r.createCell(++cell);
            c.setCellValue("Date/Time");
            c = r.createCell(++cell);
            c.setCellValue("Item Code");
            c = r.createCell(++cell);
            c.setCellValue("Item Name");
            c = r.createCell(++cell);
            c.setCellValue("Reserved");
            c = r.createCell(++cell);
            c.setCellValue("Issued");

            List<KioskAdminZReportRedeemHistoryResult> redeemHistoryReuslt = redeemService.getRedeemHistory(channel, tranDate, kioskId);
            for (KioskAdminZReportRedeemHistoryResult redeemHistory : redeemHistoryReuslt) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getStarTranId());
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getTranDate());
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getItemCode());
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getItemName());
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getBookedQty());
                c = r.createCell(++cell);
                c.setCellValue(redeemHistory.getIssuedQty());
            }

            r = s.createRow(++row);

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Ticket Sold");

            r = s.createRow(++row);
            cell = -1;
            c = r.createCell(++cell);
            c.setCellValue("Item Code");
            c = r.createCell(++cell);
            c.setCellValue("Item Name");
            c = r.createCell(++cell);
            c.setCellValue("Qty");
            c = r.createCell(++cell);
            c.setCellValue("Total Amout");
            List<KioskAdminZReportTicketSoldResult> soldResult = ticketService.getTicketSold(channel, tranDate, kioskId);

            for (KioskAdminZReportTicketSoldResult sold : soldResult) {
                cell = -1;
                r = s.createRow(++row);
                c = r.createCell(++cell);
                c.setCellValue(sold.getItemCode());
                c = r.createCell(++cell);
                c.setCellValue(sold.getItemName());
                c = r.createCell(++cell);
                c.setCellValue(sold.getQty());
                c = r.createCell(++cell);
                c.setCellValue(NumberFormat.getCurrencyInstance().format(new BigDecimal(sold.getTotalAmount())));

            }

            for (int i = 0; i <= 100; i++) {
                s.autoSizeColumn(i);
            }

            wb.write(outByteStream);
            wb.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return outByteStream;
    }

    @Override
    public OutputStream generateTicketQueryExcelReport(KioskAdminTranQueryFilter filter) {

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        Workbook wb = new XSSFWorkbook();
        try {

            Sheet s = wb.createSheet("Ticket Query Report");

            Integer row = 0;
            Row r = s.createRow(row);

            Integer cell = 0;
            Cell c = r.createCell(cell);

            c.setCellValue("Kiosk Ticket Query Report");

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Start Date : ");
            c = r.createCell(++cell);
            c.setCellValue(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM
                    .format(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(filter.getTranStartDate() + " " +filter.getTranStartTime() + ":00")));
            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("End Date : ");
            c = r.createCell(++cell);
            c.setCellValue(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM
                    .format(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(filter.getTranEndDate() + " " + filter.getTranEndTime() + ":00")));

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Trans ID : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getStarTranId());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Kiosk ID : ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTicketingKioskId());

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Ticket No: ");
            c = r.createCell(++cell);
            c.setCellValue(filter.getTicketNo());

            row++; // blank row

            r = s.createRow(++row);
            cell = 0;
            c = r.createCell(cell);
            c.setCellValue("Locatoin");
            c = r.createCell(++cell);
            c.setCellValue("Transaction ID");
            c = r.createCell(++cell);
            c.setCellValue("PIN Code");
            c = r.createCell(++cell);
            c.setCellValue("Receipt #");
            c = r.createCell(++cell);
            c.setCellValue("Time Ticket Printed");
            c = r.createCell(++cell);
            c.setCellValue("Ticket Printed Kiosk ID");
            c = r.createCell(++cell);
            c.setCellValue("Printed");
            c = r.createCell(++cell);
            c.setCellValue("Issued");
            c = r.createCell(++cell);
            c.setCellValue("Ticket ID");
            c = r.createCell(++cell);
            c.setCellValue("Ticket Description");
            c = r.createCell(++cell);
            c.setCellValue("Item Name");
            c = r.createCell(++cell);
            c.setCellValue("Price");
            c = r.createCell(++cell);
            c.setCellValue("Expiry Date");
            c = r.createCell(++cell);
            c.setCellValue("Activation Date");
            c = r.createCell(++cell);
            c.setCellValue("Validity Period");

            List<KioskAdminTicketQueryResult> resultList = ticketService.getAllTickets(filter, null, null);
            for (KioskAdminTicketQueryResult result : resultList) {
                r = s.createRow(++row);
                cell = -1;
                c = r.createCell(++cell);
                c.setCellValue(result.getLocation());
                c = r.createCell(++cell);
                c.setCellValue(result.getStarTranId());
                c = r.createCell(++cell);
                c.setCellValue(result.getPinCode());
                c = r.createCell(++cell);
                c.setCellValue(result.getReceiptSeq());
                c = r.createCell(++cell);
                c.setCellValue(result.getTimeTicketPrinted());
                c = r.createCell(++cell);
                c.setCellValue(result.getTicketPrintedKioskId());
                c = r.createCell(++cell);
                c.setCellValue(result.getPrinted());
                c = r.createCell(++cell);
                c.setCellValue(result.getIssued());
                c = r.createCell(++cell);
                c.setCellValue(result.getTicketNo());
                c = r.createCell(++cell);
                c.setCellValue(result.getTicketDescription());
                c = r.createCell(++cell);
                c.setCellValue(result.getItemName());
                c = r.createCell(++cell);
                c.setCellValue(NumberFormat.getCurrencyInstance().format(new BigDecimal(new Double(result.getPrice()))));
                c = r.createCell(++cell);
                c.setCellValue(result.getExpiryDate());
                c = r.createCell(++cell);
                c.setCellValue(result.getActivationDate());
                c = r.createCell(++cell);
                c.setCellValue(result.getValidityPeriod());
            }

            for (int i = 0; i <= cell; i++) {
                s.autoSizeColumn(i);
            }

            wb.write(outByteStream);
            wb.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return outByteStream;
    }

}
