package com.enovax.star.cms.kiosk.admin.web.model.report;

import java.util.ArrayList;
import java.util.List;

public class KioskAdminTranQueryFilter {

    private String channel;

    private String tranStartDate;

    private String tranEndDate;

    private String starTranId;

    private String axReceiptNo;

    private String tranStartTime;

    private String tranEndTime;

    private String paymentStatus;

    private String paymentType;

    private String ticketingKioskId;

    private String sortField;

    private String sortDirection;

    private String ticketNo;

    private List<String> tranTypeList = new ArrayList<>();

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTranStartDate() {
        return tranStartDate;
    }

    public void setTranStartDate(String tranStartDate) {
        this.tranStartDate = tranStartDate;
    }

    public String getTranEndDate() {
        return tranEndDate;
    }

    public void setTranEndDate(String tranEndDate) {
        this.tranEndDate = tranEndDate;
    }

    public String getStarTranId() {
        return starTranId;
    }

    public void setStarTranId(String starTranId) {
        this.starTranId = starTranId;
    }

    public String getAxReceiptNo() {
        return axReceiptNo;
    }

    public void setAxReceiptNo(String axReceiptNo) {
        this.axReceiptNo = axReceiptNo;
    }

    public String getTranStartTime() {
        return tranStartTime;
    }

    public void setTranStartTime(String tranStartTime) {
        this.tranStartTime = tranStartTime;
    }

    public String getTranEndTime() {
        return tranEndTime;
    }

    public void setTranEndTime(String tranEndTime) {
        this.tranEndTime = tranEndTime;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTicketingKioskId() {
        return ticketingKioskId;
    }

    public void setTicketingKioskId(String ticketingKioskId) {
        this.ticketingKioskId = ticketingKioskId;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public List<String> getTranTypeList() {
        return tranTypeList;
    }

    public void setTranTypeList(List<String> tranTypeList) {
        this.tranTypeList = tranTypeList;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

}
