package com.enovax.star.cms.kiosk.admin.web.model;

public class KioskAdminApiAggTranTotalAmount {

    private String sum;

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

}
