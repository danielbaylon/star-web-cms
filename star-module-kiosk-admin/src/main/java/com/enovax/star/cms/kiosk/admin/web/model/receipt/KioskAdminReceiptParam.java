package com.enovax.star.cms.kiosk.admin.web.model.receipt;

public class KioskAdminReceiptParam {

    private String channel;

    private String address;

    private String companyName;

    private String contactDetails;

    private String gstDetails;

    private String logo;

    private String postalCode;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(String contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getGstDetails() {
        return gstDetails;
    }

    public void setGstDetails(String gstDetails) {
        this.gstDetails = gstDetails;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}
