package com.enovax.star.cms.kiosk.admin.web.model;

/**
 * 
 * @author Justin
 *
 * @see com.enovax.star.cms.kiosk.api.store.reference.PaymentCardType
 */
public class KioskAdminPaymentCardTypeFilter {

	private String value;

	private String desp;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

}
