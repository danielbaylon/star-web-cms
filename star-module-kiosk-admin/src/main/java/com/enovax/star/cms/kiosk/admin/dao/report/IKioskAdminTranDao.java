package com.enovax.star.cms.kiosk.admin.dao.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;

public interface IKioskAdminTranDao {

    List<KioskTransactionEntity> getTranList(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskTransactionEntity> getTranList(KioskAdminTranQueryFilter filter);

    List<KioskTransactionEntity> getRedeemedTranList(String channel, KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection);

    List<KioskTransactionEntity> getRedeemedTranList(String channel, KioskAdminTranQueryFilter filte);
}
