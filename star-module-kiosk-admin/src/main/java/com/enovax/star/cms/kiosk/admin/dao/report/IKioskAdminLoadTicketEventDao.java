package com.enovax.star.cms.kiosk.admin.dao.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminEventQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;

public interface IKioskAdminLoadTicketEventDao {

    List<KioskLoadPaperTicketInfoEntity> getEventList(String channel, KioskAdminEventQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskLoadPaperTicketInfoEntity> getEventList(String channel, KioskAdminEventQueryFilter filter);

}
