package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTicketQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketMgtEventResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSoldResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSummaryResult;

public interface IKioskAdminTicketService {

    List<KioskAdminZReportTicketSummaryResult> getTicketIssuanceSummary(String channel, String date, String kioskId);

    List<KioskAdminZReportTicketMgtEventResult> getTicketMgtEvent(String channel, String date, String kioskId);

    List<KioskAdminZReportTicketMgtEventResult> getTicketMgtEvent(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection);

    List<KioskAdminZReportTicketSoldResult> getTicketSold(String channel, String date, String kioskId);

    List<KioskAdminZReportTicketSoldResult> getTicketSold(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection);

    List<String> getTicketNoList(String channel, String startDate, String endDate, String kioskId);

    List<KioskAdminTicketQueryResult> getAllTickets(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskAdminTicketQueryResult> getAllTickets(KioskAdminTranQueryFilter filter);
}
