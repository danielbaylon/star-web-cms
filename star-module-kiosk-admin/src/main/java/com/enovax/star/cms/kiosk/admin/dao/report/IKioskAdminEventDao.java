package com.enovax.star.cms.kiosk.admin.dao.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminEventQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;

public interface IKioskAdminEventDao {

    List<KioskEventEntity> getEventList(String channel, KioskAdminEventQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskEventEntity> getEventList(String channel, KioskAdminEventQueryFilter filter);

}
