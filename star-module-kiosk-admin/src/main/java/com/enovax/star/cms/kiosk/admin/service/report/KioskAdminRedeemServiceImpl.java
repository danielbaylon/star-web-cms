package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminTranDao;
import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRedeemHistoryResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Service
public class KioskAdminRedeemServiceImpl implements IKioskAdminRedeemService {

    private final static Logger log = Logger.getLogger(KioskAdminRedeemServiceImpl.class);

    @Autowired
    private IKioskAdminTranDao kioskAdminTranDao;

    @Override
    @Transactional
    public List<KioskAdminZReportRedeemHistoryResult> getRedeemHistory(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection) {
        List<KioskAdminZReportRedeemHistoryResult> resultList = new ArrayList<>();
        try {
            if (StringUtils.isNotBlank(date) && StringUtils.isNotBlank(kioskId)) {
                DateTime searchDate = new DateTime(KioskAdminDateUtil.DF_DD_MM_YYYY.parse(date));
                KioskAdminTranQueryFilter filter = new KioskAdminTranQueryFilter();
                filter.setChannel(channel);
                filter.setTranStartDate(date);
                filter.setTicketingKioskId(kioskId);
                filter.setTranEndDate(KioskAdminDateUtil.DF_DD_MM_YYYY.format(searchDate.plusDays(1).toDate()));
                filter.setTranStartTime("00:00");
                filter.setTranEndTime("00:00");
                filter.getTranTypeList().add(KioskTransactionType.B2C_REDEMPTION.getCode());
                filter.getTranTypeList().add(KioskTransactionType.B2B_REDEMPTION.getCode());

                List<KioskTransactionEntity> tranList = kioskAdminTranDao.getRedeemedTranList(filter.getChannel(), filter, pageNumber, pageSize, sortField, sortDirection);

                for (KioskTransactionEntity tran : tranList) {

                    Map<String, List<KioskTransactionItemEntity>> itemGroup = tran.getItems().stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getItemId));

                    for (Map.Entry<String, List<KioskTransactionItemEntity>> entry : itemGroup.entrySet()) {
                        List<KioskTransactionItemEntity> items = entry.getValue();

                        KioskAdminZReportRedeemHistoryResult result = new KioskAdminZReportRedeemHistoryResult();

                        result.setTranDate(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.format(tran.getAxCartCheckoutDateTime()));
                        result.setStarTranId(tran.getReceiptNumber());
                        result.setPinCode(tran.getRedemptionPinCode());
                        result.setIssuedQty(String.valueOf(items.stream().mapToInt(i -> i.getRedeemedQty()).sum()));
                        result.setBookedQty(String.valueOf(items.stream().mapToInt(i -> i.getOriginalQty()).sum()));
                        result.setItemCode(entry.getKey());
                        result.setItemName(items.stream().findAny().get().getAxProductName());
                        resultList.add(result);
                    }

                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return resultList;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportRedeemHistoryResult> getRedeemHistory(String channel, String date, String kioskId) {
        return this.getRedeemHistory(channel, date, kioskId, null, null, null, null);
    }

}
