package com.enovax.star.cms.kiosk.admin.service.report;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.OptionalInt;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminTranDao;
import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportBasicInfoResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Service
public class KioskAdminReceiptServiceImpl implements IKioskAdminReceiptService {

    private static final Logger log = Logger.getLogger(KioskAdminReceiptServiceImpl.class);

    @Autowired
    private IKioskAdminTranDao tranDao;

    private String getNextDay(String date) {
        Date end = new Date();
        try {
            end = KioskAdminDateUtil.DF_DD_MM_YYYY.parse(date);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        DateTime nextDay = new DateTime(end.getTime()).plusDays(1);
        return KioskAdminDateUtil.DF_DD_MM_YYYY.format(nextDay.toDate());
    }

    @Override
    @Transactional
    public List<KioskAdminZReportBasicInfoResult> getStartEndReceiptNumber(String channel, String date, String kioskId) {
        log.info("channel : " + channel + "date : " + date + "kiosk id : " + kioskId);
        KioskAdminTranQueryFilter filter = new KioskAdminTranQueryFilter();
        filter.setChannel(channel);
        filter.setPaymentStatus("ALL");
        filter.setPaymentType("ALL");
        filter.setTicketingKioskId(kioskId);
        filter.setTranEndDate(this.getNextDay(date));
        filter.setTranEndTime("00:00");
        filter.setTranStartDate(date);
        filter.setTranStartTime("00:00");
        filter.getTranTypeList().add(KioskTransactionType.PURCHASE.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2B_REDEMPTION.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2C_REDEMPTION.getCode());
        List<KioskTransactionEntity> tranList = tranDao.getTranList(filter);
        log.info("transactio list size" + tranList.size());

        OptionalInt min = tranList.stream().mapToInt(i -> i.getAxReceiptId()).min();
        OptionalInt max = tranList.stream().mapToInt(i -> i.getAxReceiptId()).max();

        List<KioskAdminZReportBasicInfoResult> basicInfoResult = new ArrayList<>();

        KioskAdminZReportBasicInfoResult startReceipt = new KioskAdminZReportBasicInfoResult();
        startReceipt.setLabel("Start Receipt Number : ");
        startReceipt.setContent(min.isPresent() ? String.valueOf(min.getAsInt()) : "0");
        basicInfoResult.add(startReceipt);

        KioskAdminZReportBasicInfoResult endReceipt = new KioskAdminZReportBasicInfoResult();
        endReceipt.setLabel("End Receipt Number");
        endReceipt.setContent(max.isPresent() ? String.valueOf(max.getAsInt()) : "0");
        basicInfoResult.add(endReceipt);

        return basicInfoResult;
    }

}
