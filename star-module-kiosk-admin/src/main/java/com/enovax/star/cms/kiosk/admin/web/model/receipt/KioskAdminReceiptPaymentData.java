package com.enovax.star.cms.kiosk.admin.web.model.receipt;

public class KioskAdminReceiptPaymentData {

    private String tid;

    private String mid;

    private String batchNo;

    private String invoicNo;

    private String approvalCode;

    private String rrn;

    private String merchant;

    private String entryMode;

    private String tvr;

    private String aid;

    private String appLabel;

    private String tc;

    private String paymentMode;

    private String cardLabel;

    private String cardNo;

    private Boolean signatureRequired;

    private String signatureData;

    private String cardHolderName;

    private String remainingBalance;

    private String originalBalance;

    private String netsTrace;

    private String netsReceipt;

    private String netsIssuer;

    private String dateTime;

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getInvoicNo() {
        return invoicNo;
    }

    public void setInvoicNo(String invoicNo) {
        this.invoicNo = invoicNo;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getTvr() {
        return tvr;
    }

    public void setTvr(String tvr) {
        this.tvr = tvr;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Boolean getSignatureRequired() {
        return signatureRequired;
    }

    public void setSignatureRequired(Boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public String getSignatureData() {
        return signatureData;
    }

    public void setSignatureData(String signatureData) {
        this.signatureData = signatureData;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(String remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public String getOriginalBalance() {
        return originalBalance;
    }

    public void setOriginalBalance(String originalBalance) {
        this.originalBalance = originalBalance;
    }

    public String getNetsTrace() {
        return netsTrace;
    }

    public void setNetsTrace(String netsTrace) {
        this.netsTrace = netsTrace;
    }

    public String getNetsReceipt() {
        return netsReceipt;
    }

    public void setNetsReceipt(String netsReceipt) {
        this.netsReceipt = netsReceipt;
    }

    public String getNetsIssuer() {
        return netsIssuer;
    }

    public void setNetsIssuer(String netsIssuer) {
        this.netsIssuer = netsIssuer;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

}
