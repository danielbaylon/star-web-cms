package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminTranQueryResult {

    private String tranDate;

    private String starTranId;

    private String axReceiptNo;

    private String axCartId;

    private String axReceiptSeq;

    private String status;

    private String amount;

    private String paymentStatus;

    private String paymentType;

    private String cardNo;

    private String bankApprovalCode;

    private String trafficSource;

    private String browsingPrintedDateTime;

    private String ticketingKioskId;

    private String tranStartDateTime;

    private String tranEndDateTime;

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getStarTranId() {
        return starTranId;
    }

    public void setStarTranId(String starTranId) {
        this.starTranId = starTranId;
    }

    public String getAxReceiptNo() {
        return axReceiptNo;
    }

    public void setAxReceiptNo(String axReceiptNo) {
        this.axReceiptNo = axReceiptNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getBankApprovalCode() {
        return bankApprovalCode;
    }

    public void setBankApprovalCode(String bankApprovalCode) {
        this.bankApprovalCode = bankApprovalCode;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }

    public String getBrowsingPrintedDateTime() {
        return browsingPrintedDateTime;
    }

    public void setBrowsingPrintedDateTime(String browsingPrintedDateTime) {
        this.browsingPrintedDateTime = browsingPrintedDateTime;
    }

    public String getTicketingKioskId() {
        return ticketingKioskId;
    }

    public void setTicketingKioskId(String ticketingKioskId) {
        this.ticketingKioskId = ticketingKioskId;
    }

    public String getTranStartDateTime() {
        return tranStartDateTime;
    }

    public void setTranStartDateTime(String tranStartDateTime) {
        this.tranStartDateTime = tranStartDateTime;
    }

    public String getTranEndDateTime() {
        return tranEndDateTime;
    }

    public void setTranEndDateTime(String tranEndDateTime) {
        this.tranEndDateTime = tranEndDateTime;
    }

    public String getAxReceiptSeq() {
        return axReceiptSeq;
    }

    public void setAxReceiptSeq(String axReceiptSeq) {
        this.axReceiptSeq = axReceiptSeq;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getAxCartId() {
        return axCartId;
    }

    public void setAxCartId(String axCartId) {
        this.axCartId = axCartId;
    }

}
