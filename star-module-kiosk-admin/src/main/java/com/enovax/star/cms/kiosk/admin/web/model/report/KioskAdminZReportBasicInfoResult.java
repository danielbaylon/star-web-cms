package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminZReportBasicInfoResult {

    private String label;

    private String content;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
