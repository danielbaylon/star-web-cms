package com.enovax.star.cms.kiosk.admin.dao.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;

public interface IKioskAdminTicketDao {

    List<KioskTicketEntity> getTicket(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize);

    List<KioskTicketEntity> getTicket(KioskAdminTranQueryFilter filter);
}
