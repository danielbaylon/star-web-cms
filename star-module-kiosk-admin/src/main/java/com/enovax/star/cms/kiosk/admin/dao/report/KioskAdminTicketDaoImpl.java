package com.enovax.star.cms.kiosk.admin.dao.report;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Repository
public class KioskAdminTicketDaoImpl implements IKioskAdminTicketDao {

    private static final Logger log = LoggerFactory.getLogger(KioskAdminTicketDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    private Date getTranDate(String date, String time) {
        Date tranDate = new Date();
        String searchDate = date + " " + time + ":00";
        try {
            tranDate = KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(searchDate);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return tranDate;
    }

    @Override
    public List<KioskTicketEntity> getTicket(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize) {

        log.info("pageNumber : " + pageNumber + ", pageSize : " + pageSize);

        String statement = " select e from KioskTicketEntity e inner join e.tran t ";
        statement += " inner join t.payments p inner join t.kiosk k ";
        statement += " where t.type in ( :tranType ) and k.channel = :channelCode ";
        statement += " and (t.axReceiptNumber = :axReceiptNumber or '-1' = :axReceiptNumber ) ";
        statement += " and (p.status = :paymentStatus or -1 = :paymentStatus ) ";
        statement += " and (t.receiptNumber = :starTranId or '-1' = :starTranId ) ";
        statement += " and (k.jcrNodeName in (:ticketingKioskId) or '-1' in (:ticketingKioskId ) )";
        statement += " and (e.ticketNumber in (:ticketNo) or '-1' in (:ticketNo ) )";
        statement += " and (t.beginDateTime >= :beginDateTime ) ";
        statement += " and (t.endDateTime <= :endDateTime  ) ";

        if (filter.getSortField() != null && filter.getSortDirection() != null) {
            String field = "e.createdDateTime";
            if ("receiptSeq".equals(filter.getSortField())) {
                field = "t.axReceiptId";
            }
            statement += " order by " + field + " " + filter.getSortDirection();
        }

        Query query = sessionFactory.getCurrentSession().createQuery(statement);

        List<String> tranTypeList = filter.getTranTypeList();
        if (tranTypeList.size() == 0) {
            tranTypeList.add(KioskTransactionType.PURCHASE.getCode());
        }
        query.setParameterList("tranType", tranTypeList);
        query.setParameter("channelCode", filter.getChannel());
        query.setParameter("axReceiptNumber", StringUtils.isBlank(filter.getAxReceiptNo()) ? "-1" : filter.getAxReceiptNo());
        query.setInteger("paymentStatus", ("S".equals(filter.getPaymentStatus()) ? 1 : ("F".equals(filter.getPaymentStatus()) ? 0 : -1)));
        query.setParameter("starTranId", StringUtils.isBlank(filter.getStarTranId()) ? "-1" : filter.getStarTranId());
        List<String> kioskList = new ArrayList<>(Arrays.asList(filter.getTicketingKioskId().split(",")));
        if (kioskList.size() == 0 || kioskList.contains("ALL")) {
            kioskList.add("-1");
        }

        List<String> kioskNoList = new ArrayList<>();
        if (StringUtils.isBlank(filter.getTicketNo()) || new ArrayList<>(Arrays.asList(filter.getTicketNo().split(","))).contains("ALL")) {
            kioskNoList.add("-1");
        } else {
            kioskNoList = new ArrayList<>(Arrays.asList(filter.getTicketNo().split(",")));
        }
        kioskNoList = kioskNoList.stream().filter(n -> StringUtils.isNotBlank(n)).map(String::trim).collect(Collectors.toList());
        query.setParameterList("ticketNo", kioskNoList);
        query.setParameterList("ticketingKioskId", kioskList);
        query.setTimestamp("beginDateTime", this.getTranDate(filter.getTranStartDate(), filter.getTranStartTime()));
        query.setTimestamp("endDateTime", this.getTranDate(filter.getTranEndDate(), filter.getTranEndTime()));

        if (pageNumber != null && pageSize != null) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
        }
        return query.list();
    }

    @Override
    public List<KioskTicketEntity> getTicket(KioskAdminTranQueryFilter filter) {
        return this.getTicket(filter, null, null);
    }

}
