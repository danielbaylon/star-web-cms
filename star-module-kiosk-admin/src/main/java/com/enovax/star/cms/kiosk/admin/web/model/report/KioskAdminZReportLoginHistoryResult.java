package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminZReportLoginHistoryResult {

    private String username;

    private String datetime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
