package com.enovax.star.cms.kiosk.admin.dao.report;

import static com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode.CHIP;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode.CONTACTLESS;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode.FALLBACKMAGNETICSTRIPE;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode.MAGNETICSTRIPE;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode.MANUALENTRY;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType.MASTERCARD;
import static com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType.VISA;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Repository
public class KioskAdminTranDaoImpl implements IKioskAdminTranDao {

    private static final Logger log = LoggerFactory.getLogger(KioskAdminTranDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    private Date getTranDate(String date, String time) {
        Date tranDate = new Date();
        String searchDate = date + " " + time + ":00";
        try {
            tranDate = KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(searchDate);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return tranDate;
    }

    private String getPaymentType(KioskAdminTranQueryFilter filter) {
        String paymentType = filter.getPaymentType();

        if ("ALL".equals(filter.getPaymentType())) {
            paymentType = "-1";
        } else if ((MASTERCARD.getValue() + CONTACTLESS.getValue()).equals(filter.getPaymentType())) {
            paymentType = MASTERCARD.getValue();
        } else if ((VISA.getValue() + CONTACTLESS.getValue()).equals(filter.getPaymentType())) {
            paymentType = VISA.getValue();
        }

        return paymentType;
    }

    private List<String> getCardEntryMode(KioskAdminTranQueryFilter filter) {
        List<String> cardEntryModeList = new ArrayList<>();

        if ((MASTERCARD.getValue() + CONTACTLESS.getValue()).equals(filter.getPaymentType())) {
            cardEntryModeList.add(CONTACTLESS.getValue());
        } else if ((VISA.getValue() + CONTACTLESS.getValue()).equals(filter.getPaymentType())) {
            cardEntryModeList.add(CONTACTLESS.getValue());
        } else if (MASTERCARD.getValue().equals(filter.getPaymentType()) || VISA.getValue().equals(filter.getPaymentType())) {
            cardEntryModeList.add(CHIP.getValue());
            cardEntryModeList.add(FALLBACKMAGNETICSTRIPE.getValue());
            cardEntryModeList.add(MAGNETICSTRIPE.getValue());
            cardEntryModeList.add(MANUALENTRY.getValue());
        } else {
            cardEntryModeList.add("-1");
        }
        return cardEntryModeList;
    }

    @Override
    public List<KioskTransactionEntity> getTranList(KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize) {

        log.info("pageNumber : " + pageNumber + ", pageSize : " + pageSize);

        String statement = " select distinct t from KioskTransactionEntity t ";
        statement += " inner join t.payments p inner join t.kiosk k ";
        statement += " where t.type in ( :tranType ) and k.channel = :channelCode ";
        statement += " and (t.axReceiptNumber = :axReceiptNumber or '-1' = :axReceiptNumber ) ";
        statement += " and (p.status = :paymentStatus or -1 = :paymentStatus ) ";
        statement += " and (p.paymentMode = :paymentType or '-1' = :paymentType ) ";
        statement += " and (p.entryMode in ( :cardEntryMode ) or '-1' in ( :cardEntryMode ) ) ";
        statement += " and (t.receiptNumber = :starTranId or '-1' = :starTranId ) ";
        statement += " and (k.jcrNodeName in (:ticketingKioskId) or '-1' in (:ticketingKioskId ) )";
        statement += " and (t.beginDateTime >= :beginDateTime ) ";
        statement += " and (t.endDateTime <= :endDateTime  ) ";

        if (filter.getSortField() != null && filter.getSortDirection() != null) {
            String field = "axCartCheckoutDateTime";
            if ("tranDate".equals(filter.getSortField())) {
                field = "axCartCheckoutDateTime";
            } else if ("axReceiptSeq".equals(filter.getSortField())) {
                field = "axReceiptId";
            } else if ("amount".equals(filter.getSortField())) {
                field = "totalAmount";
            }
            statement += " order by t." + field + " " + filter.getSortDirection();
        }

        Query query = sessionFactory.getCurrentSession().createQuery(statement);

        List<String> tranTypeList = filter.getTranTypeList();
        if (tranTypeList.size() == 0) {
            tranTypeList.add(KioskTransactionType.PURCHASE.getCode());
        }
        query.setParameterList("tranType", tranTypeList);
        query.setParameter("channelCode", filter.getChannel());
        query.setParameter("axReceiptNumber", StringUtils.isBlank(filter.getAxReceiptNo()) ? "-1" : filter.getAxReceiptNo());
        query.setInteger("paymentStatus", ("S".equals(filter.getPaymentStatus()) ? 1 : ("F".equals(filter.getPaymentStatus()) ? 0 : -1)));
        query.setParameterList("cardEntryMode", getCardEntryMode(filter));
        query.setParameter("paymentType", getPaymentType(filter));
        query.setParameter("starTranId", StringUtils.isBlank(filter.getStarTranId()) ? "-1" : filter.getStarTranId());
        List<String> kioskList = new ArrayList<>(Arrays.asList(filter.getTicketingKioskId().split(",")));
        if (kioskList.size() == 0 || kioskList.contains("ALL")) {
            kioskList.add("-1");
        }
        query.setParameterList("ticketingKioskId", kioskList);
        query.setTimestamp("beginDateTime", this.getTranDate(filter.getTranStartDate(), filter.getTranStartTime()));
        query.setTimestamp("endDateTime", this.getTranDate(filter.getTranEndDate(), filter.getTranEndTime()));

        if (pageNumber != null && pageSize != null) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
        }
        return query.list();
    }

    @Override
    public List<KioskTransactionEntity> getTranList(KioskAdminTranQueryFilter filter) {
        return this.getTranList(filter, null, null);
    }

    @Override
    public List<KioskTransactionEntity> getRedeemedTranList(String channel, KioskAdminTranQueryFilter filter, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection) {
        String statement = " select distinct t from KioskTransactionEntity t ";
        statement += " inner join t.kiosk k ";
        statement += " where t.type in ( :tranType ) and k.channel = :channelCode ";
        statement += " and (t.status = :tranStatus or -1 = :tranStatus ) ";
        statement += " and (k.jcrNodeName in (:ticketingKioskId) or '-1' in (:ticketingKioskId ) )";
        statement += " and (t.axCartCheckoutDateTime between :beginDateTime and :endDateTime) ";
        // AX_REDEMPTION_SUCCESSFUL
        if (sortField != null && sortDirection != null) {
            String field = "axCartCheckoutDateTime";
            if ("tranDate".equals(sortField)) {
                field = "axCartCheckoutDateTime";
            } else if ("axReceiptSeq".equals(sortField)) {
                field = "axReceiptId";
            } else if ("amount".equals(sortField)) {
                field = "totalAmount";
            }
            statement += " order by t." + field + " " + sortDirection;
        }

        Query query = sessionFactory.getCurrentSession().createQuery(statement);

        query.setParameterList("tranType", filter.getTranTypeList());
        query.setParameter("channelCode", channel);

        query.setParameter("tranStatus", KioskTransactionStatus.AX_REDEMPTION_SUCCESSFUL.getCode());

        List<String> kioskList = new ArrayList<>(Arrays.asList(filter.getTicketingKioskId().split(",")));
        if (kioskList.size() == 0 || kioskList.contains("ALL")) {
            kioskList.add("-1");
        }
        query.setParameterList("ticketingKioskId", kioskList);
        query.setTimestamp("beginDateTime", this.getTranDate(filter.getTranStartDate(), filter.getTranStartTime()));
        query.setTimestamp("endDateTime", this.getTranDate(filter.getTranEndDate(), filter.getTranEndTime()));

        if (pageNumber != null && pageSize != null) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
        }
        return query.list();

    }

    @Override
    public List<KioskTransactionEntity> getRedeemedTranList(String channel, KioskAdminTranQueryFilter filter) {

        return this.getRedeemedTranList(channel, filter, null, null, null, null);
    }

}
