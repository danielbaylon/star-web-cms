package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminTicketQueryResult {

    private String location;

    private String starTranId;

    private String pinCode;

    private String receiptSeq;

    private String timeTicketPrinted;

    private String ticketPrintedKioskId;

    private String printed;

    private String issued;

    private String ticketNo;

    private String ticketDescription;

    private String itemName;

    private String price;

    private String expiryDate;

    private String activationDate;

    private String validityPeriod;

    private String remarks;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStarTranId() {
        return starTranId;
    }

    public void setStarTranId(String starTranId) {
        this.starTranId = starTranId;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTimeTicketPrinted() {
        return timeTicketPrinted;
    }

    public void setTimeTicketPrinted(String timeTicketPrinted) {
        this.timeTicketPrinted = timeTicketPrinted;
    }

    public String getTicketPrintedKioskId() {
        return ticketPrintedKioskId;
    }

    public void setTicketPrintedKioskId(String ticketPrintedKioskId) {
        this.ticketPrintedKioskId = ticketPrintedKioskId;
    }

    public String getPrinted() {
        return printed;
    }

    public void setPrinted(String printed) {
        this.printed = printed;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReceiptSeq() {
        return receiptSeq;
    }

    public void setReceiptSeq(String receiptSeq) {
        this.receiptSeq = receiptSeq;
    }

}
