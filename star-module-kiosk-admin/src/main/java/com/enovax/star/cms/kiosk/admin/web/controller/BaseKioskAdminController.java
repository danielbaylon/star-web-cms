package com.enovax.star.cms.kiosk.admin.web.controller;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.ProjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public abstract class BaseKioskAdminController {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";

    protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<List<K>> handleUncaughtExceptionForList(Throwable t, Class<K> innerListClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K, V> ApiResult<Map<K, V>> handleUncaughtExceptionForMap(
            Throwable t, Class<K> innerMapKey, Class<V> innerMapValue, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(ApiErrorCodes.UnexpectedSystemException,
                StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }
}
