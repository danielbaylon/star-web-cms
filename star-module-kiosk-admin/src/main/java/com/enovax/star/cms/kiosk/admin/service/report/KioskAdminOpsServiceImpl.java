package com.enovax.star.cms.kiosk.admin.service.report;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.kiosk.admin.dao.report.IKioskAdminEventDao;
import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminEventQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportLoginHistoryResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;

@Service
public class KioskAdminOpsServiceImpl implements IKioskAdminOpsService {

    private static final Logger log = Logger.getLogger(KioskAdminOpsServiceImpl.class);

    private final String EVENT_TYPE_POS = "OPS";

    private final String EVENT_NAME_LOGIN = "LOGIN";

    @Autowired
    private IKioskAdminEventDao kioskEventDao;

    private Date getBeginDate(String date) {
        Date beginDate = null;
        try {
            beginDate = KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(date + " 00:00:00");
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return beginDate;

    }

    private Date getEndDate(String date) {
        Date endDate = null;
        try {
            endDate = KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_SS.parse(date + " 00:00:00");
            endDate = new DateTime(endDate).plusDays(1).toDate();
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return endDate;
    }

    @Override
    @Transactional
    public List<KioskAdminZReportLoginHistoryResult> getOpsUserLoginHistory(String channel, String date, String kioskId) {

        return this.getOpsUserLoginHistory(channel, date, kioskId, null, null, null, null);
    }

    @Override
    @Transactional
    public List<KioskAdminZReportLoginHistoryResult> getOpsUserLoginHistory(String channel, String date, String kioskId, Integer pageNumber, Integer pageSize, String sortField,
            String sortDirection) {
        List<KioskAdminZReportLoginHistoryResult> resultList = new ArrayList<>();
        if (this.getBeginDate(date) != null && this.getEndDate(date) != null) {

            KioskAdminEventQueryFilter filter = new KioskAdminEventQueryFilter();
            filter.setChannel(channel);
            filter.setStartDate(this.getBeginDate(date));
            filter.setEndDate(this.getEndDate(date));
            filter.setKioskId(kioskId);
            filter.setSortField(sortField);
            filter.setSortDirection(sortDirection);
            filter.setEventType(this.EVENT_TYPE_POS);
            filter.setEventName(this.EVENT_NAME_LOGIN);
            List<KioskEventEntity> events = kioskEventDao.getEventList(channel, filter, pageNumber, pageSize);
            for (KioskEventEntity event : events) {
                KioskAdminZReportLoginHistoryResult result = new KioskAdminZReportLoginHistoryResult();
                result.setDatetime(KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM.format(event.getCreatets()));
                result.setUsername(event.getLogonuser());
                resultList.add(result);
            }
        }

        return resultList;
    }

}
