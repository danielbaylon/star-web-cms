package com.enovax.star.cms.kiosk.admin.web.model.receipt;

public class KioskAdminTranReceipt {

    private KioskAdminReceiptParam param = new KioskAdminReceiptParam();

    private KioskAdminReceiptData data = new KioskAdminReceiptData();

    public KioskAdminReceiptParam getParam() {
        return param;
    }

    public void setParam(KioskAdminReceiptParam param) {
        this.param = param;
    }

    public KioskAdminReceiptData getData() {
        return data;
    }

    public void setData(KioskAdminReceiptData data) {
        this.data = data;
    }

}
