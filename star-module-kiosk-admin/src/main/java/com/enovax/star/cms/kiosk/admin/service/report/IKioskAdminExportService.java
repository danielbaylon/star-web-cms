package com.enovax.star.cms.kiosk.admin.service.report;

import java.io.OutputStream;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRequestFilter;

public interface IKioskAdminExportService {

    OutputStream generateTranQueryExcelReport(KioskAdminTranQueryFilter filter);

    OutputStream generateZReportExcelReport(KioskAdminZReportRequestFilter filter);

    OutputStream generateTicketQueryExcelReport(KioskAdminTranQueryFilter filter);

}
