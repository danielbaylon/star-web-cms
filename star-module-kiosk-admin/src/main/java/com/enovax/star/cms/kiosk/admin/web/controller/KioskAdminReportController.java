package com.enovax.star.cms.kiosk.admin.web.controller;

import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY;
import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_DD_MM_YYYY_HH_MM_AM_PM;
import static com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil.DF_HH_MM_SS;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.kiosk.jcr.JcrKioskInfo;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminExportService;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminOpsService;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminReceiptService;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminRedeemService;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminTicketService;
import com.enovax.star.cms.kiosk.admin.service.report.IKioskAdminTranQueryService;
import com.enovax.star.cms.kiosk.admin.util.KioskAdminDateUtil;
import com.enovax.star.cms.kiosk.admin.web.model.KioskAdminApiAggTranTotalAmount;
import com.enovax.star.cms.kiosk.admin.web.model.KioskAdminApiResult;
import com.enovax.star.cms.kiosk.admin.web.model.KioskAdminPaymentCardTypeFilter;
import com.enovax.star.cms.kiosk.admin.web.model.receipt.KioskAdminTranReceipt;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTicketQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminTranQueryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportBasicInfoResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportLoginHistoryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportPaymentDetailResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRedeemHistoryResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportRequestFilter;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketMgtEventResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSoldResult;
import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportTicketSummaryResult;
import com.enovax.star.cms.kiosk.api.store.jcr.repository.kiosk.IKioskJcrRepository;
import com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

import info.magnolia.context.MgnlContext;

@Controller
@RequestMapping("/store-report/")
public class KioskAdminReportController extends BaseKioskAdminController {

    private final static Logger log = Logger.getLogger(KioskAdminReportController.class);

    @Autowired
    private IKioskJcrRepository kioskJcrRepository;

    @Autowired
    private IKioskAdminTranQueryService tranQueryService;

    @Autowired
    private IKioskAdminRedeemService redeemService;

    @Autowired
    private IKioskAdminReceiptService receiptService;

    @Autowired
    private IKioskAdminTicketService ticketService;

    @Autowired
    private IKioskAdminOpsService opsService;

    @Autowired
    private IKioskAdminExportService exportService;

    public static final String TRAN_QUERY_FILTER = "tran-query-filter";

    public static final String ZREPORT_QUERY_FILTER = "z-report-filter";

    public static final String TICKET_QUERY_FILTER = "ticket-query-filter";

    private Map<String, Object> session = new HashMap<>();

    @RequestMapping(value = "get-user-channel", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> getUserChannel() {
        log.info("Entered getUserChannel...");
        String channel = "";
        ApiResult<String> result = new ApiResult<>();
        ResponseEntity<ApiResult<String>> response = null;
        try {
            for (String role : MgnlContext.getUser().getAllRoles()) {
                if (role.equals("kiosk-mflg-admin-report")) {
                    channel = "kiosk-mflg";
                } else if (role.equals("kiosk-slm-admin-report")) {
                    channel = "kiosk-slm";
                }
            }
            log.info("channel : " + channel);
            if (StringUtils.isBlank(channel)) {
                result.setMessage("User has no access to the channel reports. Please contact system admin to grant access to reports module.");

            } else {
                result.setData(channel);
                result.setSuccess(Boolean.TRUE);

            }
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "get-device-list", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<JcrKioskInfo>>> getUserDeviceList(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel) {
        log.info("Entered getUserDeviceList...");

        ApiResult<List<JcrKioskInfo>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<JcrKioskInfo>>> response = null;
        try {
            List<JcrKioskInfo> kioskInfoList = kioskJcrRepository.getAll(channel);
            kioskInfoList.stream().forEach(k -> k.setUserPassword(""));

            result.setData(kioskInfoList);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "get-payment-card-type-list", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<KioskAdminPaymentCardTypeFilter>>> getPaymentCardTypeList() {
        log.info("Entered getPaymentCardTypeList...");
        List<KioskAdminPaymentCardTypeFilter> cardTypeList = new ArrayList<>();
        ApiResult<List<KioskAdminPaymentCardTypeFilter>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<KioskAdminPaymentCardTypeFilter>>> response = null;
        try {
            for (KioskPaymentCardType cardType : KioskPaymentCardType.values()) {
                if (cardType == KioskPaymentCardType.MASTERCARD || cardType == KioskPaymentCardType.VISA || cardType == KioskPaymentCardType.NETSPINDEBIT
                        || cardType == KioskPaymentCardType.NFP) {
                    KioskAdminPaymentCardTypeFilter filter = new KioskAdminPaymentCardTypeFilter();
                    filter.setValue(cardType.getValue());
                    filter.setDesp(cardType.getDescription());
                    cardTypeList.add(filter);
                }
            }
            cardTypeList.sort((c1, c2) -> c1.getValue().compareTo(c2.getValue()));

            KioskAdminPaymentCardTypeFilter filter = new KioskAdminPaymentCardTypeFilter();
            filter.setValue(KioskPaymentCardType.VISA.getValue() + KioskCardEntryMode.CONTACTLESS.getValue());
            filter.setDesp("VISA Paywave");
            cardTypeList.add(filter);

            filter = new KioskAdminPaymentCardTypeFilter();
            filter.setValue(KioskPaymentCardType.MASTERCARD.getValue() + KioskCardEntryMode.CONTACTLESS.getValue());
            filter.setDesp("MASTERCARD Paypass");
            cardTypeList.add(filter);

            result.setData(cardTypeList);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "get-tran-query-result", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> getTranQueryResult(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranStartDate, @RequestParam String tranEndDate, @RequestParam String starTranId, @RequestParam String axReceiptNo,
            @RequestParam String tranStartTime, @RequestParam String tranEndTime, @RequestParam String paymentStatus, @RequestParam String paymentType,
            @RequestParam String ticketingKioskId, @RequestParam(name = "page", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getTranQueryResult...");
        log.info("tranStartDate : " + tranStartDate + ", tranEndDate : " + tranEndDate + "starTranId : " + starTranId + ", axReceiptNo : " + axReceiptNo + ", tranStartTime : "
                + tranStartTime + " tranEndTime : " + tranEndTime + " paymentStatus : " + paymentStatus + ", paymentType : " + paymentType + ", ticketingKioskId : "
                + ticketingKioskId + ", pageNumber : " + pageNumber + ", pageSize : " + pageSize + ", sortField : " + sortField + ", sortDirection : " + sortDirection);

        KioskAdminApiResult<List<KioskAdminTranQueryResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> response = null;
        try {
            KioskAdminTranQueryFilter requestFilter = new KioskAdminTranQueryFilter();
            requestFilter.setChannel(channel);
            requestFilter.setAxReceiptNo(axReceiptNo);
            requestFilter.setPaymentStatus(paymentStatus);
            requestFilter.setPaymentType(paymentType);
            requestFilter.setStarTranId(starTranId);
            requestFilter.setTicketingKioskId(ticketingKioskId);
            requestFilter.setTranEndDate(tranEndDate);
            requestFilter.setTranEndTime(tranEndTime);
            requestFilter.setTranStartDate(tranStartDate);
            requestFilter.setTranStartTime(tranStartTime);
            requestFilter.setSortField(sortField);
            requestFilter.setSortDirection(sortDirection);
            session.put(TRAN_QUERY_FILTER, requestFilter);
            List<KioskAdminTranQueryResult> pagedTranQueryResult = this.tranQueryService.getTranQueryResult(requestFilter, pageNumber, pageSize);
            List<KioskAdminTranQueryResult> allTranQueryResult = this.tranQueryService.getTranQueryResult(requestFilter);

            Double totalAmount = allTranQueryResult.stream().mapToDouble(t -> new Double(t.getAmount())).sum();

            KioskAdminApiAggTranTotalAmount aggSum = new KioskAdminApiAggTranTotalAmount();
            aggSum.setSum(totalAmount.toString());

            result.getAggregate().setAmount(aggSum);

            result.setData(pagedTranQueryResult);
            result.setTotal(allTranQueryResult.size());
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "export-tran-query-result", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> exportTranQueryResultToExcel(HttpServletResponse response) {
        try {
            KioskAdminTranQueryFilter filter = (KioskAdminTranQueryFilter) session.get(TRAN_QUERY_FILTER);
            final String fileName = "transaction-query-report.xlsx";
            byte[] outArray = ((ByteArrayOutputStream) exportService.generateTranQueryExcelReport(filter)).toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [exportTranQueryResultToExcel] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-receipt-by-ax-card-id", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<KioskAdminTranReceipt>> getReceiptByAxCartId(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel, String axCartId) {
        log.info("Entered getReceiptByAxCartId...");

        ApiResult<KioskAdminTranReceipt> result = new ApiResult<>();
        ResponseEntity<ApiResult<KioskAdminTranReceipt>> response = null;
        try {
            KioskAdminTranReceipt receipt = this.tranQueryService.getReceiptByAxCartId(channel, axCartId);
            result.setData(receipt);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-basic-info", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<KioskAdminZReportBasicInfoResult>>> getZReportBasicInfo(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId) {
        log.info("Entered getZReportBasicInfo... tranDate :" + tranDate + ", kioskId :" + kioskId);
        KioskAdminZReportRequestFilter filter = new KioskAdminZReportRequestFilter();
        filter.setChannel(channel);
        filter.setTranDate(tranDate);
        filter.setKioskId(kioskId);
        session.put(ZREPORT_QUERY_FILTER, filter);
        ApiResult<List<KioskAdminZReportBasicInfoResult>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<KioskAdminZReportBasicInfoResult>>> response = null;
        try {
            List<KioskAdminZReportBasicInfoResult> basicInfoResult = receiptService.getStartEndReceiptNumber(channel, tranDate, kioskId);
            result.setData(basicInfoResult);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-ticket-summary", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<KioskAdminZReportTicketSummaryResult>>> getZReportTicketSummary(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId) {
        log.info("Entered getZReportTicketSummary... tranDate :" + tranDate + ", kioskId :" + kioskId);

        ApiResult<List<KioskAdminZReportTicketSummaryResult>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<KioskAdminZReportTicketSummaryResult>>> response = null;
        try {
            List<KioskAdminZReportTicketSummaryResult> takeSummaryResult = ticketService.getTicketIssuanceSummary(channel, tranDate, kioskId);
            result.setData(takeSummaryResult);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-ticket-mgt-event", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<KioskAdminZReportTicketMgtEventResult>>> getZReportTicketMgtEvent(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId, @RequestParam(name = "page", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getZReportTicketMgtEvent... tranDate :" + tranDate + ", kioskId :" + kioskId);

        ApiResult<List<KioskAdminZReportTicketMgtEventResult>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<KioskAdminZReportTicketMgtEventResult>>> response = null;
        try {
            List<KioskAdminZReportTicketMgtEventResult> ticketMgtEventResult = ticketService.getTicketMgtEvent(channel, tranDate, kioskId, pageNumber, pageSize, sortField,
                    sortDirection);
            List<KioskAdminZReportTicketMgtEventResult> ticketMgtEventResultTotal = ticketService.getTicketMgtEvent(channel, tranDate, kioskId);
            result.setTotal(ticketMgtEventResultTotal.size());
            result.setData(ticketMgtEventResult);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-login-history", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<KioskAdminZReportLoginHistoryResult>>> getZReportLoginHistory(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId, @RequestParam(name = "page", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getZReportTicketMgtEvent... tranDate :" + tranDate + ", kioskId :" + kioskId);

        ApiResult<List<KioskAdminZReportLoginHistoryResult>> result = new ApiResult<>();
        ResponseEntity<ApiResult<List<KioskAdminZReportLoginHistoryResult>>> response = null;
        try {
            List<KioskAdminZReportLoginHistoryResult> loginHistoryReuslt = opsService.getOpsUserLoginHistory(channel, tranDate, kioskId, pageNumber, pageSize, sortField,
                    sortDirection);
            List<KioskAdminZReportLoginHistoryResult> loginHistoryReusltTotal = opsService.getOpsUserLoginHistory(channel, tranDate, kioskId);
            result.setTotal(loginHistoryReusltTotal.size());
            result.setData(loginHistoryReuslt);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-payment-detail", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminZReportPaymentDetailResult>>> getZReportPaymentDetail(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel, @RequestParam String tranDate, @RequestParam String kioskId) {
        log.info("Entered getZReportTicketMgtEvent... tranDate :" + tranDate + ", kioskId :" + kioskId);

        KioskAdminApiResult<List<KioskAdminZReportPaymentDetailResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminZReportPaymentDetailResult>>> response = null;
        try {
            List<KioskAdminZReportPaymentDetailResult> paymentDetailResult = this.tranQueryService.getPaymentDetails(channel, tranDate, kioskId);

            Double totalAmount = paymentDetailResult.stream().mapToDouble(t -> new Double(t.getAmount())).sum();
            KioskAdminApiAggTranTotalAmount aggSum = new KioskAdminApiAggTranTotalAmount();
            aggSum.setSum(totalAmount.toString());
            result.getAggregate().setAmount(aggSum);

            result.setTotal(paymentDetailResult.size());
            result.setData(paymentDetailResult);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = "z-report/get-tran-history", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> getZReportTranHistory(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId, @RequestParam(name = "page", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getZReportTranHistory... tranDate :" + tranDate + ", kioskId :" + kioskId);
        DateTime searchDate = DateTime.now();
        KioskAdminApiResult<List<KioskAdminTranQueryResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> response = null;
        try {
            searchDate = new DateTime(KioskAdminDateUtil.DF_DD_MM_YYYY.parse(tranDate));
            response = this.getTranQueryResult(channel, tranDate, KioskAdminDateUtil.DF_DD_MM_YYYY.format(searchDate.plusDays(1).toDate()), "", "", "00:00", "00:00", "S", "ALL",
                    kioskId, pageNumber, pageSize, sortField, sortDirection);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "z-report/get-redeem-history", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> getZReportRedeemHistory(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String tranDate, @RequestParam String kioskId, @RequestParam(name = "page", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getZReportTranHistory... tranDate :" + tranDate + ", kioskId :" + kioskId);

        KioskAdminApiResult<List<KioskAdminTranQueryResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminTranQueryResult>>> response = null;
        try {

            List<KioskAdminZReportRedeemHistoryResult> redeemHistoryReuslt = redeemService.getRedeemHistory(channel, tranDate, kioskId, pageNumber, pageSize, sortField,
                    sortDirection);
            List<KioskAdminZReportRedeemHistoryResult> redeemHistoryTotalReuslt = redeemService.getRedeemHistory(channel, tranDate, kioskId);
            result.setData(redeemHistoryReuslt);
            result.setTotal(redeemHistoryTotalReuslt.size());
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "z-report/get-ticket-sold", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminZReportTicketSoldResult>>> getZReportTicketSold(
            @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel, @RequestParam String tranDate, @RequestParam String kioskId,
            @RequestParam(name = "page", defaultValue = "1") Integer pageNumber, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField, @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        log.info("Entered getZReportTranHistory... tranDate :" + tranDate + ", kioskId :" + kioskId);

        KioskAdminApiResult<List<KioskAdminZReportTicketSoldResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminZReportTicketSoldResult>>> response = null;
        try {

            List<KioskAdminZReportTicketSoldResult> soldResult = ticketService.getTicketSold(channel, tranDate, kioskId, pageNumber, pageSize, sortField, sortDirection);
            List<KioskAdminZReportTicketSoldResult> soldTotalResult = ticketService.getTicketSold(channel, tranDate, kioskId);
            result.setData(soldResult);
            result.setTotal(soldTotalResult.size());
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "export-z-report-result", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> exportZReportResultToExcel(HttpServletResponse response) {
        try {
            KioskAdminZReportRequestFilter filter = (KioskAdminZReportRequestFilter) session.get(ZREPORT_QUERY_FILTER);
            final String fileName = "Z-Report.xlsx";
            byte[] outArray = ((ByteArrayOutputStream) exportService.generateZReportExcelReport(filter)).toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [exportTranQueryResultToExcel] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/ticket-query/get-ticket-no-by-device", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<List<String>>> getTicketNoListByDevice(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel, @RequestParam String startDate,
            @RequestParam String endDate, @RequestParam String kioskId) {
        log.info("Entered getTicketNoListByDevice... startDate :" + startDate + " endDate : " + endDate + ", kioskId :" + kioskId);

        KioskAdminApiResult<List<String>> result = new KioskAdminApiResult<>();
        ResponseEntity<ApiResult<List<String>>> response = null;
        try {
            List<String> ticketNoList = this.ticketService.getTicketNoList(channel, startDate, endDate, kioskId);
            result.setData(ticketNoList);
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    private KioskAdminTranQueryFilter getQueryFilter(String channel, String startDate, String endDate, String kioskId, String ticketNo, String starTranId) throws ParseException {
        KioskAdminTranQueryFilter filter = new KioskAdminTranQueryFilter();
        filter.setChannel(channel);
        filter.setPaymentStatus("ALL");
        filter.setPaymentType("ALL");
        filter.setTicketingKioskId(StringUtils.isBlank(kioskId) ? "ALL" : kioskId);
        filter.getTranTypeList().add(KioskTransactionType.PURCHASE.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2B_REDEMPTION.getCode());
        filter.getTranTypeList().add(KioskTransactionType.B2C_REDEMPTION.getCode());
        filter.setTicketNo(ticketNo);
        Date start = DF_DD_MM_YYYY_HH_MM_AM_PM.parse(startDate);
        Date end = DF_DD_MM_YYYY_HH_MM_AM_PM.parse(endDate);
        filter.setTranStartDate(DF_DD_MM_YYYY.format(start));
        filter.setTranStartTime(DF_HH_MM_SS.format(start));
        filter.setTranEndDate(DF_DD_MM_YYYY.format(end));
        filter.setTranEndTime(DF_HH_MM_SS.format(end));
        filter.setStarTranId(starTranId);
        return filter;
    }

    @RequestMapping(value = "/ticket-query/search", method = { RequestMethod.GET })
    public ResponseEntity<KioskAdminApiResult<List<KioskAdminTicketQueryResult>>> getTicketQueryResult(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channel,
            @RequestParam String startDate, @RequestParam String endDate, @RequestParam String kioskId, @RequestParam String ticketNo, @RequestParam String starTranId,
            @RequestParam(name = "page", defaultValue = "1") Integer pageNumber, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField, @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        KioskAdminApiResult<List<KioskAdminTicketQueryResult>> result = new KioskAdminApiResult<>();
        ResponseEntity<KioskAdminApiResult<List<KioskAdminTicketQueryResult>>> response = null;
        try {
            KioskAdminTranQueryFilter filter = this.getQueryFilter(channel, startDate, endDate, kioskId, ticketNo, starTranId);
            List<KioskAdminTicketQueryResult> ticketResult = ticketService.getAllTickets(filter, pageNumber, pageSize);
            List<KioskAdminTicketQueryResult> ticketTotalResult = ticketService.getAllTickets(filter);
            result.setData(ticketResult);
            result.setTotal(ticketTotalResult.size());
            result.setSuccess(Boolean.TRUE);
            response = new ResponseEntity<>(result, HttpStatus.OK);

            session.put(TICKET_QUERY_FILTER, filter);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("System Error.");
            response = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "export-ticket-query-result", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<String>> exportTicketQueryResultToExcel(HttpServletResponse response) {
        try {
            KioskAdminTranQueryFilter filter = (KioskAdminTranQueryFilter) session.get(TICKET_QUERY_FILTER);
            final String fileName = "Ticket-Query-Report.xlsx";
            byte[] outArray = ((ByteArrayOutputStream) exportService.generateTicketQueryExcelReport(filter)).toByteArray();
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();
            outStream.close();
            return null;
        } catch (Exception e) {
            log.error("!!! System exception encountered [exportTicketQueryResultToExcel] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
}