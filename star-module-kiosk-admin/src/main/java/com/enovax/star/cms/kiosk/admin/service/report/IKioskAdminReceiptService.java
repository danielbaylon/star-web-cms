package com.enovax.star.cms.kiosk.admin.service.report;

import java.util.List;

import com.enovax.star.cms.kiosk.admin.web.model.report.KioskAdminZReportBasicInfoResult;

public interface IKioskAdminReceiptService {
    
    List<KioskAdminZReportBasicInfoResult> getStartEndReceiptNumber(String channel, String date , String kioskId);

}
