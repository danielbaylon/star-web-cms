package com.enovax.star.cms.kiosk.admin.web.model.report;

public class KioskAdminZReportPaymentDetailResult {

    private String mode;
    
    private String amount;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
