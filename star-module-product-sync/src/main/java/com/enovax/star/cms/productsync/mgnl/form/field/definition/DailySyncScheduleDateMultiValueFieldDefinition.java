package com.enovax.star.cms.productsync.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.MultiValueFieldDefinition;

/**
 * Created by jennylynsze on 7/29/16.
 */
public class DailySyncScheduleDateMultiValueFieldDefinition extends MultiValueFieldDefinition {
    private String syncOption;

    public String getSyncOption() {
        return syncOption;
    }

    public void setSyncOption(String syncOption) {
        this.syncOption = syncOption;
    }
}
