package com.enovax.star.cms.productsync.mgnl.commands.impl;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.productsync.mgnl.StarModuleProductSync;
import info.magnolia.commands.impl.BaseRepositoryCommand;
import info.magnolia.context.Context;

/**
 * Created by jennylynsze on 4/27/16.
 */
public class AXProductConfigChangedCommand extends BaseRepositoryCommand {
    private String channel;

    @Override
    public boolean execute(Context context) throws Exception {
        StarModuleProductSync.updateScheduleTask(StoreApiChannels.fromCode(context.get("channel").toString()));
        return true;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
