package com.enovax.star.cms.productsync.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.AXProductCrossSellProperties;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkMainProduct;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkRelatedProduct;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 6/25/16.
 */
public class SyncCrossSells {

    private static final Logger log = LoggerFactory.getLogger(SyncCrossSells.class);
    public static StringBuilder sb = new StringBuilder();

    public static ApiResult<String> syncCrossSells(StoreApiChannels channel,  List<String> mainProductsListingIds, CrossSellLinkPackage crossSellLinkPackage) {

        sb.append("Starting synching of AX Cross Sell " + channel.code + "..." + "<br/>");

        log.info("Synching Cross Sells for Channel: " + channel.code);

        //retrieve from an api
        if(crossSellLinkPackage.isSuccess()) {
            Map<String, CrossSellLinkMainProduct> mainProductMap = crossSellLinkPackage.getMainProductMap();

            if(mainProductMap != null) {
                for(String itemId: mainProductMap.keySet()) {
                    CrossSellLinkMainProduct crossSell = mainProductMap.get(itemId);
                    try {

                        String query = "/jcr:root/ax-products/" + channel.code + "//element(*, mgnl:ax-product)[@displayProductNumber = '" + itemId + "']";
                        Iterable<Node> axProductNodeIterable =   JcrRepository.query(JcrWorkspace.AXProducts.getWorkspaceName(), JcrWorkspace.AXProducts.getNodeType(), query);
                        Iterator<Node> axProductNodeIterator = axProductNodeIterable.iterator();

                        Node axProductNode = null;
                        if(axProductNodeIterator.hasNext()) {
                            axProductNode = axProductNodeIterator.next();
                        }

                        //Though this should never happen
                        if(axProductNode == null) {
                            continue;
                        }

                        //remove the the listing since it has promotions
                        mainProductsListingIds.remove(PropertyUtil.getString(axProductNode, AXProductProperties.ProductListingId.getPropertyName()));

                        Node crossSellNode = null;
                        if(!axProductNode.hasNode("cross-sell")) {
                            crossSellNode = axProductNode.addNode("cross-sell", "mgnl:contentNode");
                        }else {
                            crossSellNode = axProductNode.getNode("cross-sell");
                        }

                        List<CrossSellLinkRelatedProduct> crossSellList = crossSell.getRelatedProducts();
                        Map<String, CrossSellLinkRelatedProduct> crossSellMap = new HashMap<>();
                        for(CrossSellLinkRelatedProduct cs: crossSellList) {
                            crossSellMap.put(cs.getItemId(), cs);
                        }

                        Iterable<Node> crossSellNodeIterable = NodeUtil.getNodes(crossSellNode, "mgnl:contentNode");
                        Iterator<Node> crossSellNodeIterator = crossSellNodeIterable.iterator();

                        //Update the existing one
                        int lastNodeName = -1;
                        while(crossSellNodeIterator.hasNext()) {
                            Node crossSellSubNode = crossSellNodeIterator.next();
                            lastNodeName = Integer.parseInt(crossSellSubNode.getName());
                            //check property
                            String productCode = PropertyUtil.getString(crossSellSubNode, AXProductCrossSellProperties.ProductCode.getPropertyName());
                            if(crossSellMap.containsKey(productCode)) {
                                //get it here
                                CrossSellLinkRelatedProduct rp = crossSellMap.get(productCode);

                                Node relatedProductNode =  getRelatedProduct(channel, rp.getItemId());

                                if(relatedProductNode != null) {

                                    sb.append("<br/>Top up [" + rp.getItemId() + "] for Product [ " + itemId + "] already stored in JCR. Updating the details from AX...<br/>");

                                    crossSellMap.remove(productCode);
                                    assignCrossSells(relatedProductNode, crossSellSubNode, rp, false);
                                    JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), crossSellSubNode);
                                }

                                crossSellMap.remove(productCode);

                            }else {
                                sb.append("<br/>Top up [" + productCode + "] for Product [ " + itemId + "] was not retrieved from AX. This product will be set as Inactive.<br/>");

                                crossSellSubNode.setProperty(AXProductCrossSellProperties.SystemStatus.getPropertyName(), "Inactive");
                                JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), crossSellSubNode);
                            }
                        }

                        //Add a new ones na wala pa
                        for(String productCode: crossSellMap.keySet()) {
                            CrossSellLinkRelatedProduct rp = crossSellMap.get(productCode);
                            Node relatedProductNode =  getRelatedProduct(channel, rp.getItemId());

                            sb.append("<br/>Top up [" + productCode + "] for Product [ " + itemId + "] is new. Creating a new product with the details from AX...<br/>");

                            if(relatedProductNode != null) {
                                lastNodeName += 1; //increment
                                Node crossSellSubNode = crossSellNode.addNode(lastNodeName + "", "mgnl:contentNode");
                                assignCrossSells(relatedProductNode, crossSellSubNode, rp, true);
                                JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), crossSellSubNode);
                            }
                        }

                        JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), crossSellNode);
                        //TODO publish
                       // SyncProduct.publishNodes(crossSellNode.getIdentifier()); //publish the changes i think

                    } catch (RepositoryException e) {
                        log.error(e.getMessage(), e);
                    }

                }
            }


            for(String listingId: mainProductsListingIds) {
                Node axProductNode = null;
                try {
                    axProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + listingId);

                    if(axProductNode == null) {
                        continue;
                    }

                    if(axProductNode.hasNode(AXProductProperties.CrossSell.getPropertyName())) {
                        Node promotionNode = axProductNode.getNode(AXProductProperties.CrossSell.getPropertyName());
                        promotionNode.remove();
                    }

                    axProductNode.getSession().save();
                    //TODO publish
                    //SyncProduct.publishNodes(axProductNode.getIdentifier());
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }

            return new ApiResult<>(true, "", "", sb.toString());

        }else {
            log.error("Error encountered while getting the cross sells of ax product");
            log.error(crossSellLinkPackage.getMessage());
            return new ApiResult<>(false, "", "", "");
        }
    }

    public static Node getRelatedProduct(StoreApiChannels channel, String productCode) {
        try {
            String query = "/jcr:root/ax-products/" + channel.code + "//element(*, mgnl:ax-product)[@displayProductNumber = '" + productCode + "']";
            Iterable<Node> axProductNodeIterable =   JcrRepository.query(JcrWorkspace.AXProducts.getWorkspaceName(), JcrWorkspace.AXProducts.getNodeType(), query);
            Iterator<Node> axProductNodeIterator = axProductNodeIterable.iterator();

            Node axProductNode = null;
            if(axProductNodeIterator.hasNext()) {
                axProductNode = axProductNodeIterator.next();
            }

            return axProductNode;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static void assignCrossSells(Node relatedProductNode, Node crossSellSubNode, CrossSellLinkRelatedProduct rp, boolean isNew) throws RepositoryException {

        sb.append("<br/>Top up Details: <br/>");

        sb.append("[Record ID] = " + rp.getRecordId() + "<br/>");
        crossSellSubNode.setProperty(AXProductCrossSellProperties.RecordId.getPropertyName(), rp.getRecordId());

        sb.append("[Item ID] = " + rp.getItemId() + "<br/>");
        crossSellSubNode.setProperty(AXProductCrossSellProperties.ProductCode.getPropertyName(), rp.getItemId());

        crossSellSubNode.setProperty(AXProductCrossSellProperties.ListingId.getPropertyName(), PropertyUtil.getString(relatedProductNode, AXProductProperties.ProductListingId.getPropertyName()));
        String productName = PropertyUtil.getString(relatedProductNode, AXProductProperties.ProductName.getPropertyName());

        sb.append("[Product Name] = " + productName + "<br/>");
        crossSellSubNode.setProperty(AXProductCrossSellProperties.ProductName.getPropertyName(), productName);

        if(isNew) {
            String itemName = PropertyUtil.getString(relatedProductNode, AXProductProperties.ItemName.getPropertyName());
            crossSellSubNode.setProperty(AXProductCrossSellProperties.ItemName.getPropertyName(), itemName); //same with productName
            crossSellSubNode.setProperty(AXProductCrossSellProperties.Display.getPropertyName(), "hide");
        }

        crossSellSubNode.setProperty(AXProductCrossSellProperties.UnitId.getPropertyName(), rp.getUnitId());
        crossSellSubNode.setProperty(AXProductCrossSellProperties.Qty.getPropertyName(), rp.getQty());
        crossSellSubNode.setProperty(AXProductCrossSellProperties.SystemStatus.getPropertyName(), "Active");
    }
}
