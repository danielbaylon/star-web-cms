package com.enovax.star.cms.productsync.mgnl.form.field;

import com.enovax.star.cms.productsync.mgnl.form.field.definition.DailySyncScheduleDateMultiValueFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.MultiField;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by jennylynsze on 8/1/16.
 */
public class DailySyncScheduleDateMultiValueField extends MultiField {

    public DailySyncScheduleDateMultiValueField(DailySyncScheduleDateMultiValueFieldDefinition definition, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider, Item relatedFieldItem, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, fieldFactoryFactory, componentProvider, relatedFieldItem, i18nAuthoringSupport);
    }

    public void removeAllComponents() {
        if(this.root != null) {
            PropertysetItem item = (PropertysetItem)DailySyncScheduleDateMultiValueField.this.getPropertyDataSource().getValue();
            Collection<Object> ids = (Collection<Object>) item.getItemPropertyIds();

            while(ids.size() > 0) {
                Iterator idIterator = ids.iterator();
                if(idIterator.hasNext()) {
                    item.removeItemProperty(idIterator.next());
                }
                ids = (Collection<Object>) item.getItemPropertyIds();
            }
//            if(ids.size() > 0) {
//
//            }
//            Iterator idIterator = ids.iterator();
//            while(idIterator.hasNext()) {
//                item.removeItemProperty(idIterator.next());
//            }
////            for(Object id: ids) {
////                item.removeItemProperty(id);
//            }
            this.initFields(item);
        }

    }

}
