package com.enovax.star.cms.productsync.mgnl.tasks;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.productsync.service.SyncProduct;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jennylynsze on 7/25/16.
 */
public class SyncAxProductTask implements StatefulJob {

    private static final Logger log = LoggerFactory.getLogger(SyncAxProductTask.class);

    private StoreApiChannels channel;

    public SyncAxProductTask() {
        this.channel = StoreApiChannels.B2C_SLM; //set a default haha
    }

    public SyncAxProductTask(StoreApiChannels channel) {
        this.channel = channel;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Executing the batch job for Sync Ax Product...");
        //TODO, maybe send a msg that the batch job didnt continue or what HMMM

        SyncProduct.syncProduct(this.channel);
//        execute(new SilentSessionOp<Object>(JcrWorkspace.AXProducts.getWorkspaceName()) {
//            @Override
//            public Object doExec(final Session session) throws Throwable {
//                //TODO need to check if need to pass the session
//                SyncProduct.syncALLAXProduct();
//                return null;
//            }
//        });
    }
}
