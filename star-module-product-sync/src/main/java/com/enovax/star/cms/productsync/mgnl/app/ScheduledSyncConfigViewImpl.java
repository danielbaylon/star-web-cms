package com.enovax.star.cms.productsync.mgnl.app;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.vaadin.form.FormViewReduced;
import info.magnolia.ui.vaadin.layout.SmallAppLayout;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by jennylynsze on 7/25/16.
 */
/**
 * Not used
 */
public class ScheduledSyncConfigViewImpl implements ScheduledSyncConfigView {
    private final SmallAppLayout root = new SmallAppLayout();
    private Map<String, FormViewReduced> formView;
    private CssLayout cssLayout;
    private Listener listener;
    private SimpleTranslator translator;

    @Inject
    public ScheduledSyncConfigViewImpl(SimpleTranslator translator) {
        this.translator = translator;
    }

    public void build() {
        Button saveButton = new Button(this.translator.translate("mail.app.main.button.save.caption", new Object[0]));
        saveButton.addStyleName("v-button-smallapp");
        saveButton.addStyleName("commit");
        saveButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                ScheduledSyncConfigViewImpl.this.getListener().save();
            }
        });

//        for(String key: formView.keySet()) {
//            CssLayout buttonLayout = new CssLayout();
//            buttonLayout.addStyleName("v-csslayout-smallapp-actions");
//            buttonLayout.addComponent(saveButton);
//            this.cssLayout = new CssLayout();
//            this.cssLayout.addComponent(this.formView.get(key).asVaadinComponent());
//            this.cssLayout.addComponent(buttonLayout);
//            this.root.setDescription("Use the settings below to configure the scheduled batch jobs for AX Sync");
//            this.root.addSection(this.cssLayout);
//        }

        CssLayout buttonLayout = new CssLayout();
        buttonLayout.addStyleName("v-csslayout-smallapp-actions");
        buttonLayout.addComponent(saveButton);
        this.cssLayout = new CssLayout();
        this.cssLayout.addComponent(this.formView.get("config").asVaadinComponent());
        this.cssLayout.addComponent(buttonLayout);
        this.root.setDescription("Use the settings below to configure the scheduled batch jobs for AX Sync");
        this.root.addSection(this.cssLayout);


        BrowserFrame page = new BrowserFrame((String)null, new ExternalResource("/.resources/partner-portal-admin/tier-management.html"));
        page.setSizeFull();
        this.cssLayout = new CssLayout();
        this.cssLayout.addComponent(page);
        this.cssLayout.setHeight("500");
        this.root.addSection(this.cssLayout);


    }

    public Component asVaadinComponent() {
        return this.root;
    }

    public void showValidation(boolean visible) {
        this.formView.get("config").showValidation(visible);
    }

    public boolean isValid() {
        return this.formView.get("config").isValid();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void setFormViewReduced(Map<String, FormViewReduced> var) {
        if(this.formView != null) {
            for(String key: formView.keySet()) {
                this.cssLayout.replaceComponent(this.formView.get(key).asVaadinComponent(), var.get(key).asVaadinComponent());
            }
        }
        this.formView = var;
    }

    public Listener getListener() {
        return this.listener;
    }
}
