package com.enovax.star.cms.productsync.service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.product.promotions.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.PublishingUtil;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;

/**
 * Created by jennylynsze on 6/25/16.
 */
public class SyncPromotions {

    private static final Logger log = LoggerFactory.getLogger(SyncPromotions.class);
    public static StringBuilder sb = new StringBuilder();

    public static ApiResult<String> syncPromotions(StoreApiChannels channel, List<String> mainProductsListingIds, PromotionLinkPackage promotionLinkPackage) {

        sb.append("Starting synching of AX Products Promotions for Channel " + channel.code + "...<br/>");

        log.info("Starting synching of AX Products Promotions for Channel " + channel.code + "...");

        //retrieve from an api
        if(promotionLinkPackage.isSuccess()) {
            List<PromotionLinkMainPromotion> mainPromotionList = promotionLinkPackage.getMainPromotionList();
            Node channelNode = null;
            try {
               channelNode = JcrRepository.getParentNode(JcrWorkspace.Promotions.getWorkspaceName(), "/" + channel.code);
            } catch (RepositoryException e) {
                log.error(e.getMessage(), e);
                sb.append("Error encountered in getting the promotion node from JCR...");
                return new ApiResult<>(false, "", "", sb.toString());
            }

            Map<String, PromotionLinkMainPromotion> mainPromotionMap = new HashMap<>();
            for(PromotionLinkMainPromotion mainPromotion:mainPromotionList) {
                mainPromotionMap.put(mainPromotion.getPromotion().getOfferId(), mainPromotion);
            }

            try {
                Iterable<Node> promotionsIterable = NodeUtil.getNodes(channelNode);
                Iterator<Node> promotionsIterator = promotionsIterable.iterator();

                while(promotionsIterator.hasNext()) {
                    Node promotionNode = promotionsIterator.next();
                    String offerId = PropertyUtil.getString(promotionNode, AXProductPromotionProperties.OfferId.getPropertyName());
                    if(mainPromotionMap.containsKey(offerId)) {
                        //update
                        sb.append("<br/>Promotion Offer ID  [" + offerId + "] already stored in JCR. Updating the details from AX...<br/>");

                        PromotionLinkMainPromotion mainPromotion = mainPromotionMap.get(offerId);

                        assignPromotions(promotionNode, mainPromotion.getPromotion(), false); //edit the promotion details

                        //assign the affiliation
                        createUpdateAffiliations(channel, promotionNode, mainPromotion.getPromotion());

                        //add in the discount codes
                        createUpdateDiscountCodes(promotionNode, mainPromotion.getPromotion());

                        createUpdateRelatedProducts(channel, promotionNode, mainPromotion.getRelatedItems());
                        //add in the related products
                        //TODO
                        mainPromotionMap.remove(offerId);

                    }else {
                        sb.append("<br/>Promotion Offer ID  [" + offerId + "] was not retrieved from AX. This product will be set as Inactive.<br/>");
                        //incactive
                        promotionNode.setProperty(AXProductPromotionProperties.SystemStatus.getPropertyName(), "Inactive"); //set to inactive;
                    }

                    try {
                        JcrRepository.updateNode(JcrWorkspace.Promotions.getWorkspaceName(), promotionNode); //update the main promo
//                        try {
//                            PublishingUtil.publishNodes(promotionNode.getIdentifier(), JcrWorkspace.Promotions.getWorkspaceName());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }
                }

            } catch (RepositoryException e) {
                e.printStackTrace();
            }


            //save the promotion first
            for(String offerId: mainPromotionMap.keySet()) {
                //check if it exists
                PromotionLinkMainPromotion mainPromotion = mainPromotionMap.get(offerId);
                ProductRelatedPromotion promotion = mainPromotion.getPromotion();

                Node promotionNode = null;
                try {

                    if(JcrRepository.nodeExists(JcrWorkspace.Promotions.getWorkspaceName(), "/" + channel.code + "/" + promotion.getOfferId())) {
                        sb.append("<br/>Promotion Offer ID  [" + offerId + "] already stored in JCR. Updating the details from AX...<br/>");
                        promotionNode = JcrRepository.getParentNode(JcrWorkspace.Promotions.getWorkspaceName(), "/" + channel.code + "/" + promotion.getOfferId());
                        assignPromotions(promotionNode, promotion, false); //edit the promotion details
                    }
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }

                if(promotionNode == null) {
                    //create the promotion node
                    try {

                        sb.append("<br/>Promotion Offer ID  [" + offerId + "] is new. Creating a new product with the details from AX...<br/>");

                        promotionNode = JcrRepository.createNode(JcrWorkspace.Promotions.getWorkspaceName(), "/" + channel.code, promotion.getOfferId(), JcrWorkspace.Promotions.getNodeType());
                        assignPromotions(promotionNode, promotion, true);  //add the promotion details
                    } catch (RepositoryException e) {
                        //TODO dunno what to do here
                        log.error(e.getMessage(), e);
                        return new ApiResult<>(false, "", "", sb.toString());
                    }
                }

                //assign the affiliation
                createUpdateAffiliations(channel, promotionNode, promotion);

                //add in the discount codes
                createUpdateDiscountCodes(promotionNode, promotion);

                createUpdateRelatedProducts(channel, promotionNode, mainPromotion.getRelatedItems());

                try {
                    JcrRepository.updateNode(JcrWorkspace.Promotions.getWorkspaceName(), promotionNode); //update the main promo
//                    try {
//                        PublishingUtil.publishNodes(promotionNode.getIdentifier(), JcrWorkspace.Promotions.getWorkspaceName());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }

            List<PromotionLinkMainProduct> mainProductList = promotionLinkPackage.getMainProductList();

            for(PromotionLinkMainProduct mainProd: mainProductList) {
                //loop tru
                Node axProductNode = null;
                try {

                    //remove since it has promotions
                    mainProductsListingIds.remove(mainProd.getItemId());

                    if(JcrRepository.nodeExists(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + mainProd.getItemId())) {
                        axProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + mainProd.getItemId());
                    }

                    if(axProductNode == null) {
                        continue; //go to the next one
                    }

                    Node promotionNode = null;
                    if(!axProductNode.hasNode(AXProductProperties.Promotion.getPropertyName())) {
                        promotionNode = axProductNode.addNode(AXProductProperties.Promotion.getPropertyName(), "mgnl:contentNode");
                    }else {
                        promotionNode = axProductNode.getNode(AXProductProperties.Promotion.getPropertyName());
                    }

                    createUpdateRelatedPromotions(channel, promotionNode, mainProd);

                } catch (RepositoryException e) {
                    log.error(e.getMessage(), e);
                }
            }

            //those products that doesnt have a promotions, remove the key
            for(String listingId: mainProductsListingIds) {
                Node axProductNode = null;
                try {
                    axProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + listingId);

                    if(axProductNode == null) {
                        continue;
                    }

                    if(axProductNode.hasNode(AXProductProperties.Promotion.getPropertyName())) {
                        Node promotionNode = axProductNode.getNode(AXProductProperties.Promotion.getPropertyName());
                        promotionNode.remove();
                    }

                    axProductNode.getSession().save();

                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }


            //ONE MAJOR PUBLISH
            try {
                PublishingUtil.publishNodes(channelNode.getIdentifier(), JcrWorkspace.Promotions.getWorkspaceName());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }


            Node affChannelNode = null;
            try {
                affChannelNode = JcrRepository.getParentNode(JcrWorkspace.Affiliations.getWorkspaceName(), "/" + channel.code);
                PublishingUtil.publishNodes(affChannelNode .getIdentifier(), JcrWorkspace.Affiliations.getWorkspaceName());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }


            return new ApiResult<>(true, "", "", sb.toString());

        }else {
            log.error("Error encountered in getting the promotions from API.");
            log.error(promotionLinkPackage.getMessage());
            sb.append("Error encountered in getting the promotions from API.");
            return new ApiResult<>(false, "", "", sb.toString());
        }

    }

    private static void assignPromotions(Node promotionSubNode, ProductRelatedPromotion promo, boolean isNew) throws RepositoryException {

        sb.append("<br/>Promotion Details: <br/>");

        boolean promoNameChanged = false;
        if(!isNew) {
            String promoName = PropertyUtil.getString(promotionSubNode, AXProductPromotionProperties.PromotionText.getPropertyName());
            String systemName = PropertyUtil.getString(promotionSubNode, AXProductPromotionProperties.SystemName.getPropertyName());

            if(systemName != null && systemName.equals(promoName) && promo.getSystemName() != null && !promo.getSystemName().equals(systemName)) {
                promoNameChanged = true;
            }
        }

        promotionSubNode.setProperty(AXProductPromotionProperties.OfferId.getPropertyName(), promo.getOfferId());
        sb.append("[Offer Id] = " + promo.getOfferId() + "<br/>");

        promotionSubNode.setProperty(AXProductPromotionProperties.SystemName.getPropertyName(), promo.getSystemName());
        sb.append("[System Name] = " + promo.getSystemName() + "<br/>");

        promotionSubNode.setProperty(AXProductPromotionProperties.CurrencyCode.getPropertyName(), promo.getCurrencyCode());
        sb.append("[Currency Code] = " + promo.getCurrencyCode() + "<br/>");

        Calendar validFrom = Calendar.getInstance();
        validFrom.setTime(promo.getValidFrom());
        promotionSubNode.setProperty(AXProductPromotionProperties.ValidFrom.getPropertyName(), validFrom);
        sb.append("[Valid From] = " + NvxDateUtils.formatDate(promo.getValidFrom(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) + "<br/>");

        Calendar validTo= Calendar.getInstance();
        validTo.setTime(promo.getValidTo());
        promotionSubNode.setProperty(AXProductPromotionProperties.ValidTo.getPropertyName(), validTo);
        sb.append("[Valid To] = " + NvxDateUtils.formatDate(promo.getValidTo(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) + "<br/>");

        sb.append("[System Active] = " + promo.isSystemActive() + "<br/>");

        if(promo.isSystemActive()) {
            promotionSubNode.setProperty(AXProductPromotionProperties.SystemStatus.getPropertyName(), "Active");
        }else {
            promotionSubNode.setProperty(AXProductPromotionProperties.SystemStatus.getPropertyName(), "Inactive");
        }

        if(isNew || promoNameChanged || !promotionSubNode.hasProperty(AXProductPromotionProperties.PromotionText.getPropertyName())) {
            promotionSubNode.setProperty(AXProductPromotionProperties.PromotionText.getPropertyName(), promo.getSystemName());
        }

        if(isNew) {
            promotionSubNode.setProperty(AXProductPromotionProperties.PromotionDisplay.getPropertyName(), "hide");
            promotionSubNode.setProperty(AXProductPromotionProperties.UsedAsUnlocker.getPropertyName(), false);
        }

    }

    private static void assignAffiliations(Node affiliationSubNode, PromotionRelatedAffiliation affiliation, boolean isNew) throws RepositoryException{

        sb.append("Affiliation Details: <br/>");

        sb.append("[Record ID] = " + affiliation.getRecordId() + "<br/>");
        affiliationSubNode.setProperty(AXProductAffiliationProperties.RecordId.getPropertyName(), affiliation.getRecordId());
        sb.append("[System Name] = " + affiliation.getSystemName() + "<br/>");
        affiliationSubNode.setProperty(AXProductAffiliationProperties.SystemName.getPropertyName(), affiliation.getSystemName());
        sb.append("[System Description] = " + affiliation.getSystemDescription() + "<br/>");
        affiliationSubNode.setProperty(AXProductAffiliationProperties.SystemDescription.getPropertyName(), affiliation.getSystemDescription()); //TODO is this the display content?

        if(isNew) {
            affiliationSubNode.setProperty(AXProductAffiliationProperties.DiscountType.getPropertyName(), "None");
        }

        //SAVE and PUBLISH
        JcrRepository.updateNode(JcrWorkspace.Affiliations.getWorkspaceName(), affiliationSubNode);
//        try {
//            PublishingUtil.publishNodes(affiliationSubNode.getIdentifier(), JcrWorkspace.Affiliations.getWorkspaceName());
//            //TODO handling for failed publisehd
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private static void assignDiscountCodes(Node dcSubNode, PromotionRelatedDiscountCode discountCode, boolean isNew) throws RepositoryException {

        sb.append("<br/>Discount Code Details: <br/>");

        sb.append("[Record ID] = " + discountCode.getRecordId() + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.RecordId.getPropertyName(), discountCode.getRecordId());

        sb.append("[Discount Code] = " + discountCode.getDiscountCode() + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.Code.getPropertyName(), discountCode.getDiscountCode());

        sb.append("[System Name] = " + discountCode.getSystemName() + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.SystemName.getPropertyName(), discountCode.getSystemName());

        Calendar dcValidFrom = Calendar.getInstance();
        dcValidFrom.setTime(discountCode.getValidFrom());
        sb.append("[Valid From] = " + NvxDateUtils.formatDate(discountCode.getValidFrom(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.ValidFrom.getPropertyName(), dcValidFrom);


        Calendar dcValidTo= Calendar.getInstance();
        dcValidTo.setTime(discountCode.getValidTo());
        sb.append("[Valid From] = " + NvxDateUtils.formatDate(discountCode.getValidTo(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME) + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.ValidTo.getPropertyName(), dcValidTo);

        sb.append("[Barcode] = " + discountCode.getBarcode() + "<br/>");
        dcSubNode.setProperty(AXProductDiscountCodeProperties.Barcode.getPropertyName(), discountCode.getBarcode());
        sb.append("[Related Offer ID] = " + discountCode.getRelatedOfferId() + "<br/>");

        dcSubNode.setProperty(AXProductDiscountCodeProperties.RelatedOfferId.getPropertyName(), discountCode.getRelatedOfferId());

        sb.append("[System Status] = " + discountCode.isSystemActive() + "<br/>");

        if(discountCode.isSystemActive()) {
            dcSubNode.setProperty(AXProductDiscountCodeProperties.SystemStatus.getPropertyName(), "Active");
        }else {
            dcSubNode.setProperty(AXProductDiscountCodeProperties.SystemStatus.getPropertyName(), "Inactive");
        }
    }

    private static void createUpdateAffiliations(StoreApiChannels channel, Node promotionNode, ProductRelatedPromotion promotion) {

        List<PromotionRelatedAffiliation> affiliationList = promotion.getAffiliations();
        Map<String,PromotionRelatedAffiliation> affiliationMap = new HashMap<>();
        for(PromotionRelatedAffiliation affiliation: affiliationList) {
            affiliationMap.put(affiliation.getRecordId(), affiliation); //putting all the promotions in a map first
        }

        Node affiliationNode = null;
        try {
            if(!promotionNode.hasNode(AXProductPromotionProperties.Affiliation.getPropertyName())) {
                affiliationNode = promotionNode.addNode(AXProductPromotionProperties.Affiliation.getPropertyName(), "mgnl:contentNode");
            }else {
                affiliationNode = promotionNode.getNode(AXProductPromotionProperties.Affiliation.getPropertyName());
            }

            //getting all the promotions we have so far
            Iterable<Node> affFKNodeIterable = NodeUtil.getNodes(affiliationNode, "mgnl:contentNode");
            Iterator<Node> affFKNodeIterator = affFKNodeIterable.iterator();
            int affLastNodeName = -1;
            while(affFKNodeIterator.hasNext()) {
                Node affFKNode = affFKNodeIterator.next();
                affLastNodeName = Integer.parseInt(affFKNode.getName());
                //get the UUID of the Affiliations
                String relatedAffUUID = PropertyUtil.getString(affFKNode, AXProductPromotionProperties.RelatedAffUUID.getPropertyName());
                Node affMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Affiliations.getWorkspaceName(), relatedAffUUID);
                String recordId = PropertyUtil.getString(affMainNode, AXProductAffiliationProperties.RecordId.getPropertyName());
                if(affiliationMap.containsKey(recordId)) {
                    PromotionRelatedAffiliation affiliation = affiliationMap.get(recordId);


                    sb.append("<br/>Affiliation Name [" + affiliation.getSystemName() + "] already stored in JCR. Updating the details from AX...<br/>");

                    assignAffiliations(affMainNode, affiliation, false);
                    JcrRepository.updateNode(JcrWorkspace.Affiliations.getWorkspaceName(), affMainNode);
                    affiliationMap.remove(recordId);
                }else {
                    sb.append("<br/>Affiliation Name  [" + PropertyUtil.getString(affFKNode, AXProductAffiliationProperties.SystemName.getPropertyName()) + "] was not retrieved from AX. This product will be set as Inactive.<br/>");
                    affFKNode.remove();
                    log.info("The Affiliation with Record ID: " + recordId + " is not retrieved from the AX Sync. Will remove the relations from Promotions with offerId = " + promotion.getOfferId() + "...");
                }
            }

            for(String affCode: affiliationMap.keySet()) {
                affLastNodeName += 1;
                PromotionRelatedAffiliation affiliation = affiliationMap.get(affCode);//will add this new one
                Node affFKNode = affiliationNode.addNode(affLastNodeName + "", "mgnl:contentNode");
                Node affMainNode = null;
                //check the uuid
                try {
                    affMainNode = JcrRepository.getParentNode(JcrWorkspace.Affiliations.getWorkspaceName(), "/" +  channel.code + "/" + affCode);
                }catch(RepositoryException e) {
                    affMainNode = JcrRepository.createNode(JcrWorkspace.Affiliations.getWorkspaceName(), "/" + channel.code, affCode, JcrWorkspace.Affiliations.getNodeType());
                }

                sb.append("<br/>Affiliation Name [" + affiliation.getSystemName() + "] is new. Updating the details from AX...<br/>");

                assignAffiliations(affMainNode, affiliation, true);
                JcrRepository.updateNode(JcrWorkspace.Affiliations.getWorkspaceName(), affMainNode);

//                try {
//                    PublishingUtil.publishNodes(affMainNode.getIdentifier(), JcrWorkspace.Affiliations.getWorkspaceName());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                affFKNode.setProperty(AXProductPromotionProperties.RelatedAffUUID.getPropertyName(), affMainNode.getIdentifier());
            }


        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    private static void createUpdateDiscountCodes(Node promotionNode, ProductRelatedPromotion promotion) {
        List<PromotionRelatedDiscountCode> dcList = promotion.getDiscountCodes();
        Map<String,PromotionRelatedDiscountCode> dcMap = new HashMap<>();
        for(PromotionRelatedDiscountCode dc: dcList) {
            dcMap.put(dc.getRecordId(), dc); //putting all the promotions in a map first
        }

        Node dcNode = null;
        try {
            if(!promotionNode.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                dcNode = promotionNode.addNode(AXProductPromotionProperties.DiscountCode.getPropertyName(), "mgnl:contentNode");
            }else {
                dcNode = promotionNode.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName());
            }

            //getting all the promotions we have so far
            Iterable<Node> dcNodeIterable = NodeUtil.getNodes(dcNode, JcrWorkspace.DiscountCode.getNodeType());
            Iterator<Node> dcNodeIterator = dcNodeIterable.iterator();
            int dcLastNodeName = -1;
            while(dcNodeIterator.hasNext()) {
                Node dcSubNode = dcNodeIterator.next();
                dcLastNodeName = Integer.parseInt(dcSubNode.getName());

                String recordId = PropertyUtil.getString(dcSubNode, AXProductDiscountCodeProperties.RecordId.getPropertyName());

                if(dcMap.containsKey(recordId)) {
                    PromotionRelatedDiscountCode discountCode = dcMap.get(recordId);
                    sb.append("<br/>Discount Name [" + discountCode.getSystemName() + "] already stored in JCR. Updating the details from AX...<br/>");

                    assignDiscountCodes(dcSubNode, discountCode, false);
                    dcMap.remove(recordId);
                }else {
                    sb.append("<br/>Discount Name [" + PropertyUtil.getString(dcSubNode, AXProductDiscountCodeProperties.SystemName.getPropertyName()) + "] was not retrieved from AX. This product will be set as Inactive.<br/>");

                    dcSubNode.setProperty(AXProductDiscountCodeProperties.SystemStatus.getPropertyName(), "Inactive");
                    log.info("The DiscountCode with Record ID: " + recordId + " is not retrieved from the AX Sync for the Promotion with offerId = " + promotion.getOfferId() + ". Will set the system status to Inactive...");
                }
            }

            for(String recordId: dcMap.keySet()) {
                dcLastNodeName += 1;
                PromotionRelatedDiscountCode discountCode = dcMap.get(recordId);//will add this new one
                Node dcSubNode = dcNode.addNode(dcLastNodeName + "", JcrWorkspace.DiscountCode.getNodeType());

                sb.append("<br/>Discount Name [" + discountCode.getSystemName() + "] is new. Updating the details from AX...<br/>");

                assignDiscountCodes(dcSubNode, discountCode, true);
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    private static void createUpdateRelatedProducts(StoreApiChannels channel, Node promotionNode, Map<String, PromotionRelatedProduct> relatedProducts) {
        Node relatedProductNode = null;
        try {
            if(!promotionNode.hasNode(AXProductPromotionProperties.RelatedProduct.getPropertyName())) {
                relatedProductNode = promotionNode.addNode(AXProductPromotionProperties.RelatedProduct.getPropertyName(), "mgnl:contentNode");
            }else {
                relatedProductNode = promotionNode.getNode(AXProductPromotionProperties.RelatedProduct.getPropertyName());
            }

            //getting all the products
            Iterable<Node> relatedProductNodeIterable = NodeUtil.getNodes(relatedProductNode, "mgnl:contentNode");
            Iterator<Node> relatedProductNodeIterator = relatedProductNodeIterable.iterator();
            int prodLastNodeName = -1;

            while(relatedProductNodeIterator.hasNext()) {
                Node relProdFKNode = relatedProductNodeIterator.next();
                prodLastNodeName = Integer.parseInt(relProdFKNode.getName());
                //get the UUID of the Affiliations
                String relatedAxProductUUID = PropertyUtil.getString(relProdFKNode, AXProductPromotionProperties.RelatedProductUUID.getPropertyName());
                Node axProductMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), relatedAxProductUUID);
                String itemId = PropertyUtil.getString(axProductMainNode, AXProductProperties.ProductListingId.getPropertyName());
                if(relatedProducts.containsKey(itemId)) {
                    //update the price just in case
                    PromotionRelatedProduct relatedProduct = relatedProducts.get(itemId);
                    relProdFKNode.setProperty(AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), relatedProduct.getEstimatedDiscountedUnitPrice());

                    relatedProducts.remove(itemId);

                }else {
                    relProdFKNode.remove();
                    //log.info("The Affiliation with Record ID: " + recordId + " is not retrieved from the AX Sync. Will remove the relations from Promotions with offerId = " + promotion.getOfferId() + "...");
                }
            }

            for(String itemId: relatedProducts.keySet()) {
                if(JcrRepository.nodeExists(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + itemId))  {
                    PromotionRelatedProduct relatedProduct = relatedProducts.get(itemId);
                    prodLastNodeName +=1;
                    Node relProdFKNode = relatedProductNode.addNode(prodLastNodeName + "", "mgnl:contentNode");
                    Node axProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code + "/" + itemId);
                    relProdFKNode.setProperty(AXProductPromotionProperties.RelatedProductUUID.getPropertyName(), axProductNode.getIdentifier());
                    relProdFKNode.setProperty(AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), relatedProduct.getEstimatedDiscountedUnitPrice());
                }else {
                    //skip
                    //log
                }
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    private static void createUpdateRelatedPromotions(StoreApiChannels channel, Node promotionNode, PromotionLinkMainProduct mainProd) {
        List<ProductRelatedPromotion> promotionList = mainProd.getRelatedPromotions();

        Map<String,ProductRelatedPromotion> promotionMap = new HashMap<>();
        for(ProductRelatedPromotion promo: promotionList) {
            promotionMap.put(promo.getOfferId(), promo); //putting all the promotions in a map first
        }

        //getting all the promotions we have so far
        Iterable<Node> promotionFKNodeIterable = null;
        try {
            promotionFKNodeIterable = NodeUtil.getNodes(promotionNode, "mgnl:contentNode");
            Iterator<Node> promotionFKNodeIterator = promotionFKNodeIterable.iterator();

            //Update the existing one
            int lastNodeName = -1;
            while(promotionFKNodeIterator.hasNext()) {
                Node promotionFKNode = promotionFKNodeIterator.next();
                lastNodeName = Integer.parseInt(promotionFKNode.getName());
                //check property

                //Get the Promotion node
                String relatedPromotionUUID =  PropertyUtil.getString(promotionFKNode, AXProductProperties.RelatedPromotionUUID.getPropertyName());
                Node promotionMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Promotions.getWorkspaceName(), relatedPromotionUUID);
                String offerId = PropertyUtil.getString(promotionMainNode, AXProductPromotionProperties.OfferId.getPropertyName());

                if(promotionMap.containsKey(offerId)) {
                    ProductRelatedPromotion promo = promotionMap.get(offerId);
                    promotionFKNode.setProperty(AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), promo.getEstimatedDiscountedUnitPrice()); //TODO evaluate
                    JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), promotionFKNode); //TODO dunno if needed

                    promotionMap.remove(offerId);
                } else {
                    promotionFKNode.remove();
                    log.info("The Promotion with Offer ID: " + offerId + " is not retrieved from the AX Sync. Will remove the relations from Products with itemId = " + mainProd.getItemId() + "...");
                }
            }

            for(String offerId: promotionMap.keySet()) {

                ProductRelatedPromotion promo = promotionMap.get(offerId);
                lastNodeName += 1; //increment
                Node promotionFKNode = promotionNode.addNode(lastNodeName + "", "mgnl:contentNode");
                Node promotionMainNode = null;
                //check the uuid

                promotionMainNode = JcrRepository.getParentNode(JcrWorkspace.Promotions.getWorkspaceName(), "/" +  channel.code + "/" + offerId);
                promotionFKNode.setProperty(AXProductProperties.RelatedPromotionUUID.getPropertyName(), promotionMainNode.getIdentifier());
                promotionFKNode.setProperty(AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), promo.getEstimatedDiscountedUnitPrice()); //TODO evaluate
                JcrRepository.updateNode(JcrWorkspace.AXProducts.getWorkspaceName(), promotionFKNode); //TODO dunno if needed
            }
//
            //TODO evaluate
//            try {
//                PublishingUtil.publishNodes(promotionNode.getIdentifier(), JcrWorkspace.AXProducts.getWorkspaceName());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
}
