package com.enovax.star.cms.productsync.mgnl.form.field.factory;

import com.enovax.star.cms.productsync.mgnl.event.SyncOptionValueChangedEvent;
import com.enovax.star.cms.productsync.mgnl.form.field.definition.SyncOptionGroupFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.AbstractSelect;
import info.magnolia.event.EventBus;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.OptionGroupFieldDefinition;
import info.magnolia.ui.form.field.factory.OptionGroupFieldFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by jennylynsze on 7/29/16.
 */
public class SyncOptionGroupFieldFactory <T extends SyncOptionGroupFieldDefinition> extends OptionGroupFieldFactory<T> {

    private final EventBus subAppEventBus;

    @Inject
    public SyncOptionGroupFieldFactory(OptionGroupFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
        this.subAppEventBus = subAppEventBus;
    }

    @Override
    protected AbstractSelect createFieldComponent() {
        final AbstractSelect select = super.createFieldComponent();
        select.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                subAppEventBus.fireEvent(new SyncOptionValueChangedEvent((String)select.getValue()));
            }
        });
        return select;
    }

}
