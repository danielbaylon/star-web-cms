package com.enovax.star.cms.productsync.service;

import com.enovax.star.cms.api.store.component.StarModuleApiBeans;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.*;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.product.AxMainProduct;
import com.enovax.star.cms.commons.model.product.AxProductLinkPackage;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkPackage;
import com.enovax.star.cms.commons.service.product.IProductService;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.PublishingUtil;
import com.enovax.star.cms.kiosk.api.store.component.StarKioskModuleApiBeans;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.service.cmsproduct.IKioskProductService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.util.*;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class SyncProduct {

    private static final Logger log = LoggerFactory.getLogger(SyncProduct.class);

    public static final String B2CSLM_AX_SYNC_ROLE = "b2c-slm-ax-sync";
    public static final String B2CMFLG_AX_SYNC_ROLE  = "b2c-mflg-ax-sync";
    public static final String KIOSKSLM_AX_SYNC_ROLE = "kiosk-slm-ax-sync";
    public static final String KIOSKMFLG_AX_SYNC_ROLE = "kiosk-mflg-ax-sync";
    public static final String PPSLM_AX_SYNC_ROLE = "pp-slm-ax-sync";
    public static final String PPMFLG_AX_SYNC_ROLE = "pp-mflg-ax-sync";

    public static final Map<StoreApiChannels, Boolean> syncInProgressMap = new HashMap<>();

    public static final StringBuilder sb = new StringBuilder();

//    public static final boolean syncALLAXProduct() {
//        log.info("Entered syncALLAXProduct...");
//        boolean hasError = false;
//        if(!syncProduct(StoreApiChannels.B2C_SLM)) {
//            hasError = true;
//        }
//
//        if(!syncProduct(StoreApiChannels.B2C_MFLG)) {
//            hasError = true;
//        }
//
//        if(!syncProduct(StoreApiChannels.PARTNER_PORTAL_SLM)) {
//            hasError = true;
//        }
//
//        if(!syncProduct(StoreApiChannels.PARTNER_PORTAL_MFLG)) {
//            hasError = true;
//        }
//
//        if(!syncProduct(StoreApiChannels.KIOSK_SLM)) {
//            hasError = true;
//        }
//
//        if(!syncProduct(StoreApiChannels.KIOSK_MFLG)) {
//            hasError = true;
//        }
//
//        return !hasError;
//    }

    public static boolean isSynchInProgress(StoreApiChannels channel) {
        if(syncInProgressMap.get(channel) != null)
        {
            return syncInProgressMap.get(channel);
        }else {
            syncInProgressMap.put(channel, false);
        }

        return false;
    }

    public static void setSynchInProgress(StoreApiChannels channel, boolean inProgress) {
        syncInProgressMap.put(channel, inProgress);
    }


    private static boolean updateAxProductDetails(Map<Long, AxMainProduct> latestProductMapping, Node parentNode, String channel) {
        try {
            List<Node> axProductNodes = NodeUtil.asList(NodeUtil.getNodes(parentNode, JcrWorkspace.AXProducts.getNodeType()));
            for (Node axProductNode : axProductNodes) {
                Long productId = new Long(axProductNode.getName());
                //if contains in coreProduct and also in view
                if (latestProductMapping.containsKey(productId)) {
                    AxMainProduct axProduct = latestProductMapping.get(new Long(axProductNode.getName()));
                    sb.append("<br/>Product Name  [" + axProduct.getProductName() + "] already stored in JCR. Updating the details from AX...<br/>");

                    log.info("Record already in JCR. Updating the JCR node for the AX Product = " + axProduct.getProductName() + ", ListingId = " + axProduct.getRecordId() + "...");

                    assignAxProductNode(channel, axProduct, axProductNode, false);

                    axProductNode.getSession().save();

                    latestProductMapping.remove(axProduct.getRecordId()); //remove the key

                } else {

                    sb.append("<br/>Product Name  [" + PropertyUtil.getString(axProductNode, AXProductProperties.ProductName.getPropertyName()) + "] was not retrieved from AX. This product will be set as Inactive.<br/>");

                    //else if contains in coreProduct but does not exists in view or does not exist in coreProduct at all (in case la)
                    log.info("The Product Listing ID = " + axProductNode.getName() + " was not retrieved. The JCR node related to this will be set to Inactive...");
                    axProductNode.setProperty("status", "Inactive");

                    //delete the promotion relationship as well
                    if(axProductNode.hasNode(AXProductProperties.Promotion.getPropertyName())) {
                        Node promotionNode = axProductNode.getNode(AXProductProperties.Promotion.getPropertyName());
                        promotionNode.remove();
                    }

                    axProductNode.getSession().save();
                }
            }

            //Adding new to JCR
            for (Long mappingKey : latestProductMapping.keySet()) {
                AxMainProduct axProduct = latestProductMapping.get(mappingKey);
                String productListingId = String.valueOf(axProduct.getRecordId());

                //checking just to be sure
                if (!JcrRepository.nodeExists(JcrWorkspace.AXProducts.getWorkspaceName(), parentNode.getPath() + "/" + productListingId)) {

                    sb.append("<br/>Product Name  [" + axProduct.getProductName() + "] is new. Creating a new product with the details from AX...<br/>");

                    log.info("New record found. Creating the JCR node for the AX Product = " + axProduct.getProductName() + "...");
                    Node axProductNode = parentNode.addNode(String.valueOf(axProduct.getRecordId()), JcrWorkspace.AXProducts.getNodeType());
                    axProductNode = assignAxProductNode(channel, axProduct, axProductNode, true);

                    axProductNode.getSession().save();
                }
            }



            try {
                PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.AXProducts.getWorkspaceName());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

            return true;
        } catch (RepositoryException e) {
            return false;
        }
    }

    private static Node assignAxProductNode(String channel, AxMainProduct axProduct, Node axProductNode, boolean isNew) throws RepositoryException {

        sb.append("Product Details: <br/>");

        boolean productNameChanged = false;

        if(!isNew) {
            String productName = PropertyUtil.getString(axProductNode, AXProductProperties.ProductName.getPropertyName());
            String itemName = PropertyUtil.getString(axProductNode, AXProductProperties.ItemName.getPropertyName());
            //TODO if no name
            if(productName != null && productName.equals(itemName) && axProduct.getProductName() != null && !axProduct.getProductName().equals(productName)) {
                productNameChanged = true;
            }
        }

        sb.append("[Record ID] = " + axProduct.getRecordId() + "<br/>");
        axProductNode.setProperty(AXProductProperties.ProductListingId.getPropertyName(), String.valueOf(axProduct.getRecordId()));
        sb.append("[Item ID] = " + axProduct.getItemId() + "<br/>");
        axProductNode.setProperty(AXProductProperties.DisplayProductNumber.getPropertyName(), String.valueOf(axProduct.getItemId()));
        sb.append("[Search Name] = " + axProduct.getSearchName()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.ProductSearchName.getPropertyName(), axProduct.getSearchName());
        sb.append("[Product Name] = " + axProduct.getProductName()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.ProductName.getPropertyName(), axProduct.getProductName());
        sb.append("[Product Price] = " + axProduct.getPrice()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.ProductPrice.getPropertyName(), DataType.getFormattedDisplay(axProduct.getPrice(), DataType.Money.name()));
        axProductNode.setProperty(AXProductProperties.Channel.getPropertyName(), RemoteChannels.getChannelDisplayName(channel));
        axProductNode.setProperty(AXProductProperties.Status.getPropertyName(), "Active");
        sb.append("[Media Type ID] = " + axProduct.getMediaTypeId()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.MediaTypeId.getPropertyName(), axProduct.getMediaTypeId());
        sb.append("[Media Type Description] = " + axProduct.getMediaTypeDescription()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.MediaTypeDescription.getPropertyName(), axProduct.getMediaTypeDescription());
        sb.append("[Is Capacity] = " + axProduct.isCapacity()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.IsCapacity.getPropertyName(), axProduct.isCapacity());
        sb.append("[Own Attraction] = " + axProduct.isOwnAttraction()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.OwnAttraction.getPropertyName(), axProduct.isOwnAttraction());
        sb.append("[Template Name] = " + axProduct.getTemplateName()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.TemplateName.getPropertyName(), axProduct.getTemplateName());
        sb.append("[No of Pax] = " + axProduct.getNoOfPax()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.NoOfPax.getPropertyName(), axProduct.getNoOfPax());
        sb.append("[Is Package] = " + axProduct.isPackage()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.IsPackage.getPropertyName(), axProduct.isPackage());
        sb.append("[Printing] = " + axProduct.getPrinting()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.Printing.getPropertyName(), axProduct.getPrinting());
        sb.append("[Ticket Type] = " + axProduct.getTicketType()+ "<br/>");
        axProductNode.setProperty(AXProductProperties.TicketType.getPropertyName(), axProduct.getTicketType()); //TODO remove this later and replace with the correct one

        if(StringUtils.isNotEmpty(axProduct.getTicketType())) {
            Node ticketTypeNode;
            try {
                //TODO shouldn't call this everytime.. call once only....
               ticketTypeNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/ticket-type/" + axProduct.getTicketType());
               PropertyIterator iterator = ticketTypeNode.getProperties();
               while(iterator.hasNext()) {
                   Property ticketLang = iterator.nextProperty();
                   String ticketLangStr = ticketLang.getName();

                   if(ticketLangStr.startsWith(AXProductProperties.TicketType.getPropertyName())) {
                       axProductNode.setProperty(ticketLangStr,PropertyUtil.getValueString(ticketLang)); //assign the chinese or whatever language it is...
                   }
               }
            }catch (PathNotFoundException e) {
                log.error("No ticket type definition found for " + axProduct.getTicketType() + ".. Please add this in the cms-config so you can have the value for the other language...");
            }
        }


        if(axProduct.getPublishedPrice() == null) {
            sb.append("[Publish Price] = "+ "<br/>");
            axProductNode.setProperty(AXProductProperties.PublishedPrice.getPropertyName(), "");
        }else {
            sb.append("[Publish Price] = " + axProduct.getPublishedPrice()+ "<br/>");
            axProductNode.setProperty(AXProductProperties.PublishedPrice.getPropertyName(), DataType.getFormattedDisplay(axProduct.getPublishedPrice(), DataType.Money.name()));
        }



        if(axProduct.getValidFrom() != null) {
            //Ax product Ticket Validity
            Calendar validFrom = Calendar.getInstance();
            validFrom.setTime(axProduct.getValidFrom());
            axProductNode.setProperty(AXProductPromotionProperties.ValidFrom.getPropertyName(), validFrom);

            sb.append("[Valid From] = " + NvxDateUtils.formatDate(axProduct.getValidFrom(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME)+ "<br/>");
        }else {
            sb.append("[Valid From] = "+ "<br/>");
        }

        if(axProduct.getValidTo() != null) {
            Calendar validTo = Calendar.getInstance();
            validTo.setTime(axProduct.getValidTo());
            axProductNode.setProperty(AXProductPromotionProperties.ValidTo.getPropertyName(), validTo);
            sb.append("[Valid To] = " + NvxDateUtils.formatDate(axProduct.getValidTo(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME)+ "<br/>");
        }else {
            sb.append("[Valid To] = "+ "<br/>");
        }

        //TODO dunno if need to update the item name
        if(isNew || !axProductNode.hasProperty(AXProductProperties.ItemName.getPropertyName()) || productNameChanged) {
            axProductNode.setProperty(AXProductProperties.ItemName.getPropertyName(), axProduct.getProductName()); //new
        }

        if(isNew) {
            sb.append("[Description] = " + axProduct.getDescription()+ "<br/>");
            axProductNode.setProperty(AXProductProperties.Description.getPropertyName(), axProduct.getDescription()); //ok use this one nalng sa start lng
        }

        return axProductNode;
    }

    private static KioskInfoEntity getKioskAccountForLogon(StoreApiChannels channel) {
        KioskInfoEntity kioskInfo = new KioskInfoEntity();
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(),
                    "/kiosk-user-device-account/" + channel.code);

            if (node != null) {
                String deviceToken = node.getProperty("deviceToken").getString();
                kioskInfo.setUserId(node.getProperty("userId").getString());
                kioskInfo.setUserPassword(node.getProperty("userPassword").getString());
                kioskInfo.setDeviceNumber(node.getProperty("deviceNumber").getString());
                kioskInfo.setTerminalNumber(kioskInfo.getDeviceNumber());
                kioskInfo.setJcrNodeUuid(channel.code);
                StarKioskModuleApiBeans kioskContext = Components.getComponent(StarKioskModuleApiBeans.class);
                IKioskManagementService kioskManagementService = kioskContext.getKioskManagementService();

                if (StringUtils.isBlank(deviceToken)) {

                    kioskManagementService.activateDevice(channel, kioskInfo);
                    deviceToken = kioskInfo.getDeviceToken();
                    node.setProperty("deviceToken", deviceToken);
                    node.getSession().save();
                }

                kioskInfo.setDeviceToken(deviceToken);
                kioskInfo.setCurrentTransactionSequence(0);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return kioskInfo;
    }
    
    public static final ApiResult<String> syncProduct(StoreApiChannels channel) {

        //check if anyone is cynhing
        if(!isSynchInProgress(channel)) {

            setSynchInProgress(channel, true);

            log.info("Starting synching of AX Products for Channel " + channel + "...");
            sb.append("Starting synching of AX Products for Channel " + channel + "..."  + "<br/>");

            Date now = new Date();
            StarModuleApiBeans context = Components.getComponent(StarModuleApiBeans.class);
            IProductService productService = context.getProductService();
            ApiResult<AxProductRelatedLinkPackage> relatedLinkPackageResult = null;

            if (StoreApiChannels.KIOSK_SLM == channel || StoreApiChannels.KIOSK_MFLG == channel) {
                StarKioskModuleApiBeans kioskContext = Components.getComponent(StarKioskModuleApiBeans.class);
                IKioskProductService kioskProductService = kioskContext.getProductService();
                relatedLinkPackageResult = kioskProductService.getValidRelatedAxProducts(channel, getKioskAccountForLogon(channel));
            } else {
                relatedLinkPackageResult = productService.getValidRelatedAxProducts(channel);
            }

            if (relatedLinkPackageResult.isSuccess()) {

                AxProductRelatedLinkPackage relatedLinkPackage = relatedLinkPackageResult.getData();

                //getting the ax product
                AxProductLinkPackage axProductLinkPackage = relatedLinkPackage.getAxProductLinkPackage();

                List<AxMainProduct> mainProducts = axProductLinkPackage.getMainProductList();

                Node parentNode = null;
                try {
                    parentNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel.code);
                } catch (RepositoryException e) {
                    try {
                        parentNode = JcrRepository.createNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products", channel.code, "mgnl:folder");
                    } catch (RepositoryException e1) {
                        log.error(e1.getMessage(), e1);
                    }
                    log.error(e.getMessage(), e);
                }

                if(parentNode == null) {
                    log.error("Error in synching Ax products. There's no parent node found for the channel " + channel.code + ".. ");
                    setSynchInProgress(channel, false);
                    return new ApiResult<>(false, "", "", "Error in getting the ax products...");
                }

                Map<Long, AxMainProduct> mainProductsListingIdMap = new HashMap<>();
                List<String> mainProductsListingIds = new ArrayList<>();
                for(AxMainProduct mainProduct: mainProducts) {
                    mainProductsListingIdMap.put(mainProduct.getRecordId(), mainProduct);
                    mainProductsListingIds.add(mainProduct.getRecordId().toString());
                }

                boolean updateAxProductStatus = updateAxProductDetails(mainProductsListingIdMap, parentNode, channel.code);

                if(!updateAxProductStatus) {
                    setSynchInProgress(channel, false);
                    return new ApiResult<>(false, "", "", "Error in getting the ax products...");
                }

                PromotionLinkPackage promotionLinkPackage = relatedLinkPackage.getPromotionLinkPackage();
                ApiResult<String> syncPromotionRes = SyncPromotions.syncPromotions(channel, mainProductsListingIds, promotionLinkPackage);
                // boolean syncPromotionStatus = SyncPromotions.syncPromotions(channel, mainProductsListingIds, promotionLinkPackage);

                if(!syncPromotionRes.isSuccess()) {
                    setSynchInProgress(channel, false);
                    return new ApiResult<>(false, "", "", "Error in getting the ax products...");
                }else {
                    sb.append("<br/> " + syncPromotionRes.getData());
                }

                CrossSellLinkPackage crossSellLinkPackage = relatedLinkPackage.getCrossSellLinkPackage();
                ApiResult<String> syncCrossSellStatus = SyncCrossSells.syncCrossSells(channel, mainProductsListingIds, crossSellLinkPackage);

                if(!syncCrossSellStatus.isSuccess()) {
                    setSynchInProgress(channel, false);
                    return new ApiResult<>(false, "", "", "Error in getting the ax products...");
                }else {
                    sb.append("<br/> " + syncCrossSellStatus.getData());
                }

                //final sync
                try {
                    PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.AXProducts.getWorkspaceName());
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

                //Saved all the data first
                Calendar calNow = Calendar.getInstance();
                calNow.setTime(now);

                //TODO how to handle this better...

                String logNodeName = NvxDateUtils.formatDate(now, "ddMMyyHHmmss");
                try {
                    Node logNode = JcrRepository.createNode(JcrWorkspace.AXSyncLogs.getWorkspaceName(), "/ax-sync-logs/" + channel.code, logNodeName, JcrWorkspace.AXSyncLogs.getNodeType());
                    logNode.setProperty(AXSyncLogsProperties.AxProductJson.getPropertyName(), axProductLinkPackage.getAxProductsResultJson());
                    logNode.setProperty(AXSyncLogsProperties.PromotionJson.getPropertyName(), promotionLinkPackage.getRetailDiscountResultJson());
                    logNode.setProperty(AXSyncLogsProperties.AffiliationJson.getPropertyName(), promotionLinkPackage.getAffiliationResultJson());
                    logNode.setProperty(AXSyncLogsProperties.AxProductExtDataJson.getPropertyName(), axProductLinkPackage.getAxRetailProductExtDataResultJson());
                    logNode.setProperty(AXSyncLogsProperties.CrossSellJson.getPropertyName(), crossSellLinkPackage.getAxRetailUpCrossSellProductResultJson());
                    logNode.setProperty(AXSyncLogsProperties.SyncDate.getPropertyName(), calNow);
                    logNode.setProperty(AXSyncLogsProperties.SyncLog.getPropertyName(), sb.toString());

                    JcrRepository.updateNode(JcrWorkspace.AXSyncLogs.getWorkspaceName(), logNode); //This no need to publish

                } catch (RepositoryException e) {
                    e.printStackTrace();
                }


                setSynchInProgress(channel, false);

                return new ApiResult<>(true, "", "", "");

            }else {
                log.error("Error in getting the ax products...");
                setSynchInProgress(channel, false);
                return new ApiResult<>(false, "", "", "Error in getting the ax products...");
            }
        }else {
            return new ApiResult<>(false, "", "", "Synching is on Progress...");
        }

    }

//    public static void configureAutoSyncJob() {
//
//        //NOTE:
//        //0 MM HH as is
//        //0 37 23 1/1 * ? *
//        SchedulerModule schedulerModule = (SchedulerModule) Components.getComponent(ModuleRegistry.class).getModuleInstance("scheduler");
//
//        if(schedulerModule != null) {
//            //always remove the existing batch job
//            schedulerModule.removeJob("AXProductsAutoSync");
//            //get the schedule
//            Node schedule = null;
//            try {
//                schedule = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/syncProductSchedule");
//            } catch (RepositoryException e) {
//                log.error(e.getMessage(), e);
//                e.printStackTrace();
//            }
//
//            //add the job here if there is a schedule already
//            if(schedule != null) {
//                try {
//                    if(schedule.hasProperty("cron")) {
//                       String cron =  schedule.getProperty("cron").getString();
//                        //TODO need a better way of validating the cron
//                       if(StringUtils.isNotEmpty(cron) && CronExpression.isValidExpression(cron)) {
//                           JobDefinition jobDefinition = new JobDefinition("AXProductsAutoSync", "sync",
//                                   "syncAxProduct", cron , null);
//                           try {
//                               schedulerModule.addJob(jobDefinition);
//                           } catch (SchedulerException e) {
//                               e.printStackTrace();
//                           }
//                       }else {
//                           log.error("Not a valid cron expression: " + cron);
//                       }
//                    }
//                } catch (RepositoryException e) {
//                    log.error(e.getMessage(), e);
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}
