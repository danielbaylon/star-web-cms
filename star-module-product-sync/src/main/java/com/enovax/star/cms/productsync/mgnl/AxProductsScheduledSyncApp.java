package com.enovax.star.cms.productsync.mgnl;

import com.enovax.star.cms.productsync.mgnl.app.ScheduledSyncSubAppDescriptor;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.context.MgnlContext;
import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.app.AppDescriptor;
import info.magnolia.ui.api.app.AppView;
import info.magnolia.ui.api.app.SubAppDescriptor;
import info.magnolia.ui.api.location.DefaultLocation;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.framework.app.BaseApp;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by jennylynsze on 7/25/16.
 */
public class AxProductsScheduledSyncApp extends BaseApp {

    @Inject
    public AxProductsScheduledSyncApp(AppContext appContext, AppView view) {
        super(appContext, view);
    }

    @Override
    public void start(Location location)
    {
        User user = MgnlContext.getUser();
        AppDescriptor appDescriptor = getAppContext().getAppDescriptor();
        Map<String, SubAppDescriptor> map = appDescriptor.getSubApps();

        String firstSubAppLocation = null;
        String firstSubAppNameWithRights = null;

        for (String subAppName : map.keySet()) {
            if(!map.get(subAppName).isClosable()) {
                boolean openSubApp = false;

                SubAppDescriptor subAppDescriptor = map.get(subAppName);
                ScheduledSyncSubAppDescriptor scheduledSubAppDescriptor;

                if(subAppDescriptor instanceof ScheduledSyncSubAppDescriptor) {
                    scheduledSubAppDescriptor = (ScheduledSyncSubAppDescriptor) subAppDescriptor;
                    AccessDefinition accessDefinition = scheduledSubAppDescriptor.getPermissions();

                    if(accessDefinition != null) {
                        if(accessDefinition.hasAccess(user)) {
                            openSubApp = true;
                        }
                    }else {
                        //no rights define means no restrictions
                        openSubApp = true;
                    }

                    if(scheduledSubAppDescriptor.isOpenFirst() && openSubApp) {
                        firstSubAppLocation = subAppName;
                    }

                    //TODO well hardcoded for now :(
                    if(firstSubAppNameWithRights == null && !"default-browser".equals(subAppName)) {
                        firstSubAppNameWithRights = subAppName;
                    }

                }else {
                    //assume that it is using a default browser then maybe it have no rights checking or something
                    openSubApp = true;
                }

                if(openSubApp) {
                    getAppContext().openSubApp(new DefaultLocation("app", getAppContext().getName(), subAppName));

                    if(firstSubAppNameWithRights == null) {
                        firstSubAppNameWithRights = subAppName;
                    }

                }
            }
        }

        if(firstSubAppLocation != null) {
            locationChanged(new DefaultLocation("app", getAppContext().getName(), firstSubAppLocation));
        }else if(firstSubAppNameWithRights != null) {
            locationChanged(new DefaultLocation("app", getAppContext().getName(), firstSubAppNameWithRights));
        }

    }
}
