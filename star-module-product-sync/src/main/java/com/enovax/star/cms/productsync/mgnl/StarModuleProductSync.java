package com.enovax.star.cms.productsync.mgnl;

import com.enovax.mgnl.scheduler.ScheduledTaskDefinition;
import com.enovax.mgnl.scheduler.TaskScheduler;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.productsync.mgnl.tasks.SyncAxProductTask;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by jennylynsze on 3/21/16.
 */
public class StarModuleProductSync implements ModuleLifecycle{

    private static final Logger log = LoggerFactory.getLogger(StarModuleProductSync.class);
    private static final String TaskGroup = "ax-product-sync";
    private static TaskScheduler taskScheduler;
    public static final Map<String, Map<String, String>> DAILY_TASK_LIST = new HashMap<>();

    private static final MagnoliaConfigurationProperties configurationProperties = Components
            .getComponent(MagnoliaConfigurationProperties.class);

    private static final boolean isAuthorInstance = "true".equals(configurationProperties.getProperty("magnolia.bootstrap.authorInstance"));

    @Inject
    public StarModuleProductSync(TaskScheduler taskScheduler) {
        this.taskScheduler = taskScheduler;
    }

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
            initScheduleTask(this.taskScheduler);
        }
    }


    public static void initScheduleTask(TaskScheduler scheduler) {
        //get the options here
        if(!isAuthorInstance) {
            return;
        }

       for(StoreApiChannels channel : StoreApiChannels.values()) {
           Map<String, String> channelDailyTaskList = null;

           if(!DAILY_TASK_LIST.containsKey(channel.code)) {
               DAILY_TASK_LIST.put(channel.code, new HashMap<>()); //
           }

           channelDailyTaskList = DAILY_TASK_LIST.get(channel.code);


           try {
               Node scheduledSyncNode = null;

               if(JcrRepository.nodeExists(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel.code + "/scheduled-sync")) {
                   if("dailySync".equals(PropertyUtil.getString(scheduledSyncNode, "syncOption"))) {
                       Node dailySyncNode = scheduledSyncNode.getNode("dailySync");
                       Iterable<Node> dailySyncChildNodeIterable = NodeUtil.getNodes(dailySyncNode);
                       Iterator<Node> dailySyncChildNodeIterator = dailySyncChildNodeIterable.iterator();

                       while(dailySyncChildNodeIterator.hasNext()) {
                           Node dailySyncDate = dailySyncChildNodeIterator.next();
                           String dailySyncDateArr[] = dailySyncDate.getName().split("_");
                           String HH = dailySyncDateArr[0];
                           String mm = dailySyncDateArr[1];
                           String cron =   "0 " +  mm + " " + HH + " 1/1 * ? *";
                           channelDailyTaskList.put( channel.code + "_D_" + HH + "_" + mm, cron);
                       }

                       for(String key: channelDailyTaskList.keySet()) {
                           addScheduleTask(new SyncAxProductTask(channel), key, channelDailyTaskList.get(key));
                       }
                   }

               }
           } catch (RepositoryException e) {
               log.error(e.getMessage(), e);
           }
       }

    }

    public static void updateScheduleTask(StoreApiChannels channel) {

        if(!isAuthorInstance) {
            return;
        }

        //get the options here
        try {

            Node scheduledSyncNode = JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel.code + "/scheduled-sync");

            Map<String, String> channelDailyTaskList = null;

            if(!DAILY_TASK_LIST.containsKey(channel.code)) {
                DAILY_TASK_LIST.put(channel.code, new HashMap<>()); //
            }

            channelDailyTaskList = DAILY_TASK_LIST.get(channel.code);


            if("dailySync".equals(PropertyUtil.getString(scheduledSyncNode, "syncOption"))) {


                Node dailySyncNode = null;

                if(!scheduledSyncNode.hasNode("dailySync")) {
                   scheduledSyncNode.addNode("dailySync", "mgnl:content");

                }
                dailySyncNode = scheduledSyncNode.getNode("dailySync");

                Iterable<Node> dailySyncChildNodeIterable = NodeUtil.getNodes(dailySyncNode);
                Iterator<Node> dailySyncChildNodeIterator = dailySyncChildNodeIterable.iterator();
                Map<String,String> dailyTaskList = new HashMap<>();

                for(String key: channelDailyTaskList.keySet()) {
                    dailyTaskList.put(key, dailyTaskList.get(key));
                }

                while(dailySyncChildNodeIterator.hasNext()) {
                    Node dailySyncDate = dailySyncChildNodeIterator.next();

                    String dailySyncDateArr[] = dailySyncDate.getName().split("_");
                    String HH = dailySyncDateArr[0];
                    String mm = dailySyncDateArr[1];

                    String cron =   "0 " +  mm + " " + HH + " 1/1 * ? *";
                    String taskName = channel.code + "_D_" + HH + "_" + mm;
                    if(dailyTaskList.containsKey(taskName)) {
                        dailyTaskList.remove(taskName); //remove those arelady in the list
                    }else {
                        channelDailyTaskList.put(taskName, cron);
                        addScheduleTask(new SyncAxProductTask(), taskName, cron); //add the batch job as well
                    }
                }

                for(String key: dailyTaskList.keySet()) {
                    removeScheduleTask(key);
                    DAILY_TASK_LIST.remove(key);
                }

            }else {
                try {
                    if(scheduledSyncNode.hasNode("dailySync")) {
                        Node dailySyncNode = scheduledSyncNode.getNode("dailySync");
                        Iterable<Node> dailySyncChildNodeIterable = NodeUtil.getNodes(dailySyncNode);
                        Iterator<Node> dailySyncChildNodeIterator = dailySyncChildNodeIterable.iterator();

                        while(dailySyncChildNodeIterator.hasNext()) {
                            dailySyncChildNodeIterator.next().remove();
                        }
                        dailySyncNode.getSession().save();
                    }

                }catch (RepositoryException e) {
                    log.error(e.getMessage(), e);
                }

                for(String key : channelDailyTaskList.keySet()) {
                    removeScheduleTask(key);
                }

                channelDailyTaskList.clear();
            }


        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
           // Hibernate.shutdown();
        }
    }

    private static <T extends Job> void addScheduleTask(T job, String name, String cron) {
        try {
            ScheduledTaskDefinition<T> task = new ScheduledTaskDefinition<>(job, TaskGroup, name, cron, false);
            log.trace("Adding schedule task: [" + task.getGroup() + " - " + task.getName() + " (" + task.getCron() + ")]");
            StarModuleProductSync.taskScheduler.addTask(task);
        } catch (Exception e) {
            log.error("addScheduleTask", e);
        }
    }

    private static void removeScheduleTask(String name) {
        try {
            log.trace("Stopping schedule task: [" + TaskGroup + " - " + name);
            StarModuleProductSync.taskScheduler.stopTask(name, TaskGroup);
        } catch (Exception e) {
            log.error("addScheduleTask", e);
        }
    }

//    private void observeSyncProperties() {
//        ObservationUtil.registerChangeListener(JcrWorkspace.CMSConfig.getWorkspaceName(),
//                "/syncProductSchedule",
//                false,
//                "mgnl:content",
//                Event.PROPERTY_CHANGED,
//                new EventListener() {
//                    @Override
//                    public void onEvent(EventIterator events) {
//                        try {
//                            while (events.hasNext()) {
//                                final Event event = events.nextEvent();
//                                System.out.println(event.getIdentifier());
//                                System.out.println(event.getInfo());
//                                System.out.println("value change?");
//                            }
//                        } catch (Exception e) {
//                           e.printStackTrace();
//                        }
//                    }
//                });
//    }
}
