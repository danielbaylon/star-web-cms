package com.enovax.star.cms.productsync.mgnl.app;

import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.ui.framework.app.embedded.EmbeddedPageSubAppDescriptor;

/**
 * Created by jennylynsze on 7/25/16.
 */
public class ScheduledSyncSubAppDescriptor extends EmbeddedPageSubAppDescriptor {

    private AccessDefinition permissions;
    private boolean openFirst;
    private String channel;

    public ScheduledSyncSubAppDescriptor() {
    }

    public AccessDefinition getPermissions() {
        return permissions;
    }

    public void setPermissions(AccessDefinition permissions) {
        this.permissions = permissions;
    }

    public boolean isOpenFirst() {
        return openFirst;
    }

    public void setOpenFirst(boolean openFirst) {
        this.openFirst = openFirst;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}

