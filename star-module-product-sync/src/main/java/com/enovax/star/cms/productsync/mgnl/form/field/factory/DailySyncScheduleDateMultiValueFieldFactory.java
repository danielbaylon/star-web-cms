package com.enovax.star.cms.productsync.mgnl.form.field.factory;

import com.enovax.star.cms.productsync.mgnl.event.SyncOptionValueChangedEvent;
import com.enovax.star.cms.productsync.mgnl.form.field.DailySyncScheduleDateMultiValueField;
import com.enovax.star.cms.productsync.mgnl.form.field.definition.DailySyncScheduleDateMultiValueFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.ui.Field;
import info.magnolia.event.EventBus;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;
import info.magnolia.ui.form.field.factory.MultiValueFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;

/**
 * Created by jennylynsze on 7/29/16.
 */
public class DailySyncScheduleDateMultiValueFieldFactory extends MultiValueFieldFactory implements SyncOptionValueChangedEvent.Handler {

    private final EventBus subAppEventBus;
    private final String syncOption;
    private String selectedValue;
    private Field field;
    private final FieldFactoryFactory fieldFactoryFactory;
    private final ComponentProvider componentProvider;
    private final I18NAuthoringSupport i18nAuthoringSupport;
    private DailySyncScheduleDateMultiValueField multiValueField;
    private DailySyncScheduleDateMultiValueFieldDefinition definition;

    @Inject
    public DailySyncScheduleDateMultiValueFieldFactory(DailySyncScheduleDateMultiValueFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider, @Named(SubAppEventBus.NAME) EventBus subAppEventBus) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, fieldFactoryFactory, componentProvider);
        this.subAppEventBus = subAppEventBus;
        this.subAppEventBus.addHandler(SyncOptionValueChangedEvent.class, this);
        this.syncOption = definition.getSyncOption();
        this.fieldFactoryFactory = fieldFactoryFactory;
        this.componentProvider = componentProvider;
        this.i18nAuthoringSupport = i18nAuthoringSupport;
        this.definition = definition;

        if(relatedFieldItem instanceof JcrNodeAdapter) {
            Node scheduleNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();
            this.selectedValue = PropertyUtil.getString(scheduleNode, "syncOption");
        }
    }


    @Override
    public Field createField() {
        this.field = super.createField();
        setVisible();
        return super.createField();
    }


    @Override
    protected Field<PropertysetItem> createFieldComponent() {
        this.multiValueField = new DailySyncScheduleDateMultiValueField(definition, fieldFactoryFactory, componentProvider, item, i18nAuthoringSupport);
        // Set Caption
        this.multiValueField.setButtonCaptionAdd("ADD");
        this.multiValueField.setButtonCaptionRemove("REMOVE");
        return this.multiValueField;
    }

    @Override
    public void onSyncOptionValueChanged(SyncOptionValueChangedEvent event) {
        if (event.getSelectedValue() == null || StringUtils.isBlank(syncOption)) {
            return;
        }
        this.selectedValue = event.getSelectedValue();

        setVisible();
    }

    private void setVisible() {
        if(this.syncOption.equals(this.selectedValue)) {
            this.field.setVisible(true);
        }else {
            this.field.setVisible(false);

            if(this.multiValueField != null) {
                this.multiValueField.removeAllComponents();
            }

            //need a way to remove the fields also. :(
        }
    }
}
