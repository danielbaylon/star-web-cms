package com.enovax.star.cms.productsync.repository;

import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class Hibernate {

    private static final Logger log = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    private static final String B2CSLM_PATH = "/WEB-INF/config/repo-conf/b2c-slm-admin-hibernate.xml";
    private static final String B2CMFLG_PATH = "/WEB-INF/config/repo-conf/b2c-mflg-admin-hibernate.xml";
    private static final String PPSLM_PATH = "/WEB-INF/config/repo-conf/partner-portal-slm-admin-hibernate.xml";
    private static final String PPMFLG_PATH = "/WEB-INF/config/repo-conf/partner-portal-mflg-admin-hibernate.xml";
    private static final String KIOSKSLM_PATH = "/WEB-INF/config/repo-conf/kiosk-slm-admin-hibernate.xml";
    private static final String KIOSKMFLG_PATH = "/WEB-INF/config/repo-conf/kiosk-mflg-admin-hibernate.xml";

    private static SessionFactory B2CSLMSessionFactory;
    private static SessionFactory B2CMFLGSessionFactory;
    private static SessionFactory PPSLMSessionFactory;
    private static SessionFactory PPMFLGSessionFactory;
    private static SessionFactory KioskSLMSessionFactory;
    private static SessionFactory KioskMFLGSessionFactory;

    private static final MagnoliaConfigurationProperties mgnlConfiguration = Components.getComponent(MagnoliaConfigurationProperties.class);

    public static Session openB2CSLMSession() {
        if(B2CSLMSessionFactory == null) {
            initB2CSLM();
        }
        return B2CSLMSessionFactory.openSession();
    }

    public static Session openB2CMFLGSession() {
        if(B2CMFLGSessionFactory == null) {
            initB2CMFLG();
        }
        return B2CMFLGSessionFactory.openSession();
    }

    public static Session openPPSLMSession() {
        if(PPSLMSessionFactory == null) {
            initPPSLM();
        }
        return PPSLMSessionFactory.openSession();
    }

    public static Session openPPMFLGSession() {
        if(PPMFLGSessionFactory == null) {
            initPPSLM();
        }
        return PPMFLGSessionFactory.openSession();
    }

    public static Session openKioskSLMSession() {
        if(KioskSLMSessionFactory == null) {
            initKioskSLM();
        }
        return KioskSLMSessionFactory.openSession();
    }

    public static Session openKioskMFLGSession() {
        if(KioskMFLGSessionFactory == null) {
            initKioskMFLG();
        }
        return KioskMFLGSessionFactory.openSession();
    }



    public static void init() {
        initB2CSLM();
        initB2CMFLG();
        initPPSLM();
        initPPMFLG();
        initKioskSLM();
        initKioskMFLG();
        initKioskMFLG();
    }

    private static void initB2CSLM() {
        try {
            String hibernateConfigPath = mgnlConfiguration.getProperty("star.b2c.slm.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + B2CSLM_PATH;
            }
            log.info("CMS Build hibernate session factory for B2C SLM using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            B2CSLMSessionFactory= configuration.buildSessionFactory(builder.build());

        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private static void initB2CMFLG() {
        try {
            String hibernateConfigPath = mgnlConfiguration.getProperty("star.b2c.mflg.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + B2CMFLG_PATH;
            }

            log.info("CMS Build hibernate session factory for B2C MFLG using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            B2CMFLGSessionFactory= configuration.buildSessionFactory(builder.build());
        }catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void initPPSLM() {
        try {
            String hibernateConfigPath = mgnlConfiguration.getProperty("star.pp.slm.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + PPSLM_PATH;
            }

            log.info("CMS Build hibernate session factory for PP_SLM using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            PPSLMSessionFactory = configuration.buildSessionFactory(builder.build());
        }catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void initPPMFLG() {
        try {
            String hibernateConfigPath = mgnlConfiguration.getProperty("star.pp.mflg.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + PPMFLG_PATH;
            }

            log.info("CMS Build hibernate session factory for PP_SLM using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            PPMFLGSessionFactory= configuration.buildSessionFactory(builder.build());
        }catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    private static void initKioskSLM() {
        try {
            String hibernateConfigPath = mgnlConfiguration.getProperty("star.kiosk.slm.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + KIOSKSLM_PATH;
            }
            log.info("CMS Build hibernate session factory for KIOSK_SLM using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            KioskSLMSessionFactory = configuration.buildSessionFactory(builder.build());
        }catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void initKioskMFLG() {
        try {

            String hibernateConfigPath = mgnlConfiguration.getProperty("star.kiosk.mflg.admin.hibernate.config");

            if(StringUtils.isEmpty(hibernateConfigPath)) {
                hibernateConfigPath = mgnlConfiguration.getProperty("magnolia.home") + KIOSKMFLG_PATH;
            }
            log.info("CMS Build hibernate session factory for KIOSK_MFLG using config file {}", hibernateConfigPath);
            File hibernateConfigFile = new File(hibernateConfigPath);
            Configuration configuration = new Configuration().configure(hibernateConfigFile);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            KioskMFLGSessionFactory= configuration.buildSessionFactory(builder.build());
        }catch(Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void shutdown() {
        log.info("CMS Hibernate shutdown");
        closeB2CSLM();
        closeB2CMFLG();
        closePPSLM();
        closePPMFLG();
        closeKioskSLM();
        closeKioskMFLG();
    }


    private static void closeB2CSLM() {
        if(B2CSLMSessionFactory != null) {
            B2CSLMSessionFactory.close();
        }
    }

    private static void closeB2CMFLG() {
        if(B2CMFLGSessionFactory != null) {
            B2CMFLGSessionFactory.close();
        }
    }

    private static void closePPSLM() {
        if(PPSLMSessionFactory != null) {
            PPSLMSessionFactory.close();
        }
    }

    private static void closePPMFLG() {
        if(PPMFLGSessionFactory != null) {
            PPMFLGSessionFactory.close();
        }
    }

    private static void closeKioskSLM() {
        if(KioskSLMSessionFactory != null) {
            KioskSLMSessionFactory.close();
        }
    }

    private static void closeKioskMFLG() {
        if(KioskMFLGSessionFactory != null) {
            KioskMFLGSessionFactory.close();
        }
    }
}
