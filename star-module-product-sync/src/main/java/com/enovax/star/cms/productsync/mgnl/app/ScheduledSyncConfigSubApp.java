package com.enovax.star.cms.productsync.mgnl.app;

import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.framework.app.embedded.EmbeddedPageSubApp;
import info.magnolia.ui.framework.app.embedded.EmbeddedPageView;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 7/25/16.
 */
public class ScheduledSyncConfigSubApp extends EmbeddedPageSubApp {
    private SimpleTranslator translator;

    @Inject
    public ScheduledSyncConfigSubApp(SubAppContext subAppContext, EmbeddedPageView pageView) {
        super(subAppContext, pageView);
    }
}
