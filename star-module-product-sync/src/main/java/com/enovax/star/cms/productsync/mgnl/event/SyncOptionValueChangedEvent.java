package com.enovax.star.cms.productsync.mgnl.event;

import info.magnolia.event.Event;
import info.magnolia.event.EventHandler;

/**
 * Created by jennylynsze on 7/25/16.
 */
public class SyncOptionValueChangedEvent implements Event<SyncOptionValueChangedEvent .Handler> {

    private String selectedValue;

    public SyncOptionValueChangedEvent(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public interface Handler extends EventHandler {
        void onSyncOptionValueChanged( SyncOptionValueChangedEvent event);
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    @Override
    public void dispatch(Handler handler) {
        handler.onSyncOptionValueChanged(this);
    }

}