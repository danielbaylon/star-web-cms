package com.enovax.star.cms.productsync.mgnl.tasks;

import info.magnolia.cms.security.SilentSessionOp;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.SimpleContext;
import info.magnolia.context.SystemContext;
import info.magnolia.objectfactory.Components;

import java.util.Map;

/**
 * Created by jennylynsze on 7/25/16.
 */
public abstract class SilentSessionOpTask {
    protected <T> T execute(SilentSessionOp<T> sessionOp) {
        MgnlContext.setInstance(new SimpleContext((Map) Components.getComponent(SystemContext.class)));
        return MgnlContext.doInSystemContext(sessionOp);
    }
}
