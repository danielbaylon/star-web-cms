package com.enovax.star.cms.productsync.mgnl.component;

import com.enovax.star.cms.commons.service.product.IProductService;
import info.magnolia.objectfactory.Components;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

/**
 * Created by jennylynsze on 7/19/16.
 */
public class ApiBeanComponent {

    @Autowired
    private IProductService productService;

    public ApiBeanComponent() {
        ServletContext servletContext = Components.getComponent(ServletContext.class);
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        wac.getAutowireCapableBeanFactory().autowireBean(this);
    }

    public IProductService getProductService() {
        return productService;
    }
}
