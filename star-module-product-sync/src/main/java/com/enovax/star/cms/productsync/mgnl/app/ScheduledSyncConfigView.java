package com.enovax.star.cms.productsync.mgnl.app;

import info.magnolia.ui.api.view.View;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.form.FormViewReduced;

import java.util.Map;

/**
 * Created by jennylynsze on 7/25/16.
 */
/**
 * Not used
 */
public interface ScheduledSyncConfigView extends View, EditorValidator {
    void setListener(ScheduledSyncConfigView.Listener var1);

    void setFormViewReduced(Map<String, FormViewReduced> var);

    void build();

    public interface Listener {
        void save();

        void reset();
    }
}
