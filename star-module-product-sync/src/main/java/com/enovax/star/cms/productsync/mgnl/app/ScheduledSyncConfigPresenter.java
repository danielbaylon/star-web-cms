package com.enovax.star.cms.productsync.mgnl.app;

/**
 * Created by jennylynsze on 7/25/16.
 */

/**
 * Not used
 */
public class ScheduledSyncConfigPresenter implements ScheduledSyncConfigView.Listener {
    @Override
    public void save() {

    }

    @Override
    public void reset() {

    }
//    private ScheduledSyncConfigView view;
//    private FormBuilder builder;
//    private ComponentProvider componentProvider;
//    private FormDefinition formDefinition;
//    private FormDefinition logDefinition;
//    private JcrNodeAdapter jcrItem;
//    private UiContext context;
//    private SimpleTranslator translator;
//    private static final Logger log = LoggerFactory.getLogger( ScheduledSyncConfigPresenter.class);
//    private final TaskScheduler taskScheduler;
//    private String channel;
//
//    @Inject
//    public ScheduledSyncConfigPresenter(ScheduledSyncConfigView view, FormBuilder builder, ComponentProvider componentProvider, SubAppContext subAppContext, UiContext context, SimpleTranslator translator, TaskScheduler taskScheduler) {
//        this.view = view;
//        this.builder = builder;
//        this.componentProvider = componentProvider;
//        this.context = context;
//        this.translator = translator;
//
//        ScheduledSyncSubAppDescriptor descriptor = (ScheduledSyncSubAppDescriptor)subAppContext.getSubAppDescriptor();
//        this.channel = descriptor.getChannel();
//        this.formDefinition = (FormDefinition)(descriptor.getFormDefinitions()).get("config");
//        this.logDefinition =  (FormDefinition)(descriptor.getFormDefinitions()).get("log");
//        this.jcrItem = new JcrNodeAdapter(getScheduledSyncConfigurationNode(channel));
//        this.taskScheduler = taskScheduler;
//    }
//
//    public ScheduledSyncConfigView start() {
////        FormViewReduced formView = (FormViewReduced)this.componentProvider.getComponent(FormViewReduced.class);
////        this.builder.buildReducedForm(this.formDefinition, formView, this.jcrItem, (FormItem)null);
////        this.view.setListener(this);
////        this.view.setFormViewReduced(formView);
////        this.view.build();
////        return this.view;
////
//
//
//        HashMap formViews = new HashMap();
//        FormViewReduced formViewReduced = (FormViewReduced)this.componentProvider.getComponent(FormViewReduced.class);
//        this.builder.buildReducedForm(this.formDefinition, formViewReduced, this.jcrItem, (FormItem) null);
//        formViews.put("config", formViewReduced);
//        formViewReduced = (FormViewReduced)this.componentProvider.getComponent(FormViewReduced.class);
//        this.builder.buildReducedForm(this.logDefinition, formViewReduced, this.jcrItem, (FormItem) null);
//        formViews.put("log", formViewReduced);
//        this.view.setFormViewReduced(formViews);
//        this.view.build();
//        this.view.setListener(this);
//        return this.view;
//    }
//
//    public void save() {
//        this.view.showValidation(true);
//        if(this.view.isValid()) {
//            try {
//                Node e = this.jcrItem.applyChanges();
//                e.getSession().save();
//
//                //update the batch jobs
//                StarModuleProductSync.updateScheduleTask(StoreApiChannels.fromCode(this.channel));
//
//                this.context.openNotification(MessageStyleTypeEnum.INFO, true, this.translator.translate("mail.app.main.button.save.success", new Object[0]));
//            } catch (RepositoryException var2) {
//                log.error(var2.getMessage(), var2);
//                this.context.openNotification(MessageStyleTypeEnum.ERROR, true, this.translator.translate("mail.app.main.button.save.error", new Object[0]));
//            }
//        }
//
//    }
//
//    public void reset() {
//        HashMap formViews = new HashMap();
//        FormViewReduced formViewReduced = (FormViewReduced)this.componentProvider.getComponent(FormViewReduced.class);
//        this.builder.buildReducedForm(this.formDefinition, formViewReduced, this.jcrItem, (FormItem) null);
//        formViews.put("config", formViewReduced);
//        formViewReduced = (FormViewReduced)this.componentProvider.getComponent(FormViewReduced.class);
//        this.builder.buildReducedForm(this.logDefinition, formViewReduced, this.jcrItem, (FormItem) null);
//        formViews.put("log", formViewReduced);
//        this.view.setFormViewReduced(formViews);
//
//        this.context.openNotification(MessageStyleTypeEnum.INFO, true, this.translator.translate("mail.app.main.button.reset.success", new Object[0]));
//    }
//
//    protected static Node getScheduledSyncConfigurationNode(String channel) {
//        return SessionUtil.getNode("product-sync-config", "/" + channel + "/scheduled-sync");
//    }
}

