<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Session :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/session-view.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">

  <div class="row-fluid admin-main">
    <div class="row-fluid admin-main" id="session-view">
      <div class="span20">
        <div class="product-title">
          <h2 data-bind="text: headerText">Session Name</h2>

          <div data-bind="visible: !isNew()" class="active-sect">
            <span class="menu-active-ind"
                  data-bind="text:isActive()?'Active':'Inactive', style:{'color':isActive()?'#038603':'#C40202'}"></span>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span4">
          <div class="float-menu" data-spy="affix" data-offset-top="40">
            <div class="menu-active-section">
              <h3>Sections</h3>
              <ul id="navbar" class="navbar nav menu-links">
                <li><a href="#info-head">Session Information</a></li>
                <li data-bind="visible: !isNew()"><a href="#packages-head">Packages</a></li>
                <li data-bind="visible: !isNew()"><a href="#blockout-head">Blockout Dates</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="span16" style="min-height: 1000px; margin-bottom: 100px;">
          <div class="product-main">
            <div class="product-header" id="info-head">
              <h3>Session Information</h3>
              <a class="btn btn-primary header-btn" data-bind="visible: !isEditingMainInfo(), click: doEditMainInfo">Edit</a>
              <a class="btn header-btn" data-bind="visible: isEditingMainInfo, click: doCancelMainInfo">Cancel</a>
              <a class="btn btn-success header-btn" data-bind="visible: isEditingMainInfo, click: doSaveMainInfo">Save
                Changes</a>
            </div>
            <div class="product-sect">
              <div class="row-fluid field-row" style="display: none;" data-bind="visible: mainInfoMessage() != ''">
                <div class="offset1 span18 alert"
                     data-bind="css:{'alert-error':mainInfoHasError}, html: mainInfoMessage"></div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Session Name</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: name"></span>
                  <input class="input-xxlarge" type="text" maxlength="300"
                         data-bind="visible: isEditingMainInfo, value: newName"/>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Session Number</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: sessionNumber"></span>
                  <input class="input-small" type="number" min="0"
                         data-bind="visible: isEditingMainInfo, value: newSessionNumber"/>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Session Type</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: sessionTypeText"></span>
                  <select class="input-medium" data-bind="visible: isEditingMainInfo, value: newSessionType">
                    <option value="0">Date</option>
                    <option value="1">Normal</option>
                  </select>
                </div>
              </div>
              <div class="row-fluid field-row" data-bind="visible: newSessionType() == '1'">
                <div class="offset1 span5 field-label">Effective Days</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: effectiveDaysText"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <div><input type="checkbox" value="1" data-bind="checked: newEffectiveDays"> Monday</div>
                    <div><input type="checkbox" value="2" data-bind="checked: newEffectiveDays"> Tuesday</div>
                    <div><input type="checkbox" value="3" data-bind="checked: newEffectiveDays"> Wednesday</div>
                    <div><input type="checkbox" value="4" data-bind="checked: newEffectiveDays"> Thursday</div>
                    <div><input type="checkbox" value="5" data-bind="checked: newEffectiveDays"> Friday</div>
                    <div><input type="checkbox" value="6" data-bind="checked: newEffectiveDays"> Saturday</div>
                    <div><input type="checkbox" value="7" data-bind="checked: newEffectiveDays"> Sunday</div>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">From Date</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: startDate"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <input class="input-medium" type="text" id="from-date-field" data-bind="value: newStartDate"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">End Date</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: endDate"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <input class="input-medium" type="text" id="end-date-field" data-bind="value: newEndDate"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Time</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: time"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <input class="input-medium" type="text" id="time-field" data-bind="value: newTime"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Start of Sales Period</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: salesStartDate"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <input class="input-medium" type="text" id="sales-start-field"
                           data-bind="value: newSalesStartDate"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">End of Sales Period</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: salesEndDate"></span>

                  <div data-bind="visible: isEditingMainInfo">
                    <input class="input-medium" type="text" id="sales-end-field" data-bind="value: newSalesEndDate"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Capacity</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingMainInfo(), text: capacity"></span>
                  <input class="input-small" type="number" min="1"
                         data-bind="visible: isEditingMainInfo, value: newCapacity"/>
                </div>
              </div>
            </div>
          </div>
          <div class="product-main" data-bind="visible: !isNew()">
            <div class="product-header" id="packages-head">
              <h3>Packages</h3>
            </div>
            <div class="product-sect">
              <div class="data_grid grid-wrapper prod-grid" id="packages-grid"></div>
              <div class="grid-actions">
                <a class="btn btn-danger" id="remove-package-button" data-bind="click: doRemovePackage">Remove Selected
                  Packages</a>
                <a class="btn btn-primary" id="add-package-button" data-bind="click: doAddPackage">Add Package</a>
              </div>
            </div>
          </div>
          <div class="product-main" data-bind="visible: !isNew()">
            <div class="product-header" id="blockout-head">
              <h3>Blockout Dates</h3>
            </div>
            <div class="product-sect">
              <div class="data_grid grid-wrapper prod-grid" id="blockouts-grid"></div>
              <div class="grid-actions">
                <a class="btn btn-danger" id="remove-blockout-button" data-bind="click: doRemoveBlockOut">Remove
                  Blockout Dates</a>
                <a class="btn btn-primary" id="add-blockout-button" data-bind="click: doAddBlockOut">Add Blockout
                  Dates</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="blockout-modal" style="display: none;" class="admin-modal">
      <div class="row-fluid">
        <div class="span20">
          <form class="form-horizontal">
            <div class="alert alert-error" style="display: none;"
                 data-bind="visible: blockOutHasError, html: blockOutMessage"></div>
            <div class="control-group">
              <label class="control-label">Start Date</label>

              <div class="controls">
                <input type="text" id="blockout-start-date" data-bind="value: newStartDate">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">End Date</label>

              <div class="controls">
                <input type="text" id="blockout-end-date" data-bind="value: newEndDate">
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <a class="btn btn-success" data-bind="click: doSaveBlockOut">Save</a>
                <a class="btn" data-bind="click: doCancelBlockOut">Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="package-modal" style="display: none;" class="admin-modal">
      <div class="row-fluid">
        <div class="span20">
          <div class="data_grid grid-wrapper prod-grid" id="select-packages-grid"></div>
          <div style="margin-top: 10px;" class="grid-actions">
            <a class="btn btn-primary" id="add-selected-packages">Add Selected</a>
            <a class="btn" id="cancel-package-select">Cancel</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>