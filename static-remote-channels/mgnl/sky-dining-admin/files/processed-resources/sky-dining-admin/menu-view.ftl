<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Menu :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/tinymce/scriptloader-tinymce.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/menu-view.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main" id="menu-view">
    <div class="span20">
      <div class="product-title">
        <h2 data-bind="text: headerText">Menu Name</h2>

        <div class="clearfix"></div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span4">
        <div class="float-menu" data-spy="affix" data-offset-top="40">
          <div class="menu-active-section">
            <h3>Sections</h3>
            <ul id="navbar" class="navbar nav menu-links">
              <li><a href="#menu-head">Menu Information</a></li>
              <li><a href="#display-head">Display Information</a></li>
              <li data-bind="visible: !isNew()"><a href="#main-courses-head">Main Courses</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="span16" style="min-height: 1000px; margin-bottom: 100px;">
        <div class="product-main">
          <div class="product-header" id="menu-head">
            <h3>Menu Information</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingMenuInfo(), click: doEditMenuInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingMenuInfo, click: doCancelMenuInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingMenuInfo, click: doSaveMenuInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: menuInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':menuInfoHasError}, html: menuInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Menu Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: name"></span>
                <input class="input-xxlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingMenuInfo, value: newName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Start of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: startDate"></span>

                <div data-bind="visible: isEditingMenuInfo">
                  <input class="input-medium" type="text" id="start-date-field" data-bind="value: newStartDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">End of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: endDate"></span>

                <div data-bind="visible: isEditingMenuInfo">
                  <input class="input-medium" type="text" id="end-date-field" data-bind="value: newEndDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Sales Cut-off Before Event Date (Days)</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: salesCutOffDays"></span>
                <input id="sales-cutoff-field" class="input-small" type="number" min="0"
                       data-bind="visible: isEditingMenuInfo, value: newSalesCutOffDays"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Price in Cents (Per Pax)</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: price"></span>
                <input class="input-small" type="number" min="0"
                       data-bind="visible: isEditingMenuInfo, value: newPrice"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Service Charges</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: serviceCharge() + '%'"></span>
                <input class="input-small" type="number" min="0"
                       data-bind="visible: isEditingMenuInfo, value: newServiceCharge"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">GST</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingMenuInfo(), text: gst() + '%'"></span>
                <input class="input-small" type="number" min="0" data-bind="visible: isEditingMenuInfo, value: newGst"/>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="display-head">
            <h3>Display Information</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingDisplayInfo(), click: doEditDisplayInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingDisplayInfo, click: doCancelDisplayInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingDisplayInfo, click: doSaveDisplayInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: displayInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':displayInfoHasError}, html: displayInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingDisplayInfo(), text: name"></span>
                <input class="input-xxlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingDisplayInfo, value: newName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Description</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingDisplayInfo() && description()==''">no description specified</span>
                <span data-bind="visible: !isEditingDisplayInfo() && description()!='', html: description"></span>

                <div data-bind="visible: isEditingDisplayInfo">
                  <textarea maxlength="4000" id="description-field" data-bind="value: newDescription"
                            style="height: 300px; width: 100%;"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="main-courses-head">
            <h3>Main Courses</h3>
          </div>
          <div class="product-sect">
            <div class="data_grid grid-wrapper prod-grid" id="main-courses-grid"></div>
            <div class="grid-actions">
              <a class="btn btn-danger" id="remove-main-course-button" data-bind="click: doRemoveMainCourse">Remove Main
                Courses</a>
              <a class="btn btn-primary" id="add-main-course-button" data-bind="click: doAddMainCourse">Add Main
                Courses</a>
              <a class="btn btn-success" id="add-main-course-button" data-bind="click: doCreateMainCourse">Create Main
                Course</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="main-course-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <form class="form-horizontal">
          <div class="alert alert-error" style="display: none;"
               data-bind="visible: mainCourseHasError, html: mainCourseMessage"></div>
          <div class="control-group">
            <label class="control-label">Main Course Code</label>

            <div class="controls">
              <input type="text" maxlength="50" class="input-small" data-bind="value: newCode"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Main Course Type</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newType">
                <option value="0">Standard</option>
                <option value="1">Kids</option>
                <option value="2">Vegetarian</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Description</label>

            <div class="controls">
              <textarea rows="4" maxlength="4000" class="input-xxlarge" name="newDescription"
                        data-bind="value: newDescription"></textarea>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <a class="btn btn-success" data-bind="click: doSaveMainCourse">Save</a>
              <a class="btn" data-bind="click: doCancelMainCourse">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="select-main-course-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <div class="data_grid grid-wrapper prod-grid" id="select-main-courses-grid"></div>
        <div style="margin-top: 10px;" class="grid-actions">
          <a class="btn btn-primary" id="add-selected-main-courses">Add Selected</a>
          <a class="btn" id="cancel-main-course-select">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>