<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Schedule :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/sessions.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main">
    <div class="span20 grid-section">
      <h3>Schedule</h3>
      <div class="data_grid grid-wrapper" id="sessions-grid"></div>
      <div class="grid-actions">
        <a class="btn btn-danger" id="remove-button">Remove Selected Sessions</a>
        <a class="btn btn-success" id="add-button">New Session</a>
      </div>
    </div>
  </div>
</div>
</body>
