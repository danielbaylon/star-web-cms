<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Reservation :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/tinymce/scriptloader-tinymce.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/reservation-view.js"></script>
  <script src="/resources/sky-dining-admin/js/reservation-view-grids.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main" id="reservation-view">
    <div class="span20">
      <div class="product-title">
        <h2 data-bind="text: headerText">Edit Reservation</h2>

        <div class="clearfix"></div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span4">
        <div class="float-menu" data-spy="affix" data-offset-top="40">
          <div class="menu-active-section">
            <h3>Sections</h3>
            <ul id="navbar" class="navbar nav menu-links">
              <li><a href="#guest-head">Guest Information</a></li>
              <li><a href="#package-head">Package Information</a></li>
              <li><a href="#maincourse-head">Main Courses</a></li>
              <li><a href="#topup-head">Top Ups</a></li>
              <li><a href="#special-head">Special Request</a></li>
              <li><a href="#summary-head">Transaction Summary</a></li>
              <li data-bind="visible: !isNew()"><a href="#transaction-head">Payment Details</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="span16" style="min-height: 1000px; margin-bottom: 100px;">
        <div class="product-main">
          <div class="product-header" id="guest-head">
            <h3>Guest Information</h3>
            <a class="btn btn-primary header-btn" data-bind="visible: !isEditingGuestInfo(), click: doEditGuestInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingGuestInfo, click: doCancelGuestInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingGuestInfo, click: doSaveGuestInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: guestInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':guestInfoHasError}, html: guestInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Guest Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingGuestInfo(), text: guestName"></span>
                <input class="input-xxlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingGuestInfo, value: newGuestName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Contact Number</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingGuestInfo(), text: contactNumber"></span>
                <input class="input-xlarge" type="text" maxlength="100"
                       data-bind="visible: isEditingGuestInfo, value: newContactNumber"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Email Address</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingGuestInfo(), text: emailAddress"></span>
                <input class="input-xlarge" type="text" maxlength="200"
                       data-bind="visible: isEditingGuestInfo, value: newEmailAddress"/>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="package-head">
            <h3>Package Information</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingPackageInfo(), click: doEditPackageInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingPackageInfo, click: doCancelPackageInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingPackageInfo, click: doSavePackageInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: packageInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':packageInfoHasError}, html: packageInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Date</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: dateFormatted"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input class="input-medium" type="text" id="date-field" data-bind="value: newDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Time</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: time"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input class="input-medium" type="text" id="time-field" data-bind="value: newTime"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Package</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: packageName"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input type="hidden" id="package-field"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Session</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: sessionName"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input type="hidden" id="session-field"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Theme</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingPackageInfo() && themeDescription() == ''">no theme selected</span>
                <span
                  data-bind="visible: !isEditingPackageInfo() && themeDescription() !='', text: themeDescription"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input type="hidden" id="theme-field"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Number of Pax</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: pax"></span>
                <input class="input-small" id="pax-field" type="number" min="1"
                       data-bind="visible: isEditingPackageInfo, value: newPax"
                       onchange="mflg.reservationModel.doComputePrices();"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Menu</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: menuName"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input type="hidden" id="menu-field"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Menu Price</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: menuPriceText"></span>
                <span data-bind="visible: isEditingPackageInfo(), text: newMenuPriceText"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="maincourse-head">
            <h3>Main Courses</h3>
            <a class="btn btn-primary header-btn" data-bind="visible: !isEditingMainCourse(), click: doEditMainCourse">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingMainCourse, click: doCancelMainCourse">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingMainCourse, click: doSaveMainCourse">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: mainCourseMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':mainCourseHasError}, html: mainCourseMessage"></div>
            </div>
            <div class="data_grid grid-wrapper prod-grid" id="main-courses-grid"></div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="topup-head">
            <h3>Top Ups</h3>
            <a class="btn btn-primary header-btn" data-bind="visible: !isEditingTopUp(), click: doEditTopUp">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingTopUp, click: doCancelTopUp">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingTopUp, click: doSaveTopUp">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: topUpMessage() != ''">
              <div class="offset1 span18 alert" data-bind="css:{'alert-error':topUpHasError}, html: topUpMessage"></div>
            </div>
            <div class="data_grid grid-wrapper prod-grid" id="topups-grid"></div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="special-head">
            <h3>Special Requests</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingSpecialRequest(), click: doEditSpecialRequest">Edit</a>
            <a class="btn header-btn"
               data-bind="visible: isEditingSpecialRequest, click: doCancelSpecialRequest">Cancel</a>
            <a class="btn btn-success header-btn"
               data-bind="visible: isEditingSpecialRequest, click: doSaveSpecialRequest">Save Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: specialRequestMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':specialRequestHasError}, html: specialRequestMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Special Requests</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingSpecialRequest() && specialRequest() == ''">no special requests specified</span>
                <span
                  data-bind="visible: !isEditingSpecialRequest() && specialRequest() != '', text: specialRequest"></span>
                <textarea class="input-xxlarge" rows="4" maxlength="4000"
                          data-bind="visible: isEditingSpecialRequest, value: newSpecialRequest"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="summary-head">
            <h3>Transaction Summay</h3>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Sub-Total</div>
              <div class="offset1 span13">
                <span data-bind="text: subTotalPriceText"></span>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Service Charge (<span
                data-bind="text: serviceChargePercentText"></span>)
              </div>
              <div class="offset1 span13">
                <span data-bind="text: serviceChargePriceText"></span>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">GST (<span data-bind="text: gstPercentText"></span>)</div>
              <div class="offset1 span13">
                <span data-bind="text: gstPriceText"></span>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Additional Items</div>
              <div class="offset1 span13">
                <span data-bind="text: topUpPriceText"></span>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Total</div>
              <div class="offset1 span13">
                <span data-bind="text: totalPriceText"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="resend-head" data-bind="visible: type() == 0 && status() == 1">
            <h3>Resend Receipt</h3>
            <a class="btn btn-primary header-btn" data-bind="click: doResendReceipt">Resend Receipt</a>
          </div>
          <div class="product-header" id="transaction-head">
            <h3>Payment Details</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingTransSummary(), click: doEditTransSummary">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingTransSummary, click: doCancelTransSummary">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingTransSummary, click: doSaveTransSummary">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: transSummaryMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':transSummaryHasError}, html: transSummaryMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Reservation Type</div>
              <div class="offset1 span13">
                <span data-bind="text: reservationTypeText"></span>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Reservation Status</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTransSummary(), text: reservationStatusText"></span>
                <select class="input-large" id="reservation-status-field"
                        data-bind="visible: isEditingTransSummary, value: newStatus">
                  <option data-bind="visible: type()==1" value="2">Not Paid</option>
                  <option data-bind="visible: type()==0" value="1">Paid Inside System</option>
                  <option data-bind="visible: type()==1" value="3">Paid Outside System</option>
                  <option value="0">Cancelled</option>
                </select>
              </div>
            </div>
            <div data-bind="visible: showPaymentDetails">
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Received By</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingPaymentDetails() && (receivedBy() == '')">-</span>
                  <span
                    data-bind="visible: !isEditingPaymentDetails() && (receivedBy() != ''), text: receivedBy"></span>
                  <input class="input-xxlarge" type="text" maxlength="100"
                         data-bind="visible: isEditingPaymentDetails, value: newReceivedBy"/>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Receipt Number</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingPaymentDetails() && (receiptNum() == '')">-</span>
                  <span
                    data-bind="visible: !isEditingPaymentDetails() && (receiptNum() != ''), text: receiptNum"></span>
                  <input class="input-xxlarge" type="text" maxlength="50"
                         data-bind="visible: isEditingPaymentDetails, value: newReceiptNum"/>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Date of Payment</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingPaymentDetails() && (paymentDate() == '')">-</span>
                  <span
                    data-bind="visible: !isEditingPaymentDetails() && (paymentDate() != ''), text: paymentDate"></span>

                  <div data-bind="visible: isEditingPaymentDetails">
                    <input class="input-medium" type="text" id="payment-date-field" data-bind="value: newPaymentDate"/>
                  </div>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Mode of Payment</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingPaymentDetails() && (paymentMode() == 0)">-</span>
                  <span
                    data-bind="visible: !isEditingPaymentDetails() && (paymentMode() != 0), text: paymentModeText"></span>
                  <select class="input-large" id="payment-mode-field"
                          data-bind="visible: isEditingPaymentDetails, value: newPaymentMode">
                    <option value="1">Cash</option>
                    <option value="2">eNETS</option>
                    <option value="3">VISA</option>
                    <option value="4">MasterCard</option>
                    <option value="5">China UnionPay</option>
                    <option value="6">Other Card</option>
                  </select>
                </div>
              </div>
              <div class="row-fluid field-row" data-bind="visible: hasDiscount()">
                <div class="offset1 span5 field-label">Online Promotion</div>
                <div class="offset1 span13">
                  <span data-bind="text: discountName"></span>
                </div>
              </div>
              <div class="row-fluid field-row" data-bind="visible: hasDiscount()">
                <div class="offset1 span5 field-label">Discount Price</div>
                <div class="offset1 span13">
                  <span data-bind="text: discountPriceText"></span>
                </div>
              </div>
              <div class="row-fluid field-row">
                <div class="offset1 span5 field-label">Amount Paid</div>
                <div class="offset1 span13">
                  <span data-bind="visible: !isEditingPaymentDetails() && (paidAmount() == 0)">-</span>
                  <span
                    data-bind="visible: !isEditingPaymentDetails() && (paidAmount() != 0), text: paidAmountText"></span>
                  <input class="input-small" id="paid-amount-field" type="number" min="1"
                         data-bind="visible: isEditingPaymentDetails, value: newPaidAmount"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>