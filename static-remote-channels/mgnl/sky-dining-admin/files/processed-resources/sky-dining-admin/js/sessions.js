$(document).ready(function () {
    mflg.pageSize = 10;
    mflg.initHackKendoGridSortForStruts();

    mflg.initScheduleGrid();

    $('#add-button').click(function () {
        window.location.assign('session-view.html');
    });
    $('#remove-button').click(function () {
        mflg.doRemove();
    });
});

(function (mflg, $) {

    mflg.scheduleGrid = {};
    mflg.initScheduleGrid = function () {
        mflg.scheduleGrid = new mflg.ScheduleGrid();
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.ScheduleGrid = function () {
        var s = this;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/schedule/get-schedule';
        s.refreshUrl = function () {
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        initGrid();
        function initGrid() {
            s.kGrid = $('#sessions-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    sessionNumber: {
                                        type: 'string'
                                    },
                                    schedule: {
                                        type: 'string'
                                    },
                                    time: {
                                        type: 'string'
                                    },
                                    status: {
                                        type: 'string'
                                    },
                                    timestamp: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="remove-checkbox" id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'name',
                            title: 'Session Name'
                        },
                        {
                            field: 'sessionNumber',
                            title: 'Session Number',
                            width: 100
                        },
                        {
                            field: 'schedule',
                            title: 'Schedule',
                            width: 100,
                            sortable: false
                        },
                        {
                            field: 'time',
                            title: 'Time',
                            width: 100
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            sortable: false,
                            template: '<div style="text-align:center;"><a class="btn #if (status != "Active") {#btn-danger#} else {#btn-success#}#" onclick="mflg.toggleStatus(\'${uid}\')">${status}</a></div>'
                        },
                        {
                            title: 'Details',
                            width: 100,
                            sortable: false,
                            template: '<div style="text-align:center;"><a class="btn btn-primary" href="session-view.html?id=${id}">Details</a></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.doRemove = function () {
        var idList = '';
        $("input.remove-checkbox:checked('checked')").each(function () {
            idList += this.id;
            idList += ',';
        });

        if (idList == '') {
            mflg
                .ShowInfoDialog('Removing Sessions...',
                'No sessions selected.');
            return;
        }

        mflg
            .ShowConfirmDialog(
            'Removing Sessions...',
            'Are you sure you want to remove the selected sessions?',
            function () {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/schedule/remove-sessions',
                        data: {
                            idList: idList
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Removed Sessions',
                                    'The selected sessions have been successfully removed.');
                                mflg.scheduleGrid.kGrid.dataSource
                                    .page(1);
                            } else {
                                mflg.ShowInfoDialog('Error',
                                    mflg.ErrorSpan.replace(
                                        'THE_MSG',
                                        data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            });

    };

    mflg.toggleStatus = function (rowUid) {
        if (rowUid) {
            var modelData = mflg.scheduleGrid.kGrid.dataSource.getByUid(rowUid);
        } else {
            return;
        }

        mflg
            .ShowConfirmDialog(
            'Changing Status...',
            'Are you sure you want to change the status of this session?',
            function () {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/schedule/toggle-status',
                        data: {
                            id: modelData.id,
                            timestamp: modelData.timestamp
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Changed Status',
                                    'The status of the session was successfully changed.');
                                mflg.scheduleGrid.kGrid.dataSource
                                    .page(1);
                            } else {
                                mflg.ShowInfoDialog('Error',
                                    mflg.ErrorSpan.replace(
                                        'THE_MSG',
                                        data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            });

    };

})(window.mflg = window.mflg || {}, jQuery);