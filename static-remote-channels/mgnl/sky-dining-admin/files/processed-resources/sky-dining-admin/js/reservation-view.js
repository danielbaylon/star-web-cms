$(document).ready(
    function () {
        mflg.pageSize = 10;
        mflg.initHackKendoGridSortForStruts();

        mflg.reservationModel = new mflg.ReservationModel();
        mflg.getModelData();

        ko.applyBindings(mflg.reservationModel, document
            .getElementById("reservation-view"));

        mflg.mainCourseGrid = new mflg.MainCourseGrid(mflg.getParameterByName("id"), mflg.reservationModel.menuId());
        mflg.topUpGrid = new mflg.TopUpGrid(mflg.getParameterByName("id"), mflg.reservationModel.packageId());

        mflg.reservationModel.doComputePrices();

        $("body").scrollspy();
    });

(function (mflg, $) {
    mflg.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

    mflg.getModelData = function () {
        var modelId = mflg.getParameterByName("id");

        mflg.spinner.start();
        $
            .ajax({
                url: '/.sky-dining-admin/reservation/view-reservation',
                data: {
                    id: modelId,
                },
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.reservationModel.init(data.data);
                        //mflg.mainCourseGrid.refresh(mflg.reservationModel.id(), mflg.reservationModel.menuId());
                        //mflg.topUpGrid.refresh(mflg.reservationModel.id(), mflg.reservationModel.packageId());
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
    };

    Number.prototype.formatMoney = function (p, f, c, d, t) {
        var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "."
            : d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = parseInt(n = Math
                .abs(+n || 0).toFixed(c))
            + "", j = (j = i.length) > 3 ? j % 3 : 0, p = p == undefined ? "$"
            : p, f = f == undefined ? "" : f;
        return s + p + (j ? i.substr(0, j) + t : "")
            + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
            + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + f;
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.reservationModel = {};

    mflg.ReservationModel = function () {
        var s = this;

        s.timestamp = ko.observable('');

        s.dateField = $("#date-field")
            .kendoDatePicker(
            {
                format: mflg.DATE_FORMAT,
                change: function () {
                    s.packageField.value(null);
                    s.sessionField.value(null);
                    if (s.dateField.value()) {
                        s.packageField.dataSource.transport.options.read.url = "/.sky-dining-admin/reservation/get-select-packages?date="
                            + $("#date-field").val();
                    } else {
                        s.packageField.dataSource.transport.options.read.url = "/.sky-dining-admin/reservation/get-select-packages";
                    }
                    s.packageField.dataSource.read();
                }
            }).data("kendoDatePicker");

        s.paymentDateField = $("#payment-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        s.timeField = $("#time-field").kendoTimePicker({
            format: mflg.TIME_FORMAT
        }).data("kendoTimePicker");

        s.packageField = $("#package-field").width(530).kendoComboBox({
            filter: "contains",
            dataTextField: "name",
            dataValueField: "id",
            dataSource: {
                serverFiltering: false,
                transport: {
                    read: "/.sky-dining-admin/reservation/get-select-packages"
                },
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: 'id',
                        fields: {
                            name: {
                                type: 'string'
                            },
                            price: {
                                type: 'string'
                            },
                            priceInCents: {
                                type: 'number'
                            },
                            gst: {
                                type: 'number'
                            },
                            serviceCharge: {
                                type: 'number'
                            }
                        }
                    }
                }
            },
            change: function () {
                s.newPackageId(s.packageField.value());
                mflg.topUpGrid.refresh(s.id(), s.packageField.value());
            }
        }).data("kendoComboBox");

        s.sessionField = $("#session-field")
            .width(530)
            .kendoComboBox(
            {
                autoBind: false,
                cascadeFrom: "package-field",
                filter: "contains",
                dataTextField: "nameWithSessionNumber",
                dataValueField: "id",
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: "/.sky-dining-admin/reservation/get-select-sessions",
                        parameterMap: function (obj, action) {
                            var value = obj.filter.filters[0].value;
                            obj = {};
                            obj.packageId = value;
                            obj.date = $("#date-field").val();
                            return obj;
                        }
                    },
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: 'id',
                            fields: {
                                name: {
                                    type: 'string'
                                },
                                sessionNumber: {
                                    type: 'string'
                                },
                                nameWithSessionNumber: {
                                    type: 'string'
                                },
                                time: {
                                    type: 'string'
                                }
                            }
                        }
                    }
                },
                change: function () {
                    s.newSessionId(s.sessionField.value());

                    if (this.dataSource.get(this.value())) {
                        s.timeField.value(this.dataSource.get(this
                            .value()).time);
                    }
                }
            }).data("kendoComboBox");

        s.themeField = $("#theme-field").width(530).kendoComboBox({
            autoBind: false,
            cascadeFrom: "package-field",
            filter: "contains",
            dataTextField: "description",
            dataValueField: "id",
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: "/.sky-dining-admin/reservation/get-select-themes",
                    parameterMap: function (obj, action) {
                        var value = obj.filter.filters[0].value;
                        obj = {};
                        obj.packageId = value;
                        return obj;
                    }
                },
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: 'id',
                        fields: {
                            description: {
                                type: 'string'
                            }
                        }
                    }
                }
            },
            change: function () {
                s.newThemeId(s.themeField.value());
            }
        }).data("kendoComboBox");

        s.menuField = $("#menu-field")
            .width(530)
            .kendoComboBox(
            {
                autoBind: false,
                cascadeFrom: "package-field",
                filter: "contains",
                dataTextField: "name",
                dataValueField: "id",
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: "/.sky-dining-admin/reservation/get-select-menus",
                        parameterMap: function (obj, action) {
                            var value = obj.filter.filters[0].value;
                            obj = {};
                            obj.packageId = value;
                            return obj;
                        }
                    },
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: 'id',
                            fields: {
                                name: {
                                    type: 'string'
                                }
                            }
                        }
                    }
                },
                change: function (e) {
                    s.newMenuId(s.menuField.value());

                    if (this.dataSource.get(this.value())) {
                        s.newMenuPrice(this.dataSource.get(this
                            .value()).priceInCents);
                    } else {
                        s.newMenuPrice(0);
                    }

                    if (this.dataSource.get(this.value())) {
                        s.newMenuGst(this.dataSource.get(this
                            .value()).gst);
                    } else {
                        s.newMenuGst(0);
                    }

                    if (this.dataSource.get(this.value())) {
                        s.newMenuServiceCharge(this.dataSource
                            .get(this.value()).serviceCharge);
                    } else {
                        s.newMenuServiceCharge(0);
                    }

                    mflg.mainCourseGrid.refresh(s.id(), s.menuField
                        .value());
                    s.doComputePrices();
                }
            }).data("kendoComboBox");

        s.guestInfoEditFlag = ko.observable(false);
        s.guestInfoHasError = ko.observable(false);
        s.guestInfoMessage = ko.observable('');

        s.packageInfoEditFlag = ko.observable(false);
        s.packageInfoHasError = ko.observable(false);
        s.packageInfoMessage = ko.observable('');

        s.mainCourseEditFlag = ko.observable(false);
        s.mainCourseHasError = ko.observable(false);
        s.mainCourseMessage = ko.observable('');

        s.topUpEditFlag = ko.observable(false);
        s.topUpHasError = ko.observable(false);
        s.topUpMessage = ko.observable('');

        s.specialRequestEditFlag = ko.observable(false);
        s.specialRequestHasError = ko.observable(false);
        s.specialRequestMessage = ko.observable('');

        s.transSummaryEditFlag = ko.observable(false);
        s.transSummaryHasError = ko.observable(false);
        s.transSummaryMessage = ko.observable('');

        s.displayInfoEditFlag = ko.observable(false);
        s.displayInfoHasError = ko.observable(false);
        s.displayInfoMessage = ko.observable('');

        s.id = ko.observable(0);
        s.guestName = ko.observable('');
        s.contactNumber = ko.observable('');
        s.emailAddress = ko.observable('');
        s.date = ko.observable('');
        s.time = ko.observable('');
        s.pax = ko.observable(0);
        s.packageId = ko.observable(0);
        s.sessionId = ko.observable(0);
        s.themeId = ko.observable(0);
        s.menuId = ko.observable(0);
        s.specialRequest = ko.observable('');
        s.type = ko.observable(0);
        s.status = ko.observable(0);
        s.discountId = ko.observable(0);
        s.jewelCard = ko.observable('');
        s.jewelCardExpiry = ko.observable('');
        s.promoCode = ko.observable('');
        s.merchantId = ko.observable(0);
        s.receiptNum = ko.observable('');
        s.receivedBy = ko.observable('');
        s.paymentDate = ko.observable('');
        s.paymentMode = ko.observable(0);
        s.paidAmount = ko.observable('');

        s.dateFormatted = ko.observable('');
        s.date.subscribe(function (newValue) {
            s.dateFormatted(kendo.toString(kendo.parseDate(s.date(),
                'dd/MM/yyyy'), 'dd/MM/yyyy dddd'));
        });

        s.newGuestName = ko.observable('');
        s.newContactNumber = ko.observable('');
        s.newEmailAddress = ko.observable('');
        s.newDate = ko.observable('');
        s.newTime = ko.observable('');
        s.newPax = ko.observable(0);
        s.newPackageId = ko.observable(0);
        s.newSessionId = ko.observable(0);
        s.newThemeId = ko.observable(0);
        s.newMenuId = ko.observable(0);
        s.newSpecialRequest = ko.observable('');
        s.newType = ko.observable(0);
        s.newStatus = ko.observable(0);
        s.newDiscountId = ko.observable(0);
        s.newJewelCard = ko.observable('');
        s.newJewelCardExpiry = ko.observable('');
        s.newPromoCode = ko.observable('');
        s.newMerchantId = ko.observable(0);
        s.newReceiptNum = ko.observable('');
        s.newReceivedBy = ko.observable('');
        s.newPaymentDate = ko.observable('');
        s.newPaymentMode = ko.observable(0);
        s.newPaidAmount = ko.observable('');

        s.mainCourseIdList = ko.observable('');
        s.mainCourseQuantityList = ko.observable('');

        s.topUpIdList = ko.observable('');
        s.topUpQuantityList = ko.observable('');

        s.packageName = ko.observable('');
        s.sessionName = ko.observable('');
        s.themeDescription = ko.observable('');
        s.menuName = ko.observable('');

        s.menuPrice = ko.observable(0);
        s.newMenuPrice = ko.observable(0);
        s.menuGst = ko.observable(0);
        s.newMenuGst = ko.observable(0);
        s.menuServiceCharge = ko.observable(0);
        s.newMenuServiceCharge = ko.observable(0);
        s.subTotalPrice = ko.observable(0);
        s.gstPrice = ko.observable(0);
        s.serviceChargePrice = ko.observable(0);
        s.topUpPrice = ko.observable(0);
        s.totalPrice = ko.observable(0);

        s.priceType = ko.observable(0);
        s.discountPrice = ko.observable(0);
        s.originalPrice = ko.observable(0);
        s.discountName = ko.observable('');

        s.doResendReceipt = function () {
            if (s.isNew()) {
                console.log('NOPE');
                return;
            }

            mflg.spinner.start();
            $.ajax({
                url: '/.sky-dining-admin/reservation/resend-receipt',
                data: {
                    receiptNumber: s.receiptNum()
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.ShowInfoDialog('Resend Receipt',
                            'Successfully resent the receipt.');
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
        };

        s.init = function (data) {
            s.id(data.id);
            s.guestName(data.guestName);
            s.contactNumber(data.contactNumber);
            s.emailAddress(data.emailAddress);
            s.date(data.dateText);
            s.time(data.timeText);
            s.pax(data.pax);
            s.packageId(data.packageId);
            s.sessionId(data.sessionId);
            s.themeId(data.themeId);
            s.menuId(data.menuId);
            s.menuPrice(data.menuPrice);
            s.menuGst(data.menuGst);
            s.menuServiceCharge(data.menuServiceCharge);
            s.specialRequest(data.specialRequest);
            s.type(data.type);
            s.status(data.status);
            s.discountId(data.discountId);
            s.jewelCard(data.jewelCard);
            s.jewelCardExpiry(data.jewelCardExpiry);
            s.promoCode(data.promoCode);
            s.merchantId(data.merchantId);
            s.receiptNum(data.receiptNum ? data.receiptNum : '');
            s.receivedBy(data.receivedBy ? data.receivedBy : '');
            s.paymentDate(data.paymentDateText ? data.paymentDateText : '');
            s.paymentMode(data.paymentMode ? data.paymentMode : 0);
            s.paidAmount(data.paidAmount ? data.paidAmount : 0);

            s.newGuestName(data.guestName);
            s.newContactNumber(data.contactNumber);
            s.newEmailAddress(data.emailAddress);
            s.newDate(data.dateText);
            s.newTime(data.timeText);
            s.newPax(data.pax);
            s.newPackageId(data.packageId);
            s.newSessionId(data.sessionId);
            s.newThemeId(data.themeId);
            s.newMenuId(data.menuId);
            s.newMenuPrice(data.menuPrice);
            s.newMenuGst(data.menuGst);
            s.newMenuServiceCharge(data.menuServiceCharge);
            s.newSpecialRequest(data.specialRequest);
            s.newType(data.type);
            s.newStatus(data.status);
            s.newDiscountId(data.discountId);
            s.newJewelCard(data.jewelCard);
            s.newJewelCardExpiry(data.jewelCardExpiry);
            s.newPromoCode(data.promoCode);
            s.newMerchantId(data.merchantId);
            s.newReceiptNum(data.receiptNum);
            s.newReceivedBy(data.receivedBy);
            s.newPaymentDate(data.paymentDate);
            s.newPaymentMode(data.paymentMode);
            s.newPaidAmount(data.paidAmount);

            s.packageName(data.packageName);
            s.sessionName(data.sessionName);
            s.themeDescription(data.themeDescription);
            s.menuName(data.menuName);

            s.priceType(data.priceType);
            s.discountPrice(data.discountPrice);
            s.originalPrice(data.originalPrice);
            s.discountName(data.discountName);

            s.dateField.value(s.date());
            s.paymentDateField.value(s.paymentDate());
            s.timeField.value(s.time());

            s.packageField.value(s.packageId());
            s.sessionField.value(s.sessionId());
            s.themeField.value(s.themeId());
            s.menuField.value(s.menuId());

            s.timestamp(data.timestamp);
        };

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.headerText = ko.computed(function () {
            if (s.id() < 1) {
                return "New Reservation";
            } else if (s.guestInfoEditFlag() || s.packageInfoEditFlag()
                || s.mainCourseEditFlag() || s.topUpEditFlag()
                || s.specialRequestEditFlag() || s.transSummaryEditFlag()) {
                return "Edit Reservation";
            }

            else {
                return "View Reservation";
            }
        });

        s.menuPriceText = ko.computed(function () {
            return (s.menuPrice() / 100).formatMoney("$", "++");
        });

        s.newMenuPriceText = ko.computed(function () {
            return (s.newMenuPrice() / 100).formatMoney("$", "++");
        });

        s.gstPercentText = ko.computed(function () {
            return (s.newMenuGst() ? s.newMenuGst() : 0) + '%';
        });

        s.serviceChargePercentText = ko.computed(function () {
            return (s.newMenuServiceCharge() ? s.newMenuServiceCharge() : 0)
                + '%';
        });

        s.subTotalPriceText = ko.computed(function () {
            return (s.subTotalPrice() / 100).formatMoney("$");
        });

        s.gstPriceText = ko.computed(function () {
            return (s.gstPrice() / 100).formatMoney("$");
        });

        s.serviceChargePriceText = ko.computed(function () {
            return (s.serviceChargePrice() / 100).formatMoney("$");
        });

        s.topUpPriceText = ko.computed(function () {
            return (s.topUpPrice() / 100).formatMoney("$");
        });

        s.totalPriceText = ko.computed(function () {
            return (s.totalPrice() / 100).formatMoney("$");
        });

        s.hasDiscount = ko.computed(function () {
            return s.discountId() > 0;
        });

        s.discountPriceText = ko
            .computed(function () {
                var text = '';
                if (s.discountId() > 0) {
                    switch (s.priceType()) {
                        case 1:
                            text = (s.originalPrice()
                                * (100 - s.discountPrice()) / 100 / 100)
                                    .formatMoney("$")
                                + "++ per pax ("
                                + s.discountPrice()
                                + "% up from "
                                + (s.originalPrice() / 100)
                                    .formatMoney("$") + ")";
                            break;
                        case 2:
                            ((s.originalPrice() - s.discountPrice()) / 100)
                                .formatMoney("$")
                            + "++ per pax ("
                            + (s.discountPrice() / 100)
                                .formatMoney("$")
                            + "up from "
                            + (s.originalPrice() / 100)
                                .formatMoney("$") + ")";
                            break;
                        case 3:
                            (s.discountPrice() / 100).formatMoney("$")
                            + "++ per pax (up from "
                            + (s.originalPrice() / 100)
                                .formatMoney("$") + ")";
                            break;
                        default:
                            break;
                    }
                }
                return text;
            });

        s.reservationTypeText = ko.computed(function () {
            switch (s.type()) {
                case 1:
                    return "Admin";
                default:
                    return "Online";
            }
        });

        s.reservationStatusText = ko.computed(function () {
            switch (Number(s.status())) {
                case 1:
                    return "Paid Inside System";
                case 2:
                    return "Not Paid";
                case 3:
                    return "Paid Outside System";
                default:
                    return "Cancelled";
            }
        });

        s.doComputePrices = function () {
            var pax = Number($("#pax-field").val());
            var subTotalPrice = s.newMenuPrice() * (pax ? pax : s.newPax());
            var serviceChargePrice = subTotalPrice
                * (s.newMenuServiceCharge() / 100);
            var gstPrice = (subTotalPrice + serviceChargePrice)
                * (s.newMenuGst() / 100);
            var topUpPrice = 0;
            $("input.topup-quantity").each(function () {
                var quantity = Number(this.value);
                var price = Number(this.getAttribute("price"));
                topUpPrice += quantity * price;
            });
            var totalPrice = (subTotalPrice ? subTotalPrice : 0)
                + (gstPrice ? gstPrice : 0)
                + (serviceChargePrice ? serviceChargePrice : 0)
                + (topUpPrice ? topUpPrice : 0);

            s.subTotalPrice(subTotalPrice);
            s.gstPrice(gstPrice);
            s.serviceChargePrice(serviceChargePrice);
            s.topUpPrice(topUpPrice);
            s.totalPrice(totalPrice);
        };

        s.doSaveNewReservation = function () {
            var isGuestInfoValid = s.doValidateGuestInfo();
            var isPackageInfoValid = s.doValidatePackageInfo();
            var isMainCourseValid = s.doValidateMainCourse();
            var isTopUpValid = s.doValidateTopUp();
            var isSpecialRequestValid = s.doValidateSpecialRequest();
            var isTransSummaryValid = s.doValidateTransSummary();

            if (isGuestInfoValid && isPackageInfoValid && isMainCourseValid
                && isTopUpValid && isSpecialRequestValid
                && isTransSummaryValid) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newGuestName: s.newGuestName(),
                            newContactNumber: s.newContactNumber(),
                            newEmailAddress: s.newEmailAddress(),
                            newDate: s.newDate(),
                            newTime: s.newTime(),
                            newPax: s.newPax(),
                            newPackageId: s.newPackageId(),
                            newSessionId: s.newSessionId(),
                            newThemeId: s.newThemeId() ? s.newThemeId()
                                : 0,
                            newMenuId: s.newMenuId(),
                            newSpecialRequest: s.newSpecialRequest(),
                            newType: 1,
                            newStatus: 2,
                            newTopUpIds: s.topUpIdList(),
                            newTopUpQuantities: s.topUpQuantityList(),
                            newMainCourseIds: s.mainCourseIdList(),
                            newMainCourseQuantities: s
                                .mainCourseQuantityList(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Reservation',
                                    'The reservation has been saved successfully. Refreshing the page.',
                                    function () {
                                        window.location
                                            .replace('reservation-view.html?id='
                                            + data.data.newId);
                                    });
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doValidateGuestInfo = function () {
            var messsage = '';

            if (!nvx.isLengthWithin(s.newGuestName(), 1, 300, true)) {
                messsage += "<li>Guest name is required and must not exceed 300 characters.</li>";
            }

            if (nvx.isEmpty(s.newContactNumber())
                || !nvx.isLengthWithin(s.newContactNumber(), 1, 100, true)) {
                messsage += "<li>Contact number is required and must not exceed 100 characters.</li>";
            }

            if (nvx.isEmpty(s.newEmailAddress())
                || !nvx.isLengthWithin(s.newEmailAddress(), 1, 200, true)) {
                messsage += "<li>Email address is required and must not exceed 200 characters.</li>";
            }

            if (messsage != '') {
                s.guestInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.guestInfoHasError(true);
                return false;
            }
            s.guestInfoMessage('');
            s.guestInfoHasError(false);
            return true;
        };

        s.doSaveGuestInfo = function () {
            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidateGuestInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newGuestName: s.newGuestName(),
                            newContactNumber: s.newContactNumber(),
                            newEmailAddress: s.newEmailAddress(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Guest Information',
                                    'The guest information has been saved successfully.');
                                s.guestName(s.newGuestName());
                                s.contactNumber(s.newContactNumber());
                                s.emailAddress(s.newEmailAddress());

                                s.timestamp(data.timestamp);
                                s.doEditGuestInfo();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelGuestInfo = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditGuestInfo();
            }
        };

        s.doEditGuestInfo = function () {
            if (s.guestInfoEditFlag()) {
                s.guestInfoMessage('');
                s.guestInfoHasError(false);

                s.guestInfoEditFlag(false);
            } else {
                s.newGuestName(s.guestName());
                s.newContactNumber(s.contactNumber());
                s.newEmailAddress(s.emailAddress());

                s.guestInfoEditFlag(true);
            }
        };

        s.isEditingGuestInfo = ko.computed(function () {
            return s.guestInfoEditFlag() || s.isNew();
        });

        s.doValidatePackageInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.dateField.value())) {
                messsage += "<li>Date is required and must be a valid date.</li>";
            } else {
                s.newDate($("#date-field").val());
            }

            if (nvx.isEmpty(s.timeField.value())) {
                messsage += "<li>Time is required and must be a valid time.</li>";
            } else {
                s.newTime($("#time-field").val());
            }

            if (nvx.isEmpty(s.newPackageId())
                || !nvx.isNumber(s.newPackageId())
                || !nvx.isInteger(Number(s.newPackageId()))
                || (((Number(s.newPackageId()) < 1)))) {
                messsage += "<li>Package is required.</li>";
            }

            if (nvx.isEmpty(s.newSessionId())
                || !nvx.isNumber(s.newSessionId())
                || !nvx.isInteger(Number(s.newSessionId()))
                || (((Number(s.newSessionId()) < 1)))) {
                messsage += "<li>Session is required.</li>";
            }

            if (nvx.isEmpty(s.newPax()) || !nvx.isNumber(s.newPax())
                || !nvx.isInteger(Number(s.newPax()))
                || (((Number(s.newPax()) < 1)))) {
                messsage += "<li>Number of pax is required and must be a number.</li>";
            }

            if (nvx.isEmpty(s.newMenuId()) || !nvx.isNumber(s.newMenuId())
                || !nvx.isInteger(Number(s.newMenuId()))
                || (((Number(s.newMenuId()) < 1)))) {
                messsage += "<li>Menu is required.</li>";
            }

            if (messsage != '') {
                s
                    .packageInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.packageInfoHasError(true);
                return false;
            }
            s.packageInfoMessage('');
            s.packageInfoHasError(false);
            return true;
        };

        s.doSavePackageInfo = function () {
            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidatePackageInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newDate: s.newDate(),
                            newTime: s.newTime(),
                            newPax: s.newPax(),
                            newPackageId: s.newPackageId(),
                            newSessionId: s.newSessionId(),
                            newThemeId: s.newThemeId() ? s.newThemeId()
                                : 0,
                            newMenuId: s.newMenuId(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Package Information',
                                    'The package information has been saved successfully.');
                                s.date(s.newDate());
                                s.time(s.newTime());
                                s.pax(s.newPax());
                                s.packageId(s.newPackageId());
                                s.sessionId(s.newSessionId());
                                s.themeId(s.newThemeId());
                                s.menuId(s.newMenuId());
                                s.menuPrice(s.newMenuPrice());
                                s.menuGst(s.newMenuGst());
                                s.menuServiceCharge(s
                                    .newMenuServiceCharge());

                                s.packageName(s.packageField.text());
                                s.sessionName(s.sessionField.text());
                                s.themeDescription(s.themeField.text());
                                s.menuName(s.menuField.text());

                                s.timestamp(data.timestamp);
                                s.doEditPackageInfo();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelPackageInfo = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditPackageInfo();
            }
        };

        s.doEditPackageInfo = function () {
            if (s.packageInfoEditFlag()) {
                s.packageInfoMessage('');
                s.packageInfoHasError(false);

                s.newPax(s.pax());

                s.newPackageId(s.packageId());
                s.newSessionId(s.sessionId());
                s.newThemeId(s.themeId());
                s.newMenuId(s.menuId());
                s.newMenuPrice(s.menuPrice());
                s.newMenuGst(s.menuGst());
                s.newMenuServiceCharge(s.menuServiceCharge());

                mflg.mainCourseGrid.refresh(s.id(), s.newMenuId());
                mflg.topUpGrid.refresh(s.id(), s.newPackageId());

                s.packageInfoEditFlag(false);
            } else {
                s.newDate(s.date());
                s.newTime(s.time());
                s.newPax(s.pax());

                s.newPackageId(s.packageId());
                s.newSessionId(s.sessionId());
                s.newThemeId(s.themeId());
                s.newMenuId(s.menuId());
                s.newMenuPrice(s.menuPrice());
                s.newMenuGst(s.menuGst());
                s.newMenuServiceCharge(s.menuServiceCharge());

                s.dateField.value(s.date());
                s.timeField.value(s.time());

                s.packageField.value(s.packageId());
                s.sessionField.value(s.sessionId());
                s.themeField.value(s.themeId());
                s.menuField.value(s.menuId());

                s.packageInfoEditFlag(true);
            }
        };

        s.isEditingPackageInfo = ko.computed(function () {
            return s.packageInfoEditFlag() || s.isNew();
        });

        s.doValidateMainCourse = function () {
            var messsage = '';

            var invalidQuantity = false;
            var idList = '';
            var quantityList = '';
            $("input.main-course-quantity").each(
                function () {
                    var quantity = this.value;
                    if (nvx.isEmpty(quantity) || !nvx.isNumber(quantity)
                        || !nvx.isInteger(Number(quantity))
                        || (Number(quantity) < 0)) {
                        invalidQuantity = true;
                    } else {
                        quantityList += quantity;
                        quantityList += ',';
                        idList += this.getAttribute("main-course-id");
                        idList += ',';
                    }
                });
            if (invalidQuantity) {
                messsage += "<li>Quantity is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.mainCourseMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.mainCourseHasError(true);
                return false;
            }
            s.mainCourseMessage('');
            s.mainCourseHasError(false);
            s.mainCourseIdList(idList);
            s.mainCourseQuantityList(quantityList);
            return true;
        };

        s.doSaveMainCourse = function () {
            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidateMainCourse()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newMainCourseIds: s.mainCourseIdList(),
                            newMainCourseQuantities: s
                                .mainCourseQuantityList(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Main Courses',
                                    'The main courses have been saved successfully.');

                                s.timestamp(data.timestamp);
                                s.doEditMainCourse();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelMainCourse = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditMainCourse();
            }
        };

        s.doEditMainCourse = function () {
            if (s.mainCourseEditFlag()) {
                s.mainCourseMessage('');
                s.mainCourseHasError(false);

                s.mainCourseEditFlag(false);

                mflg.mainCourseGrid.disableEdit();
                mflg.mainCourseGrid.refresh(s.id(), s.newMenuId());
            } else {
                s.mainCourseEditFlag(true);

                mflg.mainCourseGrid.enableEdit();
                mflg.mainCourseGrid.refresh(s.id(), s.newMenuId());
            }
        };

        s.isEditingMainCourse = ko.computed(function () {
            return s.mainCourseEditFlag() || s.isNew();
        });

        s.doValidateTopUp = function () {
            var messsage = '';

            var invalidQuantity = false;
            var idList = '';
            var quantityList = '';
            $("input.topup-quantity").each(
                function () {
                    var quantity = this.value;
                    if (nvx.isEmpty(quantity) || !nvx.isNumber(quantity)
                        || !nvx.isInteger(Number(quantity))
                        || (Number(quantity) < 0)) {
                        invalidQuantity = true;
                    } else {
                        quantityList += quantity;
                        quantityList += ',';
                        idList += this.getAttribute("topup-id");
                        idList += ',';
                    }
                });
            if (invalidQuantity) {
                messsage += "<li>Quantity is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.topUpMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.topUpHasError(true);
                return false;
            }
            s.topUpMessage('');
            s.topUpHasError(false);
            s.topUpIdList(idList);
            s.topUpQuantityList(quantityList);
            return true;
        };

        s.doSaveTopUp = function () {
            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidateTopUp()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newTopUpIds: s.topUpIdList(),
                            newTopUpQuantities: s.topUpQuantityList(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog('Saved Top Ups',
                                    'The top ups have been saved successfully.');

                                s.timestamp(data.timestamp);
                                s.doEditTopUp();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelTopUp = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditTopUp();
            }
        };

        s.doEditTopUp = function () {
            if (s.topUpEditFlag()) {
                s.topUpMessage('');
                s.topUpHasError(false);

                s.topUpEditFlag(false);

                mflg.topUpGrid.disableEdit();
                mflg.topUpGrid.refresh(s.id(), s.newPackageId());
            } else {
                s.topUpEditFlag(true);

                mflg.topUpGrid.enableEdit();
                mflg.topUpGrid.refresh(s.id(), s.newPackageId());
            }
        };

        s.isEditingTopUp = ko.computed(function () {
            return s.topUpEditFlag() || s.isNew();
        });

        s.doValidateSpecialRequest = function () {
            var messsage = '';

            if (!nvx.isEmpty(s.newSpecialRequest())
                && !nvx
                    .isLengthWithin(s.newSpecialRequest(), 0, 4000,
                    true)) {
                messsage += "<li>Special requests must not exceed 4000 characters.</li>";
            }

            if (messsage != '') {
                s
                    .specialRequestMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.specialRequestHasError(true);
                return false;
            }
            s.specialRequestMessage('');
            s.specialRequestHasError(false);
            return true;
        };

        s.doSaveSpecialRequest = function () {
            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidateSpecialRequest()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: {
                            id: s.id(),
                            newSpecialRequest: s.newSpecialRequest(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Special Requests',
                                    'The special requests have been saved successfully.');
                                s.specialRequest(s.newSpecialRequest());

                                s.timestamp(data.timestamp);
                                s.doEditSpecialRequest();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelSpecialRequest = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditSpecialRequest();
            }
        };

        s.doEditSpecialRequest = function () {
            if (s.specialRequestEditFlag()) {
                s.specialRequestMessage('');
                s.specialRequestHasError(false);

                s.specialRequestEditFlag(false);
            } else {
                s.newSpecialRequest(s.specialRequest());

                s.specialRequestEditFlag(true);
            }
        };

        s.isEditingSpecialRequest = ko.computed(function () {
            return s.specialRequestEditFlag() || s.isNew();
        });

        s.doValidateTransSummary = function () {
            var messsage = '';

            if (!((s.type() == 0) || (s.newStatus() == 0) || (s.newStatus() == 2))) {

                if (!nvx.isEmpty(s.newReceivedBy())
                    && !nvx.isLengthWithin(s.newReceivedBy(), 1, 100, true)) {
                    messsage += "<li>Received by must not exceed 100 characters.</li>";
                }

                if (!nvx.isEmpty(s.newReceiptNum())
                    && !nvx.isLengthWithin(s.newReceiptNum(), 1, 50, true)) {
                    messsage += "<li>Receipt number must not exceed 50 characters.</li>";
                }

                if (nvx.isEmpty(s.paymentDateField.value())) {
                    messsage += "<li>Date of payment is required and must be a valid date.</li>";
                } else {
                    s.newPaymentDate($("#payment-date-field").val());
                }

                if (nvx.isEmpty(s.newPaidAmount())
                    || !nvx.isNumber(s.newPaidAmount())
                    || (((Number(s.newPaidAmount()) < 1)))) {
                    messsage += "<li>Amount paid is required and must be a number.</li>";
                }
            }

            if (messsage != '') {
                s
                    .transSummaryMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.transSummaryHasError(true);
                return false;
            }
            s.transSummaryMessage('');
            s.transSummaryHasError(false);
            return true;
        };

        s.doSaveTransSummary = function () {
            var data;
            var saveOnlyStatus = (s.type() == 0) || (s.newStatus() == 0)
                || (s.newStatus() == 2);

            if (saveOnlyStatus) {
                data = {
                    id: s.id(),
                    newStatus: s.newStatus,
                    timestamp: s.timestamp()
                };
            } else {
                data = {
                    id: s.id(),
                    newStatus: s.newStatus(),
                    newReceiptNum: s.newReceiptNum(),
                    newReceivedBy: s.newReceivedBy(),
                    newPaymentDate: s.newPaymentDate(),
                    newPaymentMode: s.newPaymentMode(),
                    newPaidAmount: s.newPaidAmount(),
                    timestamp: s.timestamp()
                };
            }

            if (s.isNew()) {
                s.doSaveNewReservation();
            } else if (s.doValidateTransSummary()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/reservation/save-reservation',
                        data: data,
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Reservation Status',
                                    'The reservation status has been saved successfully.');
                                s.status(s.newStatus());
                                if (!saveOnlyStatus) {
                                    s.receiptNum(s.newReceiptNum());
                                    s.receivedBy(s.newReceivedBy());
                                    s.paymentDate(s.newPaymentDate());
                                    s.paymentMode(s.newPaymentMode());
                                    s.paidAmount(s.newPaidAmount());
                                }
                                s.timestamp(data.timestamp);
                                s.doEditTransSummary();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelTransSummary = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/reservations');
            } else {
                s.doEditTransSummary();
            }
        };

        s.doEditTransSummary = function () {
            if (s.transSummaryEditFlag()) {
                s.newStatus(s.status());

                s.transSummaryMessage('');
                s.transSummaryHasError(false);

                s.transSummaryEditFlag(false);
            } else {
                s.newStatus(s.status());
                s.newReceiptNum(s.receiptNum());
                s.newReceivedBy(s.receivedBy());
                s.newPaymentDate(s.paymentDate());
                s.newPaymentMode(s.paymentMode());
                s.newPaidAmount(s.paidAmount());

                s.paymentDateField.value(s.paymentDate());

                s.transSummaryEditFlag(true);
            }
        };

        s.isEditingTransSummary = ko.computed(function () {
            return s.transSummaryEditFlag() || s.isNew();
        });

        s.canEditPaymentDetails = ko.computed(function () {
            return (s.type() != 0) && (s.newStatus() != 0)
                && (s.newStatus() != 2);
        });

        s.isEditingPaymentDetails = ko.computed(function () {
            return s.isEditingTransSummary() && s.canEditPaymentDetails();
        });

        s.showPaymentDetails = ko.computed(function () {
            return s.newStatus() != 2;
        });

        s.paymentModeText = ko.computed(function () {
            switch (Number(s.paymentMode())) {
                case 1:
                    return "Cash";
                case 2:
                    return "eNETS";
                case 3:
                    return "VISA";
                case 4:
                    return "MasterCard";
                case 5:
                    return "China UnionPay";
                case 6:
                    return "Other Card";
                default:
                    return "";
            }
        });

        s.paidAmountText = ko.computed(function () {
            return Number(s.paidAmount()).formatMoney("$");
        });

    }

})(window.mflg = window.mflg || {}, jQuery);