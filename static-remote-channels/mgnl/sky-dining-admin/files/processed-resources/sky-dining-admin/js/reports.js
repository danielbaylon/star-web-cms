$(document).ready(function () {
    mflg.pageSize = 10;
    mflg.initHackKendoGridSortForStruts();

    $('#start-date-filter').kendoDatePicker({
        format: mflg.DATE_FORMAT,
        change: function () {
        }
    });
    $('#end-date-filter').kendoDatePicker({
        format: mflg.DATE_FORMAT,
        change: function () {
        }
    });
    $("#report-type-filter").kendoDropDownList();

    $('#generate-button').click(function () {
        mflg.generate();
    });

    $('#reset-button').click(function () {
        $('#start-date-filter').val('');
        $('#end-date-filter').val('');
        $('#report-type-filter').data("kendoDropDownList").value('');
    });
});

(function (mflg, $) {

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.validate = function () {
        return true;
    };

    mflg.generate = function () {
        if (mflg.validate()) {
            var url;
            var exportType = $('#report-type-filter').data('kendoDropDownList')
                .value();
            switch (exportType) {
                case 'DailyBooking':
                    url = '/.sky-dining-admin/report/daily-booking?';
                    break;
                case 'DailyOrder':
                    url = '/.sky-dining-admin/report/daily-order?';
                    break;
                case 'Revenue':
                    url = '/.sky-dining-admin/report/revenue?';
                    break;
                default:
                    break;
            }
            url += "startDate=" + $('#start-date-filter').val();
            url += "&endDate=" + $('#end-date-filter').val();
            window.open(url);
        }
    };

})(window.mflg = window.mflg || {}, jQuery);