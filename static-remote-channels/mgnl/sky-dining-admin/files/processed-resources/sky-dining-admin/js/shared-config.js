//$(document).ready(function() {
//    //tnc
//    $("#tncTitle").kendoAutoComplete();
//    $("#tncType").kendoDropDownList();
//    $("#tncRelateCat").kendoDropDownList();
//    mflg.initHackKendoGridSortForStruts();
//    initTNCGrid(true);
//    initTNCDialog();
//    $("#tncEditor").change(clearCharacters);
//
//    tinymce.init({
//        selector: "#tncEditor",
//        plugins: [
//            "advlist lists link charmap anchor",
//            "visualblocks code",
//            "table contextmenu paste"
//        ],
//        menubar: false,
//        statusbar: false,
//        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
//    });
//
//    $("#addTNC").click(showAddTNCDialog);
//    $("#removeTNC").click(deleteTnc);
//
//    //closure
//    initClosureGrid(true);
//    initClosureDialog();
//    $('#cloFrom').kendoDatePicker({
//        format: mflg.DATE_FORMAT
//    });
//    $('#cloTo').kendoDatePicker({
//        format: mflg.DATE_FORMAT
//    });
//    $("#addClosure").click(showAddClosureDialog);
//    $("#removeClosure").click(deleteClosure);
//    $("input[name=closureProducts]").click(checkClosureProduct);
//
//    //merchant
//    initMerchantGrid(true);
//    initMerchantDialog();
//    $("#merName").kendoAutoComplete();
//    $("#merID").kendoAutoComplete();
//    $("#merBank").kendoDropDownList();
//    $("#addMerchant").click(showAddMerchantDialog);
//    $("#removeMerchante").click(deleteMerchant);
//
//    //Faber Licence logic is in another document ready block below.
//});
//tnc
function initTNCGrid(resetPage) {
    var grid = $("#tncMainGrid").data('kendoGrid');
    if (!grid) {
        var url = '/admin/products/shared-config/getTNCGrid';

        $('#tncMainGrid').kendoGrid({
            dataSource: {
                transport: {read: {url: url, cache: false}},
                schema: {
                    data: 'tncRecords',
                    total: 'tncRecordTotal',
                    model: {
                        fields: {
                            id: {type: 'number'},
                            title: {type: 'String'},
                            type: {type: 'String'},
                            proCata: {type: 'string'},
                            tncContent: {type: 'string'},
                            typeCode: {type: 'string'},
                            relId: {type: 'string'}
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true,
                serverSorting: true,
                sort: {field: "title", dir: "asc"}
            },
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    template: '<div><span id="content#=id#" style="display:none;">#= tncContent #</span>' +
                    '<span id="title#=id#" style="display:none;">#=title #</span>' +
                    '<span id="proCata#=id#" style="display:none;">#=proCata #</span>' +
                    '<a href="javascript:void(0)" onclick="showEditTNCDialog(#=id#, \'#:typeCode#\', \'#:relId#\')" />#=title#</div>',
                    width: 250
                },
                {field: 'type', title: 'Type', width: 200},
                {
                    field: 'proCata',
                    title: 'Related Product/Category',
                    width: 250,
                    sortable: false,
                    template: '<div>#= proCata #</div>'
                },
                {
                    title: 'Remove',
                    template: '<div style="text-align:center;"><input type="checkbox" onclick="checkDeleteTNC(this, #=id#)" /></div>',
                    width: 60
                }
            ],
            pageable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            dataBound: clearSelectedTNCDeleteIds
        });
    }
    else {
        var ds = grid.dataSource;
        if (resetPage) {
            ds.page(1);
        }
        else {
            ds.read();
        }
    }
}

function initTNCDialog() {
    if (!$("#tncDialogContainer").data("kendoWindow")) {
        $("#tncDialogContainer").kendoWindow({
            width: "1000px",
            visible: false,
            modal: true,
            actions: []
        }).data("kendoWindow");
        $("#tncCancelBtn").click(function () {
            $("#tncDialogContainer").data("kendoWindow").close();
        });
        $("#tncDialogContainer").parent().find(".k-window-action").hide();
        $("#tncDialogContainer_wnd_title").css('height', '25px');
    }
}

function changeTncType() {
    var value = $("#tncType").data('kendoDropDownList').value();
    switch (value) {
        case 'Category':
            $('#tncCatRow').show();
            $('#tncProdRow').hide();
            break;
        case 'Product':
            $('#tncCatRow').hide();
            $('#tncProdRow').show();
            break;
        default:
            $('#tncCatRow').hide();
            $('#tncProdRow').hide();
    }
}

function clearCharacters() {
    var str = $("#tncEditor").html();
    str = mflg.ClearWordCharacters(str);
    $("#tncEditor").html(str);
}

function showEditTNCDialog(id, typeCode, relId) {
    var window = $("#tncDialogContainer").data("kendoWindow");
    window.open().center().title("Edit Terms & Conditions");
    $("#tncSaveBtn").unbind('click');


    $("#tncTitle").data('kendoAutoComplete').value($("#title" + id).text());
    $("#tncType").data('kendoDropDownList').value(typeCode);

    switch (typeCode) {
        case 'Category':
            $('#tncCatRow').show();
            $('#tncProdRow').hide();
            $("#tncRelateCat").data('kendoDropDownList').value(relId);
            break;
        case 'Product':
            $('#tncCatRow').hide();
            $('#tncProdRow').show();
            $("#tncRelateProduct").html($("#proCata" + id).text());
            break;
        default:
            $('#tncCatRow').hide();
            $('#tncProdRow').hide();
    }

    var content = $("#content" + id).html();

    $("#tncEditor").tinymce().setContent(content);
    $("#tncEditor").val(content);

    $("#tncSaveBtn").click(function () {
        if (!validateTncSave()) {
            return;
        }

        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            tncSave(id);
        });
    });
}

function showAddTNCDialog() {
    var window = $("#tncDialogContainer").data("kendoWindow");
    window.open().center().title("Add Terms & Conditions");
    $("#tncSaveBtn").unbind('click');

    $("#tncRelateProduct").html("");
    $("#tncTitle").data('kendoAutoComplete').value("");
    $("#tncType").data('kendoDropDownList').value("MainTNC");

    $("#tncEditor").tinymce().setContent('');
    $("#tncEditor").val('');

    $("#tncSaveBtn").click(function () {
        if (!validateTncSave()) {
            return;
        }

        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            tncSave(0);
        });
    });
}

function tncSave(id) {
    var url = '/admin/products/shared-config/updateTNC?';
    url += 'tncId=' + id;
    var tncTitle = $("#tncTitle").data('kendoAutoComplete').value().trim();
    var tncType = $("#tncType").data('kendoDropDownList').value();
    var tncCat = $("#tncRelateCat").data('kendoDropDownList').value();
    url += '&tncType=' + tncType;
    var tncContent = $("#tncEditor").val();

    mflg.spinner.start();
    $.ajax({
        url: url,
        type: 'POST', cache: false, dataType: 'json',
        data: {
            tncTitle: tncTitle,
            tncContent: tncContent,
            tncCat: tncCat
        },
        success: function (data) {
            if (data.success) {
                if (id > 0) {
                    mflg.ShowInfoDialog('Success', 'T&C has been updated.');
                }
                else {
                    mflg.ShowInfoDialog('Success', 'T&C has been added.');
                }
                initTNCGrid(false);
                $("#tncDialogContainer").data("kendoWindow").close();
            } else {
                mflg.ShowInfoDialog('Error', data.message);
            }
        },
        error: function () {
            mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
        },
        complete: function () {
            mflg.spinner.stop();
        }
    });
}

function validateTncSave() {
    $("#tncEditor").val($("#tncEditor").tinymce().getContent());
    var message = '';
    var result = true;
    if ($("#tncTitle").data('kendoAutoComplete').value().trim() == "") {
        message += 'T&C title is required.<br />';
        result = false;
    }
//	for(var i = 1;i<=tncContentLines;i++)
//	{
//		if($("#tncContent" + i).val().trim() == '')
//		{
//			message += 'T&C content line ' + i + ' is required.<br />';
//			result = false;
//		}
//	}
    if (message != '') {
        mflg.ShowInfoDialog('Error', message);
    }

    return result;
}

var selectedTNCDeleteIds = [];

function checkDeleteTNC(check, id) {
    if ($(check).attr("checked")) {
        selectedTNCDeleteIds.push(id);
    }
    else {
        for (var i = selectedTNCDeleteIds.length; i >= 0; i--) {
            if (selectedTNCDeleteIds[i] == id) {
                selectedTNCDeleteIds.splice(i, 1);
                break;
            }
        }
    }
}

function clearSelectedTNCDeleteIds() {
    selectedTNCDeleteIds = [];
}

function deleteTnc() {
    if (selectedTNCDeleteIds.length == 0) {
        mflg.ShowInfoDialog('Error', 'Please select T&C(s) to delete.');
        return;
    }
    var url = '/admin/products/shared-config/deleteTNC?deleteTncIds=' + selectedTNCDeleteIds;
    mflg.ShowConfirmDialog("Warning", "Are you sure you want to remove the selected T&C(s)?");
    $("#confirm_yes_btn").click(function () {
        mflg.spinner.start();
        $.ajax({
            url: url,
            type: 'POST', cache: false, dataType: 'json',
            success: function (data) {
                if (data.success) {
                    mflg.ShowInfoDialog('Success', 'T&C(s) have been removed.');
                    initTNCGrid(true);
                    $("#tncDialogContainer").data("kendoWindow").close();
                } else {
                    mflg.ShowInfoDialog('Error', data.message);
                }
            },
            error: function () {
                mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
            },
            complete: function () {
                mflg.spinner.stop();
            }
        });
    });
}

//cable car closure
var selectedClosureProducts = [];

function checkClosureProduct() {
    if ($(this).attr('checked')) {
        var isAdded = false;
        for (var i = 0; i < selectedClosureProducts.length; i++) {
            if (selectedClosureProducts[i] == $(this).val()) {
                isAdded = true;
                break;
            }
        }
        if (!isAdded) {
            selectedClosureProducts.push($(this).val());
        }
    }
    else {
        var index = -1;
        for (var i = 0; i < selectedClosureProducts.length; i++) {
            if (selectedClosureProducts[i] == $(this).val()) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            selectedClosureProducts.splice(index, 1);
        }
    }
}

function initClosureGrid(resetPage) {
    var grid = $("#closureMainGrid").data('kendoGrid');
    if (!grid) {
        var url = '/admin/products/shared-config/getClosureGrid';

        $('#closureMainGrid').kendoGrid({
            dataSource: {
                transport: {read: {url: url, cache: false}},
                schema: {
                    data: 'closureRecords',
                    total: 'closureRecordTotal',
                    model: {
                        fields: {
                            id: {type: 'number'},
                            period: {type: 'String'},
                            remark: {type: 'String'},
                            closeFrom: {type: 'String'},
                            closeTo: {type: 'String'},
                            products: {type: 'String'},
                            productIds: {type: 'String'},
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true
            },
            columns: [
                {
                    field: 'period',
                    title: 'Closure Schedule',
                    template: '<div><a href="javascript:void(0)"onclick="showEditClosureDialog(#=id#, \'#:closeFrom#\', \'#:closeTo#\', \'#:remark#\', \'#:productIds#\' )">\'#:period#\'</a></div>',
                    width: 250
                },
                {field: 'remark', title: 'Closure Details', width: 250},
                {field: 'products', title: 'Products', width: 200},
                {
                    title: 'Remove',
                    template: '<div style="text-align:center;"><input type="checkbox" onclick="checkDeleteClosure(this, #=id#)" /></div>',
                    width: 60
                }
            ],
            pageable: true,
            sortable: false,
            dataBound: clearSelectedClosureDeleteIds
        });
    }
    else {
        var ds = grid.dataSource;
        if (resetPage) {
            ds.page(1);
        }
        else {
            ds.read();
        }
    }
}

function initClosureDialog() {
    if (!$("#closureDialogContainer").data("kendoWindow")) {
        $("#closureDialogContainer").kendoWindow({
            width: "500px",
            visible: false,
            modal: true,
            actions: []
        }).data("kendoWindow");
        $("#cloCancelBtn").click(function () {
            $("#closureDialogContainer").data("kendoWindow").close();
        });
        $("#closureDialogContainer").parent().find(".k-window-action").hide();
        $("#closureDialogContainer_wnd_title").css('height', '25px');
    }
}

function showEditClosureDialog(id, from, to, remark, products) {
    var window = $("#closureDialogContainer").data("kendoWindow");
    window.open().center().title("Edit Closure Schedule");
    $("#cloSaveBtn").unbind('click');

    $("#cloFrom").data('kendoDatePicker').value(from);
    $("#cloTo").data('kendoDatePicker').value(to);
    $("#cloReamrk").val(remark);

    selectedClosureProducts = [];
    $("input[name=closureProducts]").attr("checked", false);

    var splittedProductsId = products.split(",");

    for (var i = 0; i < splittedProductsId.length; i++) {
        if (splittedProductsId[i] != "") {
            selectedClosureProducts.push(splittedProductsId[i]);
            $("#cp" + splittedProductsId[i]).attr("checked", true);
        }
    }


    $("#cloSaveBtn").click(function () {
        if (!validateClosureSave()) {
            return;
        }
        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            closureSave(id);
        });
    });
    console.log(selectedClosureProducts);
}

function showAddClosureDialog() {
    var window = $("#closureDialogContainer").data("kendoWindow");
    window.open().center().title("Add Closure Schedule");
    $("#cloSaveBtn").unbind('click');

    $("#cloFrom").data('kendoDatePicker').value("");
    $("#cloTo").data('kendoDatePicker').value("");
    $("#cloReamrk").val("");

    selectedClosureProducts = [];
    $("input[name=closureProducts]").attr("checked", false);

    $("#cloSaveBtn").click(function () {
        if (!validateClosureSave()) {
            return;
        }
        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            closureSave(0);
        });
    });
    console.log(selectedClosureProducts);
}

function validateClosureSave() {
    var message = '';
    var result = true;
    if ($("#cloFrom").data('kendoDatePicker').value() == null) {
        message += 'Closed from date is required.<br />';
        result = false;
    }
    if ($("#cloTo").data('kendoDatePicker').value() == null) {
        message += 'Closed to date is required.<br />';
        result = false;
    }

    var from = $('#cloFrom').data('kendoDatePicker').value();
    var to = $('#cloTo').data('kendoDatePicker').value();
    if (from != null && to != null) {
        if (from > to) {
            message += "From date cannot be greater than to date.<br />";
            result = false;
        }
    }

    var remark = $("#cloReamrk").val().trim();
    if (remark == "") {
        message += "Closure details are required.";
        result = false;
    }

    if (selectedClosureProducts.length == 0) {
        message += "Please select at least one product.";
        result = false;
    }

    if (message != '') {
        mflg.ShowInfoDialog('Error', message);
    }

    return result;
}

function closureSave(id) {
    var url = '/admin/products/shared-config/updateClosure?';
    url += 'cloId=' + id;
    var from = convertDate2String($("#cloFrom").data('kendoDatePicker').value());
    var to = convertDate2String($("#cloTo").data('kendoDatePicker').value());
    url += '&cloFrom=' + from;
    url += '&cloTo=' + to;
    url += "&pros=" + selectedClosureProducts.toString();
    var remark = $("#cloReamrk").val().trim();
    //url += '&cloRemark=' + remark;

    mflg.spinner.start();
    $.ajax({
        url: url,
        type: 'POST', cache: false, dataType: 'json',
        data: {
            cloRemark: remark
        },
        success: function (data) {
            if (data.success) {
                if (id > 0) {
                    mflg.ShowInfoDialog('Success', 'Closure schedule has been updated.');
                }
                else {
                    mflg.ShowInfoDialog('Success', 'Closure schedule has been added.');
                }
                initClosureGrid(false);
                $("#closureDialogContainer").data("kendoWindow").close();
            } else {
                mflg.ShowInfoDialog('Error', data.message);
            }
        },
        error: function () {
            mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
        },
        complete: function () {
            mflg.spinner.stop();
        }
    });
}

var selectedClosureDeleteIds = [];

function checkDeleteClosure(check, id) {
    if ($(check).attr("checked")) {
        selectedClosureDeleteIds.push(id);
    }
    else {
        for (var i = selectedClosureDeleteIds.length; i >= 0; i--) {
            if (selectedClosureDeleteIds[i] == id) {
                selectedClosureDeleteIds.splice(i, 1);
                break;
            }
        }
    }
}

function clearSelectedClosureDeleteIds() {
    selectedClosureDeleteIds = [];
}

function deleteClosure() {
    if (selectedClosureDeleteIds.length == 0) {
        mflg.ShowInfoDialog('Error', 'Please select cable car closure schedule(s) to delete.');
        return;
    }
    var url = '/admin/products/shared-config/deleteClosure?deleteCloIds=' + selectedClosureDeleteIds;
    mflg.ShowConfirmDialog("Warning", "Are you sure you want to remove the selected car closure schedule(s)?");
    $("#confirm_yes_btn").click(function () {
        mflg.spinner.start();
        $.ajax({
            url: url,
            type: 'POST', cache: false, dataType: 'json',
            success: function (data) {
                if (data.success) {
                    mflg.ShowInfoDialog('Success', 'Cable car closure schedule(s) have been removed.');
                    initClosureGrid(true);
                    $("#closureDialogContainer").data("kendoWindow").close();
                } else {
                    mflg.ShowInfoDialog('Error', data.message);
                }
            },
            error: function () {
                mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
            },
            complete: function () {
                mflg.spinner.stop();
            }
        });
    });
}

//merchant
function initMerchantGrid(resetPage) {
    var grid = $("#merchantMainGrid").data('kendoGrid');

    if (!grid) {
        var url = '/admin/products/shared-config/getMerchantGrid';

        $('#merchantMainGrid').kendoGrid({
            dataSource: {
                transport: {read: {url: url, cache: false}},
                schema: {
                    data: 'merchantRecords',
                    total: 'merchantRecordTotal',
                    model: {
                        fields: {
                            id: {type: 'number'},
                            name: {type: 'String'},
                            bank: {type: 'String'},
                            acceptsAllCards: {type: 'String'},
                            isActive: {type: 'String'},
                            tmMerchantId: {type: 'String'}
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true
            },
            columns: [
                {
                    field: 'name',
                    title: 'Merchant Name',
                    template: '<div><a href="javascript:void(0)"onclick="showEditMerchantDialog(#=id#, \'#:name#\', \'#:bank#\', \'#:acceptsAllCards#\', \'#:isActive#\', \'#:tmMerchantId#\')" />#:name#</div>',
                    width: 200
                },
                {field: 'bank', title: 'Bank', width: 120},
                {field: 'acceptsAllCards', title: 'Accepts All Cards', width: 120},
                {field: 'isActive', title: 'Is Active', width: 120},
                {field: 'tmMerchantId', title: 'TM Merchant ID', width: 150},
                {
                    title: 'Remove',
                    template: '<div style="text-align:center;"><input type="checkbox" onclick="checkDeleteMerchant(this, #=id#)" /></div>',
                    width: 60
                }
            ],
            pageable: true,
            sortable: false,
            dataBound: clearSelectedMerchantDeleteIds
        });
    }
    else {
        var ds = grid.dataSource;
        if (resetPage) {
            ds.page(1);
        }
        else {
            ds.read();
        }
    }

}

function initMerchantDialog() {
    if (!$("#merchantDialogContainer").data("kendoWindow")) {
        $("#merchantDialogContainer").kendoWindow({
            width: "500px",
            visible: false,
            modal: true,
            actions: []
        }).data("kendoWindow");
        $("#merCancelBtn").click(function () {
            $("#merchantDialogContainer").data("kendoWindow").close();
        });
        $("#merchantDialogContainer").parent().find(".k-window-action").hide();
        $("#merchantDialogContainer_wnd_title").css('height', '25px');
    }
}

function showEditMerchantDialog(id, name, bank, accAll, active, merId) {
    var window = $("#merchantDialogContainer").data("kendoWindow");
    window.open().center().title("Edit Merchant");
    $("#cloSaveBtn").unbind('click');

    $("#merName").data('kendoAutoComplete').value(name);
    $("#merID").data('kendoAutoComplete').value(merId);
    $("#merBank").data('kendoDropDownList').value(bank);
    if (accAll == "Yes") {
        $("#merAccept").attr("checked", true);
    }
    else {
        $("#merAccept").attr("checked", false);
    }
    if (active == "Yes") {
        $("#merActive").attr("checked", true);
    }
    else {
        $("#merActive").attr("checked", false);
    }

    $("#merSaveBtn").click(function () {
        if (!validateMerchantSave()) {
            return;
        }
        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            merchantSave(id);
        });
    });
}

function showAddMerchantDialog() {
    var window = $("#merchantDialogContainer").data("kendoWindow");
    window.open().center().title("Add Merchant");
    $("#cloSaveBtn").unbind('click');

    $("#merName").data('kendoAutoComplete').value("");
    $("#merID").data('kendoAutoComplete').value("");
    $("#merBank").data('kendoDropDownList').value("Any");
    $("#merAccept").attr("checked", false);
    $("#merActive").attr("checked", false);

    $("#merSaveBtn").click(function () {
        if (!validateMerchantSave()) {
            return;
        }
        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            merchantSave(0);
        });
    });
}

function validateMerchantSave() {
    var message = '';
    var result = true;
    if ($("#merName").data('kendoAutoComplete').value().trim() == "") {
        message += 'Merchant name is required.<br />';
        result = false;
    }
    if ($("#merID").data('kendoAutoComplete').value().trim() == "") {
        message += 'TM merchant ID is required.<br />';
        result = false;
    }

    if (message != '') {
        mflg.ShowInfoDialog('Error', message);
    }

    return result;
}

function merchantSave(id) {
    var url = '/admin/products/shared-config/updateMerchant?';
    url += 'merId=' + id;
    var merName = $("#merName").data('kendoAutoComplete').value().trim();
    var merTMId = $("#merID").data('kendoAutoComplete').value().trim();
    url += '&merName=' + merName;
    url += '&merTMId=' + merTMId;
    var merBank = $("#merBank").data('kendoDropDownList').value();
    url += '&merBank=' + merBank;
    var merAccAll = "No";
    if ($("#merAccept").attr("checked")) {
        merAccAll = "Yes";
    }
    var merIsActive = "No";
    if ($("#merActive").attr("checked")) {
        merIsActive = "Yes";
    }
    url += '&merAccAll=' + merAccAll;
    url += '&merIsActive=' + merIsActive;

    mflg.spinner.start();
    $.ajax({
        url: url,
        type: 'POST', cache: false, dataType: 'json',
        success: function (data) {
            if (data.success) {
                if (id > 0) {
                    mflg.ShowInfoDialog('Success', 'Merchant has been updated.');
                }
                else {
                    mflg.ShowInfoDialog('Success', 'Merchant has been added.');
                }
                initMerchantGrid(false);
                $("#merchantDialogContainer").data("kendoWindow").close();
            } else {
                mflg.ShowInfoDialog('Error', data.message);
            }
        },
        error: function () {
            mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
        },
        complete: function () {
            mflg.spinner.stop();
        }
    });
}

var selectedMerchantDeleteIds = [];

function checkDeleteMerchant(check, id) {
    if ($(check).attr("checked")) {
        selectedMerchantDeleteIds.push(id);
    }
    else {
        for (var i = selectedMerchantDeleteIds.length; i >= 0; i--) {
            if (selectedMerchantDeleteIds[i] == id) {
                selectedMerchantDeleteIds.splice(i, 1);
                break;
            }
        }
    }
}

function clearSelectedMerchantDeleteIds() {
    selectedMerchantDeleteIds = [];
}

function deleteMerchant() {
    if (selectedMerchantDeleteIds.length == 0) {
        mflg.ShowInfoDialog('Error', 'Please select merchant(s) to delete.');
        return;
    }
    var url = '/admin/products/shared-config/deleteMerchant?deleteMerIds=' + selectedMerchantDeleteIds;
    mflg.ShowConfirmDialog("Warning", "Are you sure you want to remove the selected merchant(s)?");
    $("#confirm_yes_btn").click(function () {
        mflg.spinner.start();
        $.ajax({
            url: url,
            type: 'POST', cache: false, dataType: 'json',
            success: function (data) {
                if (data.success) {
                    mflg.ShowInfoDialog('Success', 'Merchant(s) have been removed.');
                    initMerchantGrid(true);
                    $("#merchantDialogContainer").data("kendoWindow").close();
                } else {
                    mflg.ShowInfoDialog('Error', data.message);
                }
            },
            error: function () {
                mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
            },
            complete: function () {
                mflg.spinner.stop();
            }
        });
    });
}

//jcMember

$(document).ready(function () {
    //jcmember
    $('#jcFrom').kendoDatePicker({
        start: 'year', depth: 'year', format: 'MM/yyyy'
    });
    $('#jcTo').kendoDatePicker({
        start: 'year', depth: 'year', format: 'MM/yyyy'
    });
    $("#searchJCMemberInput").kendoAutoComplete();
    $("#searchJCMember").click(searchJCMember);
    initJCMemberGrid(true);
    initUploader();
    $("#jcMemberCardNumber").kendoAutoComplete();
    $('#jcMemberExpiryDate').kendoDatePicker({
        format: mflg.DATE_FORMAT
    });
    initJCMemberDialog();
    $("#removeJCMember").click(deleteJCMember);
    $('#removeFilteredRecords').click(deleteJcFiltered).hide();
});

function deleteJcFiltered() {
    if (searchJcFrom == '' && searchJcTo == '') {
        alert('Please filter by expiry dates first.');
        $('#removeFilteredRecords').fadeOut();
        return;
    }
    if (confirm('Are you sure you want to remove the filtered Faber Licence records? This operation cannot be undone.')) {
        mflg.spinner.start();
        $.ajax({
            url: '/.sky-dining-admin/shared-config/delete-jc-filtered?searchMemberCard=' +
            searchJcCard + '&searchJcFrom=' + searchJcFrom + '&searchJcTo=' + searchJcTo,
            type: 'POST', cache: false, dataType: 'json',
            success: function (data) {
                if (data.success) {
                    mflg.ShowInfoDialog('Success', 'Filtered Faber Licence members have been removed.');
                    initJCMemberGrid(true);
                } else {
                    mflg.ShowInfoDialog('Error', data.message);
                }
            },
            error: function () {
                mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
            },
            complete: function () {
                mflg.spinner.stop();
            }
        });
    }
}

function searchJCMember() {
    var $from = $('#jcFrom');
    var kFrom = $from.data('kendoDatePicker');
    if ($from.val() != '' && kFrom.value() == null) {
        $from.val('');
    }
    searchJcFrom = $from.val();

    var $to = $('#jcTo');
    var kTo = $to.data('kendoDatePicker');
    if ($to.val() != '' && kTo.value() == null) {
        $to.val('');
    }
    searchJcTo = $to.val();

    searchJcCard = $("#searchJCMemberInput").data('kendoAutoComplete').value();
    initJCMemberGrid(true);
    toggleRemoveFilterBtn();
}

function toggleRemoveFilterBtn() {
    if (searchJcFrom != '' || searchJcTo != '') {
        $('#removeFilteredRecords').fadeIn();
    } else {
        $('#removeFilteredRecords').fadeOut();
    }
}

var searchJcFrom = '';
var searchJcTo = '';
var searchJcCard = '';

function initJCMemberGrid(resetPage) {
    var grid = $('#jcMemberMainGrid').data("kendoGrid");
    var url = '/.sky-dining-admin/shared-config/get-jc-member-grid?searchMemberCard=' +
        searchJcCard + '&searchJcFrom=' + searchJcFrom + '&searchJcTo=' + searchJcTo;
    if (!grid) {
        $('#jcMemberMainGrid').kendoGrid({
            dataSource: {
                transport: {read: {url: url, cache: false}},
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        fields: {
                            id: {type: 'number'},
                            cardNumber: {type: 'String'},
                            expiryDate: {type: 'date'}
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true
            },
            columns: [
                {
                    field: 'cardNumber',
                    title: 'Membership No.',
                    width: 250,
                    template: '<div><a href="javascript:void(0)" onclick="showEditJCMemberDialog(#=id#, \'#:cardNumber#\', \'#= kendo.toString(expiryDate,"dd/MM/yyyy") #\')" />#:cardNumber#</div>'
                },
                {
                    field: 'expiryDate',
                    title: 'Expiry Date',
                    width: 250,
                    template: '#= kendo.toString(expiryDate,"MM/yyyy") #'
                },
                {
                    title: 'Remove',
                    template: '<div style="text-align:center;"><input type="checkbox" onclick="checkDeleteJCMember(this, #=id#)" /></div>',
                    width: 60
                }
            ],
            pageable: true,
            sortable: false,
            dataBound: clearSelectedJCMemberDeleteIds
        });
    }
    else {
        var ds = grid.dataSource;
        ds.options.transport.read.url = url;
        if (resetPage) {
            ds.page(1);
        }
        else {
            ds.read();
        }
    }
}

function initJCMemberDialog() {
    if (!$("#jcMemberDialogContainer").data("kendoWindow")) {
        $("#jcMemberDialogContainer").kendoWindow({
            width: "500px",
            visible: false,
            modal: true,
            actions: []
        }).data("kendoWindow");
        $("#jcMemberCancelBtn").click(function () {
            $("#jcMemberDialogContainer").data("kendoWindow").close();
        });
        $("#jcMemberDialogContainer").parent().find(".k-window-action").hide();
        $("#jcMemberDialogContainer_wnd_title").css('height', '25px');
    }
}

function validateJCMemberSave() {
    var message = '';
    var result = true;
    var jcNumberValue = $("#jcMemberCardNumber").data('kendoAutoComplete').value().trim();
    if (jcNumberValue == "") {
        message += 'Membership number is required.<br />';
        result = false;
    }
    else if (jcNumberValue.length != 11 || !(/^[IF0-9]+$/.test(jcNumberValue))) {
        message += 'Membership number is invalid.<br />';
        result = false;
    }

    if ($("#jcMemberExpiryDate").data('kendoDatePicker') == null) {
        message += 'Expiry date is required.<br />';
        result = false;
    }

    if (message != '') {
        mflg.ShowInfoDialog('Error', message);
    }

    return result;
}

function showEditJCMemberDialog(id, cardNumber, expiryDate) {
    var window = $("#jcMemberDialogContainer").data("kendoWindow");
    window.open().center().title("Edit Faber Licence Member");
    $("#jcMemberSaveBtn").unbind('click');

    $("#jcMemberCardNumber").data('kendoAutoComplete').value(cardNumber);
    $("#jcMemberExpiryDate").data('kendoDatePicker').value(expiryDate);

    $("#jcMemberSaveBtn").click(function () {
        if (!validateJCMemberSave()) {
            return;
        }
        mflg.ShowConfirmDialog("Warning", "Are you sure you want to save the changes?");
        $("#confirm_yes_btn").click(function () {
            jcMemberSave(id);
        });
    });
}

function jcMemberSave(id) {
    var url = '/.sky-dining-admin/shared-config/update-jc-member?';
    url += 'jcId=' + id;
    var jcCardNumber = $("#jcMemberCardNumber").data('kendoAutoComplete').value().trim().toUpperCase();
    var jcExpiry = $("#jcMemberExpiryDate").data('kendoDatePicker').value();
    url += '&jcCardNumber=' + jcCardNumber;
    url += '&jcExpiry=' + convertDate2String(jcExpiry);

    mflg.spinner.start();
    $.ajax({
        url: url,
        type: 'POST', cache: false, dataType: 'json',
        success: function (data) {
            if (data.success) {
                if (id > 0) {
                    mflg.ShowInfoDialog('Success', 'Faber Licence member has been updated.');
                }
                initJCMemberGrid(false);
                $("#jcMemberDialogContainer").data("kendoWindow").close();
            } else {
                mflg.ShowInfoDialog('Error', data.message);
            }
        },
        error: function () {
            mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
        },
        complete: function () {
            mflg.spinner.stop();
        }
    });
}

var selectedJCMemberDeleteIds = [];

function checkDeleteJCMember(check, id) {
    if ($(check).attr("checked")) {
        selectedJCMemberDeleteIds.push(id);
    }
    else {
        for (var i = selectedJCMemberDeleteIds.length; i >= 0; i--) {
            if (selectedJCMemberDeleteIds[i] == id) {
                selectedJCMemberDeleteIds.splice(i, 1);
                break;
            }
        }
    }
}

function clearSelectedJCMemberDeleteIds() {
    selectedJCMemberDeleteIds = [];
}

function deleteJCMember() {
    if (selectedJCMemberDeleteIds.length == 0) {
        mflg.ShowInfoDialog('Error', 'Please select Faber Licence member(s) to delete.');
        return;
    }
    var url = '/.sky-dining-admin/shared-config/delete-jc-member?deleteJcIds=' + selectedJCMemberDeleteIds;
    mflg.ShowConfirmDialog("Warning", "Are you sure you want to remove the selected Faber Licence member(s)?");
    $("#confirm_yes_btn").click(function () {
        mflg.spinner.start();
        $.ajax({
            url: url,
            type: 'POST', cache: false, dataType: 'json',
            success: function (data) {
                if (data.success) {
                    mflg.ShowInfoDialog('Success', 'Faber Licence member(s) have been removed.');
                    initJCMemberGrid(true);
                    $("#jcMemberDialogContainer").data("kendoWindow").close();
                } else {
                    mflg.ShowInfoDialog('Error', data.message);
                }
            },
            error: function () {
                mflg.ShowInfoDialog('Error', 'Unable to process your request. Please try again in a few moments.');
            },
            complete: function () {
                mflg.spinner.stop();
            }
        });
    });
}


function initUploader() {
    if ($("#jcMemberUploader").data('kendoUpload')) {
        $("#jcMemberUploader").data('kendoUpload').destroy();
        $("#uploaderContainer").html('<input name="jcMemberUploader" id="jcMemberUploader" type="file" />');
    }

    $("#jcMemberUploader").kendoUpload({
        async: {
            saveUrl: "/.sky-dining-admin/shared-config/upload-jc-member-excel",
            autoUpload: false
        },
        localization: {
            dropFilesHere: "",
            select: "Upload Excel"
        },
        select: onJCMemberUploadSelect,
        success: onJCMemberUploadSuccess,
        error: onJCMemberUploadError,
        multiple: false
    });
}

function onJCMemberUploadSelect(e) {
    var fileName = e.files[0].name;
    var ext = fileName.split('.').pop();
    if (ext == null || (ext != "xls" && ext != "xlsx")) {
        alert("The excel file you selected is invalid. Only .xls or .xlsx file is allowed.");
        e.preventDefault();
    }
}

function onJCMemberUploadSuccess(e) {
    alert("The excel file has been uploaded successfully.");
    initJCMemberGrid(true);
}

function onJCMemberUploadError(e) {
    alert("The excel file has not been uploaded successfully.");
}

function convertDate2String(date) {
    if (date == null) {
        return "";
    }
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var monthString = month;
    if (month < 10) {
        monthString = "0" + monthString;
    }
    var day = date.getDate();
    var dayString = day;
    if (day < 10) {
        dayString = "0" + dayString;
    }
    return dayString + "/" + monthString + "/" + year;
}

(function (mflg, $) {

})(window.mflg = window.mflg || {}, jQuery);