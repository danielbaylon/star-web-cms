$(document).ready(function () {
    mflg.pageSize = 10;
    mflg.initHackKendoGridSortForStruts();

    $('#transStartDate').kendoDatePicker({
        format: mflg.DATE_FORMAT,
        change: function () {
        }
    });
    $('#transEndDate').kendoDatePicker({
        format: mflg.DATE_FORMAT,
        change: function () {
        }
    });
    $('#dateOfVisit').kendoDatePicker({
        format: mflg.DATE_FORMAT,
        change: function () {
        }
    });
    $("#tranStatus").kendoDropDownList();
    $("#resType").kendoDropDownList();
    $("#resStatus").kendoDropDownList();
    $("#recNum").kendoAutoComplete();
    $("#pckName").kendoAutoComplete();
    $("#guestName").kendoAutoComplete();

    $("#exportType").kendoDropDownList();

    $("#export_btn").click();

    $('#add_btn').click(function () {
        window.location.assign('reservation-view.html');
    });

    $('#search_btn').click(function () {
        mflg.reservationsGrid.refreshUrl({
            transactionStartDate: $('#transStartDate').val(),
            transactionEndDate: $('#transEndDate').val(),
            transactionStatus: $('#tranStatus').val(),
            receiptNumber: $('#recNum').val(),
            packageName: $('#pckName').val(),
            dateOfVisit: $('#dateOfVisit').val(),
            reservationType: $('#resType').val(),
            guestName: $('#guestName').val(),
            reservationStatus: $('#resStatus').val(),
        });
    });

    $('#reset_btn').click(function () {
        mflg.reservationsGrid.refreshUrl();
        $('#transStartDate').val('');
        $('#transEndDate').val('');
        $('#tranStatus').data("kendoDropDownList").value('');
        $('#recNum').val('');
        $('#pckName').val('');
        $('#dateOfVisit').val('');
        $('#resType').data("kendoDropDownList").value('');
        $('#guestName').val('');
        $('#resStatus').data("kendoDropDownList").value('');
    });

    $("#exportDiv").hide();

    mflg.initReservationsGrid();
});

(function (mflg, $) {

    mflg.reservationsGrid = {};
    mflg.initReservationsGrid = function () {
        mflg.reservationsGrid = new mflg.ReservationsGrid();
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.ReservationsGrid = function () {
        var s = this;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/reservation/get-reservations';
        s.theUrl = s.BASE_URL;
        s.refreshUrl = function (filters) {
            if (filters) {
                s.theUrl = s.BASE_URL + '?transactionStartDate='
                    + filters.transactionStartDate + '&transactionEndDate='
                    + filters.transactionEndDate + '&transactionStatus='
                    + filters.transactionStatus + '&receiptNumber='
                    + filters.receiptNumber + '&packageName='
                    + filters.packageName + '&dateOfVisit='
                    + filters.dateOfVisit + '&reservationType='
                    + filters.reservationType + '&guestName='
                    + filters.guestName + '&reservationStatus='
                    + filters.reservationStatus;
            } else {
                s.theUrl = s.BASE_URL;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        initGrid();
        function initGrid() {
            s.kGrid = $('#mainGrid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.theUrl,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    guestName: {
                                        type: 'string'
                                    },
                                    packageName: {
                                        type: 'string'
                                    },
                                    dateOfVisit: {
                                        type: 'string'
                                    },
                                    transactionDate: {
                                        type: 'string'
                                    },
                                    receiptNumber: {
                                        type: 'string'
                                    },
                                    reservationStatus: {
                                        type: 'string'
                                    },
                                    reservationType: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            field: 'receiptNumber',
                            title: 'Receipt Number',
                            template: '<a href="reservation-view.html?id=${id}">${receiptNumber}</a>',
                            sortable: false
                        },
                        {
                            field: 'transactionDate',
                            title: 'Transaction Date',
                            width: 125
                        },
                        {
                            field: 'guestName',
                            title: 'Guest Name',
                            template: '<a href="reservation-view.html?id=${id}">${guestName}</a>'
                        },
                        {
                            field: 'packageName',
                            title: 'Package Name',
                            template: '<a href="reservation-view.html?id=${id}">${packageName}</a>',
                            sortable: false
                        }, {
                            field: 'dateOfVisit',
                            title: 'Date of Visit',
                            width: 100
                        }, {
                            field: 'reservationStatus',
                            title: 'Status',
                            width: 100
                        }, {
                            field: 'reservationType',
                            title: 'Type',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    }

})(window.mflg = window.mflg || {}, jQuery);