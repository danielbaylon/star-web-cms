$(document).ready(function () {
});

(function (mflg, $) {

    mflg.mainCourseGrid = {};
    mflg.topUpGrid = {};

    mflg.MainCourseGrid = function (parentId, menuId) {
        var s = this;
        s.parentId = parentId ? parentId : 0;
        s.menuId = menuId ? menuId : 0;

        s.kGrid = {};
        s.isEditing = parentId < 1;

        s.BASE_URL = '/.sky-dining-admin/reservation/get-edit-main-courses';
        s.theUrl = s.BASE_URL + '?parentId=' + s.parentId + '&menuId='
            + s.menuId;
        s.refresh = function (parentId, menuId) {
            s.parentId = parentId ? parentId : 0;
            s.menuId = menuId ? menuId : 0;
            s.theUrl = s.BASE_URL + '?parentId=' + s.parentId + '&menuId='
                + s.menuId;
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }

            if (s.isEditing) {
                $("span.main-course-quantity-text").hide();
                $("input.main-course-quantity").show();
            } else {
                $("span.main-course-quantity-text").show();
                $("input.main-course-quantity").hide();
            }
        };

        init();
        function init() {
            s.kGrid = $('#main-courses-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.theUrl,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    mainCourseId: {
                                        type: 'number'
                                    },
                                    mainCourseDescription: {
                                        type: 'string'
                                    },
                                    quantity: {
                                        type: 'number'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: 'mainCourseDescription',
                            title: 'Main Course',
                        },
                        {
                            field: 'quantity',
                            title: 'Quantity',
                            width: 125,
                            template: '<span class="main-course-quantity-text">${quantity}</span><input class="input-small main-course-quantity" min="0" type="number" main-course-id="${mainCourseId}" value="${quantity}"/>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }

        s.enableEdit = function () {
            s.isEditing = true;

        };

        s.disableEdit = function () {
            s.isEditing = false;
        }
    };

    mflg.TopUpGrid = function (parentId, packageId) {
        var s = this;
        s.parentId = parentId ? parentId : 0;
        s.packageId = packageId ? packageId : 0;

        s.kGrid = {};
        s.isEditing = parentId < 1;

        s.BASE_URL = '/.sky-dining-admin/reservation/get-edit-top-ups';
        s.theUrl = s.BASE_URL + '?parentId=' + s.parentId + '&packageId='
            + s.packageId;
        s.refresh = function (parentId, packageId) {
            s.parentId = parentId ? parentId : 0;
            s.packageId = packageId ? packageId : 0;
            s.theUrl = s.BASE_URL + '?parentId=' + s.parentId + '&packageId='
                + s.packageId;
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }

            if (s.isEditing) {
                $("span.topup-quantity-text").hide();
                $("input.topup-quantity").show();
            } else {
                $("span.topup-quantity-text").show();
                $("input.topup-quantity").hide();
            }

            mflg.reservationModel.doComputePrices();
        };

        init();
        function init() {
            s.kGrid = $('#topups-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.theUrl,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    topUpId: {
                                        type: 'number'
                                    },
                                    topUpName: {
                                        type: 'string'
                                    },
                                    topUpPrice: {
                                        type: 'string'
                                    },
                                    topUpPriceInCents: {
                                        type: 'number'
                                    },
                                    quantity: {
                                        type: 'number'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: 'topUpName',
                            title: 'Top Up',
                        },
                        {
                            field: 'topUpPrice',
                            title: 'Price',
                            width: 100
                        },
                        {
                            field: 'quantity',
                            title: 'Quantity',
                            width: 125,
                            template: '<span class="topup-quantity-text">${quantity}</span><input class="input-small topup-quantity" min="0" type="number" price="${topUpPriceInCents}" topup-id="${topUpId}" value="${quantity}" onchange="mflg.reservationModel.doComputePrices();"/>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }

        s.enableEdit = function () {
            s.isEditing = true;

        };

        s.disableEdit = function () {
            s.isEditing = false;
        }
    }

})(window.mflg = window.mflg || {}, jQuery);