$(document).ready(function () {
});

(function (mflg, $) {

    mflg.tncGrid = {};
    mflg.tncModel = {};
    mflg.tncDialog = {};

    mflg.themeGrid = {};
    mflg.themeModel = {};
    mflg.themeDialog = {};

    mflg.menuGrid = {};
    mflg.menuDialog = {};
    mflg.selectMenuGrid = {};

    mflg.topUpGrid = {};
    mflg.topUpDialog = {};
    mflg.selectTopUpGrid = {};

    mflg.TncModel = function () {
        var s = this;

        tinymce
            .init({
                selector: "#tnc-content-field",
                plugins: ["advlist lists link charmap anchor",
                    "visualblocks code", "table contextmenu paste"],
                menubar: false,
                statusbar: false,
                toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
            });

        s.id = ko.observable(0);
        s.title = ko.observable('');
        s.tncContent = ko.observable('');
        s.selected = ko.observable('');

        s.newTitle = ko.observable('');
        s.newTncContent = ko.observable('');
        s.newSelected = ko.observable('');

        s.tncHasError = ko.observable(false);
        s.tncMessage = ko.observable('');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.title(data.title);
                s.tncContent(data.tncContent);
                s.selected(data.selected);

                s.newTitle(data.title);
                s.newTncContent(data.tncContent);
                s.newSelected(data.selected);
            } else {
                s.id(0);
                s.title('');
                s.tncContent('');
                s.selected(false);

                s.newTitle('');
                s.newTncContent('');
                s.newSelected(false);
            }

            if ($('#tnc-content-field').tinymce()) {
                $('#tnc-content-field').tinymce().setContent(s.newTncContent());
            }

            s.tncHasError(false);
            s.tncMessage('');
        };

        s.doValidateTnc = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newTitle())
                || !nvx.isLengthWithin(s.newTitle(), 1, 200, true)) {
                messsage += "<li>Title is required and must not exceed 200 characters.</li>";
            }

            s.newTncContent($('#tnc-content-field').tinymce().getContent());
            if (nvx.isEmpty(s.newTncContent())
                || !nvx.isLengthWithin(s.newTncContent(), 1, 4000, true)) {
                messsage += '<li>Content is required and must not exceed 4000 characters.</li>';
            }

            if (messsage != '') {
                s.tncMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.tncHasError(true);
                return false;
            }
            s.tncMessage('');
            s.tncHasError(false);
            return true;
        };

        s.doSaveTnc = function () {
            if (s.doValidateTnc()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/package/save-tnc',
                        data: {
                            id: s.id(),
                            parentId: mflg.packageModel.id(),
                            newTitle: s.newTitle(),
                            newTncContent: s.newTncContent(),
                            newSelected: s.newSelected(),
                            timestamp: mflg.packageModel.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Terms and Conditions',
                                    'The terms and conditions have been saved successfully.');
                                mflg.packageModel.newSelectedTncId(s.id());
                                mflg.packageModel.newTncContent(s
                                    .newTncContent());
                                mflg.packageModel.timestamp(data.timestamp);
                                mflg.tncDialog.close(true);
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelTnc = function () {
            mflg.tncDialog.close(false);
        }
    };

    mflg.TncGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-tncs?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#tncs-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    title: {
                                        type: 'string'
                                    },
                                    tncContent: {
                                        type: 'string'
                                    },
                                    selected: {
                                        type: 'boolean'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: 'title',
                            title: 'Select Terms and Conditions',
                            template: '<a onclick="mflg.tncDialog.open(\'${uid}\')">${title}</a>'
                        },
                        {
                            width: 125,
                            template: '<div style="text-align:center;"><a class="btn btn-success" onclick="mflg.packageModel.doSelectTnc(\'${uid}\')">Select This</a></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.TncDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#tnc-modal').kendoWindow({
                title: 'Terms and Conditions',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };

    };

    mflg.ThemeModel = function () {
        var s = this;

        s.imageSelected = ko.observable(false);
        $('#theme-image').kendoUpload({
            multiple: false,
            localization: {
                select: 'Select image to upload...'
            },
            select: function () {
                s.imageSelected(true);
            },
            remove: function () {
                s.imageSelected(false);
            },
        });

        s.id = ko.observable(0);
        s.description = ko.observable('');
        s.imageHash = ko.observable('');
        s.capacity = ko.observable(0);

        s.newDescription = ko.observable('');
        s.newImageHash = ko.observable('');
        s.newCapacity = ko.observable(0);

        s.themeHasError = ko.observable(false);
        s.themeMessage = ko.observable('');

        s.parentId = ko.observable('');
        s.timestamp = ko.observable('');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.description(data.description);
                s.imageHash(data.imageHash);
                s.capacity(data.capacity);

                s.newDescription(data.description);
                s.newImageHash(data.imageHash);
                s.newCapacity(data.capacity);
            } else {
                s.description('');
                s.imageHash('');
                s.capacity(null);

                s.newDescription('');
                s.newImageHash('');
                s.newCapacity(null);
            }
            s.imageSelected(false);
            $("#theme-modal :file").not(document.getElementById("theme-image"))
                .remove();
            $("#theme-modal .k-upload-files").remove();
            $("#theme-modal .k-upload-status").remove();
            $("#theme-modal .k-upload.k-header").addClass("k-upload-empty");
            $("#theme-modal .k-upload-button").removeClass("k-state-focused");

            s.parentId(mflg.packageModel.id());
            s.timestamp(mflg.packageModel.timestamp());

            s.themeHasError(false);
            s.themeMessage('');
        };

        s.doValidateTheme = function () {
            var messsage = '';

            if (s.isNew() && !s.imageSelected()) {
                messsage += '<li>Display image is required.</li>';
            }

            if (nvx.isEmpty(s.newDescription())
                || !nvx.isLengthWithin(s.newDescription(), 1, 4000, true)) {
                messsage += '<li>Description is required and must not exceed 4000 characters.</li>';
            }

            if (nvx.isEmpty(s.newCapacity()) || !nvx.isNumber(s.newCapacity())
                || !nvx.isInteger(Number(s.newCapacity()))
                || ((Number(s.newCapacity()) < 1))) {
                messsage += "<li>Capacity is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.themeMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.themeHasError(true);
                return false;
            }
            s.themeMessage('');
            s.themeHasError(false);
            return true;
        };

        $("form#theme-form").submit(function () {
            if (s.doValidateTheme()) {
                mflg.spinner.start();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: '/.sky-dining-admin/package/save-theme',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    cache: false,
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Saved Theme',
                                'The theme has been saved successfully.');
                            mflg.packageModel.timestamp(data.data.timestampText);
                            mflg.themeDialog.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error',
                            mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
            }

            return false;
        });

        s.doCancelTheme = function () {
            mflg.themeDialog.close(false);
        }
    };

    mflg.ThemeGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-themes?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#themes-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    imageHash: {
                                        type: 'string'
                                    },
                                    description: {
                                        type: 'string'
                                    },
                                    capacity: {
                                        type: 'number'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="themes-checkbox" theme-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'imageHash',
                            title: 'Image',
                            template: '<img alt="No image selected." src="${imageHash}"/>',
                            width: 300,
                        },
                        {
                            field: 'description',
                            title: 'Item Description',
                            template: '<a onclick="mflg.themeDialog.open(\'${uid}\')">${description}</a>'
                        }, {
                            field: 'capacity',
                            title: 'Daily Capacity',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.ThemeDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#theme-modal').kendoWindow({
                title: 'Theme',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };

    };

    mflg.MenuGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-menus?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#menus-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    price: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="remove-menus-checkbox" menu-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'name',
                            title: 'Name',
                            template: '<a href="menu-view.html?id=${id}">${name}</a>'
                        },
                        {
                            field: 'price',
                            title: 'Price',
                            template: '<a href="menu-view.html?id=${id}">${price}</a>',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.TopUpGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-topups?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#topups-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    displayOrder: {
                                        type: 'int'
                                    },
                                    imageHash: {
                                        type: 'string'
                                    },
                                    description: {
                                        type: 'string'
                                    },
                                    price: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        sort: {
                            field: 'displayOrder',
                            dir: 'asc',
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="remove-topups-checkbox" topup-id="${id}" /></div>',
                            width: 60,
                            sortable: false
                        },
                        {
                            field: 'displayOrder',
                            title: 'Order',
                            width: 60
                        },
                        {
                            field: 'imageHash',
                            title: 'Image',
                            template: '<img alt="No image selected." src="${imageHash}"/>',
                            width: 300,
                            sortable: false
                        },
                        {
                            field: 'description',
                            title: 'Description',
                            template: '<a href="top-up-view.html?id=${id}">${description}</a>'
                        },
                        {
                            field: 'price',
                            title: 'Price',
                            template: '<a href="top-up-view.html?id=${id}">${price}</a>',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.SelectMenuGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-select-menus?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.theUrl = s.BASE_URL;
        s.refresh = function (filters) {
            s.theUrl = s.BASE_URL;
            if (filters) {
                s.theUrl += '?nameFilter=' + filters.nameFilter
                    + '&activeFilter=' + filters.activeFilter;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#select-menus-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Select',
                            template: '<div style="text-align:center;"><input type="checkbox" class="add-menus-checkbox" menu-id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'name',
                            title: 'Name'
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.SelectTopUpGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-select-topups?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.theUrl = s.BASE_URL;
        s.refresh = function (filters) {
            s.theUrl = s.BASE_URL;
            if (filters) {
                s.theUrl += '?nameFilter=' + filters.nameFilter
                    + '&activeFilter=' + filters.activeFilter;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#select-topups-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    displayOrder: {
                                        type: 'int'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Select',
                            template: '<div style="text-align:center;"><input type="checkbox" class="add-topups-checkbox" topup-id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'name',
                            title: 'Name'
                        },
                        {
                            field: 'displayOrder',
                            title: 'Order',
                            width: 60
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.MenuDialog = function (parentGrid, selectGrid) {
        var s = this;

        s.parentGrid = parentGrid;
        s.selectGrid = selectGrid;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#menu-modal').kendoWindow({
                title: 'Select Menus',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');

            $('#add-selected-menus').click(function () {
                s.addSelected();
            });

            $('#cancel-menu-select').click(function () {
                s.close();
            });
        };

        s.addSelected = function () {
            var idList = '';
            $("input.add-menus-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("menu-id");
                idList += ',';
            });

            if (idList == '') {
                mflg.ShowInfoDialog('Adding Menus...', 'No menus selected.');
                return;
            }

            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/package/add-menus',
                    data: {
                        idList: idList,
                        parentId: mflg.packageModel.id(),
                        timestamp: mflg.packageModel.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Added Menus',
                                'The selected menus have been successfully added.');
                            mflg.packageModel.timestamp(data.timestamp);
                            s.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.open = function (rowUid) {
            s.selectGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.parentGrid.refresh();
            }
            s.kWin.close();
        };
    };

    mflg.TopUpDialog = function (parentGrid, selectGrid) {
        var s = this;

        s.parentGrid = parentGrid;
        s.selectGrid = selectGrid;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#topup-modal').kendoWindow({
                title: 'Select Top Ups',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');

            $('#add-selected-topups').click(function () {
                s.addSelected();
            });

            $('#cancel-topup-select').click(function () {
                s.close();
            });
        };

        s.addSelected = function () {
            var idList = '';
            $("input.add-topups-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("topup-id");
                idList += ',';
            });

            if (idList == '') {
                mflg
                    .ShowInfoDialog('Adding Top Ups...',
                    'No top ups selected.');
                return;
            }

            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/package/add-topups',
                    data: {
                        idList: idList,
                        parentId: mflg.packageModel.id(),
                        timestamp: mflg.packageModel.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Added Top Ups',
                                'The selected top ups have been successfully added.');
                            mflg.packageModel.timestamp(data.timestamp);
                            s.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.open = function (rowUid) {
            s.selectGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.parentGrid.refresh();
            }
            s.kWin.close();
        };
    }

})(window.mflg = window.mflg || {}, jQuery);