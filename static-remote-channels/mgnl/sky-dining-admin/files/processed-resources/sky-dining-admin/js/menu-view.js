$(document).ready(
    function () {
        mflg.pageSize = 10;
        mflg.initHackKendoGridSortForStruts();

        mflg.menuModel = new mflg.MenuModel();
        mflg.getModelData();

        ko.applyBindings(mflg.menuModel, document
            .getElementById("menu-view"));

        mflg.mainCourseGrid = new mflg.MainCourseGrid(mflg.getParameterByName("id"));

        mflg.mainCourseModel = new mflg.MainCourseModel();
        mflg.mainCourseModel.init();

        ko.applyBindings(mflg.mainCourseModel, document
            .getElementById("main-course-modal"));

        mflg.mainCourseDialog = new mflg.MainCourseDialog(
            mflg.mainCourseGrid, mflg.mainCourseModel);
        mflg.mainCourseDialog.init();

        mflg.selectMainCourseGrid = new mflg.SelectMainCourseGrid(
            mflg.getParameterByName("id"));
        mflg.selectMainCourseDialog = new mflg.SelectMainCourseDialog(
            mflg.mainCourseGrid, mflg.selectMainCourseGrid);
        mflg.selectMainCourseDialog.init();

        $("body").scrollspy();
    });

(function (mflg, $) {
    mflg.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    mflg.getModelData = function () {
        var modelId = mflg.getParameterByName("id");

        mflg.spinner.start();
        $
            .ajax({
                url: '/.sky-dining-admin/menu/view-menu',
                data: {
                    id: modelId,
                },
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.menuModel.init(data.data);
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.menuModel = {};

    mflg.mainCourseGrid = {};
    mflg.mainCourseModel = {};
    mflg.mainCourseDialog = {};
    mflg.selectMainCourseDialog = {};
    mflg.selectMainCourseGrid = {};

    mflg.MenuModel = function () {
        var s = this;

        s.timestamp = ko.observable('');

        s.startDateField = $("#start-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#end-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        tinymce
            .init({
                selector: "#description-field",
                plugins: ["advlist lists link charmap anchor",
                    "visualblocks code", "table contextmenu paste"],
                menubar: false,
                statusbar: false,
                toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
            });

        s.menuInfoEditFlag = ko.observable(false);
        s.menuInfoHasError = ko.observable(false);
        s.menuInfoMessage = ko.observable('');

        s.displayInfoEditFlag = ko.observable(false);
        s.displayInfoHasError = ko.observable(false);
        s.displayInfoMessage = ko.observable('');

        s.id = ko.observable(0);
        s.name = ko.observable('');
        s.description = ko.observable('');
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');
        s.salesCutOffDays = ko.observable(0);
        s.menuType = ko.observable(0);
        s.price = ko.observable(0);
        s.serviceCharge = ko.observable(0);
        s.gst = ko.observable(0);

        s.newName = ko.observable('');
        s.newDescription = ko.observable('');
        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');
        s.newSalesCutOffDays = ko.observable(0);
        s.newMenuType = ko.observable(0);
        s.newPrice = ko.observable(0);
        s.newServiceCharge = ko.observable(0);
        s.newGst = ko.observable(0);

        s.menuTypeText = ko.computed(function () {
            if (s.menuType() == 1) {
                return "Early Bird";
            } else if (s.menuType() == 2) {
                return "Promotion";
            }

            return "Normal";
        });

        s.init = function (data) {
            s.id(data.id);
            s.name(data.name);
            s.description(data.description);
            s.startDate(data.startDateText);
            s.endDate(data.endDateText);
            s.salesCutOffDays(data.salesCutOffDays);
            s.menuType(data.menuType);
            s.price(data.price);
            s.serviceCharge(data.serviceCharge);
            s.gst(data.gst);

            s.newName(data.name);
            s.newDescription(data.description);
            s.newStartDate(data.startDateText);
            s.newEndDate(data.endDateText);
            s.newSalesCutOffDays(data.salesCutOffDays);
            s.newMenuType(data.menuType);
            s.newPrice(data.price);
            s.newServiceCharge(data.serviceCharge);
            s.newGst(data.gst);

            s.startDateField.value(s.startDate());
            s.endDateField.value(s.endDate());

            s.timestamp(data.timestamp);
        };

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.headerText = ko.computed(function () {
            if (s.id() < 1) {
                return "New Menu";
            } else {
                return s.name();
            }
        });

        s.doSaveNewMenu = function () {
            var menuInfoValid = s.doValidateMenuInfo();
            var displayInfoValid = s.doValidateDisplayInfo();

            if (menuInfoValid && displayInfoValid) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/menu/save-menu',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newDescription: s.newDescription(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate(),
                            newSalesCutOffDays: s.newSalesCutOffDays(),
                            newMenuType: s.newMenuType(),
                            newPrice: s.newPrice(),
                            newServiceCharge: s.newServiceCharge(),
                            newGst: s.newGst(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Created Menu',
                                    'The menu has been created successfully. Refreshing the page.',
                                    function () {
                                        window.location
                                            .replace('menu-view.html?id='
                                            + data.data.newId);
                                    });
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doValidateMenuInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Menu name is required and must not exceed 300 characters.</li>";
            }

            if (nvx.isEmpty(s.startDateField.value())
                || nvx.isEmpty(s.endDateField.value())) {
                messsage += "<li>Both start date and end dates are required and must be valid dates.</li>";
            } else if (s.startDateField.value() > s.endDateField.value()) {
                messsage += "<li>Start date must not be later than end date.</li>";
            } else {
                s.newStartDate($("#start-date-field").val());
                s.newEndDate($("#end-date-field").val());
            }

            if (nvx.isEmpty(s.newSalesCutOffDays())
                || !nvx.isNumber(s.newSalesCutOffDays())
                || !nvx.isInteger(Number(s.newSalesCutOffDays()))
                || ((Number(s.newSalesCutOffDays()) < 0))) {
                if ($("#sales-cutoff-field").val() != '0') {
                    messsage += "<li>Sales cut-off before event date is required and must be a number.</li>";
                }
            }

            if (nvx.isEmpty(s.newPrice()) || !nvx.isNumber(s.newPrice())
                || !nvx.isInteger(Number(s.newPrice()))
                || ((Number(s.newPrice()) < 0))) {
                messsage += "<li>Price is required and must be a number.</li>";
            }

            if (nvx.isEmpty(s.newServiceCharge())
                || !nvx.isNumber(s.newServiceCharge())
                || !nvx.isInteger(Number(s.newServiceCharge()))
                || ((Number(s.newServiceCharge()) < 0))) {
                messsage += "<li>Service charges are required and must be a number.</li>";
            }

            if (nvx.isEmpty(s.newGst()) || !nvx.isNumber(s.newGst())
                || !nvx.isInteger(Number(s.newGst()))
                || ((Number(s.newGst()) < 0))) {
                messsage += "<li>GST is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.menuInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.menuInfoHasError(true);
                return false;
            }
            s.menuInfoMessage('');
            s.menuInfoHasError(false);
            return true;
        };

        s.doSaveMenuInfo = function () {
            if (s.isNew()) {
                s.doSaveNewMenu();
            } else if (s.doValidateMenuInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/menu/save-menu-info',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate(),
                            newSalesCutOffDays: s.newSalesCutOffDays(),
                            newMenuType: s.newMenuType(),
                            newPrice: s.newPrice(),
                            newServiceCharge: s.newServiceCharge(),
                            newGst: s.newGst(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Updated Menu Information',
                                    'The menu information has been updated successfully.');
                                s.name(s.newName());
                                s.startDate(s.newStartDate());
                                s.endDate(s.newEndDate());
                                s.salesCutOffDays(s.newSalesCutOffDays());
                                s.menuType(s.newMenuType());
                                s.price(s.newPrice());
                                s.serviceCharge(s.newServiceCharge());
                                s.gst(s.newGst());

                                s.timestamp(data.timestamp);
                                s.doEditMenuInfo();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelMenuInfo = function () {
            if (s.isNew()) {
                window.location.assign('menus.html');
            } else {
                s.doEditMenuInfo();
            }
        };

        s.doEditMenuInfo = function () {
            if (s.menuInfoEditFlag()) {
                s.menuInfoMessage('');
                s.menuInfoHasError(false);

                s.menuInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newStartDate(s.startDate());
                s.newEndDate(s.endDate());
                s.newSalesCutOffDays(s.salesCutOffDays());
                s.newMenuType(s.menuType());
                s.newPrice(s.price());
                s.newServiceCharge(s.serviceCharge());
                s.newGst(s.gst());

                s.startDateField.value(s.startDate());
                s.endDateField.value(s.endDate());

                s.menuInfoEditFlag(true);
            }
        };

        s.isEditingMenuInfo = ko.computed(function () {
            return s.menuInfoEditFlag() || s.isNew();
        });

        s.doValidateDisplayInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Name is required and must not exceed 300 characters.</li>";
            }

            s.newDescription($('#description-field').tinymce().getContent());
            if (nvx.isEmpty(s.newDescription())
                || !nvx.isLengthWithin(s.newDescription(), 1, 4000, true)) {
                messsage += '<li>Description is required and must not exceed 4000 characters.</li>';
            }

            if (messsage != '') {
                s
                    .displayInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.displayInfoHasError(true);
                return false;
            }
            s.displayInfoMessage('');
            s.displayInfoHasError(false);
            return true;
        };

        s.doSaveDisplayInfo = function () {
            if (s.isNew()) {
                s.doSaveNewMenu();
            } else if (s.doValidateDisplayInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/menu/save-display-info',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newDescription: s.newDescription(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Updated Display Information',
                                    'The display information has been updated successfully.');
                                s.name(s.newName());
                                s.description(s.newDescription());

                                s.timestamp(data.timestamp);
                                s.doEditDisplayInfo();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelDisplayInfo = function () {
            if (s.isNew()) {
                window.location.assign('menus.html');
            } else {
                s.doEditDisplayInfo();
            }
        };

        s.doEditDisplayInfo = function () {
            if (s.displayInfoEditFlag()) {
                s.displayInfoMessage('');
                s.displayInfoHasError(false);

                s.displayInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newDescription(s.description());

                $('#description-field').tinymce().setContent(s.description());

                s.displayInfoEditFlag(true);
            }
        };

        s.isEditingDisplayInfo = ko.computed(function () {
            return s.displayInfoEditFlag() || s.isNew();
        });

        s.doAddMainCourse = function () {
            mflg.selectMainCourseDialog.open();
        };

        s.doCreateMainCourse = function () {
            mflg.mainCourseDialog.open();
        };

        s.doRemoveMainCourse = function () {
            var idList = '';
            $("input.main-courses-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("main-course-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Main Courses...',
                    'No main courses selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing MainCourses...',
                'Are you sure you want to remove the selected main courses?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/menu/remove-main-courses',
                            data: {
                                idList: idList,
                                parentId: mflg.menuModel.id(),
                                timestamp: mflg.menuModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Main Courses',
                                        'The selected main courses have been successfully removed.');
                                    mflg.menuModel
                                        .timestamp(data.timestamp);
                                    mflg.mainCourseGrid
                                        .refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        }

    };

    mflg.MainCourseModel = function () {
        var s = this;

        s.id = ko.observable(0);
        s.code = ko.observable('');
        s.type = ko.observable(0);
        s.description = ko.observable('');

        s.newCode = ko.observable('');
        s.newType = ko.observable(0);
        s.newDescription = ko.observable('');

        s.mainCourseHasError = ko.observable(false);
        s.mainCourseMessage = ko.observable('');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.code(data.code);
                s.type(data.type);
                s.description(data.description);

                s.newCode(data.code);
                s.newType(data.type);
                s.newDescription(data.description);
            } else {
                s.id(0);
                s.code('');
                s.type(0);
                s.description('');

                s.newCode('');
                s.newType(0);
                s.newDescription('');
            }
            s.mainCourseHasError(false);
            s.mainCourseMessage('');
        };

        s.doValidateMainCourse = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newCode())
                || !nvx.isLengthWithin(s.newCode(), 1, 50, true)) {
                messsage += '<li>Main course code is required and must not exceed 50 characters.</li>';
            }

            if (nvx.isEmpty(s.newDescription())
                || !nvx.isLengthWithin(s.newDescription(), 1, 4000, true)) {
                messsage += '<li>Description is required and must not exceed 4000 characters.</li>';
            }

            if (messsage != '') {
                s.mainCourseMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.mainCourseHasError(true);
                return false;
            }
            s.mainCourseMessage('');
            s.mainCourseHasError(false);
            return true;
        };

        s.doSaveMainCourse = function () {
            if (s.doValidateMainCourse()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/menu/save-main-course',
                        data: {
                            id: s.id(),
                            parentId: mflg.menuModel.id(),
                            newCode: s.newCode(),
                            newType: s.newType(),
                            newDescription: s.newDescription(),
                            timestamp: mflg.menuModel.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Main Course',
                                    'The main course has been saved successfully.');
                                mflg.mainCourseDialog.close(true);
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelMainCourse = function () {
            mflg.mainCourseDialog.close(false);
        }
    };

    mflg.MainCourseGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/menu/get-main-courses?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#main-courses-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    description: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="main-courses-checkbox" main-course-id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'description',
                            title: 'Main Course',
                            template: '<a onclick="mflg.mainCourseDialog.open(\'${uid}\')">${description}</a>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.MainCourseDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#main-course-modal').kendoWindow({
                title: 'Main Course',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };
    };

    mflg.SelectMainCourseDialog = function (parentGrid, selectGrid) {
        var s = this;

        s.parentGrid = parentGrid;
        s.selectGrid = selectGrid;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#select-main-course-modal').kendoWindow({
                title: 'Select Main Courses',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');

            $('#add-selected-main-courses').click(function () {
                s.addSelected();
            });

            $('#cancel-main-course-select').click(function () {
                s.close();
            });
        };

        s.addSelected = function () {
            var idList = '';
            $("input.add-main-courses-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("main-course-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Adding Main Courses...',
                    'No main courses selected.');
                return;
            }

            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/menu/add-main-courses',
                    data: {
                        idList: idList,
                        parentId: mflg.menuModel.id(),
                        timestamp: mflg.menuModel.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Added Main Courses',
                                'The selected main courses have been successfully added.');
                            mflg.menuModel.timestamp(data.timestamp);
                            s.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.open = function (rowUid) {
            s.selectGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.parentGrid.refresh();
            }
            s.kWin.close();
        };
    };

    mflg.SelectMainCourseGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/menu/get-select-main-courses?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.theUrl = s.BASE_URL;
        s.refresh = function (filters) {
            s.theUrl = s.BASE_URL;
            if (filters) {
                s.theUrl += '?nameFilter=' + filters.nameFilter
                    + '&activeFilter=' + filters.activeFilter;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#select-main-courses-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    description: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Select',
                            template: '<div style="text-align:center;"><input type="checkbox" class="add-main-courses-checkbox" main-course-id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'description',
                            title: 'Description'
                        },
                        {
                            title: 'Delete',
                            width: 100,
                            sortable: false,
                            template: '<div style="text-align:center;"><a class="btn btn-danger" onclick="mflg.deleteMainCourse(\'${id}\')">Delete</a></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.deleteMainCourse = function (id) {
        mflg
            .ShowConfirmDialog(
            'Deleting Main Course...',
            'Are you sure you want to delete the main course?',
            function () {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/menu/delete-main-course',
                        data: {
                            id: id,
                            timestamp: mflg.menuModel
                                .timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Deleted Main Course',
                                    'The main course has been successfully deleted.');
                                mflg.selectMainCourseGrid
                                    .refresh();
                            } else {
                                mflg.ShowInfoDialog('Error',
                                    mflg.ErrorSpan.replace(
                                        'THE_MSG',
                                        data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            });

    }

})(window.mflg = window.mflg || {}, jQuery);