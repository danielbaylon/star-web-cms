$(document).ready(function () {
    mflg.pageSize = 10;
    mflg.initHackKendoGridSortForStruts();

    mflg.initTopUpsGrid();

    $('#add-button').click(function () {
        window.location.assign('top-up-view.html');
    });

    $('#filter-button').click(function () {
        mflg.topUpsGrid.refreshUrl({
            nameFilter: $('#name-filter').val(),
            activeFilter: $('#active-filter').val()
        });
    });

    $('#reset-button').click(function () {
        mflg.topUpsGrid.refreshUrl({
            nameFilter: '',
            activeFilter: -1
        });
        $('#name-filter').val('');
        $('#active-filter').val(-1);
    });
});

(function (mflg, $) {

    mflg.topUpsGrid = {};
    mflg.initTopUpsGrid = function () {
        mflg.topUpsGrid = new mflg.TopUpsGrid();
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.TopUpsGrid = function () {
        var s = this;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/top-up/get-top-ups';
        s.theUrl = s.BASE_URL;
        s.refreshUrl = function (filters) {
            s.theUrl = s.BASE_URL + '?nameFilter=' + filters.nameFilter
                + '&activeFilter=' + filters.activeFilter;
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var column;


                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        initGrid();
        function initGrid() {
            s.kGrid = $('#top-ups-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.theUrl,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    displayOrder: {
                                        type: 'int'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            field: 'name',
                            title: 'Top Ups',
                            template: '<a href="top-up-view.html?id=${id}">${name}</a>'
                        },
                        {
                            field: 'displayOrder',
                            title: 'Order',
                            width: 60
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 150,
                            sortable: false,
                            template: '<span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    }

})(window.mflg = window.mflg || {}, jQuery);