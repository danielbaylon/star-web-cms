$(document).ready(function () {
    mflg.pageSize = 10;
    mflg.initHackKendoGridSortForStruts();

    mflg.topUpModel = new mflg.TopUpModel();
    mflg.getModelData();

    ko.applyBindings(mflg.topUpModel, document.getElementById("top-up-view"));

    $("body").scrollspy();
});

(function (mflg, $) {
    mflg.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    mflg.getModelData = function () {
        var modelId = mflg.getParameterByName("id");

        mflg.spinner.start();
        $
            .ajax({
                url: '/.sky-dining-admin/top-up/view-top-up',
                data: {
                    id: modelId,
                },
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.topUpModel.init(data.data);
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.topUpModel = {};

    mflg.TopUpModel = function () {
        var s = this;

        s.timestamp = ko.observable('');

        s.startDateField = $("#start-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#end-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        s.imageSelected = ko.observable(false);
        $('#image-file').kendoUpload({
            multiple: false,
            localization: {
                select: 'Select image to upload...'
            },
            select: function () {
                s.imageSelected(true);
            },
            remove: function () {
                s.imageSelected(false);
            },
        });

        s.topUpInfoEditFlag = ko.observable(false);
        s.topUpInfoHasError = ko.observable(false);
        s.topUpInfoMessage = ko.observable('');

        s.id = ko.observable(0);
        s.name = ko.observable('');
        s.type = ko.observable(0);
        s.description = ko.observable('');
        s.displayOrder = ko.observable();
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');
        s.salesCutOffDays = ko.observable(0);
        s.price = ko.observable(0);
        s.imageHash = ko.observable('');

        s.newName = ko.observable('');
        s.newType = ko.observable(0);
        s.newDescription = ko.observable('');
        s.newDisplayOrder = ko.observable();
        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');
        s.newSalesCutOffDays = ko.observable(0);
        s.newPrice = ko.observable(0);
        s.newImageHash = ko.observable('');

        s.typeText = ko.computed(function () {
            switch (Number(s.type())) {
                case 1:
                    return "Cake";
                    break;
                case 2:
                    return "Flowers";
                    break;
                case 3:
                    return "Wine";
                    break;
                default:
                    return "Other";
                    break;
            }
        });

        s.init = function (data) {
            s.id(data.id);
            s.name(data.name);
            s.type(data.type);
            s.description(data.description);
            s.displayOrder(data.displayOrder);
            s.startDate(data.startDateText);
            s.endDate(data.endDateText);
            s.salesCutOffDays(data.salesCutOffDays);
            s.price(data.price);
            s.imageHash(data.imageHash);

            s.newName(data.name);
            s.newType(data.type);
            s.newDescription(data.description);
            s.newDisplayOrder(data.displayOrder);
            s.newStartDate(data.startDateText);
            s.newEndDate(data.endDateText);
            s.newSalesCutOffDays(data.salesCutOffDays);
            s.newPrice(data.price);
            s.newImageHash(data.imageHash);

            s.startDateField.value(s.startDate());
            s.endDateField.value(s.endDate());

            s.timestamp(data.timestamp);
        };

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.headerText = ko.computed(function () {
            if (s.id() < 1) {
                return "New Top Up";
            } else {
                return s.name();
            }
        });

        s.doValidateTopUpInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Top Up name is required and must not exceed 300 characters.</li>";
            }

            if (!nvx.isEmpty(s.newDescription())
                && !nvx.isLengthWithin(s.newDescription(), 0, 4000, true)) {
                messsage += "<li>Description must not exceed 4000 characters.</li>";
            }

            if (nvx.isEmpty(s.startDateField.value())
                || nvx.isEmpty(s.endDateField.value())) {
                messsage += "<li>Both start date and end dates are required and must be valid dates.</li>";
            } else if (s.startDateField.value() > s.endDateField.value()) {
                messsage += "<li>Start date must not be later than end date.</li>";
            } else {
                s.newStartDate($("#start-date-field").val());
                s.newEndDate($("#end-date-field").val());
            }

            if (nvx.isEmpty(s.newSalesCutOffDays())
                || !nvx.isNumber(s.newSalesCutOffDays())
                || !nvx.isInteger(Number(s.newSalesCutOffDays()))
                || ((Number(s.newSalesCutOffDays()) < 0))) {
                if ($("#sales-cutoff-field").val() != '0') {
                    messsage += "<li>Sales cut-off before event date is required and must be a number.</li>";
                }
            }

            if (nvx.isEmpty(s.newPrice()) || !nvx.isNumber(s.newPrice())
                || !nvx.isInteger(Number(s.newPrice()))
                || ((Number(s.newPrice()) < 0))) {
                messsage += "<li>Price is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.topUpInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.topUpInfoHasError(true);
                return false;
            }
            s.topUpInfoMessage('');
            s.topUpInfoHasError(false);
            return true;
        };

        s.doSaveTopUpInfo = function () {
            if (s.doValidateTopUpInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/top-up/save-top-up',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newType: s.newType(),
                            newDescription: s.newDescription(),
                            newDisplayOrder: s.newDisplayOrder(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate(),
                            newSalesCutOffDays: s.newSalesCutOffDays(),
                            newPrice: s.newPrice(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                if (s.isNew()) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Saved Top Up',
                                        'The top up has been saved successfully. Refreshing the page.',
                                        function () {
                                            window.location
                                                .replace('top-up-view.html?id='
                                                + data.data.newId);
                                        });
                                } else {
                                    mflg
                                        .ShowInfoDialog('Saved Top Up',
                                        'The top up has been saved successfully.');
                                    s.name(s.newName());
                                    s.type(s.newType());
                                    s.description(s.newDescription());
                                    s.displayOrder(s.newDisplayOrder());
                                    s.startDate(s.newStartDate());
                                    s.endDate(s.newEndDate());
                                    s.salesCutOffDays(s
                                        .newSalesCutOffDays());
                                    s.price(s.newPrice());

                                    s.timestamp(data.timestamp);
                                    s.doEditTopUpInfo();
                                }
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelTopUpInfo = function () {
            if (s.isNew()) {
                window.location.assign('top-ups.html');
            } else {
                s.doEditTopUpInfo();
            }
        };

        s.doEditTopUpInfo = function () {
            if (s.topUpInfoEditFlag()) {
                s.topUpInfoMessage('');
                s.topUpInfoHasError(false);

                s.topUpInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newType(s.type());
                s.newDescription(s.description());
                s.newDisplayOrder(s.displayOrder());
                s.newStartDate(s.startDate());
                s.newEndDate(s.endDate());
                s.newSalesCutOffDays(s.salesCutOffDays());
                s.newPrice(s.price());

                s.startDateField.value(s.startDate());
                s.endDateField.value(s.endDate());

                s.topUpInfoEditFlag(true);
            }
        };

        s.isEditingTopUpInfo = ko.computed(function () {
            return s.topUpInfoEditFlag() || s.isNew();
        });

        $("form#display-image-form").submit(function () {
            mflg.spinner.start();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: '/.sky-dining-admin/top-up/save-display-image',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                cache: false,
                success: function (data) {
                    if (data.success) {
                        mflg.ShowInfoDialog('Uploaded Image',
                            'Display image has been successfully updated.');
                        s.timestamp(data.data.timestampText);
                        s.imageHash(data.data.imageHash);

                        s.imageSelected(false);
                        $("#image-sect :file").not(
                            document.getElementById("image-file")).remove();
                        $("#image-sect .k-upload-files").remove();
                        $("#image-sect .k-upload-status").remove();
                        $("#image-sect .k-upload.k-header").addClass(
                            "k-upload-empty");
                        $("#image-sect .k-upload-button").removeClass(
                            "k-state-focused");
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });

            return false;
        });
    }

})(window.mflg = window.mflg || {}, jQuery);