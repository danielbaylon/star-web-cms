$(document).ready(function () {
});

(function (mflg, $) {

    mflg.promoGrid = {};
    mflg.promoModel = {};
    mflg.promoDialog = {};

    mflg.promoCodeGrid = {};
    mflg.promoCodeModel = {};
    mflg.promoCodeDialog = {};

    mflg.promoMenuGrid = {};
    mflg.promoMenuDialog = {};
    mflg.selectPromoMenuGrid = {};

    mflg.merchGrid = {};

    mflg.PromoModel = function () {
        var s = this;

        s.id = ko.observable(0);
        s.name = ko.observable('');
        s.active = ko.observable('');
        s.type = ko.observable('');
        s.merchantId = ko.observable(0);
        s.priceType = ko.observable(0);
        s.discountPrice = ko.observable(0);

        s.newName = ko.observable('');
        s.newActive = ko.observable('');
        s.newType = ko.observable('');
        s.newMerchantId = ko.observable(0);
        s.newPriceType = ko.observable(0);
        s.newDiscountPrice = ko.observable(0);

        s.promoHasError = ko.observable(false);
        s.promoMessage = ko.observable('');

        s.validFrom = ko.observable('');
        s.validTo = ko.observable('');
        s.tnc = ko.observable('');

        s.newValidFrom = ko.observable('');
        s.newValidTo = ko.observable('');
        s.newTnc = ko.observable('');

        tinymce.init({
            selector: "#promo-tnc",
            plugins: [
                "advlist lists link charmap anchor",
                "visualblocks code",
                "table contextmenu paste"
            ],
            menubar: false,
            statusbar: false,
            toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
        });

        s.validFromField = $("#promo-valid-from").kendoDatePicker({
            format: mflg.DATE_FORMAT,
            change: function () {
            }
        }).data('kendoDatePicker');

        s.validToField = $("#promo-valid-to").kendoDatePicker({
            format: mflg.DATE_FORMAT,
            change: function () {
            }
        }).data('kendoDatePicker');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.name(data.name);
                s.active(data.active ? 'true' : 'false');
                s.type(data.type);
                s.merchantId(data.merchantId);
                s.priceType(data.priceType);
                s.discountPrice(data.discountPrice);

                s.validFrom(data.validFromText);
                s.validTo(data.validToText);
                s.tnc(data.tnc == null ? '' : data.tnc);

                s.newName(data.name);
                s.newActive(data.active ? 'true' : 'false');
                s.newType(data.type);
                s.newMerchantId(data.merchantId);
                s.newPriceType(data.priceType);
                s.newDiscountPrice(data.discountPrice);

                s.newValidFrom(data.validFromText);
                s.newValidTo(data.validToText);
                s.newTnc(data.tnc == null ? '' : data.tnc);

                tinymce.get('promo-tnc').setContent(s.tnc());
                s.validFromField.value(s.validFrom());
                s.validToField.value(s.validTo());
            } else {
                s.id(0);
                s.name('');
                s.active('');
                s.type('');
                s.merchantId(0);
                s.priceType(1);
                s.discountPrice(0);

                s.validFrom('');
                s.validTo('');
                s.tnc('');

                s.newName('');
                s.newActive('');
                s.newType('');
                s.newMerchantId(0);
                s.newPriceType(1);
                s.newDiscountPrice(0);

                s.newValidFrom('');
                s.newValidTo('');
                s.newTnc('');

                if (tinymce.get('promo-tnc')) {
                    tinymce.get('promo-tnc').setContent(s.tnc());
                }
                s.validFromField.value(s.validFrom());
                s.validToField.value(s.validTo());
            }

            s.initMerchGridValue();

            s.promoHasError(false);
            s.promoMessage('');
        };

        s.doValidatePromo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 200, true)) {
                messsage += "<li>Promotion name is required and must not exceed 200 characters.</li>";
            }

            if (nvx.isEmpty(s.validFromField.value())
                || nvx.isEmpty(s.validToField.value())) {
                // messsage += "<li>Both valid from and valid until are required and must be valid dates.</li>";
            } else if (s.validFromField.value() > s.validToField.value()) {
                messsage += "<li>Valid from must not be later than valid until.</li>";
            }

            var type = s.newType();
            if (type == 'CreditCard') {
                var merchChk = $('input[name=merch-grid-radio]:checked');
                if (merchChk.length == 0) {
                    messsage += '<li>Please select at least one related merchant.</li>';
                } else {
                    s.newMerchantId(merchChk.val());
                }
            } else {
                s.newMerchantId(0);
            }

            if (messsage != '') {
                s.promoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.promoHasError(true);
                return false;
            }
            s.promoMessage('');
            s.promoHasError(false);
            return true;
        };

        s.doSavePromo = function () {
            if (s.doValidatePromo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/package/save-promo',
                        data: {
                            id: s.id(),
                            parentId: mflg.packageModel.id(),
                            newName: s.newName(),
                            newActive: s.newActive(),
                            newType: s.newType(),
                            newMerchantId: s.newMerchantId(),
                            timestamp: mflg.packageModel.timestamp(),
                            newPriceType: s.newPriceType(),
                            newDiscountPrice: s.newDiscountPrice,
                            newValidFrom: s.newValidFrom(),
                            newValidTo: s.newValidTo(),
                            newTnc: tinymce.get('promo-tnc').getContent()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog('Saved Promotion',
                                    'The promotion has been saved successfully.');
                                //mflg.packageModel.timestamp(data.data.timestamp);
                                if (s.isNew()) {
                                    s.id(data.data.newId);
                                    s.name(s.newName());
                                    s.active(s.newActive());
                                    s.type(s.newType());
                                    s.merchantId(s.newMerchantId());
                                    mflg.promoGrid.refresh();
                                } else {
                                    mflg.promoDialog.close(true);
                                }
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.initMerchGridValue = function () {
            if (s.merchantId() != -1) {
                $('input[name=merch-grid-radio]').each(function (idx, obj) {
                    var $obj = $(obj);
                    if ($obj.val() == s.merchantId()) {
                        $obj.attr('checked', 'checked');
                        return false;
                    }
                });
            }
        };

        s.doCancelPromo = function () {
            mflg.promoDialog.close(false);
        };

        s.doAddPromoMenu = function () {
            mflg.promoMenuDialog.open();
        };

        s.doAddPromoCode = function () {
            mflg.promoCodeDialog.open();
        };

        s.doRemovePromoCode = function () {
            var idList = '';
            $("input.promo-codes-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("promo-code-id");
                idList += ',';
            });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Promo Codes...',
                    'No promotion codes selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Promo Codes...',
                'Are you sure you want to remove the selected promotion codes?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-promo-codes',
                            data: {
                                idList: idList,
                                parentId: mflg.promoModel.id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Promo Codes',
                                        'The selected promotion codes have been successfully removed.');
                                    mflg.promoCodeGrid
                                        .refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doRemovePromoMenu = function () {
            var idList = '';
            $("input.remove-promo-menus-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("menu-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Menus...', 'No menus selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Menus...',
                'Are you sure you want to remove the selected menus?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-promo-menus',
                            data: {
                                idList: idList,
                                parentId: mflg.promoModel.id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Menus',
                                        'The selected menus have been successfully removed.');
                                    mflg.promoMenuGrid
                                        .refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        }
    };

    mflg.PromoCodeModel = function () {
        var s = this;

        s.startDateField = $("#promo-code-start-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#promo-code-end-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        s.id = ko.observable(0);
        s.code = ko.observable('');
        s.hasLimit = ko.observable('');
        s.limit = ko.observable(0);
        s.timesUsed = ko.observable(0);
        s.active = ko.observable('');
        s.type = ko.observable('');
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');

        s.newCode = ko.observable('');
        s.newHasLimit = ko.observable('');
        s.newLimit = ko.observable(0);
        s.newTimesUsed = ko.observable(0);
        s.newActive = ko.observable('');
        s.newType = ko.observable('');
        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');

        s.promoCodeHasError = ko.observable(false);
        s.promoCodeMessage = ko.observable('');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.code(data.code);
                s.hasLimit(data.hasLimit ? 'true' : 'false');
                s.limit(data.limit);
                s.timesUsed(data.timesUsed);
                s.active(data.active ? 'true' : 'false');
                s.type(data.type);
                s.startDate(data.startDateText);
                s.endDate(data.endDateText);

                s.newCode(data.code);
                s.newHasLimit(data.hasLimit ? 'true' : 'false');
                s.newLimit(data.limit);
                s.newTimesUsed(data.timesUsed);
                s.newActive(data.active ? 'true' : 'false');
                s.newType(data.type);
                s.newStartDate(data.startDateText);
                s.newEndDate(data.endDateText);
            } else {
                s.id(0);
                s.code('');
                s.hasLimit('');
                s.limit(0);
                s.timesUsed(0);
                s.active('');
                s.type('');
                s.startDate('');
                s.endDate('');

                s.newCode('');
                s.newHasLimit('');
                s.newLimit(0);
                s.newTimesUsed(0);
                s.newActive('');
                s.newType('');
                s.newStartDate('');
                s.newEndDate('');
            }

            s.promoCodeHasError(false);
            s.promoCodeMessage('');
        };

        s.doValidatePromoCode = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newCode())
                || !nvx.isLengthWithin(s.newCode(), 1, 100, true)) {
                messsage += "<li>Promotion code is required and must not exceed 100 characters.</li>";
            }

            if (messsage != '') {
                s.promoCodeMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.promoCodeHasError(true);
                return false;
            }
            s.promoCodeMessage('');
            s.promoCodeHasError(false);
            return true;
        };

        s.doSavePromoCode = function () {
            if (s.doValidatePromoCode()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/package/save-promo-code',
                        data: {
                            id: s.id(),
                            parentId: mflg.promoModel.id(),
                            newCode: s.newCode(),
                            newHasLimit: s.newHasLimit(),
                            newLimit: s.newLimit(),
                            newTimesUsed: s.newTimesUsed(),
                            newActive: s.newActive(),
                            newType: s.newType(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Promotion Code',
                                    'The promotion code has been saved successfully.');
                                mflg.packageModel.timestamp(data.timestamp);
                                mflg.promoCodeDialog.close(true);
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelPromoCode = function () {
            mflg.promoCodeDialog.close(false);
        }
    };

    mflg.PromoGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-promos?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#promos-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    active: {
                                        type: 'boolean'
                                    },
                                    type: {
                                        type: 'string'
                                    },
                                    typeText: {
                                        type: 'string'
                                    },
                                    activeText: {
                                        type: 'string'
                                    },
                                    priceType: {
                                        type: 'number'
                                    },
                                    discountPrice: {
                                        type: 'number'
                                    },
                                    validFromText: {
                                        type: 'string'
                                    },
                                    validToText: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="promos-checkbox" promo-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'name',
                            title: 'Name',
                            template: '<a onclick="mflg.promoDialog.open(\'${uid}\')">${name}</a>'
                        },
                        {
                            field: 'typeText',
                            title: 'Type',
                            width: 100
                        },
                        {
                            field: 'activeText',
                            title: 'Status',
                            width: 80,
                            template: '<div style="text-align:center;"><span style="color: #if (activeText != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${activeText}</span></div>'
                        },
                        {
                            field: 'validFromText',
                            title: 'Valid From',
                            width: 80
                        },
                        {
                            field: 'validToText',
                            title: 'Valid Until',
                            width: 80
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.PromoDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#promo-modal').kendoWindow({
                title: 'Promotion',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            mflg.promoMenuGrid.refresh();
            mflg.promoCodeGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };

    };

    mflg.PromoCodeDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#promo-code-modal').kendoWindow({
                title: 'Promotion Code',
                modal: true,
                resizable: false,
                width: '550px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };

    };

    mflg.PromoMenuGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-promo-menus?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.parentId = mflg.promoModel.id();
            s.BASE_URL = '/.sky-dining-admin/package/get-promo-menus?&parentId='
                + (s.parentId ? s.parentId : 0);
            s.kGrid.dataSource.options.transport.read.url = s.BASE_URL;
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#promo-menus-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    price: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="remove-promo-menus-checkbox" menu-id="${id}" /></div>',
                            width: 60
                        }, {
                            field: 'name',
                            title: 'Name',
                        }, {
                            field: 'price',
                            title: 'Price',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.PromoCodeGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-promo-codes?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.parentId = mflg.promoModel.id();
            s.BASE_URL = '/.sky-dining-admin/package/get-promo-codes?&parentId='
                + (s.parentId ? s.parentId : 0);
            s.kGrid.dataSource.options.transport.read.url = s.BASE_URL;
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#promo-codes-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    active: {
                                        type: 'boolean'
                                    },
                                    code: {
                                        type: 'string',
                                    },
                                    hasLimit: {
                                        type: 'boolean'
                                    },
                                    timesUsed: {
                                        type: 'number'
                                    },
                                    type: {
                                        type: 'string'
                                    },
                                    startDateText: {
                                        type: 'string'
                                    },
                                    endDateText: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="promo-codes-checkbox" promo-code-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'code',
                            title: 'Code',
                            template: '<a onclick="mflg.promoCodeDialog.open(\'${uid}\')">${code}</a>',
                            width: 100
                        },
                        {
                            field: 'hasLimit',
                            title: 'Has Limit?',
                            width: 80,
                            template: '#if (hasLimit) {#Yes#} else {#No#}#'
                        },
                        {
                            field: 'limit',
                            title: 'Limit',
                            width: 70,
                            format: '{0:n0}'
                        },
                        {
                            field: 'timesUsed',
                            title: 'Used',
                            width: 50
                        },
                        {
                            field: 'active',
                            title: 'Active?',
                            width: 75,
                            template: '#if (active) {#<span style="color: \\#1DA20B">Active</span>#} else {#<span style="color: \\#E00000">Inactive</span>#}#'
                        }, {
                            field: 'type',
                            title: 'Validity Type',
                            width: 140
                        }, {
                            field: 'startDateText',
                            title: 'Valid From',
                            width: 100
                        }, {
                            field: 'endDateText',
                            title: 'Valid To',
                            width: 100
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.PromoMenuDialog = function (parentGrid, selectGrid) {
        var s = this;

        s.parentGrid = parentGrid;
        s.selectGrid = selectGrid;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#promo-menu-modal').kendoWindow({
                title: 'Select Menus',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');

            $('#add-selected-promo-menus').click(function () {
                s.addSelected();
            });

            $('#cancel-promo-menu-select').click(function () {
                s.close();
            });
        };

        s.addSelected = function () {
            var idList = '';
            $("input.add-promo-menus-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("menu-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Adding Menus...', 'No menus selected.');
                return;
            }

            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/package/add-promo-menus',
                    data: {
                        idList: idList,
                        parentId: mflg.promoModel.id(),
                        timestamp: mflg.packageModel.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Added Menus',
                                'The selected menus have been successfully added.');
                            mflg.promoMenuGrid.refresh();
                            s.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.open = function (rowUid) {
            s.selectGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.parentGrid.refresh();
            }
            s.kWin.close();
        };
    };

    mflg.SelectPromoMenuGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/package/get-select-promo-menus?&menuTypeFilter=2&parentId='
            + (s.parentId ? s.parentId : 0);
        s.theUrl = s.BASE_URL;
        s.refresh = function (filters) {
            s.parentId = mflg.promoModel.id();
            s.BASE_URL = '/.sky-dining-admin/package/get-select-promo-menus?&menuTypeFilter=2&parentId='
                + (s.parentId ? s.parentId : 0);
            s.theUrl = s.BASE_URL;
            if (filters) {
                s.theUrl += '?nameFilter=' + filters.nameFilter
                    + '&activeFilter=' + filters.activeFilter;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#select-promo-menus-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Select',
                            template: '<div style="text-align:center;"><input type="checkbox" class="add-promo-menus-checkbox" menu-id="${id}" /></div>',
                            width: 75
                        },
                        {
                            field: 'name',
                            title: 'Name'
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.MerchGrid = function (gridId, radioName, parent) {
        var s = this;
        s.gridId = gridId;
        s.radioName = radioName;
        s.kGrid = {};
        s.parent = parent;

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            } else {
                s.parent.initMerchGridValue();
            }
        };

        initGrid();
        function initGrid() {
            s.kGrid = $('#' + s.gridId)
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: '/.sky-dining-admin/package/get-merchants',
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    isActive: {
                                        type: 'boolean'
                                    },
                                    bank: {
                                        type: 'string'
                                    },
                                    mid: {
                                        type: 'string'
                                    },
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: 'id',
                            title: ' ',
                            width: 30,
                            template: '<div style="text-align:center;"><input type="radio" name="'
                            + s.radioName
                            + '" value="${id}" /></div>'
                        }, {
                            field: 'name',
                            title: 'Merchant Name'
                        }, {
                            field: 'mid',
                            title: 'Telemoney MID',
                            width: 150,
                            attributes: {
                                style: 'text-align:center;'
                            }
                        }, {
                            field: 'bank',
                            title: 'Bank',
                            width: 150,
                            attributes: {
                                style: 'text-align:center;'
                            }
                        },
                        /*
                         * { field: 'isActive', title: 'Status', width:
                         * 85, attributes: {style:'text-align:center;'},
                         * template: '#if (isActive) {#<span
                         * style="color: \\#1DA20B">Active</span>#}
                         * else {#<span style="color:
                         * \\#E00000">Inactive</span>#}#'},
                         */
                    ],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

})(window.mflg = window.mflg || {}, jQuery);