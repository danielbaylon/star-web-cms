$(document).ready(
    function () {
        mflg.pageSize = 10;
        mflg.initHackKendoGridSortForStruts();

        mflg.sessionModel = new mflg.SessionModel();
        mflg.getModelData();

        ko.applyBindings(mflg.sessionModel, document
            .getElementById("session-view"));

        mflg.blockOutGrid = new mflg.BlockOutGrid(mflg.getParameterByName("id"));

        mflg.blockOutModel = new mflg.BlockOutModel();
        mflg.blockOutModel.init();

        ko.applyBindings(mflg.blockOutModel, document
            .getElementById("blockout-modal"));

        mflg.blockOutDialog = new mflg.BlockOutDialog(mflg.blockOutGrid,
            mflg.blockOutModel);
        mflg.blockOutDialog.init();

        mflg.packageGrid = new mflg.PackageGrid(mflg.getParameterByName("id"));
        mflg.selectPackageGrid = new mflg.SelectPackageGrid(
            mflg.getParameterByName("id"));

        mflg.packageDialog = new mflg.PackageDialog(mflg.packageGrid,
            mflg.selectPackageGrid);
        mflg.packageDialog.init();

        $("body").scrollspy();
    });

(function (mflg, $) {
    mflg.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    mflg.getModelData = function () {
        var modelId = mflg.getParameterByName("id");

        mflg.spinner.start();
        $
            .ajax({
                url: '/.sky-dining-admin/schedule/view-session',
                data: {
                    id: modelId,
                },
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.sessionModel.init(data.data);
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.sessionModel = {};

    mflg.blockOutGrid = {};
    mflg.blockOutModel = {};
    mflg.blockOutDialog = {};

    mflg.packageGrid = {};
    mflg.packageDialog = {};
    mflg.selectPackageGrid = {};

    mflg.SessionModel = function () {
        var s = this;

        s.timestamp = ko.observable('');

        s.fromDateField = $("#from-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#end-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.timeField = $("#time-field").kendoTimePicker({
            format: mflg.TIME_FORMAT
        }).data("kendoTimePicker");
        s.salesStartField = $("#sales-start-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.salesEndField = $("#sales-end-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        s.mainInfoEditFlag = ko.observable(false);
        s.mainInfoHasError = ko.observable(false);
        s.mainInfoMessage = ko.observable('');

        s.id = ko.observable(0);
        s.name = ko.observable('');
        s.sessionNumber = ko.observable(0);
        s.sessionType = ko.observable(0);
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');
        s.time = ko.observable('');
        s.salesStartDate = ko.observable('');
        s.salesEndDate = ko.observable('');
        s.capacity = ko.observable(0);
        s.active = ko.observable('false');
        s.effectiveDays = ko.observableArray([]);

        s.newName = ko.observable('');
        s.newSessionNumber = ko.observable(0);
        s.newSessionType = ko.observable(0);
        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');
        s.newTime = ko.observable('');
        s.newSalesStartDate = ko.observable('');
        s.newSalesEndDate = ko.observable('');
        s.newCapacity = ko.observable(0);
        s.newActive = ko.observable('false');
        s.newEffectiveDays = ko.observableArray([]);

        s.parseEffectiveDays = function (effectiveDaysString, observableArray) {
            observableArray.removeAll();
            if (effectiveDaysString) {
                for (var i = 1; i < 8; i++) {
                    if (effectiveDaysString.indexOf(i.toString()) > -1) {
                        observableArray.push(i.toString());
                    }
                }
            }
        };

        s.getEffectiveDaysString = function (observableArray) {
            observableArray.sort();
            var resultString = '';
            for (var i = 0; i < observableArray().length; i++) {
                resultString += observableArray()[i];
            }
            return resultString;
        };

        s.effectiveDaysText = ko.computed(function () {
            var text = '';
            for (var i = 0; i < s.effectiveDays().length; i++) {
                if (i > 0) {
                    text += ', ';
                }
                switch (s.effectiveDays()[i]) {
                    case '1':
                        text += 'Monday';
                        break;
                    case '2':
                        text += 'Tuesday';
                        break;
                    case '3':
                        text += 'Wednesday';
                        break;
                    case '4':
                        text += 'Thursday';
                        break;
                    case '5':
                        text += 'Friday';
                        break;
                    case '6':
                        text += 'Saturday';
                        break;
                    case '7':
                        text += 'Sunday';
                        break;
                    default:
                        break;
                }
            }
            return text;
        });

        s.init = function (data) {
            s.id(data.id);
            s.name(data.name);
            s.sessionNumber(data.sessionNumber);
            s.sessionType(data.sessionType);
            s.startDate(data.startDateText);
            s.endDate(data.endDateText);
            s.time(data.timeText);
            s.salesStartDate(data.salesStartDateText);
            s.salesEndDate(data.salesEndDateText);
            s.capacity(data.capacity);
            s.active(data.active ? 'true' : 'false');

            s.newName(data.name);
            s.newSessionNumber(data.sessionNumber);
            s.newSessionType(data.sessionType);
            s.newStartDate(data.startDateText);
            s.newEndDate(data.endDateText);
            s.newTime(data.timeText);
            s.newSalesStartDate(data.salesStartDateText);
            s.newSalesEndDate(data.salesEndDateText);
            s.newCapacity(data.capacity);
            s.newActive(data.active ? 'true' : 'false');

            s.parseEffectiveDays(data.effectiveDays, s.effectiveDays);
            s.parseEffectiveDays(data.effectiveDays, s.newEffectiveDays);

            s.fromDateField.value(s.startDate());
            s.endDateField.value(s.endDate());
            s.timeField.value(s.time());
            s.salesStartField.value(s.salesStartDate());
            s.salesEndField.value(s.salesEndDate());

            s.timestamp(data.timestamp);
        };

        s.sessionTypeText = ko.computed(function () {
            switch (Number(s.sessionType())) {
                case 0:
                    return "Date";
                case 1:
                    return "Normal";
                default:
                    return "-";
            }
        });

        s.isActive = ko.computed(function () {
            return s.active() == "true";
        });

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.headerText = ko.computed(function () {
            if (s.id() < 1) {
                return "New Session";
            } else {
                return s.name();
            }
        });

        s.doValidateMainInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Session name is required and must not exceed 300 characters.</li>";
            }

            if ((nvx.isEmpty(s.newSessionNumber()) && (s.newSessionNumber() != 0))
                || !nvx.isNumber(s.newSessionNumber())
                || !nvx.isInteger(Number(s.newSessionNumber()))) {
                messsage += "<li>Session number is required and must be a number.</li>";
            }

            if (nvx.isEmpty(s.fromDateField.value())
                || nvx.isEmpty(s.endDateField.value())) {
                messsage += "<li>Both from date and end dates are required and must be valid dates.</li>";
            } else if (s.fromDateField.value() > s.endDateField.value()) {
                messsage += "<li>From date must not be later than end date.</li>";
            } else {
                s.newStartDate($("#from-date-field").val());
                s.newEndDate($("#end-date-field").val());
            }

            if (nvx.isEmpty(s.timeField.value())) {
                messsage += "<li>Time is required and must be a valid time.</li>";
            } else {
                s.newTime($("#time-field").val());
            }

            if (nvx.isEmpty(s.salesStartField.value())
                || nvx.isEmpty(s.salesEndField.value())) {
                messsage += "<li>Both from start and end of sales period are required and must be valid dates.</li>";
            } else if (s.salesStartField.value() > s.salesEndField.value()) {
                messsage += "<li>Start of sales period must not be later than end of sales period.</li>";
            } else {
                s.newSalesStartDate($("#sales-start-field").val());
                s.newSalesEndDate($("#sales-end-field").val());
            }

            if (nvx.isEmpty(s.newCapacity()) || !nvx.isNumber(s.newCapacity())
                || !nvx.isInteger(Number(s.newCapacity()))
                || (Number(s.newCapacity()) < 1)) {
                messsage += "<li>Capacity is required and must be a number.</li>";
            }

            if (messsage != '') {
                s.mainInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.mainInfoHasError(true);
                return false;
            }
            s.mainInfoMessage('');
            s.mainInfoHasError(false);
            return true;
        };

        s.doSaveMainInfo = function () {
            if (s.doValidateMainInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/schedule/save-session',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newSessionNumber: s.newSessionNumber(),
                            newSessionType: s.newSessionType(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate(),
                            newTime: s.newTime(),
                            newSalesStartDate: s.newSalesStartDate(),
                            newSalesEndDate: s.newSalesEndDate(),
                            newCapacity: s.newCapacity(),
                            newActive: s.newActive() == 'true' ? true
                                : false,
                            timestamp: s.timestamp(),
                            newEffectiveDays: s
                                .getEffectiveDaysString(s.newEffectiveDays)
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                if (s.isNew()) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Saved Session',
                                        'The session has been saved successfully. Refreshing the page.',
                                        function () {
                                            window.location
                                                .replace('session-view.html?id='
                                                + data.data.newId);
                                        });
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Saved Session',
                                        'The session has been saved successfully.');
                                    s.name(s.newName());
                                    s.sessionNumber(s.newSessionNumber());
                                    s.sessionType(s.newSessionType());
                                    s.startDate(s.newStartDate());
                                    s.endDate(s.newEndDate());
                                    s.time(s.newTime());
                                    s.salesStartDate(s.newSalesStartDate());
                                    s.salesEndDate(s.newSalesEndDate());
                                    s.capacity(s.newCapacity());
                                    s.active(s.newActive());
                                    s.effectiveDays(s.newEffectiveDays());

                                    s.timestamp(data.timestamp);
                                    s.doEditMainInfo();
                                }
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelMainInfo = function () {
            if (s.isNew()) {
                window.location.assign('sessions.html');
            } else {
                s.doEditMainInfo();
            }
        };

        s.doEditMainInfo = function () {
            if (s.mainInfoEditFlag()) {
                s.mainInfoMessage('');
                s.mainInfoHasError(false);

                s.newSessionType(s.sessionType());

                s.mainInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newSessionNumber(s.sessionNumber());
                s.newStartDate(s.startDate());
                s.newEndDate(s.endDate());
                s.newTime(s.time());
                s.newSalesStartDate(s.salesStartDate());
                s.newSalesEndDate(s.salesEndDate());
                s.newCapacity(s.capacity());
                s.newActive(s.active());
                s.newEffectiveDays(s.effectiveDays());

                s.fromDateField.value(s.startDate());
                s.endDateField.value(s.endDate());
                s.timeField.value(s.time());
                s.salesStartField.value(s.salesStartDate());
                s.salesEndField.value(s.salesEndDate());

                s.mainInfoEditFlag(true);
            }
        };

        s.isEditingMainInfo = ko.computed(function () {
            return s.mainInfoEditFlag() || s.isNew();
        });

        s.doRemovePackage = function () {
            var idList = '';
            $("input.remove-packages-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("package-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Packages...',
                    'No packages selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Packages...',
                'Are you sure you want to remove the selected packages?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/schedule/remove-packages',
                            data: {
                                idList: idList,
                                parentId: mflg.sessionModel
                                    .id(),
                                timestamp: mflg.sessionModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Packages',
                                        'The selected packages have been successfully removed.');
                                    mflg.sessionModel
                                        .timestamp(data.timestamp);
                                    mflg.packageGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doAddPackage = function () {
            mflg.packageDialog.open();
        };

        s.doRemoveBlockOut = function () {
            var idList = '';
            $("input.blockouts-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("blockout-id");
                idList += ',';
            });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Blockout Dates...',
                    'No blockout dates selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Blockout Dates...',
                'Are you sure you want to remove the selected blockout dates?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/schedule/remove-blockouts',
                            data: {
                                idList: idList,
                                parentId: mflg.sessionModel
                                    .id(),
                                timestamp: mflg.sessionModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Blockout Dates',
                                        'The selected blockout dates have been successfully removed.');
                                    mflg.sessionModel
                                        .timestamp(data.timestamp);
                                    mflg.blockOutGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doAddBlockOut = function () {
            mflg.blockOutDialog.open();
        }

    };

    mflg.BlockOutModel = function () {
        var s = this;

        s.startDateField = $("#blockout-start-date").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#blockout-end-date").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        s.id = ko.observable(0);
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');

        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');

        s.blockOutHasError = ko.observable(false);
        s.blockOutMessage = ko.observable('');

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.init = function (data) {
            if (data) {
                s.id(data.id);
                s.startDate(data.startDateText);
                s.endDate(data.endDateText);

                s.newStartDate(data.startDateText);
                s.newEndDate(data.endDateText);
            } else {
                s.id(0);
                s.startDate('');
                s.endDate('');

                s.newStartDate('');
                s.newEndDate('');
            }
            s.blockOutHasError(false);
            s.blockOutMessage('');

            s.startDateField.value(s.startDate());
            s.endDateField.value(s.endDate());
        };

        s.doValidateBlockOut = function () {
            var messsage = '';

            if (nvx.isEmpty(s.startDateField.value())
                || nvx.isEmpty(s.endDateField.value())) {
                messsage += "<li>Both start and end dates are required and must be valid dates.</li>";
            } else if (s.startDateField.value() > s.endDateField.value()) {
                messsage += "<li>Start date must not be later than end date.</li>";
            } else {
                s.newStartDate($("#blockout-start-date").val());
                s.newEndDate($("#blockout-end-date").val());
            }

            if (messsage != '') {
                s.blockOutMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.blockOutHasError(true);
                return false;
            }
            s.blockOutMessage('');
            s.blockOutHasError(false);
            return true;
        };

        s.doSaveBlockOut = function () {
            if (s.doValidateBlockOut()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/schedule/save-blockout',
                        data: {
                            id: s.id(),
                            parentId: mflg.sessionModel.id(),
                            newStartDate: s.newStartDate(),
                            newEndDate: s.newEndDate(),
                            timestamp: mflg.sessionModel.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Saved Blockout Date',
                                    'The blockout date has been saved successfully.');
                                mflg.sessionModel.timestamp(data.timestamp);
                                mflg.blockOutDialog.close(true);
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelBlockOut = function () {
            mflg.blockOutDialog.close(false);
        }
    };

    mflg.BlockOutGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/schedule/get-blockouts?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#blockouts-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    status: {
                                        type: 'string'
                                    },
                                    startDateText: {
                                        type: 'string'
                                    },
                                    endDateText: {
                                        type: 'string'
                                    }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="blockouts-checkbox" blockout-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'name',
                            title: 'Blockout Dates'
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        },
                        {
                            title: 'Details',
                            width: 100,
                            template: '<div style="text-align:center;"><a class="btn btn-primary" onclick="mflg.blockOutDialog.open(\'${uid}\')">Details</a></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: false,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.BlockOutDialog = function (parentGrid, dialogModel) {
        var s = this;

        s.grid = parentGrid;
        s.model = dialogModel;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#blockout-modal').kendoWindow({
                title: 'Blockout Dates',
                modal: true,
                resizable: false,
                width: '420px',
                close: s.closeDialog
            }).data('kendoWindow');
        };

        s.open = function (rowUid) {
            if (rowUid) {
                var modelData = s.grid.kGrid.dataSource.getByUid(rowUid);
                s.model.init(modelData);
            } else {
                s.model.init();
            }

            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.grid.refresh();
            }
            s.kWin.close();
        };
    };

    mflg.PackageGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/schedule/get-packages?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.refresh = function () {
            s.kGrid.dataSource.read();
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#packages-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    displayOrder: {
                                        type: 'int'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        sort: {
                            field: 'displayOrder',
                            dir: 'asc'
                        }
                    },
                    columns: [
                        {
                            title: 'Remove',
                            template: '<div style="text-align:center;"><input type="checkbox" class="remove-packages-checkbox" package-id="${id}" /></div>',
                            width: 60,
                            sortable: false
                        },
                        {
                            field: 'displayOrder',
                            title: 'Order',
                            width: 60
                        },
                        {
                            field: 'name',
                            title: 'Name'
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        },
                        {
                            title: 'Details',
                            width: 100,
                            template: '<div style="text-align:center;"><a class="btn btn-primary" href="package-view.html?id=${id}">Details</a></div>',
                            sortable: false
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: false
                }).data('kendoGrid');
        }
    };

    mflg.SelectPackageGrid = function (parentId) {
        var s = this;
        s.parentId = parentId;
        s.kGrid = {};

        s.BASE_URL = '/.sky-dining-admin/schedule/get-select-packages?&parentId='
            + (s.parentId ? s.parentId : 0);
        s.theUrl = s.BASE_URL;
        s.refresh = function (filters) {
            s.theUrl = s.BASE_URL;
            if (filters) {
                s.theUrl += '?nameFilter=' + filters.nameFilter
                    + '&activeFilter=' + filters.activeFilter;
            }
            s.kGrid.dataSource.options.transport.read.url = s.theUrl;
            s.kGrid.dataSource.page(1);
        };

        s.onDataBound = function (e) {
            var grid = e.sender;
            var gridHasData = grid.dataSource.data().length > 0;
            if (!gridHasData) {
                var columnCount = $(grid.thead.get(0)).children("tr").first()
                    .children("th").length;
                $(grid.tbody.get(0))
                    .append(
                    kendo
                        .format(
                        "<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>",
                        columnCount));
            }
        };

        init();
        function init() {
            s.kGrid = $('#select-packages-grid')
                .kendoGrid(
                {
                    dataSource: {
                        transport: {
                            read: {
                                url: s.BASE_URL,
                                cache: false
                            }
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: 'id',
                                fields: {
                                    name: {
                                        type: 'string'
                                    },
                                    status: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        pageSize: mflg.pageSize,
                        serverSorting: true,
                        serverPaging: true
                    },
                    columns: [
                        {
                            title: 'Select',
                            template: '<div style="text-align:center;"><input type="checkbox" class="add-packages-checkbox" package-id="${id}" /></div>',
                            width: 60
                        },
                        {
                            field: 'name',
                            title: 'Name'
                        },
                        {
                            field: 'status',
                            title: 'Status',
                            width: 100,
                            template: '<div style="text-align:center;"><span style="color: #if (status != "Active") {#\\#E00000#} else {#\\#1DA20B#}#;">${status}</span></div>'
                        }],
                    dataBound: s.onDataBound,
                    sortable: true,
                    pageable: true
                }).data('kendoGrid');
        }
    };

    mflg.PackageDialog = function (parentGrid, selectGrid) {
        var s = this;

        s.parentGrid = parentGrid;
        s.selectGrid = selectGrid;

        s.kWin = {};
        s.init = function () {
            s.kWin = $('#package-modal').kendoWindow({
                title: 'Select Packages',
                modal: true,
                resizable: false,
                width: '740px',
                close: s.closeDialog
            }).data('kendoWindow');

            $('#add-selected-packages').click(function () {
                s.addSelected();
            });

            $('#cancel-package-select').click(function () {
                s.close();
            });
        };

        s.addSelected = function () {
            var idList = '';
            $("input.add-packages-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("package-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Adding Packages...',
                    'No packages selected.');
                return;
            }

            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/schedule/add-packages',
                    data: {
                        idList: idList,
                        parentId: mflg.sessionModel.id(),
                        timestamp: mflg.sessionModel.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog('Added Packages',
                                'The selected packages dates have been successfully added.');
                            mflg.sessionModel.timestamp(data.timestamp);
                            s.close(true);
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.open = function (rowUid) {
            s.selectGrid.refresh();
            s.kWin.center().open();
        };

        s.close = function (refreshGrid) {
            if (refreshGrid) {
                s.parentGrid.refresh();
            }
            s.kWin.close();
        };
    }

})(window.mflg = window.mflg || {}, jQuery);