$(document).ready(
    function () {
        mflg.pageSize = 10;
        mflg.initHackKendoGridSortForStruts();

        mflg.packageModel = new mflg.PackageModel();
        mflg.getModelData();

        ko.applyBindings(mflg.packageModel, document
            .getElementById("package-view"));

        mflg.tncGrid = new mflg.TncGrid(mflg.getParameterByName("id"));

        mflg.tncModel = new mflg.TncModel();
        mflg.tncModel.init();

        ko.applyBindings(mflg.tncModel, document
            .getElementById("tnc-modal"));

        mflg.tncDialog = new mflg.TncDialog(mflg.tncGrid, mflg.tncModel);
        mflg.tncDialog.init();

        mflg.themeGrid = new mflg.ThemeGrid(mflg.getParameterByName("id"));

        mflg.themeModel = new mflg.ThemeModel();
        mflg.themeModel.init();

        ko.applyBindings(mflg.themeModel, document
            .getElementById("theme-modal"));

        mflg.themeDialog = new mflg.ThemeDialog(mflg.themeGrid,
            mflg.themeModel);
        mflg.themeDialog.init();

        mflg.menuGrid = new mflg.MenuGrid(mflg.getParameterByName("id"));
        mflg.selectMenuGrid = new mflg.SelectMenuGrid(mflg.getParameterByName("id"));

        mflg.menuDialog = new mflg.MenuDialog(mflg.menuGrid,
            mflg.selectMenuGrid);
        mflg.menuDialog.init();

        mflg.topUpGrid = new mflg.TopUpGrid(mflg.getParameterByName("id"));
        mflg.selectTopUpGrid = new mflg.SelectTopUpGrid(mflg.getParameterByName("id"));

        mflg.topUpDialog = new mflg.TopUpDialog(mflg.topUpGrid,
            mflg.selectTopUpGrid);
        mflg.topUpDialog.init();

        mflg.promoGrid = new mflg.PromoGrid(mflg.getParameterByName("id"));

        mflg.promoModel = new mflg.PromoModel();
        mflg.promoModel.init();

        ko.applyBindings(mflg.promoModel, document
            .getElementById("promo-modal"));

        mflg.promoDialog = new mflg.PromoDialog(mflg.promoGrid,
            mflg.promoModel);
        mflg.promoDialog.init();

        mflg.promoMenuGrid = new mflg.PromoMenuGrid(0);
        mflg.selectPromoMenuGrid = new mflg.SelectPromoMenuGrid(0);

        mflg.promoMenuDialog = new mflg.PromoMenuDialog(mflg.promoMenuGrid,
            mflg.selectPromoMenuGrid);
        mflg.promoMenuDialog.init();

        mflg.merchGrid = new mflg.MerchGrid('merch-grid',
            'merch-grid-radio', mflg.promoModel);

        mflg.promoCodeModel = new mflg.PromoCodeModel();
        mflg.promoCodeModel.init();

        ko.applyBindings(mflg.promoCodeModel, document
            .getElementById("promo-code-modal"));

        mflg.promoCodeGrid = new mflg.PromoCodeGrid(0);

        mflg.promoCodeDialog = new mflg.PromoCodeDialog(mflg.promoCodeGrid,
            mflg.promoCodeModel);
        mflg.promoCodeDialog.init();

        $("body").scrollspy();
    });

(function (mflg, $) {
    mflg.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    mflg.getModelData = function () {
        var modelId = mflg.getParameterByName("id");

        mflg.spinner.start();
        $
            .ajax({
                url: '/.sky-dining-admin/package/view-package',
                data: {
                    id: modelId,
                },
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        mflg.packageModel.init(data.data);
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });
    };

    mflg.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    mflg.GenericErrorMsg = mflg.ErrorSpan
        .replace(
        'THE_MSG',
        'Unable to process your request. Please try again in a few moments or refresh the page.');

    mflg.packageModel = {};

    mflg.PackageModel = function () {
        var s = this;

        s.timestamp = ko.observable('');

        s.earlyBirdField = $("#early-bird-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.startDateField = $("#start-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");
        s.endDateField = $("#end-date-field").kendoDatePicker({
            format: mflg.DATE_FORMAT
        }).data("kendoDatePicker");

        tinymce
            .init({
                selector: "#description-field",
                plugins: ["advlist lists link charmap anchor",
                    "visualblocks code", "table contextmenu paste"],
                menubar: false,
                statusbar: false,
                toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
            });
        tinymce
            .init({
                selector: "#notes-field",
                plugins: ["advlist lists link charmap anchor",
                    "visualblocks code", "table contextmenu paste"],
                menubar: false,
                statusbar: false,
                toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link"
            });

        s.imageSelected = ko.observable(false);
        $('#image-file').kendoUpload({
            multiple: false,
            localization: {
                select: 'Select image to upload...'
            },
            select: function () {
                s.imageSelected(true);
            },
            remove: function () {
                s.imageSelected(false);
            },
        });

        s.packageInfoEditFlag = ko.observable(false);
        s.packageInfoHasError = ko.observable(false);
        s.packageInfoMessage = ko.observable('');

        s.displayInfoEditFlag = ko.observable(false);
        s.displayInfoHasError = ko.observable(false);
        s.displayInfoMessage = ko.observable('');

        s.notesAndTncEditFlag = ko.observable(false);
        s.notesAndTncHasError = ko.observable(false);
        s.notesAndTncMessage = ko.observable('');

        s.tncContent = ko.observable('');
        s.newTncContent = ko.observable('');

        s.id = ko.observable(0);
        s.name = ko.observable('');
        s.earlyBirdEndDate = ko.observable('');
        s.startDate = ko.observable('');
        s.endDate = ko.observable('');
        s.salesCutOffDays = ko.observable(0);
        s.description = ko.observable('');
        s.link = ko.observable('');
        s.displayOrder = ko.observable();
        s.minPax = ko.observable(0);
        s.maxPax = ko.observable(0);
        s.extraPax = ko.observable(0);
        s.imageHash = ko.observable('');
        s.notes = ko.observable('');
        s.selectedTncId = ko.observable(0);

        s.newName = ko.observable('');
        s.newEarlyBirdEndDate = ko.observable('');
        s.newStartDate = ko.observable('');
        s.newEndDate = ko.observable('');
        s.newSalesCutOffDays = ko.observable(0);
        s.newDescription = ko.observable('');
        s.newLink = ko.observable('');
        s.newDisplayOrder = ko.observable(0);
        s.newMinPax = ko.observable(0);
        s.newMaxPax = ko.observable(0);
        s.newExtraPax = ko.observable(0);
        s.newImageHash = ko.observable('');
        s.newNotes = ko.observable('');
        s.newSelectedTncId = ko.observable(0);

        s.init = function (data) {
            s.id(data.id);
            s.name(data.name);
            s.earlyBirdEndDate(data.earlyBirdEndDateText);
            s.startDate(data.startDateText);
            s.endDate(data.endDateText);
            s.salesCutOffDays(data.salesCutOffDays);
            s.description(data.description);
            s.link(data.link);
            s.displayOrder(data.displayOrder);
            s.minPax(data.minPax);
            s.maxPax(data.maxPax);
            s.extraPax(data.extraPax);
            s.imageHash(data.imageHash);
            s.notes(data.notes);
            s.selectedTncId(data.selectedTncId);
            s.tncContent(data.tncContent);

            s.newName(data.name);
            s.newEarlyBirdEndDate(data.earlyBirdEndDateText);
            s.newStartDate(data.startDateText);
            s.newEndDate(data.endDateText);
            s.newSalesCutOffDays(data.salesCutOffDays);
            s.newDescription(data.description);
            s.newLink(data.link);
            s.newDisplayOrder(data.displayOrder);
            s.newMinPax(data.minPax);
            s.newMaxPax(data.maxPax);
            s.newExtraPax(data.extraPax);
            s.newImageHash(data.imageHash);
            s.newNotes(data.notes);
            s.newSelectedTncId(data.selectedTncId);
            s.newTncContent(data.tncContent);

            s.earlyBirdField.value(s.earlyBirdEndDate());
            s.startDateField.value(s.startDate());
            s.endDateField.value(s.endDate());

            s.timestamp(data.timestamp);
        };

        s.isNew = ko.computed(function () {
            return s.id() < 1;
        });

        s.headerText = ko.computed(function () {
            if (s.id() < 1) {
                return "New Package";
            } else {
                return s.name();
            }
        });

        s.doSaveNewPackage = function () {
            var packageInfoValid = s.doValidatePackageInfo();
            var displayInfoValid = s.doValidateDisplayInfo();

            if (packageInfoValid && displayInfoValid) {
                if (s.newSalesCutOffDays() == 0) {
                    mflg
                        .ShowConfirmDialog(
                        'Warning...',
                        'Sales cut-off days is set to 0. Reservations can be made on the same date before 12:00 pm.',
                        function () {
                            s.executeNewPackage();
                        });
                } else {
                    s.executeNewPackage();
                }
            }
        };

        s.executeNewPackage = function () {
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/package/save-package',
                    data: {
                        id: s.id(),
                        newName: s.newName(),
                        newEarlyBirdEndDate: s.newEarlyBirdEndDate(),
                        newStartDate: s.newStartDate(),
                        newEndDate: s.newEndDate(),
                        newSalesCutOffDays: s.newSalesCutOffDays(),
                        newDescription: s.newDescription(),
                        newLink: s.newLink(),
                        newDisplayOrder: s.newDisplayOrder(),
                        newMinPax: s.newMinPax(),
                        newMaxPax: s.newMaxPax(),
                        newImageHash: '',
                        newNotes: '',
                        newSelectedTncId: 0,
                        timestamp: s.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog(
                                'Created Package',
                                'The package has been created successfully. Refreshing the page.',
                                function () {
                                    window.location
                                        .replace('package-view.html?id='
                                        + data.data.newId);
                                });
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.doValidatePackageInfo = function () {
            var messsage = '';

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Package name is required and must not exceed 300 characters.</li>";
            }

            if (nvx.isEmpty(s.earlyBirdField.value())) {
                messsage += "<li>End of early bird period is required and must be a valid date.</li>";
            } else {
                s.newEarlyBirdEndDate($("#early-bird-field").val());
            }

            if (nvx.isEmpty(s.startDateField.value())
                || nvx.isEmpty(s.endDateField.value())) {
                messsage += "<li>Both start date and end dates are required and must be valid dates.</li>";
            } else if (s.startDateField.value() > s.endDateField.value()) {
                messsage += "<li>Start date must not be later than end date.</li>";
            } else {
                s.newStartDate($("#start-date-field").val());
                s.newEndDate($("#end-date-field").val());
            }

            if (nvx.isEmpty(s.newSalesCutOffDays())
                || !nvx.isNumber(s.newSalesCutOffDays())
                || !nvx.isInteger(Number(s.newSalesCutOffDays()))
                || ((Number(s.newSalesCutOffDays()) < 0))) {
                if ($("#sales-cutoff-field").val() != '0') {
                    messsage += "<li>Sales cut-off before event date is required and must be a number.</li>";
                }
            }

            if (messsage != '') {
                s
                    .packageInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.packageInfoHasError(true);
                return false;
            }
            s.packageInfoMessage('');
            s.packageInfoHasError(false);
            return true;
        };

        s.doSavePackageInfo = function () {
            if (s.isNew()) {
                s.doSaveNewPackage();
            } else if (s.doValidatePackageInfo()) {
                if (s.newSalesCutOffDays() == 0) {
                    mflg
                        .ShowConfirmDialog(
                        'Warning...',
                        'Sales cut-off days is set to 0. Reservations can be made on the same date before 12:00 pm.',
                        function () {
                            s.executeSavePackageInfo();
                        });
                } else {
                    s.executeSavePackageInfo();
                }
            }
        };

        s.executeSavePackageInfo = function () {
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.sky-dining-admin/package/save-package-info',
                    data: {
                        id: s.id(),
                        newName: s.newName(),
                        newEarlyBirdEndDate: s.newEarlyBirdEndDate(),
                        newStartDate: s.newStartDate(),
                        newEndDate: s.newEndDate(),
                        newSalesCutOffDays: s.newSalesCutOffDays(),
                        timestamp: s.timestamp()
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            mflg
                                .ShowInfoDialog(
                                'Updated Package Information',
                                'The package information has been updated successfully.');
                            s.name(s.newName());
                            s.earlyBirdEndDate(s.newEarlyBirdEndDate());
                            s.startDate(s.newStartDate());
                            s.endDate(s.newEndDate());
                            s.salesCutOffDays(s.newSalesCutOffDays());

                            s.timestamp(data.timestamp);
                            s.doEditPackageInfo();
                        } else {
                            mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                .replace('THE_MSG', data.message));
                        }
                    },
                    error: function () {
                        mflg.ShowInfoDialog('Error', mflg.GenericErrorMsg);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.doCancelPackageInfo = function () {
            if (s.isNew()) {
                window.location.assign('packages.html');
            } else {
                s.doEditPackageInfo();
            }
        };

        s.doEditPackageInfo = function () {
            if (s.packageInfoEditFlag()) {
                s.packageInfoMessage('');
                s.packageInfoHasError(false);

                s.packageInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newEarlyBirdEndDate(s.earlyBirdEndDate());
                s.newStartDate(s.startDate());
                s.newEndDate(s.endDate());
                s.newSalesCutOffDays(s.salesCutOffDays());

                s.earlyBirdField.value(s.earlyBirdEndDate());
                s.startDateField.value(s.startDate());
                s.endDateField.value(s.endDate());

                s.packageInfoEditFlag(true);
            }
        };

        s.isEditingPackageInfo = ko.computed(function () {
            return s.packageInfoEditFlag() || s.isNew();
        });

        s.doValidateDisplayInfo = function () {
            var messsage = '';
            var minPaxValid = false;
            var extraPaxValid = false;

            if (nvx.isEmpty(s.newName())
                || !nvx.isLengthWithin(s.newName(), 1, 300, true)) {
                messsage += "<li>Name is required and must not exceed 300 characters.</li>";
            }

            s.newDescription($('#description-field').tinymce().getContent());
            if (nvx.isEmpty(s.newDescription())
                || !nvx.isLengthWithin(s.newDescription(), 1, 4000, true)) {
                messsage += '<li>Description is required and must not exceed 4000 characters.</li>';
            }

            if (nvx.isNotEmpty(s.newLink())
                && s.newLink().lastIndexOf('http://', 0) !== 0) {
                s.newLink('http://' + s.newLink());
            }

            if (nvx.isEmpty(s.newMinPax()) || !nvx.isNumber(s.newMinPax())
                || !nvx.isInteger(Number(s.newMinPax()))
                || ((Number(s.newMinPax()) < 1))) {
                messsage += "<li>Pax per booking is required and must be a number.</li>";
            } else {
                minPaxValid = true;
            }

            if ((nvx.isEmpty(s.newExtraPax()) && (s.newExtraPax() != 0))
                || !nvx.isNumber(s.newExtraPax())
                || !nvx.isInteger(Number(s.newExtraPax()))
                || ((Number(s.newExtraPax()) < 0))) {
                messsage += "<li>Additional pax allowed is required and must be a number.</li>";
            } else {
                extraPaxValid = true;
            }

            if (minPaxValid && extraPaxValid) {
                s.newMaxPax(Number(s.newMinPax()) + Number(s.newExtraPax()));
            }

            if (messsage != '') {
                s
                    .displayInfoMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.displayInfoHasError(true);
                return false;
            }
            s.displayInfoMessage('');
            s.displayInfoHasError(false);
            return true;
        };

        s.doSaveDisplayInfo = function () {
            if (s.isNew()) {
                s.doSaveNewPackage();
            } else if (s.doValidateDisplayInfo()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/package/save-display-info',
                        data: {
                            id: s.id(),
                            newName: s.newName(),
                            newDescription: s.newDescription(),
                            newLink: s.newLink(),
                            newDisplayOrder: s.newDisplayOrder(),
                            newMinPax: s.newMinPax(),
                            newMaxPax: s.newMaxPax(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Updated Package Information',
                                    'The package information has been updated successfully.');
                                s.name(s.newName());
                                s.description(s.newDescription());
                                s.link(s.newLink());
                                s.displayOrder(s.newDisplayOrder());
                                s.minPax(s.newMinPax());
                                s.maxPax(s.newMaxPax());
                                s.extraPax(s.newExtraPax());

                                s.timestamp(data.timestamp);
                                s.doEditDisplayInfo();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelDisplayInfo = function () {
            if (s.isNew()) {
                window.location.assign('/.sky-dining-admin/packages');
            } else {
                s.doEditDisplayInfo();
            }
        };

        s.doEditDisplayInfo = function () {
            if (s.displayInfoEditFlag()) {
                s.displayInfoMessage('');
                s.displayInfoHasError(false);

                s.displayInfoEditFlag(false);
            } else {
                s.newName(s.name());
                s.newDescription(s.description());
                s.newLink(s.link());
                s.newDisplayOrder(s.displayOrder());
                s.newMinPax(s.minPax());
                s.newMaxPax(s.maxPax());
                s.newExtraPax(s.extraPax());

                $('#description-field').tinymce().setContent(s.description());

                s.displayInfoEditFlag(true);
            }
        };

        s.isEditingDisplayInfo = ko.computed(function () {
            return s.displayInfoEditFlag() || s.isNew();
        });

        $("form#display-image-form").submit(function () {
            mflg.spinner.start();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: '/.sky-dining-admin/package/save-display-image',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                cache: false,
                success: function (data) {
                    if (data.success) {
                        mflg.ShowInfoDialog('Uploaded Image',
                            'Display image has been successfully updated.');
                        s.timestamp(data.data.timestampText);
                        s.imageHash(data.data.imageHash);

                        s.imageSelected(false);
                        $("#image-sect :file").not(
                            document.getElementById("image-file")).remove();
                        $("#image-sect .k-upload-files").remove();
                        $("#image-sect .k-upload-status").remove();
                        $("#image-sect .k-upload.k-header").addClass(
                            "k-upload-empty");
                        $("#image-sect .k-upload-button").removeClass(
                            "k-state-focused");
                    } else {
                        mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                            .replace('THE_MSG', data.message));
                    }
                },
                error: function () {
                    mflg.ShowInfoDialog('Error',
                        mflg.GenericErrorMsg);
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });

            return false;
        });

        s.doValidateNotesAndTnc = function () {
            var messsage = '';

            s.newNotes($('#notes-field').tinymce().getContent());
            if (!nvx.isLengthWithin(s.newDescription(), 0, 4000, true)) {
                messsage += '<li>Notes must not exceed 4000 characters.</li>';
            }

            if (messsage != '') {
                s
                    .notesAndTncMessage('Please correct the following errors: <ul>'
                    + messsage + "</ul>");
                s.notesAndTncHasError(true);
                return false;
            }
            s.notesAndTncMessage('');
            s.notesAndTncHasError(false);
            return true;
        };

        s.doSaveNotesAndTnc = function () {
            if (s.doValidateNotesAndTnc()) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.sky-dining-admin/package/save-notes-tnc',
                        data: {
                            id: s.id(),
                            newNotes: s.newNotes(),
                            newSelectedTncId: s.newSelectedTncId(),
                            timestamp: s.timestamp()
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                mflg
                                    .ShowInfoDialog(
                                    'Updated Notes and T&Cs',
                                    'The notes and T&Cs have been updated successfully.');
                                s.notes(s.newNotes());
                                s.selectedTncId(s.newSelectedTncId());
                                s.tncContent(s.newTncContent());

                                s.timestamp(data.timestamp);
                                s.doEditNotesAndTnc();
                            } else {
                                mflg.ShowInfoDialog('Error', mflg.ErrorSpan
                                    .replace('THE_MSG', data.message));
                            }
                        },
                        error: function () {
                            mflg.ShowInfoDialog('Error',
                                mflg.GenericErrorMsg);
                        },
                        complete: function () {
                            mflg.spinner.stop();
                        }
                    });
            }
        };

        s.doCancelNotesAndTnc = function () {
            s.doEditNotesAndTnc();
        };

        s.doEditNotesAndTnc = function () {
            if (s.notesAndTncEditFlag()) {
                s.notesAndTncMessage('');
                s.notesAndTncHasError(false);

                s.notesAndTncEditFlag(false);
            } else {
                s.newNotes(s.notes());
                s.newSelectedTncId(s.selectedTncId());
                s.newTncContent(s.tncContent());

                $('#notes-field').tinymce().setContent(s.notes());

                s.notesAndTncEditFlag(true);
            }
        };

        s.isEditingNotesAndTnc = ko.computed(function () {
            return s.notesAndTncEditFlag();
        });

        s.doAddTnc = function () {
            mflg.tncDialog.open();
        };

        s.doRemoveTnc = function () {
            s.newSelectedTncId(0);
            s.newTncContent('');
        };

        s.doSelectTnc = function (rowUid) {
            var tncData = mflg.tncGrid.kGrid.dataSource.getByUid(rowUid);

            s.newSelectedTncId(tncData.id);
            s.newTncContent(tncData.tncContent);
        };

        s.doAddTheme = function () {
            mflg.themeDialog.open();
        };

        s.doRemoveTheme = function () {
            var idList = '';
            $("input.themes-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("theme-id");
                idList += ',';
            });

            if (idList == '') {
                mflg
                    .ShowInfoDialog('Removing Themes...',
                    'No themes selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Themes...',
                'Are you sure you want to remove the selected themes?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-themes',
                            data: {
                                idList: idList,
                                parentId: mflg.packageModel
                                    .id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Themes',
                                        'The selected themes have been successfully removed.');
                                    mflg.packageModel
                                        .timestamp(data.timestamp);
                                    mflg.themeGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doAddMenu = function () {
            mflg.menuDialog.open();
        };

        s.doRemoveMenu = function () {
            var idList = '';
            $("input.remove-menus-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("menu-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Menus...', 'No menus selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Menus...',
                'Are you sure you want to remove the selected menus?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-menus',
                            data: {
                                idList: idList,
                                parentId: mflg.packageModel
                                    .id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Menus',
                                        'The selected menus have been successfully removed.');
                                    mflg.packageModel
                                        .timestamp(data.timestamp);
                                    mflg.menuGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doAddTopUp = function () {
            mflg.topUpDialog.open();
        };

        s.doRemoveTopUp = function () {
            var idList = '';
            $("input.remove-topups-checkbox:checked('checked')").each(
                function () {
                    idList += this.getAttribute("topup-id");
                    idList += ',';
                });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Top Ups...',
                    'No top ups selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Top Ups...',
                'Are you sure you want to remove the selected top ups?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-topups',
                            data: {
                                idList: idList,
                                parentId: mflg.packageModel
                                    .id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Top Ups',
                                        'The selected top ups have been successfully removed.');
                                    mflg.packageModel
                                        .timestamp(data.timestamp);
                                    mflg.topUpGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        };

        s.doAddPromo = function () {
            mflg.promoDialog.open();
        };

        s.doRemovePromo = function () {
            var idList = '';
            $("input.promos-checkbox:checked('checked')").each(function () {
                idList += this.getAttribute("promo-id");
                idList += ',';
            });

            if (idList == '') {
                mflg.ShowInfoDialog('Removing Promotions...',
                    'No promotions selected.');
                return;
            }

            mflg
                .ShowConfirmDialog(
                'Removing Promotions...',
                'Are you sure you want to remove the selected promotions?',
                function () {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url: '/.sky-dining-admin/package/remove-promos',
                            data: {
                                idList: idList,
                                parentId: mflg.packageModel
                                    .id(),
                                timestamp: mflg.packageModel
                                    .timestamp()
                            },
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    mflg
                                        .ShowInfoDialog(
                                        'Removed Promotions',
                                        'The selected promotions have been successfully removed.');
                                    mflg.packageModel
                                        .timestamp(data.timestamp);
                                    mflg.promoGrid.refresh();
                                } else {
                                    mflg
                                        .ShowInfoDialog(
                                        'Error',
                                        mflg.ErrorSpan
                                            .replace(
                                            'THE_MSG',
                                            data.message));
                                }
                            },
                            error: function () {
                                mflg.ShowInfoDialog('Error',
                                    mflg.GenericErrorMsg);
                            },
                            complete: function () {
                                mflg.spinner.stop();
                            }
                        });
                });

        }

    }

})(window.mflg = window.mflg || {}, jQuery);