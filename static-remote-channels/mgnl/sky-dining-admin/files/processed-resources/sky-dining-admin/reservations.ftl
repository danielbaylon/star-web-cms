<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Reservations :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/reservations.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main">
    <div class="span20">
      <div class="page-section" style="width: 80%; margin-left:auto; margin-right:auto;">
        <table style="width:100%;">
          <tr>
            <td rowspan="3" style="text-align:center; width:12%;">
              <i class="icon-search"></i>
              <h4 class="inline-display">Filter</h4>
            </td>
            <td style="width:22%;">
              <div class="row-fluid param-row">
                <span class="param-label">Transaction Start Date</span>

                <div>
                  <input type="text" class="input" name="transStartDate" id="transStartDate"/>
                </div>
              </div>
            </td>
            <td style="width:22%;">
              <div class="row-fluid param-row">
                <span class="param-label">Transaction End Date</span>

                <div>
                  <input type="text" class="input" name="transEndDate" id="transEndDate"/>
                </div>
              </div>
            </td>
            <td style="width:22%;">
              <div class="row-fluid param-row">
                <span class="param-label">Transaction Status</span>

                <div>
                  <select id="tranStatus">
                    <option value="">ALL</option>
                    <option value="SUCCESS">SUCCESS</option>
                    <option value="FAILED">FAILED</option>
                    <option value="VOID_SUCCESS"> VOID_SUCCESS</option>
                    <option value="VOID_FAILED">VOID_FAILED</option>
                    <option value="NA"></option>
                  </select>
                </div>
              </div>
            </td>
            <td style="width:22%;">
              <div class="row-fluid param-row">
                <span class="param-label">&nbsp;</span>

                <div>
                  <a class="btn btn-success" id="add_btn" style="margin-right: 5px">New Reservation</a>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Receipt Number</span>

                <div>
                  <input type="text" class="input" name="recNum" id="recNum"/>
                </div>
              </div>
            </td>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Package Name</span>

                <div>
                  <input type="text" class="input" name="pckName" id="pckName"/>
                </div>
              </div>
            </td>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Date of Visit</span>

                <div>
                  <input type="text" class="input" name="dateOfVisit" id="dateOfVisit"/>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Reservation Type</span>

                <div>
                  <select id="resType">
                    <option value="">ALL</option>
                    <option value="0">Online</option>
                    <option value="1">Admin</option>
                  </select>
                </div>
              </div>
            </td>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Guest Name</span>

                <div>
                  <input type="text" class="input" name="guestName" id="guestName"/>
                </div>
              </div>
            </td>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">Reservation Status</span>

                <div>
                  <select id="resStatus">
                    <option value="">ALL</option>
                    <option value="1">Paid Inside System</option>
                    <option value="3">Paid Outside System</option>
                    <option value="2">Not Paid</option>
                    <option value="0">Cancelled</option>
                  </select>
                </div>
              </div>
            </td>
            <td>
              <div class="row-fluid param-row">
                <span class="param-label">&nbsp;</span>

                <div>
                  <a class="btn btn-primary" id="search_btn" style="margin-right: 5px"><i class="icon-search"></i>
                    Filter</a>
                  <a class="btn" id="reset_btn">Reset</a>
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="span20" id="exportDiv">
      <div style="float:right; margin:10px 10px;">
        Export Type:
        <select id="exportType">
          <option value="pdf" selected="selected">PDF</option>
          <option value="excel">Excel</option>
        </select>
        <a class="btn btn-primary" id="export_btn">Export</a>
      </div>
    </div>
    <div class="span20" style="font-size:85%;">
      <div class="data_grid grid-wrapper" id="mainGrid">
      </div>
    </div>
  </div>
</div>
</body>