<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Reports :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/reports.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main">
    <div class="span20 grid-section">
      <h3>Reports</h3>
      <table class="filter-box">
        <tr>
          <td class="filter-fields">
            <div class="filter-field">
              <div class="row-fluid param-row">
                <span class="param-label">Report Type</span>

                <div>
                  <select class="input" id="report-type-filter">
                    <option value="DailyBooking">Daily Booking</option>
                    <option value="DailyOrder">Daily Order</option>
                    <option value="Revenue">Revenue</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="filter-field">
              <div class="row-fluid param-row">
                <span class="param-label">Date From</span>

                <div><input type="text" class="input" id="start-date-filter"/></div>
              </div>
            </div>
            <div class="filter-field">
              <div class="row-fluid param-row">
                <span class="param-label">Date Until</span>

                <div><input type="text" class="input" id="end-date-filter"/></div>
              </div>
            </div>
          </td>
          <td class="filter-actions">
            <a class="btn btn-primary" id="generate-button">Generate</a>
            <a class="btn" id="reset-button">Reset</a>
          </td>
        </tr>
      </table>
      <div class="data_grid grid-wrapper" id="menus-grid"></div>
    </div>
  </div>
</div>
</body>