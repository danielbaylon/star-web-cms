<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Top Up :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/top-up-view.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main" id="top-up-view">
    <div class="span20">
      <div class="product-title">
        <h2 data-bind="text: headerText">TopUp Name</h2>

        <div class="clearfix"></div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span4">
        <div class="float-menu" data-spy="affix" data-offset-top="40">
          <div class="menu-active-section">
            <h3>Sections</h3>
            <ul id="navbar" class="navbar nav menu-links">
              <li><a href="#top-up-head">Top Up Information</a></li>
              <li data-bind="visible: !isNew()"><a href="#image-head">Display Image</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="span16" style="min-height: 1000px; margin-bottom: 100px;">
        <div class="product-main">
          <div class="product-header" id="top-up-head">
            <h3>TopUp Information</h3>
            <a class="btn btn-primary header-btn" data-bind="visible: !isEditingTopUpInfo(), click: doEditTopUpInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingTopUpInfo, click: doCancelTopUpInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingTopUpInfo, click: doSaveTopUpInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: topUpInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':topUpInfoHasError}, html: topUpInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: name"></span>
                <input class="input-xlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingTopUpInfo, value: newName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Top Up Type</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: typeText"></span>
                <select class="input-medium" data-bind="visible: isEditingTopUpInfo, value: newType">
                  <option value="0">Other</option>
                  <option value="1">Cake</option>
                  <option value="2">Flowers</option>
                  <option value="3">Wine</option>
                </select>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Description</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: description"></span>
                <input class="input-xxlarge" type="text" maxlength="4000"
                       data-bind="visible: isEditingTopUpInfo, value: newDescription"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Display Order</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo() && displayOrder()==null">-</span>
                <span data-bind="visible: !isEditingTopUpInfo() && displayOrder()!=null, text: displayOrder"></span>
                <input id="sales-cutoff-field" class="input-small" type="number" min="0"
                       data-bind="visible: isEditingTopUpInfo, value: newDisplayOrder"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Start of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: startDate"></span>

                <div data-bind="visible: isEditingTopUpInfo">
                  <input class="input-medium" type="text" id="start-date-field" data-bind="value: newStartDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">End of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: endDate"></span>

                <div data-bind="visible: isEditingTopUpInfo">
                  <input class="input-medium" type="text" id="end-date-field" data-bind="value: newEndDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Sales Cut-off Before Event Date (Days)</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: salesCutOffDays"></span>
                <input id="sales-cutoff-field" class="input-small" type="number" min="0"
                       data-bind="visible: isEditingTopUpInfo, value: newSalesCutOffDays"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Price in Cents</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingTopUpInfo(), text: price"></span>
                <input class="input-small" type="number" min="0"
                       data-bind="visible: isEditingTopUpInfo, value: newPrice"/>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="image-head">
            <h3>Display Image</h3>
          </div>
          <div class="product-sect" id="image-sect">
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Display Image</div>
              <div class="offset1 span13">
                <div>
                  <span class="field-nothing" data-bind="visible: imageHash() == ''">no image selected</span>
                  <img alt="IMAGE" data-bind="visible: imageHash() != '', attr:{'src':imageHash}"/>
                </div>
                <span>Max dimensions: 369px (W) by 276px (H)</span>

                <form action="/.sky-dining-admin/top-up/save-display-image" method="post" enctype="multipart/form-data"
                      id="display-image-form">
                  <input type="file" name="image" id="image-file"/>
                  <input type="hidden" name="timestamp" data-bind="value: timestamp"/>
                  <input type="hidden" name="id" data-bind="value: id"/>
                  <input type="submit" class="btn btn-success" data-bind="visible: imageSelected"
                         value="Upload New Image">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>