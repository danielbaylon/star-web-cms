<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Shared Configurations :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/tinymce/scriptloader-tinymce.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/shared-config.js"></script>

  <script type="text/javascript">
    var recsPerPage = 10;
    var pageSize = 10;
  </script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main">
    <div class="span20" style="padding-left: 20px;">
      <h2>Shared Configuration</h2>
    </div>
    <!--&lt;!&ndash; Terms & Conditions &ndash;&gt;-->
    <!--<div class="span20">-->
    <!--<div style="width:80%; margin-left:auto; margin-right:auto;">-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<h3>Terms &amp; Conditions</h3>-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper" id="tncMainGrid" style="font-size:85%;">-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<div style="float:right; margin-top:5px;">-->
    <!--<a class="btn btn-danger" id="removeTNC" style="width: 80px">Remove</a>-->
    <!--<a class="btn btn-primary" id="addTNC" style="width: 120px">Add T &amp; C</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div id='tncDialogContainer'>-->
    <!--<table style='width:90%; margin: 0 auto;'>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px; width:30%;'>Title</td>-->
    <!--<td style='padding: 5px;  width:70%;'><input type="text" class="input" name="tncTitle" id="tncTitle"-->
    <!--style='width:100%;'/></td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>T&amp;C Type</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<select id="tncType" style='width:100%;' onchange="changeTncType()">-->
    <!--<s:iterator value="%{tncTypeOptions}">-->
    <!--<option value="${code}">-->
    <!--<s:property value="name" escapeHtml="false"/>-->
    <!--</option>-->
    <!--</s:iterator>-->
    <!--</select>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr id="tncCatRow" style="display: none;">-->
    <!--<td style='text-align:right; padding: 5px;'>Related Category</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<select id="tncRelateCat" style='width:100%;'>-->
    <!--<s:iterator value="%{categs}">-->
    <!--<option value="${code}">-->
    <!--<s:property value="name" escapeHtml="false"/>-->
    <!--</option>-->
    <!--</s:iterator>-->
    <!--</select>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr id="tncProdRow" style="display: none;">-->
    <!--<td style='text-align:right; padding: 5px;'>Related Product</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<span id='tncRelateProduct'></span>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Content</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<textarea id="tncEditor" style="width:100%">-->
    <!--</textarea>-->
    <!--&lt;!&ndash;-->
    <!--<textarea id="tncEditor" rows="10" cols="30" style="width:740px;height:440px">-->
    <!--</textarea>-->
    <!--&ndash;&gt;-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:center;' colspan="2"><a class='btn btn-primary' id='tncSaveBtn'-->
    <!--style='margin-right:10px; width: 80px;'>Save</a><a class='btn'-->
    <!--id='tncCancelBtn'-->
    <!--style='width: 80px;'>Cancel</a>-->
    <!--</td>-->
    <!--</tr>-->
    <!--</table>-->
    <!--</div>-->
    <!--&lt;!&ndash; Cable Car Closures &ndash;&gt;-->
    <!--<div class="span20" style="margin-top:20px;">-->
    <!--<div style="width:80%; margin-left:auto; margin-right:auto;">-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<h3>Cable Car Closures</h3>-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper" id="closureMainGrid" style="font-size:85%;">-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<div style="float:right; margin-top:5px;">-->
    <!--<a class="btn btn-danger" id="removeClosure" style="width: 80px">Remove</a>-->
    <!--<a class="btn btn-primary" id="addClosure" style="width: 120px">Add Schedule</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div id='closureDialogContainer'>-->
    <!--<table style='width:90%; margin: 0 auto;'>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px; width:30%;'>Closed From</td>-->
    <!--<td style='padding: 5px;  width:70%;'>-->
    <!--<input type="text" class="input" name="cloFrom" id="cloFrom" style='width:100%;'/>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px; width:30%;'>Closed To</td>-->
    <!--<td style='padding: 5px;  width:70%;'>-->
    <!--<input type="text" class="input" name="cloTo" id="cloTo" style='width:100%;'/>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Closure Details</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<textarea name="cloReamrk" id="cloReamrk" style='width:100%;'></textarea>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Products</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<table>-->
    <!--<s:iterator value="%{productsForClosure}">-->
    <!--<tr>-->
    <!--<td>-->
    <!--<label>-->
    <!--<input type="checkbox" name="closureProducts" id="cp${id}" value="${id}" style="margin-top:0px;"/>-->
    <!--${displayTitle}-->
    <!--</label>-->
    <!--</td>-->
    <!--</tr>-->
    <!--</s:iterator>-->
    <!--</table>-->

    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:center;' colspan="2">-->
    <!--<a class='btn btn-primary' id='cloSaveBtn' style='margin-right:10px; width: 80px;'>Save</a>-->
    <!--<a class='btn' id='cloCancelBtn' style='width: 80px;'>Cancel</a></td>-->
    <!--</tr>-->
    <!--</table>-->
    <!--</div>-->
    <!--&lt;!&ndash; Credit Card Merchants &ndash;&gt;-->
    <!--<div class="span20" style="margin-top:20px;">-->
    <!--<div style="width:80%; margin-left:auto; margin-right:auto;">-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<h3>Credit Card Merchants</h3>-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper" id="merchantMainGrid" style="font-size:85%;">-->
    <!--</div>-->
    <!--<div class="data_grid grid-wrapper">-->
    <!--<div style="float:right; margin-top:5px;">-->
    <!--<a class="btn btn-danger" id="removeMerchante" style="width: 80px">Remove</a>-->
    <!--<a class="btn btn-primary" id="addMerchant" style="width: 120px">Add Merchant</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div id='merchantDialogContainer'>-->
    <!--<table style='width:90%; margin: 0 auto;'>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px; width:30%;'>Merchant Name</td>-->
    <!--<td style='padding: 5px;  width:70%;'>-->
    <!--<input type="text" class="input" name="merName" id="merName" style='width:100%;'/>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px; width:30%;'>TM Merchant ID</td>-->
    <!--<td style='padding: 5px;  width:70%;'>-->
    <!--<input type="text" class="input" name="merID" id="merID" style='width:100%;'/>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Bank</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<select id="merBank" style='width:100%;'>-->
    <!--<s:iterator value="%{bankOptions}">-->
    <!--<option value="${code}">-->
    <!--<s:property value="name" escapeHtml="false"/>-->
    <!--</option>-->
    <!--</s:iterator>-->
    <!--</select>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Accepts All Cards</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<label class="checkbox"><input type="checkbox" id='merAccept' name='merAccept'/> Yes</label>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:right; padding: 5px;'>Is Active</td>-->
    <!--<td style='padding: 5px;'>-->
    <!--<label class="checkbox"><input type="checkbox" id='merActive' name='merActive'/> Yes</label>-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--<td style='text-align:center;' colspan="2">-->
    <!--<a class='btn btn-primary' id='merSaveBtn' style='margin-right:10px; width: 80px;'>Save</a>-->
    <!--<a class='btn' id='merCancelBtn' style='width: 80px;'>Cancel</a></td>-->
    <!--</tr>-->
    <!--</table>-->
    <!--</div>-->
    <!-- Faber Licence Members -->
    <div class="span20" style="margin-top:20px;">
      <div style="width:80%; margin-left:auto; margin-right:auto;">
        <div class="data_grid grid-wrapper">
          <h3>Faber Licence Members</h3>
        </div>
        <div class="data_grid grid-wrapper" style="font-size:85%; margin-bottom: 5px;">
          <div id="uploaderContainer" style="margin-bottom:5px; width:100%; text-align: right;">
            <input name="jcMemberUploader" id="jcMemberUploader" type="file"/>
          </div>
          <label style="display: inline-block">
            Filter Expiry Dates From
            <input type="text" class="input" id="jcFrom"/>
          </label>
          <label style="display: inline-block">
            To
            <input type="text" class="input" id="jcTo"/>
          </label>
          <label>
            Membership No.
            <input type="text" class="input" name="searchJCMemberInput" id="searchJCMemberInput" style='width:200px;'/>
            <a class="btn" id="searchJCMember" style="width: 80px">Search</a>
          </label>
        </div>
        <div class="data_grid grid-wrapper" id="jcMemberMainGrid" style="font-size:85%;">
        </div>
        <div class="data_grid grid-wrapper">
          <div style="float:right; margin-top:5px;">
            <a class="btn btn-danger" id="removeFilteredRecords" style="width: 180px">Remove All Filtered Records</a>
            <a class="btn btn-danger" id="removeJCMember" style="width: 80px">Remove</a>
          </div>
        </div>
      </div>
    </div>
    <div id='jcMemberDialogContainer'>
      <table style='width:90%; margin: 0 auto;'>
        <tr>
          <td style='text-align:right; padding: 5px; width:30%;'>Membership No.</td>
          <td style='padding: 5px;  width:70%;'>
            <input type="text" class="input" name="jcMemberCardNumber" id="jcMemberCardNumber" style='width:100%;'/>
          </td>
        </tr>
        <tr>
          <td style='text-align:right; padding: 5px; width:30%;'>Expiry Date</td>
          <td style='padding: 5px;  width:70%;'>
            <input type="text" class="input" name="jcMemberExpiryDate" id="jcMemberExpiryDate" style='width:100%;'/>
          </td>
        </tr>
        <tr>
          <td style='text-align:center;' colspan="2">
            <a class='btn btn-primary' id='jcMemberSaveBtn' style='margin-right:10px; width: 80px;'>Save</a>
            <a class='btn' id='jcMemberCancelBtn' style='width: 80px;'>Cancel</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
</body>