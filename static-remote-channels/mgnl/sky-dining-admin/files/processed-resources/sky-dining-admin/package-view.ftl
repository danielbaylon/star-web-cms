<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Package :: MFLG Sky Dining Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='//fonts.googleapis.com/css?family=Alegreya|Open+Sans:400,700|Source+Sans+Pro:400,600,700' rel='stylesheet'
        type='text/css'>

  <link rel="stylesheet" type="text/css" href="/resources/sky-dining-admin/css/styleloader.css"/>

  <script src="/resources/sky-dining-admin/js/scriptloader-lib.js"></script>
  <script src="/resources/sky-dining-admin/js/tinymce/scriptloader-tinymce.js"></script>
  <script src="/resources/sky-dining-admin/js/scriptloader-common.js"></script>
  <script src="/resources/sky-dining-admin/js/package-view.js"></script>
  <script src="/resources/sky-dining-admin/js/package-view-grids.js"></script>
  <script src="/resources/sky-dining-admin/js/package-view-promos.js"></script>
</head>

<body>
<div class="container" style="margin-bottom: 50px;">
  <div class="row-fluid admin-main" id="package-view">
    <div class="span20">
      <div class="product-title">
        <h2 data-bind="text: headerText">Package Name</h2>

        <div class="clearfix"></div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span4">
        <div class="float-menu" data-spy="affix" data-offset-top="40">
          <div class="menu-active-section">
            <h3>Sections</h3>
            <ul id="navbar" class="navbar nav menu-links">
              <li><a href="#package-head">Package Information</a></li>
              <li><a href="#display-head">Display Information</a></li>
              <li data-bind="visible: !isNew()"><a href="#image-head">Display Image</a></li>
              <li data-bind="visible: !isNew()"><a href="#notes-head">Notes and T&amp;Cs</a></li>
              <li data-bind="visible: !isNew()"><a href="#themes-head">Themes</a></li>
              <li data-bind="visible: !isNew()"><a href="#menu-head">Menu</a></li>
              <li data-bind="visible: !isNew()"><a href="#topups-head">Top Ups</a></li>
              <li data-bind="visible: !isNew()"><a href="#promos-head">Promotions</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="span16" style="min-height: 1000px; margin-bottom: 100px;">
        <div class="product-main">
          <div class="product-header" id="package-head">
            <h3>Package Information</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingPackageInfo(), click: doEditPackageInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingPackageInfo, click: doCancelPackageInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingPackageInfo, click: doSavePackageInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: packageInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':packageInfoHasError}, html: packageInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Package Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: name"></span>
                <input class="input-xxlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingPackageInfo, value: newName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">End of Early Bird Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: earlyBirdEndDate"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input class="input-medium" type="text" id="early-bird-field" data-bind="value: newEarlyBirdEndDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Start of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: startDate"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input class="input-medium" type="text" id="start-date-field" data-bind="value: newStartDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">End of Sales Period</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: endDate"></span>

                <div data-bind="visible: isEditingPackageInfo">
                  <input class="input-medium" type="text" id="end-date-field" data-bind="value: newEndDate"/>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Sales Cut-off Before Event Date (Days)</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingPackageInfo(), text: salesCutOffDays"></span>
                <input id="sales-cutoff-field" class="input-small" type="number" min="0"
                       data-bind="visible: isEditingPackageInfo, value: newSalesCutOffDays"/>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main">
          <div class="product-header" id="display-head">
            <h3>Display Information</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingDisplayInfo(), click: doEditDisplayInfo">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingDisplayInfo, click: doCancelDisplayInfo">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingDisplayInfo, click: doSaveDisplayInfo">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: displayInfoMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':displayInfoHasError}, html: displayInfoMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Name</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingDisplayInfo(), text: name"></span>
                <input class="input-xxlarge" type="text" maxlength="300"
                       data-bind="visible: isEditingDisplayInfo, value: newName"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Description</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingDisplayInfo() && description()==''">no description specified</span>
                <span data-bind="visible: !isEditingDisplayInfo() && description()!='', html: description"></span>

                <div data-bind="visible: isEditingDisplayInfo">
                  <textarea maxlength="4000" id="description-field" data-bind="value: newDescription"
                            style="height: 300px; width: 100%;"></textarea>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Description Link</div>
              <div class="offset1 span13">
                <span class="field-nothing"
                      data-bind="visible: !isEditingDisplayInfo() && link()==''">no link specified</span>
                <a data-bind="visible: !isEditingDisplayInfo() && link()!='', attr:{'href':link}">open link in new
                  window</a>
                <input class="input-xxlarge" type="url" maxlength="500"
                       data-bind="visible: isEditingDisplayInfo, value: newLink"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Display Order</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingDisplayInfo() && displayOrder() == null">-</span>
                <span data-bind="visible: !isEditingDisplayInfo() && displayOrder() != null, text: displayOrder"></span>
                <input class="input-small" type="number"
                       data-bind="visible: isEditingDisplayInfo, value: newDisplayOrder"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Pax Per Booking</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingDisplayInfo(), text: minPax"></span>
                <input class="input-small" type="number" min="1"
                       data-bind="visible: isEditingDisplayInfo, value: newMinPax"/>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Additional Pax Allowed</div>
              <div class="offset1 span13">
                <span data-bind="visible: !isEditingDisplayInfo(), text: extraPax"></span>
                <input class="input-small" type="number" min="0"
                       data-bind="visible: isEditingDisplayInfo, value: newExtraPax"/>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="image-head">
            <h3>Display Image</h3>
          </div>
          <div class="product-sect" id="image-sect">
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Display Image</div>
              <div class="offset1 span13">
                <div>
                  <span class="field-nothing" data-bind="visible: imageHash() == ''">no image selected</span>
                  <img alt="IMAGE" data-bind="visible: imageHash() != '', attr:{'src':imageHash}"/>
                </div>
                <span>Max dimensions: 369px (W) by 276px (H)</span>

                <form action="/admin/sky-dining/packages/save-display-image" method="post" enctype="multipart/form-data"
                      id="display-image-form">
                  <input type="file" name="image" id="image-file"/>
                  <input type="hidden" name="timestamp" data-bind="value: timestamp"/>
                  <input type="hidden" name="id" data-bind="value: id"/>
                  <input type="submit" class="btn btn-success" data-bind="visible: imageSelected"
                         value="Upload New Image">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="notes-head">
            <h3>Notes and T&amp;Cs</h3>
            <a class="btn btn-primary header-btn"
               data-bind="visible: !isEditingNotesAndTnc(), click: doEditNotesAndTnc">Edit</a>
            <a class="btn header-btn" data-bind="visible: isEditingNotesAndTnc, click: doCancelNotesAndTnc">Cancel</a>
            <a class="btn btn-success header-btn" data-bind="visible: isEditingNotesAndTnc, click: doSaveNotesAndTnc">Save
              Changes</a>
          </div>
          <div class="product-sect">
            <div class="row-fluid field-row" style="display: none;" data-bind="visible: notesAndTncMessage() != ''">
              <div class="offset1 span18 alert"
                   data-bind="css:{'alert-error':notesAndTncHasError}, html: notesAndTncMessage"></div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Notes</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingNotesAndTnc() && notes()==''">no notes specified</span>
                <span data-bind="visible: !isEditingNotesAndTnc() && notes()!='', html: notes"></span>

                <div data-bind="visible: isEditingNotesAndTnc">
                  <textarea maxlength="4000" id="notes-field" data-bind="value: newNotes"
                            style="height: 300px; width: 100%;"></textarea>
                </div>
              </div>
            </div>
            <div class="row-fluid field-row">
              <div class="offset1 span5 field-label">Terms &amp; Conditions</div>
              <div class="offset1 span13">
                <span class="field-nothing" data-bind="visible: !isEditingNotesAndTnc() && tncContent()==''">There are no terms &amp; conditions assigned to the package.</span>

                <div class="ntnc-holder"
                     data-bind="visible: !isEditingNotesAndTnc() && tncContent()!='', html: tncContent"></div>
                <div data-bind="visible: isEditingNotesAndTnc">
                  <div class="data_grid grid-wrapper" id="tncs-grid"></div>
                  <div class="tnc-action-new">
                    <span>Haven't added any T&amp;C yet?</span>&nbsp;&nbsp;
                    <a style="vertical-align: top" class="btn btn-primary header-btn" data-bind="click: doAddTnc">Add
                      New T&amp;C</a>
                    <a style="vertical-align: top" class="btn btn-danger header-btn"
                       data-bind="click: doRemoveTnc">Remove Existing T&amp;C</a>
                  </div>
                  <div class="ntnc-holder">
                    <span class="field-nothing" data-bind="visible: newTncContent()==''">There are no existing terms &amp; conditions assigned to the package.</span>

                    <div data-bind="visible: newTncContent()!=''">
                      <h4 style="margin: 5px;color: #008076">Existing T&amp;Cs</h4>

                      <div data-bind="html: newTncContent"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="themes-head">
            <h3>Themes</h3>
          </div>
          <div class="product-sect">
            <div class="data_grid grid-wrapper prod-grid" id="themes-grid"></div>
            <div class="grid-actions">
              <a class="btn btn-danger" id="remove-theme-button" data-bind="click: doRemoveTheme">Remove Themes</a>
              <a class="btn btn-primary" id="add-theme-button" data-bind="click: doAddTheme">Add Theme</a>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="menu-head">
            <h3>Menu</h3>
          </div>
          <div class="product-sect">
            <div class="data_grid grid-wrapper prod-grid" id="menus-grid"></div>
            <div class="grid-actions">
              <a class="btn btn-danger" id="remove-menu-button" data-bind="click: doRemoveMenu">Remove Menus</a>
              <a class="btn btn-primary" id="add-menu-button" data-bind="click: doAddMenu">Add Menu</a>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="topups-head">
            <h3>Top Ups</h3>
          </div>
          <div class="product-sect">
            <div class="data_grid grid-wrapper prod-grid" id="topups-grid"></div>
            <div class="grid-actions">
              <a class="btn btn-danger" id="remove-topup-button" data-bind="click: doRemoveTopUp">Remove Top Ups</a>
              <a class="btn btn-primary" id="add-topup-button" data-bind="click: doAddTopUp">Add Top Up</a>
            </div>
          </div>
        </div>
        <div class="product-main" data-bind="visible: !isNew()">
          <div class="product-header" id="promos-head">
            <h3>Promotions</h3>
          </div>
          <div class="product-sect">
            <div class="data_grid grid-wrapper prod-grid" id="promos-grid"></div>
            <div class="grid-actions">
              <a class="btn btn-danger" id="remove-promo-button" data-bind="click: doRemovePromo">Remove Promotions</a>
              <a class="btn btn-primary" id="add-promo-button" data-bind="click: doAddPromo">Add Promotions</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="tnc-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <form class="form-horizontal">
          <div class="alert alert-error" style="display: none;"
               data-bind="visible: tncHasError, html: tncMessage"></div>
          <div class="control-group">
            <label class="control-label">Title</label>

            <div class="controls">
              <input class="input-xxlarge" type="text" data-bind="value: newTitle"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Content</label>

            <div class="controls">
              <textarea maxlength="4000" id="tnc-content-field" data-bind="value: newTncContent"
                        style="height: 300px; width: 100%;"></textarea>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <a class="btn btn-success" data-bind="click: doSaveTnc">Save</a>
              <a class="btn" data-bind="click: doCancelTnc">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="theme-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <form class="form-horizontal" action="/admin/sky-dining/packages/save-theme" method="post"
              enctype="multipart/form-data" id="theme-form">
          <div class="alert alert-error" style="display: none;"
               data-bind="visible: themeHasError, html: themeMessage"></div>
          <div class="control-group">
            <div class="control-label">Display Image</div>
            <div class="controls">
              <div>
                <span class="field-nothing" data-bind="visible: imageHash() == ''">no image selected</span>
                <img alt="IMAGE" data-bind="visible: imageHash() != '', attr:{'src':imageHash}"/>
              </div>
              <span>Max dimensions: 369px (W) by 276px (H)</span>
              <input type="file" name="image" id="theme-image"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Description</label>

            <div class="controls">
              <textarea rows="4" maxlength="4000" class="input-xxlarge" name="newDescription"
                        data-bind="value: newDescription"></textarea>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Capacity</label>

            <div class="controls">
              <input class="input-small" type="number" min="1" name="newCapacity" data-bind="value: newCapacity"/>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <input type="submit" class="btn btn-success" value="Save">
              <a class="btn" data-bind="click: doCancelTheme">Cancel</a>
            </div>
          </div>
          <input type="hidden" name="id" data-bind="value: id"/>
          <input type="hidden" name="parentId" data-bind="value: parentId"/>
          <input type="hidden" name="timestamp" data-bind="value: timestamp"/>
        </form>
      </div>
    </div>
  </div>

  <div id="menu-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <div class="data_grid grid-wrapper prod-grid" id="select-menus-grid"></div>
        <div style="margin-top: 10px;" class="grid-actions">
          <a class="btn btn-primary" id="add-selected-menus">Add Selected</a>
          <a class="btn" id="cancel-menu-select">Cancel</a>
        </div>
      </div>
    </div>
  </div>

  <div id="topup-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <div class="data_grid grid-wrapper prod-grid" id="select-topups-grid"></div>
        <div style="margin-top: 10px;" class="grid-actions">
          <a class="btn btn-primary" id="add-selected-topups">Add Selected</a>
          <a class="btn" id="cancel-topup-select">Cancel</a>
        </div>
      </div>
    </div>
  </div>

  <div id="promo-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <form class="form-horizontal">
          <div class="alert alert-error" style="display: none;"
               data-bind="visible: promoHasError, html: promoMessage"></div>
          <div class="control-group">
            <label class="control-label">Name</label>

            <div class="controls">
              <input class="input-xxlarge" type="text" data-bind="value: newName"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Status</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newActive">
                <option value="true">Active</option>
                <option value="false">Inactive</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Discount Type</label>

            <div class="controls">
              <select data-bind="value: newPriceType">
                <option value="1">Percent</option>
                <option value="2">Fixed Amount (In Cents)</option>
                <option value="3">Set Price (In Cents)</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Discount Price</label>

            <div class="controls">
              <input type="number" class="input-small" placeholder="Discount" data-bind="value: newDiscountPrice">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Promotion Type</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newType">
                <option value="EarlyBird">Early Bird</option>
                <option value="CreditCard">Credit Card</option>
                <option value="PromoCode">Promo Code</option>
              </select>
            </div>
          </div>
          <div data-bind="fadeVisible: newType() == 'JewelCard' || newType() == 'CreditCard'">
            <div class="control-group">
              <label class="control-label">Valid From</label>

              <div class="controls">
                <input type="text" id="promo-valid-from" data-bind="value: newValidFrom">
              </div>
            </div>
          </div>
          <div data-bind="fadeVisible: newType() == 'JewelCard' || newType() == 'CreditCard'">
            <div class="control-group">
              <label class="control-label">Valid Until</label>

              <div class="controls">
                <input type="text" id="promo-valid-to" data-bind="value: newValidTo">
              </div>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Terms and Conditions</label>

            <div class="controls">
              <textarea id="promo-tnc"></textarea>
            </div>
          </div>
          <div data-bind="fadeVisible: newType() == 'CreditCard'" style="display: none;">
            <div class="control-group">
              <label class="control-label">Select Merchant</label>

              <div class="controls">
                <div id="merch-grid"></div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <a class="btn btn-success" data-bind="click: doSavePromo">Save</a>
              <a class="btn" data-bind="click: doCancelPromo">Cancel</a>
            </div>
          </div>
        </form>
      </div>
      <div data-bind="fadeVisible: !isNew() && newType() == 'PromoCode'">
        <div class="span20">
          <h4 style="background: #2EAFC0;padding: 5px 10px;color:#ffffff; font-weight: normal">Promo Codes</h4>
        </div>
        <div class="span-20">
          <div class="data_grid grid-wrapper prod-grid" id="promo-codes-grid"></div>
          <div style="margin-top: 10px;" class="grid-actions">
            <a class="btn btn-danger" id="remove-promo-code-button" data-bind="click: doRemovePromoCode">Remove Promo
              Codes</a>
            <a class="btn btn-primary" id="add-promo-code-button" data-bind="click: doAddPromoCode">Add Promo Codes</a>
          </div>
        </div>
      </div>
      <div data-bind="visible: !isNew()">
        <div class="span20">
          <h4 style="background: #2EAFC0;padding: 5px 10px;color:#ffffff; font-weight: normal">Related Menus</h4>
        </div>
        <div class="span-20">
          <div class="data_grid grid-wrapper prod-grid" id="promo-menus-grid"></div>
          <div style="margin-top: 10px;" class="grid-actions">
            <a class="btn btn-danger" id="remove-promo-menu-button" data-bind="click: doRemovePromoMenu">Remove
              Menus</a>
            <a class="btn btn-primary" id="add-promo-menu-button" data-bind="click: doAddPromoMenu">Add Menu</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="promo-menu-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <div class="data_grid grid-wrapper prod-grid" id="select-promo-menus-grid"></div>
        <div style="margin-top: 10px;" class="grid-actions">
          <a class="btn btn-primary" id="add-selected-promo-menus">Add Selected</a>
          <a class="btn" id="cancel-promo-menu-select">Cancel</a>
        </div>
      </div>
    </div>
  </div>

  <div id="promo-code-modal" style="display: none;" class="admin-modal">
    <div class="row-fluid">
      <div class="span20">
        <form class="form-horizontal">
          <div class="alert alert-error" style="display: none;"
               data-bind="visible: promoCodeHasError, html: promoCodeMessage"></div>
          <div class="control-group">
            <label class="control-label">Code</label>

            <div class="controls">
              <input class="input-medium" type="text" data-bind="value: newCode"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Status</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newActive">
                <option value="true">Active</option>
                <option value="false">Inactive</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Has Limit</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newHasLimit">
                <option value="false">No</option>
                <option value="true">Yes</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Limit</label>

            <div class="controls">
              <input type="number" class="input-small" data-bind="value: newLimit">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Times Used</label>

            <div class="controls">
              <input type="number" class="input-small" data-bind="value: newTimesUsed">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Limit Type</label>

            <div class="controls">
              <select class="input-medium" data-bind="value: newType">
                <option value="VisitBased">Visit Based</option>
                <option value="BookingBased">Booking Based</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Start Date</label>

            <div class="controls">
              <input class="input-medium" type="text" id="promo-code-start-field" data-bind="value: newStartDate"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">End Date</label>

            <div class="controls">
              <input class="input-medium" type="text" id="promo-code-end-field" data-bind="value: newEndDate"/>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <a class="btn btn-success" data-bind="click: doSavePromoCode">Save</a>
              <a class="btn" data-bind="click: doCancelPromoCode">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</body>