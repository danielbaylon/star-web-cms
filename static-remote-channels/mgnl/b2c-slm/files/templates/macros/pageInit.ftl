[#assign lang = sitefn.site().getI18n().getLocale().toString()]
[#assign sitePath = "${ctx.contextPath}/${lang}/b2c-slm"]
[#assign themePath = "${ctx.contextPath}/resources/b2c-slm/theme/default"]
[#if lang == "en" || lang == "zh_CN"]
    [#assign imgSuffix = lang!"en"]
[#else]
    [#assign imgSuffix = "en"]
[/#if]

<script>
    var jsLang = "${lang?js_string!}";
</script>