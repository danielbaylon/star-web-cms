[#assign staticText =
    {"en":
        {
        "languageDisplayName" : "English",
        "signUp": "Sign Up",
        "cartItems": "item(s)",
        "yourCart": "YOUR CART",
        "cartEmpty": "Your Cart is empty",
        "total": "total",
        "checkout": "Check out",
        "quickFind": "Quick Find",
        "enterPromotionCode":"Enter Promotion Code",
        "product":"Product",
        "promoCode":"Promo Code"
        },
    "zh_CN":
        {
        "languageDisplayName" : "简体（中文）",
        "signUp": "报名",
        "cartItems": "件商品",
        "yourCart": "您的预订单",
        "cartEmpty": "您的预订单是空的",
        "total": "总计",
        "checkout": "结算"
        }
    ,
    "ja":
        {
        "languageDisplayName" : "日本語"
        }
 }
]
