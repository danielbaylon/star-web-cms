[#assign staticText =
    {"en":
        {
            "onJourney": "Go on a journey at The State of Fun.",
            "back": "Back",
            "next": "Next",
            "recommendation": "Go with our Recommendation",
            "chooseAdventure": "OR CHOOSE YOUR OWN ADVENTURE!",
            "select": "Select",
            "unselect": "Unselect",
            "youSelected": "You have selected",
            "go": "Go",
            "chosen": "These are the Fun you have chosen",
            "total": "Total: ",
            "youCanSave": "You can Save ",
            "byChoosing": " by choosing our Fun Pass",
            "wouldYouLike": "Would you like to have more Fun?",
            "continueJourney": "Yes, I want to continue the Fun Finder journey.",
            "restartJourney": "Restart the Fun Finder journey",
            "great": "Great!",
            "youSelected2": " You have selected:",
            "viewSummary": "View Summary",
            "excellent": "Excellent!",
            "tellUs": " Tell us how many passes you need?",
            "proceedToPay": "Proceed To Pay"
        },
    "zh_CN":
        {
            "onJourney": "在玩美奇域遨游",
            "back": "返回",
            "next": "继续",
            "recommendation": "随着我们的建议",
            "chooseAdventure": "选择自己的行程",
            "select": "选择此景点",
            "unselect": "删除此景点",
            "youSelected": "您选择了",
            "go": "继续",
            "chosen": "您选择的欢乐",
            "total": "总计: ",
            "youCanSave": "选购欢乐套票，节省更多！",
            "byChoosing": "",
            "wouldYouLike": "想体验更多欢乐？",
            "continueJourney": "继续寻找你的欢乐行程",
            "restartJourney": "再次启动欢乐行程",
            "great": "太好了！",
            "youSelected2": " 您挑选了：",
            "viewSummary": "查看总结",
            "excellent": "太棒了！",
            "tellUs": "请问您需要多少张欢乐套票？",
            "proceedToPay": "结算"
        }
    }
]
