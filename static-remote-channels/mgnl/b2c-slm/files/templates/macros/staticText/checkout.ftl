[#assign staticText =
    {"en":
        {
            "checkout" : "Checkout",
            "continueShopping" : "Continue Shopping",
            "clearCart" : "Clear Cart",
            "item" : "Item",
            "details" : "Details",
            "unitPrice" : "Unit Price",
            "qty" : "Qty",
            "amount" : "Amount",
            "remove" : "Remove",
            "addMoreTopups" : "add more top-ups",
            "subtotal" : "SUBTOTAL:",
            "bookingFee" : "Booking Fee",
            "grandTotal" : "GRAND TOTAL (Inclusive of GST):",
            "feautureUnderConstruction" : "This feature is still under construction! Check back again or subscribe to our e-newsletter to be updated.",
            "ok": "OK",
            "personalInformation": "Personal Information",
            "name" : "Name:",
            "asAppearInCC" : "(as appears on credit card)",
            "email" : "Email:",
            "paymentType" : "Payment Type:",
            "paymentOptions" : "<option value=''>Please select...</option>
                                <option value='Visa'>Visa</option>
                                <option value='Mastercard'>Mastercard</option>
                                <option value='Amex'>American Express</option>
                                <option value='JCB'>JCB</option>
                                <option value='ChinaUnionPay'>China UnionPay</option>",
            "nationality" : "Nationality:",
            "nationalityOptions" : "<option value='' selected='selected'>Please select...</option>
                                    <option value='Singaporean'>Singaporean</option><option value='Afghan'>Afghan</option><option value='Albanian'>Albanian</option><option value='Algerian'>Algerian</option><option value='American'>American</option><option value='Andorran'>Andorran</option><option value='Angolan'>Angolan</option><option value='Antiguans'>Antiguans</option><option value='Argentinean'>Argentinean</option><option value='Armenian'>Armenian</option><option value='Australian'>Australian</option><option value='Austrian'>Austrian</option><option value='Azerbaijani'>Azerbaijani</option><option value='Bahamian'>Bahamian</option><option value='Bahraini'>Bahraini</option><option value='Bangladeshi'>Bangladeshi</option><option value='Barbadian'>Barbadian</option><option value='Barbudans'>Barbudans</option><option value='Batswana'>Batswana</option><option value='Belarusian'>Belarusian</option><option value='Belgian'>Belgian</option><option value='Belizean'>Belizean</option><option value='Beninese'>Beninese</option><option value='Bhutanese'>Bhutanese</option><option value='Bolivian'>Bolivian</option><option value='Bosnian'>Bosnian</option><option value='Brazilian'>Brazilian</option><option value='British'>British</option><option value='Bruneian'>Bruneian</option><option value='Bulgarian'>Bulgarian</option><option value='Burkinabe'>Burkinabe</option><option value='Burmese'>Burmese</option><option value='Burundian'>Burundian</option><option value='Cambodian'>Cambodian</option><option value='Cameroonian'>Cameroonian</option><option value='Canadian'>Canadian</option><option value='Cape Verdean'>Cape Verdean</option><option value='Central African'>Central African</option><option value='Chadian'>Chadian</option><option value='Chilean'>Chilean</option><option value='Chinese'>Chinese</option><option value='Colombian'>Colombian</option><option value='Comoran'>Comoran</option><option value='Congolese'>Congolese</option><option value='Costa Rican'>Costa Rican</option><option value='Croatian'>Croatian</option><option value='Cuban'>Cuban</option><option value='Cypriot'>Cypriot</option><option value='Czech'>Czech</option><option value='Danish'>Danish</option><option value='Djibouti'>Djibouti</option><option value='Dominican'>Dominican</option><option value='Dutch'>Dutch</option><option value='East Timorese'>East Timorese</option><option value='Ecuadorean'>Ecuadorean</option><option value='Egyptian'>Egyptian</option><option value='Emirian'>Emirian</option><option value='Equatorial Guinean'>Equatorial Guinean</option><option value='Eritrean'>Eritrean</option><option value='Estonian'>Estonian</option><option value='Ethiopian'>Ethiopian</option><option value='Fijian'>Fijian</option><option value='Filipino'>Filipino</option><option value='Finnish'>Finnish</option><option value='French'>French</option><option value='Gabonese'>Gabonese</option><option value='Gambian'>Gambian</option><option value='Georgian'>Georgian</option><option value='German'>German</option><option value='Ghanaian'>Ghanaian</option><option value='Greek'>Greek</option><option value='Grenadian'>Grenadian</option><option value='Guatemalan'>Guatemalan</option><option value='Guinea-Bissauan'>Guinea-Bissauan</option><option value='Guinean'>Guinean</option><option value='Guyanese'>Guyanese</option><option value='Haitian'>Haitian</option><option value='Herzegovinian'>Herzegovinian</option><option value='Honduran'>Honduran</option><option value='Hungarian'>Hungarian</option><option value='I-Kiribati'>I-Kiribati</option><option value='Icelander'>Icelander</option><option value='Indian'>Indian</option><option value='Indonesian'>Indonesian</option><option value='Iranian'>Iranian</option><option value='Iraqi'>Iraqi</option><option value='Irish'>Irish</option><option value='Israeli'>Israeli</option><option value='Italian'>Italian</option><option value='Ivorian'>Ivorian</option><option value='Jamaican'>Jamaican</option><option value='Japanese'>Japanese</option><option value='Jordanian'>Jordanian</option><option value='Kazakhstani'>Kazakhstani</option><option value='Kenyan'>Kenyan</option><option value='Kittian and Nevisian'>Kittian and Nevisian</option><option value='Kuwaiti'>Kuwaiti</option><option value='Kyrgyz'>Kyrgyz</option><option value='Laotian'>Laotian</option><option value='Latvian'>Latvian</option><option value='Lebanese'>Lebanese</option><option value='Liberian'>Liberian</option><option value='Libyan'>Libyan</option><option value='Liechtensteiner'>Liechtensteiner</option><option value='Lithuanian'>Lithuanian</option><option value='Luxembourger'>Luxembourger</option><option value='Macedonian'>Macedonian</option><option value='Malagasy'>Malagasy</option><option value='Malawian'>Malawian</option><option value='Malaysian'>Malaysian</option><option value='Maldivan'>Maldivan</option><option value='Malian'>Malian</option><option value='Maltese'>Maltese</option><option value='Marshallese'>Marshallese</option><option value='Mauritanian'>Mauritanian</option><option value='Mauritian'>Mauritian</option><option value='Mexican'>Mexican</option><option value='Micronesian'>Micronesian</option><option value='Moldovan'>Moldovan</option><option value='Monacan'>Monacan</option><option value='Mongolian'>Mongolian</option><option value='Moroccan'>Moroccan</option><option value='Mosotho'>Mosotho</option><option value='Motswana'>Motswana</option><option value='Mozambican'>Mozambican</option><option value='Namibian'>Namibian</option><option value='Nauruan'>Nauruan</option><option value='Nepalese'>Nepalese</option><option value='New Zealander'>New Zealander</option><option value='Nicaraguan'>Nicaraguan</option><option value='Nigerian'>Nigerian</option><option value='Nigerien'>Nigerien</option><option value='North Korean'>North Korean</option><option value='Northern Irish'>Northern Irish</option><option value='Norwegian'>Norwegian</option><option value='Omani'>Omani</option><option value='Pakistani'>Pakistani</option><option value='Palauan'>Palauan</option><option value='Panamanian'>Panamanian</option><option value='Papua New Guinean'>Papua New Guinean</option><option value='Paraguayan'>Paraguayan</option><option value='Peruvian'>Peruvian</option><option value='Polish'>Polish</option><option value='Portuguese'>Portuguese</option><option value='Qatari'>Qatari</option><option value='Romanian'>Romanian</option><option value='Russian'>Russian</option><option value='Rwandan'>Rwandan</option><option value='Saint Lucian'>Saint Lucian</option><option value='Salvadoran'>Salvadoran</option><option value='Samoan'>Samoan</option><option value='San Marinese'>San Marinese</option><option value='Sao Tomean'>Sao Tomean</option><option value='Saudi'>Saudi</option><option value='Scottish'>Scottish</option><option value='Senegalese'>Senegalese</option><option value='Serbian'>Serbian</option><option value='Seychellois'>Seychellois</option><option value='Sierra Leonean'>Sierra Leonean</option><option value='Slovakian'>Slovakian</option><option value='Slovenian'>Slovenian</option><option value='Solomon Islander'>Solomon Islander</option><option value='Somali'>Somali</option><option value='South African'>South African</option><option value='South Korean'>South Korean</option><option value='Spanish'>Spanish</option><option value='Sri Lankan'>Sri Lankan</option><option value='Sudanese'>Sudanese</option><option value='Surinamer'>Surinamer</option><option value='Swazi'>Swazi</option><option value='Swedish'>Swedish</option><option value='Swiss'>Swiss</option><option value='Syrian'>Syrian</option><option value='Taiwanese'>Taiwanese</option><option value='Tajik'>Tajik</option><option value='Tanzanian'>Tanzanian</option><option value='Thai'>Thai</option><option value='Togolese'>Togolese</option><option value='Tongan'>Tongan</option><option value='Trinidadian or Tobagonian'>Trinidadian or Tobagonian</option><option value='Tunisian'>Tunisian</option><option value='Turkish'>Turkish</option><option value='Tuvaluan'>Tuvaluan</option><option value='Ugandan'>Ugandan</option><option value='Ukrainian'>Ukrainian</option><option value='Uruguayan'>Uruguayan</option><option value='Uzbekistani'>Uzbekistani</option><option value='Venezuelan'>Venezuelan</option><option value='Vietnamese'>Vietnamese</option><option value='Welsh'>Welsh</option><option value='Yemenite'>Yemenite</option><option value='Zambian'>Zambian</option><option value='Zimbabwean'>Zimbabwean</option>",
            "whereDidYouHearUs": "Where did you hear about us?",
            "whereDidYouHearUsOptions" : "<option value=''>Please select...</option>
                                            <option value='Television Programme'>Television Programme</option>
                                            <option value='Internet'>Internet</option>
                                            <option value='Tour Guide Books'>Tour Guide Books</option>
                                            <option value='Pamphlets/Leaflets'>Pamphlets/Leaflets</option>
                                            <option value='Newspaper/Magazines'>Newspaper/Magazines</option>
                                            <option value='Advertisements'>Advertisements</option>
                                            <option value='Trade/Travel Fairs'>Trade/Travel Fairs</option>
                                            <option value='Word-of-mouth'>Word-of-mouth</option>
                                            <option value='Hotel Staff'>Hotel Staff</option>
                                            <option value='Tour Guides/Agencies'>Tour Guides/Agencies</option>
                                            <option value='Taxi Driver'>Taxi Driver</option>
                                            <option value='Buswrap'>Buswrap</option>
                                            <option value='Outdoor Billboard'>Outdoor Billboard</option>
                                            <option value='Others'>Others</option>",
            "dateOfBirth": "Date of Birth:",
            "contactNo" : "Contact No.:",
            "keyInValidEmailAddress" : "Please key in a valid email address. Upon confirmation, a receipt with a redemption PIN code will be sent to your email.",
            "fieldsWithAsterisks" : "Fields with asterisks",
            "areMandatory" : "are mandatory.",
            "wishToReceive" : "I wish to receive the latest promotions and updates from Sentosa.",
            "haveAgree" : "I have read and agree to both the",
            "tnc" : "Terms and Conditions",
            "setOutInSentosa" : "as set out in the Sentosa Online Store and Sentosa Leisure Management Pte Ltd's (\"SLM\")",
            "dataProtectionPolicy": "Data Protection Policy",
            "includePolicy": ", including as to how my personal data may be collected, used, disclosed and processed, and confirm that all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself, I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents, to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out in the Data Protection Policy.",
        "proceedToPay": "Proceed to Pay",
        "bankPromotion" : "Bank Promotion:",
        "selectBank" : "Please select..."
        },
    "zh_CN":
        {
            "checkout" : "结算",
            "continueShopping" : "继续购物",
            "clearCart" : "清空购物车",
            "item" : "票类名称",
            "details" : "详情",
            "unitPrice" : "单价",
            "qty" : "数量",
            "amount" : "金额",
            "remove" : "清除",
            "addMoreTopups" : "更多加价购",
            "subtotal" : "小计:",
            "bookingFee" : "预订费",
            "grandTotal" : "总计:",
            "feautureUnderConstruction" : "这个特征还在建设中！请再查询或订阅我们的电子通讯报获知最新信息。",
            "ok": "确定",
            "personalInformation": "个人信息",
            "name" : "姓名:",
            "asAppearInCC" : "(信用卡上所示的)",
            "email": "电子邮件:",
            "paymentType" : "付款类型:",
            "paymentOptions" : "<option value=''>请选择...</option>
                                <option value='VISA'>VISA</option>
                                <option value='Mastercard'>万事达卡</option>
                                <option value='Amex'>AmericanExpress</option>
                                <option value='JCB'>JCB</option>
                                <option value='China UnionPay'>中国银联</option>",
            "nationality" : "国籍:",
            "nationalityOptions" : "<option value='' selected='selected'>请选择...</option>
                                    <option value='Singaporean'>新加坡</option><option value='Afghan'>阿富汗</option><option value='Albanian'>阿尔巴尼亚</option><option value='Algerian'>阿尔及利亚人</option><option value='American'>美国</option><option value='Andorran'>安道尔</option><option value='Angolan'>安哥拉</option><option value='Antiguans'>安提瓜</option><option value='Argentinean'>阿根廷</option><option value='Armenian'>亚美尼亚</option><option value='Australian'>澳大利亚</option><option value='Austrian'>奥地利</option><option value='Azerbaijani'>阿塞拜疆</option><option value='Bahamian'>巴哈马</option><option value='Bahraini'>巴林</option><option value='Bangladeshi'>孟加拉国</option><option value='Barbadian'>巴巴多斯</option><option value='Barbudans'>巴布达</option><option value='Batswana'>博茨瓦纳</option><option value='Belarusian'>白俄罗斯</option><option value='Belgian'>比利时</option><option value='Belizean'>伯利兹</option><option value='Beninese'>黎巴嫩</option><option value='Bhutanese'>不丹</option><option value='Bolivian'>玻利维亚</option><option value='Bosnian'>波斯尼亚</option><option value='Brazilian'>巴西</option><option value='British'>英国</option><option value='Bruneian'>文莱</option><option value='Bulgarian'>保加利亚</option><option value='Burkinabe'>布基纳法索</option><option value='Burmese'>缅甸</option><option value='Burundian'>布隆迪</option><option value='Cambodian'>柬埔寨</option><option value='Cameroonian'>喀麦隆</option><option value='Canadian'>加拿大</option><option value='Cape Verdean'>佛得角</option><option value='Central African'>中非共和国</option><option value='Chadian'>乍得</option><option value='Chilean'>智利</option><option value='Chinese'>中国</option><option value='Colombian'>哥伦比亚</option><option value='Comoran'>科摩罗</option><option value='Congolese'>刚果</option><option value='Costa Rican'>哥斯达黎加</option><option value='Croatian'>克罗地亚</option><option value='Cuban'>库班</option><option value='Cypriot'>塞</option><option value='Czech'>捷克</option><option value='Danish'>丹麦</option><option value='Djibouti'>吉布提</option><option value='Dominican'>多明尼加</option><option value='Dutch'>荷兰</option><option value='East Timorese'>东帝汶</option><option value='Ecuadorean'>厄瓜多尔</option><option value='Egyptian'>埃及</option><option value='Emirian'>埃米尔</option><option value='Equatorial Guinean'>赤道几内亚</option><option value='Eritrean'>厄立特里亚</option><option value='Estonian'>爱沙尼亚</option><option value='Ethiopian'>埃塞俄比亚</option><option value='Fijian'>斐济</option><option value='Filipino'>菲律宾</option><option value='Finnish'>芬兰</option><option value='French'>法国</option><option value='Gabonese'>加蓬</option><option value='Gambian'>冈比亚</option><option value='Georgian'>格鲁吉亚</option><option value='German'>德国</option><option value='Ghanaian'>加纳</option><option value='Greek'>希腊</option><option value='Grenadian'>格林纳达</option><option value='Guatemalan'>危地马拉</option><option value='Guinea-Bissauan'>几内亚比绍</option><option value='Guinean'>几内亚</option><option value='Guyanese'>圭亚那</option><option value='Haitian'>海地</option><option value='Herzegovinian'>黑塞哥维</option><option value='Honduran'>洪都拉斯</option><option value='Hungarian'>匈牙利</option><option value='I-Kiribati'>-基里巴斯</option><option value='Icelander'>冰岛</option><option value='Indian'>印度</option><option value='Indonesian'>印度尼西亚</option><option value='Iranian'>伊朗</option><option value='Iraqi'>伊拉克</option><option value='Irish'>爱尔兰的</option><option value='Israeli'>以色列</option><option value='Italian'>意大利</option><option value='Ivorian'>科特迪瓦</option><option value='Jamaican'>牙买加</option><option value='Japanese'>日本</option><option value='Jordanian'>约旦</option><option value='Kazakhstani'>哈萨克斯坦</option><option value='Kenyan'>肯亚的</option><option value='Kittian and Nevisian'>ittian和Nevisian</option><option value='Kuwaiti'>科威特</option><option value='Kyrgyz'>吉尔吉斯</option><option value='Laotian'>老挝的</option><option value='Latvian'>拉脱维亚</option><option value='Lebanese'>黎巴嫩</option><option value='Liberian'>利比里亚</option><option value='Libyan'>利比亚</option><option value='Liechtensteiner'>iechtensteine​​r</option><option value='Lithuanian'>立陶宛</option><option value='Luxembourger'>uxembourger</option><option value='Macedonian'>马其顿</option><option value='Malagasy'>马尔加什</option><option value='Malawian'>马拉维</option><option value='Malaysian'>马来西亚</option><option value='Maldivan'>马尔代夫</option><option value='Malian'>马里</option><option value='Maltese'>马耳他</option><option value='Marshallese'>马绍尔</option><option value='Mauritanian'>毛里塔尼亚</option><option value='Mauritian'>毛里求斯</option><option value='Mexican'>墨西哥人</option><option value='Micronesian'>密克罗尼西亚</option><option value='Moldovan'>摩尔多瓦</option><option value='Monacan'>摩纳哥</option><option value='Mongolian'>蒙</option><option value='Moroccan'>摩洛哥</option><option value='Mosotho'>osotho</option><option value='Motswana'>otswana</option><option value='Mozambican'>莫桑比克</option><option value='Namibian'>纳米比亚</option><option value='Nauruan'>瑙鲁</option><option value='Nepalese'>尼泊尔</option><option value='New Zealander'>新西兰</option><option value='Nicaraguan'>尼加拉瓜</option><option value='Nigerian'>尼日利亚</option><option value='Nigerien'>尼日尔</option><option value='North Korean'>朝鲜</option><option value='Northern Irish'>北爱尔兰</option><option value='Norwegian'>挪威</option><option value='Omani'>阿曼</option><option value='Pakistani'>巴基斯坦</option><option value='Palauan'>帕劳</option><option value='Panamanian'>巴拿马</option><option value='Papua New Guinean'>巴布亚新几内亚的</option><option value='Paraguayan'>巴拉圭</option><option value='Peruvian'>秘鲁</option><option value='Polish'>波兰语</option><option value='Portuguese'>葡萄牙</option><option value='Qatari'>卡塔尔</option><option value='Romanian'>罗马尼亚</option><option value='Russian'>俄</option><option value='Rwandan'>卢旺达</option><option value='Saint Lucian'>圣卢西亚</option><option value='Salvadoran'>萨尔瓦多</option><option value='Samoan'>萨摩亚</option><option value='San Marinese'>圣Marinese</option><option value='Sao Tomean'>圣保罗Tomean</option><option value='Saudi'>沙特</option><option value='Scottish'>苏格兰的</option><option value='Senegalese'>塞内加尔</option><option value='Serbian'>塞尔维亚</option><option value='Seychellois'>塞舌尔</option><option value='Sierra Leonean'>塞拉利昂</option><option value='Slovakian'>斯洛伐克</option><option value='Slovenian'>斯洛文尼亚</option><option value='Solomon Islander'>所罗门岛</option><option value='Somali'>索马里</option><option value='South African'>南非的</option><option value='South Korean'>韩国</option><option value='Spanish'>西班牙语</option><option value='Sri Lankan'>斯里兰卡</option><option value='Sudanese'>苏丹人</option><option value='Surinamer'>urinamer</option><option value='Swazi'>斯威士兰</option><option value='Swedish'>瑞典</option><option value='Swiss'>瑞士</option><option value='Syrian'>叙利亚的</option><option value='Taiwanese'>台湾</option><option value='Tajik'>塔吉克</option><option value='Tanzanian'>坦桑尼亚</option><option value='Thai'>泰国</option><option value='Togolese'>多哥</option><option value='Tongan'>汤加</option><option value='Trinidadian or Tobagonian'>特立尼达或Tobagonian</option><option value='Tunisian'>突尼斯</option><option value='Turkish'>土耳其</option><option value='Tuvaluan'>图瓦卢</option><option value='Ugandan'>乌干达</option><option value='Ukrainian'>乌克兰</option><option value='Uruguayan'>乌拉圭</option><option value='Uzbekistani'>乌兹别克斯坦</option><option value='Venezuelan'>委内瑞拉</option><option value='Vietnamese'>越南</option><option value='Welsh'>威尔士</option><option value='Yemenite'>也门</option><option value='Zambian'>赞比亚</option><option value='Zimbabwean'>津巴布韦</option>",
            "whereDidYouHearUs": "您如何知道我们的信息?",
            "whereDidYouHearUsOptions" : "<option value=''>请选择...</option>
                                          <option value='Television Programme'>电视节目</option>
                                          <option value='Internet'>因特网</option>
                                          <option value='Tour Guide Books'>旅游指南书</option>
                                          <option value='Pamphlets/Leaflets'>小册子/传单</option>
                                          <option value='Newspaper/Magazines'>报纸/杂志</option>
                                          <option value='Advertisements'>广告</option>
                                          <option value='Trade/Travel Fairs'>贸易/旅游交易会</option>
                                          <option value='Word-of-mouth'>口碑的</option>
                                          <option value='Hotel Staff'>饭店服务人员</option>
                                          <option value='Hotel Staff'>导游/通讯社</option>
                                          <option value='Tour Guides/Agencies'>出租车司机</option>
                                          <option value='Taxi Driver'>公交车裹</option>
                                          <option value='Buswrap'>户外广告牌</option>
                                          <option value='Outdoor Billboard'>他人</option>
                                          <option value='Others'>其他</option>",
            "dateOfBirth": "出生日期:",
            "contactNo" : "联系方式:",
            "keyInValidEmailAddress" : "请输入有效的电子邮件地址。确认后，我们会将兑换PIN码的收据发送到您的电子邮箱。",
            "fieldsWithAsterisks" : "带有星号",
            "areMandatory" : "的项是必填内容。",
            "wishToReceive" : "我希望接收圣淘沙最新的促销信息和其他信息。",
            "haveAgree" : "我已阅读并同意圣淘沙网购所述",
            "tnc" : "条款和条件",
            "setOutInSentosa" : "Sentosa Leisure Management Pte Ltds (\"SLM\")提供的",
            "dataProtectionPolicy": "个人资料保护政策",
            "includePolicy": "，包括有关可能将如何收集、使用、披露和处理我的个人资料的条款和政策，并确认自己所有的信息和细节是真实、正确和完整的。对于我所提供的除我本人之外的其他人的个人资料，我保证我是其中每一个人的有效代表人，并且取得其个人同意，向SLM披露其个人资料，用于SLM按照个人资料保护政策中所述各项收集、使用、披露和处理他们的个人资料。",
        "proceedToPay": "下一步：付款",
        "bankPromotion" : "银行促销:",
        "selectBank" : "Please select..."
        }
    }
]