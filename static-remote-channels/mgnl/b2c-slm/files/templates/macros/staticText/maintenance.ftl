[#assign staticText =
{"en":
{
"title": "We will be back soon!",
"content": "Our system is currently undergoing maintenance. Please check back shortly."
},
"zh_CN":
{
"title": "We will be back soon!",
"content": "Our system is currently undergoing maintenance. Please check back shortly."
}
}
]
