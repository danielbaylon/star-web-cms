[#assign staticText =
    {"en":
        {
            "confirmation" : "Confirmation",
            "pleaseBookIn" : "Please complete booking in:",
            "item" : "Item",
            "details" : "Details",
            "unitPrice" : "Unit Price",
            "qty" : "Qty",
            "amount" : "Amount",
            "ticketType" : "Ticket Type:",
            "subtotal" : "SUBTOTAL:",
            "grandTotal" : "GRAND TOTAL (Inclusive of GST):",
            "personalInformation": "Personal Information",
            "name" : "Name:",
            "email" : "Email:",
            "paymentType" : "Payment Type:",
            "nationality" : "Nationality:",
            "whereDidYouHearUs": "Where did you hear about us?",
            "dateOfBirth": "Date of Birth:",
            "contactNo" : "Contact No.:",
            "wouldLikeToSubscribe" : "You would like to subscribe to Sentosa eNewsletter.",
            "backToCart" : "Back to Cart",
        "payNow" : "Pay Now",
        "bankPromotion" : "Bank Promotion:"
        },
    "zh_CN":
        {
            "confirmation" : "确认",
            "pleaseBookIn" : "请完成预订:",
            "item" : "票类名称",
            "details" : "详情",
            "unitPrice" : "单价",
            "qty" : "数量",
            "amount" : "金额",
            "ticketType" : "票类:",
            "subtotal" : "小计:",
            "grandTotal" : "总计:",
            "personalInformation": "个人信息",
            "name" : "姓名:",
            "email": "电子邮件:",
            "paymentType" : "付款类型:",
            "nationality" : "国籍:",
            "whereDidYouHearUs": "您如何知道我们的信息?",
            "dateOfBirth": "出生日期:",
            "contactNo" : "联系方式:",
            "wouldLikeToSubscribe" : "你想订阅圣淘沙通讯。",
            "backToCart" : "返回预订单",
        "payNow" : "立即付款",
        "bankPromotion" : "银行促销:"
        }
    }
]
