[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#assign staticText =
    {"en":
        {
            "moreInfo": "MORE INFORMATION",
            "toNAround": "Getting To &amp; Around Sentosa",
            "aboutUs": "About Us",
            "legalNotices": "Legal Notices",
            "dpp": "Data Protection Policy",
            "newsletter": "GET OUR NEWSLETTER",
            "newsletterHint": "Enter your email address to get the latest updates and special promotions delivered straight to you.",
            "enterEmail": "Enter your email",
            "agreement": "I have read and agree to the Sentosa Online Store and Sentosa Leisure Management Pte Ltd's (\"SLM\") <a target=\"_blank\" href=\"${sitePath}/data-protection-policy\">Data Protection Policy</a>, including as to how my personal data may be collected, used, disclosed and processed, and confirm that all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself, I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents, to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out in the Data Protection Policy.",
            "subscribe": "Subscribe",
            "service": "WE ARE HERE TO SERVE YOU",
            "phone": "Our Guest Services Officers are available at <span class=\"call-content\">1800-SENTOSA (736 8672) / +65-6736 8672 |<br>Mon to Sun, from 9am to 6pm</span>",
            "email": "Or you may reach us via email at <a href=\"mailto:guest_services@sentosa.com.sg\" target=\"_top\" class=\"highlight\">guest_services@sentosa.com.sg</a>",
            "followUs": "FOLLOW US",
            "copyright": "Copyright © 2016 Sentosa. All rights reserved."
        },
    "zh_CN":
        {
            "moreInfo": "更多信息",
            "toNAround": "玩转圣淘沙",
            "aboutUs": "关于我们",
            "legalNotices": "法律声明",
            "dpp": "个人资料保护政策",
            "newsletter": "订阅我们的简报",
            "newsletterHint": "输入您的电子邮件地址，以获得直接传送到你最新的更新和特别促销活动。",
            "enterEmail": "输入您的电子邮件",
            "agreement": "我已阅读并同意 Sentosa Leisure Management Pte Ltds (\"SLM\")提供的<a target=\"_blank\" href=\"${sitePath}/data-protection-policy\">个人资料保护政策</a>，包括有关可能将如何收集、使用、披露和处理我的个人资料的条款和政策，并确认自己所有的信息和细节是真实、正确和完整的。对于我所提供的除我本人之外的其他人的个人资料，我保证我是其中每一个人的有效代表人，并且取得其个人同意，向SLM披露其个人资料，用于SLM按照个人资料保护政策中所述各项收集、使用、披露和处理他们的个人资料。",
            "subscribe": "订阅",
            "service": "联系我们",
            "phone": "我们的客户服务人员可在 <span class=\"call-content\">1800-SENTOSA (736 8672) / +65-6736 8672 |<br>星期一至星期日，上午9时至下午6时</span>",
            "email": "您还可以发送电子邮件到 <a href=\"mailto:guest_services@sentosa.com.sg\" target=\"_top\" class=\"highlight\">guest_services@sentosa.com.sg</a>",
            "followUs": "关注我们",
            "copyright": "版权© 2016 圣淘沙。版权所有。"
        }
    }
]
