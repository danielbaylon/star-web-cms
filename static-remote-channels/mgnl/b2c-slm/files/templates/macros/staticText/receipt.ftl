[#assign staticText =
    {"en":
        {
            "transactionStatus" : "Transaction Status",
            "transactionProcessing" : "We are still processing your transaction. Please wait a few moments.",
            "thanksForShopping" : "Thank you for shopping with us!",
            "orderCompleted" : "Your order has been completed. An email will be sent to the address that you have provided (please check your junk folder as well).",
            "continueShopping" : "Continue Shopping",
            "downloadTickets" : "Download Tickets",
            "surveySubmitted" : "Survey submitted. Thank you for filling out our customer survey!",
            "satisfaction" : "Sentosa Online Store Guest Satisfaction",
            "feedback" : "If you have a moment, we'd welcome some feedback.",
            "submit" : "Submit",
            "dateOfPurchase" : "Date Of Purchase:",
            "receiptNumber" : "Receipt Number",
            "confirmationEmail" : "If you do not receive the confirmation email within the next 30 minutes, please contact Sentosa Hotline at 1800-SENTOSA (736 8672) / +65-6736 8672 from 9am to 6pm daily. Alternatively, you may write in to <a href=\"mailto:guest_services@sentosa.com.sg\">guest_services@sentosa.com.sg</a>.",
            "dear" : "Dear",
            "summarizedOrderDetails" : "Your order details are summarized below:",
            "personalInformation": "Personal Information",
            "name" : "Name:",
            "email" : "Email:",
            "paymentType" : "Payment Type:",
            "contactNo" : "Contact No.:",
            "item" : "Item",
            "details" : "Details",
            "unitPrice" : "Unit Price",
            "qty" : "Qty",
            "amount" : "Amount",
            "ticketType" : "Ticket Type:",
            "subtotal" : "SUBTOTAL:",
            "grandTotal" : "GRAND TOTAL (Inclusive of GST):",
            "notes" : "Notes",
            "notesDetails": "<p style=\"font-weight: bold\">For verification purposes, please present the following upon redemption:</p>
            <ol>
            <li>Email Confirmation</li>
            <li>Credit card used for purchase</li>
            </ol>
            <p style=\"font-weight: bold\">For a proxy to collect on your behalf, he/she needs to presents:</p>
            <ol>
            <li>Email Confirmation</li>
            <li>This <a href=\"/dam/jcr:c9111e86-6cd0-4588-b25e-addbaf84d17d/letter-of-auth-slm.pdf\" target=\"_blank\">Letter of Authorisation</a> (duly completed and signed by you)</li>
            <li>Clear photocopy of your photo identification card such as NRIC/Passport/FIN Card</li>
            <li>Proxy's original photo identification</li>
            </ol>
             <p style=\"font-weight: bold\">Important note:<br/>
                Sentosa Development Corporation reserves the right not to release tickets if the above documents are not in order.</p>",
            "tnc" : "Terms and Conditions"
        },
    "zh_CN":
        {
            "transactionStatus" : "交易状况",
            "transactionProcessing" : "我们正在处理您的交易。请稍候。",
            "thanksForShopping" : "感谢您与我们购物！",
            "orderCompleted" : "您的订单已完成。收据和兑换密码将发送到您所提供的电邮地址（若无法在您的收件箱收到该电邮，请查看您的垃圾邮件）。请在兑换门票的时候出示该收据。",
            "continueShopping" : "继续购物",
            "downloadTickets" : "下载门票",
            "surveySubmitted": "问卷已提交",
            "satisfaction" : "Sentosa Online Store Guest Satisfaction",
            "feedback" : "If you have a moment, we'd welcome some feedback.",
            "submit" : "提交",
            "dateOfPurchase" : "付款日期:",
            "receiptNumber" : "收据号码",
            "confirmationEmail" : "如果如果您没有在接下来的30分钟之内收到确认邮件，请在每天上午9时到下午6时联系圣淘沙热线1800-SENTOSA（7368672）/+65-67368672。你还可以写信到<a href=\"mailto:guest_services@sentosa.com.sg\">guest_services@sentosa.com.sg</a>.",
            "dear" : "亲爱的",
            "summarizedOrderDetails" : "您的订单详情:",
            "personalInformation": "个人信息",
            "name" : "姓名:",
            "email": "电子邮件:",
            "paymentType" : "付款类型:",
            "contactNo" : "联系方式:",
            "item" : "票类名称",
            "details" : "详情",
            "unitPrice" : "单价",
            "qty" : "数量",
            "amount" : "金额",
            "ticketType" : "票类:",
            "subtotal" : "小计:",
            "grandTotal" : "总计:",
            "notes" : "注意事项",
            "notesDetails": "<p>这张圣淘沙确认收据有效期为购买日期的3个月内。一旦索取圣淘沙入门票的确认收据，所有的圣淘沙欢乐套票必须在14天内使用。</p>
                            <p>欲委任一名代表索取，他/她必须出示：</p>
                            <ol>
                                <li>影印的确认信函</li>
                                <li><a href=\"/dam/jcr:c9111e86-6cd0-4588-b25e-addbaf84d17d/letter-of-auth-slm.pdf\" target=\"_blank\">由您（购买者)签名的授权信函</a></li>
                                <li>他/她的原本身份证/护照/工作准证</li>
                            </ol>
                            ",
            "tnc" : "条款和条件"
        }
    }
]
