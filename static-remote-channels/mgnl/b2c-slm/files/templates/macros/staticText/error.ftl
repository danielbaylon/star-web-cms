[#assign staticText =
    {"en":
        {
            "message": "Uh oh, something unexpected occurred! Apologies. We are working on this right now.",
            "goToHome": "Go to the home page!"
        },
    "zh_CN":
        {
            "message": "系统发生异常，我们正在尝试解决问题。",
            "goToHome": "返回主页！"
        }
    }
]
