[#include "/b2c-slm/templates/macros/pageInit.ftl"]

[#assign title = cmsfn.decode(content).title!""]
[#assign message = cmsfn.decode(content).message!""]

<div class="terms-conditions">
    <h1 class="heading">${title}</h1>
    <div style="text-align:justify;">${message}</div>
</div>