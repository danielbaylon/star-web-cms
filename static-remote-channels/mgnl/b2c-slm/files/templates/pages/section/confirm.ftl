[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/confirm.ftl"]

<script>
  var jsLang = "${lang?js_string!}";
</script>

<style>
    .personal-information .personal-details-wrapper .personal-details-confirmation li.attribute {
        height:30px;
    }
</style>

<!-- Checkout Process 3 -->

<div class="process-step-3">
    <img src="${themePath}/img/process/process-confirm-${imgSuffix}.jpg">
</div>

<!-- Checkout Section -->


<section>

    <!-- Confirmation Section -->

    <div class="checkout-section">
        <div class="checkout-header">
            <h3 class="heading confirm">${staticText[lang]["confirmation"]!"Confirmation"}</h3>
            <p class="countdown">${staticText[lang]["pleaseBookIn"]!"Please complete booking in:"} <span class="time"><span id="confirmTimer">00:00</span> minutes.</span></p>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="checkout-items" id="theCheckout">

        <!-- ko foreach:items -->
        <div class="heading" data-bind="text: baseModel.title"></div>
        <table class="tickets">
            <tr>
                <th width="35%">${staticText[lang]["item"]!"Item"}</th>
                <th width="20%">${staticText[lang]["details"]!"Details"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="10%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span data-bind="text: title"></span>
                </td>
                <td>
                    <span data-bind="html: description"></span>
                    <br>
                    <span data-bind="visible: discountTotal > 0, text: 'Discount: ' + discountLabel" style="color: red;"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize">
                    <span data-bind="text: totalText"></span><br data-bind="if: discountTotal > 0">
                    <span data-bind="visible: discountTotal > 0, text: 'S$ ' + Number(discountTotal + total).toFixed(2)"
                          style="text-decoration: line-through; font-style: italic;"></span>
                </td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="35%" class="items-add-topups">
                    <!-- ko if:baseModel.hasTopup -->
                    <a class="items-add-topups-hyperlink" data-bind="attr:{'href':createTopupUrl()}">${staticText[lang]["addMoreTopups"]!"add more top-ups"}</a>
                    <!-- /ko -->
                </td>
                <td width="40%" class="title" colspan="3">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: baseModel.subtotalText"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- Compact -->
        <table class="tickets s-tickets size-s">
            <tr>
                <th class="col-item">${staticText[lang]["item"]!"Item"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span style="font-weight: bold" data-bind="text: title"></span><br/>
                    <span data-bind="html: description"></span><br>
                    <span data-bind="visible: discountTotal > 0, text: 'Discount: ' + discountLabel" style="color: red;"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize">
                    <span data-bind="text: totalText"></span><br data-bind="if: discountTotal > 0">
                    <span data-bind="visible: discountTotal > 0, text: 'S$ ' + Number(discountTotal + total).toFixed(2)"
                          style="text-decoration: line-through; font-style: italic;"></span>
                </td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="80%" class="title" colspan="2">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: baseModel.subtotalText"></td>
            </tr>
        </table>
        <!-- /ko -->

        <!-- ko if:hasBookingFee -->
        <div class="heading" >${staticText[lang]["bookingFee"]!"Booking Fee"} </div>
        <table class="tickets">
            <tr>
                <th width="50%">${staticText[lang]["bookingFee"]!"Booking Fee"} </th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
                <th width="5%" class="centerlize">&nbsp;</th>
            </tr>
            <tbody>
            <tr>
                <td><span data-bind="text: bookFeeMode"></span></td>
                <td class="centerlize" data-bind="text: bookPriceText"></td>
                <td class="centerlize" data-bind="text: bookFeeQty"></td>
                <td class="centerlize" data-bind="text: bookFeeSubTotalText()"></td>
                <td class="centerlize">&nbsp;</td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="55%" class="title waiveDiv">

                </td>
                <td width="20%" colspan="2" class="title">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount"  data-bind="text: bookFeeSubTotalText()"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <div class="grandtotal-section">
            <div class="buy-gift">
            </div>
            <div class="grandtotal">
            ${staticText[lang]["grandTotal"]!"Grand Total:"}
                <span class="amount" data-bind="text: totalText()">S$ 64.90</span>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>

    <div class="mktg-bottom">

        [@cms.area name="adBanner_c1"/]

    </div>

    <div class="personal-information" id="personalInfo">
        <div class="heading-wrapper">
            <h3 class="heading">${staticText[lang]["personalInformation"]!"Personal Information"}</h3>
        </div>
        <div class="personal-details-wrapper">
            <ul class="personal-details-confirmation">
                <li class="title">${staticText[lang]["name"]!"Name:"}</li>
                <li class="attribute" data-bind="text: name"></li>
                <li class="title">${staticText[lang]["email"]!"Email:"}</li>
                <li class="attribute" data-bind="text: email">sofia.luo@enovax.com</li>
                <!--
                <li class="title">ID:</li>
                <li class="attribute">(Passport) </li>
                -->
                <li class="title">${staticText[lang]["nationality"]!"Nationality:"}</li>
                <li class="attribute"  data-bind="text: nationality"></li>
                <li class="title">${staticText[lang]["paymentType"]!"Payment Type:"}</li>
                <li class="attribute"  data-bind="text: paymentType"></li>
                <li class="title">${staticText[lang]["whereDidYouHearUs"]!"Where did you hear about us?"}</li>
                <li class="attribute"  data-bind="text: referSource"></li>
              <!-- ko if: selectedMerchant() != null && selectedMerchant() != '' -->
              <li class="title">${staticText[lang]["bankPromotion"]!"Bank Promotion:"}</li>
              <li class="attribute" data-bind="text: selectedMerchant"></li>
              <!-- /ko -->
                <li class="title">${staticText[lang]["dateOfBirth"]!"Date of Birth:"}</li>
                <li class="attribute"  data-bind="text: dob"></li>
                <li class="title">${staticText[lang]["contactNo"]!"Contact No.:"}</li>
                <li class="attribute"  data-bind="text: mobile"></li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="subscribe-option" data-bind="visible: subscribed">${staticText[lang]["wouldLikeToSubscribe"]!"You would like to subscribe to Sentosa eNewsletter."}</div>

    </div>

    <div class="btn-confirmation">
        <div class="button">
            <a class="back-to-cart" id="btnCancel">${staticText[lang]["backToCart"]!"Back to Cart"}</a>
            <a class="checkout-btn" id="btnPay">${staticText[lang]["payNow"]!"Pay Now"}</a>
            <div class="clearfix"></div>
        </div>
    </div>


    <div class="mktg-bottom">

        <!--<a href="https://islander.com.sg/" target="_blank" onclick="return logAdClick('Confirmation Page', 'https://islander.com.sg/');"><img src="${themePath}/img/static/mktg/b279df098f0d42dc1e266ed6336fcf35.jpg" ></a>-->
        [@cms.area name="adBanner"/]

    </div>

</section>
