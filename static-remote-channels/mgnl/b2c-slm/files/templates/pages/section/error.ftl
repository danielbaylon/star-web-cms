[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/error.ftl"]

[#assign text = cmsfn.decode(content).text!staticText[lang]["message"]!"Uh oh, something unexpected occurred! Apologies. We are working on this right now."]

<div class="general-info-section">

    <h1 class="title" id="titleDefault">${text}</h1>
    <p class="content"><a href="${sitePath}">${staticText[lang]["goToHome"]!"Go to the home page!"}</a></p>

</div>