[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/checkout.ftl"]

<script>
  var jsLang = "${lang?js_string!}";
</script>

<!-- Checkout Process 2 -->

<div class="process-step-2">
    <img src="${themePath}/img/process/process-checkout-${imgSuffix}.jpg">
</div>

<div class="s-process-checkout size-s">
    <img src="${themePath}/img/process/process-checkout-s-${imgSuffix}.png">
</div>

<!-- Checkout Section -->



<section>

    <div class="checkout-section">
        <div class="checkout-header">
            <h3 class="heading">${staticText[lang]["checkout"]!"Checkout"}</h3>
            <a class="continue-btn" href="${sitePath}">${staticText[lang]["continueShopping"]!"Continue Shopping"}</a>
            <div class="clearfix"></div>
        </div>
        <div class="clear-cart">
            <a class="clear-btn" id="btnClearCart">${staticText[lang]["clearCart"]!"Clear Cart"}</a>
            <div class="clearfix"></div>
        </div>
    </div>



    <div class="checkout-items" id="theCheckout">


        <!-- ko foreach:items -->
        <div class="heading" data-bind="text: baseModel.title"></div>
        <table class="tickets">
            <tr>
                <th width="30%">${staticText[lang]["item"]!"Item"}</th>
                <th width="20%">${staticText[lang]["details"]!"Details"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="10%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
                <th width="10%" class="centerlize">${staticText[lang]["remove"]!"Remove"}</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                  <div>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span data-bind="text: title"></span>
                  </div>
                  <div data-bind="foreach: {data: promoLabels, as: 'promoLabel'}">
                        <span style="color: #f9383f; font-size: 13px; font-weight: bold; border: 1px solid #ffd3d6;
                                            background-color: #fff8f8; border-radius: 4px; -webkit-border-radius: 4px;  -moz-border-radius: 4px;
                                            padding: 1px 5px;">
                            <span style="font-size: 0.9em; vertical-align: middle;" data-bind="text: promoLabel"></span>
                        </span>
                    <br>
                  </div>
                </td>
                <td>
                    <span data-bind="html: description"></span><br>
                    <span data-bind="visible: discountTotal > 0, text: 'Discount: ' + discountLabel" style="color: red;"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize">
                    <span data-bind="text: totalText"></span><br data-bind="if: discountTotal > 0">
                    <span data-bind="visible: discountTotal > 0, text: 'S$ ' + Number(discountTotal + total).toFixed(2)"
                          style="text-decoration: line-through; font-style: italic;"></span>
                </td>
                <td class="centerlize">&nbsp;&nbsp;&nbsp;<a data-bind="click: function(model, evt){ $root.doRemove(model, evt, cartId); }" title="Remove this item"><i class="fw-icon-remove"></i></a>&nbsp;&nbsp;&nbsp;</td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="35%" class="items-add-topups">
                    <!-- ko if:baseModel.hasTopup -->
                    <a class="items-add-topups-hyperlink" data-bind="attr:{'href':createTopupUrl()}">${staticText[lang]["addMoreTopups"]!"add more top-ups"}</a>
                    <!-- /ko -->
                </td>
                <td width="40%" class="title" colspan="3">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: baseModel.subtotalText"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- Compact -->
        <table class="tickets s-tickets size-s">
            <tr>
                <th class="col-item">${staticText[lang]["item"]!"Item"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
                <th width="5%" class="centerlize">&nbsp;</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span style="font-weight: bold" data-bind="text: title"></span><br/>
                    <span data-bind="html: description"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize" data-bind="text: 'S$ ' + totalText"></td>
                <td class="centerlize"><a data-bind="click: function(model, evt){ $root.doRemove(model, evt, cartId); }" title="Remove this item"><i class="fw-icon-remove"></i></a></td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="55%" class="items-add-topups">
                    <!-- ko if:baseModel.hasTopup -->
                    <a class="items-add-topups-hyperlink" data-bind="attr:{'href':createTopupUrl()}">${staticText[lang]["addMoreTopups"]!"add more top-ups"}</a>
                    <!-- /ko -->
                </td>
                <td width="20%" class="title" colspan="2">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: 'S$ ' + baseModel.subtotalText"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <!-- ko if:hasBookingFee -->
        <div class="heading" >${staticText[lang]["bookingFee"]!"Booking Fee"} </div>
        <table class="tickets">
            <tr>
                <th width="50%">${staticText[lang]["bookingFee"]!"Booking Fee"} </th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
                <th width="5%" class="centerlize">&nbsp;</th>
            </tr>
            <tbody>
            <tr>
                <td><span data-bind="text: bookFeeMode"></span></td>
                <td class="centerlize" data-bind="text: bookPriceText"></td>
                <td class="centerlize" data-bind="text: bookFeeQty"></td>
                <td class="centerlize" data-bind="text: 'S$ ' + bookFeeSubTotalText()"></td>
                <td class="centerlize">&nbsp;</td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="55%" class="title waiveDiv">

                </td>
                <td width="20%" colspan="2" class="title">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount"  data-bind="text: 'S$ ' + bookFeeSubTotalText()"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <div class="grandtotal-section">
            <div class="grandtotal">
            ${staticText[lang]["grandTotal"]!"Grand Total:"}
                <span class="amount" data-bind="text: 'S$ ' + totalText()">S$ 64.90</span>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>

    <div class="notification-section" id="msgBuyThis" style="display:none;">
        <div class="message-area">
        ${staticText[lang]["feautureUnderConstruction"]!"This feature is still under construction! Check back again or subscribe to our e-newsletter to be updated."}
        </div>
        <div class="close-section"><span class="close" id="btnCloseBuyThis">${staticText[lang]["ok"]!"OK"}</span></div>
    </div>

    <div class="mktg-bottom">

    [@cms.area name="adBanner_c1"/]

    </div>

    <section id="personalInfo">
        <form action="${sitePath}/confirm" method="post" id="deetsForm">
          <div class="promo-bar">
            <div class="promo-form">
              <div class="promo-header">AVAILABLE PROMOTION</div>
              <div class="arrow-right"></div>
              <div class="promo-field">
                <span class="promo-title">Promo Code:</span>
                <input type="text" class="field" data-bind="value: promoCode"/>
                <button data-bind="click: applyPromoCode">Apply</button>
              </div>
              <!-- ko if: hasPromoMerchant-->
              <div class="promo-field">
                <span class="promo-title">Bank Deals:</span>
                <select name="cust.merchantId" data-bind="
                                options: promoMerchants,
                                optionsText: function(item) {return item ? item.name : ''},
                                optionsValue: function(item) {return item ? item.merchantId : ''},
                                value: merchantId,
                                optionsCaption: '${staticText[lang]["selectBank"]!"Select Bank..."}',
                                event:{ change: changeMerchant}
                                "></select>
              </div>
              <!-- /ko -->
              <div class="promo-field">
                <span class="promo-title">Payment Type:</span>
                <select name="cust.paymentType" data-bind="value: paymentType,
                                event:{ change: changePaymentType}">
                ${staticText[lang]["paymentOptions"]!'
                        <option value="">Please select...</option>
                        <option value="Visa">Visa</option>
                        <option value="Mastercard">Mastercard</option>
                        <option value="Amex">American Express</option>
                        <option value="JCB">JCB</option>
                        <option value="ChinaUnionPay">China UnionPay</option>
                        '}
                </select>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <input type="hidden" value="" name="bfw" id="bfw" />
            <div class="personal-information">
                <div class="heading-wrapper">
                    <h3 class="heading">${staticText[lang]["personalInformation"]!"Personal Information"}</h3>
                </div>
                <div class="message-section success" data-bind="visible: payMethod != '', html: message"></div>
                <div class="message-section error" style="display:none" data-bind="visible: isErr, html: errMsg"></div>
                <div class="personal-details-wrapper">
                    <ul class="personal-details">
                        <li class="title" style="line-height: inherit">
                <span class="top-liner" style="
                line-height: 18px;
                height: 18px;
                display: block;"><span class="star">*</span>${staticText[lang]["name"]!"Name:"}</span>
                <span class="red-liner" style="
                line-height: 12px;
                height: 12px;
                font-size: 12px;
                color: #C20202;
                display: block;
                padding-left: 5px;">${staticText[lang]["asAppearInCC"]!"(as appears on credit card)"}</span>
                        </li>
                        <li class="attribute">
                            <input type="text" name="cust.name" class="input-attribute" data-bind="value: name" />
                        </li>
                        <li class="title">
                            <span class="star">*</span>${staticText[lang]["email"]!"Email:"}
                        </li>
                        <li class="attribute">
                            <input type="text" name="cust.email" class="input-attribute" data-bind="value: email" />
                        </li>
                        <!--
                        <li class="title">
                            <span class="star">*</span>ID:
                            <div class="nric-passport">
                                <div class="identify">
                                    <input type="radio" id="nric-fin" name="cust.idType" value="NricFin" data-bind="checked: idType" /><label for="nric-fin">NRIC^/FIN</label>
                                    <input type="radio" id="passport" name="cust.idType" value="Passport" data-bind="checked: idType"  /><label for="passport">Passport</label>
                                </div>
                                <div class="note">^For Singaporeans and PRs only</div>
                            </div>
                        </li>
                        <li class="attribute">
                                <input type="text" name="cust.idNo" data-bind="value: idNo" class="input-attribute" />
                        </li>
                         -->
                        <li class="title">
                            <span class="star">*</span>${staticText[lang]["paymentType"]!"Payment Type:"}
                        </li>
                        <li class="attribute">
                            <!-- ko if:payMethod == '' -->
                            <div class="select-attribute">
                                <!--
                                <input type="radio" name="thePaymentType" value="VISA" data-bind="checked: paymentType"> VISA <br>
                                <input type="radio" name="thePaymentType" value="Mastercard" data-bind="checked: paymentType"> Mastercard <br>
                                <input type="radio" name="thePaymentType" value="China UnionPay" data-bind="checked: paymentType"> China UnionPay <br>
                                -->
                              <select name="cust.paymentType" data-bind="value: paymentType,
                                event:{ change: changePaymentType}">
                                ${staticText[lang]["paymentOptions"]!'
                        <option value="">Please select...</option>
                        <option value="Visa">Visa</option>
                        <option value="Mastercard">Mastercard</option>
                        <option value="Amex">American Express</option>
                        <option value="JCB">JCB</option>
                        <option value="ChinaUnionPay">China UnionPay</option>
                        '}
                                </select>
                            </div>
                            <!-- /ko --><!-- ko ifnot:payMethod == '' -->
                            <span data-bind="text: paymentTypeLabel"></span>
                            <input type="hidden" name="cust.paymentType" data-bind="value: paymentType">
                            <!-- /ko -->
                        </li>
                        <li class="title">
                            <span class="star">*</span>${staticText[lang]["nationality"]!"Nationality:"}
                        </li>
                        <li class="attribute">
                            <div class="select-attribute">
                                <select name="cust.nationality" data-bind="value: nationality">
                                ${staticText[lang]["nationalityOptions"]!'
                        <option value="" selected="selected">Please select...</option>
                        <option value="Singaporean">Singaporean</option><option value="Afghan">Afghan</option><option value="Albanian">Albanian</option><option value="Algerian">Algerian</option><option value="American">American</option><option value="Andorran">Andorran</option><option value="Angolan">Angolan</option><option value="Antiguans">Antiguans</option><option value="Argentinean">Argentinean</option><option value="Armenian">Armenian</option><option value="Australian">Australian</option><option value="Austrian">Austrian</option><option value="Azerbaijani">Azerbaijani</option><option value="Bahamian">Bahamian</option><option value="Bahraini">Bahraini</option><option value="Bangladeshi">Bangladeshi</option><option value="Barbadian">Barbadian</option><option value="Barbudans">Barbudans</option><option value="Batswana">Batswana</option><option value="Belarusian">Belarusian</option><option value="Belgian">Belgian</option><option value="Belizean">Belizean</option><option value="Beninese">Beninese</option><option value="Bhutanese">Bhutanese</option><option value="Bolivian">Bolivian</option><option value="Bosnian">Bosnian</option><option value="Brazilian">Brazilian</option><option value="British">British</option><option value="Bruneian">Bruneian</option><option value="Bulgarian">Bulgarian</option><option value="Burkinabe">Burkinabe</option><option value="Burmese">Burmese</option><option value="Burundian">Burundian</option><option value="Cambodian">Cambodian</option><option value="Cameroonian">Cameroonian</option><option value="Canadian">Canadian</option><option value="Cape Verdean">Cape Verdean</option><option value="Central African">Central African</option><option value="Chadian">Chadian</option><option value="Chilean">Chilean</option><option value="Chinese">Chinese</option><option value="Colombian">Colombian</option><option value="Comoran">Comoran</option><option value="Congolese">Congolese</option><option value="Costa Rican">Costa Rican</option><option value="Croatian">Croatian</option><option value="Cuban">Cuban</option><option value="Cypriot">Cypriot</option><option value="Czech">Czech</option><option value="Danish">Danish</option><option value="Djibouti">Djibouti</option><option value="Dominican">Dominican</option><option value="Dutch">Dutch</option><option value="East Timorese">East Timorese</option><option value="Ecuadorean">Ecuadorean</option><option value="Egyptian">Egyptian</option><option value="Emirian">Emirian</option><option value="Equatorial Guinean">Equatorial Guinean</option><option value="Eritrean">Eritrean</option><option value="Estonian">Estonian</option><option value="Ethiopian">Ethiopian</option><option value="Fijian">Fijian</option><option value="Filipino">Filipino</option><option value="Finnish">Finnish</option><option value="French">French</option><option value="Gabonese">Gabonese</option><option value="Gambian">Gambian</option><option value="Georgian">Georgian</option><option value="German">German</option><option value="Ghanaian">Ghanaian</option><option value="Greek">Greek</option><option value="Grenadian">Grenadian</option><option value="Guatemalan">Guatemalan</option><option value="Guinea-Bissauan">Guinea-Bissauan</option><option value="Guinean">Guinean</option><option value="Guyanese">Guyanese</option><option value="Haitian">Haitian</option><option value="Herzegovinian">Herzegovinian</option><option value="Honduran">Honduran</option><option value="Hungarian">Hungarian</option><option value="I-Kiribati">I-Kiribati</option><option value="Icelander">Icelander</option><option value="Indian">Indian</option><option value="Indonesian">Indonesian</option><option value="Iranian">Iranian</option><option value="Iraqi">Iraqi</option><option value="Irish">Irish</option><option value="Israeli">Israeli</option><option value="Italian">Italian</option><option value="Ivorian">Ivorian</option><option value="Jamaican">Jamaican</option><option value="Japanese">Japanese</option><option value="Jordanian">Jordanian</option><option value="Kazakhstani">Kazakhstani</option><option value="Kenyan">Kenyan</option><option value="Kittian and Nevisian">Kittian and Nevisian</option><option value="Kuwaiti">Kuwaiti</option><option value="Kyrgyz">Kyrgyz</option><option value="Laotian">Laotian</option><option value="Latvian">Latvian</option><option value="Lebanese">Lebanese</option><option value="Liberian">Liberian</option><option value="Libyan">Libyan</option><option value="Liechtensteiner">Liechtensteiner</option><option value="Lithuanian">Lithuanian</option><option value="Luxembourger">Luxembourger</option><option value="Macedonian">Macedonian</option><option value="Malagasy">Malagasy</option><option value="Malawian">Malawian</option><option value="Malaysian">Malaysian</option><option value="Maldivan">Maldivan</option><option value="Malian">Malian</option><option value="Maltese">Maltese</option><option value="Marshallese">Marshallese</option><option value="Mauritanian">Mauritanian</option><option value="Mauritian">Mauritian</option><option value="Mexican">Mexican</option><option value="Micronesian">Micronesian</option><option value="Moldovan">Moldovan</option><option value="Monacan">Monacan</option><option value="Mongolian">Mongolian</option><option value="Moroccan">Moroccan</option><option value="Mosotho">Mosotho</option><option value="Motswana">Motswana</option><option value="Mozambican">Mozambican</option><option value="Namibian">Namibian</option><option value="Nauruan">Nauruan</option><option value="Nepalese">Nepalese</option><option value="New Zealander">New Zealander</option><option value="Nicaraguan">Nicaraguan</option><option value="Nigerian">Nigerian</option><option value="Nigerien">Nigerien</option><option value="North Korean">North Korean</option><option value="Northern Irish">Northern Irish</option><option value="Norwegian">Norwegian</option><option value="Omani">Omani</option><option value="Pakistani">Pakistani</option><option value="Palauan">Palauan</option><option value="Panamanian">Panamanian</option><option value="Papua New Guinean">Papua New Guinean</option><option value="Paraguayan">Paraguayan</option><option value="Peruvian">Peruvian</option><option value="Polish">Polish</option><option value="Portuguese">Portuguese</option><option value="Qatari">Qatari</option><option value="Romanian">Romanian</option><option value="Russian">Russian</option><option value="Rwandan">Rwandan</option><option value="Saint Lucian">Saint Lucian</option><option value="Salvadoran">Salvadoran</option><option value="Samoan">Samoan</option><option value="San Marinese">San Marinese</option><option value="Sao Tomean">Sao Tomean</option><option value="Saudi">Saudi</option><option value="Scottish">Scottish</option><option value="Senegalese">Senegalese</option><option value="Serbian">Serbian</option><option value="Seychellois">Seychellois</option><option value="Sierra Leonean">Sierra Leonean</option><option value="Slovakian">Slovakian</option><option value="Slovenian">Slovenian</option><option value="Solomon Islander">Solomon Islander</option><option value="Somali">Somali</option><option value="South African">South African</option><option value="South Korean">South Korean</option><option value="Spanish">Spanish</option><option value="Sri Lankan">Sri Lankan</option><option value="Sudanese">Sudanese</option><option value="Surinamer">Surinamer</option><option value="Swazi">Swazi</option><option value="Swedish">Swedish</option><option value="Swiss">Swiss</option><option value="Syrian">Syrian</option><option value="Taiwanese">Taiwanese</option><option value="Tajik">Tajik</option><option value="Tanzanian">Tanzanian</option><option value="Thai">Thai</option><option value="Togolese">Togolese</option><option value="Tongan">Tongan</option><option value="Trinidadian or Tobagonian">Trinidadian or Tobagonian</option><option value="Tunisian">Tunisian</option><option value="Turkish">Turkish</option><option value="Tuvaluan">Tuvaluan</option><option value="Ugandan">Ugandan</option><option value="Ukrainian">Ukrainian</option><option value="Uruguayan">Uruguayan</option><option value="Uzbekistani">Uzbekistani</option><option value="Venezuelan">Venezuelan</option><option value="Vietnamese">Vietnamese</option><option value="Welsh">Welsh</option><option value="Yemenite">Yemenite</option><option value="Zambian">Zambian</option><option value="Zimbabwean">Zimbabwean</option>
                        '}
                                </select>
                            </div>
                        </li>
                      <!-- ko if: hasPromoMerchant-->
                      <li class="title">
                      ${staticText[lang]["bankPromotion"]!"Bank Promotion:"}
                      </li>
                      <li class="attribute">
                        <div class="select-attribute">
                          <select name="cust.merchantId" data-bind="
                                options: promoMerchants,
                                optionsText: function(item) {return item ? item.name : ''},
                                optionsValue: function(item) {return item ? item.merchantId : ''},
                                value: merchantId,
                                optionsCaption: '${staticText[lang]["selectBank"]!"Select Bank..."}',
                                event:{ change: changeMerchant}
                                "></select>
                        </div>
                      </li>
                      <!-- /ko -->
                        <li class="title">
                            <span class="star">*</span>${staticText[lang]["whereDidYouHearUs"]!"Where did you hear about us?"}
                        </li>
                        <li class="attribute">
                            <div class="select-attribute">
                                <select name="cust.referSource" data-bind="value: referSource">
                                ${staticText[lang]["whereDidYouHearUsOptions"]!'
                        <option value="">Please select...</option>
                        <option value="Television Programme">Television Programme</option><option value="Internet">Internet</option><option value="Tour Guide Books">Tour Guide Books</option><option value="Pamphlets/Leaflets">Pamphlets/Leaflets</option><option value="Newspaper/Magazines">Newspaper/Magazines</option><option value="Advertisements">Advertisements</option><option value="Trade/Travel Fairs">Trade/Travel Fairs</option><option value="Word-of-mouth">Word-of-mouth</option><option value="Hotel Staff">Hotel Staff</option><option value="Tour Guides/Agencies">Tour Guides/Agencies</option><option value="Taxi Driver">Taxi Driver</option><option value="Buswrap">Buswrap</option><option value="Outdoor Billboard">Outdoor Billboard</option><option value="Others">Others</option>
                        '}
                                </select>
                            </div>
                        </li>
                        <li class="title">
                            <span class="star"></span>${staticText[lang]["dateOfBirth"]!"Date of Birth:"}
                        </li>
                        <li class="attribute">
                            <input type="text" name="cust.dob" data-bind="value: dob" id="dob" class="input-attribute" />
                            <i class="fw-icon-calendar" data-bind="click: dobCalClick"></i>
                        </li>
                        <li class="title">
                            <span class="star">*</span>${staticText[lang]["contactNo"]!"Contact No.:"}
                        </li>
                        <li class="attribute">
                            <input type="text" name="cust.mobile" maxlength="15" class="input-attribute" data-bind="value: mobile"  />
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="mandatory-fields">
                        <div class="title" style="font-weight: bold; font-size: 1.1em; color: #A20000">${staticText[lang]["keyInValidEmailAddress"]!"Please key in a valid email address. Upon confirmation, a receipt with a redemption PIN code will be sent to your email."}</div>
                        <div class="title">${staticText[lang]["fieldsWithAsterisks"]!"Fields with asterisks"} (<span class="star">*</span>) ${staticText[lang]["areMandatory"]!"are mandatory."}</div>
                    </div>
                </div>
                <div class="subscribe-option">
                    <label for="subChk">
                        <input type="checkbox" id="subChk" value="true" name="cust.subscribed" data-bind="checked: subscribed"/>${staticText[lang]["wishToReceive"]!"I wish to receive the latest promotions and updates from Sentosa."}
                    </label>
                </div>
                <div class="t-c">
                    <label for="tncChk">
                        <input type="checkbox" id="tncChk" data-bind="checked: tnc" /><span class="star">*</span>
                    ${staticText[lang]["haveAgree"]!"I have read and agree to both the"}  <a target="_blank"
                                                                                             href="./tnc"
                                                                                             data-bind="click: goToTnc">${staticText[lang]["tnc"]!"Terms and Conditions"}</a> ${staticText[lang]["setOutInSentosa"]!"as set out in the Sentosa Online Store and Sentosa Leisure Management Pte Ltd's (\"SLM\")"}
                      <a target="_blank"
                         href="./data-protection-policy">${staticText[lang]["dataProtectionPolicy"]!"Data Protection Policy"}</a>${staticText[lang]["includePolicy"]!", including as to how my personal data may be collected, used, disclosed and processed, and confirm that all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself, I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents, to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out in the Data Protection Policy."}
                    </label>
                </div>
            </div>

            <div class="btn-proceed">
                <a class="continue-btn" data-bind="click: doCheckout">${staticText[lang]["proceedToPay"]!"Proceed to Pay"}</a>
                <div class="clearfix"></div>
            </div>
        </form>


        <div class="mktg-bottom">

            <!--<a href="https://islander.com.sg/" target="_blank" onclick="return logAdClick('Checkout Page', 'https://islander.com.sg/');"><img src="${themePath}/img/static/mktg/b279df098f0d42dc1e266ed6336fcf35.jpg"></a>-->
        [@cms.area name="adBanner"/]

        </div>

    </section>

</section>
