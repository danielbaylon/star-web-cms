[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/tnc.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign generalTncs = starfn.getGeneralTncContent("b2c-slm")]
[#if params?has_content]
    [#assign productTncs = starfn.getProductTncContent("b2c-slm",params)!]
[/#if]
[#assign hasTnc = generalTncs?has_content || productTnc?has_content]

<div class="terms-conditions">
  <h1 class="heading">${staticText[lang]["tnc"]!"Terms and Conditions"}</h1>
[#if hasTnc]
  <div style="text-align:justify;">
    [#if generalTncs?has_content]
        [#list generalTncs as generalTnc]
          <h4 class="title">${generalTnc.title?html!""}</h4>
          <div class="the-tnc">${generalTnc.content!""}</div>
        [/#list]
    [/#if]
      [#if productTncs?has_content]
          [#list productTncs as productTnc]
            <h4 class="title">${productTnc.title?html!""}</h4>

            <div class="the-tnc">${productTnc.content!""}</div>
          [/#list]
    [/#if]
</div>
[#else]
  <div style="text-align:center;font-size:2em;margin-top:2em;font-style:italic">
  ${staticText[lang]["noTncAvailable"]!"No Terms &amp; Conditions available."}
  </div>
[/#if]
</div>