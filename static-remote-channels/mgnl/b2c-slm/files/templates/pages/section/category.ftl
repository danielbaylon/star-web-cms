[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/category.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign channel = 'b2c-slm']
[#assign cat = params[1]!]
[#if cat?has_content]
    [#assign catNode = starfn.getCMSProductCategory(channel, cat)!]
[/#if]

[#if catNode?has_content]
    [#assign catName = ""]
    [#assign catUrlPath = ""]
    [#if catNode.hasProperty("name")]
        [#assign catName = catNode.getProperty("name").getString()]
    [/#if]
    [#if catNode.hasProperty("generatedURLPath")]
        [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
    [/#if]
    [#assign subCats = starfn.getCMSProductSubCategories(catNode)!]
    [#assign hasItemDisplayed = false]
    [#list subCats as subCat]
        [#assign cmsPrds = starfn.getCMSProductBySubCategory(subCat)!]
    <div class="list">
        [#if subCat.getName() != "default"]
            <h3>${subCat.getProperty("name").getString()!}</h3>
        [/#if]
        <ul class="panels">
            [#list cmsPrds as cmsPrd]
                [#assign hasItemDisplayed = true]
                [#assign cmsProductName = ""]
                [#assign cmsProductShortDesc = ""]
                [#assign cmsProductUrl = "javascript:void(0);"]
                [#if cmsPrd.hasProperty("name")]
                    [#assign cmsProductName = cmsPrd.getProperty("name").getString()]
                [/#if]
                [#if cmsPrd.hasProperty("shortDescription")]
                    [#assign cmsProductShortDesc = cmsPrd.getProperty("shortDescription").getString()]
                [/#if]
                [#if cmsPrd.hasProperty("generatedURLPath")]
                    [#assign cmsProductUrl = sitePath + "/category/product~" + cmsPrd.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${cmsPrd.getName()}~.html?category=${catUrlPath}"]
                [/#if]
                [#assign thumbnailAssetLink = ""]
                [#assign cmsProductImages = starfn.getCMSProductImagesUUID(cmsPrd, lang)!]

                [#if cmsProductImages?has_content]
                    [#assign cmsProductImage = cmsProductImages[0]]
                    [#if cmsProductImage?has_content]
                        [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)!]
                    [/#if]
                [/#if]


                [#assign lowestPrice = starfn.getCMSProductLowestTicketPrice(cmsPrd)]
                <li class="panel">
                    <a href="${cmsProductUrl}" class="images">
                        <img src="${thumbnailAssetLink}" alt="${cmsProductName}" />
                    </a>
                    <a href="${cmsProductUrl}" class="heading" style="padding-top:5px">${cmsProductName}</a>
                    <p class="description">${cmsProductShortDesc}</p>
                    <a class="buy" href="${cmsProductUrl}">
                        <span class="from">From</span><span class="price">S$${lowestPrice}</span><span class="buy-now">Buy Now!</span>
                    </a>
                </li>
            [/#list]
        </ul>
        <div class="clearfix"></div>
    </div>
    [/#list]

    [#if !hasItemDisplayed]
    <div style="margin:20px;text-align:center;">No items under this category.</div>
    [/#if]
[/#if]