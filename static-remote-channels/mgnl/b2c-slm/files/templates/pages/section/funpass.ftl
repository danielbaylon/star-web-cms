[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/funpass.ftl"]

<div id="app"  style="height: 100%;" >
</div>
<script id="funpassLandTemp" type="text/x-kendo-template">
    <div id='homeDv' class="top-wrapper index">
        <div class="container-fluid">
            <div class="inner-wrapper">
                <div class="btn-group lang">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-bind="text: lang().optionText">
                        English <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" data-bind="foreach: languages">
                        <li><a href="\#" data-bind="text: optionText,click: clickLan">English</a></li>
                    </ul>
                </div>
                <div class="begin-section">
                    <div class="text-center logo"><img src="${themePath}/img/funpass/logo/home-logo-${lang}.png"/></div>
                    <div class="text-center title"><img src="${themePath}/img/funpass/title/home-title-${lang}.png"/></div>
                    <div class="btn-container text-center">
                        <button type="button" class="btn-begin" style="background-image:url(${themePath}/img/funpass/begin/btn-begin-${lang}.png)"   data-bind="click: next" ></button>
                    </div>
                </div>
            </div>
        </div>
        <div class='broswerTip' data-bind="visible: broswerObsolete" >For better experience, Please upgrade your internet browser to Internet Explorer 9.0 or higher<br/>or the latest version of (Safari / Google Chrome/ Mozilla Firefox)</div>
    </div>
</script>
<script id="funpassQuestTemp" type="text/x-kendo-template">
    <div  id='questDv' class="top-wrapper">
        <div class="container-fluid">
            <div class="inner-wrapper text-center">
                <div class="logo"><img src="${themePath}/img/funpass/logo/logo-${lang}.png"/></div>
                <h2 class="main-title">${staticText[lang]["onJourney"]!"Go on a journey at The State of Fun."}</h2>
                <div class="survey-wrapper">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="title-group"  data-bind="html: currentQuest().content_${lang}" >
                                <h1>WHO</h1>
                                <h3>will you come with?</h3>
                            </div>
                        </div>
                        <div class="col-md-2 hidden-xs col-sm-2">
                            <div id="equalizer" class="slider-vertical-wrapper">
                                <input  id="fSlider"  class="eqSlider" value="0"/>
                                <p class="slider-vertical-attribute"  data-bind="html: currentQuest().currentSelec().description_${lang}">Lone Ranger</p>
                            </div>
                        </div>
                        <div class="col-xs-12 hidden-md hidden-lg hidden-sm">
                            <div class="slider-horizontal-wrapper">
                                <input id="mSlider" class="balSlider" value="0" />
                                <p class="slider-horizontal-attribute"  data-bind="html: currentQuest().currentSelec().description_${lang}" >Lone Ranger</p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="state-img">
                                <img  data-bind="attr: { src: currentQuest().currentSelec().image }" width="99%"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="group-btn">
                    <button type="button" class="btn btn-lg btn-secondary" data-bind="click: back">${staticText[lang]["back"]!"Back"}</button>
                    <button type="button" class="btn btn-lg btn-primary" data-bind="click: next">${staticText[lang]["next"]!"Next"}</button>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="funpassAttrTemp" type="text/x-kendo-template">
    <div id="attracDv" class="top-wrapper">
        <div class="container-fluid">
            <div class="inner-wrapper text-center">
                <div class="logo"><img src="${themePath}/img/funpass/logo/logo-${lang}.png"/></div>
                <div class="group-selects">
                    <div class="row"  data-bind="foreach: questions">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="btn-group">
                                <button  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    <span style='display: inline-block;' data-bind="html: currentSelec().description_${lang}"></span><span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" style='width: 100%;'  data-bind="foreach: selections">
                                    <li style='width: 100%;text-align: center;'><a href="\#" style='font-size: 1.1em;' data-bind="html: description_en,click: $parent.selectedThis">Single</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="quick-link"><a href="\#" data-bind="click: goRcmdFunPass" ><span class="glyphicon glyphicon-chevron-right"> </span>${staticText[lang]["recommendation"]!"Go with our Recommendation"}</a></h3>
                <h6 class="small-subtitle">${staticText[lang]["chooseAdventure"]!"OR CHOOSE YOUR OWN ADVENTURE!"}</h6>

                <ul class="items-list" data-bind="foreach: attractions">
                    <li>
                        <a href="\#" data-bind="css: { 'recommended': rcmd},click: $parent.viewThisAttr">
                            <span class="item-masked" data-bind="visible: (chk() || $parent.showDetail()) && !inView()"></span>
                            <span class="item-selected" data-bind="visible: chk"></span>
                            <img  data-bind="attr: { src: image }" />
                        </a>
                    </li>
                </ul>

                <div class="clearfix"></div>

            </div>
        </div>
        <div class="selection-wrapper">
            <div class="selection">
                <h3>${staticText[lang]["youSelected"]!"You have selected"}</h3>
                <ul class="selection-list"  data-bind="foreach: selectedAttracs">
                    <li><img  data-bind="attr: { src: image }" /><a href="\#"  data-bind="click: $parent.rmSelectedAttr"><span class="btn-del glyphicon glyphicon-remove"></span></a></li>
                </ul>
                <div class="clearfix"></div>
                <div class="group-btn">
                    <button type="button" data-bind="visible: selectedAttracs().length > 0,click: goFunPass" class="btn btn-lg btn-primary">${staticText[lang]["go"]!"Go"}</button>
                </div>
            </div>
        </div>
        <div style="color: black;font-size: 0.6em;padding: 15px;">
            Jurassic Park TM &amp; © Universal Studios/Amblin Entertainment. All rights reserved. Shrek, Puss In Boots and all DreamWorks Animation elements © 2016 DreamWorks Animation L.L.C. TRANSFORMERS and its logo and all related characters are trademarks of Hasbro and are used with permission. © 2016 Hasbro. All Rights Reserved. © 2016 DreamWorks L.L.C. and Paramount Pictures Corporation. All Rights Reserved. Battlestar Galactica TM Universal Studios &amp; © 2016 Universal Cable Productions LLC. All rights reserved. Sesame Street TM and associated characters, trademarks and design elements are owned and licensed by Sesame Workshop. © 2016 Sesame Workshop. All rights reserved. Despicable Me and related characters are trademarks and copyrights of Universal Studios. Licensed by Universal Studios Licensing LLC. All Rights Reserved. UNIVERSAL STUDIOS, UNIVERSAL STUDIOS SINGAPORE, RIDE THE MOVIES, Universal Globe logo, and all Universal elements and related indicia TM &amp; © Universal Studios. All Rights Reserved. Genting International Management Limited. All rights reserved.
        </div>
    </div>
</script>

<script  id="funpassSelTemp" type="text/x-kendo-template">
    <div  id="funpassDv" class="top-wrapper">
        <div class="container-fluid">
            <div class="inner-wrapper text-center">
                <div class="logo"><img src="${themePath}/img/funpass/logo/logo-${lang}.png"/></div>
                <h2 class="main-title">${staticText[lang]["chosen"]!"These are the Fun you have chosen"}</h2>
                <ul class="selection-list selected-items"  data-bind="foreach: selectedAttracs">
                    <li><img data-bind="attr: { src: image }"/></li>
                </ul>
                <div class="clearfix"></div>
                <h3>${staticText[lang]["total"]!"Total: "}<span data-bind="text: '$ '+total()">$58.00</span></h3>
                <div class="group-list-wrapper">
                    <h1 class="match-title-md hidden-xs hidden-sm" data-bind="html: fFunHeaderMsg"></h1>
                    <h1 class="match-title-xs hidden-md hidden-lg" data-bind="html: mFunHeaderMsg"></h1>
                    <div class="match-group"><a href="\#"><img data-bind="attr: { src: funpassInView().image },click: viewDetail" /></a></div>

                    <div class="product-details-panel" data-bind="visible: showDetail()">
                        <span class="btn-del glyphicon glyphicon-remove" data-bind="click: closeDetail"></span>
                        <div class="notification"  data-bind="visible: funpassInView().notification_${lang}">
                            <h6 class="text-left">Notification</h6>
                            <p class="text-left" data-bind="html: funpassInView().notification_${lang}"></p>
                        </div>
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <img  data-bind="attr: { src: funpassInView().images()[0]?funpassInView().images()[0].url:funpassInView().image  }"  width="100%">
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <h4 class="text-left product-title"  data-bind="html: funpassInView().name_${lang}"></h4>
                                <div class="text-left product-details descrow" data-bind="html: funpassInView().description_${lang}"></div>
                                <button type="button" class="btn btn-primary" data-bind="visible: (funpassInView().id()!=funpassInSel().id()),click: changeSelFunPass" >${staticText[lang]["select"]!"Select"}</button>
                                <div class="notification" data-bind="visible: funpassInView().id()==funpassInSel().id()">
                                    <p>Selected</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="sub-title title-color" data-bind="visible: hasFunpasses" >${staticText[lang]["wouldYouLike"]!"Would you like to have more Fun?"}</h4>
                    <ul class="group-list"  data-bind="foreach: funpasses">
                        <li data-bind="visible: chk" >
                            <a href="\#" data-bind="click: $parent.selectFunPass">
                                <img data-bind="attr: { src: image }" width="100%"/>
                            </a>
                        </li>
                        <!-- ko if: ($index() === ($parent.funpasses().length - 1)) -->
                        <div class="clearfix"></div>
                        <!-- /ko -->
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="group-btn">
                <button type="button" class="btn btn-lg btn-primary"  data-bind="click: next">${staticText[lang]["continueJourney"]!"Yes, I want to continue the Fun Finder journey."}</button>
                <h3 class="quick-link"><a href="\#" class="title-color" data-bind="click: restart" >${staticText[lang]["restartJourney"]!"Restart the Fun Finder journey"}</a></h3>
            </div>
        </div>
    </div>
</script>


<script  id="upsellSelTemp" type="text/x-kendo-template">
    <div   id="upsellDv"  class="top-wrapper">
        <div class="container-fluid">
            <div class="inner-wrapper text-center">
                <div class="logo"><img src="${themePath}/img/funpass/logo/logo-${lang}.png"/></div>
                <h2 class="main-title"><span class="highlight">${staticText[lang]["great"]!"Great!"}</span> ${staticText[lang]["youSelected2"]!"You have selected:"}</h2>
                <div class="match-group-selected"><a href="\#"><img data-bind="attr: { src: funpassInSel().image }"/></a></div>
                <h4 class="sub-title title-color" data-bind="visible: upsells().length">${staticText[lang]["wouldYouLike"]!"Would you like to have more Fun?"}</h4>
                <ul class="items-list"   data-bind="foreach: upsells">
                    <li  data-bind="visible: standardRows().length">
                        <a href="\#" data-bind="click: $parent.viewUpsell"  >
                            <span class="item-masked" data-bind="visible: (!$parent.showDetail() && chk) || ($parent.showDetail() && !(id() == $parent.upsellInView().id()))"></span>
                            <span class="item-selected" data-bind="visible: chk"></span>
                            <img data-bind="attr: { src: image }"/>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="group-btn">
                <button type="button" class="btn btn-lg btn-secondary"  data-bind="click: back">${staticText[lang]["back"]!"Back"}</button>
                <button type="button" class="btn btn-lg btn-primary"  data-bind="click: summary" >${staticText[lang]["viewSummary"]!"View Summary"}</button>
            </div>
        </div>
    </div>
</script>

<script  id="summaryTemp" type="text/x-kendo-template">
    <div id="summaryDv" class="top-wrapper">
        <div class="container-fluid">
            <div class="inner-wrapper text-center">
                <div class="logo"><img src="${themePath}/img/funpass/logo/logo-${lang}.png"/></div>
                <h2 class="main-title"><span class="highlight">${staticText[lang]["excellent"]!"Excellent!"}</span>${staticText[lang]["tellUs"]!" Tell us how many passes you need?"}</h2>
                <ul class="summary-list">
                    <li>
                        <div class="row">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <img  data-bind="attr: { src: funpassInSel().image }" height="160"/>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <table  data-bind="foreach: funpassInSel().standardRows" cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody  data-bind="foreach: items">
                                    <tr>
                                        <td class="price"  style='text-align: left;width: 60%;' data-bind="text: typeDisplay() +' - $ ' + unitPrice() ">Adult - $59.00</td>
                                        <td>
                                            <div class="btn-group">
                                                <button data-bind="html: qty"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    1<span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" style="min-width:110px;" data-bind="foreach: qtyOptions">
                                                    <li style='margin-bottom: 0px;text-align: center;'><a href="\#" style="padding:0px;"  data-bind="html: optionText,click: $parent.setQty" >1</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div data-bind="foreach: upsellsInSel">
                            <div class="row"  style='border-top:1px solid rgba(255, 255, 255,.45);padding-top:10px;'  data-bind="visible: standardRows().length">
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <img data-bind="attr: { src:image }"  height="160"/>
                                </div>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <table  data-bind="foreach: standardRows" cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 160px;">
                                        <tbody  data-bind="foreach: items">
                                        <tr>
                                            <td class="price" style='text-align: left;padding-top:10px;font-size:16px;width: 60%;'   data-bind="html: name(),style: { 'border-top': ($parentContext.$index() == 0 ? '' : '1px solid rgba(255, 255, 255,.45)') }">Adult - $95.00</td>
                                            <td  data-bind="style: { 'border-top': ($parentContext.$index() == 0 ? '' : '1px solid rgba(255, 255, 255,.45)') }" style='padding-bottom:10px;' rowspan="2">
                                                <div class="btn-group">
                                                    <button data-bind="html: qty"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" style="min-width:110px;"data-bind="foreach: qtyOptions" >
                                                        <li style='margin-bottom: 0px;text-align: center;'><a style="padding:0px;" href="\#" data-bind="html: optionText,click: $parent.setQty" >1</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="price" style='text-align: left;padding-bottom:30px;padding-top:10px;font-size:20px;'  data-bind="html: typeDisplay() +' - $ ' + unitPrice() ">Adult - $95.00</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="clearfix"></div>
                </ul>
                <p class="text-right total-field" data-bind="text: '${staticText[lang]["total"]!"Total: "}$ ' + total() ">&nbsp;</p>
            </div>
            <div class="group-btn">
                <button type="button" class="btn btn-lg btn-primary"   data-bind="click: paynow">${staticText[lang]["proceedToPay"]!"Proceed To Pay"}</button>
            </div>
        </div>
    </div>

</script>

<script  id="upsellLiTemp" type="text/x-kendo-template">
    <div class="product-details-panel"  style="z-index: 2;" data-bind="visible: showDetail()">
        <span class="btn-del glyphicon glyphicon-remove" data-bind="click: closeDetail"></span>
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-12">
                <img  data-bind="attr: { src: upsellInView().topupImage?upsellInView().topupImage:'' }"  style="width:100%;">
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <h4 class="text-left product-title"  data-bind="html: upsellInView().name_${lang}"></h4>
                <div class="text-left product-details descrow" style="white-space: pre-wrap;" data-bind="html: upsellInView().description_${lang}"></div>
                <button type="button" class="btn btn-primary" data-bind="visible: !upsellInView().chk(), click: toggleUpsell" >${staticText[lang]["select"]!"Select"}</button>
                <button type="button" class="btn btn-primary btn-red" data-bind="visible: upsellInView().chk(), click: toggleUpsell" >${staticText[lang]["unselect"]!"Unselect"}</button>
            </div>
        </div>
    </div>
</script>
<script  id="attrLiTemp" type="text/x-kendo-template">
    <div class="product-details-panel"  style="z-index: 2;" data-bind="visible: showDetail()">
        <span class="btn-del glyphicon glyphicon-remove" data-bind="click: closeDetail"></span>
        <div class="notification"  data-bind="visible: attracInSelect().notification_${lang}">
            <h6 class="text-left">Notification</h6>
            <p class="text-left" data-bind="html: attracInSelect().notification_${lang}"></p>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-12">
                <img  data-bind="attr: { src: attracInSelect().prodImage() }"  width="100%">
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <h4 class="text-left product-title"  data-bind="html: attracInSelect().name_${lang}"></h4>
                <div class="text-left product-details descrow" style="white-space: pre-wrap;" data-bind="html: attracInSelect().description_${lang}"></div>
                <button type="button" class="btn btn-primary" data-bind="visible: !attracInSelect().chk(), click: addToSelected" >${staticText[lang]["select"]!"Select"}</button>
                <button type="button" class="btn btn-primary btn-red" data-bind="visible: attracInSelect().chk(), click: removToSelected" >${staticText[lang]["unselect"]!"Unselect"}</button>
            </div>
        </div>
    </div>
</script>
<div id="spinner-overlay" class="spinner-overlay" style="display:none;"></div>
<script>
    var lang = "${lang}";
</script>
