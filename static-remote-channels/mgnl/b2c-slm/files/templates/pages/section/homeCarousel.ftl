[#include "/b2c-slm/templates/macros/pageInit.ftl"]

[#-------------- RENDERING PART --------------]
[#-- Rendering: Home Carousel item --]

<!-- Home Carousel Section -->

[#if content.images?has_content]
    [#assign images = cmsfn.children(cmsfn.contentByPath(content.@handle + "/images"))]
    [#if images?size > 0]
    <div class="slidershow-land">
        <img class="sequence-prev slider-arrow" src="${ctx.contextPath}/resources/b2c-slm/theme/default/img/left-arrow.png" alt="Prev" />
        <img class="sequence-next slider-arrow" src="${ctx.contextPath}/resources/b2c-slm/theme/default/img/right-arrow.png" alt="Next" />
        <ul class="sequence-canvas">
            [#assign imageIndex = 0]
            [#assign displayImageIndex = 0]
            [#list images as image]
                [#if image?has_content]
                    [#assign isCorrectImage = false]
                    [#if image?index_of(lang,0) > -1]
                        [#assign isCorrectImage = true]
                    [#elseif lang = "en" && image?index_of("_",0) == -1]
                        [#assign isCorrectImage = true]
                    [/#if]
                    [#if isCorrectImage == true]

                        [#assign rendition = ""]
                        [#if images[imageIndex].image?has_content]
                            [#assign rendition = damfn.getRendition(images[imageIndex].image, "original")]
                        [/#if]
                        [#assign assetTitle = displayImageIndex + 1]
                        [#if rendition != ""]
                            [#if rendition.asset?? && rendition.asset.title?has_content]
                                [#assign assetTitle = rendition.asset.title]
                            [/#if]
                        [/#if]

                    [#-- Alt text and title --]
                        [#assign imageAlt = images[imageIndex].title!assetTitle!]
                        [#assign imageTitle = images[imageIndex].title!assetTitle!]

                        [#assign imageLink = ""]
                        [#if rendition != ""]
                            [#assign imageLink = rendition.link]
                        [/#if]
                        [#assign imageHyperlink = images[imageIndex].url!"javascript:void(0);"]

                        <li>
                            <div class="promotion-1">
                                <a href="${imageHyperlink}" target="_blank" onclick="return logBannerClick('${imageTitle}', '${imageHyperlink}');">
                                    <span class="promotion-pic"><img src="${imageLink}" alt="${imageAlt}" /></span>
                                </a>
                            </div>
                        </li>

                        [#assign displayImageIndex = displayImageIndex + 1]
                    [/#if]
                [/#if]
                [#assign imageIndex = imageIndex + 1]
            [/#list]
        </ul>
        <div class="slidershow-bar">
            <div class="backdrop">&nbsp;</div>
            <ul>
                [#assign imageIndex = 0]
                [#assign displayImageIndex = 0]
                [#list images as image]
                    [#if image?has_content]
                        [#assign isCorrectImage = false]
                        [#if image?index_of(lang,0) > -1]
                            [#assign isCorrectImage = true]
                        [#elseif lang = "en" && image?index_of("_",0) == -1]
                            [#assign isCorrectImage = true]
                        [/#if]
                        [#if isCorrectImage == true]

                            [#assign rendition = ""]
                            [#if images[imageIndex].image?has_content]
                                [#assign rendition = damfn.getRendition(images[imageIndex].image, "original")]
                            [/#if]
                            [#assign assetTitle = displayImageIndex + 1]
                            [#if rendition != ""]
                                [#if rendition.asset?? && rendition.asset.title?has_content]
                                    [#assign assetTitle = rendition.asset.title]
                                [/#if]
                            [/#if]

                        [#-- Alt text and title --]
                            [#assign imageAlt = images[imageIndex].title!assetTitle!]
                            [#assign imageTitle = images[imageIndex].title!assetTitle!]

                            <li class="bar-label bar-active" data-bar-idx="${displayImageIndex}">
                                <div class="selector-arrow"><img src="${ctx.contextPath}/resources/b2c-slm/theme/default/img/arrow-up.png"></div>
                                <span>${imageTitle}</span>
                            </li>

                            [#assign displayImageIndex = displayImageIndex + 1]
                        [/#if]
                    [/#if]
                    [#assign imageIndex = imageIndex + 1]
                [/#list]
            </ul>
        </div>
    </div>
    [/#if]
[/#if]
    