[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/receipt.ftl"]

<script>
  var jsLang = "${lang?js_string!}";
</script>

<div class="general-info-section" style="padding-bottom: 2em; display: none;" id="transactionAbnormalSection">
    <h1 class="title">${staticText[lang]["transactionStatus"]!"Transaction Status"}</h1>
    <p class="content" id="tasPending" style="display: none;">${staticText[lang]["transactionProcessing"]!"We are still processing your transaction. Please wait a few moments."}</p>
    <p class="content" id="tasMsg" style="display: none;">Some Message</p>
</div>

<div class="general-info-section" style="padding-bottom: 2em; display: none;" id="transactionOkSection">
    <h1>${staticText[lang]["thanksForShopping"]!"Thank you for shopping with us!"}</h1>
    <p>
        <strong>${staticText[lang]["orderCompleted"]!"Your order has been completed. An email with a PIN CODE will be sent to the address that you have provided (please check your junk folder as well). Kindly present the email receipt for redemption of tickets."}</strong>
    </p>
</div>

<div style="text-align: center; margin-bottom: 1.5em; display: none;" id="pi4" >
    <a class="continue-btn pinky bitin-bleu" href="${sitePath}" style="display: inline-block">${staticText[lang]["continueShopping"]!"Continue Shopping"}</a>
</div>

<div id="afterSurvey" style=" font-size: 1.3em; text-align: center; margin: 2em 0;color: #EC6179;display: none;">
    ${staticText[lang]["surveySubmitted"]!"Survey submitted. Thank you for filling out our customer survey!"}
</div>
<div class="survey-sect" style="display: none;">
    <h3>${staticText[lang]["satisfaction"]!"Sentosa Online Store Guest Satisfaction"}</h3>
    <div class="desc">
        <p><strong>${staticText[lang]["feedback"]!"If you have a moment, we'd welcome some feedback."}</strong></p>
    </div>
    <div class="question-sect">
    </div>
    <div class="q-controls">
        <a class="continue-btn pinky" id="submitSurvey" style="display: inline-block">${staticText[lang]["submit"]!"Submit"}</a>
    </div>
</div>

<div class="receipt-wrapper" style="display: none;" id="receiptWrapper">

    <div class="mktg-bottom" style="width: auto">
    [@cms.area name="adBanner_c1"/]
    </div>

    <div class="topbar-receipt" id="pi0">
        <a class="ticketing-logo"><img src= "${themePath}/img/ticket-logo.png" /></a>
        <div class="right-head">
            <p>${staticText[lang]["dateOfPurchase"]!"Date Of Purchase:"} <span class="date" data-bind="text: dateOfPurchase"></span></p>
        </div>
        <div style="float: left; margin-top: 10px;margin-left: 110px;">
            <span>GST No.: MB-8100139-6</span>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="receipt-code" id="pi1">
        <div style="width: 100%;">
            <div style="width: 49%; float: left;">
                <p class="receipt-number">${staticText[lang]["receiptNumber"]!"Receipt Number"}</p>
                <p class="number" data-bind="text: receiptNumber"></p>
            </div>
            <div style="width: 50%; float: right;">
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="margin-top: 1em; text-align: center;">
        ${staticText[lang]["confirmationEmail"]!'If you do not receive the confirmation email within the next 30 minutes, please contact Sentosa Hotline at 1800-SENTOSA (736 8672) / +65-6736 8672 from 9am to 6pm daily. Alternatively, you may write in to <a href="mailto:guest_services@sentosa.com.sg">guest_services@sentosa.com.sg</a>.'}
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="text-section" id="pi2">
        <p>${staticText[lang]["dear"]!"Dear"} <span data-bind="text: name"></span,</p>
        <p>${staticText[lang]["summarizedOrderDetails"]!"Your order details are summarized below:"}</p>
    </div>

    <table class="information" id="pi3">
        <tr>
            <th colspan="2">${staticText[lang]["personalInformation"]!"Personal Information"}</th>
        </tr>
        <tr>
            <td width="25%">${staticText[lang]["name"]!"Name:"} </td>
            <td width="75%" class="attribute" data-bind="text: name"></td>
        </tr>
        <tr>
            <td>${staticText[lang]["email"]!"Email:"}</td>
            <td class="attribute" data-bind="text: email"></td>
        </tr>
        <!--
        <tr>
            <td>ID:</td>
            <td class="attribute">(Passport) </td>
        </tr>
         -->
        <tr>
            <td>${staticText[lang]["contactNo"]!"Contact No.:"}</td>
            <td class="attribute" data-bind="text: mobile"></td>
        </tr>
        <tr>
            <td>${staticText[lang]["paymentType"]!"Payment Type:"}</td>
            <td class="attribute" data-bind="text: paymentType"></td>
        </tr>
    </table>

    <div class="checkout-items" id="theCheckout">
        <!-- ko foreach:items -->
        <div class="heading" data-bind="text: baseModel.title"></div>
        <table class="tickets">
            <tr>
                <th width="35%">${staticText[lang]["item"]!"Item"}</th>
                <th width="20%">${staticText[lang]["details"]!"Details"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="10%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span data-bind="text: title"></span><br/>
                    <span data-bind="text: productDescription" style="color: gray"></span>
                </td>
                <td>
                    <span data-bind="html: description"></span><br>
                    <span data-bind="visible: discountTotal > 0, text: 'Discount: ' + discountLabel" style="color: red;"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize">
                    <span data-bind="text: totalText"></span><br data-bind="if: discountTotal > 0">
                    <span data-bind="visible: discountTotal > 0, text: 'S$ ' + Number(discountTotal + total).toFixed(2)"
                          style="text-decoration: line-through; font-style: italic;"></span>
                </td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="35%" class="items-add-topups">
                    <!-- ko if:baseModel.hasTopup -->
                    <a class="items-add-topups-hyperlink" data-bind="attr:{'href':createTopupUrl()}">${staticText[lang]["addMoreTopups"]!"add more top-ups"}</a>
                    <!-- /ko -->
                </td>
                <td width="40%" class="title" colspan="3">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: baseModel.subtotalText"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- Compact -->
        <table class="tickets s-tickets size-s">
            <tr>
                <th class="col-item">${staticText[lang]["item"]!"Item"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
            </tr>
            <tbody data-bind="foreach: baseModel.products">
            <tr data-bind="css:{'main-ticket':cartType!='Topup','topup':cartType=='Topup'}">
                <td>
                    <!-- ko if:cartType == 'Topup' -->
                    <i class="fw-icon-plus-sign"></i>
                    <!-- /ko -->
                    <span style="font-weight: bold" data-bind="text: title"></span><br/>
                    <span data-bind="text: productDescription" style="color: gray"></span>
                    <br/>
                    <span data-bind="html: description"></span>
                    <span data-bind="visible: discountTotal > 0, text: 'Discount: ' + discountLabel" style="color: red;"></span>
                </td>
                <td class="centerlize" data-bind="text: priceText"></td>
                <td class="centerlize" data-bind="text: qty"></td>
                <td class="centerlize">
                    <span data-bind="text: totalText"></span><br data-bind="if: discountTotal > 0">
                    <span data-bind="visible: discountTotal > 0, text: 'S$ ' + Number(discountTotal + total).toFixed(2)"
                          style="text-decoration: line-through; font-style: italic;"></span>
                </td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="80%" class="title" colspan="2">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount" data-bind="text: baseModel.subtotalText"></td>
            </tr>
        </table>
        <!-- /ko -->

        <!-- ko if:hasBookingFee -->
        <div class="heading" >${staticText[lang]["bookingFee"]!"Booking Fee"} </div>
        <table class="tickets">
            <tr>
                <th width="50%">${staticText[lang]["bookingFee"]!"Booking Fee"} </th>
                <th width="15%" class="centerlize">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th width="15%" class="centerlize">${staticText[lang]["qty"]!"Qty"}</th>
                <th width="20%" class="centerlize">${staticText[lang]["amount"]!"Amount"}</th>
                <th width="5%" class="centerlize">&nbsp;</th>
            </tr>
            <tbody>
            <tr>
                <td><span data-bind="text: bookFeeMode"></span></td>
                <td class="centerlize" data-bind="text: bookPriceText"></td>
                <td class="centerlize" data-bind="text: bookFeeQty"></td>
                <td class="centerlize" data-bind="text: 'S$ ' + bookFeeSubTotalText()"></td>
                <td class="centerlize">&nbsp;</td>
            </tr>
            </tbody>
            <tr class="subtotal">
                <td width="55%" class="title waiveDiv">

                </td>
                <td width="20%" colspan="2" class="title">${staticText[lang]["subtotal"]!"SUBTOTAL:"}</td>
                <td width="20%" class="amount"  data-bind="text: 'S$ ' + bookFeeSubTotalText()"></td>
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <div class="grandtotal-section">
            <div class="buy-gift">
            </div>
            <div class="grandtotal">
            ${staticText[lang]["grandTotal"]!"GRAND TOTAL:"}
                <span class="amount" data-bind="text: totalText()">S$ 64.90</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="remark" id="theRemark">
        <div class="note">
            <div class="heading">${staticText[lang]["notes"]!"Notes"}</div>
            <div class="details">
            ${staticText[lang]["notesDetails"]!"<p style=\"font-weight: bold\">For verification purposes, please present the following upon redemption:</p>
            <ol>
            <li>Email Confirmation</li>
            <li>Credit card used for purchase</li>
            </ol>
            <p style=\"font-weight: bold\">For a proxy to collect on your behalf, he/she needs to presents:</p>
            <ol>
            <li>Email Confirmation</li>
            <li>This <a href=\"/dam/jcr:c9111e86-6cd0-4588-b25e-addbaf84d17d/letter-of-auth-slm.pdf\" target=\"_blank\">Letter of Authorisation</a> (duly completed and signed by you)</li>
            <li>Clear photocopy of your photo identification card such as NRIC/Passport/FIN Card</li>
            <li>Proxy's original photo identification</li>
            </ol>
             <p style=\"font-weight: bold\">Important note:<br/>
                Sentosa Development Corporation reserves the right not to release tickets if the above documents are not in order.</p>"}
            </div>
        </div>

    <!-- ko if:hasTnc -->
        <div class="terms">
            <div class="heading">${staticText[lang]["tnc"]!"Terms and Conditions"}</div>
            <div class="details">

              <!-- ko foreach:tncs -->
              <h4 data-bind="text: title"></h4>

              <div class="the-tnc" data-bind="html: content"></div>
              <!-- /ko -->

            </div>
        </div>
    <!-- /ko -->
    </div>

    <div class="mktg-bottom" style="width: auto">
    [@cms.area name="adBanner"/]
    </div>
</div>