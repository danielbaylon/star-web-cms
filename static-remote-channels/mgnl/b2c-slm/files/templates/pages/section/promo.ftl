[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/promo.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign channel = 'b2c-slm']
[#assign promoCode = params[0]!]
[#assign catNodes = starfn.getCMSProductCategories(channel)!]
[#assign noResults = true]
[#if catNodes?has_content]
    [#list catNodes as catNode]
        [#assign cmsPrds = starfn.getPromoSearchProducts(catNode,channel,promoCode)!]
        [#if cmsPrds?has_content]
            [#assign catUrlPath = ""]
            [#if catNode.hasProperty("generatedURLPath")]
                [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
            [/#if]
        <div style="width:100%;">
          <span
            style="font-family: 'Flama','Helvetica Neue','Helvetica','Arial',sans-serif;font-size: 1.2em;line-height: 35px;">${staticText[lang]["searchResult"]!"Search Result : "}${promoCode?html!}</span>
        </div>
            [#assign noResults = false]
        <div class="list">
            [#if catNode.hasProperty("name")]
              <h3>${catNode.getProperty("name").getString()?html!}</h3>
            [/#if]
          <ul class="panels">
              [#list cmsPrds as cmsPrd]
                  [#assign hasItemDisplayed = true]
                  [#assign cmsProductName = ""]
                  [#assign cmsProductShortDesc = ""]
                  [#assign cmsProductUrl = "javascript:void(0);"]
                  [#if cmsPrd.hasProperty("name")]
                      [#assign cmsProductName = cmsPrd.getProperty("name").getString()]
                  [/#if]
                  [#if cmsPrd.hasProperty("shortDescription")]
                      [#assign cmsProductShortDesc = cmsPrd.getProperty("shortDescription").getString()]
                  [/#if]
                  [#if cmsPrd.hasProperty("generatedURLPath")]
                      [#assign cmsProductUrl = sitePath + "/category/product~" + cmsPrd.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${cmsPrd.getName()}~${promoCode?url(lang)}~.html?category=${catUrlPath}"]
                  [/#if]
                  [#assign thumbnailAssetLink = ""]
                  [#assign cmsProductImages = starfn.getCMSProductImagesUUID(cmsPrd, lang)!]

                  [#if cmsProductImages?has_content]
                      [#assign cmsProductImage = cmsProductImages[0]]
                      [#if cmsProductImage?has_content]
                          [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)!]
                      [/#if]
                  [/#if]

                  [#assign lowestPrice = starfn.getCMSProductLowestTicketPrice(cmsPrd)]
                <li class="panel">
                  <a href="${cmsProductUrl}" class="images">
                    <img src="${thumbnailAssetLink}" alt="${cmsProductName}"/>
                  </a>
                  <a href="${cmsProductUrl}" class="heading" style="padding-top:5px">${cmsProductName}</a>

                  <p class="description">${cmsProductShortDesc}</p>
                  <a class="buy" href="${cmsProductUrl}">
                    <span class="from">From</span><span class="price">S$${lowestPrice}</span><span
                    class="buy-now">Buy Now!</span>
                  </a>
                </li>
              [/#list]
          </ul>
          <div class="clearfix"></div>
        </div>
        [/#if]
    [/#list]
[/#if]
[#if noResults]
<div class="general-info-section">
  <h1 class="title"
      id="titleDefault">${staticText[lang]["noResults"]!"We are sorry but no results were found for "}${promoCode?html}
    .</h1>

  <p class="content"><a href="/">${staticText[lang]["goToHome"]!"Go to the home page!"}</a></p>
</div>
[/#if]