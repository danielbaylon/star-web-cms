[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/product.ftl"]

<script src="${themePath}/js/jquery.sequence-min.js"></script>
<script type="text/javascript">
  var cmsBookingModel = null;
</script>

[#assign params = ctx.aggregationState.getSelectors()]
[#if params[2]?has_content]
    [#assign cat = params[2]]
[/#if]
[#if params[3]?has_content]
    [#assign prd = params[3]]
[/#if]
[#assign rootPath = "/b2c-slm/"]
[#assign cmsProductUrl = "javascript:void(0);"]
[#assign previewMode = ctx.getParameter("preview")!]
[#assign proceedShowingProduct = false]
[#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance && prd?has_content]
    [#assign proceedShowingProduct = true]
[/#if]
[#if cat?has_content && prd?has_content]
    [#assign proceedShowingProduct = true]
[/#if]
[#if proceedShowingProduct]
    [#assign catUrlPath = ""]
    [#assign catNode = ""]
    [#if cat?has_content]
        [#assign catNode = starfn.getCMSProductCategory("b2c-slm", cat)]
    [/#if]
    [#assign cmsProductNode = starfn.getCMSProduct("b2c-slm", prd)]
    [#assign hasPcPromo = false]
    [#if cmsProductNode?has_content]
        [#assign hasPcPromo = starfn.hasActivePromoCode(cmsProductNode)!false]
        [#assign cmsProductName = ""]
        [#assign cmsProductShortDesc = ""]
        [#assign cmsProductNotes = ""]
        [#assign cmsProductUrl = "javascript:void(0);"]
        [#assign cmsProductUUID = cmsProductNode.getIdentifier()]
        [#if cmsProductNode.hasProperty("name")]
            [#assign cmsProductName = cmsProductNode.getProperty("name").getString()]
        [/#if]
        [#if cmsProductNode.hasProperty("description")]
            [#assign cmsProductDesc =  cmsProductNode.getProperty("description").getString()]
        [/#if]
        [#if cmsProductNode.hasProperty("notes")]
            [#assign cmsProductNotes = cmsProductNode.getProperty("notes").getString()]
        [/#if]
        [#if cat?has_content]
            [#if catNode.hasProperty("generatedURLPath")]
                [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
            [/#if]
            [#if cmsProductNode.hasProperty("generatedURLPath")]
                [#assign cmsProductUrl = sitePath + "/category/product~" + cmsfn.asContentMap(cmsProductNode).generatedURLPath?lower_case + "~${lang}~${cat}~${cmsProductNode.name}~.html?category=${catUrlPath?lower_case}"]
            [/#if]
        [/#if]
    <script type="text/javascript">
      cmsBookingModel = {
        "productId": "${prd}",
        "productUrl": "${cmsProductUrl}",
        "type": "GeneralTicket",
        "topupLimitType": "LimitToMainTicket",
        "isGeneral": true,
        "isRecurring": false,
        "isActive": true,
        "displayDate": false,
        "isEmpty": false,
        "name": "${cmsProductName}",
        "desc": "",
        "url": "",
        "showPromo": true,
        "productWidget": "",
        "images": [],
        "hasNotes": false,
        "notes": "",
        "recommendedItems": [],
        "standardRows": [],
        "topups": []
      };
    </script>

    <div id="${prd}" style="display:none" data-prd-uuid="${cmsProductUUID}" data-prd-name="${cmsProductName}"></div>
    [/#if]
[/#if]

<script>
    [#if params[4]?has_content]
    var urlPromoCode = "${params[4]?js_string!""}";
    var loadUrlPromoCode = true;
    [#else]
    var urlPromoCode = "";
    var loadUrlPromoCode = false;
    [/#if]
</script>

[#if cmsProductNode?has_content]
<!-- Checkout Process 1 -->

<div class="process-step-1">
  <img src="${themePath}/img/process/process-booking-${imgSuffix}.jpg">
</div>

<div class="s-process-checkout size-s">
  <img src="${themePath}/img/process/process-booking-s-${imgSuffix}.jpg">
</div>

<!-- Product Details -->


<div class="ticket-details">
  <div class="the-main">
    <h2 class="main-heading">${cmsProductName}</h2><!--
        -->
    <div class="s-shortcuto size-s" id="bookNowShortcut">
      <a href="javascript:nvx.scrollTo('bookingSection');"><i
        class="fw-icon-arrow-down"></i> ${staticText[lang]["bookNow"]!"Book Now!"}</a>
    </div>
  </div>
  <div class="rightbar">
    <div class="slide-container">
      <img class="sequence-prev slider-arrow" src="${themePath}/img/left-arrow.png" alt="Prev"/>
      <img class="sequence-next slider-arrow" src="${themePath}/img/right-arrow.png" alt="Next"/>
      <ul class="sequence-canvas">

          [#assign cmsProductImages = starfn.getCMSProductImagesUUID(cmsProductNode, lang)!]
          [#list cmsProductImages as cmsProductImage]
              [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)]
            <li>
              <div class="slide"><img src="${thumbnailAssetLink}"/></div>
            </li>
          [/#list]


      </ul>
    </div>
  </div>
  <div class="leftbar">
    <div class="ticket-description">
      <div class="details">
        <div class="constrain-small">
          <div class="prod-desc">${cmsProductDesc}</div>
        </div>
        <div class="size-s expand-small">${staticText[lang]["readMore"]!"read more..."}</div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<!-- Booking Section -->

<div class="booking-topup-section" id="bookingForm">


  <div class="successful-information" style="display: none;" data-bind="visible: showStatusSection">
    <div class="information">
      <img src="${themePath}/img/successful.png"/><span class="item"
                                                        style="margin: 0;">${staticText[lang]["itemsAddedToCart"]!"Item(s) added to Cart"}</span>

      <div class="clearfix"></div>
    </div>
    <div class="extra-message" data-bind="visible: showExtraMsg, text: extraMsg"></div>
  </div>

  <!-- Booking Form -->

  <div class="leftbar">

    <!-- Ticketing Section -->

    <div class="empty-category" style="min-height: 0; display: none;" data-bind="visible: !baseProd.isActive">
      <h1 class="title">${staticText[lang]["productIsUnavailable"]!"This product is unavailable."}</h1>

      <p class="content">${staticText[lang]["checkOutOther"]!"Please check out our other"} <a href="${sitePath}"
                                                                                              class="hyperlink">${staticText[lang]["tickets"]!"tickets"}</a>!
      </p>
    </div>

    <!-- ko if:baseProd.isActive -->

    <div data-bind="visible: showMainForm" id="bookingSection" style="display: none">
      <div class="ticket-book">
        <div class="message-section" style="display:none;"
             data-bind="visible: showBookMsg, text: bookMsg, css: {'error':bookMsgErr}"></div>

        <!-- ko if:hasDate -->
        <div class="date">
          <h3 class="heading">${staticText[lang]["whenAreYouGoing"]!"When are you going?"} </h3>

          <div class="date-input">
            <div class="date-input-field">
              <input type="text" class="select-date" id="theDate" placeholder="dd / mm / yyyy"
                     data-bind="value: theDate"/>
              <i class="fw-icon-calendar" data-bind="click: toggleTheDate"></i>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /ko -->

        <!-- ko ifnot:isGeneral -->
        <div class="date">
          <h3 class="heading">${staticText[lang]["whenAreYouGoing"]!"When are you going?"} </h3>

          <div class="date-input">
            <select class="book-event-date"
                    data-bind="options: schedules, optionsText: 'scheduleName', optionsValue: 'id', value: currentSchedId">
            </select>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /ko -->

        <div class="select-tix" id="theProdForm">
          <h3 class="heading"
              data-bind="css:{'no-top-margin':!hasDate() && isGeneral}">${staticText[lang]["selectYourTickets"]!"Select your tickets!"}</h3><!--
                    -->
          <div class="s-shortcuto size-s">
            <a href="javascript:nvx.scrollTo('theInfoSec')"><i
              class="fw-icon-arrow-down"></i> ${staticText[lang]["moreInfo"]!"More Info"}</a>
          </div>
        </div>

          [#if hasPcPromo]
            <div class="promo-code">
              <div class="promo-title">
                <h4 class="title" data-bind="text: promoCallToArms"></h4>
                <a class="signup-hyperlink" target="_blank"
                   data-bind="text: promoLinkText, attr: {'href':promoLink}"></a>
              </div>
              <input class="type" type="text" data-bind="value: promoCode"/>
              <a class="apply-btn" data-bind="click: applyCode">${staticText[lang]["apply"]!"Apply"}</a>

              <div class="message-section promo-code-msg" data-bind="visible: codeApplyError, text: codeApplyMsg,
                         css: {'error':codeApplyError}"
                   style="display: none;">${staticText[lang]["abortError"]!"error error, ABORT!"}</div>
            </div>
          [/#if]

        <div class="clearfix"></div>

        <div class="tickets-list">

          <!-- ko if:isGeneral -->

          <table class="standard-ticket">
            <tr>
              <th class="col-detail">${staticText[lang]["standardTickets"]!"Standard Tickets"}</th>
              <th class="col-price unit-price">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
              <th class="col-qty qty-list">${staticText[lang]["quantity"]!"Qty."}</th>
              <th class="col-end-space">
                <i class="fw-icon-collapse-alt icon-2" data-bind="click: toggleExpandStandard"></i>
              </th>
            </tr>
            <tbody data-bind="visible: expandStandard">
                [#assign axPrdList = starfn.getAXProductsByCMSProductNode(cmsProductNode)]
                [#assign axPrdDetailsMap = starfn.getAXProductsDetailByCMSProductNode(cmsProductNode)]
                [#list axPrdList as axPrd]
                    [#assign axPrdDetail = axPrdDetailsMap[axPrd.getProperty("relatedAXProductUUID").getString()]]
                    [#assign itemName = ""]
                    [#assign description = ""]
                    [#assign ticketType = ""]
                    [#assign relatedAXProductUUID = ""]
                    [#if axPrdDetail.hasProperty("itemName")]
                        [#assign itemName = axPrdDetail.getProperty("itemName").getString()]
                    [/#if]
                    [#if axPrdDetail.hasProperty("description")]
                        [#assign description = axPrdDetail.getProperty("description").getString()]
                    [/#if]
                    [#if axPrdDetail.hasProperty("ticketType")]
                        [#assign ticketType = axPrdDetail.getProperty("ticketType").getString()]
                    [/#if]
                    [#assign productPriceUnitId = ""]
                    [#assign displayProductNumber = ""]
                    [#assign productPrice = ""]
                    [#if axPrdDetail.hasProperty("status")]
                        [#assign status = axPrdDetail.getProperty("status").getString()]
                    [/#if]
                    [#if status?lower_case == "Active"?lower_case]
                        [#if axPrdDetail.hasProperty("productListingId")]
                            [#assign productListingId = axPrdDetail.getProperty("productListingId").getString()]
                        [/#if]
                        [#if axPrdDetail.hasProperty("displayProductNumber")]
                            [#assign displayProductNumber = axPrdDetail.getProperty("displayProductNumber").getString()]
                        [/#if]
                        [#if axPrdDetail.hasProperty("productPrice")]
                            [#assign productPrice = axPrdDetail.getProperty("productPrice").getString()]
                        [/#if]
                    <tr class="bordered-top">
                      <td>
                        <h3 class="heading">${itemName}</h3>
                      </td>
                      <td class="unit-price">${ticketType}  - S$${productPrice}</td>
                      <td class="qty-list">
                        <select id="qty${productListingId}" class="qty" data-std-id="${productListingId}"
                                data-std-code="${displayProductNumber}"
                                data-std-name="${itemName}" data-std-type="${ticketType}"
                                data-std-price="${productPrice}" data-std-set="false">
                          <option value="0">0</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                        </select>
                        <a id="ebSelect${productListingId}" style="display: none;" class="sebs-btn-select"
                           href="javascript:void(0)">${staticText[lang]["Select"]!"Select"}</a>
                        <a id="ebCancel${productListingId}" style="display: none;" class="sebs-btn-cancel"
                           href="javascript:void(0)">${staticText[lang]["cancel"]!"Cancel"}</a>
                      </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr id="eventBar${productListingId}" style="display: none;">
                      <td colspan="4" style="padding: 0">
                        <div style="padding: 5px 7px;">
                          <div class="star-event-booking-section">
                            <div class="seb-subsection seb-subsection-date">
                              <div
                                class="sebs-label">${staticText[lang]["chooseDateOfVisit"]!"Choose Date of Visit"}</div>
                              <div>
                                <input id="ebDate${productListingId}" type="text" class="sebs-input"
                                       style="width: 70%;">
                                <label for="ebDate${productListingId}"><i class="fw-icon-calendar"
                                   style="vertical-align:middle; font-size:1.2em; color: #1ca9ea; padding: 0 0.3em;"
                                                                          data-profile-dov-cal-icon="1"></i></label>
                              </div>
                            </div>
                            <div class="seb-subsection seb-subsection-session">
                              <div class="sebs-label">${staticText[lang]["chooseSession"]!"Choose Session"}</div>
                              <div>
                                <select id="ebSession${productListingId}" style="width: 95%">
                                </select>
                              </div>
                            </div>
                            <div class="seb-subsection seb-subsection-qty">
                              <select id="ebQty${productListingId}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                        [#assign promotionList = starfn.getAXProductPromotions(axPrdDetail)]
                        [#if description?has_content]
                        <tr>
                          <td style="padding: 0 0 0 0.4em;">
                            <span class="details">${description}</span>
                          </td>
                          <td colspan="3">&nbsp;</td>
                        </tr>
                        [/#if]
                        [#if promotionList?has_content]
                        <tr hidden id="upArrow${productListingId}"
                            data-bind="click: function(){$root.hidePromoRows('${productListingId}')}">
                          <td colspan="4">
                            <div class="view-promotions-header">
                            ${staticText[lang]["hidePromotions"]!"Hide Promotions"}
                              <div class="promo-arrow-up"></div>
                            </div>
                          </td>
                        </tr>
                        <tr id="downArrow${productListingId}"
                            data-bind="click: function(){$root.showPromoRows('${productListingId}')}">
                          <td colspan="4">
                            <div class="view-promotions-header">
                            ${staticText[lang]["viewPromotions"]!"View Promotions"}
                              <div class="promo-arrow-down"></div>
                            </div>
                          </td>
                        </tr>
                            [#list promotionList as promotion]
                                [#assign promotionIconUUID = promotion.getProperty("promotionIcon").getString()]
                                [#if promotionIconUUID?has_content]
                                    [#assign promotionIconImageURL = damfn.getAssetLink(promotionIconUUID)]
                                [#else]
                                    [#assign promotionIconImageURL = ""]
                                [/#if]
                                [#assign promotionText = promotion.getProperty("promotionText").getString()]
                                [#assign additionalInfo = ""]
                                [#if promotion.hasProperty("promotionAdditionalInfo")]
                                    [#assign additionalInfo = promotion.getProperty("promotionAdditionalInfo").getString()]
                                [/#if]
                                [#assign cachedDiscountPrice = promotion.getProperty("cachedDiscountPrice").getDouble()]
                            <tr hidden class="promoLabel${productListingId}">
                              <td style="padding: 0 0 0 0.7em;">
                                        <span class="promotion-content">
                                        <span style="padding: 3px; 3px; display: block;">
                                            <span
                                              style="vertical-align: middle; width: 30px; display: inline-block;"><img
                                              src="${promotionIconImageURL}"></span>
                                            <span
                                              style="font-size: 0.9em; vertical-align: middle;">${promotionText}</span>
                                        </span>
                                        </span>
                              </td>
                              <td>
                                        <span style="color: #f9383f; font-size: 13px; font-weight: bold; border: 1px solid #ffd3d6;
                                            background-color: #fff8f8; border-radius: 4px; -webkit-border-radius: 4px;  -moz-border-radius: 4px;
                                            padding: 1px 5px;">
                                            Promo Price: S$${cachedDiscountPrice?string["0.00"]}
                                        </span>
                              </td>
                              <td colspan="2">&nbsp;</td>
                            </tr>
                            [/#list]
                        [/#if]
                    [/#if]
                [/#list]
            </tbody>
            <!--
            <tbody data-bind="visible: expandStandard">



            <tr class="bordered-top">


                <td rowspan="2">
                    <h3 class="heading">DAY FUN Pass: Play up to 20 attractions</h3>

                        <span class="details">
                        Usual Price: $330.90 (Adult); $276.90 (Child)
                        </span>

                </td>

                <td class="unit-price">Adult  - S$79.00</td>
                <td class="qty-list">
                    <select class="qty" data-std-id="456" data-std-code="8814"
                            data-std-type="Adult" data-std-set="false">
                        <option value="0">0</option>
                        <option value="1">1</option><option value="2">2</option>
                        <option value="3">3</option><option value="4">4</option>
                        <option value="5">5</option><option value="6">6</option>
                        <option value="7">7</option><option value="8">8</option>
                    </select>
                </td>
                <td>&nbsp;</td>
            </tr>


            <tr>


                <td class="unit-price">Child  - S$69.00</td>
                <td class="qty-list">
                    <select class="qty" data-std-id="457" data-std-code="8816"
                            data-std-type="Child" data-std-set="false">
                        <option value="0">0</option>
                        <option value="1">1</option><option value="2">2</option>
                        <option value="3">3</option><option value="4">4</option>
                        <option value="5">5</option><option value="6">6</option>
                        <option value="7">7</option><option value="8">8</option>
                    </select>
                </td>
                <td>&nbsp;</td>
            </tr>


            </tbody>
            -->
          </table>

          <!-- /ko --><!-- ko ifnot:isGeneral -->


          <!-- /ko -->

          <div class="split-table-container" style="display: none" data-bind="visible: showPromos">
            <table class="split-table split-header">
              <tr>
                <th class="col-detail">${staticText[lang]["promotions"]!"Promotions"}</th>
                <th class="col-price unit-price">${staticText[lang]["unitPrice"]!"Unit Price"}</th>
                <th class="col-qty qty-list">${staticText[lang]["quantity"]!"Qty."}</th>
                <th class="col-end-space">
                  <i class="fw-icon-collapse-alt icon-2" data-bind="click: toggleExpandPromos"></i>
                </th>
              </tr>
            </table>
            <div data-bind="visible: expandPromos">
              <!-- ko foreach:promoRows -->
              <table class="split-table split-content">
                <tbody data-bind="foreach: items">
                <tr data-bind="if: $index() == 0, css: {hidden: $index() != 0}">
                  <td class="col-detail" data-bind="attr: {rowspan: $parent.items().length}">
                    <h3 class="heading" data-bind="text: $parent.name"></h3>
                    <!-- ko if:$parent.shouldTruncate -->
                    <span class="details" data-bind="attr:{'data-trunc-trunc':$parent.name}">
                        <span data-bind="text: $parent.truncatedDesc"></span>
                        <a class="view-more"
                           data-bind="click: function(){$root.toggleFullDesc($parent.name, true)}">(more)</a>
                    </span>
                    <span class="details" style="display: none;"
                          data-bind="attr:{'data-trunc-full':$parent.name}">
                        <span data-bind="text: $parent.desc"></span>
                        <a class="view-more"
                           data-bind="click: function(){$root.toggleFullDesc($parent.name, false)}">(less)</a>
                    </span>
                    <!-- /ko -->
                    <!-- ko ifnot:$parent.shouldTruncate -->
                    <span class="details" data-bind="text: $parent.desc">
                    </span>
                    <!-- /ko -->
                  </td>
                  <td class="col-price unit-price" data-bind="text:typeDisplay + ' - S$' + price"></td>
                  <td class="col-qty qty-list">
                    <select class="qty" data-bind="value: qty">
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </td>
                  <td class="col-end-space">&nbsp;</td>
                </tr>
                <tr data-bind="if: $index() > 0, css: {hidden: $index() == 0}">
                  <td class="col-price unit-price" data-bind="text:typeDisplay + ' - S$' + price"></td>
                  <td class="col-qty qty-list">
                    <select class="qty" data-bind="value: qty">
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </td>
                  <td class="col-end-space">&nbsp;</td>
                </tr>
                <tr>
                  <td style="padding: 0 0 0 0.7em;">
                    <span class="promotion-content">
                        <span style="padding: 3px; 3px; display: block;">
                            <span data-bind="visible: promotionIconImageURL != ''"
                                  style="vertical-align: middle; width: 30px; display: inline-block;">
                              <img data-bind="attr: {'src': promotionIconImageURL}"/>
                            </span>
                            <span data-bind="text: promotionText"
                                  style="font-size: 0.9em; vertical-align: middle;"></span>
                        </span>
                    </span>
                  </td>
                  <td>
                    <span style="color: #f9383f; font-size: 13px; font-weight: bold; border: 1px solid #ffd3d6;
                        background-color: #fff8f8; border-radius: 4px; -webkit-border-radius: 4px;  -moz-border-radius: 4px;
                        padding: 1px 5px;">
                        Promo Price: S$<span data-bind="text: cachedDiscountPrice"></span>
                    </span>
                  </td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
              </table>
              <!-- /ko -->
            </div>
          </div>

        </div>
      </div>

      <div class="add-to-cart">
        <a class="continue-btn" id="btnAddToCartMain" data-bind="click: addToCart">${staticText[lang]["addToCart"]!"Add to Cart"}</a>

        <div class="clearfix"></div>
      </div>

    </div>

    <!-- /ko -->

    <script type="text/javascript">
      cmsBookingModel.topups = [
          [#list axPrdList as axPrd]
              [#assign axPrdDetail = axPrdDetailsMap[axPrd.getProperty("relatedAXProductUUID").getString()]]
              [#assign crossSells = starfn.getAXProductCrossSells(axPrdDetail)]
              [#if crossSells?has_content]
                  [#list crossSells as cs]
                      [#assign csParentListingId = axPrdDetail.getProperty("productListingId").getString()]
                      [#assign csName = cs.getProperty("crossSellItemName").getString()]
                      [#assign csItemImageUuid = cs.getProperty("crossSellItemImage").getString()]
                      [#assign csItemImageUrl =   damfn.getAssetLink(csItemImageUuid)]
                      [#assign csItemDesc = ""]
                      [#if cs.hasProperty("crossSellItemDesc")]
                          [#assign csItemDesc = cs.getProperty("crossSellItemDesc").getString()]
                      [/#if]
                      [#assign csPrice = "0.00"]
                      [#assign csType = "Adult"]
                      [#assign csProductCode = cs.getProperty("crossSellProductCode").getString()]
                      [#assign csRelatedAxProduct = starfn.getAxProductByProductCode("b2c-slm", csProductCode)]
                      [#assign csTicketType = "Standard"]
                      [#if csRelatedAxProduct.hasProperty("ticketType")]
                          [#assign csTicketType = csRelatedAxProduct.getProperty("ticketType").getString()]
                      [/#if]
                      [#assign csProductPrice = ""]
                      [#if csRelatedAxProduct.hasProperty("productPrice")]
                          [#assign csProductPrice = csRelatedAxProduct.getProperty("productPrice").getString()]
                      [/#if]
                      [#assign csListingId = cs.getProperty("crossSellProductListingId").getString()]
                    {
                      name: "${csName}",
                      url: "",
                      id: "",
                      desc: "${csItemDesc}",
                      image: "${csItemImageUrl}",
                      price: "${csProductPrice}",
                      unitPrice: ${csProductPrice},
                      type: "${csTicketType}",
                      typeDisplay: "${csTicketType}",
                      productCode: "${csProductCode}",
                      listingId: "${csListingId}",
                      csParentListingId: "${csParentListingId}",
                      csParentCmsProductId: "${prd}"
                    },
                  [/#list]
              [/#if]
          [/#list]
      ];
      // cmsBookingModel.topups.pop();
    </script>


    <!-- Topup Section -->

    <div class="add-topup" style="display: block;" data-bind="visible: showTopups() && topups().length > 0">
      <h3 class="heading">${staticText[lang]["wouldYouLikeTopUp"]!"Would you like to add a top-up?"}</h3>

      <div class="topup-list">
        <!-- ko foreach: topups -->
        <div class="topup">
          <div class="topup-pic"><img data-bind="attr: {'src': image}"/></div>
          <div class="topup-detail">
            <h4 class="heading" data-bind="text: name"></h4>

            <p class="description" data-bind="text: desc"></p>
            <!-- ko foreach: topupItems -->
            <div class="message-section" style="display:none"
                 data-bind="text: topupMsg, visible: showTopupMsg, css:{'error':topupMsgErr, 'success':!topupMsgErr()}"></div>
            <div class="price-detail">
              <span class="price" data-bind="text: typeDisplay + ' - S$' + price"></span>
              <select class="num" data-bind="value: qty">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
              <a class="add-btn" data-bind="click: addTopup">${staticText[lang]["add"]!"Add"}</a>
            </div>
            <!-- /ko -->
          </div>
        </div>
        <!-- /ko -->
      </div>
    </div>

    <!-- Recommended Items Section -->
      [#assign recommendedProducts = starfn.getRecommendProducts('b2c-slm',prd,'en')]
      [#if recommendedProducts?has_content]
        <script type="text/javascript">
          cmsBookingModel.recommendedItems = [
              [#list  recommendedProducts as rc]
                  [#assign rpImageUrl = ""]
                  [#if rc.imageUuid?has_content]
                      [#assign rpImageUrl = damfn.getAssetLink(rc.imageUuid)]
                  [/#if]
                {
                  name: "${rc.name!}",
                  url: "${rc.productUrl!}",
                  id: "${rc.@name}",
                  image: "${rpImageUrl}",
                  desc: "${rc.shortDescription!}"
                },
              [/#list]
          ];
        </script>
      [/#if]

    <div class="recommended" style="display: none;" data-bind="visible: showRecommended() && recommendeds().length > 0">
      <h3 class="heading">${staticText[lang]["youMayAlsoLike"]!"You may also like..."}</h3>
      <ul data-bind="foreach: recommendeds">
        <li>
          <a class="ticket" data-bind="attr: {'href': url},
                       style: {'background': 'url(\'' + image + '\')', 'backgroundSize': 'cover'}">
            <span class="mask-circle"></span>
                        <span class="info">
                            <span class="description" data-bind="text: desc"></span>
                        </span>
          </a>
          <a class="ticket-heading" data-bind="attr: {'href': url},
                        text: name"></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div data-bind="visible: !showMainForm() && topups().length == 0 && recommendeds().length == 0">
      <div class="btn-confirmation-nopad" style="display: none" data-bind="visible: showEndActions">
        <div class="button">
          <a class="continue-btn re-btn" style="width:230px"
             href="${sitePath}">${staticText[lang]["continueShopping"]!"Continue Shopping"}</a>
          <a class="continue-btn re-btn" style="width:230px"
             href="${cmsProductUrl}">${staticText[lang]["backToProduct"]!"Back To Product"}</a>
          <a href="${sitePath}/checkout" class="checkout-btn re-btn"
             style="width:230px">${staticText[lang]["checkout"]!"Check out"}</a>

          <div class="clearfix"></div>
        </div>
      </div>
    </div>

  </div>


  <!-- Booking Sidebar -->

  <div class="rightbar">
    <div class="notes" id="theInfoSec" data-bind="visible: showMainForm">
      <h3 class="heading">${staticText[lang]["notes"]!"Notes"}</h3>
    ${cmsProductNotes}
      <div>${staticText[lang]["pleaseCheck"]!"Please check"} <a href="../tnc~${prd}~"
                                                                target="_blank">${staticText[lang]["tnc"]!"Terms and Conditions"}</a>
      </div>
    </div>
    <div class="notes" data-bind="visible: showMainForm">
      <h3 class="heading">${staticText[lang]["recommendToFriends"]!"Recommend To Friends!"}</h3>

      <div class="share-division">
        <div class="fb-share-button" data-type="button_count"></div>
        <a href="https://twitter.com/share" class="twitter-share-button"
           data-text="Get the DAY FUN PASS Play 20 from the Sentosa Online Store!" data-via="Sentosa_Island"
           data-dnt="true">Tweet</a>
        <script>!function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
          if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
          }
        }(document, 'script', 'twitter-wjs');</script>


      </div>
    </div>
    <!-- ko stopBinding:true -->
    <div class="summary" id="bookingSummary">
      <h3 class="heading">
      ${staticText[lang]["bookingSummary"]!"Booking Summary"}
        <i class="fw-icon-collapse-alt" data-bind="visible: !noItems(), click: toggleExpand,
                   css: {'fw-icon-collapse-alt': isExpanded, 'fw-icon-expand-alt': !isExpanded()}"></i>
      </h3>
      <!-- ko if:noItems -->
      <div class="booking-list">
        <div class="empty">- ${staticText[lang]["cartIsEmpty"]!"Your Cart is empty"} -</div>
      </div>
      <!-- /ko -->
      <!-- ko foreach:items -->
      <div class="booking-list" data-bind="visible: $root.isExpanded()">
        <!-- ko foreach:baseModel.products -->
        <!-- ko if:cartType != 'Topup' -->
        <div class="main-ticket">
          <div class="remove-section">
            <a title="Click to remove item."
               data-bind="click: function(model, evt){ $root.doRemoveInSummary(model, evt, cartId); }"><i
              class="fw-icon-remove"></i></a>
          </div>
          <div class="ticket-type" data-bind="text: title"></div>
          <div class="num-price">
                        <span class="ticket-price" style="padding-left: 15px; width: auto;">
                            <span data-bind="text: totalText"></span>
                            <span data-bind="visible: discountTotal > 0, text: '(' + discountTotalText + ' off)'"
                                  style="color: red; padding-left: 5px;"></span>
                        </span>
            <span class="ticket-num">x<span data-bind="text: qty"></span></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /ko --><!-- ko if:cartType == 'Topup' -->
        <div class="ticket-topup">
          <div class="remove-section">
            <a data-bind="click: function(model, evt){ $root.doRemoveInSummary(model, evt, cartId); }"><i
              class="fw-icon-remove"></i></a>
          </div>
          <div class="ticket-type"><i class="fw-icon-plus-sign"></i>
            <span data-bind="text: title"></span></div>
          <div class="num-price">
            <span class="ticket-price"><span data-bind="text: totalText"></span></span>
            <span class="ticket-num">x<span data-bind="text: qty"></span></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /ko -->
        <!-- /ko -->
        <!-- ko if:baseModel.hasTopup -->
        <a class="add-more-topup" data-bind="click: goTopup">${staticText[lang]["addMoreTopUps"]!"add more top-ups"}</a>
        <!-- /ko -->
      </div>
      <!-- /ko -->

      <div class="total-price" style="height:50px" data-bind="visible: $root.isExpanded()">
        <span class="total">${staticText[lang]["total"]!"Total"} </span>
        <span class="amount"><span data-bind="text: totalText"></span></span>
      </div>
      <a class="viewcart" href="${sitePath}/checkout"><span
        class="view-text">${staticText[lang]["checkout"]!"Check out"}</span><img src="${themePath}/img/pink-arrow.png"/></a>

      <div class="clearfix"></div>
    </div>
    <!-- /ko -->

    <div class="help" style="display:none;">
      <h3 class="heading">${staticText[lang]["needHelp"]!"Need help?"}</h3>

      <p
        class="contact-phone-title">${staticText[lang]["servicesOfficersLocation"]!"Our Guest Services Officers are available at"} </p>

      <p class="contact-phone-content"><span
        class="call-content">1800-SENTOSA (736 8672) / +65-6736 8672 |<br>${staticText[lang]["servicesOfficersTime"]!"Mon to Sun, from 9am to 6pm"}</span>

      <p class="contact-email-title">${staticText[lang]["reachByEmail"]!"Or you may reach us via email at"} </p>

      <p class="contact-email-content"><a href="mailto:guest_services@sentosa.com.sg" target="_top" class="highlight">guest_services@sentosa.com.sg</a>
      </p>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="btn-submit" style="display: none"
       data-bind="visible: showEndActions() && (topups().length != 0 || recommendeds().length != 0)">
    <div class="button re-btn">
      <a class="continue-btn re-btn" style="width:230px"
         href="${sitePath}">${staticText[lang]["continueShopping"]!"Continue Shopping"}</a>
      <a class="continue-btn re-btn" style="width:230px"
         href="${cmsProductUrl}">${staticText[lang]["backToProduct"]!"Back To Product"}</a>
      <a href="${sitePath}/checkout" class="checkout-btn re-btn"
         style="width:230px">${staticText[lang]["checkout"]!"Check out"}</a>

      <div class="clearfix"></div>
    </div>
  </div>

</div>

<div class="back-to-top"><a href="javascript:nvx.scrollTo('top');"><i
  class="fw-icon-arrow-up"></i> ${staticText[lang]["backToTop"]!"Back to top"}</a></div>

<div style="height: 20px;">
</div>
[#elseif !cmsProductNode?has_content]
<h3 style="margin:20px;text-align:center;">No such product.</div>
[/#if]