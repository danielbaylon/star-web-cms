[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/maintenance.ftl"]

[#assign maintMessage = starfn.getMaintenanceMessage("b2c-slm")!""]

<div class="general-info-section">
[#if maintMessage?has_content]
  <div class="main-content-section-container" style="text-align: center;margin: 10px auto;">
    <h1 class="title">${maintMessage.getProperty("name").getString()}</h1>

    <div class="content">${maintMessage.getProperty("message").getString()}</div>
  </div>
[#else]
  <h1 class="title">${staticText[lang]["title"]!"We will be back soon!"}</h1>

  <p
    class="content">${staticText[lang]["content"]!"Our system is currently undergoing maintenance. Please check back shortly."}</p>
[/#if]
</div>
