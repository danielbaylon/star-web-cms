[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/footer.ftl"]

[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
[#assign isEditMode = cmsfn.editMode]
[#assign isAuthorPreview = cmsfn.authorInstance && cmsfn.previewMode]

[#assign isMaintenanceMode = starfn.isMaintenanceMode("b2c-slm") && !isOnPreview && !isEditMode && !isAuthorPreview]

<div class="footer">
    <div class="content">
        <div class="s-footer-other size-s">
            <a href="/pick-a-play-pass"><img src="${themePath}/img/help-me-decide-s.png"></a>
        </div>
        <div class="contact-us">
            <div class="contact-type">
                <h3 class="cleek" data-footer-toggle="contact">${staticText[lang]["service"]!"WE ARE HERE TO SERVE YOU"} <i class="size-s excoltoggle fw-icon-arrow-right"></i></h3>
                <div class="footer-section-content collapse" data-footer-toggle-body="contact">
                    <p class="call">${staticText[lang]["phone"]!"Our Guest Services Officers are available at
                        <span class=\"call-content\">1800-SENTOSA (736 8672) / +65-6736 8672 |<br>Mon to Sun, from 9am to 6pm</span>"}
                    </p>
                    <p class="email">${staticText[lang]["email"]!"Or you may reach us via email at <a href=\"mailto:guest_services@sentosa.com.sg\" target=\"_top\" class=\"highlight\">guest_services@sentosa.com.sg</a>"}</p>
                </div>
            </div>
            <div class="follow-us">
                <h3>${staticText[lang]["followUs"]!"FOLLOW US"}</h3>
                <a href="http://www.facebook.com/sentosaofficial" target="_blank" class="social-media"><img src="${themePath}/img/Facebook.png" alt="Facebook"></a>
                <a href="http://www.twitter.com/Sentosa_Island" target="_blank" class="social-media"><img src="${themePath}/img/Twitter.png" alt="twitter"></a>
                <a href="http://www.youtube.com/user/SentosaTV" target="_blank" class="social-media"><img src="${themePath}/img/YouTube.png" alt="YouTube"></a>
                <a href="http://instagram.com/sentosa_island" target="_blank" class="social-media"><img src="${themePath}/img/Instagram.png" alt="Instagram"></a>
                [#if lang == "zh_CN"]
                <a href="http://www.weibo.com/u/5329409910" target="_blank" class="social-media"><img style="width: 40px;height: 40px;" src="${themePath}/img/Sina_weibo.png" alt="SinaWeiBo"></a>
                [/#if]
            </div>
        </div>
        <div class="more-information">
            <h3 class="cleek" data-footer-toggle="more">${staticText[lang]["moreInfo"]!"MORE INFORMATION"} <i class="size-s excoltoggle fw-icon-arrow-right"></i></h3>
            <div class="footer-section-content collapse" data-footer-toggle-body="more">
                <a href="http://www.sentosa.com.sg" target="_blank">Sentosa.com</a>
                <a href="http://www.sentosa.com.sg/en/getting-to-around-sentosa/getting-to-sentosa" target="_blank">${staticText[lang]["toNAround"]!"Getting To &amp; Around Sentosa"}</a>
                <a href="http://www.sentosa.com.sg/en/about-us/sentosa-island" target="_blank">${staticText[lang]["aboutUs"]!"About Us"}</a>
                <a href="http://www.sentosa.com.sg/en/legal-notices/" target="_blank">${staticText[lang]["legalNotices"]!"Legal Notices"}</a>
                <a href="./data-protection-policy" target="_blank">${staticText[lang]["dpp"]!"Data Protection Policy"}</a>
            </div>
        </div>
        <div class="newsletter">
        [#if !isMaintenanceMode]
            <h3 class="cleek" data-footer-toggle="newsletter">${staticText[lang]["newsletter"]!"GET OUR NEWSLETTER"} <i class="size-s excoltoggle fw-icon-arrow-right"></i></h3>
            <div class="footer-section-content collapse" data-footer-toggle-body="newsletter">
                <p>${staticText[lang]["newsletterHint"]!"Enter your email address to get the latest updates and special promotions delivered straight to you."}</p>
                <div class="message-section" style="display:none;" id="msgNews"></div>
                <div class="subscribe">
                    <input placeholder="${staticText[lang]['enterEmail']!'Enter your email'}" id="inputNews" class="type-email" type="text">
                    <a class="subscribeBtn" id="btnNews">
                    ${staticText[lang]["subscribe"]!"Subscribe"}
                    </a>
                </div>
            </div>
        [/#if]
        </div>
        <div class="size-s follow-us">
            <h3>${staticText[lang]["followUs"]!"FOLLOW US"}</h3>
            <a href="http://www.facebook.com/sentosaofficial" target="_blank" class="social-media"><img src="${themePath}/img/Facebook.png" alt="Facebook"></a>
            <a href="http://www.twitter.com/Sentosa_Island" target="_blank" class="social-media"><img src="${themePath}/img/Twitter.png" alt="twitter"></a>
            <a href="http://www.youtube.com/user/SentosaTV" target="_blank" class="social-media"><img src="${themePath}/img/YouTube.png" alt="YouTube"></a>
            <a href="http://instagram.com/sentosa_island" target="_blank" class="social-media"><img src="${themePath}/img/Instagram.png" alt="Instagram"></a>
            [#if lang == "zh_CN"]
            <a href="http://www.weibo.com/u/5329409910" target="_blank" class="social-media"><img style="width: 40px;height: 40px;" src="${themePath}/img/Sina_weibo.png" alt="SinaWeiBo"></a>
            [/#if]
        </div>
        <div class="clearfix"></div>
        <div class="back-to-top"><a href="javascript:nvx.scrollTo('top');"><i class="fw-icon-arrow-up"></i> ${staticText[lang]["backToTop"]!"Back to top"}</a></div>
        <div class="copyright">${staticText[lang]["copyright"]!"Copyright © 2016 Sentosa. All rights reserved."}</div>
    </div>
</div>