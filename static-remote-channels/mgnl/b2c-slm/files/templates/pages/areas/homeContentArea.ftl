[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/highlights.ftl"]

[@cms.area name="homeCarousel"/]

<!-- Welcome Section -->
<div class="wrapper landing-main lmain">
[@cms.area name="welcome"/]
</div>

<!-- Highlights Section -->
<div class="landing-main lhighlights">
    <div class="highlights">
        <h2>${staticText[lang]["title"]!"Highlights"}</h2>
        <div class="highlights-main" style="padding:0 25px">
        [@cms.area name="highlights"/]
        </div>
    </div>
</div>