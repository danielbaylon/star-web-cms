[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/homeFooter.ftl"]

<div class="footer">
    <div class="content">
        <div class="s-footer-other size-s">
            <a href="/pick-a-play-pass"><img src="${themePath}/img/help-me-decide-s.png"></a>
        </div>
    [#assign cats = starfn.getCMSProductCategories("b2c-slm")!]
    [#if cats?has_content]
        [#assign catIndex = 0]
        [#list cats as catNode]
            [#if catIndex%2 == 0]
            <div class="col-sm-3">
            [/#if]
            [#if catNode?has_content]
                [#if catNode.hasProperty("name")]
                    [#assign catName = catNode.getProperty("name").getString()!]
                    [#assign catUrlPath = ""]
                    [#if catNode.hasProperty("generatedURLPath")]
                        [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
                    [/#if]
                    <div class="col-smap smap-packages" style="margin-top:${catIndex%2*2}em">
                        <h3 class="smap-main-head" style="text-transform:uppercase">${catName}<i class="excoltoggle fw-icon-minus"></i></h3>
                        [#assign subCats = starfn.getCMSProductSubCategories(catNode)!]
                        [#list subCats as subCat]
                            [#assign cmsPrds = starfn.getCMSProductBySubCategory(subCat)!]
                            <ul class="smap-list-main">
                                [#if subCat.getName() == "default"]
                                    [#assign cmsPrds = starfn.getCMSProductBySubCategory(subCat)!]
                                    [#list cmsPrds as cmsPrd]
                                        [#if cmsPrd.hasProperty("name")]
                                            [#assign cmsProductName = cmsPrd.getProperty("name").getString()]
                                            [#if cmsPrd.hasProperty("generatedURLPath")]
                                                [#assign cmsProductUrl = sitePath + "/category/product~" + cmsPrd.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${cmsPrd.getName()}~.html?category=${catUrlPath}"]
                                                <li class="smap-link"><a href="${cmsProductUrl}">${cmsProductName}</a></li>
                                            [/#if]
                                        [/#if]
                                    [/#list]
                                [#else]
                                    [#assign catName = subCat.getProperty("name").getString()!]
                                    <li class="smap-list-item-sub">
                                        <h4 class="smap-sub-head">${catName} <i class="excoltoggle fw-icon-plus"></i></h4>
                                        <ul class="smap-sub-rel collapse">
                                            [#assign cmsPrds = starfn.getCMSProductBySubCategory(subCat)!]
                                            [#list cmsPrds as cmsPrd]
                                                [#if cmsPrd.hasProperty("name")]
                                                    [#assign cmsProductName = cmsPrd.getProperty("name").getString()]
                                                    [#if cmsPrd.hasProperty("generatedURLPath")]
                                                        [#assign cmsProductUrl = sitePath + "/category/product~" + cmsPrd.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${cmsPrd.getName()}~.html?category=${catUrlPath}"]
                                                        <li class="smap-link"><a href="${cmsProductUrl}">${cmsProductName}</a></li>
                                                    [/#if]
                                                [/#if]
                                            [/#list]
                                        </ul>
                                    </li>
                                [/#if]
                            </ul>
                        [/#list]
                    </div>
                [/#if]
            [/#if]
            [#if catIndex%2 == 1 || catIndex == cats?size-1]
            </div>
                <!---->
            [/#if]
            [#assign catIndex = catIndex + 1]
        [/#list]
    [/#if]
        <div class="col-sm-3">
            <div class="more-information" style="width:100%;height:auto;">
                <h3 class="cleek" data-footer-toggle="more">${staticText[lang]["moreInfo"]!"MORE INFORMATION"} <i class="size-s excoltoggle fw-icon-plus"></i></h3>
                <div class="footer-section-content" data-footer-toggle-body="more">
                    <a href="http://www.sentosa.com.sg" target="_blank">Sentosa.com</a>
                    <a href="http://www.sentosa.com.sg/en/getting-to-around-sentosa/getting-to-sentosa" target="_blank">${staticText[lang]["toNAround"]!"Getting To &amp; Around Sentosa"}</a>
                    <a href="http://www.sentosa.com.sg/en/about-us/sentosa-island" target="_blank">${staticText[lang]["aboutUs"]!"About Us"}</a>
                    <a href="http://www.sentosa.com.sg/en/legal-notices/" target="_blank">${staticText[lang]["legalNotices"]!"Legal Notices"}</a>
                    <a href="${sitePath}/data-protection-policy" target="_blank">${staticText[lang]["dpp"]!"Data Protection Policy"}</a>
                </div>
            </div>
            <div class="newsletter" style="width:100%;height:auto;">
                <h3 class="cleek" data-footer-toggle="newsletter">${staticText[lang]["newsletter"]!"GET OUR NEWSLETTER"} <i class="size-s excoltoggle fw-icon-plus"></i></h3>
                <div class="footer-section-content" data-footer-toggle-body="newsletter">
                    <p>${staticText[lang]["newsletterHint"]!"Enter your email address to get the latest updates and special promotions delivered straight to you."}</p>
                    <div class="message-section" style="display:none;" id="msgNews"></div>
                    <div class="subscribe">
                        <input type="text" placeholder="${staticText[lang]['enterEmail']!'Enter your email'}" id="inputNews" class="type-email" />
                        <div class="keyster" id="keysterDiv" style="display:none; padding: 5px 10px 5px 0; line-height: 1.2; font-size: 0.9em; float: left;">
                            <input type="checkbox" id="keyster">&nbsp;&nbsp;&nbsp;${staticText[lang]["agreement"]!"I have read and agree to the Sentosa Online Store and Sentosa Leisure Management Pte Ltd's (\"SLM\") <a target=\"_blank\" href=\"${sitePath}/data-protection-policy\">Data Protection Policy</a>, including as to how my personal data may be collected, used, disclosed and processed, and confirm that all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself, I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents, to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out in the Data Protection Policy."}
                        </div>
                        <a class="subscribeBtn" id="btnNews" style="display:none;">
                        ${staticText[lang]["subscribe"]!"Subscribe"}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <div class="col-sm-3">
            <div class="contact-us" style="width:100%;height:auto;">
                <div class="contact-type">
                    <h3 class="cleek" data-footer-toggle="contact">${staticText[lang]["service"]!"WE ARE HERE TO SERVE YOU"} <i class="size-s excoltoggle fw-icon-plus"></i></h3>
                    <div class="footer-section-content" data-footer-toggle-body="contact">
                        <p class="call">${staticText[lang]["phone"]!"Our Guest Services Officers are available at <span class=\"call-content\">1800-SENTOSA (736 8672) / +65-6736 8672 |<br>Mon to Sun, from 9am to 6pm</span>"}
                        </p>
                        <p class="email">${staticText[lang]["email"]!"Or you may reach us via email at <a href=\"mailto:guest_services@sentosa.com.sg\" target=\"_top\" class=\"highlight\">guest_services@sentosa.com.sg</a>"}</p>
                    </div>
                </div>
                <div class="follow-us">
                    <h3>${staticText[lang]["followUs"]!"FOLLOW US"}</h3>
                    <a href="http://www.facebook.com/sentosaofficial" target="_blank" class="social-media"><img src="${themePath}/img/Facebook.png" alt="Facebook"></a>
                    <a href="http://www.twitter.com/Sentosa_Island" target="_blank" class="social-media"><img src="${themePath}/img/Twitter.png" alt="twitter"></a>
                    <a href="http://www.youtube.com/user/SentosaTV" target="_blank" class="social-media"><img src="${themePath}/img/YouTube.png" alt="YouTube"></a>
                    <a href="http://instagram.com/sentosa_island" target="_blank" class="social-media"><img src="${themePath}/img/Instagram.png" alt="Instagram"></a>
                    [#if lang == "zh_CN"]
                    <a href="http://www.weibo.com/u/5329409910" target="_blank" class="social-media"><img style="width: 40px;height: 40px;" src="${themePath}/img/Sina_weibo.png" alt="SinaWeiBo"></a>
                    [/#if]
                </div>
            </div>
        </div>
        <div class="size-s follow-us">
            <h3>${staticText[lang]["followUs"]!"FOLLOW US"}</h3>
            <a href="http://www.facebook.com/sentosaofficial" target="_blank" class="social-media"><img src="${themePath}/img/Facebook.png" alt="Facebook"></a>
            <a href="http://www.twitter.com/Sentosa_Island" target="_blank" class="social-media"><img src="${themePath}/img/Twitter.png" alt="twitter"></a>
            <a href="http://www.youtube.com/user/SentosaTV" target="_blank" class="social-media"><img src="${themePath}/img/YouTube.png" alt="YouTube"></a>
            <a href="http://instagram.com/sentosa_island" target="_blank" class="social-media"><img src="${themePath}/img/Instagram.png" alt="Instagram"></a>
            [#if lang == "zh_CN"]
            <a href="http://www.weibo.com/u/5329409910" target="_blank" class="social-media"><img style="width: 40px;height: 40px;" src="${themePath}/img/Sina_weibo.png" alt="SinaWeiBo"></a>
            [/#if]
        </div>
        <div class="clearfix"></div>
        <div class="back-to-top"><a href="javascript:nvx.scrollTo('top');"><i class="fw-icon-arrow-up"></i> ${staticText[lang]["backToTop"]!"Back to top"}</a></div>
        <div class="copyright">${staticText[lang]["copyright"]!"Copyright © 2016 Sentosa. All rights reserved."}</div>
    </div>
</div>