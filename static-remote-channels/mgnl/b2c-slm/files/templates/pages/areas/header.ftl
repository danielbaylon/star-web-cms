[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#include "/b2c-slm/templates/macros/staticText/header.ftl"]

[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
[#assign isEditMode = cmsfn.editMode]
[#assign isAuthorPreview = cmsfn.authorInstance && cmsfn.previewMode]

[#assign isMaintenanceMode = starfn.isMaintenanceMode("b2c-slm") && !isOnPreview && !isEditMode && !isAuthorPreview]

[#assign templateName = state.templateName!""]
[#if isMaintenanceMode && templateName != "b2c-slm:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/b2c-slm/maintenance")}
[#elseif !isMaintenanceMode && templateName == "b2c-slm:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/b2c-slm")}
[/#if]

<script>
  var jsSitePath = "${sitePath?js_string!""}";
</script>

<!-- HEADER-D -->
<div class="top-bar">
    <h1 class="visuallyhidden">Sentosa Online Store - Tickets</h1>
    <a class="ticketing-logo" href="${sitePath}"><img src="${themePath}/img/logo/ticket-logo-${imgSuffix}.png" alt="Sentosa Online Store - Sentosa Tickets" /></a>
[#if !isMaintenanceMode]
    <div class="right-head">
        <a class="signup" href="https://islander.com.sg/" target="_blank" onclick="return logIslanderClick();">${staticText[lang]["signUp"]!"Sign up"}</a>
        <div class="cart-section" id="headerCart">
            <a class="shopping-cart" href="${sitePath}/checkout">
                <span class="cart-items"><span class="number" data-bind="text: totalMainQty">0</span>&nbsp;${staticText[lang]["cartItems"]!"item(s)"}</span>
            </a>
            <div class="cart-summary" style="display: none;">
                <div class="up-arrow"></div>
                <div class="cart-heading">${staticText[lang]["yourCart"]!"YOUR CART"}</div>
                <!-- ko if:noItems -->
                <div class="booking-list">
                    <div class="empty">- ${staticText[lang]["cartEmpty"]!"Your Cart is empty"} -</div>
                </div>
                <!-- /ko -->
                <!-- ko foreach:items -->
                <div class="booking-list">
                    <!-- ko foreach:baseModel.products -->
                    <!-- ko if:cartType != 'Topup' -->
                    <div class="standard-ticket">
                        <div class="ticket-title" data-bind="text: title"></div>
                        <div class="ticket-num">x<span data-bind="text: qty"></span></div>
                        <div class="ticket-subtotal"><span data-bind="text: totalText"></span></div>
                    </div>
                    <!-- /ko --><!-- ko if:cartType == 'Topup' -->
                    <div class="topups">
                        <div class="ticket-title"><i class="fw-icon-plus-sign"></i><div class="ticket-tile-value" data-bind="text: title"></div></div>
                        <div class="ticket-num">x<span data-bind="text: qty"></span></div>
                        <div class="ticket-subtotal"><span data-bind="text: totalText"></span></div>
                    </div>
                    <!-- /ko -->
                    <!-- /ko -->
                </div>
                <!-- /ko -->

                <div class="total">${staticText[lang]["total"]!"Total"}: <div class="total-value"><span data-bind="text: totalText"></span></div></div>
                <a class="checkout" href="${sitePath}/checkout">${staticText[lang]["checkout"]!"Check out"}</a>
            </div>
        </div>
    </div>
[/#if]
    <div class="clearfix"></div>
</div>

<!-- HEADER-M -->
<header class="s-header size-s" id="top">
    <div class="s-logo">
        <a class="ticketing-logo" href="${sitePath}"><img src="${themePath}/img/logo/ticket-logo-en.png" alt="Sentosa Ticketing" /></a>
    </div>
    <nav class="s-nav-main">
        <ul>
            <li id="simpleNewsIcon" style="display: none;"><a href="javascript:void(0)"><img src="${themePath}/img/s-icon-news.png" /></a></li><!--
            --><li id="headerCartSimple">
            <!-- ko if:totalMainQty() > 0 -->
            <a href="${sitePath}/checkout" data-bind="visible: totalMainQty() > 0"
               style="display: none;" class="cart-simple-icon">
                <img src="${themePath}/img/s-icon-cart-orange.png" /><span data-bind="text: totalMainQty, css: { 'two-digit' : totalMainQty() > 9 }">0</span>
            </a>
            <!-- /ko -->
            <!-- ko if:totalMainQty() == 0 -->
            <a href="${sitePath}/checkout"><img src="${themePath}/img/s-icon-cart.png" /></a>
            <!-- /ko -->
        </li>
        </ul>
    </nav>
    <div class="clearfix"></div>
</header>

<div class="main-search" style="margin: 10px 0 10px 0;padding-bottom: 55px;">
[#if !isMaintenanceMode]
    <div class="search-form" style="float: left;width: 65%;margin: 0px auto;text-align:left;">
      <div style="padding-bottom: 5px; width:100%;">
        <i class="fw-icon-search" style="text-indent: 100%;white-space: nowrap;overflow: hidden;"></i>
        <input type="radio" name="qryTypeGrp" value="qryProduct"
               data-bind="checked: qryType">${staticText[lang]["product"]!"Product"}
        &nbsp;
        <input type="radio" name="qryTypeGrp" value="qryPromoCode"
               data-bind="checked: qryType">${staticText[lang]["promoCode"]!"Promo Code"}
      </div>
      <div style="width:100%;">
        <i class="fw-icon-search"></i>
        <input type="text" id="promoCodeInput" style="width:76%;display:none;"
               placeholder="${staticText[lang]["enterPromotionCode"]!"Enter Promotion Code"}"
               data-bind="value: qryPromoCode, visible: isQryPromoCodeVisible(), valueUpdate: 'keyup', event: { 'keypress' : checkEnterPromoCode }">
        <input type="text" id="quickFindInput" style="width:76%;"
               placeholder="${staticText[lang]["quickFind"]!"Quick Find"}"
               data-bind="value: qry, visible: isQryProductVisible(), valueUpdate: 'keyup', hasFocus: isSearchFocused, event: { 'keypress' : checkEnter }">
      </div>
    </div>
[/#if]
    <div style="float: right;">
    [#assign params = ctx.aggregationState.getSelectors()]
    [#if params?size == 4]
        [#assign langParam = params[1]]
    [/#if]
    [#assign localizedLinks = cmsfn.localizedLinks()!]
    [#if localizedLinks?has_content]
        <select id="languageSel" style="line-height: 28px;height: 28px;width: 110px; margin-top: 24px;" onChange="window.location.href=this.value">
        [#-- Current locale should not be linked. --]
        [#-- Use "compress" to put "li" and "a" on one line to prevent white spaces from interfering with layout. --]
            [#assign locales = sitefn.site().getI18n().getLocales()]
            [#list locales as locale]
                [#assign language = locale.toString()]
                [#assign current = cmsfn.isCurrentLocale(language)]
                [#if langParam?has_content]
                    [#assign newUrl = localizedLinks[language]?replace("~${langParam}~", "~${language}~", "r")]
                [#else]
                    [#assign newUrl = localizedLinks[language]]
                [/#if]
                [#assign queryString = ctx.request.getQueryString()!]
                [#if newUrl?has_content && queryString?has_content]
                    [#assign newUrl = newUrl + "?" + queryString]
                [/#if]
                <option value="${newUrl!'#'}" [#if current] selected="selected" [/#if]>${staticText[language]["languageDisplayName"]!locale.getDisplayName()}</option>
            [/#list]
        </select>
    [/#if]
    </div>
    <div class="search-results" style="margin: 10px;float: left;display: none;" data-bind="visible: showResPane, hasFocus: isResFocused, event: { 'mouseover': refocus, 'mouseout': unfocus }" style="display: none;" tabindex="1" >
        <div class="tooltip" data-bind="visible: showTooltip, text: tooltipText, css: tooltipState">
            <span>sample text here</span>
        </div>
        <div class="results-pane">
            <ul class="main-res" data-bind="foreach: titleResults, visible: titleResults().length > 0">
                <li><a data-bind="css: { 'selected': isDefault },
                   html: displayTitle + '<span class=\'ctg\'> in ' + category + '</span>', attr: { 'href':  '${sitePath}/'+urlTitle  }"></a></li>
            </ul>
            <ul class="sec-res" data-bind="foreach: descResults, visible: descResults().length > 0">
                <li><a data-bind="css: { 'selected': isDefault },
                   attr: { 'href':  '${sitePath}/'+urlTitle  }, html: '<span class=\'titlez\'>' + displayTitle + '</span> : ' + displayDescription"></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>

[#if !isMaintenanceMode]
    [#assign systemAnnouncements = starfn.getAnnouncements("b2c-slm")]
    [#if systemAnnouncements?has_content]
<div class="s-announce size-s size-s-force" style="display: none;">
    <ul class="">
        [#list systemAnnouncements as sa]
        <li class="">
            <div class="content">
              <h4 class="title">${sa.title?html}</h4>

              <p class="description">${sa.content!}</p>
            </div>
        </li>
        [/#list]
    </ul>
</div>

<div class="announcement-panel closed">
    <a class="puller" id="newsPuller"></a>
    <ul class="sequence-canvas">
        [#list systemAnnouncements as sa]
        <li class="newsee">
            <div class="content">
              <h4 class="title">${sa.title?html}</h4>

              <p class="description">${sa.content!}</p>
                [#if sa.image?has_content]
                    [#assign imageAssest = damfn.getAsset(sa.image)]
                    [#if imageAssest?has_content]
                        [#if sa.relatedLink?has_content]
                          <a href="${sa.relatedLink}" class="adv" target="_blank"><img src="${imageAssest.getLink()!}"
                                                                                       alt="${imageAssest.getCaption()!}"
                                                                                       width="261" height="143"/></a>
                        [#else]
                          <span class="adv"><img src="${imageAssest.getLink()!}" alt="${imageAssest.getCaption()!}"
                                                 width="261" height="143"/></span>
                        [/#if]
                    [/#if]
                [/#if]
            </div>
        </li>
        [/#list]
    </ul>
    <div class="page-switch">
        <div class="page-circle sequence-pagination">
            [#list systemAnnouncements as sa]
            <a class="grey-circle"></a>
            [/#list]
        </div>
        <div class="page-arrow">
            <a class="prev slider-arrow aprev"></a>
            <a class="next slider-arrow anext"></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
    [/#if]
[/#if]

<div class="overlay-container" style="display:none;">
    <a id='maxFunClose' href="#" class="btn-close"><img src="${themePath}/img/funpass/btn-close.png"/></a>
    <div class="overlay-panel">
        <div class="rudder-container">
            <a class="fun-finder" href="#"><img src="${themePath}/img/funpass/logo/funpass-${lang}.png"/></a>
            <div class="rudder"></div>
            <div class="bubbles">
                <div class="lt-pink-be"></div>
                <div class="lt-blue-be"></div>
                <div class="lt-purple-be"></div>
                <div class="rt-blue-be"></div>
                <div class="rt-yellow-be"></div>
                <div class="rt-red-be"></div>
                <div class="rt-pink-be"></div>
                <div class="rt-purple-be"></div>
                <div class="rt-green-be"></div>
            </div>
        </div>
    </div>
</div>
<div class="overlay-shortcut" style="display:none;">
    <a id='minFunClose' href="#" class="overlay-shortcut-close">&#10006;</a>
    <a href="#"><img src="${themePath}/img/funpass/logo/shortcut-${lang}.png"/></a>
</div>