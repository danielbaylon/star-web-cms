[#include "/b2c-slm/templates/macros/pageInit.ftl"]
[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = false]
[#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
    [#assign isOnPreview = true]
<div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
<br/>
[/#if]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign rootPath = "/b2c-slm/"]
[#assign cats = starfn.getCMSProductCategories("b2c-slm")!]
[#if cats?has_content]
<div class="nav">
    <ul>
        [#list cats as catNode]
            [#if catNode?has_content]
                [#assign assetLink = ""]
                [#assign catNameEN = ""]
                [#assign catName = ""]
                [#if catNode.hasProperty("iconImage")]
                    [#assign iconImage = catNode.getProperty("iconImage").getString()]
                    [#assign assetLink = damfn.getAssetLink(iconImage)]
                [/#if]
                [#if catNode.hasProperty("name")]
                    [#assign catName = catNode.getProperty("name").getString()]
                [/#if]
                [#if catNode.hasProperty("generatedURLPath")]
                    [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
                [/#if]
                [#assign catClass=""]
                [#assign catDisplay="none"]
                [#if catNode.getName() == params[2]!params[1]!]
                    [#assign catExists = true]
                    [#assign catClass="current"]
                    [#assign catDisplay="block"]
                [/#if]
                <li class="${catClass}">
                    <a href="${sitePath}/category~${catUrlPath}~${catNode.getName()!}~">
                        <img src="${assetLink}" />
                        <span class="nav-heading">${catName}</span>
                    </a>
                    <div class="nav-arrow" style="display:${catDisplay}"><img src="${themePath}/img/nav-arrow.png" /></div>
                </li>
            [/#if]
        [/#list]
    </ul>
    <div class="clearfix"></div>
    <!-- TODO -->
    <!--<a class="help-me-decide-section" href="pick-a-play-pass"></a>-->
</div>
<div class="s-categ size-s" id="products">
    <ul style="min-height: 44px; height: auto;">
        [#list cats as catNode]
            [#if catNode?has_content]
                [#if catNode.getName() == params[2]!params[1]!]
                    [#assign assetLink = ""]
                    [#assign catNameEN = ""]
                    [#assign catName = ""]
                    [#if catNode.hasProperty("iconImage")]
                        [#assign iconImage = catNode.getProperty("iconImage").getString()]
                        [#assign assetLink = damfn.getAssetLink(iconImage)]
                    [/#if]
                    [#if catNode.hasProperty("name")]
                        [#assign catName = catNode.getProperty("name").getString()]
                    [/#if]
                    <li class="current" style="width:55%;">
                        <img src="${assetLink}" />
                        <span>${catName}</span>
                    </li>[#list cats as catNodeInside][#if catNodeInside?has_content][#if catNodeInside.getName() != params[2]!params[1]!][#assign assetLink = ""][#assign catUrlPath = "#"][#if catNodeInside.hasProperty("iconImage")][#assign iconImage = catNodeInside.getProperty("iconImage").getString()][#assign assetLink = damfn.getAssetLink(iconImage)][/#if][#if catNodeInside.hasProperty("generatedURLPath")][#assign catUrlPath = catNodeInside.getProperty("generatedURLPath").getString()][/#if]<li><a href="${sitePath}/category~${catUrlPath}~${catNodeInside.getName()!}~"><img src="${assetLink}" /></a></li>[/#if][/#if][/#list]
                [/#if]
            [/#if]
        [/#list]
    </ul>
</div>
<div class="clearfix"></div>
[/#if]


[#if !isOnPreview && !cmsfn.editMode]
    [#assign requestUri = ctx.getRequest().getRequestURI()]
    [#if params?size == 0]
        [#if cats?size > 0]
            [#assign firstCatUrlPath = cats[0].getProperty("generatedURLPath").getString()]
        <script type="text/javascript">
            if ('${requestUri}' == '${sitePath}/category' || '${requestUri}' == '${sitePath}/category/') {
                window.location = "${sitePath}/category~${firstCatUrlPath}~${cats[0].getName()!}~";
            }
        </script>
        [/#if]
    [/#if]

    [#if requestUri?index_of("${sitePath}/category",0) > -1]
        [#if !catExists?has_content]
        <h3 style="margin:20px;text-align:center;">No such category.</div>
        [/#if]
    [/#if]
[/#if]