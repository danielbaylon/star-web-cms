[#-------------- ASSIGNMENTS --------------]
[#-- Page's model & definition, based on the rendering hierarchy and not the node hierarchy --]
[#assign urlVersion = "?ver=1.0r002"]
[#assign site = sitefn.site()!]

[#-------------- RENDERING --------------]
<title>${cmsfn.page(content).title!content.windowTitle!content.title!}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="${content.description!""}" />
<meta name="keywords" content="${content.keywords!""}" />
<meta name="author" content="Enovax Pte Ltd." />
<meta name="generator" content="Powered by Enovax - Intuitive Opensource CMS" />
<link rel="shortcut icon" href="${ctx.contextPath}/resources/b2c-slm/theme/default/img/favicon.ico${urlVersion}" />

<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script>window.html5 || document.write('<script src="${ctx.contextPath}/.resources/b2c-slm/theme/js/html5shiv.min.js"><\/script>')</script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

[#assign requestUri = ctx.getRequest().getRequestURI()]
[#assign requestUriArr = "${requestUri}"?split("/")]
[#assign lastUri = requestUriArr[requestUriArr?size-1]!""]
[#assign lastSecUri = requestUriArr[requestUriArr?size-2]!""]

[#assign isPreview = cmsfn.editMode!false]
[#if isPreview == true || lastUri == "b2c-slm" || lastUri == "b2c-slm.html" || (lastSecUri == "b2c-slm" && lastUri == "")]
<link href="${ctx.contextPath}/resources/b2c-slm/theme/default/css/styleloader-landing.css${urlVersion}" rel="stylesheet">
[/#if]
[#if requestUri?index_of("/funfinder",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-slm/theme/default/css/styleloader-funpass.css${urlVersion}" rel="stylesheet">
[/#if]
<link href="${ctx.contextPath}/resources/b2c-slm/theme/default/css/styleloader.css${urlVersion}" rel="stylesheet">

[#if requestUri?index_of("/funfinder",0) == -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-lib.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/jquery.sequence-min.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader.js${urlVersion}"></script>
[/#if]
[#if requestUri?index_of("/product",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-product.js${urlVersion}"></script>
[/#if]
[#if requestUri?index_of("/checkout",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-checkout.js${urlVersion}"></script>
[/#if]
[#if requestUri?index_of("/confirm",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-confirm.js${urlVersion}"></script>
[/#if]
[#if requestUri?index_of("/receipt",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-receipt.js${urlVersion}"></script>
[/#if]
[#if requestUri?index_of("/funfinder",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-slm/theme/default/js/scriptloader-funpass.js${urlVersion}"></script>
[/#if]
[#if lastUri == "b2c-slm" || lastUri == "b2c-slm.html" || (lastSecUri == "b2c-slm" && lastUri == "")]
<script type="text/javascript">
    $(document).ready(function() {
        $(".overlay-container").show();
        $(".overlay-container-bg").show();
        $(".overlay-shortcut").hide();
    });
</script>
[#else]
<script type="text/javascript">
    $(document).ready(function() {
        $(".overlay-container").hide();
        $(".overlay-container-bg").hide();
        $(".overlay-shortcut").show();
    });
</script>
[/#if]
<script type="text/javascript">
    // TODO: Temporarily hide Fun Finder until Backend ready. Remove this portion in production. ->
    $(document).ready(function() {
        $(".overlay-container").hide();
        $(".overlay-container-bg").hide();
        $(".overlay-shortcut").hide();
    });
    // <-
</script>

<style>
    .announcement-panel .puller{
        background: rgba(0, 0, 0, 0) url("${ctx.contextPath}/resources/b2c-slm/theme/default/img/slide/slide-btn-en.png") no-repeat scroll -41px 0;
    }
    .announcement-panel .puller:hover{
        background: rgba(0, 0, 0, 0) url("${ctx.contextPath}/resources/b2c-slm/theme/default/img/slide/slide-btn-en.png") no-repeat scroll 0 0;
    }
</style>