[#-------------- RENDERING PART --------------]

[#assign title = cmsfn.decode(content).title!"Welcome to Sentosa's Online Store!"]
[#assign message = cmsfn.decode(content).message!]

<div class="main-column" style="width:100%;padding:0 15px">
    <div class="main-column-text" style="min-height:150px">
        <h1>${title}</h1>
        <div id="mainContentDiv">
        ${message}
        </div>
    </div>
</div>