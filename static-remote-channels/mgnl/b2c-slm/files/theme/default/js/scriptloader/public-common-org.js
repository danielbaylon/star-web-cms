var baseCartView = {"sessionId": "", "totalQty": 0, "totalMainQty": 0};
var lang = '';
var sysLanVmsJson = '[{"id":1,"code":"en","name":"English","label":"English","status":"A"},{"id":2,"code":"cn","name":"Chinese","label":"简体（中文）","status":"A"}]';
var msg = {};
msg.MSG_ITEM_REMOVED_CART = 'Item removed from cart.';
msg.MSG_GEN_ERROR = 'Unable to process your request. Please try again in a few moments.';
msg.MSG_ENTER_EMAIL = 'Please enter a valid email address.';
msg.MSG_SUBSCRIB_EMAIL = "You've successfully subscribed to our mailing list!";
msg.MSG_SEARCHING = 'Searching...';
msg.MSG_PLS_ENTER = 'Please enter';
msg.MSG_MORE_CHARAC = 'more character(s).';
msg.MSG_SEARCH_NO_RESULT = "We didn't find anything related to your search. Please try again.";
msg.MSG_CONFIRM_REMOVE = 'Are you sure you want to remove';

function logIslanderClick() {
    try {
        ga('send', 'event', 'Sign Up Islander', 'Click');
    } catch (e) {
        console.log(e);
    }
    return true;
}
function logAdClick(fromWhere, adPath) {
    try {
        ga('send', 'event', 'Advertisement', 'Click', 'Originating Page: ' + fromWhere + ', Ad Link : ' + adPath);
    } catch (e) {
        console.log(e);
    }
    return true;
}

(function (nvx, $, Spinner, ko, baseCartView) {

    nvx.scrollTo = function (id) {
        $(window).scrollTop($('#' + id).offset().top);
    };

    var subbing = false;

    var decideClicked = false;
    var decideOut = false;

    var menuOut = false;
    var newsOut = false;

    $(document).ready(function () {
        //Cart-related change
        nvx.cart = new nvx.CartViewModel();
        nvx.cart.updateModel(baseCartView);
        try {
            ko.applyBindings(nvx.cart, document.getElementById('headerCart'));
            ko.applyBindings(nvx.cart, document.getElementById('headerCartSimple'));
        } catch (e) {
            console.log(e);
        }
        nvx.cart.getCart();

        $('#headerCart').hover(function () {
            $('.cart-summary').slideDown(200);
        }, function () {
            $('.cart-summary').slideUp(200);
        });

        //Search-related
        nvx.search = new nvx.SearchModel();
        try {
            ko.applyBindings(nvx.search, $('.main-search')[0]);
        } catch (e) {
            console.log(e);
        }

        //Nav-related
        $('#headerBtnMenu').click(function (evt) {
            if (menuOut) {
                $('#headerBtnMenu').removeClass('selected');
                $('#headerBtnMenu > img').attr('src', '/img/s-icon-menu.png');
                $('nav.s-nav-sub').slideUp(200);
            } else {
                if (newsOut) {
                    $('#simpleNewsIcon').click();
                }
                $('#headerBtnMenu').addClass('selected');
                $('#headerBtnMenu > img').attr('src', '/img/s-icon-menu-selected.png');
                $('nav.s-nav-sub').slideDown(200);
            }
            menuOut = !menuOut;
        });

        $('#simpleNewsIcon').click(function (evt) {
            if (newsOut) {
                $('#simpleNewsIcon > a').removeClass('selected');
                $('#simpleNewsIcon > a > img').attr('src', '/img/s-icon-news.png');
                $('div.s-announce').slideUp(200);
            } else {
                if (menuOut) {
                    $('#headerBtnMenu').click();
                }
                $('#simpleNewsIcon > a').addClass('selected');
                $('#simpleNewsIcon > a > img').attr('src', '/img/s-icon-news-selected.png');
                $('div.s-announce').slideDown(200);
            }
            newsOut = !newsOut;
        });

        //Help Me Decide-related
        $('#btnHelpMeDecide').click(function (evt) {
            if (!decideClicked) {
                decideClicked = true;
                try {
                    ga('send', 'event', 'Help Me Decide', 'Click');
                } catch (e) {
                    console.log(e);
                }
            }
            if (decideOut) {
                $('#msgHelpMeDecide').slideUp();
            } else {
                $('#msgHelpMeDecide').slideDown();
            }
            decideOut = !decideOut;
        });
        $('#btnCloseHelpMeDecide').click(function (evt) {
            decideOut = false;
            $('#msgHelpMeDecide').slideUp();
        });

        //Footer-related
        $('h3.cleek').click(function (evt) {
            var boom = $(evt.currentTarget).attr('data-footer-toggle');
            var icn = $(evt.currentTarget).find('i.excoltoggle');
            var bdy = $('.footer-section-content[data-footer-toggle-body=' + boom + ']');
            if (bdy.hasClass('collapse')) {
                icn.removeClass('fw-icon-plus').addClass('fw-icon-minus');
                bdy.removeClass('collapse');
            } else {
                icn.removeClass('fw-icon-minus').addClass('fw-icon-plus');
                bdy.addClass('collapse');
            }
        });

        //MAIN STACK JS

        //Newsletter-related

        $('#inputNews').on('change keyup paste', function () {
            if ($('#inputNews').val()) {
                $('#keysterDiv').show();
            } else {
                if (!$('#keyster').is(':checked')) {
                    $('#keysterDiv').hide();
                }
            }
        });

        $('#keyster').change(function () {
            var checked = $('#keyster').is(':checked');
            if (checked) {
                $('#btnNews').show();
            } else {
                $('#btnNews').hide();
            }
        });

        $('#btnNews').click(function (evt) {
            if (subbing) {
                return;
            }
            var $msg = $('#msgNews');
            var email = $('#inputNews').val();

            if (nvx.isEmpty(email) || !nvx.isValidEmail(email)) {
                $msg.text(msg.MSG_ENTER_EMAIL).removeClass('success').addClass('error').show();
                return;
            }

            if (!$('#keyster').is(':checked')) {
                $msg.text('Please tick the checkbox of the agreement.').removeClass('success').addClass('error').show();
                return;
            }

            $msg.hide();
            subbing = true;

            function doSubscribe(opts, callback) {
                $.ajax({
                    url: '/ajax/subscribe',
                    data: {email: email}, type: 'POST', cache: false, dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            $msg.text(msg.MSG_SUBSCRIB_EMAIL).removeClass('error').addClass('success').show();
                        } else {
                            $msg.text(data.message).removeClass('success').addClass('error').show();
                        }
                    },
                    error: function () {
                        $msg.text(msg.MSG_GEN_ERROR).removeClass('success').addClass('error').show();
                    },
                    complete: function () {
                        callback();
                        subbing = false;
                    }
                });
            }

            nvx.doButtonLoading(evt.target, doSubscribe, {});
        });
    });

    /*
     * Knockout extensions
     */

    ko.bindingHandlers.stopBinding = {
        init: function () {
            return {controlsDescendantBindings: true};
        }
    };
    ko.virtualElements.allowedBindings.stopBinding = true;

    /*
     * Search
     */

    nvx.search = {};

    nvx.SearchModel = function () {
        var s = this;

        s.qry = ko.observable('');

        s.qryPromoCode = ko.observable('');

        s.qryType = ko.observable('qryProduct');

        s.AUTO_THRESHOLD = 500;

        var toid = null;

        s.qry.subscribe(function (newVal) {
            if (newVal.length < 3) {
                s.titleResults([]);
                s.descResults([]);

                s.tt(true, msg.MSG_PLS_ENTER + ' ' + (3 - newVal.length) + ' ' + msg.MSG_MORE_CHARAC);

                return;
            }

            s.tt(true, msg.MSG_SEARCHING, 'success');

            clearTimeout(toid);

            toid = setTimeout(function () {
                s.tt(true, msg.MSG_SEARCH_NO_RESULT, 'warning');   //TO BE REMOVED
                $.ajax({
                    url: (nvx.lanns ? nvx.lanns : '/') + '.store/store/quick-search',
                    type: 'POST',
                    data: JSON.stringify({
                        qry: newVal
                    }),
                    headers: {'Store-Api-Channel': 'b2c-slm', 'Store-Api-Locale': jsLang},
                    cache: false,
                    dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        s.titleResults([]);
                        s.descResults([]);

                        if (data.success) {
                            populateResults(data.data);
                            if (data.data.result == 'NotFound') {
                                s.tt(true, msg.MSG_SEARCH_NO_RESULT, 'warning');
                            } else {
                                s.tt(false);
                                //s.tt(true, 'We found ' + data.qsRes.prods.length + ' item(s) matching your search!', 'success');
                            }
                        } else {
                            console.log(data.message);
                            s.tt(true, msg.MSG_GEN_ERROR, 'error');
                        }
                    },
                    error: function () {
                        s.titleResults([]);
                        s.descResults([]);
                        s.tt(true, msg.MSG_GEN_ERROR, 'error');
                    }
                });
            }, s.AUTO_THRESHOLD);
        });

        s.isQryPromoCodeVisible = function () {
            if ('qryPromoCode' == s.qryType()) {
                return true;
            } else {
                return false;
            }
        };

        s.isQryProductVisible = function () {
            if ('qryProduct' == s.qryType()) {
                return true;
            } else {
                return false;
            }
        };

        s.checkEnter = function (model, e) {
            if (e.keyCode == 13 || e.which == 13) {
                $.each(s.titleResults(), function (idx, val) {
                    if (val.isDefault) {
                        window.location.href = (nvx.lanns ? nvx.lanns : '/') + val.urlTitle;
                        return false;
                    }
                });
                $.each(s.descResults(), function (idx, val) {
                    if (val.isDefault) {
                        window.location.href = (nvx.lanns ? nvx.lanns : '/') + val.urlTitle;
                        return false;
                    }
                });
            } else {
                return true;
            }
        };

        s.checkEnterPromoCode = function (model, e) {
            if (e.keyCode == 13 || e.which == 13) {
                window.location.href = jsSitePath + '/promo~' + s.qryPromoCode() + '~';
                return false;
            } else {
                return true;
            }
        };

        s.showTooltip = ko.observable(true);
        s.tooltipText = ko.observable('');
        s.tooltipState = ko.observable('default');
        s.tt = function (toggle, text, state) {
            if (text) {
                s.tooltipText(text);
            }
            s.showTooltip(toggle);
            s.tooltipState(state ? state : 'default');
        };

        s.isSearchFocused = ko.observable(true);
        s.isResFocused = ko.observable(false);
        s.hoverFocused = ko.observable(false);

        s.refocus = function () {
            s.hoverFocused(true);
            return true;
        };
        s.unfocus = function () {
            s.hoverFocused(false);
            return true;
        };

        s.showResPane = ko.computed(function () {
            return (s.hoverFocused() || s.isSearchFocused() || s.isResFocused()) && s.qry().length > 0;
        });

        s.titleResults = ko.observableArray([]);
        s.descResults = ko.observableArray([]);

        function populateResults(qsRes) {
            $.each(qsRes.prods, function (idx, res) {
                if (res.foundInTitle) {
                    s.titleResults.push(res);
                } else {
                    s.descResults.push(res);
                }
            });
        }
    };

    /*
     * Utilities and shared functions
     */

    nvx.SPIN_OPTS_BTN = {
        lines: 9, // The number of lines to draw
        length: 6, // The length of each line
        width: 3, // The line thickness
        radius: 4, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset 
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#FFFFFF', // #rgb or #rrggbb or array of colors
        speed: 1.2, // Rounds per second
        trail: 47, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };

    nvx.BASE_SPINNER_OPTS = {
        lines: 13, // The number of lines to draw
        length: 16, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        rotate: 0, // The rotation offset
        color: '#000', // #rgb or #rrggbb
        speed: 1.2, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };

    nvx.spinner = new function () {
        var s = this;
        s.spinner = new Spinner(nvx.BASE_SPINNER_OPTS);
        s.elemId = 'spinner-overlay';
        s.start = function () {
            $('#' + s.elemId).show();
            s.spinner.spin(document.getElementById(s.elemId));
        };
        s.stop = function () {
            s.spinner.stop();
            $('#' + s.elemId).hide();
        };
    };

    nvx.doButtonLoading = function (target, mainFunc, opts) {
        var $target = $(target);
        var savedText = $target.html();
        $target.html('&nbsp;');
        var loader = new Spinner(nvx.SPIN_OPTS_BTN).spin($target[0]);

        mainFunc(opts, function () {
            loader.stop();
            $target.html(savedText);
        });
    };

    nvx.showButtonLoading = function (target) {
        var $target = $(target);
        var savedText = $target.html();
        $target.html('&nbsp;');
        var loader = new Spinner(nvx.SPIN_OPTS_BTN).spin($target[0]);

        return {savedText: savedText, loader: loader};
    };

})(window.nvx = window.nvx || {}, jQuery, Spinner, ko, baseCartView);