(function (nvx, $, Spinner, ko, baseCartView) {
    /*
     * Cart
     */

    nvx.cart = {};

    nvx.CartViewModel = function () {
        var s = this;

        s.baseModel = null; //This base model is not guaranteed to be updated.

        s.total = ko.observable(0);
        s.totalText = ko.observable('0.00');
        s.totalQty = ko.observable(0);
        s.totalMainQty = ko.observable(0);
        s.noItems = ko.observable(true);

        s.isExpanded = ko.observable(true);
        s.toggleExpand = function () {
            s.isExpanded(!s.isExpanded());
        };

        s.items = ko.observableArray([]);

        s.getCart = function () {
            $.ajax({
                url: '/.store/store/get-cart',
                data: {}, headers: {'Store-Api-Channel': 'b2c-slm'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        if (data.data) {
                            nvx.cart.updateModel(data.data);
                        }
                    } else {

                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };

        s.updateModel = function (model) {
            s.baseModel = model;
            s.total(model.total);
            s.totalText(model.totalText ? model.totalText : '0.00');
            s.totalQty(model.totalQty);
            s.totalMainQty(model.totalMainQty);

            s.items([]);
            s.noItems(true);
            if (model.cmsProducts) {
                s.noItems(false);
                $.each(model.cmsProducts, function (idx, product) {
                    s.items.push(new nvx.CartUnitViewModel(product));
                });
            }
        };

        s.doRemoveInSummary = function (model, evt, cartId) {
            if (!confirm(msg.MSG_CONFIRM_REMOVE + ' ' + model.title + '?')) {
                return;
            }

            nvx.spinner.start();

            $.ajax({
                url: '/.store/store/remove-item-from-cart/' + cartId,
                data: {},
                headers: {'Store-Api-Channel': 'b2c-slm'},
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        console.log(data);
                        s.updateModel(data.data);
                        alert(msg.MSG_ITEM_REMOVED_CART);
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(msg.MSG_GEN_ERROR);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        };

        s.doRemove = function (model, evt, cartId) {
            if (!confirm(msg.MSG_CONFIRM_REMOVE + ' ' + model.title + '?')) {
                return;
            }

            nvx.spinner.start();

            $.ajax({
                url: '/.store/store/remove-item-from-cart/' + cartId,
                data: {},
                headers: {'Store-Api-Channel': 'b2c-slm'},
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(msg.MSG_GEN_ERROR);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        };
    };

    nvx.TncViewModel = function (model) {
        var s = this;

        s.title = model.title;
        s.content = model.content;
    };

    nvx.CartUnitViewModel = function (model) {
        var s = this;

        s.baseModel = {};
        s.baseModel.hasTopup = false;
        s.baseModel.type = 'GeneralTicket';
        s.baseModel.id = model.id;
        s.baseModel.title = model.name;
        s.baseModel.subtotal = model.subtotal;
        s.baseModel.subtotalText = model.subtotalText;
        s.baseModel.products = [];

        $.each(model.items, function (idx, item) {
            var prod = {};
            prod.cartId = item.cartItemId;
            prod.itemCode = item.productCode;
            prod.listingId = item.listingId;
            prod.cartType = item.topup ? 'Topup' : 'Standard';
            prod.title = item.name + ' - ' + item.type;
            prod.productDescription = item.productDescription;
            prod.description = item.description;
            prod.isTopup = false;
            prod.multiplier = 1;
            prod.qty = item.qty;
            prod.ticketType = item.type;
            prod.price = item.price;
            prod.priceText = item.priceText;
            prod.total = item.total;
            prod.totalText = item.totalText;
            prod.discountTotal = item.discountTotal;
            prod.discountTotalText = item.discountTotalText;
            prod.discountLabel = item.discountLabel;
            prod.promoLabels = item.promoLabels;
            s.baseModel.products.push(prod);
        });

        s.createTopupUrl = function () {
            var heyUrl = '#!/topups';
            return heyUrl;
        };

        s.goTopup = function () {
            window.location.href = s.createTopupUrl();
        };
    };
})(window.nvx = window.nvx || {}, jQuery, Spinner, ko, baseCartView);