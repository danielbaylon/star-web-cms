$(document).ready(function() {
    $("#maxFunClose").on('click', function() {
        $(".overlay-container").hide();
        $(".overlay-container-bg").hide();
        $(".overlay-shortcut").show();
    });
    var closeMaxFunPop = function() {
        $(".overlay-container").hide();
        $(".overlay-container-bg").hide();
        $(".overlay-shortcut").show();
    };
    $(".overlay-shortcut").on('click',function() {
        if($(".overlay-shortcut").is(":visible")){
            $(".overlay-container").show();
            $(".overlay-container-bg").show();
            $(".overlay-shortcut").hide();
        }
    });
    $(".fun-finder").on('click',function() {
        nvx.spinner.start();
        var site = 'b2c-slm';
        var currUrl = window.location.href;
        var begin = currUrl.indexOf(site);
        if (begin > 0) {
            var targetUrl = currUrl.substring(0, begin) + site + '/funfinder';
            window.open(targetUrl);
        }
        window.setTimeout(closeFunPassPop,3000);
    });
    var closeFunPassPop = function() {
        nvx.spinner.stop();
        $(".overlay-container").hide();
        $(".overlay-container-bg").hide();
        $(".overlay-shortcut").show();
    };
    $("#minFunClose").on('click',function() {
        $(".overlay-container").hide();
        $(".overlay-shortcut").hide();
    });
});