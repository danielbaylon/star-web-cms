(function(nvx) {
    
    // Avoid `console` errors in browsers that lack a console.
    (function() {
        var method;
        var noop = function () {};
        var methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeStamp', 'trace', 'warn'
        ];
        var length = methods.length;
        var console = (window.console = window.console || {});

        while (length--) {
            method = methods[length];

            // Only stub undefined methods.
            if (!console[method]) {
                console[method] = noop;
            }
        }
    }());
    
    if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
          return this.replace(/^\s+|\s+$/g, ''); 
        };
    }
    
    nvx.doIframeSubmit = function(iframeId, callback) {
        $('<iframe id="' + iframeId + '" name="' + iframeId + '" src="" style="display:none;"></iframe>').appendTo('body');
        $('#' + iframeId).load(function() {
            var $iframe = $('#' + iframeId);
            callback($iframe.contents());
            $iframe.remove();
        });
    };
    
    nvx.internalDateFormat = 'dd/mm/yyyy';
    
    nvx.dateToString = function(inputDate, truncDate) {
        var month = inputDate.getMonth() + 1;
        var day = inputDate.getDate();
        var year = inputDate.getFullYear();
        if (truncDate) { year = ('' + year).substring(2,4); }
        return day + '/' + month + '/' + year ;
    };
    
    /*
     * Parses a date string (no time component) and returns an object with 2 fields:
     * date: the new Date object
     * parts: a Parts object with year, month (month is 1-indexed), day fields
     */
    nvx.parseInternalDate = function(dateString) {
        var parts = dateString.match(/(\d+)/g); //Match all digit substrings
        var year = parts[2];
        var month = parts[1];
        var day = parts[0];
        return { date: new Date(year, month - 1, day) , parts: { year: year, month: month, day: day } };
    };
    
    nvx.DEFAULT_ZEROES = 2; //default 2
    nvx.prependZeroes = function(number, len) {
        var s = number.toString();
        var _len = len ? len : nvx.DEFAULT_ZEROES;
        while (s.length < _len) {
            s = '0' + s;
        }
        return s;
    };

    /* UTILS */
    
    nvx.sortBy = function(field, reverse, primer) {
       var key = function (x) {return primer ? primer(x[field]) : x[field];};
       return function (a,b) {
           var A = key(a), B = key(b);
           return ((A < B) ? -1 : (A > B) ? +1 : 0) * [-1,1][+!!reverse];                  
       };
    };
    
    nvx.endsWith = function(str, suffix) { return str.indexOf(suffix, str.length - suffix.length) !== -1; };
    
    nvx.removeDuplicates = function(arr) {
        var unduped = [];
        var unduper = {};
        for (var i = 0; i < arr.length; i++) {
            unduper[arr[i]] = arr[i];
        }
        for (var key in unduper) {
            unduped.push(unduper[key]);
        }
        return unduped;
    };
    
    nvx.sortNumberCast = function(a, b) { return Number(a) - Number(b); };
    
    nvx.isNumber = function(n) { return !isNaN(parseFloat(n)) && isFinite(n); };
    
    /** Input MUST be already be a number. WILL NOT accept strings. */
    nvx.isInteger = function(n) { return n===+n && n===(n|0); };
})(window.nvx = window.nvx || {});