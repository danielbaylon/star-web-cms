doTimeoutDetection = false;
nvx.MSG_CANCEL_TRANSACTION = 'Are you sure you want to cancel your transaction?';
nvx.MSG_OK_PAYMENT = 'Please click OK to continue your payment.';
nvx.MSG_LEAVE_CHECKOUT = 'Leaving this page will cancel your checkout. Are you sure you want to do this?';
nvx.MSG_TRANS_TIMEOUT = 'Your transaction has timed out. Please try again.';

(function(nvx, $) {

    $(document).ready(function() {

        nvx.checkout = new nvx.CheckoutViewModel();
        $.ajax({
            url: '/.store/store/get-receipt',
            data: {}, headers: {'Store-Api-Channel': 'b2c-slm', 'Store-Api-Locale': jsLang},
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    nvx.checkout.updateCheckoutModel(data.data);
                    ko.applyBindings(nvx.checkout, document.getElementById('theCheckout'));

                    if (data.data.customer) {
                        nvx.deets.initDeets(data.data.customer, data.data.payMethod);
                    }
                    if (data.data.payMethod) {
                        nvx.deets.initPayMethod(data.data.payMethod);
                        nvx.deets.paymentTypeLabel(data.data.paymentTypeLabel);
                        nvx.deets.message(data.data.message);
                    }

                    ko.applyBindings(nvx.deets, document.getElementById('personalInfo'));
                } else {
                    alert(data.message);
                }
            },
            error: function () {
                alert('System encountered an error. Please refresh your page.');
            },
            complete: function () {
            }
        });
        nvx.deets = new nvx.CheckoutDeets('deetsForm');

        $('#btnPay').click(function() {
            if (nvx.btnGuard) {return;}
            if (confirm(nvx.MSG_OK_PAYMENT)) {
                nvx.btnGuard = true;

                $.ajax({
                    url: '/.store/store/checkout-confirm',
                    data: {}, headers: { 'Store-Api-Channel' : 'b2c-slm' },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            console.log(data.data);
                            if (data.data.tmRedirect) {
                                window.location.replace(data.data.redirectUrl);
                            } else {
                                window.location.replace('./receipt');
                            }
                        } else {
                            //TODO have to handle other error scenarios that may require page reload
                            alert(data.message);
                            nvx.btnGuard = true;
                            $.ajax({
                                url: '/.store/store/checkout-cancel',
                                data: {}, headers: { 'Store-Api-Channel' : 'b2c-slm' },
                                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                                success: function (data) {
                                },
                                error: function () {
                                },
                                complete: function () {
                                    window.location.replace('./checkout');
                                }
                            });
                        }
                    },
                    error: function () {
                        alert('System error occurred. Please try again.');
                        nvx.btnGuard = true;
                        $.ajax({
                            url: '/.store/store/checkout-cancel',
                            data: {}, headers: { 'Store-Api-Channel' : 'b2c-slm' },
                            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                            success: function (data) {
                            },
                            error: function () {
                            },
                            complete: function () {
                                window.location.replace('./checkout');
                            }
                        });
                    },
                    complete: function () {
                    }
                });

                //nvx.showButtonLoading(document.getElementById('btnPay'));
                ////window.location.replace((nvx.lanns?nvx.lanns:'/')+'confirm/finalize');
                //window.location.replace('./receipt');
            }
        });

        $('#btnCancel').click(function() {
            if (nvx.btnGuard) {return;}
            if (confirm(nvx.MSG_CANCEL_TRANSACTION)) {
                nvx.btnGuard = true;
                $.ajax({
                    url: '/.store/store/checkout-cancel',
                    data: {}, headers: { 'Store-Api-Channel' : 'b2c-slm' },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                    },
                    error: function () {
                    },
                    complete: function () {
                        window.location.replace('./checkout');
                    }
                });
            }
        });

        window.onbeforeunload = function(e) {
            if (nvx.btnGuard) {return;}
            return nvx.MSG_LEAVE_CHECKOUT;
        };

        window.onunload = function(e) {
            if (nvx.btnGuard) {return;}
            $.ajax({
                type: 'POST', async: false, cache: false,
                url: (nvx.lanns?nvx.lanns:'/')+'confirm/process-incomplete',
                data: {}, dataType: "json",
                success: function (result) {}
            });
        };

        nvx.tickCountdown(600);
    });

    nvx.btnGuard = false;

    nvx.tickCountdown = function(totalSeconds) {
        if (totalSeconds <= 0) {
            alert(nvx.MSG_TRANS_TIMEOUT);
            nvx.btnGuard = true;
            //window.location.replace('/checkout/cancel');
            window.location.replace('./checkout');
            return;
        }
        var text = formatTotalSeconds(totalSeconds);
        $('#confirmTimer').text(text);

        setTimeout(function () { nvx.tickCountdown(totalSeconds - 1); }, 1000);
    };

    function formatTotalSeconds(totalSeconds) {
        var minutes = 0;
        var seconds = 0;
        if(totalSeconds > 0) {
            minutes = Math.floor(totalSeconds / 60);
            seconds = totalSeconds % 60;
        }
        return formatMinuteSecond(minutes, seconds);
    }

    function formatMinuteSecond(minutes, seconds) {
        return to2Digits(minutes) + ":" + to2Digits(seconds);
    }

    function to2Digits(num) {
        var s = num.toString();
        if (s.length < 2) { s = "0" + s; }
        return s;
    }

    nvx.checkout = {};

    nvx.CheckoutViewModel = function() {
        var s = this;

        s.hasBookingFee = ko.observable(true);
        s.bookFeeMode = ko.observable('');
        s.bookFeeQty = ko.observable(0);
        s.bookPriceText = ko.observable('');
        s.bookFeeSubTotalText = ko.observable('');
        s.waivedTotalText = ko.observable('');
    };
    nvx.CheckoutViewModel.prototype = new nvx.CartViewModel();
    nvx.CheckoutViewModel.prototype.updateCheckoutModel = function(baseView) {
        this.updateModel(baseView);

        this.hasBookingFee(baseView.hasBookingFee);
        this.bookFeeMode(baseView.bookFeeMode);
        this.bookFeeQty(baseView.bookFeeQty);
        this.bookPriceText(baseView.bookPriceText);
        this.bookFeeSubTotalText(baseView.bookFeeSubTotalText);
        this.waivedTotalText(baseView.waivedTotalText);
    };

    nvx.deets = {};

    nvx.CheckoutDeets = function(formId) {
        var s = this;

        s.formId = formId;

        s.initDeets = function(base, payMethod) {
            s.idType(base.idType);
            s.idNo(base.idNo);
            s.email(base.email);
            s.mobile(base.mobile);
            s.nationality(base.nationality);
            s.referSource(base.referSource);
            s.dob(base.dob);
            s.initPayMethod(payMethod, base.paymentType);
            s.subscribed(base.subscribed);
            s.name(base.name);
        };

        s.initPayMethod = function(payMethod, paymentType) {
            if (payMethod) {
                s.payMethod = payMethod;
                s.paymentType(payMethod);
            } else {
                s.paymentType(paymentType);
            }
        };

        s.dobPicker = {};

        s.idType = ko.observable('NricFin');
        s.idNo = ko.observable();
        s.idDisplay = ko.computed(function() {
            return (s.idType() === 'NricFin' ? '(NRIC/FIN) ' : '(Passport) ') + s.idNo();
        });

        s.email = ko.observable('');

        s.name = ko.observable();
        s.mobile = ko.observable();
        s.nationality = ko.observable('');
        s.referSource = ko.observable('');
        s.dob = ko.observable('');
        s.subscribed = ko.observable(true);
        s.tnc = ko.observable(false);
        s.paymentType = ko.observable('');
        s.paymentTypeLabel = ko.observable('');
        s.payMethod = '';
        s.message = ko.observable('');

        s.selectedMerchant = ko.observable('');
        s.merchantId = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();

        s.btnGuard = false;

        s.packageDeets = function() {
            return {
                email: s.email(),
                idType: s.idType(),
                idNo: s.idNo(),
                name: s.name(),
                mobile: s.mobile(),
                paymentType: s.paymentType(),
                subscribed: s.subscribed(),
                nationality: s.nationality(),
                referSource: s.referSource(),
                dob: s.dob(),
                selectedMerchant : s.selectedMerchant(),
                merchantId : s.merchantId()
            };
        };

        s.unpackDeets = function(d) {
            s.email(d.email);
            s.idType(d.idType);
            s.idNo(d.idNo);
            s.name(d.name);
            s.mobile(d.mobile);
            s.paymentType(d.paymentType);
            s.subscribed(d.subscribed);
            s.nationality(d.nationality);
            s.referSource(d.referSource);
            s.dob(d.dob);
            s.selectedMerchant(d.selectedMerchant);
            s.merchantId(d.merchantId);

            s.isErr(d.isErr);
            s.errMsg(d.errMsg);
        };
    };


})(window.nvx = window.nvx || {}, jQuery);
