function triggerDownload() {
    window.open('/.store/store/generate-ticket/b2c-slm');
}

(function(nvx, $) {

    $(document).ready(function() {

        nvx.checkout = new nvx.CheckoutViewModel();
        nvx.deets = new nvx.CheckoutDeets('deetsForm');
        $.ajax({
            url: '/.store/store/get-finalised-receipt',
            data: {}, headers: {'Store-Api-Channel': 'b2c-slm', 'Store-Api-Locale': jsLang},
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    if (data.data == null) {
                        //Pending status
                        $('#tasMsg').hide();
                        $('#tasPending').show();
                        $('#transactionAbnormalSection').show();
                        $('#transactionOkSection').hide();
                        $('#receiptWrapper').hide();

                        setTimeout(function() {
                            window.location.reload();
                        }, 3000);
                    } else {
                        //Successful status
                        nvx.checkout.updateCheckoutModel(data.data);
                        ko.applyBindings(nvx.checkout, document.getElementById('theCheckout'));
                        ko.applyBindings(nvx.checkout, document.getElementById('theRemark'));

                        if (data.data.customer) {
                            nvx.deets.initDeets(data.data.customer, data.data.payMethod);
                            nvx.deets.dateOfPurchase(data.data.dateOfPurchase);
                            nvx.deets.receiptNumber(data.data.receiptNumber);
                            nvx.deets.pinCode(data.data.pinCode == null ? '' : data.data.pinCode);
                            nvx.deets.hasPinCode(nvx.deets.pinCode() != '');
                        }
                        if (data.data.payMethod) {
                            nvx.deets.initPayMethod(data.data.payMethod);
                            nvx.deets.paymentTypeLabel(data.data.paymentTypeLabel);
                            nvx.deets.message(data.data.message);
                        }

                        ko.applyBindings(nvx.deets, document.getElementById('pi4'));
                        ko.applyBindings(nvx.deets, document.getElementById('pi0'));
                        ko.applyBindings(nvx.deets, document.getElementById('pi1'));
                        ko.applyBindings(nvx.deets, document.getElementById('pi2'));
                        ko.applyBindings(nvx.deets, document.getElementById('pi3'));

                        $('#pi4').show();

                        $('#transactionAbnormalSection').hide();
                        $('#transactionOkSection').show();
                        $('#receiptWrapper').show();
                    }
                } else {
                    $('#tasPending').hide();
                    $('#tasMsg').text(data.message).show();
                    $('#transactionAbnormalSection').show();
                    $('#transactionOkSection').hide();
                    $('#receiptWrapper').hide();
                }
            },
            error: function () {
                alert('System encountered an error. Please refresh your page.');
            },
            complete: function () {
            }
        });
    });

    nvx.btnGuard = false;

    nvx.checkout = {};

    nvx.CheckoutViewModel = function() {
        var s = this;

        s.hasBookingFee = ko.observable(true);
        s.bookFeeMode = ko.observable('');
        s.bookFeeQty = ko.observable(0);
        s.bookPriceText = ko.observable('');
        s.bookFeeSubTotalText = ko.observable('');
        s.waivedTotalText = ko.observable('');

        s.hasTnc = ko.observable(false);
        s.tncs = ko.observableArray([]);
    };
    nvx.CheckoutViewModel.prototype = new nvx.CartViewModel();
    nvx.CheckoutViewModel.prototype.updateCheckoutModel = function(baseView) {
        this.updateModel(baseView);

        this.hasBookingFee(baseView.hasBookingFee);
        this.bookFeeMode(baseView.bookFeeMode);
        this.bookFeeQty(baseView.bookFeeQty);
        this.bookPriceText(baseView.bookPriceText);
        this.bookFeeSubTotalText(baseView.bookFeeSubTotalText);
        this.waivedTotalText(baseView.waivedTotalText);

        this.hasTnc(baseView.hasTnc);
        this.tncs([]);
        if (baseView.tncs) {
            for (var i = 0; i < baseView.tncs.length; i++) {
                var tnc = baseView.tncs[i];
                this.tncs.push(new nvx.TncViewModel(tnc));
            }
        }
    };

    nvx.deets = {};

    nvx.CheckoutDeets = function(formId) {
        var s = this;

        s.formId = formId;

        s.initDeets = function(base, payMethod) {
            s.idType(base.idType);
            s.idNo(base.idNo);
            s.email(base.email);
            s.mobile(base.mobile);
            s.nationality(base.nationality);
            s.referSource(base.referSource);
            s.dob(base.dob);
            s.initPayMethod(payMethod, base.paymentType);
            s.subscribed(base.subscribed);
            s.name(base.name);
        };

        s.initPayMethod = function(payMethod, paymentType) {
            if (payMethod) {
                s.payMethod = payMethod;
                s.paymentType(payMethod);
            } else {
                s.paymentType(paymentType);
            }
        };

        s.dateOfPurchase = ko.observable('');
        s.receiptNumber = ko.observable('');
        s.hasPinCode = ko.observable(false);
        s.pinCode = ko.observable('');

        s.dobPicker = {};

        s.idType = ko.observable('NricFin');
        s.idNo = ko.observable();
        s.idDisplay = ko.computed(function() {
            return (s.idType() === 'NricFin' ? '(NRIC/FIN) ' : '(Passport) ') + s.idNo();
        });

        s.email = ko.observable('');

        s.name = ko.observable();
        s.mobile = ko.observable();
        s.nationality = ko.observable('');
        s.referSource = ko.observable('');
        s.dob = ko.observable('');
        s.subscribed = ko.observable(true);
        s.tnc = ko.observable(false);
        s.paymentType = ko.observable('');
        s.paymentTypeLabel = ko.observable('');
        s.payMethod = '';
        s.message = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();

        s.btnGuard = false;

        s.packageDeets = function() {
            return {
                email: s.email(),
                idType: s.idType(),
                idNo: s.idNo(),
                name: s.name(),
                mobile: s.mobile(),
                paymentType: s.paymentType(),
                subscribed: s.subscribed(),
                nationality: s.nationality(),
                referSource: s.referSource(),
                dob: s.dob()
            };
        };

        s.unpackDeets = function(d) {
            s.email(d.email);
            s.idType(d.idType);
            s.idNo(d.idNo);
            s.name(d.name);
            s.mobile(d.mobile);
            s.paymentType(d.paymentType);
            s.subscribed(d.subscribed);
            s.nationality(d.nationality);
            s.referSource(d.referSource);
            s.dob(d.dob);

            s.isErr(d.isErr);
            s.errMsg(d.errMsg);
        };
    };

})(window.nvx = window.nvx || {}, jQuery);