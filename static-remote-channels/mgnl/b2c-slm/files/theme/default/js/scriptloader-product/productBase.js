var productModel = {
    "productId": 100,
    "productUrl": "day-fun-pass-play-20",
    "type": "GeneralTicket",
    "topupLimitType": "LimitToMainTicket",
    "isGeneral": true,
    "isRecurring": false,
    "isActive": true,
    "displayDate": false,
    "isEmpty": false,
    "name": "DAY FUN PASS Play 20",
    "desc": "\u003cp\u003eSentosa FUN PASS \u0026ndash; your ticket to The State of Fun! Be spoilt for choice with over 20 exciting attractions, at up to 70% savings! Choose from various passes for your choice of fun.\u003c/p\u003e\n\u003cp\u003e\u0026nbsp;\u003c/p\u003e\n\u003cp\u003eChoose \u003cstrong\u003eDAY FUN PASS Play 20\u003c/strong\u003e to play up to 20 attractions! Save up to 70%.\u003c/p\u003e\n\u003cp\u003eValid for 1 day from 9am^\u003c/p\u003e\n\u003cp\u003e\u0026nbsp;\u003c/p\u003e\n\u003cp\u003eDAY FUN\u0026nbsp;Pass attractions include:\u003c/p\u003e\n\u003cul\u003e\n\u003cli\u003e4D AdventureLand: Desperados in 4D\u003c/li\u003e\n\u003cli\u003e4D AdventureLand: Extreme Log Ride\u003c/li\u003e\n\u003cli\u003e4D AdventureLand: Journey 2: The Mysterious Island\u003c/li\u003e\n\u003cli\u003eBi-Pedal Bicycle (1 Hour)\u003c/li\u003e\n\u003cli\u003eButterfly Park \u0026amp; Insect Kingdom\u003c/li\u003e\n\u003cli\u003eFort Siloso Combat Skirmish: Indoor Maze\u003c/li\u003e\n\u003cli\u003eMegaBounce\u003c/li\u003e\n\u003cli\u003eParajump\u003c/li\u003e\n\u003cli\u003eS.E.A. Aquarium\u003c/li\u003e\n\u003cli\u003eSeaBreeze Water-Sports @ Wave House Sentosa\u003c/li\u003e\n\u003cli\u003eSegway Fun Ride\u003c/li\u003e\n\u003cli\u003eSentosa Merlion\u003c/li\u003e\n\u003cli\u003eSkyline Luge Sentosa (1 Luge \u0026amp; 1 Skyride)\u003c/li\u003e\n\u003cli\u003eTrick Eye Museum Singapore\u003c/li\u003e\n\u003cli\u003eThe Flying Trapeze\u003c/li\u003e\n\u003cli\u003eTiger Sky Tower\u003c/li\u003e\n\u003cli\u003eWave House Sentosa: Catch-A-Wave (1 Try)\u003c/li\u003e\n\u003cli\u003eWings of Time (Standard Seat)\u003c/li\u003e\n\u003cli\u003eImages of Singapore LIVE*\u003c/li\u003e\n\u003cli\u003eMadame Tussauds Singapore*\u003c/li\u003e\n\u003c/ul\u003e\n\u003cp\u003e\u0026nbsp;\u003c/p\u003e\n\u003cp\u003e^Pass valid during the attractions\u0026rsquo; usual operating hours or until closure of attractions, whichever is earlier.\u003c/p\u003e\n\u003cp\u003e*Images of Singapore Live \u0026amp; Madame Tussauds Singapore must be redeemed together and cannot be visited separately. This will be counted as TWO redemption of attractions under DAY FUN PASS.\u003c/p\u003e",
    "url": "",
    "showPromo": true,
    "productWidget": "",
    "images": [{"image": "Sentosa-Online-Banner_581x298-3.jpg", "order": 1}],
    "hasNotes": false,
    "notes": "\u003cul\u003e\n\u003cli\u003e\u003cstrong\u003eFood Republic Dollar Deals: Enjoy $1\u0026nbsp;Dessert\u0026nbsp;with any purchase at Food Republic VivoCity (Level 3) \u003c/strong\u003e\u003c/li\u003e\n\u003cli\u003ePresent original same-day receipt (no min. spending) and a valid Sentosa Fun Pass prior to purchase. Valid only on promotional-sized dessert. Valid until 31 May 2016. Only one redemption per Fun Pass is allowed. Item subject to availability. Image is for illustration purpose only. Food Republic reserves the right to amend the T\u0026amp;Cs without prior notice.\u003c/li\u003e\n\u003c/ul\u003e\n\u003cp\u003e\u0026nbsp;\u003c/p\u003e\n\u003cul\u003e\n\u003cli\u003e\n\u003cp\u003e\u003cstrong\u003ePlease note that all FUN Passes purchased before 31 March 2016 must be redeemed and used by 31 March 2016.\u0026nbsp;\u003c/strong\u003e\u003c/p\u003e\n\u003c/li\u003e\n\u003c/ul\u003e\n\u003cp\u003e\u0026nbsp;\u003c/p\u003e\n\u003cul\u003e\n\u003cli\u003eWings of Time tickets must be redeemed at Sentosa Ticketing Counters prior to show.\u003c/li\u003e\n\u003cli\u003eImages of Singapore LIVE \u0026amp; Madame Tussauds Singapore must be redeemed together and cannot be visited separately. This will be counted as TWO redemption of attractions under DAY FUN PASS.\u003c/li\u003e\n\u003cli\u003e\u003cstrong\u003ePlease refer to the Terms and Conditions below for the respective attractions\u0027 height, weight, age and other restrictions.\u003c/strong\u003e\u003c/li\u003e\n\u003cli\u003eDAY FUN PASS is valid for 1 day and must be utilised within the day the first attractino is redeemed.\u003c/li\u003e\n\u003cli\u003ePass is valid during the attractions\u0027 usual operating hours or until closure of attractions, whichever, is earlier.\u003c/li\u003e\n\u003cli\u003eEach attraction is limited to a one-time entry unless otherwise stated.\u003c/li\u003e\n\u003cli\u003eOne top-up per DAY FUN PASS.\u003c/li\u003e\n\u003cli\u003eChild price is applicable to children aged 3 - 12 years inclusive.\u003c/li\u003e\n\u003cli\u003eUsual prices indicated are based on approximate rate.\u003c/li\u003e\n\u003cli\u003eCredit Card used for purchase will be required upon redemption for verification purposes.\u003c/li\u003e\n\u003cli\u003eOnline confirmation receipt must be redeemed within 3 months from date of purchase.\u003c/li\u003e\n\u003cli\u003eSENTOSA FUN PASS has a 14 days validity period upon exchange of confirmation receipt.\u003c/li\u003e\n\u003c/ul\u003e",
    "recommendedItems": [],
    "standardRows": [],
    "topups": []
};

nvx.MSG_RCMD_DESC = 'Click here for more details!';
nvx.MSG_ONE_ITEM = 'Please select at least one item.';
nvx.MSG_ONE_TOPUP = 'Please select at least one top-up to add.';
nvx.MSG_HAVE_PROMO = 'Have a promo code?';
nvx.MSG_TOPUP_ADDED = 'Top-up has been added to your cart.';
nvx.MSG_SELECT_DATE = 'Please choose a date of visit.';

function logRecommendedClick(name, rcmdProdId, href, mainProdId) {
    try {
        $.ajax({
            type: 'POST', async: false, cache: false,
            url: (nvx.lanns?nvx.lanns:'/')+'b2c/log-rcmd',
            data: { mainProdId: mainProdId, rcmdProdId: rcmdProdId }, dataType: "json",
            complete: function (result) {
                try {
                    ga('send', 'event', 'Recommended Item', 'Click', name + ' : ' + rcmdProdId, {
                        'hitCallback': function() {
                            window.location.href = href;
                        }
                    });
                } catch (e) { console.log(e); window.location.href = href; }
            }
        });
    } catch (e) { console.log(e); return true; }
    return false;
}

(function(baseProd, nvx, $, ko) {

    nvx.imageSlider = {};

    $(document).ready(function() {

        $('div.details,div.notes').find('a').attr('target','_blank');

        nvx.booking = new nvx.BookingFormModel();
        if (cmsBookingModel != null) {
            nvx.booking.init(cmsBookingModel);
        } else {
            nvx.booking.init(baseProd);
        }

        var theFormElement = document.getElementById('bookingForm');
        ko.cleanNode(theFormElement);
        ko.applyBindings(nvx.booking, theFormElement);

        nvx.booking.doInitialState();

        ko.applyBindings(nvx.cart, document.getElementById('bookingSummary'));

        var onlyOneImage = $('.slide').length == 1;
        if (onlyOneImage) {
            $('.slider-arrow').hide();
        }

        if ($('.slide-container').length > 0) {
            nvx.imageSlider = $('.slide-container').sequence({
                autoPlay: true,
                autoPlayDelay: 5000,
                nextButton: !onlyOneImage,
                prevButton: !onlyOneImage
            }).data('sequence');
        }

        doResizeLogic();
    });

    var xh = null;

    function doResizeLogic() {
        $('.expand-small').click(function() {
            xh = $('.ticket-description .constrain-small')[0].scrollHeight + 20;
            $('.ticket-description').addClass('xpnd').height(xh);
            $('.expand-small').hide();
        });

        function resizeSlides() {
            var width = $('.ticket-details .rightbar').width();
            var ratio = width / 581;
            var height = 298 * ratio;

            if ($(window).width() < 480) {
                if ($('.ticket-description .constrain-small')[0].scrollHeight < height - 33) {
                    $('.expand-small').hide();
                } else {
                    $('.ticket-description').removeClass('xpnd');
                    $('.expand-small').show();
                }
            } else {
                xh = null;
                $('.ticket-description').removeClass('xpnd');
                $('.expand-small').hide();
            }

            $('.ticket-details .rightbar .slide-container').height(height);
            $('.ticket-details .leftbar .ticket-description').height(xh ? xh : height - 19.5);
            $('.ticket-details .rightbar .sequence-next, .ticket-details .rightbar .sequence-prev').css('top', ((height / 2) - 25) + 'px');
        }
        resizeSlides();
        $(window).resize(function() {
            resizeSlides();
        });
    }

    /* Models */

    nvx.FormRowModel = function() {
        var s = this;

        s.name = '';

        s.shouldTruncate = false;
        s.truncatedDesc = '';
        s.desc = '';

        s.items = ko.observableArray([]);
    };

    nvx.ItemModel = function() {
        var s = this;

        s.productId = 0;

        s.name = '';
        s.url = '';
        s.id = 0;
        s.parentId = 0;
        s.itemCode = 0;
        s.image = '';
        s.desc = '';

        s.buyAsSet = false;
        s.type = '';
        s.typeDisplay = '';
        s.price = '';
        s.unitPrice = 0;

        s.discountId = 0;
        s.discountCode = '';

        s.promotionText = '';
        s.promotionIconImageURL = '';
        s.cachedDiscountPrice = '';

        s.hasCapacity = ko.observable(true);
        s.qty = ko.observable(0);
    };

    nvx.TopupGroupModel = function(parent) {
        var s = this;

        s.parent = parent;
        s.topupItems = ko.observableArray([]);
    };
    nvx.TopupGroupModel.prototype = new nvx.ItemModel();

    nvx.TopupModel = function(parent) {
        var s = this;

        s.parent = parent;

        s.showTopupMsg = ko.observable(false);
        s.topupMsgErr = ko.observable(false);
        s.topupMsg = ko.observable('');
        s.processingTopup = false;
        s.qty = ko.observable(0);

        s.addTopup = function(model, evt) {
            if (s.processingTopup) {
                return;
            }
            s.processingTopup = true;

            $.each(s.parent.topups(), function(idx, t) {
                $.each(t.topupItems(), function(idx, u) {
                    u.showTopupMsg(false);
                });
            });

            function doAddTopup(opts, callback) {
                if (Number(s.qty()) == 0) {
                    s.topupMsgErr(true);
                    s.showTopupMsg(true);
                    s.topupMsg(nvx.MSG_ONE_TOPUP);
                    s.processingTopup = false;
                    callback();
                    return;
                }

                /*

                 topupItem.name = obj.name;
                 topupItem.desc = obj.desc;
                 topupItem.image = obj.image;
                 topupItem.price = obj.price;
                 topupItem.unitPrice = obj.unitPrice;
                 topupItem.type = obj.type;
                 topupItem.typeDisplay = obj.typeDisplay;
                 topupItem.productCode = obj.productCode;
                 topupItem.listingId = obj.listingId;
                 topupItem.csParentListingId = obj.csParentListingId;
                 topupItem.csParentCmsProductId = obj.csParentCmsProductId;
                 */

                $.ajax({
                    url: '/.store/store/add-topup-to-cart',
                    data: JSON.stringify({
                        sessionId: '',
                        items: [{
                            cmsProductName: '',
                            cmsProductId: s.csParentCmsProductId,
                            parentCmsProductId: s.csParentCmsProductId,
                            parentListingId: s.csParentListingId,
                            listingId: s.listingId,
                            productCode: s.productCode,
                            name: s.name,
                            type: s.type,
                            qty: s.qty(),
                            price: Number(s.price),
                            isTopup: true
                        }]
                    }), headers: { 'Store-Api-Channel' : 'b2c-slm' },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function(data) {
                        if (data.success) {
                            if (data.data) {
                                nvx.cart.updateModel(data.data);
                            }
                            s.showTopupMsg(true);
                            s.topupMsgErr(false);
                            s.qty(0);
                            s.topupMsg(nvx.MSG_TOPUP_ADDED);
                        } else {
                            s.showTopupMsg(true);
                            s.topupMsgErr(true);
                            s.topupMsg(data.message);
                        }
                    },
                    error: function() {
                        s.showTopupMsg(true);
                        s.topupMsgErr(true);
                        s.topupMsg(msg.MSG_GEN_ERROR);
                    },
                    complete: function() {
                        callback();
                        s.processingTopup = false;
                    }
                });
            }

            nvx.doButtonLoading(evt.target, doAddTopup, {});
        };
    };
    nvx.TopupModel.prototype = new nvx.ItemModel();

    /* Booking */

    nvx.booking = null;

})(productModel, window.nvx = window.nvx || {}, jQuery, ko);
