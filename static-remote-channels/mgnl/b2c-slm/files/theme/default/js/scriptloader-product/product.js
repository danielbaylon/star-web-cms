(function(baseProd, nvx, $, ko) {

    nvx.BookingFormModel = function() {
        var s = this;

        /*
         * Date-related
         */

        s.hasDate = ko.observable(false);
        s.theDate = ko.observable('');
        s.datePicker = null;

        function initDate(base) {
            s.hasDate(base.displayDate);
            if (s.hasDate() && s.datePicker == null) {
                s.datePicker = $('#theDate').pickadate({
                    format: 'dd/mm/yyyy',
                    min: 1, selectYears: true, selectMonths: true, max: 90
                }).data('pickadate');
            }
        }

        s.toggleTheDate = function(hell, evt) {
            if (s.datePicker.get('open')) {
                s.datePicker.close();
            } else {
                s.datePicker.open();
            }

            evt.stopPropagation();
        };

        /**
         * View More/View Less
         */
        s.toggleFullDesc = function(theName, showFull) {
            var $trunc = $('span[data-trunc-trunc="' + theName +'"]');
            var $full = $('span[data-trunc-full="' + theName +'"]');
            if (showFull) {
                $trunc.hide();
                $full.fadeIn();
            } else {
                $full.hide();
                $trunc.fadeIn();
            }
        };

        s.hidePromoRows = function (productListingId) {
            $('#downArrow' + productListingId).show();
            $('#upArrow' + productListingId).hide();
            $('.promoLabel' + productListingId).each(function () {
                $(this).hide();
            });
        };

        s.showPromoRows = function (productListingId) {
            $('#downArrow' + productListingId).hide();
            $('#upArrow' + productListingId).show();
            $('.promoLabel' + productListingId).each(function () {
                $(this).show();
            });
        };

        /*
         * Promotion-related
         */

        s.promoCode = ko.observable('');
        s.currentPromoCode = '';
        s.processingPromo = false;
        s.codeApplyError = ko.observable(false);
        s.codeApplyMsg = ko.observable('');

        s.applyCode = function(model, evt) {
            if (s.processingPromo) {
                return;
            }
            s.codeApplyError(false);
            s.processingPromo = true;

            function doApply(opts, callback) {
                $.ajax({
                    url: '/.store/store/check-promo-code',
                    data: JSON.stringify({
                        productId: s.baseProd.productId,
                        promoCode: s.promoCode(),
                        isEvent: s.isGeneral ? false : true,
                        schedId: s.isGeneral ? -1 : s.currentSchedId(),
                        dateOfVisit: null
                    }),
                    headers: {'Store-Api-Channel': 'b2c-slm'},
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.initializePromos(data.data);
                        } else {
                            s.codeApplyError(true);
                            s.codeApplyMsg(data.message);
                        }
                    },
                    error: function () {
                        s.codeApplyError(true);
                        s.codeApplyMsg(msg.MSG_GEN_ERROR);
                    },
                    complete: function () {
                        callback();
                        s.processingPromo = false;
                    }
                });
            }

            if (evt) {
                nvx.doButtonLoading(evt.target, doApply, {});
            } else {
                nvx.doButtonLoading(null, doApply, {});
            }
        };

        s.showPromos = ko.observable(false);
        s.promoRows = ko.observableArray([]);

        s.initializePromos = function (promoResult) {
            s.promoRows([]); //clear up the prev rows
            s.currentPromoCode = s.promoCode();

            $.each(promoResult.unlockedTickets, function (idx, tix) {
                //console.log(tix);

                var promo = new nvx.FormRowModel();
                promo.name = tix.productName;
                promo.shouldTruncate = false;
                promo.desc = tix.productDesc;
                promo.truncatedDesc = "";

                var pi = new nvx.ItemModel();
                pi.id = tix.productListingId;
                pi.parentId = s.baseProd.productId;
                pi.itemCode = tix.displayProductNumber;
                pi.type = tix.productType;
                pi.typeDisplay = tix.ticketTypeText;
                pi.price = tix.productPriceText;
                pi.unitPrice = tix.productPrice;
                pi.buyAsSet = false;
                pi.discountId = 0;
                pi.discountCode = promoResult.promoCode;
                pi.promotionText = tix.promotionText;
                pi.promotionIconImageURL = tix.promotionIconImageURL;
                pi.cachedDiscountPrice = tix.cachedDiscountPrice;
                promo.items.push(pi);

                s.promoRows.push(promo);
            });

            s.expandStandard(true);
            s.toggleExpandStandard();
            s.expandPromos(false);
            s.toggleExpandPromos();
            s.showPromos(true);
        };

        s.processingCart = false;

        /*
         * STAR Event
         */

        s.prodExtData = [];

        function initEventControls() {
            $.each(s.prodExtData, function(idx, prod) {
                var isSelectDate = prod.event && (!prod.openDate || prod.singleEvent);
                if (isSelectDate) {
                    var productId = prod.productId;

                    var $ebDate = $('#ebDate' + productId);
                    $ebDate.pickadate({
                        format: 'dd/mm/yyyy'
                    });

                    var $sessionSelect = $('#ebSession' + productId);
                    $.each(prod.eventLines, function(idx, mdl) {
                        $sessionSelect.append($("<option />").val(mdl.eventLineId).text(mdl.eventName));
                    });
                    $sessionSelect.change(function() {
                        var selectedSession = $(this).val();
                        $.each(prod.eventLines, function(idx, mdl) {
                            if (mdl.eventLineId == selectedSession) {
                                var eventDates = mdl.eventDates;
                                var enableDates = [];
                                enableDates.push(true);

                                var singleEventDateYear;
                                var singleEventDateMonth;
                                var singleEventDateDay;

                                $.each(eventDates, function(idx, eventDate) {
                                    var dtArr = eventDate.date.split('/');
                                    enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);

                                    if(prod.singleEvent) {
                                        singleEventDateYear = Number(dtArr[2]);
                                        singleEventDateMonth = Number(dtArr[1]) - 1;
                                        singleEventDateDay = Number(dtArr[0]);
                                    }

                                });
                                var $ebDate = $('#ebDate' + productId);
                                $ebDate.pickadate().pickadate('picker').stop();
                                setTimeout(function() {
                                    $ebDate.pickadate({
                                        onStart: function() {
                                            if(prod.singleEvent) {
                                                this.set('select', [singleEventDateYear , singleEventDateMonth, singleEventDateDay]);
                                            }
                                        },
                                        format: 'dd/mm/yyyy', disable: enableDates
                                    });
                                }, 600);
                            }
                        });
                    });
                    $sessionSelect.change();

                    $('#qty' + productId).hide();

                    var $eventBar = $('#eventBar' + productId);
                    var $ebCancelBtn = $('#ebCancel' + productId);
                    var $ebSelectBtn = $('#ebSelect' + productId).show();
                    prod['eventSelected'] = false;

                    $ebSelectBtn.click(function() {
                        $ebSelectBtn.hide();
                        $ebCancelBtn.show();
                        $eventBar.show();
                        prod['eventSelected'] = true;
                    });
                    $ebCancelBtn.click(function() {
                        $ebCancelBtn.hide();
                        $ebSelectBtn.show();
                        $eventBar.hide();
                        prod['eventSelected'] = false;
                    });
                }
            });


            if (loadUrlPromoCode) {
                s.promoCode(urlPromoCode);
                s.applyCode();
            }
        }

        var extRetrievalFailCount = 0;
        function initProdExtData(sth, callback) {
            if (extRetrievalFailCount > 3) {
                console.log('Error retrieving extended data final.');
                s.processingCart = false;
                callback();

                if (loadUrlPromoCode) {
                    s.promoCode(urlPromoCode);
                    s.applyCode();
                }

                return;
            }

            s.processingCart = true;

            var prods = [];
            var $items = $('select[data-std-id]');
            $.each($items, function(idx, elem) {
                var $elem = $(elem);

                var anItem = {
                    productId: $elem.attr('data-std-id'),
                    itemId: $elem.attr('data-std-code')
                };
                prods.push(anItem);
            });

            $.ajax({
                url: '/.store/store/products-ext',
                data: JSON.stringify({
                    products: prods
                }), headers: { 'Store-Api-Channel' : 'b2c-slm' },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    // console.log(data);
                    if (data.success) {
                        s.processingCart = false;
                        $.each(data.data.products, function(idx, product) {
                            s.prodExtData.push(product);
                        });
                        initEventControls();
                        callback();
                    } else {
                        console.log('Error retrieving extended data. Retrying. ' + data);
                        extRetrievalFailCount++;
                        initProdExtData(sth, callback);
                    }
                },
                error: function () {
                    console.log('Error retrieving extended data. Retrying. [General]');
                    extRetrievalFailCount++;
                    initProdExtData(sth, callback);
                },
                complete: function () {
                }
            });
        }
        $(document).ready(function() {
            nvx.doButtonLoading($('#btnAddToCartMain')[0], initProdExtData, {});
        });

        /*
         * Cart-related
         */

        s.showBookMsg = ko.observable(false);
        s.bookMsgErr = ko.observable(false);
        s.bookMsg = ko.observable('');

        s.hideChildTopup = ko.observable(false);
        s.hideAdultTopup = ko.observable(false);
        s.hideSetAdultTopup = ko.observable(false);
        s.hideSetChildTopup = ko.observable(false);

        s.fromDirectAdd = ko.observable(false);

        s.addToCart = function(model, evt) {
            if (s.processingCart) {
                return;
            }
            s.processingCart = true;

            s.showBookMsg(false);

            function doAddToCart(opts, callback) {
                var hasQty = false;
                var mainItems = [];
                var promoItems = [];

                var hasAdult = false;
                var hasChild = false;
                var hasSetAdult = false;
                var hasSetChild = false;

                var $prodInfo = $('[data-prd-uuid]');
                var productId = $prodInfo.attr('id');
                var productUuid = $prodInfo.attr('data-prd-uuid');
                var productName = $prodInfo.attr('data-prd-name');

                var pxdMap = {};
                $.each(s.prodExtData, function(idx, mdl) {
                    pxdMap[mdl.productId] = mdl;
                });

                var $items = $('select[data-std-id]');
                $.each($items, function(idx, elem) {
                    var $elem = $(elem);
                    var listingId = $elem.attr('data-std-id');
                    var extData = pxdMap[listingId];
                    var isSelectDate = extData==null?false:(extData.event && !extData.openDate);

                    var qty = isSelectDate ? $('#ebQty' + listingId).val() : Number($elem.val());

                    if (qty > 0) {
                        hasQty = true;

                        var anItem = {
                            cmsProductName: productName,
                            cmsProductId: productId,
                            listingId: listingId,
                            productCode: $elem.attr('data-std-code'),
                            name: $elem.attr('data-std-name'),
                            type: $elem.attr('data-std-type'),
                            qty: qty,
                            price: Number($elem.attr('data-std-price')),
                            discountId: 0,
                            discountCode: ''
                        };
                        if (isSelectDate) {
                            anItem.eventGroupId = extData.eventGroupId;
                            anItem.eventLineId = $('#ebSession' + listingId).val();
                            $.each(extData.eventLines, function(idx, line) {
                                if (anItem.eventLineId == line.eventLineId) {
                                    anItem.eventSessionName = line.eventName;
                                    return false;
                                }
                            });
                            anItem.selectedEventDate = $('#ebDate' + listingId).val();
                        }
                        mainItems.push(anItem);

                    }
                });

                $.each(s.promoRows(), function (idx, promoRow) {
                    $.each(promoRow.items(), function (idx, promo) {
                        var qty = Number(promo.qty());
                        var anItem = {
                            cmsProductName: productName,
                            cmsProductId: productId,
                            listingId: promo.id.toString(),
                            productCode: promo.itemCode.toString(),
                            name: promoRow.name,
                            type: '',
                            qty: qty,
                            price: promo.unitPrice,
                            discountId: promo.discountId,
                            discountCode: promo.discountCode
                        };

                        if (qty > 0) {
                            hasQty = true;
                            if (promo.type == 'Adult') {
                                hasAdult = true;
                                hasSetAdult = promo.buyAsSet;
                            } else if (promo.type == 'Child') {
                                hasChild = true;
                                hasSetChild = promo.buyAsSet;
                            }
                            mainItems.push(anItem);
                        }
                    });
                });

                if (!hasQty) {
                    s.showBookMsg(true);
                    s.bookMsgErr(true);
                    s.bookMsg(nvx.MSG_ONE_ITEM);
                    s.processingCart = false;
                    callback();
                    return;
                }

                s.hideAdultTopup(!hasAdult);
                s.hideChildTopup(!hasChild);
                s.hideSetAdultTopup(!hasSetAdult);
                s.hideSetChildTopup(!hasSetChild);

                $.ajax({
                    url: '/.store/store/add-to-cart',
                    data: JSON.stringify({
                        sessionId: '',
                        items: mainItems
                    }), headers: { 'Store-Api-Channel' : 'b2c-slm' },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            if (data.data) {
                                nvx.cart.updateModel(data.data);
                            }
                            s.showStatusSection(true);
                            s.extraMsg(data.extraMsg ? data.extraMsg : '');
                            s.fromDirectAdd(true);
                            if (s.isGeneral) {
                                s.app.setLocation('#!/topups');
                            } else {
                                s.app.setLocation('#!/topups/' + s.currentSchedId());
                            }
                        } else {
                            s.showBookMsg(true);
                            s.bookMsgErr(true);
                            s.bookMsg(data.message);
                        }
                    },
                    error: function () {
                        s.showBookMsg(true);
                        s.bookMsgErr(true);
                        s.bookMsg(msg.MSG_GEN_ERROR);
                    },
                    complete: function () {
                        callback();
                        s.processingCart = false;
                    }
                });

                // TODO: Modify to new API
                //s.showStatusSection(true);
                ////s.extraMsg(data.extraMsg ? data.extraMsg : '');
                //s.fromDirectAdd(true);
                //if (s.isGeneral) {
                //    s.app.setLocation('#!/topups');
                //} else {
                //    //s.app.setLocation('#!/topups/' + s.currentSchedId());
                //}
            }

            nvx.doButtonLoading(evt.target, doAddToCart, {});
        };

        /*
         * Recommended-related
         */

        s.recommendeds = ko.observableArray([]);

        s.getRecomBgUrlString = function(base, image) {
            return 'url("' + base + image + '") center center no-repeat';
        };

        function initRecommendeds(base) {
            if (base.recommendedItems) {
                $.each(base.recommendedItems, function(idx, obj) {
                    var rcmd = new nvx.ItemModel();
                    rcmd.name = obj.name;
                    rcmd.url = obj.url;
                    rcmd.id = obj.id;
                    rcmd.image = obj.image;
                    rcmd.desc = obj.desc;
                    if (rcmd.desc == null || rcmd.desc == '') {
                        rcmd.desc = nvx.MSG_RCMD_DESC;
                    }
                    s.recommendeds.push(rcmd);
                });
            }
        }

        /*
         * Topups-related
         */

        s.topups = ko.observableArray([]);

        function initTopups(topups) {
            s.topups([]);
            if (topups) {
                if (!s.fromDirectAdd()) {
                    s.hideAdultTopup(true);
                    s.hideChildTopup(true);
                    s.hideSetAdultTopup(true);
                    s.hideSetChildTopup(true);
                    $.each(nvx.cart.items(), function(idx, categ) {
                        if (categ.baseModel.id != s.baseProd.productId) {
                            return;
                        }
                        $.each(categ.baseModel.products, function(idx, item) {
                            if (item.ticketType == 'Adult') {
                                s.hideAdultTopup(false);
                                if (item.multiplier > 1) {
                                    //TODO able to bypass using remove cart item in sidebar
                                    s.hideSetAdultTopup(false);
                                }
                            } else if (item.ticketType == 'Child') {
                                s.hideChildTopup(false);
                                if (item.multiplier > 1) {
                                    //TODO able to bypass using remove cart item in sidebar
                                    s.hideSetChildTopup(false);
                                }
                            }
                        });
                    });
                }

                var $prodInfo = $('[data-prd-uuid]');
                var productId = $prodInfo.attr('id');
                var tpDisplayMap = {};
                $.each(nvx.cart.items(), function(idx, categ) {
                    if (categ.baseModel.id != productId) {
                        return;
                    }
                    $.each(categ.baseModel.products, function(idx, item) {
                        tpDisplayMap[item.listingId] = true;
                    });
                });

                $.each(topups, function(idx, obj) {
                    if (s.hideAdultTopup() && obj.type == 'Adult') {
                        //return;
                    }
                    if (s.hideChildTopup() && obj.type == 'Child') {
                        //return;
                    }
                    if (s.hideSetAdultTopup() && obj.type == 'Adult' && obj.displayWhenSet) {
                        //return;
                    }
                    if (s.hideSetChildTopup() && obj.type == 'Child' && obj.displayWhenSet) {
                        //return;
                    }
                    if (tpDisplayMap[obj.csParentListingId] != true) {
                        return;
                    }

                    var topupItem = new nvx.TopupModel(s);
                    topupItem.url = obj.url;
                    topupItem.id = obj.id;
                    topupItem.productId = s.baseProd.productId;

                    topupItem.name = obj.name;
                    topupItem.desc = obj.desc;
                    topupItem.image = obj.image;
                    topupItem.price = obj.price;
                    topupItem.unitPrice = obj.unitPrice;
                    topupItem.type = obj.type;
                    topupItem.typeDisplay = obj.typeDisplay;
                    topupItem.productCode = obj.productCode;
                    topupItem.listingId = obj.listingId;
                    topupItem.csParentListingId = obj.csParentListingId;
                    topupItem.csParentCmsProductId = obj.csParentCmsProductId;

                    var existing = false;
                    $.each(s.topups(), function(idx, tp) {
                        if (tp.name == obj.name) {
                            tp.topupItems.push(topupItem);
                            existing = true;
                            return false;
                        }
                    });

                    if (!existing) {
                        var topup = new nvx.TopupGroupModel(s);
                        topup.name = obj.name;
                        topup.url = obj.url;
                        topup.desc = obj.desc;
                        topup.image = obj.image;
                        topup.productId = s.baseProd.productId;

                        topup.topupItems.push(topupItem);
                        s.topups.push(topup);
                    }
                });
            }
        }

        /*
         * Event Schedule-related
         */

        s.schedules = ko.observableArray([]);
        s.currentSchedId = ko.observable(-1);
        s.currentSchedId.subscribe(function(newValue) {
            s.promoRows([]);
            s.showPromos(false);
            s.promoCode('');
            s.expandStandard(true);
            s.expandPromos(false);
        });

        function initSchedules(schedules) {
            if (schedules) {
                $.each(schedules, function(idx, sched) {
                    s.schedules.push({
                        id: sched.id,
                        scheduleName: sched.scheduleName
                    });
                });
            }
        }

        /*
         * Promo label-related
         */

        s.promoCallToArms = ko.observable(nvx.MSG_HAVE_PROMO);
        s.promoLink = ko.observable('');
        s.promoLinkText = ko.observable('');

        function initPromoStatics(base) {
            if (base.promoCallToArms) {
                s.promoCallToArms(base.promoCallToArms);
            }
            if (base.promoLink) {
                s.promoLink(base.promoLink);
            }
            if (base.promoLinkText) {
                s.promoLinkText(base.promoLinkText);
            }
        }

        /*
         * Initialization-related
         */

        s.expandStandard = ko.observable(true);
        s.expandPromos = ko.observable(true);

        s.toggleExpandStandard = function(mdl, evt) {
            s.expandStandard(!s.expandStandard());
            var $elem;
            if (evt) {
                $elem = $(evt.target);
            } else {
                $elem = $('i[data-bind="click: toggleExpandStandard"]');
            }
            if (s.expandStandard()) {
                $elem.addClass('fw-icon-collapse-alt').removeClass('fw-icon-expand-alt');
            } else {
                $elem.addClass('fw-icon-expand-alt').removeClass('fw-icon-collapse-alt');
            }
        };
        s.toggleExpandPromos = function(mdl, evt) {
            s.expandPromos(!s.expandPromos());
            var $elem;
            if (evt) {
                $elem = $(evt.target);
            } else {
                $elem = $('i[data-bind="click: toggleExpandPromos"]');
            }
            if (s.expandPromos()) {
                $elem.addClass('fw-icon-collapse-alt').removeClass('fw-icon-expand-alt');
            } else {
                $elem.addClass('fw-icon-expand-alt').removeClass('fw-icon-collapse-alt');
            }
        };

        s.showMainForm = ko.observable(false);
        s.showRecommended = ko.observable(false);
        s.showTopups = ko.observable(false);
        s.showStatusSection = ko.observable(false);
        s.showEndActions = ko.observable(false);
        s.extraMsg = ko.observable('');
        s.showExtraMsg = ko.computed(function() {
            return s.extraMsg() != '';
        });

        s.app = {};

        s.baseProd = null;
        s.isGeneral = true;

        function initializeGeneral(base) {
            //initDate(base);
        }

        function initializeEvent(base) {
            initSchedules(base.schedules);
        }

        s.init = function(base) {
            s.baseProd = base;

            s.isGeneral = base.isGeneral;

            if (s.isGeneral) {
                initializeGeneral(base);
            } else {
                initializeEvent(base);
            }

            initRecommendeds(base);

            initPromoStatics(base);
        };

        /*
         * Routing Section
         */

        s.initAfterMainView = function(hasSched, schedId) {
            s.codeApplyError(false);
            s.showMainForm(false);
            $('#bookNowShortcut').hide();

            hasSched = false;
            if (hasSched) {
                s.currentSchedId(schedId);
                var tops = [];
                $.each(s.baseProd.schedules, function(idx, sched) {
                    if (sched.id == schedId) {
                        tops = sched.topups;
                        return false;
                    }
                });
                initTopups(tops);
            } else {
                initTopups(s.baseProd.topups);
            }

            s.showTopups(true);
            s.showRecommended(true);
            s.showEndActions(true);
        };

        s.initMainView = function() {
            s.showStatusSection(false);
            s.showTopups(false);
            s.showRecommended(false);
            s.showEndActions(false);

            $.each(s.topups(), function(idx, obj) {
                $.each(obj.topupItems(), function(idx2, u) {
                    u.showTopupMsg(false);
                });
            });

            initDate(s.baseProd);

            s.showMainForm(true);
            if ($(window).width() <= 480) { $('#bookNowShortcut').show(); }
        };

        s.doInitialState = function() {
            s.app = Sammy(function() {
                this.get('#!/topups', function() {
                    console.log('Initializing topups view.');
                    try {
                        ga('send', 'pageview', window.location.pathname + window.location.search + window.location.hash);
                    } catch (e) { console.log(e); }
                    if (s.baseProd.isActive) {
                        s.initAfterMainView();
                    }
                });
                this.get('#!/topups/:schedId', function() {
                    console.log('Initializing topups view (schedule-based).');
                    try {
                        ga('send', 'pageview', window.location.pathname + window.location.search + window.location.hash);
                    } catch (e) { console.log(e); }
                    if (s.baseProd.isActive) {
                        var schedId = this.params['schedId'];
                        s.initAfterMainView(true, schedId);
                    }
                });
                this.get('#!/', function() {
                    console.log('Initializing main view.');
                    s.initMainView();
                });
            });

            s.app.run('#!/');
        };

    };

})(productModel, window.nvx = window.nvx || {}, jQuery, ko);
