nvx.MSG_FIELD_REQUIRED = 'Please fill in the following required field(s):';
nvx.MSG_NAME = 'Name';
nvx.MSG_EMAIL = 'Email';
nvx.MSG_CONTACT_NO = 'Contact No.';
nvx.MSG_WHERE_HEAR = 'Where did you hear about us?';
nvx.MSG_PAYMENT_TYPE = 'Payment Type';
nvx.MSG_NATIONALITY = 'Nationality';
nvx.MSG_ENTER_PROMO = 'Please enter a promo code';
nvx.MSG_BOOKING_FEE_WAIVED = 'Booking fee has been waived.';
nvx.MSG_BOOKING_FEE_INVALID = 'Invalid. Please try again.';
nvx.MSG_BOOKING_FEE_INVALID = 'Invalid. Please try again.';
nvx.MSG_VALID_EMAIL = 'Please enter a valid email address.';
nvx.MSG_EMAIL_MAX = 'Email can only have a maximum of 500 characters.';
nvx.MSG_NAME_MAX = 'Name can only have a maximum of 200 characters.';
nvx.MSG_NAME_CHARACTERS = 'Name can only contain alphabet characters and spaces.';
nvx.MSG_CONTACT_MAX = 'Contact No. can only have a maximum of 100 characters.';
nvx.MSG_VALID_CONTACT = 'Please enter a valid Contact No.';
nvx.MSG_CONFIRM_CLEAR_CART = 'Are you sure you want to remove all the items from your cart?';
nvx.MSG_PLEASE_WAIT = 'Please wait...';
nvx.MSG_ACCEPT_TNC = 'Acceptance of the Terms and Conditions.';

(function (nvx, $, ko) {

    var buyThisOut = false;
    var buyThisClicked = false;

    $(document).ready(function () {
        $('#btnClearCart').click(function () {
            if (confirm(nvx.MSG_CONFIRM_CLEAR_CART)) {
                nvx.spinner.start();
                $.ajax({
                    url: '/.store/store/clear-cart', headers: {'Store-Api-Channel': 'b2c-slm'},
                    type: 'POST', cache: false, dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            window.location.reload();
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function () {
                        alert(msg.MSG_GEN_ERROR);
                    },
                    complete: function () {
                        nvx.spinner.stop();
                    }
                });
            }
        });

        nvx.checkout = new nvx.CheckoutViewModel();
        $.ajax({
            url: '/.store/store/get-checkout-display',
            data: {}, headers: {'Store-Api-Channel': 'b2c-slm', 'Store-Api-Locale': jsLang},
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    if (data.data) {
                        nvx.checkout.updateCheckoutModel(data.data);
                        ko.applyBindings(nvx.checkout, document.getElementById('theCheckout'));

                        if (data.data.customer) {
                            nvx.deets.initDeets(data.data.customer, data.data.payMethod);
                        }
                        if (data.data.hasPromoMerchant) {
                            nvx.deets.hasPromoMerchant(data.data.hasPromoMerchant);
                            nvx.deets.promoMerchants(data.data.promoMerchants);
                        }
                        if (data.data.hasAffiliations) {
                            nvx.deets.hasAffiliations(data.data.hasAffiliations);
                            nvx.deets.affiliations(data.data.affiliations);
                        }
                        if (data.data.payMethod) {
                            nvx.deets.initPayMethod(data.data.payMethod);
                            nvx.deets.paymentTypeLabel(data.data.paymentTypeLabel);
                            nvx.deets.message(data.data.message);
                        }

                        ko.applyBindings(nvx.deets, document.getElementById('personalInfo'));
                    }
                } else {
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });

        nvx.deets = new nvx.CheckoutDeets('deetsForm');
        nvx.deets.initDobPicker();
    });

    nvx.checkout = {};

    nvx.CheckoutViewModel = function () {
        var s = this;

        s.hasBookingFee = ko.observable(true);
        s.bookFeeMode = ko.observable('');
        s.bookFeeQty = ko.observable(0);
        s.bookPriceText = ko.observable('');
        s.bookFeeSubTotalText = ko.observable('');
        s.waivedTotalText = ko.observable('');

    };
    nvx.CheckoutViewModel.prototype = new nvx.CartViewModel();
    nvx.CheckoutViewModel.prototype.updateCheckoutModel = function (baseView) {
        this.updateModel(baseView);

        this.hasBookingFee(baseView.hasBookingFee);
        this.bookFeeMode(baseView.bookFeeMode);
        this.bookFeeQty(baseView.bookFeeQty);
        this.bookPriceText(baseView.bookPriceText);
        this.bookFeeSubTotalText(baseView.bookFeeSubTotalText);
        this.waivedTotalText(baseView.waivedTotalText);
    };

    nvx.deets = {};

    nvx.CheckoutDeets = function (formId) {
        var s = this;

        s.formId = formId;

        s.initDeets = function (base, payMethod) {
            s.idType(base.idType);
            s.idNo(base.idNo);
            s.email(base.email);
            s.mobile(base.mobile);
            s.nationality(base.nationality);
            s.referSource(base.referSource);
            s.dob(base.dob);
            s.initPayMethod(payMethod, base.paymentType);
            s.subscribed(base.subscribed);
            s.name(base.name);
            s.selectedMerchant(base.selectedMerchant);
            s.merchantId(base.merchantId);
            s.oldMerchant(base.merchantId);
        };

        s.initPayMethod = function (payMethod, paymentType) {
            if (payMethod) {
                s.payMethod = payMethod;
                s.paymentType(payMethod);
                s.oldPaymentType(payMethod);
            } else {
                s.paymentType(paymentType);
                s.oldPaymentType(paymentType);
            }
        };

        s.dobPicker = {};
        s.initDobPicker = function () {
            s.dobPicker = $('#dob').pickadate({
                format: 'dd/mm/yyyy', selectYears: 100, selectMonths: true, max: true
            }).data('pickadate');
        };
        s.dobCalClick = function (hey, evt) {
            if (s.dobPicker.get('open')) {
                s.dobPicker.close();
            } else {
                s.dobPicker.open();
            }
            evt.stopPropagation();
        };

        s.idType = ko.observable('NricFin');
        s.idNo = ko.observable();
        s.idDisplay = ko.computed(function () {
            return (s.idType() === 'NricFin' ? '(NRIC/FIN) ' : '(Passport) ') + s.idNo();
        });

        s.email = ko.observable('');

        s.name = ko.observable();
        s.mobile = ko.observable();
        s.nationality = ko.observable('');
        s.referSource = ko.observable('');
        s.dob = ko.observable('');
        s.subscribed = ko.observable(true);
        s.tnc = ko.observable(false);
        s.paymentType = ko.observable('');
        s.paymentTypeLabel = ko.observable('');
        s.payMethod = '';
        s.message = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();

        s.btnGuard = false;

        s.hasPromoMerchant = ko.observable(false);
        s.promoMerchants = ko.observableArray([]);
        s.selectedMerchant = ko.observable('');
        s.merchantId = ko.observable('');

        s.affiliations = ko.observableArray([]);
        s.hasAffiliations = ko.observable(true);

        s.activeAffiliation = ko.observable('');
        s.oldMerchant = ko.observable('');
        s.oldPaymentType = ko.observable('');
        s.waitForAffUpdate = ko.observable(false);

        s.promoCode = ko.observable('');

        s.applyPromoCode = function () {
            $.ajax({
                url: '/.store/store/apply-promo-code',
                data: s.promoCode(), headers: {'Store-Api-Channel': 'b2c-slm'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        window.alert(data.message);
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };

        s.changeMerchant = function (data, event) {
            var nextAction = s.checkMerchantAffiliation(s.oldMerchant(), s.merchantId());
            if (nextAction > 0) {
                s.updateAffiliation(nextAction, true);
            }
        };

        s.changePaymentType = function (data, event) {
            var nextAction = s.checkPaymentTypeAffiliation(s.oldPaymentType(), s.paymentType());
            if (nextAction > 0) {
                s.updateAffiliation(nextAction, false);
            }
        };

        s.updateAffiliation = function (affiliationAction, isBankAffiliation) {
            if (s.waitForAffUpdate()) {
                return;
            }

            s.waitForAffUpdate(true);

            var data = {
                isRemove: affiliationAction == 2,
                isBankAffiliation: isBankAffiliation,
                affiliationToAdd: {
                    affiliationId: s.activeAffiliation()
                },
                deets: s.packageDeets()
            };

            $.ajax({
                url: '/.store/store/update-affiliation',
                data: JSON.stringify(data), headers: {'Store-Api-Channel': 'b2c-slm'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    // console.log(data);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        s.errMsg(data.message);
                        s.isErr(true);
                    }
                },
                error: function () {
                    s.errMsg('System encountered an error. Please try again.');
                    s.isErr(true);
                },
                complete: function () {
                    s.waitForAffUpdate(false)
                }
            });
        };

        // 0 - no need to update
        // 1 - add affiliation
        // 2 - remove affiliation
        s.checkMerchantAffiliation = function (oldId, newId) {
            if (oldId == newId) {
                return 0;
            }
            if (s.hasAffiliations()) {
                var isOldAffiliated = false;
                if (oldId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.creditCard == oldId) {
                            s.activeAffiliation(value.affiliationId);
                            isOldAffiliated = true;
                        }
                    });
                }
                var isNewAffiliated = false;
                if (newId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.creditCard == newId) {
                            s.activeAffiliation(value.affiliationId);
                            isNewAffiliated = true;
                        }
                    });
                }
                if (isOldAffiliated && !isNewAffiliated) {
                    return 2;
                } else if (!isOldAffiliated && isNewAffiliated) {
                    return 1;
                } else if (isOldAffiliated && isNewAffiliated) {
                    return 1;
                }
            }
            return 0;
        };

        // 0 - no need to update
        // 1 - add affiliation
        // 2 - remove affiliation
        s.checkPaymentTypeAffiliation = function (oldId, newId) {
            // console.log("oldId " + oldId + " newId " + newId);
            if (oldId == newId) {
                return 0;
            }
            if (s.hasAffiliations()) {
                var isOldAffiliated = false;
                if (oldId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.paymentType == oldId) {
                            s.activeAffiliation(value.affiliationId);
                            isOldAffiliated = true;
                        }
                    });
                }
                var isNewAffiliated = false;
                if (newId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.paymentType == newId) {
                            s.activeAffiliation(value.affiliationId);
                            isNewAffiliated = true;
                        }
                    });
                }
                if (isOldAffiliated && !isNewAffiliated) {
                    return 2;
                } else if (!isOldAffiliated && isNewAffiliated) {
                    return 1;
                } else if (isOldAffiliated && isNewAffiliated) {
                    return 1;
                }
            }
            return 0;
        };

        s.doCheckout = function (model, evt) {
            if (s.btnGuard) {
                return;
            }

            s.isErr(false);
            s.btnGuard = true;

            if (!s.validate()) {
                s.btnGuard = false;
                return false;
            }

            //nvx.showButtonLoading(evt.target);
            $.ajax({
                url: '/.store/store/checkout',
                data: JSON.stringify(s.packageDeets()), headers: {
                    'Store-Api-Channel': 'b2c-slm',
                    'Store-Api-Locale': jsLang},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        window.location.href = $('#' + s.formId).attr('action');
                    } else {
                        //TODO have to handle other error scenarios that may require page reload e.g. when cart is changed.
                        s.errMsg(data.message);
                        s.isErr(true);
                        s.btnGuard = false;
                    }
                },
                error: function () {
                    s.errMsg('System encountered an error. Please try again.');
                    s.isErr(true);
                    s.btnGuard = false;
                },
                complete: function () {
                }
            });
            //$('#' + s.formId).submit();
        };

        s.validate = function () {
            var errs = '';

            // nvx.isEmpty(s.idType()) || nvx.isEmpty(s.idNo()) ||
            if (nvx.isEmpty(s.nationality()) || nvx.isEmpty(s.referSource()) || nvx.isEmpty(s.mobile()) ||
                nvx.isEmpty(s.email()) || nvx.isEmpty(s.name()) || nvx.isEmpty(s.paymentType()) || !s.tnc()) {
                errs += nvx.MSG_FIELD_REQUIRED + '<ul>';
                if (nvx.isEmpty(s.name())) {
                    errs += '<li>' + nvx.MSG_NAME + '</li>';
                }
                if (nvx.isEmpty(s.email())) {
                    errs += '<li>' + nvx.MSG_EMAIL + '</li>';
                }
                if (nvx.isEmpty(s.mobile())) {
                    errs += '<li>' + nvx.MSG_CONTACT_NO + '</li>';
                }
                /*
                 if (nvx.isEmpty(s.idType())) {
                 errs += '<li>ID Type</li>';
                 }
                 if (nvx.isEmpty(s.idNo())) {
                 errs += '<li>ID No.</li>';
                 }
                 */
                if (nvx.isEmpty(s.nationality())) {
                    errs += '<li>' + nvx.MSG_NATIONALITY + '</li>';
                }
                if (nvx.isEmpty(s.paymentType())) {
                    errs += '<li>' + nvx.MSG_PAYMENT_TYPE + '</li>';
                }
                if (nvx.isEmpty(s.referSource())) {
                    errs += '<li>' + nvx.MSG_WHERE_HEAR + '</li>';
                }
                if (!s.tnc()) {
                    errs += '<li>' + nvx.MSG_ACCEPT_TNC + '</li>';
                }
                errs += '</ul>';
            } else {
                if (!nvx.isValidEmail(s.email())) {
                    errs += nvx.MSG_VALID_EMAIL + '<br/>';
                } else if (!nvx.isLengthWithin(s.email(), 0, 500, true)) {
                    errs += nvx.MSG_EMAIL_MAX + '<br/>';
                } else if (s.email() == 'andrew@gmail.com') {
                    errs += 'For UAT purposes, please use your own email address instead.<br/>';
                }

                /*
                 if ('NricFin' === s.idType() && !(nvx.nric.isNricValid(s.idNo().toUpperCase()) || nvx.nric.isFinValid(s.idNo().toUpperCase()))) {
                 errs += 'Please enter a valid NRIC/FIN, e.g. S1234567G, G1234567G.<br/>';
                 } else if (!nvx.isLengthWithin(s.idNo(), 0, 100, true)) {
                 errs += 'ID No. can only have a maximum of 100 characters.<br/>';
                 }
                 */

                var r = /^[a-zA-Z ]*$/;

                if (!nvx.isLengthWithin(s.name(), 0, 200, true)) {
                    errs += nvx.MSG_NAME_MAX + '<br/>';
                } else if (!r.test(s.name())) {
                    errs += nvx.MSG_NAME_CHARACTERS + '<br/>';
                }
                if (nvx.isNotEmpty(s.mobile()) && !nvx.isLengthWithin(s.mobile(), 0, 100, true)) {
                    errs += nvx.MSG_CONTACT_MAX + '<br/>';
                }
                if (nvx.isNotEmpty(s.mobile())
                    && !nvx.isValidPhoneNumber(s.mobile())) {
                    errs += nvx.MSG_VALID_CONTACT + '<br/>';
                }
            }

            if (errs != '') {
                s.errMsg(errs);
                s.isErr(true);
                return false;
            }
            return true;
        };

        s.packageDeets = function () {
            return {
                email: s.email(),
                idType: s.idType(),
                idNo: s.idNo(),
                name: s.name(),
                mobile: s.mobile(),
                paymentType: s.paymentType(),
                subscribed: s.subscribed(),
                nationality: s.nationality(),
                referSource: s.referSource(),
                dob: s.dob(),
                selectedMerchant: s.getMerchantName(s.merchantId()),
                merchantId: s.merchantId()
            };
        };

        s.getMerchantName = function (merchantId) {
            for (var i = 0; i < s.promoMerchants().length; i++) {
                var promoMerchant = s.promoMerchants()[i];
                if (promoMerchant.merchantId == merchantId) {
                    return promoMerchant.name;
                }
            }
            return "";
        };

        s.unpackDeets = function (d) {
            s.email(d.email);
            s.idType(d.idType);
            s.idNo(d.idNo);
            s.name(d.name);
            s.mobile(d.mobile);
            s.paymentType(d.paymentType);
            s.oldPaymentType(d.paymentType);
            s.subscribed(d.subscribed);
            s.nationality(d.nationality);
            s.referSource(d.referSource);
            s.dob(d.dob);
            s.selectedMerchant(d.selectedMerchant);
            s.merchantId(d.merchantId);
            s.oldMerchant(d.merchantId);

            s.isErr(d.isErr);
            s.errMsg(d.errMsg);
        };

        s.getProductIdList = function () {
            var idList = "";
            $.each(nvx.checkout.baseModel.cmsProducts, function (k, v) {
                idList += v.id;
                idList += "~";
            });
            return idList;
        };

        s.goToTnc = function (data, event) {
            event.stopPropagation();
            window.location.href = './tnc~' + s.getProductIdList();
        }
    };

})(window.nvx = window.nvx || {}, jQuery, ko);
