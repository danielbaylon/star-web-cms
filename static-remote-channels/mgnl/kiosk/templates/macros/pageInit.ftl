[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign lang = sitefn.site().getI18n().getLocale().toString()]
[#assign channel = 'kiosk']
[#assign sitePath = "${ctx.contextPath}/${lang}/kiosk"]
[#assign themePath = "${ctx.contextPath}/resources/kiosk/theme/default"]
[#if lang == "en" || lang == "zh_CN"]
    [#assign imgSuffix = lang!"en"]
[#else]
    [#assign imgSuffix = "en"]
[/#if]

<script>
    setDate();
    setTime();
    setInterval(function () {
        setDate("${lang}");
        setTime("${lang}");

        $('#todayDate').html(window.today['date']);
        $('#todayTime').html("|&nbsp;" + window.today['time']);
    }, 1000);

    function redirectToErrorPage() {
        window.location.href = "${sitePath}/error";
    }

    function showAlertDialog(alertTitle, alertMessage) {

        $("#alertTitle").text(alertTitle);
        $("#alertMessage").text(alertMessage);
        $("#alert-dialog").show();
    }

    function hideAlertDialog() {
        $("#alert-dialog").hide();
    }
</script>

<div id="alert-dialog" class="dialog-container box-wrapper center-center" style="display: none;">
    <div class="alert-container">
        <div style="z-index:-1" class="dialog-container"></div>
        <h2 class="alert-title" id="alertTitle"></h2>
        <p id="alertMessage"></p>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-primary btn-md" onclick="hideAlertDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>