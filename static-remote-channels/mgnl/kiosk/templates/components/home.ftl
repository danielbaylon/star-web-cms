[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign categoryList = starfn.getCMSProductCategories(channel, true, true)]

<div class="main-menu">
    <h1 ng-bind-html="centerText.t" class="ng-binding">
    [#if content.selectTicketText?has_content] ${content.selectTicketText} [/#if]
    </h1>

[#list categoryList as category]

    [#if category.hasProperty("iconImage")]
        [#assign categoryImage = category.getProperty("iconImage").getString()]
    [/#if]

    [#if categoryImage?has_content]
        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
    [/#if]

    [#if category.getName() == "VTBbp"]

        <a id="btnAttractions" class="menu-icon attraction" href="${sitePath}/category~${category.getName()}~" style="margin-top: 0px; margin-left: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>

    [/#if]

    [#if category.getName() == "W5Gxm"]

        <a id="btnExpress" class="menu-icon train" href="${sitePath}/category~${category.getName()}~" style="margin-top: 0px; margin-left: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>

    [/#if]

    [#if category.getName() == "F46KC"]

        <a id="btnPromotion" class="menu-icon promotion" href="${sitePath}/category~${category.getName()}~" style="margin-top: 0px; margin-right: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>

    [/#if]

    [#if category.getName() == "VBzTX"]

        <a id="btnFunPass" class="menu-icon funpass" href="${sitePath}/category~VBzTX~" style="margin-top: 0px; margin-left: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>

    [/#if]
[/#list]
</div>

<div class="bottom-barcode center-center">
    <button type="button" class="btn btn-lg btn-primary ng-binding">
    [#if content.barcodeText?has_content] ${content.barcodeText} [/#if]
    </button>
</div>