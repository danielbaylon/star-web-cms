[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign categoryList = starfn.getCMSProductCategories(channel, true, true)]

<div class="main">

    <aside ng-include="templateSideBarUrl" class="ng-scope">

    [#assign counter = 1]

        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">

        [#list categoryList as category]
            <li>
                [#if (category.getName() == 'VBzTX')]
                    <button type="button" class="menu-${counter} icon-funpass box-wrapper center-center" onclick="javascript:window.location.href='category~${category.getName()}~'">
                        <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                    </button>
                [#else]
                    <button type="button" class="menu-${counter} box-wrapper center-left" onclick="javascript:window.location.href='category~${category.getName()}~'">
                        <span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
                    </button>
                [/#if]
            </li>
            [#assign counter = counter + 1]
        [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">
    [#if lang == "en"]
        <h1 class="primary-heading-1 list-title ng-binding" style="display: block;">${staticText[lang]['searchLabel']!"Search"}</h1>
        <div class="control-bar" style="display: block;">
            <form class="search-form ng-pristine ng-valid">
                    <span>
                        <input id="search-text" type="text" class="search ng-pristine ng-untouched ng-valid" placeholder="Search . . ." ng-model="searchText" ng-click="openKeyboard()"><button type="button" class="btn btn-xxs btn-primary ng-binding" ng-bind="btn.search" ng-click="showResult(searchText)">${staticText[lang]['searchLabel']!"Search"}</button>
                    </span>
                <!-- ngIf: searchText.length>0 && isResult==false -->
            </form>
            <!-- ngIf: isResult==true && previousSearchText.length>0 -->
        </div>
    [/#if]
        <div class="adv-banner ng-scope" ng-if="isResult==false">
            <p class="note-text ng-binding" ng-bind-html="title.promo">
            [#if lang == "en"]
            ${staticText[lang]['searchBodyLabel']!"Search"}
            [#else]
            ${staticText[lang]['searchBodyLabel1']}
                <br />
            ${staticText[lang]['searchBodyLabel2']}
                <br />
            ${staticText[lang]['searchBodyLabel3']}
            [/#if]
            </p>
            <a href="#"><img src="${themePath}/images/samples/search-banner.jpg"></a>
        </div><!-- end ngIf: isResult==false -->
        <!-- ngIf: isResult==true -->
    </div>
</div>