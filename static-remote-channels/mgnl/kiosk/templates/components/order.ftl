[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

[#if productNodeName?has_content]
    [#assign productNode = starfn.getCMSProduct(channel, productNodeName)]
    [#assign axProducts = starfn.getAXProductsByCMSProductNode(productNode)]
    [#assign productNodeDetails = starfn.getAXProductsDetailByCMSProductNode(productNode)]
[/#if]

<script>
    var productsInShoppingCart = [];
    var productEvents = [];
    var itemsInShoppingCart = [];
    var selectedItemQuantityElement;
    var selectedItemDecrementCounterElement;
    var tempNumber = '';

    function decreaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) - 1;

        if(currentValue >= 0)
            $('#' + elementId).val(currentValue);

        if(currentValue == 0)
            $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-inactive');
    }

    function increaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) + 1;

        $('#' + elementId).val(currentValue);
        $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-active');
    }

    function popupTNC() {

        $("#tncDialog").show();
        $('#scrollbar6-scrollbar').remove();
        var myScroll6;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
            myScroll6 = new IScroll('#scrollbar6', { scrollbars: 'custom', interactiveScrollbars: true });
        }, 1);
    }

    function closeOnTNC() {
        $("#tncDialog").hide();
    }

    function openViewMoreDialog() {

        $("#viewMoreDialog").show();
        $('#scrollbar7-scrollbar').remove();
        var myScroll7;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
            myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
        }, 1);
    }

    function closeViewMoreDialog() {
        $("#viewMoreDialog").hide();
    }

    function refreshShoppingCartItemsValue() {

        $.ajax({
            url : '/.store/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    productsInShoppingCart = result.data.cmsProducts;

                    if(result.data.totalQty == 0) {
                        resetButtonToAdd();
                        return;
                    }

                    displayItemsForSelectedProduct();
                    updateItemsSelectedDate();
                    resetButtonToUpdate();

                    refreshScroller() ;
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function resetButtonToAdd() {
        $("#addToFunCartButton").text("${staticText[lang]['addToFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","addToShoppingCart()");
    }

    function resetButtonToUpdate() {
        $("#addToFunCartButton").text("${staticText[lang]['updateFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","updateShoppingCart()");
    }

    function displayItemsForSelectedProduct() {

        itemsInShoppingCart = [];

        $.each(productsInShoppingCart, function(index, product) {

            if(product.id === '${productNodeName}') {
                itemsInShoppingCart = product.items;
            }
        });

        $.each(itemsInShoppingCart, function(index, item) {

            $("#counter-" + item.listingId).val(item.qty);
            $("#minusButton-" + item.listingId).attr('class', 'btn-num icon-minus btn-active');
        });
    }

    function validateShoppingCart() {

        productsInShoppingCart = $.grep(productsInShoppingCart, function(product, index) {

            if(product.id === '${productNodeName}') {
                return false;
            }

            return true;
        });

        itemsInShoppingCart = [];

        $.each(productsInShoppingCart, function(index, product) {

            $.each(product.items, function(index, item) {

                item.cmsProductId = product.id;
                item.cmsProductName = product.name;

                itemsInShoppingCart.push(item);
            });
        });

    [#list axProducts as axProduct]
        [#assign axProductDetail = productNodeDetails[axProduct.getProperty("relatedAXProductUUID").getString()]]

        var quantity = 0;

        var selectedProductEvent;

        $.each(productEvents, function(index, productEvent) {

            if(productEvent.event && productEvent.productId == "${axProductDetail.getProperty('productListingId').getString()}") {
                selectedProductEvent = productEvent;
            }
        });

        if(selectedProductEvent != undefined) {
            quantity = parseInt($("#event-counter-${axProductDetail.getProperty('productListingId').getString()}").val());
        }
        else {
            quantity = parseInt($("#counter-${axProductDetail.getProperty('productListingId').getString()}").val());
        }

        if(quantity > 0) {

            var mainItem = {
                cmsProductId : '${productNodeName}',
                cmsProductName : '${productNode.getProperty("name").getString()}',
                listingId : '${axProductDetail.getProperty("productListingId").getString()}',
                productCode : '${axProductDetail.getProperty("displayProductNumber").getString()}',
                name : '${axProduct.getProperty("itemName").getString()}',
                type : '${axProduct.getProperty("ticketType").getString()}',
                qty : quantity,
                price : parseFloat(${axProductDetail.getProperty("productPrice").getString()})
            };

            if(selectedProductEvent != undefined) {

                mainItem.eventGroupId = selectedProductEvent.eventGroupId;
                mainItem.eventLineId = $('#event-session-' + selectedProductEvent.productId).val();

                $.each(selectedProductEvent.eventLines, function(index, eventLine) {
                    if (mainItem.eventLineId == eventLine.eventLineId) {
                        mainItem.eventSessionName = eventLine.eventName;
                        return false;
                    }
                });

                var eventDate = $('#event-date-' + selectedProductEvent.productId).val();

                if(eventDate == "") {
                    showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyEventDate']}");
                    return false;
                }

                mainItem.selectedEventDate = eventDate;
            }

            itemsInShoppingCart.push(mainItem);
        }
    [/#list]

        if(itemsInShoppingCart.length == 0) {
            showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty2']}");
            return false;
        }

        return true;
    };

    function commitToShoppingCart() {

        $.ajax({
            url : '/.store/store/add-to-cart',
            data : JSON.stringify({
                sessionId : '',
                items : itemsInShoppingCart
            }),
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    redirectToShoppingCartPage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function addToShoppingCart() {

        if(!validateShoppingCart()) {
            return;
        };

        commitToShoppingCart();
    }

    function updateShoppingCart() {

        if(!validateShoppingCart()) {
            return;
        };

        $.ajax({
            url : '/.store/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    commitToShoppingCart();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    };

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function redirectToProductDetailPage() {
        window.location.href="${sitePath}/category/product~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function loaded() {
        myScroll1 = new IScroll('#scrollbar1', { scrollbars: 'custom', interactiveScrollbars: true });
        myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
        myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    function refreshScroller() {

        setTimeout(function() {
            myScroll2.refresh();
        }, 500);
    }

    function displayKeyboard(itemDecrementCounterId, itemQuantityId) {

        tempNumber = '';

        selectedItemDecrementCounterElement = $("#" + itemDecrementCounterId);
        selectedItemQuantityElement = $("#" + itemQuantityId);

        var offset = selectedItemQuantityElement.offset();

        $("#number-keypad").css('top', offset.top + 53);
        $("#number-keypad").css('left', offset.left - 290);

        if($("#number-keypad").is(':visible')) {
            $("#number-keypad").hide();
        }
        else {
            $("#number-keypad").show();
        }
    }

    function hideKeyboard() {
        $("#number-keypad").hide();
    }

    var prods = [];

    function pushIntoProductArray(productItem) {
        prods.push(productItem);
    }

    function initProductEvents() {

        $.ajax({
            url: '/.store/store/products-ext',
            data: JSON.stringify({
                products: prods
            }), headers: { 'Store-Api-Channel' : 'kiosk' },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {

                if (data.success) {

                    productEvents = data.data.products;

                    $.each(data.data.products, function(index, product) {

                        if(product.event) {

                            changeToEventRow(product.productId);
                            resetEventSessionSelectOptions(product);
                            updateEventQuantityValue(product.productId);
                        }
                    });
                }
                else {
                    redirectToErrorPage();
                }
            },
            error: function () {
                redirectToErrorPage();
            },
            complete: function () {
            }
        });
    }

    function updateEventQuantityValue(productId) {

        $.each(itemsInShoppingCart, function(index, item) {

            if(item.listingId == productId) {
                $("#event-counter-" + item.listingId).val(item.qty);
                $("#event-minusButton-" + item.listingId).attr('class', 'btn-num icon-minus btn-active');
            }
        });
    }

    function hideSelectButton(productId) {
        $('#select-event-button-' + productId).hide();
        $('#cancel-event-button-' + productId).show();
    }

    function hideCancelButton(productId) {
        $('#select-event-button-' + productId).show();
        $('#cancel-event-button-' + productId).hide();
    }

    function changeToEventRow(productId) {

        $('#non-event-type-product-' + productId).hide();
        $('#event-type-product-' + productId).show();

        hideEventRow(productId);
    }

    function showEventRow(productId) {

        $('#product-row-' + productId).attr("class", "");
        $('#closing-row-' + productId).show();
        $('#event-row-' + productId).show();

        hideSelectButton(productId);
        refreshScroller();
    }

    function hideEventRow(productId) {

        $('#product-row-' + productId).attr("class", "first-level");
        $('#closing-row-' + productId).hide();
        $('#event-row-' + productId).hide();

        hideCancelButton(productId);
        refreshScroller();
    }

    function resetEventSessionSelectOptions(product) {

        $.each(product.eventLines, function (index, eventLine) {
            $('#event-session-' + product.productId).append($('<option/>', {
                value: eventLine.eventLineId,
                text : eventLine.eventName
            }));
        });

        resetEventSessionSelectListener(product);
    }

    function resetEventSessionSelectListener(product) {

        var productId = product.productId;

        var $sessionSelect = $('#event-session-' + productId);

        $sessionSelect.change(function() {

            var selectedSession = $sessionSelect.val();

            $.each(product.eventLines, function(index, eventLine) {

                if (eventLine.eventLineId == selectedSession) {

                    var eventDates = eventLine.eventDates;
                    var enableDates = [];

                    enableDates.push(true);

                    $.each(eventDates, function(index, eventDate) {
                        var dtArr = eventDate.date.split('/');
                        enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);
                    });

                    var eventDateWidget = $('#event-date-' + productId);

                    var input = eventDateWidget.pickadate({

                        format: 'dd/mm/yyyy',
                        disable: enableDates,
                        container: '#event-date-container',
                        closeOnSelect: true,
                        closeOnClear: true
                    });
                }
            });
        });

        $sessionSelect.change();
    }

    function updateItemsSelectedDate(productId, selectedDate) {

        $.each(productsInShoppingCart, function(index, product) {

            if(product.id == "${productNodeName}") {

                $.each(product.items, function(index, item) {

                    if(item.eventGroupId != "") {

                        var enableDates = [];
                        enableDates.push(true);
                        var dtArr = item.selectedEventDate.split('/');
                        enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);

                        var eventDateWidget = $('#event-date-' + item.listingId);

                        eventDateWidget.attr("value", item.selectedEventDate);
                    }
                });
            }
        });
    }

    function selectNumber(number) {

        tempNumber += number;

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);

        if(actualNumber > 0) {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-active');
        }
        else {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
        }
    }

    function deleteNumber() {

        var inputText = selectedItemQuantityElement.val().toString();

        var textSize = inputText.length - 1;

        if(textSize == 0) {
            selectedItemQuantityElement.val(0);
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
            tempNumber = '';
            return;
        }

        tempNumber = inputText.substring(0, inputText.length-1);

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);
    }

    $(document).ready(function() {
        loaded();
        refreshShoppingCartItemsValue();

        $(document).click(function(e) {

            var container = $("#number-keypad");

            if(e.target.id != undefined && e.target.id.startsWith("counter-")) {
                return;
            }

            if(e.target.id != undefined && e.target.id.startsWith("event-counter-")) {
                return;
            }

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });

        initProductEvents();
    });

</script>

<style>

    .event-row {

        border-style: solid;
        border-width: 1px;
        border-color: #1ca9ea;
        background-color: #f7fdff;
    }

    .select-event {

        width: 200px;
        height:36px;
        border: 4px solid #cccccc;
        padding: 0px;
        margin: 0px;
        text-align: center;
        font-size: 18px;
        font-family: 'harabara', 'STHeiti', Arial, Helvetica, sans-serif;
    }

</style>

[#if productNodeName?has_content]
<div class="main-wrapper" id="event-date-container">
    <div>
        <div>
            <div class="tix-container tix-list tix-collapse">

                [#assign previewMode = ctx.getParameter("preview")!]
                [#assign isOnPreview = false]
                [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
                    [#assign isOnPreview = true]
                    <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
                [/#if]

                <h1 class="secondary-heading sy-hg-md left">${productNode.getProperty("name").getString()}</h1>

                <!--div class="alert-section">
                    <h3>Alert / Announcement</h3>
                    <div id="scrollbar9" style="position:relative;height:45px;width:100%;background-color:#C41C33">
                        <div class="scroller" style="transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">
                            <ul>
                                <li>
                                    <p style="height:100%;width:95%">Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...</p>
                                </li>
                            </ul>
                        </div>
                    <div id="scrollbar9-scrollbar" class="iScrollVerticalScrollbar iScrollLoneScrollbar" style="overflow: hidden; display: block;"><div class="iScrollIndicator" style="transition-duration: 0ms; display: block; height: 12px; transform: translate(0px, 0px) translateZ(0px); transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1);"></div></div></div>
                </div-->

                <div class="clearfix"></div>

                <div class="ticket-container">

                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                        <tr>
                            <td width="47%">${staticText[lang]['standardTickets']}</td>
                            <td width="19%" class="center">${staticText[lang]['price']}</td>
                            <td width="25%" class="center">${staticText[lang]['qty']}</td>
                            <td width="5%" class="center">&nbsp;</td>
                        </tr>
                    </table>

                    <div class="tab-content-container fir-template">
                        <div id="scrollbar2" style="width: 860px;">
                            <div class="scroller">
                                <ul>
                                    <li>
                                        <table cellpadding="0" cellspacing="0" border="0" class="table-content">

                                            [#list axProducts as axProduct]
                                                [#assign axProductDetail = productNodeDetails[axProduct.getProperty("relatedAXProductUUID").getString()]]

                                                <script>
                                                    var productItem = {
                                                        productId: ${axProductDetail.getProperty('productListingId').getString()},
                                                        itemId: ${axProductDetail.getProperty("displayProductNumber").getString()}
                                                    };

                                                    pushIntoProductArray(productItem);
                                                </script>

                                                <tr id="product-row-${axProductDetail.getProperty('productListingId').getString()}" class="first-level">
                                                    <td colspan="3">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr id="axproduct-row-${axProductDetail.getProperty('productListingId').getString()}">
                                                                <td width="45%" class="inner-container">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td class="product-title">${axProduct.getProperty("itemName").getString()}</td>
                                                                        </tr>
                                                                        [#assign promotionList = starfn.getAXProductPromotions(axProduct)]
                                                                        [#list promotionList as promotion]
                                                                            <tr>
                                                                                <td class="product-dn">
                                                                                    [#assign promotionIconUUID = promotion.getProperty("promotionIcon").getString()]
                                                                                    [#assign promotionIconImageURL = damfn.getAssetLink(promotionIconUUID)]
                                                                                    <img src="${promotionIconImageURL}" />
                                                                                    [#assign promotionText = promotion.getProperty("promotionText").getString()]
                                                                                ${promotionText}
                                                                                </td>
                                                                            </tr>
                                                                        [/#list]
                                                                    </table>
                                                                </td>
                                                                <td width="30%" class="center">${axProduct.getProperty("ticketType").getString()}-S$${axProductDetail.getProperty("productPrice").getString()}</td>
                                                                <td width="25%" class="center inner-container">

                                                                    <div id="event-type-product-${axProductDetail.getProperty('productListingId').getString()}" style="display: none;">
                                                                        <button id="cancel-event-button-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="hideEventRow(${axProductDetail.getProperty('productListingId').getString()})">Cancel</button>
                                                                        <button id="select-event-button-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="showEventRow(${axProductDetail.getProperty('productListingId').getString()})">Select</button>
                                                                        <br />
                                                                    </div>
                                                                    <div id="non-event-type-product-${axProductDetail.getProperty('productListingId').getString()}">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <button id="minusButton-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-num icon-minus btn-inactive" onclick="decreaseCounter('counter-${axProductDetail.getProperty('productListingId').getString()}', 'minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                                <td>
                                                                                	<span onclick="displayKeyboard('minusButton-${axProductDetail.getProperty('productListingId').getString()}', 'counter-${axProductDetail.getProperty('productListingId').getString()}')">
                                                                                    	<input id="counter-${axProductDetail.getProperty('productListingId').getString()}" type="text" value="0" class="ng-pristine ng-untouched ng-valid" disabled />
                                                                                	</span>
                                                                                </td>
                                                                                <td>
                                                                                    <button type="button" class="btn-num icon-plus btn-active" onclick="increaseCounter('counter-${axProductDetail.getProperty('productListingId').getString()}', 'minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="event-row-${axProductDetail.getProperty('productListingId').getString()}" class="event-row" style="display: none;">
                                                    <td width="45%" class="inner-container" style="padding-left:20px;">
                                                        <div style="font-weight: bold;">Choose Date of Visit</div>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        <input id="event-date-${axProductDetail.getProperty('productListingId').getString()}" style="width: 200px;" type="text" placeholder="dd/mm/yyyy" readonly />
                                                                        <button type="button" id="event-date-button-${axProductDetail.getProperty('productListingId').getString()}" style="vertical-align: middle; background:transparent;border:none;margin-left:5px;width:26px;">
                                                                            <img src="${themePath}/images/icon-calendar.png" />
                                                                        </button>
                                                                        <div id="datepicker-container" style="position:relative"></div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="30%" class="inner-container">
                                                        <div style="font-weight: bold;">Choose Session</div>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <select id="event-session-${axProductDetail.getProperty('productListingId').getString()}" class="select-event">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="25%" class="center inner-container" style="padding-top:5px;">
                                                        <div style="font-weight: bold;">Qty</div>
                                                        <div>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <button id="event-minusButton-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-num icon-minus btn-inactive" onclick="decreaseCounter('event-counter-${axProductDetail.getProperty('productListingId').getString()}', 'event-minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                    </td>
                                                                    <td>
                                                                    	<span onclick="displayKeyboard('event-minusButton-${axProductDetail.getProperty('productListingId').getString()}', 'event-counter-${axProductDetail.getProperty('productListingId').getString()}')">
                                                                        	<input id="event-counter-${axProductDetail.getProperty('productListingId').getString()}" type="text" value="0" class="ng-pristine ng-untouched ng-valid" disabled />
                                                                    	</span>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn-num icon-plus btn-active" onclick="increaseCounter('event-counter-${axProductDetail.getProperty('productListingId').getString()}', 'event-minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="closing-row-${axProductDetail.getProperty('productListingId').getString()}" style="border-bottom: 1px solid #d9d9d9; display: none;">
                                                    <td colspan="3"></td>
                                                </tr>
                                            [/#list]
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="more-info-container">
                    <div class="more-info">
                        <button class="btn-fifth" onclick="popupTNC()">${staticText[lang]['tAndC']}</button>
                    </div>
                </div>

                <div class="notes-container">
                    <h3>
                        <span>${staticText[lang]['notes']}</span>
                        <button type="button" class="btn-view" onclick="openViewMoreDialog()"></button>
                    </h3>

                    <div id="scrollbar1" style="width: 840px;">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <div style="width:95%">
                                        [#if productNode.hasProperty("notes")]
                                            <p style="height:100%;margin:0">${productNode.getProperty("notes").getString()}</p>
                                        [/#if]
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-md btn-third" onclick="[#if ctx.getParameter('backToCart')?has_content] redirectToShoppingCartPage() [#else] redirectToProductDetailPage() [/#if]">${staticText[lang]['backButtonLabel']}</button>
                    <button id="addToFunCartButton" type="button" class="btn btn-md btn-primary" onclick="addToShoppingCart()"></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tncDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button" onclick="closeOnTNC()"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['tAndC']}</h2>
        <section>
            <div id="scrollbar6" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                <p>
                                    1. General Terms & Conditions<br /><br />

                                    1.1 Ticket price is in Singapore Dollars (SGD) and consist of 7% Goods and Services Tax (GST).<br /><br />

                                    1.2 Online confirmation receipt must be redeemed from Sentosa Ticketing Counters* within 3 months from date of purchase, unless otherwise stated on the respective product notes.<br /><br />

                                    Sentosa Ticketing Counters*:<br /><br />

                                    • Beach Station (10.00am to 9.00pm)<br /><br />

                                    • Fort Siloso (10.30am to 5.30pm)<br /><br />

                                    • Imbiah Forecourt (9.00am to 6.00pm)<br /><br />

                                    • Imbiah Lookout (10.00am to 7.00pm)<br /><br />

                                    • Merlion Plaza (9.00am to 6.00pm)<br /><br />

                                    • Sentosa Boardwalk (9.00am to 5.00pm)<br /><br />

                                    • Sentosa Station, Vivocity, Lobby L, Level 3 (8.30am to 9.00pm) <br /><br />

                                    • Sentosa Merlion (10.00am to 7.30pm)<br /><br />

                                    • Waterfront Station (9.00am to 8.00pm)<br /><br />

                                    *Purchases containing Universal Studios Singapore®, Adventure Cove Waterpark and S.E.A. Aquarium tickets, please redeem your tickets at Waterfront Ticketing Counter. Please note that redemption for these tickets are not available at the Automated Ticket Redemption Kiosk.<br /><br />

                                    1.3 Child price is applicable to children aged 3-12 years inclusive.<br /><br />

                                    1.4 For Universal Studios Singapore®, Adventure Cove Waterpark, S.E.A. Aquarium and Trick Eye Museum, child price is applicable to children aged 4-12 years inclusive.<br /><br />

                                    1.5 Island admission and transportation charges apply separately, unless otherwise stated.<br /><br />

                                    1.6 Tickets purchased are strictly non-refundable. This includes weather conditions.<br /><br />

                                    1.7 Lost or damaged card(s) will not be replaced or refunded.<br /><br />

                                    1.8 Credit Card used for purchase will be required upon redemption for verification purposes.<br /><br />

                                    1.9 Sentosa reserves the right to make changes without further notice to information herein.<br /><br />

                                    2. For Attractions<br />
                                    2.1 Upon exchange of confirmation receipt for attraction tickets, all attraction tickets are only valid on the day of exchange.<br /><br />

                                    2.2 Purchases containing Universal Studios Singapore®, Adventure Cove Waterpark and S.E.A. Aquarium tickets, please redeem your tickets at Waterfront Ticketing Counter and note that redemption for these tickets are not available at the Automated Ticket Redemption Kiosk.<br /><br />

                                    2.3 Refer below for the respective attractions's restrictions.<br /><br />

                                    GoGreen (Fun Ride / Eco Adventure / Eco Adventure+)<br /><br />

                                    Minimum 10 years old<br />
                                    Minimum height : 105cm<br />
                                    Maximum weight : 120kg<br />
                                    iFly Singapore (First Timer Challenge Package)<br /><br />

                                    Photo identity required<br />
                                    Minimum 7 years old<br />
                                    Maximum weight : 120kg if under 1.8m tall / 140kg maximum if over 1.8m tall<br />
                                    MegaZip Adventure Park<br /><br />

                                    Megabounce<br /><br />

                                    Minimum weight : 10kg<br />
                                    Maximum weight : 90kg<br />
                                    ClimbMax<br /><br />

                                    Minimum height : 120cm<br />
                                    Minimum weight : 25kg<br />
                                    Maximum weight : 120kg<br />
                                    Megazip<br /><br />

                                    Minimum height : 90cm (accompanied by adult) / 120cm (unaccompanied)<br />
                                    Minimum weight : 30kg - Maximum weight : 140kg<br />
                                    NorthFace<br /><br />

                                    Minimum weight : 25kg<br />
                                    Maximum weight : 120kg<br />
                                    Parajump<br /><br />

                                    Minimum weight : 30kg<br />
                                    Maximum weight : 120kg<br />
                                    Skyline Luge Sentosa<br /><br />

                                    Luge minimum height : 85cm (accompanied by adult) / 110cm (unaccompanied)<br />
                                    Skyride minimum height : 85cm (accompanied by adult) / 135cm (unaccompanied)<br />
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeOnTNC()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="viewMoreDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['notes']}</h2>
        <section>
            <div id="scrollbar7" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                [#if productNode.hasProperty("notes")]
                                    <p style="height:100%; margin:0">${productNode.getProperty("notes").getString()}</p>
                                [/#if]
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeViewMoreDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="number-keypad" style="z-index:1001; display: none" class="keypad-dialog">
    <div class="arrow-up"></div>
    <h2>Please Key In Quantity</h2>
    <ul class="keypad-controller">
        <li><button type="button" onclick="selectNumber(1)">1</button></li>
        <li><button type="button" onclick="selectNumber(2)">2</button></li>
        <li><button type="button" onclick="selectNumber(3)">3</button></li>
        <li><button type="button" onclick="selectNumber(4)">4</button></li>
        <li><button type="button" onclick="selectNumber(5)">5</button></li>
        <li><button type="button" onclick="selectNumber(6)">6</button></li>
        <li><button type="button" onclick="selectNumber(7)">7</button></li>
        <li><button type="button" onclick="selectNumber(8)">8</button></li>
        <li><button type="button" onclick="selectNumber(9)">9</button></li>
        <li><button type="button" onclick="selectNumber(0)">0</button></li>
        <div class="clearfix"></div>
    </ul>
    <div class="group-inner-btn keypad-group">
        <button type="button" class="btn btn-third btn-xs ng-binding" onclick="deleteNumber()">Delete</button>
        <button type="button" class="btn btn-primary btn-xs ng-binding" onclick="hideKeyboard()">OK</button>
    </div>
</div>
[/#if]