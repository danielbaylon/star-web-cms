[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

<style>
    /* styles for '...' */
    .block-with-text {
        /* hide text if it more than N lines  */
        overflow: hidden;
        /* for set '...' in absolute position */
        position: relative;
        /* use this value to count block height */
        line-height: 1.2em;
        /* max-height = line-height (1.2) * lines max number (3) */
        max-height: 3.6em;
        /* place for '...' */
        margin-right: -1em;
        padding-right: 1em;
    }
    /* create the ... */
    .block-with-text:before {
        /* points in the end */
        content: '...';
        /* absolute position */
        position: absolute;
        /* set position to right bottom corner of block */
        right: 0;
        bottom: 0;
    }
    /* hide ... if we have text, which is less than or equal to max lines */
    .block-with-text:after {
        /* points in the end */
        content: '';
        /* absolute position */
        position: absolute;
        /* set position to right bottom corner of text */
        right: 0;
        /* set width and height */
        width: 1em;
        height: 1em;
        margin-top: 0.2em;
        /* bg color = bg color under block */
        background: white;
    }
</style>

[#if params[0]?has_content]
    [#assign categoryNodeName = params[0]]
    [#assign categoryNode = starfn.getCMSProductCategory(channel, categoryNodeName)]
    [#assign subCategoryList = starfn.getCMSProductSubCategories(categoryNode, false, true)]
    [#assign categoryList = starfn.getCMSProductCategories(channel, true, true)]

    [#assign counter = 0]

<div class="main">
    <aside ng-include="templateSideBarUrl" class="ng-scope">

        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">

            [#list categoryList as category]
                [#assign counter = counter + 1]

                <li>
                    [#if (category.getName() == 'VBzTX')]
                        <button type="button" class="menu-${counter} icon-funpass box-wrapper center-center [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                        </button>
                    [#else]
                        <button type="button" class="menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
                        </button>
                    [/#if]
                </li>

            [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">

        [#assign previewMode = ctx.getParameter("preview")!]
        [#assign isOnPreview = false]
        [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
            [#assign isOnPreview = true]
            <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
        [/#if]

        <h1 class="primary-heading-1 list-title ng-binding">${categoryNode.getProperty("name").getString()}</h1>

        [#if categoryNodeName == "W5Gxm"]

            <div class="box-wrapper center-center">
                <div class="side-an-dialog box-wrapper center-center">
                    <img src="${themePath}/images/icon-express.png">
                    <h1 class="primary-heading sy-hg-md ng-binding" ng-bind-html="title.question">${staticText[lang]['addTicketToCartConfirmationLabel']!"Would you like to Sentosa Express tickets to your cart?"}</h1>
                    <p ng-bind-html="title.hint1" class="ng-binding">${staticText[lang]['addTicketToCartBodyMessageLabel']!"You can enter Sentosa by using your EZlink card or purchase Sentosa Express tickets here."}</p>
                    <table cellpadding="0" cellspacing="0" border="0" class="admission-qty">
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td class="qty-top ng-binding" ng-bind="lbl.quantity">${staticText[lang]['quantityLabel']!"Quantity"}</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="qty-title ng-binding" width="50%" ng-bind="lbl.type">${staticText[lang]['customerTypeLabel']!"Adult/Child"}</td>
                            <td width="15%"><button type="button" class="icon-minus btn-active" ng-class="{'btn-inactive':ticket.qty<1,'btn-active':ticket.qty>=1}" ng-click="minusQty()"></button></td>
                            <td width="20%"><input id="qtyInput" type="text" placeholder="0" ng-model="ticket.qty" ng-readonly="true" ng-click="clickQtyInput()" class="ng-pristine ng-untouched ng-valid" readonly="readonly"></td>
                            <td width="15%"><button type="button" class="icon-plus btn-active" ng-class="{'btn-inactive':ticket.qty>=maxTickets,'btn-active':ticket.qty<maxTickets}" ng-click="addQty()"></button></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top: 10px; color: #666; text-align: center;"><span style="color:red;">*</span><span ng-bind="title.hint2" class="ng-binding">${staticText[lang]['noteLabel']!"Children from the age of 3 will need an express ticket."}</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="group-inner-btn">
                        <button type="button" class="btn btn-primary btn-md ng-binding" ng-click="addToFunCart()" ng-bind="toFunCart">${staticText[lang]['addTicketToCartButtonLabel']!"Update FUN Cart"}</button>
                    </div>
                </div>
            </div>

        [#else]

            <div class="control-bar">
                [#if categoryNodeName == "VTBbp" && lang == "en"]
                    <form class="search-form ng-pristine ng-valid ng-scope">
                            <span>
                                <input id="search-text" type="text" class="search ng-pristine ng-untouched ng-valid" placeholder="Search . . ." ng-model="searchText" ng-click="openKeyboard()"><button type="button" class="btn btn-xxs btn-primary ng-binding" ng-bind="btn.search" ng-click="showResult(searchText)">${staticText[lang]['searchLabel']!"Search"}</button>
                            </span>
                            <span class="sort-option">
                                <span class="sort ng-binding" ng-bind="sort.start">Sort By</span>
                                <button type="button" class="btn-six btn-selected ng-binding" ng-bind="sort.by1">Name</button>
                                <button type="button" class="btn-six ng-binding" ng-bind="sort.by2">Price</button>
                            </span>
                    </form>
                [/#if]

                <div class="filter-option">

                    [#if params[1]?has_content]
                        [#assign selectedSubCategory = params[1]]
                    [/#if]

                    [#assign counter = 0]

                    [#list subCategoryList as subCategory]

                        [#assign counter = counter + 1]

                        <button type="button" class="btn-six [#if selectedSubCategory?has_content] [#if selectedSubCategory == subCategory.getName()] [#assign subCategoryNode = subCategory] btn-selected [/#if] [#elseif counter == 1] [#assign subCategoryNode = subCategory] btn-selected [/#if]"
                                onclick="javascript:window.location.href='category~${categoryNodeName}~${subCategory.getName()}~'">
                        ${subCategory.getProperty("name").getString()}
                        </button>

                    [/#list]
                </div>
            </div>

            [#assign subCategoryProductList = starfn.getCMSProductBySubCategory(subCategoryNode, false)]
            [#if subCategoryProductList?has_content]

                <div class="product-list">

                    [#if pageNumber?has_content]
                        [#if pageNumber > 1]
                            <button type="button" class="left-arrow" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${pageNumber-1}'"></button>
                        [/#if]
                    [/#if]

                    [#assign totalPages = (subCategoryProductList?size/9)?floor + 1]

                    [#if totalPages > 1]
                        [#if pageNumber?has_content]
                            [#if pageNumber != totalPages]
                                <button type="button" class="right-arrow" data-bind="click: kiosk.ProductNavigator.prevPage" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${pageNumber+1}'"></button>
                            [/#if]
                        [#else]
                            <button type="button" class="right-arrow" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=2'"></button>
                        [/#if]
                    [/#if]

                    [#assign totalProducts = 0]

                    <ul class="thumbnail-list">

                        [#list subCategoryProductList as subCategoryProduct]

                            [#assign productImages = starfn.getCMSProductImagesUUID(subCategoryProduct, lang)!]

                            [#assign totalProducts = totalProducts + 1]
                            [#assign pageIndex = ((totalProducts/9)?floor) + 1]

                            [#if pageIndex == pageNumber]

                                <li onclick="javascript:window.location.href='${sitePath}/category/product~${categoryNodeName}~${subCategoryNode.getName()}~${subCategoryProduct.getName()}~?pageNumber=${pageIndex}'">

                                    <img [#if productImages[0]?has_content] src="${damfn.getAssetLink(productImages[0])}" [#else] src="" [/#if] alt="" class="thumbnail-img"  id="rotator-${subCategoryProduct.getName()}" />

                                    [#if productImages?size > 1]

                                        <script type="text/javascript">
                                            (function() {

                                                var rotator = $("#rotator-${subCategoryProduct.getName()}");
                                                var delayInSeconds = 5;
                                                var images = new Array(${productImages?size});

                                                [#assign counter = 0]
                                                [#list productImages as productImage]
                                                    images[${counter}] = "${damfn.getAssetLink(productImage)}";
                                                    [#assign counter = counter + 1]
                                                [/#list]

                                                var num = 1;
                                                var changeImage = function() {

                                                    rotator.fadeOut(1000, function() {

                                                        var len = images.length;
                                                        num++;

                                                        if (num == len) {
                                                            num = 0;
                                                        }

                                                        rotator.attr("src",images[num]);
                                                    }).fadeIn(1000);
                                                };

                                                setInterval(changeImage, delayInSeconds * 1000);
                                            })();
                                        </script>
                                    [/#if]

                                        <span class="caption">
                                            <span class="caption-title">${subCategoryProduct.getProperty("name").getString()}</span>
                                            <span class="caption-description">
                                                <div class="block-with-text">Your full experience in Madame Tussauds Singapore includes:
A journey thatuncovers Singapore extraordinary history – from humble fishing village to 21st century powerhouse. Experience the sights, sounds and smells of yesteryear in this 45 minute actor-led experience which features 15 immersive areas, live actors and special effects.${subCategoryProduct.getProperty("shortDescription").getString()}</div>
                                            </span>
                                        </span>
                                </li>
                            [/#if]

                        [/#list]

                        <div class="clearfix"></div>
                    </ul>

                    <ul class="paging box-wrapper center-center">

                        [#list 1..totalPages as page]

                            <li>
                                <a href="${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${page}" [#if pageNumber?has_content] [#if page == pageNumber] class="selected" [/#if] [#else] class="selected" [/#if]></a>
                            </li>

                        [/#list]
                        <div class="clearfix"></div>
                    </ul>
                </div>
            [/#if]
        [/#if]
    </div>
</div>
[/#if]