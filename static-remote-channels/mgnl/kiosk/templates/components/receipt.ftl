[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

<script>

    function clearShoppingCart() {

        $.ajax({
            url : '/.store/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    redirectToHomePage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function redirectToHomePage() {
        window.location.href = "${sitePath}";
    }

    function checkoutAfterPayment() {

        $.ajax({
            url: '/.store/store/checkout-complete-sale',
            data: {},
            headers: { 'Store-Api-Channel' : 'kiosk' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {

                if (result.success) {
                    generateTicket();
                } else {
                    //redirectToErrorPage();
                }
            },
            error: function () {
                redirectToErrorPage();
            },
            complete: function () {
            }
        });
    }

    function generateTicket() {

        $.ajax({
            url : '/.store/store/generate-paper-tickets-package/kiosk',
            data : '',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    processTicket(result);
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                // redirectToErrorPage();
            }
        });
    }

    function processTicket(result) {

        var index = 0;
        var ticketXmls = result.data.ticketXmls;
        var totalIndexes = ticketXmls.length;

        if(totalIndexes == 0)
            return;

        printTicket(ticketXmls, index, totalIndexes, result.data.receiptXml);
    }

    function printTicket(ticketXmls, index, totalIndexes, receiptXml) {

        var ticketXml = ticketXmls[index].ticketXml;

        $.ajax({
            url : 'http://127.0.0.1:9100/api/print-ticket',
            data : '{ "templateXml" : "' + ticketXml + '" }',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
        }).then(function() {

            setTimeout(function() {

                var nextIndex = index+1;

                console.log("Ticket: " + nextIndex + " printed.");

                if(nextIndex == totalIndexes) {
                    printReceipt(receiptXml);
                }
                else {
                    printTicket(ticketXmls, nextIndex, totalIndexes, receiptXml);
                }
            }, 5000);
        });
    }

    function printReceipt(receiptXml) {

        $.ajax({
            url : 'http://127.0.0.1:9100/api/print-receipt',
            data : '{ "templateXml" : "' + receiptXml + '" }',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    console.log("Receipt printed");
                    showCompletedScreen();
                }
                else {
                    redirectToPrintErrorPage();
                }
            },
            error : function() {
                redirectToPrintErrorPage();
            }
        });
    }

    function showCompletedScreen() {
        $("#paymentSuccessfulContent").hide();
        $("#printSuccessfulContent").show();
    }

    function redirectToPrintErrorPage(errorMessage) {
        $("#printErrorMessage").text(errorMessage);
        $("#printErrorContent").show();
    }

    $(document).ready(function() {
        checkoutAfterPayment();
    });

</script>

<div id="paymentSelectionContent">
    <div id="paymentSuccessfulContent">
        <div class="main-wrapper">
            <h1 class="box-wrapper top-center">${staticText[lang]['transactionSuccessLabel1']}</h1>
            <h2 class="primary-heading py-hg-sm">${staticText[lang]['transactionSuccessLabel2']}</h2>
            <ul class="box-wrapper center-center printing-img">
                <li>
                    <img src="${themePath}/images/printing-at.gif" class="printing-at"/>
                    <img src="${themePath}/images/collection/take-receipt-ticket.jpg" />
                </li>
            </ul>
        </div>
    </div>

    <div id="printSuccessfulContent" style="display: none;">
        <div class="main-wrapper">
            <ul class="box-wrapper center-center printing-img complete-box">
                <li>
                    <img src="${themePath}/images/icon-success.png" />
                    <h2 class="success-heading">${staticText[lang]['printSuccessLabel1']}</h2>
                    <p class="secondary-heading sy-hg-sm">
                    ${staticText[lang]['printSuccessLabel2']}
                        <br />
                        <img src="${themePath}/images/logo-complete.png" />
                    </p>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="clearShoppingCart()">${staticText[lang]['backToHomeButtonLabel']}</button>
        </div>
    </div>

    <div id="printErrorContent" style="display: none;">
        <div class="main-wrapper">
            <h1 class="secondary-heading sy-hg-lg"></h1>
            <ul class="box-wrapper center-center error-box">
                <li>
                    <img src="${themePath}/images/icon-fail.png" class="icon-fail"/>
                    <h2 id="printErrorMessage" class="error-heading"></h2>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary">OK</button>
        </div>
        <div ng-include="'templates/pop-up-loading.html'" ng-show="showLoading==true"></div>
    </div>
</div>