[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<div>
    <div class="bubble-animated">
        <div class="lt-pink-be"></div>
        <div class="lt-blue-be"></div>
        <div class="lt-purple-be"></div>
        <div class="rt-blue-be"></div>
        <div class="rt-yellow-be"></div>
        <div class="rt-red-be"></div>
        <div class="rt-pink-be"></div>
        <div class="rt-purple-be"></div>
    </div>
    <div class="error-container">
        <div class="error-content">
            <h1 ng-bind-html="title.message">We are currently unable to accept any transactions. Please kindly proceed to other kiosks or to ticketing counters to make your purchase.</h1>
            <h3 ng-bind-html="preTrxChkMsg"></h3>
        </div>
    </div>
</div>