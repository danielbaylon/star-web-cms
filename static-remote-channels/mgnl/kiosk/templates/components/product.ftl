[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]
[#assign productNode = starfn.getCMSProduct(channel, productNodeName)]
[#assign productImages = starfn.getCMSProductImagesUUID(productNode, lang)!]
[#assign axProducts = starfn.getAXProductsByCMSProductNode(productNode)]

[#if productNodeName?has_content]

<script>

    $(document).ready(function() {
        var myScroll3 = new IScroll('#scrollbar3', { scrollbars: 'custom', interactiveScrollbars: true });
        var myScroll4 = new IScroll('#scrollbar4', { scrollbars: 'custom', interactiveScrollbars: true });
        var myScroll8 = new IScroll('#scrollbar8', { scrollbars: 'custom', interactiveScrollbars: true });
    });
</script>

<div class="main-wrapper">
    <div ng-include="templateDetailsUrl" class="ng-scope">
        <div ng-controller="ProductInformationCntl" class="ng-scope">

            <div class="tix-container tix-collapse">

                [#assign previewMode = ctx.getParameter("preview")!]
                [#assign isOnPreview = false]
                [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
                    [#assign isOnPreview = true]
                    <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
                [/#if]

                <h1 class="secondary-heading sy-hg-md left ng-binding" ng-show="haveTitle==true" ng-bind-html="product.Title">${productNode.getProperty("name").getString()}</h1>

                [#if productImages?has_content]
                    [#assign thumbnailAssetLink = damfn.getAssetLink(productImages[0])]
                    <div class="product-img">
                        <img src="${thumbnailAssetLink}" />
                    </div>
                [/#if]

                <div class="alert-section">
                    <h3>Alert / Announcement</h3>
                    <div id="scrollbar3" style="position:relative;height:105px;width:100%;background-color:#C41C33">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <p style="height:100%;width:95%">Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <h2 class="discription-title">Description</h2>
                <div id="scrollbar8" class="description" style="position:relative;height:150px;width:870px">
                    <div class="scroller">
                        <ul>
                            <li>
                                <div style="width:95%">
                                ${productNode.getProperty("description").getString()}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="main-info">
                    <div id="scrollbar4" style="height:265px;width:870px">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <p>
                                        <img src="${themePath}/images/icon-time.png" />
                                    	<span class="info-element">
                                    		<span class="label ng-binding">Opening Hours: </span>10am - 8pm daily(Last Entry 7:30pm)
                                    	</span>
                                    </p>
                                    <p>
                                        <img src="${themePath}/images/icon-admission.png" />
                                    	<span class="info-element">
                                    		<span class="label ng-binding">Admission Info: </span>S$10 / Adult, S$7 / Child (Child 3 - 12 years old)
                                    	</span>
                                    </p>
                                    <p>
                                        <img src="${themePath}/images/icon-duration.png" />
                                    	<span class="info-element">
                                    		<span class="label ng-binding">Recommended Duration: </span>
                                    	</span>
                                    </p>
                                    <p ng-show="haveZone==true">
                                        <img src="${themePath}/images/icon-station.png" />
                                    	<span class="info-element">
                                    		<span class="label ng-binding">Zone / Nearest Station: </span>Imbiah Lookout
                                    	</span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-md btn-third ng-binding" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}'">${staticText[lang]['backButtonLabel']!"Back"}</button>
                    [#if axProducts?size != 0]
                        <button type="button" class="btn btn-md btn-primary ng-binding" onclick="javascript:window.location.href='${sitePath}/category/product/order~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}'">${staticText[lang]['buyTicketButtonLabel']}</button>
                    [/#if]
                </div>
            </div>
        </div>
    </div>
</div>
[/#if]