[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

<script>

    function exitPayment() {
        showExitConfirmationDialog();
    }

    function showExitConfirmationDialog() {
        $('#exitConfirmationDialog').show();
    }

    function hideExitConfirmationDialog() {
        $('#exitConfirmationDialog').hide();
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart";
    }

    function redirectToHomePage() {
        window.location.href = "${sitePath}";
    }

    function clearShoppingCart() {

        $.ajax({
            url : '/.store/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    redirectToHomePage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function makePayment() {
        $('#paymentSelectionContent').hide();
        $('#paymentProgressContent').show();

        setTimeout(function() {
            redirectToReceiptPage();
        }, 5000);
    }

    function redirectToReceiptPage() {
        window.location.href = "${sitePath}/receipt";
    }
</script>

<div id="paymentSelectionContent">
    <div class="main-wrapper">
        <h1 class="primary-heading py-hg-lg ng-binding" ng-bind-html="title.header1">Please select one of the payment modes</h1>
        <h3 class="third-heading ng-binding" ng-bind-html="title.header2">Total Amount Payable: <span id="totalPayAmount" class="payable-amount">S$50.00</span></h3>
        <ul class="box-wrapper center-center pt-mode-list">
            <li>
                <h4 ng-bind="title.creditCard" class="ng-binding">Credit Card</h4>
                <button type="button" class="btn-credit" onclick="makePayment()"></button>
            </li>
            <li>
                <h4 ng-bind="title.nets" class="ng-binding">NETS</h4>
                <button type="button" class="btn-nets" onclick="makePayment()"></button>
            </li>
            <div class="clearfix"></div>
        </ul>
        <ul class="box-wrapper center-center pt-mode-list">
            <li>
                <h4 ng-bind="title.contactlessCreditCard" class="ng-binding">Contactless Credit Card</h4>
                <button type="button" class="btn-contactless" onclick="makePayment()"></button>
            </li>
            <li>
                <h4 ng-bind="title.netsFlashPay" class="ng-binding">NETS FlashPay</h4>
                <button type="button" class="btn-flashpay" onclick="makePayment()"></button>
            </li>
            <div class="clearfix"></div>
        </ul>
    </div>
    <div class="btn-exit-container">
        <button type="button" class="btn btn-md btn-exit" onclick="exitPayment()">Exit Payment</button>
    </div>

    <div id="exitConfirmationDialog" class="dialog-container box-wrapper center-center" style="display: none;">
        <div class="dialog-ct" style="height:430px">
            <div class="btn-close">
                <button type="button" onclick="hideExitConfirmationDialog()"></button>
            </div>
            <h2 class="primary-heading sy-hg-md">No amount will be deducted.</h2>
            <h2 class="primary-heading sy-hg-md">Would you like to go back to FUN Cart or exit this transaction?</h2>
            <div class="group-inner-btn">
                <button type="button" class="btn btn-md btn-primary" onclick="redirectToShoppingCartPage()">Back to FUN Cart</button>
                <button type="button" class="btn btn-xs btn-primary" onclick="clearShoppingCart()">Exit and Clear My Transaction</button>
            </div>
        </div>
    </div>
</div>

<div id="paymentProgressContent" style="display: none;">
    <div class="main-wrapper">
        <div class="box-wrapper center-center box-full">
            <h1 class="primary-heading py-hg-lg">${staticText[lang]['paymentProgressLabel']}</h1>
        </div>
    </div>
</div>