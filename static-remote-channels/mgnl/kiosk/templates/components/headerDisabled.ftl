[#include "/kiosk/templates/macros/pageInit.ftl"]

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<header>
    <button type="button" class="logo"></button>
    <nav ng-include="'templates/top-bar-disabled.html'" class="ng-scope">
        <ul ng-controller="TopBarCntl" class="ng-scope">
            <li>
                <button type="button" class="btn-home-grey ng-binding" disabled="" ng-bind="btn.home">Home</button>
            </li>
            <li>
                <button type="button" class="btn-search-grey ng-binding ng-scope" disabled="" ng-if="showTopSearch==true" ng-bind="btn.search">Search</button>
            </li>
            <li>
                <button type="button" class="btn-lang-grey ng-binding" disabled="" ng-bind="btn.translate">华 语</button>
            </li>
        </ul>
    </nav>

    <div ng-include="templateTodayUrl" class="ng-scope">
        <div ng-controller="TodayCntl" class="ng-scope">
            <div class="date-time">
                <span class="date ng-binding" ng-bind="today.date">14 April 2016</span>&nbsp;|&nbsp;<span class="time ng-binding" ng-bind="today.time">06:11 PM</span>
            </div>
        </div>
    </div>

    <div class="container center-center">
        <div class="fun-box center-center">
            <div class="items">
                <span class="cart-grey ng-binding" ng-bind="cartTotalQty">1</span>
            </div>
            <div class="funbox-logo ng-binding" ng-bind-html="btnFunBox">
                <img src="${themePath}/images/funbox-logo.png" />
            </div>
        </div>
    </div>

    <div class="clearcart-container center-center">
        <button type="button" class="btn-clearcart btn-clearcart-grey ng-binding" disabled="" ng-bind-html="btnFunBoxRestart">Clear Cart</button>
    </div>
</header>