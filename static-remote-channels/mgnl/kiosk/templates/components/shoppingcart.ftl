[#include "/kiosk/templates/macros/pageInit.ftl"]
[#include "/kiosk/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    var productsInShoppingCart = [];

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var itemsInShoppingCart = [];
                    var totalPrice = "S$0.00";

                    if(result.data.totalQty != 0) {
                        totalPrice = result.data.totalText.replace(/\s/g, '');
                    }

                    productsInShoppingCart = result.data.cmsProducts;

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);

                    loadScroller();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function deleteItemInCart(cartItemId) {

        $.ajax({
            url : '/.store/store/remove-item-from-cart/' + cartItemId,
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPrice = "S$0.00";

                    productsInShoppingCart = result.data.cmsProducts;
                    totalPrice = result.data.totalText.replace(/\s/g, '');

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);
                    setTimeout(function () {
                        $('.fun-box').addClass('move');
                        setTimeout(function () {
                            $('.fun-box').removeClass('move');
                        }, 800);
                    }, 300);
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {

            rowsHtml += '<tr>' +
                    '	<td colspan="6" class="cart-title">' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                    '			<tr>' +
                    '				<td>' +
                    '					<span class="product-title">' + product.name + '</span>' +
                    '					<button type="button" class="btn-cart btn-edit" onclick="redirectToOrderPage(\'' + product.id + '\')">${staticText[lang]["editButtonLabel"]}</button>' +
                    '				</td>' +
                    '				<td colspan="4">&nbsp;</td>' +
                    '			</tr>' +
                    '		</table>' +
                    '	</td>' +
                    '</tr>' +
                    '<tr>' +
                    '	<td>' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">';

            $.each(product.items, function(index, item) {

                rowsHtml += '<tr>' +
                        '   <td>' +
                        '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                        '           <tr class="cart-list">' +
                        '               <td width="46%" class="title">' + item.name +
                        '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]}: ' + item.type + '</p>' +
                        '                   <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                        '               </td>';

                rowsHtml += ' <td width="15%" class="center">S$' + item.price.toFixed(2) + '</td>' +
                        ' <td width="11%" class="center">' + item.qty + '</td>';

                if(item.discountTotal != undefined && item.discountTotal > 0) {
                    rowsHtml += ' <td width="18%" class="center">' +
                            '	<span style="color: red; text-decoration: line-through;">S$' + (parseFloat(item.total) + parseFloat(item.discountTotal)).toFixed(2) + '</span><br />' +
                            ' 	S$' + parseFloat(item.total).toFixed(2) +
                            ' </td>';
                }
                else {
                    rowsHtml += ' <td width="18%" class="center">S$' + parseFloat(item.total).toFixed(2) + '</td>';
                }

                rowsHtml += '  				<td width="5%" class="center del-container">' +
                        '                   <button type="button" class="btn-del" onclick="deleteItemInCart(\'' + item.cartItemId + '\')"></button>' +
                        '               </td>' +
                        '           </tr>' +
                        '       </table>' +
                        '   </td>' +
                        '</tr>';
            });

            rowsHtml += '		</table>' +
                    '	</td>' +
                    '</tr>';
        });

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }

    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }

    function clearShoppingCart() {

        $.ajax({
            url : '/.store/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : 'kiosk'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    redirectToHomePage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function checkout() {

        var customerDetail = function() {
            return {
                email: 'abc@test123.com',
                idType: '',
                idNo: '1221322323',
                name: 'Abc',
                mobile: 89377834,
                paymentType: 'master',
                subscribed: true,
                nationality: 'Singapore',
                referSource: '',
                dob: '20/10/1990'
            };
        };

        $.ajax({
            url: '/.store/store/checkout',
            data: JSON.stringify(customerDetail()), headers: { 'Store-Api-Channel' : 'kiosk' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    redirectToOrderSummaryPage();
                } else {
                    redirectToErrorPage();
                }
            },
            error: function () {
                redirectToErrorPage();
            },
            complete: function () {
            }
        });
    }

    function redirectToHomePage() {
        window.location.href='${sitePath}';
    }

    function redirectToCategoryPage() {
        window.location.href='${sitePath}/category~VBzTX~';
    }

    function redirectToOrderPage(productNodeId) {
        window.location.href='${sitePath}/category/product/order~${categoryNodeName}~${subCategoryName}~' + productNodeId + '~?pageNumber=${pageNumber}&backToCart=true';
    }

    function redirectToOrderSummaryPage() {
        window.location.href='${sitePath}/ordersummary';
    }

    function loadScroller() {

        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    $(document).ready(function() {
        fetchShoppingCartItems();
    });

</script>

[#assign categoryList = starfn.getCMSProductCategories(channel, true, true)]

[#assign counter = 0]

<div class="main">
    <aside>

        <ul class="sidebar-list">

        [#list categoryList as category]
            [#assign counter = counter + 1]

            <li>
                [#if (category.getName() == 'VBzTX')]
                    <button type="button" class="menu-${counter} icon-funpass box-wrapper center-center" onclick="javascript:window.location.href='category~${category.getName()}~'">
                        <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                    </button>
                [#else]
                    <button type="button" class="menu-${counter} box-wrapper center-left" onclick="javascript:window.location.href='category~${category.getName()}~'">
                        <span class="menu-text">${category.getProperty("name").getString()}</span>
                    </button>
                [/#if]
            </li>

        [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['funCartLabel']}</h1>
        <div class="fun-cart-content">
            <p class="cart-note">${staticText[lang]['shoppingCartNote']}</p>
            <div class="cart-box ticket-container">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                    <tr>
                        <td width="46%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitlePrice']}</td>
                        <td width="11%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                        <td width="18%" class="center">${staticText[lang]['shoppingCartTitleSubtotal']}</td>
                        <td width="10%" class="center">&nbsp;</td>
                    </tr>
                </table>
                <div class="tab-content-container for-template">
                    <div id="scrollbar2" style="height:440px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="total-bar">
                    <div class="btn-container">
                        <button type="button" class="btn btn-edit btn-clear" onclick="clearShoppingCart()">${staticText[lang]['clearCartButtonLabel']}</button>
                    </div>
                    <div class="total">
                    ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                    </div>
                </div>
                <div class="admission-note">
                    <p>Please note that the above items DO NOT include admission/entry fee into Sentosa island.</p>
                </div>
                <div class="cart-group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-secondary btn-md" onclick="redirectToCategoryPage()">${staticText[lang]['addMoreFunButtonLabel']}</button>
                    <button type="button" class="btn btn-primary btn-md" onclick="checkout()">${staticText[lang]['proceedButtonLabel']}</button>
                </div>
            </div>
        </div>
    </div>
</div>