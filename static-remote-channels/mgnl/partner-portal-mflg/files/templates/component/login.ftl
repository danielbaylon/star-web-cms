[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
[#assign loginErrorRequest = ctx.getRequest() ]
[#if loginErrorRequest?? && loginErrorRequest?has_content && loginErrorRequest.getAttribute("PPMFLG_LOGIN_SUSPENDED_CUSTOMER_SUPPORT_MSG")?? ]
    [#assign loginErrorSupportMsg = loginErrorRequest.getAttribute("PPMFLG_LOGIN_SUSPENDED_CUSTOMER_SUPPORT_MSG") ]
    [#if loginErrorSupportMsg?? && loginErrorSupportMsg?has_content]
    <div id="loginErrorCustomerSupportMsgDiv" style="display: none;">${loginErrorSupportMsg!""}</div>
    [/#if]
[/#if]

[#if loginErrorRequest?? && loginErrorRequest?has_content && loginErrorRequest.getAttribute("PPMFLG_LOGIN_FAILED_ERROR")?? ]
    [#assign loginErrorMsg = loginErrorRequest.getAttribute("PPMFLG_LOGIN_FAILED_ERROR") ]
    [#if loginErrorMsg?? && loginErrorMsg?has_content]
    <div id="loginErrorMsgDiv" style="display: none;">${loginErrorMsg!""}</div>
    [/#if]
[/#if]
<div class="main-content-section-container">
    <h1 class="heading center">Get Started!</h1>
    <div class="panel background login-panel" id="loginForm">
        <h3 class="heading center">Already a Sentosa Partner?</h3>
        <form method="post" action="${ctx.contextPath}${API_PREFIX!""}/publicity/partner-account/login" class="form-horizontal" id="userLoginForm">
            <div class="error-section" id="loginAlert" style="display: none;" data-bind="visible: isErr, html: errMsg"></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="user-main-info">
                <tr>
                    <td width="25%">
                        <label class="login-bold-text">Username:</label>
                    </td>
                    <td width="75%">
                        <input type="text" name="username"
                               class="input-textarea" placeholder="Username" id="un" data-bind="value: un">
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                        <label class="login-bold-text">Password:</label>
                    </td>
                    <td width="75%">
                        <input type="password" name="password"
                               class="input-textarea" placeholder="Password" id="pw" data-bind="value: pw">
                    </td>
                </tr>
                <!--
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td>
                         <input type="checkbox" name="remember" value="remeber" id="checkbox_id" class="login-checkbox"><label for="checkbox_id" class="login-checkbox-text">Remember me</label>
                    </td>
                </tr>
                 -->
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="75%">
                        <input type="button" class="btn btn-primary" data-bind="click: doLogin" value="Sign In"></input>

                    </td>
                </tr>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="75%">
                        <a href="${sitePath}/forgot-password" class="link-text">Forgot Password?</a>
                    </td>
                </tr>
            </table>
        </form>

    </div>
    <div>
    [@cms.area name="adBanner"/]
    </div>
</div>
[#assign firstCategoryNode = starfn.getFirstCategory(channel)]
[#assign firstCategoryNodeName = ""]
[#if firstCategoryNode?has_content]
    [#assign firstCategoryNodeName = firstCategoryNode.getName()]
[/#if]

[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = false]
[#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
    [#assign isOnPreview = true]
[/#if]

[#assign checkUserLogin = true]

[#if cmsfn.authorInstance && cmsfn.previewMode]
    [#assign checkUserLogin = false]
[/#if]

[#if cmsfn.authorInstance && cmsfn.editMode]
    [#assign checkUserLogin = false]
[/#if]

[#if cmsfn.authorInstance && isOnPreview]
    [#assign checkUserLogin = false]
[/#if]


[#if checkUserLogin]
<script>
    var user_account_suspended_error_msg = '';
    $(document).ready(function() {
        $.ajax({
            url: '${API_PREFIX!""}/publicity/partner-account/is-logged-in',
            headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    window.location.replace('/${channel}/dashboard');
                }
            },
            error: function () {

            },
            complete: function () {
            }
        });

        var LoginModel = function(){
            var s = this;
            s.un = ko.observable("");
            s.pw = ko.observable("");
            s.isErr = ko.observable(false);
            s.errMsg = ko.observable("");
            s.doCheckAndLogin = function(){
                if(s.un() != undefined && s.un() != null && s.un() != ''){
                    if(s.pw() != undefined && s.pw() != null && s.pw() != ''){
                        s.doLogin();
                    }
                }
            };
            s.doLogin = function() {
                s.isErr(false);
                s.errMsg('');
                if (!s.un()) {
                    s.isErr(true);
                    s.errMsg("Please fill in username.");
                    return;
                }
                if (!s.pw()) {
                    s.isErr(true);
                    s.errMsg("Please fill in password.");
                    return;
                }
                $('#userLoginForm').submit();
            }
        }
        var loginModel = new LoginModel();
        if($('#loginErrorMsgDiv').size() > 0){
            var loginErrorMsg = $('#loginErrorMsgDiv').text();
            if(loginErrorMsg != undefined && loginErrorMsg != null && loginErrorMsg != ''){
                var processedErrorMsg = nvx.isDefinedMsg(loginErrorMsg) ? nvx.getDefinedMsg(loginErrorMsg) : loginErrorMsg;
                if('login.err.LoginSuspended' == loginErrorMsg){
                    if($('#loginErrorCustomerSupportMsgDiv').size() > 0){
                        var v = $('#loginErrorCustomerSupportMsgDiv').text();
                        if(v != undefined && v != null && v != '' && v != ' '){
                            processedErrorMsg = '<div>'+(processedErrorMsg)+'</div><div><br/></div><div>'+(v)+'</div>'
                        }
                    }
                }
                loginModel.errMsg(processedErrorMsg);
                loginModel.isErr(true);
            }
        }
        ko.applyBindings(loginModel,$('#loginForm')[0]);
        $(document).on('keypress', function(e) {
            if(e != undefined && e != null && e != ''){
                if(e.keyCode == 13){
                    var tag = e.target.tagName.toLowerCase();
                    if(tag != undefined && tag != null && tag != '' && tag == 'input'){
                        loginModel.doCheckAndLogin();
                    }
                }
            }
        });
    });
</script>
[/#if]
