[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
[#include "/partner-portal-mflg/templates/function/objectToJsonFunction.ftl"]

<style>
    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        /*background: #F5F5F5;*/
    }
    /*remove row  background */
    .k-alt, .k-separator {
        background-color: #FFFFFF;
    }
    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-grid .k-hierarchy-col {
        width: 1px;
    }
    .k-grid td {
        word-wrap: break-word;
        border-width: 0 0 1px 1px;
    }

    element.style {
        padding-right: 0px;
    }

    td.k-detail-cell {
        padding-left: 3.69cm;
        padding-right: 1.3cm;
        padding-top: 0cm;
        padding-bottom: 2px;
    }
    .sub-detail.k-grid.k-widget> .k-grid-header{
        display: none;
    }
    .k-grid-content {
        overflow-y: hidden;
    }
    td.k-detail-cell {
        opacity: 0.6; /* Real browsers */
        filter: alpha(opacity = 60); /* MSIE */
    }
    .k-grid-header {
        padding-right: 0px;
    }
    .action-container {
        font-size: 1.4em;
        text-align: center;
        margin: 0px;
        margin-top: 10px;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }

    .bundled-items-content ul, .bundled-items-content li {
        list-style: none;
        padding: 0;
        margin: 0;
    }
</style>

<div class="main-content-section-container">
    <h2 class="heading">View Inventory</h2>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section">
        <div class="filter-section background">
            <form id="inventoryform" action="searchInventory">
                <table cellpadding="0" cellspacing="0" width="100%"
                       class="filter-section-content">
                    <tr>
                        <th colspan="4" width="48%">Tickets Validity Date</th>
                        <th width="20%">Search Product</th>
                        <th width="2%">&nbsp;</th>
                        <th width="12%">&nbsp;</th>
                        <th width="8%">&nbsp;</th>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><input id="startDate"
                                   name="startDateStr" type="text"
                                   class="input-textarea"
                                   value=""></td>
                        <td>To</td>
                        <td><input id="endDate"
                                   name="endDateStr" type="text"
                                   class="input-textarea"
                                   value=""></td>
                        <td><input id="productName"
                                   name="productName" type="text"
                                   class="input-textarea"
                                   value=""></td>
                        <td><input id="availableOnly"
                                   name="availableOnly" type="checkbox"
                                   value="on" /></td>
                        <td>Show Available<BR>Items Only</td>
                    </tr>
                </table>
                <div class="action-container">
                    <input id="btnfilter" class="btn btn-primary thin-btn"
                           type="button" value="Filter">
                    <input class="btn btn-third thin-btn" type="reset"
                           value="Reset Filters">
                </div>
                </s:form>
        </div>
        <form id="inventoryGridform" action="exportInventoryItems">
            <div id="bundledNoteDv" class="bundled-note" style="display: none;">
                <span id="itemNumNote" class="hightlight-remain-orange">&nbsp;</span> items selected, Quantity Per Product: <span id="pkgQtyNote" class="hightlight-remain-orange">&nbsp;</span>, Expiry Date: <span  id="expDateStrNote" class="hightlight-remain-orange">&nbsp;</span>, Ticket Type: <span id="pkgTypeNote" class="hightlight-remain-orange">&nbsp;</span>
            </div>
            <div id="inventoryGrid">
            </div>

            <div class="export-to-excel-section" style="display: none;" >
                <a type="button" id="btnExportToExcel"
                   class="btn btn-secondary">Export to Excel</a>
            </div>



        </form>
    </div>

    <!-- mix&match package div start -->
    <div  id = 'itemdv' class="create-package-section" style="display: none;" >
        <h3 class="primary-heading-bar">Create New Package</h3>
        <table cellpadding="0" cellspacing="0" width="100%" class="background information-section">
            <tr><td colspan="7">&nbsp;</td></tr>
            <tr>
                <td width="5%">&nbsp;</td>
                <td width="15%">Package Name:</td>
                <td width="25%"><input type="text" class="input-textarea" data-bind="value: pkgName" readonly/></td>
                <td width="5%">&nbsp;</td>
                <td width="20%">Expiry Date:</td>
                <td width="25%"><span data-bind="text: pkgExpiryDateStr" ></span></td>
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="5%">&nbsp;</td>
                <td width="15%" rowspan="2">Description:</td>
                <td width="25%" rowspan="2"><textarea rows="4" class="input-textarea" data-bind="value: pkgDesc" ></textarea></td>
                <td width="5%">&nbsp;</td>
                <td width="20%">Ticket Type:</td>
                <td width="25%">
                    <select class="select-options" data-bind="options: ticketTypes,
                                                       optionsText: 'display',
                                                       optionsValue: 'id',
                                                       value: pkgTktType,
                                                       event: {change: onTypeChange},
                        optionsCaption: 'Choose...'">
                    </select>
                </td>
                <td width="5%">&nbsp;</td>
            </tr>

            <tr>
                <td width="5%">&nbsp;</td>
                <td width="5%">&nbsp;</td>
                <td width="20%">Ticket Media:</td>
                <td width="25%">
                    <select class="select-options" data-bind="options: ticketMedias,
                                                       optionsText: 'display',
                                                       optionsValue: 'id',
                                                       value: pkgTktMedia,
                                                       event: {change: onTktMediaChange},
                                                       optionsCaption: 'Choose...'">
                    </select>
                </td>
                <td width="5%">&nbsp;</td>
            </tr>

            <tr>
                <td width="5%">&nbsp;</td>
                <td width="20%">Quantity Per Product:</td>
                <td width="25%"><input type="text" class="input-textarea" data-bind="value: pkgQtyPerProd"></td>
                <td width="5%">&nbsp;</td>
                <td width="20%"><span data-bind="visible: showPkgGroupTicket">Group Ticket:</span></td>
                <td width="25%">
                    <select class="select-options" data-bind="options: groupTickets,
                                                       optionsText: 'display',
                                                       optionsValue: 'id',
                                                       value: pkgGroupTicket,
                                                       visible: showPkgGroupTicket,
                                                       optionsCaption: 'Choose...'">
                    </select>
                </td>
                <td width="5%">&nbsp;</td>
            </tr>

            <tr><td colspan="7">&nbsp;</td></tr>
        </table>
        <div class="bundled-items-section">
            <h3 class="heading">Bundled Items</h3>
            <div class="bundled-items-content">
                <ul data-bind="foreach: bundledItems">

                    <li>
                        <h4 class="primary-heading-bar center"><span data-bind="text: displayTitle" ></span><a class="btn-close" data-bind="click: $parent.rmBundleItem"><i class="fa fa-times"></i></a></h4>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr class="forth-multi-heading-bar">
                                <th width="40%">Receipt Number</th>
                                <th width="20%">Quantity</th>
                                <th width="35%" class="center">Validity End Date</th>
                                <th width="5%" class="center">&nbsp;</th>
                            </tr>
                            <tbody data-bind="foreach: tranItems">
                            <tr  class="main-inner-items">
                                <td class="link-text" data-bind="text: receiptNum"></td>
                                <td class="center" data-bind="text: packagedQty"></td>
                                <td class="center" data-bind="text: validityEndDateStr"></td>
                                <td class="center"><a class="icon-remove" data-bind="click: $parent.rmTransItem"><i class="fa fa-times"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div data-bind="foreach: topupItems">
                            <div class="bundled-items-topup background"><i class="fa fa-plus-circle"></i><span data-bind="text: displayName" ></span></div>
                        </div>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- mix&match package div end -->
    </div>
    <div class="action-container">
        <input id="btnMixMatch" type="button" class="btn btn-primary" value="Create Mix &amp; Match Package" />
    </div>
</div>
<script type="text/x-kendo-template" id="template">
    <div class="sub-detail"></div>
</script>