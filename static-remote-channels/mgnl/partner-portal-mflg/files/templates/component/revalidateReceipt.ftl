<div class="main-content-section-container" id="revalidateReceiptView">
    <div class="breadcrumb">
        <h2 class="heading">View Inventory</h2>
        <a class="link-text" data-bind="attr: {href: mainReceiptLink}">Back to main receipt</a>
    </div>
    <div class="main-content-section">
        <div class="information-section-container">
            <h4 class="primary-heading-bar">Revalidation Tickets</h4>
            <table cellpadding="0" cellspacing="0" width="100%"
                   border="0" class="pincode-section">
                <tr>
                    <td width="20%">Receipt Status:</td>
                    <td width="80%" class="status-attribute" data-bind="text: status"></td>
                </tr>
                <tr>
                    <td width="20%">Receipt Number:</td>
                    <td width="80%" class="pincode-attribute" data-bind="text: receiptNum"></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%"
                   border="0" class="information-section">
                <tr>
                    <td width="20%">Partner Code:</td>
                    <td width="25%" data-bind="text: accountCode"></td>
                    <td width="5%">&nbsp;</td>
                    <td width="20%">&nbsp;</td>
                    <td width="30%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td data-bind="text: orgName"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Current Validity Start Date:</td>
                    <td data-bind="text: validateStartDateStr"></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td data-bind="text: address"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Current Validity End Date:</td>
                    <td data-bind="text: validityEndDateStr"></td>
                </tr>
            </table>

            <table cellpadding="0" cellspacing="0" width="50%"
                   border="0"
                   class="information-section updated-information-section">
                <tr>
                    <td>Number of Months To Exend:<br><span
                            class="date-note">(starting from expired date)</span></td>
                    <td><span data-bind="text: extendRevalMonth"></span>&nbsp;months</td>
                </tr>
                <tr>
                    <td>New Validity Period:<br><span
                            class="date-note">(starting from expired date)</span></td>
                    <td><span data-bind="text: newvalidateStartDateStr"></span> to <span data-bind="text: newvalidityEndDateStr"></span></td>
                </tr>
                <tr>
                    <td>Revalidation Fee Per Ticket:</td>
                    <td data-bind="text: normalRevFeeStr"></td>
                </tr>
                <#--<tr data-bind="visible: topupRevFeeStr != '$ 0.00'">-->
                    <#--<td>Revalidation Fee Per Topup Ticket:</td>-->
                    <#--<td data-bind="text: topupRevFeeStr"></td>-->
                <#--</tr>-->
            </table>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%"
               class="items-tickets-content-container package-items"
               border="0">
            <tr class="forth-multi-heading-bar">
                <td width="25%">Product</td>
                <td width="25%" class="center">Revalidation Fee Per Ticket</td>
                <td width="25%" class="center">Quantity</td>
                <td width="25%" class="center">Amount</td>
            </tr>
            <!-- ko foreach: receiptitems -->
                 <tr data-bind="css: {'main-items': !itemTopup(), 'top-up-items': itemTopup()}">
                    <td width="25%">
                        <!-- ko if:itemTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                        <span data-bind="text: displayName"></span>
                    </td>
                    <td width="25%" class="center" data-bind="text: singleRevalFeeFullStr"></td>
                    <td width="25%" class="center" data-bind="text: unpackedQty"></td>
                    <td width="25%" class="center" data-bind="text: revalFeeFullStr"></td>
                </tr>
            <!-- /ko -->
        </table>

        <table cellpadding="0" cellspacing="0" width="100%"
               class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="30%">GST <span data-bind="text: revalGSTRateStr"></span>:</td>
                <td class="center" width="20%"><span data-bind="text: currency"></span>&nbsp;<span data-bind="text: revalGSTStr"></span></td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="30%">Total Amount (excl. GST <span data-bind="text:revalGSTRateStr"></span>):</td>
                <td class="center" width="20%"><span data-bind="text: currency"></span>&nbsp;<span data-bind="text: revalExclGSTStr"></span></td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title"
                    colspan="2" width="30%">Grand Total:</td>
                <td class="center highlight-attribute" width="20%">
                    <span data-bind="text: currency"></span>&nbsp;<span data-bind="text: totalRevalFeeStr"></span>
                </td>
            </tr>
        </table>


        <div class="terms-conditions">
            <label for="agreeTnc">
                <input type="checkbox" id="agreeTnc"> <span class="star">*</span>
                I have read and agree to both the <a href="tnc" target="_blank" class="link-text">Terms and Conditions</a> as set out in the Sentosa Online Store
                and Sentosa Leisure Management Pte Ltd's ("SLM") <a href="data-protection-policy" target="_blank" class="link-text">Data Protection Policy</a>,
                including as to how my personal data may be collected, used, disclosed and processed, and confirm that
                all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself,
                I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents,
                to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out
                in the Data Protection Policy.
            </label>
        </div>
        <div class="action-container">
            <a class="btn btn-third" data-bind="attr: {href: mainReceiptLink}">Cancel</a>
            <input id="revalReceiptDo" type="button" class="btn btn-primary" value="Pay By credit card" />
        </div>
    </div>
</div>
<div class=" clearfix"></div>