[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<style>
    .deposit-number {
        text-align: right;
        font-weight: bold;
        font-size: 1.1em;
    }

    .deposit-total-amount {
        font-size: 1.5em;
        color: red;
    }
</style>

<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action> .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-i-close{
        display: inline-block;
        background-image: url("/resources/${channel}/theme/default/img/close.png");

    }
    .k-grid td {
        word-wrap: break-word;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }

    .picker {
        z-index: 100000;
    }

    .depositBalance {
        float: right;
        border: 2px solid #13669b;
        padding: 10px;
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: 10px;
    }

</style>
<div class="main-content-section-container">
    <h2 class="heading">Deposit Top Up</h2>
    <div class="main-content-section">
        <div class="items-records-container">
            <h4 class="primary-heading-bar">Deposit Top Up Details</h4>
            <div id="errordv" class="error-section" style="display: none;" ></div>
            <table cellpadding="0" cellspacing="0" width="100%"
                   class="information-section" id="depositBalanceDiv">
                <tr>
                    <td width="20%">Deposit Balance:</td>
                    <td width="60%" data-bind="text: depositBalance">
                    <td width="20%">&nbsp;</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%"
                   class="information-section" id="creditPurchaseForm">
                <tr>
                    <td width="20%">Add Deposit Amount:</td>
                    <td width="60%">
                        S$<input type="text" class="input-text" style="width: 20%; text-align: right" data-bind="value: depositAmount"></td>
                    <td width="20%">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center">
                        <input id="btnProcceed" class="btn btn-primary thin-btn" type="button" value="Proceed to Payment > " data-bind="click: proceedToPayment"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="confirmMsgWindow" style="display: none;">
    <div><br/></div>
    <div style="text-align: center" data-bind="text: confirmMessage"></div>
    <div><br/></div>
    <div><br/></div>
    <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
        <a type="button" id="btnDoubleConfirmPerform"
           class="btn btn-primary" data-bind="click: confirmModal">&nbsp;&nbsp;Confirm&nbsp;&nbsp;</a>
        <a type="button" id="btnDoubleConfirmCancel"
           class="btn btn-third" data-bind="click: cancelModal">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</a>
    </div>
    <div class=" clearfix"></div>
</div>