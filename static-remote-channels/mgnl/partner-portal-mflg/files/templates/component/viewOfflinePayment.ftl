<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action> .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/img/close.png");
    }
    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/img/close.png");
    }
    .k-i-close{
        display: inline-block;
        background-image: url("/img/close.png");

    }
    .k-grid td {
        word-wrap: break-word;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }
</style>
<div class="main-content-section-container">
    <h2 class="heading">Offline Payment</h2>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section">
        <div class="filter-section background">
            <form id="filterDv" action="">
                <table cellpadding="0" cellspacing="0" width="100%"
                       class="filter-section-content">
                    <tr>
                        <th width="18%">Order Reference No.:</th>
                        <th colspan="4" width="40%">Payment Created Date:</th>
                        <th width="15%">Status:</th>
                        <th width="17%">Created By:</th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                    <tr>
                        <td><input type="text" class="input-textarea" data-bind="value: receiptNumString"/></td>
                        <td>From</td>
                        <td><input id="startDate" type="text" class="input-textarea" data-bind="value: startDtStr"></td>
                        <td>To</td>
                        <td><input id="endDate" type="text" class="input-textarea" data-bind="value: endDtStr"></td>
                        <td>
                            <select data-bind="options: statusList,
                               optionsText: 'name',
                               optionsValue: 'code',
                               value: status,
                               optionsCaption: 'Choose...'">
                            </select>
                        </td>
                        <td><input type="text" class="input-textarea" data-bind="value: username"/></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <div class="action-container">
                    <input   class="btn btn-primary thin-btn" type="button" value="Filter" data-bind="click: filter">
                    <input   class="btn btn-third thin-btn" type="button" value="Reset Filters" data-bind="click: resetFilters">
                </div>
            </form>
        </div>
        <div class="items-records-container">
            <div id="pkgGrid">
            </div>
        </div>
    </div>
</div>
<div class=" clearfix"></div>
<div id="vPkgWindow" style="display: none;">
</div>