<style>
    .deposit-number {
        text-align: right;
        font-weight: bold;
        font-size: 1.1em;
    }

    .deposit-total-amount {
        font-size: 1.5em;
        color: red;
    }
</style>
<div class="main-content-section-container">
    <div class="main-content-section">
        <div class="items-records-container">
            <div id="resultDiv" style="width:100%;">
                <div>
                    <br/>
                    <br/>
                </div>
                <div class="error-section" style="display: none; text-align: center;" data-bind="visible: errVisible">
                    <span data-bind="text: errMsg"></span>
                </div>
                <div style="display: none;" data-bind="visible: rsMsgVisible">
                    <h2 style="text-align: center;">
                        <span data-bind="text: rsMsg"></span>
                    </h2>
                </div>
            </div>
            <div>
                <br/>
            </div>
            <div style="width:100%;">
                <h3 style="text-align: center; display: none;" id="depositBalanceDiv">
                    Your Deposit Balance now is <b><span data-bind="text: depositBalance"></span></b>.
                </h3>
            </div>
            <div>
                <br/>
                <br/>
            </div>
            <div style="text-align: center;width: 100%;">
                <input id="btnDepositAgain" class="btn btn-primary thin-btn" type="button" value="Back to Deposit Top Up"/>
            </div>
        </div>
    </div>
</div>