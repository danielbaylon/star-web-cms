[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">
    <h2 class="heading">Account Management</h2>
    <div class="main-content-section">
        <div class="items-records-container" id="partnerProfileManagementDiv">
            <h4 class="primary-heading-bar">Edit Main Account</h4>
            <div class="error-section" style="display: none;" data-bind="visible: isErr, text: errMsg"></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="information-section">
                <tr>
                    <td width="40%">Main Account Login Name<span class="star">*</span>:</td>
                    <td width="50%" >
                        <span data-bind="text: username"></span>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Nature of Business:</td>
                    <td><span data-bind="text: lineOfBusinessLabel"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Registered Country:</td>
                    <td><span data-bind="text: countryRegionLabel"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Account Status:</td>
                    <td><span data-bind="text: status"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Organization Name:</td>
                    <td><span data-bind="text: orgName"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Unique Entity Number(UEN):</td>
                    <td><span data-bind="text: uen"></span></td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>Travel Agent License Number:</td>
                    <td><span data-bind="text: licenseNum"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Travel Agent License Expiry Date:</td>
                    <td><span data-bind="text: licenseExpDate"></span></td>
                    <td>&nbsp;</td>
                </tr>

                <tr id="partnerExistingDocSection" style="display:none;">
                    <td>Document Attachment:</td>
                    <td><table cellpadding="0" cellspacing="0" width="100%" class="information-section" id="prevFileList"></table></td>
                    <td>&nbsp;</td>
                </tr>
                <!--address, begin -->
                <tr>
                    <td>Business Registered Address<span class="star">*</span>:</td>
                    <td><input type="text"  data-bind="value: address" class="input-textarea" maxlength="60"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Business Registered Postal Code<span class="star">*</span>:</td>
                    <td><input type="text"  data-bind="value: postalCode" class="input-textarea" maxlength="10"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Business Registered City:</td>
                    <td><input type="text"  data-bind="value: city" class="input-textarea" maxlength="60"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Correspondence Address<span class="star">*</span>:</td>
                    <td>
                        <div><input type="checkbox" data-bind="checked: useBizAddrAsCorrespAddr, click : applyBizAddrAsCorrespAddr"/>&nbsp;Same as business registered address</div>
                        <div>
                            <input type="text"  data-bind="value: correspondenceAddress, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="60" />
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Correspondence Postal Code<span class="star">*</span>:</td>
                    <td><input type="text"  data-bind="value: correspondencePostalCode, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="10"/></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Correspondence City:</td>
                    <td><input type="text"  data-bind="value: correspondenceCity, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="60"></td>
                    <td>&nbsp;</td>
                </tr>
                <!--address, end-->
                <tr>
                    <td>Contact Person<span class="star">*</span>:
                    </td>
                    <td><input type="text" data-bind="value: contactPerson" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Designation or Position<span class="star">*</span>:
                    </td>
                    <td><input type="text" data-bind="value: contactDesignation" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Office No.<span class="star">*</span>:
                    </td>
                    <td><input type="text"  data-bind="value: telNum" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Mobile No.<span class="star">*</span>:
                    </td>
                    <td><input type="text"  data-bind="value: mobileNum" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Fax No.:</td>
                    <td><input type="text"  data-bind="value: faxNum" class="input-textarea" maxlength="20"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><span data-bind="text: email"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Website (if Available):</td>
                    <td><input type="text"  data-bind="value: website" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Key market / Country<span class="star">*</span>:</td>
                    <td><input type="text"  data-bind="value: mainDestinations" class="input-textarea" maxlength="50"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="inner-action-container">
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <button type="submit" class="btn btn-primary" data-bind="click: doSave">Save</button>
                        <button type="button" data-bind="click: doCancel" class="btn btn-third">Cancel</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>