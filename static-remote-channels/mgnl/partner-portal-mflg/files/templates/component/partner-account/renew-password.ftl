[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<script type="text/javascript">
    var errMsg = null;
    var warnMsg = null;
</script>
[#assign httpRequest = ctx.getRequest() ]
[#if httpRequest?? && httpRequest?has_content && httpRequest.getAttribute("renewRequired")?? ]
    [#assign renewRequired = httpRequest.getAttribute("renewRequired") ]
[/#if]
[#if httpRequest?? && httpRequest?has_content && httpRequest.getAttribute("error-message")?? ]
    [#assign errorMsg = httpRequest.getAttribute("error-message") ]
[/#if]
[#if httpRequest?? && httpRequest?has_content && httpRequest.getAttribute("warning-message")?? ]
    [#assign warningMsg = httpRequest.getAttribute("warning-message") ]
[/#if]
[#if warningMsg?? && warningMsg?has_content ]
<script type="text/javascript">
    warnMsg = ${warningMsg!""};
</script>
[/#if]
[#if errorMsg?? && errorMsg?has_content ]
<script type="text/javascript">
    errMsg ='${errorMsg!""}';
</script>
[/#if]
<div class="main-content-section-container">
    <h2 class="heading">Account Management</h2>
    <div class="main-content-section">
        <form method="post" action="${API_PREFIX}/secured/partner-account/renew-password" class="form-horizontal" id="renew-password-form">
            <div class="items-records-container">
                <h4 class="primary-heading-bar">Change Password</h4>
                <div class="guideline">
                    <h3 class="heading">Guidelines:</h3>
                    - Password must be at least of eight(8) characters.
                    <br>
                    - Password cannot have any leading or trailing white space.
                    <br>
                    - Password should contain a mixture of upper case letters, lower case letters, numeric characters and special characters.
                    <br>
                    - Password cannot be the same as the past 5 passwords for the same account.
                    <br>
                    - Password used must be changed at least once every 90 days.
                </div>
                <div class="error-section" style="display: none;" data-bind="visible: isErr, text: errMsg"></div>
                <table cellpadding="0" cellspacing="0" width="100%" class="information-section">
                    <tr>
                        <td width="20%">Current Password
                            <span class="star">*</span>:
                        </td>
                        <td width="40%">
                            <input class="input-textarea" type="password" name="pw" placeholder="Current Password" data-bind="value: pw">
                        </td>
                        <td width="40%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>New Password
                            <span class="star">*</span>:
                        </td>
                        <td>
                            <input class="input-textarea" type="password" name="newPw" placeholder="New Password" data-bind="value: newPw">
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Retype New Password
                            <span class="star">*</span>:
                        </td>
                        <td>
                            <input class="input-textarea" type="password" name="newPw2" placeholder="New Password" data-bind="value: newPw2">
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="inner-action-container">
                        <td>&nbsp;</td>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary" data-bind="click: doRenew">Save</button>
                            [#if renewRequired??]
                                <button type="button" data-bind="click: doCancel" class="btn btn-third">Cancel</button>
                            [#else]
                                <button type="button" data-bind="click: doLogout" class="btn btn-third">Cancel</button>
                            [/#if]
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</div>