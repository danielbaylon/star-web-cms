[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]

<style type="text/css">
    .hi-tnc .the-tnc {
        margin-bottom: 30px;
        border-bottom: 1px solid #DADADA;
        padding-bottom: 10px;
    }

    .hi-tnc ul{
        list-style: disc;
        padding: 0;
        margin: 0;
        margin-left: 0.5em;
    }
    .hi-tnc ol {
        list-style: deciaml;
        padding: 0;
        margin: 0;
        margin-left: 0.5em;
    }
    .hi-tnc li {
        list-style:inherit;
        padding: 0;
        margin: 0;
        margin-left: 0.5em;
    }
</style>

<div class="main-content-section-container" id="tncDiv">

    <h2 class="heading">Terms &amp; Conditions</h2>


    <div class="main-content-section hi-tnc" style="text-align: justify; display:none" data-bind="visible: tncItem().length >0">
        <div class="the-tnc" style="font-family:Helvetica,Arial,sans-serif; font-size:12px; margin: 0; padding: 0;">
            <!-- ko foreach: tncItem -->
                <div style='text-decoration: underline;font-weight: bold;padding-top: 20px;' data-bind="text: title"></div>
                <div data-bind="html: content"></div>
            <!-- /ko -->
        </div>
    </div>
    <div class="main-content-section" style="text-align: center; margin: 5em 0; display:none" data-bind="visible: tncItem().length  == 0">
        <h3>No Terms &amp; Conditions available.</h3>
    </div>

</div>


