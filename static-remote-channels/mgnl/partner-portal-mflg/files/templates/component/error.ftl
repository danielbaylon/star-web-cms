[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]

<h2 class="heading" style="text-align: center; margin-top: 2em;">
    Uh oh, something unexpected occurred! Sorry for the inconvenience. Our tech team is working on this right now.
</h2>
<h3 style="text-align: center; margin-bottom: 3em">Go back to the <a href="/${channel}/dashboard">home page</a>!</h3>