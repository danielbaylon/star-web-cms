[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action> .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-i-close{
        display: inline-block;
        background-image: url("/resources/${channel}/theme/default/img/close.png");

    }
    .k-grid td {
        word-wrap: break-word;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }

    .depositBalance {
        float: right;
        border: 2px solid #13669b;
        padding: 10px;
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: 10px;
    }
</style>
<div class="main-content-section-container">
    <h2 class="heading">Deposit Management</h2>
    <div id="depositBalanceDiv">
        <div class="depositBalance">
            <span>
                Deposit Balance:&nbsp;&nbsp;&nbsp;
            </span>
            <span style="color: red" data-bind="text: depositBalance">
            </span>
        </div>

        <div style="clear:both"></div>
    </div>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section" id="depositManagementDiv">
        <div class="filter-section background">
            <form action="">
                <table cellpadding="0" cellspacing="0" width="100%"
                       class="filter-section-content">
                    <tr>
                        <th width="30%">Type</th>
                        <th colspan="5" width="70%">Inclusive Date(s):</th>
                    </tr>
                    <tr>
                        <td><input id="typeStr"
                                   name="typeStr" type="text"
                                   class="input-textarea"></td>
                        <td>From</td>
                        <td><input id="startDate"
                                   name="startDateStr" type="text"
                                   class="input-textarea"></td>
                        <td>To</td>
                        <td><input id="endDate" name="endDateStr"
                                   type="text" class="input-textarea"></td>
                        <td></td>
                    </tr>
                </table>
                <div class="action-container">
                    <input id="btnfilter" class="btn btn-primary thin-btn"
                           type="button" value="Filter">
                    <input id="btnReset" class="btn btn-third thin-btn" type="button"
                           value="Reset Filters">
                </div>
            </form>
        </div>
        <div class="items-records-container">
            <div style="text-align: right;">
                <input id="btnExportAsExcel" type="button" class="btn btn-third thin-btn" value="Export to Excel" style="margin-bottom: 0px; margin-top: 10px; text-align: right;"/>
            </div>
            <div>
                <div id="depositGrid"></div>
            </div>
        </div>
    </div>
</div>
<div class=" clearfix"></div>


<div style="display:none;">
    <form id="depositExportForm" method="post" action="${ctx.contextPath}${API_PREFIX!""}/secured/partner-deposit/export-to-excel" target="excel_export_iframe_target">
        <input type="hidden" name="startDateStr"            id="depositExportForm_startDateStr"   value="" />
        <input type="hidden" name="endDateStr"              id="depositExportForm_endDateStr"     value="" />
        <input type="hidden" name="depositTxnQueryType"     id="depositExportForm_depositTxnQueryType"   value="" />
    </form>
</div>