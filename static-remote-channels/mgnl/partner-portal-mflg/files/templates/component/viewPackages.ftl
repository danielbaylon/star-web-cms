[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action> .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-i-close{
        display: inline-block;
        background-image: url("/resources/${channel}/theme/default/img/close.png");

    }
    .k-grid td {
        word-wrap: break-word;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }
</style>
<div class="main-content-section-container">
    <h2 class="heading">View Packages</h2>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section">
        <div class="filter-section background">
            <form action="">
                <table cellpadding="0" cellspacing="0" width="100%"
                       class="filter-section-content">
                    <tr>
                        <th colspan="4" width="50%">Ticket Generated Date</th>
                        <th width="5%">&nbsp;</th>
                        <th width="20%">Ticket Media</th>
                        <th width="15%">Status</th>
                        <th width="10%">&nbsp;</th>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><input id="startDate"
                                   name="startDateStr" type="text"
                                   class="input-textarea"></td>
                        <td>To</td>
                        <td><input id="endDate" name="endDateStr"
                                   type="text" class="input-textarea"></td>

                        <td>&nbsp;</td>
                        <td>
                            <select id="ticketMedia">
                                <option value="">Choose...</option>
                                <option value="Pincode">PINCODE</option>
                                <option value="ETicket">E-Ticket</option>
                                <option value="ExcelFile">Excel File</option>
                            </select>
                        </td>
                        <td>
                            <select id="status">
                                <option value="">Choose...</option>
                                <option value="Available">Available</option>
                                <option value="Expired">Expired</option>
                                <option value="Redeemed">Redeemed</option>
                            </select>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%"
                       class="filter-section-content">
                    <tr>
                        <th width="20%">Package Name</th>
                        <th><input id="pkgName" name="pkgName"
                                   type="text" class="input-textarea"></th>
                        <th width="10%">&nbsp;</th>
                    </tr>

                </table>
                <div class="action-container">
                    <input id="btnfilter" class="btn btn-primary thin-btn"
                           type="button" value="Filter">
                    <input class="btn btn-third thin-btn" type="reset"
                           value="Reset Filters">
                </div>
            </form>
        </div>
        <div class="export-to-excel-section">
            <a type="button" id="btnExportToExcel"
               class="btn btn-secondary">Export to Excel</a>
        </div>
        <div class="items-records-container">
            <div id="pkgGrid">
            </div>
        </div>
    </div>
</div>
<div class=" clearfix"></div>
<div id="vPkgWindow" style="display: none;">
    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="pincode-section" style="display:none" data-bind="visible: pkgTktMedia() == 'Pincode'">
        <tr>
            <td width="20%">PIN CODE:</td>
            <td width="80%" class="pincode-attribute" data-bind="text: pinCode"></td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
        <tr>
            <td width="20%">Booking Reference No:</td>
            <td width="80%" data-bind="text: bookingRefId"></td>
        </tr>
        <tr>
            <td width="20%">Description:</td>
            <td width="80%" data-bind="text: description"></td>
        </tr>
        <tr>
            <td width="20%">Redemption Status:</td>
            <td width="80%" data-bind="text: status"></td>
        </tr>
        [#--<tr>--]
            [#--<td width="20%">Quantity Redeemed:</td>--]
            [#--<td width="80%" data-bind="text: qtyRedeemedStr"></td>--]
        [#--</tr>--]
        <tr>
            <td width="20%">Generated Date Time:</td>
            <td width="80%" data-bind="text: ticketGeneratedDateStr"></td>
        </tr>
        <tr>
            <td width="20%">Package Expiry Date:</td>
            <td width="80%" data-bind="text: expiryDateStr"></td>
        </tr>
        <tr>
            <td width="20%">Last Redemption Date Time:</td>
            <td width="80%" data-bind="text: lastRedemptionDateStr"></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%" class="items-tickets-content-container package-items" border="0">
        <tr class="forth-multi-heading-bar">
            <td width="30%">Product</td>
            <td width="20%">Ticket Type</td>
            <td width="10%">Quantity</td>
            <td width="10%">Quantity<br/>Redeemed</td>
            <td width="30%" class="center">Receipt Number</td>
        </tr>
        <!-- ko foreach: transItems -->
        <tr class="main-items" data-bind="css: {odd: $index()%2==1, even: $index()%2==0}">
            <td width="30%">
                <span data-bind="text: displayName">
                </span>
                <span data-bind="html: displayDetails">
                </span>
            </td>
            <td width="20%" data-bind="text: ticketType"></td>
            <td width="10%" data-bind="text: qty "></td>
            <td width="10%" data-bind="text: qtyRedeemed "></td>
            <td width="30%" class="center" data-bind="text: receiptNum"></td>
        </tr>
            <!-- ko foreach: topupItems -->
            <tr style="border-bottom: 1px solid #e5e5e5">
                <td><i class="fa fa-plus-circle"></i><span data-bind="text: displayName"></span></td>
                <td>&nbsp;</td>
                <td style="padding: 0.8em" data-bind="text: pkgQty"></td>
                <td style="padding: 0.8em" data-bind="text: qtyRedeemed "></td>
                <td >&nbsp;</td>
            </tr>
            <!-- /ko -->
        <!-- /ko -->
    </table>
    <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
        <a type="button" id="downloadTicket"
           class="btn btn-secondary" data-bind="visible:  pkgTktMedia() == 'ETicket'" style="display:none">Download E-Ticket</a>
        <a type="button" id="emailTicket"
           class="btn btn-primary" data-bind="visible:  pkgTktMedia() == 'ETicket', click: sendEmailTicket" style="display:none">Email E-Ticket</a>
        <a type="button" id="emailPincode"
           class="btn btn-primary" data-bind="visible:  pkgTktMedia() == 'Pincode', click: sendEmailPincode" style="display:none">Email PIN Code</a>
        <a type="button" id="downloadExcelTicket"
           class="btn btn-secondary" data-bind="visible:  pkgTktMedia() == 'ExcelFile'" style="display:none">Download Excel File</a>
        <a type="button" id="emailExcelTicket"
           class="btn btn-primary" data-bind="visible:  pkgTktMedia() == 'ExcelFile', click: sendEmailExcel" style="display:none">Email Excel File</a>
    </div>
    <div class=" clearfix"></div>
</div>
<script type="text/x-kendo-template" id="template">
    <div class="sub-detail"></div>
</script>