[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<style>
    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0.8em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11.9px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11.9px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11.9px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-icon,.k-sprite,.k{
        width: 40px;
        height: 50px;
        background-image: none;
    }

    .k-icon,.k-sprite{
        background: #0c9d62;
        background-repeat: no-repeat;
        background-position:8px 15px;
    }

    .k-i-close{
        display: inline-block;
        background-image: url("/img/close.png");

    }

    .k-link:not(.k-state-disabled):hover>.k-i-close,.k-link:not(.k-state-disabled):hover>.k-delete,.k-link:not(.k-state-disabled):hover>.k-group-delete,.k-state-hover .k-i-close,.k-state-hover .k-delete,.k-state-hover .k-group-delete,.k-button:hover .k-i-close,.k-button:hover .k-delete,.k-button:hover .k-group-delete,.k-textbox:hover .k-i-close,.k-textbox:hover .k-delete,.k-textbox:hover .k-group-delete,.k-button:active .k-i-close,.k-button:active .k-delete,.k-button:active .k-group-delete{
        background-position:8px 15px;
    }

</style>

<div class="main-content-section-container" id="receiptView">
    <div class="breadcrumb">
        <h2 class="heading">View Inventory</h2>
        <a href="/${channel}/view-inventory" class="link-text">Back to view inventory</a>
    </div>
    <div class="receipt-section">
        <h3 class="heading center">Receipt Number: <span data-bind="text: receiptNumber"></span></h3>
        <p>Date Of Purchase: <span data-bind="text: createdDateStr"></span></p>
    </div>

    <div class="main-content-section">
        <div class="information-section-container">
            <h4 class="primary-heading-bar">Receipt Information</h4>
            <table cellpadding="0" cellspacing="0" width="100%"
                   border="0" class="information-section">
                <tr>
                    <td width="20%">Partner Code:</td>
                    <td width="25%" data-bind="text: accountCode"></td>
                    <td width="5%">&nbsp;</td>
                    <td width="20%">&nbsp;</td>
                    <td width="30%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td data-bind="text: orgName"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Validity Start Date:</td>
                    <td data-bind="text: validityStartDateStr"></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td data-bind="text: address"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Validity End Date:</td>
                    <td data-bind="text: validityEndDateStr"></td>
                </tr>
                <tr>
                    <td>Username:</td>
                    <td data-bind="text: username"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Receipt Status:</td>
                    <td style="white-space: nowrap;">
                        <span data-bind="text: displayStatus"></span>
                        <a class="btn btn-secondary" data-bind="visible: allowRevalidation, attr: {href: revalidationLink}" style="display:none">Revalidate</a>
                    </td>
                </tr>
                <tr>
                    <td>Transaction Status:</td>
                    <td data-bind="text: tmStatus"></td>
                    <td width="5%">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Payment Type:</td>
                    <td data-bind="text: paymentType"></td>
                    <td width="5%">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="information-section-container" data-bind="visible: showRevalidationTrans" style="display:none">
            <h4 class="secondary-heading-bar">Revalidation Information</h4>
            <table cellpadding="0" cellspacing="0" width="100%"
                   border="0" class="information-section">
                <tr>
                    <td width="25%">Revalidation Date/Time:</td>
                    <td width="20%" data-bind="text: revalTransaction.tmStatusDateStr"></td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td width="25%">Revalidation Receipt Number:</td>
                    <td width="20%" data-bind="text: revalTransaction.receiptNum"></td>
                    <td colspan="2">
                        <input type="button"
                               id="btnViewRevalReceipt"
                               class="btn btn-secondary"
                               value="View Revalidate Receipt" />
                        <input type="button" id="btnRegenRevalReceipt"
                               class="btn btn-secondary"
                               value="Regenerate Revalidation Receipt" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>

        <div class="items-details">

            <!-- ko foreach: products -->
            <h3 class="heading" data-bind="text: title"></h3>
            <table cellpadding="0" cellspacing="0" width="100%"
                   border="0">
                <tr class="forth-multi-heading-bar">
                    <td width="35%">Product</td>
                    <td width="15%" class="center">Price</td>
                    <td width="18%" class="center">Original Quantity</td>
                    <td width="18%" class="center">Remaining Quantity</td>
                    <td width="14%" class="center">Amount</td>
                </tr>
                <!-- ko foreach: items -->
                <tr data-bind="css: {'main-items': !topup(), 'top-up-items': topup()}">
                    <td width="45%">
                        <!-- ko if:topup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                        <span data-bind="text:title" class="product-item-name"></span>
                        <span data-bind="html: description"></span>
                    </td>
                    <td width="30%" class="center"><span data-bind="text: ticketType"></span> - $<span data-bind="text: priceText"></span></td>
                    <td class="center" data-bind="text: qty"></td>
                    <td class="hightlight-remain-blue center" data-bind="text: unpackagedQty"></td>
                    <td class="center">S$<span data-bind="text: totalText"></span></td>
                </tr>
                <!-- /ko -->
                <tr class="subtotal-items main-inner-items">
                    <td colspan="2" width="50%">&nbsp;</td>
                    <td class="subtotal-items-title" colspan="2"
                        width="36%">Subtotal:</td>
                    <td class="center" width="14%">S$<span data-bind="text: subtotalText"></span></td>
                </tr>
            </table>
            <!-- /ko -->

        </div>

        <table cellpadding="0" cellspacing="0" width="100%"
               class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="36%">GST <span data-bind="text: gstRateStr"></span>:</td>
                <td class="center" width="14%">S$ <span data-bind="text: gstText"></span></td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="36%">Total Amount (excl. GST <span data-bind="text: gstRateStr"></span>):</td>
                <td class="center" width="14%">S$ <span data-bind="text: totalNoGstText "></span></td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title"
                    colspan="2" width="36%">Grand Total:</td>
                <td class="center highlight-attribute" width="14%">S$<span data-bind="text: grandTotalText"></span></td>
            </tr>
        </table>
        <div class="terms-conditions">
            View this receipt's <a href="/${channel}/tnc" target="_blank">Terms &amp; Conditions.</a>
        </div>
        <div class="action-container">
            <input type="button" id="btnRegenReceipt" class="btn btn-secondary" value="Regenerate Receipt"/>
            <input type="button" id="btnRegenPIN" class="btn btn-secondary" value="Generate PIN CODE" style="display: none;" />
        </div>
    </div>

    <div class=" clearfix"></div>
</div>

<div id="revalWindow" style="display: none;">
    <div class="main-content-section-container" id="revalReceiptView">
        <div class="receipt-section">
            <h3 class="heading center" data-bind="text: 'Receipt Number: ' + revalReceiptNum()"></h3>
        </div>

        <div class="main-content-section">
            <div class="information-section-container">
                <table cellpadding="0" cellspacing="0" width="100%" border="0"
                       class="information-section">
                    <tr>
                        <td width="20%">Partner Code:</td>
                        <td width="25%" data-bind="text: accountCode"></td>
                        <td width="5%">&nbsp;</td>
                        <td>Validity Start Date:</td>
                        <td data-bind="text: validateStartDateStr"></td>
                    </tr>
                    <tr>
                        <td>Company:</td>
                        <td data-bind="text: orgName"></td>
                        <td width="5%">&nbsp;</td>
                        <td>Validity End Date:</td>
                        <td data-bind="text: validityEndDateStr"></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td data-bind="text: address"></td>
                        <td width="5%">&nbsp;</td>
                        <td>Revalidation Date/Time:</td>
                        <td data-bind="text: revalidateDtTmStr"></td>
                    </tr>
                    <tr>
                        <td>Username:</td>
                        <td data-bind="text: revalidatedBy"></td>
                        <td width="5%">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Transaction Status:</td>
                        <td data-bind="text: tmStatus"></td>
                        <td width="5%">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Payment Type:</td>
                        <td data-bind="text: revalPaymentType"></td>
                        <td width="5%">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <table cellpadding="0" cellspacing="0" width="100%"
                   class="items-tickets-content-container package-items"
                   border="0">
                <tr class="forth-multi-heading-bar">
                    <td width="25%">Product</td>
                    <td width="25%" class="center">Revalidation Fee Per Ticket</td>
                    <td width="25%" class="center">Quantity</td>
                    <td width="25%" class="center">Amount</td>
                </tr>
                <!-- ko foreach: receiptitems -->
                    <tr data-bind="css: {'main-items': !itemTopup(), 'top-up-items': itemTopup()}">
                        <td width="25%">
                            <!-- ko if:itemTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                            <span data-bind="text: displayName"></span>
                        </td>
                        <td width="25%" class="center" data-bind="text: singleRevalFeeFullStr"></td>
                        <td width="25%" class="center" data-bind="text: unpackedQty"></td>
                        <td width="25%" class="center" data-bind="text: revalFeeFullStr"></td>
                    </tr>
                <!-- /ko -->
            </table>

            <table cellpadding="0" cellspacing="0" width="100%"
                   class="total-account-section-container">
                <tr class="subtotal-items main-inner-items">
                    <td colspan="2" width="50%">&nbsp;</td>
                    <td class="subtotal-items-title" colspan="2"
                        width="30%">GST <span data-bind="text: revalGSTRateStr"></span>:</td>
                    <td class="center" width="20%"><span data-bind="text: currency"></span>&nbsp;<span data-bind="text: revalGSTStr"></span></td>
                </tr>
                <tr class="total-items subtotal-items">
                    <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                    <td class="subtotal-items-title" colspan="2"
                        width="30%">Total Amount (excl. GST <span data-bind="text:revalGSTRateStr"></span>):</td>
                    <td class="center" width="20%"><span data-bind="text: currency"></span>&nbsp;<span data-bind="text: revalExclGSTStr"></span></td>
                </tr>
                <tr class="main-items subtotal-items">
                    <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                    <td class="subtotal-items-title highlight-title"
                        colspan="2" width="30%">Grand Total:</td>
                    <td class="center highlight-attribute" width="20%">
                        <span data-bind="text: currency"></span>&nbsp;<span data-bind="text: totalRevalFeeStr"></span>
                    </td>
                </tr>
            </table>

            <div class="action-container">
                <input type="button" id="btnPopupRegenRevalReceipt"
                       class="btn btn-secondary"
                       value="Regenerate Revalidation Receipt" />
            </div>
        </div>
    </div>
    <div class=" clearfix"></div>
</div>

