[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
[#include "/partner-portal-mflg/templates/function/objectToJsonFunction.ftl"]
<style type="text/css">
    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 3px 0;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-grid .k-hierarchy-col {
        width: 1px;
    }
    .k-grid td {
        word-wrap: break-word;
    }

    element.style {
        padding-right: 0px;
    }

    td.k-detail-cell {
        padding-left: 3.69cm;
        padding-right: 1.3cm;
        padding-top: 0cm;
        padding-bottom: 2px;
    }
    .sub-detail.k-grid.k-widget> .k-grid-header{
        display: none;
    }
    .k-grid-content {
        overflow-y: hidden;
    }
    td.k-detail-cell {
        opacity: 0.6; /* Real browsers */
        filter: alpha(opacity = 60); /* MSIE */
    }
    .k-grid-header {
        padding-right: 0px;
    }
</style>

<div class="main-content-section-container" id="filterView">

    <h2 class="heading">Transaction Report</h2>

    <div class="alert alert-error" style="display: none; margin-bottom: 10px;" data-bind="visible: genIsErr, html: genErrMsg"></div>

    <div class="main-content-section">
        <div class="filter-section background">

            <table cellpadding="0" cellspacing="0" style="width: 100%; display: table; margin: 1em 0;" class="filter-section-content">
                <colgroup>
                    <col style="width: 5%">
                    <col style="width: 20%">
                    <col style="width: 20%">
                    <col style="width: 20%">
                    <col style="width: 20%">
                    <col style="width: 5%">
                </colgroup>
                <tr>
                    <td>&nbsp;</td>
                    <td>Transaction Date (From) <span class="required-ind">*</span></td>
                    <td>Transaction Date (To) <span class="required-ind">*</span></td>
                    <td>Payment Type</td>
                    <td>Transaction Status</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="text" class="input-large input-textarea" id="filterTdf" data-bind="value: filterTdf"></td>
                    <td><input type="text" class="input-large input-textarea" id="filterTdt" data-bind="value: filterTdt"></td>
                    <td>
                        <select class="input-large the-select" data-bind="value: filterPaymentType">
                            <option value="">All</option>
                            <option value="Online">Online</option>
                            <option value="Offline">Offline</option>
                        </select>
                    </td>
                    <td>
                        <select class="input-large the-select" data-bind="value: filterStatus" id="filterStatusDrpList">
                            <option value="All">All</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Receipt No.</td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><input type="text" class="input-large input-textarea" data-bind="value: filterReceiptNo"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Filter Using Items</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <section class="product-filter">
                            <!-- ko foreach: filterProds -->
                            <div class="prod-row">
                                <div class="prod-chk">
                                    <input type="checkbox" data-bind="checked: checked">
                                </div>
                                <div class="prod-content">
                                    <div class="prod-title"><strong data-bind="text: name">TEST PROD NAME</strong></div>
                                    <!-- ko foreach: items -->
                                    <div class="item-sect">
                                        <div class="item-chk"><input type="checkbox" data-bind="checked: checked"></div>
                                        <div class="item-name" data-bind="text: name">TEST ITEM NAME</div>
                                    </div>
                                    <!-- /ko -->
                                </div>
                            </div>
                            <!-- /ko -->
                            <div class="prod-row">
                                <div class="prod-chk">
                                    <input type="checkbox" data-bind="checked: includeRevals">
                                </div>
                                <div class="prod-content">
                                    <div class="prod-title"><strong>Revalidation Transactions</strong></div>
                                </div>
                            </div>
                        </section>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="4" style="text-align: center; padding-top: 15px; padding-bottom: 5px;">
                        <a class="btn btn-primary layout" data-bind="click: doGenerate">Generate Report</a>
                        <a class="btn btn-third layout" data-bind="click: doReset">Reset Filters</a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </div>
    </div>

    <section class="result-view" style="margin-top: 1em;">
        <section data-bind="visible: showExport" style="display: none; text-align: right; margin-bottom: 1em;">
            <a class="btn btn-primary" style="display: inline-block" data-bind="click: doExport">Export to Excel</a>
        </section>

        <div id="resultMarker"></div>
    </section>

</div>