[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]

[#assign maintMessage = starfn.getMaintenanceMessage(channel)!""]

<style type="text/css">
    h1 {
        font-size: 2.85em;
        color: #26b67c;
        font-family: 'Gotham-Medium', sans-serif;
        font-weight: 400;
    }
</style>
[#if maintMessage?has_content]
    <div class="main-content-section-container" style="text-align: center;margin: 10px auto;">
        <!--TODO add an image-->
        <!--<s:if test="mi != ''">-->
        <!--<img src="<s:property value="mi"/>" class="maint-img">-->
        <!--</s:if>-->
        <h1 class="title">${maintMessage.getProperty("name").getString()}</h1>
        <div class="content">${maintMessage.getProperty("message").getString()}</div>
    </div>
[/#if]
