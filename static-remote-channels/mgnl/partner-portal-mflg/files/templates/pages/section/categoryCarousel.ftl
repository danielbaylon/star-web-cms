[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]

[#if content.images?has_content]
    [#assign images = cmsfn.children(cmsfn.contentByPath(content.@handle + "/images"))]
    [#if images?size > 0]
    <div class="highlight-section">
        <img class="sequence-prev slider-arrow" src="${ctx.contextPath}/resources/${channel}/theme/default/img/left-arrow.png" alt="Prev" />
        <img class="sequence-next slider-arrow" src="${ctx.contextPath}/resources/${channel}/theme/default/img/right-arrow.png" alt="Next" />
        <ul class="sequence-canvas">
            [#list images as imageNode]
                [#if imageNode?has_content]
                    [#assign rendition = ""]
                    [#if imageNode.image?has_content]
                        [#assign rendition = damfn.getRendition(imageNode.image, "original")]
                    [/#if]
                    [#assign assetTitle = "Image not found"]
                    [#if rendition != ""]
                        [#if rendition.asset?? && rendition.asset.title?has_content]
                            [#assign assetTitle = rendition.asset.title]
                        [/#if]
                    [/#if]

                [#-- Alt text and title --]
                    [#assign imageAlt = imageNode.title!assetTitle!]
                    [#assign imageTitle = imageNode.title!assetTitle!]

                    [#assign imageLink = ""]
                    [#if rendition != ""]
                        [#assign imageLink = rendition.link]
                    [/#if]
                    [#assign imageHyperlink = imageNode.url!"javascript:void(0);"]
                    <li>
                        <div class="promotion-1">
                            <a href="${imageHyperlink}" target="_blank">
                                <img src="${imageLink}" alt="${imageAlt}" />
                                <img class="banner-exclusive" src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-exclusive-big.png" />
                            </a>
                        </div>
                    </li>
                [/#if]
            [/#list]
        </ul>
    </div>
    [/#if]
[/#if]