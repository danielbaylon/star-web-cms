[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }
    /*remove row  background */
    .k-alt, .k-separator {
        background-color: #FFFFFF;
    }
    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-grid .k-hierarchy-col {
        width: 1px;
    }
    .k-grid td {
        word-wrap: break-word;
        border-width: 0 0 1px 1px;
    }

    element.style {
        padding-right: 0px;
    }

    td.k-detail-cell {
        padding-left: 3.69cm;
        padding-right: 1.3cm;
        padding-top: 0cm;
        padding-bottom: 2px;
    }
    .sub-detail.k-grid.k-widget> .k-grid-header{
        display: none;
    }
    .k-grid-content {
        overflow-y: hidden;
    }
    td.k-detail-cell {
        opacity: 0.6; /* Real browsers */
        filter: alpha(opacity = 60); /* MSIE */
    }
    .k-grid-header {
        padding-right: 0px;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    element.style {
        padding-right: 0px;
    }
    div.k-grid-header, div.k-grid-footer {
        padding-right: 0px;
    }
</style>

[#assign systemAnnouncements = starfn.getAnnouncements("${channel}")]
<div class="main-content-section-container">
    <h2 class="heading">Dashboard</h2>
    <div class="main-content-section">
        <div class="sidebar-content-section">
            <div class="notice-board-container">
                <h4 class="secondary-heading-bar">Notice Board</h4>
                <table class="notice-board-content-container" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    [#if systemAnnouncements?has_content]
                        [#list systemAnnouncements as sa]
                        <tr>
                            <td class="notice-main-title"><a href="${sa.relatedLink!}" target="_blank" class="link-text">${sa.title?html}</a></td>
                            <td class="notice-main-sub-title">${sa.content!}</td>
                            <td class="date">${sa.validFrom?string["dd/MM/yyyy"]!}</td>
                        </tr>
                        [/#list]
                    [/#if]
                    </tbody>
                </table>
            </div>
            <div class="purchased-items-container">
                <h4 class="primary-heading-bar">Recent Purchased Items</h4>
                <div class="purchased-items-list-container">
                    <ul class="product-list-content-container sidebar-items-spacing">


                        [#--<li>--]
                            [#--<a href="/products/Attractions#!/show/13" class="product-content product-size-small">--]
                                [#--<img src="/static/prod/Madam_Tussaud.jpg" alt="" height="100%" width="100%">--]
                                [#--<span class="product-title">Madam Tuss'auds Combo</span>--]

                                [#--<img class="icon-exclusive" src="img/icon-exclusive.png">--]

                            [#--</a>--]
                        [#--</li>--]

                        [#--<li>--]
                            [#--<a href="/products/Attractions#!/show/2" class="product-content product-size-small">--]
                                [#--<img src="/static/prod/Merlion Tower - TA.jpg" alt="" height="100%" width="100%">--]
                                [#--<span class="product-title">Sentosa Merlion</span>--]

                            [#--</a>--]
                        [#--</li>--]

                        [#--<li>--]
                            [#--<a href="/products/Attractions#!/show/1" class="product-content product-size-small">--]
                                [#--<img src="/static/prod/Luge.jpg" alt="" height="100%" width="100%">--]
                                [#--<span class="product-title">Sentosa's Luge &amp; Skyride</span>--]

                            [#--</a>--]
                        [#--</li>--]

                        [#--<li>--]
                            [#--<a href="/products/Packages#!/show/28" class="product-content product-size-small">--]
                                [#--<img src="/static/prod/default-prod-image.jpg" alt="" height="100%" width="100%">--]
                                [#--<span class="product-title">SG's SG's50 Tourist Pack</span>--]

                                [#--<img class="icon-exclusive" src="img/icon-exclusive.png">--]

                            [#--</a>--]
                        [#--</li>--]

                        [#--<li>--]
                            [#--<a href="/products/Attractions#!/show/3" class="product-content product-size-small">--]
                                [#--<img src="/static/prod/default-prod-image.jpg" alt="" height="100%" width="100%">--]
                                [#--<span class="product-title">Cable Car (DC Super Heroes)</span>--]

                            [#--</a>--]
                        [#--</li>--]

                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="items-records-container">
                <h4 class="primary-heading-bar">Expiring Tickets</h4>
                [#--<div style="" class="k-grid k-widget" data-role="grid" id="epTransGrid">--]
                    [#--<div style="padding-right: 0px;" class="k-grid-header">--]
                        [#--<div class="k-grid-header-wrap">--]
                            [#--<table role="grid" cellspacing="0">--]
                                [#--<colgroup>--]
                                    [#--<col style="width:40px">--]
                                    [#--<col style="width:126px">--]
                                    [#--<col style="width:130px">--]
                                    [#--<col style="width:124px">--]
                                    [#--<col style="width:79px">--]
                                    [#--<col style="width:78px">--]
                                    [#--<col style="width:91px">--]
                                [#--</colgroup>--]
                                [#--<thead>--]
                                [#--<tr>--]
                                    [#--<th class="k-header" role="columnheader" data-field="rowNumber" data-title="No.">No.</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="receiptNum" data-title="Receipt<BR>Number">Receipt<br>Number</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="validityStartDateStr" data-title="Validity Start<BR>Date">Validity Start<br>Date</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="validityEndDateStr" data-title="Validity End<BR>Date">Validity End<br>Date</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="transQty" data-title="Quantity">Quantity</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="totalAmount" data-title="Amt(S$)">Amt(S$)</th>--]
                                    [#--<th class="k-header" role="columnheader" data-field="username" data-title="Purchased By">Purchased By</th>--]
                                [#--</tr>--]
                                [#--</thead>--]
                            [#--</table>--]
                        [#--</div>--]
                    [#--</div>--]
                    [#--<div class="k-grid-content">--]
                        [#--<table role="grid" cellspacing="0">--]
                            [#--<colgroup>--]
                                [#--<col style="width:40px">--]
                                [#--<col style="width:126px">--]
                                [#--<col style="width:130px">--]
                                [#--<col style="width:124px">--]
                                [#--<col style="width:79px">--]
                                [#--<col style="width:78px">--]
                                [#--<col style="width:91px">--]
                            [#--</colgroup>--]
                            [#--<tbody>--]
                            [#--<tr data-uid="16154a5f-87dc-4d05-9113-9f0b4a86c428" role="row">--]
                                [#--<td role="gridcell"><span class="row-number">1</span></td>--]
                                [#--<td role="gridcell"><a href="/viewExpiringTrans?currentTransNum=1" class="link-text transhref">SB2B160802TVDLY</a></td>--]
                                [#--<td role="gridcell">02/08/2016</td><td role="gridcell">31/08/2016</td>--]
                                [#--<td role="gridcell">2</td>--]
                                [#--<td role="gridcell"> 28.00</td>--]
                                [#--<td role="gridcell">LUCY123_ADMIN</td>--]
                            [#--</tr>--]
                            [#--<tr class="k-alt" data-uid="f72d1690-a90d-4197-baad-0f56baf6edf4" role="row">--]
                                [#--<td role="gridcell"><span class="row-number">2</span></td>--]
                                [#--<td role="gridcell"><a href="/viewExpiringTrans?currentTransNum=2" class="link-text transhref">SB2B160802QHAZ0</a></td>--]
                                [#--<td role="gridcell">15/08/2016</td><td role="gridcell">31/08/2016</td>--]
                                [#--<td role="gridcell">4</td>--]
                                [#--<td role="gridcell"> 28.00</td>--]
                                [#--<td role="gridcell">LUCY123_ADMIN</td>--]
                            [#--</tr>--]
                            [#--<tr data-uid="6567dad2-0e0a-46db-983a-2c979f1428f4" role="row">--]
                                [#--<td role="gridcell"><span class="row-number">3</span></td>--]
                                [#--<td role="gridcell"><a href="/viewExpiringTrans?currentTransNum=3" class="link-text transhref">SB2B160802DZEC7</a></td>--]
                                [#--<td role="gridcell">02/08/2016</td><td role="gridcell">31/08/2016</td>--]
                                [#--<td role="gridcell">2</td><td role="gridcell"> 28.00</td>--]
                                [#--<td role="gridcell">LUCY123_ADMIN</td>--]
                            [#--</tr>--]
                            [#--<tr class="k-alt" data-uid="141506a6-b776-4400-8d8f-2aa569a86e0d" role="row">--]
                                [#--<td role="gridcell"><span class="row-number">4</span></td>--]
                                [#--<td role="gridcell"><a href="/viewExpiringTrans?currentTransNum=4" class="link-text transhref">SB2B160318QV2YC</a></td>--]
                                [#--<td role="gridcell">18/03/2016</td><td role="gridcell">18/09/2016</td>--]
                                [#--<td role="gridcell">10</td><td role="gridcell"> 165.00</td>--]
                                [#--<td role="gridcell">LUCY123_ADMIN</td>--]
                            [#--</tr>--]
                            [#--<tr data-uid="6ed0376e-9d6a-49f1-8ffa-cb70111dd8ab" role="row">--]
                                [#--<td role="gridcell"><span class="row-number">5</span></td>--]
                                [#--<td role="gridcell"><a href="/viewExpiringTrans?currentTransNum=5" class="link-text transhref">SB2B1603184K1KS</a></td>--]
                                [#--<td role="gridcell">18/03/2016</td><td role="gridcell">18/09/2016</td><td role="gridcell">20</td>--]
                                [#--<td role="gridcell"> 140.00</td>--]
                                [#--<td role="gridcell">LUCY123_ADMIN</td>--]
                            [#--</tr>--]
                            [#--</tbody>--]
                        [#--</table>--]
                    [#--</div>--]
                    [#--<div data-role="pager" class="k-pager-wrap k-grid-pager k-widget">--]
                        [#--<a tabindex="-1" data-page="1" href="#" title="Go to the first page" class="k-link k-state-disabled">--]
                            [#--<span class="k-icon k-i-seek-w">Go to the first page</span>--]
                        [#--</a>--]
                        [#--<a tabindex="-1" data-page="1" href="#" title="Go to the previous page" class="k-link k-state-disabled">--]
                            [#--<span class="k-icon k-i-arrow-w">Go to the previous page</span>--]
                        [#--</a>--]
                        [#--<ul class="k-pager-numbers k-reset">--]
                            [#--<li><span class="k-state-selected">1</span></li>--]
                            [#--<li><a tabindex="-1" href="#" class="k-link" data-page="2">2</a></li>--]
                        [#--</ul>--]
                        [#--<a tabindex="-1" data-page="2" href="#" title="Go to the next page" class="k-link">--]
                            [#--<span class="k-icon k-i-arrow-e">Go to the next page</span>--]
                        [#--</a>--]
                        [#--<a tabindex="-1" data-page="2" href="#" title="Go to the last page" class="k-link">--]
                            [#--<span class="k-icon k-i-seek-e">Go to the last page</span>--]
                        [#--</a>--]
                        [#--<span class="k-pager-info k-label">1 - 5 of 9 items</span>--]
                    [#--</div>--]
                [#--</div>--]
            </div>
        </div>
        <div class="adv-section">
        [@cms.area name="advertisement"/]
        </div>
        <div class=" clearfix"></div>
    </div>
</div>