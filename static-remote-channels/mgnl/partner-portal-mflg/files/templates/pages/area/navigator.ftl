[#include "/partner-portal-mflg/templates/macros/pageInit.ftl"]
[#assign partnerChannel = "partner-portal-mflg"]
[#assign urlVersion = "?ver=1.0r002"]
[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = false]
[#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
    [#assign isOnPreview = true]
[/#if]
[#assign requestUri = ctx.getRequest().getRequestURI()]


<style>
    .top-header-section-content {
        float: none;
    }
</style>

[#assign checkUserLogin = true]

[#if cmsfn.authorInstance && cmsfn.previewMode]
    [#assign checkUserLogin = false]
[/#if]

[#if cmsfn.authorInstance && cmsfn.editMode]
    [#assign checkUserLogin = false]
[/#if]

[#if cmsfn.authorInstance && isOnPreview]
    [#assign checkUserLogin = false]
[/#if]


[#if checkUserLogin]
<script>
    function logout(){
        $('#default-user-logout-form').submit();
    }
    $(document).ready(function() {
        var currentPageURL = "${requestUri}";
        //TODO add checking here

        $.ajax({
            url: '${API_PREFIX!""}/publicity/partner-account/get-login-username',
            headers: { 'Store-Api-Channel' : '${partnerChannel}' },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    $("#login-username").text(data.data);
                } else {
                    window.location.replace('/${partnerChannel}/');
                }
            },
            error: function () {
                alert("System Error, please try again later.");
            },
            complete: function () {
            }
        });
    });
</script>
[/#if]

[#assign cats = starfn.getCMSProductCategories(partnerChannel)]
[#if cats?has_content]
    [#assign defaultCat = cats[0]]
    [#if defaultCat.hasProperty("name")]
        [#assign defaultCatName = defaultCat.getProperty("name").getString()]
    [/#if]
    [#assign defaultCatNodeName = cats[0].getName()]
[/#if]

<div class="background top-header">
    <div class="top-header-section">
        <div class="top-header-section-content">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr>
                    <td width="50%">
                        <a class="logo" href="${ctx.contextPath}/${partnerChannel}/category~${defaultCatName}~${defaultCatNodeName}~">
                            <img src="${ctx.contextPath}/resources/${partnerChannel}/theme/default/img/logo-sentosaB2B.png${urlVersion}" alt="Sentosa THE STATE OF FUN" style="height: 80%"/>
                        </a>
                    </td>
                    <td>
                        <form id="default-user-logout-form" method="post" action="${API_PREFIX!""}/publicity/partner-account/logout">
                            <div class="user-login-info">
                                <i class="fa fa-user"></i><p> Hi, <span id="login-username"></span> <a href="javascript:logout();" class="link-text">Log Out</a></p>
                            </div>
                        </form>
                    </td>
                    <td width="20%">
                        <a class="shopping-cart link-text" id="headerCartSummary" href="${ctx.contextPath}/partner-portal-mflg/cart">
                            <i class="fa fa-shopping-cart"></i>
                            [#if checkUserLogin]
                                <span class="sc-text">My Cart (<span id="cartItemNum"><span data-bind="text: totalQty"></span> items</span>)</span>
                            [#else]
                                <span class="sc-text">My Cart (0 items)</span>
                            [/#if]
                        </a>
                    </td>
                </tr>

                </tbody></table>

        </div>
        <div class="clearfix"></div>
    </div>
</div>


<nav id="adminNav" style="display: none;">
    <div class="background main-header" style='min-height: 100px;'>
        <div class="main-header-section">
            <ul class="nav">
                <li class="odd">
                    <a href="${rootPath}/dashboard" class="dashboard" data-bind="css: { 'selected': currNav() == 'Dashboard' }"></a><span class="nav-text">Dashboard</span>
                </li>

            [#if starfn.hasRights("purchaseTickets")]
                [#if cats?has_content]
                    [#list cats as catNode]
                        [#assign catName = ""]
                        [#if catNode.hasProperty("iconImage")]
                            [#assign iconImage = catNode.getProperty("iconImage").getString()]
                            [#assign assetLink = damfn.getAssetLink(iconImage)]
                        [/#if]
                        [#if catNode.hasProperty("generatedURLPath")]
                            [#assign catURLPath = catNode.getProperty("generatedURLPath").getString()]
                        [/#if]
                        [#if catNode.hasProperty("name")]
                            [#assign catName = catNode.getProperty("name").getString()]
                        [/#if]
                        <li class="even">
                            <span class="divider"></span>
                        </li>
                        <li class="odd">
                            <a href="${rootPath}/category~${catURLPath}~${catNode.getName()!}~">
                                <img src="${assetLink}" style="height: 57px; width: 57px;"/>
                            </a>
                            <span class="nav-text">${catName}</span>
                        </li>
                    [/#list]
                [/#if]
            [/#if]

            [#if starfn.hasRights("reserveWOT")]
                <!--TODO rights-->
                <li class="even">
                    <span class="divider"></span>
                </li>

                <li class="odd category" data-bind="event: { 'mouseover': function(){stickied('WOT');}, 'mouseout' : function(){stickied('');} }">
                    <a href="#" class="wot" data-bind="css: { 'selected': currNav() == 'WOT' || stickied() == 'WOT' }, click: function(){stickyThis('WOT')}"></a>
                    <span class="nav-text">Wings of Time</span>
                    <div class="sub-nav-container" style="top: 80px;" data-bind="visible: stickied() == 'WOT'">
                        <div class="arrow-up"></div>
                        <ul class="sub-nav" >
                            <li class="first-option"><a href="${rootPath}/view-reservations"><i class="fa fa-chevron-right"></i> Reservations</a></li>
                            <li class="first-option"><a href="${rootPath}/wot-receipts"><i class="fa fa-chevron-right"></i> View Receipt</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
            [/#if]


            [#if starfn.hasRights("viewInventory") || starfn.hasRights("offlinePayment")]
                <li class="even">
                    <span class="divider"></span>
                </li>

                <li class="odd category" data-bind="visible: hasNavRights('Inventory'), event: { 'mouseover': function(){stickied('Inventory');}, 'mouseout' : function(){stickied('');} }">
                    <a href="#" class="inventory" data-bind="css: { 'selected': currNav() == 'Inventory' || stickied() == 'Inventory' }, click: function(){stickyThis('Inventory')}"></a>
                    <span class="nav-text">Inventory<br/>&nbsp;</span>
                    <div class="sub-nav-container" style="top: 80px;" data-bind="visible: stickied() == 'Inventory'">
                        <div class="arrow-up"></div>
                        <ul class="sub-nav" >
                            <li class="first-option"><a href="${rootPath}/view-inventory"><i class="fa fa-chevron-right"></i> View Inventory</a></li>
                            <li class="first-option"><a href="${rootPath}/view-packages"><i class="fa fa-chevron-right"></i> View Packages</a></li>
                            <li class="first-option"><a href="${rootPath}/offlinePay-list"><i class="fa fa-chevron-right"></i> Offline Payment</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                </li>
            [/#if]

            [#if starfn.hasRights("reports")]

                <li class="even">
                    <span class="divider"></span>
                </li>
                <li class="odd category" data-bind="event: { 'mouseover': function(){stickied('Reports');}, 'mouseout' : function(){stickied('');} }">
                    <a href="#" class="reports" data-bind="css: { 'selected': currNav() == 'Reports' || stickied() == 'Reports' }"></a>
                    <span class="nav-text">Reports<br/>&nbsp;</span>
                    <!--TODO add data bind below-->
                    <div class="sub-nav-container panel-width" style='top: 80px;' data-bind="visible: stickied() == 'Reports'">
                        <div class="arrow-up"></div>
                        <ul class="sub-nav" >
                            <li data-bind="event:{'mouseover': function(){stickied('Reports');}, 'mouseout' : function(){stickied('');}}" class="first-option">
                                <a href="${rootPath}/transaction-report"><i class="fa fa-chevron-right"></i> Transaction Report</a>
                            </li>
                            <li data-bind="event:{'mouseover': function(){stickied('Reports');}, 'mouseout' : function(){stickied('');}}" class="first-option">
                                <a href="${rootPath}/inventory-report"><i class="fa fa-chevron-right"></i> Inventory Report</a>
                            </li>
                            <li data-bind="event:{'mouseover': function(){stickied('Reports');}, 'mouseout' : function(){stickied('');}}" class="first-option">
                                <a href="${rootPath}/package-report"><i class="fa fa-chevron-right"></i> Package Report</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
            [/#if]

            [#if starfn.hasRights("topupDeposits")]
                <li class="even">
                    <span class="divider"></span>
                </li>

                <li class="odd category" data-bind="event: { 'mouseover': function(){stickied('Deposits');}, 'mouseout' : function(){stickied('');} }">
                    <a href="#" class="deposits" data-bind="css: { 'selected': currNav() == 'Deposits' || stickied() == 'Deposits' }, click: function(){stickyThis('Deposits')}"></a>
                    <span class="nav-text">Deposits</span>
                    <div class="sub-nav-container" style="top: 80px;" data-bind="visible: stickied() == 'Deposits'">
                        <div class="arrow-up"></div>
                        <ul class="sub-nav" >
                            <li class="first-option"><a href="${rootPath}/deposit-management"><i class="fa fa-chevron-right"></i> Deposit Management</a></li>
                            <li class="first-option"><a href="${rootPath}/deposit-purchase"><i class="fa fa-chevron-right"></i> Deposit Top Up</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
            [/#if]

            <li class="even">
                <span class="divider"></span>
            </li>
            <li class="odd category" data-bind=" event: { 'mouseover': function(){stickied('AccountManagement');}, 'mouseout' : function(){stickied('');} }">
                <a href="#" class="account-management" data-bind="css: { 'selected': currNav() == 'AccountManagement' || stickied() == 'AccountManagement' }, click: function(){stickyThis('AccountManagement')}"></a>
                <span class="nav-text">Accounts</span>
                <!--TODO add data bind below-->
                <div class="sub-nav-container panel-width" style="top: 80px;" data-bind="visible: stickied() == 'AccountManagement'">
                    <div class="arrow-up"></div>
                    <ul class="sub-nav" >
                        <li class="first-option"><a href="${rootPath}/renew-password" ><i class="fa fa-chevron-right"></i> Change Password</a></li>
                        [#if starfn.hasRights("accMgmt")]
                            <li class="first-option"><a href="${rootPath}/manage-profile"><i class="fa fa-chevron-right"></i> Partner Profile Management</a></li>
                        [/#if]
                        [#if starfn.hasRights("subAccMgmt")]
                            <li class="first-option"><a href="${rootPath}/manage-subaccount-access"><i class="fa fa-chevron-right"></i> Access Rights Management</a></li>
                        [/#if]
                        [#if starfn.hasRights("subAccMgmt")]
                            <li class="first-option"><a href="${rootPath}/manage-subaccounts"><i class="fa fa-chevron-right"></i> Manage Sub-User</a></li>
                        [/#if]
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>

            </ul>
        </div>
    </div>
</nav>
[#if isOnPreview]
<div class="nav" style="text-align: center;background-color: red; width: 100%"><h3>This is for preview only</h3></div>
<br/>
[/#if]
