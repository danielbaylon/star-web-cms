(function(nvx) {
    /*
     * Utilities and shared functions
     */

    nvx.SPIN_OPTS_BTN = {
        lines: 9, // The number of lines to draw
        length: 6, // The length of each line
        width: 3, // The line thickness
        radius: 4, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#FFFFFF', // #rgb or #rrggbb or array of colors
        speed: 1.2, // Rounds per second
        trail: 47, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };

    nvx.BASE_SPINNER_OPTS = {
        lines: 13, // The number of lines to draw
        length: 16, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        rotate: 0, // The rotation offset
        color: '#000', // #rgb or #rrggbb
        speed: 1.2, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };

    nvx.spinner = new function() {
        var s = this;
        s.spinner = new Spinner(nvx.BASE_SPINNER_OPTS);
        s.elemId = 'spinner-overlay';
        s.start = function() {
            $('#' + s.elemId).show();
            s.spinner.spin(document.getElementById(s.elemId));
        };
        s.stop = function() {
            s.spinner.stop();
            $('#' + s.elemId).hide();
        };
    };

    nvx.doButtonLoading = function(target, mainFunc, opts) {
        var $target = $(target);
        var savedText = $target.html();
        $target.html('&nbsp;');
        var loader = new Spinner(nvx.SPIN_OPTS_BTN).spin($target[0]);

        mainFunc(opts, function() {
            loader.stop();
            $target.html(savedText);
        });
    };

    nvx.showButtonLoading = function(target) {
        var $target = $(target);
        var savedText = $target.html();
        $target.html('&nbsp;');
        var loader = new Spinner(nvx.SPIN_OPTS_BTN).spin($target[0]);

        return { savedText : savedText, loader: loader };
    };

})(window.nvx = window.nvx || {});