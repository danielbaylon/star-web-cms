(function(nvx, $) {
    /*
     * Deposit Balance
     */
    $(document).ready(function() {

        if(document.getElementById('depositBalanceDiv') != null) {
            try{
                nvx.depositBalanceModel  = new nvx.DepositBalanceModel();
                ko.applyBindings(nvx.depositBalanceModel, document.getElementById('depositBalanceDiv'));
                nvx.depositBalanceModel.getLatestDepositBalance();
            }catch(e){
            }
        }
    });

    nvx.depositBalanceModel = null;

    nvx.DepositBalanceModel = function() {
        var s = this;
        s.depositBalance = ko.observable("S$ 0.00");
        s.getLatestDepositBalance = function(callback) {
            try{
                $.ajax({
                    type: "POST",
                    url:  nvx.API_PREFIX + '/secured/partner-deposit/get-deposit-balance',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            s.depositBalance("S$ " + kendo.toString(Math.abs(parseFloat(data.data)), 'n2'));
                        }
                        else
                        {
                            s.depositBalance("S$ ");
                            console.log('ERRORS: ' + data['errorCode'] + ' - '+data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        s.depositBalance("S$ ");
                        console.log('Unable to process get deposit balance. Please try again in a few moments.');
                    },
                    complete: function() {
                        if(callback) {
                            callback();
                        }
                    }
                });
            }catch(e){
                console.log('loading-customer-account-balance-failed : '+e);
            }
        }
    };

})(window.nvx = window.nvx || {}, jQuery);