(function(nvx, $) {
    nvx.API_PREFIX = "/.store/ppmflg";
    nvx.API_CHANNEL = "partner-portal-mflg";
    nvx.ROOT_PATH = "/partner-portal-mflg";
})(window.nvx = window.nvx || {}, jQuery);