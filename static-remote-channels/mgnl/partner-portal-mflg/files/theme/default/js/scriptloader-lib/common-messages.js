(function(nvx, $) {
    nvx.COM_MSG = {};
    nvx.isDefinedMsg = function(key){
        if(nvx.COM_MSG[key] != undefined){
            return true;
        }
        return false;
    };
    nvx.getDefinedMsg = function(key){
        if(nvx.isDefinedMsg(key)){
            var msg = nvx.COM_MSG[key];
            if(msg != undefined && msg[CURR_LANG] != undefined && msg[CURR_LANG] != null){
                return msg[CURR_LANG];
            }
        }
        return null;
    };
    nvx.getPreferDefinedMsg = function(key){
        var msg = nvx.getDefinedMsg(key);
        if(!(msg != undefined && msg != null && msg != '')){
            console.log('message key ['+key+'] is not defined.');
            msg = key;
        }
        return msg;
    };
})(window.nvx = window.nvx || {}, jQuery);

/** common messsage, begin **/
nvx.COM_MSG['IncompleteLoginDetails'] = { 'en' : 'Both username and password cannot be blank.'};
nvx.COM_MSG['AllFieldsRequired'] = { 'en' : 'All fields must be filled up.'};
nvx.COM_MSG['RenewPassNotMatch'] = { 'en' : 'New passwords do not match. Please check your input.'};
nvx.COM_MSG['PasswordPolicyNotMet'] = { 'en' : 'Password does not match the policy. Please see the password guidelines.'};
nvx.COM_MSG['UsernameInvalid'] = { 'en' : 'Username is required and must be less than 100 characters.'};
nvx.COM_MSG['NameInvalid'] = { 'en' : 'Name is required and must be less than 100 characters.'};
nvx.COM_MSG['EmailInvalid'] = { 'en' : 'Email is invalid.'};
nvx.COM_MSG['ImageRequired'] = { 'en' : 'An image is required.'};
nvx.COM_MSG['OrderingInvalid'] = { 'en' : 'Order must be a number. Please leave it blank if you want to add it to the end.'};
nvx.COM_MSG['TitleInvalid'] = { 'en' : 'Title is required and must be less than 200 characters.'};
nvx.COM_MSG['NewsInvalid'] = { 'en' : 'Content is required and must be less than 4000 characters.'};
nvx.COM_MSG['ValidityRequired'] = { 'en' : 'Dates of validity are required.'};
nvx.COM_MSG['ValidityInvalid'] = { 'en' : 'Valid (From) must be earlier or equal to the Valid (To) date.'};
nvx.COM_MSG['PasswordLeadingTrailing'] = { 'en' : 'Password must not contain leading or trailing spaces.'};
nvx.COM_MSG['Last5Passwords'] = { 'en' : 'Password may not be similar to last 5 passwords.'};
nvx.COM_MSG['InvalidOrignialPassword'] = { 'en' : 'Invalid current password, please verify and try again.'};

/** common message, end **/

/** user login, begin **/
nvx.COM_MSG['login.err.LoginFailed'] = { 'en' : 'Invalid username or password. Please try again.'};
nvx.COM_MSG['login.err.AccountInactive'] = { 'en' : 'Sorry, your account is inactive. Please contact an administrator.'};
nvx.COM_MSG['login.err.LoginSuspended'] = { 'en' : 'Sorry, your account is suspended. Please contact an administrator.'};
nvx.COM_MSG['login.err.AccountLocked'] = { 'en' : 'Your account is locked. Please contact the main account holder to unlock this account or proceed to "Forgot Password" to reset your password and unlock your account.'};
nvx.COM_MSG['login.err.ExceedPasswordAttempts'] = { 'en' : 'Invalid username or password. You have exceeded the number of login retries and your account is suspended. Please contact the main account holder to unlock this account or proceed to "Forgot Password" to reset your password and unlock your account.'};
nvx.COM_MSG['login.err.ExceedConcurrentLogin'] = { 'en' : 'This account has reached the maximum number of users logging into with this username. Please try again later.'};
/** user login, end **/

/** partner registration, begin **/
nvx.COM_MSG['INVALID_VERIFICATION_CODE'] = { 'en' : 'Invalid Verification Code.'};
nvx.COM_MSG['REG_CODE_INVALID'] = {'en' : 'Invalid partner code.'};
nvx.COM_MSG['MAXIMUM_PARTNER_DOC_SIZE'] = {'en' : '1. Maximum file size 4MB.\r\n2. Supported file format for attachment are PDF, Microsoft Word (DOC, Docx, RTF, TXT), image files (JPEG, JPG, BMP, PNG, TIFF).'};
nvx.COM_MSG['REG_CODE_IN_USE'] = {'en' : 'This Partner Username already been used, please choose another partner code.'};
nvx.COM_MSG['REG_CODE_NOT_IN_USE'] = {'en' : 'This Partner Username is available for registration.'};
nvx.COM_MSG['REG_LOGIN_NAME_IN_USE'] = {'en' : 'This Partner Username already been used, please choose another Partner Username.'};
nvx.COM_MSG['REG_LOGIN_NAME_NOT_IN_USE'] = {'en' : 'This Partner Username is available for registration.'};
nvx.COM_MSG['INVALID_PARTNER_ID'] = {'en' : 'Invalid Partner Id.'};
nvx.COM_MSG['INVALID_PARTNER_RESUBMIT_REQUEST'] = {'en' : 'Invalid Request, Access Denied.'};
/** partner registration, end **/

/** forgot password, begin **/
nvx.COM_MSG['EMPTY_USERNAME_AND_PWD'] = {'en' : 'All fields must be filled up.'};
nvx.COM_MSG['EMPTY_USERNAME_AND_PWD'] = {'en' : 'All fields must be filled up.'};
nvx.COM_MSG['RESET_USERNAME_AND_PWD_SUCCESS'] = {'en' : 'We have successfully reset your password. Please check your email for details.'};
nvx.COM_MSG['RESET_USERNAME_AND_PWD_FAILED'] = {'en' : 'Unable to complete forgot password process. Please check if your username, email and CAPTCHA is valid.'};
/** forgot password, end **/

nvx.COM_MSG['IncompleteLoginDetails'] = {'en' : 'Both username and password cannot be blank.'};
nvx.COM_MSG['AllFieldsRequired'] = {'en' : 'All fields must be filled up.'};
nvx.COM_MSG['RenewPassNotMatch'] = {'en' : 'New passwords do not match. Please check your input.'};
nvx.COM_MSG['PasswordPolicyNotMet'] = {'en' : 'Password does not match the policy. Please see the password guidelines.'};
nvx.COM_MSG['UsernameInvalid'] = {'en' : 'Username is required and must be less than 100 characters.'};
nvx.COM_MSG['NameInvalid'] = {'en' : 'Name is required and must be less than 100 characters.'};
nvx.COM_MSG['EmailInvalid'] = {'en' : 'Email is invalid.'};
nvx.COM_MSG['ImageRequired'] = {'en' : 'An image is required.'};
nvx.COM_MSG['OrderingInvalid'] = {'en' : 'Order must be a number. Please leave it blank if you want to add it to the end.'};
nvx.COM_MSG['TitleInvalid'] = {'en' : 'Designation is required and must be less than 200 characters.'};
nvx.COM_MSG['NewsInvalid'] = {'en' : 'Content is required and must be less than 4000 characters.'};
nvx.COM_MSG['ValidityRequired'] = {'en' : 'Dates of validity are required.'};
nvx.COM_MSG['ValidityInvalid'] = {'en' : 'Valid (From) must be earlier or equal to the Valid (To) date.'};
nvx.COM_MSG['PasswordLeadingTrailing'] = {'en' : 'Password must not contain leading or trailing spaces.'};
