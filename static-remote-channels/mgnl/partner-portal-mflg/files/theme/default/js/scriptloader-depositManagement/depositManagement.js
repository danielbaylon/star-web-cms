
(function(nvx, $, ko) {
    $(document).ready(function() {

        (function init(){
            var s = this;
            s.BASE_URL = nvx.API_PREFIX + '/secured/partner-deposit/search-deposit';
            s.theUrl = s.BASE_URL;
            s.typeStr = $('#typeStr').kendoDropDownList({
                dataSource: {
                    data: ['All','OPEN', 'INVOICED','REFUND']
                },
                animation: false
            }).data('kendoDropDownList');
            s.startDate = $('#startDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');
            s.endDate = $('#endDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');

            s.refreshUrl = function() {
                var depositTxnQueryType = $('#typeStr').data('kendoDropDownList').value();
                var startDateObj = $('#startDate').data('kendoDatePicker').value();
                var endDateObj = $('#endDate').data('kendoDatePicker').value();
                if(depositTxnQueryType == undefined || depositTxnQueryType == null || depositTxnQueryType == ''){
                    alert('Type is mandatory.');
                    return false;
                }
                if(startDateObj == undefined || startDateObj == null || startDateObj == ''){
                    alert('"From" date is mandatory.');
                    return false;
                }
                if(endDateObj == undefined || endDateObj == null || endDateObj == ''){
                    alert('"To" date is mandatory.');
                    return false;
                }
                if(startDateObj.getTime() > endDateObj.getTime()){
                    alert('"From" date must be earlier than "To" date.');
                    return false;
                }

                var calculatedEndMonth = new Date(kendo.parseDate( $('#startDate').data('kendoDatePicker').value()));
                calculatedEndMonth = calculatedEndMonth.setMonth(calculatedEndMonth.getMonth() + 3);
                if(calculatedEndMonth < endDateObj.getTime()){
                    alert('Maximum three months from "From" date to "To" date is allowed.');
                    return false;
                }

                s.theUrl = s.BASE_URL + '?startDateStr=' + $('#startDate').val()
                + '&endDateStr=' + $('#endDate').val()+ '&displayCancelled=' + ($("#displayCancelled").is(":checked")?"Y":"N")
                + '&showTime=' +  $("input[name=showTimes]:checked").val()+'&depositTxnQueryType='+depositTxnQueryType;

                $('#depositExportForm_startDateStr').val($('#startDate').val());
                $('#depositExportForm_endDateStr').val($('#endDate').val());
                $('#depositExportForm_depositTxnQueryType').val(depositTxnQueryType);

                itemsdataSource.options.transport.read.url = s.theUrl;
                itemsdataSource.page(1);
            };

            $('#btnExportAsExcel').click(function(){
                $('#depositExportForm').submit();
            });

            $('#btnfilter').click(function(event){
                event.preventDefault();
                s.refreshUrl();
            });

            $('#btnReset').click(function(event){
                event.preventDefault();
                $('#typeStr').data('kendoDropDownList').value('All');
                $('#startDate').data('kendoDatePicker').value('');
                $('#endDate').data('kendoDatePicker').value('');

                $('#depositExportForm_startDateStr').val('');
                $('#depositExportForm_endDateStr').val('');
                $('#depositExportForm_depositTxnQueryType').val('All');

            });

            s.pageSize = 10;
            var itemsdataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: s.theUrl,
                        headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                        cache: false,
                        error: function (xhr, error) {
                            console.debug(xhr);
                            console.debug(error);
                        }
                    }
                },
                schema: {
                    data: 'data.viewModel',
                    total: 'data.total',
                    model: {
                        id: 'id',
                        fields: {
                            id: {type: 'number'},
                            transactionDate: {type: 'string'},
                            transactionType:{type: 'string'},
                            credit: {type: 'number'},
                            debit:  {type: 'number'},
                            type:  {type: 'string'},
                            receiptNo: {type: 'string'},
                            paymentReference : {type : 'string'}
                        }
                    }
                }, pageSize: s.pageSize, serverSorting: true, serverPaging: true,
                requestStart: function () {
                    nvx.spinner.start();
                },
                requestEnd: function () {
                    nvx.spinner.stop();
                }
            });

            s.kGrid = $("#depositGrid").kendoGrid({
                dataSource: itemsdataSource,
                dataBound: dataBound,
                columns: [{
                    field: "transactionDate",
                    title: "Transaction Date",
                    sortable: false,
                    width: 200
                },{
                    field: "transactionType",
                    title: 'Transaction Type',
                    width: 170
                },{
                    field: "type",
                    title: "Type",
                    width: 70
                }, {
                    field: "credit",
                    title: "Amount(S$)",
                    template: function(dataItem) {
                        return kendo.toString(dataItem.credit, "n2");
                    },
                    width: 70,
                    attributes:{style:"text-align:right;"},
                    headerAttributes: {style:"text-align:right;"}
                },{
                    field: "paymentReference",
                    title: "Reference Number.",
                    width: 120
                }],
                sortable: true, pageable: true
            });

            function dataBound(e){
                var grid = $("#depositGrid").data("kendoGrid");
                var currentPage = itemsdataSource.page();
                var pageSize = itemsdataSource.pageSize();
                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });
            }

            $(".k-grid-header").css("padding-right", "0px");

        })();
    });

})(window.nvx = window.nvx || {}, jQuery, ko);
