
(function(nvx, $, ko) {
    $(document).ready(function() {
        var confirmMsgPopup = $("#confirmMsgWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');
        nvx.confirmMsgModeal = new nvx.ConfirmMessageModel();
        nvx.depositPurchaseModel = new nvx.DepositPurchaseModel();
        ko.applyBindings(nvx.confirmMsgModeal,document.getElementById('confirmMsgWindow'));
        ko.applyBindings(nvx.depositPurchaseModel , document.getElementById('creditPurchaseForm'));
        window.confirmMsgPopup = confirmMsgPopup;
    });

    nvx.depositPurchaseModel =  null;

    nvx.DepositPurchaseModel = function() {
        var s = this;
        s.depositAmount = ko.observable(0);

        s.doDepositPurchaseValidation = function(){
            if(!(s.depositAmount() != undefined && s.depositAmount() != null && s.depositAmount() != '')){
                alert('Please enter correct amount to top-up.');
                return false;
            }
            if(isNaN(s.depositAmount())) {
                alert('Please enter correct amount to top-up.');
                return false;
            }
            var amt = s.depositAmount();
            if(parseFloat(amt) <= 0){
                alert('Please enter correct amount to top-up.');
                return false;
            }
            return true;
        };

        s.doDepositPurchaseProcess = function(){
            nvx.showConfirmMsgWindow('Confirm Deposit Top-Up', 'You will be redirect to payment page, please confirm to continue your Deposit Top-Up.', function(){
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url:  nvx.API_PREFIX + '/secured/partner-deposit/pre-deposit-topup-process',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    data: s.depositAmount(),
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        nvx.spinner.stop();
                        if(data.success)
                        {
                            s.doDepositPurchaseProcessRedirect();
                        }
                        else
                        {
                            alert(data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            });
        };

        s.doDepositPurchaseProcessRedirect = function(){
            $.ajax({
                type: "POST",
                url:  nvx.API_PREFIX + '/secured/partner-deposit/redirect-deposit-topup-payment',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                data: s.depositAmount(),
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    nvx.spinner.stop();
                    if(data.success)
                    {
                        console.log('Redirect URL : '+data.data.redirectUrl);
                        window.location.replace(data.data.redirectUrl);
                    }
                    else
                    {
                        alert(data['message']);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.proceedToPayment = function() {
            if(s.doDepositPurchaseValidation()){
                nvx.doButtonLoading($('#btnProcceed'), s.doDepositPurchaseProcess, {});
            }
        }
    };

    nvx.confirmMsgModeal = null;
    nvx.ConfirmMessageModel = function(){
        var s = this;
        s.confirmMessage = ko.observable();
        s.callback = '';
        s.confirmModal = function() {
            window.confirmMsgPopup.close();
            if(s.callback != undefined && s.callback != null && s.callback != ''){
                try{
                    s.callback();
                    s.callback = '';
                }catch(e){
                    console.log('Confirm callback failed : '+e);
                }
            }
        }
        s.cancelModal = function(){
            window.confirmMsgPopup.close();
        }
    };

    nvx.showConfirmMsgWindow = function(winTitle, winMsg, confirmActionCallBack){
        nvx.spinner.start();
        nvx.confirmMsgModeal.callback = confirmActionCallBack;
        nvx.confirmMsgModeal.confirmMessage(winMsg);
        window.confirmMsgPopup.title(winTitle);
        window.confirmMsgPopup.center().open();
        nvx.spinner.stop();
    };

})(window.nvx = window.nvx || {}, jQuery, ko);
