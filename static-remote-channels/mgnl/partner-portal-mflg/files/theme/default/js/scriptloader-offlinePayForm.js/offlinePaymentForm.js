(function(nvx, $, ko) {
    $(document).ready(function() {
        $.ajax({
            url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/get-cart-display',
            headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    initOfflinePaymentDetailsForm(data.data);
                } else {
                    alert("System Error, please try again later.");
                    window.location.replace( nvx.ROOT_PATH + '/offlinePay-list');
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
    });

    function initOfflinePaymentDetailsForm(data){
        // cart
        var cart = data['cart'];
        if(cart != undefined && cart != null && cart != ''){
            nvx.checkoutView = new nvx.CheckoutView(cart);
            ko.applyBindings(nvx.checkoutView, document.getElementById('checkoutView'));
        }

        // notification
        nvx.partnerNotificationModel = new nvx.PartnerNotificationModel();

        // offline payment details
        var offlinePaymentDetails = data['paymentDetail'];
        if(offlinePaymentDetails != undefined && offlinePaymentDetails != null && offlinePaymentDetails != ''){
            nvx.partnerNotificationModel = new nvx.PartnerNotificationModel();
            ko.applyBindings(nvx.partnerNotificationModel, document.getElementById('PartnerNotificationWindowDiv'));
            nvx.oprVm = new nvx.PaymentInfoModel();
            nvx.oprVm.update(offlinePaymentDetails);
            ko.applyBindings(nvx.oprVm, document.getElementById('offlinePaymentFormSection1n2'));
        }

    }

    nvx.btnGuard = false;

    nvx.checkoutView = null;

    /* product view, begin */
    nvx.CheckoutView = function(base) {
        var s = this;
        s.base = base;
        s.gstRate =  ko.observable(base.gstRate);
        s.hasEdit = ko.observable(false);
        s.agreeTnc = ko.observable(false);

        s.products = ko.observableArray([]);
        // console.log("base.cmsProducts:"+base.cmsProducts.length);
        $.each(base.cmsProducts, function(idx, obj) {
            s.products.push(new nvx.CheckoutProduct(s, obj));
        });

        s.grandTotal = ko.computed(function() {
            var gt = 0;
            $.each(s.products(), function(idx, prod) {
                gt += Number(prod.subtotal());
            });
            return gt.toFixed(2);
        });
        s.grandTotalText = ko.computed(function() {
            return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
        });
        s.gst = ko.computed(function() {
            return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
        });
        s.gstRateStr = ko.computed(function() {
            return (s.gstRate()*100).toFixed(0) +'%';
        });
        s.gstText = ko.computed(function() {
            return kendo.toString(Number(s.gst()), "###,###,##0.00");
        });
        s.totalNoGst = ko.computed(function() {
            return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
        });
        s.totalNoGstText = ko.computed(function() {
            return kendo.toString(Number(s.totalNoGst()), "###,###,##0.00");
        });
    };

    nvx.CheckoutProduct = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.title = ko.observable(base.name);
        s.subtotal = ko.observable(base.subtotal);

        s.showTopupsToAdd = ko.observable(false);
        s.topupsToAddText = ko.computed(function() {
            return s.showTopupsToAdd() ? 'Hide Unselected Top Ups' : 'Add More Top Ups';
        });
        s.doToggleTopupsToAdd = function() {
            s.showTopupsToAdd(!s.showTopupsToAdd());
        };

        s.items = ko.observableArray([]);
        (function initItems(theBase) {
            var iarr = [];
            $.each(theBase.items, function(idx, obj) {
                //console.log("obj.qty:"+obj.qty)
                var addItem = new nvx.CheckoutItem(s, obj);
                iarr.push(addItem);
            });
            s.items(iarr);
        })(base);

        s.additionalTopups = ko.observableArray([]);
        //console.log("s.items:"+s.items.length);
        //Further init (in order for parent dot remove item to work)
        $.each(s.items(), function(idx, item) {
            item.includeInCart(true);
        });

        s.subtotalText = ko.computed(function() {
            var total = 0;
            $.each(s.items(), function(dix, item) {
                total += Number(item.total());
            });

            s.subtotal(total.toFixed(2));

            return kendo.toString(Number(s.subtotal()), "###,###,##0.00");
        });
        s.subtotalText.subscribe(function(newValue) {
            s.parent.hasEdit(true);
        });

        s.doRemoveItem = function(mdl, evt) {
            if (mdl.isTopup) {
                mdl.includeInCart(false);
            } else {

                var onlyTopups = true;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup && itm != mdl) {
                        onlyTopups = false;
                        return false;
                    }
                });

                if (onlyTopups || s.items().length == 1) {
                    s.parent.doRemoveProd(s);
                    return;
                }

                mdl.qty(0);
                s.items.remove(mdl);

                var hasTicketType = false;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup) {
                        if (itm.cartId == mdl.cartId) { return; }

                        if (itm.ticketType == mdl.ticketType) {
                            hasTicketType = true;
                            return false;
                        }
                    }
                });

                if (!hasTicketType) {
                    $.each(s.items(), function(idx, itm) {
                        if (itm.isTopup) {
                            if (itm.ticketType == mdl.ticketType) {
                                itm.includeInCart(false);
                            }
                        }
                    });
                }
            }
        };
    };

    nvx.CheckoutItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.isNew = ko.observable(false);
        s.scheduleId = base.scheduleId;
        s.cartId = base.cartId;
        s.cartType = base.cartType;
        s.itemId = base.id;
        s.isTopup = base.topup;
        s.title = base.name;
        s.ticketType = base.type;
        s.includeInCart =  ko.observable(true);
        s.description = base.description;
        s.priceInCents = base.price * 100;
        s.priceText = base.price;
        s.qty = ko.observable(base.qty);
        s.total = ko.observable(base.total);
        s.totalText = ko.computed(function() {
            if (!Number.isInteger(s.qty())){ return 0; }

            var totalInCents = Number(s.qty()) * Number(s.priceInCents);
            s.total((totalInCents / 100).toFixed(2));
            return kendo.toString(Number(s.total()), "###,###,##0.00");
        });
    };
    /* product view, end */


    /* offline payment form details, begin */
    nvx.PaymentInfoModel = function(){
        var s = this;
        s.id = ko.observable();
        s.transId = ko.observable();
        s.status = ko.observable();
        s.receiptNumber = ko.observable();
        s.paymentTypes = ko.observableArray([]);

        var countries = [{name:"Singapore"},{name:"Afghanistan"},{name:"Åland Islands"},{name:"Albania"},{name:"Algeria"},{name:"American Samoa"},{name:"Andorra"},{name:"Angola"},{name:"Anguilla"},{name:"Antarctica"},{name:"Antigua and Barbuda"},{name:"Argentina"},{name:"Armenia"},{name:"Aruba"},{name:"Australia"},{name:"Austria"},{name:"Azerbaijan"},{name:"Bahamas"},{name:"Bahrain"},{name:"Bangladesh"},{name:"Barbados"},{name:"Belarus"},{name:"Belgium"},{name:"Belize"},{name:"Benin"},{name:"Bermuda"},{name:"Bhutan"},{name:"Bolivia, Plurinational State of"},{name:"Bonaire, Sint Eustatius and Saba"},{name:"Bosnia and Herzegovina"},{name:"Botswana"},{name:"Bouvet Island"},{name:"Brazil"},{name:"British Indian Ocean Territory"},{name:"Brunei Darussalam"},{name:"Bulgaria"},{name:"Burkina Faso"},{name:"Burundi"},{name:"Cambodia"},{name:"Cameroon"},{name:"Canada"},{name:"Cape Verde"},{name:"Cayman Islands"},{name:"Central African Republic"},{name:"Chad"},{name:"Chile"},{name:"China"},{name:"Christmas Island"},{name:"Cocos (Keeling) Islands"},{name:"Colombia"},{name:"Comoros"},{name:"Congo"},{name:"Congo, the Democratic Republic of the"},{name:"Cook Islands"},{name:"Costa Rica"},{name:"Côte d'Ivoire"},{name:"Croatia"},{name:"Cuba"},{name:"Curaçao"},{name:"Cyprus"},{name:"Czech Republic"},{name:"Denmark"},{name:"Djibouti"},{name:"Dominica"},{name:"Dominican Republic"},{name:"Ecuador"},{name:"Egypt"},{name:"El Salvador"},{name:"Equatorial Guinea"},{name:"Eritrea"},{name:"Estonia"},{name:"Ethiopia"},{name:"Falkland Islands (Malvinas)"},{name:"Faroe Islands"},{name:"Fiji"},{name:"Finland"},{name:"France"},{name:"French Guiana"},{name:"French Polynesia"},{name:"French Southern Territories"},{name:"Gabon"},{name:"Gambia"},{name:"Georgia"},{name:"Germany"},{name:"Ghana"},{name:"Gibraltar"},{name:"Greece"},{name:"Greenland"},{name:"Grenada"},{name:"Guadeloupe"},{name:"Guam"},{name:"Guatemala"},{name:"Guernsey"},{name:"Guinea"},{name:"Guinea-Bissau"},{name:"Guyana"},{name:"Haiti"},{name:"Heard Island and McDonald Islands"},{name:"Holy See (Vatican City State)"},{name:"Honduras"},{name:"Hong Kong"},{name:"Hungary"},{name:"Iceland"},{name:"India"},{name:"Indonesia"},{name:"Iran, Islamic Republic of"},{name:"Iraq"},{name:"Ireland"},{name:"Isle of Man"},{name:"Israel"},{name:"Italy"},{name:"Jamaica"},{name:"Japan"},{name:"Jersey"},{name:"Jordan"},{name:"Kazakhstan"},{name:"Kenya"},{name:"Kiribati"},{name:"Korea, Democratic People's Republic of"},{name:"Korea, Republic of"},{name:"Kuwait"},{name:"Kyrgyzstan"},{name:"Lao People's Democratic Republic"},{name:"Latvia"},{name:"Lebanon"},{name:"Lesotho"},{name:"Liberia"},{name:"Libya"},{name:"Liechtenstein"},{name:"Lithuania"},{name:"Luxembourg"},{name:"Macao"},{name:"Macedonia, the former Yugoslav Republic of"},{name:"Madagascar"},{name:"Malawi"},{name:"Malaysia"},{name:"Maldives"},{name:"Mali"},{name:"Malta"},{name:"Marshall Islands"},{name:"Martinique"},{name:"Mauritania"},{name:"Mauritius"},{name:"Mayotte"},{name:"Mexico"},{name:"Micronesia, Federated States of"},{name:"Moldova, Republic of"},{name:"Monaco"},{name:"Mongolia"},{name:"Montenegro"},{name:"Montserrat"},{name:"Morocco"},{name:"Mozambique"},{name:"Myanmar"},{name:"Namibia"},{name:"Nauru"},{name:"Nepal"},{name:"Netherlands"},{name:"New Caledonia"},{name:"New Zealand"},{name:"Nicaragua"},{name:"Niger"},{name:"Nigeria"},{name:"Niue"},{name:"Norfolk Island"},{name:"Northern Mariana Islands"},{name:"Norway"},{name:"Oman"},{name:"Pakistan"},{name:"Palau"},{name:"Palestinian Territory, Occupied"},{name:"Panama"},{name:"Papua New Guinea"},{name:"Paraguay"},{name:"Peru"},{name:"Philippines"},{name:"Pitcairn"},{name:"Poland"},{name:"Portugal"},{name:"Puerto Rico"},{name:"Qatar"},{name:"Réunion"},{name:"Romania"},{name:"Russian Federation"},{name:"Rwanda"},{name:"Saint Barthélemy"},{name:"Saint Helena, Ascension and Tristan da Cunha"},{name:"Saint Kitts and Nevis"},{name:"Saint Lucia"},{name:"Saint Martin (French part)"},{name:"Saint Pierre and Miquelon"},{name:"Saint Vincent and the Grenadines"},{name:"Samoa"},{name:"San Marino"},{name:"Sao Tome and Principe"},{name:"Saudi Arabia"},{name:"Senegal"},{name:"Serbia"},{name:"Seychelles"},{name:"Sierra Leone"},{name:"Sint Maarten (Dutch part)"},{name:"Slovakia"},{name:"Slovenia"},{name:"Solomon Islands"},{name:"Somalia"},{name:"South Africa"},{name:"South Georgia and the South Sandwich Islands"},{name:"South Sudan"},{name:"Spain"},{name:"Sri Lanka"},{name:"Sudan"},{name:"Suriname"},{name:"Svalbard and Jan Mayen"},{name:"Swaziland"},{name:"Sweden"},{name:"Switzerland"},{name:"Syrian Arab Republic"},{name:"Taiwan, Province of China"},{name:"Tajikistan"},{name:"Tanzania, United Republic of"},{name:"Thailand"},{name:"Timor-Leste"},{name:"Togo"},{name:"Tokelau"},{name:"Tonga"},{name:"Trinidad and Tobago"},{name:"Tunisia"},{name:"Turkey"},{name:"Turkmenistan"},{name:"Turks and Caicos Islands"},{name:"Tuvalu"},{name:"Uganda"},{name:"Ukraine"},{name:"United Arab Emirates"},{name:"United Kingdom"},{name:"United States"},{name:"United States Minor Outlying Islands"},{name:"Uruguay"},{name:"Uzbekistan"},{name:"Vanuatu"},{name:"Venezuela, Bolivarian Republic of"},{name:"Viet Nam"},{name:"Virgin Islands, British"},{name:"Virgin Islands, U.S."},{name:"Wallis and Futuna"},{name:"Western Sahara"},{name:"Yemen"},{name:"Zambia"},{name:"Zimbabwe"}];
        s.countries = ko.observableArray(countries);

        s.paymentType = ko.observable();
        s.paymentTypeText = ko.observable();
        s.bankName = ko.observable().extend({ maxlength: 200 });
        s.bankCountry = ko.observable();
        s.bankCountryText = ko.observable();
        s.referenceNum = ko.observable().extend({ maxlength: 200 });
        s.paymentDateStr = ko.observable();
        s.remarks = ko.observable('').extend({ maxlength: 1000 });
        s.hasChatHists = ko.observable(false);
        s.totalAmountStr = ko.observable();
        s.createDtStr = ko.observable();
        s.offlinePaymentApprovalDateText = ko.observable();
        s.chatHists  = ko.observableArray([]);
        s.nonEmptyString = function(str){
            if(str != undefined && str != null && str != ''){
                return str;
            }else{
                return '';
            }
        };
        s.update = function(data){
            s.id(s.nonEmptyString(data['id']));
            s.transId(s.nonEmptyString(data['transId']));
            s.status(s.nonEmptyString(data['status']));
            s.receiptNumber(s.nonEmptyString(data['receiptNum']));
            if('Pending_Submit' == s.status()){
                s.paymentTypes(data['paymentTypes']);
                s.countries(data['countries']);
            }
            s.paymentType(s.nonEmptyString(data['paymentType']));
            s.paymentTypeText(s.nonEmptyString(data['paymentTypeText']));
            s.bankName(s.nonEmptyString(data['bankName']));
            s.bankCountry(s.nonEmptyString(data['bankCountry']));
            s.bankCountryText(s.nonEmptyString(data['bankCountry']));
            s.referenceNum(s.nonEmptyString(data['referenceNum']));
            s.paymentDateStr(s.nonEmptyString(data['paymentDateStr']));
            s.remarks(s.nonEmptyString(data['remarks']));
            s.totalAmountStr(s.nonEmptyString(data['totalAmountStr']));
            s.createDtStr(s.nonEmptyString(data['createDtStr']));
            s.offlinePaymentApprovalDateText(s.nonEmptyString(data['approvalDateText']));

            s.chatHists.removeAll();
            if(data != null && data.chatHists != null){
                $.each(data.chatHists, function(key, item) {
                    s.hasChatHists(true);
                    s.chatHists.push(new nvx.ChatHist(item));
                });
            }else{
                s.hasChatHists(false);
            }
        };
        if($('#paymentDatePicker').size() > 0) {
            $('#paymentDatePicker').kendoDatePicker({
                format: 'dd/MM/yyyy'
            });
        }
        if($('#paymentDatePicker').size() > 0) {
            $('#paymentDatePicker').data('kendoDatePicker').value(null);
            if(s.paymentDateStr() != undefined && s.paymentDateStr() != null && s.paymentDateStr() != '') {
                try {
                    $('#paymentDatePicker').data('kendoDatePicker').value(kendo.parseDate(s.paymentDateStr(), 'dd/MM/yyyy'));
                } catch (e) {}
            }
        }
        if($('#btnIncomplete').size() > 0) {
            $('#btnIncomplete').click(function () {
                if (confirm('Are you sure you want to withdrawn this payment request?')) {
                    nvx.spinner.start();
                    $.ajax({
                        url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/withdraw-offline-request',
                        headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                        data: JSON.stringify({
                            id : nvx.oprVm.id(),
                            transId : nvx.oprVm.transId(),
                            paymentType : nvx.oprVm.paymentType(),
                            remarks :  nvx.oprVm.remarks()
                        }),
                        type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                        success:  function(data, textStatus, jqXHR)
                        {
                            if (data.success) {
                                alert("Payment withdrawn successfully.");
                                var retData = data['data'];
                                if(retData != undefined && retData != null && retData != '' ){
                                    var paymentDetail = retData['paymentDetail'];
                                    if(paymentDetail != undefined && paymentDetail != null && paymentDetail != ''){
                                        nvx.oprVm.update(paymentDetail);
                                    }
                                }
                            } else {
                                alert(data['message']);
                            }
                        },
                        error: function(jqXHR, textStatus)
                        {
                            console.log(textStatus);
                        },
                        complete: function() {
                            nvx.spinner.stop();
                        }
                    });
                }
            });
        }
        if($('#btnSubmitUserComments').size() > 0) {
            $('#btnSubmitUserComments').click(function () {
                nvx.partnerNotificationModel.reset();
                nvx.partnerNotificationPopup.center().open();
            });
        }
        if($('#btnSubmit').size() > 0) {
            $('#btnSubmit').click(function () {
                var tmpOpr = {};
                tmpOpr.id = nvx.oprVm.id();
                tmpOpr.transId = nvx.oprVm.transId();
                tmpOpr.paymentType = nvx.oprVm.paymentType();
                tmpOpr.remarks = nvx.oprVm.remarks();
                tmpOpr.bankName = nvx.oprVm.bankName();
                tmpOpr.bankCountry = nvx.oprVm.bankCountry();
                tmpOpr.referenceNum = nvx.oprVm.referenceNum();
                tmpOpr.paymentDateStr = '';
                if($('#paymentDatePicker').size() > 0) {
                    tmpOpr.paymentDateStr = kendo.toString($('#paymentDatePicker').data('kendoDatePicker').value(),'dd/MM/yyyy');
                }
                if(!tmpOpr.paymentType){
                    alert("Please choose a payment type first.");
                    return;
                }
                if(!tmpOpr.bankName){
                    alert("Please input bank name.");
                    return;
                }
                if(tmpOpr.bankName.trim().length  == 0){
                    alert("Please input bank name.");
                    return;
                }
                if(tmpOpr.bankName.trim().length > 200){
                    alert("Please enter no more than 200 characters for bank name. You've currently entered "+tmpOpr.bankName.trim().length+" characters.");
                    return;
                }
                if(!tmpOpr.bankCountry){
                    alert("Please select bank country.");
                    return;
                }
                if(!tmpOpr.referenceNum){
                    alert("Please input Reference Number/ Cheque Number.");
                    return;
                }
                if(tmpOpr.referenceNum.trim().length  == 0){
                    alert("Please input Reference Number/ Cheque Number.");
                    return;
                }
                if(tmpOpr.referenceNum.trim().length > 200){
                    alert("Please enter no more than 200 characters for Reference Number/ Cheque Number. You've currently entered "+tmpOpr.referenceNum.trim().length+" characters.");
                    return;
                }
                if(!(tmpOpr.paymentDateStr != undefined  && tmpOpr.paymentDateStr != null && tmpOpr.paymentDateStr != '')){
                    alert("Please select a payment date.");
                    return;
                }
                if(tmpOpr.remarks != null && tmpOpr.remarks != '' && tmpOpr.remarks != undefined && tmpOpr.remarks.trim().length > 1000){
                    alert("Please enter no more than 1000 characters for Remarks. You've currently entered "+tmpOpr.remarks.trim().length+" characters.");
                    return;
                }
                nvx.spinner.start();

                $.ajax({
                    url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/submit-request',
                    headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                    data: JSON.stringify(tmpOpr),
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if (data.success) {
                            alert("Payment submit successfully, please verify your email.");
                            var retData = data['data'];
                            if(retData != undefined && retData != null && retData != ''){
                                nvx.oprVm.update(retData['paymentDetail']);
                            }
                        } else {
                            alert(data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert(ERROR_MSG);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            });
        }
        if($('#btnCancel').size() > 0) {
            $('#btnCancel').click(function () {
                window.location.replace( nvx.ROOT_PATH + '/offlinePay-list');
            });
        }
    };
    nvx.ChatHist = function(data){
        var s = this;
        s.username = ko.observable(data.username);
        s.content = ko.observable(data.content);
        s.createDtStr = ko.observable(data.createDtStr);
        s.usertype = ko.observable(data.usertype);
    };

    nvx.PartnerNotificationModel = function(){
        var s = this;
        s.notificationText = ko.observable();

        s.reset = function(){
            s.notificationText('');
        }

        s.sendNotification = function(){
            if(!(s.notificationText() != null && s.notificationText() != '' && $.trim(s.notificationText()) != null && $.trim(s.notificationText()) != '')){
                alert('Please enter "Content of Notification".');
                return;
            }
            if(s.notificationText().length < 20){
                alert('Minimum characters for "Content of Notification" is 20.');
                return;
            }
            if(s.notificationText().length > 1000){
                alert('Maximum characters for "Content of Notification" is 1000.');
                return;
            }
            nvx.spinner.start();

            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/send-partner-msg',
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                data: JSON.stringify({
                    id : nvx.oprVm.id(),
                    transId : nvx.oprVm.transId(),
                    paymentType : nvx.oprVm.paymentType(),
                    remarks :  s.notificationText()
                }),
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if (data.success) {
                        nvx.spinner.stop();
                        alert("Your notification has been sent successfully.");
                        window.location.reload();
                    } else {
                        nvx.spinner.stop();
                        alert(data.message);
                        s.cancelNotification();
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    alert('System error, Please try again.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.cancelNotification = function(){
            nvx.partnerNotificationPopup.close();
        };

        nvx.partnerNotificationPopup = $("#PartnerNotificationWindowDiv").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Notification',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
            }
        }).data('kendoWindow');
        nvx.partnerNotificationPopup.close();
    };
    /* offline payment form details, end*/

})(window.nvx = window.nvx || {}, jQuery, ko);