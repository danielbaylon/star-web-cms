(function(nvx, $, ko) {
    $(document).ready(function() {

        nvx.theNav.currNav('Dashboard');
        nvx.theNav.currSubNav('Dashboard');

        var $page = this;
        $page.pageSize = 5;
        $page.baseUrl = '/dashboard/loadEpTrans'
        $page.serverSorting = false;
        var itemsdataSource = new kendo.data.DataSource({
                transport: {read: {url: $page.baseUrl, cache: false}},
                schema: {
                    data: 'expiringTansVMs',
                    total: 'total',
                    model: {
                        id: 'id',
                        fields: {
                            receiptNum: {type: 'string'},
                            username: {type: 'string'},
                            totalAmount: {type: "string"},
                            transQty: {type: "number"},
                            validityStartDateStr: {type: "string"},
                            validityEndDateStr: {type: 'string'}
                        }
                    }
                }, pageSize: $page.pageSize, serverSorting: $page.serverSorting, serverPaging: true
        });
        var epTransGrid = $("#epTransGrid").kendoGrid({
            dataSource: itemsdataSource,
            dataBound: dataBound,
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
              },{
                field: "receiptNum",
                title: "Receipt<BR>Number",
                template: '<a href="" class="link-text transhref" >#=receiptNum#</a>',
                width: 126
              },{
                field: "validityStartDateStr",
                title: "Validity Start<BR>Date",
                width: 130
              },{
                field: "validityEndDateStr",
                title: "Validity End<BR>Date",
                width: 124
              },{
                field: "transQty",
                title: "Quantity",
                width: 79
              },{
                field: "totalAmount",
                title: "Amt(S$)",
                width: 78
              },{
                field: "username",
                title: "Purchased By",
                width: 91
              }
              ],
              sortable: $page.serverSorting, pageable: true
          });

        function dataBound(e){
            var grid = $("#pkgGrid").data("kendoGrid");
            var currentPage = itemsdataSource.page();
            var pageSize = itemsdataSource.pageSize();
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                var alink = $(this).find(".transhref");
                $(rowLabel).html(index);
                $(alink).attr('href','/viewExpiringTrans?currentTransNum='+index);;
            });
        }
        $(".k-grid-header").css("padding-right", "0px");
    });
})(window.nvx = window.nvx || {}, jQuery, ko);