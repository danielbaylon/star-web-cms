
(function(nvx, $, ko) {
    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        nvx.mmpackageModel = new nvx.MMPackageModel();
        ko.applyBindings(nvx.mmpackageModel , document.getElementById('vPkgWindow'));

        (function init(){
            var s = this;
            s.BASE_URL = nvx.API_PREFIX + '/secured/partner-inventory/search-package';
            s.theUrl = s.BASE_URL;
            s.startDate = $('#startDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');
            s.endDate = $('#endDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');

            s.refreshUrl = function() {
                s.theUrl = s.BASE_URL + '?startDateStr=' + $('#startDate').val()
                + '&endDateStr=' + $('#endDate').val()+ '&status=' + $('#status').val()
                + '&ticketMedia=' + $('#ticketMedia').val()
                + '&pkgName=' +  $('#pkgName').val();
                itemsdataSource.options.transport.read.url = s.theUrl;
                itemsdataSource.page(1);
            };

            s.refreshExportUrl = function() {
                var exporturl =  nvx.API_PREFIX + '/secured/partner-inventory/download-pkg-report';
                exporturl = exporturl+ '?startDateStr=' + $('#startDate').val()
                + '&endDateStr=' + $('#endDate').val()+ '&status=' + $('#status').val()
                + '&ticketMedia=' + $('#ticketMedia').val()
                + '&pkgName=' +  $('#pkgName').val();

                $("#btnExportToExcel").attr("href", exporturl);

            };

            s.refreshExportUrl();

            $('#btnfilter').click(function(event){
                event.preventDefault();
//    var msg = '';
//    if (s.startDate.value() == null || s.endDate.value() == null) {
//        msg += (msg != '' ? '<br>' : '') + 'Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
//    } else if (s.startDate.value() > s.endDate.value()) {
//        msg += (msg != '' ? '<br>' : '') + 'Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
//    }
//    if(msg !== ''){
//        $("#errordv").html(msg);
//        $("#errordv").show();
//        window.scrollTo(0,0);
//        return false;
//    }else{
//        $("#errordv").hide();
//    }
                initialLoad = true;
                s.refreshUrl();
                s.refreshExportUrl();
            });

            s.pageSize = 10;
            var initialLoad = true;
            var itemsdataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: s.theUrl,
                        headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                        cache: false}},
                schema: {
                    data: 'data.viewModel',
                    total: 'data.total',
                    model: {
                        id: 'id',
                        fields: {
                            name: {type: 'name'},
                            pkgTktMedia: {type: 'string'},
                            pinCode: {type: 'string'},
                            qty: {type: 'number'},
                            ticketGeneratedDateStr: {type: 'string'},
                            expiryDateStr: {type: "string"},
                            status: {type: "string"},
                            qtyRedeemedStr: {type: "string"},
                            lastRedemptionDateStr: {type: 'string'},
                            description: {type: 'string'},
                            username: {type: 'username'},
                            canRedeem:{type:"boolean"},
                            canRedeemDtStr:{type: 'string'}
                        }
                    }
                }, pageSize: s.pageSize, serverSorting: true, serverPaging: true,
                requestStart: function () {
                    if (initialLoad){
                        nvx.spinner.start();
//            kendo.ui.progress($("#pkgGrid"), true);
                    }
                },
                requestEnd: function () {
                    if(initialLoad){
                        nvx.spinner.stop();
//            kendo.ui.progress($("#pkgGrid"), false);
                    }
                    initialLoad = false;
                }
            });

            s.kGrid = $("#pkgGrid").kendoGrid({
                dataSource: itemsdataSource,
                dataBound: dataBound,
                detailInit: detailInit,
                columns: [{
                    field: "rowNumber",
                    title: "No.",
                    template: "<span class='row-number'></span>",
                    sortable: false,
                    width: 40
                },{
                    field: "name",
                    title: 'Package Name',
                    template: '<a href="javascript:viewPackage(#=id#,&quot;#=name#&quot;);" >#=name#</a>',
                    width: 100
                },{
                    field: "description",
                    title: "Description",
                    width: 120
                }, {
                    field: "qty",
                    title: "Qty",
                    width: 50
                }, {
                    field: "pkgTktMedia",
                    title: "Ticket<BR>Media",
                    with: 90,
                    template: function(data) {
                        if(data.pkgTktMedia == 'Pincode') {
                            return 'PINCODE <br/>' + data.pinCode + '';
                        }else if (data.pkgTktMedia == 'ETicket') {
                            return 'E-Ticket';
                        }else if (data.pkgTktMedia == 'ExcelFile') {
                            return 'Excel File'
                        }
                    }
                }, {
                    field: "ticketGeneratedDateStr",
                    title: "Ticket<BR>Generated<BR>Date Time",
                    width: 90
                },{
                    field: "expiryDateStr",
                    title: "Expiry<BR>Date"
                }, {
                    field: "status",
                    title: "Status",
                    width: 90
                },
                //{
                //    field: "qtyRedeemedStr",
                //    title: "Quantity<BR>Redeemed"
                //},
                 {
                    field: "lastRedemptionDateStr",
                    title: "Last<BR>Redemption<BR>Date Time",
                    width: 90
                },{
                    field: "username",
                    title: "Action By"
                    }, {
                        title: "&nbsp;",
                        sortable: false,
                        template: '<a #if( pkgTktMedia != "Pincode" ) {# style="display:none" #} else if(canRedeem) {#class="btn btn-secondary btn-block"'+
                        '  onclick="javascript:checkRedeem(this,#=id#)"# } else '+
                        '{#class="btn btn-disabled btn-block" '
                        +' onclick="javascript:checkRedeem(this,#=id#,\'#=canRedeemDtStr#\')"  #}#  >'
                        +'Get<br>Redemption<br>Status</a>',
                        //btn btn-disabled btn-block
                        width: 110
                    }],
                sortable: true, pageable: true
            });

            function dataBound(e){
                var grid = $("#pkgGrid").data("kendoGrid");
                var currentPage = itemsdataSource.page();
                var pageSize = itemsdataSource.pageSize();
                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });
                //this.expandRow(this.tbody.find("tr.k-master-row"));
            }

            function detailInit(e) {

                var itemDisplay = "";
                var ticketMedia = e.data.pkgTktMedia;

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-inventory/get-package/' + e.data.id,
                    headers: {'Store-Api-Channel': nvx.API_CHANNEL},
                    success: function (data, textStatus, jqXHR) {
                        if (typeof data.error === 'undefined') {
                            itemDisplay +=  "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"items-tickets-content-container package-items\" style=\"border: 1px solid #26b67c; margin-top: 10px; margin-bottom: 10px;\">";
                            itemDisplay += "<tr class=\"forth-multi-heading-bar\" style=\"background-color: #26b67c\">";
                            itemDisplay += "<td width=\"30%\" style=\"background-color: #26b67c\">Product</td>";
                            itemDisplay += "<td width=\"10%\" style=\"background-color: #26b67c\">Quantity</td>";

                            if(ticketMedia == 'Pincode') {
                               itemDisplay += "<td width=\"10%\" style=\"background-color: #26b67c\">Quantity Redeemed</td>";
                            }

                            itemDisplay += "<td width=\"30%\" class=\"center\" style=\"background-color: #26b67c\">Receipt Number</td>";
                            itemDisplay += "</tr>";

                            for(var i = 0; i < data.data.transItems.length; i++) {
                                var transItem = data.data.transItems[i];
                                itemDisplay += "<tr class=\"main-items\">";
                                itemDisplay += "<td width=\"30%\">";
                                itemDisplay += "<span>" + transItem.displayName + "</span>";
                                itemDisplay += "<span>" + transItem.displayDetails + "</span>";
                                itemDisplay += "</td>";
                                itemDisplay += "<td width=\"10%\">" + transItem.pkgQty + "</td>";

                                if(ticketMedia == 'Pincode') {
                                    itemDisplay += "<td width=\"10%\">" + transItem.qtyRedeemed + "</td>";
                                }

                                itemDisplay += "<td width=\"30%\" class=\"center\">" + transItem.receiptNum + "</td>";
                                itemDisplay += "</tr>";

                                for(var j = 0; j < transItem.topupItems.length; j++) {
                                    var topupItem = transItem.topupItems[j];
                                   itemDisplay += "<tr>";
                                   itemDisplay += "<td><i class=\"fa fa-plus-circle\"></i><span>" + topupItem.displayName + "</span></td>";
                                   itemDisplay += "<td style=\"padding: 0.8em\">" + topupItem.pkgQty + "</td>";

                                    if(ticketMedia == 'Pincode') {
                                        itemDisplay += "<td width=\"10%\">" + topupItem.qtyRedeemed + "</td>";
                                    }

                                    itemDisplay += "<td >&nbsp;</td>";
                                    itemDisplay += "</tr>";
                                }
                            }

                            itemDisplay += "</table>"

                            $(itemDisplay).appendTo(e.detailCell);
                        }
                        else {
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function (jqXHR, textStatus) {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function () {
                        nvx.spinner.stop();
                    }
                });
            }

//view pop start

            var pkgPopup = $("#vPkgWindow").kendoWindow({
                width: '800px',
                modal: true,
                resizable: false,
                // title: "View Package",
                actions: ["Close"],
                viewable : false
            }).data('kendoWindow');

            function viewPackage(pkgId,pgkName){
                var htmlcontent;
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url:  nvx.API_PREFIX + '/secured/partner-inventory/get-package/' + pkgId,
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(typeof data.error === 'undefined')
                        {

                            nvx.mmpackageModel.init(data.data);

                            if(nvx.mmpackageModel.pkgTktMedia() == 'ETicket') {
                                $("#downloadTicket").attr("href", nvx.API_PREFIX + "/secured/partner-inventory/download-ticket/" + nvx.mmpackageModel.bookingRefId());
                            }else if(nvx.mmpackageModel.pkgTktMedia() == 'ExcelFile') {
                                $("#downloadExcelTicket").attr("href", nvx.API_PREFIX + "/secured/partner-inventory/download-excel-ticket/" + nvx.mmpackageModel.bookingRefId());
                            }

                            pkgPopup.title(pgkName);
                            //pkgPopup.content(htmlcontent);
                            pkgPopup.center().open();
                        }
                        else
                        {
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });


            }

            //TODO how this checkRedeem works
            function checkRedeem(element,pkgId,canRedeemDtStr){
                if(canRedeemDtStr){
                    var dt = kendo.parseDate(canRedeemDtStr, "dd/MM/yyyy HH:mm:ss");
                    var now = new Date();
                    console.log(dt);
                    console.log(now);
                    if(dt.getTime() > now.getTime()){
                        alert("You may check the status after "+canRedeemDtStr);
                        return;
                    }
                }

                $(element).removeClass();
                //btn btn-secondary btn-block
                //btn btn-disabled btn-block
                $(element).addClass("btn btn-disabled btn-block");
//    var data = new FormData();
//    data.append('pkgId', pkgId);
                var data = {'pkgId':pkgId};

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/check-redeem-status/" + pkgId,
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(data, textStatus, jqXHR)
                    {
                        checkRedeemCallBack(data);
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                });
            }
            function checkRedeemCallBack(data){
                s.refreshUrl();
                if(!data.success){
                    alert("System is not able to verify status now, please try again later.");
                }
                console.log(data.message);
            }

            $(".k-grid-header").css("padding-right", "0px");
            window.checkRedeem = checkRedeem;
            window.viewPackage = viewPackage;
        })();


    });


    nvx.mmpackageModel =  null;

    nvx.MMPackageModel = function(base) {
        var s = this;

        s.bookingRefId = ko.observable();
        s.description = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.qtyRedeemed = ko.observable();
        s.qtyRedeemedStr = ko.observable();
        s.pkgTktMedia = ko.observable();
        s.ticketGeneratedDateStr = ko.observable();
        s.expiryDateStr = ko.observable();
        s.lastRedemptionDateStr = ko.observable();
        s.pinCode = ko.observable();
        s.transItems = ko.observableArray([]);

        s.init = function(data) {
            s.bookingRefId(data.id);
            s.description(data.description);
            s.status(data.status);
            s.qty(data.pinCodeQty);
            s.qtyRedeemed(data.qtyRedeemed);
            s.qtyRedeemedStr(data.qtyRedeemedStr);
            s.pkgTktMedia(data.pkgTktMedia);
            s.pinCode(data.pinCode);
            s.ticketGeneratedDateStr(data.ticketGeneratedDateStr);
            s.expiryDateStr(data.expiryDateStr);
            s.lastRedemptionDateStr(data.lastRedemptionDateStr);
            s.transItems([]);

            $.each(data.transItems, function(idx, obj) {
                s.transItems.push(new nvx.MMPackageItem(s, obj));
            });
        }

        s.sendEmailTicket = function() {
            var result = confirm("Are you going to send the E-Ticket email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-eticket/" + s.bookingRefId(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.message);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }

        s.sendEmailPincode = function() {
            var result = confirm("Are you going to send the PIN code email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-pincode/" + s.bookingRefId(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.message);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });

        }

        s.sendEmailExcel = function() {
            var result = confirm("Are you going to send the Excel File email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-excel/" + s.bookingRefId(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.message);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    }


    nvx.MMPackageItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.itemId = ko.observable(base.id);
        s.displayName = ko.observable(base.displayName);
        s.displayDetails = ko.observable(base.displayDetails);
        s.ticketType = ko.observable(base.ticketType);
        s.description = ko.observable(base.displayDetails);
        s.receiptNum = ko.observable(base.receiptNum);
        s.qty = ko.observable(base.pkgQty);
        s.qtyRedeemed = ko.observable(base.qtyRedeemed);
        s.topupItems = ko.observableArray([]);
        $.each(base.topupItems, function(idx, model) {
            var vm = new nvx.TopupItems(model);
            s.topupItems.push(vm);
        });
    }


    nvx.TopupItems = function(data) {
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.pkgQty = ko.observable(data.pkgQty);
        s.qtyRedeemed = ko.observable(data.qtyRedeemed);
    }


})(window.nvx = window.nvx || {}, jQuery, ko);
