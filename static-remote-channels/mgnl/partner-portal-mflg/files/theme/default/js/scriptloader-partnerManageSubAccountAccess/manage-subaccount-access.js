
var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

$(document).ready(function() {
    nvx.subAccRightsModel = new nvx.AccessRightMaintenanceModel();
    nvx.getPartnerSubAccountRoles(function(){
        nvx.getPartnerSubAccountList(function(){
            nvx.initGrid(false);
        });
    });
});

(function(nvx, $) {

    nvx.AccessRightMaintenanceModel = function(){
        var s = this;
        s.subAccountRights = ko.observableArray([]);
        s.subAccounts = ko.observableArray([]);
        s.initSubAccountRights = function(list){
            if(list != undefined && list != null && list.length > 0){
                var c = list.length;
                for(var i =  0 ;  i < c ; i++){
                    var role = list[i];
                    var item = new nvx.SubAccountRightItemModel(role);
                    s.subAccountRights.push(item);
                }
            }
        };
        s.initSubAccounts = function(list){
            if(list != undefined && list != null && list.length > 0){
                var c = list.length;
                for(var i =  0 ;  i < c ; i++){
                    var ta = list[i];
                    var item = new nvx.SubAccount(ta, s.subAccountRights());
                    s.subAccounts.push(item);
                }
            }
        };

        s.doReset = function(){
            $('#accountMgtDiv').hide();
            s.subAccounts([]);
            nvx.getPartnerSubAccountList(function(){
                nvx.initGrid(true);
            });
        };
        s.doSave = function(){
            var subAccRightRequestVM = [];
            var accs = nvx.subAccRightsModel.subAccounts();
            $.each(accs, function(idx, acc){
                var id = acc['id'];
                var name = acc['name'];
                var rightsIds = [];
                var rights = acc.rights();
                if(rights != undefined && rights != null && rights != '' && rights.length > 0){
                    $.each(rights, function(idx, obj){
                        if(obj.checked()){
                            rightsIds[rightsIds.length] = obj.id;
                        }
                    });
                }
                subAccRightRequestVM[subAccRightRequestVM.length] = {
                    id : id,
                    name : name,
                    rightsIds : rightsIds
                };
            });
            var requestStr = JSON.stringify({ accounts : subAccRightRequestVM });
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/secured/partner-account/save-subaccount-rights',
                data : {
                    subAccountRightsStr : requestStr
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        alert('Updated successfully');
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(nvx.getPreferDefinedMsg(data.message));
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };

    nvx.initGrid = function(isReset){
        if(isReset){
            $('#accountMgtDiv').show();
        }else{
            $(".k-grid-header").css("padding-right", "0px");
            ko.applyBindings(nvx.subAccRightsModel, document.getElementById('accountMgtDiv'));
            $('#accountMgtDiv').show();
        }
    };

    nvx.getPartnerSubAccountRoles = function(callback){
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/secured/partner-account/get-partner-subaccount-roles',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    nvx.subAccRightsModel.initSubAccountRights(data['data']);
                    callback();
                }else{
                    if(data.message != null && data.message != ''){
                        alert(nvx.getPreferDefinedMsg(data.message));
                    }
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    nvx.getPartnerSubAccountList = function(callback){
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/secured/partner-account/get-all-subaccounts',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    nvx.subAccRightsModel.initSubAccounts(data['data']);
                    callback();
                }else{
                    if(data.message != null && data.message != ''){
                        alert(nvx.getPreferDefinedMsg(data.message));
                    }
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    nvx.SubAccount = function(ta, allRights) {
        var s = this;
        s.id = ta.id;
        s.name = ta.name;
        s.rights = ko.observableArray([]);

        if(allRights != undefined && allRights != null && allRights != '' && allRights.length > 0){
            var taRights = ta['rightsIds'];
            if(taRights != undefined && taRights != null && taRights != '' && taRights.length > 0){
                var taRightCount = taRights.length;
                $.each(allRights, function(idx, obj){
                    var hasThisRight = false;
                    for(var i = 0 ;  i < taRightCount ; i++){
                        var tempRight = taRights[i];
                        if(tempRight != undefined && tempRight != null && tempRight == obj['id']){
                            hasThisRight = true;
                            break;
                        }
                    }
                    var newItem = new nvx.SubAccountRightItem(s, obj);
                    newItem.checked(hasThisRight);
                    s.rights.push(newItem);
                });
            }else{
                $.each(allRights, function(idx, obj){
                    var newItem = new nvx.SubAccountRightItem(s, obj);
                    s.rights.push(newItem);
                });
            }
        }
    };

    nvx.SubAccountRightItem = function(parent, right) {
        var s = this;
        s.parent = parent;
        s.id = right.id;
        s.checked = ko.observable(false);
    };


    nvx.SubAccountRightItemModel = function(theObj){
       var s = this;
        s.id = theObj.id;
        s.name = theObj.name;
        s.description = theObj.description;
    };

})(window.nvx = window.nvx || {}, jQuery);