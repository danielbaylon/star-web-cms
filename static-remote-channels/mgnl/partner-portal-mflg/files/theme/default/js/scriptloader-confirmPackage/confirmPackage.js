(function(nvx, $) {

    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        nvx.mainModel = new nvx.MainModel();
        ko.applyBindings(nvx.mainModel, document.getElementById("main-content-section"));
        nvx.mainModel.init();

    });

    nvx.mainModel = null;

    nvx.MainModel = function() {
        var s = this;

        s.id = ko.observable("");
        s.name = ko.observable("");
        s.description = ko.observable("");
        s.expiryDateStr = ko.observable("");
        s.qty = ko.observable();
        s.transItems = ko.observableArray([]);
        s.pkgTktMedia = ko.observable("");
        s.generateBtnClicked = ko.observable(false);

        s.init = function() {
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/get-confirm-pkg",
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(result, textStatus, jqXHR)
                {
                    if(result.success)
                    {
                        var data = result.data;
                        s.id(data.id);
                        s.name(data.name);
                        s.description(data.description);
                        s.expiryDateStr(data.expiryDateStr);
                        s.qty(data.qty);
                        s.pkgTktMedia(data.pkgTktMedia);
                        $.each(data.transItems, function(idx, model) {
                            var vm = new nvx.TransactionItems(model);
                            s.transItems.push(vm);
                        });

                        s.initOthers();
                    }
                    else
                    {
                        window.location.href= nvx.ROOT_PATH + "/error";
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    window.location.href= nvx.ROOT_PATH + "/error";
                }
            });
        }

        s.initOthers = function() {
            $('#editPkg').click(function(event) {
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/edit-inventory-package",
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(result, textStatus, jqXHR)
                    {
                        if(result.success)
                        {
                            window.location.href = nvx.ROOT_PATH + "/view-inventory"
                        }
                        else
                        {
                            console.log('ERRORS: ' + result.message);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                });

            });
            function addFormParam(key , value){
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value",value);
                return hiddenField;
            }

            window.onbeforeunload = function(e) {
                if (s.generateBtnClicked()) {return;}
                return "Leaving this page will cancel your checkout. Are you sure you want to do this?";
            };

            window.onunload = function(e) {
                if (s.generateBtnClicked()) {return;}
                $.ajax({
                    type: 'POST', async: false, cache: false,
                    url : nvx.API_PREFIX + '/secured/partner-inventory/cancel-package',
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    data: {}, dataType: "json",
                    success: function (result) {}
                });
            };

        }

        s.canProceedToGenerateTicket = function() {
            if (!$('#agreeTnc').is(':checked')) {
                alert('Please read and agree to the Terms and Conditions first.');
                return false;
            }

            return true;
        }
        s.genETicket = function() {

            if(s.generateBtnClicked()) {
                return;
            }

            if(s.canProceedToGenerateTicket()) {
                s.generateBtnClicked(true);
                nvx.spinner.start();

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/gen-eticket/" + nvx.mainModel.id(),
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(result, textStatus, jqXHR)
                    {
                        if(result.success)
                        {

                            window.location.href = nvx.ROOT_PATH + "/gen-package";
                        }
                        else
                        {
                            alert(result.message);
                            window.location.href = nvx.ROOT_PATH + "/view-inventory";
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        }


        s.genPinCode = function() {
            if(s.generateBtnClicked()) {
                return;
            }

            if(s.canProceedToGenerateTicket()) {
                s.generateBtnClicked(true);
                nvx.spinner.start();

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/gen-pincode/" + nvx.mainModel.id(),
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(result, textStatus, jqXHR)
                    {
                        if(result.success)
                        {

                            window.location.href = nvx.ROOT_PATH + "/gen-package";
                        }
                        else
                        {
                            alert(result.message);
                            window.location.href = nvx.ROOT_PATH + "/view-inventory";
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                });
            }
        }

        s.genExcel = function() {
            if(s.generateBtnClicked()) {
                return;
            }

            if(s.canProceedToGenerateTicket()) {
                s.generateBtnClicked(true);
                nvx.spinner.start();

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/gen-excel-ticket/" + nvx.mainModel.id(),
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(result, textStatus, jqXHR)
                    {
                        if(result.success)
                        {

                            window.location.href = nvx.ROOT_PATH + "/gen-package";
                        }
                        else
                        {
                            alert("System Error, please try again later.");
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        }

    }


    nvx.TransactionItems =  function(data){
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.description = ko.observable(data.displayDetails);
        s.pkgQty = ko.observable(data.pkgQty);
        s.receiptNum = ko.observable(data.receiptNum);
        s.ticketTye = ko.observable(data.ticketType);
        s.topupItems = ko.observableArray([]);
        $.each(data.topupItems, function(idx, model) {
            var vm = new nvx.TopupItems(model);
            s.topupItems.push(vm);
        });
    }

    nvx.TopupItems = function(data) {
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.pkgQty = ko.observable(data.pkgQty);

    }

})(window.nvx = window.nvx || {}, jQuery);