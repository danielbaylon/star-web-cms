$(document).ready(function() {
    $page = {};
    $page.OfflinePaymentModel = function(){
        var s = this;
        s.receiptNumString = ko.observable('');
        s.startDtStr = ko.observable();
        s.endDtStr = ko.observable();
        s.status = ko.observable();
        s.username = ko.observable();
        s.statusList = ko.observableArray([
            {code : 'Pending_Approval', name : 'Pending Approve'},
            {code : 'Pending_Submit', name : 'Pending Submit'},
            {code : 'Approved', name : 'Approved'},
            {code : 'Rejected', name : 'Rejected'},
            {code : 'Cancelled', name : 'Cancelled'}
        ]);
        s.resetFilters = function(){
            s.receiptNumString('');
            s.startDtStr('');
            s.endDtStr('');
            s.status('');
            s.username('');
        }

        s.filter = function(event){
            if(event != undefined && event != null && event != ''){
                try{
                    event.preventDefault();
                }catch(e){}
            }
            s.refreshUrl();
        }

        s.refreshUrl = function() {
            var url = $page.paymentUrl;
            var _receiptNum = s.receiptNumString();
            var _startDtStr = s.startDtStr();
            var _endDtStr = s.endDtStr();
            var _status = s.status();
            var _username = s.username();
            if(!(_receiptNum != undefined && _receiptNum != null && _receiptNum != '')){
                _receiptNum = '';
            }
            if(!(_startDtStr != undefined && _startDtStr != null && _startDtStr != '')){
                _startDtStr = '';
            }
            if(!(_endDtStr != undefined && _endDtStr != null && _endDtStr != '')){
                _endDtStr = '';
            }
            if(!(_status != undefined && _status != null && _status != '')){
                _status = '';
            }
            if(!(_username != undefined && _username != null && _username != '')){
                _username = '';
            }
            url = url + '?receiptNum='+(_receiptNum) + '&startDtStr='+(_startDtStr)+'&endDtStr='+(_endDtStr)+'&status='+(_status)+'&username='+(_username);
            $page.paymentDs.options.transport.read.url = url;
            $page.paymentDs.page(1);
        };

        s.viewRequest = function(id){
            if(id != undefined && id != null && id != ''){
                $.ajax({
                    url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/view-request?id='+id,
                    type: 'GET', cache: false, dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            window.location.replace(nvx.ROOT_PATH + '/offlinePay-form');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function() {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }else{
                alert('Invalid Request');
            }
        };
    };
    $page.paymentUrl = nvx.API_PREFIX + '/secured/store-partner-offlinePay/list-request',
    $page.paymentDs = new kendo.data.DataSource({
        transport: {read: {url: $page.paymentUrl, cache: false}},
        schema: {
            data: 'data.viewModel',
            total: 'data.total',
            model: {
                id: 'id',
                fields: {
                    name: {type: 'name'},
                    pinCode: {type: 'string'},
                    pinCodeQty: {type: 'number'},
                    pinGeneratedDateStr: {type: 'string'},
                    expiryDateStr: {type: "string"},
                    status: {type: "string"},
                    qtyRedeemedStr: {type: "string"},
                    lastRedemptionDateStr: {type: 'string'},
                    description: {type: 'string'},
                    username: {type: 'username'},
                    canRedeem:{type:"boolean"},
                    canRedeemDtStr:{type: 'string'}
                }
            }
        }, pageSize: 10, serverSorting: true, serverPaging: true
    });
    $page.kGrid = $("#pkgGrid").kendoGrid({
        dataSource:$page.paymentDs,
        columns: [{
            field: "receiptNum",
            title: 'Order Reference No.',
            template: "<a href=\"javascript:$page.opModel.viewRequest('#=id#');\" >#=receiptNum#</a>",
            width: 150
        },{
            field: "totalAmountStr",
            title: "Amount",
            width: 120
        },{
            field: "status",
            title: "Status"
        },{
            field: "createDtStr",
            title: "CreateDate"
        },{
            field: "username",
            title: "Done By"
        }],
        sortable: true, pageable: true
    });

    $page.startDate = $('#startDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');
    $page.endDate = $('#endDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');

    $page.opModel = new $page.OfflinePaymentModel();
    ko.applyBindings($page.opModel ,$('#filterDv')[0]);
});