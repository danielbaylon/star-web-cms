(function(nvx, $, ko) {

    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        (function init(){
            var s = this;
    //    s.startDate = ko.observable('');
    //    s.endDate = ko.observable('');
    //    s.productName = ko.observable('');
    //    s.availableOnly = ko.observable('');

            s.startDate = $('#startDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');
            s.endDate = $('#endDate').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');

            $('#btnfilter').click(function(event){
                event.preventDefault();
    //        var msg = '';
    //        if (s.startDate.value() == null || s.endDate.value() == null) {
    //            msg += (msg != '' ? '<br>' : '') + 'Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
    //        } else if (s.startDate.value() > s.endDate.value()) {
    //            msg += (msg != '' ? '<br>' : '') + 'Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
    //        }
    //        if(msg !== ''){
    //            $("#errordv").html(msg);
    //            $("#errordv").show();
    //            window.scrollTo(0,0);
    //            return false;
    //        }else{
    //            $("#errordv").hide();
    //        }
                initialLoad = true;
                s.refreshUrl();
            });
            var template = kendo.template($("#template").html());
            s.BASE_URL = nvx.API_PREFIX + '/secured/partner-inventory/search-inventory';
            s.theUrl = s.BASE_URL;
            s.refreshUrl = function() {
                s.theUrl = s.BASE_URL + '?startDateStr=' + $('#startDate').val()
                + '&endDateStr=' + $('#endDate').val()+ '&productName=' + $('#productName').val()
                + '&availableOnly=' +  ($('#availableOnly').is(':checked')?$('#availableOnly').val():'null');
                itemsdataSource.options.transport.read.url = s.theUrl;
                itemsdataSource.page(1);
            };
            s.pageSize = 10;
            var initialLoad = true;
            var itemsdataSource = new kendo.data.DataSource({
                transport: {read: {
                    url: s.theUrl,
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    cache: false}},
                schema: {
                    data: 'data.transItemVms',
                    total: 'data.total',
                    model: {
                        id: 'id',
                        fields: {
                            transId: {type: "number"},
                            receiptNumber: {type: 'string'},
                            name: {type: 'string'},
                            type: {type: 'string'},
                            validityStartDateStr: {type: 'string'},
                            validityEndDateStr: {type: 'string'},
                            qty: {type: "number"},
                            unpackagedQty: {type: "number"},
                            subtotal: {type: "number"},
                            status: {type: 'string'},
                            username: {type: 'string'},
                            ableToPkg: {type: 'boolean'},
                            topupItems: {},
                            selectedEventDate: {type: 'string'},
                            eventSessionName: {type: 'string'}
                        }
                    }
                }, pageSize: s.pageSize, serverSorting: true, serverPaging: true,
                requestStart: function () {
                    if (initialLoad){
                        nvx.spinner.start();
    //                kendo.ui.progress($("#inventoryGrid"), true);
                    }
                },
                requestEnd: function () {
                    if(initialLoad){
                        nvx.spinner.stop();
    //                kendo.ui.progress($("#inventoryGrid"), false);
                    }
                    initialLoad = false;
                }
            });

            s.kGrid = $("#inventoryGrid").kendoGrid({
                dataSource: itemsdataSource,
                detailTemplate: kendo.template($("#template").html()),
                detailInit: detailInit,
                dataBound: dataBound,
                columns: [{
                    field: "rowNumber",
                    title: "No.",
                    template: "<span class='row-number'></span>",
                    sortable: false,
                    width: 40
                },{
                    field: "receiptNumber",
                    title: 'Receipt Number',
                    template: '<a  class="link-text"  href="' + nvx.ROOT_PATH + '/view-inventory-receipt?transId=#=transId#" >#=receiptNumber#</a>',
                    width: 100
                }, {
                    field: "name",
                    title: "Product",
                    width: 150,
                    template: function(dataItem) {
                        var tmpl = dataItem.name;

                        if(dataItem.displayDetails != null) {
                            tmpl = tmpl + dataItem.displayDetails;
                        }

                        return tmpl;
                    }
                },{
                    field: "type",
                    title: "Ticket<BR>Type",
                    width: 50
                },{
                    field: "validityStartDateStr",
                    title: "Validity Start<BR>Date",
                    width: 90
                }, {
                    field: "validityEndDateStr",
                    title: "Validity End<BR>Date",
                    width: 90
                },{
                    field: "qty",
                    title: "Original<BR>Quantity"
                },{
                    field: "unpackagedQty",
                    title: "Remaining<BR>Quantity"
                },{
                    field: "subtotal",
                    title: "Amt(S$)"
                },{
                    field: "status",
                    title: "Status"
                },{
                    field: "username",
                    title: "Purchased<BR>By"
                },{
                    field:"id",
                    title: "Select",
                    width: 50,
                    sortable: false,
                    template: '# if(ableToPkg) {#<input name="itemId" value="#=id#" type="checkbox" onclick="addItem(#=id#,#=unpackagedQty#,this);" /># } else {##}#'
                }],
                sortable: true, pageable: true
            });
        })();

        function dataBound(e){
            var grid = $("#inventoryGrid").data("kendoGrid");
            var currentPage = grid.dataSource.page();
            var pageSize = grid.dataSource.pageSize();
            if(this.dataSource.total() >0){
                $(".export-to-excel-section").show();
            }
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
            });
            this.expandRow(this.tbody.find("tr.k-master-row"));
            $('.k-hierarchy-cell').html("&nbsp;");
            $('.k-detail-row').hide();
            $('.k-detail-row').has(".sub-detail.k-grid.k-widget").show();
            refreshTickedItem();
        }

        function detailInit(e) {
            var detailRow = e.detailRow;
            var subData = e.data.topupItems;
            if(subData.length == 0){
                return;
            }
            detailRow.find(".sub-detail").kendoGrid({
                dataSource: {
                    data: subData,
                    schema: {
                        model: {
                            fields: {
                                displayName: {type: 'string'},
                                ticketType: {type: 'string'},
                                createdDateStr: {type: 'string'},
                                validityEndDateStr: {type: 'string'},
                                qty: {type: "number"},
                                unpackagedQty: {type: "number"},
                                subTotal: {type: "number"},
                                status: {type: 'string'},
                                username: {type: 'string'}
                            }
                        }
                    }
                },
                columns: [{
                    field: "displayName",
                    template: '<i class="fa fa-plus-circle"></i>#=displayName#',
                    title: "Top Up",
                    width: 150
                },{
                    field: "ticketType",
                    title: "Ticket<BR>Type",
                    width: 50
                },{
                    field: "validateStartDateStr",
                    title: "Validity Start<BR>Date",
                    width: 90
                }, {
                    field: "validityEndDateStr",
                    title: "Validity End<BR>Date",
                    width: 90
                },{
                    field: "qty",
                    title: "Original<BR>Quantity"
                },{
                    field: "unpackagedQty",
                    title: "Remaining<BR>Quantity"
                },{
                    field: "subTotal",
                    title: "Amt(S$)"
                },{
                    field: "status",
                    title: "Status"
                },{
                    field: "username",
                    title: "Purchased<BR>By"
                }]
            });
        };

        var exporturl = nvx.API_PREFIX + '/secured/partner-inventory/download-inventory-report';
        exporturl = exporturl+ '?startDateStr=' + $('#startDate').val()
        + '&endDateStr=' + $('#endDate').val()+ '&productName=' + $('#productName').val()
        + '&availableOnly=' +  ($('#availableOnly').is(':checked')?$('#availableOnly').val():'null');

        $("#btnExportToExcel").attr("href", exporturl);

        $(".k-grid-header").css("padding-right", "0px");
    });

})(window.nvx = window.nvx || {}, jQuery, ko);