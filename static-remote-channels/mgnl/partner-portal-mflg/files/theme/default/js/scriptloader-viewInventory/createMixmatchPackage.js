(function(nvx, $, ko) {

    $(document).ready(function() {
        function BundledItem(displayTitle,tranItems,topupItems) {
            var self = this;
            self.displayTitle = displayTitle;
            self.tranItems = ko.observableArray(tranItems);
            self.topupItems = ko.observableArray(topupItems);
            self.rmTransItem = function() {
                packageVm.tranIdsMap.removeItem(this.itemId);
                self.tranItems.remove(this);
                refreshTickedItem();
            }
        }
        BundledItem.fromJs = function (obj){
            var itranItems = [];
            var itopupItems = [];
            var tranLength = obj.transItems.length;
            for (var i = 0; i < tranLength; i++) {
                var tem = obj.transItems[i];
                var o = new TransItem(tem.itemId,tem.receiptNum,tem.packagedQty,tem.validityEndDateStr);
                itranItems.push(o);
            }
            var topLength = obj.topupItems.length;
            for(var i = 0; i < topLength; i++){
                var tem = obj.topupItems[i];
                var o = new TopupItem(tem.displayName);
                itopupItems.push(o);
            }
            return new BundledItem (obj.displayTitle, itranItems, itopupItems);
        };

        function TransItem(itemId,receiptNum,packagedQty,validityEndDateStr){
            var self = this;
            self.itemId = itemId;
            self.receiptNum = receiptNum;
            self.packagedQty = packagedQty;
            self.validityEndDateStr = validityEndDateStr;

        }
        function TopupItem(displayName){
            var self = this;
            self.displayName = displayName;
        }
        function PackageViewModel() {
            var self = this;
            self.tranIdsMap = new HashTable();
            self.pkgName = ko.observable();
            self.pkgExpiryDateStr = ko.observable();

            self.pkgQtyPerProd = ko.observable();

            self.pkgDesc = ko.observable();
            self.prepkgQtyPerProd = 0;
            self.bundledItems = ko.observableArray([]);
            self.ticketTypes = ko.observableArray([
                { display: 'Adult', id: 'Adult'},
                { display: 'Child', id: 'Child'},
                { display: 'Standard', id: 'Standard'}
                //,{ display: 'Special', id: 4},
                //{ display: 'AdultSet', id: 10000},
                //{ display: 'ChildSet', id: 4},
                //{ display: 'StandardSet', id: 4}
            ]);
            self.ticketMedias = ko.observableArray([
                { display: 'E-Ticket', id: 'ETicket'},
                { display: 'PINCODE', id: 'Pincode'},
                { display: 'Excel File', id: 'ExcelFile'}
            ]);

            self.pkgTktMedia = ko.observable();
            self.tktMediaAllowed = ''; //the only ticket media that is allowed

            self.onTktMediaChange = function(){

                if (typeof self.pkgTktMedia() == 'undefined'
                    && self.tktMediaAllowed != ''
                    && packageVm.tranIdsMap.length > 0) {
                    alert("Cannot change the type to blank as you still have selected item.");
                    self.pkgTktMedia(self.tktMediaAllowed);
                }

                if(self.tktMediaAllowed != ''  &&
                    self.tktMediaAllowed != self.pkgTktMedia() &&
                    packageVm.tranIdsMap.length > 0  ){
                    alert("The selected ticket media is the only allowable ticket media for this mix and match package. You are not allowed to change the Ticket Media.");
                    self.pkgTktMedia(self.tktMediaAllowed);
                }
            }

            self.groupTickets = ko.observableArray([
                { display: 'Yes', id: 'Yes'},
                { display: 'No', id: 'No'}
            ]);
            self.pkgGroupTicket = ko.observable();
            self.showPkgGroupTicket = ko.computed(function() {
                return self.pkgTktMedia() == "CombinedETicket";
            });

            self.getTypeById = function(iId){
                var types = self.ticketTypes();
                var length = types.length;
                for(var i=0;i<length;i++){
                    if(types[i].id==iId){
                        return types[i].display;
                    }
                }
            }
            self.pkgTktType = ko.observable();
            self.pkgQtyPerProd.subscribe(function(newValue) {
//            if(packageVm.prepkgQtyPerProd == 0){
//                submitPkg(true);
//                packageVm.prepkgQtyPerProd = newValue;
//            }else if (Number(newValue) > Number(packageVm.prepkgQtyPerProd)){
//                alert("Please increase the quantity of each item.");
//                submitPkg(true);
//            }else if (Number(newValue) < Number(packageVm.prepkgQtyPerProd)){
//                submitPkg(true);
//                packageVm.prepkgQtyPerProd = newValue;
//            }
                submitPkg(true);
            });
            self.preTypeChange = undefined;
            self.onTypeChange = function(){
                if (typeof self.pkgTktType() == 'undefined'
                    && packageVm.tranIdsMap.length > 0) {
                    alert("Cannot change the type to blank as you still have selected item.");
                    self.pkgTktType(self.preTypeChange);
                }

                if(typeof self.pkgTktType != 'undefined' &&
                    self.preTypeChange != self.pkgTktType() &&
                    packageVm.tranIdsMap.length > 0  ){
                    var r = confirm("Do you want to discard the items in package?");
                    if (r == true) {
                        submitPkg(true);
                        self.preTypeChange = self.pkgTktType();
                    } else {
                        self.pkgTktType(self.preTypeChange);
                    }
                }
            }

            self.rmBundleItem = function() {
                var tranLength = this.tranItems().length;
                for(var i = 0; i < tranLength; i++){
                    packageVm.tranIdsMap.removeItem(this.tranItems()[i].itemId);
                }
                self.bundledItems.remove(this);
                refreshTickedItem();
            }

            self.rmEmptyBundleItem = function () {
                self.bundledItems.remove(function(bitem) {
                    return bitem.tranItems().length == 0;
                });
            }
        }
        var packageVm = new PackageViewModel();
        ko.cleanNode($('#itemdv')[0]);
        ko.applyBindings(packageVm,$('#itemdv')[0]);
        $('#btnMixMatch').click(function(event) {
            //check item
            if(packageVm.tranIdsMap.length == 0){
                alert("Please select an item first to start creating a package");
                return;
            }
            //check quantity
            if((typeof packageVm.pkgQtyPerProd()) == 'undefined'){
                alert("Quantity Per Product cannot be empty.");
                return;
            }
            //check ticket type
            if((typeof packageVm.pkgTktType()) == 'undefined'){
                alert("Please select a Ticket Type.");
                return;
            }
            //check Description
            if((typeof packageVm.pkgDesc()) == 'undefined'){
                alert("Description cannot be empty.");
                return;
            }

            //chek Ticket Media
            if((typeof packageVm.pkgTktMedia()) == 'undefined'){
                alert("Please select a Ticket Media.");
                return;
            }

            verifyPkgBfCreate();
        });
        function verifyPkgBfCreate(){
            //var data = new FormData();
            var iitransItemIds = packageVm.tranIdsMap.keys();
            var ipkgQtyPerProd = null;
            var ipkgTktType = null;
            var ipkgName = null;
            var ipkgTktMedia = null;
            //data.append('transItemIds', packageVm.tranIdsMap.keys());
            if ((typeof  packageVm.pkgQtyPerProd()) != 'undefined'){
                ipkgQtyPerProd = packageVm.pkgQtyPerProd();
            }
            if ((typeof packageVm.pkgTktType()) != 'undefined'){
                ipkgTktType = packageVm.pkgTktType();
            }
            if ((typeof packageVm.pkgTktMedia()) != 'undefined'){
                ipkgTktMedia = packageVm.pkgTktMedia();
            }
            ipkgName = packageVm.pkgName();

            var data = {'transItemIds':iitransItemIds.join(),
                'pkgQtyPerProd': ipkgQtyPerProd,
                'pkgName': ipkgName,
                'pkgTktType': ipkgTktType,
                 'pkgTktMedia' : ipkgTktMedia};

            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/verify-pkg",
                data: JSON.stringify(data),
                dataType: 'json', contentType: 'application/json',
                success:  function(result, textStatus, jqXHR)
                {
                    if(result.success)
                    {
                        verifyPkgCallBack(result);
                    }
                    else
                    {
                        alert(result.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                }
            });
            //$.submitFormRequest('/pkg/verifyPkg',data,verifyPkgCallBack,true);
        }
        function verifyPkgCallBack(data){
            if(data.message != ''&& data.message != 'null'&& data.message){
                alert(data.message);
            }
            if(data.success){

                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX +  "/secured/partner-inventory/create-pkg-do",
                    data: JSON.stringify({
                        transItemIds: packageVm.tranIdsMap.keys().join(),
                        pkgQtyPerProd: packageVm.pkgQtyPerProd(),
                        pkgTktType: packageVm.pkgTktType(),
                        pkgDesc: packageVm.pkgDesc(),
                        pkgName: packageVm.pkgName(),
                        pkgTktMedia: packageVm.pkgTktMedia()
                    }),
                    dataType: 'json', contentType: 'application/json',
                    success:  function(result, textStatus, jqXHR)
                    {
                        if(result.success)
                        {
                            //redirect
                            window.location.href = nvx.ROOT_PATH + "/confirm-package"
                        }
                        else
                        {
                            console.log('ERRORS: ' + result.message);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                });
            }
        }

        function addFormParam(key , value){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value",value);
            return hiddenField;
        }

        function createPkgCallBack(result){
            if(result.message != ''&& result.message != 'null'&& result.message){
                alert(result.message);
            }
            var ipkgvm = JSON.parse(result.data);
            packageVm.bundledItems.removeAll();
            packageVm.pkgExpiryDateStr(ipkgvm.pkgExpiryDateStr);
//        packageVm.pkgQtyPerProd(ipkgvm.pkgQtyPerProd);
//        packageVm.pkgDesc(ipkgvm.pkgDesc);
            packageVm.pkgName(ipkgvm.pkgName);
            packageVm.pkgTktType(ipkgvm.pkgTktType);
            packageVm.preTypeChange = packageVm.pkgTktType();
            packageVm.pkgTktMedia(ipkgvm.pkgTktMedia);
            packageVm.tktMediaAllowed = ipkgvm.pkgTktMedialAllowed;
            if(packageVm.tktMediaAllowed != '') {
                packageVm.pkgTktMedia(packageVm.tktMediaAllowed);
                var ticketMediaOption;
                var ticketMedias = packageVm.ticketMedias();
                for(var i = 0; i < ticketMedias.length; i++) {
                    if(ticketMedias[i].id == packageVm.tktMediaAllowed ) {
                        ticketMediaOption = ticketMedias[i];
                    }
                }
                packageVm.ticketMedias([ticketMediaOption]);
            }else {
                packageVm.ticketMedias([
                    { display: 'E-Ticket', id: 'ETicket'},
                    { display: 'PINCODE', id: 'Pincode'},
                    { display: 'Excel File', id: 'ExcelFile'}
                ]);
            }
            var arrayLength = ipkgvm.bundledItems.length;
            packageVm.tranIdsMap.clear();
            for (var i = 0; i < arrayLength; i++) {
                var bitem = BundledItem.fromJs(ipkgvm.bundledItems[i]);
                packageVm.bundledItems.push(bitem);
                var transLength =bitem.tranItems().length;
                for (var j = 0; j < transLength; j++) {
                    var tempItem = bitem.tranItems()[j];
                    packageVm.tranIdsMap.setItem(tempItem.itemId,tempItem.packagedQty);
                }
            }
            refreshTickedItem();
            $('#itemdv').show();
            //goToByScroll($('#itemdv'));
        };

        function goToByScroll(elmt){
            $('html,body').animate({
                    scrollTop: elmt.offset().top},
                'slow');
        };
        function addItem( itemId,unpackagedQty,el){
            if(el.checked){
                packageVm.tranIdsMap.setItem(itemId,unpackagedQty);
                submitPkg(true, itemId);
            }else{
                packageVm.tranIdsMap.removeItem(itemId);
                submitPkg(true);
            }
            refreshTickedItem();
        };
        (function loadFromEditPkgPage(){

            var ids = "";
            var pkgQtyPerProd = "";
            var pkgName = "";
            var pkgDesc = "";
            var pkgTktType = "";
            var pkgTktMedia = "";

            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX +  "/secured/partner-inventory/get-edit-package-info",
                success:  function(result, textStatus, jqXHR)
                {
                    if(result.success)
                    {
                        var data = result.data;

                        if(data != null) {
                            ids = data.transItemIds;
                            pkgQtyPerProd = data.pkgQtyPerProd;
                            pkgName = data.pkgName;
                            pkgDesc = data.pkgDesc;
                            pkgTktType = data.pkgTktType;
                            pkgTktMedia = data.pkgTktMedia;


                            if(pkgDesc != ""){
                                packageVm.pkgDesc(pkgDesc);
                            }
                            if(pkgTktType != ""){
                                packageVm.pkgTktType(pkgTktType);
                            }
                            if(pkgQtyPerProd != ""){
                                packageVm.pkgQtyPerProd(pkgQtyPerProd);
                            }
                            if(pkgTktMedia != "") {
                                packageVm.pkgTktMedia(pkgTktMedia);
                            }

                            if(ids != ""){
                                //TODO Need to save value when from edit page
                                var res = ids.split(",");
                                var len = res.length;
                                for(var i=0;i<len;i++){
                                    packageVm.tranIdsMap.setItem(res[i],res[i]);
                                }
                                var data = {'transItemIds':ids,
                                    'pkgQtyPerProd': pkgQtyPerProd,
                                    'pkgName': pkgName,
                                    'pkgDesc': pkgDesc,
                                    'pkgTktType': pkgTktType,
                                    'pkgTktMedia' : pkgTktMedia,
                                    'bundledItems' : []
                                };
                                //data.append('transItemIds', ids);
                                //            data.append('pkgQtyPerProd', pkgQtyPerProd);
                                //            data.append('pkgName', pkgName);
                                //            data.append('pkgDesc', pkgDesc);
                                //            data.append('pkgTktType', pkgTktType);
                                //            $.submitFormRequest('/pkg/createPkg',data,createPkgCallBack,false);
                                $.ajax({
                                    type: "POST",
                                    url: nvx.API_PREFIX + "/secured/partner-inventory/create-pkg",
                                    data: JSON.stringify({
                                        transItemIds:ids,
                                        pkgQtyPerProd: pkgQtyPerProd,
                                        pkgTktType: pkgTktType,
                                        pkgDesc: pkgDesc,
                                        pkgName: pkgName,
                                        pkgTktMedia: pkgTktMedia
                                    }),
                                    dataType: 'json', contentType: 'application/json',
                                    success:  function(result, textStatus, jqXHR)
                                    {
                                        if(result.success == true)
                                        {
                                            createPkgCallBack(result);
                                        }
                                        else
                                        {
                                            console.log('ERRORS: ' + result.message);
                                        }
                                    },
                                    error: function(jqXHR, textStatus)
                                    {
                                        alert('Unable to process your request. Please try again in a few moments.');
                                    }
                                });
                            }
                        }

                    }
                    else
                    {
                        console.log('ERRORS: ' + result.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                }
            });

        }
        )();

        function submitPkg(withQty, itemId){
            if(packageVm.tranIdsMap.length > 0){
                $('#itemdv').show();
            }
            if(packageVm.tranIdsMap.length == 0){
                packageVm.bundledItems.removeAll();
                packageVm.pkgTktType("");
                return;
            }
            //var data = new FormData();
            var iitransItemIds = packageVm.tranIdsMap.keys();
            var ipkgQtyPerProd = null;
            var ipkgTktType = null;
            var iitemIdToAdd = null;
            var ipkgName = null;
            var ipkgDesc = null;
            var ipkgTktMedia = null;
//            data.append('transItemIds', packageVm.tranIdsMap.keys());
            if (withQty && (typeof  packageVm.pkgQtyPerProd()) != 'undefined'){
                ipkgQtyPerProd = packageVm.pkgQtyPerProd();
            }
            if ((typeof packageVm.pkgTktType()) != 'undefined'){
                ipkgTktType = packageVm.pkgTktType();
            }
            if ((typeof itemId )!= 'undefined'){
                iitemIdToAdd= itemId;
            }
            if ((typeof packageVm.pkgName()) != 'undefined'){
                ipkgName = packageVm.pkgName();
            }
            if ((typeof packageVm.pkgDesc()) != 'undefined'){
                ipkgDesc = packageVm.pkgDesc();
            }
            if ((typeof packageVm.pkgTktMedia()) != 'undefined'){
                ipkgTktMedia = packageVm.pkgTktMedia();
            }

            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/create-pkg",
                data: JSON.stringify(
                    { transItemIds:iitransItemIds.join(),
                        pkgQtyPerProd: ipkgQtyPerProd,
                        pkgName: ipkgName,
                        itemIdToAdd: iitemIdToAdd,
                        pkgDesc: ipkgDesc,
                        pkgTktType: ipkgTktType,
                        pkgTktMedia: ipkgTktMedia
                    }),
                dataType: 'json', contentType: 'application/json',
                success:  function(result, textStatus, jqXHR)
                {
                    if(result.success == true)
                    {
                        createPkgCallBack(result);
                    }
                    else
                    {
                        console.log('ERRORS: ' + result.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                }
            });
//            alert(JSON.stringify(transIdMap));
            //$.submitFormRequest('/pkg/createPkg',data,createPkgCallBack,false);

        }

//    function TransMap(id,qty){
//        var self = this;
//        self.id = id;
//        self.qty = qty;
//    }
        function refreshTickedItem(){
            $('[name="itemId"]').each(function(index){
                if(packageVm.tranIdsMap.hasItem($(this).val())){
                    $(this).prop('checked', true);
                }else{
                    $(this).prop('checked', false);
                }
            });
            packageVm.rmEmptyBundleItem();
            if(packageVm.tranIdsMap.length > 0){
                $('#bundledNoteDv').show("slow");
                $('#itemNumNote').text(packageVm.tranIdsMap.length);
                $('#pkgQtyNote').text(packageVm.pkgQtyPerProd());
                $('#expDateStrNote').text(packageVm.pkgExpiryDateStr());
                $('#pkgTypeNote').text(packageVm.getTypeById(packageVm.pkgTktType()));
            }else{
                $('#pkgTypeNote').text(packageVm.getTypeById(packageVm.pkgTktType()));
                $('#bundledNoteDv').hide("slow");
            }
            if(packageVm.tranIdsMap.length == 0){
                packageVm.pkgTktType("");
            }
        };
        function HashTable() {
            this.length = 0;
            this.items = {};

            this.setItem = function(key, value)
            {
                var previous = undefined;
                if (this.hasItem(key)) {
                    previous = this.items[key];
                }
                else {
                    this.length++;
                }
                this.items[key] = value;
                return previous;
            }
            this.getItem = function(key) {
                return this.hasItem(key) ? this.items[key] : undefined;
            }
            this.hasItem = function(key)
            {
                return this.items.hasOwnProperty(key);
            }
            this.removeItem = function(key)
            {
                if (this.hasItem(key)) {
                    previous = this.items[key];
                    this.length--;
                    delete this.items[key];
                    return previous;
                }
                else {
                    return undefined;
                }
            }
            this.keys = function()
            {
                var keys = [];
                for (var k in this.items) {
                    if (this.hasItem(k)) {
                        keys.push(k);
                    }
                }
                return keys;
            }
            this.values = function()
            {
                var values = [];
                for (var k in this.items) {
                    if (this.hasItem(k)) {
                        values.push(this.items[k]);
                    }
                }
                return values;
            }
            this.each = function(fn) {
                for (var k in this.items) {
                    if (this.hasItem(k)) {
                        fn(k, this.items[k]);
                    }
                }
            }
            this.clear = function()
            {
                this.items = {};
                this.length = 0;
            }
        };

        window.addItem = addItem;
        window.refreshTickedItem =refreshTickedItem;
    });



})(window.nvx = window.nvx || {}, jQuery, ko);
