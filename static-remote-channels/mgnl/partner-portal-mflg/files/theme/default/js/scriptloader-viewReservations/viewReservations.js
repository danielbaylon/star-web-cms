(function(nvx, $, ko) {
    $(document).ready(function() {
        try{
            nvx.initStaticPageData();
            nvx.getWotSchedules(function(data){
                nvx.initPage(data);
            });
        }catch(e){
            nvx.showInitPageContent();
            nvx.spinner.stop();
            console.log('init page with schedule failed : '+e);
        }
        try{
            nvx.refreshWoTPinCode();
        }catch(e){
            try{
                nvx.refreshWoTPinCode();
            }catch(e){}
        }
    });

    nvx.initStaticPageData = function(){
        nvx.errorModel = new nvx.ErrorModel();
        var errorMessagePopup = $("#errorWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');
        ko.applyBindings(nvx.errorModel, document.getElementById('errorWindow'));
        window.errorMessagePopup = errorMessagePopup;
    };

    nvx.initPage = function(data) {
        nvx.spinner.start();

        nvx.searchCritirialModel = new nvx.searchCritirialModel();
        nvx.searchCritirialModel.initSearchModel(data);

        nvx.reservationModel = new nvx.ReservationModel();
        nvx.reservationModel.initScheduleList(data);

        nvx.splitReservationModel = new nvx.SplitReservationModel();

        nvx.reservationConfirmModel = new nvx.ReservationConfirmModel();

        nvx.confirmMsgModeal = new nvx.ConfirmMessageModel();

        nvx.purchaseReservationModel = new nvx.PurchaseReservationModel();

        ko.applyBindings(nvx.reservationModel , document.getElementById('viewReservationWindow'));
        ko.applyBindings(nvx.splitReservationModel,document.getElementById('splitReservationWindow'));
        ko.applyBindings(nvx.purchaseReservationModel,document.getElementById('purchaseReservationWindow'));
        ko.applyBindings(nvx.searchCritirialModel , document.getElementById('filterSectionContentDiv'));
        ko.applyBindings(nvx.reservationConfirmModel,document.getElementById('viewReservationConfirmWindow'));
        ko.applyBindings(nvx.confirmMsgModeal,document.getElementById('confirmMsgWindow'));

        nvx.initPageContent();
        nvx.showInitPageContent();
        nvx.spinner.stop();
    };

    nvx.showInitPageContent = function(){
        $('.pendingInitElementIndicator').show();
    };

    nvx.refreshWoTPinCode = function(){
        try{
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/refresh-wot-pin',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    nvx.spinner.stop();
                    if(data['success'] != undefined && data['success'] != null && data['success'] != '' && data['success'] == true){
                        console.log('Refresh WoT PIN code success.');
                        nvx.refreshWoTPinCodeCallback();
                    }else{
                        if(data['message'] != undefined && data['message'] != null && data['message'] != ''){
                            console.log('Refresh WoT PIN code failed : '+ data['message']);
                        }
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }catch(e){
            nvx.spinner.stop();
        }
    };

    nvx.refreshWoTPinCodeCallback = function(){
        window.refreshGrid();
    };

    nvx.getWotSchedules = function(callbackFunction){
        try{
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/get-schedule',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data['success'] != undefined && data['success'] != null && data['success'] != '' && data['success'] == true){
                        callbackFunction(data['data']);
                    }else{
                        if(data['message'] != undefined && data['message'] != null && data['message'] != ''){
                            console.log('Loading schedule failed : '+ data['message']);
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                        }
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }catch(e){
            nvx.spinner.stop();
        }
    };

    nvx.initPageContent = function(){
        var s = this;
        s.BASE_URL = nvx.API_PREFIX + '/secured/partner-wot/search-reservation';
        s.theUrl = s.BASE_URL;
        s.startDate = $('#startDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');
        s.endDate = $('#endDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');
        s.reservationStartDate = $('#reservationStartDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');
        s.reservationEndDate = $('#reservationEndDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');
        $($("input[name=showTimes]")[0]).prop('checked',true);
        $('#wotReservationStatus').kendoDropDownList({
            dataTextField: "name",
            dataValueField: "id",
            dataSource: [
                { name: "All", id: "All" },
                { name: "Confirmed", id: "Confirmed" },
                { name: "Cancelled", id: "Cancelled" },
                { name: "Reserved", id: "Reserved" },
                { name: "Partially Redeemed", id: "Partially Redeemed" },
                { name: "Fully Redeemed", id: "Fully Redeemed" },
                { name: "Released", id: "Released" }
            ]
        });

        s.refreshUrl = function() {
            var showTime = '';
            if($("input[name=showTimes]:checked").val() != undefined && $("input[name=showTimes]:checked").val() != null && $("input[name=showTimes]:checked").val() != ''){
                showTime = $("input[name=showTimes]:checked").val();
            }
            var statusSltVal = $('#wotReservationStatus').data('kendoDropDownList').value();
            var refreshCriteria = '?startDateStr=' + $('#startDate').val() + '&endDateStr=' + $('#endDate').val()+ '&reservationStartDateStr=' + $('#reservationStartDate').val() + '&reservationEndDateStr=' + $('#reservationEndDate').val()+ '&filterStatus=' + statusSltVal + '&showTime=' + showTime;

            s.theUrl = s.BASE_URL + refreshCriteria;

            s.refreshSearchCriteria($('#startDate').val(), $('#endDate').val(), $('#reservationStartDate').val(), $('#reservationEndDate').val(), statusSltVal, showTime);
            itemsdataSource.options.transport.read.url = s.theUrl;
            itemsdataSource.page(1);
        };

        s.refreshSearchCriteria = function(startDate, endDate, reservationStartDate, reservationEndDate, statusSltVal, showTime){
            $('#wotExportForm_startDateStr').val('');
            $('#wotExportForm_endDateStr').val('');
            $('#wotExportForm_reservationStartDateStr').val('');
            $('#wotExportForm_reservationEndDateStr').val('');
            $('#wotExportForm_filterStatus').val('');
            $('#wotExportForm_showTime').val('');
            $('#wotExportForm_startDateStr').val(startDate);
            $('#wotExportForm_endDateStr').val(endDate);
            $('#wotExportForm_reservationStartDateStr').val(reservationStartDate);
            $('#wotExportForm_reservationEndDateStr').val(reservationEndDate);
            $('#wotExportForm_filterStatus').val(statusSltVal);
            $('#wotExportForm_showTime').val(showTime);
        };

        $('#btnfilter').click(function(event){
            event.preventDefault();
            s.refreshUrl();
        });
        $('#btnfilterReset').click(function(event){
            event.preventDefault();
            $('#startDate').val('');
            $('#endDate').val('');
            $('#reservationStartDate').val('');
            $('#reservationEndDate').val('');
            $('#wotReservationStatus').data('kendoDropDownList').value('All');
            $($("input[name=showTimes]")[0]).prop('checked',true);
            $($("#displayCancelled")[0]).prop('checked',false);
        });
        s.pageSize = 10;
        var itemsdataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: s.theUrl,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: 'number'},
                        now: {type: 'number'},
                        cutOffDateTime: {type: 'number'},
                        reservationType: {type: 'string'},
                        eventDate: {type: 'number'},
                        receiptNo: {type: 'string'},
                        pinCode: {type: 'string'},
                        qty: {type: 'number'},
                        qtySold: {type: 'number'},
                        rdm: {type: 'number'},
                        unr: {type: 'number'},
                        status: {type: 'string'},
                        remarks: {type: 'string'},
                        releasedDate :  {type: 'string'},
                        eventStartTime: {type: 'number'},
                        eventTime: {type: 'number'},
                        eventEndTime: {type: 'number'},
                        isWasheddown: {type: 'number'},
                        qtyWashedDown: {type: 'number'}
                    }
                }
            }, pageSize: s.pageSize, serverSorting: true, serverPaging: true,
            requestStart: function () {
                nvx.spinner.start();
            },
            requestEnd: function () {
                nvx.spinner.stop();
            }
        });

        s.kGrid = $("#reservationGrid").kendoGrid({
            dataSource: itemsdataSource,
            dataBound: dataBound,
            columns: [{
                field: "eventDate",
                title: "Show Date",
                sortable: false,
                width: 140,
                template: function(dataItem) {
                    return kendo.toString(new Date(dataItem.eventDate), 'dddd, dd MMM yyyy') + '<br>' +  dataItem.showTimeDisplay;
                }
            },{
                field: "receiptNo",
                title: 'Receipt No',
                template: '<a href="javascript:viewReservation(#=id#);" >#=receiptNo#</a>',
                sortable: false,
                width: 150
            },{
                field: "pinCode",
                title: "PIN CODE",
                sortable: false,
                width: 100
            }, {
                field: "qty",
                title: "QTY",
                template: '#= qty - qtySold #',
                sortable: false,
                width: 50
            }, {
                field: "rdm",
                title: "RDM",
                sortable: false,
                width: 50
            },{
                field: "unr",
                title: "UNR",
                sortable: false,
                width: 50
            },{
                field: "qtyWashedDown",
                title: "WDN",
                sortable: false,
                width: 50,
                template: function(dataItem){
                    var wdn = dataItem['isWasheddown'];
                    if(wdn != undefined && wdn != null && wdn != '' && (wdn == '1' || wdn == 1)){
                        return parseInt(dataItem['qtyWashedDown']);
                    }
                    return '';
                }
            }, {
                field: "status",
                title: "Status",
                sortable: false,
                width: 100,
                template: function(dataItem) {
                    var xst = dataItem['status'];
                    if(xst != undefined && xst != null && xst != ''){
                        if('FullyPurchased' == xst || 'PartiallyPurchased' == xst){
                            return 'Reserved';
                        }else if('PartiallyRedeemed' == xst) {
                            return 'Partially Redeemed';
                        }else if('FullyRedeemed' == xst) {
                            return 'Fully Redeemed';
                        }
                        return xst;
                    }
                    return '';
                }
            }, {
                field: "remarks",
                title: "Remarks",
                sortable: false,
                template: function(dataItem) {
                    var _remarks = '';
                    if (dataItem.status == 'Released') {
                        _remarks += '<div>'+dataItem.remarks+'</div>';
                        _remarks += '<div>SYSTEM: Reservation released at '+dataItem.releasedDate+'</div>';
                    }else{
                        _remarks += '<div>'+dataItem.remarks+'</div>';
                    }
                    return '<div>'+_remarks+'</div>';
                }
            }, {
                field: "id",
                title: "&nbsp",
                sortable: false,
                width: 100,
                template: function(dataItem) {
                    var btnTmpl = "";
                    if(dataItem.reservationType == 'Purchase') {
                        //can split and cancel
                        if (dataItem.status == 'Confirmed') {
                            if (dataItem.now < dataItem.cutOffDateTime) {
                                if (dataItem.qty > 1) {
                                    btnTmpl = '<input id="btnSplit" type="button" class="btn btn-primary" value="Split" style="margin-bottom: 5px; width: 58px; white-space: nowrap;" onclick="nvx.splitReservationModel.popupSplitReservationWindow(' + dataItem.id + ')"/><br/>';
                                }
                            }
                            if (dataItem.now < dataItem.cutOffDateTime) {
                                btnTmpl += '<input id="btnCancel" type="button" class="btn btn-primary" value="Cancel" style="margin-bottom: 5px; width: 58px; white-space: nowrap;" onclick="nvx.reservationModel.cancelReservation(' + dataItem.id + ')"/><br/>';
                            }
                        }
                        if (dataItem.status == 'Confirmed' || dataItem.status == 'PartiallyRedeemed' || dataItem.status == 'FullyRedeemed') {
                            var eventEndDttm = dataItem.eventDate + (dataItem.eventEndTime * 1000);
                            if(dataItem.now < eventEndDttm){
                                btnTmpl += '<input id="btnPrint" type="button" class="btn btn-primary" value="Print" style="margin-bottom: 5px; width: 58px; white-space: nowrap;" onclick="nvx.reservationModel.printReservation(' + dataItem.id + ')"/>';
                            }
                        }
                    }else if(dataItem.reservationType == 'Reserve') {
                        if (( dataItem.status == 'Reserved' || dataItem.status == 'PartiallyPurchased' ) && dataItem.qty > 0 && dataItem.qty > dataItem.qtySold) {
                            var eventStartDttm = dataItem.eventDate + (dataItem.eventStartTime * 1000);
                            if(dataItem.now < eventStartDttm){
                                btnTmpl += '<input id="btnPurchase" type="button" class="btn btn-primary" value="Purchase" style="margin-bottom: 5px; width: 58px; white-space: nowrap;" onclick="nvx.purchaseReservationModel.popupPurchaseReservationWindow(' + dataItem.id + ')"/>';
                            }
                        }
                    }
                    return btnTmpl;
                }
            }],
            sortable: false, pageable: true
        });

        function dataBound(e){
            var grid = $("#reservationGrid").data("kendoGrid");
            var currentPage = itemsdataSource.page();
            var pageSize = itemsdataSource.pageSize();
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
            });
        }
        //view pop start

        var reservationPopup = $("#viewReservationWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var splitReservationPopup = $("#splitReservationWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var purchaseReservationPopup = $("#purchaseReservationWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var confirmPopup = $("#viewReservationConfirmWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var confirmMsgPopup = $("#confirmMsgWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        function exportReservation(){
            $('#wotExportForm').submit();
        };

        function viewReservation(id){
            if(id != undefined && id != null && id != '' && id >= 0) {
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/get-reservation/' + id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    success:  function(data, textStatus, jqXHR)
                    {
                        if (data['message'] != undefined && data['message'] != null && data['message'] != '') {
                            nvx.spinner.stop();
                            console.log('ERRORS: ' + data['message']);
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                        }else{
                            nvx.reservationModel.initReservationMode(data.data);
                            if(nvx.reservationModel.isEditable()) {
                                reservationPopup.title("Change WOT Reservation");
                            }else {
                                reservationPopup.title("View WOT Reservation");
                            }
                            reservationPopup.center().open();
                            nvx.spinner.stop();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }else {
                nvx.spinner.start();
                nvx.reservationModel.createNewModel();
                reservationPopup.title("New WOT Reservation");
                reservationPopup.center().open();
                nvx.spinner.stop();
            }
        }
        
        $('#btnNewReservation').click(function(event) {
            viewReservation(-1);
        });

        $('#btnExportAsExcel').click(function(){
            exportReservation();
        });

        $(".k-grid-header").css("padding-right", "0px");

        window.viewReservation = viewReservation;
        window.reservationPopup = reservationPopup;
        window.refreshGrid = s.refreshUrl;
        window.confirmPopup = confirmPopup;
        window.splitReservationPopup = splitReservationPopup;
        window.purchaseReservationPopup = purchaseReservationPopup;
        window.confirmMsgPopup = confirmMsgPopup;
    };

    nvx.searchCritirialModel = null;

    nvx.searchCritirialModel = function(){
        var s = this;
        s.showTimeSearchCriterialList = ko.observableArray([]);
        s.initShowTimeSearchCriterialList = function(result){
            var showTimesCrtieralDataMapInd = new Object();
            var showTimesCrtieralDataMap = [{  value : '', text : 'All' }];
            if(result != undefined && result != null && result != '') {
                //get all the dates
                $.each(result, function(idx, prod) {
                    if(prod != undefined && prod != null && prod != ''){
                        var lines = prod['eventLines'];
                        if(lines != undefined && lines != null && lines != ''){
                            $.each(lines, function(idx2, el) {
                                var lineId = el['eventLineId'];
                                if(lineId != undefined && lineId != null && lineId != ''){
                                    var exists = showTimesCrtieralDataMapInd[lineId];
                                    if(!(exists != undefined && exists != null && exists != '' && exists == 1)){
                                        showTimesCrtieralDataMap.push({
                                            value : el['eventLineId'],
                                            text : el['eventName']
                                        });
                                        showTimesCrtieralDataMapInd[lineId] = 1;
                                    }
                                }
                            });
                        }
                    }
                });
            }
            s.showTimeSearchCriterialList(showTimesCrtieralDataMap);
            showTimesCrtieralDataMapInd = null;
        };

        s.initSearchModel = function(data){
            if(data != undefined && data != null && data != ''){
                if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                    s.initShowTimeSearchCriterialList(data['products']);
                }
            }
        };
    };

    nvx.reservationModel =  null;

    nvx.ReservationModel = function(base) {
        var s = this;

        s.id = ko.observable();
        s.partnerId = ko.observable();
        s.partnerName = ko.observable();
        s.receiptNo = ko.observable();
        s.pinCode = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.rdm = ko.observable();
        s.unr = ko.observable();
        s.reservationType = ko.observable();
        s.remarks = ko.observable();
        s.createMode = ko.observable(false);
        s.updateMode = ko.observable(false);
        s.splitMode = ko.observable(false);
        s.createdBy = ko.observable();
        s.createdByUsername = ko.observable();
        s.createdDate = ko.observable();
        s.notifInfo = ko.observable("");
        s.showTime = ko.observable();
        s.showDate = ko.observable();
        s.productShowScheduleList = ko.observableArray([]);
        s.showTimeList = ko.observableArray([]);
        s.showTimeDisplay = ko.observable("");
        s.eventLineId = ko.observable("");
        s.productId = ko.observable("");
        s.penaltyCharge = ko.observable("");
        s.eventDate = ko.observable(-1);
        s.cutOffDateTime = ko.observable(-1);
        s.now = ko.observable(-1);

        s.itemId = ko.observable();
        s.mediaTypeId = ko.observable();
        s.eventGroupId = ko.observable();
        s.openValidityEndDate = ko.observable();
        s.openValidityStartDate = ko.observable();

        s.qtyFromDB = ko.observable();
        s.showDateFromDB = ko.observable();
        s.remarksFromDB = ko.observable();
        s.showTimeDisplayFromDB = ko.observable();

        s.eventStartTime = ko.observable();
        s.eventTime = ko.observable();
        s.eventEndTime = ko.observable();
        s.eventCapacityId = ko.observable();

        s.splitSrcId = ko.observable();
        s.splitSrcQty = ko.observable();
        s.purchaseSrcId = ko.observable();
        s.purchaseSrcQty = ko.observable();

        s.isEditable = ko.computed(function(){
            if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                if(s.status() != undefined && s.status() != null && s.status() != ''){
                    if(s.reservationType() == 'Purchase' && s.status() == 'Confirmed'){
                        if(s.now() < s.cutOffDateTime()){
                            return true;
                        }
                    }
                }
            }else{
                return true;
            }
            return false;
        });

        s.isAnExistingOrder = ko.computed(function(){
            if(s.status() != undefined && s.status() != null && s.status() != ''){
                if(s.id() != undefined && s.id() != null && s.id() != ''){
                    return true;
                }
            }
            return false;
        });

        s.reset = function(){
            s.partnerId('');
            s.partnerName('');
            s.createMode(false);
            s.updateMode(false);
            s.splitMode(false);
            s.id(-1);
            s.receiptNo('');
            s.pinCode('');
            s.showTime('');
            s.showTimeDisplay('');
            s.showTimeDisplayFromDB('');
            s.eventLineId('');
            s.productId('');
            s.showDate('');
            s.showDateFromDB('');
            s.status('');
            s.qty(0);
            s.qtyFromDB(0);
            s.rdm(0);
            s.unr(0);
            s.remarks('');
            s.remarksFromDB('');
            s.createdBy('');
            s.createdByUsername('');
            s.createdDate('');
            s.itemId('');
            s.mediaTypeId('');
            s.eventGroupId('');
            s.openValidityEndDate('');
            s.openValidityStartDate('');
            s.eventDate(-1);
            s.cutOffDateTime(-1)
            s.now(-1);

            s.eventStartTime('');
            s.eventTime('');
            s.eventEndTime('');
            s.eventCapacityId('');

            s.splitSrcId('');
            s.splitSrcQty('');
            s.purchaseSrcId('');
            s.purchaseSrcQty('');

        };

        s.initScheduleList = function (data){
            if(data != undefined && data != null && data != ''){
                if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                    s.productShowScheduleList(data['products']);
                }
            }
        };

        s.refreshScheduleList = function (){
            nvx.getWotSchedules(function(data){
                if(data != undefined && data != null && data != ''){
                    if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                        s.productShowScheduleList(data['products']);
                    }
                }
            });
        };

        s.initReservationMode = function(data) {
            s.reset();
            if(data.id != undefined && data.id != null && data.id != '' && data.id != -1 && data.id != -1){
                s.updateMode(true);
            }else{
                s.createMode(true);
            }
            s.id(data.id);
            s.partnerId(data.partnerId);
            s.partnerName(data.partnerName);
            s.receiptNo(data.receiptNo);
            s.pinCode(data.pinCode);
            s.showTime(data.productId+'-'+data.eventLineId);
            s.showTimeDisplay(data.showTimeDisplay);
            s.showTimeDisplayFromDB(data.showTimeDisplay);
            s.eventLineId(data.eventLineId);
            s.productId(data.productId);
            s.showDate(data.showDate);
            s.showDateFromDB(data.showDate);
            s.status(data.status);
            s.qty(parseInt(data.qty) - parseInt(data.rdm));
            s.qtyFromDB(data.qty);
            s.rdm(data.rdm);
            s.unr(data.unr);
            s.remarks(data.remarks);
            s.remarksFromDB(data.remarks);
            s.createdBy(data.createdBy);
            s.createdByUsername(data.createdByUsername);
            s.createdDate(data.createdDate);
            s.itemId(data.itemId);
            s.mediaTypeId(data.mediaTypeId);
            s.eventGroupId(data.eventGroupId);
            s.openValidityEndDate(data.openValidityEndDate);
            s.openValidityStartDate(data.openValidityStartDate);
            s.eventDate(data.eventDate);
            s.cutOffDateTime(data.cutOffDateTime)
            s.now(data.now);

            s.eventStartTime(data.eventStartTime);
            s.eventTime(data.eventTime);
            s.eventEndTime(data.eventEndTime);
            s.eventCapacityId(data.eventCapacityId);

            s.splitSrcId(data.splitSrcId);
            s.splitSrcQty(data.splitSrcQty);
            s.purchaseSrcId(data.purchaseSrcId);
            s.purchaseSrcQty(data.purchaseSrcQty);

            s.reservationType(data.reservationType);

            s.initShowDate();
        }

        s.createNewModel = function() {
            s.reset();
            s.createMode(true);
            s.initShowDate();
        };

        s.printReservation = function(id) {
            if(id != undefined && id != null && id != '' && id >= 0) {
                var _w = $( window ).width();
                var _h = $( window ).height();

                var _left = (_w - 960)/2;

                if(_left < 0){
                    _left = 200;
                }

                var url = '/partner-portal-mflg/print-reservations?id='+id;
                var ticketPrintWindow = window.open(url, 'ticketPrintWindow', 'resizable=no,top=50,left='+_left+',width=620,height=1024');
                ticketPrintWindow.focus(); // necessary for IE >= 10
            }
        };

        s.cancelReservation = function(id) {
            s.id('');
            s.id(id); //set the id being cancelled now
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/validate-cancel-reservation/' + s.id(),
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    nvx.spinner.stop();
                    if(data.success) {
                        //show the confirm message here if validation is successful
                        nvx.reservationConfirmModel.initConfirmMode('CC', data.data, s.id());
                        window.confirmPopup.title("Cancel Purchase");
                        window.confirmPopup.center().open();
                    } else {
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                }
            });
        };

        s.proceedCancelReservation = function() {
            var _input_id = s.id();
            var _input_refund = nvx.reservationConfirmModel.depositToBeRefunded();
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/cancel-reservation/'+_input_id+'/'+_input_refund,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        window.refreshGrid();
                        nvx.depositBalanceModel.getLatestDepositBalance();
                        window.confirmPopup.close();
                        s.closeModal();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation is now cancelled!', function(){});
                    }
                    else
                    {
                        window.refreshGrid();
                        nvx.depositBalanceModel.getLatestDepositBalance();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.initShowDate = function() {
            var result = s.productShowScheduleList();
            if(result != undefined && result != null && result != '' ) {
                var productDatesMap = [];
                var enableDates = [];
                enableDates.push(true);
                //get all the dates
                $.each(result, function(idx, prod) {
                    if(prod != undefined && prod != null && prod != '' ) {
                        var lines = prod['eventLines'];
                        if(lines != undefined && lines != null && lines != '' ) {
                            $.each(lines, function(idx2, el) {
                                var eds = el['eventDates'];
                                if(eds != undefined && eds != null && eds != '' ) {
                                    $.each(eds, function(idx3, ed) {
                                        var eventDate = ed.date;
                                        if(productDatesMap[eventDate] == null) {
                                            var dtArr = eventDate.split('/');
                                            enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);
                                            productDatesMap[eventDate] = [];
                                        }
                                        var product = {
                                            eventLine: el,
                                            productId: prod.productId,
                                            itemId : prod.itemId,
                                            mediaTypeId : prod.mediaTypeId,
                                            eventGroupId : prod.eventGroupId,
                                            openValidityStartDate : prod.openValidityStartDate,
                                            openValidityEndDate : prod.openValidityEndDate
                                        };

                                        productDatesMap[eventDate].push(product);
                                    });
                                }
                            });
                        }
                    }
                });

                var $ebDate = $('#showDate');
                $ebDate.pickadate().pickadate('picker').stop();
                setTimeout(function() {
                    $ebDate.pickadate({
                        format: 'dd/mm/yyyy',
                        container: '.main-content-section-container',
                        disable: enableDates
                    });
                }, 300);

                s.showDate.subscribe(function(newSelectedDate) {
                    s.updateShowTimeList(productDatesMap, newSelectedDate);
                });

                s.updateShowTimeList(productDatesMap, s.showDate());
            }
        };

        s.updateShowTimeList = function(productDatesMap, newSelectedDate) {
            var updatedShowTimeList = [];
            s.showTimeList([]);
            if(newSelectedDate != "") {
                var foundDate = false;
                if(productDatesMap[newSelectedDate] != undefined && productDatesMap[newSelectedDate] != null){
                    foundDate = true;
                }
                if(!foundDate){
                    newSelectedDate = new Date();
                }
                if(productDatesMap[newSelectedDate] != undefined && productDatesMap[newSelectedDate] != null){
                    $.each(productDatesMap[newSelectedDate], function(idx, prod) {
                        var showTime = new Object();
                        showTime.itemId = prod.itemId;
                        showTime.productId = prod.productId;
                        showTime.mediaTypeId = prod.mediaTypeId;
                        showTime.text = prod.eventLine.eventName;
                        showTime.eventGroupId = prod.eventGroupId;
                        showTime.eventLineId = prod.eventLine.eventLineId;
                        showTime.openValidityEndDate = prod.openValidityEndDate;
                        showTime.openValidityStartDate = prod.openValidityStartDate;
                        showTime.value = showTime.productId + "-" + showTime.eventLineId;
                        showTime.eventStartTime = prod.eventLine.eventStartTime;
                        showTime.eventTime = prod.eventLine.eventTime;
                        showTime.eventEndTime = prod.eventLine.eventEndTime;
                        showTime.eventCapacityId = prod.eventLine.eventCapacityId;
                        updatedShowTimeList.push(showTime);
                    });
                }
            }

            s.showTimeList(updatedShowTimeList);
        };

        s.setShowTimeInfo = function() {
            $.each(s.showTimeList(), function(idx, mdl) {
                if(s.showTime() == mdl.value) {
                    s.showTimeDisplay(mdl.text);
                    s.productId(mdl.productId);
                    s.eventLineId(mdl.eventLineId);
                    s.itemId(mdl.itemId);
                    s.mediaTypeId(mdl.mediaTypeId);
                    s.eventGroupId(mdl.eventGroupId);
                    s.openValidityEndDate(mdl.openValidityEndDate);
                    s.openValidityStartDate(mdl.openValidityStartDate);
                    s.eventStartTime(mdl.eventStartTime);
                    s.eventTime(mdl.eventTime);
                    s.eventEndTime(mdl.eventEndTime);
                    s.eventCapacityId(mdl.eventCapacityId);
                }
            });
        };

        s.doSaveValidate = function() {
            //Validations

            //Validate that there's show date
            if(s.showDate() == '') {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Show Date.', function(){});
                return false;
            }

            //Validate that there's show time
            if(s.showTime() == '') {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Show Time.', function(){});
                return false;
            }

            //Validate that quantity is 1 and above
            if(s.qty() == '' || s.qty() == 0) {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please enter a minimum Quantity of 1.', function(){});
                return false;
            }

            //validate if the quantity is number
            var numberRegex = /^\d+$/;

            if(!numberRegex.test(s.qty())) {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please enter a valid Quantity', function(){});
                return false;
            }

            var productFound = false;
            var showTimeList = s.showTimeList();
            if(showTimeList != undefined || showTimeList != null || showTimeList.length != 0){
                var showTimeListCount = showTimeList.length;
                for(var i = 0 ; i < showTimeListCount ; i++){
                    var showTime = showTimeList[i];
                    if(showTime != undefined && showTime != null){
                        if(showTime.value == s.showTime()){
                            var eventShowDate = kendo.parseDate(s.showDate(), 'dd/MM/yyyy');
                            var eventShowDateLong = eventShowDate.getTime();
                            var eventStartTime = showTime.eventStartTime;
                            var eventStartDttm = eventShowDateLong + (eventStartTime * 1000);
                            productFound = true;
                            var now = new Date();
                            var nowLong = now.getTime();
                            if(nowLong > eventStartDttm){
                                nvx.showMsgWindowWithCallBack('Error Message', 'Please choose a valid upcoming show date/time.', function(){});
                                return false;
                            }
                        }
                    }
                }
            }
            if(!productFound){
                nvx.showMsgWindowWithCallBack('Error Message', 'Please choose a valid upcoming show date/time.', function(){});
                return false;
            }

            return true;
        };

        s.refreshWoTReservationDetails = function(id){
            try{
                if(!(id != undefined && id != null && id != '' && id != -1)){
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/refresh-reservation/'+id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            console.log('Refresh WoT reservation details successful for '+id);
                        }
                        else
                        {
                            console.log('Refresh WoT reservation details for '+id+' is failed : '+data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        console.log('Refresh WoT reservation details for '+id+' is failed');
                    },
                    complete: function() {
                    }
                });
            }catch(e){
                console.log('Refresh WoT reservation details for '+id+' is failed');
            }
        };

        s.saveReservation = function() {
            nvx.spinner.start();
            var apiURL =  nvx.API_PREFIX + '/secured/partner-wot/save-reservation';
            $.ajax({
                type: "POST",
                url: apiURL,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                data: JSON.stringify({
                    id : s.id(),
                    qty : s.qty(),
                    showTime : s.showTime(),
                    showTimeDisplay : s.showTimeDisplay(),
                    productId : s.productId(),
                    eventLineId : s.eventLineId(),
                    showDate : s.showDate(),
                    remarks : s.remarks(),
                    itemId : s.itemId(),
                    mediaTypeId : s.mediaTypeId(),
                    eventGroupId : s.eventGroupId(),
                    openValidityEndDate : s.openValidityEndDate(),
                    openValidityStartDate : s.openValidityStartDate(),
                    eventStartTime : s.eventStartTime(),
                    eventTime : s.eventTime(),
                    eventEndTime : s.eventEndTime(),
                    eventCapacityId : s.eventCapacityId()
                }),
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    s.closeModal();
                    window.confirmPopup.close();
                    if(data.success)
                    {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                            s.refreshWoTReservationDetails(wot['id']);
                        }
                        window.refreshGrid();
                        nvx.depositBalanceModel.getLatestDepositBalance();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation is successful!', function(){});
                    }else {
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    s.closeModal();
                    window.confirmPopup.close();
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.updaeReservation = function() {
            nvx.spinner.start();
            var apiURL =  nvx.API_PREFIX + '/secured/partner-wot/update-reservation';
            $.ajax({
                type: "POST",
                url: apiURL,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                data: JSON.stringify({
                    id : s.id(),
                    qty : s.qty(),
                    showTime : s.showTime(),
                    showTimeDisplay : s.showTimeDisplay(),
                    productId : s.productId(),
                    eventLineId : s.eventLineId(),
                    showDate : s.showDate(),
                    remarks : s.remarks(),
                    itemId : s.itemId(),
                    mediaTypeId : s.mediaTypeId(),
                    eventGroupId : s.eventGroupId(),
                    openValidityEndDate : s.openValidityEndDate(),
                    openValidityStartDate : s.openValidityStartDate(),
                    eventStartTime : s.eventStartTime(),
                    eventTime : s.eventTime(),
                    eventEndTime : s.eventEndTime(),
                    eventCapacityId : s.eventCapacityId()
                }),
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    s.closeModal();
                    window.confirmPopup.close();
                    if(data.success) {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                            s.refreshWoTReservationDetails(wot['id']);
                        }
                        window.refreshGrid();
                        nvx.depositBalanceModel.getLatestDepositBalance();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation modification is successful!', function(){});
                    }else{
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    s.closeModal();
                    window.confirmPopup.close();
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.verifyNewReservation = function(){
            if(s.doSaveValidate()) {
                nvx.spinner.start();
                s.setShowTimeInfo(); //set the show time display here
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/validate-new-reservation',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    data: JSON.stringify({
                        id : s.id(),
                        qty : s.qty(),
                        showTime : s.showTime(),
                        showTimeDisplay : s.showTimeDisplay(),
                        productId : s.productId(),
                        eventLineId : s.eventLineId(),
                        showDate : s.showDate(),
                        remarks : s.remarks(),
                        itemId : s.itemId(),
                        mediaTypeId : s.mediaTypeId(),
                        eventGroupId : s.eventGroupId(),
                        openValidityEndDate : s.openValidityEndDate(),
                        openValidityStartDate : s.openValidityStartDate(),
                        eventStartTime : s.eventStartTime(),
                        eventTime : s.eventTime(),
                        eventEndTime : s.eventEndTime(),
                        eventCapacityId : s.eventCapacityId()
                    }),
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            //show the confirm message here if validation is successful
                            nvx.reservationConfirmModel.initConfirmMode('S', data.data, s.id());
                            window.confirmPopup.title("Confirm Reservation");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }else {
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){
                                reservationPopup.close();
                            });
                        }
                    },
                    error: function(jqXHR, textStatus){
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        };

        s.verifyUpdateReservation = function() {
            if(s.doSaveValidate()) {
                nvx.spinner.start();
                s.setShowTimeInfo(); //set the show time display here
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/validate-update-reservation',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    data: JSON.stringify({
                        id : s.id(),
                        qty : s.qty(),
                        showTime : s.showTime(),
                        showTimeDisplay : s.showTimeDisplay(),
                        productId : s.productId(),
                        eventLineId : s.eventLineId(),
                        showDate : s.showDate(),
                        remarks : s.remarks(),
                        itemId : s.itemId(),
                        mediaTypeId : s.mediaTypeId(),
                        eventGroupId : s.eventGroupId(),
                        openValidityEndDate : s.openValidityEndDate(),
                        openValidityStartDate : s.openValidityStartDate(),
                        eventStartTime : s.eventStartTime(),
                        eventTime : s.eventTime(),
                        eventEndTime : s.eventEndTime(),
                        eventCapacityId : s.eventCapacityId()
                    }),
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            //show the confirm message here if validation is successful
                            nvx.reservationConfirmModel.initConfirmMode('U', data.data, s.id());
                            window.confirmPopup.title("Confirm Reservation");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }else{
                            nvx.spinner.stop();
                            console.log('ERRORS: ' + data['message']);
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        }

        s.closeModal = function() {
            window.reservationPopup.close();
        };
    };

    nvx.SplitReservationModelItem = function(data){
        var s = this;
        s.id = ko.observable();
        s.receiptNo = ko.observable();
        s.showDateInfo = ko.observable();
        s.showTimeInfo = ko.observable();
        s.pinCodeQty = ko.observable();
        s.pinCodeType = ko.observable();
        s.status = ko.observable();

        s.id(data.id);
        s.receiptNo(data.receiptNo);
        s.showDateInfo(data.showDateInfo);
        s.showTimeInfo(data.showTimeInfo);
        s.pinCodeQty(data.pinCodeQty);
        s.pinCodeType(data.pinCodeType);
        s.status(data.status);
    };

    nvx.splitReservationModel = null;

    nvx.SplitReservationModel = function(base){
        var s = this;

        s.id = ko.observable();
        s.partnerId = ko.observable();
        s.partnerName = ko.observable();
        s.receiptNo = ko.observable();
        s.pinCode = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.rdm = ko.observable();
        s.unr = ko.observable();
        s.remarks = ko.observable();
        s.createMode = ko.observable(false);
        s.updateMode = ko.observable(false);
        s.splitMode = ko.observable(false);
        s.createdBy = ko.observable();
        s.createdByUsername = ko.observable();
        s.createdDate = ko.observable();
        s.notifInfo = ko.observable("");
        s.showTime = ko.observable();
        s.showDate = ko.observable();
        s.productShowScheduleList = ko.observableArray([]);
        s.showTimeList = ko.observableArray([]);
        s.showTimeDisplay = ko.observable("");
        s.eventLineId = ko.observable("");
        s.productId = ko.observable("");
        s.penaltyCharge = ko.observable("");
        s.eventDate = ko.observable(-1);
        s.cutOffDateTime = ko.observable(-1);
        s.now = ko.observable(-1);

        s.itemId = ko.observable();
        s.mediaTypeId = ko.observable();
        s.eventGroupId = ko.observable();
        s.openValidityEndDate = ko.observable();
        s.openValidityStartDate = ko.observable();

        s.qtyFromDB = ko.observable();
        s.showDateFromDB = ko.observable();
        s.remarksFromDB = ko.observable();
        s.showTimeDisplayFromDB = ko.observable();

        s.eventStartTime = ko.observable();
        s.eventTime = ko.observable();
        s.eventEndTime = ko.observable();
        s.eventCapacityId = ko.observable();

        s.pendingSplitQty = ko.observable(0);
        s.inputPendingSplitQty = ko.observable(0);
        s.pendingSplitOriginalItem = ko.observable({});
        s.pendingSplitNewItem = ko.observable({});

        s.isNotInSplitting = ko.observable(true);

        s.isEditable = ko.computed(function(){
            if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                if(s.status() != undefined && s.status() != null && s.status() != ''){
                    if(s.status() == 'Confirmed') {
                        if(s.now() < s.cutOffDateTime()){
                            return true;
                        }
                    }
                }
            }else{
                return true;
            }
            return false;
        });

        s.reset = function(){
            s.isNotInSplitting(true);
            s.createMode(false);
            s.updateMode(false);
            s.splitMode(false);
            s.partnerId('');
            s.partnerName('');
            s.id(-1);
            s.receiptNo('');
            s.pinCode('');
            s.showTime('');
            s.showTimeDisplay('');
            s.showTimeDisplayFromDB('');
            s.eventLineId('');
            s.productId('');
            s.showDate('');
            s.showDateFromDB('');
            s.status('');
            s.qty(0);
            s.qtyFromDB(0);
            s.rdm(0);
            s.unr(0);
            s.remarks('');
            s.remarksFromDB('');
            s.createdBy('');
            s.createdByUsername('');
            s.createdDate('');
            s.itemId('');
            s.mediaTypeId('');
            s.eventGroupId('');
            s.openValidityEndDate('');
            s.openValidityStartDate('');
            s.eventDate(-1);
            s.cutOffDateTime(-1)
            s.now(-1);

            s.eventStartTime('');
            s.eventTime('');
            s.eventEndTime('');
            s.eventCapacityId('');

            s.pendingSplitQty(0);
            s.inputPendingSplitQty(0);
            s.pendingSplitOriginalItem({});
            s.pendingSplitNewItem({});
        };

        s.initSplitMode = function(data){
            s.reset();
            s.splitMode(true);
            s.id(data.id);
            s.partnerId(data.partnerId);
            s.partnerName(data.partnerName);
            s.receiptNo(data.receiptNo);
            s.pinCode(data.pinCode);
            s.showTime(data.productId+'-'+data.eventLineId);
            s.showTimeDisplay(data.showTimeDisplay);
            s.showTimeDisplayFromDB(data.showTimeDisplay);
            s.eventLineId(data.eventLineId);
            s.productId(data.productId);
            s.showDate(data.showDate);
            s.showDateFromDB(data.showDate);
            s.status(data.status);
            s.qty(parseInt(data.qty) - parseInt(data.rdm));
            s.qtyFromDB(data.qty);
            s.rdm(data.rdm);
            s.unr(data.unr);
            s.remarks(data.remarks);
            s.remarksFromDB(data.remarks);
            s.createdBy(data.createdBy);
            s.createdByUsername(data.createdByUsername);
            s.createdDate(data.createdDate);
            s.itemId(data.itemId);
            s.mediaTypeId(data.mediaTypeId);
            s.eventGroupId(data.eventGroupId);
            s.openValidityEndDate(data.openValidityEndDate);
            s.openValidityStartDate(data.openValidityStartDate);

            s.eventStartTime(data.eventStartTime);
            s.eventTime(data.eventTime);
            s.eventEndTime(data.eventEndTime);
            s.eventCapacityId(data.eventCapacityId);

            s.eventDate(data.eventDate);
            s.cutOffDateTime(data.cutOffDateTime)
            s.now(data.now);

            var selectedSplitQty = 0;
            var _pendingSplitList = data['pendingSplitList'];
            if(_pendingSplitList != undefined && _pendingSplitList != null && _pendingSplitList != ''){
                //
            }
            s.pendingSplitQty(selectedSplitQty);
            s.inputPendingSplitQty(selectedSplitQty);


            s.pendingSplitOriginalItem(new nvx.SplitReservationModelItem({
                id : s.id(),
                receiptNo : s.receiptNo(),
                showDateInfo : kendo.toString(new Date(s.eventDate()), 'dddd, dd MMM yyyy'),
                showTimeInfo : s.showTimeDisplay(),
                pinCodeQty : s.qty() - s.pendingSplitQty(),
                pinCodeType : 'Original',
                status : s.status()
            }));
        };

        s.popupSplitReservationWindow = function(id){
            nvx.spinner.start();
            s.id(id); //set the id being cancelled now
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/get-reservation/' + s.id(),
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != ''){
                            nvx.splitReservationModel.initSplitMode(wot);
                            if(nvx.splitReservationModel.isEditable()){
                                window.splitReservationPopup.title("Split Reservation");
                                window.splitReservationPopup.center().open();
                                nvx.spinner.stop();
                            }else{
                                var msgShown = false;
                                nvx.splitReservationModel.reset();
                                if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                                    if(s.status() != undefined && s.status() != null && s.status() != ''){
                                        if(s.status() == 'Confirmed') {
                                            if(s.now() >= s.cutOffDateTime()){
                                                nvx.showMsgWindow('Error Message','Split reservation is not available for tickets after cut-off time.');
                                                msgShown = true;
                                            }
                                        }else{
                                            nvx.showMsgWindow('Error Message','Split reservation is not available for tickets in '+ s.status() +'.');
                                            msgShown = true;
                                        }
                                    }
                                }
                                if(!msgShown){
                                    nvx.showMsgWindow('Error Message','Split reservation is not available.');
                                }
                            }
                        }
                    }
                    else
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){
                        });
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.applySplitQty = function(){
            if((!isNaN(s.inputPendingSplitQty())) && s.inputPendingSplitQty() > 0 && s.inputPendingSplitQty() < s.qty()){
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/verify-split-reservation/' + s.id() + '/' + s.inputPendingSplitQty(),
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            var wot = data['data'];
                            if(wot != undefined && wot != null && wot != ''){
                                s.pendingSplitNewItem(new nvx.SplitReservationModelItem({
                                    id : wot['id'],
                                    receiptNo : wot['receiptNum'],
                                    showDateInfo : kendo.toString(new Date(wot['eventDate']), 'dddd, dd MMM yyyy'),
                                    showTimeInfo : wot['eventName'],
                                    pinCodeQty : wot['qty'],
                                    pinCodeType : 'New',
                                    status : wot['status']
                                }));
                                s.pendingSplitQty(wot['qty']);
                                s.pendingSplitOriginalItem().pinCodeQty(s.qty() - wot['qty']);
                            }else{
                                nvx.showMsgWindow('Error Message','Unable to perform split reservation at this moment.');
                            }
                        }else{
                            nvx.spinner.stop();
                            nvx.showMsgWindow('Error Message', data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus){
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }else{
                if(isNaN(s.inputPendingSplitQty())){
                    nvx.showMsgWindow('Error Message', 'Please enter correct quantity.');
                    return false;
                }
                if(s.inputPendingSplitQty() <= 0){
                    nvx.showMsgWindow('Error Message', 'Please enter at least 1 quantity.');
                    return false;
                }
                if(s.inputPendingSplitQty() >= s.qty()){
                    nvx.showMsgWindow('Error Message', 'Quantity to be split must be less than original quantity.');
                    return false;
                }
            }
        };

        s.clearSplitQty = function(){
            if(s.pendingSplitNewItem() != undefined && s.pendingSplitNewItem() != null && s.pendingSplitNewItem() != '' && s.pendingSplitNewItem()['id'] != undefined && s.pendingSplitNewItem()['id'] != null && s.pendingSplitNewItem()['id'] != '' && s.pendingSplitNewItem().id() > 0){
                if(s.pendingSplitNewItem().status() == 'Pending'){
                    nvx.spinner.stop();
                    $.ajax({
                        type: "POST",
                        url: nvx.API_PREFIX + '/secured/partner-wot/cancel-split-reservation/' + s.pendingSplitNewItem().id(),
                        headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                        cache: false, dataType: 'json', contentType: 'application/json',
                        success:  function(data, textStatus, jqXHR)
                        {
                            if(data.success){
                                s.inputPendingSplitQty(0);
                                s.pendingSplitQty(0);
                                s.pendingSplitNewItem('');
                                s.pendingSplitOriginalItem().pinCodeQty(s.qty() - s.pendingSplitQty());
                            }else{
                                nvx.spinner.stop();
                                nvx.showMsgWindow('Error Message', data['message']);
                            }
                        },
                        error: function(jqXHR, textStatus){
                            nvx.spinner.stop();
                            nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                        },
                        complete: function() {
                            nvx.spinner.stop();
                        }
                    });
                }
            }else{
                s.inputPendingSplitQty(0);
                s.pendingSplitQty(0);
                s.pendingSplitNewItem('');
                s.pendingSplitOriginalItem().pinCodeQty(s.qty() - s.pendingSplitQty());
            }
        };

        // refresh splitted reservation details
        s.refreshSplittedWoTReservationDetails = function(id){
            try{
                if(!(id != undefined && id != null && id != '' && id != -1)){
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/refresh-reservation/'+id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            console.log('Refresh WoT reservation details successful for '+id);
                        }
                        else
                        {
                            console.log('Refresh WoT reservation details for '+id+' is failed : '+data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        console.log('Refresh WoT reservation details for '+id+' is failed');
                    },
                    complete: function() {
                    }
                });
            }catch(e){
                console.log('Refresh WoT reservation details for '+id+' is failed');
            }
        };

        // save split reservation
        s.splitReservation = function() {
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/confirm-split-reservation/'+s.pendingSplitNewItem().id(),
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    window.splitReservationPopup.center().open();
                    var wot = data['data'];
                    if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                        s.pendingSplitNewItem().status(wot['status']);
                    }
                    if(data.success)
                    {
                        window.splitReservationPopup.close();
                        s.refreshSplittedWoTReservationDetails(wot['id']);
                        window.refreshGrid();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'You have successfully split your reservation.', function(){ });
                    }else{
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    s.isNotInSplitting(true);
                },
                complete: function() {
                    nvx.spinner.stop();
                    s.isNotInSplitting(true);
                }
            });
        };

        // validate split reservation
        s.doSplitReservationValidation = function() {
            if((s.pendingSplitNewItem()['id'] != undefined && s.pendingSplitNewItem()['id'] != null && s.pendingSplitNewItem()['id'] != '' && s.pendingSplitNewItem()['id'] != -1)){
                nvx.showConfirmMsgWindow('Confirm Split Reservation', 'Please confirm to split reservation', function(){
                    s.isNotInSplitting(false);
                    s.splitReservation();
                });
            }else{
                nvx.showMsgWindow('Error Message', 'Please add at least 1 quantity to split.');
            }
        };

        s.closeModal = function() {
            s.clearSplitQty();
            window.splitReservationPopup.close();
        };
    };

    /*******************************************************************************************************************************************************************************************************/

    nvx.PurchaseReservationModelItem = function(data){
        var s = this;
        s.id = ko.observable();
        s.receiptNo = ko.observable();
        s.showDateInfo = ko.observable();
        s.showTimeInfo = ko.observable();
        s.pinCodeQty = ko.observable();
        s.pinCodeType = ko.observable();
        s.status = ko.observable();

        s.id(data.id);
        s.receiptNo(data.receiptNo);
        s.showDateInfo(data.showDateInfo);
        s.showTimeInfo(data.showTimeInfo);
        s.pinCodeQty(data.pinCodeQty);
        s.pinCodeType(data.pinCodeType);
        s.status(data.status);
    };

    nvx.purchaseReservationModel = null;

    nvx.PurchaseReservationModel = function(base){
        var s = this;
        s.id = ko.observable();
        s.reservationType = ko.observable();

        s.splitSrcId = ko.observable();
        s.splitSrcQty = ko.observable();
        s.purchaseSrcId = ko.observable();
        s.purchaseSrcQty = ko.observable();
        s.qtySold = ko.observable();
        s.isAdminRequest = ko.observable(false);
        s.partnerId = ko.observable();
        s.partnerName = ko.observable();
        s.receiptNo = ko.observable();
        s.pinCode = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.rdm = ko.observable();
        s.unr = ko.observable();
        s.remarks = ko.observable();
        s.createMode = ko.observable(false);
        s.updateMode = ko.observable(false);
        s.splitMode = ko.observable(false);
        s.purchaseModel = ko.observable(false);
        s.createdBy = ko.observable();
        s.createdByUsername = ko.observable();
        s.createdDate = ko.observable();
        s.notifInfo = ko.observable("");
        s.showTime = ko.observable();
        s.showDate = ko.observable();
        s.productShowScheduleList = ko.observableArray([]);
        s.showTimeList = ko.observableArray([]);
        s.showTimeDisplay = ko.observable("");
        s.eventLineId = ko.observable("");
        s.productId = ko.observable("");
        s.penaltyCharge = ko.observable("");
        s.eventDate = ko.observable(-1);
        s.cutOffDateTime = ko.observable(-1);
        s.now = ko.observable(-1);

        s.itemId = ko.observable();
        s.mediaTypeId = ko.observable();
        s.eventGroupId = ko.observable();
        s.openValidityEndDate = ko.observable();
        s.openValidityStartDate = ko.observable();

        s.qtyFromDB = ko.observable();
        s.showDateFromDB = ko.observable();
        s.remarksFromDB = ko.observable();
        s.showTimeDisplayFromDB = ko.observable();

        s.eventStartTime = ko.observable();
        s.eventTime = ko.observable();
        s.eventEndTime = ko.observable();
        s.eventCapacityId = ko.observable();

        s.pendingPurchaseQty = ko.observable(0);
        s.inputPendingPurchaseQty = ko.observable(0);
        s.pendingPurchaseOriginalItem = ko.observable({});
        s.pendingPurchaseNewItem = ko.observable({});

        s.isNotInPurchase = ko.observable(true);

        s.isEditable = ko.computed(function(){
            if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                if(s.status() != undefined && s.status() != null && s.status() != ''){
                    if((s.status() == 'Reserved' || s.status() == 'PartiallyPurchased' ) && s.qty() > 0 && s.qty() > s.qtySold() ) {
                        return true;
                    }
                }
            }
            return false;
        });

        s.reset = function(){
            s.reservationType('');
            s.qtySold(0);
            s.splitSrcId('');
            s.splitSrcQty('');
            s.purchaseSrcId('');
            s.purchaseSrcQty('');

            s.isAdminRequest(false);
            s.isNotInPurchase(true);
            s.createMode(false);
            s.updateMode(false);
            s.splitMode(false);
            s.purchaseModel(false);
            s.partnerId('');
            s.partnerName('');
            s.id(-1);
            s.receiptNo('');
            s.pinCode('');
            s.showTime('');
            s.showTimeDisplay('');
            s.showTimeDisplayFromDB('');
            s.eventLineId('');
            s.productId('');
            s.showDate('');
            s.showDateFromDB('');
            s.status('');
            s.qty(0);
            s.qtyFromDB(0);
            s.rdm(0);
            s.unr(0);
            s.remarks('');
            s.remarksFromDB('');
            s.createdBy('');
            s.createdByUsername('');
            s.createdDate('');
            s.itemId('');
            s.mediaTypeId('');
            s.eventGroupId('');
            s.openValidityEndDate('');
            s.openValidityStartDate('');
            s.eventDate(-1);
            s.cutOffDateTime(-1)
            s.now(-1);

            s.eventStartTime('');
            s.eventTime('');
            s.eventEndTime('');
            s.eventCapacityId('');

            s.pendingPurchaseQty(0);
            s.inputPendingPurchaseQty(0);
            s.pendingPurchaseOriginalItem({});
            s.pendingPurchaseNewItem({});
        };

        s.initPurchaseMode = function(data){
            s.reset();
            s.purchaseModel(true);
            s.reservationType(data.reservationType);
            s.qtySold(data.qtySold);
            s.splitSrcId(data.splitSrcId);
            s.splitSrcQty(data.splitSrcQty);
            s.purchaseSrcId(data.purchaseSrcId);
            s.purchaseSrcQty(data.purchaseSrcQty);
            s.isAdminRequest(data.isAdminRequest);
            s.id(data.id);
            s.partnerId(data.partnerId);
            s.partnerName(data.partnerName);
            s.receiptNo(data.receiptNo);
            s.pinCode(data.pinCode);
            s.showTime(data.productId+'-'+data.eventLineId);
            s.showTimeDisplay(data.showTimeDisplay);
            s.showTimeDisplayFromDB(data.showTimeDisplay);
            s.eventLineId(data.eventLineId);
            s.productId(data.productId);
            s.showDate(data.showDate);
            s.showDateFromDB(data.showDate);
            s.status(data.status);
            s.qty(parseInt(data.qty) - parseInt(data.rdm));
            s.qtyFromDB(data.qty);
            s.rdm(data.rdm);
            s.unr(data.unr);
            s.remarks(data.remarks);
            s.remarksFromDB(data.remarks);
            s.createdBy(data.createdBy);
            s.createdByUsername(data.createdByUsername);
            s.createdDate(data.createdDate);
            s.itemId(data.itemId);
            s.mediaTypeId(data.mediaTypeId);
            s.eventGroupId(data.eventGroupId);
            s.openValidityEndDate(data.openValidityEndDate);
            s.openValidityStartDate(data.openValidityStartDate);

            s.eventStartTime(data.eventStartTime);
            s.eventTime(data.eventTime);
            s.eventEndTime(data.eventEndTime);
            s.eventCapacityId(data.eventCapacityId);

            s.eventDate(data.eventDate);
            s.cutOffDateTime(data.cutOffDateTime)
            s.now(data.now);
            s.purchaseModel(true);

            var selectedPurchaseQty = 0;
            s.pendingPurchaseQty(selectedPurchaseQty);
            s.inputPendingPurchaseQty(selectedPurchaseQty);

            s.pendingPurchaseOriginalItem(new nvx.PurchaseReservationModelItem({
                id : s.id(),
                receiptNo : s.receiptNo(),
                showDateInfo : kendo.toString(new Date(s.eventDate()), 'dddd, dd MMM yyyy'),
                showTimeInfo : s.showTimeDisplay(),
                pinCodeQty : s.qty() - s.qtySold() - s.pendingPurchaseQty(),
                pinCodeType : 'Reserved',
                status : s.status()
            }));
        };

        s.popupPurchaseReservationWindow = function(id){
            nvx.spinner.start();
            s.id(id); //set the id being cancelled now
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/get-reservation/' + s.id(),
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != ''){
                            nvx.purchaseReservationModel.initPurchaseMode(wot);
                            if(nvx.purchaseReservationModel.isEditable()){
                                window.purchaseReservationPopup.title("Purchase Reservation");
                                window.purchaseReservationPopup.center().open();
                                nvx.spinner.stop();
                            }else{
                                nvx.showMsgWindow('Error Message','Purchase reserved ticket(s) is not available.');
                            }
                        }
                    }
                    else
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){
                        });
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.applyPurchaseQty = function(){
            if((!isNaN(s.inputPendingPurchaseQty())) && s.inputPendingPurchaseQty() > 0 && s.inputPendingPurchaseQty() <= s.qty()){
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/verify-purchase-reservation/' + s.id() + '/' + s.inputPendingPurchaseQty(),
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            var wot = data['data'];
                            if(wot != undefined && wot != null && wot != ''){
                                s.pendingPurchaseNewItem(new nvx.PurchaseReservationModelItem({
                                    id : wot['id'],
                                    receiptNo : wot['receiptNum'],
                                    showDateInfo : kendo.toString(new Date(wot['eventDate']), 'dddd, dd MMM yyyy'),
                                    showTimeInfo : wot['eventName'],
                                    pinCodeQty : wot['qty'],
                                    pinCodeType : 'New',
                                    status : wot['status']
                                }));
                                s.pendingPurchaseQty(wot['qty']);
                                s.pendingPurchaseOriginalItem().pinCodeQty(s.qty() - wot['qty']);
                            }else{
                                nvx.showMsgWindow('Error Message','Unable to perform purchase reservation at this moment.');
                            }
                        }else{
                            nvx.spinner.stop();
                            nvx.showMsgWindow('Error Message', data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus){
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }else{
                if(isNaN(s.inputPendingPurchaseQty())){
                    nvx.showMsgWindow('Error Message', 'Please enter correct quantity.');
                    return false;
                }
                if(s.inputPendingPurchaseQty() <= 0){
                    nvx.showMsgWindow('Error Message', 'Please enter at least 1 quantity.');
                    return false;
                }
                if(s.inputPendingPurchaseQty() > s.qty()){
                    nvx.showMsgWindow('Error Message', 'Quantity to be purchased must be less than or equals to reserved quantity.');
                    return false;
                }
            }
        };

        s.clearPurchaseQty = function(){
            if(s.pendingPurchaseNewItem() != undefined && s.pendingPurchaseNewItem() != null && s.pendingPurchaseNewItem() != '' && s.pendingPurchaseNewItem()['id'] != undefined && s.pendingPurchaseNewItem()['id'] != null && s.pendingPurchaseNewItem()['id'] != '' && s.pendingPurchaseNewItem().id() > 0){
                if(s.pendingPurchaseNewItem().status() == 'Pending'){
                    nvx.spinner.stop();
                    $.ajax({
                        type: "POST",
                        url: nvx.API_PREFIX + '/secured/partner-wot/cancel-purchase-reservation/' + s.pendingPurchaseNewItem().id(),
                        headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                        cache: false, dataType: 'json', contentType: 'application/json',
                        success:  function(data, textStatus, jqXHR)
                        {
                            s.inputPendingPurchaseQty(0);
                            s.pendingPurchaseQty(0);
                            s.pendingPurchaseNewItem('');
                            s.pendingPurchaseOriginalItem().pinCodeQty(s.qty() - s.qtySold() - s.pendingPurchaseQty());
                            if(data.success){
                            }else{
                                nvx.spinner.stop();
                                nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                            }
                        },
                        error: function(jqXHR, textStatus){
                            nvx.spinner.stop();
                            nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                        },
                        complete: function() {
                            nvx.spinner.stop();
                        }
                    });
                }
            }else{
                s.inputPendingPurchaseQty(0);
                s.pendingPurchaseQty(0);
                s.pendingPurchaseNewItem('');
                s.pendingPurchaseOriginalItem().pinCodeQty(s.qty() - s.qtySold() - s.pendingPurchaseQty());
            }
        };

        // refresh splitted reservation details
        s.refreshPurchasedWoTReservationDetails = function(id){
            try{
                if(!(id != undefined && id != null && id != '' && id != -1)){
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/refresh-reservation/'+id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            console.log('Refresh WoT reservation details successful for '+id);
                        }
                        else
                        {
                            console.log('Refresh WoT reservation details for '+id+' is failed : '+data['message']);
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        console.log('Refresh WoT reservation details for '+id+' is failed');
                    },
                    complete: function() {
                    }
                });
            }catch(e){
                console.log('Refresh WoT reservation details for '+id+' is failed');
            }
        };
        // validate split reservation
        s.doPurchaseReservationValidation = function() {
            if((s.pendingPurchaseNewItem()['id'] != undefined && s.pendingPurchaseNewItem()['id'] != null && s.pendingPurchaseNewItem()['id'] != '' && s.pendingPurchaseNewItem()['id'] != -1)){
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/validate-purchase-reserved-reservation/'+s.pendingPurchaseNewItem().id(),
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            //show the confirm message here if validation is successful
                            nvx.reservationConfirmModel.initConfirmMode('CP', data.data, s.id());
                            window.confirmPopup.title("Confirm Purchase");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }
                        else
                        {
                            nvx.purchaseReservationModel.closeModal();
                            window.refreshGrid();
                            nvx.spinner.stop();
                            console.log('ERRORS: ' + data['message']);
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                        s.isNotInPurchase(true);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                        s.isNotInPurchase(true);
                    }
                });
            }else{
                nvx.showMsgWindow('Error Message', 'Please add at least 1 quantity to split.');
            }
        };

        // save split reservation
        s.purchaseReservation = function() {
            nvx.spinner.start();
            s.isNotInPurchase(false);
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-wot/confirm-purchase-reservation/'+s.pendingPurchaseNewItem().id(),
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    window.confirmPopup.close();
                    window.purchaseReservationPopup.close();
                    var wot = data['data'];
                    if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                        s.pendingPurchaseNewItem().status(wot['status']);
                    }
                    if(data.success)
                    {
                        s.refreshPurchasedWoTReservationDetails(wot['id']);
                        nvx.depositBalanceModel.getLatestDepositBalance();
                        window.refreshGrid();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your purchase(s) are successful.', function(){});
                    }else{
                        window.refreshGrid();
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){ });
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    window.confirmPopup.close();
                    window.purchaseReservationPopup.close();
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    s.isNotInPurchase(true);
                },
                complete: function() {
                    nvx.spinner.stop();
                    s.isNotInPurchase(true);
                }
            });
        };

        s.closeModal = function() {
            try{
                s.clearPurchaseQty();
            }catch(e){
                console.log('exception when closing purchaseReservationPopup : '+e);
            };
            window.purchaseReservationPopup.close();
        };
    };

    /*******************************************************************************************************************************************************************************************************/

    nvx.reservationConfirmModel = null;

    nvx.ReservationConfirmModel = function() {
        var s = this;

        s.noChangesFound = ko.observable(false);

        s.reservationId = ko.observable();
        s.isNew = ko.computed(function() {
            if(s.reservationId() == -1) {
                return true;
            }
            return false;
        });

        s.confirmType = ko.observable();  //S for Saving, C for Cancelling

        s.showDateDisplay = ko.observable(0);
        s.showTimeDisplay = ko.observable(0);

        s.originalRefundAmount = ko.observable(0);
        s.originalRefundAmountText = ko.computed(function() {
            if(s.originalRefundAmount() > 0) {
                return 'S$ ' + kendo.toString(s.originalRefundAmount(), "n2");
            }
        });

        s.originalTicketPrice = ko.observable(0);
        s.originalTicketPriceText = ko.computed(function() {
            if(s.originalTicketPrice() > 0) {
                return 'S$ ' + kendo.toString(s.originalTicketPrice(), "n2");
            }
        });

        s.originalTotalQty = ko.observable(0);
        s.originalTotalQtyText = ko.computed(function() {
            if(s.originalTotalQty() > 0) {
                return '  ' + kendo.toString(s.originalTotalQty(), "n0");
            }
        });

        s.ticketPrice = ko.observable(0);
        s.ticketPriceText = ko.computed(function() {
            if(s.ticketPrice() > 0) {
                return 'S$ ' + kendo.toString(s.ticketPrice(), "n2");
            }
        });

        s.totalQty = ko.observable(0);
        s.totalQtyText = ko.computed(function() {
            if(s.totalQty() > 0) {
                return '  ' + kendo.toString(s.totalQty(), "n0");
            }
        });

        s.penaltyCharge = ko.observable(0);
        s.penaltyChargeText = ko.computed(function() {
            if(s.penaltyCharge() > 0) {
                return '( S$ ' + kendo.toString(s.penaltyCharge(), "n2") + ')';
            }
        });

        s.penaltyChargeCfg = ko.observable(0);
        s.penaltyChargeCfgText = ko.computed(function() {
            if(s.penaltyChargeCfg() > 0) {
                return 'S$ ' + kendo.toString(s.penaltyChargeCfg(), "n2");
            }
        });

        s.penaltyChargeQty = ko.observable(0);
        s.penaltyChargeQtyText = ko.computed(function() {
            if(s.penaltyCharge() > 0  && s.penaltyChargeCfg() > 0)   {
                s.penaltyChargeQty(s.penaltyCharge() /  s.penaltyChargeCfg());
                return '  ' + kendo.toString(s.penaltyChargeQty(), "n0");
            }else {
                return '  ' + '0';
            }
        });

        s.refundedAmount = ko.observable(0);
        s.refundedAmountText = ko.computed(function() {
            if(s.refundedAmount() > 0) {
                return 'S$ ' + kendo.toString(s.refundedAmount(), "n2");
            }
        });

        s.ticketPurchased = ko.observable(0);
        s.ticketPurchasedText = ko.computed(function() {
            if(s.ticketPurchased() > 0) {
                return '(S$ ' + kendo.toString(s.ticketPurchased(), "n2") + ')';
            }
        });

        s.depositToBeCharged = ko.observable(0);
        s.depositToBeChargedText = ko.computed(function() {
            if(s.depositToBeCharged() > 0) {
                return 'S$ ' + kendo.toString(s.depositToBeCharged(), "n2");
            }
        });

        s.depositToBeRefunded = ko.observable(0);
        s.depositToBeRefundedText = ko.computed(function() {
            if(s.depositToBeRefunded() > 0) {
                return 'S$ ' + kendo.toString(s.depositToBeRefunded(), "n2");
            }
        });

        s.totalAmountText = ko.computed(function() {
            if(s.depositToBeRefunded() > s.depositToBeCharged()){
                return s.depositToBeRefundedText();
            }else{
                if(s.depositToBeCharged() > 0) {
                    return '(' + s.depositToBeChargedText()+')';
                }else {
                    var _t = s.depositToBeRefundedText();
                    if(_t != undefined && _t != null && _t != ''){
                        return _t;
                    }else{
                        return '$$ 0.0';
                    }
                }
            }
        });

        s.showComputation = ko.computed(function() {
            if(s.noChangesFound()){
                return false;
            }else{
                return true;
            }
        });

        s.initConfirmMode = function(confirmType, data, reservationId) {
            s.noChangesFound(false);
            s.penaltyChargeCfg('');
            s.showDateDisplay('');
            s.showTimeDisplay('');
            if(data['noChangesFound'] != undefined && data['noChangesFound'] != null && data['noChangesFound'] != ''){
                s.noChangesFound(data['noChangesFound']);
            }
            if(data['penaltyChargeCfg'] != undefined && data['penaltyChargeCfg'] != null && data['penaltyChargeCfg'] != ''){
                s.penaltyChargeCfg(data['penaltyChargeCfg']);
            }
            if(data['showDateDisplay'] != undefined && data['showDateDisplay'] != null && data['showDateDisplay'] != ''){
                s.showDateDisplay(data['showDateDisplay']);
            }
            if(data['showTimeDisplay'] != undefined && data['showTimeDisplay'] != null && data['showTimeDisplay'] != ''){
                s.showTimeDisplay(data['showTimeDisplay']);
            }
            s.confirmType(confirmType);
            s.originalTicketPrice(data.originalTicketPrice);
            s.originalRefundAmount(data.originalRefundAmount);
            s.originalTotalQty(data.originalTotalQty);
            s.ticketPrice(data.ticketPrice);
            s.totalQty(data.totalQty);
            s.penaltyCharge(data.penaltyCharge);
            s.refundedAmount(data.refundedAmount);
            s.ticketPurchased(data.ticketPurchased);
            s.depositToBeCharged(data.depositToBeCharged);
            s.depositToBeRefunded(data.depositToBeRefunded);
            s.reservationId(reservationId);
        };

        s.proceedCancel = function() {
            nvx.reservationModel.proceedCancelReservation();
        };

        s.proceedSave = function() {
            nvx.reservationModel.saveReservation();
        };

        s.proceedUpdate = function() {
            nvx.reservationModel.updaeReservation();
        };

        s.closeModal = function() {
            window.confirmPopup.close();
        };

        s.proceedPurchase = function(){
            nvx.purchaseReservationModel.purchaseReservation();
        }
    };

    nvx.errorModel = null;

    nvx.ErrorModel = function() {
        var s = this;
        s.errorMessage = ko.observable();
        s.callback = '';
        s.closeModal = function() {
            window.errorMessagePopup.close();
            if(s.callback != undefined && s.callback != null && s.callback != ''){
                try{
                    s.callback();
                    s.callback = '';
                }catch(e){
                    console.log('Message callback failed : '+e);
                }
            }
        }
    };

    nvx.showMsgWindow = function(winTitle, winMsg){
        nvx.spinner.start();
        nvx.errorModel.errorMessage(winMsg);
        window.errorMessagePopup.title(winTitle);
        window.errorMessagePopup.center().open();
        nvx.spinner.stop();
    };

    nvx.showMsgWindowWithCallBack = function(winTitle, winMsg, callBack){
        nvx.spinner.start();
        if(callBack != undefined && callBack != null){
            nvx.errorModel.callback = callBack;
        }
        nvx.errorModel.errorMessage(winMsg);
        window.errorMessagePopup.title(winTitle);
        window.errorMessagePopup.center().open();
        nvx.spinner.stop();
    };

    nvx.confirmMsgModeal = null;

    nvx.ConfirmMessageModel = function(){
        var s = this;
        s.confirmMessage = ko.observable();
        s.callback = '';
        s.confirmModal = function() {
            window.confirmMsgPopup.close();
            if(s.callback != undefined && s.callback != null && s.callback != ''){
                try{
                    s.callback();
                    s.callback = '';
                }catch(e){
                    console.log('Confirm callback failed : '+e);
                }
            }
        }
        s.cancelModal = function(){
            window.confirmMsgPopup.close();
        }
    };

    nvx.showConfirmMsgWindow = function(winTitle, winMsg, confirmActionCallBack){
        nvx.spinner.start();
        nvx.confirmMsgModeal.callback = confirmActionCallBack;
        nvx.confirmMsgModeal.confirmMessage(winMsg);
        window.confirmMsgPopup.title(winTitle);
        window.confirmMsgPopup.center().open();
        nvx.spinner.stop();
    };
})(window.nvx = window.nvx || {}, jQuery, ko);
