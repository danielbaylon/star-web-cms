(function(nvx, $, ko) {
    $(document).ready(function() {
        $('#btnPay').click(function() {
            if (nvx.btnGuard) {return;}
            if (confirm('Please click OK to continue your offline payment.')) {
                nvx.btnGuard = true;
                nvx.spinner.start();
                $.ajax({
                    url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/save-offline-request',
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            window.location.replace(nvx.ROOT_PATH + '/offlinePay-form');
                        } else {
                            alert("System Error, please try again later.");
                        }
                    },
                    error: function () {
                    },
                    complete: function () {
                        nvx.spinner.stop();
                    }
                });
            }
        });

        //TODO what is this???
        $('#btnCancel').click(function() {
            if (nvx.btnGuard) {return;}
            if (confirm('Are you sure you want to cancel your transaction?')) {
                nvx.btnGuard = true;

                $.ajax({
                    type: 'POST', async: false, cache: false,
                    url: nvx.API_PREFIX + '/secured/store-partner/checkout-cancel',
                    headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                    data: {}, dataType: "json",
                    success: function (result) {
                        window.location.replace( nvx.ROOT_PATH + '/cart');
                    }
                });
            }
        });

        window.onbeforeunload = function(e) {
            if (nvx.btnGuard) {return;}
            return "Leaving this page will cancel your checkout. Are you sure you want to do this?";
        };

        //TOOD what is this???
        window.onunload = function(e) {
            if (nvx.btnGuard) {return;}
            $.ajax({
                type: 'POST', async: false, cache: false,
                url: nvx.API_PREFIX + '/secured/store-partner/checkout-cancel',
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                data: {}, dataType: "json",
                success: function (result) {}
            });
        };

        $.ajax({
            url: nvx.API_PREFIX + '/secured/store-partner/get-checkout-display',
            headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {

                    if(data.data == null) {
                        nvx.btnGuard = true;
                        window.location.replace(nvx.ROOT_PATH + '/cart');
                    }

                    //console.log("data.data.cmsProducts:"+data.data.cmsProducts.length);
                    nvx.checkoutView = new nvx.CheckoutView(data.data);
                    ko.applyBindings(nvx.checkoutView, document.getElementById('checkoutView'));
                } else {
                    alert("System Error, please try again later.");
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
    });

    nvx.btnGuard = false;

    nvx.checkoutView = null;

    nvx.CheckoutView = function(base) {
        var s = this;
        s.base = base;
        s.gstRate =  ko.observable(base.gstRate);
        s.hasEdit = ko.observable(false);
        s.agreeTnc = ko.observable(false);

        s.products = ko.observableArray([]);
        // console.log("base.cmsProducts:"+base.cmsProducts.length);
        $.each(base.cmsProducts, function(idx, obj) {
            s.products.push(new nvx.CheckoutProduct(s, obj));
        });

        s.grandTotal = ko.computed(function() {
            var gt = 0;
            $.each(s.products(), function(idx, prod) {
                gt += Number(prod.subtotal());
            });
            return gt.toFixed(2);
        });
        s.grandTotalText = ko.computed(function() {
            return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
        });
        s.gst = ko.computed(function() {
            return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
        });
        s.gstRateStr = ko.computed(function() {
            return (s.gstRate()*100).toFixed(0) +'%';
        });
        s.gstText = ko.computed(function() {
            return kendo.toString(Number(s.gst()), "###,###,##0.00");
        });
        s.totalNoGst = ko.computed(function() {
            return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
        });
        s.totalNoGstText = ko.computed(function() {
            return kendo.toString(Number(s.totalNoGst()), "###,###,##0.00");
        });

        //TOOD this one needs to be used, but when and how
        s.doRemoveProd = function(prod) {
            if (s.products().length == 1) {
                if (confirm('Are you sure you want to remove all the items from your cart?')) {
                    nvx.spinner.start();
                    $.ajax({
                        url: '/purchase/cart/clear',
                        type: 'POST', cache: false, dataType: 'json',
                        success: function(data) {
                            if (data.success) { window.location.replace('/purchase/cart'); } else { alert(data.message); }
                        },
                        error: function() { alert('Unable to process your request. Please try again in a few moments.'); },
                        complete: function() { nvx.spinner.stop(); }
                    });
                }
            } else {
                s.hasEdit(true);
                s.products.remove(prod);
            }
        };

        s.clickGuard = false;

        s.doClearChanges = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            if (confirm('Are you sure you want to clear your changes?')) {
                window.location.reload();
            } else {
                s.clickGuard = false;
            }

        };

        //TODO this one not being used for now, need to update this as well.
        s.doSaveChanges = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            var updatedProds = [];
            $.each(s.products(), function(pidx, prod) {
                var itemsToSave = [];
                $.each(prod.items(), function(iidx, item) {
                    if (item.qty() == 0) {
                        return;
                    }
                    itemsToSave.push({
                        cartId: item.cartId,
                        id: item.itemId,
                        scheduleId: item.scheduleId,
                        qty: item.qty(),
                        cartType: item.cartType
                    });
                });

                updatedProds.push({
                    id: prod.prodId,
                    products: itemsToSave
                });
            });

            nvx.spinner.start();
            $.ajax({
                url: '/purchase/cart/update',
                data: {
                    payload: JSON.stringify({
                        items: updatedProds
                    })
                },
                type: 'POST', cache: false, dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        //TODO
                        alert(data.message);
                    }
                },
                error: function() {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                    s.clickGuard = false;
                }
            });
        };
    };

    nvx.CheckoutProduct = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.title = ko.observable(base.name);
        s.subtotal = ko.observable(base.subtotal);

        s.showTopupsToAdd = ko.observable(false);
        s.topupsToAddText = ko.computed(function() {
            return s.showTopupsToAdd() ? 'Hide Unselected Top Ups' : 'Add More Top Ups';
        });
        s.doToggleTopupsToAdd = function() {
            s.showTopupsToAdd(!s.showTopupsToAdd());
        };

        s.items = ko.observableArray([]);
        (function initItems(theBase) {
            var iarr = [];
            $.each(theBase.items, function(idx, obj) {
                //console.log("obj.qty:"+obj.qty)
                var addItem = new nvx.CheckoutItem(s, obj);
                iarr.push(addItem);
            });
            s.items(iarr);
        })(base);

        s.additionalTopups = ko.observableArray([]);
        //console.log("s.items:"+s.items.length);
        //Further init (in order for parent dot remove item to work)
        $.each(s.items(), function(idx, item) {
            item.includeInCart(true);
        });

        s.subtotalText = ko.computed(function() {
            var total = 0;
            $.each(s.items(), function(dix, item) {
                total += Number(item.total());
            });

            s.subtotal(total.toFixed(2));

            return kendo.toString(Number(s.subtotal()), "###,###,##0.00");
        });
        s.subtotalText.subscribe(function(newValue) {
            s.parent.hasEdit(true);
        });

        s.doRemoveItem = function(mdl, evt) {
            if (mdl.isTopup) {
                mdl.includeInCart(false);
            } else {

                var onlyTopups = true;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup && itm != mdl) {
                        onlyTopups = false;
                        return false;
                    }
                });

                if (onlyTopups || s.items().length == 1) {
                    s.parent.doRemoveProd(s);
                    return;
                }

                mdl.qty(0);
                s.items.remove(mdl);

                var hasTicketType = false;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup) {
                        if (itm.cartId == mdl.cartId) { return; }

                        if (itm.ticketType == mdl.ticketType) {
                            hasTicketType = true;
                            return false;
                        }
                    }
                });

                if (!hasTicketType) {
                    $.each(s.items(), function(idx, itm) {
                        if (itm.isTopup) {
                            if (itm.ticketType == mdl.ticketType) {
                                itm.includeInCart(false);
                            }
                        }
                    });
                }
            }
        };
    };

    nvx.CheckoutItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.isNew = ko.observable(false);
        s.scheduleId = base.scheduleId;
        s.cartId = base.cartId;
        s.cartType = base.cartType;
        s.itemId = base.id;
        s.isTopup = base.topup;
        s.title = base.name;
        s.ticketType = base.type;
        s.includeInCart =  ko.observable(true);

        s.description = base.description;

        s.priceInCents = base.price * 100;
        s.priceText = base.price;
        // console.log("base.qty:"+base.qty);
        s.qty = ko.observable(base.qty);

        s.total = ko.observable(base.total);

        s.totalText = ko.computed(function() {
            if (!Number.isInteger(s.qty())){ return 0; }

            var totalInCents = Number(s.qty()) * Number(s.priceInCents);
            s.total((totalInCents / 100).toFixed(2));
            return kendo.toString(Number(s.total()), "###,###,##0.00");
        });
    };

})(window.nvx = window.nvx || {}, jQuery, ko);