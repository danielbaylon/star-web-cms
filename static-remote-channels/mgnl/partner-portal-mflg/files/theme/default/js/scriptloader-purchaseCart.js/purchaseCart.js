(function(nvx, $, ko) {
    $(document).ready(function() {
        $.ajax({
            url: nvx.API_PREFIX + '/secured/store-partner/get-cart',
            headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    //console.log("data.data.cmsProducts:"+data.data.cmsProducts.length);
                    nvx.checkoutView = new nvx.CheckoutView(data.data);
                    ko.applyBindings(nvx.checkoutView, document.getElementById('checkoutView'));
                } else {
                    alert("System Error, please try again later.");
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
        //TODO TOPUPs
    });
    nvx.CheckoutView = function(base) {
        var s = this;
        s.base = base;
        s.gstRate =  ko.observable(base.gstRate);
        s.hasEdit = ko.observable(false);
        s.agreeTnc = ko.observable(false);
        s.offlinePaymentEnabled = ko.observable(false); //TODO
        s.errorMessage = ko.observable("");

        s.products = ko.observableArray([]);


        $.each(base.cmsProducts, function(idx, obj) {
            s.products.push(new nvx.CheckoutProduct(s, obj, base.additionalTopups[obj.id]));  //push all the products
        });

        s.update = function(data){
            s.products.removeAll();
            s.base = data;
            $.each(data.cmsProducts, function(idx, obj) {
                s.products.push(new nvx.CheckoutProduct(s, obj));
            });
        }

        s.grandTotal = ko.computed(function() {
            var gt = 0;
            $.each(s.products(), function(idx, prod) {
                gt += Number(prod.subtotal());
            });
            return gt.toFixed(2);
        });
        s.grandTotalText = ko.computed(function() {
            return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
        });
        s.gst = ko.computed(function() {
            return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
        });
        s.gstRateStr = ko.computed(function() {
            return (s.gstRate()*100).toFixed(0) +'%';
        });
        s.gstText = ko.computed(function() {
            return  kendo.toString(Number(s.gst()), "###,###,##0.00");
        });
        s.totalNoGst = ko.computed(function() {
            return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
        });
        s.totalNoGstText = ko.computed(function() {
            return kendo.toString(Number(s.totalNoGst()) , "###,###,##0.00");
        });

        s.offlinePaymentEnabled(false);
        if(s.base != undefined && s.base != null && s.base.payMethodOptions != undefined && s.base.payMethodOptions.length > 0){
            var payMethodOptionsLen = s.base.payMethodOptions.length;
            for(var i = 0 ; i < payMethodOptionsLen ; i++){
                if('offlinePaymentEnabled' == s.base.payMethodOptions[i]){
                    s.offlinePaymentEnabled(true);
                    break;
                }
            }
        }

        //TODO add this code
        s.doRemoveProd = function(prod) {
            //TODO need to double check this one
            if (s.products().length == 1) {
                if (confirm('Are you sure you want to remove all the items from your cart?')) {
                    nvx.spinner.start();
                    $.ajax({
                        url: nvx.API_PREFIX + '/secured/store-partner/clear-cart',
                        headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                        type: 'POST', cache: false, dataType: 'json',
                        success: function(data) {
                            if (data.success) { window.location.replace(nvx.ROOT_PATH + '/cart'); } else { alert(data.message); }
                        },
                        error: function() { alert('Unable to process your request. Please try again in a few moments.'); },
                        complete: function() { nvx.spinner.stop(); }
                    });
                }
            } else {
                s.hasEdit(true);
                s.products.remove(prod);
            }
        };

        s.clickGuard = false;

        s.doClearChanges = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            if (confirm('Are you sure you want to clear your changes?')) {
                window.location.reload();
            } else {
                s.clickGuard = false;
            }

        };

        s.doSaveChanges = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            var itemsToSave = [];
            $.each(s.products(), function(pidx, prod) {
                $.each(prod.items(), function(iidx, item) {
                    if (item.qty() == 0) {
                        return;
                    }
                    var addItem = {};
                    addItem.listingId = item.listingId;
                    addItem.productCode = item.productCode;
                    addItem.eventGroupId = item.eventGroupId;
                    addItem.eventLineId = item.eventLineId;
                    addItem.eventSessionName = item.eventSessionName;
                    addItem.selectedEventDate = item.selectedEventDate;
                    addItem.isTopup = item.isTopup;
                    addItem.qty = item.qty();
                    addItem.cmsProductId = prod.id();
                    addItem.cmsProductName = prod.title();
                    addItem.parentCmsProductId =  prod.id();
                    addItem.parentListingId = item.parentListingId;
                    addItem.price = item.price;
                    addItem.type = item.type;
                    addItem.name = item.title;
                    itemsToSave.push(addItem);
                });
            });

            nvx.spinner.start();
            $.ajax({
                url:  nvx.API_PREFIX + '/secured/store-partner/update-cart',
                data: JSON.stringify({
                    sessionId: '',
                    items: itemsToSave
                }),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function(data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        //TODO
                        alert(data.message);
                    }
                },
                error: function() {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                    s.clickGuard = false;
                }
            });
        };


        s.doCheckout = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            if (!s.agreeTnc()) {
                alert('Please read and agree to the Terms and Conditions first.');
                s.clickGuard = false;
                return false;
            }


            //TODO!!!!!!
            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner/checkout',
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        var receiptNum = data.data.receiptNumber;
                        window.location.href = nvx.ROOT_PATH + "/confirm~"+receiptNum+"~";
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });

        };

        s.doOfflinePayment = function() {
            if (s.clickGuard) { return; }
            s.clickGuard = true;

            if (!s.agreeTnc()) {
                alert('Please read and agree to the Terms and Conditions first.');
                s.clickGuard = false;
                return false;
            }

            //TODO!!!!!!
            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner-offlinePay/checkout-offline',
                data: JSON.stringify({
                    name: 'TA1',
                    email: 'shen.yang@enovax.com'
                }),
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        var receiptNum = data.data.receiptNumber;
                        window.location.href = nvx.ROOT_PATH + "/offlinePay-confirm~"+receiptNum+"~";
                    } else {
                        alert("System Error, please try again later.");
                        s.clickGuard = false;
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        }
    };

    nvx.CheckoutProduct = function(parent, base, additionalTopupMap) {
        var s = this;
        s.parent = parent;


        s.id = ko.observable(base.id);

        s.title = ko.observable(base.name);
        s.subtotal = ko.observable(base.subtotal);

        s.showTopupsToAdd = ko.observable(false);
        s.topupsToAddText = ko.computed(function() {
            return s.showTopupsToAdd() ? 'Hide Unselected Top Ups' : 'Add More Top Ups';
        });
        s.doToggleTopupsToAdd = function() {
            s.showTopupsToAdd(!s.showTopupsToAdd());
        };
        s.items = ko.observableArray([]);
        $.each(base.items, function(idx, obj) {
            var addItem = new nvx.CheckoutItem(s, obj);
            s.items.push(addItem);
        });
        s.additionalTopups = ko.observableArray([]);
        (function initAdditionalTopups(theBase) {
            var iarr = [];
            $.each(additionalTopupMap, function(idx, obj) {
                var addItem = new nvx.CheckoutItem(s, obj);
                addItem.isUnchosen(true);
                iarr.push(addItem);
            });
            s.additionalTopups(iarr);
        })(base);

        $.each(s.items(), function(idx, item) {
            item.includeInCart(true);
        });
        s.subtotalText = ko.computed(function() {
            var total = 0;
            $.each(s.items(), function(dix, item) {
                total += Number(item.total());
            });

            s.subtotal(total.toFixed(2));
            return  kendo.toString(Number(s.subtotal()), "###,###,##0.00");
        });
        s.subtotalText.subscribe(function(newValue) {
            s.parent.hasEdit(true);
        });

        //s.doRemoveItem = function() {
        //
        //    $.ajax({
        //        url: nvx.API_PREFIX + '/secured/store-partner/remove-item-from-cart/' + this.cartId,
        //        headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
        //        type: 'POST', cache: false,
        //        dataType: 'json', contentType: 'application/json',
        //        success: function (data) {
        //            if (data.success) {
        //                window.nvx.cartViewModel.totalQty(data.data.totalQty);
        //                s.parent.update(data.data);
        //                //s.items.remove(this);
        //            } else {
        //                alert("System Error, please try again later.");
        //            }
        //        },
        //        error: function () {
        //        },
        //        complete: function () {
        //        }
        //    });
        //};
        s.doRemoveItem = function(mdl, evt) {
            if (mdl.isTopup) {
                mdl.includeInCart(false);  //topup dont do anything
            } else {

                var onlyTopups = true;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup && itm != mdl) {
                        onlyTopups = false;
                        return false;
                    }
                });

                if (onlyTopups || s.items().length == 1) {
                    s.parent.doRemoveProd(s);
                    return;
                }

                mdl.qty(0);
                s.items.remove(mdl);

                var hasTicketType = false;
                $.each(s.items(), function(idx, itm) {
                    if (!itm.isTopup) {
                        if (itm.cartId == mdl.cartId) { return; }

                        if (itm.ticketType == mdl.ticketType) {
                            hasTicketType = true;
                            return false;
                        }
                    }
                });

                if (!hasTicketType) {
                    $.each(s.items(), function(idx, itm) {
                        if (itm.isTopup) {
                            if (itm.ticketType == mdl.ticketType) {
                                itm.includeInCart(false);
                            }
                        }
                    });
                }
            }
        };

    };

    nvx.CheckoutItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.isNew = ko.observable(false);

        s.scheduleId = base.scheduleId;
        s.cartId = base.cartItemId;
        s.cartType = base.cartType;
        s.itemId = base.id;
        s.isTopup = base.topup;
        s.title = base.name;
        s.ticketType = base.type;
        s.productCode = base.productCode;
        s.listingId = base.listingId;
        s.parentListingId = base.parentListingId; //for top up, the related ax product

        s.description = base.description;
        s.priceInCents = base.price * 100;
        s.priceText = base.price;
        s.eventGroupId = base.eventGroupId;
        s.eventLineId = base.eventLineId;
        s.eventSessionName = base.eventSessionName;
        s.selectedEventDate = base.selectedEventDate;

        s.type = base.type;
        s.price = base.price;

        s.qty = ko.observable(base.qty);

        s.qty.subscribe(function(newVal) {
            if (s.isTopup) {
                //Do nothing
            } else {
                //Only update if the main item triggered this.
                var mainQty = newVal;
                var currTopupQty = 0;
                var listingId = s.listingId;

                var topups = [];
                $.each(s.parent.items(), function(dix, item) {
                    //if (s.scheduleId != item.scheduleId) {
                    //    return;
                    //}

                    if (item.isTopup) {
                        if (item.parentListingId == listingId) {
                            currTopupQty = item.qty();
                            topups.push(item);
                        }
                    }
                });

                var addTopups = [];
                $.each(s.parent.additionalTopups(), function(dix, item) {
                    if (item.parentListingId == listingId) {
                        currTopupQty = item.qty();
                        addTopups.push(item);
                    }
                });

                if (mainQty != currTopupQty) {
                    $.each(topups, function(dix, tp) {
                        tp.qty(mainQty);
                    });
                    $.each(addTopups, function(dix, tp) {
                        tp.qty(mainQty);
                    });
                }
            }
        });

        s.total = ko.observable(base.total);

        s.totalText = ko.computed(function() {

            if (!nvx.isNumber(s.qty())) { return 0; }

            var totalInCents = Number(s.qty()) * Number(s.priceInCents);
            s.total((totalInCents / 100).toFixed(2));
            return  kendo.toString(Number(s.total()), "###,###,##0.00");
        });

        s.isUnchosen = ko.observable(false);
        s.isUnchosen.subscribe(function(newVal) {
            if (!newVal) { return; }
            var mainQty = 0;
            var listingId = s.parentListingId;
            $.each(s.parent.items(), function(dix, item) {
                if (s.scheduleId != item.scheduleId) {
                    return;
                }

                if (!item.isTopup) {
                    if (item.listingId == listingId) {
                        mainQty = item.qty();
                    }
                }
            });

            s.qty(mainQty);
        });
        s.includeInCart =  ko.observable(false);
        s.includeInCart.subscribe(function(include) {
            if (include) {
                if (s.parent.items.indexOf(s) != -1) {
                    return;
                }
                s.isUnchosen(false);
                s.parent.items.push(s);
                s.parent.additionalTopups.remove(s);
            } else {
                s.isUnchosen(true);
                s.parent.additionalTopups.push(s);
                s.parent.items.remove(s);
            }
        });
        s.topupSelectable = ko.computed(function() {
            if (!s.isUnchosen()) { return false; }
            if (!s.isTopup) { return false; }

            if (s.ticketType == 'Standard') {
                return true;
            }

            var canLah = false;
            $.each(s.parent.items(), function(dix, item) {
                if (s.scheduleId != item.scheduleId) {
                    return;
                }

                if (!item.isTopup) {
                    if (item.ticketType == s.ticketType) {
                        canLah = true;
                        return false;
                    }
                }
            });
            return canLah;
        });


        //private String cmsProductName;
        //private String cmsProductId;
        //
        //private String listingId;
        //private String productCode;
        //private String name;
        //private String type;
        //private Integer qty;
        //private BigDecimal price;
        //
        //private String cartItemId;
        //
        //private String eventGroupId;
        //private String eventLineId;
        //private String eventSessionName;
        //private String selectedEventDate = ""; //dd/MM/yyyy
        //
        //private boolean isTopup = false;
        //private String parentCmsProductId;
        //private String parentListingId;
    };

})(window.nvx = window.nvx || {}, jQuery, ko);