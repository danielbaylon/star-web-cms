var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

jQuery(document).ready(function() {
    nvx.initForgot();
});

(function(nvx, $) {
    nvx.showMsg = function(key){
        var error = nvx.getDefinedMsg(key);
        if(error != undefined && error != null && error != ''){
            $('#alert').html(error);
        }else{
            $('#alert').html(key);
        }
        $('#alert').show();
    };

    nvx.forgotPwd = function(){
        nvx.spinner.start();
        if(!$('[name="tempIframe"]').length){
            var iframe=document.createElement('iframe');
            $(iframe).attr('name', 'tempIframe');
            $(iframe).attr('id', 'tempIframe');
            $(iframe).hide();
            $('body').append(iframe);
        }

        //
        var frm = document.getElementById('forgot-password-reset-form');
        frm.target = 'tempIframe';
        frm.submit();

        //
        $('[name="tempIframe"]').load(function() {
            var content = $('[name="tempIframe"]').contents().find("html").text();
            try {
                var data = JSON.parse(content);
                if(data['success'] != null){
                    if(data.message != null && data.message != ''){
                        var msg = nvx.getDefinedMsg(data.message);
                        if (msg != undefined && msg != '') {
                            nvx.showMsg(msg);
                        } else {
                            nvx.showMsg(data.message);
                        }
                    }
                }
            } catch (err) {
                console.log(err);
                alert(ERROR_MSG);
            }
            nvx.spinner.stop();
            Recaptcha.reload();
            $('[name="tempIframe"]').remove();
        });
    };

    nvx.initForgot = function() {
        $('#alert').hide();
        $('#btnReset').click(function(ev) {
            $('#alert').hide();
            var un = $('#un').val();
            var email = $('#email').val();
            if((un == undefined || un == null || un == '' || un.trim().length == 0) || (email == undefined || email == null || email == '' || email.trim().length == 0)){
                nvx.showMsg('EMPTY_USERNAME_AND_PWD');
                return false;
            }
            nvx.forgotPwd();
        });
    };
})(window.nvx = window.nvx || {}, jQuery);