
var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

$(document).ready(function() {
    nvx.initPartnerProfile(function(){
        nvx.getPartnerSubAccountRoles(function(){
            nvx.initAccountsGrid('accountsGrid', 'accountsModal');
        });
    });
});

(function(nvx, $) {
    nvx.profile = null;
    nvx.subAccountRoles = null;
    nvx.accGrid = {};
    nvx.initAccountsGrid = function(gridId, modalId) {
        $('a[data-forbind="new-acc"]').each(function(idx, elem) {
            $(elem).click(function() {
                nvx.accGrid.showAccountModal(-1);
            });
        });
        nvx.accGrid = new nvx.AccountsGrid(gridId, modalId);
    };

    nvx.AccountsGrid = function(gridId, modalId) {
        var s = this;
        s.gridId = gridId;
        s.modalId = modalId;
        s.kGrid = {};
        initGrid();
        function initGrid() {
            s.kGrid = $('#' + s.gridId).kendoGrid({
                dataSource: {
                    transport: {read: {url: nvx.API_PREFIX + '/secured/partner-account/get-all-subaccounts', cache: false}},
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: 'id',
                            fields: {
                                name: {type: 'string'},
                                title: {type: 'string'},
                                username: {type: 'string'},
                                email: {type: 'string'},
                                status: {type: 'string'}
                            }
                        }
                    }
                },
                columns: [
                    { field: 'username', title: 'User Name' },
                    { field: 'title', title: 'Designation' },
                    { field: 'email', title: 'Email' },
                    { field: 'status', title: 'Status' },
                    { width: 100,
                        template: '<a class="btn btn-secondary btn-block" href="javascript:nvx.accGrid.showAccountModal(\'${id}\')">' +
                        'Edit</a>'
                    }
                ],
                sortable: false, pageable: false
            }).data('kendoGrid');
        }

        s.accModal = null;
        s.accModel = {};

        s.showAccountModal = function(userId) {
            var isNew = userId == -1;
            if (isNew) {
                showAccountModalCallback(isNew);
            } else {
                nvx.spinner.start();
                $.ajax({
                    url: nvx.API_PREFIX + '/secured/partner-account/get-subaccount-details?userId=' + userId,
                    type: 'GET',
                    cache: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            showAccountModalCallback(false, data['data']);
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function() {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        };

        function showAccountModalCallback(isNew, data) {
            if (s.accModal === null) {
                s.accModal = $('#' + s.modalId).kendoWindow({
                    title: isNew ? 'New User Account' : 'Edit User Account',
                    modal: true,
                    resizable: false,
                    width: '700px',
                    close: s.cleanupAccountModal
                }).data('kendoWindow');
                s.accModel = new nvx.AccountModel(s.accModal, s.kGrid);
                ko.applyBindings(s.accModel, document.getElementById(s.modalId));
            } else {
                s.accModel.clear();
            }
            if (isNew) {
                s.accModel.clear();
                s.accModel.isNew(isNew);
                s.accModel.isNotSelf(true);
            } else {
                s.accModel.isNew(false);
                s.accModel.init(data);
            }
            s.accModal.setOptions({
                title: isNew ? 'New User Account' : 'Edit User Account',
            });
            s.accModal.refresh();
            s.accModal.center().open();
        }

        s.cleanupAccountModal = function() {
            if (s.accModal !== null) {
                s.accModel.clear();
            }
        };
    };

    nvx.AccountModel = function(kWin, kGrid) {

        var s = this;

        s.kWin = kWin;
        s.kGrid = kGrid;

        s.userId = -1;
        s.accCode = ko.observable(nvx.profile.accountCode);
        s.username = ko.observable();
        s.name = ko.observable();
        s.title = ko.observable();
        s.email = ko.observable();
        s.access = ko.observableArray();
        s.status = ko.observable();

        s.isNotSelf = ko.observable(true);
        s.isNew = ko.observable(true);
        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();
        s.unErr = ko.observable(false);
        s.nameErr = ko.observable(false);
        s.emailErr = ko.observable(false);
        s.titleErr = ko.observable(false);

        s.accountAccessGroups = ko.observableArray([]);

        if(nvx.subAccountRoles != undefined && nvx.subAccountRoles != null && nvx.subAccountRoles != ''){
            $.each(nvx.subAccountRoles, function(idx, obj){
                var opt = new nvx.MainOpt(obj);
                s.accountAccessGroups.push(opt);
            });
        }

        s.init = function(data) {
            s.userId = data.id;
            s.username(data.username);
            s.name(data.name);
            s.email(data.email);
            s.title(data.title);
            s.status(data.status);
            s.isNew(false);
            s.isNotSelf(true);
            if(data != undefined && data != null && data['notSelf'] != undefined && data['notSelf'] != null){
                s.isNotSelf(data['notSelf']);
            }
            s.clearErrs();
            $.each(s.accountAccessGroups(), function(idx, obj){
                obj.checked(false);
            });
            var rightsIds = data['rightsIds'];
            if(rightsIds != undefined && rightsIds != null && rightsIds != ''){
                $.each(s.accountAccessGroups(), function(idx, obj){
                    var tempId = obj['id'];
                    $.each(rightsIds, function(index, rightId){
                        if(tempId != undefined && obj != null && obj != '' && rightId != undefined && rightId != null && rightId != '' && rightId == tempId){
                            obj.checked(true);
                        }
                    });
                });
            }
        };

        s.clear = function() {
            s.userId = -1;
            s.username('');
            s.name('');
            s.email('');
            s.title('');
            s.status('');
            s.access([]);
            s.isNew(true);
            s.isNotSelf(true);
            s.clearErrs();
            $.each(s.accountAccessGroups(), function(idx, obj){
                obj.checked(false);
            });
        };

        s.clearErrs = function() {
            s.isErr(false);
            s.errMsg('');
            s.unErr(false);
            s.nameErr(false);
            s.emailErr(false);
            s.titleErr(false);
        };

        s.doCancel = function() {
            s.kWin.close();
        };

        s.doResetPassword = function() {
            var result = confirm("Are you sure you want to reset the password?");
            if (result == false) {
                return;
            }
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/publicity/partner-account/reset-password',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: {
                    un: s.username(),
                    email : s.email()
                },
                success: function(data) {
                    if (data.success) {
                        alert('Successfully reset user password.');
                        s.kGrid.dataSource.read();
                        s.kWin.close();
                    } else {
                        s.clearErrs();
                        s.errMsg(nvx.getPreferDefinedMsg(data.message));
                        s.isErr(true);
                    }
                },
                error: function() {
                    s.clearErrs();
                    s.errMsg('Unable to process your request. Please try again in a few moments.');
                    s.isErr(true);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.doSave = function() {
            if (s.validate()) {
                var grpId = '';
                $.each(s.accountAccessGroups(), function(idx, obj){
                    if(obj.checked()){
                        var tempId = obj['id'];
                        grpId += (grpId != '' ? ',' : '') + tempId;
                    }
                });
                nvx.spinner.start();
                $.ajax({
                    url: nvx.API_PREFIX + '/secured/partner-account/save-subaccount-details',
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    data: {
                        isNew: s.isNew(),
                        userId: s.userId,
                        username: s.name(),
                        name: s.name(),
                        email: s.email(),
                        title: s.title(),
                        status: s.status(),
                        access: grpId
                    },
                    success: function(data) {
                        if (data.success) {
                            alert('Successfully saved user account.');
                            s.kGrid.dataSource.read();
                            s.kWin.close();
                        } else {
                            s.clearErrs();
                            s.errMsg(nvx.getPreferDefinedMsg(data.message));
                            s.isErr(true);
                        }
                    },
                    error: function() {
                        s.clearErrs();
                        s.errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        };

        s.validate = function() {
            s.clearErrs();
            var msg = '';
            if (s.isNew() && !nvx.isLengthWithin(s.name(), 1, 100, true)) {
                s.nameErr(true);
                msg += (msg != '' ? '<br>' : '') + nvx.getPreferDefinedMsg('NameInvalid');
            }
            if (s.isNew() && !nvx.isLengthWithin(s.title(), 1, 100, true)) {
                s.titleErr(true);
                msg += (msg != '' ? '<br>' : '') + nvx.getPreferDefinedMsg('TitleInvalid');
            }
            if (!nvx.isValidEmail(s.email())) {
                s.emailErr(true);
                msg += (msg != '' ? '<br>' : '') + nvx.getPreferDefinedMsg('EmailInvalid');
            }
            if (msg !== '') {
                s.isErr(true);
                s.errMsg(msg);
                return false;
            }
            return true;
        };
    };

    $(".k-grid-header").css("padding-right", "0px");

    nvx.initPartnerProfile = function(callback){
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/secured/partner-account/get-partner-profile',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    nvx.profile = data['data'];
                    callback();
                }else{
                    if(data.message != null && data.message != ''){
                        alert(nvx.getPreferDefinedMsg(data.message));
                    }
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    nvx.getPartnerSubAccountRoles = function(callback){
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/secured/partner-account/get-partner-subaccount-roles',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    nvx.subAccountRoles = data['data'];
                    callback();
                }else{
                    if(data.message != null && data.message != ''){
                        alert(nvx.getPreferDefinedMsg(data.message));
                    }
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    nvx.MainOpt = function(theBase) {
        var s = this;
        s.checked = ko.observable(false);
        s.id = theBase.id;
        s.name = theBase.name;
        s.description = theBase.description;
    };

})(window.nvx = window.nvx || {}, jQuery);