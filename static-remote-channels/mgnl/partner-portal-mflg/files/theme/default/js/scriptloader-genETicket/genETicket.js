(function(nvx, $) {

    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        nvx.mainModel = new nvx.MainModel();
        ko.applyBindings(nvx.mainModel, document.getElementById("main-content-section"));
        nvx.mainModel.init();

    });

    nvx.mainModel = null;

    nvx.MainModel = function() {
        var s = this;

        s.id = ko.observable("");
        s.status = ko.observable("");
        s.pinCode = ko.observable("");
        s.ticketMedia = ko.observable("");
        s.pinRequestId = ko.observable("");
        s.name = ko.observable("");
        s.description = ko.observable("");
        s.expiryDateStr = ko.observable("");
        s.qty = ko.observable();
        s.transItems = ko.observableArray([]);

        s.init = function() {
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/get-eticket-info",
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(result, textStatus, jqXHR)
                {
                    if(result.success)
                    {
                        var data = result.data;
                        s.id(data.id);
                        s.status(data.status);

                        s.name(data.name);
                        s.description(data.description);
                        s.expiryDateStr(data.expiryDateStr);
                        s.qty(data.qty);
                        s.pinRequestId(data.pinRequestId);
                        s.pinCode(data.pinCode);
                        s.ticketMedia(data.pkgTktMedia);

                        $.each(data.transItems, function(idx, model) {
                            var vm = new nvx.TransactionItems(model);
                            s.transItems.push(vm);
                        });

                        if(s.ticketMedia() == 'ETicket') {
                            $("#downloadTicket").attr("href", nvx.API_PREFIX + "/secured/partner-inventory/download-ticket/" + s.id());
                        }else if(s.ticketMedia() == 'ExcelFile') {
                            $("#downloadExcelFile").attr("href", nvx.API_PREFIX + "/secured/partner-inventory/download-excel-ticket/" + s.id());
                        }

                    }
                    else
                    {
                        window.location.href= nvx.ROOT_PATH + "/error";
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    window.location.href= nvx.ROOT_PATH + "/error";
                }
            });
        }

    }


    nvx.TransactionItems =  function(data){
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.description = ko.observable(data.displayDetails)
        s.pkgQty = ko.observable(data.pkgQty);
        s.receiptNum = ko.observable(data.receiptNum);
        s.ticketType = ko.observable(data.ticketType);
        s.topupItems = ko.observableArray([]);
        $.each(data.topupItems, function(idx, model) {
            var vm = new nvx.TopupItems(model);
            s.topupItems.push(vm);
        });
    }

    nvx.TopupItems = function(data) {
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.pkgQty = ko.observable(data.pkgQty);

    }

})(window.nvx = window.nvx || {}, jQuery);