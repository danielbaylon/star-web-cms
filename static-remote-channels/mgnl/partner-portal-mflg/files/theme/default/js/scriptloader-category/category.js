(function(nvx, $) {

    $(document).ready(function() {

        var pathArr = window.location.pathname.split("~");

        if(pathArr.length >=2 && pathArr[0].indexOf("category") != -1) {
            nvx.theNav.currNav(pathArr[1]);
            nvx.theNav.currSubNav(pathArr[1]);
        }

        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function(item) {
                var i = this.length;
                while (i--) {
                    if (this[i] === item) return i;
                }
                return -1;
            };
        }

        var onlyOneImage = $('.highlight-section').find('.promotion-1').length == 1;
        if (onlyOneImage) {
            $('.highlight-section').find('.slider-arrow').hide();
        }
        theSlider = $('.highlight-section').sequence({
            autoPlay: true,
            autoPlayDelay: 5000,
            nextButton: !onlyOneImage,
            prevButton: !onlyOneImage
        }).data('sequence');

        //$('.slideshow-slides').sequence({
        //    autoPlay: true,
        //    autoPlayDelay: 5000,
        //    nextButton: '.icon-left-arrow',
        //    prevButton: '.icon-right-arrow'
        //}).data('sequence');

        var categJson = {products:prodsJson};
        nvx.mdl = new nvx.MainModel(categJson);
        ko.applyBindings(nvx.mdl,$('#main-div')[0]);
        //$page.mdl.initProdExtData();

        $(".moretext").click(function() {
            $(".summaryDiv").css('overflow','visible');
            $(".summaryDiv").css('height','');
            $(".moretext").toggle();
            $(".lesstext").toggle();
        });
        $(".lesstext").click(function() {
            $(".summaryDiv").css('overflow','hidden');
            $(".summaryDiv").css('height','100px');
            $(".moretext").toggle();
            $(".lesstext").toggle();
        });


    });

    //nvx.retrieveProducts = function() {
    //    $.ajax({
    //        type: "POST",
    //        url: nvx.API_PREFIX + '/secured/store-partner/get-products-by-category/' + categoryName,
    //        headers: { 'Store-Api-Channel' : nvx.API_CHANNEL},
    //        success:  function(data, textStatus, jqXHR)
    //        {
    //            if(data.success)
    //            {
    //                var result = data.data;
    //                var partnerProds = [];
    //                $.each(prodsJson, function(idx, prod) {
    //                    $.each(result, function(idx2, prodFromDB) {
    //                        if(prod.prodId == prodFromDB.id) {
    //                            prod.isExclusiveDeal = prodFromDB.exclusiveDeal;
    //                            partnerProds.push(prod);
    //
    //                        }
    //                    });
    //
    //                });
    //
    //                prodsJson = []; //remove all the info :D
    //
    //                var categJson = {products:partnerProds};
    //                nvx.mdl = new nvx.MainModel(categJson);
    //                ko.applyBindings(nvx.mdl,$('#main-div')[0]);
    //                //$page.mdl.initProdExtData();
    //
    //            }
    //            else
    //            {
    //                alert(data.error);
    //            }
    //        },
    //        error: function(jqXHR, textStatus)
    //        {
    //            alert('Unable to process your request. Please try again in a few moments.');
    //        },
    //        complete: function() {
    //            nvx.spinner.stop();
    //        }
    //
    //    });
    //}

    nvx.mdl = null;

    nvx.ProductModel = function(data){
        var s = this;
        s.Data = data;
        s.prodId = ko.observable(data.prodId);
        s.name = ko.observable(data.name);
        s.shortDescription = ko.observable(data.shortDescription);
        s.description = ko.observable(data.description);
        s.axProducts = ko.observableArray([]);
        if(data.axProducts && data.axProducts.length >0){
            $.each(data.axProducts, function(i, item) {
                s.axProducts.push(new nvx.AxProduct(item));
            });
        }

        s.isExclusiveDeal = ko.observable(data.isExclusiveDeal);
        s.images = ko.observableArray([]);
        if(data.images && data.images.length >0){
            $.each(data.images, function(i, item) {
                s.images.push(new nvx.ProductImage(item));
            });
        }else {
            var item = { url: "/resources/" + nvx.API_CHANNEL + "/theme/default/img/default-prod-image.jpg"};
            s.images.push(new nvx.ProductImage(item));
        }
        if(data.images && data.images.length >0){
            s.imagePath = ko.observable(data.images[0].url);
        }else{
            s.imagePath = ko.observable("/resources/" + nvx.API_CHANNEL + "/theme/default/img/default-prod-image.jpg");
        }
        //console.log("s.images:"+s.images().length);
    }

    nvx.AxProduct = function(data) {
        var s = this;
        s.itemName = ko.observable(data.itemName);
        s.ticketType = ko.observable(data.ticketType);
        s.publishPrice = ko.observable(data.publishPrice);
        s.publishPriceFormat = ko.computed(function() {
            if(s.publishPrice() == '' || s.publishPrice() == null || s.publishPrice() == 'N/A') {
                return '';
            }
            return 'S$ ' + kendo.toString(parseFloat(s.publishPrice()), "n2");
        });
        s.productPrice = ko.observable(data.productPrice);

        s.productPriceFormat = ko.computed(function() {
            return 'S$ ' + kendo.toString(parseFloat(s.productPrice()), "n2");
        });

        s.productCode = ko.observable(data.productCode);
        s.listingId = ko.observable(data.listingId);
        s.qty = ko.observable();
        s.crossSells = ko.observableArray();
        if(data.crossSells && data.crossSells.length >0){
            $.each(data.crossSells, function(i, item) {
                s.crossSells.push(new nvx.CrossSell(item));
            });
        }
    }


    nvx.CrossSell = function(data) {
        var s = this;
        s.itemName = ko.observable(data.itemName);
        s.imageLink = ko.observable(data.imageLink);
        s.listingId = ko.observable(data.listingId);
        s.productCode = ko.observable(data.productCode);
        s.ticketPublishPrice = ko.observable(data.ticketPublishPrice);
        s.ticketPublishPriceFormat = ko.computed(function() {
            return kendo.toString(parseFloat(s.ticketPublishPrice()), "n2");
        });

        s.ticketPrice = ko.observable(data.ticketPrice);
        s.ticketPriceFormat = ko.computed(function() {
            return kendo.toString(parseFloat(s.ticketPrice()), "n2");
        });

        s.ticketType = ko.observable(data.ticketType);
        s.parentAxProductListingId = ko.observable(data.parentAxProductListingId);
        s.parentAxProductProductCode = ko.observable(data.parentAxProductProductCode);
        s.parentEventGroupId = ko.observable(); //seems like need to remember the date
        s.parentEventLineId = ko.observable();//seems like need to remember the date
        s.parentSelectedEventDate = ko.observable();//seems like need to remember the date
        s.parentItemQty = ko.observable(0);
        s.qty = ko.observable(0);
        s.addedInCart = ko.observable(false);
        s.addToTopUp = ko.observable(false);
    }

    nvx.ProductImage =  function(data){
        var s = this;
        s.url = ko.observable(data.url);
    }


    nvx.MainModel = function(data){
        var s = this;
        s.products = ko.observableArray([]);
        s.currentProd = ko.observable(new nvx.ProductModel({}));
        s.showProduct = ko.observable(false);
        s.showProductTable = ko.observable(true);
        s.showTopupTable = ko.observable(false);
        s.showViewCart = ko.observable(false);
        s.topups = ko.observableArray([]);
        s.errorMessage = ko.observable('');
        s.sliderShown = ko.observable(false);

        s.showAddTopups = ko.observable(true);

        this.update = function(data){
            var length = data.products.length;
            for(var i=0;i<length;i++){
                this.products.push(new nvx.ProductModel(data.products[i]));
            }
        }

        this.update(data);

        s.addToCart = function(){
            nvx.spinner.start();
            var mainItems = [];

            var tmpItems = s.currentProd().axProducts();

            var pxdMap = {};
            $.each(s.prodExtData, function(idx, mdl) {
                pxdMap[mdl.productId] = mdl;
            });

            var topups = [];

            for(var i = 0; i < tmpItems.length; i++) {
                if(tmpItems[i].qty() != null && tmpItems[i].qty() > 0) {
                    var tmpItem = {};
                    var listingId = tmpItems[i].listingId();
                    var extData = pxdMap[listingId];
                    var qty = (extData != null)?(extData.event ? $('#ebQty' + listingId).val() : tmpItems[i].qty()): tmpItems[i].qty();

                    tmpItem.cmsProductName = s.currentProd().name();
                    tmpItem.cmsProductId = s.currentProd().prodId();
                    tmpItem.name = tmpItems[i].itemName();
                    tmpItem.type = tmpItems[i].ticketType();
                    tmpItem.qty = qty;
                    tmpItem.price = tmpItems[i].productPrice();
                    tmpItem.productCode = tmpItems[i].productCode();
                    tmpItem.listingId = tmpItems[i].listingId();
                    tmpItem.crossSells = tmpItems[i].crossSells();

                    if (extData != null && extData.event && extData.eventLines != null) {
                        tmpItem.eventGroupId = extData.eventGroupId;

                        //open date
                        if(!extData.defineOpenDate) {
                            tmpItem.eventLineId = extData.defaultEventLineId;
                        }else {
                            tmpItem.eventLineId = $('#ebSession' + listingId).val();
                            $.each(extData.eventLines, function(idx, line) {
                                if (tmpItem.eventLineId == line.eventLineId) {
                                    tmpItem.eventSessionName = line.eventName;
                                    return false;
                                }
                            });
                            tmpItem.selectedEventDate = $('#ebDate' + listingId).val();
                        }

                    }

                    mainItems.push(tmpItem);

                    //here get the top up
                    var crossSells = tmpItem.crossSells;
                    for(var j = 0; j < crossSells.length; j++) {
                        crossSells[j].parentItemQty(tmpItem.qty);
                        crossSells[j].qty(0);

                        //TODO store also the event details
                        //s.parentEventGroupId = ko.observable(); //seems like need to remember the date
                        //s.parentEventLineId = ko.observable();//seems like need to remember the date
                        //s.parentSelectedEventDate = ko.observable();//seems like need to remember the date
                        if(tmpItem.eventGroupId != null && tmpItem.eventGroupId != "") {
                            crossSells[j].parentEventGroupId(tmpItem.eventGroupId);
                            crossSells[j].parentEventLineId(tmpItem.eventLineId);
                            crossSells[j].parentSelectedEventDate(tmpItem.selectedEventDate);
                        }


                        topups.push(crossSells[j]);
                    }
                }
            }

            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner/add-to-cart',
                data: JSON.stringify({
                    sessionId: '',
                    items: mainItems
                }), headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        s.errorMessage('');
                        console.log("data.data.items.length:"+data.data.totalQty);
                        nvx.cartViewModel.totalQty(data.data.totalQty);
                        //$("#cartItemNum").text(data.data.totalQty+" items");
                        s.showProductTable(false);

                        var savedCmsProducts = data.data.cmsProducts;
                        var savedItems;
                        $.each(savedCmsProducts, function(idx, item) {
                            if(item.id == s.currentProd().prodId()) {
                                savedItems = item.items;
                            }
                        });

                        var mainItems = [];
                        $.each(savedItems, function(idx, item) {
                            if(item.topup == false) {
                                mainItems[item.listingId] = item;
                            }
                        });

                        var savedTopupsMap = data.data.cartTopupMap;
                        var savedTopups = savedTopupsMap[s.currentProd().prodId()]; //list of topups from the cart for this product

                        s.topups([]);
                        var hasUncheckedTopup = false;
                        for(var i = 0; i < topups.length; i++) {
                            //TODO evaluate this one!!!!
                            var key = topups[i].parentAxProductListingId() + "-" + topups[i].listingId();
                            var savedTopup = savedTopups[key];
                            if(savedTopup != null) {
                                topups[i].qty(savedTopup.qty);  //ito naman is well, auto add na kasi pag meron na value si parent eh
                                topups[i].addedInCart(true);
                            }else {
                                var mainItemKey = topups[i].parentAxProductListingId();
                                if(mainItems[mainItemKey] != null) {
                                    topups[i].parentItemQty(mainItems[mainItemKey].qty); //this one is if may inadd na bago, tapos di pa nakatick, tapos kasi diba papakita ung value
                                }
                                topups[i].addedInCart(false);
                                topups[i].addToTopUp(false);
                                hasUncheckedTopup = true;

                            }
                        }
                        s.topups(topups);

                        if(hasUncheckedTopup) {
                            s.showAddTopups(true);
                        }else {
                            s.showAddTopups(false);
                        }

                        //loop tru each topups, then check if top up is already in the cart, if so, then have a property like "addedInCart" = true, then get the qty
                        //
                        //
                        //var $topupForm = $("div[data-topups-body=true]");
                        //var $rows = $topupForm.find('[data-topup-id]');
                        //$rows.each(function(idx, row) {
                        //    var $row = $(row);
                        //    var topupId = $row.attr('data-topup-id');
                        //    var chkbox = $row.find('[data-topup-chk]');
                        //
                        //    if(chkbox.length > 0) {
                        //        //get the topups
                        //
                        //    }
                        //    var isAdded = tam.existingTopups[topupId] != undefined;
                        //
                        //    if (isAdded) {
                        //        $row.find('[data-topup-added]').show();
                        //        $row.find('[data-topup-qty]').text(tam.mainQty);
                        //    } else {
                        //        hasUnfilled = true;
                        //        if (tam.hideAdult && ttype == 'Adult') { return; }
                        //        if (tam.hideAdult && ttype == 'Child') { return; }
                        //
                        //        var $chk = $row.find('[data-topup-chk]').show().attr('data-chk-active', 'true');
                        //        if ($chk.attr('data-chk-init') == 'true') {
                        //            $chk.off('change');
                        //        }
                        //
                        //        $chk.attr('data-chk-init', 'true');
                        //        $chk.change(function() {
                        //            var $this = $(this);
                        //            if ($this.is(':checked')) {
                        //                $row.find('[data-topup-qty]').text(tam.mainQty);
                        //            } else {
                        //                $row.find('[data-topup-qty]').text('0');
                        //            }
                        //        });
                        //    }
                        //
                        //    hasVisible = true;
                        //    $row.show();
                        //});

                        //the add to cart API should return the topup so far, including those that are just added
                        //then dito mo na iprocess ung logic na if merong top up, you just show the top up qty with non editable checkbox
                        //then then if walang top up, you show the checkbox, then pag nag tick ka, you show the qty of the main qty
                        //so when you add to topup, you call the api call, then you actually rerender nlng ulit the whole grid to make sure it is the latest one.
                        if(s.topups() != null && s.topups().length > 0) {
                            s.showTopupTable(true);
                            $('a[data-btn-view-cart]').addClass('btn-secondary').removeClass('btn-primary');
                        }else {
                            $('a[data-btn-view-cart]').addClass('btn-primary').removeClass('btn-secondary');
                        }

                        s.showViewCart(true);


                        for(var i = 0; i < tmpItems.length; i++) {
                            var listingId = tmpItems[i].listingId();
                            var extData = pxdMap[listingId];
                            tmpItems[i].qty("");

                            if(extData != null && extData.event) {
                                $('#ebQty' + listingId).val("");
                                $('#ebDate' + listingId).val("");
                                $('#ebCancel' + listingId).click();
                            }
                        }
                    } else {
                        s.errorMessage(data.message);
                    }
                },
                error: function () {
                    s.errorMessage('Unable to process your request. Please try again in a few moments.');
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }

        s.addTopupToCart = function() {

            var topupItems = [];
            var topups = s.topups();

            for(var i = 0; i < topups.length; i++) {
                var topupItem = {};
                if(topups[i].addToTopUp()) {
                    topupItem.cmsProductId = s.currentProd().prodId();
                    topupItem.parentCmsProductId = s.currentProd().prodId();
                    topupItem.parentListingId = topups[i].parentAxProductListingId();
                    topupItem.listingId = topups[i].listingId();
                    topupItem.productCode = topups[i].productCode();
                    topupItem.name = topups[i].itemName();
                    topupItem.type = topups[i].ticketType();
                    topupItem.qty = topups[i].qty();
                    topupItem.price = Number(topups[i].ticketPrice());
                    topupItem.isTopup = true;
                    topupItems.push(topupItem);
                }

            }

            if(topupItems.length == 0) {
                alert("Please select at least 1 top up to add.");
                return;
            }

            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner/add-topup-to-cart',
                data: JSON.stringify({
                    sessionId: '',
                    items: topupItems
                }), headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function(data) {
                    if (data.success) {
                        if (data.data) {
                            //do something here

                            nvx.cartViewModel.totalQty(data.data.totalQty);
                            var savedTopupsMap = data.data.cartTopupMap;
                            var savedTopups = savedTopupsMap[s.currentProd().prodId()]; //list of topups from the cart for this product

                            var updatedTopup = s.topups();
                            var hasUncheckedTopup = false;
                            for(var i = 0; i < updatedTopup.length; i++) {
                                var key = updatedTopup[i].parentAxProductListingId() + "-" + updatedTopup[i].listingId();
                                var savedTopup = savedTopups[key];
                                if(savedTopup != null) {
                                    updatedTopup[i].qty(savedTopup.qty);
                                    updatedTopup[i].addToTopUp(false);
                                    updatedTopup[i].addedInCart(true);
                                }else {
                                    hasUncheckedTopup = true;
                                    updatedTopup[i].addToTopUp(false);
                                    updatedTopup[i].addedInCart(false);
                                }
                            }
                            s.topups(updatedTopup);

                            if(hasUncheckedTopup) {
                                s.showAddTopups(true);
                                $('a[data-btn-view-cart]').addClass('btn-secondary').removeClass('btn-primary');
                            }else {
                                s.showAddTopups(false);
                                $('a[data-btn-view-cart]').addClass('btn-primary').removeClass('btn-secondary');
                            }

                        }
                    } else {
                        alert(data.message);
                    }
                },
                error: function() {
                },
                complete: function() {
                }
            });
        }

        s.addMore = function(){
            s.showProductTable(true);
            s.showTopupTable(false);
            s.showViewCart(false);
        }
        s.changeProduct = function(){

            if(s.sliderShown()) {
                s.destroySlider();
            }
            s.showProduct(true);
            s.showProductTable(true);
            s.showTopupTable(false);
            s.currentProd(this);
            s.showViewCart(false);
            s.errorMessage('');

            s.showSlider(); //TODO find out how to do the slider correctly....
            s.sliderShown(true);


            s.clearQtyForCurrProd();
            s.initProdExtData();
        }

        s.showSlider = function() {
            $("#sequence-canvas-" + s.currentProd().prodId()).addClass("sequence-canvas")
            $("#slideshow-slides-" + s.currentProd().prodId()).sequence({
                autoPlay: true,
                autoPlayDelay: 5000,
                nextButton: '.icon-left-arrow',
                prevButton: '.icon-right-arrow'
            }).data('sequence');

        }

        s.destroySlider = function() {
            $("#slideshow-slides-" + s.currentProd().prodId()).data('sequence').destroy();
            $("#sequence-canvas-" + s.currentProd().prodId()).removeClass("sequence-canvas");
            $("#sequence-canvas-" + s.currentProd().prodId() + " li").each(function() {
                $(this).remove();
            })
        }

        s.clearQtyForCurrProd = function() {
            $.each(s.currentProd().axProducts(), function(i, item) {
                item.qty("");
            });
        }

        s.closeProduct = function(){
            s.sliderShown(false);
            s.showProduct(false);
            var tmpItems = s.currentProd().axProducts();
            var pxdMap = {};
            $.each(s.prodExtData, function(idx, mdl) {
                pxdMap[mdl.productId] = mdl;
            });

            for(var i = 0; i < tmpItems.length; i++) {
                var listingId = tmpItems[i].listingId();
                var extData = pxdMap[listingId];
                tmpItems[i].qty("");
                if(extData.event) {
                    $('#ebQty' + listingId).val("");
                    $('#ebDate' + listingId).val("");
                    $('#ebCancel' + listingId).click();
                }
            }
        }

        /*
         * STAR Event
         */

        s.prodExtData = [];

        s.initEventControls = function() {
            $.each(s.prodExtData, function(idx, prod) {
                var isSelectDate = prod.event && (!prod.openDate || prod.singleEvent);

                if (isSelectDate) {

                    var productId = prod.productId;

                    if(prod.eventLines == null) {
                        $('#qty' + productId).hide();
                        $('#soldOut' + productId).show();
                        return;
                    }

                    //var soldOut = false;
                    //$.each(prod.eventLines, function(idx, mdl) {
                    //    $('#qty' + productId).hide();
                    //    var eventDates = mdl.eventDates;
                    //    if(eventDates == null || eventDates.length == 0) {
                    //        $('#qty' + productId).hide();
                    //        $('#soldOut' + productId).show();
                    //        soldOut = true;
                    //        return;
                    //    }
                    //});
                    //
                    //if(soldOut)
                    //   return;

                    var $ebDate = $('#ebDate' + productId);
                    $ebDate.pickadate({
                        format: 'dd/mm/yyyy'
                    });

                    var $sessionSelect = $('#ebSession' + productId);

                    $sessionSelect.empty(); //reset

                    var defaultEventLineId = prod.defaultEventLineId;

                    //filter to default session if there's a default event line id
                    $.each(prod.eventLines, function(idx, mdl) {
                        var appendSession = defaultEventLineId == "" || (defaultEventLineId != "" && mdl.eventLineId == defaultEventLineId);
                        if(appendSession) {
                            $sessionSelect.append($("<option />").val(mdl.eventLineId).text(mdl.eventName));
                        }
                    });


                    $sessionSelect.change(function() {
                        var selectedSession = $(this).val();
                        $.each(prod.eventLines, function(idx, mdl) {
                            if (mdl.eventLineId == selectedSession) {
                                var eventDates = mdl.eventDates;
                                var enableDates = [];
                                enableDates.push(true);

                                var singleEventDateYear;
                                var singleEventDateMonth;
                                var singleEventDateDay;

                                $.each(eventDates, function(idx, eventDate) {
                                    var dtArr = eventDate.date.split('/');
                                    enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])])

                                    if(prod.singleEvent) {
                                        singleEventDateYear = Number(dtArr[2]);
                                        singleEventDateMonth = Number(dtArr[1]) - 1;
                                        singleEventDateDay = Number(dtArr[0]);
                                    }

                                });
                                var $ebDate = $('#ebDate' + productId);
                                $ebDate.pickadate().pickadate('picker').stop();
                                setTimeout(function() {
                                    $ebDate.pickadate({
                                        onStart: function() {
                                            if(prod.singleEvent) {
                                                this.set('select', [singleEventDateYear , singleEventDateMonth, singleEventDateDay]);
                                            }
                                        },
                                        format: 'dd/mm/yyyy', disable: enableDates
                                    });
                                }, 600);
                            }
                        });
                    });
                    $sessionSelect.change();

                    $('#qty' + productId).hide();

                    var $eventBar = $('#eventBar' + productId);
                    var $ebCancelBtn = $('#ebCancel' + productId);
                    var $ebSelectBtn = $('#ebSelect' + productId).show();
                    prod['eventSelected'] = false;

                    $ebSelectBtn.click(function() {
                        $ebSelectBtn.hide();
                        $ebCancelBtn.show();
                        $eventBar.show();
                        prod['eventSelected'] = true;
                    });
                    $ebCancelBtn.click(function() {
                        $ebCancelBtn.hide();
                        $ebSelectBtn.show();
                        $eventBar.hide();
                        prod['eventSelected'] = false;
                    });
                }
            });
        };

        s.initProdExtData = function() {

            var prods = [];
            var $items = $('input[std-id]');
            $.each($items, function(idx, elem) {
                var $elem = $(elem);

                var anItem = {
                    productId: $elem.attr('std-id'),
                    itemId: $elem.attr('std-code')
                };
                prods.push(anItem);
            });

            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner/products-ext',
                data: JSON.stringify({
                    products: prods
                }), headers: { 'Store-Api-Channel' : nvx.API_CHANNEL},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        $.each(data.data.products, function(idx, product) {
                            nvx.spinner.stop();
                            s.prodExtData.push(product);
                        });
                        s.initEventControls();
                    } else {
                        console.log('Error retrieving extended data. Retrying. ' + data);
                        s.initProdExtData();
                    }
                },
                error: function () {
                    console.log('Error retrieving extended data. Retrying. [General]');
                    s.initProdExtData();
                },
                complete: function () {
                }
            });
        };
    }
})(window.nvx = window.nvx || {}, jQuery);

