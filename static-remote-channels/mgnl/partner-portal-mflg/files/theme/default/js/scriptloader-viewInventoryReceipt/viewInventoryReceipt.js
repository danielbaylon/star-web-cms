(function(nvx, $, ko) {
    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        var transId = nvx.getUrlParam("transId");

        if(transId != "") {
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/view-inventory-receipt/" + transId,
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        nvx.receiptView = new nvx.ReceiptView(data.data);
                        ko.applyBindings(nvx.receiptView , document.getElementById('receiptView'));
                    }
                    else
                    {
                        alert(data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }

            });
        }


        $('#btnRegenReceipt').click(function(event) {
            var result = confirm("Are you going to regenerate the receipt?");
            if (result == false) {
                return;
            }
            //        var data = new FormData();
            //        data.append('transId', transId);
            //        $.submitFormRequest('/regenerateReceipt',data,regenerateReceiptCallBack,true);

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/regenerate-receipt/" + nvx.receiptView.id(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        regenerateReceiptCallBack(data);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }

            });
        });
        function regenerateReceiptCallBack(data){
            nvx.spinner.stop();
            alert(data.message);
        };


        $('#btnPopupRegenRevalReceipt').click(function(event) {
            var result = confirm("Are you going to regenerate the revalidate receipt?");
            if (result == false) {
                return;
            }
            //        var data = new FormData();
            //        data.append('transId', transId);
            //        $.submitFormRequest('/regenerateRevalReceipt',data,regenRevalCallBack,true);
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-reval-ticket/" + nvx.receiptView.id(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        regenRevalCallBack(data);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        });

        //TODO
        $('#btnRegenRevalReceipt').click(function(event) {
            var result = confirm("Are you going to regenerate the revalidate receipt?");
            if (result == false) {
                return;
            }
            //        var data = new FormData();
            //        data.append('transId', transId);
            //        $.submitFormRequest('/regenerateRevalReceipt',data,regenRevalCallBack,true);
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-reval-ticket/" + nvx.receiptView.id(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        regenRevalCallBack(data);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        });

        function regenRevalCallBack(data){
            nvx.spinner.stop();
            alert(data.message);
        }

        $('#btnViewRevalReceipt').click(function(event) {
            var htmlcontent;
            var transId =$('#transId').val();
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/get-revalidated-receipt-by-transid/" + nvx.receiptView.id(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        var revalKenoPopup = $("#revalWindow").kendoWindow({
                            width: '980px',
                            modal: true,
                            resizable: false,
                            title: "Revalidation Receipt",
                            actions: ["Close"],
                            viewable : false
                            //deactivate: function() {
                            //    this.destroy();
                            //    if(!$("#revalWindow").length){
                            //        var $div = $('<div />').appendTo('body');
                            //        $div.attr('id', 'revalWindow');
                            //    }
                            //}
                        }).data('kendoWindow');

                        if(nvx.revalReceiptView == null) {
                            nvx.revalReceiptView = new nvx.RevalidationReceiptView(data.data);
                            ko.applyBindings(nvx.revalReceiptView , document.getElementById('revalReceiptView'));
                        }

                        revalKenoPopup.center().open();
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        });
    });


    nvx.receiptView = null;

    nvx.ReceiptView = function(base) {
        var s = this;
        s.base = base;
        s.gstRate =  ko.observable(base.gstRate);
        s.id = ko.observable(base.id);
        s.createdDateStr = ko.observable(base.createdDateStr);
        s.receiptNumber = ko.observable(base.receiptNumber);
        s.tmStatus = ko.observable(base.tmStatus);
        s.accountCode = ko.observable(base.partner.accountCode);
        s.orgName = ko.observable(base.partner.orgName);
        s.address = ko.observable(base.partner.address);

        s.validityStartDateStr = ko.observable(base.validityStartDateStr);
        s.validityEndDateStr = ko.observable(base.validityEndDateStr);
        s.displayStatus = ko.observable(base.displayStatus)
        s.username = ko.observable(base.username);
        s.paymentType = ko.observable(base.paymentType);
        
        s.products = ko.observableArray([]);

        s.revalTransaction = new nvx.RevalidationTransaction(base.revalTransaction);

        s.showRevalidationTrans = ko.computed(function() {
            if(s.revalTransaction != null && s.revalTransaction.tmStatus() == 'Success') {
                return true;
            }
            return false;
        });

        s.allowRevalidation = ko.observable(base.allowRevalidation);

        s.revalidationLink = ko.computed(function() {
            return nvx.ROOT_PATH + "/revalidate-receipt?transId=" + s.id();
        });

        //s.items = ko.observableArray([]);
        //
        //$.each(base.items, function(idx, obj) {
        //    s.items.push(new nvx.ReceiptItem(s, obj));
        //});

        $.each(base.productList, function(idx, obj) {
            s.products.push(new nvx.ReceiptProduct(s, obj, base.items));
        });

        s.grandTotal = ko.computed(function() {
            var gt = 0;
            $.each(s.products(), function(idx, prod) {
                gt += Number(prod.subtotal());
            });
            return gt.toFixed(2);
        });
        s.grandTotalText = ko.computed(function() {
            return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
        });
        s.gst = ko.computed(function() {
            return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
        });
        s.gstRateStr = ko.computed(function() {
            return (s.gstRate()*100).toFixed(0) +'%';
        });
        s.gstText = ko.computed(function() {
            return kendo.toString(Number(s.gst()), "###,###,##0.00");
        });
        s.totalNoGst = ko.computed(function() {
            return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
        });
        s.totalNoGstText = ko.computed(function() {
            return kendo.toString(Number(s.totalNoGst()), "###,###,##0.00");
        });
        s.clickGuard = false;
    };


    nvx.ReceiptProduct = function(parent, base, transItems) {
        var s = this;
        s.parent = parent;

        s.id = ko.observable(base.id);
        s.title = ko.observable(base.name);
        s.subtotal = ko.observable(base.subtotal.toFixed(2));

        s.subtotalText =  ko.computed(function() {
            return kendo.toString(Number(s.subtotal()), "###,###,##0.00");
        });

        s.items = ko.observableArray([]);


        $.each(transItems, function(idx, obj) {
            if(obj.cmsProductId == s.id()) {
                s.items.push(new nvx.ReceiptItem(s, obj));
            }
        });


        //s.showTopupsToAdd = ko.observable(false);
        //s.topupsToAddText = ko.computed(function() {
        //    return s.showTopupsToAdd() ? 'Hide Unselected Top Ups' : 'Add More Top Ups';
        //});
        //s.doToggleTopupsToAdd = function() {
        //    s.showTopupsToAdd(!s.showTopupsToAdd());
        //};

        //s.items = ko.observableArray([]);
        //(function initItems(theBase) {
        //    var iarr = [];
        //    $.each(theBase.items, function(idx, obj) {
        //        var addItem = new nvx.ReceiptItem(s, obj);
        //        iarr.push(addItem);
        //    });
        //    s.items(iarr);
        //})(base);
        //
        //s.additionalTopups = ko.observableArray([]);
        ////Further init (in order for parent dot remove item to work)
        //$.each(s.items(), function(idx, item) {
        //    item.includeInCart(true);
        //});

        //s.subtotalText = ko.computed(function() {
        //    var total = 0;
        //    $.each(s.items(), function(dix, item) {
        //        total += Number(item.total());
        //    });
        //
        //    s.subtotal(total.toFixed(2));
        //    return s.subtotal();
        //});
        //s.subtotalText.subscribe(function(newValue) {
        //    s.parent.hasEdit(true);
        //});
    };

    nvx.ReceiptItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.cmsProductId = ko.observable(base.cmsProductId);
        s.itemId = ko.observable(base.id);
        s.title = ko.observable(base.name);
        s.ticketType = ko.observable(base.type);
        s.description = ko.observable(base.displayDetails);
        s.topup = ko.observable(base.topup);
        //s.priceInCents = base.price * 100;
        s.priceText = ko.observable(base.unitPrice.toFixed(2));
        s.qty = ko.observable(base.qty);
        s.unpackagedQty = ko.observable(base.unpackagedQty);
        s.total = ko.observable(base.subtotal.toFixed(2));

        s.totalText =  ko.computed(function() {
            return kendo.toString(Number(s.total()), "###,###,##0.00");
        });

    };

    nvx.RevalidationTransaction = function(data) {
        var s = this;


        s.id = ko.observable(data==null?"":data.id);
        s.receiptNum = ko.observable(data==null?"":data.receiptNum);
        s.tmStatus = ko.observable(data==null?"":data.tmStatus);
        s.tmStatusDate = ko.observable(data==null?"":data.tmStatusDate);
        s.tmStatusDateStr = ko.computed(function() {
            return kendo.toString(new Date(), "dd MMM yyyy HH:mm");
        });

    }

    nvx.revalReceiptView = null;

    nvx.RevalidationReceiptView = function(data) {
        var s = this;

        s.transId = ko.observable(data.transId);
        s.status = ko.observable(data.status);
        s.revalReceiptNum = ko.observable(data.revalRecepitNum);
        s.accountCode = ko.observable(data.accountCode);
        s.address = ko.observable(data.address);
        s.orgName = ko.observable(data.orgName);
        s.validateStartDateStr = ko.observable(data.validateStartDateStr);
        s.validityEndDateStr = ko.observable(data.validityEndDateStr);
        s.extendRevalMonth = ko.observable(data.extendRevalMonth);
        s.newvalidateStartDateStr = ko.observable(data.newvalidateStartDateStr);
        s.newvalidityEndDateStr = ko.observable(data.newvalidityEndDateStr);
        s.normalRevFeeStr = ko.observable(data.normalRevFeeStr);
        s.topupRevFeeStr = ko.observable(data.topupRevFeeStr);
        s.revalidatedBy = ko.observable(data.revalidatedBy);
        s.tmStatus = ko.observable(data.tmStatus);
        s.revalPaymentType = ko.observable(data.revalPaymentType);
        s.revalidateDtTmStr = ko.observable(data.revalidateDtTmStr);

        s.receiptitems = ko.observableArray([]);

        $.each(data.receiptitems , function(idx, obj) {
            s.receiptitems.push(new nvx.RevalidationReceiptItemView(obj));
        });

        s.revalGSTRateStr = ko.observable(data.revalGSTRateStr);
        s.currency = ko.observable(data.currency);
        s.revalGSTStr = ko.observable(data.revalGSTStr);
        s.revalExclGSTStr = ko.observable(data.revalExclGSTStr);
        s.totalRevalFeeStr = ko.observable(data.totalRevalFeeStr);

        s.mainReceiptLink = nvx.ROOT_PATH + "/view-inventory-receipt?transId=" + s.transId();
    }

    nvx.RevalidationReceiptItemView = function(data) {
        var s = this;
        s.itemTopup = ko.observable(data.itemTopup);
        s.displayName = ko.observable(data.displayName);
        s.singleRevalFeeFullStr = ko.observable(data.singleRevalFeeFullStr);
        s.unpackedQty = ko.observable(data.unpackagedQty);
        s.revalFeeFullStr = ko.observable(data.revalFeeFullStr);
    }

})(window.nvx = window.nvx || {}, jQuery, ko);