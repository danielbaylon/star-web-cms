(function(nvx, $, ko) {
    $(document).ready(function() {
        $.ajax({
            url: nvx.API_PREFIX + '/secured/store-partner/get-finalised-receipt',
            headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    //Check the TM Status
                    if (data.data == null) {

                        //TODO copy the exitsing system pls!!!!
                        $('#pendingMsg').show();
                        $('#transactionAbnormalSection').show();
                        $('#transactionOkSection').hide();
                        $('.receipt-section').hide();
                        $('.main-content-section').hide();
                        setTimeout(function() {
                            window.location.reload();
                        }, 3000);

                    }else {
                        $('#transactionOkSection').show();
                        nvx.checkoutView = new nvx.CheckoutView(data.data);
                        ko.applyBindings(nvx.checkoutView, document.getElementById('receiptView'));
                    }

                } else {
                    $('#tasMsg').show();
                    $('#pendingMsg').hide();
                    $('#transactionAbnormalSection').show();
                    $('#transactionOkSection').hide();
                    $('.receipt-section').hide();
                    $('.main-content-section').hide();
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
        //TODO TOPUPs
    });

    nvx.checkoutView = null;

    nvx.CheckoutView = function(base) {
        var s = this;
        s.base = base;
        s.gstRate =  ko.observable(base.gstRate);

        s.dateOfPurchase = ko.observable(base.dateOfPurchase);
        s.receiptNumber = ko.observable(base.receiptNumber);
        s.transStatus = ko.observable(base.transStatus);

        s.accountCode = ko.observable(base.partner.accountCode);
        s.orgName = ko.observable(base.partner.orgName);
        s.address = ko.observable(base.partner.address);
        s.username = ko.observable(base.username);
        s.validityStartDate = ko.observable(base.validityStartDate);
        s.validityEndDate = ko.observable(base.validityEndDate);

        s.products = ko.observableArray([]);

        $.each(base.cmsProducts, function(idx, obj) {
            s.products.push(new nvx.CheckoutProduct(s, obj));
        });

        s.grandTotal = ko.computed(function() {
            var gt = 0;
            $.each(s.products(), function(idx, prod) {
                gt += Number(prod.subtotal());
            });
            return gt.toFixed(2);
        });
        s.grandTotalText = ko.computed(function() {
            return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
        });
        s.gst = ko.computed(function() {
            return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
        });
        s.gstRateStr = ko.computed(function() {
            return (s.gstRate()*100).toFixed(0) +'%';
        });
        s.gstText = ko.computed(function() {
            return kendo.toString(Number(s.gst()), "###,###,##0.00");
        });
        s.totalNoGst = ko.computed(function() {
            return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
        });
        s.totalNoGstText = ko.computed(function() {
            return kendo.toString(Number(s.totalNoGst()), "###,###,##0.00");
        });
        s.clickGuard = false;
    };

    nvx.CheckoutProduct = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.title = ko.observable(base.name);
        s.subtotal = ko.observable(base.subtotal);

        s.showTopupsToAdd = ko.observable(false);
        s.topupsToAddText = ko.computed(function() {
            return s.showTopupsToAdd() ? 'Hide Unselected Top Ups' : 'Add More Top Ups';
        });
        s.doToggleTopupsToAdd = function() {
            s.showTopupsToAdd(!s.showTopupsToAdd());
        };

        s.items = ko.observableArray([]);
        (function initItems(theBase) {
            var iarr = [];
            $.each(theBase.items, function(idx, obj) {
               // console.log("obj.qty:"+obj.qty)
                var addItem = new nvx.CheckoutItem(s, obj);
                iarr.push(addItem);
            });
            s.items(iarr);
        })(base);

        s.additionalTopups = ko.observableArray([]);
        //console.log("s.items:"+s.items.length);
        //Further init (in order for parent dot remove item to work)
        $.each(s.items(), function(idx, item) {
            item.includeInCart(true);
        });

        s.subtotalText = ko.computed(function() {
            var total = 0;
            $.each(s.items(), function(dix, item) {
                total += Number(item.total());
            });

            s.subtotal(total.toFixed(2));
            return s.subtotal();
        });
        s.subtotalText.subscribe(function(newValue) {
            s.parent.hasEdit(true);
        });
    };

    nvx.CheckoutItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.isNew = ko.observable(false);
        s.scheduleId = base.scheduleId;
        s.cartId = base.cartId;
        s.cartType = base.cartType;
        s.itemId = base.id;
        s.isTopup = base.topup;
        s.title = base.name;
        s.ticketType = base.type;
        s.includeInCart =  ko.observable(true);

        s.description = base.description;

        s.priceInCents = base.price * 100;
        s.priceText = base.price;
       // console.log("base.qty:"+base.qty);
        s.qty = ko.observable(base.qty);

        s.total = ko.observable(base.total);

        s.totalText = ko.computed(function() {
            if (!Number.isInteger(s.qty())){ return 0; }

            var totalInCents = Number(s.qty()) * Number(s.priceInCents);
            s.total((totalInCents / 100).toFixed(2));
            return s.total();
        });
    };

})(window.nvx = window.nvx || {}, jQuery, ko);