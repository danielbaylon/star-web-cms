(function(nvx, $, ko) {

    $(document).ready(function() {

        $("#SCHEDULED_SYNC");

        var channel = nvx.getUrlParam("channel");

        $.ajax({
            url: '/.cms-api/scheduled-sync/get-sync-config',
            headers: {
                'Store-Api-Channel' : channel
            },
            type: 'GET', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    nvx.syncConfigView = new nvx.SyncConfigView(data.data, channel);
                    nvx.syncConfigView.initConfig();
                    ko.applyBindings(nvx.syncConfigView ,document.getElementById("SCHEDULED_SYNC"));

                } else {
                    alert("System Error, please try again later.");
                }
            },
            error: function () {
                alert("System Error, please try again later.");
                //nvx.syncConfigView = new nvx.SyncConfigView('noSync', channel);
                //nvx.syncConfigView.initConfig();
                //ko.applyBindings(nvx.syncConfigView ,document.getElementById("SCHEDULED_SYNC"));
            },
            complete: function () {
                //s.syncOption('noSync');
            }
        });

        $("#scheduleGrid").kendoGrid({
            //dataSource: {
            //    transport: {
            //        read: {
            //            url: '/.cms-api/scheduled-sync/get-daily-sync-dates',
            //            headers: {
            //                'Store-Api-Channel' : channel
            //            },
            //            dataType: 'json',
            //            contentType: 'application/json',
            //            type: 'GET'
            //        }
            //    },
            //    schema: {
            //        data: 'data'
            //    },
            //    pageSize: 10, serverPaging: false, serverSorting: false
            //},
            //dataBound: function () {
            //    //nvx.displayNoResultsFound($("#itemGrid"));
            //},
            //filterable: false,
            //pageable: true,
            columns: [
                {
                    field: "syncDateStr",
                    title: "Sync Time"
                },
                {   field: "syncDate",
                    title: " ",
                    width: 140,
                    template: function(data) {
                        return '<input type = "button" class="btn btn-third" value="REMOVE" onclick="nvx.syncConfigView.removeSyncTime(\'' + data.syncDateNodeName + '\')"/>';
                    }
                }
            ]
        });

        $("#logGrid").kendoGrid({
            //dataSource: {
            //    transport: {
            //        read: {
            //            url: '/.cms-api/scheduled-sync/get-sync-logs',
            //            headers: {
            //                'Store-Api-Channel' : channel
            //            },
            //            dataType: 'json',
            //            contentType: 'application/json',
            //            type: 'GET'
            //        }
            //    },
            //    schema: {
            //        data: 'data'
            //    },
            //    pageSize: 10, serverPaging: false, serverSorting: false
            //},
            //dataBound: function () {
            //    //nvx.displayNoResultsFound($("#itemGrid"));
            //},
            //filterable: false,
            //pageable: true,
            columns: [
                {
                    field: "axProductSyncDateStr",
                    title: "Sync Date Time",
                    template: function(data) {
                        return '<a href="#" onclick="nvx.syncConfigView.viewLog(\'' + data.axSyncLogNodeName + '\')">' + data.axProductSyncDateStr + '</a>';
                    }
                }
            ]
        });

        nvx.dailySyncView = new nvx.DailySyncView(channel);
        ko.applyBindings(nvx.dailySyncView , document.getElementById("syncDateContainer"));

        nvx.logView = new nvx.LogView(channel);
        ko.applyBindings(nvx.logView, document.getElementById("syncLogContainer"));

    });


    nvx.logView = null;

    nvx.LogView = function(channel) {
        var s= this;

        s.logDetails = ko.observable("");

        s.getLogView = function(syncLogNodeName) {
            $.ajax({
                url: '/.cms-api/scheduled-sync/get-sync-log/' + syncLogNodeName,
                headers: {
                    'Store-Api-Channel' : channel
                },
                type: 'GET', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        s.logDetails(data.data.axProductSyncLog);
                    } else {
                        alert("System Error, please try again later.");
                    }
                },
                error: function () {
                    alert("System Error, please try again later.");
                },
                complete: function () {
                    //s.syncOption('noSync');
                }
            });
        }

        s.closeLogView = function() {
            $('#syncLogContainer').data('kendoWindow').close();
        }
    }

    nvx.dailySyncView = null;

    nvx.DailySyncView = function(channel) {
        var s= this;
        s.dailySyncDate = ko.observable("");

        s.closeSyncDateModal = function() {
            $('#syncDateContainer').data('kendoWindow').close();
        }

        s.saveSyncTime = function() {

            if(s.dailySyncDate() == "") {
                alert("Please select a time.");
            }
            if(kendo.parseDate(s.dailySyncDate(), "hh:mm tt") != null) {

                var dailySyncDateDate = kendo.parseDate(s.dailySyncDate(), "hh:mm tt");

                $.ajax({
                    url: '/.cms-api/scheduled-sync/add-daily-sync-date',
                    data: JSON.stringify({
                             syncDate: dailySyncDateDate
                        }),
                    headers: {
                        'Store-Api-Channel' : channel
                    },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            nvx.syncConfigView.refreshDailySyncSchedules();
                            s.closeSyncDateModal();
                        } else {
                            alert("System Error, please try again later.");
                        }
                    },
                    error: function () {
                        alert("System Error, please try again later.");
                    },
                    complete: function () {
                        //s.syncOption('noSync');
                    }
                });
            }else {
                alert("The data format is not correct. Please correct your format to hh:mm tt.");
            }

        }
    }
    nvx.syncConfigView= null;

    nvx.SyncConfigView = function(syncOption, channel) {
        var s = this;
        s.syncOption = ko.observable(syncOption);
        s.savedSyncOption = ko.observable(syncOption);

        s.refreshDailySyncSchedules = function() {
            $.ajax({
                url: '/.cms-api/scheduled-sync/get-daily-sync-dates',
                headers: {
                    'Store-Api-Channel' : channel
                },
                type: 'GET', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        //update the grid data here
                        var dataSource = new kendo.data.DataSource({
                            data:  data.data,
                            pageSize: 10
                        });

                        $('#scheduleGrid').data('kendoGrid').setDataSource(dataSource);
                        $('#scheduleGrid').data('kendoGrid').refresh();

                    } else {
                        alert("System Error, please try again later.");
                    }
                },
                error: function () {
                    alert("System Error, please try again later.");
                },
                complete: function () {
                    //s.syncOption('noSync');
                }
            });
        }

        s.addSynTime = function() {
            s.initConfig();
            $('#syncDateContainer').data('kendoWindow').center().open();
        }

        s.removeSyncTime = function(syncDate) {
            if(confirm("Are you sure you want to remove this scheduled sync time?")) {
                //do the remove
                $.ajax({
                    url: '/.cms-api/scheduled-sync/remove-daily-sync-date/' + syncDate,
                    headers: {
                        'Store-Api-Channel' : channel
                    },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshDailySyncSchedules();
                            alert("Sync Option is removed successfully.");
                        } else {
                            alert("Sync Date already exists. Please select another time.");
                        }
                    },
                    error: function () {
                        alert("System Error, please try again later.");
                    },
                    complete: function () {
                        //s.syncOption('noSync');
                    }
                });
            }
        }

        s.saveSyncConfig = function() {

            if(s.syncOption() != '') {
                $.ajax({
                    url: '/.cms-api/scheduled-sync/save-sync-config/' + s.syncOption(),
                    headers: {
                        'Store-Api-Channel' : channel
                    },
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.savedSyncOption(s.syncOption());
                            s.refreshDailySyncSchedules();
                            alert("Sync Option is saved successfully.");
                        } else {
                            alert("System Error, please try again later.");
                        }
                    },
                    error: function () {
                        alert("System Error, please try again later.");
                    },
                    complete: function () {
                        //s.syncOption('noSync');
                    }
                });
            }else {
                alert("Please select a Sync Schedule Opton.");
            }

        }

        s.refreshLogs = function() {
            $.ajax({
                url: '/.cms-api/scheduled-sync/get-sync-logs',
                headers: {
                    'Store-Api-Channel' : channel
                },
                type: 'GET', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        //update the grid data here
                        var dataSource = new kendo.data.DataSource({
                            data:  data.data,
                            pageSize: 10
                        });

                        $('#logGrid').data('kendoGrid').setDataSource(dataSource);
                        $('#logGrid').data('kendoGrid').refresh();

                    } else {
                        alert("System Error, please try again later.");
                    }
                },
                error: function () {
                    alert("System Error, please try again later.");
                },
                complete: function () {
                    //s.syncOption('noSync');
                }
            });
        }

        s.initConfig = function() {
            if(!$('#syncDateContainer').data('kendoWindow'))
            {
                $('#syncDateContainer').kendoWindow({
                    width: 720,
                    visible: false,
                    modal: true,
                    pinned: true,
                    resizable: false,
                    title: 'Daily Scheduled Sync Date Time',
                    actions: []
                }).data('kendoWindow');
            }


            if(!$('#syncLogContainer').data('kendoWindow'))
            {
                $('#syncLogContainer').kendoWindow({
                    width: 1000,
                    visible: false,
                    modal: true,
                    pinned: true,
                    resizable: false,
                    title: 'Sync Log',
                    actions: []
                }).data('kendoWindow');
            }

            $("#scheduleTime").kendoTimePicker();

            s.refreshDailySyncSchedules();
            s.refreshLogs();

        }

        s.viewLog = function(syncLogNodeName) {
            nvx.logView.getLogView(syncLogNodeName);
            $('#syncLogContainer').data('kendoWindow').center().open();
        }
    };


})(window.nvx = window.nvx || {}, jQuery, ko);