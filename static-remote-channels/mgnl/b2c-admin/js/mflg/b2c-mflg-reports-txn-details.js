$(document).ready(function() {
    $('#transStartDate').kendoDatePicker({
        format: 'dd/MM/yyyy',
        change: function() {
        }
    });
    $('#transEndDate').kendoDatePicker({
        format: 'dd/MM/yyyy',
        change: function() {
        }
    });
    $('#dateOfVisit').kendoDatePicker({
        format: 'dd/MM/yyyy',
        change: function() {
        }
    });
    $("#tranStatus").kendoDropDownList();
    $("#recNum").kendoAutoComplete();
    $("#proName").kendoAutoComplete();
    $("#proCode").kendoAutoComplete();
    $("#exportType").kendoDropDownList();

    $("#search_btn").click(search);
    $("#reset_btn").click(resetFilters);
    $("#export_btn").click(exportFun);

    $("#exportDiv").hide();
});


function initHackKendoGridSortForStruts() {
    $(document).ajaxSend(function(e, xhr, settings) {
        settings.url = settings.url.replace(/%5Bfield%5D/g, '.field').replace(/%5Bdir%5D/g, '.dir');
    });
}

function showInfoDialog(title, msg, callback) {
    if($("#infoDIVContainer").size() == 0)
    {
        var html = "<div id='infoDIVContainer'>";
        html += "<table style='width:80%; margin: 0 auto;'><tr><td style='text-align:center;'><div id = 'infoDIVMSGContiner' style='height:150px;'></div></td></tr>";
        html += "<tr><td style='text-align:center;'><a class='btn' id='info_close_btn' style='margin-right:10px;'>Close</a></td></tr>";
        html += "</table></div>";

        var div = $(html);
        $('body').append(div);
    }
    $("#infoDIVMSGContiner").html(msg);

    if(!$("#infoDIVContainer").data("kendoWindow"))
    {
        $("#infoDIVContainer").kendoWindow({
            width: "400px",
            height: "220px",
            visible: false,
            modal: true,
            actions: []
        }).data("kendoWindow").open().center().title(title);
        $("#info_close_btn").click(function(){
            $("#infoDIVContainer").data("kendoWindow").close();
            if (callback != undefined || callback != null) {
                callback();
            }
        });
        $("#infoDIVContainer").parent().find(".k-window-action").hide();
        $("#infoDIVContainer_wnd_title").css('height', '25px');
    }
    else
    {
        var confirmWindow = $("#infoDIVContainer").data("kendoWindow");
        confirmWindow.open().center().title(title);
    }
}


function resetFilters()
{
    $('#transStartDate').data('kendoDatePicker').value("");
    $('#transEndDate').data('kendoDatePicker').value("");
    $('#dateOfVisit').data('kendoDatePicker').value("");
    $('#recNum').data('kendoAutoComplete').value("");
    $('#proName').data('kendoAutoComplete').value("");
    $('#proCode').data('kendoAutoComplete').value("");
    $('#tranStatus').data('kendoDropDownList').value("");
}

function search()
{
    if(!validate())
    {
        return;
    }

    initGrid(true);
}

var efrom, eto, eProName, eStatus, eRecNum, eDov, eProCode;

function exportFun()
{
    var url;
    url = '/.b2c-admin/b2cmflgadmin/report/transaction-details/exportExcel?';
    url += "startDate=" + efrom;
    url += "&endDate=" + eto;
    url += "&proName=" + eProName;
    url += "&status=" + eStatus;
    url += "&recNum=" + eRecNum;
    url += "&dov=" + eDov;
    url += "&proCode=" + eProCode;
    window.open(url);
}

function initGrid(resetPage)
{
    var from = $('#transStartDate').data('kendoDatePicker').value();
    var to = $('#transEndDate').data('kendoDatePicker').value();
    var status = $('#tranStatus').data('kendoDropDownList').value();
    var recNum = $("#recNum").data("kendoAutoComplete").value();
    var proName = $('#proName').data('kendoAutoComplete').value();
    var dov = $("#dateOfVisit").data('kendoDatePicker').value();
    var proCode = $("#proCode").data('kendoAutoComplete').value();

    var url = '/.b2c-admin/b2cmflgadmin/report/transaction-details/search?';
    url += "startDate=" + convertDate2String(from);
    url += "&endDate=" + convertDate2String(to);
    url += "&proName=" + proName;
    url += "&status=" + status;
    url += "&recNum=" + recNum;
    url += "&dov=" + convertDate2String(dov);
    url += "&proCode=" + proCode;;

    efrom = convertDate2String(from);
    eto = convertDate2String(to);
    eProName = proName;
    eStatus = status;
    eRecNum = recNum;
    eDov = convertDate2String(dov);
    eProCode = proCode;
    $("#exportDiv").show();

    initHackKendoGridSortForStruts();

    var grid = $('#mainGrid').data("kendoGrid");
    if(!grid)
    {
        $('#mainGrid').kendoGrid({
            dataSource: {
                transport: { read: { url: url, cache: false } },
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        fields: {
                            receiptNum: {type: 'string'},
                            createdDate: {type: 'string'},
                            paymentType: {type: 'string'},
                            sactPin: {type: 'string'},
                            displayName: {type: 'string'},
                            itemCode: {type: 'string'},
                            qty: {type: 'number'},
                            subTotal: {type: 'number'},
                            tmStatus: {type: 'string'},
                            promoCode: {type: 'string'},
                            dateOfVisit: {type:'string'},
                            custName: {type:'string'},
                            custEmail: {type:'string'},
                            custMobile: {type:'string'}
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true,
                serverSorting: true,
                sort: { field: "createdDate", dir: "desc" }
            },
            columns: [
                { field: 'receiptNum', title: 'Receipt Number', width: 200 },
                { field: 'createdDate', title: 'Transaction Date', width: 150},
                { field: 'paymentType', title: 'Payment Type', width: 120},
                { field: 'sactPin', title: 'Pin Code', width: 100},
                { field: 'displayName', title: 'Item Name', width: 200},
                { field: 'itemCode', title: 'Item Code', width: 100},
                { field: 'qty', title: 'Quantity', width: 100},
                { field: 'subTotal', title: 'Subtotal (S$)', format: "{0:n}", width: 100 },
                { field: 'tmStatus', title: 'Status', width: 150 },
                { field: 'promoCode', title: 'Promo Code', width: 100},
                { field: 'dateOfVisit', title: 'Date Of Visit', width: 150},
                { field: 'custName', title: 'Customer Name', width: 150},
                { field: 'custEmail', title: 'Email', width: 100},
                { field: 'custMobile', title: 'Contact No.', width: 100}
            ],
            pageable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
        });
    }
    else
    {
        var ds = grid.dataSource;
        ds.options.transport.read.url = url;
        if(resetPage)
        {
            ds.page(1);
        }
        else
        {
            ds.read();
        }
    }
}


function validate()
{
    var from = $('#transStartDate').data('kendoDatePicker').value();
    var to = $('#transEndDate').data('kendoDatePicker').value();
    if((from == null) || (to == null))
    {
        showInfoDialog('Error', "Start date and end date are required.");
        return false;
    }
    if((from != null) && (to != null))
    {
        if(from > to)
        {
            showInfoDialog('Error', "Start date cannot be greater than end date.");
            return false;
        }
    }
    return true;
}

function convertDate2String(date)
{
    if(date == null)
    {
        return "";
    }
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var monthString = month;
    if(month < 10)
    {
        monthString = "0" + monthString;
    }
    var day = date.getDate();
    var dayString = day;
    if(day < 10)
    {
        dayString = "0" + dayString;
    }
    return dayString + "/" + monthString + "/" + year;
}


(function(mflg, $) {

})(window.mflg = window.mflg || {}, jQuery);