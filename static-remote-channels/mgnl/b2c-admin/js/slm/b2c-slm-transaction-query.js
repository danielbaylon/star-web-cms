$(document).ready(function() {

    $("#recNum").kendoAutoComplete();
    $("#custName").kendoAutoComplete();
    $("#idNumber").kendoAutoComplete();
    $("#email").kendoAutoComplete();
    $("#mobile").kendoAutoComplete();
    $("#pin").kendoAutoComplete();

    $("#search_btn").click(search);
    $("#reset_btn").click(resetFilters);

    $("#exportDiv").hide();
});

function resetFilters()
{
    $("#recNum").data('kendoAutoComplete').value("");
    $("#custName").data('kendoAutoComplete').value("");
    $("#idNumber").data('kendoAutoComplete').value("");
    $("#email").data('kendoAutoComplete').value("");
    $("#mobile").data('kendoAutoComplete').value("");
    $("#pin").data('kendoAutoComplete').value("");
}

function search()
{
    initGrid(true);
}

function initGrid(resetPage)
{
    var recNum = $("#recNum").data('kendoAutoComplete').value();
    var custName = $("#custName").data('kendoAutoComplete').value();
    var nric = $("#idNumber").data('kendoAutoComplete').value();
    var email = $("#email").data('kendoAutoComplete').value();
    var mobile = $("#mobile").data('kendoAutoComplete').value();
    var pin = $("#pin").data('kendoAutoComplete').value();
    var url = '/.b2c-admin/b2cslmadmin/transaction-query/search?';
    url += "recNum=" + recNum;
    url += "&custName=" + custName;
    url += "&nric=" + nric;
    url += "&email=" + email;
    url += "&mobile=" + mobile;
    url += "&pin=" + pin;

    //nvx.initHackKendoGridSortForStruts();

    var grid = $('#mainGrid').data("kendoGrid");
    if(!grid)
    {
        $('#mainGrid').kendoGrid({
            dataSource: {
                transport: { read: { url: url, cache: false } },
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: "receiptNum",
                        fields: {
                            receiptNum: {type: 'string'},
                            tmStatus: {type: 'string'},
                            createdDate: {type: 'string'},
                            custName: {type: 'string'},
                            custEmail: {type: 'string'},
                            custMobile: {type: 'string'},
                            pinCode: {type: 'string'},
                            langCode: {type: 'string'},
                        }
                    }
                }, pageSize: recsPerPage, serverPaging: true,
                serverSorting: true,
                sort: { field: "createdDate", dir: "desc" }
            },
            columns: [
                { field: 'receiptNum', title: 'Receipt Number', width: 140,
                    template: '<a href="javascript:showReceipt(\'${id}\')">#:receiptNum #</a>'
                },
                { field: 'tmStatus', title: 'Status', width: 90},
                { field: 'createdDate', title: 'Transaction Date', width: 135},
                { field: 'custName', title: 'Customer Name', width: 150 },
                { field: 'custEmail', title: 'Email', width: 180},
                { field: 'custMobile', title: 'Contact Number', width: 110 },
                { field: 'pinCode', title: 'Pin Code', width: 70},
                { field: 'langCode', title: 'Language', width: 80,
                    template: '<div style="text-align: center;text-transform: uppercase;">#:langCode #</div>'}
            ],
            pageable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            }
        });
    }
    else
    {
        var ds = grid.dataSource;
        ds.options.transport.read.url = url;
        if(resetPage)
        {
            ds.page(1);
        }
        else
        {
            ds.read();
        }
    }

}

function showReceipt(receiptNumber)
{

    nvx.spinner.start();
    $.ajax({
        url: '/.b2c-admin/b2cslmadmin/transaction-query/show-details/' + receiptNumber,
        type: 'GET', cache: false, dataType: 'json',
        success: function(data) {
            if (data.success) {
                initModal(data.data);
            } else {
                alert(data.message);
            }
        },
        error: function() {
            alert('Unable to process your request. Please try again in a few moments.');
        },
        complete: function() { nvx.spinner.stop(); }
    });
}

function initModal(cvm) {
    if (nvx.queryModal == null) {
        nvx.queryModal = $('#queryModal').kendoWindow({
            title: 'Transaction Details',
            modal: true,
            resizable: false,
            width: '1000px'
        }).data('kendoWindow');

        nvx.queryModel = new nvx.QueryModel(nvx.queryModal);
        ko.applyBindings(nvx.queryModel, document.getElementById('queryModal'));
    }

    nvx.queryModel.init(cvm);
    nvx.queryModal.center().open();
}

(function(mflg, $) {

    nvx.queryModal = null;
    nvx.queryModel = null;

    nvx.QueryModel = function(kWin) {
        var s = this;
        s.kWin = kWin;

        s.cvm = {};

        s.transStatus = ko.observable('');
        s.isError = ko.observable(false);
        s.errorCode = ko.observable('');
        s.errorMessage = ko.observable('');

        s.receiptNumber = ko.observable('');
        s.pin = ko.observable('');
        s.barcodeUrl = ko.observable('');

        s.dateOfPurchase = ko.observable('');
        s.paymentType = ko.observable('');
        s.name = ko.observable('');
        s.idText = ko.observable('');
        s.email = ko.observable('');
        s.mobile = ko.observable('');
        s.ccDigits = ko.observable('');

        s.categ = ko.observableArray([]);

        s.hasBookingFee = ko.observable(true);
        s.bookPriceText = ko.observable('');
        s.bookFeeMode = ko.observable('');
        s.bookFeeQty = ko.observable('');
        s.bookFeeSubTotalText = ko.observable('');

        s.grandTotalText = ko.observable('');

        s.hasTnc = ko.observable(false);
        s.tncs = ko.observableArray([]);

        s.init = function(cvm) {
            s.clear();
            s.cvm = cvm;

            //TODO s.hasTnc(cvm.tncs != null && cvm.tncs.length > 0);
            //TODO s.tncs(cvm.tncs);

            //TODO s.ccDigits(cvm.ccDigits);

            s.transStatus(cvm.transStatus);
            s.isError(cvm.error);
            s.errorCode(cvm.errorCode);
            s.errorMessage(cvm.errorMessage);
            s.receiptNumber(cvm.receiptNumber);
            s.pin(cvm.pinCode);
            //TODO s.barcodeUrl(cvm.barcodeUrl);

            s.dateOfPurchase(cvm.dateOfPurchase);
            s.paymentType(cvm.paymentType);

            var cust = cvm.customer;
            s.name(cust.name);
            s.idText(cust.idType + " " + cust.idNo); //TODO, should be coming from the JAVA... there's some logic there
            s.email(cust.email);
            s.mobile(cust.mobile);

            $.each(cvm.cmsProducts, function(idx, val) {
                s.categ.push(new nvx.ReceiptCateg(val));
            });

            s.hasBookingFee(cvm.hasBookingFee);
            s.bookPriceText(cvm.bookPriceText);
            s.bookFeeMode(cvm.bookFeeMode);
            s.bookFeeQty(cvm.bookFeeQty);
            s.bookFeeSubTotalText(cvm.bookFeeSubTotalText);
            s.grandTotalText(cvm.totalTotalText);
        };

        s.clear = function() {
            s.cvm = {};

            s.msg('');
            s.msgErr(false);
            s.transStatus('');
            s.isError(false);
            s.errorCode('');
            s.errorMessage('');
            s.receiptNumber('');
            s.pin('');
            s.barcodeUrl('');
            s.dateOfPurchase('');
            s.paymentType('');
            s.name('');
            s.idText('');
            s.email('');
            s.mobile('');
            s.categ([]);
            s.hasBookingFee(true);
            s.bookPriceText('');
            s.bookFeeMode('');
            s.bookFeeQty('');
            s.bookFeeSubTotalText('');
            s.grandTotalText('');
            s.newEmail('');
            s.updateEmail(false);
            s.ccDigits('');
        };

        s.close = function() {
            s.kWin.close();
            s.clear();
        };

        s.newEmail = ko.observable('');
        s.updateEmail = ko.observable(false);
        s.msg = ko.observable('');
        s.msgErr = ko.observable(false);

        s.doSendEmail = function() {
            s.msg('');

            if(s.updateEmail() && s.newEmail() == "") {
                alert("Please input an Alternate Email.")
                return;
            }

            var theData = {
                receiptNumber: s.receiptNumber(),
                newEmail: s.newEmail(),
                updateEmail: s.updateEmail()
            };

            nvx.spinner.start();
            $.ajax({
                url: '/.b2c-admin/b2cslmadmin/transaction-query/send-email',
                data:  JSON.stringify(theData),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function(data) {
                    s.msgErr(data.success);
                    s.msg('Successfully sent receipt.');
                    if (data.success) {
                        if (s.updateEmail()) {
                            s.email(s.newEmail());
                        }
                        s.newEmail('');
                        s.updateEmail(false);
                    } else {
                        s.msg(data.message);
                    }
                },
                error: function() {
                    s.msgErr(true);
                    s.msg('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };

    nvx.ReceiptCateg = function(data) {
        var s = this;
        s.title = ko.observable(data.name);
        s.type = ko.observable(''); //TODO what is this type?
        s.subtotalText = ko.observable(data.subtotalText);
        s.products = ko.observableArray([]);
        $.each(data.items, function(idx, val) {
            s.products.push(new nvx.ReceiptCategItem(val));
        });
    };

    nvx.ReceiptCategItem = function(data) {
        var s = this;
        s.type = ko.observable(data.type);
        s.title = ko.observable(data.name);
        s.description = ko.observable(data.description);
        s.priceText = ko.observable(data.priceText);
        s.qty = ko.observable(data.qty);
        s.totalText = ko.observable(data.totalText);
    };

})(window.mflg = window.mflg || {}, jQuery);