(function(nvx, $) {

    $(document).ready(function() {
        nvx.hidePage();
        nvx.filterView = new nvx.FilterView();
        nvx.filterView.loadFilterData();
        ko.applyBindings(nvx.filterView, document.getElementById('filterView'));
        nvx.filterView.init();

        nvx.resultView = new nvx.ResultView();
        nvx.resultView.initGrid();

        nvx.showPage();
    });

    nvx.dateRangeWithinNoofMonth = function(from, to){
        var temp = new Date(from.getTime());
        temp.setMonth(parseInt(temp.getMonth()) + 6);
        if(temp < to){
            return false;
        }
        return true;
    };

    nvx.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    nvx.GenericErrorMsg = nvx.ErrorSpan.replace('THE_MSG', 'Unable to process your request. Please try again in a few moments or refresh the page.');

    // view mode
    nvx.filterView = {};
    nvx.resultView = {};

    nvx.FilterView = function() {
        var s = this;
        // list values
        s.fullDisplay = ['amt','email','contact','id','bday','referral','natl','paytype','bookfee','cclast4','bankapprovalcode', 'trafficsource'];
        s.fullDisplayIndexMap = {amt : 21, email : 6, contact : 7, id : 8, bday : 19, referral : 10, natl : 11, paytype : 12, bookfee : 13, cclast4 : 14, bankapprovalcode : 15, trafficsource : 16};

        s.getInvisibleFieldIndex = function(field){
            if(field == undefined || field == null || field == null){
                return -1;
            }
            return s.fullDisplayIndexMap[field];
        };

        // kendo object
        s.ktdf  = null;
        s.ktdt  = null;

        // initialization data
        s.recsPerPage = pageRecsPerPage;
        s.savedPrefs = pageRavedPrefs;

        // variable
        s.filterTdf = ko.observable('');
        s.filterTdt = ko.observable('');
        s.filterName   = ko.observable('');
        s.filterStatus = ko.observable('');
        s.displayCols    = ko.observableArray([]);
        s.filterReceiptNo = ko.observable('');

        // error message
        s.genIsErr = ko.observable(false);
        s.genErrMsg = ko.observable('');

        // generating and exporting
        s.isGenerating = false;
        s.showGrid = ko.observable(false);
        s.showExport = ko.observable(false);

        s.init = function(){
            s.displayCols([]);
            $.each(s.fullDisplay, function(idx, col) {
                if (col != 'pin') {
                    s.displayCols.push(col);
                }
            });

            s.ktdf = $('#filterTdf').kendoDatePicker({ format: nvx.DATE_FORMAT }).data('kendoDatePicker');
            s.ktdt = $('#filterTdt').kendoDatePicker({ format: nvx.DATE_FORMAT }).data('kendoDatePicker');

            if (s.savedPrefs) {
                s.filterReceiptNo(s.savedPrefs.receipt);
                s.filterName(s.savedPrefs.name);
                s.filterStatus(s.savedPrefs.status);
                s.displayCols([]);
                $.each(s.savedPrefs.columnFilters, function(idx, col) {
                    if (col != 'pin') {
                        s.displayCols.push(col);
                    }
                });
            }
        };

        s.loadFilterData = function(){};

        // action reset

        s.doReset = function(){
            s.showExport(false);
            s.showGrid(false);

            s.genErrMsg('');
            s.genIsErr(false);

            s.ktdf.value(null);
            s.ktdt.value(null);
            s.filterTdf('');
            s.filterTdt('');
            s.filterStatus('');

            s.filterReceiptNo('');
            s.filterName('');

            s.displayCols([]);
            $.each(s.fullDisplay, function(idx, col) {
                if (col != 'pin') {
                    s.displayCols.push(col);
                }
            });
        };
        // action generate report
        s.doGenerate = function(){
            if(s.isGenerating){
                // report is in generation
                return;
            }
            try{
                s.genErrMsg('');
                s.genIsErr(false);
                s.showExport(false);
                s.showGrid(false);
                s.startGenerating();
                s.doReportPreview();
            }catch(e){
                console.log('exception : '+e);
                s.stopGenerating();
            }
        };

        s.validatingInput = function(){
            var errMsg = '';
            if (s.ktdf.value() == null || s.ktdt.value() == null) {
                errMsg += '<li>Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
            } else if (s.ktdf.value() > s.ktdt.value()) {
                errMsg += '<li>Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
            }

            var fromDt = s.ktdf.value();
            var toDt = s.ktdt.value();
            if(fromDt != null && toDt != null && (!nvx.dateRangeWithinNoofMonth(fromDt, toDt))){
                errMsg += '<li>Maximum 6 month(s) from "Transaction Date (From)" to "Transaction Date (To)" is allowed.</li>';
            }

            if (errMsg != '') {
                s.stopGenerating();
                s.genErrMsg(errMsg);
                s.genIsErr(true);
                return false;
            }
            return true;
        };

        s.buildRptParam = function(){
            if(!s.validatingInput()){
                return null;
            }

            var pkg = {
                dateFrom: s.filterTdf(),
                dateTo: s.filterTdt(),
                name: s.filterName(),
                receipt: s.filterReceiptNo(),
                status: s.filterStatus(),
                columnFilters: s.displayCols()
            };
            return pkg;
        };

        s.buildInvisibleFields = function(){
            var invisibleFields = [];
            var showFields = s.displayCols();
            var invisilen = s.fullDisplay.length;
            for(var i =  0 ;  i < invisilen ; i++){
                var field = s.fullDisplay[i];
                if($('input[type="checkbox"][value="'+field+'"][data-bind="checked: displayCols"]:checked').length == 0){
                    invisibleFields[invisibleFields.length] = field;
                }
            }
            return invisibleFields
        };

        s.doReportPreview = function(){
            if(!s.validatingInput()){
                return null;
            }
            try{
                var criteria = s.buildRptParam();
                if(criteria != null){
                    var invisibleFields = s.buildInvisibleFields();
                    nvx.resultView.refreshGrid(criteria, invisibleFields);
                }
            }catch(e){
                console.log('exception : '+e);
                s.stopGenerating();
                nvx.spinner.stop();
                if(!e){
                    nvx.ShowInfoDialog('Error', nvx.GenericErrorMsg);
                }else{
                    nvx.ShowInfoDialog('Error', nvx.ErrorSpan.replace('THE_MSG', e));
                }
            }
        };
        s.doDisplayReportPreviewWindow = function(){
            if(s.isGenerating ){
                s.showGrid(true);
            }
            s.stopGenerating();
        };
        s.doDisplayReportExprt = function(){
            s.showExport(true);
        };
        s.startGenerating = function(){
            s.isGenerating = true;
            nvx.spinner.start();
        };
        s.stopGenerating = function(){
            s.isGenerating = false;
            nvx.spinner.stop();
        };
        // action export
        s.doExport = function(){
            var theJson = nvx.resultView.queryCriteria;
            window.open('/.b2c-admin/b2cslmadmin/report/transaction-details/export?theJson=' + encodeURIComponent(JSON.stringify(theJson)));
        };
    };

    nvx.ResultView = function(){
        var t = this;
        t.invisibleColumns = null;
        t.queryCriteria = null;
        t.recsPerPage = pageRecsPerPage ? pageRecsPerPage : nvx.pageSize;
        t.theGrid = null;

        t.refreshGrid = function(criteria, invisibleFields){
            t.invisibleColumns = invisibleFields;
            t.queryCriteria = criteria;
            t.theGrid.dataSource.page(1);
        };
        t.processGridResultList = function(xhr){
            if(xhr){
                if(xhr.response){
                    if(xhr.response.txnRptList){
                        var lst = xhr.response.txnRptList;
                        var len = lst.length;
                        var idx = 0;
                        for(var i = 0 ; i < len ; i++){
                            var obj = lst[i];
                            if(obj){
                                obj['idx'] = (++idx);
                                obj['transDate'] = null;
                            }
                        }
                    }
                }
            }
        };
        t.displayGridFields = function(hideFields){
            var fields = nvx.filterView.fullDisplay;
            var flen   = fields.length;
            for(var i =  0 ; i < flen ; i++){
                var idx = nvx.filterView.getInvisibleFieldIndex(fields[i]);
                t.theGrid.showColumn(parseInt(idx));
            }
            if( t.invisibleColumns != undefined && t.invisibleColumns != null && t.invisibleColumns.length > 0){
                var len = t.invisibleColumns.length;
                for(var i =  0 ; i < len ; i++){
                    var idx = nvx.filterView.getInvisibleFieldIndex(t.invisibleColumns[i]);
                    if(idx != undefined && idx != null && (!isNaN(idx)) && parseInt(idx) > 0){
                        t.theGrid.hideColumn(parseInt(idx));
                    }
                }
            }
            if(t.theGrid != null && t.theGrid.dataSource.data().length > 0){
                nvx.filterView.doDisplayReportExprt();
            }
            // display the windows
            nvx.filterView.doDisplayReportPreviewWindow();
        };
        t.initGrid = function(){
            var dataGrid = jQuery('#resultGrid').data('kendoGrid');
            if(!t.theGrid){
                var dg_trspt = function(){
                    return {
                        read: {
                            url: '/.b2c-admin/b2cslmadmin/report/transaction-details/pre',
                            type: 'post', cache: false, dataType: 'json'
                        },
                        parameterMap: function(options, operation) {
                            if('read' === operation){
                                var theJson = t.queryCriteria;
                                if(theJson != null && theJson != null && theJson != undefined){
                                    options['theJson'] = JSON.stringify(theJson);
                                    return options;
                                }
                            }
                            return options;
                        }
                    };
                };
                var dg_sorting = function(){
                    return { field: 'createdDate', dir: 'desc' };
                };
                var dg_model = function(){
                    return {
                        fields: {
                            idx                :{ type : 'num'},
                            transDateText      :{ type : 'string'},
                            receiptNum         :{ type : 'string'},
                            status             :{ type : 'string'},
                            custName           :{ type : 'string'},

                            lanCode            :{ type : 'string'},
                            pin                :{ type : 'string'},
                            totalAmountText    :{ type : 'string'},

                            custEmail          :{ type : 'string'},
                            custMobile         :{ type : 'string'},
                            custIdNo           :{ type : 'string'},
                            custDob            :{ type : 'string'},

                            custReferSource    :{ type : 'string'},
                            custNationality    :{ type : 'string'},
                            paymentType        :{ type : 'string'},
                            bookFeeText        :{ type : 'string'},

                            ccLast4Digits      :{ type : 'string'},
                            bankApprovalCode   :{ type : 'string'},
                            trafficSource      :{ type : 'string'},

                            value1             :{ type : 'string'},
                            value2             :{ type : 'string'},
                            value3             :{ type : 'string'},
                            value4             :{ type : 'string'}
                        }
                    };
                };

                var dg_columns = function(){
                    return [
                        { width : '135px', sortable: true, field : 'transDateText',        title : 'Transaction Date' },
                        { width : '130px', sortable: true, field : 'receiptNum',           title : 'Receipt No.' },
                        { width : '80px',  sortable: true, field : 'status',               title : 'Status' },
                        { width : '120px', sortable: true, field : 'custName',             title : 'Name' },

                        { width : '100px', sortable: true, field : 'lanCode',              title : 'Language',
                            template: '<div style="text-align: center;text-transform: uppercase;">#:lanCode #</div>' },
                        { width : '100px', sortable: true, field : 'pin',                  title : 'PIN' },

                        { width : '160px', sortable: true, field : 'custEmail',            title : 'Email' },
                        { width : '90px',  sortable: true, field : 'custMobile',           title : 'Mobile' },
                        { width : '90px',  sortable: true, field : 'custIdNo',             title : 'ID' },
                        { width : '100px', sortable: true, field : 'custDob',              title : 'Date of Birth' },

                        { width : '150px', sortable: true, field : 'custReferSource',      title : 'Referral Source' },
                        { width : '120px', sortable: true, field : 'custNationality',      title : 'Nationality' },
                        { width : '120px', sortable: true, field : 'paymentType',          title : 'Payment Type' },
                        { width : '120px', sortable: true, field : 'bookFeeText',          title : 'Booking Fee', attributes: { style : 'text-align: right;' }},

                        { width : '120px', sortable: true, field : 'ccLast4Digits',        title : 'Credit Card' },
                        { width : '120px', sortable: true, field : 'bankApprovalCode',     title : 'Bank<br/>Approval Code' },
                        { width : '200px', sortable: true, field : 'trafficSource',        title : 'Traffic Source' },

                        { width : '80px',  sortable: true, field : 'value1',               title : 'Qty', attributes: { style : 'text-align: right;' }},
                        { width : '180px', sortable: true, field : 'value2',               title : 'Item name' },
                        { width : '100px', sortable: true, field : 'value3',               title : 'Unit Price', attributes: { style : 'text-align: right;' } },
                        { width : '100px', sortable: true, field : 'value4',               title : 'Total Price', attributes: { style : 'text-align: right;' } },
                        { width : '100px', sortable: true, field : 'totalAmountText',      title : 'Receipt Price', attributes: { style : 'text-align: right;' } }
                    ];
                };

                t.theGrid = jQuery('#resultGrid').kendoGrid({
                    dataSource: {
                        transport: dg_trspt(),
                        requestEnd: function(xhr) {
                            t.processGridResultList(xhr);
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: dg_model()
                        },
                        groupable: false,
                        serverPaging: true,
                        serverSorting:true,
                        pageSize: t.recsPerPage,
                        sort: dg_sorting()
                    },
                    dataBound : t.displayGridFields,
                    columns: dg_columns(),
                    pageable: true,
                    columnMenu: true,
                    resizable: true,
                    reorderable: true
                }).data('kendoGrid');
            }
        };
    };

    nvx.showPage = function(){
        $('#pageView').show();
    };

    nvx.hidePage = function(){
        $('#pageView').hide();
    };
})(window.nvx = window.nvx || {}, jQuery);