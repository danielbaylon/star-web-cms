(function(nvx, $) {
    // 1.
    $(document).ready(function() {
        nvx.hidePage();
        nvx.filterView = new nvx.FilterView();
        nvx.filterView.loadFilterData();
        ko.applyBindings(nvx.filterView, document.getElementById('filterView'));
        nvx.filterView.init();

        nvx.resultView = new nvx.ResultView();
        nvx.resultView.initGrid();

        nvx.showPage();
    });

    nvx.dateRangeWithinNoofMonth = function(from, to){
        var temp = new Date(from.getTime());
        temp.setMonth(parseInt(temp.getMonth()) + 6);
        if(temp < to){
            return false;
        }
        return true;
    };

    nvx.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    nvx.GenericErrorMsg = nvx.ErrorSpan.replace('THE_MSG', 'Unable to process your request. Please try again in a few moments or refresh the page.');

    // view mode
    nvx.filterView = {};
    nvx.resultView = {};

    nvx.FilterView = function() {
        var s = this;

        // kendo object
        s.ktdf  = null;
        s.ktdt  = null;

        // initialization data
        s.recsPerPage = pageRecsPerPage;
        s.savedPrefs = pageRavedPrefs;

        // variable
        s.filterTdf = ko.observable('');
        s.filterTdt = ko.observable('');
        s.filterPromoCode = ko.observable('');

        // error message
        s.genIsErr = ko.observable(false);
        s.genErrMsg = ko.observable('');

        // generating and exporting
        s.isGenerating = false;
        s.showGrid = ko.observable(false);
        s.showExport = ko.observable(false);

        s.init = function(){
            s.ktdf = $('#filterTdf').kendoDatePicker({ format: nvx.DATE_FORMAT }).data('kendoDatePicker');
            s.ktdt = $('#filterTdt').kendoDatePicker({ format: nvx.DATE_FORMAT }).data('kendoDatePicker');
        };

        s.loadFilterData = function(){};

        // action reset
        s.doReset = function(){
            s.showExport(false);
            s.showGrid(false);

            s.genErrMsg('');
            s.genIsErr(false);

            s.ktdf.value(null);
            s.ktdt.value(null);
            s.filterTdf('');
            s.filterTdt('');
            s.filterPromoCode('');
        };
        // action generate report
        s.doGenerate = function(){
            if(s.isGenerating){
                // report is in generation
                return;
            }
            try{
                s.genErrMsg('');
                s.genIsErr(false);
                s.showExport(false);
                s.showGrid(false);
                s.startGenerating();
                s.doReportPreview();
            }catch(e){
                console.log('exception : '+e);
                s.stopGenerating();
            }
        };

        s.validatingInput = function(){
            var errMsg = '';
            if (s.ktdf.value() == null || s.ktdt.value() == null) {
                errMsg += '<li>Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
            } else if (s.ktdf.value() > s.ktdt.value()) {
                errMsg += '<li>Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
            }

            var fromDt = s.ktdf.value();
            var toDt = s.ktdt.value();
            if(fromDt != null && toDt != null && (!nvx.dateRangeWithinNoofMonth(fromDt, toDt))){
                errMsg += '<li>Maximum 6 month(s) from "Transaction Date (From)" to "Transaction Date (To)" is allowed.</li>';
            }
            if (errMsg != '') {
                s.stopGenerating();
                s.genErrMsg(errMsg);
                s.genIsErr(true);
                return false;
            }
            return true;
        };

        s.buildRptParam = function(){
            if(!s.validatingInput()){
                return null;
            }
            var pkg = {
                dateFrom: s.filterTdf(),
                dateTo: s.filterTdt(),
                txnPromoCode: s.filterPromoCode()
            };
            return pkg;
        };

        s.doReportPreview = function(){
            if(!s.validatingInput()){
                return null;
            }
            try{
                var criteria = s.buildRptParam();
                if(criteria != null){
                    nvx.resultView.refreshGrid(criteria);
                }
            }catch(e){
                console.log('exception : '+e);
                s.stopGenerating();
                nvx.spinner.stop();
                if(!e){
                    nvx.ShowInfoDialog('Error', nvx.GenericErrorMsg);
                }else{
                    nvx.ShowInfoDialog('Error', nvx.ErrorSpan.replace('THE_MSG', e));
                }
            }
        };
        s.doDisplayReportPreviewWindow = function(){
            if(s.isGenerating ){
                s.showGrid(true);
            }
            s.stopGenerating();
        };
        s.doDisplayReportExprt = function(){
            s.showExport(true);
        };
        s.startGenerating = function(){
            s.isGenerating = true;
            nvx.spinner.start();
        };
        s.stopGenerating = function(){
            s.isGenerating = false;
            nvx.spinner.stop();
        };
        // action export
        s.doExport = function(){
            var theJson = nvx.resultView.queryCriteria;
            window.open('/.b2c-admin/b2cslmadmin/report/transaction-promotion/export?theJson=' + encodeURIComponent(JSON.stringify(theJson)));
        };
    };

    nvx.ResultView = function(){
        var t = this;
        t.queryCriteria = null;
        t.recsPerPage = pageRecsPerPage ? pageRecsPerPage : nvx.pageSize;
        t.theGrid = null;

        t.refreshGrid = function(criteria){
            t.queryCriteria = criteria;
            t.theGrid.dataSource.page(1);
        };
        t.processGridResultList = function(xhr){
            if(xhr){
                if(xhr.response){
                    if(xhr.response.txnRptList){
                        var lst = xhr.response.txnRptList;
                        var len = lst.length;
                        var idx = 0;
                        for(var i = 0 ; i < len ; i++){
                            var obj = lst[i];
                            if(obj){
                                obj['idx'] = (++idx);
                                obj['validFrom'] = kendo.toString(kendo.parseDate(obj['validFrom']), 'dd/MM/yyyy');
                                obj['validTo'] = kendo.toString(kendo.parseDate(obj['validTo']), 'dd/MM/yyyy');
                            }
                        }
                    }
                }
            }
        };
        t.displayGridFields = function(hideFields){
            if(t.theGrid != null && t.theGrid.dataSource.data().length > 0){
                nvx.filterView.doDisplayReportExprt();
            }
            // display the windows
            nvx.filterView.doDisplayReportPreviewWindow();
        };
        t.initGrid = function(){
            var dataGrid = jQuery('#resultGrid').data('kendoGrid');
            if(!t.theGrid){
                var dg_trspt = function(){
                    return {
                        read: {
                            url: '/.b2c-admin/b2cslmadmin/report/transaction-promotion/pre',
                            type: 'post', cache: false, dataType: 'json'
                        },
                        parameterMap: function(options, operation) {
                            if('read' === operation){
                                var theJson = t.queryCriteria;
                                if(theJson != null && theJson != null && theJson != undefined){
                                    options['theJson'] = JSON.stringify(theJson);
                                    return options;
                                }
                            }
                            return options;
                        }
                    };
                };
                var dg_sorting = function(){
                    return { field: 'promoCode', dir: 'asc' };
                };
                var dg_model = function(){
                    return {
                        fields: {
                            promoCode               :{ type : 'string'},
                            validFromText           :{ type : 'string'},
                            validToText             :{ type : 'string'},
                            txnItemCode             :{ type : 'string'},
                            txnItemName             :{ type : 'string'},
                            txnItemUnitPrice        :{ type : 'number'},
                            txnItemQty              :{ type : 'number'},
                            totalAmount             :{ type : 'number'}
                        }
                    };
                };

                var dg_columns = function(){
                    return [
                        { width : '90px',  sortable: true, field : 'promoCode',           title : 'Promo Code' },
                        { width : '80px', sortable: true, field : 'validFromText',        title : 'Promo Code<br/>(Valid from)' },
                        { width : '80px', sortable: true, field : 'validToText',          title : 'Promo Code<br/>(Valid to)'  },
                        { width : '80px',  sortable: true, field : 'txnItemCode',         title : 'Item Code' },
                        { width : '300px', sortable: true, field : 'txnItemName',         title : 'Item Name' },
                        { width : '80px',  sortable: true, field : 'txnItemUnitPrice',    title : 'Unit Price',           format:'{0:n2}',  attributes: { style : 'text-align: right;' } },
                        { width : '60px',  sortable: false, field : 'txnItemQty',          title : 'Qty',                                    attributes: { style : 'text-align: right;' } },
                        { width : '90px',  sortable: false, field : 'totalAmount',         title : 'Total Amount',         format:'{0:n2}',  attributes: { style : 'text-align: right;' } }
                    ];
                };

                t.theGrid = jQuery('#resultGrid').kendoGrid({
                    dataSource: {
                        transport: dg_trspt(),
                        requestEnd: function(xhr) {
                            t.processGridResultList(xhr);
                        },
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: dg_model()
                        },
                        groupable: false,
                        serverPaging: true,
                        serverSorting:true,
                        pageSize: t.recsPerPage,
                        sort: dg_sorting()
                    },
                    dataBound : t.displayGridFields,
                    columns: dg_columns(),
                    pageable: true,
                    columnMenu: true,
                    resizable: true,
                    reorderable: true
                }).data('kendoGrid');
            }
        };
    };

    nvx.showPage = function(){
        $('#pageView').show();
    };

    nvx.hidePage = function(){
        $('#pageView').hide();
    };
})(window.nvx = window.nvx || {}, jQuery);