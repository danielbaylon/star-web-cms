var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

(function(nvx, $) {

	var channel = '';
	$page = {};
	$(document).ready(function() {

		$.ajax({
			type : 'GET',
			url : nvx.API_PREFIX + 'get-user-channel',
			success : function(result) {
				if (result.success) {
					channel = result.data;
					init();
				} else {
					alert(result.message);
				}
			},
			error : function() {
				alert(ERROR_MSG);
			},
		});

	});

	function init() {

		$page.filterView = new nvx.FilterView();
		ko.applyBindings($page.filterView, $('#filterSectionDiv')[0]);
		$page.filterView.loading(function() {
			$page.baisInfoView = new nvx.BasicInfoView();
			$page.ticketSummaryView = new nvx.TicketSummaryView();
			$page.ticketMgtView = new nvx.TicketMgtView();
			$page.loginHistoryView = new nvx.LoginHistoryView();
			$page.paymentDetailView = new nvx.PaymentDetailView();
			$page.tranHistoryView = new nvx.TranHistoryView();
			$page.redeemHistoryView = new nvx.RedeemHistoryView();
			$page.ticketSoldView = new nvx.TicketSoldView();
			window.winpage = $page;
		});

	}
	nvx.FilterView = function() {
		var s = this;
		s.deviceListData = [];

		s.prepareDeviceListData = function(deviceListData) {
			var devices = [];

			if (deviceListData) {
				var count = deviceListData.length;
				for (var i = 0; i < count; i++) {
					var device = deviceListData[i];
					devices[i] = {
						'device' : ' (' + device.deviceNumber + ') '
								+ device.jcrName,
						'id' : device.jcrName
					};
				}
			}
			return devices;
		};

		s.loadingDeviceList = function(callback) {
			$
					.ajax({
						type : 'GET',
						url : nvx.API_PREFIX + 'get-device-list',
						headers : {
							'Store-Api-Channel' : channel
						},
						beforeSend : function() {
							nvx.spinner.start();
						},
						success : function(result) {
							if (result.success) {
								s.deviceListData = s.prepareDeviceListData(result.data);
								callback();
							} else {
								if (result.message != null
										&& result.message != '') {
									alert(result.message);
								}
							}
						},
						error : function() {
							alert(ERROR_MSG);
						},
						complete : function() {
							nvx.spinner.stop();
						}
					});
		};

		s.loading = function(callback) {
			s.loadingDeviceList(function() {
				s.init();
				callback();
			});

		};

		s.init = function() {
			$('#filterDate').kendoDatePicker({
				format : nvx.DATE_FORMAT
			}).data('kendoDatePicker');
			var now = new Date();
			var today = kendo.toString(kendo.parseDate(now), nvx.DATE_FORMAT);
			$("#filterDate").data("kendoDatePicker").value(today);
			var defaultKioskId = '';
			if (s.deviceListData[0]) {
				defaultKioskId = s.deviceListData[0].id;
			}
			$("#filterKioskId").kendoComboBox({
				dataSource : s.deviceListData,
				dataTextField : "device",
				dataValueField : "id",
				value : defaultKioskId,
			}).data("kendoComboBox");

		};

		s.doReset = function() {
			
            var now = new Date();
            var today = kendo.toString(kendo.parseDate(now), nvx.DATE_FORMAT);
            $("#filterDate").data("kendoDatePicker").value(today);
          
			var defaultKioskId = '';
			if (s.deviceListData[0]) {
				defaultKioskId = s.deviceListData[0].id;
			}

            $('#filterKioskId').data('kendoComboBox').value(defaultKioskId);

		};
		s.doView = function() {
			window.winpage.baisInfoView.refreshGrid();
			window.winpage.ticketSummaryView.refreshGrid();
			window.winpage.ticketMgtView.refreshGrid();
			window.winpage.loginHistoryView.refreshGrid();
			window.winpage.paymentDetailView.refreshGrid();
			window.winpage.tranHistoryView.refreshGrid();
			window.winpage.redeemHistoryView.refreshGrid();
			window.winpage.ticketSoldView.refreshGrid();
			
		};
		s.doExport = function() {
			window.open(nvx.API_PREFIX + '/export-z-report-result');
		};
	};

	nvx.BasicInfoView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-basic-info';
		s.requestUrl = s.baseUrl + '?' + param;
		s.basicInfoDS = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {

				data : 'data',
				model : {
					fields : {
						label : {
							type : "string"
						},
						content : {
							type : "string"
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			},
			pageSize : 10
		});

		s.basicInfoGrid = $('#basicInfoGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.basicInfoDS,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Basic Information</b></label>'
							} ],
							columns : [ {
								field : "label",
								width : 300,
							}, {
								field : "content",
							}, ],
							editable : false,
							pageable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.basicInfoDS.options.transport.read.url = s.baseUrl + '?' + param;
			s.basicInfoDS.read();
		};

		s.basicInfoDS.read();
	};

	nvx.TicketSummaryView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-ticket-summary';
		s.requestUrl = s.baseUrl + '?' + param;
		s.tsDS = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {

				data : 'data',
				model : {
					fields : {
						label : {
							type : "string"
						},
						content : {
							type : "string"
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			},
			pageSize : 10
		});

		s.tsGrid = $('#ticketSummaryGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.tsDS,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Ticket Issuance Summary</b></label>'
							} ],
							columns : [ {
								field : "label",
								width : 300,
							}, {
								field : "content",
							}, ],
							editable : false,
							pageable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.tsDS.options.transport.read.url = s.baseUrl + '?' + param;
			s.tsDS.read();
		};

		s.tsDS.read();
	};
	
	
	nvx.TicketMgtView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-ticket-mgt-event';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
                total: 'total',
				model : {
					fields : {
						event : {
							type : "string"
						},
						qty : {
							type : "string"
						},
						date : {
							type : "string"
						},
						user : {
							type : "string"
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			}, pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true
		});

		s.grid = $('#ticketMgtGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Ticket Management Event</b></label>'
							} ],
							columns : [ {
								field : "event",
								title : "Event",
								sortable : true
							}, {
								field : "qty",
								title : "Qty of Ticket",
								sortable : true
							}, {
								field : "date",
								title : "Done On",
								sortable : true
							}, {
								field : "user",
								title : "Done By",
								sortable : true
							} ],
							editable : false,
							pageable : true,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
	
	
	nvx.LoginHistoryView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-login-history';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
                total: 'total',
				model : {
					fields : {
						username : {
							type : "string"
						},
						datetime : {
							type : "string"
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			},
			pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true
		});

		s.grid = $('#loginHistoryGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Login History</b></label>'
							} ],
							columns : [ {
								field : "username",
								title : "User ID",
								sortable : true
							}, {
								field : "datetime",
								title : "Date/Time",
								sortable : true
							}],
							editable : false,
							pageable : true,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
	
	nvx.PaymentDetailView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-payment-detail';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
				model : {
					fields : {
						mode : {
							type : "string"
						},
						amount : {
							type : "number"
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
	            aggregates: function (response) {
	                return response.aggregate;
	            },
				parse : function(resp) {
					return resp;
				}
			},
			serverAggregates: true,
            aggregate: [{ field: "amount", aggregate: "sum" }]
		});

		s.grid = $('#paymentDetailGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Payment Detail</b></label>'
							} ],
							columns : [ {
								field : "mode",
								title : "Payment Mode",
								sortable : true,
								width : 300,
							}, {
								field : "amount",
								title : "Amount",
								sortable : true,
								format: "{0:c}",
				                aggregates: ["sum"], 
				                footerTemplate: "Total Collection: #= kendo.toString(parseFloat(sum), 'c') #"
							} ],
							editable : false,
							pageable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
	
	nvx.TranHistoryView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-tran-history';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
                total: 'total',
				model : {
					fields : {
                        starTranId: {type: 'string'},
                        axReceiptSeq: {type: "string"},
                        tranDate: {type: 'string'},
                        amount: {type: "number"}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			}, pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true
		});

		s.grid = $('#tranHistoryGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Transaction History</b></label>'
							} ],
				            dataBound: function(){
				                var grid = $("#tranHistoryGrid").data("kendoGrid");
				                var currentPage = s.ds.page();
				                var pageSize = s.ds.pageSize();
				                var rows = this.items();
				                $(rows).each(function () {
				                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
				                    var rowLabel = $(this).find(".row-number");
				                    $(rowLabel).html(index);
				                });
				            },
				            columns: [{
				                field: "rowNumber",
				                title: "No.",
				                template: "<span class='row-number'></span>",
				                sortable: false,
				                width: 40
				            },{
				                field: "starTranId",
				                title: "Transaction ID",
				                sortable: false,
				                width: 150
				            },{
				                field: "axReceiptSeq",
				                title: "Receipt #",
				                sortable: true,
				                width: 80
				            },{
				                field: "tranDate",
				                title: "Transaction Date",
				                sortable: true,
				                width: 160
				            },{
				                field: "amount",
				                title: "Amount",
				                sortable: true,
				                width: 100, 
				                format: "{0:c}"
				            } ],
				            sortable: true, 
				            pageable: true,
							editable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
	
	
	nvx.RedeemHistoryView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-redeem-history';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
                total: 'total',
				model : {
					fields : {
                        starTranId: {type: 'string'},
                        tranDate: {type: "string"},
                        pinCode: {type: 'string'},
                        itemCode: {type: 'string'},
                        itemName: {type: 'string'},
                        bookedQty: {type: 'string'},
                        issuedQty: {type: 'string'}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			}, pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true
		});

		s.grid = $('#redeemHistoryGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Redemption History</b></label>'
							} ],
				            dataBound: function(){
				                var grid = $("#tranHistoryGrid").data("kendoGrid");
				                var currentPage = s.ds.page();
				                var pageSize = s.ds.pageSize();
				                var rows = this.items();
				                $(rows).each(function () {
				                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
				                    var rowLabel = $(this).find(".row-number");
				                    $(rowLabel).html(index);
				                });
				            },
				            columns: [{
				                field: "rowNumber",
				                title: "No.",
				                template: "<span class='row-number'></span>",
				                sortable: false,
				                width: 40
				            },{
				                field: "starTranId",
				                title: "Transaction ID",
				                sortable: false,
				                width: 150
				            },{
				                field: "tranDate",
				                title: "Daet/Tiime",
				                sortable: true,
				                width: 160
				            },{
				                field: "itemCode",
				                title: "Item Code",
				                sortable: true,
				                width: 160
				            },{
				                field: "itemName",
				                title: "Item Name",
				                sortable: true,
				                width: 160
				            },{
				                field: "bookedQty",
				                title: "Reserved",
				                sortable: true,
				                width: 160
				            },{
				                field: "issuedQty",
				                title: "Issued",
				                sortable: true,
				                width: 160
				            }],
				            sortable: true, 
				            pageable: true,
							editable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
	
	nvx.TicketSoldView = function() {
		var s = this;
		var tranDate = $('#filterDate').val();
		var kioskId = $('#filterKioskId').val();

		var param = 'tranDate=' + (tranDate || '');
		param += '&kioskId=' + (kioskId || '');

		s.baseUrl = nvx.API_PREFIX + '/z-report/get-ticket-sold';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				alert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
                data: 'data',
                total: 'total',
				model : {
					fields : {

                        itemCode: {type: 'string'},
                        itemName: {type: 'string'},
                        qty: {type: 'string'},
                        totalAmount: {type: 'number'}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			}, pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true
		});

		s.grid = $('#ticketSoldGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Ticket Sold</b></label>'
							} ],
				            dataBound: function(){
				                var grid = $("#ticketSoldGrid").data("kendoGrid");
				                var currentPage = s.ds.page();
				                var pageSize = s.ds.pageSize();
				                var rows = this.items();
				                $(rows).each(function () {
				                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
				                    var rowLabel = $(this).find(".row-number");
				                    $(rowLabel).html(index);
				                });
				            },
				            columns: [{
				                field: "rowNumber",
				                title: "No.",
				                template: "<span class='row-number'></span>",
				                sortable: false,
				                width: 40
				            },{
				                field: "itemCode",
				                title: "Item Code",
				                sortable: true,
				                width: 160
				            },{
				                field: "itemName",
				                title: "Item Name",
				                sortable: true,
				                width: 160
				            },{
				                field: "qty",
				                title: "QTY",
				                sortable: true,
				                width: 160
				            },{
				                field: "totalAmount",
				                title: "Total Amount",
				                sortable: true,
				                format: "{0:c}",
				                width: 160
				            }],
				            sortable: true, 
				            pageable: true,
							editable : false,
							scrollable : false,
							resizable : false
						});

		s.refreshGrid = function() {
			var tranDate = $('#filterDate').val();
			var kioskId = $('#filterKioskId').val();
			var param = 'tranDate=' + (tranDate || '');
			param += '&kioskId=' + (kioskId || '');

			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};
})(window.nvx = window.nvx || {}, jQuery);
