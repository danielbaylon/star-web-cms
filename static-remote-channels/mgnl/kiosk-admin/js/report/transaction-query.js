var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

(function(nvx, $) {
	
	var channel = '';
	$page = {};
    $(document).ready(function() {
    	
        $.ajax({
            type: 'GET',
            url: nvx.API_PREFIX + 'get-user-channel',
            success:  function(result) {
                if(result.success){
                	channel = result.data;
                	init();
                }else{
                	alert(result.message);
                }
            },
            error: function() {
                alert(ERROR_MSG);
            },
        });

    });

    function init(){
    	
        $page.filterView = new nvx.FilterView();
        ko.applyBindings($page.filterView,$('#filterSectionDiv')[0]);
        $page.filterView.loading(function (){
        	$page.resultView = new nvx.ResultView();
        	window.winpage = $page;
        });
        
    }
    nvx.FilterView = function(){
        var s = this;
        s.filterTdf = null;
        s.filterTdt = null;
        s.selPaVms  = null;
        s.fromDtStr = ko.observable('');
        s.toDtStr   = ko.observable('');
        s.filterPaymentType = ko.observable('');
        s.receiptNum = ko.observable('');
        s.orgName = ko.observable('');
        s.filterStatus = ko.observable('');
        s.accountCode = ko.observable('');
        s.partners    = ko.observable([]);
        s.tranID = ko.observable('');
        s.selPaymentType = null;
        s.selPaymentStatus = null;
        s.filterFromTime = ko.observable('');
        s.filterEndTime = ko.observable('');
        s.selKioskDeviceNum = ko.observable('');
        s.paymentStatus = ko.observable('');
        s.deviceListData = [];
        s.paymentCardTypeListData = [];
        s.paymentStatusListData = [];
		

        s.prepareDeviceListData = function(deviceListData){
            var devices = [];
            devices[0] = {'device' : '[All Hosts]', 'id' : 'ALL'};
            if(deviceListData){
                var count = deviceListData.length;
                for(var i = 0 ; i < count ; i++){
                    var device = deviceListData[i];
                    devices[i + 1] = {'device' : ' (' + device.deviceNumber + ') ' + device.jcrName , 'id' : device.jcrName };
                }
            }
            return devices;
        };
        
        s.preparePaymentCardListData = function(paymentCardListData){
            var cards = [];
            cards[0] = {'card' : '[All Payment Modes]', 'id' : 'ALL'};
            if(paymentCardListData){
                var count = paymentCardListData.length;
                for(var i = 0 ; i < count ; i++){
                    var card = paymentCardListData[i];
                    cards[i + 1] = {'card' : card.desp , 'id' : card.value };
                }
            }
            return cards;
        };
        
        s.loading = function(callback){
            s.loadingDeviceList(function(){
                s.loadingPaymentCardTypeList(function(){
                    s.loadingPaymentStatusList(function(){
                    		s.init();
                    		callback();
                	});
                });
            });
        };

        s.loadingDeviceList = function(callback){
            $.ajax({
                type: 'GET',
                url: nvx.API_PREFIX + 'get-device-list',
                headers : {
                    'Store-Api-Channel' : channel
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(result) {
                    if(result.success){
                    	s.deviceListData = s.prepareDeviceListData(result.data);
                        callback();
                    }else{
                        if(result.message != null && result.message != ''){
                            alert(result.message);
                        }
                    }
                },
                error: function() {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.loadingPaymentCardTypeList = function(callback){
            $.ajax({
                type: 'GET',
                url: nvx.API_PREFIX + 'get-payment-card-type-list',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(result) {
                    if(result.success){
                        s.paymentCardTypeListData = s.preparePaymentCardListData(result.data);
                        callback();
                    }else{
                        if(result.message != null && result.message != ''){
                           alert(result.message);
                        }
                    }
                },
                error: function(result) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.loadingPaymentStatusList = function(callback){
            s.paymentStatusListData = [];
            s.paymentStatusListData[0] = {'id' : 'ALL', "status" : '[All Payment Status]'};
            s.paymentStatusListData[1] = {'id' : 'F', "status" : 'Payment Failed'};
            s.paymentStatusListData[2] = {'id' : 'S', "status" : 'Payment Succeed'};
            callback();
        };
        
        s.init = function(){
            $('#filterTdf').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            $('#filterTdt').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            
            var now = new Date();
            var today = kendo.toString(kendo.parseDate(now), nvx.DATE_FORMAT);
            $("#filterTdf").data("kendoDatePicker").value(today);
          
            var tmr = kendo.toString(kendo.parseDate(new Date(now.setDate(now.getDate()+1))), nvx.DATE_FORMAT);
            $("#filterTdt").data("kendoDatePicker").value(tmr);
            
            $("#filterFromTime").kendoTimePicker({
                format: "HH:mm"
            });
         
            $("#filterEndTime").kendoTimePicker({
                format: "HH:mm"
            });
            var midnight = "00:00";
            $("#filterFromTime").data("kendoTimePicker").value(midnight);
            $("#filterEndTime").data("kendoTimePicker").value(midnight);
            
         
            s.selKioskDeviceNum  = $("#selKioskDeviceNum").kendoMultiSelect({
                dataSource: s.deviceListData,
                dataTextField: "device",
                dataValueField: "id",
                value: "ALL"
            }).data("kendoMultiSelect");
            
            s.selPaymentStatus  = $("#selPaymentStatus").kendoComboBox({
                dataSource: s.paymentStatusListData,
                dataTextField: "status",
                dataValueField: "id",
                value: "ALL"
            }).data("kendoComboBox");
            
            s.selPaymentType = $('#selPaymentType').kendoComboBox({
                dataSource: s.paymentCardTypeListData,
                dataTextField: "card",
                dataValueField: "id",
                value: "ALL"
            }).data("kendoComboBox");
        };

        s.doReset = function(){
            var now = new Date();
            var today = kendo.toString(kendo.parseDate(now), nvx.DATE_FORMAT);
            $("#filterTdf").data("kendoDatePicker").value(today);
          
            var tmr = kendo.toString(kendo.parseDate(new Date(now.setDate(now.getDate()+1))), nvx.DATE_FORMAT);
            $("#filterTdt").data("kendoDatePicker").value(tmr);

            var midnight = "00:00";
            $("#filterFromTime").data("kendoTimePicker").value(midnight);
            $("#filterEndTime").data("kendoTimePicker").value(midnight);
            
            $('#selKioskDeviceNum').data('kendoMultiSelect').value("ALL");
            $('#selPaymentStatus').data('kendoComboBox').value("ALL");
            $('#selPaymentType').data('kendoComboBox').value("ALL");
            $('#tranID').val('');
            $('#axReceiptNo').val('');
        };
        s.doView = function(){
            window.winpage.resultView.refreshGrid();
        };
        s.doExport = function() {
            window.open(nvx.API_PREFIX + '/export-tran-query-result');
        };
    };

    nvx.ResultView = function(){
        var s = this;
        var receipt = null;
        var t = $page.filterView;
        var tranStartDate = $('#filterTdf').val();
        var tranEndDate = $('#filterTdt').val();
        var starTranId = $('#tranID').val();
        var axReceiptNo = $('#axReceiptNo').val();
        var tranStartTime = $('#filterFromTime').val();
        var tranEndTime = $('#filterEndTime').val();
        var paymentStatus = $('#selPaymentStatus').val();
        var paymentType = $('#selPaymentType').val();
        var ticketingKioskId = $('#selKioskDeviceNum').val();
        
        var param = 'tranStartDate=' + (tranStartDate || '');
        param += '&tranEndDate=' + (tranEndDate || '');
        param += '&starTranId=' + (starTranId || '');
        param += '&axReceiptNo=' + (axReceiptNo || '');
        param += '&tranStartTime=' + (tranStartTime || '');
        param += '&tranEndTime=' + (tranEndTime || '');
        param += '&paymentStatus=' + (paymentStatus || '');
        param += '&paymentType=' + (paymentType || '');
        param += '&ticketingKioskId=' + (ticketingKioskId || '');
        
        s.baseUrl = nvx.API_PREFIX + '/get-tran-query-result';
        s.requestUrl = s.baseUrl + '?' + param;
        s.refreshGrid = function(){
        	
            var tranStartDate = $('#filterTdf').val();
            var tranEndDate = $('#filterTdt').val();
            var starTranId = $('#tranID').val();
            var axReceiptNo = $('#axReceiptNo').val();
            var tranStartTime = $('#filterFromTime').val();
            var tranEndTime = $('#filterEndTime').val();
            var paymentStatus = $('#selPaymentStatus').val();
            var paymentType = $('#selPaymentType').val();
            var ticketingKioskId = $('#selKioskDeviceNum').val();
            
            var param = 'tranStartDate=' + (tranStartDate || '');
            param += '&tranEndDate=' + (tranEndDate || '');
            param += '&starTranId=' + (starTranId || '');
            param += '&axReceiptNo=' + (axReceiptNo || '');
            param += '&tranStartTime=' + (tranStartTime || '');
            param += '&tranEndTime=' + (tranEndTime || '');
            param += '&paymentStatus=' + (paymentStatus || '');
            param += '&paymentType=' + (paymentType || '');
            param += '&ticketingKioskId=' + (ticketingKioskId || '');
            
            s.itemsdataSource.options.transport.read.url = s.baseUrl + '?' + param;
            s.itemsdataSource.page(1);
        };
        s.itemsdataSource = new kendo.data.DataSource({
            transport: {
                read: {url: s.requestUrl, cache: false, type : 'get', beforeSend: function(req) {
                    req.setRequestHeader('Store-Api-Channel', channel);
                }}
            },
            requestEnd: function(e) {
                if(e){
					if (e.response) {
						if (e.response.total == 0) {
							$('#exportToExcelBtn').hide();
						}else if (e.response.total > 0) {
							$('#exportToExcelBtn').show();
						}
						
						
					}
                }
            },
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: 'id',
                    fields: {
                    	tranDate: {type: 'string'},
                    	axReceiptNo: {type: 'string'},
                        starTranId: {type: 'string'},
                        axReceiptSeq: {type: "string"},
                        axCartId: {type: "string"},
                        paymentStatus: {type: "string"},
                        amount: {type: "number"},
                        paymentType: {type: 'string'},
                        cardNo: {type: 'string'},
                        bankApprovalCode: {type: 'string'},
                        ticketingKioskId: {type: 'string'},
                        tranStartDateTime: {type: 'string'},
                        tranEndDateTime: {type: 'string'}
                    }
                },
	            aggregates: function (response) {
	                return response.aggregate;
	            },
            }, pageSize: 10, 
            serverSorting: true, 
            serverPaging: true, 
            scrollable:true,
            serverAggregates: true,
            aggregate: [{ field: "amount", aggregate: "sum" }]
        });

        s.queryGrid = $("#queryGrid").kendoGrid({
            dataSource: s.itemsdataSource,
            dataBound: function(){
                var grid = $("#queryGrid").data("kendoGrid");
                var currentPage = s.itemsdataSource.page();
                var pageSize = s.itemsdataSource.pageSize();
                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });
            },
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
            },{
                field: "tranDate",
                title: "Transaction Date",
                sortable: true,
                width: 160
            },{
                field: "axReceiptNo",
                title: "Receipt ID",
                template: '<a href="javascript:window.winpage.resultView.showReceipt(\'#=axCartId#\')" class="link-text" >#=axReceiptNo#</a>',
                sortable: false,
                width: 140
            },{
                field: "starTranId",
                title: "Transaction ID",
                sortable: false,
                width: 150
            },{
                field: "axReceiptSeq",
                title: "Receipt #",
                sortable: true,
                width: 80
            },{
                field: "paymentStatus",
                title: "Payment Status",
                sortable: false,
                width: 130
            },{
                field: "amount",
                title: "Amount",
                sortable: true,
                width: 100, 
                format: "{0:c}",
                aggregates: ["sum"], 
                footerTemplate: "Total Amount: #= kendo.toString(parseFloat(sum), 'c') #"
            },{
                field: "paymentType",
                title: "Payment Type",
                sortable: false,
                width: 150
            },{
                field: "cardNo",
                title: "Card #",
                sortable: false,
                width: 140
            },{
                field: "bankApprovalCode",
                title: "Bank Approval Code",
                sortable: false,
                width: 150
            },{
                field: "ticketingKioskId",
                title: "Kiosk ID",
                sortable: false,
                width: 150
            },{
                field: "tranStartDateTime",
                title: "Transaction Start Date/Time",
                sortable: false,
                width: 180
            },{
                field: "tranEndDateTime",
                title: "Transaction End Date/Time",
                sortable: false,
                width: 180
            }
            
            ],
            sortable: true, pageable: true
        });
        
        

        s.initTransDetailsModel = function(receipt){
            if (window.winpage.queryModal == undefined || window.winpage.queryModal == null) {
                window.winpage.queryModal = $('#tranReceiptModal').kendoWindow({
                    width: '1000px',
                    title: 'Transaction Receipt',
                    modal: true,
                    resizable: false
                }).data('kendoWindow');
                window.winpage.queryModel = new nvx.QueryModel(window.winpage.queryModal);
                ko.applyBindings(window.winpage.queryModel, $('#tranReceiptModal')[0]);
            }
            window.winpage.queryModel.init(receipt);
            window.winpage.queryModal.center().open();
        };
        s.showReceipt = function(axCartId){
            $.ajax({
                type: 'GET',
                url: nvx.API_PREFIX + 'get-receipt-by-ax-card-id?axCartId='+axCartId,
                headers : {
                    'Store-Api-Channel' : channel
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(result) {
                    if(result.success){
                        s.receipt = result.data;
                        s.initTransDetailsModel(s.receipt);
                    }else{
                        if(result.message != null && result.message != ''){
                            alert(result.message);
                        }
                    }
                },
                error: function() {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        
        };
        
        s.getPDF = function(tranReceiptModalClass){
            kendo.drawing.drawDOM($(tranReceiptModalClass)).then(function(group){
                kendo.drawing.pdf.saveAs(group, 'receipt-' + $('#repAxReceiptNo').text() + '.pdf');
              });
        };
        
    };

    nvx.QueryModel = function(kWin) {
        var s = this;
        s.init = function(receipt) {
            s.clear();
            kendo.culture("en-US");
            $('#repChannelLogo').attr('src', 'data:image/png;base64,' + receipt.param.logo);
            
			$('#repAddress').text(receipt.param.address || '');
			$('#repPostalCode').text(receipt.param.postalCode || '');
			$('#repContactDetails').text(receipt.param.contactDetails || '');
			$('#repGstDetails').text(receipt.param.gstDetails || '');
			$('#repCompanyName').text(receipt.param.companyName || '');
			
			
			$('#repKioskLocaction').text(receipt.data.location || '');
			$('#repTranDate').text(receipt.data.date);
			$('#repKioskId').text(receipt.data.kioskId || '');
			$('#repStarTranId').text(receipt.data.transId|| '');
			$('#repReceiptSeq').text(receipt.data.axReceiptId|| '');
			
			$('#repPuchasedProduct').text('');
	        $.each(receipt.data.cmsList, function (index, cms) {
	        	var cmsHTML = '';
	        
	        	if(index != 0 ){
	        		cmsHTML += '<br/>';
	        	}
	        	cmsHTML += '<div class="span20"><div class="span4"></div><div class="span12"><b>' + cms.name + '</b></div></div>';
	        	$('#repPuchasedProduct').append(cmsHTML);
	        	
	        	$.each(cms.axDataList, function (index, ax) {
	        		var axHTML = '<div class="span20"><div class="span4"></div><div class="span12">' + ax.name + '</div></div>';
	        		 axHTML += '<div><div class="span10"></div><div class="span3">' + ax.qty+'</div><div class="span3 k-text-right">' + ax.amount + '</div></div>';
	        		 axHTML += '<div><div class="span9"></div><div class="span4"> Sub Total :' +'</div><div class="span3 k-text-right">' + ax.subTotalAmount + '</div></div>';
	        		 $('#repPuchasedProduct').append(axHTML);
	        	});
	        });
		
			$('#repTranTotalAmount').text(kendo.toString(receipt.data.totalAmount, 'c') || '');
		
			$('#repTranGstAmount').text(kendo.toString(receipt.data.totalGstAmount, 'c') || '');
			
			$('#repPayTid').text(receipt.data.payment.tid|| '');
			$('#repPayMid').text(receipt.data.payment.mid || '');
			$('#repPayBatchNo').text(receipt.data.payment.batchNo || '');
			$('#repPayInvoiceNo').text(receipt.data.payment.invoiceNo || '');
			$('#repPayApprovalCode').text(receipt.data.payment.approvalCode || '');
			$('#repPayRrn').text(receipt.data.payment.rrn || '');
			$('#repPayMerchant').text(receipt.data.merchant || '');
			$('#repPayEntryMode').text(receipt.data.payment.entryMode|| '');
			$('#repPayTvr').text(receipt.data.payment.tvr|| '');
			$('#repPayAid').text(receipt.data.payment.aid || '');
			$('#repPayAppLabel').text(receipt.data.payment.appLabel || '');
			$('#repPayTc').text(receipt.data.payment.tc || '');
			$('#repPayMode').text(receipt.data.payment.paymentMode || '');
			$('#repPayCardLabel').text(receipt.data.payment.cardLabel || '');
			$('#repPayCardNo').text(receipt.data.payment.cardNo || '');
			
			$('#repSignatureDiv').hide();
			$('#repNoSignatureDiv').show();
			if(receipt.data.payment.signatureRequired){
				$('#repNoSignatureDiv').hide();
				$('#repSignatureDiv').show();
				$('#repSignagureData').attr('src', 'data:image/png;base64,' + receipt.data.payment.signatureData);
				$('#repCardHolderName').text(receipt.data.payment.cardHolderName);
			}
			
			$('#repTicketNotIssued').hide();
			$('#repTicketIssued').show();
			if(!receipt.data.allTicketsPrinted){
				$('#repTicketIssued').hide();
				$('#repTicketNotIssued').show();
				$('#repQrCodeData').attr('src', 'data:image/png;base64,' + receipt.data.qrCodeData);
				$('#repAxReceiptNo').text(receipt.data.axReceiptNo);
			}
			$('.nfp-section').hide();
			$('.nets-section').hide();
			$('.cc-section').show();
			if(receipt&&receipt.data&&receipt.data.payment&&receipt.data.payment.paymentMode){
				if('NETS FLASHPAY (NFP)' == receipt.data.payment.paymentMode){
					$('.cc-section').hide();
					$('.nets-section').hide();
					$('.nfp-section').show();
					$('#repOrigBal').text(receipt.data.payment.originalBalance || '');
					$('#repNFPAmount').text(' - ' + receipt.data.totalAmount || '');
				    $('#repRemainBal').text(receipt.data.payment.remainingBalance || '');
				    $('#repDateTime').text(receipt.data.payment.dateTime || '');

				}else if('NETS PIN DEBIT' == receipt.data.payment.paymentMode){
					$('.nfp-section').hide();
					$('.cc-section').hide();
					$('.nets-section').show();
					$('#repNETSTrace').text(receipt.data.payment.netsTrace || '');
				    $('#repNETSReceipt').text(receipt.data.payment.netsReceipt || '');
				    $('#repDateTime').text(receipt.data.payment.dateTime || '');
				    $('#repNETSIssuer').text(receipt.data.payment.netsIssuer || '');
				}
			}

        };

        s.clear = function() {

        };
        
        s.downloadReceipt = function() {

        };
        s.close = function() {
            s.kWin.close();
            s.clear();
        };
    };



})(window.nvx = window.nvx || {}, jQuery);


