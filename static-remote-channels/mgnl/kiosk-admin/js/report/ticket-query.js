var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

(function(nvx, $) {

	var channel = '';
	$page = {};
	$(document).ready(function() {

		$.ajax({
			type : 'GET',
			url : nvx.API_PREFIX + 'get-user-channel',
			success : function(result) {
				if (result.success) {
					channel = result.data;
					init();
				} else {
					nvx.kendoAlert(result.message);
				}
			},
			error : function() {
				nvx.kendoAlert(ERROR_MSG);
			},
		});

	});

	function init() {

		$page.filterView = new nvx.FilterView();
		ko.applyBindings($page.filterView, $('#filterSectionDiv')[0]);
		$page.filterView.loading(function() {
			$page.ticketQueryView = new nvx.TicketQueryView();
			window.winpage = $page;
		});

	}
	nvx.FilterView = function() {
		var s = this;
		s.deviceListData = [];
		s.ticketNoData = [];
		s.ticketNoSelection = null;
		s.prepareDeviceListData = function(deviceListData) {
			var devices = [];
			devices[0] = {
				'device' : '[All Hosts]',
				'id' : 'ALL'
			};
			if (deviceListData) {
				var count = deviceListData.length;
				for (var i = 0; i < count; i++) {
					var device = deviceListData[i];
					devices[i + 1] = {
						'device' : ' (' + device.deviceNumber + ') '
								+ device.jcrName,
						'id' : device.jcrName
					};
				}
			}
			return devices;
		};

		s.loadingDeviceList = function(callback) {
			$
					.ajax({
						type : 'GET',
						url : nvx.API_PREFIX + 'get-device-list',
						headers : {
							'Store-Api-Channel' : channel
						},
						beforeSend : function() {
							nvx.spinner.start();
						},
						success : function(result) {
							if (result.success) {
								s.deviceListData = s
										.prepareDeviceListData(result.data);
								callback();
							} else {
								if (result.message != null
										&& result.message != '') {
									nvx.kendoAlert(result.message);
								}
							}
						},
						error : function() {
							nvx.kendoAlert(ERROR_MSG);
						},
						complete : function() {
							nvx.spinner.stop();
						}
					});
		};

		s.loading = function(callback) {
			s.loadingDeviceList(function() {
					 s.init();
					 s.loadingTicketNo();
					 callback();
			});

		};
		
		s.loadingTicketNo = function() {
			var param = 'startDate=' + $('#filterStartDate').val();
			param += '&endDate=' + $('#filterEndDate').val();
			param += '&kioskId=' + $('#filterKioskId').val();
			$.ajax({
				type : 'GET',
				url : nvx.API_PREFIX + '/ticket-query/get-ticket-no-by-device?'
						+ param,
				headers : {
					'Store-Api-Channel' : channel
				},
				beforeSend : function() {
					nvx.spinner.start();
				},
				success : function(result) {
					if (result.success) {
						s.ticketNoData = result.data
						if (s.ticketNoSelection) {
							s.ticketNoSelection.setDataSource(s.ticketNoData);
						}
					} else {
						if (result.message != null && result.message != '') {
							nvx.kendoAlert(result.message);
						}
					}
				
				},
				error : function() {
					nvx.kendoAlert(ERROR_MSG);
				},
				complete : function() {
					nvx.spinner.stop();
				}
			});
		};

		s.init = function() {

			$('#filterStartDate').kendoDateTimePicker({
				format : 'dd/MM/yyyy hh:mm tt'
			});
			$('#filterEndDate').kendoDateTimePicker({
				format : 'dd/MM/yyyy hh:mm tt'
			});

			var now = new Date();
			now.setHours(0, 0, 0, 0);
			$("#filterStartDate").data("kendoDateTimePicker").value(now);

			var tmr = new Date(now.setDate(now.getDate() + 1));
			tmr.setHours(0, 0, 0, 0);
			$("#filterEndDate").data("kendoDateTimePicker").value(tmr);

			$("#filterKioskId").kendoComboBox({
				dataSource : s.deviceListData,
				dataTextField : "device",
				dataValueField : "id",
				value : 'ALL',
				change : s.loadingTicketNo
			});

			s.ticketNoSelection = $("#filterTicketNo").kendoAutoComplete({
				dataSource : s.ticketNoData,
				filter : "startswith",
				separator : ", "
			}).data("kendoAutoComplete");

		};

		s.doReset = function() {
			
			var now = new Date();
			now.setHours(0, 0, 0, 0);
			$("#filterStartDate").data("kendoDateTimePicker").value(now);

			var tmr = new Date(now.setDate(now.getDate() + 1));
			tmr.setHours(0, 0, 0, 0);
			$("#filterEndDate").data("kendoDateTimePicker").value(tmr);

			$('#filterKioskId').data('kendoComboBox').value("ALL");
			
			$('#filterTranId').val('');
			$('#filterTicketNo').val('');
		};
		s.doView = function() {
			window.winpage.ticketQueryView.refreshGrid();
			s.loadingTicketNo();

		};
		s.doExport = function() {
			window.open(nvx.API_PREFIX + '/export-ticket-query-result');
		};
	};

	nvx.TicketQueryView = function() {
		var s = this;
		var startDate = $('#filterStartDate').val();
		var endDate = $('#filterEndDate').val();
		var kioskId = $('#filterKioskId').val();
		var ticketNo = $('#filterTicketNo').val();
		var starTranId = $('#filterTranId').val();
		var param = 'startDate=' + (startDate || '');
		param += '&endDate=' + (endDate || '');
		param += '&kioskId=' + (kioskId || '');
		param += '&ticketNo=' + (ticketNo || '');
		param += '&starTranId=' + (starTranId || '');
		s.baseUrl = nvx.API_PREFIX + '/ticket-query/search';
		s.requestUrl = s.baseUrl + '?' + param;
		s.ds = new kendo.data.DataSource({
			transport : {
				read : {
					url : s.requestUrl,
					cache : false,
					type : 'get',
					beforeSend : function(req) {
						req.setRequestHeader('Store-Api-Channel', channel);
					}
				},
			},
			error : function(e) {
				nvx.kendoAlert(ERROR_MSG);
				e.sender.data([]);
			},
			schema : {
				data : 'data',
				total : 'total',
				model : {
					fields : {
						location : {
							type : 'string'
						},
						starTranId : {
							type : 'string'
						},
						pinCode : {
							type : 'string'
						},
						timeTicketPrinted : {
							type : 'string'
						},
						ticketPrintedKioskId : {
							type : 'string'
						},
						printed : {
							type : 'string'
						},
						issued : {
							type : 'string'
						},
						ticketNo : {
							type : 'string'
						},
						ticketDescription : {
							type : 'string'
						},
						itemName : {
							type : 'string'
						},
						price : {
							type : 'number'
						},
						expiryDate : {
							type : 'string'
						},
						activationDate : {
							type : 'string'
						},
						validityPeriod : {
							type : 'string'
						}
					},
					headerAttributes : {
						"class" : "table-header-cell",
						style : "white-space: normal;"
					}
				},
				parse : function(resp) {
					return resp;
				}
			},
			pageSize : 10,
			serverSorting : true,
			serverPaging : true,
			scrollable : true
		});

		s.grid = $('#ticketQueryGrid')
				.kendoGrid(
						{
							autoBind : false,
							dataSource : s.ds,
							toolbar : [ {
								template : '<label class="k-label "><b>&nbsp;Ticket Query</b></label>'
							} ],
							dataBound : function() {
								var grid = $("#ticketQueryGrid").data(
										"kendoGrid");
								var currentPage = s.ds.page();
								var pageSize = s.ds.pageSize();
								var rows = this.items();
								$(rows).each(
										function() {
											var index = $(this).index() + 1
													+ (currentPage - 1)
													* pageSize;
											var rowLabel = $(this).find(
													".row-number");
											$(rowLabel).html(index);
										});
							},
							columns : [ {
								field : "rowNumber",
								title : "No.",
								template : "<span class='row-number'></span>",
								sortable : false,
								width : 40
							}, {
								field : "location",
								title : "Location",
								sortable : true,
								width : 160
							}, {
								field : "starTranId",
								title : "Transaction ID",
								sortable : true,
								width : 160
							}, {
								field : "pinCode",
								title : "PIN Code",
								sortable : true,
								width : 160
							}, {
								field : "receiptSeq",
								title : "Receipt #",
								sortable : true,
								width : 160
							}, {
								field : "timeTicketPrinted",
								title : "Time Ticket Printed",
								sortable : true,
								width : 160
							}, {
								field : "ticketPrintedKioskId",
								title : "Ticket Printed Kiosk ID",
								sortable : true,
								width : 160
							}, {
								field : "printed",
								title : "Printed",
								sortable : true,
								width : 160
							}, {
								field : "issued",
								title : "Issued",
								sortable : true,
								width : 160
							}, {
								field : "ticketNo",
								title : "Ticket ID",
								sortable : true,
								width : 160
							}, {
								field : "ticketDescription",
								title : "Ticket Description",
								sortable : true,
								width : 160
							}, {
								field : "itemName",
								title : "Item Name",
								sortable : true,
								width : 160
							}, {
								field : "price",
								title : "Price",
								sortable : true,
								width : 160,
								format: "{0:c}"
							}, {
								field : "expiryDate",
								title : "Expiry Date",
								sortable : true,
								width : 160
							}, {
								field : "activationDate",
								title : "Activation Date",
								sortable : true,
								width : 160
							}, {
								field : "validityPeriod",
								title : "Validity Period",
								sortable : true,
								width : 160
							} ],
							sortable : true,
							pageable : true,
							editable : false,
							scrollable : true,
							resizable : true
						});

		s.refreshGrid = function() {
			var startDate = $('#filterStartDate').val();
			var endDate = $('#filterEndDate').val();
			var kioskId = $('#filterKioskId').val();
			var ticketNo = $('#filterTicketNo').val();
			var starTranId = $('#filterTranId').val();
			var param = 'startDate=' + (startDate || '');
			
			param += '&endDate=' + (endDate || '');
			param += '&kioskId=' + (kioskId || '');
			param += '&ticketNo=' + (ticketNo || '');
			param += '&starTranId=' + (starTranId || '');
			
			s.ds.options.transport.read.url = s.baseUrl + '?' + param;
			s.ds.read();
		};

		s.ds.read();
	};

	// attach kendoAlert to the window
	nvx.kendoAlert = function(msg) {
		var win = $("<div>").kendoWindow({
			modal : true
		}).getKendoWindow();
		win.content(msg);
		win.center().open();
	};

})(window.nvx = window.nvx || {}, jQuery);
