[#-------------- ASSIGNMENTS --------------]
[#-- Page's model & definition, based on the rendering hierarchy and not the node hierarchy --]
[#assign urlVersion = "?ver=1.0r002"]
[#assign site = sitefn.site()!]

[#-------------- RENDERING --------------]
<title>${cmsfn.page(content).title!content.windowTitle!content.title!}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="${content.description!""}" />
<meta name="keywords" content="${content.keywords!""}" />
<meta name="author" content="Enovax Pte Ltd." />
<meta name="generator" content="Powered by Enovax - Intuitive Opensource CMS" />

[#assign requestUri = ctx.getRequest().getRequestURI()]
[#assign requestUriArr = "${requestUri}"?split("/")]
[#assign lastUri = requestUriArr[requestUriArr?size-1]!""]
[#assign lastSecUri = requestUriArr[requestUriArr?size-2]!""]

<link href="${ctx.contextPath}/resources/kiosk-mflg/theme/default/css/styleloader.css${urlVersion}" rel="stylesheet">
<script src="${ctx.contextPath}/resources/kiosk-mflg/theme/default/js/scriptloader-lib.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/kiosk-mflg/theme/default/js/scriptloader-home.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/kiosk-mflg/theme/default/js/scriptloader.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/kiosk-mflg/theme/default/js/iscroll.js${urlVersion}"></script>