[#-------------- RENDERING --------------]
<!DOCTYPE html>
<head>
    [@cms.page/]

    [@cms.area name="htmlHeader"/]
</head>
<body>

    [@cms.area name="header"/]

    [@cms.area name="content"/]

    [@cms.area name="footer"/]

</body>