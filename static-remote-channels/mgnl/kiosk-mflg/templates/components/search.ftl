[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]
[#include "/kiosk-mflg/templates/macros/common/keyboard.ftl"]

[#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]

[#if ctx.getParameter("productFilter")?has_content]
    [#assign productFilter = ctx.getParameter("productFilter")]
[#else]
    [#assign productFilter = ""]
[/#if]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = ""]

<div class="main">

    <aside ng-include="templateSideBarUrl" class="ng-scope">
        [#assign counter = 0]
        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">

            [#list categoryList as category]
                [#assign counter = counter + 1]
			    [#if category.hasProperty("iconImage")]
			        [#assign categoryImage = category.getProperty("iconImage").getString()]
			    [/#if]
			
			    [#if categoryImage?has_content]
			        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
			    [/#if]
			    [#assign menuTransportLayout = "false"]
				[#if category.hasProperty("transportLayout")]
			        [#assign menuTransportLayout = category.getProperty("transportLayout").getString()]
			    [/#if]
                <li>
                    [#if (category.getProperty("name").getString()?upper_case == 'FUN PASS')]
                        <div><button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} icon-funpass box-wrapper center-center [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                        </button><div>
                    [#elseif menuTransportLayout == "true"]
                    	
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='admission~${category.getName()}~'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            [#else]
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~?transportLayout=${menuTransportLayout}'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            
                    [/#if]
                    

                </li>

            [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">
    [#if lang == "en"]
        <h1 class="primary-heading-1 list-title ng-binding" style="display: block;">${staticText[lang]['searchLabel']!"Search"}</h1>
        <div class="control-bar" style="display: block;">
            <form class="search-form ng-pristine ng-valid">
                    <span>
                        <input id="search-text" type="text" class="search ng-pristine ng-untouched ng-valid" placeholder="${(productFilter?has_content)?string('','Search . . .')}" ng-model="searchText" ng-click="openKeyboard()">
                        <button id="btn-search" type="button" class="btn btn-xxs btn-primary ng-binding" ng-bind="btn.search" ng-click="showResult(searchText)">${staticText[lang]['searchLabel']!"Search"}</button>
                    </span>
                <!-- ngIf: searchText.length>0 && isResult==false -->
            </form>
            <!-- ngIf: isResult==true && previousSearchText.length>0 -->
        </div>
    [/#if]
    
            [#assign productList = kioskfn.getCMSProductListByName(channel, productFilter)]
            [#if productList?has_content]
                 <div class="product-list">

                    [#if pageNumber?has_content]
                        [#if pageNumber > 1]
                            <button type="button" class="left-arrow" onclick="javascript:window.location.href='${sitePath}/search~?pageNumber=${pageNumber-1}&&productFilter=${productFilter}'"></button>
                        [/#if]
                    [/#if]

                    [#assign totalPages = (productList?size/8)?floor + 1]
					
                    [#if totalPages > 2]
                        [#if pageNumber?has_content]
                            [#if pageNumber != totalPages]
                                <button type="button" class="right-arrow" data-bind="click: kiosk.ProductNavigator.prevPage" onclick="javascript:window.location.href='${sitePath}/search~?pageNumber=${pageNumber+1}&&productFilter=${productFilter}'"></button>
                            [/#if]
                        [#else]
                           
                        [/#if]
                    [/#if]

                    [#assign totalProducts = 0]

                    <ul class="thumbnail-list"> 
                        [#list productList as cmsProduct]

                            [#assign productImages = starfn.getCMSProductImagesUUID(cmsProduct, lang)!]
                       
                            [#assign totalProducts = totalProducts + 1]
                            
                            [#if (((totalProducts/8)?floor) - (totalProducts/8)) == 0]
                            	[#assign pageIndex = (totalProducts/8)]
                            [#else]
                            	[#assign pageIndex = ((totalProducts/8)?floor) + 1]
                            [/#if]
                          
                            [#if pageIndex == pageNumber]
                            
                            	[#assign priceIndicator = 0]
                            	[#assign priceSignLeft = "0"]
                            	[#if cmsProduct.hasProperty("priceIndicator")]
	                                [#assign priceIndicator =cmsProduct.getProperty("priceIndicator").getString()?number]
	                                [#if priceIndicator == 1]
	                                    [#assign priceSignLeft="170px"]
	                                [/#if]
	                                [#if priceIndicator == 2]
	                                    [#assign priceSignLeft="140px"]
	                                [/#if]
	                                [#if priceIndicator == 3]
	                                    [#assign priceSignLeft="110px"]
	                                [/#if]
								[/#if]
                                <li onclick="javascript:window.location.href='${sitePath}/category/product~~~${cmsProduct.getName()}~?pageNumber=${pageIndex}&&productFilter=${productFilter}'">
                                    <div style="position: relative; left: 0; top: 0;">
                                        <img style="position: relative; top: 0; left: 0;" [#if productImages[0]?has_content] src="${damfn.getAssetLink(productImages[0])}" [#else] src="" [/#if] alt="" class="thumbnail-img"  id="rotator-${cmsProduct.getName()}" />
                						<div style="position: absolute; top: 100px; left: ${priceSignLeft};">
                						    [#if priceIndicator != 0]
                    						    [#list 1..priceIndicator as indicator]
                    						        <img  src="${themePath}/images/price-sign.png">
                    						
                    						    [/#list]
                						    [/#if]
                						</div>
            						</div>
                                    [#if productImages?size > 1]

                                        <script type="text/javascript">
                                            (function() {

                                                var rotator = $("#rotator-${cmsProduct.getName()}");
                                                var delayInSeconds = 5;
                                                var images = new Array(${productImages?size});

                                                [#assign counter = 0]
                                                [#list productImages as productImage]
                                                    images[${counter}] = "${damfn.getAssetLink(productImage)}";
                                                    [#assign counter = counter + 1]
                                                [/#list]

                                                var num = 1;
                                                var changeImage = function() {

                                                    rotator.fadeOut(1000, function() {

                                                        var len = images.length;
                                                        num++;

                                                        if (num == len) {
                                                            num = 0;
                                                        }

                                                        rotator.attr("src",images[num]);
                                                    }).fadeIn(1000);
                                                };

                                                setInterval(changeImage, delayInSeconds * 1000);
                                            })();
                                        </script>
                                    [/#if]

                                        <span class="caption">
                                            <span class="caption-title">${cmsProduct.getProperty("name").getString()}</span>
                                            <span class="caption-description">
                                                <div class="block-with-text">${cmsProduct.getProperty("shortDescription").getString()}</div>
                                            </span>
                                        </span>
                                </li>
                            [/#if]

                        [/#list]

                        <div class="clearfix"></div>
                    </ul>

                    <ul class="paging box-wrapper center-center">

                        [#list 1..totalPages as page]

                            <li>
                                <a href="${sitePath}/search?pageNumber=${page}" [#if pageNumber?has_content] [#if page == pageNumber] class="selected" [/#if] [#else] class="selected" [/#if]></a>
                            </li>

                        [/#list]
                        <div class="clearfix"></div>
                    </ul>
                </div>
            
            [#else]
            
            	<div class="adv-banner ng-scope" ng-if="isResult==false">
		            <p class="note-text ng-binding" ng-bind-html="title.promo">
		            [#if lang == "en"]
		            ${staticText[lang]['searchBodyLabel']!"Search"}
		            [#else]
		            ${staticText[lang]['searchBodyLabel1']}
		                <br />
		            ${staticText[lang]['searchBodyLabel2']}
		                <br />
		            ${staticText[lang]['searchBodyLabel3']}
		            [/#if]
		            </p>
		            <a href="#"><img src="${themePath}/images/samples/search-banner.jpg"></a>
	        	</div>
            [/#if]
		<!-- end ngIf: isResult==false -->
        <!-- ngIf: isResult==true -->
    </div>
</div>

<script>
        function inputCharacter(character) {
	        var existingText = $("#search-text").val();
	        var updatedText = existingText.concat(character);
	        $("#search-text").val(updatedText);
	    }
	
	    function deletePreviousCharacter() {
	        var existingText = $("#search-text").val();
	        if(existingText.length > 0) {
	            var updatedText = existingText.slice(0, -1);
	            $("#search-text").val(updatedText);
	        }
	    }
	    function clearAllCharacters() {
	        $("#search-text").val('');
	    }
	    
	    function showKeyboard() {
	        $("#keyboardContainer").show();
	    }
	
	    function hideKeyboard() {
	        $("#keyboardContainer").hide();
	    }
	    function submitKeyboard() {
	       
	        window.location.href='search~?&&productFilter='+$("#search-text").val();
	    }
	    
	    $(document).ready(function() {
	        
	        $('#btn-search').click(function(e) {
	            window.location.href='search~?productFilter='+$("#search-text").val();
	        });
	        
	        $('#search-text').click(function(e) {
	            showKeyboard();
	        });
	        
	    });

	    
</script>