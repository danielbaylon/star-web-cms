[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<header>
    <button type="button" class="logo"></button>

    <nav>
        <ul>
            <li id="btnHome" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-home-grey">${staticText[lang]["homeLabel"]!"Home"}</button>
            </li>
        [#if lang == "en"]
            <li id="btnSearch" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-search-grey">${staticText[lang]["searchLabel"]!"Search"}</button>
            </li>
        [/#if]
            <li id="btnTranslate" style="float: left; display: block; margin-top: 0px;">

            [#if lang == "en"]
                [#if state.originalBrowserURL?contains("/en/")]
                    [#assign localeURL = state.originalBrowserURL?replace("/en/", "/zh_CN/")]
                [#else]
                    [#assign localeURL = "${ctx.contextPath}/zh_CN/kiosk"]
                [/#if]
            [#else]
                [#assign localeURL = state.originalBrowserURL?replace("/zh_CN/", "/en/")]
            [/#if]

                <button type="button" class="btn-lang-grey">[#if lang == "en"] 华 语 [#else] English [/#if]</button>
            </li>
        </ul>
    </nav>

    <div ng-include="templateTodayUrl" class="ng-scope">
        <div class="ng-scope">
            <div class="date-time">
                <span class="date ng-binding" id="todayDate"></span>&nbsp;<span class="time ng-binding" id="todayTime"></span>
            </div>
        </div>
    </div>

    <div id="shoppingCartArea" class="container center-center">
        <div id="funShoppingCart" class="fun-box center-center">
            <div class="items">
                <span id="totalShoppingItemCount" class="cart-grey">0</span>
            </div>
            <div class="funbox-logo ng-binding">
                <img src="${themePath}/images/${staticText[lang]['addToCartLogoName']}" />
            </div>
        </div>
    </div>

    <div id="clearCartArea" class="clearcart-container center-center">
        <button type="button" class="btn-clearcart btn-clearcart-grey">
        [#if lang == "en"]
        ${staticText[lang]["clearCartLabel"]!"Clear Cart"}
        [#else]
        ${staticText[lang]["clearCartLabel1"]!""}
            <br />
        ${staticText[lang]["clearCartLabel2"]!""}
        [/#if]
        </button>
    </div>

</header>