[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]
[#if ctx.getParameter("pageSort")?has_content]
    [#assign pageSort = ctx.getParameter("pageSort")]
[#else]
    [#assign pageSort = ""]
[/#if]
[#if ctx.getParameter("productFilter")?has_content]
    [#assign productFilter = ctx.getParameter("productFilter")]
[#else]
    [#assign productFilter = ""]
[/#if]
[#assign transportLayout = ""]
[#if ctx.getParameter("transportLayout")?has_content]
    [#assign transportLayout = ctx.getParameter("transportLayout")]
[/#if]

<style>
    /* styles for '...' */
    .block-with-text {
        /* hide text if it more than N lines  */
        overflow: hidden;
        /* for set '...' in absolute position */
        position: relative;
        /* use this value to count block height */
        line-height: 1.2em;
        /* max-height = line-height (1.2) * lines max number (3) */
        max-height: 3.6em;
        /* place for '...' */
        margin-right: -1em;
        padding-right: 1em;
    }
    /* create the ... */
    .block-with-text:before {
        /* points in the end */
        content: '...';
        /* absolute position */
        position: absolute;
        /* set position to right bottom corner of block */
        right: 0;
        bottom: 0;
    }
    /* hide ... if we have text, which is less than or equal to max lines */
    .block-with-text:after {
        /* points in the end */
        content: '';
        /* absolute position */
        position: absolute;
        /* set position to right bottom corner of text */
        right: 0;
        /* set width and height */
        width: 1em;
        height: 1em;
        margin-top: 0.2em;
        /* bg color = bg color under block */
        background: white;
    }
</style>
<script>
var itemsInShoppingCart = [];
</script>
[#if params[0]?has_content]
    [#assign categoryNodeName = params[0]]
    [#if params[1]?has_content]
    	[#assign subCaegoryNodeName = params[1]]
    [/#if]
    [#assign categoryNode = starfn.getCMSProductCategory(channel, categoryNodeName)]
    [#assign subCategoryList = starfn.getCMSProductSubCategories(categoryNode, false, true)]
    [#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]

    [#assign counter = 0]

<div class="main">
    <aside ng-include="templateSideBarUrl" class="ng-scope">

        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">

            [#list categoryList as category]
                [#assign counter = counter + 1]
			    [#if category.hasProperty("iconImage")]
			        [#assign categoryImage = category.getProperty("iconImage").getString()]
			    [/#if]
			
			    [#if categoryImage?has_content]
			        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
			    [/#if]
			    [#assign menuTransportLayout = "false"]
				[#if category.hasProperty("transportLayout")]
			        [#assign menuTransportLayout = category.getProperty("transportLayout").getString()]
			    [/#if]
                <li>
                    [#if (category.getProperty("name").getString()?upper_case == 'FUN PASS')]
                        <div><button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} icon-funpass box-wrapper center-center [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                        </button><div>
                    [#elseif menuTransportLayout == "true"]
                    	
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='admission~${category.getName()}~'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            [#else]
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~?transportLayout=${menuTransportLayout}'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            
                    [/#if]
                    

                </li>

            [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">

        [#assign previewMode = ctx.getParameter("preview")!]
        [#assign isOnPreview = false]
        [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
            [#assign isOnPreview = true]
            <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
        [/#if]

        <h1 class="primary-heading-1 list-title ng-binding">${categoryNode.getProperty("name").getString()}</h1>

        [#if transportLayout == "true"]
			[#assign transportProduct = kioskfn.getTransportLayoutProduct(channel)]
			<script>
				var mainItem = {
                    cmsProductId : '${transportProduct.getName()}',
                    cmsProductName : '${transportProduct.getProperty("name").getString()}',
                    listingId : '${transportProduct.getProperty("productListingId").getString()}',
                    productCode : '${transportProduct.getProperty("displayProductNumber").getString()}',
                    name : '${transportProduct.getProperty("itemName").getString()}',
                    type : '${transportProduct.getProperty("ticketType").getString()}',
                    qty : 0,
                    price : parseFloat(${transportProduct.getProperty("productPrice").getString()})
                };
                itemsInShoppingCart.push(mainItem);
             </script>
            <div class="box-wrapper center-center">
                <div class="side-an-dialog box-wrapper center-center">
                    <img src="${themePath}/images/icon-express.png">
                    <h1 class="primary-heading sy-hg-md ng-binding" ng-bind-html="title.question">${staticText[lang]['addTicketToCartConfirmationLabel']!"Would you like to Sentosa Express tickets to your cart?"}</h1>
                    <p ng-bind-html="title.hint1" class="ng-binding">${staticText[lang]['addTicketToCartBodyMessageLabel']!"You can enter Sentosa by using your EZlink card or purchase Sentosa Express tickets here."}</p>
                    <table cellpadding="0" cellspacing="0" border="0" class="admission-qty">
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td class="qty-top ng-binding" ng-bind="lbl.quantity">${staticText[lang]['quantityLabel']!"Quantity"}</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="qty-title ng-binding" width="50%" ng-bind="lbl.type">${staticText[lang]['customerTypeLabel']!"Adult/Child"}</td>
                            <td width="15%"><button id="btn-minus-qty"type="button" class="icon-minus btn-inactive" ng-class="{'btn-inactive':ticket.qty<1,'btn-active':ticket.qty>=1}" ng-click="minusQty()"></button></td>
                            <td width="20%"><input id="qtyInput" type="text" placeholder="0" ng-click="clickQtyInput()" class="ng-pristine ng-untouched ng-valid">
                            
                            </td>
                            <td width="15%"><button id="btn-add-qty" type="button" class="icon-plus btn-active" ng-class="{'btn-inactive':ticket.qty>=maxTickets,'btn-active':ticket.qty<maxTickets}" ng-click="addQty()"></button></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top: 10px; color: #666; text-align: center;"><span style="color:red;">*</span><span ng-bind="title.hint2" class="ng-binding">${staticText[lang]['noteLabel']!"Children from the age of 3 will need an express ticket."}</span></td>
                        </tr>
                        </tbody>
                    </table>
                   
                    <div class="group-inner-btn">
                        <button id="btn-admission-add-to-cart" type="button" class="btn btn-primary btn-md ng-binding" ng-click="addToFunCart()" ng-bind="toFunCart">${staticText[lang]['addTicketToCartButtonLabel']!"Update FUN Cart"}</button>
                    </div>
                </div>
            </div>
			
        [#else]

            <div class="control-bar">
                [#if lang == "en"]
                    <form class="search-form ng-pristine ng-valid ng-scope">
                            <span>
                                <input id="search-text" type="text" class="search ng-pristine ng-untouched ng-valid" placeholder="${(productFilter?has_content)?string('','Search . . .')}" ng-model="searchText" ng-click="openKeyboard()" value="${productFilter}">
                                <button id="btn-search" type="button" class="btn btn-xxs btn-primary ng-binding" ng-bind="btn.search" ng-click="showResult(searchText)">${staticText[lang]['searchLabel']!"Search"}</button>
                            </span>
                            <span class="sort-option">
                                <span class="sort ng-binding" ng-bind="sort.start">Sort By</span>
                                <button id="btn-sort-by-name" type="button" class="btn-six ${ (pageSort == 'name')?string('btn-selected','')} ng-binding" ng-bind="sort.by1">Name</button>
                                <button id="btn-sort-by-price" type="button" class="btn-six ${ (pageSort == 'price')?string('btn-selected','')} ng-binding" ng-bind="sort.by2">Price</button>
                            </span>
                    </form>
                [/#if]

                <div class="filter-option">

                    [#if params[1]?has_content]
                        [#assign selectedSubCategory = params[1]]
                    [/#if]

                    [#assign counter = 0]

                    [#list subCategoryList as subCategory]

                        [#assign counter = counter + 1]

                        <button type="button" class="btn-six [#if selectedSubCategory?has_content] [#if selectedSubCategory == subCategory.getName()] [#assign subCategoryNode = subCategory] btn-selected [/#if] [#elseif counter == 1] [#assign subCategoryNode = subCategory] btn-selected [/#if]"
                                onclick="javascript:window.location.href='category~${categoryNodeName}~${subCategory.getName()}~'">
                        ${subCategory.getProperty("name").getString()}
                        </button>

                    [/#list]
                </div>
            </div>

            [#assign subCategoryProductList = kioskfn.getCMSProductBySubCategory(channel, subCategoryNode, false, pageSort, productFilter)]
            [#if subCategoryProductList?has_content]

                <div class="product-list">

                    [#if pageNumber?has_content]
                        [#if pageNumber > 1]
                            <button type="button" class="left-arrow" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${pageNumber-1}&&pageSort=${pageSort}&&productFilter=${productFilter}'"></button>
                        [/#if]
                    [/#if]

                    [#assign totalPages = (subCategoryProductList?size/8)?floor + 1]
					
                    [#if totalPages > 1]
                        [#if pageNumber?has_content]
                            [#if pageNumber != totalPages]
                                <button type="button" class="right-arrow" data-bind="click: kiosk.ProductNavigator.prevPage" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${pageNumber+1}&&pageSort=${pageSort}&&productFilter=${productFilter}'"></button>
                            [/#if]
                        [#else]
                           
                        [/#if]
                    [/#if]

                    [#assign totalProducts = 0]

                    <ul class="thumbnail-list">
                        [#list subCategoryProductList as subCategoryProduct]

                            [#assign productImages = starfn.getCMSProductImagesUUID(subCategoryProduct, lang)!]

                            [#assign totalProducts = totalProducts + 1]
                            
                            [#if (((totalProducts/8)?floor) - (totalProducts/8)) == 0]
                            	[#assign pageIndex = (totalProducts/8)]
                            [#else]
                            	[#assign pageIndex = ((totalProducts/8)?floor) + 1]
                            [/#if]

                            [#if pageIndex == pageNumber]
                            	[#assign priceIndicator = 0]
                            	[#assign priceSignLeft = "0"]
                            	[#if subCategoryProduct.hasProperty("priceIndicator")]
	                                [#assign priceIndicator =subCategoryProduct.getProperty("priceIndicator").getString()?number]
	                                [#if priceIndicator == 1]
	                                    [#assign priceSignLeft="170px"]
	                                [/#if]
	                                [#if priceIndicator == 2]
	                                    [#assign priceSignLeft="140px"]
	                                [/#if]
	                                [#if priceIndicator == 3]
	                                    [#assign priceSignLeft="110px"]
	                                [/#if]
								[/#if]
                                <li onclick="javascript:window.location.href='${sitePath}/category/product~${categoryNodeName}~${subCategoryNode.getName()}~${subCategoryProduct.getName()}~?pageNumber=${pageIndex}'">
                                    <div style="position: relative; left: 0; top: 0;">
                                        <img style="position: relative; top: 0; left: 0;" [#if productImages[0]?has_content] src="${damfn.getAssetLink(productImages[0])}" [#else] src="" [/#if] alt="" class="thumbnail-img"  id="rotator-${subCategoryProduct.getName()}" />
                						<div style="position: absolute; top: 100px; left: ${priceSignLeft};">
                						    [#if priceIndicator != 0]
                    						    [#list 1..priceIndicator as indicator]
                    						        <img  src="${themePath}/images/price-sign.png">
                    						
                    						    [/#list]
                						    [/#if]
                						</div>
            						</div>
                                    [#if productImages?size > 1]

                                        <script type="text/javascript">
                                            (function() {

                                                var rotator = $("#rotator-${subCategoryProduct.getName()}");
                                                var delayInSeconds = 5;
                                                var images = new Array(${productImages?size});

                                                [#assign counter = 0]
                                                [#list productImages as productImage]
                                                    images[${counter}] = "${damfn.getAssetLink(productImage)}";
                                                    [#assign counter = counter + 1]
                                                [/#list]

                                                var num = 1;
                                                var changeImage = function() {

                                                    rotator.fadeOut(1000, function() {

                                                        var len = images.length;
                                                        num++;

                                                        if (num == len) {
                                                            num = 0;
                                                        }

                                                        rotator.attr("src",images[num]);
                                                    }).fadeIn(1000);
                                                };

                                                setInterval(changeImage, delayInSeconds * 1000);
                                            })();
                                        </script>
                                    [/#if]

                                        <span class="caption">
                                            <span class="caption-title">${subCategoryProduct.getProperty("name").getString()}</span>
                                            <span class="caption-description">
                                                <div class="block-with-text">${subCategoryProduct.getProperty("shortDescription").getString()}</div>
                                            </span>
                                        </span>
                                </li>
                            [/#if]

                        [/#list]

                        <div class="clearfix"></div>
                    </ul>

                    <ul class="paging box-wrapper center-center">

                        [#list 1..totalPages as page]

                            <li>
                                <a href="${sitePath}/category~${categoryNodeName}~${subCategoryNode.getName()}~?pageNumber=${page}" [#if pageNumber?has_content] [#if page == pageNumber] class="selected" [/#if] [#else] class="selected" [/#if]></a>
                            </li>

                        [/#list]
                        <div class="clearfix"></div>
                    </ul>
                </div>
            [/#if]
        [/#if]
    </div>
</div>
[/#if]

[#include "/kiosk-mflg/templates/macros/common/keyboard.ftl"]
[#include "/kiosk-mflg/templates/macros/common/num-keypad.ftl"]

<script>
	
	    function commitToShoppingCart() {
	
			var qty = $('#qtyInput').val();
			if(qty == ''){
				 showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty2']}");
			}else{
					
				itemsInShoppingCart[0].qty = parseInt(qty);
				
		        $.ajax({
		            url : '/.store-kiosk/store/add-to-cart',
		            data : JSON.stringify({
		                sessionId : '',
		                items : itemsInShoppingCart
		            }),
		            headers : {
		                'Store-Api-Channel' : '${channel}'
		            },
		            type : 'POST',
		            cache : false,
		            dataType : 'json',
		            contentType : 'application/json',
		            success : function(result) {
		
		                if (result.success) {
		                    redirectToShoppingCartPage();
		                }
		                else {
		                    redirectToErrorPage();
		                }
		            },
		            error : function() {
		                redirectToErrorPage();
		            }
		        });
			}

	    }
    
	    function redirectToShoppingCartPage() {
	        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName?if_exists}~${subCaegoryNodeName?if_exists}~?pageNumber=${pageNumber}";
	    }
        function inputCharacter(character) {
	        var existingText = $("#search-text").val();
	        var updatedText = existingText.concat(character);
	        $("#search-text").val(updatedText);
	    }
	
	    function deletePreviousCharacter() {
	        var existingText = $("#search-text").val();
	        if(existingText.length > 0) {
	            var updatedText = existingText.slice(0, -1);
	            $("#search-text").val(updatedText);
	        }
	    }
	
	    function clearAllCharacters() {
	        $("#search-text").val('');
	    }
	    
	    function showKeyboard() {
	        $("#keyboardContainer").show();
	    }
	
	    function hideKeyboard() {
	        $("#keyboardContainer").hide();
	    }
	    function submitKeyboard() {
	       
	        window.location.href='category~${categoryNodeName?if_exists}~${subCaegoryNodeName?if_exists}~?pageSort=${pageSort}&&productFilter='+$("#search-text").val();
	    }
	    

	    
	    function deleteNumber(){
	    	var qty = $('#qtyInput');
	        var qtyInString  = qty.val().toString();

        	var textSize = qtyInString.length - 1;

	        if(textSize == 0) {
	            qty.val('');
	            $('#btn-minus-qty').addClass('btn-inactive');
	        }else{
	        	qtyInString = qtyInString.substring(0, textSize);
	        	qty.val(qtyInString);
	        }
	    }
	    
	    function selectNumber(number){
	    
	    	
	    	var qty = parseInt($('#qtyInput').val().toString() + number);
		    $('#qtyInput').val(qty);
	
	        if(qty == 0) {
	            $('#btn-minus-qty').removeClass('btn-active');
	            $('#btn-minus-qty').addClass('btn-inactive');
	        }
	    }
	    function hideKeyboard(){
	    	$('#keyboardContainer').hide();
	    }
	    
	    $(document).ready(function() {
			
			$('#btn-admission-add-to-cart').click(function(e){
				commitToShoppingCart();
			});
			
	        $('#btn-sort-by-price').click(function(e) {
	            window.location.href='category~${categoryNodeName?if_exists}~${subCaegoryNodeName?if_exists}~?pageSort=price&&productFilter='+$("#search-text").val();
	        });
	        
	        $('#btn-sort-by-name').click(function(e) {
	            window.location.href='category~${categoryNodeName?if_exists}~${subCaegoryNodeName?if_exists}~?pageSort=name&&productFilter='+$("#search-text").val();
	        });
	       	$('#btn-search').click(function(e) {
	            window.location.href='category~${categoryNodeName?if_exists}~${subCaegoryNodeName?if_exists}~?pageSort=${pageSort}&&productFilter='+$("#search-text").val();
	        });
	        
	        
	        $('#search-text').click(function(e) {
	            showKeyboard();
	        });
	        
	        $('#qtyInput').click(function(e){
		        var offset = $('#qtyInput').offset();
		
		        $("#number-keypad").css('top', offset.top + 53);
		        $("#number-keypad").css('left', offset.left - 290);
	        
	        	if($("#number-keypad").is(':visible')) {
		            $("#number-keypad").hide();
		        }
		        else {
		            $("#number-keypad").show();
		        }
	        });
	        
	        $('#btn-add-qty').click(function(e){
	        	var qtyInString= $('#qtyInput').val();
	        	if(qtyInString == ''){
	        		qtyInString = '0';
	        	}
	        	$('#qtyInput').val(parseInt(qtyInString) + 1);
        		$('#btn-minus-qty').removeClass('btn-inactive');
        		$('#btn-minus-qty').addClass('btn-active');
	        });

	        $('#btn-minus-qty').click(function(e){
		    	var qty = parseInt($('#qtyInput').val());
		    	if(qty > 0){
		    		qty = qty -1;
		    		$('#qtyInput').val(qty);
		    	}
		    	if(qty == 0 ){
		    		$('#qtyInput').val('')
		    		$('#btn-minus-qty').removeClass('btn-active');
		        	$('#btn-minus-qty').addClass('btn-inactive');
		    	}
	        });    
	    
	    })
</script>