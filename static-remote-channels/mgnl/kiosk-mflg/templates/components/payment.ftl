[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

   <script>
    var paymentData = {};
    var receipt = "XXXXXXXXXXX01";
    var urlAdapter;
	var signatureRequired = false;

    function exitPayment() {
        showExitConfirmationDialog();
    }

    function showExitConfirmationDialog() {
        $('#exitConfirmationDialog').show();
    }

    function hideExitConfirmationDialog() {
        $('#exitConfirmationDialog').hide();
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart";
    }

    function redirectToHomePage() {
        window.location.href = "${sitePath}";
    }

    function getTransactionInfo() {

    var customerDetail = function() {
                return {
                    email: 'abc@test123.com',
                    idType: '',
                    idNo: '1221322323',
                    name: 'Abc',
                    mobile: 89377834,
                    paymentType: 'master',
                    subscribed: true,
                    nationality: 'Singapore',
                    referSource: '',
                    dob: '20/10/1990'
                };
            };

        $.ajax({
            url: '/.store-kiosk/store/checkout',
            data: JSON.stringify(customerDetail()),
            headers: {
                'Store-Api-Channel': '${channel}'
            },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(result) {
                if (result.success) {
                    receipt = result.data.receiptNumber;
                } 
            },
            error: function(e) {
                alert(e);
            }
        });
    }

    function clearShoppingCart() {

        $.ajax({
            url: '/.store-kiosk/store/clear-cart',
            data: '',
            headers: {
                'Store-Api-Channel': '${channel}'
            },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(result) {

                if (result.success) {
                    redirectToHomePage();
                } else {
                    redirectToErrorPage();
                }
            },
            error: function() {
                redirectToErrorPage();
            }
        });
    }

    function makePayment(type) {

         var start_time = new Date().getTime();

        $('#paymentSelectionContent').hide();
        $('#paymentProgressContent').show();

        var dateValue = new Date();
        var timeStart = dateValue.getTime();

        var doublenumber = Number($("#totalPayAmount").text().replace(/[^0-9\.]+/g, ""));

        var data = {}
        data["receiptNumber"] = receipt;
        data["amount"] = doublenumber * 100;

        var url;

        switch (type) {
            case "nets-sale":
                url = urlAdapter + "nets-sale";
                data["accountType"] = "Default";
                break;
            case "cc-sale":
                url = urlAdapter + "cc-sale"
                break;
            case "nets-contactless-sale":
                url = urlAdapter + "nets-contactless-sale"
                break;
            case "cc-contactless-sale":
                url = urlAdapter + "cc-contactless-sale"
                break;
            default:

        }

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            dataType: 'json',
            success: function(result) {
                if(!result.success){
                    redirectToErrorPage();
                }
                
                if(result.data.result) { 
                	signatureRequired = result.data.signatureRequired;
                	var responseCode = result.data.details.responseCode;
                      var paymentDetails = function() {
                        return {
                                result: result.data.result,
                                receipt: receipt,
                                type: result.data.type,
                                terminalId: result.data.terminalId,
                                merchantId:  result.data.merchantId,
                                cardType: result.data.cardType,
                                cardTypeValue: result.data.cardTypeValue,
                                details : result.data.details,
                                transactionStart : start_time,
                                transactionEnd : new Date().getTime() - start_time,
                                callbacks : result.data.callbacks,
                                signatureRequired : result.data.signatureRequired
                             };
                        };
        
                        $.ajax({
                            url: '/.store-kiosk/store/add-payment',
                            headers : {
                				'Store-Api-Channel' : '${channel}'
            				},
                            type: 'POST',
                            contentType: "application/json",
                            data: JSON.stringify(paymentDetails()),
                            dataType: 'json',
                            success: function(results) {
                                if(result.data.result){
                                	 if('00' == responseCode){
                                        redirectToReceiptPage();
                                     }else{
                                     	$('#paymentSelectionContent').show();
                                        $('#paymentSelection').hide();
                                        showErrorScreen('Payment Failed. Please Try Again.');
                                        $('#paymentProgressContent').hide();
                                     }
                                }
                            },
                            error: function(e) {
                                redirectToOrderSummary();
                            }
                        });
                } else {
                    redirectToOrderSummary();
                }
                
            },
            error: function(e) {
                redirectToOrderSummary();
            }
        });
    }
    $(document).ready(function() {
        getTransactionInfo();
        connect();
    });

    function showErrorScreen(errorMessage) {
        $("#errorMessage").html(errorMessage);
        $('#errorMessageContent').show();
    }

    function connect() {
        var urlWebSocket;
        
       $.ajax({
            url: '/.store-kiosk/store/get-config',
            data: '',
            headers: {
                'Store-Api-Channel': 'kiosk-slm'
            },
            type: 'GET',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(result) {
                urlAdapter = result.data.kioskPaymentAdapter;
                urlWebSocket = result.data.kioskPaymentwebSocket;

                var ws = new WebSocket(urlWebSocket);
                ws.onopen = function() {};

                ws.onmessage = function(evt) {
                    var received_msg = evt.data;
                    $('#callback').text(received_msg);
                };
                ws.onclose = function() {
          
                };
                ws.onerror = function(evt) {
                   redirectToOrderSummary();
                };
                },
                error: function(e) {
                    redirectToOrderSummary();
                }
                });
     };

    function redirectToReceiptPage() {
        window.location.href = "${sitePath}/receipt?signatureRequired="+signatureRequired;
    }

    function redirectToOrderSummary() {
        window.location.href = "${sitePath}/ordersummary";
    }

    function showPaymentContent(){
        $('#paymentSelectionContent').show();
        $('#paymentSelection').show();
        $('#paymentProgressContent').hide();
    }

    </script>

<div id="paymentSelectionContent">
    <div id="errorMessageContent" class="main-wrapper" style="display: none;"> 
        <h1 class="secondary-heading sy-hg-lg"></h1>
            <ul class="box-wrapper center-center error-box">
                <li>
                    <img src="${themePath}/images/icon-fail.png" class="icon-fail"/>
                    <h2 id="errorMessage" class="error-heading"></h2>
                    <div>
                        <button type="button" class="btn btn-md btn-primary" onclick="showPaymentContent()">Try Again</button>
                    </div>
                </li>
            </ul>
    </div>
    <div id="paymentSelection" class="main-wrapper" >
        <h1 class="primary-heading py-hg-lg ng-binding" ng-bind-html="title.header1">Please select one of the payment modes</h1>
        <h3 class="third-heading ng-binding" ng-bind-html="title.header2">Total Amount Payable: <span id="totalPayAmount" class="payable-amount">S$50.00</span></h3>
        <ul class="box-wrapper center-center pt-mode-list">
            <li>
                <h4 ng-bind="title.creditCard" class="ng-binding">Credit Card</h4>
                <button type="button" class="btn-credit" onclick="makePayment('cc-sale')"></button>
            </li>
            <li>
                <h4 ng-bind="title.nets" class="ng-binding">NETS</h4>
                <button type="button" class="btn-nets" onclick="makePayment('nets-sale')"></button>
            </li>
            <div class="clearfix"></div>
        </ul>
        <ul class="box-wrapper center-center pt-mode-list">
            <li>
                <h4 ng-bind="title.contactlessCreditCard" class="ng-binding">Contactless Credit Card</h4>
                <button type="button" class="btn-contactless" onclick="makePayment('cc-contactless-sale')"></button>
            </li>
            <li>
                <h4 ng-bind="title.netsFlashPay" class="ng-binding">NETS FlashPay</h4>
                <button type="button" class="btn-flashpay" onclick="makePayment('nets-contactless-sale')"></button>
            </li>
            <div class="clearfix"></div>
        </ul>
    </div>
    <div class="btn-exit-container">
        <button type="button" class="btn btn-md btn-exit" onclick="exitPayment()">Exit Payment</button>
    </div>

    <div id="exitConfirmationDialog" class="dialog-container box-wrapper center-center" style="display: none;">
        <div class="dialog-ct" style="height:430px">
            <div class="btn-close">
                <button type="button" onclick="hideExitConfirmationDialog()"></button>
            </div>
            <h2 class="primary-heading sy-hg-md">No amount will be deducted.</h2>
            <h2 class="primary-heading sy-hg-md">Would you like to go back to FUN Cart or exit this transaction?</h2>
            <div class="group-inner-btn">
                <button type="button" class="btn btn-md btn-primary" onclick="redirectToShoppingCartPage()">Back to FUN Cart</button>
                <button type="button" class="btn btn-xs btn-primary" onclick="clearShoppingCart()">Exit and Clear My Transaction</button>
            </div>
        </div>
    </div>
</div>

<div id="paymentProgressContent" style="display: none;">
    <div class="main-wrapper">
        <div class="box-wrapper center-center box-full">
            <h1 id="callback" class="primary-heading py-hg-lg">${staticText[lang]['paymentProgressLabel']}</h1>
        </div>
  
    </div>
</div>