[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#if ctx.getParameter("receiptNumber")?has_content]
    [#assign receiptNumber = ctx.getParameter("receiptNumber")]
[#else]
    [#assign receiptNumber = '']
[/#if]

<script>

    var productsInShoppingCart = [];

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/get-cart-by-receipt-number',
            data : '${receiptNumber}',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var itemsInShoppingCart = [];
                    var totalPrice = "S$";

                    totalPrice = totalPrice.concat(result.data.totalAmount);

                    productsInShoppingCart = result.data.items;

                    resetShoppingCartQuantity();
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);

                    loadScroller();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {

            rowsHtml += '<tr>' +
                    '	<td colspan="6" class="cart-title">' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                    '			<tr>' +
                    '				<td>' +
                    '					<span class="product-title">' + product.cmsProductName + '</span>' +
                    '				</td>' +
                    '				<td colspan="4">&nbsp;</td>' +
                    '			</tr>' +
                    '		</table>' +
                    '	</td>' +
                    '</tr>' +
                    '<tr>' +
                    '	<td>' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">';

            rowsHtml += '<tr>' +
                    '     <td>' +
                    '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                    '           <tr class="cart-list">' +
                    '               <td width="46%" class="title">' + product.name +
                    '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]}: ' + product.displayDetails + '</p>' +
                    '                   <p class="tix-type" style="color: red">' + product.discountLabel + '</p>' +
                    '               </td>';

            rowsHtml += ' <td width="15%" class="center">S$' + product.unitPrice.toFixed(2) + '</td>' +
                    ' <td width="11%" class="center">' + product.qty + '</td>';

            if(product.discountTotal != undefined && product.discountTotal > 0) {
                rowsHtml += ' <td width="18%" class="center">' +
                        '	<span style="color: red; text-decoration: line-through;">S$' + (parseFloat(product.total) + parseFloat(product.discountTotal)).toFixed(2) + '</span><br />' +
                        ' 	S$' + parseFloat(product.subtotal).toFixed(2) +
                        ' </td>';
            }
            else {
                rowsHtml += ' <td width="18%" class="center">S$' + parseFloat(product.subtotal).toFixed(2) + '</td>';
            }

            rowsHtml += ' 		</tr>' +
                    '       </table>' +
                    '   </td>' +
                    '</tr>';
        });

        rowsHtml += '		</table>' +
                '	</td>' +
                '</tr>';

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity() {
        var totalQuantity = 0;

        $.each(productsInShoppingCart, function(index, product) {
            totalQuantity += product.qty;
        });

        $('#totalShoppingItemCount').text(totalQuantity);
    }

    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }

    function loadScroller() {
        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    function redirectToHomePage() {
        window.location.href='${sitePath}';
    }

    function printTickets() {

        $.ajax({
            url : '/.store-kiosk/kiosk-slm/printTickets',
            data : JSON.stringify('${receiptNumber}'),
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    showCompletedScreen();
                }
                else {
                    showErrorScreen(result.message);
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function showCompletedScreen() {
        $("#summaryContent").hide();
        $("#printSuccessfulContent").show();
    }

    function showErrorScreen(errorMessage) {
        $("#summaryContent").hide();
        $("#printErrorMessage").html(errorMessage);
        $("#printErrorContent").show();
    }

    $(document).ready(fetchShoppingCartItems());

</script>

<div class="main box-wrapper top-center">
    <div class="main-content" id="summaryContent">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['orderSummaryTitle']}</h1>
        <p class="sub-text ng-binding ng-scope">${staticText[lang]['recoveryFlowSubTitle']}</p>

        <div class="fun-cart-content">
            <div class="cart-box ticket-container summary-dialog" style="height: 680px;">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                    <tr>
                        <td width="37%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitlePrice']}</td>
                        <td width="10%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitleSubtotal']}</td>
                        <td width="5%" class="center">&nbsp;</td>
                    </tr>
                </table>
                <div class="tab-content-container fif-template">
                    <div id="scrollbar2" style="height:490px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="total-bar">
                        <div class="total">
                        ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                        </div>
                    </div>
                    <div class="cart-group-btn box-wrapper center-center">
                        <button type="button" class="btn btn-third btn-md" onclick="redirectToHomePage()">${staticText[lang]['cancelButtonLabel']}</button>
                        <button type="button" class="btn btn-primary btn-md" onclick="printTickets()">${staticText[lang]['printTicketButtonLabel']}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="printSuccessfulContent" style="display: none;">
    <div class="main-wrapper">
        <ul class="box-wrapper center-center printing-img complete-box">
            <li>
                <img src="${themePath}/images/icon-success.png" />
                <h2 class="success-heading">${staticText[lang]['printSuccessLabel1']}</h2>
                <p class="secondary-heading sy-hg-sm">
                ${staticText[lang]['printSuccessLabel3']}
                    <br />
                    <img src="${themePath}/images/logo-complete.png" />
                </p>
            </li>
        </ul>
    </div>
    <div class="group-btn">
        <button type="button" class="btn btn-md btn-primary" onclick="redirectToHomePage()">${staticText[lang]['backToHomeButtonLabel']}</button>
    </div>
</div>

<div id="printErrorContent" style="display: none;">
    <div class="main-wrapper">
        <h1 class="secondary-heading sy-hg-lg">${staticText[lang]['printErrorHeading']}</h1>
        <ul class="box-wrapper center-center error-box">
            <li>
                <img src="${themePath}/images/icon-fail.png" class="icon-fail"/>
                <h2 id="printErrorMessage" class="error-heading"></h2>
            </li>
        </ul>
    </div>
    <div class="group-btn">
        <button type="button" class="btn btn-md btn-primary" onclick="redirectToHomePage()">${staticText[lang]['okButtonLabel']}</button>
    </div>
</div>