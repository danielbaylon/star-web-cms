[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    function getShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPayAmount = "S$0.00";
                    var totalQuantity = result.data.totalQty;

                    if(totalQuantity != 0) {
                        totalPayAmount = result.data.totalText.replace(/\s/g, '');
                    }

                    refreshShoppingCart(totalQuantity);
                    refreshTotalPayAmount(totalPayAmount);
                }
                else {
                    s.errMsg(result.message);
                    s.isErr(true);
                    s.scrollToErr();
                }
            },
            error : function() {
                s.errMsg('Unable to process your request. Please try again in a few moments.');
                s.isErr(true);

                var topOfErr = $('#productDiv').find(
                        'div[data-errorsection]').offset().top;
                $('html, body').animate({
                    scrollTop : topOfErr - 85
                }, 500);
            }
        });
    }

    function refreshShoppingCart(totalQuantity) {

        $("#totalShoppingItemCount").text(totalQuantity);

        if(totalQuantity > 0) {
            $("#shoppingCartArea").show();
            $("#clearCartArea").show();
        }
        else {
            $("#shoppingCartArea").hide();
            $("#clearCartArea").hide();
        }
    }

    function refreshTotalPayAmount(totalAmount) {
        $("#totalPayAmount").text(totalAmount);
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}"
    }

    $(document).ready(function() {
        getShoppingCartItems();
    })
</script>

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<header>
    <button type="button" class="logo"></button>

    <nav>
        <ul>
            <li id="btnHome" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-home-grey">${staticText[lang]["homeLabel"]!"Home"}</button>
            </li>
        [#if lang == "en"]
            <li id="btnSearch" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-search-grey">${staticText[lang]["searchLabel"]!"Search"}</button>
            </li>
        [/#if]
            <li id="btnTranslate" style="float: left; display: block; margin-top: 0px;">

            [#if lang == "en"]
                [#if state.originalBrowserURL?contains("/en/")]
                    [#assign localeURL = state.originalBrowserURL?replace("/en/", "/zh_CN/")]
                [#else]
                    [#assign localeURL = "${ctx.contextPath}/zh_CN/kiosk"]
                [/#if]
            [#else]
                [#assign localeURL = state.originalBrowserURL?replace("/zh_CN/", "/en/")]
            [/#if]

                <button type="button" class="btn-lang-grey">[#if lang == "en"] 华 语 [#else] English [/#if]</button>
            </li>
        </ul>
    </nav>

    <div ng-include="templateTodayUrl" class="ng-scope">
        <div class="ng-scope">
            <div class="date-time">
                <span class="date ng-binding" id="todayDate"></span>&nbsp;<span class="time ng-binding" id="todayTime"></span>
            </div>
        </div>
    </div>

    <div id="shoppingCartArea" class="container center-center">
        <div id="funShoppingCart" class="fun-box center-center">
            <div class="items">
                <span id="totalShoppingItemCount" class="cart-grey">0</span>
            </div>
            <div class="funbox-logo ng-binding">
                <img src="${themePath}/images/${staticText[lang]['addToCartLogoName']}" />
            </div>
        </div>
    </div>

    <div id="clearCartArea" class="clearcart-container center-center">
        <button type="button" class="btn-clearcart btn-clearcart-grey">
        [#if lang == "en"]
        ${staticText[lang]["clearCartLabel"]!"Clear Cart"}
        [#else]
        ${staticText[lang]["clearCartLabel1"]!""}
            <br />
        ${staticText[lang]["clearCartLabel2"]!""}
        [/#if]
        </button>
    </div>

</header>