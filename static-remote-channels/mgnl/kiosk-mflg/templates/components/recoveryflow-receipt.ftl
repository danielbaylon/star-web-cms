[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

[#if ctx.getParameter("receiptNumber")?has_content]
    [#assign receiptNumber = ctx.getParameter("receiptNumber")]
[#else]
    [#assign receiptNumber = false]
[/#if]

<script>
    
    function reprintTicketStart() {

        $.ajax({
            url : '/.store-kiosk/store/recovery/reprint',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            data : '${receiptNumber}',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                if (result.success) {
                    printTicketsAndReceipt();
                }else {
                    showErrorScreen('We are sorry for any inconvenience caused.');
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }
        
    function printTicketsAndReceipt() {

        $.ajax({
            url : '/.store-kiosk/kiosk-mflg/printTicketsAndReceipt',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            data : '${receiptNumber}',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                if (result.success) {
                    showCompletedScreen() 
                }else {
                    showErrorScreen('We are sorry for any inconvenience caused.');
                }
                
                updateReprintSuccess();
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }
    
    function updateReprintSuccess() {

        $.ajax({
            url : '/.store-kiosk/store/recovery/reprint-success',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            data : '${receiptNumber}',
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function showCompletedScreen() {
        $("#paymentSuccessfulContent").hide();
        $("#printSuccessfulContent").show();
    }

    function showErrorScreen(errorMessage) {
        $("#printErrorMessage").html(errorMessage);
        $("#printErrorContent").show();
    }
     $(document).ready(function() {
        reprintTicketStart();

     });

    function redirectToHomePage() {
        window.location.href = "${sitePath}";
    }

</script>

    
    
<div id="paymentSelectionContent">
    <div id="paymentSuccessfulContent">
        <div class="main-wrapper">
            <h1 class="box-wrapper top-center">${staticText[lang]['transactionSuccessLabel1']}</h1>
            <h2 class="primary-heading py-hg-sm">${staticText[lang]['transactionSuccessLabel2']}</h2>
            <ul class="box-wrapper center-center printing-img">
                <li>
                    <img src="${themePath}/images/printing-at.gif" class="printing-at"/>
                    <img src="${themePath}/images/collection/take-receipt-ticket.jpg" />
                </li>
            </ul>
        </div>
    </div>

    <div id="printSuccessfulContent" style="display: none">
        <div class="main-wrapper">
            <ul class="box-wrapper center-center printing-img complete-box">
                <li>
                    <img src="${themePath}/images/icon-success.png" />
                    <h2 class="success-heading">${staticText[lang]['printSuccessLabel1']}</h2>
                    <p class="secondary-heading sy-hg-sm">
                    ${staticText[lang]['printSuccessLabel2']}
                        <br />
                        <img src="${themePath}/images/logo-complete.png" />
                    </p>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="redirectToHomePage()">${staticText[lang]['backToHomeButtonLabel']}</button>
        </div>
    </div>

    <div id="printErrorContent" style="display: none;">
        <div class="main-wrapper">
            <h1 class="secondary-heading sy-hg-lg"></h1>
            <ul class="box-wrapper center-center error-box">
                <li>
                    <img src="${themePath}/images/icon-fail.png" class="icon-fail"/>
                    <h2 id="printErrorMessage" class="error-heading"></h2>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="redirectToHomePage()">${staticText[lang]['okButtonLabel']}</button>
        </div>
        <div ng-include="'templates/pop-up-loading.html'" ng-show="showLoading==true"></div>
    </div>
</div>