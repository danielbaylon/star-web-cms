[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

<script>
    function redirectToScanEnterBarcodePage() {
        window.location.href = "${sitePath}/scan-enter-barcode";
    }
 $(document).ready(function() {
        startScanner();
    });

    function startScanner(){
        var scannerWebSocket;
        $.ajax({
            url: '/.store-kiosk/store/get-config',
            data: '',
            headers: {
                'Store-Api-Channel': 'kiosk-mflg'
            },
            type: 'GET',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(resultConfig) {
                var  scannerAdapter = resultConfig.data.kioskScannerAdapter;
                $.ajax({
                    url: scannerAdapter + "init",
                    data: '',
                    type: 'GET',
                    cache: false,
                    success: function(result) {
                        
                        scannerWebSocket = resultConfig.data.kioskScannerWebSocket;

                        var ws = new WebSocket(scannerWebSocket);
                        ws.onopen = function() {};
    
                        ws.onmessage = function(evt) {
                            var received_msg = evt.data;
                            retrieveData(received_msg);

                        };
                        ws.onclose = function() {
                  
                        };
                        ws.onerror = function(evt) {
   
                        };
                    }
                });
            },
            error: function(e) {}
        });
    }

    function retrieveData(data) {
        
        var value = JSON.parse(data);
       
       
        if(value.singleValue.length == 8) {
            getStartPINRedemption(value.singleValue);
        } else {
            showAlertDialog('ERROR', "Invalid Data For Processing.");
        }
    }


    function getStartPINRedemption(pinCode){
        openLoadingPage();
        $.ajax({
            url : '/.store-kiosk/store/redemption/start',
            data : JSON.stringify({
                pinCode : pinCode
            }),
            headers : {
                'Store-Api-Channel' : 'kiosk-mflg'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                closeLoadingPage();
                if (result.success) {
                    redirectToShoppingcartPage();
                }else{
                    showAlertDialog('ERROR', result.message);
                }       
            },
            error : function(result) {
                closeLoadingPage();
                redirectToErrorPage();
            }
        });
    }

    function redirectToShoppingcartPage() {
        window.location.href = "${sitePath}/redemption-shoppingcart?receiptNumber=";
    }

</script>

<div>
    <div></div>
    <div class="main-wrapper">
        <h1 class="primary-heading py-hg-lg">
            <span ng-bind-html="centerText.t1">${staticText[lang]['scanBarcodeContent1']}</span>
            <br />
            <span ng-bind-html="centerText.t2">${staticText[lang]['scanBarcodeContent2']}</span>
        </h1>
        <div class="box-wrapper center-center se-barcode">
            <img src="${themePath}/images/barcode/ticket-scan-big.jpg"/>
        </div>
    </div>
    <div class="group-btn">
        <button type="button" class="btn btn-md btn-third" onclick="redirectToScanEnterBarcodePage()">${staticText[lang]['cancelButtonLabel']}</button>
    </div>
    <div ng-include="'templates/pop-up-alert.html'" ng-show="showAlert==true"></div>
    <div ng-include="'templates/pop-up-loading.html'" ng-show="showLoading==true"></div>
</div>