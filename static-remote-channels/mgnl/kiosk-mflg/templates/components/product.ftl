[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#if ctx.getParameter("productFilter")?has_content]
    [#assign productFilter = ctx.getParameter("productFilter")]
[#else]
    [#assign productFilter = ""]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]
[#assign productNode = starfn.getCMSProduct(channel, productNodeName)]
[#assign announcementList = kioskfn.getAnnouncementsByProduct(channel,productNode)!]
[#assign productImages = starfn.getCMSProductImagesUUID(productNode, lang)!]
[#assign axProducts = starfn.getAXProductsByCMSProductNode(productNode)!]
[#assign closingTimeLabel = kioskfn.getClosingTime(productNode)!]
[#assign isProductClosed = kioskfn.isProductClosed(productNode)!]
[#assign otherInfo = kioskfn.getOtherInformationByProduct(productNode)!]
[#assign topUpInfoList = kioskfn.getTopUpInfo(productNode)!]





[#if productNodeName?has_content]

<script>

    $(document).ready(function() {
    	if($('#scrollbar3').length){
        	var myScroll3 = new IScroll('#scrollbar3', { scrollbars: 'custom', interactiveScrollbars: true });
        }
        if($('#scrollbar4').length){
        	var myScroll4 = new IScroll('#scrollbar4', { scrollbars: 'custom', interactiveScrollbars: true });
        }
        if($('#scrollbar8').length){
        	var myScroll8 = new IScroll('#scrollbar8', { scrollbars: 'custom', interactiveScrollbars: true });
        }
    });
</script>

<div class="main-wrapper">
    <div ng-include="templateDetailsUrl" class="ng-scope">
        <div ng-controller="ProductInformationCntl" class="ng-scope">

            <div class="tix-container tix-collapse">

                [#assign previewMode = ctx.getParameter("preview")!]
                [#assign isOnPreview = false]
                [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
                    [#assign isOnPreview = true]
                    <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
                [/#if]

                <h1 class="secondary-heading sy-hg-md left ng-binding" ng-show="haveTitle==true" ng-bind-html="product.Title">${productNode.getProperty("name").getString()}</h1>

                [#if productImages?has_content]
                    [#assign thumbnailAssetLink = damfn.getAssetLink(productImages[0])]
                    <div class="product-img">
                        <img src="${thumbnailAssetLink}" />
                    </div>
                [/#if]

                [#if announcementList?has_content]
                    <div class="alert-section-product-details alert-section">
                        <h3>Alert / Announcement</h3>
                        <div id="scrollbar3" style="position:relative;height:105px;width:100%;background-color:#C41C33">
                            <div class="scroller">
                                <ul>
                                    [#list announcementList as announcement]
                                        <li>
                                            <p style="height:100%;width:95%">
                                                 ${announcement.content}
                                            </p>
                                        </li>
                                    [/#list]
                                </ul>
                            </div>
                        </div>
                    </div>
                [/#if]

                [#if productNode?has_content]
                <div class="clearfix"></div>
                <h2 class="discription-title">Description</h2>
                <h3 class="short-description-title">${productNode.getProperty("shortDescription").getString()}</h3>
                <div id="scrollbar8" class="description" style="position:relative;height:150px;width:870px">
                    <div class="scroller">
                        <ul>
                            <li>
                                <div style="width:95%">
                                ${productNode.getProperty("description").getString()}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="main-info">
                    <div id="scrollbar4" style="height:265px;width:870px">
                        <div class="scroller">
                            <ul>
                                <li>
                                    [#if productNode.hasProperty("operationHours")]
                                        [#assign hours= productNode.getProperty("operationHours").getString()]
                                        [#if hours?has_content]
                                            <p>
                                                <img src="${themePath}/images/icon-time.png" />
                                                <span class="info-element">
                                                    <span class="label ng-binding">Opening Hours: </span>${hours}
                                                </span>
                                            </p>
                                        [/#if]
                                    [/#if]
                                    
                                    [#if productNode.hasProperty("closingTime")]
                                      
                                        [#if closingTimeLabel?has_content]
                                            <p>
                                                <img src="${themePath}/images/icon-time.png" />
                                                <span class="info-element">
                                                    <span class="label ng-binding">Closing Time: </span>${closingTimeLabel}
                                                </span>
                                            </p>
                                        [/#if]
                                    [/#if]
                                    [#if productNode.hasProperty("admissionInformation")]
                                        [#assign admissionInfo = productNode.getProperty("admissionInformation").getString()]
                                        [#if admissionInfo?has_content]
                                            <p>
                                                <img src="${themePath}/images/icon-admission.png" />
                                                <span class="info-element">
                                                    <span class="label ng-binding">Admission Info: </span>${admissionInfo}
                                                </span>
                                            </p>
                                        [/#if]
                                    [/#if]
                                    
                                    [#if productNode.hasProperty("eventStartDate") && productNode.hasProperty("eventEndDate")]
                                        [#assign eventStartDate = productNode.getProperty("eventStartDate").getDate()]
                                         [#assign eventEndDate = productNode.getProperty("eventEndDate").getDate()]
                                        [#if eventStartDate?has_content && eventEndDate?has_content]
                                            <p>
                                                <img src="${themePath}/images/icon-date.png" />
                                                <span class="info-element">
                                                    <span class="label ng-binding">Date: </span>${eventStartDate?string["dd MMM yyyy"]} - ${eventEndDate?string["dd MMM yyyy"]}
                                                </span>
                                            </p>
                                        [/#if]
                                    [/#if]

                                    [#if productNode.hasProperty("recommendedDuration")]
                                        [#assign recommendedDuration= productNode.getProperty("recommendedDuration").getString()]
                                        [#if recommendedDuration?has_content]
                                            <p>
                                                <img src="${themePath}/images/icon-duration.png" />
			                                    	<span class="info-element">
			                                    		<span class="label ng-binding">Recommended Duration: </span> ${recommendedDuration}
			                                    	</span>
                                            </p>
                                        [/#if]
                                    [/#if]
                                    [#if otherInfo?has_content]
                                    [#if otherInfo.hasProperty("zone")]
                                        [#assign zone= otherInfo.getProperty("zone").getString()]
                                        [#if zone?has_content]
                                            <p ng-show="haveZone==true">
                                                <img src="${themePath}/images/icon-station.png" />
                                                <span class="info-element">
                                                    <span class="label ng-binding">Zone / Nearest Station: </span>${zone}
                                                </span>
                                            </p>
                                        [/#if]
                                    [/#if]
                                    [/#if]
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                [/#if]


                [#if productNode?has_content]
                [#if productNode.hasProperty("notes")]
                    [#assign notes= productNode.getProperty("notes").getString()]
                    [#if notes?has_content]
                    <div class="more-info-container-product-details">
                        <div class="more-info">
                            <button class="btn-fifth" onclick="popupMoreInfo()">${staticText[lang]['moreInformation']!"More Information"}</button>
                        </div>
                    </div>


                    <div id="moreInfoDialog" class="dialog-container-product-details box-wrapper center-center" style="display: none">
                        <div style="z-index:-1" class="dialog-container"></div>
                        <div style="z-index:1" class="dialog-ct popup-tc">
                            <div class="btn-close" style="display:none">
                                <button type="button" onclick="closeOnTNC()"></button>
                            </div>
                            <h2 class="primary-heading py-hg-sm">${staticText[lang]['moreInformation']!"More Information"}</h2>
                            <section>
                                <div id="scrollbar6" style="position:relative;height:100%;width:100%">
                                    <div class="scroller" style="width: 95%;">
                                        <ul>
                                            <li>
                                                <div>
                                                    <p id="tncContent">
                                                    ${notes}
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <div class="group-inner-btn">
                                <button type="button" class="btn btn-md btn-primary" onclick="closeMoreInfo()">${staticText[lang]['okButtonLabel']}</button>
                            </div>
                        </div>
                    </div>
                    [/#if]
                [/#if]
                [/#if]


                <div class="group-btn box-wrapper center-center">
                	[#if categoryNodeName?has_content]
                    	<button type="button" class="btn btn-md btn-third ng-binding" onclick="javascript:window.location.href='${sitePath}/category~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}'">${staticText[lang]['backButtonLabel']!"Back"}</button>
                    [#else]
                    	<button type="button" class="btn btn-md btn-third ng-binding" onclick="javascript:window.location.href='${sitePath}/search?pageNumber=${pageNumber}&&productFilter=${productFilter}'">${staticText[lang]['backButtonLabel']!"Back"}</button>
                    [/#if]
                    
                    [#if axProducts?size != 0 && productNode.hasProperty("layoutMode") && productNode.getProperty("layoutMode").getString() == "purchase" && isProductClosed != "true" ]
                        <button type="button" class="btn btn-md btn-primary ng-binding" onclick="javascript:window.location.href='${sitePath}/category/product/order~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}'">${staticText[lang]['buyTicketButtonLabel']}</button>
                    [/#if]
                </div>
            </div>
        </div>
        [#assign recommendProductList = kioskfn.getRecommendProducts(channel,productNode.getName())!]
        [#if recommendProductList?has_content ]
            <div ng-controller="ProductSideBarCntl" class="ng-scope">

                <div id="div-menu-board-close" class="tix-sidebar sidebar-collapse ng-scope" style="display:none">
                    <div class="box-wrapper center-right" style="height:100%;">
                        <img src="${themePath}/images/arrow-animate.png" class="arrow-move">
                        <button id="btn-expand-menu-board" type="button" class="btn-expand"></button>
                    </div>
                </div>

                <div id="div-menu-board-slider" class="tix-sidebar sidebar-expand ng-scope">
                    <div class="sidebar-panel">
                        <button id="btn-close-menu-board" type="button" class="close-panel"></button>
                        <div class="heading-bar"></div>
                        <ul class="all-list">
                            [#list recommendProductList as recProd]
                                <li>
                                    <div class="item-list box-wrapper center-center">

                                        <div class="item-name">${recProd.name}</div>
                                        <div class="item-price"><span class="from">From</span><br>S$${recProd.productPrice}</div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="list-divider"></div>
                                </li>
                            [/#list]
                        </ul>
                        <div class="box-wrapper center-center">
                            <button id="btn-view-more" type="button" class="btn-secondary-1">View More</button>
                        </div>
                    </div>
                </div>
            </div>
        [/#if]
    </div>
</div>
[/#if]

<script>
    $(document).ready(function() {


        $('#btn-close-menu-board').click(function(e) {
            $('#div-menu-board-slider').hide();
            $('#div-menu-board-close').show();
        });

        $('#btn-expand-menu-board').click(function(e) {
            $('#div-menu-board-close').hide();
            $('#div-menu-board-slider').show();
        });

        $('#btn-view-more').click(function(e) {
            window.location.href='${sitePath}/category~${categoryNodeName}~';
        });

    })

    function popupMoreInfo() {

        $("#moreInfoDialog").show();
        $('#scrollbar6-scrollbar').remove();
        var myScroll6;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
        	if($('#scrollbar6').length){
            	myScroll6 = new IScroll('#scrollbar6', { scrollbars: 'custom', interactiveScrollbars: true });
            }
        }, 1);
    }

    function closeMoreInfo() {
        $("#moreInfoDialog").hide();
    }
</script>