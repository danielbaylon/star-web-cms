[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]
<script>

    $(document).ready(function() {
        var urlParams = new URLSearchParams(window.location.search);
        time =urlParams.get('time');

        if(time != null && time != "null"){
            setTimeout(redirectToHomePage, parseInt(time));
        }

    });

    function moveToOpsAdmin() {
        var throttle = false;
        var classname = document.getElementsByClassName("btn-goto-ops-from-maintenance");

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', function (evt) {
                var o = this,
                ot = this.textContent;

                if (!throttle && evt.detail === 3) {
                    redirectToOpsAdminPage();
                    throttle = true;
                    setTimeout(function () {
                        o.textContent = ot;
                        throttle = false;
                    }, 1000);
                }
            });
        }
    }

    function redirectToOpsAdminPage() {
        var channel = "${channel}";
        window.location.href = "${opsPath}?channel="+channel;
    }
</script>

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<div>
    <div class="bubble-animated">
        <div class="lt-pink-be"></div>
        <div class="lt-blue-be"></div>
        <div class="lt-purple-be"></div>
        <div class="rt-blue-be"></div>
        <div class="rt-yellow-be"></div>
        <div class="rt-red-be"></div>
        <div class="rt-pink-be"></div>
        <div class="rt-purple-be"></div>
    </div>
    <div class="error-container">
        <div class="error-content">
            <h1 ng-bind-html="title.message">System Maintenance</h1>
            <h3 ng-bind-html="preTrxChkMsg">We are busy updating the store for you and we will be back shortly</h3>
        </div>
    </div>
    <div class="btn-goto-ops-from-maintenance"  onclick="moveToOpsAdmin()">
    </div>

</div>