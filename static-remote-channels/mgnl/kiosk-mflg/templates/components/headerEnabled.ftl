[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    function getShoppingCartItems() {

        $.ajax({
            url : '${ctx.contextPath}/.store-kiosk/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalQuantity = result.data.totalQty;
                    refreshShoppingCart(totalQuantity);
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCart(totalQuantity) {

        $("#totalShoppingItemCount").text(totalQuantity);

        if(totalQuantity > 0) {
            $("#shoppingCartArea").show();
            $("#clearCartArea").show();
        }
        else {
            $("#shoppingCartArea").hide();
            $("#clearCartArea").hide();
        }
    }

    function clearShoppingCart() {
		openLoadingPage();
        $.ajax({
            url : '${ctx.contextPath}/.store-kiosk/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
				closeLoadingPage();
                if (result.success) {

                    refreshShoppingCart(0);
                    window.location.href = "${sitePath}";
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}"
    }

    $(document).ready(function() {
        $('#funShoppingCart').click(function(e) {
            redirectToShoppingCartPage();
        });
    })
</script>

<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<header>
    <button type="button" class="logo"></button>

    <nav>
        <ul>
            <li id="btnHome" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-home ng-binding" onclick="javascript:window.location.href='${sitePath}'">
                ${staticText[lang]["homeLabel"]!"Home"}
                </button>
            </li>
        [#if lang == "en"]
            <li id="btnSearch" style="float: left; display: block; margin-top: 0px;">
                <button type="button" class="btn-search ng-binding ng-scope" onclick="javascript:window.location.href='${sitePath}/search'">
                ${staticText[lang]["searchLabel"]!"Search"}
                </button>
            </li>
        [/#if]
            <li id="btnTranslate" style="float: left; display: block; margin-top: 0px;">

            [#if lang == "en"]
                [#if state.originalBrowserURL?contains("/en/")]
                    [#assign localeURL = state.originalBrowserURL?replace("/en/", "/zh_CN/")]
                [#else]
                    [#assign localeURL = "${ctx.contextPath}/zh_CN/kiosk"]
                [/#if]
            [#else]
                [#assign localeURL = state.originalBrowserURL?replace("/zh_CN/", "/en/")]
            [/#if]

                <button type="button" class="btn-lang ng-binding" onclick="javascript:window.location.href='${localeURL}'">
                [#if lang == "en"] 华 语 [#else] English [/#if]
                </button>
            </li>
        </ul>
    </nav>

    <div ng-include="templateTodayUrl" class="ng-scope">
        <div class="ng-scope">
            <div class="date-time">
                <span class="date ng-binding" id="todayDate"></span>&nbsp;<span class="time ng-binding" id="todayTime"></span>
            </div>
        </div>
    </div>

    <div id="shoppingCartArea" class="container center-center" style="display: none;">
        <div id="funShoppingCart" class="fun-box center-center">
            <div class="items">
                <span id="totalShoppingItemCount">0</span>
            </div>
            <div class="funbox-logo ng-binding">
                <img src="${themePath}/images/${staticText[lang]['addToCartLogoName']}" />
            </div>
        </div>
    </div>

    <div id="clearCartArea" class="clearcart-container center-center" style="display: none;">
        <button type="button" class="btn-clearcart ng-binding" onclick="clearShoppingCart()">
        [#if lang == "en"]
        ${staticText[lang]["clearCartLabel"]!"Clear Cart"}
        [#else]
        ${staticText[lang]["clearCartLabel1"]!""}
            <br />
        ${staticText[lang]["clearCartLabel2"]!""}
        [/#if]
        </button>
    </div>

    <script>
        getShoppingCartItems();
    </script>

</header>