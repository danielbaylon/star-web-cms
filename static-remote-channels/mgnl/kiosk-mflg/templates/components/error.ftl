[#include "/kiosk-mflg/templates/macros/pageInit.ftl"]
[#include "/kiosk-mflg/templates/macros/staticText/header.ftl"]
<script>

    function moveToOpsAdmin() {
        var throttle = false;
        var classname = document.getElementsByClassName("btn-goto-ops-from-maintenance");

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', function (evt) {
                var o = this,
                        ot = this.textContent;

                if (!throttle && evt.detail === 3) {
                    redirectToOpsAdminPage();
                    throttle = true;
                    setTimeout(function () {
                        o.textContent = ot;
                        throttle = false;
                    }, 1000);
                }
            });
        }
    }

    function redirectToOpsAdminPage() {
        var channel = "${channel}";
        window.location.href = "${opsPath}?channel="+channel;
    }
</script>
<div id="container">
    <div id="text-container">
        <canvas id="text"></canvas>
    </div>
    <div id="stage-container">
        <canvas id="stage"></canvas>
    </div>
    <form id="form"></form>
</div>

<div>
    <div class="bubble-animated">
        <div class="lt-pink-be"></div>
        <div class="lt-blue-be"></div>
        <div class="lt-purple-be"></div>
        <div class="rt-blue-be"></div>
        <div class="rt-yellow-be"></div>
        <div class="rt-red-be"></div>
        <div class="rt-pink-be"></div>
        <div class="rt-purple-be"></div>
    </div>
    <div class="error-container">
        <div class="error-content">
            <h1 ng-bind-html="title.message">We are currently unable to accept any transactions. Please kindly proceed to other kiosks or to ticketing counters to make your purchase.</h1>
            <h3 ng-bind-html="preTrxChkMsg"></h3>
        </div>
    </div>
    <div class="btn-goto-ops-from-maintenance"  onclick="moveToOpsAdmin()">
</div>
