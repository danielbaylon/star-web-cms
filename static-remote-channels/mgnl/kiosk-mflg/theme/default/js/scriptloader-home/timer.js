var today = {};
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function setDate(language) {
    var date = new Date();
    //var language = window.navigator.userLanguage || window.navigator.language;

    if (language == 'en') {
        window.today['date'] = ('0' + (date.getDate())).slice(-2) + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
    } else if (language == 'zh_CN') {
        window.today['date'] = date.getFullYear() + ' 年 ' + ('0' + (date.getMonth() + 1)).slice(-2) + ' 月 ' + ('0' + (date.getDate())).slice(-2) + ' 日';
    } else {
        return;
    }


}

function setTime(language) {
    var date = new Date();
    //var language = window.navigator.userLanguage || window.navigator.language;
    var hh = date.getHours();
    var ampm = hh >= 12 ? 'PM' : 'AM';
    if (language == 'zh_CN') {
        ampm = hh >= 12 ? '下午' : '上午';
    }
    if (hh > 12) {
        hh = hh - 12;
    }
    window.today['time'] = ('0' + hh).slice(-2) + ':' + ('0' + (date.getMinutes())).slice(-2) + ' ' + ampm;

}