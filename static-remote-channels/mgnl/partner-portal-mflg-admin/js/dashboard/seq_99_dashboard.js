var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

$(document).ready(function() {
    var $page = {};
    try{
        if(applogVmsJson != undefined && applogVmsJson != null && applogVmsJson != ''){ $page.applogVmsJson = JSON.parse(applogVmsJson);}
    }catch(e){};
    try{
        if(isApprover != undefined && isApprover != null && isApprover != ''){ $page.isApprover = isApprover;};
    }catch(e){};
    try{
        if(reasonVmsJson != undefined && reasonVmsJson != null && reasonVmsJson != ''){ $page.reasonVmsJson = JSON.parse(reasonVmsJson);};
    }catch(e){};
    $page.loadLabel = "Loading..";
    $page.accConLabel = "Account Controls";
    $page.requestLabel = "Request";
    $page.offlinePaymentLabel = "Offline Payment";
    $page.offlineReviewPopTemp = kendo.template($("#offlineReviewPopTemp").html());
    $page.offlinePaymentApprovalModel = new nvx.OfflinePaymentApprovalModel($page);
    $page.appLogsTemp = kendo.template($("#appLogsTemp").html());
    $page.reasonPopTemp  = kendo.template($("#reasonPopTemp").html());
    $page.offlinePaymentTemp = kendo.template($("#offlinePaymentTemp").html());
    $page.transPopTemp = kendo.template($("#transPopTemp").html());
    $page.tabData = [];
    $page.tabDataStatus = {};
    $page.RequestsModel = function(){
        var s = this;
        s.applogs = ko.observableArray([]);
    };
    $page.ApplogModel = function(app){
        var s = this;
        s.Data = app;
        var s = this;
        s.id = ko.observable();
        s.createdDateStr = ko.observable();
        s.createdBy = ko.observable();
        s.description = ko.observable();
        s.update = function(app){
            for(var k in app) s.Data[k]=app[k];
            s.id(app.id);
            s.createdDateStr(app.createdDateStr);
            s.createdBy(app.createdBy);
            s.description(app.description);
        };
        s.update(app);
        s.viewDetail = function(){
            $page.viewDetail(s.id());
        };
        s.reject = function(){
            $page.openRejectPop(s.id());
        };
        s.approve = function(){
            $page.approveRequest(s.id());
        };
    };
    $page.viewDetail = function(appId){
        $pop.initPopPage(appId);
    };
    function viewPackage(pkgId,pgkName){
        var htmlcontent;
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/pkg/viewSinglePkg?pkgId='+pkgId,
            success:  function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    htmlcontent = data;
                    var pkgPopup = $("#vPkgWindow").kendoWindow({
                        width: '800px',
                        modal: true,
                        resizable: false,
                        // title: "View Package",
                        actions: ["Close"],
                        viewable : false,
                        deactivate: function() {
                            this.destroy();
                            if(!$("#revalWindow").length){
                                var $div = $('<div />').appendTo('body');
                                $div.attr('id', 'vPkgWindow');
                            }
                        }
                    }).data('kendoWindow');
                    pkgPopup.title(pgkName);
                    pkgPopup.content(htmlcontent);
                    pkgPopup.center().open();
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.syncContent = function(index){
        var tab = $page.tabData[index];
        tab.Content = tab.Template(tab.Data);
        $($page.tabstrip.contentElement(index)).html(tab.Content);
    };

    $page.initOfflineList = function(){
        $page.paymentUrl = nvx.API_PREFIX + '/offlinePay/list';
        $page.paymentDs = new kendo.data.DataSource({
            transport: {read: {url: $page.paymentUrl, cache: false}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        name: {type: 'name'},
                        pinCode: {type: 'string'},
                        pinCodeQty: {type: 'number'},
                        pinGeneratedDateStr: {type: 'string'},
                        expiryDateStr: {type: "string"},
                        status: {type: "string"},
                        qtyRedeemedStr: {type: "string"},
                        lastRedemptionDateStr: {type: 'string'},
                        description: {type: 'string'},
                        referenceNum: {type: 'string'},
                        username: {type: 'string'},
                        canRedeem:{type:"boolean"},
                        canRedeemDtStr:{type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });

        $page.kGrid = $("#resultMarker").kendoGrid({
            dataSource:$page.paymentDs,
            columns: [{
                field: "referenceNum",
                title: "Payment Reference No.",
                template: "#if(referenceNum != null && referenceNum != '' && referenceNum != undefined){# <span>#=referenceNum#</span> #}#",
                width: 150
            },{
                field: "id",
                title: 'Order Reference No.',
                template: '<a href="javascript:void(0)" onclick="nvxpage.loadTrans(#=id#);">#=receiptNum#</a>',
                width: 150
            },{
                field: "tran.totalAmount",
                title: "Amount",
                template: '#=totalAmountStr#',
                width: 120,
                sortable: false
            },{
                field: "status",
                title: "Status"
            },{
                field: "createdDate",
                title: "CreateDate",
                template: '#=createDtStr#'
            },{
                field: "accountCode",
                title: "Partner Code",
                template: '#=accountCode#',
                sortable: false
            },{
                field: "orgName",
                title: "Org Name",
                template: '#=orgName#',
                sortable: false
            },{
                field: "username",
                title: "Done By"
            }],
            sortable: true, pageable: true
        });

        $page.OfflinePaymentModel = function(){
            var s = this;
            s.startDtStr = ko.observable();
            s.endDtStr = ko.observable();
            s.status = ko.observable();
            s.username = ko.observable();
            s.orgName = ko.observable();
            s.accountCode = ko.observable();
            s.referenceNum = ko.observable();

            s.resetFilters = function(){
                s.startDtStr('');
                s.endDtStr('');
                s.status('');
                s.username('');
                s.orgName('');
                s.accountCode('');
                s.referenceNum('');
            }

            s.filter = function(){
                var url = nvx.API_PREFIX + '/offlinePay/list';
                var startDtStr = (s.startDtStr() != undefined && s.startDtStr() != null && s.startDtStr() != '') ? s.startDtStr() : '';
                var endDtStr = (s.endDtStr() != undefined && s.endDtStr() != null && s.endDtStr() != '') ? s.endDtStr() : '';
                var status = (s.status() != undefined && s.status() != null && s.status() != '') ? s.status() : '';
                var username = (s.username() != undefined && s.username() != null && s.username() != '') ? s.username() : '';
                var orgName = (s.orgName() != undefined && s.orgName() != null && s.orgName() != '') ? s.orgName() : '';
                var referenceNum = (s.referenceNum() != undefined && s.referenceNum() != null && s.referenceNum() != '') ? s.referenceNum() : '';
                var accountCode = (s.accountCode() != undefined && s.accountCode() != null && s.accountCode() != '') ? s.accountCode() : '';
                var url = url + '?startDtStr='+startDtStr+'&endDtStr='+endDtStr+'&status='+status+'&username='+username+'&orgName='+orgName+'&referenceNum='+referenceNum+'&accountCode='+accountCode;
                $page.paymentDs.options.transport.read.url = url;
                $page.paymentDs.page(1);
            }
        };
        $page.startDate = $('#startDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');
        $page.endDate = $('#endDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');

        $page.opModel = new $page.OfflinePaymentModel();
        ko.applyBindings($page.opModel ,$('#filterDv')[0]);
    };
    $page.tabSelect = function(e){
        var selectedIndex = $(e.item).index();
        var tab = $page.tabData[selectedIndex];
        $page.currentTab = tab;
        console.log("Content: "+tab.Content);
        if(tab.Content == $page.loadLabel){
            tab.Reload();
        };
    };
    $page.closePopById = function(id){
        var dialog = $("#"+id+"").data("kendoWindow");
        dialog.destroy();
    }
    $page.ReasonModel = function(data){
        var self = this;
        self.id = data.id;
        self.title = data.title;
    };
    $page.RejectModel = function(appId){
        var s = this;
        s.remarks = ko.observable();
        s.save = function(){
            $page.rejectLog(appId);
        };
    };

    $page.approvePa = function(appId){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url:  nvx.API_PREFIX + '/admin-func/verify-request/approve',
            data: { id : appId},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Approved Successfully.");
                    nvxpage.backToPreviousPage();
                } else {
                    alert(data.message);
                    nvx.spinner.stop();
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
                nvx.spinner.stop();
            }
        });
    };

    $page.rejectLog = function(appId){
        var reason = {};
        var selReasons = $("#selReasons").data("kendoMultiSelect");
        reason.reasonIds = selReasons.value();
        reason.remarks = $page.rejModel.remarks();
        if(reason.reasonIds.length < 1){
            alert("Please select a reason.");
            return;
        }
        if(!$page.rejModel.remarks()){
            alert("Please key in remarks.");
            return;
        }
        var reasonVmJson = JSON.stringify(reason);
        console.log("reason.remarks:"+reasonVmJson);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url:  nvx.API_PREFIX + '/admin-func/verify-request/reject',
            data: { id : appId, 'reasonVmJson':reasonVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    nvxpage.backToPreviousPage();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    $page.approveRequest = function(appId){
        var result = confirm("Are you sure you want to approve the request?");
        if (result == false) {
            return;
        }
        $page.approvePa(appId);
    };

    $page.openRejectPop = function(appId){
        var htmlcontent =$page.reasonPopTemp({});
        if(!$("#tempWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'tempWindow');
        }
        var tempWindow = $("#tempWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Reject',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        var selReasons = $("#selReasons").kendoMultiSelect({
            dataSource: $page.reasonVmsJson,
            dataTextField: "title",
            dataValueField: "id"
        }).data("kendoMultiSelect");
        $page.rejModel = new $page.RejectModel(appId);
        ko.applyBindings($page.rejModel,$('#reasonForm')[0]);
        tempWindow.center().open();
    };

    $page.loadTrans= function(id){
        nvx.spinner.start();
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/offlinePay/detail?id='+id,
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    var tdVm =  data['data'];
                    $page.openTransPop(tdVm);
                } else {
                    alert(data['message']);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.openTransPop = function(data){
        $page.offlinePaymentApprovalModel.reset();
        $page.offlinePaymentApprovalModel.initOfflinePaymentApprovalModel(data);
        var htmlcontent =$page.transPopTemp($page.offlinePaymentApprovalModel);
        if(!$("#transPopWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'transPopWindow');
        }
        $page.transPopWindow = $("#transPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Detail',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        $page.transPopWindow.content(htmlcontent);
        ko.applyBindings($page.offlinePaymentApprovalModel,$('#transModal')[0]);
        $page.transPopWindow.center().open();
    };

    $page.loadingPendingApprovalRequestList = function(callback){
        try{
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/dashboard/get-pending-approval-requests',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success : function(response, textStatus, jqXHR){
                    if(response.success){
                        var data = response['data'];
                        if(data != undefined && data != null){
                            $page.reasonVmsJson = [];
                            if(data['reasons'] != null && data['reasons'].length > 0){
                                $page.reasonVmsJson = data['reasons'];
                            }
                            var requests = data['requests'];
                            if(requests != null && requests.length > 0){
                                var len = requests.length;
                                var applogs = $page.reqModel.applogs;
                                for(var i = 0 ; i < len ; i++){
                                    var item = requests[i];
                                    applogs.push(new $page.ApplogModel(item));
                                }
                            }
                        }
                    }else{
                        if(data.message != null && data.message != ''){
                            var errorMsg = nvx.getDefinedMsg(data.message);
                            if(errorMsg != undefined && errorMsg != ''){
                                alert(errorMsg);
                            }else{
                                alert(data.message);
                            }
                        }
                    }
                    try{
                        callback();
                    }catch(e){console.log('error : '+e);};
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }catch(e){console.log('error : '+e);};
    };

    $page.initTabDataWithRoles = function(role){
        if(role != undefined && role != null && role != ''){
            if(role == 'dynamic-pp-mflg-general-approver' || role == 'dynamic-pp-mflg-general-backup-approver'){
                if(!($page.tabDataStatus['GeneralApprovalTab'] != undefined && $page.tabDataStatus['GeneralApprovalTab'] != null)){
                    $page.tabDataStatus['GeneralApprovalTab'] = true;
                    var tabIndex = $page.tabData.length ;
                    $page.tabData.push({
                        Name : $page.requestLabel,
                        Content : $page.loadLabel,
                        Data : {},
                        Template:$page.appLogsTemp,
                        Reload: function(){
                            $page.syncContent(tabIndex);
                            $page.reqModel = new $page.RequestsModel();
                            $page.loadingPendingApprovalRequestList(function () {
                                ko.applyBindings($page.reqModel,$('#appLogsDv')[0]);
                            });
                        }
                    });
                }
            }
            if(role == 'pp-mflg-approve-offline-payment' || role == 'pp-mflg-view-offline-payment'){
                if(!($page.tabDataStatus['OfflinePaymentTab'] != undefined && $page.tabDataStatus['OfflinePaymentTab'] != null)){
                    $page.tabDataStatus['OfflinePaymentTab'] = true;
                    var tabIndex = $page.tabData.length ;
                    $page.tabData.push({
                        Name :$page.offlinePaymentLabel ,
                        Content : $page.loadLabel,
                        Data : {},
                        Template:$page.offlinePaymentTemp,
                        Reload: function(){
                            $page.syncContent(tabIndex);
                            $page.initOfflineList();
                        }
                    });
                }
            }
        }
    };

    $page.popuplatePageWithRoles = function(roles){
        var hasRights = false;
        if(roles != null && roles.length > 0){
            $.each(roles, function(idx, role){
                if(role == 'dynamic-pp-mflg-general-approver' || role == 'dynamic-pp-mflg-general-backup-approver'){
                    $page.initTabDataWithRoles(role);
                }
            });
            $.each(roles, function(idx, role){
                if(role == 'pp-mflg-approve-offline-payment' || role == 'pp-mflg-view-offline-payment'){
                    $page.initTabDataWithRoles(role);
                }
            });
        }
        if( $page.tabData != undefined && $page.tabData != null && $page.tabData.length > 0){
            $page.tabstrip = $("#tabstrip").kendoTabStrip({
                dataTextField : "Name",
                dataContentField : "Content",
                dataSource : $page.tabData,
                select : $page.tabSelect
            }).data("kendoTabStrip");
            var eles = $('div.k-tabstrip-wrapper div.k-tabstrip[role="tablist"] li.k-state-default[aria-controls="tabstrip-1"] span.k-link');
            if(eles != null && eles.length > 0){
                $.each(eles, function(idx, item){
                    item.click();
                });
            }
        }else{
            $("#tabstrip").html('<div>Access Denied.</div>');
        }
    };

    $page.backToPreviousPage = function(){
        if($('#backToPreviousPage').size() > 0){
            $('#backToPreviousPage')[0].click();
        }
    };

    $page.loadingPage = function(){
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/admin-func/get-admin-access-and-approval-rights',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success : function(data, textStatus, jqXHR){
                var permissionList = null;
                if(data.success){
                    permissionList = JSON.parse(data['data']);
                }else{
                    if(data.message != null && data.message != ''){
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if(errorMsg != undefined && errorMsg != ''){
                            alert(errorMsg);
                        }else{
                            alert(data.message);
                        }
                    }
                }
                $page.popuplatePageWithRoles(permissionList);
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    window.nvxpage = $page;

    window.nvxpage.loadingPage();
});