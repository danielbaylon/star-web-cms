var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

/* product view, begin */
nvx.CheckoutView = function(base) {
    var s = this;
    s.gstRate =  ko.observable(0.0);
    s.hasEdit = ko.observable(false);
    s.agreeTnc = ko.observable(false);

    s.products = ko.observableArray([]);

    s.grandTotal = ko.computed(function() {
        var gt = 0;
        $.each(s.products(), function(idx, prod) {
            gt += Number(prod.subtotal());
        });
        return gt.toFixed(2);
    });
    s.grandTotalText = ko.computed(function() {
        return kendo.toString(Number(s.grandTotal()), "###,###,##0.00");
    });
    s.gst = ko.computed(function() {
        return (Number(s.grandTotal()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
    });
    s.gstRateStr = ko.computed(function() {
        return (s.gstRate()*100).toFixed(0) +'%';
    });
    s.gstText = ko.computed(function() {
        return kendo.toString(Number(s.gst()), "###,###,##0.00");
    });
    s.totalNoGst = ko.computed(function() {
        return (Number(s.grandTotal()) - Number(s.gst())).toFixed(2);
    });
    s.totalNoGstText = ko.computed(function() {
        return kendo.toString(Number(s.totalNoGst()), "###,###,##0.00");
    });

    s.initCheckoutView = function(data){
        s.gstRate(data['gstRate']);
        $.each(data.cmsProducts, function(idx, obj) {
            s.products.push(new nvx.CheckoutProduct(s, obj));
        });
    };

    s.reset = function(){
        s.gstRate('');
        s.hasEdit('');
        s.agreeTnc('');
        s.products([])
    };
};

nvx.CheckoutProduct = function(parent, base) {
    var s = this;
    s.parent = parent;

    s.title = ko.observable(base.name);
    s.subtotal = ko.observable(base.subtotal);

    s.showTopupsToAdd = ko.observable(false);
    s.topupsToAddText = ko.computed(function() {
        return s.showTopupsToAdd() ? '' : '';
    });
    s.doToggleTopupsToAdd = function() {
        s.showTopupsToAdd(!s.showTopupsToAdd());
    };

    s.items = ko.observableArray([]);
    (function initItems(theBase) {
        var iarr = [];
        $.each(theBase.items, function(idx, obj) {
            //console.log("obj.qty:"+obj.qty)
            var addItem = new nvx.CheckoutItem(s, obj);
            iarr.push(addItem);
        });
        s.items(iarr);
    })(base);

    s.additionalTopups = ko.observableArray([]);
    //console.log("s.items:"+s.items.length);
    //Further init (in order for parent dot remove item to work)
    $.each(s.items(), function(idx, item) {
        item.includeInCart(true);
    });

    s.subtotalText = ko.computed(function() {
        var total = 0;
        $.each(s.items(), function(dix, item) {
            total += Number(item.total());
        });

        s.subtotal(total.toFixed(2));
        return kendo.toString(Number(s.subtotal()), "###,###,##0.00");
    });
    s.subtotalText.subscribe(function(newValue) {
        s.parent.hasEdit(true);
    });
};

nvx.CheckoutItem = function(parent, base) {
    var s = this;
    s.parent = parent;

    s.isNew = ko.observable(false);
    s.scheduleId = base.scheduleId;
    s.cartId = base.cartId;
    s.cartType = base.cartType;
    s.itemId = base.id;
    s.isTopup = base.topup;
    s.title = base.name;
    s.ticketType = base.type;
    s.includeInCart =  ko.observable(true);
    s.description = base.description;
    s.priceInCents = base.price * 100;
    s.priceText = base.price;
    s.qty = ko.observable(base.qty);
    s.total = ko.observable(base.total);
    s.totalText = ko.computed(function() {
        if (!Number.isInteger(s.qty())){ return 0; }

        var totalInCents = Number(s.qty()) * Number(s.priceInCents);
        s.total((totalInCents / 100).toFixed(2));
        return kendo.toString(Number(s.total()), "###,###,##0.00");
    });
};
/* product view, end */


