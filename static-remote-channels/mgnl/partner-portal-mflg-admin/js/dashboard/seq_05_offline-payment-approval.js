var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

nvx.OfflineReviewModel = function(pageWinDoc, reviewWin){
    var s = this;
    s.parentController = pageWinDoc;
    s.parentWindow = reviewWin;
    s.id = ko.observable('');
    s.transId = ko.observable('');
    s.action = ko.observable('');
    s.approverRemarksLabel = ko.observable('');
    s.approverRemarks = ko.observable('');
    s.approverRemarksLabel('Remarks');
    s.update = function(data){
        s.id(data.id);
        s.transId(data.transId);
        s.chatHists.removeAll();
        $.each(data.chatHists, function(key, item) {
            s.chatHists.push(new $page.ChatHist(item));
        });
    };
    s.submitOfflineReview = function(data){
        console.log('offline payment review data : '+JSON.stringify(data));
        nvx.spinner.start();
        $.ajax({
            type: 'POST',
            url:  nvx.API_PREFIX + '/offlinePay/review',
            data: { id : data['id'], action : data['action'], remarks : data['approverRemarks'] },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert(data['message']);
                    s.parentController.transPopWindow.close();
                    s.parentController.opModel.filter();
                    s.parentWindow.close();
                } else {
                    alert(data['message']);
                }
                nvx.spinner.stop();
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
                nvx.spinner.stop();
            }
        });
    };
    s.save = function(){
        var tmpObj = {};
        tmpObj.id = s.id();
        tmpObj.action = s.action();
        var tempRemarks = s.approverRemarks();
        if(!tempRemarks){
            alert(s.approverRemarksLabel() + ' is mandatory.');
            return false;
        }
        if(tempRemarks.trim().length == 0){
            alert(s.approverRemarksLabel() + ' is mandatory.');
            return false;
        }
        if(tempRemarks.trim().length > 200){
            alert("Please enter no more than 200 characters for Remarks. You've currently entered "+tempRemarks.trim().length+" characters.");
            return false;
        }
        tmpObj.approverRemarks = tempRemarks.trim();
        console.log(JSON.stringify(tmpObj));
        s.submitOfflineReview(tmpObj);
    }
};

nvx.OfflinePaymentApprovalModel = function(pageWinDoc) {
    var s = this;
    s.parentController = pageWinDoc;
    s.displayAdminActions = ko.observable(false);
    s.approveAllowed = ko.observable(false);
    s.manageAllowed = ko.observable(false);

    s.partner = ko.observable(new nvx.PartnerProflePreviewModel());
    s.cart    = ko.observable(new nvx.CheckoutView());
    s.offlinePay = ko.observable(new nvx.OfflinePaymentModel());

    s.initOfflinePaymentApprovalModel = function(data){
        var cart = data['cart'];
        if(cart != undefined && cart != null && cart != ''){
            var partner = cart['partner'];
            if(partner != undefined && partner != null && partner != ''){
                s.partner().initPartnerProflePreviewModel(partner);
            }
            s.cart().initCheckoutView(cart);
        }
        var paymentDetail = data['paymentDetail'];
        if(paymentDetail != undefined && paymentDetail != null && paymentDetail != '' ){
            s.offlinePay().initOfflinePaymentModel(paymentDetail);
        }
        var canApprove = data['canApprove'];
        if(canApprove != undefined && canApprove != null && canApprove == true){
            s.approveAllowed(true);
        }
        var canManage = data['canManage'];
        if(canManage != undefined && canManage != null && canManage == true){
            s.manageAllowed(true);
        }
        if(s.approveAllowed() || s.manageAllowed()){
            s.displayAdminActions(true);
        }
    };

    s.reset = function(){
        s.cart().reset();
        s.partner().reset();
        s.offlinePay().reset();
    };

    s.offlinePaymentOperation = function(actionName, popupTitle , remarksLabelName){
        var htmlcontent = s.parentController.offlineReviewPopTemp({});
        if(!$("#tempWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'tempWindow');
        }
        s.ofRevWindow = $("#tempWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: popupTitle,
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        s.ofRevWindow.content(htmlcontent);
        s.ofRevModel = new nvx.OfflineReviewModel(s.parentController, s.ofRevWindow);
        s.ofRevModel.approverRemarksLabel(remarksLabelName);
        s.ofRevModel.action(actionName);
        s.ofRevModel.id(s.offlinePay().id());
        ko.applyBindings(s.ofRevModel,$('#offlineReviewPopForm')[0]);
        s.ofRevWindow.center().open();
    };

    s.notifyPayment = function(){
        s.offlinePaymentOperation('Notify','Notify Partner','Notify');
    };

    s.approvePayment = function(){
        s.offlinePaymentOperation('Approve','Approve Order','Admin Remarks');

    };

    s.rejectPayment = function(){
        s.offlinePaymentOperation('Reject','Cancel Order','Admin Remarks');
    };
}