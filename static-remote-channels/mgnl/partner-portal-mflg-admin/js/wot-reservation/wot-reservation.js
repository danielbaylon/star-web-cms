var _is_init = 0;
var _advanced_reservation_days = -1;
var _advanced_reservation_date = new Date();
var _advanced_reservation_release_days = -1;
var _advanced_reservation_start_date = new Date();
(function(nvx, $, ko) {
    $(document).ready(function() {
        nvx.spinner.start();
        nvx.initStaticPageData();
        try{
            if(_advanced_reservation_release_days == -1){
                _advanced_reservation_release_days = 0;
                nvx.getWotAdvancedReservationReleaseDays(function(days){
                    /** henny asked to removed this logic,begin**/
                    /**_advanced_reservation_release_days = days;**/
                    /** henny asked to removed this logic,end**/
                    _advanced_reservation_start_date = new Date();
                    _advanced_reservation_start_date.setDate(_advanced_reservation_start_date.getDate() + _advanced_reservation_release_days);
                    _advanced_reservation_start_date.setMilliseconds(0);
                    _advanced_reservation_start_date.setSeconds(0);
                    _advanced_reservation_start_date.setMinutes(0);
                    _advanced_reservation_start_date.setHours(0);
                    if(_advanced_reservation_days == -1){
                        _advanced_reservation_days = 0;
                        nvx.getWotAdvancedReservationDays(function(days){
                            _advanced_reservation_days = days;
                            _advanced_reservation_date = new Date();
                            _advanced_reservation_date.setDate(_advanced_reservation_date.getDate() + days);
                            _advanced_reservation_date.setMilliseconds(0);
                            _advanced_reservation_date.setSeconds(0);
                            _advanced_reservation_date.setMinutes(0);
                            _advanced_reservation_date.setHours(0);
                            nvx.getWotSchedules(function(data){
                                nvx.initPage(data);
                            });
                        });
                    }
                });
            }
        }catch(e){
            nvx.showInitPageContent();
            nvx.spinner.stop();
            console.log('init page with schedule failed : '+e);
        }
    });

    nvx.initStaticPageData = function(){
        nvx.errorModel = new nvx.ErrorModel();
        var errorMessagePopup = $("#errorWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');
        ko.applyBindings(nvx.errorModel, document.getElementById('errorWindow'));
        window.errorMessagePopup = errorMessagePopup;
    };

    nvx.initPage = function(data) {
        nvx.searchCritirialModel = new nvx.searchCritirialModel();
        nvx.searchCritirialModel.initSearchModel(data);

        nvx.reservationModel = new nvx.ReservationModel();
        nvx.reservationModel.initScheduleList(data);

        nvx.reservationConfirmModel = new nvx.ReservationConfirmModel();

        nvx.confirmMsgModeal = new nvx.ConfirmMessageModel();

        ko.applyBindings(nvx.reservationModel , document.getElementById('viewReservationWindow'));
        ko.applyBindings(nvx.searchCritirialModel , document.getElementById('filterSectionContentDiv'));
        ko.applyBindings(nvx.reservationConfirmModel,document.getElementById('viewReservationConfirmWindow'));
        ko.applyBindings(nvx.confirmMsgModeal,document.getElementById('confirmMsgWindow'));

        nvx.initPageContent();
        nvx.showInitPageContent();
    };

    nvx.showInitPageContent = function(){
        $('.pendingInitElementIndicator').show();
    };

    nvx.getWotSchedules = function(callbackFunction){
        try{
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-wot/get-schedule',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR){
                    nvx.spinner.stop();
                    if(data['success'] != undefined && data['success'] != null && data['success'] != '' && data['success'] == true){
                        callbackFunction(data['data']);
                    }else{
                        if(data['message'] != undefined && data['message'] != null && data['message'] != ''){
                            console.log('Loading schedule failed : '+ data['message']);
                        }
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                }
            });
        }catch(e){
            console.log('getWotSchedules : '+e);
        }
    };
    nvx.getWotAdvancedReservationDays = function(callbackFunction){
        try{
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-wot/get-advanced-reservation-days-allowed',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR){
                    if(data['success'] != undefined && data['success'] != null && data['success'] != '' && data['success'] == true){
                        callbackFunction(data['data']);
                    }else{
                        if(data['message'] != undefined && data['message'] != null && data['message'] != ''){
                            console.log('Loading schedule failed : '+ data['message']);
                        }
                        nvx.showMsgWindowWithCallBack('Error Message',  data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                }
            });
        }catch(e){
            console.log('getWotAdvancedReservationDays : '+e);
        }
    };
    nvx.getWotAdvancedReservationReleaseDays = function(callbackFunction){
        try{
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-wot/get-advanced-reservation-release-day',
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR){
                    if(data['success'] != undefined && data['success'] != null && data['success'] != '' && data['success'] == true){
                        callbackFunction(data['data']);
                    }else{
                        if(data['message'] != undefined && data['message'] != null && data['message'] != ''){
                            console.log('Loading schedule failed : '+ data['message']);
                        }
                        nvx.showMsgWindowWithCallBack('Error Message',  data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                }
            });
        }catch(e){
            console.log('getWotAdvancedReservationReleaseDays : '+e);
        }
    };

    nvx.initPageContent = function(){
        var s = this;
        s.BASE_URL = nvx.API_PREFIX + '/partner-wot/search-reservation';
        s.theUrl = s.BASE_URL;
        s.partner = $('#partner').kendoComboBox({
            placeholder : 'Select Partner',
            dataTextField : 'orgName',
            dataValueField : 'id',
            dataSource: {
                transport: { read: {dataType: 'json',url: nvx.API_PREFIX + '/partner-wot/get-partners',cache: false }  },
                schema: { data: 'data' }
            },
            filter: 'contains'
        }).data('kendoComboBox');

        s.travelAgent = $('#travelAgent').kendoComboBox({
            placeholder : 'Select Partner',
            dataTextField : 'orgName',
            dataValueField : 'id',
            dataSource: {
                transport: { read: {dataType: 'json',url: nvx.API_PREFIX + '/partner-wot/get-partners',cache: false }  },
                schema: { data: 'data' }
            },
            filter: 'contains'
        }).data('kendoComboBox');

        s.startDate = $('#startDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');

        s.endDate = $('#endDate').kendoDatePicker({
            format: nvx.DATE_FORMAT
        }).data('kendoDatePicker');

        $('#wotReservationStatus').kendoDropDownList({
            dataTextField: "name",
            dataValueField: "id",
            dataSource: [
                { name: "All", id: "All" },
                { name: "Reserved", id: "Reserved" },
                { name: "Released", id: "Released" },
                { name: "Cancelled", id: "Cancelled" }
            ]
        });

        s.refreshUrl = function() {
            var filterStatus = $('#wotReservationStatus').data('kendoDropDownList').value();
            s.theUrl = s.BASE_URL + '?partnerId='+(s.partner.value())+'&startDateStr=' + $('#startDate').val()
                + '&endDateStr=' + $('#endDate').val()+ '&filterStatus=' + filterStatus
                + '&showTime=' +  $("input[name=showTimes]:checked").val();
            itemsdataSource.options.transport.read.url = s.theUrl;
            itemsdataSource.page(1);
        };

        $('#btnfilter').click(function(event){
            event.preventDefault();
            s.refreshUrl();
        });

        $('#btnReset').click(function(event){
            event.preventDefault();
            $('#wotReservationStatus').data('kendoDropDownList').value('All');
            s.partner.value('');
            s.startDate.value('');
            s.endDate.value('');
            $("#displayCancelled").prop('checked',false);
            var size = $("input[name=showTimes]").size();
            if(size > 0){
                $($("input[name=showTimes][value='']")).prop('checked', true);
            }
        });

        s.pageSize = 10;
        var itemsdataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: s.theUrl,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: 'number'},
                        partnerName: {type: 'string'},
                        now: {type: 'number'},
                        cutOffDateTime: {type: 'number'},
                        eventDate: {type: 'number'},
                        receiptNo: {type: 'string'},
                        pinCode: {type: 'string'},
                        reservationType: {type: 'string'},
                        qty: {type: 'number'},
                        qtySold: {type: 'number'},
                        rdm: {type: 'number'},
                        unr: {type: 'number'},
                        status: {type: 'string'},
                        remarks: {type: 'string'}
                    }
                }
            }, pageSize: s.pageSize, serverSorting: true, serverPaging: true,
            requestStart: function () {
                if(_is_init == 1){
                    nvx.spinner.start();
                }
            },
            requestEnd: function () {
                if(_is_init == 1) {
                    nvx.spinner.stop();
                }
            }
        });

        s.kGrid = $("#reservationGrid").kendoGrid({
            dataSource: itemsdataSource,
            dataBound: dataBound,
            columns: [{
                field: "partnerName",
                title: "Partner",
                width: 100
            }, {
                field: "eventDate",
                title: "Show Date",
                sortable: false,
                width: 140,
                template: function(dataItem) {
                    return kendo.toString(new Date(dataItem.eventDate), 'dddd, dd MMM yyyy') + '<br>' +  dataItem.showTimeDisplay;
                }
            },{
                field: "receiptNo",
                title: "Reservation<br/>No",
                template: '<a href="javascript:viewReservation(#=id#);" >#=receiptNo#</a>',
                width: 150
            },{
                field: "qty",
                title: "Reserved<br/>Qty",
                width: 80
            }, {
                field: "qtySold",
                title: "Purchased<br/>Qty",
                width: 80
            },{
                field: "unr",
                title: "Released<br/>Qty",
                template: function(dataItem) {
                    var xst = dataItem['status'];
                    if(xst != undefined && xst != null && xst != ''){
                        if('Released' == xst){
                            return Math.abs(parseInt(dataItem['qty']) - parseInt(dataItem['qtySold']));
                        }
                    }
                    return '';
                },
                width: 80
            }, {
                field: "status",
                title: "Status",
                width: 100,
                template: function(dataItem) {
                    var xst = dataItem['status'];
                    if(xst != undefined && xst != null && xst != ''){
                        if('FullyPurchased' == xst || 'PartiallyPurchased' == xst){
                            return 'Reserved';
                        }
                        return xst;
                    }
                    return '';
                }
            }, {
                field: "remarks",
                title: "Remarks",
                template: function(dataItem) {
                    var remarks = dataItem['remarks'];
                    if(!(remarks != undefined && remarks != null && remarks != '')){
                        remarks = '';
                    }else{
                        remarks = '<div style="text-align: left;">'+remarks+'</div>';
                    }
                    var rdt = dataItem['releasedDate'];
                    var xst = dataItem['status'];
                    if(xst != undefined && xst != null && xst != ''){
                        if('Released' == xst){
                            if(rdt != undefined && rdt != null && rdt != ''){
                                if(remarks != undefined && remarks != null && remarks != ''){
                                    remarks = '<div style="text-align: left;">'+remarks+'</div><div><br/></div><div style="text-align: left;">Released at '+rdt+'</div>';
                                }else{
                                    remarks = '<div style="text-align: left;">Released at '+rdt+'</div>';
                                }
                            }
                        }
                    }
                    return remarks;
                }
            }, {
                field: "id",
                title: "&nbsp",
                width: 100,
                template: function(dataItem) {
                    var btnTmpl = "";
                    if(dataItem.status == 'Reserved' && dataItem.reservationType == 'Reserve' && dataItem.qty > 0 && dataItem.qty > dataItem.qtySold ) {
                        btnTmpl += '<input id="btnCancel" type="button" class="btn btn-primary" value="Cancel" style="margin-bottom: 5px; width: 50px" onclick="nvx.reservationModel.cancelReservation(' + dataItem.id + ')"/><br/>';
                    }
                    return btnTmpl;
                }
            }],
            sortable: false, pageable: true
        });

        function dataBound(e){
            var grid = $("#reservationGrid").data("kendoGrid");
            var currentPage = itemsdataSource.page();
            var pageSize = itemsdataSource.pageSize();
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
            });
        }
        //view pop start

        var reservationPopup = $("#viewReservationWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var confirmPopup = $("#viewReservationConfirmWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        var confirmMsgPopup = $("#confirmMsgWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            actions: ["Close"],
            viewable : false
        }).data('kendoWindow');

        function viewReservation(id){
            if(id != undefined && id != null && id != '' && id >= 0) {
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/partner-wot/get-reservation/' + id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    success:  function(data, textStatus, jqXHR)
                    {
                        if (data['message'] != undefined && data['message'] != null && data['message'] != '') {
                            nvx.spinner.stop();
                            console.log('ERRORS: ' + data['message']);
                            nvx.showMsgWindowWithCallBack('Error Message',  data['message'], function(){});
                        }else{
                            nvx.reservationModel.initReservationMode(data.data);
                            if(nvx.reservationModel.isEditable()) {
                                reservationPopup.title("Change WOT Reservation");
                            }else {
                                reservationPopup.title("View WOT Reservation");
                            }
                            reservationPopup.center().open();
                            nvx.spinner.stop();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }else {
                nvx.spinner.start();
                nvx.reservationModel.createNewModel();
                reservationPopup.title("New WOT Reservation");
                reservationPopup.center().open();
                nvx.spinner.stop();
            }
        }

        $('#btnNewReservation').click(function(event) {
            viewReservation(-1);
        });

        $(".k-grid-header").css("padding-right", "0px");

        window.viewReservation = viewReservation;
        window.reservationPopup = reservationPopup;
        window.refreshGrid = s.refreshUrl;
        window.confirmPopup = confirmPopup;
        window.confirmMsgPopup = confirmMsgPopup;
        // default the show time selection
        var size = $("input[name=showTimes]").size();
        if(size > 0){
            $($("input[name=showTimes][value='']")).prop('checked', true);
        }

        if(_is_init == 0){
            nvx.spinner.stop();
            _is_init = 1;
        }
    };

    nvx.searchCritirialModel = null;

    nvx.searchCritirialModel = function(){
        var s = this;

        s.showTimeSearchCriterialList = ko.observableArray([]);

        s.initShowTimeSearchCriterialList = function(result){
            var showTimesCrtieralDataMapInd = new Object();
            var showTimesCrtieralDataMap = [{  value : '', text : 'All' }];
            if(result != undefined && result != null && result != '') {
                $.each(result, function(idx, prod) {
                    if(prod != undefined && prod != null && prod != '') {
                        var lines = prod['eventLines'];
                        if(lines != undefined && lines != null && lines != '') {
                            $.each(lines, function(idx2, el) {
                                var lineId = el['eventLineId'];
                                if(lineId != undefined && lineId != null && lineId != '') {
                                    var exists = showTimesCrtieralDataMapInd[lineId];
                                    if(!(exists != undefined && exists != null && exists != '' && exists == 1)){
                                        showTimesCrtieralDataMap.push({
                                            value : el['eventLineId'],
                                            text : el['eventName']
                                        });
                                        showTimesCrtieralDataMapInd[lineId] = 1;
                                    }
                                }
                            });
                        }
                    }

                });
            }
            s.showTimeSearchCriterialList(showTimesCrtieralDataMap);
            showTimesCrtieralDataMapInd = null;
        };

        s.initSearchModel = function(data){
            if(data != undefined && data != null && data != ''){
                if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                    s.initShowTimeSearchCriterialList(data['products']);
                }
            }
        };
    };

    nvx.reservationModel =  null;

    nvx.ReservationModel = function(base) {
        var s = this;
        s.id = ko.observable();
        s.partnerId = ko.observable();
        s.partnerName = ko.observable();
        s.receiptNo = ko.observable();
        s.pinCode = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.rdm = ko.observable();
        s.unr = ko.observable();
        s.remarks = ko.observable();
        s.createMode = ko.observable(false);
        s.updateMode = ko.observable(false);
        s.splitMode = ko.observable(false);
        s.createdBy = ko.observable();
        s.createdByUsername = ko.observable();
        s.createdDate = ko.observable();
        s.notifInfo = ko.observable("");
        s.showTime = ko.observable();
        s.showDate = ko.observable();
        s.productShowScheduleList = ko.observableArray([]);
        s.showTimeList = ko.observableArray([]);
        s.showTimeDisplay = ko.observable("");
        s.eventLineId = ko.observable("");
        s.productId = ko.observable("");
        s.penaltyCharge = ko.observable("");
        s.eventDate = ko.observable(-1);
        s.cutOffDateTime = ko.observable(-1);
        s.now = ko.observable(-1);

        s.itemId = ko.observable();
        s.mediaTypeId = ko.observable();
        s.eventGroupId = ko.observable();
        s.openValidityEndDate = ko.observable();
        s.openValidityStartDate = ko.observable();

        s.qtyFromDB = ko.observable();
        s.showDateFromDB = ko.observable();
        s.remarksFromDB = ko.observable();
        s.showTimeDisplayFromDB = ko.observable();

        s.eventStartTime = ko.observable();
        s.eventTime = ko.observable();
        s.eventEndTime = ko.observable();
        s.eventCapacityId = ko.observable();

        s.isEditable = ko.computed(function(){
            if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                if(s.status() != undefined && s.status() != null && s.status() != ''){
                    if(s.status() == 'Reserved') {
                        return true;
                    }
                }
            }else{
                return true;
            }
            return false;
        });

        s.isAnExistingOrder = ko.computed(function(){
            if(s.status() != undefined && s.status() != null && s.status() != ''){
                if(s.id() != undefined && s.id() != null && s.id() != ''){
                    return true;
                }
            }
            return false;
        });

        s.reset = function(){
            nvx.travelAgent.value('');
            s.partnerId('');
            s.partnerName('');
            s.createMode(false);
            s.updateMode(false);
            s.splitMode(false);
            s.id(-1);
            s.partnerId('');
            s.receiptNo('');
            s.pinCode('');
            s.showTime('');
            s.showTimeDisplay('');
            s.showTimeDisplayFromDB('');
            s.eventLineId('');
            s.productId('');
            s.showDate('');
            s.showDateFromDB('');
            s.status('');
            s.qty(0);
            s.qtyFromDB(0);
            s.rdm(0);
            s.unr(0);
            s.remarks('');
            s.remarksFromDB('');
            s.createdBy('');
            s.createdByUsername('');
            s.createdDate('');
            s.itemId('');
            s.mediaTypeId('');
            s.eventGroupId('');
            s.openValidityEndDate('');
            s.openValidityStartDate('');
            s.eventDate(-1);
            s.cutOffDateTime(-1)
            s.now(-1);

            s.eventStartTime('');
            s.eventTime('');
            s.eventEndTime('');
            s.eventCapacityId('');

        };

        s.initScheduleList = function (data){
            if(data != undefined && data != null && data != ''){
                if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                    s.productShowScheduleList(data['products']);
                }
            }
        };

        s.refreshScheduleList = function (){
            nvx.getWotSchedules(function(data){
                if(data != undefined && data != null && data != ''){
                    if(data['products'] != undefined && data['products'] != null && data['products'] != ''){
                        s.productShowScheduleList(data['products']);
                    }
                }
            });
        };

        s.initReservationMode = function(data) {
            s.reset();
            if(data.id != undefined && data.id != null && data.id != '' && data.id != -1 && data.id != -1){
                s.updateMode(true);
            }else{
                s.createMode(true);
            }
            s.id(data.id);
            s.partnerName(data.partnerName);
            s.partnerId(data.partnerId);
            s.receiptNo(data.receiptNo);
            s.pinCode(data.pinCode);
            s.showTime(data.productId+'-'+data.eventLineId);
            s.showTimeDisplay(data.showTimeDisplay);
            s.showTimeDisplayFromDB(data.showTimeDisplay);
            s.eventLineId(data.eventLineId);
            s.productId(data.productId);
            s.showDate(data.showDate);
            s.showDateFromDB(data.showDate);
            s.status(data.status);
            s.qty(parseInt(data.qty) - parseInt(data.rdm));
            s.qtyFromDB(data.qty);
            s.rdm(data.rdm);
            s.unr(data.unr);
            s.remarks(data.remarks);
            s.remarksFromDB(data.remarks);
            s.createdBy(data.createdBy);
            s.createdByUsername(data.createdByUsername);
            s.createdDate(data.createdDate);
            s.itemId(data.itemId);
            s.mediaTypeId(data.mediaTypeId);
            s.eventGroupId(data.eventGroupId);
            s.openValidityEndDate(data.openValidityEndDate);
            s.openValidityStartDate(data.openValidityStartDate);
            s.eventDate(data.eventDate);
            s.cutOffDateTime(data.cutOffDateTime)
            s.now(data.now);

            s.eventStartTime(data.eventStartTime);
            s.eventTime(data.eventTime);
            s.eventEndTime(data.eventEndTime);
            s.eventCapacityId(data.eventCapacityId);

            s.initShowDate();
        }

        s.createNewModel = function() {
            s.reset();
            s.createMode(true);
            s.initShowDate();
        }

        s.printReservation = function(id) {
            if(id != undefined && id != null && id != '' && id >= 0) {
                var _w = $( window ).width();
                var _h = $( window ).height();

                var _left = (_w - 960)/2;

                if(_left < 0){
                    _left = 200;
                }

                var url = '/partner-portal-mflg/print-reservations?id='+id;
                var ticketPrintWindow = window.open(url, 'ticketPrintWindow', 'resizable=no,top=50,left='+_left+',width=960,height=1024');
                ticketPrintWindow.focus(); // necessary for IE >= 10
            }
        };

        s.cancelReservation = function(id) {
            nvx.showConfirmMsgWindow('Confirm Cancel Reservation', 'Are you sure you want to cancel this reservation?', function(){
                nvx.spinner.start();
                s.id(id); //set the id being cancelled now
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/partner-wot/validate-cancel-reservation/' + s.id(),
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            nvx.reservationConfirmModel.initConfirmMode('C', data.data, s.id());
                            window.confirmPopup.title("Cancel Reservation");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }
                        else
                        {
                            nvx.spinner.stop();
                            nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){
                                window.location.reload();
                            });
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            });
        }

        s.proceedCancelReservation = function() {
            var _input_id = s.id();
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-wot/cancel-reservation/'+_input_id,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        window.refreshGrid();
                        window.confirmPopup.close();
                        s.closeModal();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation is now cancelled!', function(){});
                    }
                    else
                    {
                        window.refreshGrid();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.initShowDate = function() {
            var result = s.productShowScheduleList();
            if(result != undefined && result != null && result != '' ) {
                var productDatesMap = [];
                var enableDates = [];
                enableDates.push(true);
                //get all the dates
                $.each(result, function(idx, prod) {
                    if(prod != undefined && prod != null && prod != '' ) {
                        var lines = prod['eventLines'];
                        if(lines != undefined && lines != null && lines != '' ) {
                            $.each(lines, function(idx2, el) {
                                var eds = el['eventDates'];
                                if(eds != undefined && eds != null && eds != '' ) {
                                    $.each(eds, function(idx3, ed) {
                                        var eventDate = ed['date'];
                                        if(eventDate != undefined && eventDate != null && eventDate != '' ) {
                                            if(_advanced_reservation_start_date.getTime() <= ed.rawDate){
                                                if(_advanced_reservation_date.getTime() >= ed.rawDate){
                                                    if(productDatesMap[eventDate] == null) {
                                                        var dtArr = eventDate.split('/');
                                                        enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);
                                                        productDatesMap[eventDate] = [];
                                                    }
                                                    var product = {
                                                        eventLine: el,
                                                        productId: prod.productId,
                                                        itemId : prod.itemId,
                                                        mediaTypeId : prod.mediaTypeId,
                                                        eventGroupId : prod.eventGroupId,
                                                        openValidityStartDate : prod.openValidityStartDate,
                                                        openValidityEndDate : prod.openValidityEndDate
                                                    };
                                                    productDatesMap[eventDate].push(product);
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });

                var $ebDate = $('#showDate');
                $ebDate.pickadate().pickadate('picker').stop();
                setTimeout(function() {
                    $ebDate.pickadate({
                        format: 'dd/mm/yyyy',
                        container: '.main-content-section-container',
                        disable: enableDates
                    });
                }, 300);

                s.showDate.subscribe(function(newSelectedDate) {
                    s.updateShowTimeList(productDatesMap, newSelectedDate);
                });

                s.updateShowTimeList(productDatesMap, s.showDate());
            }
        };

        s.updateShowTimeList = function(productDatesMap, newSelectedDate) {
            var updatedShowTimeList = [];
            s.showTimeList([]);
            if(newSelectedDate != "") {
                var foundDate = false;
                if(productDatesMap[newSelectedDate] != undefined && productDatesMap[newSelectedDate] != null){
                    foundDate = true;
                }
                if(!foundDate){
                    newSelectedDate = new Date();
                }
                if(productDatesMap[newSelectedDate] != undefined && productDatesMap[newSelectedDate] != null){
                    $.each(productDatesMap[newSelectedDate], function(idx, prod) {
                        var showTime = new Object();
                        showTime.itemId = prod.itemId;
                        showTime.productId = prod.productId;
                        showTime.mediaTypeId = prod.mediaTypeId;
                        showTime.text = prod.eventLine.eventName;
                        showTime.eventGroupId = prod.eventGroupId;
                        showTime.eventLineId = prod.eventLine.eventLineId;
                        showTime.openValidityEndDate = prod.openValidityEndDate;
                        showTime.openValidityStartDate = prod.openValidityStartDate;
                        showTime.value = showTime.productId + "-" + showTime.eventLineId;
                        showTime.eventStartTime = prod.eventLine.eventStartTime;
                        showTime.eventTime = prod.eventLine.eventTime;
                        showTime.eventEndTime = prod.eventLine.eventEndTime;
                        showTime.eventCapacityId = prod.eventLine.eventCapacityId;
                        updatedShowTimeList.push(showTime);
                    });
                }
            }

            s.showTimeList(updatedShowTimeList);
        };

        s.setShowTimeInfo = function() {
            $.each(s.showTimeList(), function(idx, mdl) {
                if(s.showTime() == mdl.value) {
                    s.showTimeDisplay(mdl.text);
                    s.productId(mdl.productId);
                    s.eventLineId(mdl.eventLineId);
                    s.itemId(mdl.itemId);
                    s.mediaTypeId(mdl.mediaTypeId);
                    s.eventGroupId(mdl.eventGroupId);
                    s.openValidityEndDate(mdl.openValidityEndDate);
                    s.openValidityStartDate(mdl.openValidityStartDate);
                    s.eventStartTime(mdl.eventStartTime);
                    s.eventTime(mdl.eventTime);
                    s.eventEndTime(mdl.eventEndTime);
                    s.eventCapacityId(mdl.eventCapacityId);
                }
            });
        };

        s.doSaveValidate = function(isUpdate) {
            //Validations
            if(!isUpdate){
                var paId = nvx.travelAgent.value();
                if(!(paId != undefined && paId != null && paId != '')){
                    nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Travel Agent.', function(){});
                    return false;
                }
                s.partnerId(paId);
            }

            if(!(s.partnerId() != null && s.partnerId() != '')){
                nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Travel Agent.', function(){});
                return false;
            }

            //Validate that there's show date
            if(s.showDate() == '') {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Show Date.', function(){});
                return false;
            }

            //Validate that there's show time
            if(s.showTime() == '') {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please select a Show Time.', function(){});
                return false;
            }

            //Validate that quantity is 1 and above
            if(s.qty() == '' || s.qty() == 0) {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please enter a minimum Quantity of 1.', function(){});
                return false;
            }

            //validate if the quantity is number
            var numberRegex = /^\d+$/;

            if(!numberRegex.test(s.qty())) {
                nvx.showMsgWindowWithCallBack('Error Message', 'Please enter a valid Quantity', function(){});
                return false;
            }

            return true;
        };

        s.refreshWoTReservationDetails = function(id){

        };

        s.saveReservation = function() {
            nvx.spinner.start();
            var apiURL =  nvx.API_PREFIX + '/partner-wot/save-reservation';
            $.ajax({
                type: "POST",
                url: apiURL,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                data: JSON.stringify({
                    id : s.id(),
                    partnerId : s.partnerId(),
                    qty : s.qty(),
                    showTime : s.showTime(),
                    showTimeDisplay : s.showTimeDisplay(),
                    productId : s.productId(),
                    eventLineId : s.eventLineId(),
                    showDate : s.showDate(),
                    remarks : s.remarks(),
                    itemId : s.itemId(),
                    mediaTypeId : s.mediaTypeId(),
                    eventGroupId : s.eventGroupId(),
                    openValidityEndDate : s.openValidityEndDate(),
                    openValidityStartDate : s.openValidityStartDate(),
                    eventStartTime : s.eventStartTime(),
                    eventTime : s.eventTime(),
                    eventEndTime : s.eventEndTime(),
                    eventCapacityId : s.eventCapacityId()
                }),
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                            s.refreshWoTReservationDetails(wot['id']);
                        }
                        window.refreshGrid();
                        window.confirmPopup.close();
                        s.closeModal();
                        nvx.spinner.stop();

                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation is successful!', function(){});
                    }
                    else
                    {
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){
                            window.confirmPopup.close();
                            s.closeModal();
                        });
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.updaeReservation = function() {
            nvx.spinner.start();
            var apiURL =  nvx.API_PREFIX + '/partner-wot/update-reservation';
            $.ajax({
                type: "POST",
                url: apiURL,
                headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                data: JSON.stringify({
                    id : s.id(),
                    qty : s.qty(),
                    partnerId : s.partnerId(),
                    showTime : s.showTime(),
                    showTimeDisplay : s.showTimeDisplay(),
                    productId : s.productId(),
                    eventLineId : s.eventLineId(),
                    showDate : s.showDate(),
                    remarks : s.remarks(),
                    itemId : s.itemId(),
                    mediaTypeId : s.mediaTypeId(),
                    eventGroupId : s.eventGroupId(),
                    openValidityEndDate : s.openValidityEndDate(),
                    openValidityStartDate : s.openValidityStartDate(),
                    eventStartTime : s.eventStartTime(),
                    eventTime : s.eventTime(),
                    eventEndTime : s.eventEndTime(),
                    eventCapacityId : s.eventCapacityId()
                }),
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        var wot = data['data'];
                        if(wot != undefined && wot != null && wot != '' && wot['id'] != null && wot['id'] != '' && wot['id'] != -1){
                            s.refreshWoTReservationDetails(wot['id']);
                        }
                        window.refreshGrid();
                        window.confirmPopup.close();
                        s.closeModal();
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Message', 'Your reservation modification is successful!', function(){});
                    }
                    else
                    {
                        console.log('ERRORS: ' + data['message']);
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', data['message'], function(){});
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    nvx.spinner.stop();
                    nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.verifyNewReservation = function(){
            if(s.doSaveValidate(false)) {
                nvx.spinner.start();
                s.setShowTimeInfo(); //set the show time display here
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/partner-wot/validate-new-reservation',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    data: JSON.stringify({
                        id : s.id(),
                        partnerId : s.partnerId(),
                        qty : s.qty(),
                        showTime : s.showTime(),
                        showTimeDisplay : s.showTimeDisplay(),
                        productId : s.productId(),
                        eventLineId : s.eventLineId(),
                        showDate : s.showDate(),
                        remarks : s.remarks(),
                        itemId : s.itemId(),
                        mediaTypeId : s.mediaTypeId(),
                        eventGroupId : s.eventGroupId(),
                        openValidityEndDate : s.openValidityEndDate(),
                        openValidityStartDate : s.openValidityStartDate(),
                        eventStartTime : s.eventStartTime(),
                        eventTime : s.eventTime(),
                        eventEndTime : s.eventEndTime(),
                        eventCapacityId : s.eventCapacityId()
                    }),
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            //show the confirm message here if validation is successful
                            nvx.reservationConfirmModel.initConfirmMode('S', data.data, s.id());
                            window.confirmPopup.title("Confirm Reservation");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }
                        else
                        {
                            //show the error message here
                            nvx.errorModel.errorMessage(data.message);
                            window.errorMessagePopup.title("Error Message");
                            window.errorMessagePopup.center().open();
                            console.log('ERRORS: ' + data['message']);
                            nvx.spinner.stop();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        };

        s.verifyUpdateReservation = function() {
            if(s.doSaveValidate(true)) {
                nvx.spinner.start();
                s.setShowTimeInfo(); //set the show time display here
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/partner-wot/validate-update-reservation',
                    headers: { 'Store-Api-Channel' : 'partner-portal-mflg' },
                    data: JSON.stringify({
                        id : s.id(),
                        qty : s.qty(),
                        partnerId : s.partnerId(),
                        showTime : s.showTime(),
                        showTimeDisplay : s.showTimeDisplay(),
                        productId : s.productId(),
                        eventLineId : s.eventLineId(),
                        showDate : s.showDate(),
                        remarks : s.remarks(),
                        itemId : s.itemId(),
                        mediaTypeId : s.mediaTypeId(),
                        eventGroupId : s.eventGroupId(),
                        openValidityEndDate : s.openValidityEndDate(),
                        openValidityStartDate : s.openValidityStartDate(),
                        eventStartTime : s.eventStartTime(),
                        eventTime : s.eventTime(),
                        eventEndTime : s.eventEndTime(),
                        eventCapacityId : s.eventCapacityId()
                    }),
                    cache: false, dataType: 'json', contentType: 'application/json',
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            //show the confirm message here if validation is successful
                            nvx.reservationConfirmModel.initConfirmMode('U', data.data, s.id());
                            window.confirmPopup.title("Confirm Reservation");
                            window.confirmPopup.center().open();
                            nvx.spinner.stop();
                        }
                        else
                        {
                            //show the error message here
                            nvx.errorModel.errorMessage(data.message);
                            window.errorMessagePopup.title("Error Message");
                            window.errorMessagePopup.center().open();
                            console.log('ERRORS: ' + data['message']);
                            nvx.spinner.stop();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        nvx.showMsgWindowWithCallBack('Error Message', 'Unable to process your request. Please try again in a few moments.', function(){});
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        }

        s.closeModal = function() {
            window.reservationPopup.close();
        };
    };

    nvx.reservationConfirmModel = null;

    nvx.ReservationConfirmModel = function() {
        var s = this;

        s.reservationId = ko.observable();
        s.isNew = ko.computed(function() {
            if(s.reservationId() == -1) {
                return true;
            }
            return false;
        });

        s.confirmType = ko.observable();  //S for Saving, C for Cancelling

        s.penaltyCharge = ko.observable(0);
        s.penaltyChargeText = ko.computed(function() {
            if(s.penaltyCharge() > '') {
                return '- S$ ' + kendo.toString(s.penaltyCharge(), "n2");
            }
        });

        s.refundedAmount = ko.observable(0);
        s.refundedAmountText = ko.computed(function() {
            if(s.refundedAmount() > 0) {
                return '+ S$ ' + kendo.toString(s.refundedAmount(), "n2");
            }
        });

        s.ticketPurchased = ko.observable(0);
        s.ticketPurchasedText = ko.computed(function() {
            if(s.ticketPurchased() > 0) {
                return '- S$ ' + kendo.toString(s.ticketPurchased(), "n2");
            }
        });

        s.depositToBeCharged = ko.observable(0);
        s.depositToBeChargedText = ko.computed(function() {
            if(s.depositToBeCharged() > 0) {
                return 'S$ ' + kendo.toString(s.depositToBeCharged(), "n2");
            }
        });

        s.depositToBeRefunded = ko.observable(0);
        s.depositToBeRefundedText = ko.computed(function() {
            if(s.depositToBeRefunded() > 0) {
                return 'S$ ' + kendo.toString(s.depositToBeRefunded(), "n2");
            }
        });

        s.totalAmountText = ko.computed(function() {
            if(s.depositToBeCharged() > 0) {
                return '- ' + s.depositToBeChargedText();
            }else {
                return s.depositToBeRefundedText();
            }
        });

        s.showComputation = ko.computed(function() {
            if((s.penaltyCharge() > 0 && s.refundedAmount() > 0) ||
                (s.penaltyCharge() > 0 && s.ticketPurchased() > 0)) {
                return true;
            }
            return false;
        });

        s.initConfirmMode = function(confirmType, data, reservationId) {
            s.confirmType(confirmType);
            s.penaltyCharge(data.penaltyCharge);
            s.refundedAmount(data.refundedAmount);
            s.ticketPurchased(data.ticketPurchased);
            s.depositToBeCharged(data.depositToBeCharged);
            s.depositToBeRefunded(data.depositToBeRefunded);
            s.reservationId(reservationId);
        };

        s.proceedCancel = function() {
            nvx.reservationModel.proceedCancelReservation();
        };

        s.proceedSave = function() {
            nvx.reservationModel.saveReservation();
        };

        s.proceedUpdate = function() {
            nvx.reservationModel.updaeReservation();
        };

        s.closeModal = function() {
            window.confirmPopup.close();
        };
    };

    nvx.errorModel = null;

    nvx.ErrorModel = function() {
        var s = this;
        s.errorMessage = ko.observable();
        s.callback = '';
        s.closeModal = function() {
            window.errorMessagePopup.close();
            if(s.callback != undefined && s.callback != null && s.callback != ''){
                try{
                    s.callback();
                    s.callback = '';
                }catch(e){
                    console.log('Message callback failed : '+e);
                }
            }
        }
    };

    nvx.showMsgWindow = function(winTitle, winMsg){
        nvx.spinner.start();
        nvx.errorModel.errorMessage(winMsg);
        window.errorMessagePopup.title(winTitle);
        window.errorMessagePopup.center().open();
        nvx.spinner.stop();
    };

    nvx.showMsgWindowWithCallBack = function(winTitle, winMsg, callBack){
        nvx.spinner.start();
        nvx.errorModel.callback ='';
        if(callBack != undefined && callBack != null){
            nvx.errorModel.callback = callBack;
        }
        nvx.errorModel.errorMessage(winMsg);
        window.errorMessagePopup.title(winTitle);
        window.errorMessagePopup.center().open();
        nvx.spinner.stop();
    };

    nvx.confirmMsgModeal = null;

    nvx.ConfirmMessageModel = function(){
        var s = this;
        s.confirmMessage = ko.observable();
        s.callback = '';
        s.confirmModal = function() {
            window.confirmMsgPopup.close();
            if(s.callback != undefined && s.callback != null && s.callback != ''){
                try{
                    s.callback();
                    s.callback = '';
                }catch(e){
                    console.log('Confirm callback failed : '+e);
                }
            }
        }
        s.cancelModal = function(){
            window.confirmMsgPopup.close();
        }
    };

    nvx.showConfirmMsgWindow = function(winTitle, winMsg, confirmActionCallBack){
        nvx.spinner.start();
        nvx.confirmMsgModeal.callback = '';
        if(confirmActionCallBack != undefined && confirmActionCallBack != null){
            nvx.confirmMsgModeal.callback = confirmActionCallBack;
        }
        nvx.confirmMsgModeal.confirmMessage(winMsg);
        window.confirmMsgPopup.title(winTitle);
        window.confirmMsgPopup.center().open();
        nvx.spinner.stop();
    };
})(window.nvx = window.nvx || {}, jQuery, ko);
