var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

nvx.PartnerProflePreviewModel = function(base){
    var s = this;
    s.id = ko.observable();
    s.orgName = ko.observable();
    s.accountCode = ko.observable();
    s.accountCode.subscribe(function(newValue) {
        s.usernameErrMsg("");
    });
    s.uen = ko.observable();
    s.licenseNum = ko.observable();
    s.licenseExpDate = ko.observable();
    s.contactPerson = ko.observable();
    s.contactDesignation = ko.observable();
    s.address = ko.observable();
    s.postalCode = ko.observable();
    s.city = ko.observable();
    s.correspondenceAddress = ko.observable();
    s.correspondencePostalCode = ko.observable();
    s.correspondenceCity = ko.observable();
    s.useBizAddrAsCorrespAddr = ko.observable(false);
    s.isCorrespondenceAddrEnabled = ko.computed(function() {
        return !(s.useBizAddrAsCorrespAddr() == true);
    }, this);
    s.tnc = ko.observable(false);
    s.telNum = ko.observable();
    s.mobileNum = ko.observable();
    s.faxNum = ko.observable();
    s.email = ko.observable();
    s.reconfirmEmail = ko.observable();
    s.website = ko.observable();
    s.mainDestinations = ko.observable();
    s.username =  ko.computed(function() {
        return (this.accountCode()?this.accountCode():'') + "_ADMIN";
    }, this);
    s.usernameErrMsg =  ko.observable("");
    s.selCountry = ko.observable();
    s.orgType = ko.observable();

    s.reset = function(){
        s.id('');
        s.orgName('');
        s.accountCode('');
        s.uen('');
        s.licenseNum('');
        s.licenseExpDate('');
        s.contactPerson('');
        s.contactDesignation('');
        s.address('');
        s.postalCode('');
        s.telNum('');
        s.mobileNum('');
        s.faxNum('');
        s.email('');
        s.reconfirmEmail('');
        s.website('');
        s.usernameErrMsg('');
        s.mainDestinations('');
        s.correspondenceAddress('');
        s.correspondencePostalCode('');
        s.correspondenceCity('');
        s.useBizAddrAsCorrespAddr(false);
        s.city('');
    };

    s.initPartnerProflePreviewModel = function(o){
        s.id(o['id']);
        s.orgName(o['orgName']);
        s.accountCode(o['accountCode']);
        s.uen(o['uen']);
        s.licenseNum(o['licenseNum']);
        s.licenseExpDate(o['licenseExpDate']);
        s.contactPerson(o['contactPerson']);
        s.contactDesignation(o['contactDesignation']);
        s.address(o['address']);
        s.postalCode(o['postalCode']);
        s.telNum(o['telNum']);
        s.mobileNum(o['mobileNum']);
        s.faxNum(o['faxNum']);
        s.email(o['email']);
        s.reconfirmEmail('');
        s.website(o['website']);
        s.usernameErrMsg('');
        s.mainDestinations(o['mainDestinations']);
        var exts = o['extension'];
        if(exts != undefined && exts != null && exts.length > 0){
            var extlen = exts.length;
            for(var i = 0 ; i < extlen ; i++){
                var ext = exts[i];
                if(ext != undefined && ext != null && ext != ''){
                    var name = ext['attrName'];
                    var attrVal = ext['attrValue'];
                    if('correspondenceAddress' == name){
                        s.correspondenceAddress(attrVal);
                    }
                    if('correspondencePostalCode' == name){
                        s.correspondencePostalCode(attrVal);
                    }
                    if('correspondenceCity' == name){
                        s.correspondenceCity(attrVal);
                    }
                    if('useOneAddress' == name){
                        if('true' == ext['attrValue']){
                            s.useBizAddrAsCorrespAddr(true);
                        }else{
                            s.useBizAddrAsCorrespAddr(false);
                        }
                    }
                }
            }
        }
        s.city(o['city']);
    };
};