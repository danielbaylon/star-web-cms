$(document).ready(function() {
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    var $page = {};

    $page.PaProfileLabel = "Profile";
    $page.PaConfigLabel = "Configuration";
    $page.PaSubUserLabel = "Sub User";
    $page.PaLogLabel = "Audit Log";
    $page.PaInventoryLabel = "Inventory";
    $page.PaPinLabel = "Package";
    $page.OfflinePaymentLabel = "Offline Payment";
    $page.loadLabel = "Loading..";
    $page.paDetailTemp = kendo.template($("#paDetailTemp").html());
    $page.paConfigTemp = kendo.template($("#paConfigTemp").html());
    $page.paHistTemp = kendo.template($("#paHistTemp").html());
    $page.paInvenTemp = kendo.template($("#paInvenTemp").html());
    $page.transPopTemp = kendo.template($("#transPopTemp").html());
    $page.pkgPopTemp = kendo.template($("#pkgPopTemp").html());
    $page.offlineTransPopTemp = kendo.template($('#offlineTransPopTemp').html());
    $page.paSubUsersTemp = kendo.template($("#paSubUsersTemp").html());
    $page.paPinTemp = kendo.template($("#paPinTemp").html());
    $page.reasonsTemp = kendo.template($("#reasonsTemp").html());
    $page.paSubUserPopTemp = kendo.template($("#paSubUserPopTemp").html());
    $page.offlinePaymentTemplate = kendo.template($('#offlinePaymentTemplate').html());
    $page.pplsmOfflinePaymentViewDetailsTemplate = kendo.template($('#pplsmOfflinePaymentViewDetailsTemplate').html());
    $page.ppmflgOfflinePaymentApprovalModel = new nvx.OfflinePaymentApprovalModel($page);
    $page.DateFormat = 'dd/MM/yyyy';
    /*$page.baseProdUrl = '/admin/prods/list/excl';
     $page.theProdUrl = $page.baseProdUrl;
     $page.prodDS = new kendo.data.DataSource({
     transport: {read: {url: $page.baseProdUrl, cache: false,type: "POST"}},
     schema: {
     data:function(response) {
     return JSON.parse(response.prodVmsJson);
     },
     total: 'total',
     model: {
     id: 'id',
     fields: {
     id: {type: "number"},
     displayTitle: {type: 'string'},
     prodType: {type: 'string'}
     }
     }
     }, pageSize: 5, serverSorting: true, serverPaging: true
     });*/

    $page.baseProdUrl = nvx.API_PREFIX + '/product-management/get-exclusive-products';
    $page.theProdUrl = $page.baseProdUrl;
    $page.prodDS = new kendo.data.DataSource({
        transport: {
            read: {
                type: 'POST', url: $page.baseProdUrl, cache: false
            }
        },
        schema: {
            data:function(response) {
                if(response['data'] != null && response['data'] != ''){
                    var lst = JSON.parse(response['data']);
                    if(lst != null && lst.length > 0){
                        var len = lst.length;
                        for(var i = 0  ; i < len ; i++){
                            var tmp = lst[i];
                            var id = tmp['id'];
                            if(id != undefined && id != null && id != ''){
                                tmp['id'] = id.trim();
                            }
                            tmp['displayTitle'] = tmp['name'];
                        }
                    }
                    return lst;
                }
                return [];
            },
            total: 'total',
            model: {
                id: 'id',
                fields: {
                    id: {type: "string"},
                    displayTitle: {type: 'string'},
                    prodType: {type: 'string'},
                    tiers : {type: 'string'}
                }
            }
        }, pageSize: 5, serverSorting: true, serverPaging: true
    });
    $page.getProdsJson = function(data){
        var filter = {prodIds:[]};
        $.each(data, function(i, item) {
            if(item.id){
                filter.prodIds.push(item.id);
            }
        });
        return filter;
    };
    $page.refreshProd = function(isfilter){
        var prods = $page.paConfigModel.iprods();
        var filter = $page.getProdsJson(prods);
        filter.prodName = $('#prodNmFilter').val();
        var filterJson = JSON.stringify(filter);
        console.log(filterJson);
         $page.prodDS.options.transport.read.url = $page.baseProdUrl;
         $page.prodDS.options.transport.read.data = {'prodfilterJson':filterJson};
         if(isfilter){
         $page.prodDS.read();
         }
    };
    $page.PartnerModel = function(pa){
        var s = this;
        s.Data = pa;
        s.id = ko.observable();
        s.orgName = ko.observable();
        s.accountCode = ko.observable();
        s.status = ko.observable();
        s.statusLabel = ko.observable();
        s.branchName = ko.observable();
        s.uen = ko.observable();
        s.licenseNum = ko.observable();
        s.licenseExpDate = ko.observable();
        s.contactPerson = ko.observable();
        s.contactDesignation = ko.observable();
        s.address = ko.observable();
        s.postalCode = ko.observable();
        s.city = ko.observable();
        s.correspondenceAddress = ko.observable();
        s.correspondencePostalCode = ko.observable();
        s.correspondenceCity = ko.observable();
        s.telNum = ko.observable();
        s.mobileNum = ko.observable();
        s.faxNum = ko.observable();
        s.email = ko.observable();
        s.website = ko.observable();
        s.languagePreference = ko.observable();
        s.mainDestinations = ko.observable();
        s.username =  ko.computed(function() {
            return (this.accountCode()?this.accountCode():'') + "_ADMIN";
        }, this);
        s.countryName = ko.observable();
        s.orgType = ko.observable();
        s.selOrgType = ko.observable();
        s.paDocs = ko.observableArray([]);
        s.excluProds = ko.observableArray([]);
        s.tierLabel = ko.observable();
        s.remarks  = ko.observable();
        s.revalFeeStr = ko.observable();
        s.accountManager = ko.observable();
        s.revalPeriodMonths = ko.observable();
        s.dailyTransCapStr = ko.observable();

        s.docTypes = ko.observableArray([]);
        var docTypesLength = $page.docTypeJson.length;
        for (var i = 0; i < docTypesLength; i++) {
            var o = new $page.DropdownModel($page.docTypeJson[i].code,$page.docTypeJson[i].label);
            s.docTypes.push(o);
        };

        s.update = function(pa){
            for(var k in pa) s.Data[k]=pa[k];
            s.id(pa.id);
            s.orgName(pa.orgName);
            s.accountCode(pa.accountCode);
            s.status(pa.status);
            s.statusLabel(pa.statusLabel);
            s.branchName(pa.branchName);
            s.uen(pa.uen);
            s.licenseNum(pa.licenseNum);
            s.licenseExpDate(pa.licenseExpDate);
            s.contactPerson(pa.contactPerson);
            s.contactDesignation(pa.contactDesignation);
            s.address(pa.address);
            s.postalCode(pa.postalCode);
            s.city(pa.city);
            var exts = pa['extension'];
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        if('correspondenceAddress' == name){
                            s.correspondenceAddress(attrVal);
                        }
                        if('correspondencePostalCode' == name){
                            s.correspondencePostalCode(attrVal);
                        }
                        if('correspondenceCity' == name){
                            s.correspondenceCity(attrVal);
                        }
                    }
                }
            }

            s.telNum(pa.telNum);
            s.mobileNum(pa.mobileNum);
            s.faxNum(pa.faxNum);
            s.email(pa.email);
            s.website(pa.website);
            s.languagePreference(pa.languagePreference);
            s.mainDestinations(pa.mainDestinations);
            s.countryName(pa.countryName);
            s.orgType(pa.orgTypeName);
            //s.selOrgType(pa.orgTypeCode);
            s.tierLabel(pa.tierLabel);
            s.remarks(pa.remarks);
            s.revalFeeStr(pa.revalFeeStr);
            s.accountManager(pa.accountManagerId);
            s.revalPeriodMonths(pa.revalPeriodMonths);
            s.dailyTransCapStr(pa.dailyTransCapStr);
            s.paTypes = ko.observableArray([]);

            if(!pa.subAccountEnabled){
                $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(2));
            }
            var paTypeLength = $page.paTypeVmsJson.length;
            for (var i = 0; i < paTypeLength; i++) {
                var o = new $page.PaTypeModel($page.paTypeVmsJson[i]);
                s.paTypes.push(o);
            };
            s.paTypeEdit = ko.observable(new $page.EditModel($page.savePaType));
            s.statusEdit = ko.observable(new $page.EditModel($page.savePaStatus));
            s.emailEdit = ko.observable(new $page.EditModel($page.savePaEmail));
            s.uenEdit = ko.observable(new $page.EditModel($page.savePaUEN));
            s.licenseEdit = ko.observable(new $page.EditModel($page.savePaLicense));
            // var adminsLength = pa.adminVmsJson != null && pa.adminVmsJson != '' ? pa.adminVmsJson.length : 0;
            // for (var i = 0; i < adminsLength; i++){
            //     var tem = pa.adminVmsJson[i];
            //     var o = new $page.AdminModel(tem);
            //     s.admins.push(o);
            // };
            if(pa.paDocs){
                var paDocsLength = pa.paDocs.length;
                for (var i = 0; i < paDocsLength; i++) {
                    var tem = pa.paDocs[i];
                    var o = new $page.PaDocModel(tem);
                    s.paDocs.push(o);
                }
            }
            if(pa.excluProds){
                var excluProdsLength = pa.excluProds.length;
                for (var i = 0; i < excluProdsLength; i++) {
                    var tmpObj = pa.excluProds[i];
                    var o = new $page.ProdModel(tmpObj.id,tmpObj.displayTitle,tmpObj.prodType);
                    s.excluProds.push(o);
                }
            }
        }
        s.update(pa);
        s.uploadFile = function(){
            $page.uploadFile();
        };
    };
    $page.PaLogModel = function(){
        var s = this;
        s.paHist = ko.observableArray([]);
        if($page.applogVmsJson){
            var applogVmsJsonLength = $page.applogVmsJson.length;
            for (var i = 0; i < applogVmsJsonLength; i++) {
                var tem = $page.applogVmsJson[i];
                var o = new $page.PaHistModel(tem);
                s.paHist.push(o);
            }
        };
    };
    $page.PaHistModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.createdDateStr = ko.observable(data.createdDateStr);
        s.createdBy = ko.observable(data.createdBy);
        s.description = ko.observable(data.description);
        s.hasHist =  ko.observable(data.hasHist);
        s.remarks =  ko.observable(data.remarks);
        s.status =  ko.observable(data.status);
        s.viewPreviousHist = function(){
            $page.viewPreviousHist(s.id());
        };
        s.viewRemarks = function(){
            $page.viewRemarks(s);
        };
    };
    $page.CapacityGroupModel = function(data){
        var self = this;
        var _id = data['eventAllocationGroupID'];
        var _name = data['name'];
        if (_id != undefined && _id != null && _id != '' && _name != undefined && _name != null && _name != '') {
            self.id = _id;
            self.name = _name;
        } else {
            if (_id != undefined && _id != null && _id != '') {
                self.id = _id;
                self.name = _id;
            }
        }

    };
    $page.CustomerGroupModel = function(data){
        var self = this;
        self.id = data.customerGroupNumber;
        self.name = data.customerGroupName;
    };
    $page.viewRemarks = function(applog){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-reasons?logId='+applog.id(),
            success:  function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    if(!$("#remarksWindow").length){
                        var $div = $('<div />').appendTo('body');
                        $div.attr('id', 'remarksWindow');
                    };
                    var idata = {};
                    idata.reasonVms = JSON.parse(data.data);
                    idata.remarks = applog.remarks();
                    var htmlcontent = $page.reasonsTemp(idata);
                    $page.remarksWindow = $("#remarksWindow").kendoWindow({
                        width: '550px',
                        modal: true,
                        resizable: false,
                        title: "Remarks",
                        actions: ["Close"],
                        viewable : false,
                        deactivate: function() {
                            this.destroy();
                        }
                    }).data('kendoWindow');
                    $page.remarksWindow.content(htmlcontent);
                    $page.remarksWindow.center().open();
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    $page.viewPreviousHist = function(appId){
        //var htmlcontent;
        //nvx.spinner.start();
        $pop.initPopPage(appId);
        /*$.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-previous-partner-history',
            data : {
                appId : appId
            },
            success:  function(data, textStatus, jqXHR)
            {
                
                if(data.success)
                {
                    if(!$("#detailWindow").length){
                        var $div = $('<div />').appendTo('body');
                        $div.attr('id', 'detailWindow');
                    };
                    htmlcontent = data.data;
                    $page.detailWindow = $("#detailWindow").kendoWindow({
                        width: '800px',
                        modal: true,
                        resizable: false,
                        title: "Detail",
                        actions: ["Close"],
                        viewable : false,
                        deactivate: function() {
                            this.destroy();
                        }
                    }).data('kendoWindow');
                    $page.detailWindow.content(htmlcontent);
                    $page.detailWindow.center().open();
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });*/


    };
    $page.loadPaHist = function(){

        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-partner-history',
            data : {
                id : $page.paVmJson.id
            },
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    $page.applogVmsJson = JSON.parse(data.data);
                    $page.paLog =new $page.PaLogModel();
                    ko.applyBindings($page.paLog,$('#paHistDv')[0]);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.uploadFile = function(){
        var file1 = $('[name="uploadfile"]');
        if(!file1.val()){
            alert("Please select an attachment.");
            return;
        }
        if(file1&&file1.val()){
            if(!isAllowedFileType(file1.val())){
                alert("File Type is not allowed.");
                return;
            }
        }

        nvx.spinner.start();
        if(!$('[name="tempIframe"]').length){
            var iframe=document.createElement('iframe');
            $(iframe).attr('name', 'tempIframe');
            $(iframe).attr('id', 'tempIframe');
            $(iframe).hide();
            $('body').append(iframe);
        }
        $('[name="tempIframe"]').load(function() {
            var content = $('[name="tempIframe"]').contents().find("html").text();
            try {
                var data = JSON.parse(content);
                if(data.success){
                    alert("Upload Successfully.");
                    var paDocVmJson = data.data;
                    var o = new $page.PaDocModel(paDocVmJson);
                    $page.paModel.paDocs.push(o);
                    $('[name="uploadfile"]').closest('td').html('<input id="uploadfile" name="uploadfile" type="file" class="input-file">');
                    //TODO $('#uploadfile').kendoUpload({multiple:false});
                }else{
                    alert(data.message);
                }
            }catch(err) {
                console.log(err);
                alert(ERROR_MSG);
            }
            nvx.spinner.stop();
            $('[name="tempIframe"]').remove();
        });

        var frm = document.getElementById('docForm');
        var paFT = $("#pafileType").val();
        var paIdParam = addFormParam("id",$page.paModel.id());
        //var paFileType = addFormParam("paFileType",paFT);
        var paFileName = addFormParam("fileName",file1.val());
        frm.appendChild(paIdParam);
        //frm.appendChild(paFileType);
        frm.appendChild(paFileName);
        frm.target = 'tempIframe';
        frm.action = nvx.API_PREFIX + '/partner-management/fileUpload';
        frm.submit();
        frm.removeChild(paIdParam);

    };
    function addFormParam(key , value){
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value",value);
        return hiddenField;
    };
    function isAllowedFileType(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'doc':
            case 'docx':
            case 'rtf':
            case 'txt':
            case 'jpeg':
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'tiff':
            case 'pdf':
                //etc
                return true;
        }
        return false;
    };
    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    };
    $page.savePaStatus = function(){
        var idata = {};
        idata.id = $page.paModel.id();
        idata.status = $page.paModel.status();
        var paVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/update/status',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paModel.statusLabel($page.paModel.status());
                    $page.paModel.statusEdit().toggleEdit();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.savePaEmail = function(){
        var idata = {};
        idata.id = $page.paModel.id();
        idata.email = $page.paModel.email().trim();
        if(idata.email == $page.paModel.Data.email){
            alert("Please update the email first.");
            return;
        }
        var paVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/update/email',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paModel.emailEdit().toggleEdit();
                    $page.paModel.Data.email = $page.paModel.email().trim();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.savePaUEN = function(){
        var idata = {};
        idata.id = $page.paModel.id();
        idata.uen = $page.paModel.uen().trim();
        if(idata.uen == $page.paModel.Data.uen){
            alert("Please update the UEN first.");
            return;
        }
        var paVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/update/UEN',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paModel.uenEdit().toggleEdit();
                    $page.paModel.Data.uen = $page.paModel.uen().trim();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.savePaLicense = function(){
        var idata = {};
        idata.id = $page.paModel.id();
        idata.licenseNum = $page.paModel.licenseNum().trim();
        if(idata.licenseNum == $page.paModel.Data.licenseNum){
            alert("Please update the Travel Agent License No first.");
            return;
        }
        var paVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/update/license',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paModel.licenseEdit().toggleEdit();
                    $page.paModel.Data.licenseNum = $page.paModel.licenseNum().trim();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.savePaType = function(){
        var idata = {};
        idata.id = $page.paModel.id();
        idata.orgTypeCode = $page.paModel.selOrgType().id();
        var paVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/update/OrgType',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paModel.orgType($page.paModel.selOrgType().label());
                    $page.paModel.paTypeEdit().toggleEdit();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.ProdModel = function(id,displayTitle,prodType){
        var self = this;
        self.showup = ko.observable(true);
        self.id = id;
        self.displayTitle = displayTitle;
        self.prodType = prodType;
    };
    $page.PaDocModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.fileName = ko.observable(data.fileName);
        s.fileTypeLabel = ko.observable(data.fileTypeLabel);
        s.url = ko.observable(nvx.API_PREFIX + '/partner-management/attachment/downloadFile?id='+data['id']);
    };
    $page.PaTypeModel = function(data){
        var s = this;
        s.id = ko.observable('');
        s.label = ko.observable('');
        s.update = function(data){
            s.id(data.id);
            s.label(data.label);
        };
        s.update(data);
    };
    $page.EditModel = function(func){
        var s = this;
        s.editAble = ko.observable(false);
        s.toggleEdit = function() {
            var editAble = s.editAble();
            s.editAble(!editAble);
            if($page.paConfigModel!=undefined) {
                if ($page.paConfigModel.configEdit().editAble()) {
                    var kpicker = $('#partnerWoTReservationValidUntil').data("kendoDatePicker");
                    kpicker.enable(true);
                } else {
                    var kpicker = $('#partnerWoTReservationValidUntil').data("kendoDatePicker");
                    kpicker.enable(false);
                }
            }
        };
        s.save = function() {
            func();
        };

        s.addexcl = function () {
            var partner = {};
            partner.id = $page.paModel.id();
            partner.accountManagerId = $page.paConfigModel.accountManagerId();
            partner.subAccountEnabled = $page.paConfigModel.selSubAccountEnabled();
            partner.offlinePaymentEnabled = $page.paConfigModel.selOfflinePaymentEnabled();
            partner.revalPeriodMonths = $page.paConfigModel.newRevalPeriodMonths();
            partner.revalFeeItemId = $page.paConfigModel.revalFeeItemId();
            partner.dailyTransCapStr = $page.paConfigModel.newDailyTransCapStr();
            partner.tierId = $page.paConfigModel.selTier();
            partner.remarks = $page.paConfigModel.remarks()
            partner.excluProdIds = [];
            ko.utils.arrayForEach($page.paConfigModel.iprods(), function(prod) {
                partner.excluProdIds.push(prod.id);
            });
            partner.distributionMapping = [];
            var total = 0;
            ko.utils.arrayForEach($page.paConfigModel.distributionModels(), function(tmpMap) {
                var tmpMapJs = {percentage:tmpMap.percentage(),countryId:tmpMap.countryId(),adminId:tmpMap.adminId()};
                partner.distributionMapping.push(tmpMapJs);
                total = total + Number(tmpMap.percentage());
            });
            var paVmJson = JSON.stringify(partner);
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/add-excl',
                data: {paVmJson : paVmJson},
                success:  function(ajxResp, textStatus, jqXHR)
                {
                    if (ajxResp.success) {
                       alert("added EXCl");
                    } else {
                        alert(data.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    };
    $page.AdminModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.username = ko.observable(data.username);
    };
    $page.RevalFeeModel = function(data){
        var s = this;
        s.id = ko.observable(data.itemId);
        s.priceStr = ko.observable(data.priceStr);
    }
    $page.TierModel = function(data){
        var s = this;
        s.id = ko.observable(data.priceGroupId);
        s.name = ko.observable(data.name);
    }
    $page.ProdModel = function(id,displayTitle,prodType){
        var self = this;
        self.showup = ko.observable(true);
        self.id = id;
        self.displayTitle = displayTitle;
        self.prodType = prodType;
    };
    $page.CountryModel = function(data){
        var self = this;
        //Knockout 2.3.0 value can only be string
        //self.id = ko.observable(data.id.toString());
        self.ctyCode = ko.observable(data.ctyCode);
        self.ctyName = ko.observable(data.ctyName);
    };
    $page.DistributionModel = function(data){
        var s = this;
        s.percentage = ko.observable(data.percentage);
        s.countryId = ko.observable(data.countryId);
        s.countryNm = ko.observable(data.countryNm);
        s.adminId = ko.observable(data.adminId);
        s.adminNm = ko.observable(data.adminNm);
    };
    $page.TrueFalseModel = function(value,display){
        var self = this;
        self.optionValue = ko.observable(value);
        self.optionText = ko.observable(display);
    };
    $page.DropdownModel = function(value,display){
        var self = this;
        self.optionValue = ko.observable(value);
        self.optionText = ko.observable(display);
    };
    $page.PaDetailModel = function(pa){
        var s = this;
        s.Data = pa;
        s.isPending = ko.observable($page.isPending);
        s.accountManagerId = ko.observable();
        s.accountManager = ko.observable();
        s.subAccountEnabled = ko.observable();
        s.onlinePaymentEnabled = ko.observable();
        s.offlinePaymentEnabled = ko.observable();
        s.depoistEnabled = ko.observable();
        s.wotReservationEnabled = ko.observable();
        s.admins = ko.observableArray([]);
        s.revalFeeStr = ko.observable();
        s.revalFees = ko.observableArray([]);
        s.revalPeriodMonths = ko.observable();
        s.customerGroups = ko.observableArray([]);
        s.customerGroupId = ko.observable();
        s.capacityGroups = ko.observableArray([]);
        s.capacityGroupId = ko.observable();
        s.revalFeeItemId = ko.observable();
        s.dailyTransCapStr = ko.observable();
        s.remarks = ko.observable();
        s.wotReservationCap = ko.observable();
        s.wotReservationValidUntil = ko.observable();
        s.tiers = ko.observableArray([]);
        s.tierLabel = ko.observable();
        s.tierId = ko.observable();
        s.iprods = ko.observableArray([]);
        s.excluProds = ko.observableArray([]);
        s.iprods = ko.observableArray([]);
        s.prodNmFilter  = ko.observable();
        s.selAccManager = ko.observable();
        s.trueFalseOptions = ko.observableArray([
            new $page.TrueFalseModel(true,'Enable'),
            new $page.TrueFalseModel(false,'Disable')
        ]);
        s.selSubAccountEnabled = ko.observable();
        s.selOfflinePaymentEnabled = ko.observable();
        s.selCustomerGroupId = ko.observable();
        s.selCapacityGroupId = ko.observable();
        s.selWotReservationEnabled = ko.observable();
        s.newRevalPeriodMonths = ko.observable();

        s.tktRevalPeriods = ko.observableArray([]);
        var tktRevalP = new Number($page.ticketRevalidatePeriod);
        for(var i=0; i<= tktRevalP;i++){
            var tmpObj = new $page.DropdownModel(i,i);
            s.tktRevalPeriods.push(tmpObj);
        }
        s.revalFeeItem = ko.observable();
        s.newDailyTransCapStr = ko.observable();
        s.selTier  = ko.observable();
        s.configEdit = ko.observable(new $page.EditModel($page.submitPa));

        s.distributeCtys = ko.observableArray([]);
        s.distributeMgrs = ko.observableArray([]);
        s.selCty = ko.observable();
        s.selMgr = ko.observable();
        s.percentage = ko.observable(0);
        s.distributionModels = ko.observableArray([]);
        s.oriDistributionModels = ko.observableArray([]);

        var ctyVmsLength = $page.ctyVmsJson.length;
        for (var i = 0; i < ctyVmsLength; i++) {
            var o = new $page.CountryModel($page.ctyVmsJson[i]);
            s.distributeCtys.push(o);
        }

        var custGrpVmsLength = $page.customerGroupJson.length;
        for(var i = 0 ; i < custGrpVmsLength ; i++){
            var o = new $page.CustomerGroupModel($page.customerGroupJson[i]);
            s.customerGroups.push(o);
        }

        // var capacityGrpVmsLength = $page.capacityGroupJson.length;
        // for(var i = 0 ; i < capacityGrpVmsLength ; i++){
        //     var o = new $page.CapacityGroupModel($page.capacityGroupJson[i]);
        //     s.capacityGroups.push(o);
        // }

        var capacityGrpVmsLength = $page.capacityGroupJson.length;
        for(var i = 0 ; i < capacityGrpVmsLength ; i++){
            var _o = $page.capacityGroupJson[i];
            if(_o != undefined && _o != null && _o != ''){
                var _id = _o['eventAllocationGroupID'];
                var _name = _o['name'];
                if(_id != undefined && _id != null && _id != '' && _name != undefined && _name != null && _name != ''){
                    var o = new $page.CapacityGroupModel($page.capacityGroupJson[i]);
                    s.capacityGroups.push(o);
                }else{
                    if(_id != undefined && _id != null && _id != ''){
                        var o = new $page.CapacityGroupModel($page.capacityGroupJson[i]);
                        s.capacityGroups.push(o);
                    }
                }
            }
        }
        s.saveDistribution = function(){
            var idata = {percentage:s.percentage(),
                countryId:s.selCty().ctyCode(),
                countryNm:s.selCty().ctyName(),
                adminId:s.selMgr().id(),
                adminNm:s.selMgr().username()};
            var distribution = new $page.DistributionModel(idata);
            var total = 0;
            var duplicateCty = false;
            ko.utils.arrayForEach($page.paConfigModel.distributionModels(), function(tmpMap) {
                total = total + Number(tmpMap.percentage());
                if(s.selCty().ctyCode() == tmpMap.countryId()){
                    duplicateCty = true;
                }
            });
            if(duplicateCty){
                alert("Same country already added.");
                return;
            }
            var newObj = Number(s.percentage());
            if((total+ newObj)>100){
                alert("Can not add more than 100%.");
                return;
            }else{
                s.distributionModels.push(distribution);
                s.percentage(100 - total - newObj);
            }
        };
        s.rmDistribution = function() {
            s.distributionModels.remove(this);
            var total = 0;
            ko.utils.arrayForEach(s.distributionModels(), function(tmpMap) {
                total = total + Number(tmpMap.percentage());
            });
            s.percentage(100 - total);
        };
        s.remarks = ko.observable();
        s.update = function(pa){
            for(var k in pa) s.Data[k]=pa[k];
            s.accountManagerId(pa.accountManagerId);
            s.accountManager(pa.accountManager);
            var exts = pa['extension'];
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        var attrValLabel = ext['attrValueLabel'];
                        if('onlinePaymentEnabled' == name){
                            s.onlinePaymentEnabled(attrVal == 'true');
                        }
                        if('offlinePaymentEnabled' == name){
                            s.offlinePaymentEnabled(attrVal == 'true');
                        }
                        if('depoistEnabled' == name){
                            s.depoistEnabled(attrVal == 'true');
                        }
                        if('wotReservationEnabled' == name){
                            s.wotReservationEnabled(attrVal == 'true');
                        }
                        if('wotReservationCap' == name){
                            s.wotReservationCap(attrVal);
                        }
                        if('wotReservationValidUntil' == name){
                            s.wotReservationValidUntil(attrVal);
                        }
                        if('customerGroup' == name){
                            s.customerGroupId(attrValLabel);
                            s.selCustomerGroupId(attrVal);
                        }
                        if('capacityGroupId' == name){
                            s.capacityGroupId(attrValLabel);
                            s.selCapacityGroupId(attrVal);
                        }
                    }
                }
            }
            s.revalFeeStr(pa.revalFeeStr);
            s.tierLabel(pa.tierLabel);
            s.subAccountEnabled(pa.subAccountEnabled);
            //s.offlinePaymentEnabled(pa.offlinePaymentEnabled);
            s.selSubAccountEnabled(pa.subAccountEnabled);
            s.selOfflinePaymentEnabled(pa.offlinePaymentEnabled);
            s.selWotReservationEnabled(s.wotReservationEnabled());
            s.revalPeriodMonths(pa.revalPeriodMonths);
            s.revalFeeItemId(pa.revalFeeItemId);
            s.newRevalPeriodMonths(pa.revalPeriodMonths);
            s.dailyTransCapStr(pa.dailyTransCapStr);
            s.newDailyTransCapStr(pa.dailyTransCapStr);
            s.tierId(pa.tierId);
            s.selTier(pa.tierId);
            if(pa.excluProds){
                var excluProdsLength = pa.excluProds.length;
                for (var i = 0; i < excluProdsLength; i++) {
                    var tmpObj = pa.excluProds[i];
                    var o = new $page.ProdModel(tmpObj.id,tmpObj.name,tmpObj.prodType);
                    s.excluProds.push(o);
                    s.iprods.push(o);
                }
            }
            if(pa.adminVmsJson){
                var adminsLength = pa.adminVmsJson.length;
                for (var i = 0; i < adminsLength; i++){
                    var tem = pa.adminVmsJson[i];
                    var o = new $page.AdminModel(tem);
                    s.admins.push(o);
                };
            }
            if(pa.revalItemVmsJson){
                var revalLength = pa.revalItemVmsJson.length;
                for (var i = 0; i < revalLength; i++){
                    var tem = pa.revalItemVmsJson[i];
                    var o = new $page.RevalFeeModel(tem);
                    s.revalFees.push(o);
                };
            }
            if(pa.tierVmsJson){
                var tierLength = pa.tierVmsJson.length;
                for (var i = 0; i < tierLength; i++) {
                    var tem = pa.tierVmsJson[i];
                    var o = new $page.TierModel(tem);
                    s.tiers.push(o);
                };
            }
            if(pa.prodVms){
                var prodLength = pa.prodVms.length;
                for (var i = 0; i < prodLength; i++) {
                    if(prodVms[i].id&&prodVms[i].displayTitle){
                        var tmpObj = prodVms[i];
                        var o = new $page.ProdModel(tmpObj.id,tmpObj.displayTitle,tmpObj.prodType);
                        $page.tierVm.iprods.push(o);
                    }
                }
            }

            if(pa.distributionMapping){
                var pdMappingLength = pa.distributionMapping.length;
                var total = 0;
                for (var i = 0; i < pdMappingLength; i++) {
                    if(pa.distributionMapping[i].percentage){
                        var idata = {percentage:pa.distributionMapping[i].percentage,
                            countryId:pa.distributionMapping[i].countryId,
                            countryNm:pa.distributionMapping[i].countryNm,
                            adminId:pa.distributionMapping[i].adminId,
                            adminNm:pa.distributionMapping[i].adminNm};
                        var distribution = new $page.DistributionModel(idata);
                        s.distributionModels.push(distribution);
                        s.oriDistributionModels.push(distribution);
                        total = total + Number(pa.distributionMapping[i].percentage);
                    }
                }
                s.percentage(100 - total);
            }
        }
        s.update(pa);
        s.ctyChanged = function (obj, event) {
            if(s.selCty()){
                s.distributeMgrs.removeAll();
                if(s.selCty().loaded){
                    if(s.selCty().salesJson){
                        var adminsLength = s.selCty().salesJson.length;
                        for (var i = 0; i < adminsLength; i++){
                            var tem = s.selCty().salesJson[i];
                            var o = new $page.AdminModel(tem);
                            s.distributeMgrs.push(o);
                        };
                    }
                }else{
                    $page.getSalesAdmin(s.selCty());
                }
            }
        };
        s.back = function(){
            $('#paBasicDv').show();
            $('#paDetailDv').hide();
        };
        s.filterProd = function(){
            var prodNm = s.prodNmFilter();
            ko.utils.arrayForEach(s.iprods(), function(item) {
                var displayTitle = item.displayTitle;
                if(displayTitle.indexOf(prodNm)< 0&&prodNm != ''){
                    item.showup(false);
                }else{
                    item.showup(true);
                }
            });
        };
        s.rmAllProd = function() {
            s.iprods.removeAll();
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.addAllProd = function() {
            $page.addAllProd();
        };
        s.addProd = function(id) {
            var obj = $('a[elementType="pending-adding-products"][prodId="'+id+'"][prodType][prodName]');
            var name = $(obj).attr('prodName');
            var type = $(obj).attr('prodType');
            var o = new $page.ProdModel(id,decodeURIComponent(name),type);
            s.iprods.push(o);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.rmProd = function() {
            s.iprods.remove(this);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.submitVerify = function(){
            $page.submitPa();
        };
    };
    $page.getSalesAdmin= function(cty){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-sales-admin?ctyId='+cty.ctyCode(),
            data: {},
            success:  function(ajxResp, textStatus, jqXHR)
            {
                if (ajxResp.success) {
                    if(ajxResp['data'] != null && ajxResp['data'] != '') {
                        var salesJson = JSON.parse(ajxResp['data']);
                        cty.salesJson = salesJson;
                        cty.loaded = true;
                        if (cty.salesJson) {
                            var adminsLength = cty.salesJson.length;
                            for (var i = 0; i < adminsLength; i++) {
                                var tem = cty.salesJson[i];
                                var o = new $page.AdminModel(tem);
                                $page.paConfigModel.distributeMgrs.push(o);
                            }
                            ;
                        }
                    }
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.addAllProd= function(){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: $page.baseProdUrl,
            data: {},
            success:  function(ajxResp, textStatus, jqXHR)
            {
                if (ajxResp.success) {
                    if(ajxResp['data'] != null && ajxResp['data'] != ''){
                        var lst = JSON.parse(ajxResp['data']);
                        if(lst != null && lst.length > 0){
                            var len = lst.length;
                            $page.paConfigModel.iprods.removeAll();
                            for(var i = 0  ; i < len ; i++){
                                var tmp = lst[i];
                                var id = tmp['id'];
                                if(id != undefined && id != null && id != ''){
                                    tmp['id'] = id.trim();
                                }
                                tmp['displayTitle'] = tmp['name'];

                                var o = new $page.ProdModel(tmp.id,tmp.displayTitle,tmp.prodType);
                                $page.paConfigModel.iprods.push(o);
                            }
                        }
                    }
                    alert(ajxResp.total+"  products will be assigned to this partner.");
                    $page.refreshProd();
                    $page.prodDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    $page.InventoryFilterModel = function(){
        var s = this;
        s.startDate = ko.observable();
        s.endDate = ko.observable();
        s.productName = ko.observable();
        s.availableOnly = ko.observable(false);
        s.reset = function(){
            s.startDate('');
            s.endDate('');
            s.productName('');
            s.availableOnly(false);
        }
        s.refreshUrl = function(){
            s.theUrl = $page.ITEM_BASE_URL;
            if(s.startDate()){
                s.theUrl = s.theUrl + '&startDateStr=' + s.startDate();
            }
            if(s.endDate()){
                s.theUrl = s.theUrl + '&endDateStr=' + s.endDate();
            }
            if(s.productName()){
                s.theUrl = s.theUrl + '&productName=' + s.productName();
            }
            if(s.availableOnly()){
                s.theUrl = s.theUrl + '&availableOnly=on';
            }
            $page.itemsDs.options.transport.read.url = s.theUrl;
            $page.itemsDs.page(1);
        }
    };
    /*TODO
     $page.itemsDs = new kendo.data.DataSource({
     transport: {read: {url: $page.theItemUrl, cache: false}},
     schema: {
     data: function(response) {
     return JSON.parse(response.transItemVmsJson);
     },
     total: 'total',
     model: {
     id: 'id',
     fields: {
     transId: {type: "number"},
     receiptNum: {type: 'string'},
     displayName: {type: 'string'},
     ticketType: {type: 'string'},
     createdDateStr: {type: 'string'},
     validityEndDateStr: {type: 'string'},
     baseQty: {type: "number"},
     qty: {type: "number"},
     subTotal: {type: "number"},
     status: {type: 'string'},
     username: {type: 'string'},
     ableToPkg: {type: 'boolean'},
     topupItems: {}
     }
     }
     }, pageSize: 10, serverSorting: true, serverPaging: true,
     });*/
    $page.initInventoryGrid = function(){
        $page.inventoryGrid = $("#inventoryGrid").kendoGrid({
            dataSource: $page.itemsDs,
            detailTemplate: '<div class="sub-detail"></div>',
            detailInit: detailInit,
            dataBound: dataBound,
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
            },{
                field: "receiptNumber",
                title: 'Receipt Number',
                template: '<a  class="link-text"  href="javascript:nvxpage.loadTrans(#=transId#);"  >#=receiptNumber#</a>',
                width: 120
            }, {
                field: "name",
                title: "Product",
                width: 200
            },{
                field: "type",
                title: "Ticket<BR>Type",
                width: 50
            },{
                field: "validityStartDateStr",
                title: "Validity Start<BR>Date",
                width: 90
            }, {
                field: "validityEndDateStr",
                title: "Validity End<BR>Date",
                width: 90
            },{
                field: "qty",
                title: "Original<BR>Quantity",
                width: 60
            },{
                field: "unpackagedQty",
                title: "Remaining<BR>Quantity",
                width: 70
            },{
                field: "subtotal",
                title: "Amt(S$)",
                width: 60
            },{
                field: "status",
                title: "Status",
                width: 140
            },{
                field: "username",
                title: "Purchased By"
            }],
            sortable: true, pageable: true
        });


        function dataBound(e){
            var currentPage = $page.itemsDs.page();
            var pageSize = $page.itemsDs.pageSize();
            if(this.dataSource.total() >0){
                $(".export-to-excel-section").show();
            }
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
            });
            this.expandRow(this.tbody.find("tr.k-master-row"));
            //$('.k-hierarchy-cell').html("&nbsp;");
            $('.k-detail-row').hide();
            $('.k-detail-row').has(".sub-detail.k-grid.k-widget").show();
        };

        function detailInit(e) {
            var detailRow = e.detailRow;
            var subData = e.data.topupItems;
            if(subData.length == 0){
                return;
            }
            detailRow.find(".sub-detail").kendoGrid({
                dataSource: {
                    data: subData,
                    schema: {
                        model: {
                            fields: {
                                displayName: {type: 'string'},
                                ticketType: {type: 'string'},
                                validateStartDateStr: {type: 'string'},
                                validityEndDateStr: {type: 'string'},
                                qty: {type: "number"},
                                unpackagedQty: {type: "number"},
                                subTotal: {type: "number"},
                                status: {type: 'string'},
                                username: {type: 'string'}
                            }
                        }
                    }
                },
                columns: [{
                    field: "displayName",
                    template: '<i class="fa fa-plus-circle"></i>#=displayName#',
                    title: "Top Up",
                    width: 200
                },{
                    field: "ticketType",
                    title: "Ticket<BR>Type",
                    width: 50
                },{
                    field: "validateStartDateStr",
                    title: "Validity Start<BR>Date",
                    width: 90
                }, {
                    field: "validityEndDateStr",
                    title: "Validity End<BR>Date",
                    width: 90
                },{
                    field: "qty",
                    title: "Original<BR>Quantity",
                    width: 60
                },{
                    field: "unpackagedQty",
                    title: "Remaining<BR>Quantity",
                    width: 70
                },{
                    field: "subTotal",
                    title: "Amt(S$)",
                    width: 60
                },{
                    field: "status",
                    title: "Status",
                    width: 140
                },{
                    field: "username",
                    title: "Purchased By"
                }]
            });
        };
        $(".k-grid-header").css("padding-right", "0px");
        $('#btnExportToExcel').click(function(event) {
            var exporturl = '/admin/partner/transItems/download?paId='+$page.paVmJson.id;
            if($page.invenFilterModel.startDate()){
                exporturl = exporturl + '&startDateStr=' + $page.invenFilterModel.startDate();
            }
            if($page.invenFilterModel.endDate()){
                exporturl = exporturl + '&endDateStr=' + $page.invenFilterModel.endDate();
            }
            if($page.invenFilterModel.productName()){
                exporturl = exporturl + '&productName=' + $page.invenFilterModel.productName();
            }
            if($page.invenFilterModel.availableOnly()){
                exporturl = exporturl + '&availableOnly=on';
            }
            window.location.href = exporturl;
        });
    };

    $page.PkgFilterModel = function(){
        var s = this;
        s.theUrl = $page.PKG_BASE_URL;
        s.startDate = ko.observable();
        s.endDate = ko.observable();
        s.pkgName = ko.observable();
        s.ticketMedia = ko.observable();
        s.selPinStatus = ko.observable();
        s.reset = function(){
            s.startDate('');
            s.endDate('');
            s.pkgName('');
            s.selPinStatus('');
            s.ticketMedia('');
        }
        s.refreshUrl = function(){
            s.theUrl =  $page.PKG_BASE_URL;
            if(s.startDate()){
                s.theUrl = s.theUrl + '&startDateStr=' + s.startDate();
            }
            if(s.endDate()){
                s.theUrl = s.theUrl + '&endDateStr=' + s.endDate();
            }
            if(s.ticketMedia()) {
                s.theUrl = s.theUrl + '&ticketMedia=' + s.ticketMedia();
            }
            if(s.pkgName()){
                s.theUrl = s.theUrl + '&pkgName=' + s.pkgName();
            }
            if(s.selPinStatus()){
                s.theUrl = s.theUrl + '&status=' + s.selPinStatus();
            }
            $page.pkgDs.options.transport.read.url = s.theUrl;
            $page.pkgDs.page(1);
        }
        $('#showHideSubGrid').click(function(){
            var grid = $("#pkgGrid").data("kendoGrid");
            if($('section.result-view tr.k-master-row td.k-hierarchy-cell a.k-minus').size() > 0){
                grid.collapseRow(grid.tbody.find("tr.k-master-row"));
            }else{
                grid.expandRow(grid.tbody.find("tr.k-master-row"));
            }
        });
    };

    $page.attachPackageItemDetails = function(data, packageId){
        var divId = 'packageItemDetailsOf'+packageId;
        if($('#'+divId).size() > 0){
            $("#"+divId).kendoGrid({
                dataSource: { data : data },
                columns: [
                    {
                        field: "displayName",
                        title: "Product",
                        width: 100
                    },
                    {
                        field: "qty",
                        title: "Quantity",
                        width: 60
                    },
                    {
                        field: "qtyRedeemed",
                        title: "Quantity Redeemed",
                        width: 60
                    },
                    {
                        field: "receiptNum",
                        title: "Receipt Number",
                        width: 100
                    },
                ]
            });
        }
    };

    $page.loadingPackageItemDetails = function(){
        $('div.packageItemDetails[packageId]').each(function(i){
            var packageId = $(this).attr('packageId');
            if(packageId != undefined && packageId != null && packageId != ''){
                // begin.......
                $.ajax({
                    type: 'POST',
                    url: nvx.API_PREFIX + '/trans-query/get-package-item-list?packageId='+packageId,
                    beforeSend : function(){
                        nvx.spinner.start();
                    },
                    success:  function(data, textStatus, jqXHR) {
                        if(data.success){
                            console.log('loading package item details success for package id '+packageId);
                            $page.attachPackageItemDetails(data['data'], packageId);
                        }else{
                            if(data.message != null && data.message != ''){
                                console.log('loading package item details failed for package id '+packageId);
                            }
                        }
                    },
                    error: function(jqXHR, textStatus) {
                        console.log('loading package item details failed for package id '+packageId);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
                // end.........
            }
        });
    };

    $page.initPkgGrid = function(){
        $page.pkgGrid = $("#pkgGrid").kendoGrid({
            dataSource: $page.pkgDs,
            dataBound: pkgDataBound,
            detailInit: function(e){
                var container = '<div class="packageItemDetails" packageId="'+(e.data.id)+'"><div id="packageItemDetailsOf'+(e.data.id)+'"></div></div>';
                $(container).appendTo(e.detailCell);
            },
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
            },{
                field: "name",
                title: 'Package Name',
                template: '<a href="javascript:nvxpage.viewPackage(#=id#,&quot;#=name#&quot;);" >#=name#</a>',
                width: 140
            },{
                field: "description",
                title: "Description",
                width: 120
            },{
                field: "pkgTktMedia",
                title: "Ticket Media",
                width: 90
            }, {
                field: "qty",
                title: "Qty",
                width: 40
            }, {
                field: "ticketGeneratedDateStr",
                title: "Ticket<BR>Generated<BR>Date Time",
                width: 80
            },{
                field: "expiryDateStr",
                title: "Expiry<BR>Date",
                width: 80
            },{
                field: "status",
                title: "PIN<BR>Redemption<BR>Status",
                width: 80
            },{
                field: "lastRedemptionDateStr",
                title: "Last<BR>Redemption<BR>Date Time",
                width: 90
            },{
                field: "username",
                title: "Action By",
                width: 80
            },{
                title: "&nbsp;",
                sortable: false,
                template: '<a  #if( pkgTktMedia != "Pincode" ) {# style="display:none" #} else if(canRedeem) {#class="btn btn-secondary btn-block"'+
                '  onclick="javascript:nvxpage.checkRedeem(this,#=id#)"# } else '+
                '{#class="btn btn-block btn-disabled" '
                +' onclick="javascript:nvxpage.checkRedeem(this,#=id#,\'#=canRedeemDtStr#\')"  #}#  >'
                +'Get Redemption<BR>Status</a>',

                width: 120
            }],
            sortable: true, pageable: true
        });
        function pkgDataBound(e){
            var grid = $("#pkgGrid").data("kendoGrid");
            var currentPage = $page.pkgDs.page();
            var pageSize = $page.pkgDs.pageSize();
            var rows = this.items();
            $(rows).each(function () {
                var index = $(this).index() + 1+(currentPage-1)*pageSize;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
            });
            this.expandRow(this.tbody.find("tr.k-master-row"));
            $page.loadingPackageItemDetails();
        }
    };
    $page.viewPackage = function(pkgId,pkgName){
        var htmlcontent;
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-package?pkgId='+pkgId,
            success:  function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    $page.openPackagePop(data.data,pkgName);
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };

    $page.openPackagePop = function(data,pkgName){

        //$("#downloadTicket").attr("href",  nvx.API_PREFIX + "/partner-management/download-ticket?pkgId=" + data.id);
        var htmlcontent = $page.pkgPopTemp(data);
        if(!$("#pkgPopModal").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'pkgPopModal');
        }
        var pkgPopup = $("#pkgPopModal").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            // title: "View Package",
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        pkgPopup.title(pkgName);
        pkgPopup.content(htmlcontent);
        $page.mmpackageModel = new $page.MMPackageModel();
        $page.mmpackageModel.init(data);
        ko.applyBindings($page.mmpackageModel , $('#pkgPopModal')[0]);
        pkgPopup.center().open();
        /*var htmlcontent =$page.transPopTemp(data);
        if(!$("#transPopWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'transPopWindow');
        }
        var tempWindow = $("#transPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Detail',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.transModel =new $page.TransModel();
        $page.transModel.init(data);
        ko.applyBindings($page.transModel,$('#transModal')[0]);
        tempWindow.center().open();*/
    };



    $page.MMPackageModel = function(base) {
        var s = this;

        s.bookingRefId = ko.observable();
        s.description = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.qtyRedeemed = ko.observable();
        s.qtyRedeemedStr = ko.observable();
        s.pkgTktMedia = ko.observable();
        s.ticketGeneratedDateStr = ko.observable();
        s.expiryDateStr = ko.observable();
        s.lastRedemptionDateStr = ko.observable();
        s.pinCode = ko.observable();
        s.transItems = ko.observableArray([]);
        s.url = ko.observable();

        s.init = function(data) {
            s.bookingRefId(data.id);
            s.description(data.description);
            s.status(data.status);
            s.qty(data.pinCodeQty);
            s.qtyRedeemed(data.qtyRedeemed);
            s.qtyRedeemedStr(data.qtyRedeemedStr);
            s.pkgTktMedia(data.pkgTktMedia);
            s.pinCode(data.pinCode);
            s.ticketGeneratedDateStr(data.ticketGeneratedDateStr);
            s.expiryDateStr(data.expiryDateStr);
            s.lastRedemptionDateStr(data.lastRedemptionDateStr);
            if(data.pkgTktMedia == 'ETicket') {
                s.url(nvx.API_PREFIX + "/partner-management/download-ticket?pkgId=" + data.id);
            }
            else if(data.pkgTktMedia == 'ExcelFile') {
                s.url(nvx.API_PREFIX + "/partner-management/download-excel-file?pkgId=" + data.id);
            }

            s.transItems([]);

            $.each(data.transItems, function(idx, obj) {
                s.transItems.push(new nvx.MMPackageItem(s, obj));
            });
        }

        s.sendEmailExcel = function() {

            var result = confirm("Are you going to send the Excel File email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/email-excel',
                data : {
                    pkgId : s.bookingRefId()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.data);
                    }
                    else
                    {
                        alert(data.data);
                        console.log('ERRORS: ' + data.data);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });

        }

        s.sendEmailTicket = function() {
           
            var result = confirm("Are you going to send the E-Ticket email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/email-eticket',
                data : {
                    pkgId : s.bookingRefId()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.data);
                    }
                    else
                    {
                        alert(data.data);
                        console.log('ERRORS: ' + data.data);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });

        }

        s.sendEmailPincode = function() {

            var result = confirm("Are you going to send the PIN code email?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/email-pincode',
                data : {
                    pkgId : s.bookingRefId()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        nvx.spinner.stop();
                        alert(data.data);
                    }
                    else
                    {
                        alert(data.data);
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }

        s.hasDeactivateRole = function () {
          if($.inArray('pp-mflg-deactivate-pincode',$page.roles) > -1) {
              return true;
          }  else {
              return false;
          }
        };

        s.deactivatePincode = function() {
            var result = confirm("Do you confirm to deactivate the pin code?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/deactivate-pincode',
                data : {
                    pkgId : s.bookingRefId()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        alert("Pin Code deactivated.");
                        s.status('Deactivated');
                        nvxpage.pkgDs.read();
                    }
                    else
                    {
                        alert(data.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }

        s.downloadTicket = function() {
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/download-ticket?pkgId='+s.bookingRefId(),
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        //$page.openPackagePop(data.data,pkgName);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    }


    nvx.MMPackageItem = function(parent, base) {
        var s = this;
        s.parent = parent;

        s.itemId = ko.observable(base.id);
        s.displayName = ko.observable(base.displayName);
        s.displayDetails = ko.observable(base.displayDetails);
        s.ticketType = ko.observable(base.ticketType);
        s.description = ko.observable(base.displayDetails);
        s.receiptNum = ko.observable(base.receiptNum);
        s.qty = ko.observable(base.pkgQty);
        s.qtyRedeemed = ko.observable(base.qtyRedeemed);
        s.topupItems = ko.observableArray([]);
        $.each(base.topupItems, function(idx, model) {
            var vm = new nvx.TopupItems(model);
            s.topupItems.push(vm);
        });
    }


    nvx.TopupItems = function(data) {
        var s = this;
        s.itemId = ko.observable(data.itemId);
        s.displayName = ko.observable(data.displayName);
        s.pkgQty = ko.observable(data.pkgQty);
    }

    $page.checkRedeem = function(element,pkgId,canRedeemDtStr){
        if(canRedeemDtStr){
            var dt = kendo.parseDate(canRedeemDtStr, "dd/MM/yyyy HH:mm:ss");
            var now = new Date();
            if(dt.getTime() > now.getTime()){
                alert("You may check the status after "+canRedeemDtStr);
                return;
            }
        }
        $(element).removeClass();
        $(element).addClass("btn btn-block btn-disabled");
        var data = {'pkgId':pkgId};
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/chk-redemption-status',
            data: data,
            success:  function(data, textStatus, jqXHR)
            {
                $page.checkRedeemCallBack(data);
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                        nvx.spinner.stop();
                    }
        });
    };
    $page.checkRedeemCallBack = function(data){
        if(!data.success){
            alert("System is not able to verify status now, please try again later.");
            return;
        }
        $page.pkgDs.read();
        console.log(data.message);
    };
    $page.initSubUsers= function(){

        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-subusers',
            data : {
                id : $page.paVmJson.id
            },
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    $page.accPageModel = new $page.AccPageModel();
                    var data = data['data'];
                    if(data.paAccountVMs){
                        var accs = data.paAccountVMs;
                        $page.accessGroupVmsJson = data.paAccessGroupVMs;
                        var accsLength = accs.length;
                        for (var i = 0; i < accsLength; i++) {
                            var tem = accs[i];
                            var acc = new $page.AccModel();
                            acc.update(tem);
                            $page.accPageModel.accounts.push(acc);
                        }
                    };
                    ko.applyBindings($page.accPageModel,$('#paSubUsersDv')[0]);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.AccPageModel = function(){
        var s = this;
        s.accounts = ko.observableArray();
    };
    $page.AccessModel = function(data){
        var self = this;
        //Knockout 2.3.0 value can only be string
        self.id = ko.observable(data.id.toString());
        self.description = ko.observable(data.description);
    };
    $page.AccModel = function() {
        var s = this;
        s.Data = {};
        s.id = ko.observable(-1);
        s.isNew = ko.observable(true);
        s.username = ko.observable();
        s.title = ko.observable();
        s.email = ko.observable();
        s.access = ko.observableArray();
        s.status = ko.observable();
        s.errMsg = ko.observable();
        s.accesslist = ko.observableArray();
        s.accountCode = ko.observable($page.paVmJson.accountCode);
        s.isErr = ko.observable();

        s.unErr = ko.observable(false);
        s.nameErr = ko.observable(false);
        s.emailErr = ko.observable(false);
        s.titleErr = ko.observable(false);

        var accessGroupLength = $page.accessGroupVmsJson.length;
        for (var i = 0; i < accessGroupLength; i++) {
            var o = new $page.AccessModel($page.accessGroupVmsJson[i]);
            s.accesslist.push(o);
        }
        s.update = function(data) {
            for(var k in data) s.Data[k]=data[k];
            s.id(data.id);
            s.username(data.username);
            s.isNew(false);
            s.email(data.email);
            s.title(data.title);
            s.status(data.status);
            s.accountCode($page.paVmJson.accountCode);

            var ids= data.rightsIds;
            if(ids){
                var idLength = ids.length;
                for (var i = 0; i < idLength; i++){
                    s.access.push(ids[i].toString());
                }
            }
        };
        s.openEditPop = function(){
            var acc = new $page.AccModel();
            acc.update(s.Data);
            $page.openEditPop(acc);
        };

        s.doResetPassword = function() {
            var result = confirm("Are you sure you want to reset the password?");
            if (result == false) {
                return;
            }
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/reset-password',
                data: {
                    userId: s.id(),
                },
                success: function(data) {
                    if (data.success) {
                        alert('Successfully reset user password.');
                    } else {
                        s.clearErrs();
                        s.errMsg(data.message);
                        s.isErr(true);
                    }
                },
                error: function() {
                    s.clearErrs();
                    s.errMsg('Unable to process your request. Please try again in a few moments.');
                    s.isErr(true);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.doSave = function() {
            if (s.validate()) {
                var grpId = '';
                ko.utils.arrayForEach(s.access(), function(item) {
                    grpId += (grpId != '' ? ',' : '') + item;
                });
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/partner-management/save-subuser',
                    data: {
                        id: $page.paVmJson.id,
                        isNew: s.isNew(),
                        userId: s.id(),
                        userName: s.username(),
                        name: s.username(),
                        email: s.email(),
                        title: s.title(),
                        status: s.status(),
                        access: grpId
                    },
                    success: function(data) {
                        if (data.success) {
                            alert('Successfully saved user account.');
                            $page.accPageModel.accounts.removeAll();
                            if(data.data){
                                var accs = JSON.parse(data.data);
                                var accsLength = accs.length;
                                for (var i = 0; i < accsLength; i++) {
                                    var tem = accs[i];
                                    var acc = new $page.AccModel();
                                    acc.update(tem);
                                    $page.accPageModel.accounts.push(acc);
                                }
                            };
                        } else {
                            s.clearErrs();
                            s.errMsg(data.message);
                            s.isErr(true);
                        }
                    },
                    error: function() {
                        s.clearErrs();
                        s.errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        };

        s.validate = function() {
            s.errMsg('');
            var msg = '';
            if (s.isNew() && !nvx.isLengthWithin(s.username(), 1, 10, true)) {
                s.nameErr(true);
                msg += (msg != '' ? '<br>' : '') + "Name is required and must be less than 100 characters.";
            }
            if (s.isNew() && !nvx.isLengthWithin(s.title(), 1, 100, true)) {
                s.titleErr(true);
                msg += (msg != '' ? '<br>' : '') + "Title is required and must be less than 100 characters.";
            }
            if (!nvx.isValidEmail(s.email())) {
                s.emailErr(true);
                msg += (msg != '' ? '<br>' : '') + "Email is invalid.";
            }
            if (msg !== '') {
                s.isErr(true);
                s.errMsg(msg);
                return false;
            }
            return true;
        };
        s.clearErrs = function() {
            s.isErr(false);
            s.errMsg('');
            s.unErr(false);
            s.nameErr(false);
            s.emailErr(false);
            s.titleErr(false);
        };
    };
    $page.openEditPop = function(acc){
        var htmlcontent  =$page.paSubUserPopTemp({});
        if(!$("#accPopWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'accPopWindow');
        }
        var accPopWindow = $("#accPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: acc?acc.username():'New User',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        accPopWindow.content(htmlcontent);
        if(!acc){
            var newAcc = new $page.AccModel();
            ko.applyBindings(newAcc,$('#accPopDv')[0]);
        }else{
            ko.applyBindings(acc,$('#accPopDv')[0]);
        }
        accPopWindow.center().open();
    };
    initTabStrip = function() {
        $page.tabData = [ {
            Name :$page.PaProfileLabel ,
            Content : $page.loadLabel,
            Data : {},
            Template:$page.paDetailTemp,
            Reload: function(){
                $page.syncContent(0);
                //$('#uploadfile').kendoUpload({multiple:false});
                $page.paModel = new $page.PartnerModel($page.paVmJson);
                ko.applyBindings($page.paModel,$('#paProfileDv')[0]);
            }},
            { Name : $page.PaConfigLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.paConfigTemp,
                Reload: function(){
                    $page.syncContent(1);
                    $('#partnerWoTReservationValidUntil').kendoDatePicker({
                        format: nvx.DATE_FORMAT
                    }).data('kendoDatePicker');
                    var kpicker = $('#partnerWoTReservationValidUntil').data("kendoDatePicker");
                    kpicker.enable(false);
                    $page.paConfigModel = new $page.PaDetailModel($page.paVmJson);
                    ko.applyBindings($page.paConfigModel,$('#paConfigDv')[0]);
                    $page.refreshProd();
                    $page.productGrid = $("#productGrid").kendoGrid({
                        dataSource: $page.prodDS,
                        columns: [{
                            title: "Add",
                            template:
                                //'<a href="javascript:nvxpage.paConfigModel.addProd(\'#=id#\',\'#=escape(displayTitle)#\',\'#=prodType#\'\)" class="round-bt"><i  class="fa fa-plus-circle"></i></a>'
                                '<a elementType="pending-adding-products" href="javascript:nvxpage.paConfigModel.addProd(\'#=id#\')"  prodId=\"#=id#\" prodType=\"#=prodType#\" prodName=\"#=encodeURIComponent(displayTitle)#\" class="round-bt"><i  class="fa fa-plus-circle"></i></a>'
                            ,
                            sortable: false,
                            width: 20
                        },{
                            field: "displayTitle",
                            title: "Product Name",
                            width: 100
                        },{
                            field: "prodType",
                            title: "Type",
                            width: 40
                        }
                        ],
                        sortable: $page.serverSorting, pageable: true
                    });
                }},{ Name : $page.PaSubUserLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.paSubUsersTemp,
                Reload: function(){
                    $page.syncContent(2);
                    $page.initSubUsers();
                }},{ Name : $page.PaLogLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.paHistTemp,
                Reload: function(){
                    $page.syncContent(3);
                    $page.loadPaHist();
                }},{ Name : $page.PaInventoryLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.paInvenTemp,
                Reload: function(){
                    $page.syncContent(4);
                    $page.invenFilterModel = new $page.InventoryFilterModel();
                    ko.applyBindings($page.invenFilterModel,$('#inventoryform')[0]);
                    $('#startDate').kendoDatePicker({
                        format: nvx.DATE_FORMAT
                    }).data('kendoDatePicker');
                    $('#endDate').kendoDatePicker({
                        format: nvx.DATE_FORMAT
                    }).data('kendoDatePicker');
                    $page.initInventoryGrid();
                }},{ Name : $page.PaPinLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.paPinTemp,
                Reload: function(){
                    $page.syncContent(5);
                    $('#pinStartDate').kendoDatePicker({
                        format: nvx.DATE_FORMAT
                    }).data('kendoDatePicker');
                    $('#pinEndDate').kendoDatePicker({
                        format: nvx.DATE_FORMAT
                    }).data('kendoDatePicker');
                    $page.pkgFilterModel = new $page.PkgFilterModel();
                    ko.applyBindings($page.pkgFilterModel,$('#pkgForm')[0]);
                    $page.initPkgGrid();
                }
            },{
                /*offline payment, begin*/
                Name :$page.OfflinePaymentLabel,
                Content : $page.loadLabel,
                Data : {},
                Template:$page.offlinePaymentTemplate,
                Reload: function(){
                    $page.syncContent(6);
                    // binding offline payment model
                    $page.offlinePaymentViewModel = kendo.observable(new $page.OfflinePaymentViewModel());
                    //ko.applyBindings($page.offlinePaymentViewModel, $('#offlinePaymentFilterAndListView')[0]);
                    kendo.bind($('#offlinePaymentFilterAndListView')[0], $page.offlinePaymentViewModel);
                }
                /*offline payment, end*/
            }
        ];

        $page.tabstrip = $("#tabstrip").kendoTabStrip({
            dataTextField : "Name",
            dataContentField : "Content",
            dataSource : $page.tabData,
            select : $page.tabSelect
        }).data("kendoTabStrip");

        // $page.tabstrip.select(0);
    };

    $page.syncContent = function(index){
        var tab = $page.tabData[index];
        tab.Content = tab.Template(tab.Data);
        $($page.tabstrip.contentElement(index)).html(tab.Content);
    };
    $page.submitPa = function(){
        var partner = {};
        partner.id = $page.paModel.id();
        partner.accountManagerId = $page.paConfigModel.accountManagerId();
        partner.subAccountEnabled = $page.paConfigModel.selSubAccountEnabled();
        partner.offlinePaymentEnabled = $page.paConfigModel.selOfflinePaymentEnabled();
        partner.revalPeriodMonths = $page.paConfigModel.newRevalPeriodMonths();
        partner.revalFeeItemId = $page.paConfigModel.revalFeeItemId();
        partner.dailyTransCapStr = $page.paConfigModel.newDailyTransCapStr();
        partner.tierId = $page.paConfigModel.selTier();
        partner.remarks = $page.paConfigModel.remarks()
        partner.excluProdIds = [];
        ko.utils.arrayForEach($page.paConfigModel.iprods(), function(prod) {
            partner.excluProdIds.push(prod.id);
        });
        partner.distributionMapping = [];
        var total = 0;
        ko.utils.arrayForEach($page.paConfigModel.distributionModels(), function(tmpMap) {
            var tmpMapJs = {percentage:tmpMap.percentage(),countryId:tmpMap.countryId(),adminId:tmpMap.adminId()};
            partner.distributionMapping.push(tmpMapJs);
            total = total + Number(tmpMap.percentage());
        });
        if(total < 100){
            alert("Market Distribution Country doesn't much 100%.");
            return;
        }
        if(!partner.accountManagerId){
            alert("Please select an Account Manager.");
            return;
        }
        var numReg = /^\d+$/;
        if(!numReg.test(partner.revalPeriodMonths)){
            alert("Please fill in a valid Revalidation Period.");
            return;
        }
        var revMonth = new Number(partner.revalPeriodMonths);
        var tktRevalP = new Number($page.ticketRevalidatePeriod);
        if(revMonth>tktRevalP){
            alert("Revalidation Period should be less than "+tktRevalP+" months.");
            return;
        }

        if(!partner.revalFeeItemId){
            alert("Please select a Revalidation Fee.");
            return;
        }
        if(!partner.dailyTransCapStr){
            alert("Please fill in the Daily Transaction Caps.");
            return;
        }
        var isValid = partner.dailyTransCapStr.trim().search(/^\$?[\d,]+(\.\d{1,2})?$/) >= 0;
        if(!isValid){
            alert("Please fill in the Daily Transaction Caps with correct format.");
            return;
        }
        if(!partner.tierId){
            alert("Please select a Tier Level.");
            return;
        }
        var _capacityGroupId = $page.paConfigModel.selCapacityGroupId();
        if(!(_capacityGroupId != undefined && _capacityGroupId != null && _capacityGroupId != '')){
            alert("Please select a  Capacity Group.");
            return;
        }
        var _wotReservationCap = $page.paConfigModel.wotReservationCap();
        var _wotReservationValidUntil = $page.paConfigModel.wotReservationValidUntil();
        if(true == $page.paConfigModel.selWotReservationEnabled()){
            if(!(_wotReservationCap != undefined && _wotReservationCap != null && _wotReservationCap != '')){
                alert("Please fill in the Wings of Time Reservation Cap.");
                return;
            }
            if(isNaN(_wotReservationCap)){
                alert("Please fill in the Wings of Time Reservation Cap with correct value.");
                return;
            }
            if(parseInt(_wotReservationCap) <= 0){
                alert("Please fill in the Wings of Time Reservation Cap with correct value.");
                return;
            }

            var validUtilDate = kendo.parseDate(_wotReservationValidUntil, $page.DateFormat);
            if(_wotReservationValidUntil != undefined && _wotReservationValidUntil!= null && _wotReservationValidUntil != '') {
                if (!(validUtilDate != undefined && validUtilDate != null && validUtilDate != '')) {
                    alert("Please fill in the Wings of Time Valid Until with correct date.");
                    return;
                }
            }

        }else{
            _wotReservationCap = '';
            _wotReservationValidUntil = '';
        }
        if(!partner.remarks){
            alert("Please fill in the remarks.");
            return;
        }

        partner.extension = [];
        partner.extension[partner.extension.length] = { 'attrName' : 'customerGroup', 'attrValue' : $page.paConfigModel.selCustomerGroupId(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'onlinePaymentEnabled', 'attrValue' : $page.paConfigModel.onlinePaymentEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'offlinePaymentEnabled', 'attrValue' : $page.paConfigModel.offlinePaymentEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'depoistEnabled', 'attrValue' : $page.paConfigModel.depoistEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationEnabled', 'attrValue' : $page.paConfigModel.selWotReservationEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationCap', 'attrValue' : _wotReservationCap, 'attrValueType' : 'INT', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationValidUntil', 'attrValue' : _wotReservationValidUntil, 'attrValueType' : 'DATE', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'capacityGroupId', 'attrValue' : $page.paConfigModel.selCapacityGroupId(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        // TODO
        // if(!partner.offlinePaymentEnabled){
        //     var _continue_to_update = true;
        //     $.ajax({
        //         type: "POST",
        //         url: '/admin/partner/edit/checkPendingOfflinePayment',
        //         data: {'paId' : parseInt(partner.id)},
        //         async : false,
        //         success:  function(data, textStatus, jqXHR)
        //         {
        //             if (data.success) {
        //                 if('error' == data.message){
        //                     _continue_to_update = false;
        //                 }
        //             }
        //         },
        //         error: function(jqXHR, textStatus) {},
        //         complete: function() {}
        //     });
        //     if(!_continue_to_update){
        //         alert("Partner has pending offline payment request(s), disabling offline payment not allowed.");
        //         return;
        //     }
        // }



        var paVmJson = JSON.stringify(partner);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/save-partner-details',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.paConfigModel.isPending(true);
                    $page.paConfigModel.configEdit().editAble(false);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.TransModel = function() {
        var s = this;
        s.id = ko.observable('');
        //s.email = ko.observable('');
        s.status = ko.observable('');
        s.receiptNum = ko.observable('');
        s.tmStatus = ko.observable('');
        s.tmErrorCode = ko.observable('');
        s.tmErrorMessage = ko.observable('');
        s.tmStatusDateStr = ko.observable('');
        s.accountCode = ko.observable('');
        s.orgName = ko.observable('');
        s.address = ko.observable('');
        s.validityStartDateStr = ko.observable('');
        s.validityEndDateStr = ko.observable('');
        s.username = ko.observable('');
        s.paymentType = ko.observable('');
        s.totalAmount = ko.observable('');
        s.totalAmountStr = ko.observable('');
        s.revalFeeStr = ko.observable('');
        s.singleRevalFeeStr = ko.observable('');
        s.gst = ko.observable('');
        s.gstStr = ko.observable('');
        s.gstRate = ko.observable('');
        s.gstRateStr = ko.observable('');
        s.revalDtTmStr = ko.observable('');
        s.transType = ko.observable('');
        s.exclGst = ko.observable('');
        s.exclGstStr = ko.observable('');
        s.createdDateStr = ko.observable('');
        s.displayStatus = ko.observable('');
        s.prods = ko.observableArray([]);
        s.pinGenerated = ko.observable(true);
        s.init = function(tdVm) {
            s.id(tdVm.id);
            //TODO s.email(tdVm.email);
            s.status(tdVm.status);
            s.receiptNum(tdVm.receiptNumber);
            s.tmErrorCode(tdVm.tmErrorCode);
            s.tmErrorMessage(tdVm.tmErrorMessage);
            s.tmStatusDateStr(tdVm.tmStatusDateStr);
            s.accountCode(tdVm.partner.accountCode);
            s.orgName(tdVm.partner.orgName);
            s.address(tdVm.partner.address);
            s.validityStartDateStr(tdVm.validityStartDateStr);
            s.validityEndDateStr(tdVm.validityEndDateStr);
            s.username(tdVm.username);
            s.paymentType(tdVm.paymentType);
           
            s.revalFeeStr("5"); //TODO
            s.singleRevalFeeStr("1"); //TODO
            s.gstRate(tdVm.gstRate);
            s.gstRateStr = ko.computed(function() {
                return (s.gstRate()*100).toFixed(0) +'%';
            });

            s.totalAmount = ko.computed(function() {
                var gt = 0;
                $.each(s.prods(), function(idx, prod) {
                    gt += Number(prod.subTotal());
                });
                return gt.toFixed(2);
            });
            s.totalAmountStr = ko.computed(function() {
                return Number(s.totalAmount()).toFixed(2);
            });

            s.gst = ko.computed(function() {
                return (Number(s.totalAmount()) * (s.gstRate()*100*1.0 / (s.gstRate()*100+100))).toFixed(2);
            });
            s.gstStr = ko.computed(function() {
                return Number(s.gst()).toFixed(2);
            });
            s.exclGst = ko.computed(function() {
                return (Number(s.totalAmount()) - Number(s.gst())).toFixed(2);
            });
            s.exclGstStr = ko.computed(function() {
                return Number(s.exclGst()).toFixed(2);
            });
            s.revalDtTmStr("rrr"); //TODO
            s.transType("Purchase"); //TODO
            s.createdDateStr(tdVm.createdDateStr);
            s.displayStatus(tdVm.displayStatus);
            s.tmStatus(tdVm.tmStatus);
            s.pinGenerated(tdVm.ticketGenerated);
            if(tdVm.productList){
                $.each(tdVm.productList, function(idx, val) {
                    s.prods.push(new $page.Product(s,val,tdVm.items));
                });
            }
        };
        s.regenerateReceipt = function(){
            var result = confirm("Do you want to resend the receipt?");
            if (result == false) {
                return;
            }

            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/regenerate-receipt',
                data : {
                    transId : s.id()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        alert(data.data);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
        s.hasRefundRole= function() {
            if($.inArray('pp-mflg-refund-transaction',nvxpage.roles) > -1) {
                return true;
            }
            return false;
        };
        s.refund = function(){
            var result = confirm("Do you want to refund the receipt?");
            if (result == false) {
                return;
            }
            var data = {};
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/refund-trans',
                data : {
                    transId : s.id()
                },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success){
                        alert("Refund successfully.");
                        s.displayStatus('Refunded');
                        $page.itemsDs.read();
                    }else{
                        alert(data.message);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };

    $page.Product = function(parent, base, transItems) {
        var s = this;
        s.parent = parent;
        s.displayTitle = ko.observable(base.name);
        s.subTotal = ko.observable(base.subtotal);
        s.subTotalStr  = ko.computed(function() {
            return Number(s.subTotal()).toFixed(2);
        });
        s.subTotalRevalStr = ko.observable(base.subtotal);//TODO
        s.items = ko.observableArray([]);
        $.each(transItems, function(idx, val) {
            s.items.push(new $page.ProductItem(val));
        });
    };

    $page.ProductItem = function(data) {
        var s = this;
        s.itemTopup = ko.observable(data.topup);
        s.qty = ko.observable(data.qty);
        s.displayName = ko.observable(data.name);
        s.subTotal= ko.observable(data.subtotal);
        s.unitPrice = ko.observable(data.unitPrice);
        s.subTotalStr = ko.computed(function() {
            return Number(s.subTotal()).toFixed(2);
        });
        s.unitPriceStr = ko.computed(function() {
            return Number(s.unitPrice()).toFixed(2);
        });
        s.unpackagedQty = ko.observable(data.unpackagedQty);
        s.singleRevalFeeStr = ko.observable("1"); //TODO
        s.revalFeeStr = ko.observable("5"); //TODO
    };
    $page.loadTrans= function(id){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-trans-details',
            data : {
                transId : id
            },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    var tdVm =  data.data;
                    $page.openTransPop(tdVm);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.openTransPop = function(data){
        var htmlcontent =$page.transPopTemp(data);
        if(!$("#transPopWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'transPopWindow');
        }
        var tempWindow = $("#transPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Detail',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.transModel =new $page.TransModel();
        $page.transModel.init(data);
        ko.applyBindings($page.transModel,$('#transModal')[0]);
        tempWindow.center().open();
    };
    /** offline payment start **/
    $page.OfflinePaymentViewModel = function(){
        var s = this;
        s.baseUrl = '';
        s.searchUrl = '';
        s.startDate = null;
        s.endDate = null;
        s.receiptNo = '';
        s.requestStatus = '';
        s.requestStatusList = new kendo.data.DataSource({ data : [
            {reqStatusId : '', reqStatusName : ''},
            {reqStatusId : 'Pending_Approval', reqStatusName : 'Pending Approval'},
            {reqStatusId : 'Pending_Submit', reqStatusName : 'Pending Submit'},
            {reqStatusId : 'Cancelled', reqStatusName : 'Cancelled'},
            {reqStatusId : 'Approved', reqStatusName : 'Approved'},
            {reqStatusId : 'Rejected', reqStatusName : 'Rejected'}
        ]});
        s.offlineTxnModel = kendo.data.Model.define({
            id: 'id',
            fields: {
                id: {type: "number"},
                receiptNum: {type: 'string'},
                currency: {type: 'string'},
                createdDateStr: {type: 'string'},
                totalAmountStr: {type: 'string'},
                status: {type: 'string'},
                transQty : {type: "number"},
                submitted : {type : "boolean"},
                submitBy: {type: 'string'},
                submitDtStr: {type: 'string'},
                approvedBy: {type: 'string'},
                approvalDateText: {type: 'string'},
                displayStatus: {type: 'string'},
                paymentTypeText: {type: 'string'}
            }
        });
        s.offlinePaymentListDs = new kendo.data.DataSource({
            transport: {
                read: {
                    url: nvx.API_PREFIX + '/offlinePay/list', cache: false,
                    dataType : "json"
                },
                parameterMap : function(options, operation){
                    var _n_o_v_m = $page.offlinePaymentViewModel;
                    options['isPartnerOnly'] = 'Y';
                    options['paId'] = nvxpage.paVmJson.id;
                    options['startDtStr'] = kendo.toString(_n_o_v_m.startDate, 'dd/MM/yyyy');
                    options['endDtStr'] = kendo.toString(_n_o_v_m.endDate, 'dd/MM/yyyy');
                    options['receiptNum'] = _n_o_v_m.receiptNo;
                    options['status'] = _n_o_v_m.requestStatus;
                    return options;
                }
            },
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: s.offlineTxnModel
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
        s.reset = function(){
            this.set('startDate', null);
            this.set('endDate', null);
            this.set('receiptNo', '');
            this.set('requestStatus', '');
        };
        s.refreshSearchResult = function(){
            this.offlinePaymentListDs.read();
        };
        s.refreshOfflinePaymentListSequenceNo = function(){
            $('._offline_payment_list_rownumber').each(function(i){$(this).html(i+1);});
        };
        s.viewOfflinePaymentTxnDetails = function(e){
            var sltTxnId = e.target.value;
            nvx.spinner.start();
            $.ajax({
                type: "GET",
                url: nvx.API_PREFIX + '/offlinePay/preview?id='+sltTxnId,
                success:  function(data, textStatus, jqXHR)
                {
                    if (data.success) {
                        var tdVm =  data['data'];
                        $page.ppmflgOfflinePaymentApprovalModel.reset();
                        $page.ppmflgOfflinePaymentApprovalModel.initOfflinePaymentApprovalModel(tdVm);
                        var htmlcontent =$page.pplsmOfflinePaymentViewDetailsTemplate($page.ppmflgOfflinePaymentApprovalModel);
                        if(!$("#ppmflgOfflinePaymentViewDetailsPopWindow").length){
                            var $div = $('<div />').appendTo('body');
                            $div.attr('id', 'ppmflgOfflinePaymentViewDetailsPopWindow');
                        }
                        $page.ppmflgOfflinePaymentViewDetailsPopWindow = $("#ppmflgOfflinePaymentViewDetailsPopWindow").kendoWindow({
                            width: '800px',
                            modal: true,
                            resizable: false,
                            title: 'Detail',
                            actions: ["Close"],
                            viewable : false,
                            deactivate: function() {
                                this.destroy();
                            }
                        }).data('kendoWindow');
                        $page.ppmflgOfflinePaymentViewDetailsPopWindow.content(htmlcontent);
                        ko.applyBindings($page.ppmflgOfflinePaymentApprovalModel,$('#pplsmOfflinePaymentViewDetailsTransModel')[0]);
                        $page.ppmflgOfflinePaymentViewDetailsPopWindow.center().open();
                        console.log('view offline payment transaction '+sltTxnId+' details.');
                    } else {
                        alert(data['message']);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    };
    /** offline payment end **/
    $page.tabSelect = function(e){
        var selectedIndex = $(e.item).index();
        var tab = $page.tabData[selectedIndex];
        $page.currentTab = tab;
        console.log("select: "+tab.Name);
        switch (tab.Name) {
            case $page.PaConfigLabel:
                break;
            default:
                break;
        }
        if(tab.Content == $page.loadLabel){
            tab.Reload();
        }
    };
    $page.closePopById = function(id){
        var dialog = $("#"+id+"").data("kendoWindow");
        dialog.destroy();
    };




    $page.inittrans = function() {
        $page.ITEM_BASE_URL = nvx.API_PREFIX + '/partner-management/search-inventory?id='+$page.paVmJson.id;
        $page.theItemUrl = $page.ITEM_BASE_URL;
        $page.itemsDs = new kendo.data.DataSource({
            transport: {read: {url: $page.theItemUrl, cache: false}},
            schema: {
                data: 'data.transItemVms',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        transId: {type: "number"},
                        receiptNumber: {type: 'string'},
                        name: {type: 'string'},
                        type: {type: 'string'},
                        validityStartDateStr: {type: 'string'},
                        validityEndDateStr: {type: 'string'},
                        qty: {type: "number"},
                        unpackagedQty: {type: "number"},
                        subtotal: {type: "number"},
                        status: {type: 'string'},
                        username: {type: 'string'},
                        ableToPkg: {type: 'boolean'},
                        topupItems: {}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true,
        });
    };

    $page.initPkg = function() {
        $page.PKG_BASE_URL = nvx.API_PREFIX + '/partner-management/search-package?id='+$page.paVmJson.id;
        $page.pkgDs = new kendo.data.DataSource({
            transport: {read: {url:  $page.PKG_BASE_URL, cache: false}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        name: {type: 'name'},
                        pkgTktMedia: {type: 'string'},
                        pinCode: {type: 'string'},
                        qty: {type: 'number'},
                        pinGeneratedDateStr: {type: 'string'},
                        expiryDateStr: {type: "string"},
                        status: {type: "string"},
                        qtyRedeemedStr: {type: "string"},
                        lastRedemptionDateStr: {type: 'string'},
                        description: {type: 'string'},
                        username: {type: 'username'},
                        canRedeem:{type:"boolean"},
                        canRedeemDtStr:{type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    }

    $page.checkAccessRights = function() {
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-login-user-roles',
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    var roles = JSON.parse(data['data']);
                    $page.navControl(roles);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.navControl = function(roles) {

        $page.roles = roles;

         if($.inArray('pp-mflg-user-accounts',roles) == -1){

         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(0));
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(1));
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(2));
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(3));
         }else{
         $page.tabstrip.select(0);
         }

         if(($.inArray('pp-mflg-refund-transaction',roles) == -1) && ($.inArray('pp-mflg-view-inventory',roles) == -1)){
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(4));
         }else if($.inArray('pp-mflg-user-accounts',roles) == -1){
         $page.tabstrip.select(4);
         }

         if(($.inArray('pp-mflg-deactivate-pincode', roles) == -1) && ($.inArray('pp-mflg-view-package',roles) == -1)){
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(5));
         }else if(($.inArray('pp-mflg-user-accounts',roles) == -1)&&($.inArray('pp-mflg-refund-transaction',roles) == -1)&&($.inArray('pp-mflg-view-inventory',roles))){
         $page.tabstrip.select(5);
         }

        /*disable the offline payment mode by default*/
        var exts = $page.paVmJson['extension'];
        for(var i = 0 ; i < exts.length ; i++) {
            var ext = exts[i];
            if (ext != undefined && ext != null && ext != '') {
                var name = ext['attrName'];
                var attrVal = ext['attrValue'];
                if (name == 'offlinePaymentEnabled') {
                    break;
                }
            }
        }
         $page.tabstrip.disable($page.tabstrip.tabGroup.children().eq(6));

         if(($.inArray('pp-mflg-view-offline-payment',roles) > -1) || ($.inArray('pp-mflg-approve-offline-payment',roles)> -1)){
        if(attrVal == 'true'){
            //enable the offline payment mode if the user has the permission and the partner offline payment mode has enabled.
            $page.tabstrip.enable($page.tabstrip.tabGroup.children().eq(6));
        }
    }

         if(($.inArray('pp-mflg-user-accounts',roles) == -1)
         &&($.inArray('pp-mflg-refund-transaction',roles) == -1)
         &&($.inArray('pp-mflg-view-inventory',roles) == -1)
         &&($.inArray('pp-mflg-deactivate-pincode',roles) == -1)
         &&($.inArray('pp-mflg-view-package',roles) == -1)){
        window.location.href = "/.resources/partner-portal-mflg-admin/access-denied.html";
    }
    };
    $page.loadPageData = function(data, status, jqXHR){
        if(data != undefined) {

            $page.paVmJson = data['partnerVM'];
            $page.paVmJson.adminVmsJson = data['adminAccountVms'];
            $page.paVmJson.revalItemVmsJson = data['revalFeeItemVMs'];
            $page.ticketRevalidatePeriod = data['ticketRevalidatePeriod'];
            var tiervms = data['tierVMs'];
            var b2btiervms = [];
            for(var i=0; i<tiervms.length; i++) {
                var tiervm = tiervms[i];
                if(tiervm.isB2B==1) {
                    b2btiervms.push(tiervm);
                }
            }
            $page.paVmJson.tierVmsJson = b2btiervms;
            $page.docTypeJson = data['docTypeVMs']
            $page.paTypeVmsJson = data['partnerTypeVMs']
            $page.ctyVmsJson = data['ctyVMs'];
            $page.isPending = data['pending'];
            $page.customerGroupJson = data['customerGroupList'];
            $page.capacityGroupJson = data['capacityGroupList'];
            
            $page.inittrans();
            $page.initPkg();
            initTabStrip();
            $page.checkAccessRights();
            // $page.paModel = new nvxpage.PartnerModel(nvxpage.paVmJson);
            // $page.paDetailModel = new nvxpage.PaDetailModel(nvxpage.paVmJson);
            // $('#paBasicDv').html($page.paBasicTemp({}));
            // $('#paDetailDv').html($page.paDetailTemp({}));
            // ko.applyBindings($page.paModel, $('#paBasicDv')[0]);
            // ko.applyBindings($page.paDetailModel, $('#paDetailDv')[0]);

            /*$page.PKG_BASE_URL = '/admin/partner/viewPkgs/do?paId='+$page.paVmJson.id;
             $page.pkgDs = new kendo.data.DataSource({
             transport: {read: {url:  $page.PKG_BASE_URL, cache: false}},
             schema: {
             data: function(response) {
             return JSON.parse(response.pkgVmsJson);
             },
             total: 'total',
             model: {
             id: 'id',
             fields: {
             name: {type: 'name'},
             pinCode: {type: 'string'},
             pinCodeQty: {type: 'number'},
             pinGeneratedDateStr: {type: 'string'},
             expiryDateStr: {type: "string"},
             status: {type: "string"},
             qtyRedeemedStr: {type: "string"},
             lastRedemptionDateStr: {type: 'string'},
             description: {type: 'string'},
             username: {type: 'username'},
             canRedeem:{type:"boolean"},
             canRedeemDtStr:{type: 'string'}
             }
             }
             }, pageSize: 10, serverSorting: true, serverPaging: true
             });*/
            // init product grid
            //$page.initProductGrid();
            // refresh product grid
            //$page.refreshProd();
        }
    };
    $page.initPageData = function(){
        var paId = -1;

        var params = window.location.href;
        if(params != null && params != ''){
            var paIdIdx = params.indexOf('paId=');
            if(paIdIdx != null && paIdIdx > 0){
                try{
                    paId = parseInt(params.substr(paIdIdx + 5).trim());

                }catch(e){}
            }
        }
        if(paId > 0){
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/get-partner-maintenance-details',
                data : {
                    id : paId
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        $page.loadPageData(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            var errorMsg = nvx.getDefinedMsg(data.message);
                            if(errorMsg != undefined && errorMsg != ''){
                                alert(errorMsg);
                            }else{
                                alert(data.message);
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    };


    window.nvxpage = $page;
    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/

});