$(document).ready(function() {
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    var $page = {};
    $page.DateFormat = 'dd/MM/yyyy';
    $page.paBasicTemp = kendo.template($("#paBasicTemp").html());
    $page.paDetailTemp = kendo.template($("#paDetailTemp").html());
    $page.reasonPopTemp  = kendo.template($("#reasonPopTemp").html());
    $page.resubmitReasonPopTemp = kendo.template($("#resubmitReasonPopTemp").html());
    /***
    $page.paVmJson = paVmJson;
    $page.paVmJson.adminVmsJson = adminVmsJson;
    $page.paVmJson.revalItemVmsJson = revalItemVmsJson;
    $page.ticketRevalidatePeriod = ticketRevalidatePeriod;
    $page.reasonVmsJson = reasonVmsJson;
    $page.paVmJson.tierVmsJson = tierVmsJson;
    $page.ctyVmsJson = ctyVmsJson;
    ***/
    $page.baseProdUrl = nvx.API_PREFIX + '/product-management/get-exclusive-products';
    $page.prodDS = new kendo.data.DataSource({
        transport: {
            read: {
                type: 'POST', url: $page.baseProdUrl, cache: false
            }
        },
        schema: {
            data:function(response) {
                if(response['data'] != null && response['data'] != ''){
                    var lst = JSON.parse(response['data']);
                    if(lst != null && lst.length > 0){
                        var len = lst.length;
                        for(var i = 0  ; i < len ; i++){
                            var tmp = lst[i];
                            var id = tmp['id'];
                            if(id != undefined && id != null && id != ''){
                                tmp['id'] = id.trim();
                            }
                            tmp['displayTitle'] = tmp['name'];
                        }
                    }
                    return lst;
                }
                return [];
            },
            total: 'total',
            model: {
                id: 'id',
                fields: {
                    id: {type: "string"},
                    displayTitle: {type: 'string'},
                    prodType: {type: 'string'},
                    tiers : {type: 'string'}
                }
            }
        }, pageSize: 5, serverSorting: true, serverPaging: true
    });
    $page.PartnerModel = function(pa){
        var s = this;
        s.Data = pa;
        s.id = ko.observable();
        s.orgName = ko.observable();
        s.accountCode = ko.observable();
        s.branchName = ko.observable();
        s.uen = ko.observable();
        s.licenseNum = ko.observable();
        s.licenseExpDate = ko.observable();
        s.contactPerson = ko.observable();
        s.contactDesignation = ko.observable();
        s.address = ko.observable();
        s.postalCode = ko.observable();
        s.city = ko.observable();
        s.correspondenceAddress = ko.observable();
        s.correspondencePostalCode = ko.observable();
        s.correspondenceCity = ko.observable();
        s.useBizAddrAsCorrespAddr = ko.observable(false);
        s.telNum = ko.observable();
        s.mobileNum = ko.observable();
        s.faxNum = ko.observable();
        s.email = ko.observable();
        s.website = ko.observable();
        s.languagePreference = ko.observable();
        s.mainDestinations = ko.observable();
        s.username =  ko.computed(function() {
            return (this.accountCode()?this.accountCode():'') + "_ADMIN";
        }, this);
        s.countryName = ko.observable();
        s.orgTypeName = ko.observable();
        s.paDocs = ko.observableArray([]);
        s.status = ko.observable();
        s.update = function(pa){
            for(var k in pa) s.Data[k]=pa[k];
            s.id(pa.id);
            s.orgName(pa.orgName);
            s.accountCode(pa.accountCode);
            s.branchName(pa.branchName);
            s.uen(pa.uen);
            s.licenseNum(pa.licenseNum);
            s.licenseExpDate(pa.licenseExpDate);
            s.contactPerson(pa.contactPerson);
            s.contactDesignation(pa.contactDesignation);
            s.address(pa.address);
            s.postalCode(pa.postalCode);
            s.city(pa.city);
            s.telNum(pa.telNum);
            s.mobileNum(pa.mobileNum);
            s.faxNum(pa.faxNum);
            s.email(pa.email);
            s.website(pa.website);
            s.languagePreference(pa.languagePreference);
            s.mainDestinations(pa.mainDestinations);
            s.countryName(pa.countryName);
            s.orgTypeName(pa.orgTypeName);
            s.status(pa.status);
            var exts = pa['extension'];
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        if('correspondenceAddress' == name){
                            s.correspondenceAddress(attrVal);
                        }
                        if('correspondencePostalCode' == name){
                            s.correspondencePostalCode(attrVal);
                        }
                        if('correspondenceCity' == name){
                            s.correspondenceCity(attrVal);
                        }
                        if('useOneAddress' == name){
                            if('true' == ext['attrValue']){
                                s.useBizAddrAsCorrespAddr(true);
                            }else{
                                s.useBizAddrAsCorrespAddr(false);
                            }
                        }
                    }
                }
            }
            var paDocsLength = pa.paDocs != null && pa.paDocs != '' ? pa.paDocs.length : 0;
            for (var i = 0; i < paDocsLength; i++) {
                var tem = pa.paDocs[i];
                var o = new $page.PaDocModel(tem);
                s.paDocs.push(o);
            }
        }
        s.update(pa);
        s.next = function(){
            $('#paDetailDv').show();
            $('#paBasicDv').hide();
        };
        s.reject = function(){
            $page.openRejectPop();
        };
        s.resubmit = function(){
            $page.openResubmitPop();
        };
    };
    $page.CountryModel = function(data){
        var self = this;
        //Knockout 2.3.0 value can only be string
        self.id = ko.observable(data.ctyCode);
        self.ctyName = ko.observable(data.ctyName);
    };
    $page.DistributionModel = function(data){
        var s = this;
        s.percentage = ko.observable(data.percentage);
        s.countryId = ko.observable(data.countryId);
        s.countryNm = ko.observable(data.countryNm);
        s.adminId = ko.observable(data.adminId);
        s.adminNm = ko.observable(data.adminNm);
    };

    $page.DropdownModel = function(value,display){
        var self = this;
        self.optionValue = ko.observable(value);
        self.optionText = ko.observable(display);
    };

    $page.PaDetailModel = function(pa){
        var s = this;
        s.Data = pa;
        s.accountManagerId = ko.observable();
        s.subAccountEnabled = ko.observable();
        s.onlinePaymentEnabled = ko.observable(true);
        s.offlinePaymentEnabled = ko.observable(false);
        s.depoistEnabled = ko.observable(false);
        s.wotReservationEnabled = ko.observable();
        s.admins = ko.observableArray([]);
        s.revalFees = ko.observableArray([]);
        s.customerGroups = ko.observableArray([]);
        s.customerGroupId = ko.observable();
        s.revalPeriodMonths = ko.observable();
        s.tktRevalPeriods = ko.observableArray([]);
        var tktRevalP = new Number($page.ticketRevalidatePeriod);
        for(var i=0; i<= tktRevalP;i++){
            var tmpObj = new $page.DropdownModel(i,i);
            s.tktRevalPeriods.push(tmpObj);
        }
        s.revalFeeItemId = ko.observable();
        s.dailyTransCapStr = ko.observable();
        s.wotReservationCap = ko.observable();
        s.wotReservationValidUntil = ko.observable();

        s.capacityGroups  = ko.observableArray([]);
        s.capacityGroupId = ko.observable();

        s.remarks = ko.observable();
        s.tiers = ko.observableArray([]);
        s.tierId = ko.observable();
        s.iprods = ko.observableArray([]);
        s.prodNmFilter  = ko.observable();
        s.distributeCtys = ko.observableArray([]);
        s.distributeMgrs = ko.observableArray([]);
        s.selCty = ko.observable();
        s.selMgr = ko.observable();
        s.percentage = ko.observable(100);
        s.distributionModels = ko.observableArray([]);
        s.isPendingVerify = ko.computed(function(){
            if($page.paModel != undefined && $page.paModel != null && $page.paModel != '' && $page.paModel.status() != '' && $page.paModel.status() == 'PendingVerify'){
                return true;
            }
            return false;
        });
        var ctyVmsLength = $page.ctyVmsJson.length;
        for (var i = 0; i < ctyVmsLength; i++) {
            var o = new $page.CountryModel($page.ctyVmsJson[i]);
            s.distributeCtys.push(o);
        }
        var custGrpVmsLength = $page.customerGroupJson.length;
        for(var i = 0 ; i < custGrpVmsLength ; i++){
            var o = new $page.CustomerGroupModel($page.customerGroupJson[i]);
            s.customerGroups.push(o);
        }

        var capacityGroupVMsLength = $page.capacityGroupJson.length;
        for(var i = 0 ; i < capacityGroupVMsLength ; i++){
            var _o = $page.capacityGroupJson[i];
            if(_o != undefined && _o != null && _o != ''){
                var _id = _o['eventAllocationGroupID'];
                var _name = _o['name'];
                if(_id != undefined && _id != null && _id != '' && _name != undefined && _name != null && _name != ''){
                    var o = new $page.CapacityGroupModel($page.capacityGroupJson[i]);
                    s.capacityGroups.push(o);
                }else{
                    if(_id != undefined && _id != null && _id != ''){
                        var o = new $page.CapacityGroupModel($page.capacityGroupJson[i]);
                        s.capacityGroups.push(o);
                    }
                }
            }
        }

        s.update = function(pa){
            for(var k in pa) s.Data[k]=pa[k];
            s.accountManagerId(pa.accountManagerId);
            var exts = pa['extension'];
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        if('onlinePaymentEnabled' == name){
                            s.onlinePaymentEnabled(attrVal == 'true');
                        }
                        if('offlinePaymentEnabled' == name){
                            s.offlinePaymentEnabled(attrVal == 'true');
                        }
                        if('depoistEnabled' == name){
                            s.depoistEnabled(attrVal == 'true');
                        }
                        if('wotReservationEnabled' == name){
                            s.wotReservationEnabled(attrVal == 'true');
                        }
                        if('wotReservationCap' == name){
                            s.wotReservationCap(attrVal);
                        }
                        if('wotReservationValidUntil' == name){
                            s.wotReservationValidUntil(attrVal == 'true');
                        }
                        if('capacityGroupId' == name){
                            s.capacityGroupId(attrVal);
                        }
                    }
                }
            }
            var adminsLength = pa.adminVmsJson != null && pa.adminVmsJson != '' ? pa.adminVmsJson.length : 0;
            for (var i = 0; i < adminsLength; i++){
                var tem = pa.adminVmsJson[i];
                var o = new $page.AdminModel(tem);
                s.admins.push(o);
            };
            var revalLength = pa.revalItemVmsJson != null && pa.revalItemVmsJson != '' ? pa.revalItemVmsJson.length : 0;
            for (var i = 0; i < revalLength; i++){
                var tem = pa.revalItemVmsJson[i];
                var o = new $page.RevalFeeModel(tem);
                s.revalFees.push(o);
            };
            var tierLength = pa.tierVmsJson != null && pa.tierVmsJson != '' ? pa.tierVmsJson.length : 0;
            for (var i = 0; i < tierLength; i++) {
                var tem = pa.tierVmsJson[i];
                if(tem != undefined && tem != null && tem != ''){
                    var isB2B = tem['isB2B'];
                    if(isB2B != undefined && isB2B != null && isB2B != '' && (isB2B == 1 || isB2B == '1')){
                        var o = new $page.TierModel(tem);
                        s.tiers.push(o);
                    }
                }
            };
            if(pa.prodVms != null && pa.prodVms != ''){
                var prodLength = pa.prodVms.length;
                for (var i = 0; i < prodLength; i++) {
                    if(prodVms[i].id&&prodVms[i].displayTitle){
                        var tmpObj = prodVms[i];
                        var o = new $page.ProdModel(tmpObj.id,tmpObj.displayTitle,tmpObj.prodType,tmpObj.tiers);
                        $page.tierVm.iprods.push(o);
                    }
                }
            }
        }
        s.update(pa);
        s.back = function(){
            $('#paBasicDv').show();
            $('#paDetailDv').hide();
        };
        s.saveDistribution = function(){
            var idata = {percentage:s.percentage(),
                countryId:s.selCty().id(),
                countryNm:s.selCty().ctyName(),
                adminId:s.selMgr().id(),
                adminNm:s.selMgr().username()};
            var distribution = new $page.DistributionModel(idata);
            var total = 0;
            var duplicateCty = false;
            ko.utils.arrayForEach(s.distributionModels(), function(tmpMap) {
                total = total + Number(tmpMap.percentage());
                if(s.selCty().id() == tmpMap.countryId()){
                    duplicateCty = true;
                }
            });
            if(duplicateCty){
                alert("Same country already added.");
                return;
            }
            var newObj = Number(s.percentage());
            if((total+ newObj)>100){
                alert("Can not add more than 100%.");
                return;
            }else{
                s.distributionModels.push(distribution);
                s.percentage(100 - total - newObj);
            }
        };
        s.ctyChanged = function (obj, event) {
            if(s.selCty()){
                s.distributeMgrs.removeAll();
                if(s.selCty().loaded){
                    if(s.selCty().salesJson){
                        var adminsLength = s.selCty().salesJson.length;
                        for (var i = 0; i < adminsLength; i++){
                            var tem = s.selCty().salesJson[i];
                            var o = new $page.AdminModel(tem);
                            s.distributeMgrs.push(o);
                        };
                    }
                }else{
                    $page.getSalesAdmin(s.selCty());
                }
            }
        };
        s.filterProd = function(){
            var prodNm = s.prodNmFilter();
            ko.utils.arrayForEach(s.iprods(), function(item) {
                var displayTitle = item.displayTitle;
                if(displayTitle.indexOf(prodNm)< 0&&prodNm != ''){
                    item.showup(false);
                }else{
                    item.showup(true);
                }
            });
        };
        s.rmAllProd = function() {
            s.iprods.removeAll();
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.addAllProd = function() {
            $page.addAllProd();
        };
        s.addProd = function(id) {
            var obj = $('a[elementType="pending-adding-products"][prodId="'+id+'"][prodType][prodName]');
            var name = $(obj).attr('prodName');
            var type = $(obj).attr('prodType');
            var tiers = $(obj).attr('tiers');
            var o = new $page.ProdModel(id,decodeURIComponent(name),type,tiers);
            s.iprods.push(o);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.rmProd = function() {
            s.iprods.remove(this);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.submitVerify = function(){
            $page.submitPa();
        };
        s.rmDistribution = function() {
            s.distributionModels.remove(this);
            var total = 0;
            ko.utils.arrayForEach(s.distributionModels(), function(tmpMap) {
                total = total + Number(tmpMap.percentage());
            });
            s.percentage(100 - total);
        };
    };
    $page.getSalesAdmin = function(cty){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-sales-admin?ctyId='+cty.id(),
            data: {},
            success:  function(ajxResp, textStatus, jqXHR)
            {
                if (ajxResp.success) {
                    if(ajxResp['data'] != null && ajxResp['data'] != ''){
                        var salesJson = JSON.parse(ajxResp['data']);
                        cty.salesJson = salesJson;
                        cty.loaded = true;
                        if(cty.salesJson){
                            var adminsLength = cty.salesJson.length;
                            for (var i = 0; i < adminsLength; i++){
                                var tem = cty.salesJson[i];
                                var o = new $page.AdminModel(tem);
                                $page.paDetailModel.distributeMgrs.push(o);
                            };
                        }
                    }
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.addAllProd= function(){
        nvx.spinner.start();
        $.ajax({
            type: 'POST',
            url: $page.baseProdUrl,
            data: {},
            success:  function(ajxResp, textStatus, jqXHR)
            {
                if (ajxResp.success) {
                    if(ajxResp['data'] != null && ajxResp['data'] != ''){
                        var lst = JSON.parse(ajxResp['data']);
                        if(lst != null && lst.length > 0){
                            var len = lst.length;
                            for(var i = 0  ; i < len ; i++){
                                var tmp = lst[i];
                                var id = tmp['id'];
                                if(id != undefined && id != null && id != ''){
                                    tmp['id'] = id.trim();
                                }
                                tmp['displayTitle'] = tmp['name'];
                                var o = new $page.ProdModel(tmp.id,tmp.displayTitle,tmp.prodType,tmp.tiers);
                                $page.paDetailModel.iprods.push(o);
                            }
                        }
                    }
                    alert(ajxResp.total+"  products will be assigned to this partner.");
                    $page.refreshProd();
                    $page.prodDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.AdminModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.username = ko.observable(data.username);
    }
    $page.PaDocModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.fileName = ko.observable(data.fileName);
        s.fileTypeLabel = ko.observable(data.fileTypeLabel);
        s.url = ko.observable(nvx.API_PREFIX + '/partner-management/attachment/download?id='+data.id);
    }
    $page.RevalFeeModel = function(data){
        var s = this;
        s.id = ko.observable(data.itemId);
        s.priceStr = ko.observable(data.priceStr);
    }
    $page.TierModel = function(data){
        var s = this;
        s.id = ko.observable(data.priceGroupId);
        s.name = ko.observable(data.name);
    }
    $page.ProdModel = function(id,displayTitle,prodType, tiers){
        var self = this;
        self.showup = ko.observable(true);
        self.id = id;
        self.displayTitle = displayTitle;
        self.prodType = prodType;
        self.tiers = tiers;
        self.prodTypeWithTier = self.prodType + '('+ self.tiers +')';
    };
    $page.ReasonModel = function(data){
        var self = this;
        self.id = data.id;
        self.title = data.title;
    };
    $page.CustomerGroupModel = function(data){
        var self = this;
        self.id = data.customerGroupNumber;
        self.name = data.customerGroupName;
    };
    $page.CapacityGroupModel = function(data){
        var self = this;
        var _id = data['eventAllocationGroupID'];
        var _name = data['name'];
        if(_id != undefined && _id != null && _id != '' && _name != undefined && _name != null && _name != ''){
            self.id = _id;
            self.name = _name;
        }else{
            if(_id != undefined && _id != null && _id != ''){
                self.id = _id;
                self.name = _id;
            }
        }
    };

    $page.RejectModel = function(){
        var s = this;
        s.reasonId = ko.observable();
        s.remarks = ko.observable();
        s.reasons = ko.observableArray([]);
        var reasonLength = $page.reasonVmsJson.length;
        for (var i = 0; i < reasonLength; i++) {
            var tem = $page.reasonVmsJson[i];
            var o = new $page.ReasonModel(tem);
            s.reasons.push(o);
        };
        s.save = function(){
            $page.rejectPa();
        };
    };

    $page.ReSubmitModel = function(){
        var s = this;
        s.reasonId = ko.observable();
        s.remarks = ko.observable();
        s.reasons = ko.observableArray([]);
        var reasonLength = $page.reasonVmsJson.length;
        for (var i = 0; i < reasonLength; i++) {
            var tem = $page.reasonVmsJson[i];
            var o = new $page.ReasonModel(tem);
            s.reasons.push(o);
        };
        s.save = function(){
            $page.resubmitPa();
        };
    };

    $page.getProdsJson = function(data){
        var filter = {prodIds:[]};
        $.each(data, function(i, item) {
            if(item.id){
                filter.prodIds.push(item.id);
            }
        });
        return filter;
    };
    $page.refreshProd = function(isfilter){
        var prods = $page.paDetailModel.iprods();
        var filter = $page.getProdsJson(prods);
        filter.prodName = $('#prodNmFilter').val();
        var filterJson = JSON.stringify(filter);
        $page.prodDS.options.transport.read.url = $page.baseProdUrl;
        $page.prodDS.options.transport.read.data = {'prodfilterJson':filterJson};
        if(isfilter){
            $page.prodDS.read();
        }
    };
    $page.initValidUntil = function(){
        $("#partnerWoTReservationValidUntil").kendoDatePicker({
            format: $page.DateFormat
        }).data('kendoDatePicker');
    };
    $page.initProductGrid = function(){
        $page.productGrid = $("#productGrid").kendoGrid({
            dataSource: $page.prodDS,
            height: '320px',
            columns: [{
                title: "Add",
                template:
                    '<a elementType="pending-adding-products" href="javascript:nvxpage.paDetailModel.addProd(\'#=id#\')" tiers=\"#=tiers#\" prodId=\"#=id#\" prodType=\"#=prodType#\" prodName=\"#=encodeURIComponent(displayTitle)#\" class="round-bt"><i  class="fa fa-plus-circle"></i></a>'
                ,
                sortable: false,
                width: 50
            },{
                field: "displayTitle",
                title: "Product Name",
                width: 200
            },{
                field: "prodType",
                title: "Type",
                width: 500,
                template : '<span>#=prodType# (#=tiers#)</span>'
            }
            ],
            sortable: $page.serverSorting, pageable: true
        }).data('kendoGrid');
    };
    $page.openResubmitPop = function(data){
        var htmlcontent =$page.resubmitReasonPopTemp({});
        if(!$("#tempResubmitWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'tempResubmitWindow');
        }
        var tempWindow = $("#tempResubmitWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Resubmit',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.resubmitModel = new $page.ReSubmitModel();
        ko.applyBindings($page.resubmitModel,$('#resubmitReasonPopTempForm')[0]);
        var selReasons = $("#selResubmitReasons").kendoMultiSelect({
            dataSource: $page.reasonVmsJson,
            dataTextField: "title",
            dataValueField: "id"
        }).data("kendoMultiSelect");
        tempWindow.center().open();
    };
    $page.openRejectPop = function(data){
        var htmlcontent =$page.reasonPopTemp({});
        if(!$("#tempWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'tempWindow');
        }
        var tempWindow = $("#tempWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Reject',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.rejModel = new $page.RejectModel();
        ko.applyBindings($page.rejModel,$('#reasonForm')[0]);
        var selReasons = $("#selReasons").kendoMultiSelect({
            dataSource: $page.reasonVmsJson,
            dataTextField: "title",
            dataValueField: "id"
        }).data("kendoMultiSelect");
        tempWindow.center().open();
    };
    $page.closePopById = function(id){
        var dialog = $("#"+id+"").data("kendoWindow");
        dialog.destroy();
    };
    $page.rejectPa = function(){
        var reason = {};
        var selReasons = $("#selReasons").data("kendoMultiSelect");
        reason.reasonIds = selReasons.value();
        reason.remarks = $page.rejModel.remarks();
        if(reason.reasonIds.length < 1){
            alert("Please select a reason.");
            return;
        }
        if(!$page.rejModel.remarks()){
            alert("Please key in remarks.");
            return;
        }
        var reasonVmJson = JSON.stringify(reason);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/verify/reject',
            data: {'reasonVmJson' : reasonVmJson, 'id' : $page.paModel.id() },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.backToPartnerListingPage();
                } else {
                    try{
                        alert(data.message);
                    }catch(e){alert(e);}
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.backToPartnerListingPage = function(){
        if($('#backToPreviousPage').size() > 0){
            $('#backToPreviousPage')[0].click();
        }
    };
    $page.resubmitPa = function(){
        var reason = {};
        var selReasons = $("#selResubmitReasons").data("kendoMultiSelect");
        reason.reasonIds = selReasons.value();
        reason.remarks = $page.resubmitModel.remarks();
        if(reason.reasonIds.length < 1){
            alert("Please select a reason.");
            return;
        }
        if(!$page.resubmitModel.remarks()){
            alert("Please key in remarks.");
            return;
        }
        var reasonVmJson = JSON.stringify(reason);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/verify/resubmit',
            data: {'reasonVmJson' : reasonVmJson, 'id' : $page.paModel.id() },
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.backToPartnerListingPage();
                } else {
                    try{
                        alert(data.message);
                    }catch(e){alert(e);}
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.submitPa = function(){
        var partner = {};
        partner.id = $page.paModel.id();
        partner.accountManagerId = $page.paDetailModel.accountManagerId();
        partner.subAccountEnabled = $page.paDetailModel.subAccountEnabled();
        partner.revalPeriodMonths = $page.paDetailModel.revalPeriodMonths();
        partner.revalFeeItemId = $page.paDetailModel.revalFeeItemId();
        partner.dailyTransCapStr = $page.paDetailModel.dailyTransCapStr();
        partner.tierId = $page.paDetailModel.tierId();
        partner.remarks = $page.paDetailModel.remarks();

        partner.excluProdIds = [];
        ko.utils.arrayForEach($page.paDetailModel.iprods(), function(prod) {
            partner.excluProdIds.push(prod.id);
        });
        partner.distributionMapping = [];
        var total = 0;
        ko.utils.arrayForEach($page.paDetailModel.distributionModels(), function(tmpMap) {
            var tmpMapJs = {percentage:tmpMap.percentage(),countryId:tmpMap.countryId(),adminId:tmpMap.adminId()};
            partner.distributionMapping.push(tmpMapJs);
            total = total + Number(tmpMap.percentage());
        });

        if(total < 100){
            alert("Market Distribution Country doesn't much 100%.");
            return;
        }
        if(!partner.accountManagerId){
            alert("Please select an Account Manager.");
            return;
        }
        if(!($page.paDetailModel.customerGroupId() != null && $page.paDetailModel.customerGroupId() != '' && $page.paDetailModel.customerGroupId() != ' ')){
            alert("Please select a Customer Group.");
            return;
        }
        var numReg = /^\d+$/;
        if(!numReg.test(partner.revalPeriodMonths)){
            alert("Please fill in a valid Revalidation Period.");
            return;
        }
        var revMonth = new Number(partner.revalPeriodMonths);
        var tktRevalP = new Number($page.ticketRevalidatePeriod);
        if(revMonth>tktRevalP){
            alert("Revalidation Period should be less than "+tktRevalP+" months.");
            return;
        }
        if(!partner.revalFeeItemId){
            alert("Please select a Revalidation Fee.");
            return;
        }
        if(!partner.dailyTransCapStr){
            alert("Please fill in the Daily Transaction Caps.");
            return;
        }
        var isValid = partner.dailyTransCapStr.search(/^\$?[\d,]+(\.\d{1,2})?$/) >= 0;
        if(!isValid){
            alert("Please fill in the Daily Transaction Caps with correct format.");
            return;
        }
        if(!partner.tierId){
            alert("Please select a Tier Level.");
            return;
        }
        var _capacityGroupId = $page.paDetailModel.capacityGroupId();
        if(!(_capacityGroupId != undefined && _capacityGroupId != null && _capacityGroupId != '')){
            alert("Please select a  Capacity Group.");
            return;
        }
        var _wotReservationCap = $page.paDetailModel.wotReservationCap();
        var _wotReservationValidUntil = $page.paDetailModel.wotReservationValidUntil();
        if('true' == $page.paDetailModel.wotReservationEnabled()){
            if(!(_wotReservationCap != undefined && _wotReservationCap != null && _wotReservationCap != '')){
                alert("Please fill in the Wings of Time Reservation Cap.");
                return;
            }
            if(isNaN(_wotReservationCap)){
                alert("Please fill in the Wings of Time Reservation Cap with correct value.");
                return;
            }
            if(parseInt(_wotReservationCap) <= 0){
                alert("Please fill in the Wings of Time Reservation Cap with correct value.");
                return;
            }
        }else{
            _wotReservationCap = '';
            _wotReservationValidUntil = '';
        }
        if(!partner.remarks){
            alert("Please fill in the remarks.");
            return;
        }

        partner.extension = [];
        partner.extension[partner.extension.length] = { 'attrName' : 'customerGroup', 'attrValue' : $page.paDetailModel.customerGroupId(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'onlinePaymentEnabled', 'attrValue' : $page.paDetailModel.onlinePaymentEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'offlinePaymentEnabled', 'attrValue' : $page.paDetailModel.offlinePaymentEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'depoistEnabled', 'attrValue' : $page.paDetailModel.depoistEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationEnabled', 'attrValue' : $page.paDetailModel.wotReservationEnabled(), 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationCap', 'attrValue' : _wotReservationCap, 'attrValueType' : 'INT', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true };
        partner.extension[partner.extension.length] = { 'attrName' : 'wotReservationValidUntil', 'attrValue' : _wotReservationValidUntil, 'attrValueType' : 'DATE', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : true, 'attrValueFormat' : $page.DateFormat };
        partner.extension[partner.extension.length] = { 'attrName' : 'capacityGroupId', 'attrValue' : _capacityGroupId, 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

        var paVmJson = JSON.stringify(partner);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/verify/submit',
            data: {'paVmJson':paVmJson},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.backToPartnerListingPage();
                } else {
                    alert(data.message);
                    nvx.spinner.stop();
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
                nvx.spinner.stop();
            },
            complete: function() {

            }
        });
    };

    $page.processPageDataResponse = function(data, status, jqXHR){
        if(data != undefined) {
            $page.paVmJson = data['partnerVM'];
            $page.paVmJson.adminVmsJson = data['adminAccountVms'];
            $page.paVmJson.revalItemVmsJson = data['revalFeeItemVMs'];
            /** $page.paVmJson.revalItemVmsJson = data['revalFeeItemVMs']; **/
            $page.ticketRevalidatePeriod = data['ticketRevalidatePeriod'];
            $page.reasonVmsJson = data['reasonVms'];
            $page.paVmJson.tierVmsJson = data['tierVMs'];
            $page.ctyVmsJson = data['ctyVMs'];
            $page.customerGroupJson = data['customerGroupList'];
            $page.capacityGroupJson = data['capacityGroupList'];

            $page.paModel = new nvxpage.PartnerModel(nvxpage.paVmJson);
            $page.paDetailModel = new nvxpage.PaDetailModel(nvxpage.paVmJson);
            $('#paBasicDv').html($page.paBasicTemp({}));
            $('#paDetailDv').html($page.paDetailTemp({}));
            ko.applyBindings($page.paModel, $('#paBasicDv')[0]);
            ko.applyBindings($page.paDetailModel, $('#paDetailDv')[0]);
            // init date/time fields
            $page.initValidUntil();
            // init product grid
            $page.initProductGrid();
            // refresh product grid
            $page.refreshProd();
        }
    };
    $page.initPageData = function(){
        var paId = -1;
        var params = window.location.href;
        if(params != null && params != ''){
            var paIdIdx = params.indexOf('paId=');
            if(paIdIdx != null && paIdIdx > 0){
                try{
                    paId = parseInt(params.substr(paIdIdx + 5).trim());
                }catch(e){}
            }
        }
        if(paId > 0){
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/partner-management/get-partner-details',
                data : {
                    id : paId
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        $page.processPageDataResponse(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            var errorMsg = nvx.getDefinedMsg(data.message);
                            if(errorMsg != undefined && errorMsg != ''){
                                alert(errorMsg);
                            }else{
                                alert(data.message);
                                window.location.replace('/.resources/partner-portal-admin/partner-management/partner-list.html');
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        }
    };


    window.nvxpage = $page;

    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/

});