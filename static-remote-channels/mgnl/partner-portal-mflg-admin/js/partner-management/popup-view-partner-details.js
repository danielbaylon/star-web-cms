var $pop = {};

$pop.bounded = 0;
$pop.paDetailTemp = '';
$pop.DistributionModel = function(data){
    var s = this;
    s.percentage = ko.observable(data.percentage);
    s.countryId = ko.observable(data.countryId);
    s.countryNm = ko.observable(data.countryNm);
    s.adminId = ko.observable(data.adminId);
    s.adminNm = ko.observable(data.adminNm);
};
$pop.PartnerModel = function(pa) {
    var s = this;
    //s.Data = pa;
    // s.id = ko.observable(pa.id);
    // s.orgName = ko.observable(pa.orgName);
    // s.accountCode = ko.observable(pa.accountCode);
    // s.branchName = ko.observable(pa.branchName);
    // s.uen = ko.observable(pa.uen);
    // s.licenseNum = ko.observable(pa.licenseNum);
    // s.licenseExpDate = ko.observable(pa.licenseExpDate);
    // s.registrationYear = ko.observable(pa.registrationYear);
    // s.contactPerson = ko.observable(pa.contactPerson);
    // s.contactDesignation = ko.observable(pa.contactDesignation);
    // s.address = ko.observable(pa.address);
    // s.postalCode = ko.observable(pa.postalCode);
    // s.city = ko.observable(pa.city);
    // s.correspondenceAddress = ko.observable(pa.correspondenceAddress);
    // s.correspondencePostalCode = ko.observable(pa.correspondencePostalCode);
    // s.correspondenceCity = ko.observable(pa.correspondenceCity);
    // s.telNum = ko.observable(pa.telNum);
    // s.mobileNum = ko.observable(pa.mobileNum);
    // s.faxNum = ko.observable(pa.faxNum);
    // s.email = ko.observable(pa.email);
    // s.website = ko.observable(pa.website);
    // s.languagePreference = ko.observable(pa.languagePreference);
    // s.mainDestinations = ko.observable(pa.mainDestinations);
    // s.username =  ko.computed(function() {
    //     return (this.accountCode()?this.accountCode():'') + "_ADMIN";
    // }, this);
    // s.countryName = ko.observable(pa.countryName);
    // s.orgType = ko.observable(pa.orgTypeName);
    // s.paDocs = ko.observableArray([]);
    // s.excluProds = ko.observableArray([]);
    // s.tierLabel = ko.observable(pa.tierLabel);
    // s.remarks  = ko.observable(pa.remarks);
    // s.revalFeeStr = ko.observable(pa.revalFeeStr);
    // s.accountManager = ko.observable(pa.accountManager);
    // s.revalPeriodMonths = ko.observable(pa.revalPeriodMonths);
    // s.dailyTransCapStr = ko.observable(pa.dailyTransCapStr);
    // s.subAccountEnabled = ko.observable(pa.subAccountEnabled);
    // s.onlinePaymentEnabled = ko.observable(true);
    // s.offlinePaymentEnabled = ko.observable(false);
    // s.depoistEnabled = ko.observable(false);
    // s.wotReservationEnabled = ko.observable();
    // s.canApprove  = ko.observable(false);
    // s.showRemarks = ko.observable(false);
    // s.showRejectRemarks = ko.observable(false);
    // s.paHist = ko.observableArray([]);
    // s.distributionModels = ko.observableArray([]);
    // s.createdBy = ko.observable(pa.createdBy);
    // s.createdDateStr = ko.observable(pa.createdDateStr);
    //
    // s.customerGroupId = ko.observable();
    // s.customerGroupLabel = ko.observable();
    //
    // s.wotReservationCap = ko.observable();
    // s.wotReservationValidUntil = ko.observable();
    //
    //
    // if($pop.applogVmsJson){
    //     var applogVmsJsonLength = $pop.applogVmsJson.length;
    //     console.log("$pop.applogVmsJson.length:"+$pop.applogVmsJson.length);
    //     for (var i = 0; i < applogVmsJsonLength; i++) {
    //         var tem = $pop.applogVmsJson[i];
    //         var o = new $pop.PaHistModel(tem);
    //         s.paHist.push(o);
    //     }
    // }
    // if($pop.applogVmJson){s.createdBy($pop.applogVmJson.createdBy);}
    // if($pop.applogVmJson){s.createdDateStr($pop.applogVmJson.createdDateStr);}
    // if(pa.paDocs){
    //     var paDocsLength = pa.paDocs.length;
    //     for (var i = 0; i < paDocsLength; i++) {
    //         var tem = pa.paDocs[i];
    //         var o = new $pop.PaDocModel(tem);
    //         s.paDocs.push(o);
    //     }
    // }
    // if(pa.excluProds){
    //     var excluProdsLength = pa.excluProds.length;
    //     for (var i = 0; i < excluProdsLength; i++) {
    //         var tem = pa.excluProds[i];
    //         var o = new $pop.ProdModel(tem);
    //         s.excluProds.push(o);
    //     }
    // }
    // if(pa.distributionMapping){
    //     var pdMappingLength = pa.distributionMapping.length;
    //     for (var i = 0; i < pdMappingLength; i++) {
    //         if(pa.distributionMapping[i].percentage){
    //             var idata = {percentage:pa.distributionMapping[i].percentage,
    //                 countryId:pa.distributionMapping[i].id,
    //                 countryNm:pa.distributionMapping[i].countryNm,
    //                 adminId:pa.distributionMapping[i].adminId,
    //                 adminNm:pa.distributionMapping[i].adminNm};
    //             var distribution = new $pop.DistributionModel(idata);
    //             s.distributionModels.push(distribution);
    //         }
    //     }
    s.id = ko.observable();
    s.orgName = ko.observable();
    s.accountCode = ko.observable();
    s.branchName = ko.observable();
    s.uen = ko.observable();
    s.licenseNum = ko.observable();
    s.licenseExpDate = ko.observable();
    s.registrationYear = ko.observable();
    s.contactPerson = ko.observable();
    s.contactDesignation = ko.observable();
    s.address = ko.observable();
    s.postalCode = ko.observable();
    s.city = ko.observable();
    s.correspondenceAddress = ko.observable();
    s.correspondencePostalCode = ko.observable();
    s.correspondenceCity = ko.observable();
    s.telNum = ko.observable();
    s.mobileNum = ko.observable();
    s.faxNum = ko.observable();
    s.email = ko.observable();
    s.website = ko.observable();
    s.languagePreference = ko.observable();
    s.mainDestinations = ko.observable();
    s.username = ko.computed(function () {
        return (this.accountCode() ? this.accountCode() : '') + "_ADMIN";
    }, this);
    s.countryName = ko.observable();
    s.orgType = ko.observable();
    s.paDocs = ko.observableArray([]);
    s.excluProds = ko.observableArray([]);
    s.tierLabel = ko.observable();
    s.remarks = ko.observable();
    s.revalFeeStr = ko.observable();
    s.accountManager = ko.observable();
    s.revalPeriodMonths = ko.observable();
    s.dailyTransCapStr = ko.observable();
    s.subAccountEnabled = ko.observable();
    s.onlinePaymentEnabled = ko.observable(true);
    s.offlinePaymentEnabled = ko.observable(false);
    s.depoistEnabled = ko.observable(false);
    s.wotReservationEnabled = ko.observable();
    s.canApprove = ko.observable(false);
    s.showRemarks = ko.observable(false);
    s.showRejectRemarks = ko.observable(false);
    s.paHist = ko.observableArray([]);
    s.distributionModels = ko.observableArray([]);
    s.createdBy = ko.observable();
    s.createdDateStr = ko.observable();

    s.customerGroupId = ko.observable();
    s.customerGroupLabel = ko.observable();
    s.capacityGroupId = ko.observable();
    s.capacityGroupLabel = ko.observable();

    s.wotReservationCap = ko.observable();
    s.wotReservationValidUntil = ko.observable();

    s.update = function (pa) {
        //for(var k in pa) s.Data[k]=pa[k];
        //s.Data = pa;
        s.id(pa.id);
        s.orgName(pa.orgName);
        s.orgType(pa.orgTypeName);
        s.accountCode(pa.accountCode);
        s.branchName(pa.branchName);
        s.uen(pa.uen);
        s.licenseNum(pa.licenseNum);
        s.licenseExpDate(pa.licenseExpDate);
        s.contactPerson(pa.contactPerson);
        s.contactDesignation(pa.contactDesignation);
        s.address(pa.address);
        s.postalCode(pa.postalCode);
        s.city(pa.city);
        s.telNum(pa.telNum);
        s.mobileNum(pa.mobileNum);
        s.faxNum(pa.faxNum);
        s.email(pa.email);
        s.website(pa.website);
        s.languagePreference(pa.languagePreference);
        s.mainDestinations(pa.mainDestinations);
        s.countryName(pa.countryName);
        s.tierLabel(pa.tierLabel);
        s.createdBy(pa.createdBy);
        s.createdDateStr(pa.createdDateStr);
        s.remarks(pa.remarks);
        s.revalFeeStr(pa.revalFeeStr);
        s.accountManager(pa.accountManager);
        s.revalPeriodMonths(pa.revalPeriodMonths);
        s.dailyTransCapStr(pa.dailyTransCapStr);
        s.subAccountEnabled(pa.subAccountEnabled);
        s.offlinePaymentEnabled(pa.offlinePaymentEnabled);

        var exts = pa['extension'];
        if (exts != undefined && exts != null && exts.length > 0) {
            var extlen = exts.length;
            for (var i = 0; i < extlen; i++) {
                var ext = exts[i];
                if (ext != undefined && ext != null && ext != '') {
                    var name = ext['attrName'];
                    var attrVal = ext['attrValue'];
                    if ('onlinePaymentEnabled' == name) {
                        s.onlinePaymentEnabled(attrVal == 'true');
                    }
                    if ('offlinePaymentEnabled' == name) {
                        s.offlinePaymentEnabled(attrVal == 'true');
                    }
                    if ('depoistEnabled' == name) {
                        s.depoistEnabled(attrVal == 'true');
                    }
                    if ('wotReservationEnabled' == name) {
                        s.wotReservationEnabled(attrVal == 'true');
                    }
                    if ('wotReservationCap' == name) {
                        s.wotReservationCap(attrVal);
                    }
                    if ('wotReservationValidUntil' == name) {
                        s.wotReservationValidUntil(attrVal);
                    }
                    if ('correspondenceAddress' == name) {
                        s.correspondenceAddress(attrVal);
                    }
                    if ('correspondencePostalCode' == name) {
                        s.correspondencePostalCode(attrVal);
                    }
                    if ('correspondenceCity' == name) {
                        s.correspondenceCity(attrVal);
                    }

                    if ('customerGroup' == name) {
                        s.customerGroupId(attrVal);
                        s.customerGroupLabel(ext['attrValueLabel']);
                    }
                    if ('capacityGroupId' == name) {
                        s.capacityGroupId(attrVal);
                        s.capacityGroupLabel(ext['attrValueLabel']);
                    }

                }
            }
        }
        if ($pop.applogVmJson) {
            s.createdBy($pop.applogVmJson.createdBy);
        }
        if ($pop.applogVmJson) {
            s.createdDateStr($pop.applogVmJson.createdDateStr);
        }
        s.paDocs([]);
        if (pa.paDocs) {
            var paDocsLength = pa.paDocs.length;
            for (var i = 0; i < paDocsLength; i++) {
                var tem = pa.paDocs[i];
                var o = new $pop.PaDocModel(tem);
                s.paDocs.push(o);
            }
        }
        s.excluProds([]);
        if (pa.excluProds) {
            var excluProdsLength = pa.excluProds.length;
            for (var i = 0; i < excluProdsLength; i++) {
                var tem = pa.excluProds[i];
                var o = new $pop.ProdModel(tem);
                s.excluProds.push(o);
            }
        }
        s.distributionModels([]);
        if (pa.distributionMapping) {
            var pdMappingLength = pa.distributionMapping.length;
            for (var i = 0; i < pdMappingLength; i++) {
                if (pa.distributionMapping[i].percentage) {
                    var idata = {
                        percentage: pa.distributionMapping[i].percentage,
                        countryId: pa.distributionMapping[i].id,
                        countryNm: pa.distributionMapping[i].countryNm,
                        adminId: pa.distributionMapping[i].adminId,
                        adminNm: pa.distributionMapping[i].adminNm
                    };
                    var distribution = new $pop.DistributionModel(idata);
                    s.distributionModels.push(distribution);
                }
            }
        }
    };
    //s.update(pa);
};
$pop.PaDocModel = function(data){
    var s = this;
    s.id = ko.observable(data.id);
    s.fileName = ko.observable(data.fileName);
    s.fileTypeLabel = ko.observable(data.fileTypeLabel);
    s.url = ko.observable(nvx.API_PREFIX + '/partner-management/attachment/download?id='+data.id);
};
$pop.PaHistModel = function(data){
    var s = this;
    s.id = ko.observable(data.id);
    s.createdDateStr = ko.observable(data.createdDateStr);
    s.createdBy = ko.observable(data.createdBy);
    s.description = ko.observable(data.description);
};

//$pop.paModel = new $pop.PartnerModel($pop.paVmJson);
$pop.ProdModel = function(data){
    var s = this;
    s.name = ko.observable(data.name);
};

$pop.bindingPageWithData = function(data){
    $pop.paVmJson = data;
    $pop.canApprove = data['canApprove'];
    $pop.showRemarks = data['showRemarks'];
    $pop.showRejectRemarks = data['showRejectRemarks'];
    $pop.applogVmJson = data['appLogVM'];
    $pop.reasonVmsJson = data['reasonVM'];
    $pop.paModel.update($pop.paVmJson);
    //$pop.paModel = new $pop.PartnerModel($pop.paVmJson);
    $pop.paModel.showRemarks(data['showRemarks'] != undefined && data['showRemarks'] != null && data['showRemarks'] != '' && data['showRemarks'] == true);
    $pop.paModel.showRejectRemarks(data['showRejectRemarks'] != undefined && data['showRejectRemarks'] != null && data['showRejectRemarks'] != '' && data['showRejectRemarks'] == true);
    //$('#paDetailDv').html($pop.paDetailTemp({}));

    $pop.showPopupWindowWithContent();
};

$pop.showPopupWindowWithContent = function(){
    if($pop.detailWindow == null) {
        $pop.detailWindow = $('#paDetailDv').kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: "Detail",
            actions: ["Close"],
            viewable: false
        }).data('kendoWindow');
    }
    $pop.detailWindow.center().open();

};

$pop.initPageDisplay = function(paId){
    if(paId != undefined && paId != null && paId != ''){
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/partner-management/view-p-details',
            data : { 'id' :  paId },
            beforeSend : function(){
                nvx.spinner.start();
            },
            success : function(ajxResp, textStatus, jqXHR){
                if(ajxResp !=undefined && ajxResp != null){
                    if(ajxResp['success'] != null && ajxResp['success'] != '' && ajxResp['success'] == true){
                        var data = ajxResp['data'];
                        if(data != null && data != ''){
                            $pop.bindingPageWithData(data);
                        }
                    }else{
                        alert(ajxResp['message']);
                    }
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    }
};
$pop.paModel = new $pop.PartnerModel();
$pop.initPopPage = function(appId){
    if ($pop.bounded == 0) {
        $('#paDetailDv').html('');
        var tempHtml = $('#paPopDetailTemp').html();
        $pop['paDetailTemp'] = kendo.template(tempHtml);
        $('#paDetailDv').html($pop.paDetailTemp({}));
        ko.applyBindings($pop.paModel,$('#paDetailDv')[0]);
    }
    if ($pop.bounded == 0) {
        $pop.bounded = 1;
    }
    $pop.initPageDisplay(appId);
};


// $page.viewDetail = function(paId){
//     var htmlcontent;
//     nvx.spinner.start();
//     $.ajax({
//         type: "POST",
//         async: false,
//         url: nvx.API_PREFIX + '/partner-management/view-p-details',
//         data: {'id' : paId },
//         success:  function(data, textStatus, jqXHR)
//         {
//             if(typeof data.error === 'undefined')
//             {
//                 if(!$("#detailWindow").length){
//                     var $div = $('<div />').appendTo('body');
//                     $div.attr('id', 'detailWindow');
//                 };
//                 htmlcontent = data;
//                 $page.detailWindow = $("#detailWindow").kendoWindow({
//                     width: '800px',
//                     modal: true,
//                     resizable: false,
//                     title: "Detail",
//                     actions: ["Close"],
//                     viewable : false,
//                     deactivate: function() {
//                         this.destroy();
//                     }
//                 }).data('kendoWindow');
//                 $page.detailWindow.content(htmlcontent);
//                 $page.detailWindow.center().open();
//             }
//             else
//             {
//                 console.log('ERRORS: ' + data.error);
//             }
//         },
//         error: function(jqXHR, textStatus)
//         {
//             alert('Unable to process your request. Please try again in a few moments.');
//         },
//         complete: function() {
//             nvx.spinner.stop();
//         }
//     });
//};