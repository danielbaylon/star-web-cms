var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

nvx.ChatHist = function(data){
    var s = this;
    s.username = ko.observable(data.username);
    s.content = ko.observable(data.content);
    s.createDtStr = ko.observable(data.createDtStr);
    s.usertype = ko.observable(data.usertype);
};

nvx.OfflinePaymentApprovalModel = function(pageWinDoc) {
    var s = this;
    s.parentController = pageWinDoc;
    s.partner = ko.observable(new nvx.PartnerProflePreviewModel());
    s.cart    = ko.observable(new nvx.CheckoutView());
    s.offlinePay = ko.observable(new nvx.OfflinePaymentModel());

    s.initOfflinePaymentApprovalModel = function(data){
        var cart = data['cart'];
        if(cart != undefined && cart != null && cart != ''){
            var partner = cart['partner'];
            if(partner != undefined && partner != null && partner != ''){
                s.partner().initPartnerProflePreviewModel(partner);
            }
            s.cart().initCheckoutView(cart);
        }
        var paymentDetail = data['paymentDetail'];
        if(paymentDetail != undefined && paymentDetail != null && paymentDetail != '' ){
            s.offlinePay().initOfflinePaymentModel(paymentDetail);
        }
    };

    s.reset = function(){
        s.cart().reset();
        s.partner().reset();
        s.offlinePay().reset();
    };
}