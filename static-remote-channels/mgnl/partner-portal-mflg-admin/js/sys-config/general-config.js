$(document).ready(function() {
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    var $page = {};
    $page.DateTimeFormat = 'dd/MM/yyyy HH:mm';
    
    $page.generalTemp = kendo.template($("#generalTemp").html());
    $page.bkApproverTemp = kendo.template($("#bkApproverTemp").html());
    $page.reasonTemp = kendo.template($("#reasonTemp").html());
    $page.reasonPopTemp = kendo.template($("#reasonPopTemp").html());
    $page.partnerTypeTemp = kendo.template($("#partnerTypeTemp").html());
    $page.ptTypePopTemp = kendo.template($("#ptTypePopTemp").html());
    $page.regionTemp = kendo.template($("#regionTemp").html());
    $page.regionPopTemp = kendo.template($("#regionPopTemp").html());
    $page.ctyTemp = kendo.template($("#ctyTemp").html());
    $page.ctyPopTemp = kendo.template($("#ctyPopTemp").html());
    $page.revalFeeTemp = kendo.template($("#revalFeeTemp").html());
    $page.revalFeePopTemp = kendo.template($("#revalFeePopTemp").html());
    $page.otherItemTemp = kendo.template($("#otherItemTemp").html());
    $page.inventoryTemp = kendo.template($("#inventoryTemp").html());
    $page.gstTemp = kendo.template($("#gstTemp").html());
    $page.gstPopTemp = kendo.template($("#gstPopTemp").html());
    $page.wotTemp = kendo.template($("#wotTemp").html());
    $page.wotProdPopTemp = kendo.template($("#wotProdPopTemp").html());
    $page.wotOtherItemTemp = kendo.template($("#wotOtherItemTemp").html());
    $page.loadLabel = "Loading..";
    $page.GeneralLabel = "General";
    $page.ReasonLabel = "Reason List";
    $page.PartnerTypeLabel = "Partner Type";
    $page.RegionLabel = "Region List";
    $page.CountryLabel = "Country List";
    $page.RevalItemLabel = "Revalidation Item";
    $page.InventoryLabel = "Inventory/Tickets";
    $page.GSTLabel = "GST Rate";
    $page.WOTLabel = "Wings of Time";
    $page.baseBkApproverUrl = nvx.API_PREFIX + '/sys-config/get-backup-approver';
    $page.baseRevalFeeUrl = nvx.API_PREFIX + '/sys-config/get-reval-item-list';
    $page.otherbaseUrl = nvx.API_PREFIX + '/sys-config/get-ax-products-list';
    $page.baseWOTProdsUrl = nvx.API_PREFIX + '/sys-config/get-wot-products-list';
    $page.baseWotOtherItemsUrl = nvx.API_PREFIX + '/sys-config/get-wot-ax-products-list';
    $page.baseReasonUrl =  nvx.API_PREFIX + '/sys-config/get-reason-list';

    /*
    $page.ptTypeDS = new kendo.data.DataSource({
        transport: {read: {url: '/admin/sys-config/partner/type/list', cache: false, type: "POST"}},
        schema: {
            data: function (response) {
                return JSON.parse(response.ptypeVmsJson);
            },
            model: {
                id: 'id',
                fields: {
                    id: {type: "number"},
                    status: {type: 'string'},
                    statusLabel: {type: 'string'},
                    label: {type: 'string'},
                    description: {type: 'string'}
                }
            }
        }
    });
    $page.regionDS = new kendo.data.DataSource({
        transport: {read: {url: '/admin/sys-config/region/list', cache: false, type: "POST"}},
        schema: {
            data: function (response) {
                return JSON.parse(response.regionVmsJson);
            },
            model: {
                id: 'id',
                fields: {
                    id: {type: "number"},
                    status: {type: 'string'},
                    statusLabel: {type: 'string'},
                    regionName: {type: 'string'},
                    description: {type: 'string'}
                }
            }
        }
    });
    $page.ctyDS = new kendo.data.DataSource({
        transport: {read: {url: '/admin/sys-config/cty/list', cache: false, type: "POST"}},
        schema: {
            data: function (response) {
                console.log(response.regionVmsJson);
                $page.regionVmsJson = JSON.parse(response.regionVmsJson);
                return JSON.parse(response.ctyVmsJson);
            },
            model: {
                id: 'id',
                fields: {
                    id: {type: "number"},
                    status: {type: 'string'},
                    regionName: {type: 'string'},
                    statusLabel: {type: 'string'},
                    regionName: {type: 'string'},
                    description: {type: 'string'}
                }
            }
        }
    });
*/
    $page.GeneralModel = function (data) {
        var s = this;
        s.NumSubAccount = ko.observable(new $page.ParamModel(data.NumSubAccount,"Number of Sub User"));
        s.PurchaseMinQtyPerTxn = ko.observable(new $page.ParamModel(data.PurchaseMinQtyPerTxn,"Minimum of Tickets Purchased per Transaction"));
        s.PurchaseMaxAmountPerTxn = ko.observable(new $page.ParamModel(data.PurchaseMaxAmountPerTxn,"Maximum Limit Per Transaction(S$)"));
        s.PurchaseMaxDailyTxnLimit = ko.observable(new $page.ParamModel(data.PurchaseMaxDailyTxnLimit,"Maximum Total Daily Transaction(S$)"));
        s.PkgAllowedItemsCodes = ko.observable(new $page.ParamModel(data.PkgAllowedItemsCodes,"Maximum Type of Items in a Package"));
    };
    $page.TktModel = function (data) {
        var s = this;
        //s.TicketExpiringPeriod = ko.observable(new $page.ParamModel(data.TicketExpiringPeriod));
        s.TicketExpiringAlertPeriod = ko.observable(new $page.ParamModel(data.TicketExpiringAlertPeriod,""));
        s.TicketRevalidatePeriod = ko.observable(new $page.ParamModel(data.TicketRevalidatePeriod,""));
        s.TicketAllowRevalidatePeriod = ko.observable(new $page.ParamModel(data.TicketAllowRevalidatePeriod,""));
    };
    $page.WotModel = function (data) {
        var s = this;
        s.PartnerWoTAdvancedDaysReleaseBackendReservedTickets = ko.observable(new $page.ParamModel(data.PartnerWoTAdvancedDaysReleaseBackendReservedTickets,""));
        s.WotAdminReservationPeriodLimit = ko.observable(new $page.ParamModel(data.WotAdminReservationPeriodLimit,""));
        s.PartnerTicketonHoldTime = ko.observable(new $page.ParamModel(data.PartnerTicketonHoldTime,""));
        s.PartnerWoTReservationCap = ko.observable(new $page.ParamModel(data.PartnerWoTReservationCap,""));
        s.PartnerOnlinePaymentTime = ko.observable(new $page.ParamModel(data.PartnerOnlinePaymentTime,""));
        s.WoTeTicketEnabled = ko.observable(new $page.ParamModel(data.WoTeTicketEnabled,""));
        s.WoTAdvancedHoursUnredeemedReservationCancelReminder = ko.observable(new $page.ParamModel(data.WoTAdvancedHoursUnredeemedReservationCancelReminder,""));
        s.WotReservationCutoffTime = ko.observable(new $page.ParamModel(data.WotReservationCutoffTime,""));
    };
    $page.ParamModel = function (data,label) {
        var s = this;
        s.editAble = ko.observable(false);
        s.key = ko.observable(data.name);
        s.curValue = ko.observable(data.value);
        s.inValue = ko.observable(data.value);
        s.requireApprove = ko.observable(data.requireApprove);
        s.toggleEdit = function () {
            var editAble = s.editAble();
            s.editAble(!editAble);
        };
        s.save = function () {
            $page.saveParam(s,label);
        };
    };
    $page.ApproverModel = function (data) {
        console.log(JSON.stringify(data));
        if (!data) {
            data = {};
        }
        var s = this;
        s.editAble = ko.observable(false);
        s.id = ko.observable(data.id);
        s.adminId = ko.observable(data.adminId);
        s.selectedAdminId = ko.observable(data.adminId);
        s.adminNm = ko.observable(data.adminNm);

        s.backupAdmId = ko.observable('');
        s.backupFromDt = ko.observable('');
        s.backupToDt = ko.observable('');

        if (!data.adminNm) {
            s.inAdminNm = ko.observable('N.A.');
        } else {
            s.inAdminNm = ko.observable(data.adminNm);
        }
        s.candidates = ko.observableArray([]);
        $.each($page.candidatesJson, function (i, item) {
            var obj = new $page.CandidateModel(item);
            s.candidates.push(obj);
        });
        s.backupCandidates = ko.computed(function () {
            var tmpArray = [];
            $.each($page.candidatesJson, function (i, item) {
                if (s.adminId() != item.adminId) {
                    var obj = new $page.CandidateModel(item);
                    tmpArray.push(obj);
                }
            });
            return tmpArray;
        }, this);
        s.toggleEdit = function () {
            var editAble = s.editAble();
            s.editAble(!editAble);
        };
        s.save = function () {
            $page.saveApprover(s);
        };
        s.rmOfficer = function () {
            $page.rmOfficer();
        };
        s.saveBackup = function () {
            $page.saveBackupApprover(s);
        };
    };
    $page.CandidateModel = function (data) {
        var s = this;
        s.adminId = ko.observable(data.adminId);
        s.adminNm = ko.observable(data.adminNm);
    };
    $page.ReasonModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.title = ko.observable('');
        s.content = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.id);
            s.title(data.title);
            s.content(data.content);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveReason(s);
        };
    };
    $page.saveReason = function (reason) {
        var idata = {};
        idata.id = reason.id();
        idata.title = reason.title();
        idata.content = reason.content();
        idata.status = reason.status();
        if (!idata.title || 0 === idata.title.length) {
            alert("Please fill in the title.");
            return;
        }
        if (!idata.content || 0 === idata.content.length) {
            alert("Please fill in the reason detail.");
            return;
        }
        var reasonVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-reason',
            data: {'reasonVmJson': reasonVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var iReason = JSON.parse(data.data);
                    reason.update(iReason);
                    $page.reasonDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editReasonPop = function (rowid) {
        var rowdata = $page.reasonDS.getByUid(rowid);
        var reason = new $page.ReasonModel(rowdata);
        $page.openReasonPop(reason);
    };
    $page.saveReasonPop = function () {
        var reason = new $page.ReasonModel({});
        $page.openReasonPop(reason);
    };
    $page.openReasonPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.reasonPopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.reasonPopTemp(data);
        }
        if (!$("#reasonWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'reasonWindow');
        }
        var tempWindow = $("#reasonWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Rejection Reason',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#reasonPopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmReason = function () {
        var $items = $('input[name=reaDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url:  nvx.API_PREFIX + '/sys-config/remove-reason',
                data: {reasonIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.reasonDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    $page.rmOfficer = function () {
        var $items = $('input[name=bkaDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }

        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });

        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/sys-config/remove-backup-approver',
                data: {backupIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.bkApproverDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    $page.saveApprover = function (approver) {
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-approver?adminId='+approver.selectedAdminId(),
            data: {},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Update Successfully.");
                    var approverJson = data.data["viewModel"];
                    approver.inAdminNm(approverJson.adminNm);
                    approver.adminId(approverJson.adminId);
                    approver.toggleEdit();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.initTkt = function () {
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/get-ticket-config',
            data: {},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.ticketJson = JSON.parse(data.data);
                    $page.tabData[3].Data = $page.ticketJson;
                    $page.tabData[3].Reload();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.resetAll = function() {
        if (!confirm('Are you sure you want to reset reservation cap of all partners to default?')) {
            return;
        }
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/reset-wot',
            data: {},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                   alert("Reservation Cap of all Partners set to Default.")
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.initWot = function () {
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/get-wot-config',
            data: {},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.wotJson = JSON.parse(data.data);
                    $page.tabData[5].Data = $page.wotJson;
                    // $page.tabData[3].Data = $page.wotJson;
                    $page.tabData[5].Reload();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.getBackupApproverJson = function () {
        var approver = {adminId: '', startDateStr: '', endDateStr: ''};
        return approver;
    }
    $page.saveBackupApprover = function (approver) {
        var startDate = $("#backupFromDt").data('kendoDateTimePicker');
        var endDate = $("#backupToDt").data('kendoDateTimePicker');
        if (startDate.value() == null || endDate.value() == null) {
            var msg = 'Both Start Date and End Date fields are required and must be valid dates.';
            alert(msg);
            return;
        } else if (startDate.value() > endDate.value()) {
            var msg = 'End date must be equal to or later than the Start date.';
            alert(msg);
            return;
        }
        var backupAdminId = approver.backupAdmId()
        if (!backupAdminId || backupAdminId == "") {
            alert("Please select an officer first.");
        }
        var backup = $page.getBackupApproverJson();
        backup.adminId = approver.backupAdmId();
        backup.startDateStr = approver.backupFromDt();
        backup.endDateStr = approver.backupToDt();
        var approverJson = JSON.stringify(backup);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-backup-approver',
            data: {'approverJson': approverJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.bkApproverDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.saveBkAppInPop = function () {
        var startDate = $("input[name='approverVm.startDateStr']").data('kendoDateTimePicker');
        var endDate = $("input[name='approverVm.endDateStr']").data('kendoDateTimePicker');
        if (startDate.value() == null || endDate.value() == null) {
            var msg = 'Both Start Date and End Date fields are required and must be valid dates.';
            alert(msg);
            return;
        } else if (startDate.value() > endDate.value()) {
            var msg = 'End date must be equal to or later than the Start date.';
            alert(msg);
            return;
        }
        var backupId = $("input[name='approverVm.id']").val();
        var backupAdminId = $("input[name='approverVm.adminId']").val();
        var backup = $page.getBackupApproverJson();
        backup.id = backupId;
        backup.adminId = backupAdminId;
        backup.startDateStr = $("input[name='approverVm.startDateStr']").val();
        backup.endDateStr = $("input[name='approverVm.endDateStr']").val();
        backup.status = $("[name='approverVm.status']").val();
        var approverJson = JSON.stringify(backup);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-backup-approver',
            data: {'approverJson': approverJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Update Successfully.");
                    $page.bkApproverDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.saveParam = function (param,label) {
        if (param.requireApprove()) {
            if (!confirm('Are you sure you want to change this configuration, this action will require approval?')) {
                return;
            }
        }
        var numReg = /^\d+$/;
        if (!numReg.test(param.inValue())) {
            alert("Please fill in a number.");
            return;
        }
        var num = Number(param.inValue());
        if (num <= 0) {
            alert("The number of this field should be bigger than zero.");
            return;
        }
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-system-param?paramKey=' + param.key() + '&paramValue=' + param.inValue() +'&paramLabel='+label,
            data: {},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert(data.message);
                    if (!param.requireApprove()) {
                        param.curValue(param.inValue());
                        param.toggleEdit();
                    }
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    //PtType Start
    $page.PtTypeModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.label = ko.observable('');
        s.description = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.id);
            s.label(data.label);
            s.description(data.description);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.savePtType(s);
        };
    };
    $page.savePtType = function (ptType) {
        var idata = {};
        idata.id = ptType.id();
        idata.label = ptType.label();
        idata.description = ptType.description();
        idata.status = ptType.status();
        if (!idata.label || 0 === idata.label.length) {
            alert("Please fill in the title.");
            return;
        }
        if (!idata.description || 0 === idata.description.length) {
            alert("Please fill in the description.");
            return;
        }
        var ptypeVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/admin/sys-config/partner/type/save',
            data: {'ptypeVmJson': ptypeVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var iPtType = JSON.parse(data.ptypeVmJson);
                    ptType.update(iPtType);
                    $page.ptTypeDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editPtTypePop = function (rowid) {
        var rowdata = $page.ptTypeDS.getByUid(rowid);
        var reason = new $page.PtTypeModel(rowdata);
        $page.openPtTypePop(reason);
    };
    $page.savePtTypePop = function () {
        var reason = new $page.PtTypeModel({});
        $page.openPtTypePop(reason);
    };
    $page.openPtTypePop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.ptTypePopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.ptTypePopTemp(data);
        }
        if (!$("#ptTypePopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'ptTypePopWindow');
        }
        var tempWindow = $("#ptTypePopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Partner Type',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#ptTypePopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmPtType = function () {
        var $items = $('input[name=ptTypeDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: '/admin/sys-config/partner/type/remove',
                data: {ptTypeIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.ptTypeDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    //PtType End
    //Region Start
    $page.RegionModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.regionName = ko.observable('');
        s.description = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.id);
            s.regionName(data.regionName);
            s.description(data.description);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveRegion(s);
        };
    };
    $page.saveRegion = function (region) {
        var idata = {};
        idata.id = region.id();
        idata.regionName = region.regionName();
        idata.description = region.description();
        idata.status = region.status();
        if (!idata.regionName || 0 === idata.regionName.length) {
            alert("Please fill in the region name.");
            return;
        }
        if (!idata.description || 0 === idata.description.length) {
            alert("Please fill in the description.");
            return;
        }
        var regionVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/admin/sys-config/region/save',
            data: {'regionVmJson': regionVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var ireg = JSON.parse(data.regionVmJson);
                    region.update(ireg);
                    $page.regionDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editRegionPop = function (rowid) {
        var rowdata = $page.regionDS.getByUid(rowid);
        var reason = new $page.RegionModel(rowdata);
        $page.openRegionPop(reason);
    };
    $page.saveRegionPop = function () {
        var reason = new $page.RegionModel({});
        $page.openRegionPop(reason);
    };
    $page.openRegionPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.regionPopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.regionPopTemp(data);
        }
        if (!$("#regionPopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'regionPopWindow');
        }
        var tempWindow = $("#regionPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Region',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#regionPopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmRegion = function () {
        var $items = $('input[name=regionDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: '/admin/sys-config/region/remove',
                data: {regionIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.regionDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    //Region End
    //Cty Start
    $page.CtyModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.ctyName = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.regionId = ko.observable('');
        s.regions = ko.observableArray([]);
        var regionLength = $page.regionVmsJson.length;
        for (var i = 0; i < regionLength; i++) {
            var o = new $page.RegionModel($page.regionVmsJson[i]);
            s.regions.push(o);
        }
        s.update = function (data) {
            s.id(data.id);
            s.ctyName(data.ctyName);
            s.regionId(data.regionId);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveCty(s);
        };
    };
    $page.saveCty = function (cty) {
        var idata = {};
        idata.id = cty.id();
        idata.regionId = cty.regionId();
        idata.ctyName = cty.ctyName();
        idata.status = cty.status();
        if (!idata.ctyName || 0 === idata.ctyName.length) {
            alert("Please fill in the country name.");
            return;
        }
        var ctyVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/admin/sys-config/cty/save',
            data: {'ctyVmJson': ctyVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var icty = JSON.parse(data.ctyVmJson);
                    cty.update(icty);
                    $page.ctyDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editCtyPop = function (rowid) {
        var rowdata = $page.ctyDS.getByUid(rowid);
        var reason = new $page.CtyModel(rowdata);
        $page.openCtyPop(reason);
    };
    $page.saveCtyPop = function () {
        var reason = new $page.CtyModel({});
        $page.openCtyPop(reason);
    };
    $page.openCtyPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.ctyPopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.ctyPopTemp(data);
        }
        if (!$("#ctyPopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'ctyPopWindow');
        }
        var tempWindow = $("#ctyPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Country',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#ctyPopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmCty = function () {
        var $items = $('input[name=ctyDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: '/admin/sys-config/cty/remove',
                data: {ctyIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.ctyDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    //Cty End
    //Reval Fee Start


    $page.RevalFeeModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.productId = ko.observable('');
        s.itemId = ko.observable('');
        s.priceStr = ko.observable('');
        s.description = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.uuid);
            s.productId(data.productId);
            s.itemId(data.itemId);
            s.priceStr(data.priceStr);
            s.description(data.description);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveRevalFee(s);
        };
    };
    $page.doSelectItem = function() {
        var rowid = $('input[name=itemSelRdio]:checked').attr('data-rowid');
        if (rowid == undefined || rowid == null) {
            alert( 'Please select an item.');
            return;
        }
        var rowdata = $page.otherItemsDS.getByUid(rowid);
        if ($page.genTopup&&rowdata.productId == $page.genTopup.productId) {
            alert('You have selected the same item. Please select a different one.');
            return;
        }
        if (!$page.genTopup){
            $page.genTopup = {};
        }
        if($page.revalItem) {
            $page.genTopup.uuid = $page.revalItem.id();
        }else {
            $page.genTopup.uuid = undefined;
        }
        $page.genTopup.productId = rowdata.productId;
        //$page.genTopup.productNumber = rowdata.itemId;
        $page.genTopup.description = rowdata.description;
        $page.genTopup.priceStr =  rowdata.priceStr;
        $page.genTopup.status =  rowdata.status;
        $page.genTopup.itemId =  rowdata.itemId;
        var item = new $page.RevalFeeModel($page.genTopup);
        $page.openRevalFeePop(item);
        $page.closePopById('otherItemsWindow')
    };
    $page.searchOtherItems = function() {
        $page.otherItemsUrl = $page.otherbaseUrl;
        var itemName = $('[name="otherItemFilter.itemName"]').val();
        $page.otherItemsUrl = $page.otherItemsUrl +'?';
        if(itemName != ''){
            $page.otherItemsUrl = $page.otherItemsUrl +"productName="+itemName;
        }
        console.log("status:"+$page.otherItemsUrl);
        $page.otherItemsDS.options.transport.read.url = $page.otherItemsUrl;
        $page.otherItemsDS.read();
        $page.otherItemsDS.page(1);
    };

    $page.loadOtherItems = function(){
        var htmlcontent =$page.otherItemTemp({});
        console.log(htmlcontent);
        if(!$("#otherItemsWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'otherItemsWindow');
        }
        var tempWindow = $("#otherItemsWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Related Items',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.otherItemsDS.options.transport.read.url = $page.otherItemsUrl;
        $page.otherItemsDS.read();
        $page.otherItemsGrid = $("#otherItemsGrid").kendoGrid({
            dataSource: $page.otherItemsDS,
            columns: [{
                title: "Sel",
                template: "<input type='radio'  name='itemSelRdio' value='#=id#' data-rowid='${uid}' >" ,
                sortable: false,
                width: 40
            },{
                field: "productId",
                title: "Product ID (AX)",
            },{
                field: "itemId",
                title: "Item ID (AX)",
            },{
                field: "priceStr",
                title: "Price",
            },{
                field: "status",
                title: "Status",
            },{
                field: "description",
                title: "Description",
            }
            ],
            sortable: true, pageable: true
        });
        tempWindow.center().open();
    };
    $page.saveRevalFee = function (item) {
        var idata = {};
        idata.uuid = item.id();
        idata.itemId = item.itemId();
        idata.productId = item.productId();
        idata.priceStr = item.priceStr();
        idata.status = item.status();
        idata.description = item.description();
        if (!idata.productId || 0 === idata.productId.length) {
            alert("Please fill in the Product Listing ID.");
            return;
        }
        var numReg = /^\d+$/;
        if (!numReg.test(idata.productId)) {
            alert("Please fill in a valid Product Listing ID.");
            return;
        }
        if (!idata.priceStr || !(idata.priceStr.search(/^\$?[\d,]+(\.\d{1,2})?$/) >= 0)) {
            alert("Please fill in the price with correct format.");
            return;
        }
        var feeItemVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-revalidation-item',
            data: {'feeItemVmJson': feeItemVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    $page.closePopById("revalFeePopWindow");
                    var iItem = data.data.viewModel;
                    item.update(iItem);
                    $page.revalFeeDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editRevalFeePop = function (rowid) {
        var rowdata = $page.revalFeeDS.getByUid(rowid);
        var item = new $page.RevalFeeModel(rowdata);
        $page.revalItem = item;
        $page.openRevalFeePop(item);
    };
    $page.saveRevalFeePop = function () {
        $page.revalItem = undefined;
        var item = new $page.RevalFeeModel({});
        $page.openRevalFeePop(item);
    };
    $page.openRevalFeePop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.revalFeePopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.revalFeePopTemp(data);
        }
        if (!$("#revalFeePopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'revalFeePopWindow');
        }
        var tempWindow = $("#revalFeePopWindow").kendoWindow({
            width: '500px',
            modal: true,
            resizable: false,
            title: 'Revalidation Item',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#revalFeePopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmRevalFee = function () {
        var $items = $('input[name=feeDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/sys-config/remove-revalidation-item',
                data: {feeItemIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.revalFeeDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }

    };
    //Reval Fee End
    //GST start
    $page.GstModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.rateInInt = ko.observable('');
        s.startDateStr = ko.observable('');
        s.endDateStr = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.id);
            s.rateInInt(data.rateInInt);
            s.startDateStr(data.startDateStr);
            s.endDateStr(data.endDateStr);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveGst(s);
        };
    };
    $page.saveGst = function (gst) {
        var idata = {};
        idata.id = gst.id();
        idata.rateInInt = gst.rateInInt();
        idata.startDateStr = gst.startDateStr();
        idata.endDateStr = gst.endDateStr();
        var numReg = /^\d+$/;
        if (!numReg.test(idata.rateInInt)) {
            alert("Please fill in a number in GST rate.");
            return;
        }
        var rateNum = Number(idata.rateInInt);
        if (rateNum <= 0 || rateNum >= 100) {
            alert("GST rate should be from 0 to 100.");
            return;
        }
        var startDate = $("#gstFromDt").data('kendoDateTimePicker');
        var endDate = $("#gstToDt").data('kendoDateTimePicker');
        if (startDate.value() == null) {
            var msg = 'Start Date is required and must be valid dates.';
            alert(msg);
            return;
        }
        if (endDate.value() != null && startDate.value() > endDate.value()) {
            var msg = 'End date must be equal to or later than the Start date.';
            alert(msg);
            return;
        }

        var gstVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-gst',
            data: {'gstVmJson': gstVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var iGst = data.data.viewModel;
                    gst.update(iGst);
                    $page.gstDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editGstPop = function (rowid) {
        var rowdata = $page.gstDS.getByUid(rowid);
        var item = new $page.GstModel(rowdata);
        $page.openGstPop(item);
    };
    $page.saveGstPop = function () {
        var item = new $page.GstModel({});
        $page.openGstPop(item);
    };
    $page.openGstPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.gstPopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.gstPopTemp(data);
        }
        if (!$("#gstPopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'gstPopWindow');
        }
        var tempWindow = $("#gstPopWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'GST Rate',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#gstPopForm')[0]);
        $("#gstFromDt").kendoDateTimePicker({
            format: $page.DateTimeFormat
        }).data('kendoDateTimePicker');
        $("#gstToDt").kendoDateTimePicker({
            format: $page.DateTimeFormat
        }).data('kendoDateTimePicker');
        $("#gstFromDt").closest("span.k-datetimepicker").width(200);
        $("#gstToDt").closest("span.k-datetimepicker").width(200);
        tempWindow.center().open();
    };
    $page.rmGst = function () {
        var $items = $('input[name=gstDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/sys-config/remove-gst',
                data: {gstIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.gstDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }
        ;
    };
    //GST End
    $page.syncContent = function (index) {
        var tab = $page.tabData[index];
        tab.Content = tab.Template(tab.Data);
        $($page.tabstrip.contentElement(index)).html(tab.Content);
    };
    $page.editbkApprover = function (rowid) {
        var rowdata = $page.bkApproverDS.getByUid(rowid);
        $page.openBkApproverPop(rowdata);
    }
    $page.openBkApproverPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.bkApproverTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.bkApproverTemp(data);
        }
        if (!$("#bkApproverWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'bkApproverWindow');
        }
        var tempWindow = $("#bkApproverWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Eidt Backup Approver',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        tempWindow.title(data.adminNm);
        $("input[name='approverVm.startDateStr']").kendoDateTimePicker({
            format: $page.DateTimeFormat
        }).data('kendoDateTimePicker');
        $("input[name='approverVm.endDateStr']").kendoDateTimePicker({
            format: $page.DateTimeFormat
        }).data('kendoDateTimePicker');
        tempWindow.center().open();
    };
    $page.closePopById = function(id){
        var dialog = $("#"+id+"").data("kendoWindow");
        dialog.destroy();
    }
    /**WOT Products Start**/
    $page.WOTProdModel = function (data) {
        var s = this;
        s.id = ko.observable('');
        s.productId = ko.observable('');
        s.itemId = ko.observable('');
        s.priceStr = ko.observable('');
        s.description = ko.observable('');
        s.status = ko.observable('');
        s.statusLabel = ko.observable('');
        s.update = function (data) {
            s.id(data.uuid);
            s.productId(data.productId);
            s.itemId(data.itemId);
            s.priceStr(data.priceStr);
            s.description(data.description);
            s.status(data.status);
            s.statusLabel(data.statusLabel);
        };
        s.update(data);
        s.save = function () {
            $page.saveWotProd(s);
        };
    };
    $page.doSelectWotItem = function() {
        var rowid = $('input[name=itemSelRdio]:checked').attr('data-rowid');
        if (rowid == undefined || rowid == null) {
            alert( 'Please select an item.');
            return;
        }
        var rowdata = $page.wotOtherItemsDS.getByUid(rowid);
        if ($page.wotGenTopup&&rowdata.productId == $page.wotGenTopup.productId) {
            alert('You have selected the same item. Please select a different one.');
            return;
        }
        if (!$page.wotGenTopup){
            $page.wotGenTopup = {};
        }
        if($page.wotProd) {
            $page.wotGenTopup.uuid = $page.wotProd.id();
        }else {
            $page.wotGenTopup.uuid = undefined;
        }
        $page.wotGenTopup.productId = rowdata.productId;
        //$page.genTopup.productNumber = rowdata.itemId;
        $page.wotGenTopup.description = rowdata.description;
        $page.wotGenTopup.priceStr =  rowdata.priceStr;
        $page.wotGenTopup.status =  rowdata.status;
        $page.wotGenTopup.itemId =  rowdata.itemId;
        var item = new $page.WOTProdModel($page.wotGenTopup);
        $page.openWotProdPop(item);
        $page.closePopById('wotOtherItemsWindow')
    };
    $page.searchOtherWotItems = function() {
        $page.wotOtherItemsUrl = $page.baseWotOtherItemsUrl;
        var itemName = $('[name="otherItemFilter.itemName"]').val();
        $page.wotOtherItemsUrl = $page.wotOtherItemsUrl +'?';

        if(itemName != ''){
            $page.wotOtherItemsUrl = $page.wotOtherItemsUrl +"productName="+itemName;
        }
        console.log("status:"+$page.wotOtherItemsUrl);
        $page.wotOtherItemsDS.options.transport.read.url = $page.wotOtherItemsUrl;
        $page.wotOtherItemsDS.read();
        $page.wotOtherItemsDS.page(1);
    };

    $page.loadOtherWotItems = function(){
        var htmlcontent =$page.wotOtherItemTemp({});
        console.log(htmlcontent);
        if(!$("#wotOtherItemsWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'wotOtherItemsWindow');
        }
        var tempWindow = $("#wotOtherItemsWindow").kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: 'Related Items',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        $page.wotOtherItemsDS.options.transport.read.url = $page.baseWotOtherItemsUrl;
        $page.wotOtherItemsDS.read();
        $page.wotOtherItemsGrid = $("#wotOtherItemsGrid").kendoGrid({
            dataSource: $page.wotOtherItemsDS,
            columns: [{
                title: "Sel",
                template: "<input type='radio'  name='itemSelRdio' value='#=id#' data-rowid='${uid}' >" ,
                sortable: false,
                width: 40
            },{
                field: "productId",
                title: "Product ID (AX)",
            },{
                field: "itemId",
                title: "Item ID (AX)",
            },{
                field: "priceStr",
                title: "Price",
            },{
                field: "status",
                title: "Status",
            },{
                field: "description",
                title: "Description",
            }
            ],
            sortable: true, pageable: true
        });
        tempWindow.center().open();
    };
    $page.saveWotProd = function (item) {
        var idata = {};
        idata.uuid = item.id();
        idata.itemId = item.itemId();
        idata.productId = item.productId();
        idata.priceStr = item.priceStr();
        idata.status = item.status();
        idata.description = item.description();
        if (!idata.productId || 0 === idata.productId.length) {
            alert("Please fill in the Product Listing ID.");
            return;
        }
        var numReg = /^\d+$/;
        if (!numReg.test(idata.productId)) {
            alert("Please fill in a valid Product Listing ID.");
            return;
        }
        if (!idata.priceStr || !(idata.priceStr.search(/^\$?[\d,]+(\.\d{1,2})?$/) >= 0)) {
            alert("Please fill in the price with correct format.");
            return;
        }
        var wotProductVmJson = JSON.stringify(idata);
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/save-wot-product',
            data: {'wotProductVmJson': wotProductVmJson},
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    alert("Save Successfully.");
                    var iItem = data.data.viewModel;
                    item.update(iItem);
                    $page.closePopById("wotProdPopWindow");
                    $page.wotProdsDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    $page.editWotProdPop = function (rowid) {
        var rowdata = $page.wotProdsDS.getByUid(rowid);
        var item = new $page.WOTProdModel(rowdata);
        $page.wotProd = item;
        $page.openWotProdPop(item);
    };
    $page.saveWotProdPop = function () {
        $page.wotProd = undefined;
        var item = new $page.WOTProdModel({});
        $page.openWotProdPop(item);
    };
    $page.openWotProdPop = function (data) {
        var htmlcontent;
        if (data) {
            htmlcontent = $page.wotProdPopTemp(data);
        } else {
            var data = {};
            htmlcontent = $page.wotProdPopTemp(data);
        }
        if (!$("#wotProdPopWindow").length) {
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'wotProdPopWindow');
        }
        var tempWindow = $("#wotProdPopWindow").kendoWindow({
            width: '500px',
            modal: true,
            resizable: false,
            title: 'Wings of Time Product',
            actions: ["Close"],
            viewable: false,
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        ko.applyBindings(data, $('#wotProdPopForm')[0]);
        tempWindow.center().open();
    };
    $page.rmWotProd = function () {
        var $items = $('input[name=feeDelChk]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function (idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if (confirm('Are you sure you want to remove those records?')) {
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/sys-config/remove-wot-product',
                data: {wotProdIds: ids},
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        alert('The selected records have been successfully removed.');
                        $page.wotProdsDS.read();
                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                    alert(ERROR_MSG);
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        }

    };
/**WOT Products end**/
    initTabStrip = function () {
        $page.tabSelect = function(e){
            var selectedIndex = $(e.item).index();
            var tab = $page.tabData[selectedIndex];
            $page.currentTab = tab;
            console.log("select: "+tab.Name);
            switch (tab.Name) {
                case $page.CountryLabel:
                    $page.ctyDS.read();
                    break;
                case $page.InventoryLabel:
                    if(!$page.ticketJson){
                        $page.initTkt();
                        return;
                    };
                    break;
                case $page.WOTLabel:
                    if(!$page.wotJson){
                        $page.initWot();
                        return;
                    };
                    break;
                default:
                    break;
            }
            if(tab.Content == $page.loadLabel){
                tab.Reload();
            }
        };

        $page.tabData = [{
            Name: $page.GeneralLabel,
            Content: $page.loadLabel,
            Data: $page.sysSettingJson,
            Template: $page.generalTemp,
            Reload: function () {
                $page.syncContent(0);
                $page.generalModel = new $page.GeneralModel($page.sysSettingJson);
                ko.applyBindings($page.generalModel, $('#generalDv')[0]);
                $page.approverModel = new $page.ApproverModel($page.approverJson);
                ko.applyBindings($page.approverModel, $('#approverDv')[0]);
                $("#backupFromDt").kendoDateTimePicker({
                    format: $page.DateTimeFormat
                }).data('kendoDateTimePicker');
                $("#backupToDt").kendoDateTimePicker({
                    format: $page.DateTimeFormat
                }).data('kendoDateTimePicker');
                $("#backupFromDt").closest("span.k-datetimepicker").width(200);
                $("#backupToDt").closest("span.k-datetimepicker").width(200);
                $page.bkApproverGrid = $("#bkApproverGrid").kendoGrid({
                    dataSource: $page.bkApproverDS,
                    columns: [{
                        field: "adminNm",
                        template: "<a href='javascript:nvxpage.editbkApprover(\"${uid}\")' class='round-bt'>#=adminNm#</a>",
                        title: "Officer Name",
                        width: 100
                    }, {
                        field: "startDateStr",
                        title: "Start Date",
                        width: 100
                    }, {
                        field: "endDateStr",
                        title: "End Date",
                        width: 100
                    }, {
                        field: "statusLabel",
                        title: "Status",
                        width: 40
                    },
                        {
                            title: "Delete",
                            template: "<input type='checkbox'  name='bkaDelChk' value='#=id#' >",
                            sortable: false,
                            width: 40
                        }
                    ],
                    sortable: $page.serverSorting, pageable: true
                });
            }
        }, {
            Name: $page.ReasonLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.reasonTemp,
            Reload: function () {
                $page.syncContent(1);
                $page.reasonGrid = $("#reasonGrid").kendoGrid({
                    dataSource: $page.reasonDS,
                    columns: [{
                        field: "title",
                        template: "<a href='javascript:nvxpage.editReasonPop(\"${uid}\")' class='round-bt'>#=title#</a>",
                        title: "Title",
                        width: 100
                    }, {
                        field: "content",
                        title: "Rejection Reason",
                        width: 200
                    }, {
                        field: "statusLabel",
                        title: "Status",
                        width: 40
                    }, {
                        title: "Delete",
                        template: "<input type='checkbox'  name='reaDelChk' value='#=id#' >",
                        sortable: false,
                        width: 40
                    }
                    ],
                    sortable: $page.serverSorting, pageable: true
                });
            }
        }/*, {
            Name: $page.PartnerTypeLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.partnerTypeTemp,
            Reload: function () {
                $page.syncContent(2);
                $page.ptTypeGrid = $("#ptTypeGrid").kendoGrid({
                    dataSource: $page.ptTypeDS,
                    columns: [{
                        field: "label",
                        template: "<a href='javascript:nvxpage.editPtTypePop(\"${uid}\")' >#=label#</a>",
                        title: "Title",
                        width: 100
                    }, {
                        field: "description",
                        title: "Description",
                        width: 200
                    }, {
                        field: "statusLabel",
                        title: "Status",
                        width: 40
                    }, {
                        title: "Delete",
                        template: "<input type='checkbox'  name='ptTypeDelChk' value='#=id#' >",
                        sortable: false,
                        width: 40
                    }
                    ]
                });
            }
        }, {
            Name: $page.RegionLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.regionTemp,
            Reload: function () {
                $page.syncContent(3);
                $page.regionGrid = $("#regionGrid").kendoGrid({
                    dataSource: $page.regionDS,
                    columns: [{
                        field: "regionName",
                        template: "<a href='javascript:nvxpage.editRegionPop(\"${uid}\")' >#=regionName#</a>",
                        title: "Title",
                        width: 100
                    }, {
                        field: "description",
                        title: "Description",
                        width: 200
                    }, {
                        field: "statusLabel",
                        title: "Status",
                        width: 40
                    }, {
                        title: "Delete",
                        template: "<input type='checkbox'  name='regionDelChk' value='#=id#' >",
                        sortable: false,
                        width: 40
                    }
                    ]
                });
            }
        }, {
            Name: $page.CountryLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.ctyTemp,
            Reload: function () {
                $page.syncContent(4);
                $page.ctyGrid = $("#ctyGrid").kendoGrid({
                    dataSource: $page.ctyDS,
                    columns: [{
                        field: "ctyName",
                        template: "<a href='javascript:nvxpage.editCtyPop(\"${uid}\")' >#=ctyName#</a>",
                        title: "Country Name",
                        width: 100
                    }, {
                        field: "regionName",
                        title: "Region",
                        width: 40
                    }, {
                        field: "statusLabel",
                        title: "Status",
                        width: 40
                    }, {
                        title: "Delete",
                        template: "<input type='checkbox'  name='ctyDelChk' value='#=id#' >",
                        sortable: false,
                        width: 40
                    }
                    ]
                });
            }
        }*/, {
            Name: $page.RevalItemLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.revalFeeTemp,
            Reload: function () {
                $page.syncContent(2);
                $page.revalFeeGrid = $("#revalFeeGrid").kendoGrid({
            dataSource: $page.revalFeeDS,
            columns: [{
                field: "productId",
                template: "<a href='javascript:nvxpage.editRevalFeePop(\"${uid}\")' >#=productId#</a>",
                title: "Product ID (AX)",
                width: 50
            }, {
                field: "priceStr",
                title: "Revalidation Fee (S$)",
                width: 100
            }, {
                field: "description",
                title: "Description",
                width: 200
            }, {
                field: "status",
                title: "Status",
                width: 40
            }, {
                title: "Delete",
                template: "<input type='checkbox'  name='feeDelChk' value='#=uuid#' >",
                sortable: false,
                width: 40
            }
            ]
        });
    }
        }, {
            Name: $page.InventoryLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.inventoryTemp,
            Reload: function () {
                $page.syncContent(3);
                $page.tktModel = new $page.TktModel($page.ticketJson);
                ko.applyBindings($page.tktModel, $('#inventoryDv')[0]);
            }
        }, {
            Name: $page.GSTLabel,
            Content: $page.loadLabel,
            Data: {},
            Template: $page.gstTemp,
            Reload: function () {
                $page.syncContent(4);
                $page.gstGrid = $("#gstGrid").kendoGrid({
                    dataSource: $page.gstDS,
                    columns: [{
                        field: "rate",
                        template: "<a href='javascript:nvxpage.editGstPop(\"${uid}\")' >#=rateInInt#</a>",
                        title: "GST Percentage",
                        width: 100
                    }, {
                        field: "startDateStr",
                        title: "Date From",
                        width: 100
                    }, {
                        field: "endDateStr",
                        title: "Date To",
                        width: 100
                    }, {
                        title: "Delete",
                        template: "<input type='checkbox'  name='gstDelChk' value='#=id#' >",
                        sortable: false,
                        width: 40
                    }
                    ]
                });
            }
        },{
                Name: $page.WOTLabel,
                Content: $page.loadLabel,
                Data: {},
                Template: $page.wotTemp,
                Reload: function () {
                    $page.syncContent(5);
                    $page.wotModel = new $page.WotModel($page.wotJson);
                    ko.applyBindings($page.wotModel, $('#wotDv')[0]);
                    $page.wotProductsGrid = $("#wotProductsGrid").kendoGrid({
                        dataSource: $page.wotProdsDS,
                        columns: [{
                            field: "productId",
                            template: "<a href='javascript:nvxpage.editWotProdPop(\"${uid}\")' >#=productId#</a>",
                            title: "Product ID (AX)",
                            width: 50
                        }, {
                            field: "priceStr",
                            title: "Price (S$)",
                            width: 100
                        }, {
                            field: "description",
                            title: "Description",
                            width: 200
                        }, {
                            field: "status",
                            title: "Status",
                            width: 40
                        }, {
                            title: "Delete",
                            template: "<input type='checkbox'  name='feeDelChk' value='#=uuid#' >",
                            sortable: false,
                            width: 40
                        }
                        ]
                    });
                }

            }
        ];
        $page.tabstrip = $("#tabstrip").kendoTabStrip({
            dataTextField : "Name",
            dataContentField : "Content",
            dataSource : $page.tabData,
            select : $page.tabSelect
        }).data("kendoTabStrip");

        setTimeout(function(){$page.tabstrip.select(0)},500);
    };
    $page.initGst = function() {
        $page.gstDS = new kendo.data.DataSource({
            transport: {read: {url:  nvx.API_PREFIX + '/sys-config/get-gst-list', cache: false, type: "GET"}},
            schema: {
                data: 'data.viewModel',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: "number"},
                        status: {type: 'string'},
                        rateInInt: {type: 'number'},
                        statusLabel: {type: 'string'},
                        startDateStr: {type: 'string'},
                        endDateStr: {type: 'string'}
                    }
                }
            }
        });
    };
    $page.initReason = function() {
        $page.theReasonUrl = $page.baseReasonUrl;
        $page.reasonDS = new kendo.data.DataSource({
            transport: {read: {url: $page.theReasonUrl, cache: false, type: "GET"}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: "number"},
                        status: {type: 'string'},
                        statusLabel: {type: 'string'},
                        title: {type: 'string'},
                        content: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    };
    $page.initWotProds = function() {

        $page.theWOTProdsUrl = $page.baseWOTProdsUrl;
        $page.wotProdsDS = new kendo.data.DataSource({
            transport: {read: {url: $page.theWOTProdsUrl, cache: false, type: "POST"}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        itemId: {type: "number"},
                        productId: {type: 'number'},
                        priceStr: {type: 'string'},
                        status: {type: 'string'},
                        statusLabel: {type: 'string'},
                        //startDateStr: {type: 'string'},
                        description: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    };
    $page.initWotOtherItems = function() {

        $page.wotOtherItemsUrl = $page.baseWotOtherItemsUrl;
        $page.wotOtherItemsDS = new kendo.data.DataSource({
            transport: {read: {url: $page.wotOtherItemsUrl, cache: false, type: "POST"}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        itemId: {type: "number"},
                        productId: {type: 'number'},
                        priceStr: {type: 'string'},
                        status: {type: 'string'},
                        statusLabel: {type: 'string'},
                        //startDateStr: {type: 'string'},
                        description: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: false, serverPaging: false
        });
    };
$page.initRevalItem = function() {

    $page.theRevalFeeUrl = $page.baseRevalFeeUrl;
    $page.revalFeeDS = new kendo.data.DataSource({
        transport: {read: {url: $page.theRevalFeeUrl, cache: false, type: "POST"}},
        schema: {
            data: 'data.viewModel',
            total: 'data.total',
            model: {
                id: 'id',
                fields: {
                    itemId: {type: "number"},
                    productId: {type: 'number'},
                    priceStr: {type: 'string'},
                    status: {type: 'string'},
                    statusLabel: {type: 'string'},
                    //startDateStr: {type: 'string'},
                    description: {type: 'string'}
                }
            }
        }, pageSize: 10, serverSorting: true, serverPaging: true
    });
};
    $page.initOtherItems = function() {

        $page.otherItemsUrl = $page.otherbaseUrl;
        $page.otherItemsDS = new kendo.data.DataSource({
            transport: {read: {url: $page.otherItemsUrl, cache: false, type: "POST"}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        itemId: {type: "number"},
                        productId: {type: 'number'},
                        priceStr: {type: 'string'},
                        status: {type: 'string'},
                        statusLabel: {type: 'string'},
                        //startDateStr: {type: 'string'},
                        description: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: false, serverPaging: false
        });
    };
    $page.initBackupApprover = function() {

        $page.theBkApproverUrl = $page.baseBkApproverUrl;
        $page.bkApproverDS = new kendo.data.DataSource({
            transport: {read: {url: $page.theBkApproverUrl, cache: false, type: "POST"}},
            schema: {
                data: 'data.viewModel',
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: "number"},
                        adminId: {type: 'number'},
                        adminNm: {type: 'string'},
                        status: {type: 'string'},
                        statusLabel: {type: 'string'},
                        startDateStr: {type: 'string'},
                        endDateStr: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    };


    $page.loadPageData = function(data, status, jqXHR){
        if(data != undefined) {
           var newData = JSON.parse(data);
            $page.sysSettingJson = newData['sysMap'];
            $page.approverJson = newData['approverVm'];
            $page.candidatesJson = newData['candidatesList'];

            //$page.inittrans();
            $page.initWotProds();
            $page.initWotOtherItems();
            $page.initRevalItem();
            $page.initOtherItems();
            $page.initBackupApprover();
            $page.initReason();
            $page.initGst();
            initTabStrip();
        }
    };

    $page.initPageData = function () {
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/sys-config/get-general-config',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });

    };
    //$($page.tabstrip.items()[2]).attr("style", "display:none");  
    window.nvxpage = $page;
    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/
});