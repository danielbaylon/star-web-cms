$(document).ready(function() {
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    var $page = {};
    $page.tierVmsJson = {};

    $.ajax({
        type: "GET",
        url: "/.partner-admin/ppmflgadmin/tier/get-all-tiers",
        success:  function(result, textStatus, jqXHR)
        {
            if(result.success)
            {
                var tiervms = result.data;
                var b2btiervms = [];
                for(var i=0; i<tiervms.length; i++) {
                    var tiervm = tiervms[i];
                    if(tiervm.isB2B==1) {
                        b2btiervms.push(tiervm);
                    }
                }
                $page.tierVmsJson = b2btiervms;
                $page.init($page.tierVmsJson);
            }
            else
            {

            }
        },
        error: function(jqXHR, textStatus)
        {

        }
    });


    $page.tierTbTemp = kendo.template($("#tierTbTemp").html());
    $page.tierPopTemp = kendo.template($("#tierPopTemp").html());

    // $page.basePartnerUrl = '/.partner-admin/tier/get-all-partners';
    // $page.thePartnerUrl = $page.basePartnerUrl;
    // $page.partnerDS = new kendo.data.DataSource({
    //     transport: {read: {url: $page.basePartnerUrl, cache: false,type: "POST"}},
    //     schema: {
    //         data:function(response) {
    //             return JSON.parse(response.data);
    //         },
    //         total: 'total',
    //         model: {
    //             id: 'id',
    //             fields: {
    //                 id: {type: "number"},
    //                 orgName: {type: 'string'},
    //                 accountCode: {type: 'string'}
    //             }
    //         }
    //     }, pageSize: 5, serverSorting: true, serverPaging: true
    // });
    $page.baseProdUrl = '/.partner-admin/ppmflgadmin/tier/get-all-products';
    $page.theProdUrl = $page.baseProdUrl;
    $page.prodDS = new kendo.data.DataSource({
        transport: {read: {url: $page.baseProdUrl, cache: false,type: "POST"}},
        schema: {
            data:function(response) {
                return JSON.parse(response.data);
            },
            total: 'total',
            model: {
                id: 'id',
                fields: {
                    id: {type: "string"},
                    name: {type: 'string'},
                    prodType: {type: 'string'}
                }
            }
        }, pageSize: 5, serverSorting: true, serverPaging: true
    });

    $page.init = function(data){
        console.log(data.length);
        var content =  $page.tierTbTemp(data);
        console.log(data);
        $("#tierGrid").html(content);
        $("#tierTable").kendoGrid({
            scrollable:false
        });
    };
    $page.TierModel = function(idata){
        var s = this;
        s.Data = idata;
        s.id = ko.observable('');
        s.prodNmFilter = ko.observable('');
        s.partnerNmFilter = ko.observable('');
        s.name = ko.observable('');
        s.priceGroupId = ko.observable('');
        s.isB2B = ko.observable('');
        // s.status = ko.observable('');
        // s.statusLabel = ko.observable('');
        // s.ipartners = ko.observableArray([]);
        s.iprods = ko.observableArray([]);
        s.update = function(idata){
            for(var k in idata) s.Data[k]=idata[k];
            s.id(idata.priceGroupId);
            s.name(idata.name);
            s.priceGroupId(idata.priceGroupId);
            s.isB2B(idata.isB2B);
            // s.description(idata.description);
            // s.status(idata.status);
            // s.statusLabel(idata.statusLabel);
            // if(idata.partnerVms){
            //     var partnerLength = idata.partnerVms.length;
            //     for (var i = 0; i < partnerLength; i++) {
            //         if(idata.partnerVms[i].id&&idata.partnerVms[i].orgName){
            //             var tmpObj = idata.partnerVms[i];
            //             var o = new $page.PartnerModel(tmpObj.id,tmpObj.orgName,tmpObj.accountCode);
            //             s.ipartners.push(o);
            //         }
            //     }
            // }
            if(idata.prodVms){
                var prodLength = idata.prodVms.length;
                for (var i = 0; i < prodLength; i++) {
                    if(idata.prodVms[i].id&&idata.prodVms[i].name){
                        var tmpObj = idata.prodVms[i];
                        var o = new $page.ProdModel(tmpObj.id,tmpObj.name,tmpObj.prodType);
                        s.iprods.push(o);
                    }
                }
            }
        }
        // s.filterPartner = function(){
        //     var paNm = s.partnerNmFilter();
        //     ko.utils.arrayForEach(s.ipartners(), function(item) {
        //         var orgName = item.orgName;
        //         if(orgName.indexOf(paNm)< 0&&paNm != ''){
        //             item.showup(false);
        //         }else{
        //             item.showup(true);
        //         }
        //     });
        // };
        s.filterProd = function(){
            var prodNm = s.prodNmFilter();
            ko.utils.arrayForEach(s.iprods(), function(item) {
                var name = item.name;
                if(name.indexOf(prodNm)< 0&&prodNm != ''){
                    item.showup(false);
                }else{
                    item.showup(true);
                }
            });
        };
        // s.addPartner = function(id,name,code) {
        //     var o = new $page.PartnerModel(id,name,code);
        //     s.ipartners.push(o);
        //     $page.refreshPa();
        //     $page.partnerDS.read();
        // };
        // s.rmPartner = function() {
        //     s.ipartners.remove(this);
        //     $page.refreshPa();
        //     $page.partnerDS.read();
        // };
        // s.rmAllPartner = function() {
        //     s.ipartners.removeAll()
        //     $page.refreshPa();
        //     $page.partnerDS.read();
        // };
        // s.addAllPartner = function() {
        //     $page.addAllPartner();
        // };
        s.addProd = function(id,name,type) {
            var o = new $page.ProdModel(id,name,type);
            s.iprods.push(o);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.rmProd = function() {
            s.iprods.remove(this);
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.rmAllProd = function() {
            s.iprods.removeAll();
            $page.refreshProd();
            $page.prodDS.read();
        };
        s.addAllProd = function() {
            $page.addAllProd();
        };
        s.update(idata);
    };
    // $page.PartnerModel = function(id,orgName,accountCode){
    //     var self = this;
    //     self.showup = ko.observable(true);
    //     self.id = id;
    //     self.orgName = orgName;
    //     self.accountCode = accountCode;
    // };
    $page.ProdModel = function(id,name,prodType){
        var self = this;
        self.showup = ko.observable(true);
        self.id = id;
        self.name = name;
        self.prodType = prodType;
    };
    $page.loadTier= function(priceGroupId){
        //var priceGroupId = "PG1";
        nvx.spinner.start();
        $.ajax({
            type: "GET",
            url: '/.partner-admin/ppmflgadmin/tier/get-tier/' + priceGroupId,
            data: {},
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    $page.tierVmJson = JSON.parse(data.data);
                    $page.tierVm = new $page.TierModel($page.tierVmJson);
                    $page.openTierPop($page.tierVm);
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.saveTier = function(){
        if(!$page.tierVm.name() || 0 === $page.tierVm.name().length){
            alert("Please fill in the name.");
            return;
        }
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/.partner-admin/tier/save-tier',
            //data: $('#tierPopForm').serialize(),
            data: JSON.stringify({
                id: $page.tierVm.priceGroupId(),
                priceGroupId: $page.tierVm.priceGroupId(),
                name: $page.tierVm.name(),
                description: $page.tierVm.isB2B(),
                prodVms:  $page.tierVm.iprods(),
            }),
            contentType: 'application/json', dataType: 'json',
            success: function(data) {
                if (data.success) {
                    alert('Updated Successfully');

                    $page.tierVmsJson = JSON.parse(data.data.tierVmsJson);
                    var tiervms = $page.tierVmsJson;
                    var b2btiervms = [];
                    for(var i=0; i<tiervms.length; i++) {
                        var tiervm = tiervms[i];
                        if(tiervm.isB2B==1) {
                            b2btiervms.push(tiervm);
                        }
                    }
                    $page.tierVmsJson = b2btiervms;

                    $page.tierVmJson = JSON.parse(data.data.tierVmJson);
                     tiervms = $page.tierVmsJson;
                     b2btiervms = [];
                    for(var i=0; i<tiervms.length; i++) {
                        var dtiervm = tiervms[i];
                        if(tiervm.isB2B==1) {
                            b2btiervms.push(tiervm);
                        }
                    }
                    $page.tierVmJson =b2btiervms;
                    console.log("tierVm id:"+$page.tierVmJson.priceGroupId);
                    $("input[name='tierVm.priceGroupId']").val($page.tierVmJson.priceGroupId);
                    $page.init($page.tierVmsJson);
                } else {
                    alert(data.message);
                }
            },
            error: function() {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    // $page.addAllPartner= function(){
    //     nvx.spinner.start();
    //     var filter = {};
    //     filter.tierId =$("input[name='tierVm.id']").val();
    //     var filterJson = JSON.stringify(filter);
    //     $.ajax({
    //         type: "POST",
    //         url: '/.partner-admin/tier/add-all-partners',
    //         data: JSON.stringify({'ptfilterJson':filterJson}),
    //         contentType: 'application/json', dataType: 'json',
    //         success:  function(data, textStatus, jqXHR)
    //         {
    //             if (data.success) {
    //                 alert(data.total+" partners will be assigned to this tier.");
    //                 var partnerVms = JSON.parse(data.data);
    //                 $page.tierVm.ipartners.removeAll();
    //                 if(partnerVms){
    //                     var partnerLength = partnerVms.length;
    //                     for (var i = 0; i < partnerLength; i++) {
    //                         if(partnerVms[i].id&&partnerVms[i].orgName){
    //                             var tmpObj = partnerVms[i];
    //                             var o = new $page.PartnerModel(tmpObj.id,tmpObj.orgName,tmpObj.accountCode);
    //                             $page.tierVm.ipartners.push(o);
    //                         }
    //                     }
    //                 }
    //                 $page.refreshPa();
    //                 $page.partnerDS.read();
    //             } else {
    //                 alert(data.message);
    //             }
    //         },
    //         error: function(jqXHR, textStatus)
    //         {
    //             alert(ERROR_MSG);
    //         },
    //         complete: function() {
    //             nvx.spinner.stop();
    //         }
    //     });
    // };
    $page.addAllProd= function(){
        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: '/.partner-admin/ppmflgadmin/tier/add-all-products',
            data: {},
            dataType: 'json',
            success:  function(data, textStatus, jqXHR)
            {
                if (data.success) {
                    alert(data.total+"  products will be assigned to this tier.");
                    var prodVms = JSON.parse(data.data);
                    $page.tierVm.iprods.removeAll();
                    if(prodVms){
                        var prodLength = prodVms.length;
                        for (var i = 0; i < prodLength; i++) {
                            if(prodVms[i].id&&prodVms[i].name){
                                var tmpObj = prodVms[i];
                                var o = new $page.ProdModel(tmpObj.id,tmpObj.name,tmpObj.prodType);
                                $page.tierVm.iprods.push(o);
                            }
                        }
                    }
                    $page.refreshProd();
                    $page.prodDS.read();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    // $page.getPartnersJson = function(data){
    //     var filter = {partnerIds:[]};
    //     $.each(data, function(i, item) {
    //         if(item.id){
    //             filter.partnerIds.push(item.id);
    //         }
    //     });
    //     filter.tierId =$("input[name='tierVm.id']").val();
    //     return filter;
    // };
    // $page.refreshPa = function(isfilter){
    //     var partners = $page.tierVm.ipartners();
    //     var filter = $page.getPartnersJson(partners);
    //     if(isfilter){
    //         filter.orgName = $('#orgNameFilter').val();
    //     }
    //     var filterJson = JSON.stringify(filter);
    //     console.log(filterJson);
    //     $page.partnerDS.options.transport.read.url = $page.basePartnerUrl;
    //     $page.partnerDS.options.transport.read.data = {'ptfilterJson':filterJson};
    //     if(isfilter){
    //         $("#partnerGrid").data("kendoGrid").dataSource.page(1);
    //     }
    // }
    $page.getProdsJson = function(data){
        var filter = {prodIds:[]};
        $.each(data, function(i, item) {
            if(item.id){
                filter.prodIds.push(item.id);
            }
        });
        return filter;
    };
    $page.refreshProd = function(isfilter){
        var prods = $page.tierVm.iprods();
        var filter = $page.getProdsJson(prods);
        if(isfilter){
            filter.prodName = $('#prodNmFilter').val();
        }
        var filterJson = JSON.stringify(filter);
        console.log(filterJson);
        $page.prodDS.options.transport.read.url = $page.baseProdUrl;
        $page.prodDS.options.transport.read.data = {'prodfilterJson':filterJson};
        if(isfilter){
            $("#productGrid").data("kendoGrid").dataSource.page(1);
        }
    };
    $page.doRemoveTier = function() {
        var $items = $('input[name=tierforDel]:checked');
        if ($items.size() == 0) {
            alert('Please select at least one record to remove.');
            return;
        }
        var ids = '';
        $items.each(function(idx, value) {
            ids += '' + $(this).val() + ($items.length == idx + 1 ? '' : ',');
        });
        if(confirm('Deleting Display Group will unassigned Partner from this display group, are you sure you want to remove those display group?')) {
            nvx.spinner.start();
            $.ajax({
                url: '/.partner-admin/ppmflgadmin/tier/remove-tier',
                data: ids,
                contentType: 'application/json', dataType: 'json',
                type: 'POST', cache: false,
                success: function(data) {
                    if (data.success) {
                        alert('The selected Tiers have been successfully removed.');
                        $page.tierVmsJson = JSON.parse(data.data);
                        $page.init($page.tierVmsJson);
                    } else {
                        alert(data.message);
                    }
                },
                error: function() {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };
    $page.openTierPop = function(tier){
        var htmlcontent ;
        if(tier){
            htmlcontent =$page.tierPopTemp(tier.Data);
        }else{
            var tmpObj = {};
            htmlcontent =$page.tierPopTemp(tmpObj);
            $page.tierVm = new $page.TierModel(tmpObj);
        }
        // $page.refreshPa();
        $page.refreshProd();

        if(!$("#tierWindow").length){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'tierWindow');
        }
        var tempWindow = $("#tierWindow").kendoWindow({
            width: '850px',
            modal: true,
            resizable: false,
            title: 'Display Group',
            actions: ["Close"],
            viewable : false,
            deactivate: function() {
                this.destroy();
            }
        }).data('kendoWindow');
        tempWindow.content(htmlcontent);
        tempWindow.title($page.tierVm.name());
        $page.tierTabs = $("#tierTabs").kendoTabStrip({
        }).data("kendoTabStrip");
        // $page.partnerGrid = $("#partnerGrid").kendoGrid({
        //     dataSource: $page.partnerDS,
        //     columns: [{
        //         title: "Add",
        //         template: function(dataItem) {
        //             var orgName = dataItem.orgName;
        //             orgName = orgName.replace(/&/, "&amp;").replace(/'/g, "&#39;").replace(/"/g, '\\"');
        //             return "<a href='javascript:nvxpage.tierVm.addPartner(\"" + dataItem.id + "\",\"" + orgName + "\",\"" + dataItem.accountCode + "\")' class='round-bt'>"+
        //                 "<i class='fa fa-plus-circle'></i>"+
        //                 "</a>";
        //         },
        //         sortable: false,
        //         width: 20
        //     },{
        //         field: "orgName",
        //         title: "Company Name",
        //         width: 100
        //     },{
        //         field: "accountCode",
        //         title: "Code",
        //         width: 40
        //     }
        //     ],
        //     sortable: $page.serverSorting, pageable: true
        // });
        $page.productGrid = $("#productGrid").kendoGrid({
            dataSource: $page.prodDS,
            columns: [{
                title: "Add",
                template: function(dataItem) {
                    var name = dataItem.name;
                    name = name.replace(/&/, "&amp;").replace(/'/g, "&#39;").replace(/"/g, '\\"');
                    return "<a href='javascript:nvxpage.tierVm.addProd(\"" + dataItem.id + "\",\"" + name + "\",\"" + dataItem.prodType + "\")' class='round-bt'>"+
                        "<i class='fa fa-plus-circle'></i>"+
                        "</a>";
                },
                sortable: false,
                width: 20
            },{
                field: "name",
                title: "Product Name",
                width: 90
            },{
                field: "prodType",
                title: "Type/(Display<BR> Group)",
                template: "#=prodType##if(tiers){#<BR/>(#=tiers#)#}else{##}#",
                width: 50
            }
            ],
            sortable: $page.serverSorting, pageable: true
        });

        $("#productGrid").data("kendoGrid").dataSource.page(1);
       // $("#partnerGrid").data("kendoGrid").dataSource.page(1);

        ko.applyBindings($page.tierVm,$('#tierPopForm')[0]);
        tempWindow.center().open();
    };
    $page.closePopById = function(id){
        var dialog = $("#"+id+"").data("kendoWindow");
        dialog.destroy();
    }
    window.nvxpage = $page;
});