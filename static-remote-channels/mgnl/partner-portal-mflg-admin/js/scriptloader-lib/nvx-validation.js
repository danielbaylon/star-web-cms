(function(nvx) {
    
    nvx.regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    nvx.isValidEmail = function(email) { return nvx.regexEmail.test(email); };

    nvx.isNotEmpty = function(text) { return !(text == null || text == undefined || text == ""); };

    nvx.isEmpty = function(text) { return text == '' || text == null || text == undefined; };
    
    nvx.isLengthWithin = function(text, minLen, maxLen, inclusive) {
        if (inclusive) { return text != null && text.length <= maxLen && text.length >= minLen; }
        return text != null && text.length < maxLen && text.length > minLen;
    };
    
    nvx.regexLowercase = /.*?[a-z]+?.*/;
    nvx.hasLowercase = function(text) { return nvx.regexLowercase.test(text); };
        
    nvx.regexUppercase = /.*?[A-Z]+?.*/;
    nvx.hasUppercase = function(text) { return nvx.regexUppercase.test(text); };
    
    nvx.regexNumber = /.*?[0-9]+.*/;
    nvx.hasNumber = function(text) { return nvx.regexNumber.test(text); };
    
    nvx.regexNonAlphanumeric = /.*?[^a-zA-Z\d\s]+.*/;
    nvx.hasNonAlphanumeric = function(text) { return nvx.regexNonAlphanumeric.test(text); };
    
    nvx.nric = (function () {
        var multiples = [ 2, 7, 6, 5, 4, 3, 2 ];
    
        var modNric = function(numericNric) {
            var count = 0, total = 0;
            while (numericNric != 0) {
                total += (numericNric % 10) * multiples[multiples.length - (1 + count++)];
                numericNric /= 10;
                numericNric = Math.floor(numericNric);
            }
            return total;
        };
    
        return {
            isNricValid:function (theNric) {
                if (!theNric || theNric == '') {
                    return false;
                }
                if (theNric.length != 9) {
                    return false;
                }
    
                var total, numericNric;
                var first = theNric[0];
                var last = theNric[theNric.length - 1];
                if (first != 'S' && first != 'T') {
                    return false;
                }
    
                numericNric = theNric.substr(1, theNric.length - 2);
                if (isNaN(numericNric)) {
                    return false;
                }
    
                total = modNric(numericNric);
    
                var outputs;
                if (first == 'S') {
                    outputs = [ 'J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A' ];
                } else {
                    outputs = [ 'G', 'F', 'E', 'D', 'C', 'B', 'A', 'J', 'Z', 'I', 'H' ];
                }
    
                return last == outputs[total % 11];
            },
    
            isFinValid:function (fin) {
                if (!fin || fin == '') {
                    return false;
                }
                if (fin.length != 9) {
                    return false;
                }
    
                var total , numericNric;
                var first = fin[0];
                var last = fin[fin.length - 1];
    
                if (first != 'F' && first != 'G') {
                    return false;
                }
    
                numericNric = fin.substr(1, fin.length - 2);
    
                if (isNaN(numericNric)) {
                    return false;
                }
    
                total = modNric(numericNric);
    
                var outputs;
                if (first == 'F') {
                    outputs = [ 'X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K' ];
                } else {
                    outputs = [ 'R', 'Q', 'P', 'N', 'M', 'L', 'K', 'X', 'W', 'U', 'T' ];
                }
    
                return last == outputs[total % 11];
            }
        };
    })();
})(window.nvx = window.nvx || {});
