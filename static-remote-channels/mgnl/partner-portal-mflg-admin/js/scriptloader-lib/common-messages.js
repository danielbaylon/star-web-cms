(function(nvx, $) {
    nvx.COM_MSG = {};
    nvx.isDefinedMsg = function(key){
        if(nvx.COM_MSG[key] != undefined){
            return true;
        }
        return false;
    };
    nvx.getDefinedMsg = function(key){
        if(nvx.isDefinedMsg(key)){
            var msg = nvx.COM_MSG[key];
            if(msg != undefined && msg[CURR_LANG] != undefined && msg[CURR_LANG] != null){
                return msg[CURR_LANG];
            }
        }
        return null;
    }
})(window.nvx = window.nvx || {}, jQuery);

/** partner management, begin **/
nvx.COM_MSG['PARTNER-NOT-FOUND'] = {'en' : 'No partner found'};
/** partner management, end **/