var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

(function(nvx, $) {
    $(document).ready(function() {
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/trans-query/is-correct-login-user',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    // loading page, begin
                    nvx.spinner.start();
                    $page = {};
                    $page.filterView = new nvx.FilterView();
                    ko.applyBindings($page.filterView,$('#filterSectionDiv')[0]);
                    $page.filterView.loading();
                    $page.resultView = new nvx.ResultView();
                    window.winpage = $page;
                    nvx.spinner.stop();
                    // loading page, end
                }else{
                    $('div.container').html(data['message']);
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    });

    nvx.FilterView = function(){
        var s = this;
        s.partnerList = [];
        s.ticketMediaList = [];
        s.filterTdf = null;
        s.filterTdt = null;
        s.selPaVms  = null;
        s.selTicketMedia =  null;
        s.fromDtStr = ko.observable('');
        s.toDtStr   = ko.observable('');
        s.filterPaymentType = ko.observable('');
        s.pinCode = ko.observable('');
        s.receiptNum = ko.observable('');
        s.orgName = ko.observable('');
        s.filterStatus = ko.observable('');
        s.accountCode = ko.observable('');
        s.partners    = ko.observable([]);
        s.reInitFilterStatus = function(type){
            s.filterStatus('');
            $('#filterStatusDrpList > option').remove();
            $('#filterStatusDrpList').append(new Option('All', ''));
            if(type != undefined && type != null && type != ''){
                if('Offline' == type){
                    $('#filterStatusDrpList').append(new Option('Approved', 'Approved'));
                    $('#filterStatusDrpList').append(new Option('Pending Approval', 'Pending_Approval'));
                    $('#filterStatusDrpList').append(new Option('Pending Submit', 'Pending_Submit'));
                    $('#filterStatusDrpList').append(new Option('Cancelled', 'Cancelled'));
                    $('#filterStatusDrpList').append(new Option('Rejected', 'Rejected'));
                    return;
                }else if('Online' == type){
                    $('#filterStatusDrpList').append(new Option('Success', 'Successful'));
                    $('#filterStatusDrpList').append(new Option('Failed', 'Failed'));
                    $('#filterStatusDrpList').append(new Option('Refunded', 'Refunded'));
                    $('#filterStatusDrpList').append(new Option('Incomplete', 'Incomplete'));
                    return;
                }
            }
        };
        s.filterPaymentType.subscribe(function(item){
            s.reInitFilterStatus(item);
        });

        s.getPartnerVMS = function(paList){
            var vms = [];
            if(paList != undefined && paList != null && paList != ''){
                var count = paList.length;
                for(var i = 0 ; i < count ; i++){
                    var pa = paList[i];
                    vms[i] = {'orgName' : pa['orgName'], 'id' : pa['id'] };
                }
            }
            return vms;
        };

        s.loading = function(){
            s.loadingPartnerList(function(){
                s.loadingTicketMediaTypeList(function(){
                    s.init();
                });
            });
        };

        s.loadingPartnerList = function(callback){
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/get-partner-list',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.partnerList = s.getPartnerVMS(data['data']);
                        callback();
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data.message);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.loadingTicketMediaTypeList = function(callback){
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/get-ticket-media-list',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.ticketMediaList = data['data'];
                        callback();
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data.message);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.init = function(){
            s.filterTdf = $('#filterTdf').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            s.filterTdt = $('#filterTdt').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            s.selPaVms  = $('#selPaVms').kendoMultiSelect({
                dataSource: s.partnerList,
                dataTextField: "orgName",
                dataValueField: "id"
            }).data("kendoMultiSelect");
            s.selTicketMedia = $('#selTicketMedia').kendoComboBox({
                dataSource: s.ticketMediaList,
                dataTextField: "description",
                dataValueField: "name"
            }).data("kendoComboBox");
        };

        s.doReset = function(){
            $('#filterTdf').data('kendoDatePicker').value(null);
            $('#filterTdt').data('kendoDatePicker').value(null);
            $('#selPaVms').data('kendoMultiSelect').value(null);
            $('#selTicketMedia').data('kendoComboBox').value(null);
            s.pinCode('');
            s.fromDtStr('');
            s.toDtStr('');
            s.filterPaymentType('');
            s.receiptNum('');
            s.orgName('');
            s.filterStatus('');
            s.accountCode('');
            s.partners([]);
        };
        s.doView = function(){
            window.winpage.resultView.refreshGrid();
        };
    };

    nvx.ResultView = function(){
        var s = this;
        $('#showHideSubGrid').click(function(){
            var grid = $("#queryGrid").data("kendoGrid");
            if($('section.result-view tr.k-master-row td.k-hierarchy-cell a.k-minus').size() > 0){
                grid.collapseRow(grid.tbody.find("tr.k-master-row"));
            }else{
                grid.expandRow(grid.tbody.find("tr.k-master-row"));
            }
        });
        s.baseUrl = nvx.API_PREFIX + '/trans-query/view-pincode-query-results';
        s.refreshGrid = function(){
            var startDateStr = kendo.toString($('#filterTdf').data('kendoDatePicker').value(), 'dd/MM/yyyy');
            var toDateStr = kendo.toString($('#filterTdt').data('kendoDatePicker').value(), 'dd/MM/yyyy');
            if(!(startDateStr != undefined && startDateStr != null && startDateStr != '')){
                startDateStr = '';
            }
            if(!(toDateStr != undefined && toDateStr != null && toDateStr != '')){
                toDateStr = '';
            }

            var paStr = '';
            var paIds = $("#selPaVms").data('kendoMultiSelect').value();
            if(paIds != undefined && paIds != null && paIds != ''){
                var paIdsCount = paIds.length;
                paStr += ',';
                for(var i = 0  ; i < paIdsCount ; i ++){
                    if(i > 0){
                        paStr += ',';
                    }
                    paStr += (paIds[i]);
                }
                paStr += ',';
            }
            var ticketMedia = $('#selTicketMedia').data('kendoComboBox').value();
            var t = window.winpage.filterView;
            var queryURL = s.baseUrl + '?startDateStr='+startDateStr+'&ticketMediaType='+ticketMedia+'&endDateStr='+toDateStr+'&pincode='+t.pinCode()+'&receiptNum='+t.receiptNum()+'&orgName='+t.orgName()+'&accountCode='+t.accountCode()+'&partnerIds='+paStr;
            s.itemsdataSource.options.transport.read.url = queryURL;
            s.itemsdataSource.page(1);
        };
        s.itemsdataSource = new kendo.data.DataSource({
            transport: {
                read: {url: s.baseUrl, cache: false, type : 'post'}
            },
            requestEnd: function(e) {
                if(e){
                    if(e.response){
                        if(e.response.data){
                            var lst = e.response.data;
                            if(lst != undefined && lst != null && lst.length > 0){
                                var len = lst.length;
                                for(var i  = 0  ; i < len ; i++){
                                    var item = lst[i];
                                    if(item){
                                        var ticketGeneratedDate = item['ticketGeneratedDate'];
                                        if(ticketGeneratedDate != undefined && ticketGeneratedDate != null && ticketGeneratedDate != ''){
                                            item['ticketGeneratedDateStr'] = kendo.toString(new Date(ticketGeneratedDate), 'dd/MM/yyyy dd:mm:ss');
                                        }else{
                                            item['ticketGeneratedDateStr'] = '';
                                        }
                                        var lastRedemptionDate = item['lastRedemptionDate'];
                                        if(lastRedemptionDate != undefined && lastRedemptionDate != null && lastRedemptionDate != ''){
                                            item['lastRedemptionDateStr'] = kendo.toString(new Date(lastRedemptionDate), 'dd/MM/yyyy dd:mm:ss');
                                        }else{
                                            item['lastRedemptionDateStr'] = '';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: 'id',
                    fields: {
                        name: {type: 'string'},
                        pkgTktMedia: {type: 'string'},
                        pinCode: {type: 'string'},
                        ticketGeneratedDate: {type: 'string'},
                        ticketGeneratedDateStr: {type: 'string'},
                        orgName: {type: "string"},
                        lastRedemptionDate: {type: 'string'},
                        lastRedemptionDateStr: {type: 'string'},
                        status: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });

        s.attachPackageItemDetails = function(data, packageId){
            var divId = 'packageItemDetailsOf'+packageId;
            if($('#'+divId).size() > 0){
                $("#"+divId).kendoGrid({
                    dataSource: { data : data },
                    columns: [
                        {
                            field: "receiptNum",
                            title: "Receipt Num"
                        },
                        {
                            field: "qtyRedeemed",
                            template : '#=qtyRedeemed#/#=qty#',
                            title: "Quantity Redeemed"
                        },
                        {
                            field: "displayName",
                            title: "Product Name"
                        },
                        {
                            field: "transItemType",
                            title: "Item Type"
                        },
                        {
                            field: "ticketType",
                            title: "Ticket Type"
                        }
                    ]
                });
            }
        };

        s.loadingPackageItemDetails = function(){
            $('div.packageItemDetails[packageId]').each(function(i){
                var packageId = $(this).attr('packageId');
                if(packageId != undefined && packageId != null && packageId != ''){
                    // begin.......
                    $.ajax({
                        type: 'POST',
                        url: nvx.API_PREFIX + '/trans-query/get-package-item-list?packageId='+packageId,
                        beforeSend : function(){
                            nvx.spinner.start();
                        },
                        success:  function(data, textStatus, jqXHR) {
                            if(data.success){
                                console.log('loading package item details success for package id '+packageId);
                                s.attachPackageItemDetails(data['data'], packageId);
                            }else{
                                if(data.message != null && data.message != ''){
                                    console.log('loading package item details failed for package id '+packageId);
                                }
                            }
                        },
                        error: function(jqXHR, textStatus) {
                            console.log('loading package item details failed for package id '+packageId);
                        },
                        complete: function() {
                            nvx.spinner.stop();
                        }
                    });
                    // end.........
                }
            });
        };

        s.queryGrid = $("#queryGrid").kendoGrid({
            dataSource: s.itemsdataSource,
            dataBound: function(){
                var grid = $("#queryGrid").data("kendoGrid");
                var currentPage = s.itemsdataSource.page();
                var pageSize = s.itemsdataSource.pageSize();
                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });
                this.expandRow(this.tbody.find("tr.k-master-row"));
                s.loadingPackageItemDetails();
            },
            detailInit: function(e){
                var container = '<div class="packageItemDetails" packageId="'+(e.data.id)+'"><div id="packageItemDetailsOf'+(e.data.id)+'"></div></div>';
                $(container).appendTo(e.detailCell);
            },
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
            },{
                field: "name",
                title: "Package Name",
                template: '<a href="javascript:window.winpage.resultView.showDetail(#=id#)" class="link-text" >#=name#</a>',
                sortable: true,
                width: 180
            },{
                field: "pkgTktMedia",
                title: "Ticket Media",
                sortable: true,
                width: 80
            },{
                field: "pinCode",
                title: "Pin Code",
                sortable: true,
                width: 80
            },{
                field: "ticketGeneratedDate",
                title: "Date of PIN<br>Generation",
                template: '#=ticketGeneratedDateStr#',
                sortable: true,
                width: 130
            },{
                field: "orgName",
                title: "Partner<br>Company Name",
                width: 130
            },{
                field: "status",
                title: "Ticket<br>Status",
                sortable: true,
                width: 80
            },{
                field: "lastRedemptionDate",
                title: "Last Redeem<br>Date Time",
                template: '#=lastRedemptionDateStr#',
                sortable: true,
                width: 100
            }
            ],
            sortable: true, pageable: true
        });

        s.initPackageDetailsModel = function(tdvm){
            if (window.winpage.queryModal == undefined || window.winpage.queryModal == null) {
                window.winpage.queryModal = $('#queryModal').kendoWindow({
                    width: '1000px',
                    title: 'Package Details',
                    modal: true,
                    resizable: false
                }).data('kendoWindow');

                window.winpage.queryModel = new nvx.QueryModel(window.winpage.queryModal);
                ko.applyBindings(window.winpage.queryModel, $('#queryModal')[0]);
            }
            window.winpage.queryModel.init(tdvm);
            window.winpage.queryModal.center().open();
        };
        s.showDetail = function(id){
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/view-package-details?id='+id,
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.initPackageDetailsModel(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data['message']);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };

    nvx.QueryModel = function(kWin) {
        var s = this;
        s.kWin = kWin;
        s.id = ko.observable('');
        s.name = ko.observable('');
        s.pinCode = ko.observable('');
        s.status = ko.observable('');
        s.orgName = ko.observable('');
        s.qtyRedeemedStr = ko.observable('');
        s.pinGeneratedDateStr = ko.observable('');
        s.expiryDateStr = ko.observable('');
        s.lastRedemptionDateStr = ko.observable('');
        s.transItems = ko.observableArray([]);
        s.username = ko.observable('');
        s.address = ko.observable('');
        s.accountCode = ko.observable('');
        s.email = ko.observable('');
        s.pkgTktMedia = ko.observable('');
        s.isPkgWithPinCode = ko.computed(function(){
            if(s.pkgTktMedia() != undefined && s.pkgTktMedia() != null && s.pkgTktMedia() != ''){
                if(s.pkgTktMedia() == 'Pincode'){
                    return true;
                }
            }
            return false;
        });

        s.init = function(mmPkgVm) {
            s.clear();
            s.id(mmPkgVm.id);
            s.name(mmPkgVm.name);
            s.pinCode(mmPkgVm.pinCode);
            s.status(mmPkgVm.status);
            s.orgName(mmPkgVm.orgName);
            s.username(mmPkgVm.username)
            s.qtyRedeemedStr(mmPkgVm.qtyRedeemedStr);
            s.pinGeneratedDateStr(mmPkgVm.ticketGeneratedDateStr);
            s.expiryDateStr(mmPkgVm.expiryDateStr);
            s.lastRedemptionDateStr(mmPkgVm.lastRedemptionDateStr);
            s.address(mmPkgVm.address);
            s.accountCode(mmPkgVm.accountCode);
            s.email(mmPkgVm.email);
            s.pkgTktMedia(mmPkgVm.pkgTktMedia);
            if(mmPkgVm.transItems){
                $.each(mmPkgVm.transItems, function(idx, val) {
                    s.transItems.push(new nvx.TransItems(val));
                });
            }

        };

        s.clear = function() {
            s.id("");
            s.name("");
            s.pinCode("");
            s.orgName("");
            s.status("");
            s.qtyRedeemedStr("");
            s.pinGeneratedDateStr("");
            s.expiryDateStr("");
            s.lastRedemptionDateStr("");
            s.username("")
            s.transItems([]);
            s.address("");
            s.accountCode("");
            s.email("");
            s.pkgTktMedia("");
        };
        s.doSendEmail = function() {
            var result = confirm("Do you want to resend the receipt email?");
            if (result == false) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/resend-pkg-receipt?id='+s.id(),
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    alert(data['message']);
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
        s.close = function() {
            s.kWin.close();
            s.clear();
        };
    };

    nvx.TransItems = function(data) {
        var s = this;
        s.displayName = ko.observable(data.displayName);
        s.pkgQty = ko.observable(data.pkgQty);
        s.qtyRedeemed = ko.observable(data.qtyRedeemed);
        s.receiptNum = ko.observable(data.receiptNum);
        s.topupItems = ko.observableArray([]);
        $.each(data.topupItems, function(idx, val) {
            s.topupItems.push(new nvx.TopupItem(val));
        });
    };

    nvx.TopupItem = function(data) {
        var s = this;
        s.displayName = ko.observable(data.displayName);
        s.qtyRedeemed = ko.observable(data.qtyRedeemed);
        s.pkgQty = ko.observable(data.pkgQty);
        s.receiptNum = ko.observable("");
    };

})(window.nvx = window.nvx || {}, jQuery);

