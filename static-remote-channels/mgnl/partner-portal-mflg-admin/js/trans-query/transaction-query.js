var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

(function(nvx, $) {
    $(document).ready(function() {
        $.ajax({
            type: 'POST',
            url: nvx.API_PREFIX + '/trans-query/is-correct-login-user',
            beforeSend : function(){
                nvx.spinner.start();
            },
            success:  function(data, textStatus, jqXHR) {
                if(data.success){
                    // loading page, begin
                    $page = {};
                    $page.filterView = new nvx.FilterView();
                    ko.applyBindings($page.filterView,$('#filterSectionDiv')[0]);
                    $page.filterView.loading();
                    $page.resultView = new nvx.ResultView();
                    window.winpage = $page;
                    // loading page, end
                }else{
                    $('div.container').html(data['message']);
                }
            },
            error: function(jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    });

    nvx.FilterView = function(){
        var s = this;
        s.filterTdf = null;
        s.filterTdt = null;
        s.selPaVms  = null;
        s.fromDtStr = ko.observable('');
        s.toDtStr   = ko.observable('');
        s.filterPaymentType = ko.observable('');
        s.receiptNum = ko.observable('');
        s.orgName = ko.observable('');
        s.filterStatus = ko.observable('');
        s.accountCode = ko.observable('');
        s.partners    = ko.observable([]);
        s.reInitFilterStatus = function(type){
            s.filterStatus('');
            $('#filterStatusDrpList > option').remove();
            $('#filterStatusDrpList').append(new Option('All', ''));
            if(type != undefined && type != null && type != ''){
                if('Offline' == type){
                    $('#filterStatusDrpList').append(new Option('Approved', 'Approved'));
                    $('#filterStatusDrpList').append(new Option('Pending Approval', 'Pending_Approval'));
                    $('#filterStatusDrpList').append(new Option('Pending Submit', 'Pending_Submit'));
                    $('#filterStatusDrpList').append(new Option('Cancelled', 'Cancelled'));
                    $('#filterStatusDrpList').append(new Option('Rejected', 'Rejected'));
                    return;
                }else if('Online' == type){
                    $('#filterStatusDrpList').append(new Option('Success', 'Successful'));
                    $('#filterStatusDrpList').append(new Option('Failed', 'Failed'));
                    $('#filterStatusDrpList').append(new Option('Refunded', 'Refunded'));
                    $('#filterStatusDrpList').append(new Option('Incomplete', 'Incomplete'));
                    return;
                }
            }
        };
        s.filterPaymentType.subscribe(function(item){
            s.reInitFilterStatus(item);
        });

        s.getPartnerVMS = function(paList){
            var vms = [];
            if(paList != undefined && paList != null && paList != ''){
                var count = paList.length;
                for(var i = 0 ; i < count ; i++){
                    var pa = paList[i];
                    vms[i] = {'orgName' : pa['orgName'], 'id' : pa['id'] };
                }
            }
            return vms;
        };

        s.loading = function(){
            //
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/get-partner-list',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.init(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data.message);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.init = function(paList){
            var paVms = s.getPartnerVMS(paList);
            s.filterTdf = $('#filterTdf').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            s.filterTdt = $('#filterTdt').kendoDatePicker({format: nvx.DATE_FORMAT}).data('kendoDatePicker');
            s.selPaVms  = $("#selPaVms").kendoMultiSelect({
                dataSource: paVms,
                dataTextField: "orgName",
                dataValueField: "id"
            }).data("kendoMultiSelect");
        };

        s.doReset = function(){
            $('#filterTdf').data('kendoDatePicker').value(null);
            $('#filterTdt').data('kendoDatePicker').value(null);
            $('#selPaVms').data('kendoMultiSelect').value(null);

            s.fromDtStr('');
            s.toDtStr('');
            s.filterPaymentType('');
            s.receiptNum('');
            s.orgName('');
            s.filterStatus('');
            s.accountCode('');
            s.partners([]);
        };
        s.doView = function(){
            window.winpage.resultView.refreshGrid();
        };
    };

    nvx.ResultView = function(){
        var s = this;
        s.baseUrl = nvx.API_PREFIX + '/trans-query/view-txn-query-results';
        s.refreshGrid = function(){
            var startDateStr = kendo.toString($('#filterTdf').data('kendoDatePicker').value(), 'dd/MM/yyyy');
            var toDateStr = kendo.toString($('#filterTdt').data('kendoDatePicker').value(), 'dd/MM/yyyy');
            if(!(startDateStr != undefined && startDateStr != null && startDateStr != '')){
                startDateStr = '';
            }
            if(!(toDateStr != undefined && toDateStr != null && toDateStr != '')){
                toDateStr = '';
            }

            var paStr = '';
            var paIds = $("#selPaVms").data('kendoMultiSelect').value();
            if(paIds != undefined && paIds != null && paIds != ''){
                var paIdsCount = paIds.length;
                paStr += ',';
                for(var i = 0  ; i < paIdsCount ; i ++){
                    if(i > 0){
                        paStr += ',';
                    }
                    paStr += (paIds[i]);
                }
                paStr += ',';
            }

            var t = window.winpage.filterView;
            var queryURL = s.baseUrl + '?startDateStr='+startDateStr+'&endDateStr='+toDateStr+'&paymentType='+t.filterPaymentType()+'&status='+t.filterStatus()+'&receiptNum='+t.receiptNum()+'&orgName='+t.orgName()+'&accountCode='+t.accountCode()+'&partnerIds='+paStr;
            s.itemsdataSource.options.transport.read.url = queryURL;
            s.itemsdataSource.page(1);
        };
        s.itemsdataSource = new kendo.data.DataSource({
            transport: {
                read: {url: s.baseUrl, cache: false, type : 'post'}
            },
            requestEnd: function(e) {
                if(e){
                    if(e.response){
                        if(e.response.data){
                            var lst = e.response.data;
                            if(lst != undefined && lst != null && lst.length > 0){
                                var len = lst.length;
                                for(var i  = 0  ; i < len ; i++){
                                    var item = lst[i];
                                    if(item){
                                        var isOffline = false;
                                        var status = item['status'];
                                        var offlinePaymentStatus = item['offlinePaymentStatus'];
                                        var offlinePaymentId = item['offlinePaymentId'];
                                        if(offlinePaymentId != undefined && offlinePaymentId != null && offlinePaymentId != '' ){
                                            if(offlinePaymentStatus != undefined && offlinePaymentStatus != null && offlinePaymentStatus != ''){
                                                item['status'] = offlinePaymentStatus;
                                                isOffline = true;
                                            }
                                        }
                                        if(!isOffline){
                                            if(status != null && status != ''){
                                                if('Available' == status || 'Revalidated' == status || 'Refunded' == status || 'Expired' == status || 'Expiring' == status || 'Forfeited' == status ){
                                                    item['status'] = 'Success';
                                                }
                                                else if('Reserved' == status || 'Incomplete' == status){
                                                    item['status'] = 'Success';
                                                }
                                                else if('Refunded' == status){
                                                    item['status'] = 'Refunded';
                                                }
                                                else if('Failed' == status){
                                                    item['status'] = 'Failed';
                                                }else{
                                                    // ignore it.
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: 'id',
                    fields: {
                        receiptNum: {type: 'string'},
                        createdDateStr: {type: 'string'},
                        orgName: {type: "string"},
                        username: {type: "string"},
                        status: {type: "string"},
                        transType: {type: 'string'},
                        paymentType: {type: 'string'},
                        offlinePaymentStatus: {type: 'string'},
                        offlinePaymentId: {type: 'string'}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });

        s.queryGrid = $("#queryGrid").kendoGrid({
            dataSource: s.itemsdataSource,
            dataBound: function(){
                var grid = $("#queryGrid").data("kendoGrid");
                var currentPage = s.itemsdataSource.page();
                var pageSize = s.itemsdataSource.pageSize();
                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1+(currentPage-1)*pageSize;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });
            },
            columns: [{
                field: "rowNumber",
                title: "No.",
                template: "<span class='row-number'></span>",
                sortable: false,
                width: 40
            },{
                field: "receiptNum",
                title: "Receipt<BR>Number",
                template: '<a href="javascript:window.winpage.resultView.showReceipt(#=id#,\'#=transType#\')" class="link-text" >#=receiptNum#</a>',
                sortable: true,
                width: 150
            },{
                field: "createdDateStr",
                title: "Transaction DateTime",
                sortable: true,
                width: 140
            },{
                field: "orgName",
                title: "Partner Company<BR>Name",
                sortable: false,
                width: 115
            },{
                field: "username",
                title: "Purchased By",
                sortable: false,
                width: 115
            },{
                field: "status",
                title: "Status",
                sortable: false,
                width: 115
            },{
                field: "transType",
                title: "Transaction Type",
                sortable: false,
                width: 115
            },{
                field: "paymentType",
                title: "Payment Type",
                sortable: false,
                width: 115
            },{
                field: "linkedTrans",
                title: "Revalidate<BR>Reference",
                sortable: false,
                width: 150
            }
            ],
            sortable: true, pageable: true
        });

        s.initTransDetailsModel = function(tdvm){
            if (window.winpage.queryModal == undefined || window.winpage.queryModal == null) {
                window.winpage.queryModal = $('#queryModal').kendoWindow({
                    width: '1000px',
                    title: 'Transaction Details',
                    modal: true,
                    resizable: false
                }).data('kendoWindow');
                window.winpage.queryModel = new nvx.QueryModel(window.winpage.queryModal);
                ko.applyBindings(window.winpage.queryModel, $('#queryModal')[0]);
            }
            window.winpage.queryModel.init(tdvm);
            window.winpage.queryModal.center().open();
        };
        s.showReceipt = function(id, transType){
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/view-txn-details-results?id='+id+'&transType='+transType,
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.initTransDetailsModel(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data['message']);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };

    nvx.QueryModel = function(kWin) {
        var s = this;
        s.kWin = kWin;
        s.id = ko.observable('');
        s.email = ko.observable('');
        s.status = ko.observable('');
        s.receiptNum = ko.observable('');
        s.tmErrorCode = ko.observable('');
        s.tmErrorMessage = ko.observable('');
        s.tmStatusDateStr = ko.observable('');
        s.accountCode = ko.observable('');
        s.orgName = ko.observable('');
        s.address = ko.observable('');
        s.validityStartDateStr = ko.observable('');
        s.validityEndDateStr = ko.observable('');
        s.username = ko.observable('');
        s.paymentType = ko.observable('');
        s.totalAmountStr = ko.observable('');
        s.revalFeeStr = ko.observable('');
        s.singleRevalFeeStr = ko.observable('');
        s.gstStr = ko.observable('');
        s.gstRateStr = ko.observable('');
        s.revalDtTmStr = ko.observable('');
        s.transType = ko.observable('');
        s.exclGstStr = ko.observable('');
        s.createdDateStr = ko.observable('');
        s.offlinePaymentApprovalStatus = ko.observable('');
        s.offlinePaymentApprovalDateText = ko.observable('');
        s.hasChatHists = ko.observable(false);
        s.chatHists  = ko.observableArray([]);
        s.hasAdminChatHists = ko.observable(false);
        s.adminChatHists  = ko.observableArray([]);
        s.prods = ko.observableArray([]);
        s.revalidateLabel =  ko.computed(function() {
            if(s.transType() == 'Purchase'){
                return "";
            }else{
                return "Revalidation Date/Time:";
            }
        }, this);
        s.init = function(tdVm) {
            s.clear();
            s.id(tdVm.id);
            s.email(tdVm.email);
            s.status(tdVm.status);
            s.receiptNum(tdVm.receiptNum);
            s.tmErrorCode(tdVm.tmErrorCode);
            s.tmErrorMessage(tdVm.tmErrorMessage);
            s.tmStatusDateStr(tdVm.tmStatusDateStr);
            s.accountCode(tdVm.accountCode)
            s.orgName(tdVm.orgName)
            s.address(tdVm.address)
            s.validityStartDateStr(tdVm.validityStartDateStr)
            s.validityEndDateStr(tdVm.validityEndDateStr)
            s.username(tdVm.username)
            s.paymentType(tdVm.paymentType)
            s.totalAmountStr(tdVm.totalAmountStr);
            s.revalFeeStr(tdVm.revalFeeStr);
            s.singleRevalFeeStr(tdVm.singleRevalFeeStr);
            s.gstStr(tdVm.gstStr);
            s.gstRateStr(tdVm.gstRateStr);
            s.revalDtTmStr(tdVm.revalDtTmStr);
            s.transType(tdVm.transType);
            s.exclGstStr(tdVm.exclGstStr);
            s.createdDateStr(tdVm.createdDateStr);
            s.offlinePaymentApprovalStatus(tdVm.offlinePaymentApprovalStatus);
            s.offlinePaymentApprovalDateText(tdVm.offlinePaymentApprovalDateText);
            if(tdVm.prods){
                $.each(tdVm.prods, function(idx, val) {
                    s.prods.push(new nvx.Product(val));
                });
            }
            if(tdVm != undefined && tdVm != null && tdVm != ''){
                var opVM = tdVm['offlinePaymentRequestVM'];
                if(opVM != undefined && opVM != null && opVM != ''){
                    s.chatHists.removeAll();
                    if(opVM != null && opVM['chatHists'] != null){
                        $.each(opVM['chatHists'], function(key, item) {
                            s.hasChatHists(true);
                            s.chatHists.push(new nvx.ChatHist(item));
                        });
                    }else{
                        s.hasChatHists(false);
                    }
                    s.adminChatHists.removeAll();
                    if(opVM != null && opVM['finalApprovalHists'] != null){
                        $.each(opVM['finalApprovalHists'], function(key, item) {
                            s.hasAdminChatHists(true);
                            s.adminChatHists.push(new nvx.ChatHist(item));
                        });
                    }else{
                        s.hasAdminChatHists(false);
                    }
                }
            }
        };

        s.clear = function() {
            s.id("");
            s.email("");
            s.status("");
            s.receiptNum("");
            s.tmErrorCode("");
            s.tmErrorMessage("");
            s.tmStatusDateStr("");
            s.accountCode("")
            s.orgName("")
            s.address("")
            s.validityStartDateStr("")
            s.validityEndDateStr("")
            s.username("")
            s.paymentType("")
            s.prods([]);
            s.totalAmountStr("");
            s.totalAmountStr("");
            s.revalFeeStr("");
            s.gstStr("");
            s.gstRateStr("");
            s.revalDtTmStr("");
            s.transType("");
            s.exclGstStr("");
            s.createdDateStr("");
            s.offlinePaymentApprovalStatus('');
            s.offlinePaymentApprovalDateText('');
            s.hasChatHists(false);
            s.chatHists([]);
            s.hasAdminChatHists(false);
            s.adminChatHists([]);
        };
        s.doSendEmail = function() {
            var result = confirm("Do you want to resend the receipt?");
            if (result == false) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/trans-query/resend-txn-receipt?id='+s.id()+'&transType='+s.transType(),
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    alert(data['message']);
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });

        };
        s.close = function() {
            s.kWin.close();
            s.clear();
        };
    };

    nvx.Product = function(data) {
        var s = this;
        s.displayTitle = ko.observable(data.displayTitle);
        s.subTotalStr = ko.observable(data.subTotalStr);
        s.subTotalRevalStr = ko.observable(data.subTotalRevalStr);
        s.items = ko.observableArray([]);
        $.each(data.items, function(idx, val) {
            s.items.push(new nvx.ProductItem(val));
        });
    };

    nvx.ProductItem = function(data) {
        var s = this;
        s.itemTopup = ko.observable(data.itemTopup);
        s.qty = ko.observable(data.qty);
        s.displayName = ko.observable(data.displayName);
        s.subTotalStr = ko.observable(data.subTotalStr);
        s.unitPriceStr = ko.observable(data.unitPriceStr);
        s.unpackagedQty = ko.observable(data.unpackagedQty);
        s.singleRevalFeeStr = ko.observable(data.singleRevalFeeStr);
        s.revalFeeStr = ko.observable(data.revalFeeStr);
    };

    nvx.ChatHist = function(data){
        var s = this;
        s.username = ko.observable(data.username);
        s.content = ko.observable(data.content);
        s.createDtStr = ko.observable(data.createDtStr);
        s.usertype = ko.observable(data.usertype);
    };

})(window.nvx = window.nvx || {}, jQuery);

