$(document).ready(function() {
    var $page = {};

    $page.startDate = $('#startDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');
    $page.endDate = $('#endDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');
    $page.baseFinReconUrl = nvx.API_PREFIX+'/report/exception-rpt/search';

    $page.columns = [{
        field: "rowNumber",
        title: "No.",
        template: "<span class='row-number'></span>",
        sortable: false,
        width: 40
    },{field: "pinCode",template: function(data) {
        if(data.ticketMedia == 'Pincode') {
            return 'PINCODE <br/>' + data.pinCode + '';
        }else if (data.ticketMedia == 'ETicket') {
            return 'E-Ticket';
        }else if (data.ticketMedia == 'ExcelFile') {
            return 'Excel File';
        }},title: "Ticket Media",width: 100},
        {field: "tmStatusDate",template: "# for(var i=0; i < pkgItems.length; i++){ # #=pkgItems[i].tmStatusDateStr#</BR> # } #",title: "Purchase Date",width: 100},
        {field: "orgName",template: "#=orgName# (#=accountCode#)", title: "Partner Name",width: 100},
        {field: "receiptNum",template: "# for(var i=0; i < pkgItems.length; i++){ # #=pkgItems[i].receiptNum#</BR> # } #",
            title: "Receipt No.",width: 180,sortable: false},
        {field: "displayName", template: "# for(var i=0; i < pkgItems.length; i++){ # #=pkgItems[i].displayName#</BR> # } #",title: "Product Description",width: 150},
        {field: "purchasedQty", title: "Quantity Purchased",width: 150},
        {field: "pinCodeQty", title: "PINCODE Quantity </BR>( # of Ticket)",width: 150},
        {field: "qtyRedeemed",template: "#=qtyRedeemed#",title: "Quantity Redeem",width: 150},
        {field: "v1",template: "#if(v1){##=v1##}else{#0#}#",title: "Variance 1",width: 100},
        {field: "v2",template: "#if(v2){##=v2##}else{#0#}#",title: "Variance 2",width: 100},
        {field: "v3",template: "#if(v3){##=v3##}else{#0#}#",title: "Variance 3",width: 100}
    ];
    $page.FilterModel = function(){
        var s = this;
        s.startDate = ko.observable();
        s.endDate = ko.observable();
        s.orgName = ko.observable();
        s.prodDesc = ko.observable();
        s.receiptNum = ko.observable();

        s.showExport = ko.observable(false);

        s.selPaVms = $("#selPaVms").kendoMultiSelect({
            dataSource: $page.allPaVmsJson,
            dataTextField: "orgName",
            dataValueField: "id"
        }).data("kendoMultiSelect");

        s.reset = function(){
            s.startDate('');
            s.endDate('');
            s.orgName('');
            s.prodDesc('');
            s.receiptNum('');
            s.selPaVms.value('');
        };
        s.doExport = function(){
            if(!s.validate()){
                return;
            }
            window.open(nvx.API_PREFIX+'/report/exception-rpt/export?filterJson='+s.groupParam());
        };
        s.groupParam = function(){
            var filterJson = {};
            if(s.startDate()){
                filterJson.startDateStr = s.startDate();
            }
            if(s.endDate()){
                filterJson.endDateStr = s.endDate();
            }
            if(s.orgName()){
                filterJson.orgName = s.orgName();
            }
            if(s.prodDesc()){
                filterJson.prodDesc = s.prodDesc();
            }
            if(s.receiptNum()){
                filterJson.receiptNum = s.receiptNum();
            }
            filterJson.paIds = s.selPaVms.value();
            return JSON.stringify(filterJson);
        };
        s.refreshUrl = function(){
            console.log("refreshUrl.....");
            if(!s.validate()){
                return;
            }
            s.theUrl = $page.baseFinReconUrl;
            $page.finReconDS.options.transport.read.url = s.theUrl;
            $page.finReconDS.options.transport.read.data = { 'filterJson' : s.groupParam()};
            $page.finReconDS.page(1);
            if(!$page.resultDv){
                $page.resultDv = $("#resultDv").kendoGrid({
                    dataSource: $page.finReconDS,
                    dataBound: dataBound,
                    columns: $page.columns ,
                    sortable: true, pageable: true
                });
            }
        };

        s.validate = function(){
            if($page.startDate.value() == null || $page.endDate.value() == null){
                alert('Both Start date and End date is required.');
                return false;
            }else if ($page.startDate.value() > $page.endDate.value()) {
                alert('Start date must be equal to or later than the End date From date.');
                return false;
            };
            return true;
        }
    };
    function dataBound(e){
        var grid = $("#resultDv").data("kendoGrid");
        var currentPage = $page.finReconDS.page();
        var pageSize = $page.finReconDS.pageSize();
        var rows = this.items();
        $(rows).each(function () {
            var index = $(this).index() + 1+(currentPage-1)*pageSize;
            var rowLabel = $(this).find(".row-number");
            $(rowLabel).html(index);
        });
    };

    $page.initDS = function () {
        $page.finReconUrl = $page.baseFinReconUrl;
        $page.finReconDS = new kendo.data.DataSource({
            transport: {read: {url: $page.baseFinReconUrl, cache: false, type: "POST"}},
            schema: {
                data:function(response) {
                    if(response.data.viewModel){
                        var finReconVms= response.data.viewModel;
                        if(finReconVms&&finReconVms.length > 0){
                            $page.filter.showExport(true);
                        }else{
                            $page.filter.showExport(false);
                        }
                        return finReconVms;
                    }else{
                        return [];
                    }
                },
                total: 'data.total',
                model: {
                    id: 'pinCode',
                    fields: {
                        pinCode: {type: "string"},
                        ticketMedia: {type: "string"},
                        tmStatusDateStr: {type: 'string'},
                        receiptNum: {type: 'string'},
                        orgName: {type: 'string'},
                        accountCode: {type: 'string'},
                        displayName: {type: 'string'},
                        purchasedQty: {type: 'number'},
                        pinCodeQty: {type: 'number'},
                        qtyRedeemed: {type: 'number'},
                        v1: {type: 'number'},
                        v2: {type: 'number'},
                        v3: {type: 'number'},
                        pkgItems:{}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    };

   $page.loadPageData = function(data, status, jqXHR) {
        if (data != undefined) {
            var newData = JSON.parse(data);
            $page.allPaVmsJson =newData;
            $page.initDS();
            $page.filter = new $page.FilterModel();
            ko.applyBindings($page.filter,$('#filter')[0]);
            console.log("$page.filter.....");
        }
    };

    $page.initPageData = function () {
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/report/exception-rpt/init-page',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    //$($page.tabstrip.items()[2]).attr("style", "display:none");
    window.nvxpage = $page;
    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/
});