$(document).ready(function() {
    var $page = {};
    $page.baseMktShareUrl = nvx.API_PREFIX+'/report/country-mktshare-rpt/search';
    $page.startDate = $('#startDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');
    $page.endDate = $('#endDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');
    $page.columns = [{
        field: "rowNumber",
        title: "No.",
        template: "<span class='row-number'></span>",
        sortable: false,
        width: 40
    },{
        field: "uen",
        title: "UEN",
        width: 100
    },{
        field: "orgName",
        title: "Partner Company<BR>Name",
        width: 120
    },{
        field: "status",
        title: "Status",
        width: 100
    },{
        field: "total",
        template: "#=totalStr#",
        title: "Transaction<BR>Amount",
        width: 100
    }
    ];
    $page.subColumns = [{ field: "tranDateStr",
        title: "Date",
        width: 50
    },{
        field: "amount",
        template: "#=amountStr#",
        title: "Amount",
        width: 90
    }]



    $page.FilterModel = function(){
        var s = this;
        s.startDate = ko.observable();
        s.endDate = ko.observable();
        s.orgName = ko.observable();


        s.selPaVms = $("#selPaVms").kendoMultiSelect({
            dataSource: $page.allPaVmsJson,
            dataTextField: "orgName",
            dataValueField: "id"
        }).data("kendoMultiSelect");

        s.showExport = ko.observable(false);
        s.reset = function(){
            s.startDate('');
            s.endDate('');
            s.orgName('');
            s.selPaVms.value('');
        };
        s.doExport = function(){
            if(!s.validate()){
                return;
            }
            window.open(nvx.API_PREFIX+'/report/country-mktshare-rpt/export?filterJson='+s.groupParam());
        };
        s.groupParam = function(){
            var filterJson = {};
            if(s.startDate()){
                filterJson.startDateStr = s.startDate();
            }
            if(s.endDate()){
                filterJson.endDateStr = s.endDate();
            }
            if(s.orgName()){
                filterJson.orgName = s.orgName();
            }
            filterJson.paIds = s.selPaVms.value();
            return JSON.stringify(filterJson);
        };
        s.refreshUrl = function(){
            console.log("refreshUrl.....");
            if(!s.validate()){
                return;
            }
            s.theUrl = $page.baseMktShareUrl;
            $page.mktShareDS.options.transport.read.url = s.theUrl;
            $page.mktShareDS.options.transport.read.data = { 'filterJson' : s.groupParam()};
            $page.mktShareDS.read();
            $page.mktShareDS.page(1);

        };

        s.validate = function(){
            if($page.startDate.value() == null || $page.endDate.value() == null){
                alert('Both Start date and End date is required.');
                return false;
            }else if ($page.startDate.value() > $page.endDate.value()) {
                alert('Start date must be equal to or later than the End date From date.');
                return false;
            }
            var startDate = $page.startDate.value();
            var endDate = $page.endDate.value();
            var difference = endDate - startDate;
            var days = Math.floor(difference / (1000*60*60*24));
            if (days > 60) {
                alert('Date range should within 60 days');
                return false;
            }
            return true;
        }
    };

    function dataBound(e){
        var grid = $("#resultDv").data("kendoGrid");
        var currentPage = $page.mktShareDS.page();
        var pageSize = $page.mktShareDS.pageSize();
        var rows = this.items();
        $(rows).each(function () {
            var index = $(this).index() + 1+(currentPage-1)*pageSize;
            var rowLabel = $(this).find(".row-number");
            $(rowLabel).html(index);
        });
    };

    $page.initDS = function () {
        $page.mktShareUrl = $page.baseMktShareUrl;
        $page.mktShareDS = new kendo.data.DataSource({
            transport: {read: {url: $page.mktShareUrl, cache: false, type: "POST"}},
            schema: {
                data:function(response) {
                    if(response.data.viewModel){
                        $page.countryVmsJson = response.data.viewModel['allCountryVMs'];
                        var countryVmsJsonLength = $page.countryVmsJson.length;
                        for (var i = 0; i < countryVmsJsonLength; i++) {
                            var cty = $page.countryVmsJson[i];
                            $page.columns.push({ title: cty.ctyName, field: cty.ctyName.replace(/\s+/g, '')+cty.ctyCode,sortable: false, width: '100px' });
                            $page.subColumns.push({ title: cty.ctyName, field: cty.ctyName.replace(/\s+/g, '')+cty.ctyCode,sortable: false, width: '100px' });
                        }
                        if(!$page.resultDv){
                            $page.resultDv = $("#resultDv").kendoGrid({
                                dataSource: $page.mktShareDS,
//                detailTemplate: '<div class="sub-detail"></div>',
//                detailInit: detailInit,
                                dataBound: dataBound,
                                columns: $page.columns ,
                                sortable: true, pageable: true
                            });
                        }
                        var paMkts= response.data.viewModel.mktSharePartnerVMs;
                        for(var i=0;i<paMkts.length;i++){
                            for(var j=0;j< paMkts[i].mktRegionDetails.length;j++){
                                paMkts[i][paMkts[i].mktRegionDetails[j].ctyName.replace(/\s+/g, '')+paMkts[i].mktRegionDetails[j].ctyId] = paMkts[i].mktRegionDetails[j].ammountStr;
                            }
                        }
                        if(paMkts&&paMkts.length > 0){
                            $page.filter.showExport(true);
                        }else{
                            $page.filter.showExport(false);
                        }

                        return paMkts;
                    }else{
                        return [];
                    }
                },
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: "number"},
                        uen: {type: 'string'},
                        status: {type: 'string'},
                        total: {type: 'number'},
                        totalStr: {type: 'string'},
                        orgName: {type: 'string'},
                        status: {type: 'string'},
                        dailyTrans: {}
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });
    };

    $page.loadPageData = function(data, status, jqXHR) {
        if (data != undefined) {
            var newData = JSON.parse(data);
            $page.allPaVmsJson = newData['allPartnerVMs'];
            // $page.countryVmsJson = newData['countryVMs'];
            // //console.log(countryVmsJson);
            // var countryVmsJsonLength = $page.countryVmsJson.length;
            // for (var i = 0; i < countryVmsJsonLength; i++) {
            //     var cty = $page.countryVmsJson[i];
            //     $page.columns.push({ title: cty.ctyName, field: cty.ctyName.replace(/\s+/g, '')+cty.id,sortable: false, width: '100px' });
            //     $page.subColumns.push({ title: cty.ctyName, field: cty.ctyName.replace(/\s+/g, '')+cty.id,sortable: false, width: '100px' });
            // }
            // $page.sysSettingJson = newData['sysMap'];
            // $page.approverJson = newData['approverVm'];
            // $page.candidatesJson = newData['candidatesList'];
            $page.initDS();
            $page.filter = new $page.FilterModel();
            ko.applyBindings($page.filter,$('#filter')[0]);
            console.log("$page.filter.....");
        }
    };

    $page.initPageData = function () {
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/report/country-mktshare-rpt/init-page',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };
    window.nvxpage = $page;
    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/
});