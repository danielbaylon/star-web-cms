[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]


    <div ng-controller="">
        <div ng-include=""></div>
        <div class="admission-dialog">
        	<div class="admission-qn">
                <h1 ng-bind="title.question1">${staticText[lang]['adminssionTitle']}</h1>
                <p ng-bind="title.hint">${staticText[lang]['admissionTitleHit']}</p>
            </div>
            <div class="add-tix">
            	<h3 ng-bind="title.question2">${staticText[lang]['admissionTitleAddToCart']}</h3>
            	<img src="${themePath}/images/ce-line.png"/>
            </div>
            <div class="submit-btn box-wrapper center-center">
                <button type="button" class="btn btn-secondary btn-md" id="btn-no-go-summary">${staticText[lang]['admissionNO']}</button>
                <button type="button" class="btn btn-primary btn-md" id="btn-add-tickets">${staticText[lang]['admissionYes']}</button>
                <div class="btn-info"><span class="arrow-up"></span><span ng-bind="title.instruction">${staticText[lang]['admissionEZlink']}</span></div>
            </div>
        </div>
    </div>

<script>
   $(document).ready(function() {
   
    function redirectToOrderSummaryPage() {
        window.location.href='${sitePath}/ordersummary';
    }
    
   	$('#btn-no-go-summary').click(function(e){
   			var customerDetail = function() {
	            return {
	                email: 'abc@test123.com',
	                idType: '',
	                idNo: '1221322323',
	                name: 'Abc',
	                mobile: 89377834,
	                paymentType: 'master',
	                subscribed: true,
	                nationality: 'Singapore',
	                referSource: '',
	                dob: '20/10/1990'
	            };
	        };
	
	        $.ajax({
	            url: '/.store-kiosk/store/checkout',
	            data: JSON.stringify(customerDetail()), headers: { 'Store-Api-Channel' : '${channel}' },
	            type: 'POST',
	            cache: false,
	            dataType: 'json',
	            contentType: 'application/json',
	            success: function (data) {
	                if (data.success) {
	                    redirectToOrderSummaryPage();
	                } else {
	                    redirectToErrorPage();
	                }
	            },
	            error: function () {
	                redirectToErrorPage();
	            },
	            complete: function () {
	            }
	        });
   	});
   	
   	$('#btn-add-tickets').click(function(e){
   		window.location.href='${sitePath}/admission~?fromShoppingCart=true';
   	});
   	
   	
   })
</script>