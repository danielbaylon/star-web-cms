[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign admissionCategoryList = kioskfn.getIslandAdmissionCategoryList(channel)]
[#assign params = ctx.aggregationState.getSelectors()]
[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    var productsInShoppingCart = [];
    var itemToDelete;
    var hasAdmission = false;

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPrice = "S$0.00";

                    if(result.data.totalQty != 0) {
                        totalPrice = result.data.totalText.replace(/\s/g, '');
                    }

                    productsInShoppingCart = result.data.cmsProducts;

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);

                    loadScroller();
                    hasAdmission = result.data.hasAdmission;
                    if(!hasAdmission){
                    	[#if admissionCategoryList?size > 0 ]
							$('#admission-note').show();
						[/#if]
					}
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function deleteItemInCart(cartItemId) {

        itemToDelete = cartItemId;

        if(checkIfItemIsTopUp(cartItemId)) {
            showInfoDialog("${staticText[lang]['infoTitle']}", "${staticText[lang]['removeTopupConfirmationMessage']}");
            return;
        }

        deleteItem(cartItemId);
    }

    function checkIfItemIsTopUp(cartItemId) {

        var hasTopUp = false;

        $.each(productsInShoppingCart, function(index, product) {

            var foundItem = false;
            var parentListingId = "";

            $.each(product.items, function(index, item) {

                if(item.cartItemId === cartItemId && item.topup === false) {
                    foundItem = true;
                    parentListingId = item.listingId;

                    return false;
                }
            });

            if(foundItem) {
                $.each(product.items, function(index, item) {

                    if(item.parentListingId === parentListingId && item.topup === true) {
                        hasTopUp = true;
                        return false;
                    }
                });

                return false;
            }
        });

        return hasTopUp;
    }

    function deleteItem(cartItemId) {
		openLoadingPage();
        $.ajax({
            url : '/.store-kiosk/store/remove-item-from-cart/' + cartItemId,
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
				closeLoadingPage();
                if (result.success) {

                    productsInShoppingCart = result.data.cmsProducts;
                    var totalPrice = result.data.totalText.replace(/\s/g, '');

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);
                    setTimeout(function () {
                        $('.fun-box').addClass('move');
                        setTimeout(function () {
                            $('.fun-box').removeClass('move');
                        }, 800);
                    }, 300);
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
            	closeLoadingPage();
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {
			if(product.isTransportLayout){
	            rowsHtml += '<tr>' +
	                    '	<td colspan="6" class="cart-title">' +
	                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">' +
	                    '			<tr>' +
	                    '				<td>' +
	                    '					<span class="product-title">' + product.name + '</span>' +
	                    '					<button type="button" class="btn-cart btn-edit" onclick="redirectToEditAdmissionPage(\'' + product.id + '\')">${staticText[lang]["editButtonLabel"]}</button>' +
	                    '				</td>' +
	                    '				<td colspan="4">&nbsp;</td>' +
	                    '			</tr>' +
	                    '		</table>' +
	                    '	</td>' +
	                    '</tr>' +
	                    '<tr>' +
	                    '	<td>' +
	                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">';			
			
			}else{
			
	            rowsHtml += '<tr>' +
	                    '	<td colspan="6" class="cart-title">' +
	                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">' +
	                    '			<tr>' +
	                    '				<td>' +
	                    '					<span class="product-title">' + product.name + '</span>' +
	                    '					<button type="button" class="btn-cart btn-edit" onclick="redirectToOrderPage(\'' + product.id + '\')">${staticText[lang]["editButtonLabel"]}</button>' +
	                    '				</td>' +
	                    '				<td colspan="4">&nbsp;</td>' +
	                    '			</tr>' +
	                    '		</table>' +
	                    '	</td>' +
	                    '</tr>' +
	                    '<tr>' +
	                    '	<td>' +
	                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">';
			}
            $.each(product.items, function(index, item) {

                if(item.topup) {

                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="adds-on">' +
                            '                   <table cellpadding="0" cellspacing="0" border="0">' +
                            '                       <tr>' +
                            '                           <td><img src="${themePath}/images/icon-addson.png"></td>' +
                            '                           <td class="title">' + item.name +
                            '                               <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>';
               if(item.selectedEventDate){
               
					rowsHtml +=                           
					        '                               <p class="tix-type"> ${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';						
				                         }
               if(item.eventLineId){
               
					rowsHtml += 
					        '                               <p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : '  + item.eventSessionName + '</p>';						
				                   }				     
                    rowsHtml += 
                            '                               <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '                           </td>' +
                            '                       </tr>' +
                            '                   </table>' +
                            '               </td>';
                }
                else {
                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="title">' + item.name +
                            '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>' ;
               if(item.selectedEventDate){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';							
				                         }
               if(item.eventLineId){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : ' + item.eventSessionName + '</p>';						
				                   }	
				    rowsHtml += 
				            '                   <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '               </td>';
                }

                rowsHtml += ' <td width="15%" class="center">S$' + item.price.toFixed(2) + '</td>' +
                        ' <td width="11%" class="center">' + item.qty + '</td>';

                if (item.discountTotal != undefined && item.discountTotal > 0) {
                    rowsHtml += ' <td width="18%" class="center">' +
                            '	<span style="color: red; text-decoration: line-through;">S$' + (parseFloat(item.total) + parseFloat(item.discountTotal)).toFixed(2) + '</span><br />' +
                            ' 	S$' + parseFloat(item.total).toFixed(2) +
                            ' </td>';
                }
                else {
                    rowsHtml += ' <td width="18%" class="center">S$' + parseFloat(item.total).toFixed(2) + '</td>';
                }

                rowsHtml += '  				<td width="5%" class="center del-container">' +
                        '                   <button type="button" class="btn-del" onclick="deleteItemInCart(\'' + item.cartItemId + '\')"></button>' +
                        '               </td>' +
                        '           </tr>' +
                        '       </table>' +
                        '   </td>' +
                        '</tr>';
            });

            rowsHtml += '		</table>' +
                    '	</td>' +
                    '</tr>';
        });

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }

    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }

    function clearShoppingCart() {
		openLoadingPage();
        $.ajax({
            url : '/.store-kiosk/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
            	closeLoadingPage();
                if (result.success) {
                    redirectToHomePage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function checkout() {
    
    
		if(!hasAdmission){
			redirectToAdmissionPage();
		}else{
			var customerDetail = function() {
	            return {
	                email: 'abc@test123.com',
	                idType: '',
	                idNo: '1221322323',
	                name: 'Abc',
	                mobile: 89377834,
	                paymentType: 'master',
	                subscribed: true,
	                nationality: 'Singapore',
	                referSource: '',
	                dob: '20/10/1990'
	            };
	        };
	
	        $.ajax({
	            url: '/.store-kiosk/store/checkout',
	            data: JSON.stringify(customerDetail()), headers: { 'Store-Api-Channel' : '${channel}' },
	            type: 'POST',
	            cache: false,
	            dataType: 'json',
	            contentType: 'application/json',
	            success: function (data) {
	                if (data.success) {
	                    redirectToOrderSummaryPage();
	                } else {
	                    redirectToErrorPage();
	                }
	            },
	            error: function () {
	                redirectToErrorPage();
	            },
	            complete: function () {
	            }
	        });
		}

    }

    function redirectToHomePage() {
        window.location.href='${sitePath}';
    }
    
    function redirectToAdmissionPage() {
        window.location.href='${sitePath}/needadmission~';
    }

    function redirectToOrderPage(productNodeId) {
        window.location.href='${sitePath}/category/product/order~${categoryNodeName}~${subCategoryName}~' + productNodeId + '~?pageNumber=${pageNumber}&backToCart=true';
    }
    
    function redirectToEditAdmissionPage(productNodeId) {
        window.location.href='${sitePath}/admission~?fromShoppingCart=true&&productNodeId=' + productNodeId;
    }

    function redirectToOrderSummaryPage() {
        window.location.href='${sitePath}/ordersummary';
    }

    function loadScroller() {

        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    $(document).ready(function() {
        fetchShoppingCartItems();

        $("#okButton").click(function() {
            deleteItem(itemToDelete);
            hideInfoDialog();
        });
    });

</script>

[#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]

[#assign counter = 0]

<div class="main">
    <aside ng-include="templateSideBarUrl" class="ng-scope">

        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">
			[#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]
            [#list categoryList as category]
                [#assign counter = counter + 1]
			    [#if category.hasProperty("iconImage")]
			        [#assign categoryImage = category.getProperty("iconImage").getString()]
			    [/#if]
			
			    [#if categoryImage?has_content]
			        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
			    [/#if]
			    
			    [#assign menuTransportLayout = "false"]
				[#if category.hasProperty("transportLayout")]
			        [#assign menuTransportLayout = category.getProperty("transportLayout").getString()]
			    [/#if]
                <li>
                    [#if (category.getProperty("name").getString()?upper_case == 'FUN PASS')]
                        <div><button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} icon-funpass box-wrapper center-center [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                        </button><div>
						[#assign moreFunCategoryNode = category.getName()]                        
                    [#elseif menuTransportLayout == "true"]
                    	
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='admission~${category.getName()}~'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            [#else]
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~?transportLayout=${menuTransportLayout}'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            
                    [/#if]
                    

                </li>

            [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>

    <div class="main-content">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['funCartLabel']}</h1>
        <div class="fun-cart-content">
            <p class="cart-note">${staticText[lang]['shoppingCartNote']}</p>
            <div class="cart-box ticket-container">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                    <tr>
                        <td width="46%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitlePrice']}</td>
                        <td width="11%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                        <td width="18%" class="center">${staticText[lang]['shoppingCartTitleSubtotal']}</td>
                        <td width="10%" class="center">&nbsp;</td>
                    </tr>
                </table>
                <div class="tab-content-container for-template">
                    <div id="scrollbar2" style="height:440px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="total-bar">
                    <div class="btn-container">
                        <button type="button" class="btn btn-edit btn-clear" onclick="clearShoppingCart()">${staticText[lang]['clearCartButtonLabel']}</button>
                    </div>
                    <div class="total">
                    ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                    </div>
                </div>
                
                <div id="admission-note" class="admission-note" style="display:none">
                    <p>Please note that the above items DO NOT include admission/entry fee into Sentosa island.</p>
                </div>
                <div class="cart-group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-secondary btn-md" onclick="redirectToCategoryPage()">${staticText[lang]['addMoreFunButtonLabel']}</button>
                    <button type="button" class="btn btn-primary btn-md" onclick="checkout()">${staticText[lang]['proceedButtonLabel']}</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function redirectToCategoryPage() {
        window.location.href='${sitePath}/category~${moreFunCategoryNode}~';
    }
</script>
