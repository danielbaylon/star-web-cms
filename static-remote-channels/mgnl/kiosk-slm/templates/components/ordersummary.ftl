[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    var productsInShoppingCart = [];

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPrice = "S$0.00";

                    if(result.data.totalQty != 0) {
                        totalPrice = result.data.totalText.replace(/\s/g, '');
                    }

                    productsInShoppingCart = result.data.cmsProducts;

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);

                    loadScroller();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {

            rowsHtml += '<tr>' +
                    '	<td colspan="6" class="cart-title">' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                    '			<tr>' +
                    '				<td>' +
                    '					<span class="product-title">' + product.name + '</span>' +
                    '				</td>' +
                    '				<td colspan="4">&nbsp;</td>' +
                    '			</tr>' +
                    '		</table>' +
                    '	</td>' +
                    '</tr>' +
                    '<tr>' +
                    '	<td>' +
                    '		<table cellpadding="0" cellspacing="0" border="0" width="100%">';

            $.each(product.items, function(index, item) {

                if(item.topup) {

                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="adds-on">' +
                            '                   <table cellpadding="0" cellspacing="0" border="0">' +
                            '                       <tr>' +
                            '                           <td><img src="${themePath}/images/icon-addson.png"></td>' +
                            '                           <td class="title">' + item.name +
                            '                               <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>';
               if(item.selectedEventDate){
               
					rowsHtml +=                           
					        '                               <p class="tix-type"> ${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';						
				                         }
               if(item.eventLineId){
               
					rowsHtml += 
					        '                               <p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : '  + item.eventSessionName + '</p>';						
				                   }				     
                    rowsHtml += 
                            '                               <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '                           </td>' +
                            '                       </tr>' +
                            '                   </table>' +
                            '               </td>';
                }
                else {
                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="title">' + item.name +
                            '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>' ;
               if(item.selectedEventDate){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';							
				                         }
               if(item.eventLineId){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : ' + item.eventSessionName + '</p>';						
				                   }	
				    rowsHtml += 
				            '                   <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '               </td>';
                }

                rowsHtml += ' <td width="15%" class="center">S$' + item.price.toFixed(2) + '</td>' +
                        ' <td width="11%" class="center">' + item.qty + '</td>';

                if (item.discountTotal != undefined && item.discountTotal > 0) {
                    rowsHtml += ' <td width="18%" class="center">' +
                            '	<span style="color: red; text-decoration: line-through;">S$' + (parseFloat(item.total) + parseFloat(item.discountTotal)).toFixed(2) + '</span><br />' +
                            ' 	S$' + parseFloat(item.total).toFixed(2) +
                            ' </td>';
                }
                else {
                    rowsHtml += ' <td width="18%" class="center">S$' + parseFloat(item.total).toFixed(2) + '</td>';
                }

                rowsHtml += ' 		</tr>' +
                        '       </table>' +
                        '   </td>' +
                        '</tr>';
            });

            rowsHtml += '		</table>' +
                    '	</td>' +
                    '</tr>';
        });

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }

    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function popupTNC() {

        $("#tncDialog").show();
        $('#scrollbar6-scrollbar').remove();
        var myScroll6;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
            myScroll6 = new IScroll('#scrollbar6', { scrollbars: 'custom', interactiveScrollbars: true });
        }, 1);
    }

    function closeOnTNC() {
        $("#tncDialog").hide();
    }

    function checkAgreeToTAndC() {

        if($("#tnc-check-box").is(":checked")) {
            proceedToPay();
        }
        else {
            popupTNC();
        }
    }

    function proceedToPay() {
        closeOnTNC();
        redirectToPaymentPage();
    }

    function redirectToPaymentPage() {
        window.location.href = "${sitePath}/payment";
    }

    function loadScroller() {
        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    $(document).ready(fetchShoppingCartItems());

</script>

<div class="main box-wrapper top-center">
    <div class="main-content">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['orderSummaryTitle']}</h1>

        <div class="fun-cart-content">
            <div class="cart-box ticket-container summary-dialog" ng-class="{'summary-dialog-1':localPreReceiptType=='Recovery' || localPreReceiptType=='B2C','summary-dialog':localPreReceiptType!='Recovery' &amp;&amp; localPreReceiptType!='B2C'}">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                    <tr>
                        <td width="37%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitlePrice']}</td>
                        <td width="10%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitleSubtotal']}</td>
                        <td width="5%" class="center">&nbsp;</td>
                    </tr>
                </table>
                <div class="tab-content-container fif-template">
                    <div id="scrollbar2" style="height:490px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="total-bar">
                        <div class="btn-container">
                            <input id="tnc-check-box" type="checkbox" />
                            <span>${staticText[lang]['agreeToTermLabel1']}</span>
                            <button type="button" class="t-c" onclick="popupTNC()">${staticText[lang]['tAndC']}</button>
                            <span>${staticText[lang]['agreeToTermLabel2']}</span>
                        </div>
                        <div class="total">
                        ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                        </div>
                    </div>
                    <div class="cart-group-btn box-wrapper center-center">
                        <button type="button" class="btn btn-third btn-md" onclick="redirectToShoppingCartPage()">${staticText[lang]['backToCartButtonLabel']}</button>
                        <button type="button" class="btn btn-primary btn-md" onclick="checkAgreeToTAndC()">${staticText[lang]['proceedToPayButtonLabel']}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tncDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button" onclick="closeOnTNC()"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['tAndC']}</h2>
        <section>
            <div id="scrollbar6" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                <p>
									[#assign generalAndProductTnc = starfn.getTermsAndConditions(channel)]
									${generalAndProductTnc}
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-third" onclick="closeOnTNC()">${staticText[lang]['disagreeButtonLabel']}</button>
            <button type="button" class="btn btn-md btn-primary" onclick="proceedToPay()">${staticText[lang]['agreeButtonLabel']}</button>
        </div>
    </div>
</div>