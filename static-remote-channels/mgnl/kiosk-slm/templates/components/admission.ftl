[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign admissionCategoryList = kioskfn.getIslandAdmissionCategoryList(channel)]


[#assign categoryNodeName = ""]

[#assign params = ctx.aggregationState.getSelectors()]
[#if params[0]??]
    [#assign categoryNodeName = params[0]]
[/#if]

[#assign fromShoppingCart = ""]
[#if ctx.getParameter("fromShoppingCart")?has_content]
    [#assign fromShoppingCart = ctx.getParameter("fromShoppingCart")]
[/#if]

[#assign productNodeId = ""]
[#if ctx.getParameter("productNodeId")?has_content]
    [#assign productNodeId = ctx.getParameter("productNodeId")]
[/#if]

[#assign categoryNode = starfn.getCMSProductCategory(channel, categoryNodeName)]
[#assign counter = 0]

<script> 			
var currentProductListingId = '';
var itemsInShoppingCart = [];
var itemsOnCurrentPage = [];
var itemsToAdd = [];
</script>

<div class="main">
	[#if fromShoppingCart != "true"]
    <aside ng-include="templateSideBarUrl" class="ng-scope">

        <ul class="sidebar-list ng-scope" ng-controller="SideBarCntl">
			[#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]
            [#list categoryList as category]
                [#assign counter = counter + 1]
			    [#if category.hasProperty("iconImage")]
			        [#assign categoryImage = category.getProperty("iconImage").getString()]
			    [/#if]
			
			    [#if categoryImage?has_content]
			        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
			    [/#if]
			    
			    [#assign menuTransportLayout = "false"]
				[#if category.hasProperty("transportLayout")]
			        [#assign menuTransportLayout = category.getProperty("transportLayout").getString()]
			    [/#if]
                <li>
                    [#if (category.getProperty("name").getString()?upper_case == 'FUN PASS')]
                        <div><button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} icon-funpass box-wrapper center-center [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~'">
                            <img src="${themePath}/images/${staticText[lang]['funPassImageName']!'funpass-en.png'}" />
                        </button><div>
                    [#elseif menuTransportLayout == "true"]
                    	
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='admission~${category.getName()}~'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            [#else]
						<div>
						<img style="height: 80px; width: 50px;position:absolute;left:50px;" [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
		                    <button type="button" style="height:80px;margin:30px;margin-left:20px;" class="btn-fifth menu-${counter} box-wrapper center-left [#if (categoryNodeName == category.getName())] selected [/#if]" onclick="javascript:window.location.href='category~${category.getName()}~?transportLayout=${menuTransportLayout}'">
		                    		<span class="menu-text ng-binding">${category.getProperty("name").getString()}</span>
		                    </button>
		                </div>
		            
                    [/#if]
                    

                </li>

            [/#list]

            <div class="clearfix"></div>
        </ul>
    </aside>
	[/#if]

        <div id="wrapper" style="text-align: center">    


			<div class="main-content">
				[#if fromShoppingCart == "true"]
				    <div class="box-wrapper" style="float: right;">
	                	<div class="box-wrapper admission-big-dialog">
	                		
				        	<div class="admission-qn">
				                <h1 ng-bind="title.question1">${staticText[lang]['adminssionTitle']}</h1>
				                <p ng-bind="title.hint">${staticText[lang]['admissionTitleHit']}</p>
				            </div>
				            <div class="add-tix">
				            	<h3 ng-bind="title.question2">${staticText[lang]['admissionTitleAddToCart']}</h3>
				            	<img src="${themePath}/images/ce-line.png"/>
				            </div>
				            <div id="div-scrollbar-ticket" style="position:relative;height:305px;width:100%;">
				[#else]
					<h1 class="primary-heading-1 list-title ng-binding">${categoryNode.getProperty("name").getString()}</h1>
					<div class="box-wrapper center-center">
						<div class="side-an-dialog box-wrapper center-center">
	                    	<img src="${themePath}/images/icon-express.png">
	                    	<h1 class="primary-heading sy-hg-md ng-binding" ng-bind-html="title.question">${staticText[lang]['addTicketToCartConfirmationLabel']!"Would you like to Sentosa Express tickets to your cart?"}</h1>
	                    	<p ng-bind-html="title.hint1" class="ng-binding">${staticText[lang]['addTicketToCartBodyMessageLabel']!"You can enter Sentosa by using your EZlink card or purchase Sentosa Express tickets here."}</p>
				[/#if]
	                    <table cellpadding="0" cellspacing="0" border="0" class="admission-qty">
	                        <tbody>
	                        <tr>
	                            <td colspan="2">&nbsp;</td>
	                            <td colspan= "3" class="qty-top ng-binding" ng-bind="lbl.quantity">${staticText[lang]['quantityLabel']!"Quantity"}</td>
	                            
	                        </tr>
	                        
	                        [#if fromShoppingCart != "true"]
	                        	[#assign subCategoryList = starfn.getCMSProductSubCategories(categoryNode)]
	                        	[#list subCategoryList as subCategory]
		                        	[#assign cmsProductList = starfn.getCMSProductBySubCategory(subCategory)]
		                        	[#list cmsProductList as cmsProduct]
			                        	    <tr class="">
										            <td align="left" colspan="2" style="font-size: 22px;color: #f66011;font-weight: bold;"> ${cmsProduct.getProperty("name").getString()}
										            </td>
										            <td></td>
										    </tr>
								            [#assign axPrdList = starfn.getAXProductsByCMSProductNode(cmsProduct)!]
								            [#assign axPrdDetailsMap = starfn.getAXProductsDetailByCMSProductNode(cmsProduct)!]
								            [#list axPrdList as axPrd]
								                  	[#assign itemName = ""]
								                  	[#assign ticketType = ""]
								                  	[#assign productPrice = ""]
								                  	[#assign productListingId = ""]
								                  	[#assign axPrdDetail = axPrdDetailsMap[axPrd.getProperty("relatedAXProductUUID").getString()!]]
								                  	
										            [#if axPrdDetail.hasProperty("productName")]
										            [#assign itemName = axPrdDetail.getProperty("productName").getString()!]
										            [/#if]
										            
										            [#if axPrdDetail.hasProperty("ticketType")]
										            [#assign ticketType = axPrdDetail.getProperty("ticketType").getString()!]
										            [/#if]
	
										            [#if axPrdDetail.hasProperty("productPrice")]
										            [#assign productPrice = axPrdDetail.getProperty("productPrice").getString()!]
										            [/#if]
										            
										            [#if axPrdDetail.hasProperty("productListingId")]
										            [#assign productListingId = axPrdDetail.getProperty("productListingId").getString()!]
										            [/#if]
										            [#if cmsProduct.hasProperty("admissionIncluded") && cmsProduct.getProperty("admissionIncluded").getString() == "true"]
										        
				                        			<tr>
				                        				<td align="left" class="" width="50%"> <b>${itemName}</b></td>
				                        				<td width="20%"> <b>${ticketType} - ${productPrice}</b></td>
				                            			<td style="padding-top:10px;"><button product-id="${productListingId}" id="btn-minus-qty-${productListingId}" type="button" class="btn-minus-qty-class icon-minus btn-inactive" ng-class="{'btn-inactive':ticket.qty<1,'btn-active':ticket.qty>=1}" ng-click="minusQty()"></button></td>
				                            			<td style="padding-top:10px;"><input product-id="${productListingId}" id="input-qty-${productListingId}" type="text" placeholder="0" class="input-ticket-num-class ng-pristine ng-untouched ng-valid"></td>
				                            			<td style="padding-top:10px;"><button product-id="${productListingId}" id="btn-add-qty" type="button" class="btn-add-qty-class icon-plus btn-active" ng-class="{'btn-inactive':ticket.qty>=maxTickets,'btn-active':ticket.qty<maxTickets}" ng-click="addQty()"></button></td>
				                        			</tr>
				                        			[/#if]
				                        	        <script>
				                        			                var item = {
													                    cmsProductId : '${cmsProduct.getName()}',
													                    cmsProductName : '${cmsProduct.getProperty("name").getString()}',
													                    listingId : '${productListingId}',
													                    productCode : '${axPrdDetail.getProperty("displayProductNumber").getString()}',
													                    name : '${itemName}',
													                    type : '${ticketType}',
													                    qty : 0,
													                    price : parseFloat(${productPrice}),
													                    lineId : ''
													                };
													                
													                itemsOnCurrentPage.push(item);
				                        			</script>
	
			                        		[/#list]
		                        		
		                        	[/#list]
		                        [/#list]
	                        [#else]
	                        	[#list admissionCategoryList as admissionCategory]
	                        		[#assign subCategoryList = starfn.getCMSProductSubCategories(admissionCategory)]
	                        	    [#list subCategoryList as subCategory]
			                        	[#assign cmsProductList = starfn.getCMSProductBySubCategory(subCategory)]
			                        	[#list cmsProductList as cmsProduct]

			                        			[#if cmsProduct.hasProperty("admissionIncluded") && cmsProduct.getProperty("admissionIncluded").getString() == "true"]
			                        				[#if productNodeId == "" || productNodeId == cmsProduct.getName() ]
						                        	    <tr>
													            <td align="left" colspan="2" style="font-size: 22px;color: #f66011;font-weight: bold;"> ${cmsProduct.getProperty("name").getString()}
													            </td>
													            <td></td>
													    </tr>
											            [#assign axPrdList = starfn.getAXProductsByCMSProductNode(cmsProduct)!]
											            [#assign axPrdDetailsMap = starfn.getAXProductsDetailByCMSProductNode(cmsProduct)!]
											            [#list axPrdList as axPrd]
											                  	[#assign itemName = ""]
											                  	[#assign ticketType = ""]
											                  	[#assign productPrice = ""]
											                  	[#assign productListingId = ""]
											                  	[#assign axPrdDetail = axPrdDetailsMap[axPrd.getProperty("relatedAXProductUUID").getString()!]]
											                  	
													            [#if axPrdDetail.hasProperty("productName")]
													            [#assign itemName = axPrdDetail.getProperty("productName").getString()!]
													            [/#if]
													            
													            [#if axPrdDetail.hasProperty("ticketType")]
													            [#assign ticketType = axPrdDetail.getProperty("ticketType").getString()!]
													            [/#if]
				
													            [#if axPrdDetail.hasProperty("productPrice")]
													            [#assign productPrice = axPrdDetail.getProperty("productPrice").getString()!]
													            [/#if]
													            
													            [#if axPrdDetail.hasProperty("productListingId")]
													            [#assign productListingId = axPrdDetail.getProperty("productListingId").getString()!]
													            [/#if]
													            
													        
							                        			<tr>
							                        				<td align="left" class="" width="50%"> <b>${itemName}</b></td>
							                        				<td width="20%"> <b>${ticketType} - ${productPrice}</b></td>
							                            			<td style="padding-top:10px;"><button product-id="${productListingId}" id="btn-minus-qty-${productListingId}" type="button" class="btn-minus-qty-class icon-minus btn-inactive" ng-class="{'btn-inactive':ticket.qty<1,'btn-active':ticket.qty>=1}" ng-click="minusQty()"></button></td>
							                            			<td style="padding-top:10px;"><input product-id="${productListingId}" id="input-qty-${productListingId}" type="text" placeholder="0" class="input-ticket-num-class ng-pristine ng-untouched ng-valid"></td>
							                            			<td style="padding-top:10px;"><button product-id="${productListingId}" id="btn-add-qty" type="button" class="btn-add-qty-class icon-plus btn-active" ng-class="{'btn-inactive':ticket.qty>=maxTickets,'btn-active':ticket.qty<maxTickets}" ng-click="addQty()"></button></td>
							                        			</tr>
							                        			
							                        	        <script>
							                        			                var item = {
																                    cmsProductId : '${cmsProduct.getName()}',
																                    cmsProductName : '${cmsProduct.getProperty("name").getString()}',
																                    listingId : '${productListingId}',
																                    productCode : '${axPrdDetail.getProperty("displayProductNumber").getString()}',
																                    name : '${itemName}',
																                    type : '${ticketType}',
																                    qty : 0,
																                    price : parseFloat(${productPrice}),
																                    lineId : ''
																                };
																                
																                itemsOnCurrentPage.push(item);
							                        			</script>
				
						                        		[/#list]
					                        		[/#if]
			                        		    [/#if]
			                        	[/#list]
			                        	
			                        [/#list]
	                        	[/#list]
	                        [/#if]

	                        
	                        
	                        <tr>
	                            <td colspan="4" style="padding-top: 10px; color: #666; text-align: center;"><span style="color:red;">*</span><span ng-bind="title.hint2" class="ng-binding">${staticText[lang]['noteLabel']!"Children from the age of 3 will need an express ticket."}</span></td>
	                        </tr>
	                        </tbody>
	                    </table>
	                    [#if fromShoppingCart == "true"]
	                    	</div>
	                    [/#if]
	                    <div class="group-inner-btn">
	                    	[#if fromShoppingCart == "true"]
	                    		<button type="button" class="btn btn-md btn-third ng-binding" onclick="javascript:window.location.href='${sitePath}/shoppingcart'">Cancel</button>
	                    	[/#if]
	                    	 <button id="btn-admission-add-to-cart" type="button" class="btn btn-primary btn-md ng-binding" ng-click="addToFunCart()" ng-bind="toFunCart">${staticText[lang]['proceedButtonLabel']!"Update FUN Cart"}</button>
	                    </div>
	                </div> [#-- class="box-wrapper admission-big-dialog" --]
	            </div>[#--  class="box-wrapper" style="float: right;" | class="box-wrapper center-center" --]
			</div>[#-- class="main-content" --]
		</div> [#--  id="wrapper" --]
        

</div> [#-- class="main" --]
[#include "/kiosk-slm/templates/macros/common/keyboard.ftl"]
[#include "/kiosk-slm/templates/macros/common/num-keypad.ftl"]

<script>
			refreshShoppingCartItemsValue();
 			function deleteNumber(){
 				
		    	var qty = $('#input-qty-' + currentProductListingId);
		        var qtyInString  = qty.val().toString();
	
	        	var textSize = qtyInString.length - 1;
	
		        if(textSize == 0) {
		            qty.val('');
		            $('#btn-minus-qty-'+ currentProductListingId).addClass('btn-inactive');
		        }else{
		        	qtyInString = qtyInString.substring(0, textSize);
		        	qty.val(qtyInString);
		        }
		    }
		    
		    function selectNumber(number){
		    
		    	var qtyInputId = $('#input-qty-' + currentProductListingId);
		    	var qty = parseInt($(qtyInputId).val().toString() + number);
			    $(qtyInputId).val(qty);
		
		        if(qty == 0) {
		            $('#btn-minus-qty-' + currentProductListingId).removeClass('btn-active');
		            $('#btn-minus-qty-' + currentProductListingId).addClass('btn-inactive');
		        }else{
		        	$('#btn-minus-qty-' + currentProductListingId).addClass('btn-active');
		            $('#btn-minus-qty-' + currentProductListingId).removeClass('btn-inactive');
		        }
		    }
		    function hideKeyboard(){
		    	$('#number-keypad').hide();
		    }
		    
		    function redirectToShoppingCartPage() {
        		window.location.href = "${sitePath}/shoppingcart~";
            }
            
                function refreshShoppingCartItemsValue() {

			        $.ajax({
			            url : '/.store-kiosk/store/get-cart',
			            data : '',
			            headers : {
			                'Store-Api-Channel' : '${channel}'
			            },
			            type : 'GET',
			            cache : false,
			            dataType : 'json',
			            contentType : 'application/json',
			            success : function(result) {
			
			                if (result.success) {
							
						        $.each(result.data.cmsProducts, function(index, product) {
						        	 
									$.each(product.items, function(index, item) {
									    item.cmsProductId = product.id;
                						item.cmsProductName = product.name;
										itemsInShoppingCart.push(item);
										var listingId = item.listingId;
										$('#input-qty-'+ listingId).val(item.qty);
										var minusBtnId = '#btn-minus-qty-' + listingId;
										$(minusBtnId).removeClass('btn-inactive');
		        						$(minusBtnId).addClass('btn-active');
						           });
						        });
			                }
			                else {
			                    redirectToErrorPage();
			                }
			            },
			            error : function() {
			                redirectToErrorPage();
			            }
			        });
			    }
		   $(document).ready(function() {
		   
			    	if($('#div-scrollbar-ticket').length){
			        	var ticketScrollbar = new IScroll('#div-scrollbar-ticket', { scrollbars: 'custom', interactiveScrollbars: true });
			        }
			        
					$('.input-ticket-num-class').click(function(e){
						var target = $(e.target);
						var productId = target.attr('product-id');
						currentProductListingId = productId;
				        var offset = $('#input-qty-'+productId).offset();
				
				        $("#number-keypad").css('top', offset.top + 53);
				        $("#number-keypad").css('left', offset.left - 290);
			        
			        	if($("#number-keypad").is(':visible')) {
				            $("#number-keypad").hide();
				        }
				        else {
				            $("#number-keypad").show();
				        }
			        });
			        
			      	$('.btn-add-qty-class').click(function(e){
			      		var target = $(e.target);
			      		var productId = target.attr('product-id');
			      		currentProductListingId = productId;
			      		var qtyId = '#input-qty-' + productId;
			      		var minusBtnId = '#btn-minus-qty-' + productId;
			        	var qtyInString= $(qtyId).val();
			        	if(qtyInString == ''){
			        		qtyInString = '0';
			        	}
			        	$(qtyId).val(parseInt(qtyInString) + 1);
		        		$(minusBtnId).removeClass('btn-inactive');
		        		$(minusBtnId).addClass('btn-active');
			        });
		
			        $('.btn-minus-qty-class').click(function(e){
			            var target = $(e.target);
			      		var productId = target.attr('product-id');
			      		currentProductListingId = productId;
			      		var qtyId = '#input-qty-' + productId;
			      		var minusBtnId = '#btn-minus-qty-' + productId;
				    	var qty = parseInt($(qtyId).val());
				    	if(qty > 0){
				    		qty = qty -1;
				    		$(qtyId).val(qty);
				    	}
				    	if(qty == 0 ){
				    		$(qtyId).val('');
				    		$(minusBtnId).removeClass('btn-active');
				        	$(minusBtnId).addClass('btn-inactive');
				    	}
			        });
			        
			        $('#btn-admission-add-to-cart').click(function(e){
			        		var action = 'add-to-cart';
			        		for(var i in itemsOnCurrentPage){
			        		
			        			for(var j in itemsInShoppingCart){
			        			
			        				if(itemsOnCurrentPage[i].listingId == itemsInShoppingCart[j].listingId){
			        				   itemsOnCurrentPage[i].lineId = itemsInShoppingCart[j].lineId;
			        				   if(itemsOnCurrentPage[i].lineId){
			        				   		action = 'update-cart';
			        				   }
			        				}
			        			}
			        			var qty = parseInt($('#input-qty-' + itemsOnCurrentPage[i].listingId).val());
			        			itemsOnCurrentPage[i].qty = qty;
			        			if(qty >0){
									itemsToAdd.push(itemsOnCurrentPage[i]);
								}
			        		}
					        if(itemsToAdd.length == 0) {
            					showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty2']}");
            					return false;
        					}else{
        							commitToShoppingCart(action);
        				  }
			        });
			        
			     function commitToShoppingCart(action){
			     			openLoadingPage();
			     			$.ajax({
					            url : '/.store-kiosk/store/' + action,
					            data : JSON.stringify({
					                sessionId : '',
					                items : itemsToAdd
					            }),
					            headers : {
					                'Store-Api-Channel' : '${channel}'
					            },
					            type : 'POST',
					            cache : false,
					            dataType : 'json',
					            contentType : 'application/json',
					            success : function(result) {
									closeLoadingPage();
					                if (result.success) {
					                    redirectToShoppingCartPage();
					                
					                }else {
					                    redirectToErrorPage();
					                }
					            },
					            error : function() {
					            	closeLoadingPage();
					                redirectToErrorPage();
					            }
					        });
			     }
		   });
   
</script>





