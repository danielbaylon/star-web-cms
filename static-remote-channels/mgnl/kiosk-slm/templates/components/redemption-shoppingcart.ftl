[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#if ctx.getParameter("pinCode")?has_content]
    [#assign pinCode = ctx.getParameter("pinCode")]
[#else]
    [#assign pinCode = '']
[/#if]
<style>

    .event-row {

        border-style: solid;
        border-width: 1px;
        border-color: #1ca9ea;
        background-color: #f7fdff;
    }
    
    .select-event {

        width: 200px;
        height:36px;
        border: 4px solid #cccccc;
        padding: 0px;
        margin: 0px;
        text-align: center;
        font-size: 18px;
        font-family: 'harabara', 'STHeiti', Arial, Helvetica, sans-serif;
    }
</style>
<script>

    var productsInShoppingCart = [];
    var itemToDelete;
    var hasAdmission = false;
    var isPartial = false;
    var isCombined = false;

	var selectedItemDecrementCounterElement = null;
	var selectedItemQuantityElement = null;
	var tempNumber = "";
	var defaultEventDatePicker = null;
	
	function decreaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) - 1;

        if(currentValue >= 0){
	        if(isCombined){
		        $('.input-qty-class').each(function(i, obj) {
					$(obj).val(currentValue);
				});
	        }else{
	        	$('#' + elementId).val(currentValue);
	        }
        }
           

        if(currentValue == 0){
        	$('.icon-minus').each(function(i, obj) {
				$(obj).attr('class', 'btn-num icon-minus btn-inactive');
			});
        }

            

    }

    function increaseCounter(elementId, decreaseButtonId, remainingQtyId) {

        var currentValue = parseInt($('#' + elementId).val()) + 1;
		var remainingValue = parseInt($('#' + remainingQtyId).val());
		
		if(currentValue > remainingValue){
			return false;
		}else{
        	$('#' + elementId).val(currentValue);
        	$('.icon-minus').each(function(i, obj) {
				$(obj).attr('class', 'btn-num icon-minus btn-active');
			});
	        if(isCombined){
		        $('.input-qty-class').each(function(i, obj) {
					$(obj).val(currentValue);
				});
	        }else{
	        	$('#' + elementId).val(currentValue);
	        }
        }
    }
    
    function displayKeyboard(itemDecrementCounterId, itemQuantityId) {

        tempNumber = "";

        selectedItemDecrementCounterElement = $("#" + itemDecrementCounterId);
        selectedItemQuantityElement = $("#" + itemQuantityId);

        var offset = selectedItemQuantityElement.offset();

        $("#number-keypad").css('top', offset.top + 53);
        $("#number-keypad").css('left', offset.left - 290);

        if($("#number-keypad").is(':visible')) {
            $("#number-keypad").hide();
        }
        else {
            $("#number-keypad").show();
        }
    }

    function hideKeyboard() {
        $("#number-keypad").hide();
    }
    
    function selectNumber(number) {

        tempNumber += number;

        var actualNumber = parseInt(tempNumber);
        
        if(isCombined){
			$('.input-qty-class').each(function(i, obj) {
				$(obj).val(actualNumber);
			});
	    }

        selectedItemQuantityElement.val(actualNumber);

        if(actualNumber > 0) {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-active');
        }
        else {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
        }
    }

    function deleteNumber() {

        var inputText = selectedItemQuantityElement.val().toString();

        var textSize = inputText.length - 1;

        if(textSize == 0) {
            selectedItemQuantityElement.val(0);
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
            tempNumber = '';
            return;
        }

        tempNumber = inputText.substring(0, inputText.length-1);

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);
    }
    
    
    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/redemption/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPrice = "S$0.00";

                    if(result.data.totalQty != 0) {
                        //totalPrice = result.data.totalText.replace(/\s/g, '');
                        
                    }

                    productsInShoppingCart = result.data.cmsProducts;
                    refreshShoppingCartItems();
                    refreshEventDates();
					$(".btn-datepicker").click(function(event) {
						var pickadate = $(event.target).parent().prev().prev().pickadate("picker");
						pickadate.open();
						event.stopPropagation();
			        });	
                }
                else {
                    //redirectToErrorPage();
                }
            },
            error : function() {
                //redirectToErrorPage();
            }
        });
    }

	function refreshEventDates(){
	        $.each(productsInShoppingCart, function(index, product) {
            		$.each(product.items, function(index, item) {
            		
            		        var $sessionSelect = $('#event-session-' + item.listingId);
							var eventDateWidget = $('#event-date-' + item.listingId);
							var preSelectDate = null;
							var $picka = null;
							
							eventDateWidget.pickadate({
								format: 'dd/mm/yyyy',
							    disable: [],
								container: '#event-date-container',
								closeOnSelect: true,
								closeOnClear: true
							});
																	                    
        					$sessionSelect.change(function() {
            		
            					var eventLineId = $sessionSelect.val();
										if(item.eventGroupId && (item.selectedEventDate == null || item.selectedEventDate == '')){
			               					var eventLineDates =[]; 
			                 					if(eventLineId){
			                 						eventLineDates = item.eventLineDates[eventLineId];
			                 					}
												var enableDates = [];
												enableDates.push(true);
																	
												$.each(eventLineDates, function(index, eventDate) {
												    var dtArr = eventDate.split('-');
												    enableDates.push([Number(dtArr[0]), Number(dtArr[1]) -1 , Number(dtArr[2])]);
												    if(index == 0){
														preSelectDate = eventDate;
													}
												});
																		
		                                		eventDateWidget.pickadate().pickadate('picker').clear();
		                                		eventDateWidget.pickadate().pickadate('picker').stop();
				                                setTimeout(function() {
				                                $picka = eventDateWidget.pickadate({
				                                        	format: 'dd/mm/yyyy', disable: enableDates
				                                    	});
				                                  								
													if(preSelectDate == null){
														var today = new Date((new Date()).valueOf());
														$picka.set('select', today);
													}else{
														$picka.pickadate('picker').set('select', 'preSelectDate', {format:'yyyy-mm-dd'});
													}
				                                }, 600);
																                    
										}	
								});
								
								$sessionSelect.change();

					});
		});
				
	
	}

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';
        $.each(productsInShoppingCart, function(index, product) {
            $.each(product.items, function(index, item) {
            	isCombined = item.isCombined;
            	if(item.isPartial){
            		$('#td-remaining-qty').show();
            	}
                rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-content">';
                if(item.allowChangeEventDate){  
                
                rowsHtml += '           <tr class="cart-list">';
                
                }else{
                
                rowsHtml += '           <tr class="cart-list">';
                
                }     
                            
                            
                rowsHtml += '               <td width="40%" class="title">' + item.name +
                            '                               <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>';
           
           if(!item.allowChangeEventDate && item.eventLineId){
			    rowsHtml += '                               <p class="tix-type"> ${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';						
		   }
           
           if(!item.allowChangeEventDate && item.eventLineId){
			    rowsHtml += '                               <p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : '  + item.eventLineId + '</p>';						
		   }	
		   		rowsHtml += '                </td>';       

		 if(item.isPartial){
		 
		 		rowsHtml += '                <td width="30%" class="center" id="td-remaining-qty-'+item.listingId+'" >' + item.remainingQty + '</td>';
		 		rowsHtml += '                <input type="hidden" id="input-remaining-qty-'+item.listingId+'" value="' + item.remainingQty + '"/>';
		        rowsHtml += '                <td width="40%" class="center">'
						                    	+ '<div id="red-pin-line' + item.listingId + '" >'
						                    		+ '<table class="table-content">'
						                    			+'<tr>'
						                    				+'<td>'
						                    					+'<button type="button" class="btn-num icon-minus btn-active" id="btn-min-'+item.listingId+'" onclick="decreaseCounter(\'input-qty-'+item.listingId + '\',\'btn-min-'+item.listingId +'\')"></button>'
						                    				+'</td>'
						                    				+'<td>'
				                                            	+'<span onclick="displayKeyboard(\'btn-min-'+item.listingId + '\',\'input-qty-'+item.listingId +'\')">'
				                                                	+'<input class="input-qty-class" type="text" value="'+item.qty+'" id="input-qty-'+item.listingId+'" style="width:40px;" disabled/>'
				                                                +'</span>'
				                                            +'</td>'
				                                                                                
						                    				+'<td>'
						                    					+'<button type="button" class="btn-num icon-plus btn-active" id="btn-add-'+item.listingId+'" onclick="increaseCounter(\'input-qty-'+item.listingId + '\',\'btn-min-'+item.listingId + '\',\'input-remaining-qty-'+item.listingId +'\')"></button>'
						                    				+'</td>'
						                    			+'</tr>'
						                    		+ '</table>'
						                    	+ '</div>'
						                 + '</td>';

		 }else{
		  		rowsHtml += '               <td width="40%" class="center">' + item.qty + '</td>' +'<input class="input-qty-class" type="hidden" value="'+item.qty+'" id="input-qty-'+item.listingId+'"/>';  
		 }
               
                

                rowsHtml += '           </tr>';
                
                if(item.allowChangeEventDate){
                
                rowsHtml += '           <tr id="" class="event-row" style="padding-bottom:10px;">' +
                                                    '<td width="40%" class="inner-container" style="padding-left:20px;">' +
                                                       ' <div style="margin : 5px;">Choose Date of Visit</div>' +
                                                       ' <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                                                       '     <tr>' +
                                                       '        <td>' +
                                                       '             <div>' +
                                                       '                 <input class="input-datepicker" id="event-date-'+item.listingId+ '" style="width: 200px;" type="text" placeholder="dd/mm/yyyy" readonly />' +
                                                       '                 <button class="btn-datepicker" type="button" id="event-date-button-" style="vertical-align: middle; background:transparent;border:none;margin-left:5px;width:35px;">' +
                                                       '                     <img src="${themePath}/images/icon-calendar.png" />' +
                                                       '                </button>' +
                                                       '                 <div id="datepicker-container" style="position:relative"></div>' +
                                                       '             </div>' +
                                                       '         </td>' +
                                                       '     </tr>' +
                                                       ' </table>' +
                                                    '</td>' +
                                                    '<td width="28%" class="inner-container">' +
                                                    '    <div style="margin : 5px;">Choose Session</div>' +
                                                    '    <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                                                    '        <tr>' +
                                                    '            <td>' +
                                                    '                <select id="event-session-' + item.listingId + '" class="select-event">';
                                                    if(item.eventLines){
                                                    	$.each(item.eventLines, function(index, line) {
                 rowsHtml += '                                       <option value="' + line + '">' + line + '</option>';
                               	
                                                    	});
                                                    
                                                    }
                 rowsHtml += '                                      </select>' +
                                                    '            </td>' +
                                                    '        </tr>' +
                                                    '    </table>' +
                                                    '</td>';
             	if(item.isPartial){
                 rowsHtml += '<td width="30"></td></tr>';
				}else{
                  rowsHtml +=                     '</tr>';
                }
                 rowsHtml += '<tr class="cart-list">';
                 
             	if(item.isPartial){
                 rowsHtml += '<td colspan="3"></td>';
				}else{
				 rowsHtml += '<td colspan="2"></td>';
				}
                 
                 
                 rowsHtml += '</tr>';
                
                }
                
                rowsHtml += '       </table>' +
                        	'   </td>' +
                        	'</tr>';
            });

        });
        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }

	/**
    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }**/

    function clearShoppingCart() {

    }

    function checkout() {
    		
    		var cartItems = [];
    		var valid = true;
    		var totalQty = 0 ;
    		$.each(productsInShoppingCart, function(index, product) {
	            $.each(product.items, function(index, item) {
	            	var cartItem = {};
	                cartItem.qty = $('#input-qty-'+item.listingId).val();
	                totalQty = totalQty + parseInt(cartItem.qty);
	                cartItem.listingId = item.listingId;
	                var eventLineId = $('#event-session-'+item.listingId);
	                if(eventLineId.length){
	                	cartItem.eventLineId = eventLineId.val();
	                }else{
	                	cartItem.eventLineId = item.eventLineId;
	                }
	                var eventDate = $('#event-date-'+item.listingId);
	                if(eventDate.length){
	                	cartItem.selectedEventDate = eventDate.val();
	                	if(cartItem.selectedEventDate == ''){
	                		showAlertDialog("${staticText[lang]['titleAlert']}","No Event Date Selected for " + item.name);
	                		valid = false;
	                	}
	                }else{
	                	cartItem.selectedEventDate = item.selectedEventDate;
	                }
	                cartItem.eventGroupId= item.eventGroupId;
	                
	                
	                cartItems.push(cartItem);
	            });
        	});
        
        	if(totalQty == 0 ){
        		showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty1']}");
        	}else if(valid){
        		openLoadingPage();
		        $.ajax({
		            url: '/.store-kiosk/store/redemption/checkout',
		            data:  JSON.stringify(cartItems),
		            headers: { 'Store-Api-Channel' : '${channel}' },
		            type: 'POST',
		            cache: false,
		            dataType: 'json',
		            contentType: 'application/json',
		            success: function (data) {
		            	closeLoadingPage();
		                if (data.success) {
		                    redirectToSummaryPage();
		                }else if(data.errorCode == 'InsufficientQty'){
		                	showAlertDialog('ERROR', data.message);
		                } else {
		                    redirectToErrorPage();
		                }
		            },
		            error: function () {
		            	closeLoadingPage();
		                redirectToErrorPage();
		            },
		            complete: function () {
		            }
		        });
        	}
	 		

    }

    function redirectToHomePage() {
    	  	$.ajax({
	            url: '/.store-kiosk/store/redemption/unlock-pin',
	            data:  '',
	            headers: { 'Store-Api-Channel' : '${channel}' },
	            type: 'POST',
	            cache: false,
	            dataType: 'json',
	            contentType: 'application/json',
	            success: function (data) {
					window.location.href='${sitePath}';
	            },
	            error: function () {
 					window.location.href='${sitePath}';
	            },
	            complete: function () {
	            }
	        });
       
    }

    function redirectToSummaryPage() {
        window.location.href='${sitePath}/redemption-summary';
    }

    function loadScroller() {

        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }
    
    function redirectToReceiptPage() {
        window.location.href = "${sitePath}/receipt?signatureRequired=false";
    }

    $(document).ready(function() {
        
        fetchShoppingCartItems();

        $("#okButton").click(function() {
            deleteItem(itemToDelete);
            hideInfoDialog();
        });
    });

</script>
<div class="main" id="event-date-container">
    <aside ng-include="templateSideBarUrl" class="ng-scope">

    </aside>

 <div class="main-content" style="margin-left:119px;">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['onlineStoreRedemptionSummaryLabel']}</h1>
        <div class="fun-cart-content">
            <p class="cart-note">${staticText[lang]['redemptionNote']}</p>
            <div class="cart-box ticket-container">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                    <tr>
                        <td width="40%">${staticText[lang]['shoppingCartTitleItem']}</td>
                       	<td width="25%" class="center" style="display:none" id="td-remaining-qty"> Remaining Qty </td>
                        <td width="40%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                    </tr>
                </table>
                <div class="tab-content-container for-template">
                    <div id="scrollbar2" style="height:440px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--
                <div class="total-bar">
                   
                    <div class="total">
                    ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                    </div>
                </div>
				-->
                <div class="cart-group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-secondary btn-md" onclick="redirectToHomePage()">${staticText[lang]['cancelButtonLabel']}</button>
                    <button type="button" class="btn btn-primary btn-md" onclick="checkout()">${staticText[lang]['okButtonLabel']}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="number-keypad" style="z-index:1001; display: none" class="keypad-dialog">
    <div class="arrow-up"></div>
    <h2>Please Key In Quantity</h2>
    <ul class="keypad-controller">
        <li><button type="button" onclick="selectNumber(1)">1</button></li>
        <li><button type="button" onclick="selectNumber(2)">2</button></li>
        <li><button type="button" onclick="selectNumber(3)">3</button></li>
        <li><button type="button" onclick="selectNumber(4)">4</button></li>
        <li><button type="button" onclick="selectNumber(5)">5</button></li>
        <li><button type="button" onclick="selectNumber(6)">6</button></li>
        <li><button type="button" onclick="selectNumber(7)">7</button></li>
        <li><button type="button" onclick="selectNumber(8)">8</button></li>
        <li><button type="button" onclick="selectNumber(9)">9</button></li>
        <li><button type="button" onclick="selectNumber(0)">0</button></li>
        <div class="clearfix"></div>
    </ul>
    <div class="group-inner-btn keypad-group">
        <button type="button" class="btn btn-third btn-xs ng-binding" onclick="deleteNumber()">Delete</button>
        <button type="button" class="btn btn-primary btn-xs ng-binding" onclick="hideKeyboard()">OK</button>
    </div>
</div>

