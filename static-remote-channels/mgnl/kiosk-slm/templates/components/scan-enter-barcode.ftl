[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

<script>

    function redirectToScanBarcodePage() {
        window.location.href = "${sitePath}/scan-barcode";
    }

    function redirectToEnterBarcodePage() {
        window.location.href = "${sitePath}/enter-barcode";
    }

</script>

<div>
    <div></div>
    <div class="main-wrapper">
        <h1 class="primary-heading py-hg-lg" ng-bind-html="centerText.t">${staticText[lang]['barcodeHeadingTitle']}</h1>
        <ul class="box-wrapper center-center se-barcode">
            <li>
                <button type="button" class="btn btn-sm btn-primary" onclick="redirectToScanBarcodePage()">${staticText[lang]['scanBarcodeButtonLabel']}</button>
                <img src="${themePath}/images/barcode/ticket-scan.jpg" />
            </li>
            <li>
                <button type="button" class="btn btn-sm btn-primary" onclick="redirectToEnterBarcodePage()">${staticText[lang]['enterBarcodeButtonLabel']}</button>
                <img src="${themePath}/images/search.jpg" />
            </li>
            <div class="clearfix"></div>
        </ul>
    </div>
</div>