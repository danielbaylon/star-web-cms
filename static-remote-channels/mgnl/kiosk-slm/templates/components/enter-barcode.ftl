[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

<script>

    var receiptNumber;

    function redirectToScanEnterBarcodePage() {
        window.location.href = "${sitePath}/scan-enter-barcode";
    }

    function showKeyboard() {
        $("#keyboardContainer").show();
    }

    function hideKeyboard() {
        $("#keyboardContainer").hide();
    }

    function inputCharacter(character) {

        var existingText = $("#barcode-text").val();
        var updatedText = existingText.concat(character);

        $("#barcode-text").val(updatedText);
    }

    function deletePreviousCharacter() {

        var existingText = $("#barcode-text").val();

        if(existingText.length > 0) {
            var updatedText = existingText.slice(0, -1);
            $("#barcode-text").val(updatedText);
        }
    }

    function clearAllCharacters() {
        $("#barcode-text").val('');
    }

    function showErrorDialog() {
        $("#error-dialog").show();
    }


    function closeErrorDialog() {
        $("#error-dialog").hide();
    }

    function showBarcodeButton(){
        var existingText = $("#barcode-text").val();
        if(existingText.length > 0) {
             $('#keyboardContainer').hide();
               if(existingText.length == 8){
                     getStartPINRedemption();
                } else if(existingText.length == 18){
                    startRecoveryFlow();
                }else{
                 	$("#error-dialog").show();
                }
        } else {
            $("#error-dialog").show();
        }
  
    }

    function getStartPINRedemption(){
        openLoadingPage();
        $.ajax({
            url : '/.store-kiosk/store/redemption/start',
            data : JSON.stringify({
                pinCode : $("#barcode-text").val()
            }),
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                closeLoadingPage();
                if (result.success) {
                    redirectToShoppingcartPage();
                }else{
                    showAlertDialog('ERROR', result.message);
                }       
            },
            error : function(result) {
                closeLoadingPage();
                redirectToErrorPage();
            }
        });
    }

    function startRecoveryFlow(){
        openLoadingPage();
        $.ajax({
            url : '/.store-kiosk/store/recovery/start',
            data : $("#barcode-text").val(),
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                closeLoadingPage();
                if (result.success) {
                    redirectToRecoverySummary($("#barcode-text").val());
                }else{
                    showAlertDialog('ERROR', result.message);
                }       
            },
            error : function(result) {
                closeLoadingPage();
                redirectToErrorPage();
            }
        });
    }

    function redirectToShoppingcartPage() {
        window.location.href = "${sitePath}/redemption-shoppingcart?receiptNumber=";
    }

    function redirectToRecoverySummary(receiptNumber) {
        window.location.href = "${sitePath}/recoveryflow-ordersummary?receiptNumber="+receiptNumber;
    }
    
    function switchCaps(){
        if($('.keyboard-uppercase').is(":visible")){
            $('.keyboard-uppercase').hide();
            $('.keyboard-lowercase').show();
        }else{
            $('.keyboard-uppercase').show();
            $('.keyboard-lowercase').hide();
        }
    
    }

    function clearShoppingCart() {
		openLoadingPage();
        $.ajax({
            url : '${ctx.contextPath}/.store-kiosk/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
				closeLoadingPage();
                if (result.success) {

                    refreshShoppingCart(0);
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }
    
    $(document).ready(function() {
        $('.keyboard-uppercase').show();
        $('.keyboard-lowercase').hide();
        $("#barcode-text").click(function(e) {
            showKeyboard();
        });
		clearShoppingCart();
    });
</script>

<div>
    <div></div>
    <div class="main-wrapper">
        <h1 class="primary-heading py-hg-lg">
            <span>${staticText[lang]['enterBarcodeHeading1']}</span>
            <br />
            <span class="secondary-heading sy-hg-md">${staticText[lang]['enterBarcodeHeading2']}</span>
        </h1>
        <div class="box-wrapper center-center">
            <input id="barcode-text" type="text" placeholder="E.g.1NHDgnDa" class="barcode-field">
        </div>
        <ul class="box-wrapper center-center inner-wrapper">
            <li>
                <img src="${themePath}/images/barcode/num-img-1.png"/>
            </li>
            <li>
                <img src="${themePath}/images/barcode/num-img-2.png"/>
            </li>
            <div class="clearfix"></div>
        </ul>
        <div class="group-btn box-wrapper center-center">
            <button type="button" class="btn btn-md btn-third ng-binding" onclick="redirectToScanEnterBarcodePage()">${staticText[lang]['backButtonLabel']}</button>
      </div>
    </div>
    <div ng-include="'templates/keyboard.html'" ng-show="showKeyboard==true"></div>
    <div ng-include="'templates/pop-up-alert.html'" ng-show="showAlert==true"></div>
    <div ng-include="'templates/pop-up-loading.html'" ng-show="showLoading==true"></div>
</div>

<div id="keyboardContainer" class="keyboard-container" style="display: none;">
    <div class="keyboard-content">
        <div class="btn-close">
            <button type="button" onclick="hideKeyboard()"></button>
        </div>
        <div class="character-side">
            <ul class="keyboard-controller box-wrapper center-center keyboard-uppercase">
                <li><button type="button" onclick="inputCharacter('Q')">Q</button></li>
                <li><button type="button" onclick="inputCharacter('W')">W</button></li>
                <li><button type="button" onclick="inputCharacter('E')">E</button></li>
                <li><button type="button" onclick="inputCharacter('R')">R</button></li>
                <li><button type="button" onclick="inputCharacter('T')">T</button></li>
                <li><button type="button" onclick="inputCharacter('Y')">Y</button></li>
                <li><button type="button" onclick="inputCharacter('U')">U</button></li>
                <li><button type="button" onclick="inputCharacter('I')">I</button></li>
                <li><button type="button" onclick="inputCharacter('O')">O</button></li>
                <li><button type="button" onclick="inputCharacter('P')">P</button></li>
                <li><button type="button" onclick="inputCharacter('@')">@</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center keyboard-uppercase">
                <li><button type="button" onclick="inputCharacter('A')">A</button></li>
                <li><button type="button" onclick="inputCharacter('S')">S</button></li>
                <li><button type="button" onclick="inputCharacter('D')">D</button></li>
                <li><button type="button" onclick="inputCharacter('F')">F</button></li>
                <li><button type="button" onclick="inputCharacter('G')">G</button></li>
                <li><button type="button" onclick="inputCharacter('H')">H</button></li>
                <li><button type="button" onclick="inputCharacter('J')">J</button></li>
                <li><button type="button" onclick="inputCharacter('K')">K</button></li>
                <li><button type="button" onclick="inputCharacter('L')">L</button></li>
                <li><button type="button" onclick="inputCharacter('#')">#</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center keyboard-uppercase">
                <li><button type="button" onclick="inputCharacter('Z')">Z</button></li>
                <li><button type="button" onclick="inputCharacter('X')">X</button></li>
                <li><button type="button" onclick="inputCharacter('C')">C</button></li>
                <li><button type="button" onclick="inputCharacter('V')">V</button></li>
                <li><button type="button" onclick="inputCharacter('B')">B</button></li>
                <li><button type="button" onclick="inputCharacter('N')">N</button></li>
                <li><button type="button" onclick="inputCharacter('M')">M</button></li>
                <li><button type="button" onclick="inputCharacter(',')">,</button></li>
                <li><button type="button" onclick="inputCharacter('.')">.</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center keyboard-lowercase">
                <li><button type="button" onclick="inputCharacter('q')">q</button></li>
                <li><button type="button" onclick="inputCharacter('w')">w</button></li>
                <li><button type="button" onclick="inputCharacter('e')">e</button></li>
                <li><button type="button" onclick="inputCharacter('r')">r</button></li>
                <li><button type="button" onclick="inputCharacter('t')">t</button></li>
                <li><button type="button" onclick="inputCharacter('y')">y</button></li>
                <li><button type="button" onclick="inputCharacter('u')">u</button></li>
                <li><button type="button" onclick="inputCharacter('i')">i</button></li>
                <li><button type="button" onclick="inputCharacter('o')">o</button></li>
                <li><button type="button" onclick="inputCharacter('p')">p</button></li>
                <li><button type="button" onclick="inputCharacter('W')">@</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center keyboard-lowercase">
                <li><button type="button" onclick="inputCharacter('a')">a</button></li>
                <li><button type="button" onclick="inputCharacter('s')">s</button></li>
                <li><button type="button" onclick="inputCharacter('d')">d</button></li>
                <li><button type="button" onclick="inputCharacter('f')">f</button></li>
                <li><button type="button" onclick="inputCharacter('g')">g</button></li>
                <li><button type="button" onclick="inputCharacter('h')">h</button></li>
                <li><button type="button" onclick="inputCharacter('j')">j</button></li>
                <li><button type="button" onclick="inputCharacter('k')">k</button></li>
                <li><button type="button" onclick="inputCharacter('l')">l</button></li>
                <li><button type="button" onclick="inputCharacter('#')">#</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center keyboard-lowercase">
                <li><button type="button" onclick="inputCharacter('z')">z</button></li>
                <li><button type="button" onclick="inputCharacter('x')">x</button></li>
                <li><button type="button" onclick="inputCharacter('c')">c</button></li>
                <li><button type="button" onclick="inputCharacter('v')">v</button></li>
                <li><button type="button" onclick="inputCharacter('b')">b</button></li>
                <li><button type="button" onclick="inputCharacter('n')">n</button></li>
                <li><button type="button" onclick="inputCharacter('m')">m</button></li>
                <li><button type="button" onclick="inputCharacter(',')">,</button></li>
                <li><button type="button" onclick="inputCharacter('.')">.</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center">
                <li><button type="button" onclick="switchCaps()">&#8682;</button></li>
                <li><button type="button" onclick="inputCharacter('$')">$</button></li>
                <li><button type="button" onclick="inputCharacter('!')">!</button></li>
                <li><button type="button" onclick="inputCharacter('&')">&amp;</button></li>
                <li><button type="button" onclick="inputCharacter('*')">*</button></li>
                <li><button type="button" onclick="inputCharacter('(')">(</button></li>
                <li><button type="button" onclick="inputCharacter(')')">)</button></li>
                <li><button type="button" onclick="inputCharacter('-')">-</button></li>
                <li><button type="button" onclick="inputCharacter('+')">+</button></li>
                <li><button type="button" onclick="inputCharacter('\'')">'</button></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="num-side">
            <ul class="keyboard-controller">
                <li><button type="button" onclick="inputCharacter('1')">1</button></li>
                <li><button type="button" onclick="inputCharacter('2')">2</button></li>
                <li><button type="button" onclick="inputCharacter('3')">3</button></li>
                <li><button type="button" onclick="inputCharacter('4')">4</button></li>
                <li><button type="button" onclick="inputCharacter('5')">5</button></li>
                <li><button type="button" onclick="inputCharacter('6')">6</button></li>
                <li><button type="button" onclick="inputCharacter('7')">7</button></li>
                <li><button type="button" onclick="inputCharacter('8')">8</button></li>
                <li><button type="button" onclick="inputCharacter('9')">9</button></li>
                <li><button type="button" class="btn-zero" onclick="inputCharacter('0')">0</button></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="clearfix"></div>
        <ul class="keyboard-controller-1 box-wrapper center-center">
            <li class="btn-space"><button type="button" class="clear" onclick="clearAllCharacters()">Clear</button></li>
            <li class="btn-space"><button type="button" class="space" onclick="inputCharacter(' ')">Space</button></li>
            <li class="btn-space"><button type="button" class="backspace" onclick="deletePreviousCharacter()">Backspace</button></li>
            <li class="btn-space"><button type="button" class="ok" onclick="showBarcodeButton()">OK</button></li>
        </ul>
    </div>
</div>

<div id="error-dialog" class="dialog-container box-wrapper center-center" style="display: none;">
    <div class="alert-container">
        <div style="z-index:-1" class="dialog-container"></div>
        <h2 class="alert-title ng-binding">${staticText[lang]['invalidTransactionIdDialogTitle']}</h2>
        <p class="ng-binding">${staticText[lang]['invalidTransactionIdMessage']}</p>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-primary btn-md ng-binding" onclick="closeErrorDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>