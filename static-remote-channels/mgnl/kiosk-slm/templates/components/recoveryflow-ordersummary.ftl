[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#if ctx.getParameter("receiptNumber")?has_content]
    [#assign receiptNumber = ctx.getParameter("receiptNumber")]
[#else]
    [#assign receiptNumber = '']
[/#if]

<script>
    
    var kioskReceiptNumber;
    var productsInShoppingCart = [];

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/recovery/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    var itemsInShoppingCart = [];
                    var totalPrice = result.data.totalText.replace(/\s/g, '');
                    productsInShoppingCart = result.data.cmsProducts;

                    refreshShoppingCartItems();
                    resetTotalPrice(totalPrice);
                    loadScroller();

                     $.ajax({
                        url : '/.store-kiosk/store/recovery/get-transaction',
                        data :  JSON.stringify('${receiptNumber}'),
                        headers : {
                            'Store-Api-Channel' : '${channel}'
                        },
                        type : 'POST',
                        cache : false,
                        dataType : 'json',
                        contentType : 'application/json',
                        success : function(result) {
                            kioskReceiptNumber = result.data.receiptNumber;

                        },
                        error : function() {}
                    });

                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {
        
            $.each(product.items, function(index, item) {

                if(item.topup) {

                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="adds-on">' +
                            '                   <table cellpadding="0" cellspacing="0" border="0">' +
                            '                       <tr>' +
                            '                           <td><img src="${themePath}/images/icon-addson.png"></td>' +
                            '                           <td class="title">' + item.name +
                            '                               <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>';
               if(item.selectedEventDate){
               
                    rowsHtml +=                           
                            '                               <p class="tix-type"> ${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';                       
                                         }
               if(item.eventLineId){
               
                    rowsHtml += 
                            '                               <p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : '  + item.eventLineId + '</p>';                      
                                   }                     
                    rowsHtml += 
                            '                               <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '                           </td>' +
                            '                       </tr>' +
                            '                   </table>' +
                            '               </td>';
                }
                else {
                    rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="46%" class="title">' + item.name +
                            '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>' ;
               if(item.selectedEventDate){
               
                    rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';                            
                                         }
               if(item.eventLineId){
               
                    rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : ' + item.eventLineId + '</p>';                       
                                   }    
                    rowsHtml += 
                            '                   <p class="tix-type" style="color: red">' + item.discountLabel + '</p>' +
                            '               </td>';
                }

                rowsHtml += ' <td width="15%" class="center">S$' + item.price+ '</td>' +
                        ' <td width="11%" class="center">' + item.qty + '</td>';

                if (item.discountTotal != undefined && item.discountTotal > 0) {
                    rowsHtml += ' <td width="18%" class="center">' +
                            '   <span style="color: red; text-decoration: line-through;">S$' + (parseFloat(item.total) + parseFloat(item.discountTotal)).toFixed(2) + '</span><br />' +
                            '   S$' + parseFloat(item.total) +
                            ' </td>';
                }
                else {
                    rowsHtml += ' <td width="18%" class="center">S$' + parseFloat(item.total) + '</td>';
                }

                rowsHtml += '       </tr>' +
                        '       </table>' +
                        '   </td>' +
                        '</tr>';
            });


        });

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }

    function resetTotalPrice(totalPrice) {
        $('#totalItemsPrice').text(totalPrice);
    }

    function loadScroller() {
        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    function redirectToHomePage() {
        window.location.href='${sitePath}';
    }

    function printTickets() {

        $.ajax({
            url : '/.store-kiosk/kiosk-slm/printTickets',
            data : JSON.stringify('${receiptNumber}'),
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    showCompletedScreen();
                }
                else {
                    showErrorScreen(result.message);
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function showCompletedScreen() {
        $("#summaryContent").hide();
        $("#printSuccessfulContent").show();
    }

    function showErrorScreen(errorMessage) {
        $("#summaryContent").hide();
        $("#printErrorMessage").html(errorMessage);
        $("#printErrorContent").show();
    }

    $(document).ready(fetchShoppingCartItems());

    function redirectToReceiptPage() {
        window.location.href = "${sitePath}/recoveryflow-receipt?receiptNumber=" + kioskReceiptNumber;
    }

    function checkout() {
            
            var cartItems = [];
            var valid = true;
            var totalQty = 0 ;
            $.each(productsInShoppingCart, function(index, product) {
                $.each(product.items, function(index, item) {
                    var cartItem = {};
                    cartItem.qty = $('#input-qty-'+item.listingId).val();
                    totalQty = totalQty + parseInt(cartItem.qty);
                    cartItem.listingId = item.listingId;
                    var eventLineId = $('#event-session-'+item.listingId);
                    if(eventLineId.length){
                        cartItem.eventLineId = eventLineId.val();
                    }else{
                        cartItem.eventLineId = item.eventLineId;
                    }
                    var eventDate = $('#event-date-'+item.listingId);
                    if(eventDate.length){
                        cartItem.selectedEventDate = eventDate.val();
                        if(cartItem.selectedEventDate == ''){
                            showAlertDialog("${staticText[lang]['titleAlert']}","No Event Date Selected for " + item.name);
                            valid = false;
                        }
                    }else{
                        cartItem.selectedEventDate = item.selectedEventDate;
                    }
                    cartItem.eventGroupId= item.eventGroupId;
                    
                    
                    cartItems.push(cartItem);
                });
            });
        
            if(totalQty == 0 ){
                showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty1']}");
            }else if(valid){
                openLoadingPage();
                $.ajax({
                    url: '/.store-kiosk/store/recovery/checkout',
                    data:  JSON.stringify(cartItems),
                    headers: { 'Store-Api-Channel' : '${channel}' },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        closeLoadingPage();
                        if (data.success) {
                            redirectToReceiptPage(data.data.receiptNumber);
                        } else {
                            redirectToErrorPage();
                        }
                    },
                    error: function () {
                        closeLoadingPage();
                        redirectToErrorPage();
                    },
                    complete: function () {
                    }
                });
            }
            

    }


</script>

<div class="main box-wrapper top-center">
    <div class="main-content">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['orderSummaryTitle']}</h1>

        <div class="fun-cart-content">
            <div class="cart-box ticket-container summary-dialog" ng-class="{'summary-dialog-1':localPreReceiptType=='Recovery' || localPreReceiptType=='B2C','summary-dialog':localPreReceiptType!='Recovery' &amp;&amp; localPreReceiptType!='B2C'}">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="97%" class="table-title">
                    <tr>
                        <td width="46%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="15%" class="center">${staticText[lang]['shoppingCartTitlePrice']}</td>
                        <td width="18%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                        <td width="18%" class="center">${staticText[lang]['shoppingCartTitleSubtotal']}</td>
                    </tr>
                </table>
                <div class="tab-content-container fif-template">
                    <div id="scrollbar2" style="height:490px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="total-bar">
                        <div class="btn-container">
                           
                        </div>
                        <div class="total">
                        ${staticText[lang]['totalLabel']}:<span id="totalItemsPrice" class="product-title"></span>
                        </div>
                    </div>
                    <div class="cart-group-btn box-wrapper center-center">
                        <button type="button" class="btn btn-third btn-md" onclick="redirectToHomePage()">${staticText[lang]['cancelButtonLabel']}</button>
                        <button type="button" class="btn btn-primary btn-md" onclick="redirectToReceiptPage()">${staticText[lang]['printTicketButtonLabel']}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>