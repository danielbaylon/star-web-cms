[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

[#if productNodeName?has_content]
    [#assign productNode = starfn.getCMSProduct(channel, productNodeName)]
    [#assign axProducts = starfn.getAXProductsByCMSProductNode(productNode)!]
    [#assign productNodeDetails = starfn.getAXProductsDetailByCMSProductNode(productNode)!]
    [#assign announcementList = kioskfn.getAnnouncementsByProduct(channel,productNode)!]
[/#if]

<script>
    var productsInShoppingCart = [];
    var productEvents = [];
    var itemsInShoppingCart = [];
    var selectedItemQuantityElement;
    var selectedItemDecrementCounterElement;
    var tempNumber = '';
    var hasTopUpItems = [];
	var defaultEventDatePicker = null;
	var selectedEventSession = null;
	
    function decreaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) - 1;

        if(currentValue >= 0)
            $('#' + elementId).val(currentValue);

        if(currentValue == 0)
            $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-inactive');
    }

    function increaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) + 1;

        $('#' + elementId).val(currentValue);
        $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-active');
    }

    function popupTNC() {

        $("#tncDialog").show();
        $('#scrollbar6-scrollbar').remove();
        var myScroll6;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
        	if($('#scrollbar6').length){
            	myScroll6 = new IScroll('#scrollbar6', { scrollbars: 'custom', interactiveScrollbars: true });
            }
        }, 1);
    }

    function popupPromotionTNC(content) {

        $("#tncContent").html(content);

        popupTNC();
    }

    function closeOnTNC() {
        $("#tncDialog").hide();
    }

    function openViewMoreDialog() {

        $("#viewMoreDialog").show();
        $('#scrollbar7-scrollbar').remove();
        var myScroll7;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
        	if($('#scrollbar7').length){
            	myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
            }
        }, 1);
    }

    function closeViewMoreDialog() {
        $("#viewMoreDialog").hide();
    }

    function refreshShoppingCartItemsValue() {

        $.ajax({
            url : '/.store-kiosk/store/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    productsInShoppingCart = result.data.cmsProducts;

                    if(result.data.totalQty != 0) {
                    	displayItemsForSelectedProduct();
                    }
                    var showUpdateButton = false;
				    [#if axProducts?has_content]
				        [#list axProducts as axProduct]
				        	[#assign axProductDetail = productNodeDetails[axProduct.getProperty("relatedAXProductUUID").getString()]]
				        	
					        $.each(productsInShoppingCart, function(index, product) {

					
					                $.each(product.items, function(index, item) {
					
									        	if(item.qty > 0 && item.listingId == "${axProductDetail.getProperty('productListingId').getString()}" ){
									        		showUpdateButton = true;
									        	}
					                });
					            
					        });

				        [/#list]
				    [/#if]
                    if(showUpdateButton){
                     	resetButtonToUpdate();
                    }else{
                        resetButtonToAdd();
                    }
                    refreshScroller() ;
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function resetButtonToAdd() {
        $("#addToFunCartButton").text("${staticText[lang]['addToFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","addToShoppingCart()");
    }

    function resetButtonToUpdate() {
        $("#addToFunCartButton").text("${staticText[lang]['updateFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","updateShoppingCart()");
    }

    function displayItemsForSelectedProduct() {

        itemsInShoppingCart = [];

        $.each(productsInShoppingCart, function(index, product) {

            if (product.id === '${productNodeName}') {

                $.each(product.items, function(index, item) {

                    if(!item.topup) {
                        itemsInShoppingCart.push(item);
                    }
                });
            }
        });

        $.each(itemsInShoppingCart, function(index, item) {
			
			if(item.eventGroupId && item.eventGroupId != ''){

					$("#event-counter-" + item.listingId).val(item.qty);
	            	$("#event-line-id-" + item.listingId).val(item.lineId);
	            	$("#event-minusButton-" + item.listingId).attr('class', 'btn-num icon-minus btn-active');
	            	
	            	$('#event-session-' + item.listingId).val(item.eventLineId);
	            	
	            	var eventDateWidget = $('#event-date-' + item.listingId);
	            	$picka = eventDateWidget.pickadate({
						format: 'dd/mm/yyyy',
					    disable: [],
						container: '#event-date-container',
						closeOnSelect: true,
						closeOnClear: true
					});
					var dtArr = item.selectedEventDate.split('/');
                    var selectedEventDate = [Number(dtArr[2]), Number(dtArr[1]) -1 , Number(dtArr[0])];
					$picka.pickadate('picker').set('select', selectedEventDate);
            	
			}else{
			
	            $("#counter-" + item.listingId).val(item.qty);
	            $("#line-id-" + item.listingId).val(item.lineId);
	            $("#minusButton-" + item.listingId).attr('class', 'btn-num icon-minus btn-active');
            }
        });
    }

    function validateShoppingCart(isUpdate) {
		var totalQty = 0 ;
		
		itemsInShoppingCart = [];


    [#if axProducts?has_content]
        [#list axProducts as axProduct]
            [#assign axProductDetail = productNodeDetails[axProduct.getProperty("relatedAXProductUUID").getString()]]
		
            var quantity = 0;

            var selectedProductEvent;

            $.each(productEvents, function(index, productEvent) {

                if(productEvent.productId == "${axProductDetail.getProperty('productListingId').getString()}") {
                    selectedProductEvent = productEvent;
                }
            });

            if(selectedProductEvent && selectedProductEvent.event) {
                quantity = parseInt($("#event-counter-${axProductDetail.getProperty('productListingId').getString()}").val());
            }
            else {
                quantity = parseInt($("#counter-${axProductDetail.getProperty('productListingId').getString()}").val());
            }

            if(quantity > 0) {
				var topup = ("true" == $("#topup-${axProductDetail.getProperty('productListingId').getString()}").val());

                var mainItem = {
                    cmsProductId : '${productNodeName}',
                    cmsProductName : '${productNode.getProperty("name").getString()?js_string}',
                    listingId : '${axProductDetail.getProperty("productListingId").getString()}',
                    productCode : '${axProductDetail.getProperty("displayProductNumber").getString()}',
                    name : '${axProductDetail.getProperty("itemName").getString()}',
                    type : '${axProductDetail.hasProperty("ticketType")?string(axProductDetail.getProperty("ticketType").getString(),"")}',
                    qty : quantity,
                    price : parseFloat(${axProductDetail.getProperty("productPrice").getString()}),
                    lineId : ''
                };

                if(selectedProductEvent && selectedProductEvent.event) {
					mainItem.lineId = $('#event-line-id-' + mainItem.listingId).val();
                    mainItem.eventGroupId = selectedProductEvent.eventGroupId;
                    mainItem.eventLineId = $('#event-session-' + selectedProductEvent.productId).val();
                    
                    $.each(selectedProductEvent.eventLines, function(index, eventLine) {
                        if (mainItem.eventLineId == eventLine.eventLineId) {
                            mainItem.eventSessionName = eventLine.eventName;
                            return false;
                        }
                    });

                    var eventDate = $('#event-date-' + selectedProductEvent.productId).val();

                    if(eventDate == "") {
                        showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyEventDate']}");
                        return false;
                    }
                    mainItem.selectedEventDate = eventDate;
                }else{
                	mainItem.lineId = $('#line-id-' + mainItem.listingId).val();
                }
                
                $.each(productEvents, function(index, ext) {
                	if(mainItem.productCode == ext.itemId){
                		mainItem.capacity = ext.capacity;
						mainItem.openDate = ext.openDate;
                	}
                });

                itemsInShoppingCart.push(mainItem);
            }
            
            
            totalQty = totalQty + quantity;
        [/#list]
    [/#if]

    
		if(totalQty == 0){
			showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty2']}");
           	return false;
		}
        if(itemsInShoppingCart.length == 0) {
            showAlertDialog("${staticText[lang]['titleAlert']}","${staticText[lang]['alertEmptyQty2']}");
            return false;
        }

        $.each(productsInShoppingCart, function(index, product) {

            $.each(product.items, function(index, item) {
            	var existing = false;
				if(isUpdate){
					 $.each(itemsInShoppingCart, function(index, selectedItem) {
					 	if(selectedItem.eventGroupId){
					 		if(selectedItem.listingId == item.listingId && selectedItem.eventGroupId == item.eventGroupId){
					 			existing = true;
					 		}
					 	}else{
					 		if(selectedItem.listingId == item.listingId){
					 			existing = true;
					 		}					 	
					 	}

					 });
                }
                if(!existing && isUpdate && !item.topup){
                    item.cmsProductId = product.id;
	                item.cmsProductName = product.name;
	                item.isTopup = item.topup;
	                itemsInShoppingCart.push(item);
                }
            });
        });
        
        return true;
    };

	function freshAddUpdateButton(productId, eventLineId){
		var existingEventLine = false;
	
	    $.each(productsInShoppingCart, function(index, product) {
        	$.each(product.items, function(index, item) {
					if(item.listingId == productId && item.eventLineId == eventLineId){
						existingEventLine = true;
					}
        	});
        });
        
		if(existingEventLine){
			resetButtonToUpdate();
		}else{
			resetButtonToAdd();
		}
	}
	
	
    function commitToShoppingCart() {

        $.ajax({
            url : '/.store-kiosk/store/add-to-cart',
            data : JSON.stringify({
                sessionId : '',
                items : itemsInShoppingCart
            }),
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
				closeLoadingPage();
                if (result.success) {
                	var showTopupPage = false;
                    if(hasTopUpItems.length > 0){
                    	$.each(itemsInShoppingCart, function(index, item) {
                    		if(hasTopUpItems.indexOf(parseInt(item.listingId)) != -1){
                    			showTopupPage = true;
                    		}
				        });
				        
				     }
				     
				    if(showTopupPage){
                        redirectToTopupPage();
                    }
                    else {
                        redirectToShoppingCartPage();
                    }
                }
                else {
                	    if(result.errorCode == 'InsufficientQty'){
                			showAlertDialog('ERROR', result.message);
		                }else{
		                    redirectToErrorPage();
		                }
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function addToShoppingCart() {
		openLoadingPage();
        if(!validateShoppingCart(false)) {
        	closeLoadingPage();
            return;
        };

        commitToShoppingCart();
    }

    function redirectToTopupPage() {
        window.location.href = "${sitePath}/category/product/order/topuporder~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function updateShoppingCart() {
        openLoadingPage();
        if(!validateShoppingCart(true)) {
        	closeLoadingPage();
            return;
        };
        

		$.ajax({
            url : '/.store-kiosk/store/update-cart',
            data : JSON.stringify({
                sessionId : '',
                items : itemsInShoppingCart
            }),
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
				closeLoadingPage();
                if (result.success) {
                	var showTopupPage = false;
                    if(hasTopUpItems.length > 0){
                    	$.each(itemsInShoppingCart, function(index, item) {
                    		if(hasTopUpItems.indexOf(parseInt(item.listingId)) != -1){
                    			showTopupPage = true;
                    		}
				        });
				        
				     }
				     
				    if(showTopupPage){
                        redirectToTopupPage();
                    }else {
                        redirectToShoppingCartPage();
                    }
                }
                else {
                        if(result.errorCode == 'InsufficientQty'){
                			showAlertDialog('ERROR', result.message);
		                }else{
		                    redirectToErrorPage();
		                }
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
		/**
        $.ajax({
            url : '/.store-kiosk/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    commitToShoppingCart();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
        **/
    };

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function redirectToProductDetailPage() {
        window.location.href="${sitePath}/category/product~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function loaded() {
    	if($('#scrollbar1').length){
        	myScroll1 = new IScroll('#scrollbar1', { scrollbars: 'custom', interactiveScrollbars: true });
        }
        if($('#scrollbar2').length){
        	myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
        }
        if($('#scrollbar7').length){
        	myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
        }
    }

    function refreshScroller() {

        setTimeout(function() {
            myScroll2.refresh();
        }, 500);
    }

    function displayKeyboard(itemDecrementCounterId, itemQuantityId) {

        tempNumber = '';

        selectedItemDecrementCounterElement = $("#" + itemDecrementCounterId);
        selectedItemQuantityElement = $("#" + itemQuantityId);

        var offset = selectedItemQuantityElement.offset();

        $("#number-keypad").css('top', offset.top + 53);
        $("#number-keypad").css('left', offset.left - 290);

        if($("#number-keypad").is(':visible')) {
            $("#number-keypad").hide();
        }
        else {
            $("#number-keypad").show();
        }
    }

    function hideKeyboard() {
        $("#number-keypad").hide();
    }

    var prods = [];

    function pushIntoProductArray(productItem) {
        prods.push(productItem);
    }

    function initProductEvents() {
		openLoadingPage();
        $.ajax({
            url: '/.store-kiosk/store/products-ext',
            data: JSON.stringify({
                products: prods
            }), headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
				closeLoadingPage();
                if (data.success) {

                    productEvents = data.data.products;

                    $.each(data.data.products, function(index, product) {

                        if(product.event) {

                            changeToEventRow(product.productId);
                            resetEventSessionSelectOptions(product);
                            updateEventQuantityValue(product.productId);
                        }
                    });
                }
                else {
                    redirectToErrorPage();
                }
                
                refreshShoppingCartItemsValue();
            },
            error: function () {
            	closeLoadingPage();
                redirectToErrorPage();
            },
            complete: function () {
            	closeLoadingPage();
            }
        });
    }

    function updateEventQuantityValue(productId) {

        $.each(itemsInShoppingCart, function(index, item) {

            if(item.listingId == productId) {
                $("#event-counter-" + item.listingId).val(item.qty);
                $("#event-line-id-" + item.listingId).val(item.lineId);
                $("#event-minusButton-" + item.listingId).attr('class', 'btn-num icon-minus btn-active');
            }
        });
    }

    function hideSelectButton(productId) {
        $('#select-event-button-' + productId).hide();
        $('#cancel-event-button-' + productId).show();
    }

    function hideCancelButton(productId) {
        $('#select-event-button-' + productId).show();
        $('#cancel-event-button-' + productId).hide();
    }

    function changeToEventRow(productId) {

        $('#non-event-type-product-' + productId).hide();
        $('#event-type-product-' + productId).show();

        hideEventRow(productId);
    }

    function showEventRow(productId) {
		
        $('#product-row-' + productId).attr("class", "");
        $('#closing-row-' + productId).show();
        $('#event-row-' + productId).show();

        hideSelectButton(productId);
        refreshScroller();

    }

    function hideEventRow(productId) {

        $('#product-row-' + productId).attr("class", "first-level");
        $('#closing-row-' + productId).hide();
        $('#event-row-' + productId).hide();

        hideCancelButton(productId);
        refreshScroller();
    }

    function resetEventSessionSelectOptions(product) {

		if(product.eventLines){
	        $.each(product.eventLines, function (index, eventLine) {
	            $('#event-session-' + product.productId).append($('<option/>', {
	                value: eventLine.eventLineId,
	                text : eventLine.eventName
	            }));
	        });
	        if(selectedEventSession){
				$("#event-session-" + product.productId).val(selectedEventSession);
			}
	        resetEventSessionSelectListener(product);
		}

    }

    function resetEventSessionSelectListener(product) {

        var productId = product.productId;

        var $sessionSelect = $('#event-session-' + productId);
        var eventDateWidget = $('#event-date-' + productId);
							
		eventDateWidget.pickadate({
			format: 'dd/mm/yyyy',
		    disable: [],
			container: '#event-date-container',
			closeOnSelect: true,
			closeOnClear: true
		});

        $sessionSelect.change(function() {

            var eventLineId = $sessionSelect.val();
			$picka = null;
            $.each(product.eventLines, function(index, eventLine) {
				
                if (eventLine.eventLineId == eventLineId) {

                    var eventDates = eventLine.eventDates;
                    var enableDates = [];

                    enableDates.push(true);
					if(product.capacity && !product.openDate){
						var today = new Date((new Date()).valueOf());
						enableDates.push(today);
					}else{
						$.each(eventDates, function(index, eventDate) {
                        	var dtArr = eventDate.date.split('/');
                        	enableDates.push([Number(dtArr[2]), Number(dtArr[1]) -1 , Number(dtArr[0])]);
                   		 });
					}

					eventDateWidget.pickadate().pickadate('picker').clear();
					eventDateWidget.pickadate().pickadate('picker').stop();
					$picka = eventDateWidget.pickadate({
						format: 'dd/mm/yyyy', container: '#event-date-container', disable: enableDates
					});
                }
            });
            var today = new Date((new Date()).valueOf());
			$picka.pickadate('picker').set('select', today); 
        });

        $sessionSelect.change();
    }

    function updateItemsSelectedDate() {
		var today = new Date((new Date()).valueOf());
		picker.set('select', today);
        $.each(productsInShoppingCart, function(index, product) {

            if(product.id == "${productNodeName}") {

                $.each(product.items, function(index, item) {
					
                    if(item.eventGroupId != "") {
                    		var dtArr = item.selectedEventDate.split('/');
                        	var pickerDate = [Number(dtArr[2]), Number(dtArr[1]) -1 , Number(dtArr[0])];
                        	picker.set('select', pickerDate);
                    }
                });
            }
        });
    }

    function selectNumber(number) {

        tempNumber += number;

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);

        if(actualNumber > 0) {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-active');
        }
        else {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
        }
    }

    function deleteNumber() {

        var inputText = selectedItemQuantityElement.val().toString();

        var textSize = inputText.length - 1;

        if(textSize == 0) {
            selectedItemQuantityElement.val(0);
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
            tempNumber = '';
            return;
        }

        tempNumber = inputText.substring(0, inputText.length-1);

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);
    }

    $(document).ready(function() {
        loaded();
        if($('#scrollbar3').length){
            var myScroll3 = new IScroll('#scrollbar3', { scrollbars: 'custom', interactiveScrollbars: true });
        }


        $(document).click(function(e) {

            var container = $("#number-keypad");

            if(e.target.id != undefined && e.target.id.startsWith("counter-")) {
                return;
            }

            if(e.target.id != undefined && e.target.id.startsWith("event-counter-")) {
                return;
            }

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });

        initProductEvents();
    });

</script>

<style>

    .event-row {

        border-style: solid;
        border-width: 1px;
        border-color: #1ca9ea;
        background-color: #f7fdff;
    }

    .select-event {

        width: 200px;
        height:36px;
        border: 4px solid #cccccc;
        padding: 0px;
        margin: 0px;
        text-align: center;
        font-size: 18px;
        font-family: 'harabara', 'STHeiti', Arial, Helvetica, sans-serif;
    }

    .promoText {

        color: #f9383f;
        font-size: 13px;
        font-weight: bold;
        border: 1px solid #ffd3d6;
        background-color: #fff8f8;
        border-radius: 4px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
    }

</style>

[#if productNodeName?has_content]
<div class="main-wrapper" id="event-date-container">
    <div>
        <div>
            <div class="tix-container tix-list tix-collapse">

                [#assign previewMode = ctx.getParameter("preview")!]
                [#assign isOnPreview = false]
                [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
                    [#assign isOnPreview = true]
                    <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
                [/#if]

                <h1 class="secondary-heading sy-hg-md left">${productNode.getProperty("name").getString()}</h1>

                [#if announcementList?has_content]
                    <div class="alert-section-product-details alert-section">
                        <h3>Alert / Announcement</h3>
                        <div id="scrollbar3" style="position:relative;height:105px;width:100%;background-color:#C41C33">
                            <div class="scroller">
                                <ul>
                                    [#list announcementList as announcement]
                                        <li>
                                            <p style="height:100%;width:95%">
                                            ${announcement.content}
                                            </p>
                                        </li>
                                    [/#list]
                                </ul>
                            </div>
                        </div>
                    </div>
                [/#if]

                <div class="clearfix"></div>

                <div class="ticket-container">

                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                        <tr>
                            <td width="47%">${staticText[lang]['standardTickets']}</td>
                            <td width="19%" class="center">${staticText[lang]['price']}</td>
                            <td width="25%" class="center">${staticText[lang]['qty']}</td>
                            <td width="5%" class="center">&nbsp;</td>
                        </tr>
                    </table>

                    <div class="tab-content-container fir-template">
                        <div id="scrollbar2" style="width: 860px;">
                            <div class="scroller">
                                <ul>
                                    <li>
                                        <table cellpadding="0" cellspacing="0" border="0" class="table-content">

                                            [#list axProducts as axProduct]
                                                [#assign axProductDetail = productNodeDetails[axProduct.getProperty("relatedAXProductUUID").getString()]]
                                                [#assign topUpList = starfn.getAXProductCrossSells(axProductDetail)]
												[#assign productListingId = axProductDetail.getProperty('productListingId').getString()]

                                               [#if topUpList?has_content]
                                                    <script>
                                                        hasTopUpItems.push(${productListingId});
                                                    </script>
                                                [/#if]
												<input type="hidden" id="topup-${productListingId}" value="${(topUpList?has_content)?string("true","false")}" />
                                                <script>
                                                    var productItem = {
                                                        productId: ${axProductDetail.getProperty('productListingId').getString()},
                                                        itemId: ${axProductDetail.getProperty("displayProductNumber").getString()}
                                                    };

                                                    pushIntoProductArray(productItem);
                                                </script>

                                                	[#assign pairProductFlag = "no"]
                                                	[#assign bothHavePromotion = "no"]
                                                	[#assign productName = axProductDetail.getProperty("itemName").getString()]
                                                	[#assign axProdListingId = axProductDetail.getProperty('productListingId').getString()]
                                                	[#assign ticketType = axProductDetail.hasProperty("ticketType")?string(axProductDetail.getProperty("ticketType").getString(),"")]
                                                	[#assign promotionList = starfn.getAXProductPromotions(axProductDetail)]

                                                	[#assign pairAxProduct = axProduct]
                                                	[#assign pairAxProductDetail = axProductDetail]
  													[#list axProducts as childAxProduct]
  														[#assign childAxProductDetail = productNodeDetails[childAxProduct.getProperty("relatedAXProductUUID").getString()]]
  														[#assign childProductName = childAxProductDetail.getProperty("itemName").getString()]
  														[#assign childAxProdListingId = childAxProductDetail.getProperty('productListingId').getString()]


  														[#if productName == childProductName && axProdListingId != childAxProdListingId]
															[#assign pairProductFlag = "yes"]
															[#assign pairAxProduct = childAxProduct]
															[#assign pairAxProductDetail = childAxProductDetail]
															[#assign childPromotionList = starfn.getAXProductPromotions(childAxProductDetail)]
															[#if promotionList?has_content && childPromotionList?has_content]
																[#assign bothHavePromotion = "yes"]
															[/#if]
														[#else]

  													    [/#if]
  													[/#list]
  												[#if pairProductFlag == "no" || bothHavePromotion == "yes" || (pairProductFlag == "yes" && ticketType != 'Child') ]
                                                <tr id="product-row-${axProductDetail.getProperty('productListingId').getString()}" class="first-level">
                                                    <td colspan="3">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr id="axproduct-row-${axProductDetail.getProperty('productListingId').getString()}">
                                                                <td width="45%" class="inner-container" rowspan="${(pairProductFlag == 'yes')?string('2','1')}">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td class="product-title">${productName}</td>
                                                                        </tr>

                                                                        [#list promotionList as promotion]
                                                                            <tr>
                                                                                <td class="product-dn">
                                                                                    [#assign promotionIconUUID = promotion.getProperty("promotionIcon").getString()]
                                                                                    [#assign promotionIconImageURL = damfn.getAssetLink(promotionIconUUID)]
                                                                                    <img src="${promotionIconImageURL}" />
                                                                                    [#assign promotionText = promotion.getProperty("promotionText").getString()]
                                                                                    ${promotionText}

                                                                                    [#assign promotionAdditionalInfo = promotion.getProperty("promotionAdditionalInfo").getString()]

                                                                                    [#if promotionAdditionalInfo?has_content]
                                                                                        <span onclick="popupPromotionTNC([@compress single_line=true]'${promotionAdditionalInfo}'[/@compress])">
                                                                                            <img src="${themePath}/images/icon-sm-info.png" />
                                                                                        </span>
                                                                                    [/#if]
                                                                                </td>
                                                                            </tr>
                                                                        [/#list]

                                                                        [#if pairProductFlag=="yes" && bothHavePromotion == "no"]
	                                                                        [#list childPromotionList as childPromotion]
	                                                                            <tr>
	                                                                                <td class="product-dn">
	                                                                                    [#assign promotionIconUUID = childPromotion.getProperty("promotionIcon").getString()]
	                                                                                    [#assign promotionIconImageURL = damfn.getAssetLink(promotionIconUUID)]
	                                                                                    <img src="${promotionIconImageURL}" />
	                                                                                    [#assign promotionText = childPromotion.getProperty("promotionText").getString()]
	                                                                                    ${promotionText}

	                                                                                    [#assign promotionAdditionalInfo = childPromotion.getProperty("promotionAdditionalInfo").getString()]

	                                                                                    [#if promotionAdditionalInfo?has_content]
	                                                                                        <span onclick="popupPromotionTNC([@compress single_line=true]'${promotionAdditionalInfo}'[/@compress])">
	                                                                                            <img src="${themePath}/images/icon-sm-info.png" />
	                                                                                        </span>
	                                                                                    [/#if]
	                                                                                </td>
	                                                                            </tr>
	                                                                        [/#list]
                                                                        [/#if]
                                                                    </table>
                                                                </td>
                                                                <td width="30%" class="center inner-container">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td>${axProductDetail.hasProperty("ticketType")?string(axProductDetail.getProperty("ticketType").getString(),"")} ${axProductDetail.hasProperty("ticketType")?string("-","")} S$${axProductDetail.getProperty("productPrice").getString()}</td>
                                                                        </tr>
                                                                        [#assign promotionList = starfn.getAXProductPromotions(axProductDetail)]
                                                                        [#list promotionList as promotion]
                                                                            <tr>
                                                                                <td>
                                                                                    [#assign discountedPrice = promotion.getProperty("cachedDiscountPrice").getDouble()]
                                                                                    <span class="promoText">&nbsp;Promo Price: S$${discountedPrice?string["0.00"]}&nbsp;</span>
                                                                                </td>
                                                                            </tr>
                                                                        [/#list]
                                                                    </table>
                                                                </td>
                                                                <td width="25%" class="center inner-container">

                                                                    <div id="event-type-product-${axProductDetail.getProperty('productListingId').getString()}" style="display: none;">
                                                                        <button id="cancel-event-button-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="hideEventRow(${axProductDetail.getProperty('productListingId').getString()})">Cancel</button>
                                                                        <button id="select-event-button-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="showEventRow(${axProductDetail.getProperty('productListingId').getString()})">Select</button>
                                                                        <br />
                                                                    </div>
                                                                    <div id="non-event-type-product-${axProductDetail.getProperty('productListingId').getString()}">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <button id="minusButton-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-num icon-minus btn-inactive" onclick="decreaseCounter('counter-${axProductDetail.getProperty('productListingId').getString()}', 'minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                                <td>
                                                                                	<span onclick="displayKeyboard('minusButton-${axProductDetail.getProperty('productListingId').getString()}', 'counter-${axProductDetail.getProperty('productListingId').getString()}')">
                                                                                    	<input id="counter-${axProductDetail.getProperty('productListingId').getString()}" type="text" value="0" class="ng-pristine ng-untouched ng-valid" disabled />
                                                                                    	<input id="line-id-${axProductDetail.getProperty('productListingId').getString()}" type="hidden" />
                                                                                	</span>
                                                                                </td>
                                                                                <td>
                                                                                    <button type="button" class="btn-num icon-plus btn-active" onclick="increaseCounter('counter-${axProductDetail.getProperty('productListingId').getString()}', 'minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            [#if pairProductFlag == "yes" && bothHavePromotion == "no"]
                                                            <tr id="axproduct-row-${pairAxProductDetail.getProperty('productListingId').getString()}">
                                                                <td width="30%" class="center inner-container">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td>${axProductDetail.hasProperty("ticketType")?string(axProductDetail.getProperty("ticketType").getString(),"")} ${pairAxProductDetail.hasProperty("ticketType")?string("-","")} S$${pairAxProductDetail.getProperty("productPrice").getString()}</td>
                                                                        </tr>

                                                                        [#list childPromotionList as childPromotion]
                                                                            <tr>
                                                                                <td>
                                                                                    [#assign discountedPrice = childPromotion.getProperty("cachedDiscountPrice").getDouble()]
                                                                                    <span class="promoText">&nbsp;Promo Price: S$${discountedPrice?string["0.00"]}&nbsp;</span>
                                                                                </td>
                                                                            </tr>
                                                                        [/#list]
                                                                    </table>
                                                                </td>
                                                                <td width="25%" class="center inner-container">

                                                                    <div id="event-type-product-${pairAxProductDetail.getProperty('productListingId').getString()}" style="display: none;">
                                                                        <button id="cancel-event-button-${pairAxProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="hideEventRow(${pairAxProductDetail.getProperty('productListingId').getString()})">Cancel</button>
                                                                        <button id="select-event-button-${pairAxProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-secondary-1" onclick="showEventRow(${pairAxProductDetail.getProperty('productListingId').getString()})">Select</button>
                                                                        <br />
                                                                    </div>
                                                                    <div id="non-event-type-product-${pairAxProductDetail.getProperty('productListingId').getString()}">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <button id="minusButton-${pairAxProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-num icon-minus btn-inactive" onclick="decreaseCounter('counter-${pairAxProductDetail.getProperty('productListingId').getString()}', 'minusButton-${pairAxProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                                <td>
                                                                                	<span onclick="displayKeyboard('minusButton-${pairAxProductDetail.getProperty('productListingId').getString()}', 'counter-${pairAxProductDetail.getProperty('productListingId').getString()}')">
                                                                                    	<input id="counter-${pairAxProductDetail.getProperty('productListingId').getString()}" type="text" value="0" class="ng-pristine ng-untouched ng-valid" disabled />
                                                                                    	<input id="line-id-${pairAxProductDetail.getProperty('productListingId').getString()}" type="hidden" />
                                                                                	</span>
                                                                                </td>
                                                                                <td>
                                                                                    <button type="button" class="btn-num icon-plus btn-active" onclick="increaseCounter('counter-${pairAxProductDetail.getProperty('productListingId').getString()}', 'minusButton-${pairAxProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            [/#if]
                                                        </table>
                                                    </td>
                                                </tr>
                                                [/#if]
                                                <tr id="event-row-${axProductDetail.getProperty('productListingId').getString()}" class="event-row" style="display: none;">
                                                    <td width="45%" class="inner-container" style="padding-left:20px;">
                                                        <div style="font-weight: bold;">Choose Date of Visit</div>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        <input class="input-datepicker" id="event-date-${axProductDetail.getProperty('productListingId').getString()}" style="width: 200px;" type="text" placeholder="dd/mm/yyyy" readonly />
                                                                        <button class="btn-datepicker" type="button" id="event-date-button-${axProductDetail.getProperty('productListingId').getString()}" style="vertical-align: middle; background:transparent;border:none;margin-left:5px;width:35px;">
                                                                            <img src="${themePath}/images/icon-calendar.png" />
                                                                        </button>
                                                                        <div id="datepicker-container" style="position:relative"></div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="30%" class="inner-container">
                                                        <div style="font-weight: bold;">Choose Session</div>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <select id="event-session-${axProductDetail.getProperty('productListingId').getString()}" class="select-event">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>

                                                    <td width="25%" class="center inner-container" style="padding-top:5px;">
                                                        <div style="font-weight: bold;">Qty</div>
                                                        <div>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <button id="event-minusButton-${axProductDetail.getProperty('productListingId').getString()}" type="button" class="btn-num icon-minus btn-inactive" onclick="decreaseCounter('event-counter-${axProductDetail.getProperty('productListingId').getString()}', 'event-minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                    </td>
                                                                    <td>
                                                                    	<span onclick="displayKeyboard('event-minusButton-${axProductDetail.getProperty('productListingId').getString()}', 'event-counter-${axProductDetail.getProperty('productListingId').getString()}')">
                                                                        	<input id="event-counter-${axProductDetail.getProperty('productListingId').getString()}" type="text" value="0" class="ng-pristine ng-untouched ng-valid" disabled />
                                                                        	<input id="event-line-id-${axProductDetail.getProperty('productListingId').getString()}" type="hidden"/>
                                                                    	</span>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn-num icon-plus btn-active" onclick="increaseCounter('event-counter-${axProductDetail.getProperty('productListingId').getString()}', 'event-minusButton-${axProductDetail.getProperty('productListingId').getString()}')"></button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="closing-row-${axProductDetail.getProperty('productListingId').getString()}" style="border-bottom: 1px solid #d9d9d9; display: none;">
                                                    <td colspan="3"></td>
                                                </tr>
                                            [/#list]
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="more-info-container">
                    <div class="more-info">
                        <button class="btn-fifth" onclick="popupTNC()">${staticText[lang]['tAndC']}</button>
                    </div>
                </div>

                <div class="notes-container">
                    <h3>
                        <span>${staticText[lang]['notes']}</span>
                        <button type="button" class="btn-view" onclick="openViewMoreDialog()"></button>
                    </h3>

                    <div id="scrollbar1" style="width: 840px;">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <div style="width:95%">
                                        [#if productNode.hasProperty("notes")]
                                            <p style="height:100%;margin:0">${productNode.getProperty("notes").getString()}</p>
                                        [/#if]
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-md btn-third" onclick="[#if ctx.getParameter('backToCart')?has_content] redirectToShoppingCartPage() [#else] redirectToProductDetailPage() [/#if]">${staticText[lang]['backButtonLabel']}</button>
                    <button id="addToFunCartButton" type="button" class="btn btn-md btn-primary" onclick="addToShoppingCart()"></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tncDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button" onclick="closeOnTNC()"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['tAndC']}</h2>
        <section>
            <div id="scrollbar6" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                <p id="tncContent">
										[#assign tnc = starfn.getTermsAndConditions(channel, productNodeName)]
										${tnc}
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeOnTNC()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="viewMoreDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['notes']}</h2>
        <section>
            <div id="scrollbar7" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                [#if productNode.hasProperty("notes")]
                                    <p style="height:100%; margin:0">${productNode.getProperty("notes").getString()}</p>
                                [/#if]
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeViewMoreDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="number-keypad" style="z-index:1001; display: none" class="keypad-dialog">
    <div class="arrow-up"></div>
    <h2>Please Key In Quantity</h2>
    <ul class="keypad-controller">
        <li><button type="button" onclick="selectNumber(1)">1</button></li>
        <li><button type="button" onclick="selectNumber(2)">2</button></li>
        <li><button type="button" onclick="selectNumber(3)">3</button></li>
        <li><button type="button" onclick="selectNumber(4)">4</button></li>
        <li><button type="button" onclick="selectNumber(5)">5</button></li>
        <li><button type="button" onclick="selectNumber(6)">6</button></li>
        <li><button type="button" onclick="selectNumber(7)">7</button></li>
        <li><button type="button" onclick="selectNumber(8)">8</button></li>
        <li><button type="button" onclick="selectNumber(9)">9</button></li>
        <li><button type="button" onclick="selectNumber(0)">0</button></li>
        <div class="clearfix"></div>
    </ul>
    <div class="group-inner-btn keypad-group">
        <button type="button" class="btn btn-third btn-xs ng-binding" onclick="deleteNumber()">Delete</button>
        <button type="button" class="btn btn-primary btn-xs ng-binding" onclick="hideKeyboard()">OK</button>
    </div>
</div>
[/#if]

<script>
    $(document).ready(function() {
		$(".btn-datepicker").click(function(event) {
			var pickadate = $(event.target).parent().prev().pickadate("picker");
			pickadate.open();
			event.stopPropagation();
        });	
       	$(".input-datepicker").click(function(event) {
			var pickadate = $(event.target).pickadate("picker");
			pickadate.open();
			event.stopPropagation();
        });	
        
    })

 </script>