[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#if ctx.getParameter("signatureRequired")?has_content]
    [#assign signatureRequired = ctx.getParameter("signatureRequired")]
[#else]
    [#assign signatureRequired = false]
[/#if]

<script>
    var userSignatureData = '';
    var receiptNumber = '';
    function clearShoppingCart() {

        $.ajax({
            url : '/.store-kiosk/store/clear-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
                    redirectToHomePage();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function redirectToHomePage() {
        window.location.href = "${sitePath}";
    }
    
    
    function startCheckout() {

        $.ajax({
            url: '/.store-kiosk/store/start-checkout',
            data: userSignatureData,
            headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {

                if (result.success) {
                    receiptNumber = result.data.receiptNumber;

                    printTicketsAndReceipt(receiptNumber);
                } else {
                    showErrorScreen(result.message);
                }
            },
            error: function () {
                redirectToErrorPage();
            },
            complete: function () {
            }
        });
    }
    
    

    function compleletCheckout(errorPrinting) {

        $.ajax({
            url: '/.store-kiosk/store/checkout-complete-sale',
            data: '',
            headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {

				if (result.success) {
                	if(errorPrinting){
                		showErrorScreen('Please note down the receipt number (' + result.data.axReceiptNumber
                    					+ ') and print your tickets at other ticketing kiosks or counters.<br /> We are sorry for any inconvenience caused.');
                	}else{
                    	showCompletedScreen();
                    }
                } else {
						redirectToErrorPage();
                }
            },
            error: function () {
                redirectToErrorPage();
            },
            complete: function () {
            }
        });
    }

    function printTicketsAndReceipt(receiptNumber) {

        $.ajax({
            url : '/.store-kiosk/kiosk-slm/printTicketsAndReceipt',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            data : receiptNumber,
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                
                if (result.success) {
                	compleletCheckout(false);
                }else {
                    compleletCheckout(true);
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function showCompletedScreen() {
        $("#paymentSuccessfulContent").hide();
        $("#printSuccessfulContent").show();
    }

    function showErrorScreen(errorMessage) {
        $("#printErrorMessage").html(errorMessage);
        $("#printErrorContent").show();
    }

    $(document).ready(function() {
      [#if signatureRequired == "true"]
        $("#paymentSuccessfulContent").hide();
	  	$("#signature-sectioin").show();
      	launchSignature();
      [#else]
      	startCheckout();
      [/#if]

    });
    
    function launchSignature(){
    	$("#signature-sectioin").show();
    	var sigdiv = $("#signature-parent").jSignature({ height: '500px', width: '930px', color: 'black', 'background-color': 'white', 'decor-color': 'transparent', 'lineWidth': 6 });
	    $("#clear").click(function(){
	    	sigdiv.jSignature('reset')
	    });
	    $("#done").click(function(){
	    	if(sigdiv.jSignature('getData', 'native').length == 0) 
	    	{
	            return;
	        }
	        var data = sigdiv.jSignature('getData', 'image');
	        userSignatureData = data[1] ;
	        $("#signature-sectioin").hide();
	        $("#paymentSuccessfulContent").show();
	        startCheckout();
	    });
    
    }

</script>

	<section id="signature-sectioin" class="info"  style="display: none;padding-top:100px;">
    	<span class="pink-bold"></span>
        <br />
        <div class="flex-box">
        	
            <div id="signature-parent" class="signature" style="margin: 0 auto;">
	            <canvas id="signature-header" width=930 height=40></canvas>
	            <script>
		  			var canvas = document.getElementById("signature-header");
		  			var context = canvas.getContext("2d");
					context.fillStyle = "#c7c7c7";
		  			context.font = "bold 30px Arial";
		  			context.fillText("Sign Here", 10, 30);
				</script>
            </div>
            
		</div>
        <div class="canvas-box">
            <div class="canvas-content">
            	<div class="canvas-clear">
                    <button id="clear" type="button" class="btn btn-md btn-third ng-binding">Clear</button>
                </div>
                <div class="canvas-done">
               
                <button id="done" type="button" class="btn btn-md btn-primary">${staticText[lang]['okButtonLabel']}</button>
                </div>
            </div>
        </div>
	</section>
	
<div id="paymentSelectionContent">
    <div id="paymentSuccessfulContent">
        <div class="main-wrapper">
            <h1 class="box-wrapper top-center">${staticText[lang]['transactionSuccessLabel1']}</h1>
            <h2 class="primary-heading py-hg-sm">${staticText[lang]['transactionSuccessLabel2']}</h2>
            <ul class="box-wrapper center-center printing-img">
                <li>
                    <img src="${themePath}/images/printing-at.gif" class="printing-at"/>
                    <img src="${themePath}/images/collection/take-receipt-ticket.jpg" />
                </li>
            </ul>
        </div>
    </div>

    <div id="printSuccessfulContent" style="display: none">
        <div class="main-wrapper">
            <ul class="box-wrapper center-center printing-img complete-box">
                <li>
                    <img src="${themePath}/images/icon-success.png" />
                    <h2 class="success-heading">${staticText[lang]['printSuccessLabel1']}</h2>
                    <p class="secondary-heading sy-hg-sm">
                    ${staticText[lang]['printSuccessLabel2']}
                        <br />
                        <img src="${themePath}/images/logo-complete.png" />
                    </p>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="clearShoppingCart()">${staticText[lang]['backToHomeButtonLabel']}</button>
        </div>
    </div>

    <div id="printErrorContent" style="display: none;">
        <div class="main-wrapper">
            <h1 class="secondary-heading sy-hg-lg"></h1>
            <ul class="box-wrapper center-center error-box">
                <li>
                    <img src="${themePath}/images/icon-fail.png" class="icon-fail"/>
                    <h2 id="printErrorMessage" class="error-heading"></h2>
                </li>
            </ul>
        </div>
        <div class="group-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="clearShoppingCart()">${staticText[lang]['okButtonLabel']}</button>
        </div>
        <div ng-include="'templates/pop-up-loading.html'" ng-show="showLoading==true"></div>
    </div>
</div>