[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

[#if productNodeName?has_content]
    [#assign productNode = starfn.getCMSProduct(channel, productNodeName)]
    [#assign axProducts = starfn.getAXProductsByCMSProductNode(productNode)!]
    [#assign productNodeDetails = starfn.getAXProductsDetailByCMSProductNode(productNode)!]
[/#if]

<script>
    var crossSellCartItems = [];
    var newCrossSellCartItems = [];
    var cartItems = [];
    var selectedItemQuantityElement;
    var selectedItemDecrementCounterElement;
    var tempNumber = '';

    function decreaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) - 1;

        if(currentValue >= 0)
            $('#' + elementId).val(currentValue);

        if(currentValue == 0)
            $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-inactive');
    }

    function increaseCounter(elementId, decreaseButtonId) {

        var currentValue = parseInt($('#' + elementId).val()) + 1;

        $('#' + elementId).val(currentValue);
        $('#' + decreaseButtonId).attr('class', 'btn-num icon-minus btn-active');
    }

    function popupTNC() {

        $("#tncDialog").show();
        $('#scrollbar6-scrollbar').remove();
        var myScroll6;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
            myScroll6 = new IScroll('#scrollbar6', { scrollbars: 'custom', interactiveScrollbars: true });
        }, 1);
    }

    function closeOnTNC() {
        $("#tncDialog").hide();
    }

    function openViewMoreDialog() {

        $("#viewMoreDialog").show();
        $('#scrollbar7-scrollbar').remove();
        var myScroll7;
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        setTimeout(function () {
            myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
        }, 1);
    }

    function closeViewMoreDialog() {
        $("#viewMoreDialog").hide();
    }
    
    function refreshCrossSellItemsCart() {

        $.ajax({
            url : '/.store-kiosk/store-data/get-cross-sell-items/${lang}',
            data : '${productNodeName}',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {
					
					$.ajax({
							url : '/.store-kiosk/store/get-cart',
							data : '',
							headers : {
							    'Store-Api-Channel' : '${channel}'
							},
							type : 'GET',
							cache : false,
							dataType : 'json',
							contentType : 'application/json',
							success : function(getCartResult) {
													
							    if (getCartResult.success) {
													
							        var products = getCartResult.data.cmsProducts;
									crossSellCartItems = result.data.crossSellCartItems;
									$.each(products, function(x, product) {
										$.each(product.items, function(y, item) {
											$.each(crossSellCartItems, function(z, crossSell) {
												if(crossSell.crossSellProductListingId == item.listingId && crossSell.parentListingId == item.parentListingId){
													crossSell.quantity = item.qty;
													crossSell.lineId = item.lineId;
												}
											});
										}); 
									}); 
									
									createTableRows(crossSellCartItems);
									refreshScroller() ;
							    }
							    else {
							        redirectToErrorPage();
							    }
							},
							error : function() {
							    redirectToErrorPage();
							}
						});

                }else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });

        resetButtonToAdd();
    }

    function resetButtonToAdd() {
        $("#addToFunCartButton").text("${staticText[lang]['addToFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","addToShoppingCart()");
    }

    function resetButtonToUpdate() {
        $("#addToFunCartButton").text("${staticText[lang]['updateFunCartButton']}");
        $("#addToFunCartButton").attr("onclick","updateShoppingCart()");
    }

	function updateShoppingCart(){
	    if(!validateQuantity()) {
            return;
        };
        openLoadingPage();
		$.ajax({
            url: '/.store-kiosk/store/update-topup-to-cart',
            data: JSON.stringify({
                sessionId: '',
                items: cartItems
            }),
            headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(data) {
				closeLoadingPage();
                if (data.success) {
                    redirectToShoppingCartPage();
                }
                else {
                    showAlertDialog("Info", data.message);
                }
            },
            error: function() {
                redirectToErrorPage();
            }
        });
	
	}
    function commitToShoppingCart() {
		openLoadingPage();
        $.ajax({
            url: '/.store-kiosk/store/add-topup-to-cart',
            data: JSON.stringify({
                sessionId: '',
                items: cartItems
            }),
            headers: { 'Store-Api-Channel' : '${channel}' },
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function(data) {
				closeLoadingPage();
                if (data.success) {
                    redirectToShoppingCartPage();
                }
                else {
                    showAlertDialog("Info", data.message);
                }
            },
            error: function() {
                redirectToErrorPage();
            }
        });
    }

    function addToShoppingCart() {

        if(!validateQuantity()) {
            return;
        };

        commitToShoppingCart();
    }

    function validateQuantity() {
		cartItems = [];
        $.each(crossSellCartItems, function(index, crossSellCartItem) {

           var quantity = $("#counter-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId).val();

            if(quantity > 0) {
                crossSellCartItem.quantity = quantity;
                newCrossSellCartItems.push(crossSellCartItem);
            }
        });

        if(newCrossSellCartItems.length == 0) {

            showAlertDialog("${staticText[lang]['titleAlert']}", "${staticText[lang]['alertEmptyQty2']}");
            return false;
        }


        $.each(newCrossSellCartItems, function(index, crossSellCartItem) {

            var item = {
                cmsProductName: crossSellCartItem.cmsProductName,
                cmsProductId: crossSellCartItem.parentCmsProductId,
                parentCmsProductId: crossSellCartItem.parentCmsProductId,
                parentListingId: crossSellCartItem.parentListingId,
                listingId: crossSellCartItem.crossSellProductListingId,
                productCode: crossSellCartItem.crossSellProductCode,
                name: crossSellCartItem.crossSellItemName,
                type: crossSellCartItem.ticketType,
                qty: Number(crossSellCartItem.quantity),
                price: Number(crossSellCartItem.productPrice),
                isTopup: true,
                lineId: crossSellCartItem.lineId
            };

            cartItems.push(item);
        });
        
        return true;
    }

    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/shoppingcart~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function redirectToOrderPage() {
        window.location.href="${sitePath}/category/product/order~${categoryNodeName}~${subCategoryName}~${productNodeName}~?pageNumber=${pageNumber}";
    }

    function loaded() {
        myScroll1 = new IScroll('#scrollbar1', { scrollbars: 'custom', interactiveScrollbars: true });
        myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
        myScroll7 = new IScroll('#scrollbar7', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    function refreshScroller() {

        setTimeout(function() {
            myScroll2.refresh();
        }, 500);
    }

    function displayKeyboard(itemDecrementCounterId, itemQuantityId) {

        tempNumber = '';

        selectedItemDecrementCounterElement = $("#" + itemDecrementCounterId);
        selectedItemQuantityElement = $("#" + itemQuantityId);

        var offset = selectedItemQuantityElement.offset();

        $("#number-keypad").css('top', offset.top + 53);
        $("#number-keypad").css('left', offset.left - 290);

        if($("#number-keypad").is(':visible')) {
            $("#number-keypad").hide();
        }
        else {
            $("#number-keypad").show();
        }
    }

    function hideKeyboard() {
        $("#number-keypad").hide();
    }

    function selectNumber(number) {

        tempNumber += number;

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);

        if(actualNumber > 0) {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-active');
        }
        else {
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
        }
    }

    function deleteNumber() {

        var inputText = selectedItemQuantityElement.val().toString();

        var textSize = inputText.length - 1;

        if(textSize == 0) {
            selectedItemQuantityElement.val(0);
            selectedItemDecrementCounterElement.attr('class', 'btn-num icon-minus btn-inactive');
            tempNumber = '';
            return;
        }

        tempNumber = inputText.substring(0, inputText.length-1);

        var actualNumber = parseInt(tempNumber);

        selectedItemQuantityElement.val(actualNumber);
    }

    function createTableRows(crossSellCartItems) {
    	var totalQty = 0;

		
        var htmlBody = "";

        $.each(crossSellCartItems, function(index, crossSellCartItem) {
			totalQty = totalQty + crossSellCartItem.quantity;
            htmlBody +=
                    "<tr class=\"first-level\">" +
                        "<td width=\"15%\">" +
                            "<img src=\"" + crossSellCartItem.imageIconPath + "\" width=\"148\" height=\"111\" />" +
                        "</td>" +
                        "<td rowspan=\"ticketGroup.tickets.length\" width=\"35%\" class=\"inner-container\">" +
                            "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">" +
                                "<tr>" +
                                    "<td class=\"product-title\">" + crossSellCartItem.crossSellItemName + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td class=\"product-dn\">" + (crossSellCartItem.crossSellItemDesc == null ? "" : crossSellCartItem.crossSellItemDesc) + "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                        "<td width=\"25%\" class=\"center product-price\">" + (crossSellCartItem.ticketType == null ? "" : crossSellCartItem.ticketType) + " - S$" + crossSellCartItem.productPrice + "</td>" +
                        "<td width=\"25%\" class=\"center inner-container\">" +
                            "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">" +
                                "<tr>" +
                                    "<td>" +
                                        "<button id=\"minusButton-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "\" type=\"button\" class=\"btn-num icon-minus btn-inactive\" onclick=\"decreaseCounter('counter-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "', 'minusButton-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "')\"></button>" +
                                    "</td>" +
                                    "<td>" +
                                        "<span onclick=\"displayKeyboard('minusButton-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "', 'counter-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "')\">" +
                                            "<input id=\"counter-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "\" type=\"text\" value=\""+crossSellCartItem.quantity+"\" class=\"ng-pristine ng-untouched ng-valid\" disabled />" +
                                        "</span>" +
                                    "</td>" +
                                    "<td>" +
                                        "<button type=\"button\" class=\"btn-num icon-plus btn-active\" onclick=\"increaseCounter('counter-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "', 'minusButton-" + crossSellCartItem.crossSellProductListingId + crossSellCartItem.parentListingId + "')\"></button>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>";
        });

        $("#crossSellItemsTable").append(htmlBody);
        if(totalQty > 0){
        	resetButtonToUpdate();
        }
    }

    $(document).ready(function() {
        loaded();
        refreshCrossSellItemsCart();

        $(document).click(function(e) {

            var container = $("#number-keypad");

            if(e.target.id != undefined && e.target.id.startsWith("counter-")) {
                return;
            }

            if(e.target.id != undefined && e.target.id.startsWith("event-counter-")) {
                return;
            }

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });
    });

</script>

[#if productNodeName?has_content]
<div class="main-wrapper" id="event-date-container">
    <div>
        <div>
            <div class="tix-container tix-list tix-collapse">

                [#assign previewMode = ctx.getParameter("preview")!]
                [#assign isOnPreview = false]
                [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
                    [#assign isOnPreview = true]
                    <div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
                [/#if]

                <h1 class="secondary-heading sy-hg-md left">${productNode.getProperty("name").getString()}</h1>

                <!--div class="alert-section">
                    <h3>Alert / Announcement</h3>
                    <div id="scrollbar9" style="position:relative;height:45px;width:100%;background-color:#C41C33">
                        <div class="scroller" style="transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">
                            <ul>
                                <li>
                                    <p style="height:100%;width:95%">Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...Sentosa Merion will be in maintenance for 3 hours from 10:00 am -13:00 pm. Sorry for the inconvenient...</p>
                                </li>
                            </ul>
                        </div>
                    <div id="scrollbar9-scrollbar" class="iScrollVerticalScrollbar iScrollLoneScrollbar" style="overflow: hidden; display: block;"><div class="iScrollIndicator" style="transition-duration: 0ms; display: block; height: 12px; transform: translate(0px, 0px) translateZ(0px); transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1);"></div></div></div>
                </div-->

                <div class="clearfix"></div>

                <div class="ticket-container" style="float:left;width:100%">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-title">
                        <tr>
                            <td width="64%">${staticText[lang]['titleTopUpItems']}</td>
                            <td width="36%" class="center">${staticText[lang]['titleQty']}</td>
                        </tr>
                    </table>
                    <div class="tab-content-container fir-template">
                        <div id="scrollbar2" style="width: 860px;">
                            <div class="scroller">
                                <ul>
                                    <li>
                                        <table cellpadding="0" cellspacing="0" border="0" class="table-content">
                                            <tr>
                                                <td colspan="4">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="crossSellItemsTable">

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="more-info-container">
                    <div class="more-info">
                        <button class="btn-fifth" onclick="popupTNC()">${staticText[lang]['tAndC']}</button>
                    </div>
                </div>

                <div class="notes-container">
                    <h3>
                        <span>${staticText[lang]['notes']}</span>
                        <button type="button" class="btn-view" onclick="openViewMoreDialog()"></button>
                    </h3>

                    <div id="scrollbar1" style="width: 840px;">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <div style="width:95%">
                                        [#if productNode.hasProperty("notes")]
                                            <p style="height:100%;margin:0">${productNode.getProperty("notes").getString()}</p>
                                        [/#if]
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="group-btn box-wrapper center-center">
                    <button type="button" class="btn btn-md btn-third" onclick="[#if ctx.getParameter('backToCart')?has_content] redirectToShoppingCartPage() [#else] redirectToOrderPage() [/#if]">${staticText[lang]['backButtonLabel']}</button>
                    <button type="button" class="btn btn-md btn-third" onclick="redirectToShoppingCartPage();">${staticText[lang]['skipButtonLabel']}</button>
                    <button id="addToFunCartButton" type="button" class="btn btn-md btn-primary" onclick="addToShoppingCart()"></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tncDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button" onclick="closeOnTNC()"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['tAndC']}</h2>
        <section>
            <div id="scrollbar6" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                <p>
									[#assign tnc = starfn.getTermsAndConditions(channel, productNodeName)]
									${tnc}
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeOnTNC()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="viewMoreDialog" class="dialog-container box-wrapper center-center" style="display: none">
    <div style="z-index:-1" class="dialog-container"></div>
    <div style="z-index:1" class="dialog-ct popup-tc">
        <div class="btn-close" style="display:none">
            <button type="button"></button>
        </div>
        <h2 class="primary-heading py-hg-sm">${staticText[lang]['notes']}</h2>
        <section>
            <div id="scrollbar7" style="position:relative;height:100%;width:100%">
                <div class="scroller" style="width: 95%;">
                    <ul>
                        <li>
                            <div>
                                [#if productNode.hasProperty("notes")]
                                    <p style="height:100%; margin:0">${productNode.getProperty("notes").getString()}</p>
                                [/#if]
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-md btn-primary" onclick="closeViewMoreDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="number-keypad" style="z-index:1001; display: none" class="keypad-dialog">
    <div class="arrow-up"></div>
    <h2>Please Key In Quantity</h2>
    <ul class="keypad-controller">
        <li><button type="button" onclick="selectNumber(1)">1</button></li>
        <li><button type="button" onclick="selectNumber(2)">2</button></li>
        <li><button type="button" onclick="selectNumber(3)">3</button></li>
        <li><button type="button" onclick="selectNumber(4)">4</button></li>
        <li><button type="button" onclick="selectNumber(5)">5</button></li>
        <li><button type="button" onclick="selectNumber(6)">6</button></li>
        <li><button type="button" onclick="selectNumber(7)">7</button></li>
        <li><button type="button" onclick="selectNumber(8)">8</button></li>
        <li><button type="button" onclick="selectNumber(9)">9</button></li>
        <li><button type="button" onclick="selectNumber(0)">0</button></li>
        <div class="clearfix"></div>
    </ul>
    <div class="group-inner-btn keypad-group">
        <button type="button" class="btn btn-third btn-xs ng-binding" onclick="deleteNumber()">Delete</button>
        <button type="button" class="btn btn-primary btn-xs ng-binding" onclick="hideKeyboard()">OK</button>
    </div>
</div>
[/#if]