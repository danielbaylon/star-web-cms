[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]

[#if ctx.getParameter("pageNumber")?has_content]
    [#assign pageNumber = ctx.getParameter("pageNumber")?number]
[#else]
    [#assign pageNumber = 1]
[/#if]

[#assign categoryNodeName = params[0]!""]
[#assign subCategoryName = params[1]!""]
[#assign productNodeName = params[2]!""]

<script>

    var productsInShoppingCart = [];

    function fetchShoppingCartItems() {

        $.ajax({
            url : '/.store-kiosk/store/redemption/get-cart',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'GET',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {

                if (result.success) {

                    var totalPrice = "S$0.00";

                    productsInShoppingCart = result.data.cmsProducts;

                    resetShoppingCartQuantity(result.data.totalQty);
                    refreshShoppingCartItems();

                    loadScroller();
                }
                else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });
    }

    function refreshShoppingCartItems() {

        $('#shoppingCartItems').empty();

        var rowsHtml = '';

        $.each(productsInShoppingCart, function(index, product) {

            $.each(product.items, function(index, item) {

               rowsHtml += '<tr>' +
                            '   <td>' +
                            '       <table cellpadding="0" cellspacing="0" border="0" width="100%">' +
                            '           <tr class="cart-list">' +
                            '               <td width="40%" class="title">' + item.name +
                            '                   <p class="tix-type">${staticText[lang]["ticketTypeLabel"]} : ' + item.type + '</p>' ;
               if(item.eventLineId){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventDateLabel"]} : ' + item.selectedEventDate + '</p>';							
				                         }
               if(item.eventLineId){
               
					rowsHtml +=                '<p class="tix-type">${staticText[lang]["selectedEventSessionLabel"]} : ' + item.eventLineId + '</p>';						
				                   }	
				    rowsHtml += 
				            '                   <p class="tix-type" style="color: red"></p>' +
                            '               </td>';
                

                rowsHtml += ' <td width="30%" class="center">' + item.expiryDate +'</td>' +
                        ' <td width="30%" class="center">' + item.qty + '</td>';


                rowsHtml += ' 		</tr>' +
                        '       </table>' +
                        '   </td>' +
                        '</tr>';
            });

        });

        $('#shoppingCartItems').append(rowsHtml);
    }

    function resetShoppingCartQuantity(totalQuantity) {
        $('#totalShoppingItemCount').text(totalQuantity);
    }



    function redirectToShoppingCartPage() {
        window.location.href = "${sitePath}/redemption-shoppingcart";
    }

    function redirectToReceiptPage() {
        window.location.href = "${sitePath}/receipt?signatureRequired=false";
    }

    function loadScroller() {
        var myScroll2 = new IScroll('#scrollbar2', { scrollbars: 'custom', interactiveScrollbars: true });
    }

    function redirectToHomePage() {
    	  	$.ajax({
	            url: '/.store-kiosk/store/redemption/unlock-pin',
	            data:  '',
	            headers: { 'Store-Api-Channel' : '${channel}' },
	            type: 'POST',
	            cache: false,
	            dataType: 'json',
	            contentType: 'application/json',
	            success: function (data) {
					window.location.href='${sitePath}';
	            },
	            error: function () {
 					window.location.href='${sitePath}';
	            },
	            complete: function () {
	            }
	        });
       
    }
    
    $(document).ready(fetchShoppingCartItems());

</script>

<div class="main box-wrapper top-center">
    <div class="main-content">
        <h1 class="primary-heading-1 list-title">${staticText[lang]['orderSummaryTitle']}</h1>

        <div class="fun-cart-content">
            <div class="cart-box ticket-container summary-dialog" ng-class="{'summary-dialog-1':localPreReceiptType=='Recovery' || localPreReceiptType=='B2C','summary-dialog':localPreReceiptType!='Recovery' &amp;&amp; localPreReceiptType!='B2C'}">
                <h3>${staticText[lang]['shoppingCartSubTitle']}</h3>
                <table cellpadding="0" cellspacing="0" border="0" width="97%" class="table-title">
                    <tr>
                        <td width="40%">${staticText[lang]['shoppingCartTitleItem']}</td>
                        <td width="30%" class="center">${staticText[lang]['shoppingCartTitleValidity']}</td>
                        <td width="30%" class="center">${staticText[lang]['shoppingCartTitleQuantity']}</td>
                  
                    </tr>
                </table>
                <div class="tab-content-container fif-template">
                    <div id="scrollbar2" style="height:490px; width:97%">
                        <div class="scroller">
                            <ul>
                                <li>
                                    <table cellpadding="0" cellspacing="0" border="0" width="95%" class="table-content-cart">
                                        <tr>
                                            <td>
                                                <table id="shoppingCartItems" cellpadding="0" cellspacing="0" border="0" width="100%" class="inner-content">

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="cart-group-btn box-wrapper center-center">
                        <button type="button" class="btn btn-third btn-md" onclick="redirectToHomePage()">${staticText[lang]['cancelButtonLabel']}</button>
                        <button type="button" class="btn btn-primary btn-md" onclick="redirectToReceiptPage()">${staticText[lang]['printTicketButtonLabel']}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
