[#include "/kiosk-slm/templates/macros/pageInit.ftl"]
[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign categoryList = kioskfn.getCMSProductCategories(channel, true, true)]
[#assign categoryListSize = categoryList?size]
<script>

    getKioskMaintenanceModeFromCMS();


    function redirectToScanEnterBarcodePage() {
        window.location.href = "${sitePath}/scan-enter-barcode";
    }

    function getKioskMaintenanceModeFromCMS() {

        $.ajax({
            url : '/.store-kiosk/ops-data/get-operation-mode-cms',
            data : 'mode=operation',
            headers : {
                'Store-Api-Channel' : "${channel}"
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                console.log(result);
                if (result.success) {
                    if (!result.data.status) {
                        console.log(result.data.status);
                        var scheduledTimeOut = result.data.numberOfSeconds;
                        redirectToMaintenancePageCms(scheduledTimeOut);

                    }else {
                        getKioskMaintenanceMode();
                    }
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });

    }

    function getKioskMaintenanceMode() {

        $.ajax({
            url : '/.store-kiosk/ops-data/get-operation-mode',
            data : 'mode=operation',
            headers : {
                'Store-Api-Channel' : "${channel}"
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                if (result.success) {
                    if (result.data == 'false') {
                        redirectToMaintenancePage();
                    }
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });

    }

    function moveToOpsAdmin(){
        var throttle = false;
        var classname = document.getElementsByClassName("btn-goto-ops-from-kiosk");

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', function (evt) {
                var o = this,
                        ot = this.textContent;

                if (!throttle && evt.detail === 3) {
                    redirectToOpsAdminPage();
                    throttle = true;
                    setTimeout(function () {
                        o.textContent = ot;
                        throttle = false;
                    }, 1000);
                }
            });
        }
    }

    function redirectToOpsAdminPage() {
        var channel = "${channel}";
        window.location.href = "${opsPath}?channel="+channel;
    }

</script>

<div class="main-menu">
    <h1 ng-bind-html="centerText.t" class="ng-binding">
    [#if content.selectTicketText?has_content] ${content.selectTicketText} [/#if]
    </h1>

[#list categoryList as category]

    [#if category.hasProperty("iconImage")]
        [#assign categoryImage = category.getProperty("iconImage").getString()]
    [/#if]

    [#if categoryImage?has_content]
        [#assign thumbnailAssetLink = damfn.getAssetLink(categoryImage)]
    [/#if]

    [#if category.hasProperty("transportLayout")]
        [#assign transportLayout = category.getProperty("transportLayout").getString()]
    [#else]
        [#assign transportLayout = "false"]
    [/#if]

    [#if transportLayout == "true" ]
        <a id="btnAttractions" class="menu-icon ${category.getProperty("categoryLayoutCSS").getString()}" href="${sitePath}/admission~${category.getName()}~" style="margin-top: 0px; margin-left: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>
    [#else]
        <a id="btnAttractions" class="menu-icon ${category.getProperty("categoryLayoutCSS").getString()}" href="${sitePath}/category~${category.getName()}~?transportLayout=${transportLayout}" style="margin-top: 0px; margin-left: 0px;">
            <img [#if thumbnailAssetLink?has_content] src="${thumbnailAssetLink}" [#else] src='' [/#if] />
            <span class="menu-title ng-binding">${category.getProperty("name").getString()}</span>
        </a>
    [/#if]

[/#list]
</div>

<div class="btn-goto-ops-from-kiosk"  onclick="moveToOpsAdmin()">
</div>


<div class="bottom-barcode center-center">
    <button type="button" class="btn btn-lg btn-primary ng-binding" onclick="redirectToScanEnterBarcodePage()">
    ${staticText[lang]['barcodeLabel']}
    </button>
</div>
<script>
/**
    $(document).ready(function() {

        $.ajax({
            url : '/.store-kiosk/store/logon',
            data : '',
            headers : {
                'Store-Api-Channel' : '${channel}'
            },
            type : 'POST',
            cache : false,
            dataType : 'json',
            contentType : 'application/json',
            success : function(result) {
                if (result.success) {

                }else {
                    redirectToErrorPage();
                }
            },
            error : function() {
                redirectToErrorPage();
            }
        });


    })
   **/
</script>