<div id="keyboardContainer" class="keyboard-container" style="display: none;">
    <div class="keyboard-content">
        <div class="btn-close">
            <button type="button" onclick="hideKeyboard()"></button>
        </div>
        <div class="character-side">
            <ul class="keyboard-controller box-wrapper center-center">
                <li><button type="button" onclick="inputCharacter('Q')">Q</button></li>
                <li><button type="button" onclick="inputCharacter('W')">W</button></li>
                <li><button type="button" onclick="inputCharacter('E')">E</button></li>
                <li><button type="button" onclick="inputCharacter('R')">R</button></li>
                <li><button type="button" onclick="inputCharacter('T')">T</button></li>
                <li><button type="button" onclick="inputCharacter('Y')">Y</button></li>
                <li><button type="button" onclick="inputCharacter('U')">U</button></li>
                <li><button type="button" onclick="inputCharacter('I')">I</button></li>
                <li><button type="button" onclick="inputCharacter('O')">O</button></li>
                <li><button type="button" onclick="inputCharacter('P')">P</button></li>
                <li><button type="button" onclick="inputCharacter('@')">@</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center">
                <li><button type="button" onclick="inputCharacter('A')">A</button></li>
                <li><button type="button" onclick="inputCharacter('S')">S</button></li>
                <li><button type="button" onclick="inputCharacter('D')">D</button></li>
                <li><button type="button" onclick="inputCharacter('F')">F</button></li>
                <li><button type="button" onclick="inputCharacter('G')">G</button></li>
                <li><button type="button" onclick="inputCharacter('H')">H</button></li>
                <li><button type="button" onclick="inputCharacter('J')">J</button></li>
                <li><button type="button" onclick="inputCharacter('K')">K</button></li>
                <li><button type="button" onclick="inputCharacter('L')">L</button></li>
                <li><button type="button" onclick="inputCharacter('#')">#</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center">
                <li><button type="button" onclick="inputCharacter('Z')">Z</button></li>
                <li><button type="button" onclick="inputCharacter('X')">X</button></li>
                <li><button type="button" onclick="inputCharacter('C')">C</button></li>
                <li><button type="button" onclick="inputCharacter('V')">V</button></li>
                <li><button type="button" onclick="inputCharacter('B')">B</button></li>
                <li><button type="button" onclick="inputCharacter('N')">N</button></li>
                <li><button type="button" onclick="inputCharacter('M')">M</button></li>
                <li><button type="button" onclick="inputCharacter(',')">,</button></li>
                <li><button type="button" onclick="inputCharacter('.')">.</button></li>
                <div class="clearfix"></div>
            </ul>
            <ul class="keyboard-controller box-wrapper center-center">
                <li><button type="button" onclick="inputCharacter('$')">$</button></li>
                <li><button type="button" onclick="inputCharacter('!')">!</button></li>
                <li><button type="button" onclick="inputCharacter('&')">&amp;</button></li>
                <li><button type="button" onclick="inputCharacter('*')">*</button></li>
                <li><button type="button" onclick="inputCharacter('(')">(</button></li>
                <li><button type="button" onclick="inputCharacter(')')">)</button></li>
                <li><button type="button" onclick="inputCharacter('-')">-</button></li>
                <li><button type="button" onclick="inputCharacter('+')">+</button></li>
                <li><button type="button" onclick="inputCharacter('\'')">'</button></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="num-side">
            <ul class="keyboard-controller">
                <li><button type="button" onclick="inputCharacter('1')">1</button></li>
                <li><button type="button" onclick="inputCharacter('2')">2</button></li>
                <li><button type="button" onclick="inputCharacter('3')">3</button></li>
                <li><button type="button" onclick="inputCharacter('4')">4</button></li>
                <li><button type="button" onclick="inputCharacter('5')">5</button></li>
                <li><button type="button" onclick="inputCharacter('6')">6</button></li>
                <li><button type="button" onclick="inputCharacter('7')">7</button></li>
                <li><button type="button" onclick="inputCharacter('8')">8</button></li>
                <li><button type="button" onclick="inputCharacter('9')">9</button></li>
                <li><button type="button" class="btn-zero" onclick="inputCharacter('0')">0</button></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="clearfix"></div>
        <ul class="keyboard-controller-1 box-wrapper center-center">
            <li class="btn-space"><button type="button" class="clear" onclick="clearAllCharacters()">Clear</button></li>
            <li class="btn-space"><button type="button" class="space" onclick="inputCharacter(' ')">Space</button></li>
            <li class="btn-space"><button type="button" class="backspace" onclick="deletePreviousCharacter()">Backspace</button></li>
            <li class="btn-space"><button type="button" class="ok" onclick="submitKeyboard()">OK</button></li>
        </ul>
    </div>
</div>