<div id="number-keypad" style="z-index:1001; display: none" class="keypad-dialog">
    <div class="arrow-up"></div>
    <h2>Please Key In Quantity</h2>
    <ul class="keypad-controller">
        <li><button type="button" onclick="selectNumber(1)">1</button></li>
        <li><button type="button" onclick="selectNumber(2)">2</button></li>
        <li><button type="button" onclick="selectNumber(3)">3</button></li>
        <li><button type="button" onclick="selectNumber(4)">4</button></li>
        <li><button type="button" onclick="selectNumber(5)">5</button></li>
        <li><button type="button" onclick="selectNumber(6)">6</button></li>
        <li><button type="button" onclick="selectNumber(7)">7</button></li>
        <li><button type="button" onclick="selectNumber(8)">8</button></li>
        <li><button type="button" onclick="selectNumber(9)">9</button></li>
        <li><button type="button" onclick="selectNumber(0)">0</button></li>
        <div class="clearfix"></div>
    </ul>
    <div class="group-inner-btn keypad-group">
        <button type="button" class="btn btn-third btn-xs ng-binding" onclick="deleteNumber()">Delete</button>
        <button type="button" class="btn btn-primary btn-xs ng-binding" onclick="hideKeyboard()">OK</button>
    </div>
</div>