[#include "/kiosk-slm/templates/macros/staticText/header.ftl"]

[#assign lang = sitefn.site().getI18n().getLocale().toString()]
[#assign channel = 'kiosk-slm']
[#assign sitePath = "${ctx.contextPath}/${lang}/kiosk-slm"]
[#assign themePath = "${ctx.contextPath}/resources/kiosk-slm/theme/default"]
[#assign opsPath = "${ctx.contextPath}/${lang}/kiosk-ops"]
[#if lang == "en" || lang == "zh_CN"]
    [#assign imgSuffix = lang!"en"]
[#else]
    [#assign imgSuffix = "en"]
[/#if]

<script>
    setDate();
    setTime();
    setInterval(function () {
        setDate("${lang}");
        setTime("${lang}");

        $('#todayDate').html(window.today['date']);
        $('#todayTime').html("|&nbsp;" + window.today['time']);
    }, 1000);

    function redirectToErrorPage() {
        window.location.href = "${sitePath}/error";
    }

    function redirectToMaintenancePage() {
        window.location.href = "${sitePath}/maintenance";
    }

    function redirectToMaintenancePageCms(time) {
        window.location.href = "${sitePath}/maintenance?time="+time;
    }

    function redirectToHomePage(){

        window.location.href = "${sitePath}";
    }

    function showAlertDialog(alertTitle, alertMessage) {

        $("#alertTitle").text(alertTitle);
        $("#alertMessage").text(alertMessage);
        $("#alert-dialog").show();
    }

    function showInfoDialog(infoTitle, infoMessage) {

        $("#infoTitle").text(infoTitle);
        $("#infoMessage").text(infoMessage);
        $("#info-dialog").show();
    }

    function hideAlertDialog() {
        $("#alert-dialog").hide();
    }

    function hideInfoDialog() {
        $("#info-dialog").hide();
    }
    function openLoadingPage(){
        $('body').loadingModal({
            position: 'auto',
            text: '',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'wave'
        });
    }

    function closeLoadingPage(){
        $('body').loadingModal('destroy');
    }

    function destroyLoadingPage(){
        $('body').loadingModal('destroy');
    }

</script>

<div id="alert-dialog" class="dialog-container box-wrapper center-center" style="display: none;">
    <div class="alert-container">
        <div style="z-index:-1" class="dialog-container"></div>
        <h2 class="alert-title" id="alertTitle"></h2>
        <p id="alertMessage"></p>
        <div class="group-inner-btn">
            <button type="button" class="btn btn-primary btn-md" onclick="hideAlertDialog()">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>

<div id="info-dialog" class="dialog-container box-wrapper center-center" style="display: none;">
    <div class="alert-container" style="border: 4px solid rgb(83, 175, 50);">
        <div style="z-index:-1" class="dialog-container"></div>
        <h2 class="alert-title" id="infoTitle" style="background-color: rgb(83, 175, 50);"></h2>
        <p id="infoMessage"></p>
        <div class="group-inner-btn">
            <button id="okButton" type="button" class="btn btn-primary btn-md">${staticText[lang]['okButtonLabel']}</button>
        </div>
    </div>
</div>