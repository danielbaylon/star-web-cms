[#if component?has_content]
    [@cms.component content=component /]
[/#if]