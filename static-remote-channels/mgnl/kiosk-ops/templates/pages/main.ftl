[#-------------- RENDERING --------------]
<!DOCTYPE html>
<head>
[@cms.page/]

    [@cms.area name="htmlHeader"/]
</head>
<body>

[@cms.area name="content"/]

</body>