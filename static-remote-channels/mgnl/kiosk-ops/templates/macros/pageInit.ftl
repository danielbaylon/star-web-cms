[#include "/kiosk-ops/templates/macros/staticText/header.ftl"]

[#assign lang = sitefn.site().getI18n().getLocale().toString()]
[#assign channel = 'kiosk-slm']
[#assign sitePath = "${ctx.contextPath}/${lang}/kiosk-ops"]
[#assign themePath = "${ctx.contextPath}/resources/kiosk-ops/theme/default"]
[#if lang == "en" || lang == "zh_CN"]
    [#assign imgSuffix = lang!"en"]
[#else]
    [#assign imgSuffix = "en"]
[/#if]

<script>

    function redirectToErrorPage() {
        window.location.href = "${sitePath}/error";
    }

    function showAlertDialog(alertTitle, alertMessage) {

        $("#alertTitle").text(alertTitle);
        $("#alertMessage").text(alertMessage);
        $("#alert-dialog").show();
    }

    function hideAlertDialog() {
        $("#alert-dialog").hide();
    }

//    function showKeyboard(targetSource) {
//
//        var offset = targetSource.offset();
//
//        $("#admin-keyboard").css('top', offset.top + 53);
//        $("#admin-keyboard").css('left', offset.left - 290);
//
//        $("#admin-keyboard").show();
//    }

    function hideKeyboard() {
        $("#admin-keyboard").hide();
    }
</script>

<div id="admin-keyboard" style="display: block; display: none;">
    <div class="keyboard-container" style="opacity: 1;">
        <div class="keyboard-spare"></div>
        <div class="keyboard-content special">
            <div class="character-side">
                <ul class="keyboard-controller box-wrapper center-center">
                    <li><button id="key-0" type="button" onclick="adminKeyTapped(0)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-q.png&quot;);"></button></li>
                    <li><button id="key-1" type="button" onclick="adminKeyTapped(1)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-w.png&quot;);"></button></li>
                    <li><button id="key-2" type="button" onclick="adminKeyTapped(2)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-e.png&quot;);"></button></li>
                    <li><button id="key-3" type="button" onclick="adminKeyTapped(3)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-r.png&quot;);"></button></li>
                    <li><button id="key-4" type="button" onclick="adminKeyTapped(4)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-t.png&quot;);"></button></li>
                    <li><button id="key-5" type="button" onclick="adminKeyTapped(5)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-y.png&quot;);"></button></li>
                    <li><button id="key-6" type="button" onclick="adminKeyTapped(6)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-u.png&quot;);"></button></li>
                    <li><button id="key-7" type="button" onclick="adminKeyTapped(7)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-i.png&quot;);"></button></li>
                    <li><button id="key-8" type="button" onclick="adminKeyTapped(8)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-o.png&quot;);"></button></li>
                    <li><button id="key-9" type="button" onclick="adminKeyTapped(9)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-p.png&quot;);"></button></li>
                    <div class="clearfix"></div>
                </ul>
                <ul class="keyboard-controller box-wrapper center-center">
                    <li><button id="key-10" type="button" onclick="adminKeyTapped(10)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-a.png&quot;);"></button></li>
                    <li><button id="key-11" type="button" onclick="adminKeyTapped(11)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-s.png&quot;);"></button></li>
                    <li><button id="key-12" type="button" onclick="adminKeyTapped(12)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-d.png&quot;);"></button></li>
                    <li><button id="key-13" type="button" onclick="adminKeyTapped(13)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-f.png&quot;);"></button></li>
                    <li><button id="key-14" type="button" onclick="adminKeyTapped(14)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-g.png&quot;);"></button></li>
                    <li><button id="key-15" type="button" onclick="adminKeyTapped(15)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-h.png&quot;);"></button></li>
                    <li><button id="key-16" type="button" onclick="adminKeyTapped(16)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-j.png&quot;);"></button></li>
                    <li><button id="key-17" type="button" onclick="adminKeyTapped(17)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-k.png&quot;);"></button></li>
                    <li><button id="key-18" type="button" onclick="adminKeyTapped(18)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-l.png&quot;);"></button></li>
                    <div class="clearfix"></div>
                </ul>
                <ul class="keyboard-controller box-wrapper center-center">
                    <li><button id="key-19" type="button" class="key-btn-1" onclick="adminShiftTapped()" style="border-style: none; height: 60px; width: 116px; background-image: url(&quot;${themePath}/images/admin/keyboard/number/caps-lock.png&quot;);"></button></li>
                    <li><button id="key-20" type="button" onclick="adminKeyTapped(20)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-z.png&quot;);"></button></li>
                    <li><button id="key-21" type="button" onclick="adminKeyTapped(21)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-x.png&quot;);"></button></li>
                    <li><button id="key-22" type="button" onclick="adminKeyTapped(22)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-c.png&quot;);"></button></li>
                    <li><button id="key-23" type="button" onclick="adminKeyTapped(23)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-v.png&quot;);"></button></li>
                    <li><button id="key-24" type="button" onclick="adminKeyTapped(24)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-b.png&quot;);"></button></li>
                    <li><button id="key-25" type="button" onclick="adminKeyTapped(25)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-n.png&quot;);"></button></li>
                    <li><button id="key-26" type="button" onclick="adminKeyTapped(26)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/lowercase/lowercase-m.png&quot;);"></button></li>
                    <li><button id="key-27" type="button" class="key-btn-1" onclick="adminBackSpaceTapped()" style="border-style: none; height: 60px; width: 116px; background-image: url(&quot;${themePath}/images/admin/keyboard/number/del.png&quot;);"></button></li>
                    <div class="clearfix"></div>
                </ul>
                <ul class="keyboard-controller box-wrapper center-center">
                    <li><button id="key-28" type="button" class="key-btn-1" onclick="adminNumberTapped()" style="border-style: none; height: 60px; width: 116px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/number.png&quot;);"></button></li>
                    <li><button id="key-29" type="button" onclick="adminKeyTapped(29)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/backslash.png&quot;);"></button></li>
                    <li><button id="key-30" type="button" onclick="adminKeyTapped(30)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/comma.png&quot;);"></button></li>
                    <li><button id="key-31" type="button" class="key-btn-2" onclick="adminKeyTapped(31)" style="border-style: none; height: 60px; width: 259px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/space.png&quot;);"></button></li>
                    <li><button id="key-32" type="button" onclick="adminKeyTapped(32)" style="border-style: none; height: 60px; width: 60px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/period.png&quot;);"></button></li>
                    <li><button id="key-33" type="button" class="key-btn-1" onclick="adminGo()" style="border-style: none; height: 60px; width: 146px; background-image: url(&quot;${themePath}/images/admin/keyboard/common/go.png&quot;);"></button></li>
                    <div class="clearfix"></div>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script>
    function adminShiftTapped() {
        adminKeyboard.shiftTapped();
    }
    function adminNumberTapped() {
        adminKeyboard.numberTapped();
    }
    function adminKeyTapped(index) {
        adminKeyboard.keyTapped(index);
    }
    function adminBackSpaceTapped() {
        adminKeyboard.backSpaceTapped();
    }
    function adminGo() {
        adminKeyboard.go();
        adminKeyboard.limitType = 'time';
    }
</script>