[#include "/kiosk-ops/templates/macros/pageInit.ftl"]
[#include "/kiosk-ops/templates/macros/staticText/header.ftl"]
<script>var themePath = "${themePath}"; var contextPath = "${ctx.contextPath}"; var sitePath = "${sitePath}";</script>

<script>



    $(document).ready(function() {

        $(document).click(function(e) {

            var keyboardContainer = $("#admin-keyboard");



            if(e.target.id != undefined && e.target.id.startsWith("inputUsr")) {
                return;
            }

            if(e.target.id != undefined && e.target.id.startsWith("inputPsw")) {
                return;
            }

            if (!keyboardContainer.is(e.target) && keyboardContainer.has(e.target).length === 0) {
                hideKeyboard();
            }
        });
    });

    function goToDetailPage() {
        window.location.href = "${sitePath}/detail";
    }




</script>

<div id="admin-login">
    <div class="main-container">
        <div class="sub-container">
            <div class="content-container">
                <div class="header-container">
                    <div class="header">Administrator Login</div>
                    <div class="space"></div>
                </div>
                <div class="login-container">
                    <div class="login-sub-container">
                        <div class="error-msg">&nbsp;</div>
                        <div class="input-container">
                            <span class="col-side"></span>
                            <span class="label">User Name:</span>
                            <input id="inputUsr" name="inputUsr" onclick="adminUsrGotFocus()" />
                            <span class="col-side"></span>
                        </div>
                        <div class="input-container">
                            <span class="col-side"></span>
                            <span class="label">Password:</span>
                            <input id="inputPsw" name="inputPsw" type="password" onclick="adminPswGotFocus()" />
                            <span class="col-side"></span>
                        </div>
                        <div class="btn-group">
                            <div class="btn-cancel-container">
                                <button class="btn-cancel" onclick="adminCancel()"></button>
                            </div>
                            <div class="btn-login-container">
                                <button class="btn-login" onclick="doLoginPost()"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var editingUsr = true;
    var editingPsw = false;

    function adminUsrGotFocus() {
        editingUsr = true;
        editingPsw = false;
        adminKeyboard.limitType = undefined;
        formType=1;
        var targetSource = $("#inputUsr");
        showKeyboard(targetSource);
    }
    function adminPswGotFocus() {
        editingPsw = true;
        editingUsr = false;
        adminKeyboard.limitType = undefined;
        formType=1;
        var targetSource = $("#inputPsw");
        showKeyboard(targetSource);
    }
    function adminCancel() {
        editingUsr = false;
        editingPsw = false;
        $("#inputUsr").val('');
        $("#inputPsw").val('');
        window.location.href = contextPath+"/kiosk-slm?mgnlLogout";
    }
    function adminLogin() {
        editingUsr = false;
        editingPsw = false;
        //admin.showPanel();
        //adminKeyboard.limitType = 'time';
        $('.error-msg').html('&nbsp;');
        var usrId = $('#inputUsr').val();
        var psw = $('#inputPsw').val();
        $('#admin-dialog-loading').show();
        appfunc.opsLogin(usrId, psw, function (response) {
            $('#admin-dialog-loading').hide();
            if (response.IsLoginSucceed == true) {
                //console.log(JSON.stringify(response));
                // Allow user with role OpsAdmin-32 or OpsSupervisor-64 to login
                if ($.inArray(32, response.Roles) > -1 || $.inArray(64, response.Roles) > -1) {
                    admin.userId = response.UserId;
                    admin.userRoles = response.Roles;
                    console.log('User ID: ' + admin.userId);
                    admin.showPanel();
                    //adminKeyboard.limitType = 'time';
                } else {
                    $('.error-msg').html("You are not authorized to perform this operation");
                }
            } else {
                $('.error-msg').html(response.Message);
            }
        }, function (errorMessage) {
            $('#admin-dialog-loading').hide();
            $('.error-msg').html(errorMessage);
        });
    }
</script>