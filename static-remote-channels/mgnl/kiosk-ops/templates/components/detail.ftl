[#include "/kiosk-ops/templates/macros/pageInit.ftl"]
[#include "/kiosk-ops/templates/macros/staticText/header.ftl"]
<script>var themePath = "${themePath}"; var contextPath = "${ctx.contextPath}";</script>
<script src="${themePath}/js/counterparty.js${urlVersion}"></script>
<script src="${themePath}/js/picker.js${urlVersion}"></script>
<script src="${themePath}/js/picker.date.js${urlVersion}"></script>
<script src="${themePath}/js/picker.time.js${urlVersion}"></script>
<script src="${themePath}/js/flashcanvas.js${urlVersion}"></script>
<script src="${themePath}/js/jSignature.min.js${urlVersion}"></script>

<div id="admin-panel" style="display: block;">
    <div class="main-container">
        <div class="sub-container">
            <div id="date-picker"></div>
            <div class="tab-container">
                <div class="tabs">
                    <button id="tab-0" class="tab"
                            style="border: 1px solid white; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">
                        <span>Receipt History</span>
                    </button>
                    <button id="tab-10" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Transaction Status</span>
                    </button>
                    <button id="tab-1" class="tab" onclick="adminResetPaper()"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Receipt Printer</span>
                    </button>
                    <button id="tab-11" class="tab" onclick="getHardwareStatus()"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Hardware Status</span>
                    </button>
                    <!-- <button id="tab-2" class="tab"
                             style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                         <span>Load RFID Ticket</span></button>-->
                    <button id="tab-3" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Load Paper Ticket</span></button>
                    <button id="tab-4" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Servicing</span></button>
                    <button id="tab-5" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Collection Of Rejected Ticket</span></button>
                    <button id="tab-6" class="tab" onclick="getSettlementInfo()"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Kiosk Shutdown / Settlement</span></button>
                    <button id="tab-7" class="tab" onclick="getKioskMaintenanceMode()"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Operation Mode</span></button>
                    <button id="tab-8" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Others</span></button>
                    <button id="tab-9" class="tab"
                            style="border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);">
                        <span>Logout</span></button>
                </div>
            </div>
            <div id="content-0" class="content">
                <div class="search-container">
                    <div class="search-field-container">
                        <div>Select Date:</div>
                        <input class="date-input" id="txt-select-date-receipt" type="text" placeholder="yyyy-mm-dd"
                               onclick="pickDate(this)"/>
                    </div>
                    <div class="search-field-container">
                        <div>Start Time:</div>
                        <div class="input-group clockpicker">
                            <input id="txt-start-time-receipt" type="text" placeholder="00:00" class="form-control" onclick="pickTime(this)"
                                   value="">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="search-field-container">
                        <div>End Time:</div>
                        <div class="input-group clockpicker">
                            <input id="txt-end-time-receipt" type="text" placeholder="23:59" class="form-control" onclick="pickTime(this)"
                                   value="">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="btn-search-container">
                        <button class="btn-search" style="display: inline;" onclick="adminSearchReceiptHistory()"></button>
                    </div>
                </div>
                <div class="history-container">
                    <div class="space-column-side"></div>
                    <div class="history-column-main">
                        <h3>Receipt History</h3>
                        <div id="trx-grid" class="grid-container">
                            <div class="loading" style="display:none" id="table-receipt-loading">Loading...</div>
                            <div class="simple-grid" id="table-pagination-receipt">
                                <!-- <table class="table-transaction"> <tr class="table-transaction-tr"><th class="table-transaction-th">Trans ID</th><th class="table-transaction-th">Status</th></tr></table> -->
                            </div>
                            <div class="pagination" id="btn-pagination-receipt" style="display: none;">
                                <div class="btn-container">
                                    <button class="btn-receipt-first" id="btn-receipt-first"
                                            onclick="setReceiptPagination(goToFirstReceiptPage())"></button>
                                </div>
                                <div class="btn-container">
                                    <button class="btn-receipt-prev" id="btn-receipt-prev"
                                            onclick="setReceiptPagination(getPrevReceiptPageNumber())"></button>
                                </div>
                                <div class="btn-container">
                                    <button class="btn-receipt-next" id="btn-receipt-next"
                                            onclick="setReceiptPagination(getNextReceiptPageNumber())"></button>
                                </div>
                                <div class="btn-container">
                                    <button class="btn-receipt-last" id="btn-receipt-last"
                                            onclick="setReceiptPagination(goToLastReceiptPage())"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-column-mid"></div>
                    <div class="history-column-main">
                        <h3>Receipt Preview</h3>
                        <div class="preview">
                            <div class="no-document-container">
                                <!-- <div class="no-document">Document does not contain any pages</div> -->
                                <div id="" style="overflow-y: scroll; height:600px; font-size: 15px;">
                                    <div >
                                        <span id="receipt-company-logo"></span>
                                    </div>
                                    <p id="receipt-company-name" class="p-line-height"> </P>
                                    <p id="receipt-company-address" class="p-line-height"> </P>
                                    <p id="receipt-company-postal" class="p-line-height"> </P>
                                    <p id="receipt-company-contact" class="p-line-height"> </P>
                                    <p id="receipt-company-gst" class="p-line-height"> </P>
                                    <p class="p-line-height" id="receipt-dash-line-1">
                                        ----------------------------------------------------------------------------</P>

                                    <table class="receipt-header-table">
                                        <tr>
                                            <td> Location :</td>
                                            <td id="receipt-location"> </td>
                                        </tr>
                                        <tr>
                                            <td> Date :</td>
                                            <td id="receipt-date"></td>
                                        </tr>
                                        <tr>
                                            <td> Kiosk ID :</td>
                                            <td id="receipt-kiosk"> </td>
                                        </tr>
                                        <tr>
                                            <td> Trans ID :</td>
                                            <td id="receipt-tran-id"> </td>
                                        </tr>
                                        <tr>
                                            <td> Receipt No :</td>
                                            <td id="receipt-receipt-no"> </td>
                                        </tr>

                                    </table>

                                    <p class="p-line-height" id="receipt-dash-line-2">
                                        ----------------------------------------------------------------------------</P>

                                    <table id ="receipt-header-table-2" class="receipt-header-table">
                                        <tr>
                                            <th>Description</th>
                                            <th>Qty</th>
                                            <th>S$</th>
                                        </tr>
                                        <tr id="ticket-details">
                                            <td colspan="3" class="receipt-header-table-title" id="receipt-dash-line-3">
                                                -------------------------------------------------------------------------
                                            </td>

                                        </tr>

                                    [#--<tr>    <td class="receipt-product-tr" id="receipt-cms-name" style="font-weight:bold">{{productCMS}}--]

                                    [#--</tr>--]
                                    [#--<tr>--]
                                    [#--<td id="receipt-ax-name">{{productAX}}</td>--]
                                    [#--<td id="receipt-ax-qty">{{qty}}</td>--]
                                    [#--<td class="receipt-table-currency" id="receipt-ax-price">{{price}}</td>--]
                                    [#--</tr>--]
                                    [#--<tr>--]
                                    [#--<td id="receipt-ax-sub-name">{{productName}}</td>--]
                                    [#--</tr>--]
                                        <tr class="receipt-product-tr" style="padding-bottom: 10px;padding-top: 10px;">
                                        <td colspan="2">Sub Total :</td>
                                        <td class="receipt-table-currency" id="receipt-subtotal"></td>
                                    </tr>

                                        <tr>
                                            <td colspan="3" class="receipt-header-table-title" id="receipt-dash-line-4">
                                                -------------------------------------------------------------------------
                                            </td>

                                        </tr>
                                        <tr>
                                            <td> TOTAL S$</td>
                                            <td></td>
                                            <td class="receipt-table-currency" id="receipt-total"> </td>
                                        </tr>
                                        <tr>
                                            <td>(Inclusive of GST)</td>

                                        </tr>
                                        <tr>
                                            <td>GST 7%</td>
                                            <td></td>
                                            <td class="receipt-table-currency" id="receipt-qst"> </td>
                                        </tr>
                                    </table>

                                    <p class="p-line-height" id="receipt-dash-line-5">
                                        ----------------------------------------------------------------------------</P>


                                    <p class="p-line-height" id="payment-receipt-header"> Payment Receipt </P>

                                    <table id ="receipt-header-table-3" class="receipt-header-table">
                                        <tr>
                                            <td colspan="2"> TID:</td>
                                            <td id="receipt-tid"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"> MID:</td>
                                            <td id="receipt-mid"> </td>
                                        </tr>
                                        <tr id="receipt-batch-row">
                                            <td colspan="2" id="receipt-batch-lab"> Batch#:</td>
                                            <td id="receipt-batch"> </td>
                                        </tr>
                                        <tr id="receipt-invoice-row">
                                            <td colspan="2" id="receipt-invoice-lab"> Invoice#:</td>
                                            <td id="receipt-invoice"> </td>
                                        </tr>
                                        <tr id="receipt-approval-code-row">
                                            <td  colspan="2" id="receipt-approval-code-lab"> Approval Code:</td>
                                            <td id="receipt-approval-code"> </td>
                                        </tr>
                                        <tr id="receipt-rrn-row">
                                            <td colspan="2"  id="receipt-rrn-lab"> RRN:</td>
                                            <td id="receipt-rrn"> </td>
                                        </tr>
                                        <tr id="receipt-merchant-row">
                                            <td  colspan="2" id="receipt-merchant-lab"> Merchant:</td>
                                            <td id="receipt-merchant"> </td>
                                        </tr>
                                        <tr id="receipt-entry-mode-row">
                                            <td colspan="2" id="receipt-entry-mode-lab"> Entry Mode:</td>
                                            <td id="receipt-entry-mode"> </td>
                                        </tr>
                                        <tr id="receipt-tvr-row">
                                            <td colspan="2" id="receipt-tvr-lab"> TVR:</td>
                                            <td id="receipt-tvr"> </td>
                                        </tr>
                                        <tr id="receipt-aid-row">
                                            <td colspan="2" id="receipt-aid-lab"> AID:</td>
                                            <td id="receipt-aid"> </td>
                                        </tr>
                                        <tr id="receipt-applabel-row">
                                            <td colspan="2" id="receipt-applabel-lab"> App Label:</td>
                                            <td id="receipt-applabel"> </td>
                                        </tr>
                                        <tr id="receipt-tc-row">
                                            <td colspan="2" id="receipt-tc-lab"> TC:</td>
                                            <td id="receipt-tc"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" > Payment Mode:</td>
                                            <td id="receipt-payment-mode"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"> Card Label:</td>
                                            <td id="receipt-card-label"> </td>
                                        </tr>
                                        <tr id="receipt-card-no-row">
                                            <td colspan="2" id="receipt-card-no-lab"> Card No:</td>
                                            <td id="receipt-card-no"> </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <div id="signature-header">
                                        <div id="signature"></div>
                                        <p class="p-line-height" id="receipt-cardhoder_name" align="left" style="margin-left:30px; margin-bottom:20px;">  </P>
                                        <p class="p-line-height" id="receipt-signature">  </P>
                                        <br/>
                                        <p id="agreement-header"> I AGREE TO PAY ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT. </P>
                                    </div>
                                    <p class="p-line-height" id="receipt-dash-line-6">
                                        ----------------------------------------------------------------------------</P>

                                    <p class="p-line-height"> Ticket Issuance Status </P>

                                    <p class="p-line-height" id="receipt-dash-line-7">
                                        ----------------------------------------------------------------------------</P>
                                    <p class="p-line-height" id="receipt-ticket-issuance-status" style="font-weight: bold;">  </P>
                                    <p class="p-line-height" id="receipt-ticket-issuance-msg1">  </P>
                                    <br/>
                                    <p class="p-line-height" id="receipt-ticket-issuance-msg2">  </P>
                                    <p class="p-line-height" id="receipt-ticket-issuance-msg3">  </P>
                                    <p class="p-line-height" id="receipt-ticket-issuance-msg4">  </P>
                                    <p class="p-line-height" id="receipt-dash-line-8">
                                        ----------------------------------------------------------------------------</P>
                                    <div id="qr_code_image">
                                        <span  class="qr_code-receipt" ></span>
                                    </div>
                                    <p class="p-line-height" id="receipt-footer-receipt-id">  </P>
                                    <p class="p-line-height"> Thank you. </P>
                                    <p class="p-line-height"> Please come again! </P>
                                    <p class="p-line-height"> Have a nice day. </P>

                                </div>
                            </div>
                            <div class="document-container">
                                <div class="document"><img src=""/></div>
                            </div>
                            <div class="reprint-container">
                                <button class="btn-reprint" onclick="rePrintReceipt()"></button>
                            </div>
                        </div>
                    </div>
                    <div class="space-column-side"></div>
                </div>
            </div>
            <div id="content-10" class="content zoom" style="display: none;">
                <table width="100%">
                    <tr height="40px">
                        <td width="20%"><span>Select Date</span></td>
                        <td width="60%"><input class="date-input" id="txt-select-date" type="text"
                                               placeholder="yyyy-mm-dd"
                                               onclick="pickDate(this)"/></td>
                    </tr>
                    <tr height="40px">
                        <td width="20%"><span>Start Time</span></td>
                        <td width="60%">
                            <div class="input-group clockpicker">
                                <input id="txt-start-time" type="text" placeholder="00:00" class="form-control" onclick="pickTime(this)"
                                       value="">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr height="40px">
                        <td width="20%"><span>End Time</span></td>
                        <td width="60%">
                            <div class="input-group clockpicker">
                                <input id="txt-end-time" type="text" placeholder="23:59" class="form-control" onclick="pickTime(this)" value="">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr height="40px">
                        <td width="20%"><span>Trans ID</span></td>
                        <td width="60%"><input class="long-input" id="txt-trans-id" type="text"
                                               placeholder="XMTK123456T1XXX7X89" onclick="adminInputGotFocus(this)"/></td>
                    </tr>
                    <tr height="40px">
                        <td width="20%"><span>PIN Code</span></td>
                        <td width="60%"><input class="long-input" id="txt-pin-code" type="text" width="200px"
                                               placeholder="X1XXXX2345XX67890123 or 1XXXXX"
                                               onclick="adminInputGotFocus(this)"/></td>
                    </tr>
                    <tr height="60px" class="search-container">
                        <td width="20%"></td>
                        <td width="60%">
                            <button class="btn-search" style="display: inline;" onclick="adminSearchTransactionStatus()"></button>
                        </td>
                    </tr>
                </table>
                <div id="trx-status-grid" class="grid-container">
                    <div class="loading" style="display:none" id="table-loading">Loading...</div>
                    <div class="simple-grid" id="table-pagination">
                        <!-- <table class="table-transaction"> <tr class="table-transaction-tr"><th class="table-transaction-th">Trans ID</th><th class="table-transaction-th">Status</th></tr></table> -->
                    </div>
                    <div class="pagination" id="btn-pagination" style="display: none;">
                        <div class="btn-container">
                            <button class="btn-tran-first" id="btn-tran-first"
                                    onclick="setTransactionPagination(goToFirstTransactionPage())"></button>
                        </div>
                        <div class="btn-container">
                            <button class="btn-tran-prev" id="btn-tran-prev"
                                    onclick="setTransactionPagination(getPrevTransactionPageNumber())"></button>
                        </div>
                        <div class="btn-container">
                            <button class="btn-tran-next" id="btn-tran-next"
                                    onclick="setTransactionPagination(getNextTransactionPageNumber())"></button>
                        </div>
                        <div class="btn-container">
                            <button class="btn-tran-last" id="btn-tran-last"
                                    onclick="setTransactionPagination(goToLastTransactionPage())"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content-1" class="content zoom" style="display: none;">
                <div>
                    <h3 class="header">Printer</h3>
                    <div class="printer" id="receipt-printer">
                        <div class="printer-status">No printer found.</div>
                    </div>
                </div>
                <div>
                    <button class="btn-reset" onclick="adminResetPaper()"></button>
                    <div class="btn-reset-info">Press this button "Reset Paper Near End Status" to reset the printer
                        paper near end state after refill the paper.
                    </div>
                </div>
                <div class="flex">
                    <div class="col-printer-details">
                        <h3 class="header">Printer Details</h3>
                        <div class="printer-details">
                            <div class="detail-container"><span class="detail-label">Printer</span><input
                                    id="inputPrinter" type="text" disabled/></div>
                            <!--<div class="detail-container"><span class="detail-label">Port</span><input id="inputPort"
                                                                                                       type="text"
                                                                                                       disabled/></div>-->
                            <div class="detail-container"><span class="detail-label">Firmware Release</span><input
                                    id="inputFireware" type="text" disabled/></div>
                            <div class="detail-container"><span class="detail-label">Head Temperature</span><input
                                    id="inputHeadTemperature" type="text" disabled/></div>
                            <div class="detail-container"><span class="detail-label">Voltage</span><input
                                    id="inputVoltage" type="text" disabled/></div>
                            <div class="detail-container"><span class="detail-label">Printed Paper</span><input
                                    id="inputPrintedPaper" type="text" disabled/></div>
                            <div class="detail-container"><span class="detail-label">Cut Counter</span><input
                                    id="inputCutCounter" type="text" disabled/></div>
                        </div>
                    </div>
                    <div class="col-mid-space"></div>
                    <div class="col-hardware-status">
                        <h3 class="header">Hardware Status</h3>
                        <div class="hardware-status">
                            <div class="label-group">
                                <p><img width="15" height="15" src="" id="pHeadOverHeated"/> Head Overheated</p>
                                <p><img width="15" height="15" src="" id="pIrregularEject"/> Irregular Eject</p>
                                <p><img width="15" height="15" src="" id="pPrintHeadUp"/> Print Head Up</p>
                            </div>
                            <div class="label-group">
                                <p><img width="15" height="15" src="" id="pCoverUp"/> Cover Up</p>
                                <p><img width="15" height="15" src="" id="pVoltageError"/> Voltage Error</p>
                                <p><img width="15" height="15" src="" id="pPaperJam"/> Paper Jam</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex">
                    <div class="col-paper-status">
                        <h3 class="header">Paper Status</h3>
                        <div class="paper-status">
                            <div class="label-group w40"><p><img width="15" height="15" src="" id="pNoPaper"/> No Paper
                            </p></div>
                            <div class="label-group"><p><img width="15" height="15" src="" id="pNearPaperEnd"/> Near
                                Paper End</p></div>
                        </div>
                    </div>
                    <div class="col-mid-space"></div>
                    <div class="col-error">
                        <h3 class="header">Unrecoverable Error</h3>
                        <div class="unrecoverable-error">
                            <div class="label-group"><p><img width="15" height="15" src="" id="pAutoCutterError"/> Auto
                                Cutter Error</p></div>
                            <div class="label-group w40"><p><img width="15" height="15" src="" id="pRAMError"/> RAM
                                Error</p></div>
                            <div class="label-group w45"><p><img width="15" height="15" src="" id="pEEPROMError"/>
                                EEPROM Error</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content-11" class="content zoom" style="display: none;">
                <table width="100%">
                    <tr height="80px" id="app-server-status" class="status-container">
                        <td width="27%"><span>App Server Connection</span></td>
                        <td width="8%"><img id="app-server-status" width="55" height="55" src=""/></td>
                        <td width="65%"><textarea id="app-server-txt" disabled></textarea></td>
                    </tr>
                    <!--<tr height="80px" id="RFID-printer-status" class="status-container">
                        <td width="27%"><span>RFID Printer</span></td>
                        <td width="8%"><img width="55" height="55" src=""/></td>
                        <td width="65%"><textarea disabled></textarea></td>
                    </tr>
                    <tr height="80px" id="RFID-encoder-status" class="status-container">
                        <td width="27%"><span>RFID Encoder</span></td>
                        <td width="8%"><img width="55" height="55" src=""/></td>
                        <td width="65%"><textarea disabled></textarea></td>
                    </tr>-->
                    <tr height="80px" id="paper-ticket-printer-status" class="status-container">
                        <td width="27%"><span>Paper Ticket Printer</span></td>
                        <td width="8%"><img id="ticket-printer-status" width="55" height="55" src=""/></td>
                        <td width="65%"><textarea id="ticket-printer-txt" disabled></textarea></td>
                    </tr>
                    <tr height="80px" id="paper-receipt-printer-status" class="status-container">
                        <td width="27%"><span>Paper Receipt Printer</span></td>
                        <td width="8%"><img id="receipt-printer-status" width="55" height="55" src=""/></td>
                        <td width="65%"><textarea id="receipt-printer-txt" disabled></textarea></td>
                    </tr>
                    <tr height="80px" id="paper-payment-terminal-status" class="status-container">
                        <td width="27%"><span>Payment Terminal</span></td>
                        <td width="8%"><img id="payment-terminal-status" width="55" height="55" src=""/></td>
                        <td width="65%"><textarea id="payment-terminal-txt" disabled></textarea></td>
                    </tr>
                    <tr height="80px" id="paper-robotic-arm-status" class="status-container">
                        <td width="27%"><span>Robotic Arm</span></td>
                        <td width="8%"><img id="robotic-arm-status" width="55" height="55" src=""/></td>
                        <td width="65%"><textarea id="robotic-arm-txt" disabled></textarea></td>
                    </tr>
                    <tr height="80px" id="paper-reject-bin-1-status" class="status-container" style="display:none">
                        <td width="27%"><span>Reject Bin 1</span></td>
                        <td width="8%"><img width="55" height="55" src=""/></td>
                        <td width="65%"><textarea disabled></textarea></td>
                    </tr>
                </table>
            </div>
            <!-- <div id="content-2" class="content zoom" style="display: none;">
                 <div class="status-box"><p class="status">Successfully saved.</p></div>
                 <div class="field-container"><span class="field-label-1">RFID Ticket Qty Unloaded</span><input
                         id="input-104" type="text" placeholder="numeric" onclick="adminInputGotFocus(this)"/></div>
                 <div class="unloaded-tickets" data-bind="visible: tickets().length > 0, foreach: tickets">
                     <div class="field-container unloaded-fields">
                         &nbsp;<span class="index-number" data-bind="text: $index()+1"></span>&nbsp;
                         <input type="text" class="number"
                                data-bind="attr: {id:'input-'+(10000+$index()*2)}, value: Start, uniqueName: true"
                                onclick="adminInputGotFocus(this)"/>
                         &nbsp;<span>to</span>&nbsp;
                         <input type="text" class="number"
                                data-bind="attr: {id:'input-'+(10000+$index()*2+1)}, value: End, uniqueName: true"
                                onclick="adminInputGotFocus(this)"/>
                     </div>
                 </div>
                 <div class="field-container"><span class="field-label-1">RFID Ticket Qty Loaded</span><input
                         id="input-103" type="text" placeholder="numeric" onclick="adminInputGotFocus(this)"/></div>
                 <div class="field-container"><span class="field-label-1">RFID Start Ticket No</span><input id="input-1"
                                                                                                            type="text"
                                                                                                            onclick="adminInputGotFocus(this)"/>
                 </div>
                 <div class="field-container"><span class="field-label-1">RFID End Ticket No</span><input id="input-2"
                                                                                                          type="text"
                                                                                                          onclick="adminInputGotFocus(this)"/>
                 </div>
                 <div class="btn-ok-container-1">
                     <div class="btn-ok-sub-container">
                         <button class="btn-ok" onclick="adminLoadRFIDTicketOK()"></button>
                     </div>
                 </div>
             </div>-->
            <div id="content-3" class="content zoom" style="display: none;">
                <div class="status-box"><p class="status">Successfully saved.</p></div>
                <div class="field-container"><span class="field-label-1">Paper Ticket Qty Unloaded</span><input
                        id="txt-ticket-qty-unloaded" type="text" placeholder="numeric" onclick="adminNumericInputGotFocus(this)"/></div>
                <div class="unloaded-tickets" id="unloaded-tickets-list">
                    <div class="field-container unloaded-fields" id="unloaded-fields1">
                        &nbsp;<span class="index-number">1</span>&nbsp;
                        <input type="text" id="txt-lpt-from-1" class="number" onclick="adminInputGotFocus(this)"/>
                        &nbsp;<span>to</span>&nbsp;
                        <input type="text" id="txt-lpt-to-1" class="number" onclick="adminInputGotFocus(this)"/>
                    </div>
                </div>
                <div class="field-container"><span class="field-label-1">Paper Ticket Qty Loaded</span><input
                        id="txt-ticket-qty-loaded" type="text" placeholder="numeric" onclick="adminNumericInputGotFocus(this)"/></div>
                <div class="field-container"><span class="field-label-1">Paper Start Ticket No</span><input
                        id="txt-ticket-start-no" type="text" onclick="adminInputGotFocus(this)"/></div>
                <div class="field-container"><span class="field-label-1">Paper End Ticket No</span><input
                        id="txt-ticket-end-no" type="text" onclick="adminInputGotFocus(this)"/></div>
                <div class="btn-ok-container-1">
                    <div class="btn-ok-sub-container">
                        <button class="btn-ok" onclick="addLoadPaperTickerInfo()"></button>
                    </div>
                </div>
            </div>
            <div id="content-4" class="content zoom" style="display: none;">
                <div class="status-box"><p class="status">Successfully saved.</p></div>
                <div class="field-container"><span class="field-label-2">Service Name</span><input id="txt-service"
                                                                                                   type="text"
                                                                                                   onclick="adminInputGotFocus(this)"/>
                </div>
                <div class="field-container"><span class="field-label-2">Description</span><textarea
                        id="txt-service-description" onclick="adminInputGotFocus(this)"></textarea></div>
                <div class="btn-ok-container-2">
                    <div class="btn-ok-sub-container">
                        <button class="btn-ok" onclick="addAdminServiceInfo()"></button>
                    </div>
                </div>
            </div>
            <div id="content-5" class="content zoom" style="display: none;">
                <div class="status-box"><p class="status">Successfully saved.</p></div>
                <!--<div class="field-container">
                    <span class="field-label-1" style="width:250px">Qty of Rejected RFID</span>
                    <span id="rejected-rfid" style="width:90px;text-align:right"></span>
                    &nbsp;&nbsp;
                    <input id="input-109" type="text" placeholder="numeric" onclick="adminInputGotFocus(this)" />
                </div>-->
                <div class="field-container">
                    <span class="field-label-1" style="width:250px">Qty of Rejected Paper</span>
                    <span id="rejected-paper" style="width:90px;text-align:right"></span>
                    &nbsp;&nbsp;
                    <input id="txt-qty" type="text" placeholder="numeric" onclick="adminNumericInputGotFocus(this)"/>
                </div>
                <div class="btn-ok-container-1">
                    <div class="btn-ok-sub-container">
                        <button class="btn-ok" onclick="addQtyRejectedPaperInfo()"></button>
                    </div>
                </div>
            </div>
            <div id="content-6" class="content flex-center" style="display: none;">
                <div class="col-btns-side-space"></div>
                <div class="status-box"><p class="status" id="selltement-status"></p></div>
                <div class="col-large-btn">
                    <button class="btn-sd-wo-settlement" onclick="adminShutdownWOSettlement()"></button>
                </div>
            [#list ctx.user.roles as elem]
                [#if elem == "kiosk-slm-ops-sa" || elem == "kiosk-mflg-ops-sa"]
                    <div class="col-large-btn"  style="position: absolute;">
                        <button class="btn-sd-w-settlement" onclick="adminShutdownWSettlement()"></button>
                    </div>
                [/#if]
            [/#list]
                <div class="col-btns-side-space"></div>
            </div>
            <div id="content-7" class="content flex-center" style="display: none;">
                <div class="col-btns-side-space"></div>
                <div class="col-large-btn">
                    <button class="btn-resume" onclick="adminResumeOperation()"></button>
                </div>
                <div class="col-large-btn">
                    <button class="btn-maintenance" onclick="adminMaintenance()"></button>
                </div>
                <div class="col-btns-side-space"></div>
            </div>
            <div id="content-8" class="content zoom" style="display: none;">
                <div class="status-box"><p class="status">Successfully saved.</p></div>
                <div class="field-container"><span class="field-label-2">Activity</span><input id="txt-activity"
                                                                                               type="text"
                                                                                               onclick="adminInputGotFocus(this)" />
                </div>
                <div class="field-container"><span class="field-label-2">Description</span><textarea
                        id="txt-activity-description" onclick="adminInputGotFocus(this)"></textarea></div>
                <div class="btn-ok-container-2">
                    <div class="btn-ok-sub-container">
                        <button class="btn-ok" onclick="addAdminOthersInfo()"></button>
                    </div>
                </div>
            </div>
            <div id="content-9" class="content" style="display: none;">
            </div>
        </div>
    </div>
    <div id="dialog-yes-no" class="dialog-container box-wrapper center-center" style="z-index:10000;display:none">
        <div class="alert-container">
            <div style="z-index:-1" class="dialog-container"></div>
            <h2 class="alert-title">Logout</h2>
            <p class="alert-message">Are you sure you want to logout?</p>
            <div class="group-inner-btn">
                <button type="button" class="btn-yes" onclick="logoutUser()"></button>
                <button type="button" class="btn-no" onclick="hideLogoutDialog()"></button>
            </div>
        </div>
    </div>
    <div id="dialog-ok" class="dialog-container box-wrapper center-center" style="z-index:10001;display:none">
        <div class="alert-container">
            <div style="z-index:-1" class="dialog-container"></div>
            <h2 class="alert-title"></h2>
            <p class="alert-message"></p>
            <div class="group-inner-btn">
                <button type="button" class="btn-ok" onclick="adminAlertOK()"></button>
            </div>
        </div>
    </div>
    <div id="dialog-loading" class="dialog-container" style="z-index:10002;display:none">
        <div style="z-index:-1" class="dialog-container"></div>
        <div style="z-index:1;display:flex;width:100%;height:100%" class="center-center">
            <img src="${themePath}/images/loading.gif"/>
        </div>
    </div>


</div>