function openLoadingPage(){
    $('body').loadingModal({
        position: 'auto',
        text: '',
        color: '#fff',
        opacity: '0.7',
        backgroundColor: 'rgb(0,0,0)',
        animation: 'wave'
    });
}

function closeLoadingPage(){
    $('body').loadingModal('destroy');
}

function doLoginPost() {
	openLoadingPage();
    var urlParams = new URLSearchParams(window.location.search);
    var channel =urlParams.get('channel');
    $.ajax({
        type: "get",
        url: "/.store-kiosk/ops/login",
        data: "mgnlUserId=" + $('#inputUsr').val() + "&mgnlUserPSWD=" + $('#inputPsw').val()+ "&channel=" +channel,
        success: function(response){
        	closeLoadingPage();
            window.location.href = sitePath+"/detail?channel="+channel;
        },
        error: function(e){
        	closeLoadingPage();
            $('.error-msg').html("You are not authorized to perform this operation");
        }
    });
}

var adminKeyboard = function () {
    var keyboardFiled;
    var keyObj = {};
    keyObj[0] = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
        'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
        'shift', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'backspace',
        'number', '\\', ',', ' ', '.', 'go'];
    keyObj[1] = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
        'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L',
        'shift', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'del',
        'number', '\\', ',', ' ', '.', 'go'];
    keyObj[2] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
        '@', '$', '&', '_', '(', ')', ':', ';', '"',
        'shift', '-', '!', '#', '=', '/', '+', '?', 'backspace',
        'number', '\\', ',', ' ', '.', 'go'];
    var keyImgObj = {};
    var imgFolder = 'images/admin/keyboard';
    var imgP_c = imgFolder + '/common/';
    var imgP_l = imgFolder + '/lowercase/lowercase-';
    var imgP_n = imgFolder + '/number/';
    var imgP_u = imgFolder + '/uppercase/uppercase-';
    keyImgObj[0] = [imgP_l + 'q.png', imgP_l + 'w.png', imgP_l + 'e.png', imgP_l + 'r.png', imgP_l + 't.png', imgP_l + 'y.png', imgP_l + 'u.png', imgP_l + 'i.png', imgP_l + 'o.png', imgP_l + 'p.png',
        imgP_l + 'a.png', imgP_l + 's.png', imgP_l + 'd.png', imgP_l + 'f.png', imgP_l + 'g.png', imgP_l + 'h.png', imgP_l + 'j.png', imgP_l + 'k.png', imgP_l + 'l.png',
        imgP_n + 'caps-lock.png', imgP_l + 'z.png', imgP_l + 'x.png', imgP_l + 'c.png', imgP_l + 'v.png', imgP_l + 'b.png', imgP_l + 'n.png', imgP_l + 'm.png', imgP_n + 'del.png',
        imgP_c + 'number.png', imgP_c + 'backslash.png', imgP_c + 'comma.png', imgP_c + 'space.png', imgP_c + 'period.png', imgP_c + 'go.png'
    ];
    keyImgObj[1] = [imgP_u + 'q.png', imgP_u + 'w.png', imgP_u + 'e.png', imgP_u + 'r.png', imgP_u + 't.png', imgP_u + 'y.png', imgP_u + 'u.png', imgP_u + 'i.png', imgP_u + 'o.png', imgP_u + 'p.png',
        imgP_u + 'a.png', imgP_u + 's.png', imgP_u + 'd.png', imgP_u + 'f.png', imgP_u + 'g.png', imgP_u + 'h.png', imgP_u + 'j.png', imgP_u + 'k.png', imgP_u + 'l.png',
        imgP_n + 'caps-lock.png', imgP_u + 'z.png', imgP_u + 'x.png', imgP_u + 'c.png', imgP_u + 'v.png', imgP_u + 'b.png', imgP_u + 'n.png', imgP_u + 'm.png', imgP_n + 'del.png',
        imgP_c + 'number.png', imgP_c + 'backslash.png', imgP_c + 'comma.png', imgP_c + 'space.png', imgP_c + 'period.png', imgP_c + 'go.png'
    ];
    keyImgObj[2] = [imgP_n + '1.png', imgP_n + '2.png', imgP_n + '3.png', imgP_n + '4.png', imgP_n + '5.png', imgP_n + '6.png', imgP_n + '7.png', imgP_n + '8.png', imgP_n + '9.png', imgP_n + '0.png',
        imgP_n + 'at.png', imgP_n + 'dollar.png', imgP_n + 'ampersand.png', imgP_n + 'hyphen.png', imgP_n + 'open-paren.png', imgP_n + 'close-paren.png', imgP_n + 'colon.png', imgP_n + 'semicolon.png', imgP_n + 'double-quotation-marks.png',
        imgP_n + 'caps-lock.png', imgP_n + 'dash.png', imgP_n + 'exclamation-mark.png', imgP_n + 'pound.png', imgP_n + 'equal.png', imgP_n + 'slash.png', imgP_n + 'plus.png', imgP_n + 'question-mark.png', imgP_n + 'del.png',
        imgP_c + 'alphabet.png', imgP_c + 'backslash.png', imgP_c + 'comma.png', imgP_c + 'space.png', imgP_c + 'period.png', imgP_c + 'go.png'
    ];
    this.limitType = undefined;
    this.kbType = 0;
    this.changeKeyBG = function (firstLoad) {
        var keyboardObj = $('#admin-keyboard');
        $('#admin-keyboard').find('.keyboard-container').css('opacity', 0);
        var timeout = 30;
        if (firstLoad == true) {
            //kbType = 0;
            // Keyboard START
            $(keyImgObj[kbType]).each(function (i) {
                keyboardObj.find('#key-' + i).css('border-style', 'none');
                keyboardObj.find('#key-' + i).css('height', '60px');
                if (i == 19 || i == 27 || i == 28) {
                    keyboardObj.find('#key-' + i).css('width', '116px');
                } else if (i == 31) {
                    keyboardObj.find('#key-' + i).css('width', '259px');
                } else if (i == 33) {
                    keyboardObj.find('#key-' + i).css('width', '146px');
                } else {
                    keyboardObj.find('#key-' + i).css('width', '60px');
                }
            });
            // Keyboard END
            timeout = 800;
        }
        $(keyImgObj[kbType]).each(function (i) {
            keyboardObj.find('#key-' + i).css('background-image', 'url(' +themePath+'/'+ keyImgObj[kbType][i] + ')');
        });
        setTimeout(function () {
            $('#admin-keyboard').find('.keyboard-container').css('opacity', 1);
            if (formType == 2) {
                keyboardFiled.focus();
                // To be implemented
            } else if (formType == 1) {
                if (editingUsr == true) keyboardFiled.focus();
                if (editingPsw == true) keyboardFiled.focus();
            }
        }, timeout);
    };
    this.showKeyboard = function (field) {
        keyboardFiled=field;
        changeKeyBG();
        console.log(keyboardFiled);
        $('#admin-keyboard').show();
    };
    this.hideKeyboard = function () {
        $('#admin-keyboard').hide();
        keyboardFiled.gotFocus = false;
    };
    this.shiftTapped = function () {
        // Switch from lowercase keyboard to uppercase keyboard
        if (kbType == 0) kbType = 1;
        // Switch from uppercase keyboard to lowercase keyboard
        else if (kbType == 1) kbType = 0;
        // Switch from number keyboard to lowercase keyboard. To be removed
        else if (kbType == 2) kbType = 0;
        changeKeyBG();
    };
    this.numberTapped = function () {
        // Switch from lowercase or uppercase keyboard to number keyboard
        if (kbType == 0 || kbType == 1) {
            kbType = 2;
            //$('#key-19').css('visibility','hidden');
        }
        // Switch from number keyboard to lowercase keyboard. To be added
        else if (kbType == 2) {
            kbType = 0;
            //$('#key-19').css('visibility','visible');
        }
        changeKeyBG();
    };
    this.keyTapped = function (index) {
        var key = keyObj[kbType][index];
        if (limitType == 'time') {
            if ($.inArray(key, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':']) == -1) {
                if (formType == 2) {
                    keyboardFiled.focus();
                }
                return;
            }
        } else if (limitType == 'numeric') {
            if ($.inArray(key, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']) == -1) {
                if (formType == 2) {
                    keyboardFiled.focus();
                }
                return;
            }
        }
        var oldText =  keyboardFiled.val();

        if(keyboardFiled.context.id=="txt-ticket-qty-unloaded"){
            keyboardFiled.val(oldText+key);
            populateTextFieldsForLoadTicketPapaer();
        }else{
            keyboardFiled.val(oldText+key);
        }

    };
    this.backSpaceTapped = function () {
        var oldText = '';

        oldText = keyboardFiled.val();

        var newText = '';
        var selectionStart = oldText.length;
        var selectionEnd = oldText.length;
        var obj;

        if (selectionStart == selectionEnd) {
            if (selectionStart > 0) {
                newText = newText + oldText.substring(0, selectionStart - 1);
                if (selectionEnd > oldText.length) {
                    // Do nothing
                } else {
                    newText = newText + oldText.substring(selectionEnd, oldText.length);
                }

            } else {
                newText = oldText;

            }
        } else {
            newText = newText + oldText.substring(0, selectionStart);
            if (selectionEnd >= oldText.length) {
                // Do nothing
            } else {
                newText = newText + oldText.substring(selectionEnd, oldText.length);
            }

        }
        if(keyboardFiled.context.id=="txt-ticket-qty-unloaded"){
            keyboardFiled.val(newText);
            removeTextFieldsForLoadTicketPapaer(oldText);
        }else{
            keyboardFiled.val(newText);
        }

    };
    this.go = function () {
        if (formType == 2) {
            hideKeyboard();
        } else if (formType == 1) {
            doLoginPost();
        }
    };
    this.pickDate = function(element){
        $(element).pickadate({
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
        })
    };
    this.pickTime = function(element){
        $(element).pickatime({
            format: 'HH:i a',
            formatLabel: '<b>HH</b>:i <!i>a</!i>',
            formatSubmit: 'HH:i',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
        })
    };
    return this;
}();