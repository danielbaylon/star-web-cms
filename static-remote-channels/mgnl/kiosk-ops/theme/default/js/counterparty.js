var transactionList;
var transactionPageNumber;
var transactionMaxPageNumber;
var transactionPerPage = 10
var transactionIsLast=false;
var receiptList;
var receiptPageNumber;
var receiptMaxPageNumber;
var receiptPerPage = 10
var receiptIsLast=false;
var channel;

var receiptNumberForRePrint;
var ticketPrintStatusForRePrint;

function openLoadingPage(){
    $('body').loadingModal({
        position: 'auto',
        text: '',
        color: '#fff',
        opacity: '0.7',
        backgroundColor: 'rgb(0,0,0)',
        animation: 'wave'
    });
}

function closeLoadingPage(){
    $('body').loadingModal('destroy');
}


function adminShutdownWOSettlement(){
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-settlement-info",
        data: "value=SWOS",
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            console.log(response)
          //  $(".btn-resume").attr("disabled", "disabled");
          //  $(".btn-maintenance").removeAttr("disabled");

            // var classnameR = document.getElementsByClassName("btn-resume");
            // classnameR[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-resume-operation-disabled.png')";
            //
            // var classnameM = document.getElementsByClassName("btn-maintenance");
            // classnameM[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-maintenance-active.png')"

        },
        error: function (e) {
            //$('.error-msg').html("You are not authorized to perform this operation");
            redirectToErrorPage();
        }
    });
}

function adminShutdownWSettlement(){
	openLoadingPage();
    $(".btn-sd-w-settlement").attr("disabled", "disabled");
    $('#settlement-status').css("visibility", "visible");
    $('#settlement-status').html("Please wait...");
    $('#settlement-status').css('color', 'green');
    var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
    classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-disabled.png')"

    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-settlement-info",
        data: "value=SWS",
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (result) {
        	closeLoadingPage();
            console.log(result)
            if(result.success){

            if (result.data) {
                $(".btn-sd-w-settlement").removeAttr("disabled");
                var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
                classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-active.png')"

            } else {

                $(".btn-sd-w-settlement").attr("disabled", "disabled");

                var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
                classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-disabled.png')"
                visibleSuccessMessage();
                    if(result.message == "" ) {
                        $('#selltement-status').html("Settlement Successful");
                    }else{
                        $('#selltement-status').html("Close shift failed, "+result.message);
                    }
            }
            //else if(result.data == '2'){
            //    adminShutdownWSettlement();
            //}
            }
            else{
                $(".btn-sd-w-settlement").removeAttr("disabled");
                var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
                classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-active.png')"
                visibleSuccessMessage();
                $('#selltement-status').css('color', 'red');
                $('#selltement-status').html("Settlement failed, You are not able to continue this operation");

            }
        },
        error: function (e) {
        	closeLoadingPage();
            $(".btn-sd-w-settlement").removeAttr("disabled");
            var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
            classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-active.png')"
            visibleSuccessMessage();
            $('#selltement-status').css('color', 'red');
            $('#selltement-status').html("Settlement failed, You are not able to continue this operation");

            //redirectToErrorPage();
        }
    });
}


function getSettlementInfo() {

    $.ajax({
        url: '/.store-kiosk/ops-data/get-Settlement-info',
        data: 'mode=settlement',
        headers : {
            'Store-Api-Channel' : channel
        },
        type: 'POST',
        success: function (result) {
            console.log(result);
            // if value is 0 = two buttons need to enable
            // if value is 1 = with out  button need to enable
            // if value is 2 = with button need to disable
            if(result.success){
                if (result.data == '0') {

                } else if(result.data == '1'){

                    $(".btn-sd-w-settlement").attr("disabled", "disabled");

                    var classnameW = document.getElementsByClassName("btn-sd-w-settlement");
                    classnameW[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-shutdown-with-settlement-disabled.png')"
               }
            }else{
                 visibleSuccessMessage();
                $('#selltement-status').html("Settlement failed, You are not able to continue this operation");

            }
        },
        error: function () {
            visibleSuccessMessage();
            $('#selltement-status').html("Settlement failed, You are not able to continue this operation");
            //redirectToErrorPage();
        }
    });
}

function rePrintReceipt(){
    console.log("rePrintReceipt");
    $.ajax({
        type: 'POST',
        url: '/.store-kiosk/kiosk-slm/printReceipt',
        data: "receipt-number="+receiptNumberForRePrint+ "&ticket-status=" +ticketPrintStatusForRePrint ,
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (result) {
            console.log(result);
            if(result.success) {

            }else{

              $('#error').html("Reprint failed, You are not able to continue this operation");

            }
        },
        error: function () {
            $('#error').html("Reprint failed, You are not able to continue this operation");
            //redirectToErrorPage();
        }
    });
}

function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}


function clearAllElementInReceiptHistory() {
    $("#receipt-total").html("");
    $("#receipt-qst").html("");
    $("#receipt-location").html("");
    $("#receipt-date").html("");
    $("#receipt-kiosk").html("");
    $("#receipt-tran-id").html("");
    $("#receipt-footer-receipt-id").html("");
    $("#receipt-receipt-no").html("");
    $("#receipt-tid").html("");
    $("#receipt-mid").html("");
    $("#receipt-batch").html("");
    $("#receipt-invoice").html("");
    $("#receipt-approval-code").html("");
    $("#receipt-rrn").html("");
    $("#receipt-merchant").html("");
    $("#receipt-entry-mode").html("");
    $("#receipt-tvr").html("");
    $("#receipt-aid").html("");
    $("#receipt-applabel").html("");
    $("#receipt-tc").html("");
    $("#receipt-payment-mode").html("");
    $("#receipt-card-label").html("");
    $("#receipt-card-no").html("");
    $("#receipt-cardhoder_name").html("");
    $("#receipt-signature").html("");
    $("#receipt-ticket-issuance-status").html("");
    $("#receipt-ticket-issuance-msg1").html("");
    $("#receipt-ticket-issuance-msg2").html("");
    $("#receipt-ticket-issuance-msg3").html("");
    $("#receipt-ticket-issuance-msg4").html("");
    $("#qr_code_image").css("display", "none");
    $("#receipt-footer-receipt-id").css("display", "none");
}
function populateReceiptViewByTranID(tranId){
    console.log("Row value is: " +  tranId);
    $.ajax({
        type: 'POST',
        url: '/.store-kiosk/ops-data/get-receipt-details-by-transid',
        data: "tranid="+tranId.toString(),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (result) {
            console.log(result);
            if (result.success) {


                $("#receipt-company-logo").html('<img src="data:image/png;base64,'+result.data.logo+'">');
                $("#receipt-company-name").html(result.data.companyName);
                $("#receipt-company-address").html(result.data.address);
                $("#receipt-company-postal").html(result.data.postalCode);
                $("#receipt-company-contact").html(result.data.contactDetails);
                $("#receipt-company-gst").html(result.data.gstDetails);

                clearAllElementInReceiptHistory();


                for (var i = result.data.items.length - 1; i >= 0; i--) {
                
                        $('<tr class="receipt-product-tr" style="font-weight:bold"> <td id="receipt-cms-name-' + i + '">{{productCMS}}</th> </tr><tr class="receipt-product-tr"> <td id="receipt-ax-name-' + i + '">{{productAX}}</td> <td id="receipt-ax-qty-' + i + '">{{qty}}</td> <td class="receipt-table-currency" id="receipt-ax-price-' + i + '">{{price}}</td> </tr> <tr class="receipt-product-tr" id="receipt-subtotal-tr-' + i + '"> <td></td> <td>Sub Total :</td> <td class="receipt-table-currency" id="receipt-subtotal-' + i + '">{{subtotal}}</td> </tr>').insertAfter('#ticket-details');

                        $("#receipt-cms-name-" + i).html(result.data.items[i].productName);
                        $("#receipt-ax-qty-" + i).html(result.data.items[i].qty);
                        $("#receipt-ax-price-" + i).html(result.data.items[i].amount);
                        $("#receipt-ax-name-" + i).html(result.data.items[i].axProductName);
                        $("#receipt-subtotal-" + i).html(result.data.items[i].subtotalText);
                }
                
                $("#receipt-total").html(result.data.totalAmount);
                $("#receipt-qst").html(result.data.gstAmount);

                var date = new Date(result.data.printedDateText);
                var axCartCheckoutDateTime=date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+ date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();

                
                    $("#receipt-location").html(result.data.location);
                    $("#receipt-date").html(result.data.printedDateText);
                    $("#receipt-kiosk").html(result.data.kioskId);
                    $("#receipt-tran-id").html(result.data.transId);
                    $("#receipt-footer-receipt-id").html(result.data.receiptNumber);
                    $("#receipt-receipt-no").html(result.data.receiptSeqNumber);
           

                if(result.data.kioskPaymentItems[0]!=undefined) {

                    $("#receipt-tid").html(result.data.kioskPaymentItems[0].terminalId);
                    $("#receipt-mid").html(result.data.kioskPaymentItems[0].merchantId);
                    $("#receipt-batch").html(result.data.kioskPaymentItems[0].batchNumber);
                    $("#receipt-invoice").html(result.data.kioskPaymentItems[0].invoiceNumber);
                    $("#receipt-approval-code").html(result.data.kioskPaymentItems[0].approvalCode);
                    $("#receipt-rrn").html(result.data.kioskPaymentItems[0].retrivalReferenceData);
                    $("#receipt-merchant").html(result.data.kioskPaymentItems[0].merchantName);
                    $("#receipt-entry-mode").html(result.data.kioskPaymentItems[0].entryMode);
                    $("#receipt-tvr").html(result.data.kioskPaymentItems[0].terminalVerificationResponse);
                    $("#receipt-aid").html(result.data.kioskPaymentItems[0].applicationIdentifier);
                    $("#receipt-applabel").html(result.data.kioskPaymentItems[0].appLabel);
                    $("#receipt-tc").html(result.data.kioskPaymentItems[0].transactionCertificate);
                    $("#receipt-payment-mode").html(result.data.kioskPaymentItems[0].paymentMode);
                    $("#receipt-card-label").html(result.data.kioskPaymentItems[0].cardLabel);
                    $("#receipt-card-no").html(result.data.kioskPaymentItems[0].cardNumber);

                    var paymentMode = result.data.kioskPaymentItems[0].paymentMode;
                    if("NETS PIN DEBIT"==paymentMode){
                        $("#receipt-applabel-row").css("display", "none");
                        $("#receipt-entry-mode-row").css("display", "none");
                        $("#receipt-tvr-row").css("display", "none");
                        $("#receipt-aid-row").css("display", "none");
                        $("#receipt-tc-row").css("display", "none");
                        $("#receipt-card-no-row").css("display", "none");
                        $("#receipt-merchant").css("display", "none");

                        $("#receipt-batch-lab").html("NETS Trace#:");
                        $("#receipt-invoice-lab").html("NETS Receipt#:");

                        $("#receipt-batch").html(result.data.kioskPaymentItems[0].netsTraceNumber);
                        $("#receipt-invoice").html(result.data.kioskPaymentItems[0].netsReceiptNumber);

                    }else if("NETS FLASHPAY (NFP)"==paymentMode){
                        $("#receipt-applabel-row").css("display", "none");
                        $("#receipt-entry-mode-row").css("display", "none");
                        $("#receipt-tvr-row").css("display", "none");
                        $("#receipt-aid-row").css("display", "none");
                        $("#receipt-tc-row").css("display", "none");
                        $("#receipt-card-no-row").css("display", "none");

                        $("#receipt-invoice-lab").html("Original Balance:");
                        $("#receipt-approval-code-lab").html("Trans Amount:");
                        $("#receipt-rrn-lab").html("               ");
                        $("#receipt-merchant-lab").html("Remain Balance:");

                        $("#receipt-batch-lab").html("Batch#:");
                        $("#receipt-invoice").html(result.data.kioskPaymentItems[0].originalBalance);
                        $("#receipt-approval-code").html(result.data.kioskPaymentItems[0].amount);
                        $("#receipt-rrn").html("===========");
                        $("#receipt-merchant").html(result.data.kioskPaymentItems[0].originalBalance);
                    }else{
                        $("#receipt-applabel-row").css("display", "table-row");
                        $("#receipt-entry-mode-row").css("display", "table-row");
                        $("#receipt-tvr-row").css("display", "table-row");
                        $("#receipt-aid-row").css("display", "table-row");
                        $("#receipt-tc-row").css("display", "table-row");
                        $("#receipt-card-no-row").css("display", "table-row");

                        $("#receipt-batch-lab").html("Batch#:");
                        $("#receipt-invoice-lab").html("Invoice#:");
                        $("#receipt-approval-code-lab").html("Approval Code:");
                        $("#receipt-rrn-lab").html("RRN:");
                        $("#receipt-merchant-lab").html("Merchant:");
                    }

                    //show signature or write
                    if (result.data.kioskPaymentItems[0].signatureRequired) {
                        var i = new Image()
                        i.id = "signature_image";
                        i.style = "width: 35%;";
                        i.src = "data:image/png;base64," + result.data.kioskPaymentItems[0].signature;
                        $(i).appendTo($("#signature"));

                        $("#receipt-cardhoder_name").html(result.data.kioskPaymentItems[0].cardHolderName);
                        $("#receipt-signature").html("Sign x______________________________");
                    } else {
                        $("#receipt-cardhoder_name").html("");
                        $("#receipt-signature").html("NO SIGNATURE REQUIRED");
                    }
                }

               if(result.data.status=="5") {
                    $("#receipt-header-table-2").css("display", "none");
                    $("#receipt-header-table-3").css("display", "none");
                    $("#payment-receipt-header").css("display", "none");
                    $("#agreement-header").css("display", "none");
                    $("#receipt-dash-line-6").css("display", "none");
                    $("#signature-header").css("display", "none");

                }else{
                    $("#receipt-header-table-2").css("display", "table-row");
                    $("#receipt-header-table-3").css("display", "table-row");
                    $("#payment-receipt-header").css("display", "block");
                    $("#agreement-header").css("display", "block");
                    $("#receipt-dash-line-6").css("display", "block");
                    $("#signature-header").css("display", "block");
                }


                $("#receipt-ticket-issuance-status").html(result.data.ticketIssuanceStatus.ticketIssuranceStatus);
                $("#receipt-ticket-issuance-msg1").html(result.data.ticketIssuanceStatus.ticketIssuranceMsg1);
                $("#receipt-ticket-issuance-msg2").html(result.data.ticketIssuanceStatus.ticketIssuranceMsg2);
                $("#receipt-ticket-issuance-msg3").html(result.data.ticketIssuanceStatus.ticketIssuranceMsg3);
                $("#receipt-ticket-issuance-msg4").html(result.data.ticketIssuanceStatusticketIssuranceMsg4);

                //ticket issuance
                if(result.data.ticketIssuanceStatus.ticketIssuranceStatus=="NOT ISSUED") {
                    $("#qr_code_image").css("display", "block");
                    $("#receipt-footer-receipt-id").css("display", "block");
                }else{
                    $("#qr_code_image").css("display", "none");
                    $("#receipt-footer-receipt-id").css("display", "none");

                }

            } else {
                clearAllElementInReceiptHistory();
            }
        },
        error: function () {
            //redirectToErrorPage();
        }
    });
}
function onReceiptHistoryTableItemClick(x){

    $(".no-document-container").css("display", "block");
    $('.receipt-product-tr').remove();
    $('#signature_image').remove();
    var obj = receiptList.filter(function ( objList ) {
        return objList.index ===  x;
    })[0];

    receiptNumberForRePrint = obj.transId;
    ticketPrintStatusForRePrint = obj.isTicketFail;

    getCompanyDetails();

    populateReceiptViewByTranID(obj.transId);
}

function getCompanyDetails(){

    $.ajax({
        url: '/.store-kiosk/ops-data/get-company-receipt-details',
        data: '',
        headers : {
            'Store-Api-Channel' : channel
        },
        type: 'POST',
        cache: false,
        dataType: 'json',
        success: function (result) {
             console.log(result);
              if (result.success) {
                $("#receipt-company-logo").html('<img src="data:image/png;base64,'+result.data.logo+'">');
                $("#receipt-company-name").html(result.data.companyName);
                $("#receipt-company-address").html(result.data.address);
                $("#receipt-company-postal").html(result.data.postalCode);
                $("#receipt-company-contact").html(result.data.contactDetails);
                $("#receipt-company-gst").html(result.data.gstDetails);

              }
        },
        error: function () {
            //redirectToErrorPage();
        }
    });

}

function getHardwareStatus() {
    $.ajax({
        url: '/.store-kiosk/ops-data/get-hardware-status',
        data: 'mode=status',
        headers : {
            'Store-Api-Channel' : channel
        },
        type: 'POST',
        cache: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function (result) {
            console.log(result);
            if (result.success) {
                var appServerStatus = result.data.appServerStatus;
                var paymentTerminalStatus = result.data.paymentTerminalStatus;
                var receiptPrinterStatus = result.data.receiptPrinterStatus;
                var roboticArmStatus = result.data.roboticArmStatus;
                var ticketPrinterStatus = result.data.ticketPrinterStatus;

                var appServerStatusEl = document.getElementById("app-server-status");
                var paymentTerminalStatusEl = document.getElementById("payment-terminal-status");
                var receiptPrinterStatusEl = document.getElementById("receipt-printer-status");
                var roboticArmStatusEl = document.getElementById("robotic-arm-status");
                var ticketPrinterStatusEl = document.getElementById("ticket-printer-status");

                setStatusImage(ticketPrinterStatusEl, ticketPrinterStatus);
                setStatusImage(receiptPrinterStatusEl, receiptPrinterStatus);
                setStatusImage(paymentTerminalStatusEl, paymentTerminalStatus);

                $('#selltement-status').css('color', 'green');
                $("#app-server-txt").css('color',appServerStatus=="false"?'green':'red');
                $("#payment-terminal-txt").css('color',paymentTerminalStatus=="false"?"green":'red');
                $("#receipt-printer-txt").css('color',receiptPrinterStatus=="false"?"green":'red');
                $("#robotic-arm-txt").css('color',roboticArmStatus=="false"?"green":'red');
                $("#ticket-printer-txt").css('color',ticketPrinterStatus=="false"?"green":'red');

                $("#app-server-txt").val(appServerStatus=="false"?"Connected":result.data.appServerMsg);
                $("#payment-terminal-txt").val(paymentTerminalStatus=="false"?"Connected":result.data.paymentTerminalMsg);
                $("#receipt-printer-txt").val(receiptPrinterStatus=="false"?"Connected":result.data.receiptPrinterMsg);
                $("#robotic-arm-txt").val(roboticArmStatus=="false"?"Connected":result.data.roboticArmMsg);
                $("#ticket-printer-txt").val(ticketPrinterStatus=="false"?"Connected":result.data.ticketPrinterMsg);
            } else {

            }
        },
        error: function () {
            //redirectToErrorPage();
        }
    });
}

function adminSearchReceiptHistory() {
    $("#table-receipt-loading").css("display", "block");
    openLoadingPage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/search-receipt-details",
        data: "selectdate=" + $('#txt-select-date-receipt').val() + "&starttime=" + $('#txt-start-time-receipt').val() + "&endtime=" + $('#txt-end-time-receipt').val().substring(0,5),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            console.log(response);
            receiptPageNumber=1;
            receiptMaxPageNumber=1;
            // $("#table-pagination").css("display", "none");
            $("#table-receipt-loading").css("display", "none");
            clearAllElementInReceiptHistory();

            if (response.success) {
                //  $('#txt-activity').val("")
                // $('#txt-activity-description').val("")
                //visibleSuccessMessage();

                receiptList = response.data.statusDataArrayList;
                setReceiptButtonEnable();
                getMaxReceiptPageNumber();
                setReceiptPagination(receiptPageNumber);

            } else {
                $('.table-receipt').empty();
                $('.table-receipt-tr').empty();
                $("#btn-pagination-receipt").css("display", "none");
                $(".no-document-container").css("display", "none");
            }
        },
        error: function (e) {
            closeLoadingPage();
            //redirectToErrorPage();
        }
    });
}

function setReceiptButtonEnable(){
    var first =document.getElementById('btn-receipt-first');
    var prev = document.getElementById('btn-receipt-prev');
    var next = document.getElementById('btn-receipt-next');
    var last = document.getElementById('btn-receipt-last');

    if(receiptList.length<=receiptPerPage){
        $(".btn-receipt-first").attr("disabled", "disabled");
        $(".btn-receipt-prev").attr("disabled", "disabled");
        $(".btn-receipt-next").attr("disabled", "disabled");
        $(".btn-receipt-last").attr("disabled", "disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage-disabled.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev-disabled.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }else if(receiptPageNumber==1){
        $(".btn-receipt-first").attr("disabled", "disabled");
        $(".btn-receipt-prev").attr("disabled", "disabled");
        $(".btn-receipt-next").removeAttr("disabled");
        $(".btn-receipt-last").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage-disabled.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev-disabled.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage.png')";

    }else if(receiptMaxPageNumber==receiptPageNumber){
        $(".btn-receipt-next").attr("disabled", "disabled");
        $(".btn-receipt-last").attr("disabled", "disabled");
        $(".btn-receipt-first").removeAttr("disabled");
        $(".btn-receipt-prev").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }else if(receiptPageNumber!=1 && !receiptIsLast){
        $(".btn-receipt-first").removeAttr("disabled");
        $(".btn-receipt-prev").removeAttr("disabled");
        $(".btn-receipt-next").removeAttr("disabled");
        $(".btn-receipt-last").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage.png')";
    }else if(receiptPageNumber!=1 && receiptIsLast){
        $(".btn-receipt-next").attr("disabled", "disabled");
        $(".btn-receipt-last").attr("disabled", "disabled");
        $(".btn-receipt-first").removeAttr("disabled");
        $(".btn-receipt-prev").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }
}


function getNextReceiptPageNumber(){
    receiptPageNumber=receiptPageNumber + 1;
    setReceiptButtonEnable();
    return receiptPageNumber;
}

function getPrevReceiptPageNumber(){
    receiptIsLast=false;
    receiptPageNumber=receiptPageNumber - 1;
    setReceiptButtonEnable();
    return receiptPageNumber;
}

function getMaxReceiptPageNumber(){
    var length= receiptList.length;
    var page =  length/receiptPerPage;
    var pageV =  page.toString();
    if(pageV.indexOf('.') !== -1){
        var number=pageV.slice(0, pageV.indexOf("."));
        receiptMaxPageNumber = parseInt(number)+1;
    }else{
        receiptMaxPageNumber = parseInt(number);
    }
}

function goToLastReceiptPage(){
    receiptIsLast = true;
    var length= receiptList.length;
    var page =  length/receiptPerPage;
    var pageV =  page.toString();
    if(pageV.indexOf('.') !== -1){
        var number=pageV.slice(0, pageV.indexOf("."));
        receiptPageNumber = parseInt(number)+1;
    }else{
        receiptPageNumber = parseInt(number);
    }
    setReceiptButtonEnable();
    return receiptPageNumber;
}

function goToFirstReceiptPage(){
    receiptIsLast=false;
    receiptPageNumber=1;
    setReceiptButtonEnable();
    return receiptPageNumber;
}

function setReceiptPagination(indexNo){

    $('.table-receipt').empty();
    $('.table-receipt-tr').empty();

    var page = indexNo;
    var startRec = Math.max(page - 1, 0) * receiptPerPage;
    var endRec =startRec + receiptPerPage
    var recordsToShow = receiptList.slice(startRec, endRec);

    $('<table class="table-receipt"><tr class="table-receipt-tr"><th class="table-receipt-id-th">  </th><th class="table-receipt-th">Trans ID</th><th class="table-receipt-time-th">Time of Printing</th></tr></table>').insertBefore('#table-pagination-receipt');

    for (var i = recordsToShow.length  - 1; i >= 0; i--) {
        var receipt = recordsToShow[i];

        $('<tr class="table-receipt-tr" onclick="onReceiptHistoryTableItemClick('+receipt.index+')" ><td class="table-receipt-id-td">'+receipt.index+'</td> <td class="table-receipt-td">'+receipt.transId+'</td><td class="table-receipt-time-td">'+receipt.printTime+'</td></tr>').insertAfter('#table-pagination-receipt').append('<input type="hidden" name="status" id="receipt-status" value="'+receipt.isTicketFail+'"/>');
    }

    if(receiptList.length>0){
        $("#btn-pagination-receipt").css("display", "block");
    }
}

function adminSearchTransactionStatus() {

    $("#table-loading").css("display", "block");
    openLoadingPage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/search-transaction-status",
        data: "selectdate=" + $('#txt-select-date').val() + "&starttime=" + $('#txt-start-time').val() + "&endtime=" + $('#txt-end-time').val() + "&tranid=" + $('#txt-trans-id').val() + "&pincode=" + $('#txt-pin-code').val(),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            console.log(response);
            transactionPageNumber=1;
            transactionMaxPageNumber=1;
            // $("#table-pagination").css("display", "none");
            $("#table-loading").css("display", "none");
            if (response.success) {
                //  $('#txt-activity').val("")
                // $('#txt-activity-description').val("")
                //visibleSuccessMessage();

                transactionList = response.data.statusDataArrayList;
                setTransactionButtonEnable();
                getMaxTransactionPageNumber();
                setTransactionPagination(transactionPageNumber);

            } else {
                $('.table-transaction').empty();
                $('.table-transaction-tr').empty();
                $("#btn-pagination").css("display", "none");
            }
        },
        error: function (e) {
            closeLoadingPage();
           // redirectToErrorPage();
        }
    });
}


function setTransactionButtonEnable(){
    var first =document.getElementById('btn-tran-first');
    var prev = document.getElementById('btn-tran-prev');
    var next = document.getElementById('btn-tran-next');
    var last = document.getElementById('btn-tran-last');

    if(transactionList.length<=transactionPerPage){
        $(".btn-tran-first").attr("disabled", "disabled");
        $(".btn-tran-prev").attr("disabled", "disabled");
        $(".btn-tran-next").attr("disabled", "disabled");
        $(".btn-tran-last").attr("disabled", "disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage-disabled.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev-disabled.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }else if(transactionPageNumber==1){
        $(".btn-tran-first").attr("disabled", "disabled");
        $(".btn-tran-prev").attr("disabled", "disabled");
        $(".btn-tran-next").removeAttr("disabled");
        $(".btn-tran-last").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage-disabled.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev-disabled.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage.png')";

    }else if(transactionMaxPageNumber==transactionPageNumber){
        $(".btn-tran-next").attr("disabled", "disabled");
        $(".btn-tran-last").attr("disabled", "disabled");
        $(".btn-tran-first").removeAttr("disabled");
        $(".btn-tran-prev").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }else if(transactionPageNumber!=1 && !transactionIsLast){
        $(".btn-tran-first").removeAttr("disabled");
        $(".btn-tran-prev").removeAttr("disabled");
        $(".btn-tran-next").removeAttr("disabled");
        $(".btn-tran-last").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage.png')";
    }else if(transactionPageNumber!=1 && transactionIsLast){
        $(".btn-tran-next").attr("disabled", "disabled");
        $(".btn-tran-last").attr("disabled", "disabled");
        $(".btn-tran-first").removeAttr("disabled");
        $(".btn-tran-prev").removeAttr("disabled");
        first.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prevpage.png')";
        prev.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-prev.png')";
        next.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-next-disabled.png')";
        last.style.backgroundImage = "url('../.."+themePath+"/images/admin/btn-nextpage-disabled.png')";
    }
}


function getNextTransactionPageNumber(){
    transactionPageNumber=transactionPageNumber + 1;
    setTransactionButtonEnable();
    return transactionPageNumber;
}

function getPrevTransactionPageNumber(){
    transactionIsLast=false;
    transactionPageNumber=transactionPageNumber - 1;
    setTransactionButtonEnable();
    return transactionPageNumber;
}

function getMaxTransactionPageNumber(){
    var length= transactionList.length;
    var page =  length/transactionPerPage;
    var pageV =  page.toString();
    if(pageV.indexOf('.') !== -1){
        var number=pageV.slice(0, pageV.indexOf("."));
        transactionMaxPageNumber = parseInt(number)+1;
    }else{
        transactionMaxPageNumber = parseInt(number);
    }
}

function goToLastTransactionPage(){
    transactionIsLast = true;
    var length= transactionList.length;
    var page =  length/transactionPerPage;
    var pageV =  page.toString();
    if(pageV.indexOf('.') !== -1){
        var number=pageV.slice(0, pageV.indexOf("."));
        transactionPageNumber = parseInt(number)+1;
    }else{
        transactionPageNumber = parseInt(number);
    }
    setTransactionButtonEnable();
    return transactionPageNumber;
}

function goToFirstTransactionPage(){
    transactionIsLast=false;
    transactionPageNumber=1;
    setTransactionButtonEnable();
    return transactionPageNumber;
}

function setTransactionPagination(indexNo){

    $('.table-transaction').empty();
    $('.table-transaction-tr').empty();

    var page = indexNo;
    var startRec = Math.max(page - 1, 0) * transactionPerPage;
    var endRec =startRec + transactionPerPage
    var recordsToShow = transactionList.slice(startRec, endRec);

    $('<table class="table-transaction"> <tr class="table-transaction-tr"><th class="table-transaction-th">Trans ID</th><th class="table-transaction-th">Status</th></tr></table>').insertBefore('#table-pagination');

    for (var i = recordsToShow.length  - 1; i >= 0; i--) {
        var transaction = recordsToShow[i];
        $('<tr class="table-transaction-tr"><td class="table-transaction-td">'+transaction.transId+'</td><td class="table-transaction-td">'+transaction.status+'</td></tr>').insertAfter('#table-pagination');
    }

    if(transactionList.length>0){
        $("#btn-pagination").css("display", "block");
    }
}

function adminResetPaper() {
    getKioskPrinterDetails();
}

function getKioskPrinterDetails() {
    openLoadingPage();
    $.ajax({
        url: '/.store-kiosk/ops-data/print-receipt-details',
        data: 'mode=details',
        type: 'POST',
        cache: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function (result) {
            closeLoadingPage();
            if (result.success) {
                setPrinterDetails(result.data.body, result.success);
            } else {
                setPrinterDetails("false");
            }
        },
        error: function () {
            closeLoadingPage();
          //  redirectToErrorPage();
        }
    });
}


function setPrinterDetails(printerDetails, status) {

    var printer = printerDetails;

    if (status) {
        $("#receipt-printer").css("display", "none");
        var model = printer.printerDetail.modelName;
        $('#inputPrinter').val(model);
        var inputHeadTemperature = printer.printerDetail.printHeadTemperature;
        $('#inputHeadTemperature').val(inputHeadTemperature);
        var inputFireware = printer.printerDetail.firmwareVersion;
        $('#inputFireware').val(inputFireware);
        var inputVoltage = printer.printerDetail.voltage;
        $('#inputVoltage').val(inputVoltage);
        var inputPrintedPaper = printer.printerDetail.totalPaperPrinted;
        $('#inputPrintedPaper').val(inputPrintedPaper);
        var inputCutCounter = printer.printerDetail.cutCounter;
        $('#inputCutCounter').val(inputCutCounter);
        var printHeadOverheated = printer.printerError.printHeadOverheated;
        var pHeadOverHeated = document.getElementById("pHeadOverHeated");
        setStatusImage(pHeadOverHeated, printHeadOverheated);
        var coverUp = printer.printerError.coverUp;
        var pCoverUp = document.getElementById("pCoverUp");
        setStatusImage(pCoverUp, coverUp);
        var irregularEject = printer.printerError.irregularEject;
        var pIrregularEject = document.getElementById("pIrregularEject");
        setStatusImage(pIrregularEject, irregularEject);
        var voltageError = printer.printerError.voltageError;
        var pVoltageError = document.getElementById("pVoltageError");
        setStatusImage(pVoltageError, voltageError);
        var printHeadUp = printer.printerError.printHeadUp;
        var pPrintHeadUp = document.getElementById("pPrintHeadUp");
        setStatusImage(pPrintHeadUp, printHeadUp);
        var paperJam = printer.printerError.paperJam;
        var pPaperJam = document.getElementById("pPaperJam");
        setStatusImage(pPaperJam, paperJam);
        var noPaper = printer.printerError.noPaper;
        var pNoPaper = document.getElementById("pNoPaper");
        setStatusImage(pNoPaper, noPaper);
        var nearPaperEnd = printer.printerError.nearPaperEnd;
        var pNearPaperEnd = document.getElementById("pNearPaperEnd");
        setStatusImage(pNearPaperEnd, nearPaperEnd);
        var autoCutterError = printer.printerError.autoCutterError;
        var pAutoCutterError = document.getElementById("pAutoCutterError");
        setStatusImage(pAutoCutterError, autoCutterError);
        var ramError = printer.printerError.ramError;
        var pRAMError = document.getElementById("pRAMError");
        setStatusImage(pRAMError, ramError);
        var eepromError = printer.printerError.eepromError;
        var pEEPROMError = document.getElementById("pEEPROMError");
        setStatusImage(pEEPROMError, eepromError);

    } else {

        $("#receipt-printer").css("display", "block");
        $('#inputPrinter').val("");
        $('#inputHeadTemperature').val("");
        $('#inputFireware').val("");
        $('#inputVoltage').val("");
        $('#inputPrintedPaper').val("");
        $('#inputCutCounter').val("");
        var printHeadOverheated = "true";
        var pHeadOverHeated = document.getElementById("pHeadOverHeated");
        pHeadOverHeated.style.backgroundColor = "white";
        var coverUp = "true";
        var pCoverUp = document.getElementById("pCoverUp");
        pCoverUp.style.backgroundColor = "white";
        var irregularEject = "true";
        var pIrregularEject = document.getElementById("pIrregularEject");
        pIrregularEject.style.backgroundColor = "white";
        var voltageError = "true";
        var pVoltageError = document.getElementById("pVoltageError");
        pVoltageError.style.backgroundColor = "white";
        var printHeadUp = "true";
        var pPrintHeadUp = document.getElementById("pPrintHeadUp");
        pPrintHeadUp.style.backgroundColor = "white";
        var paperJam = "true";
        var pPaperJam = document.getElementById("pPaperJam");
        pPaperJam.style.backgroundColor = "white";
        var noPaper = "true";
        var pNoPaper = document.getElementById("pNoPaper");
        pNoPaper.style.backgroundColor = "white";
        var nearPaperEnd = "true";
        var pNearPaperEnd = document.getElementById("pNearPaperEnd");
        pNearPaperEnd.style.backgroundColor = "white";
        var autoCutterError = "true";
        var pAutoCutterError = document.getElementById("pAutoCutterError");
        pAutoCutterError.style.backgroundColor = "white";
        var ramError = "true";
        var pRAMError = document.getElementById("pRAMError");
        pRAMError.style.backgroundColor = "white";
        var eepromError = "true";
        var pEEPROMError = document.getElementById("pEEPROMError");
        pEEPROMError.style.backgroundColor = "white";
    }


}

function setStatusImage(element, status) {
    if (status == "true" | status == "") {
        element.style.backgroundColor = "red";
    } else {
        element.style.backgroundColor = "green";
    }
}
var selectedElementId = "tab-0";

$(document).ready(function () {
    var urlParams = new URLSearchParams(window.location.search);
    channel =urlParams.get('channel');

    $(".no-document-container").css("display", "none");
    console.log(channel);

    populateTextFieldsForLoadTicketPapaer();

    $(document).click(function (e) {

        var keyboardContainer = $("#admin-keyboard");

        if (e.target.id != undefined && e.target.id.startsWith("txt") ) {
            return;
        }

        if (!keyboardContainer.is(e.target) && keyboardContainer.has(e.target).length === 0) {
            hideKeyboard();
        }
    });

    $(".tabs button").click(function () {

        deselectTab();
        hideTabContainer();

        selectedElementId = this.id;

        selectTab();
        showTabContainer();
        hiddenSuccessMessage();
    });
});

function adminInputGotFocus(txtValue) {
    formType=2;
    limitType='';
    kbType=0;
    var targetSource = $(txtValue);
    showKeyboard(targetSource);

}

function adminNumericInputGotFocus(txtValue) {
    limitType='numeric';
    formType=2;
    kbType=2;
    var targetSource = $(txtValue);
    showKeyboard(targetSource);

}

function adminInputLostFocus(){
    hideKeyboard();
}

function hiddenSuccessMessage() {
    var classname = document.getElementsByClassName("status");

    for (var i = 0; i < classname.length; i++) {
        classname[i].style.visibility = "hidden";
    }

}

function visibleSuccessMessage() {
    var classname = document.getElementsByClassName("status");

    for (var i = 0; i < classname.length; i++) {
        classname[i].style.visibility = "visible";
    }

}

function selectTab() {
    $("#" + selectedElementId).attr("style", "border: 1px solid white; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);");
}

function deselectTab() {
    $("#" + selectedElementId).attr("style", "border-style: groove; color: rgb(255, 255, 255); background-color: rgb(51, 51, 51);");
}

function hideTabContainer() {
    var containerId = selectedElementId.split("-")[1];
    $("#content-" + containerId).hide();
}

function showTabContainer() {
    var containerId = selectedElementId.split("-")[1];
    $("#content-" + containerId).show();

    if (containerId == 9)
        $("#dialog-yes-no").show();
}

function logoutUser() {
    window.location.href = contextPath+"/"+channel+"?mgnlLogout";
}

function hideLogoutDialog() {
    $("#dialog-yes-no").hide();
}

function addAdminOthersInfo() {
    openLoadingPage();
    hiddenSuccessMessage();

    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-other-info",
        data: "activity=" + $('#txt-activity').val() + "&description=" + $('#txt-activity-description').val(),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            if (response.data) {
                $('#txt-activity').val("")
                $('#txt-activity-description').val("")
                visibleSuccessMessage();
            }
        },
        error: function (e) {
            closeLoadingPage();
         //   redirectToErrorPage();
        }
    });
}

function addAdminServiceInfo() {
    openLoadingPage();
    hiddenSuccessMessage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-service-info",
        data: "servicename=" + $('#txt-service').val() + "&description=" + $('#txt-service-description').val(),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            if (response.data == 'true') {
                $('#txt-service').val("")
                $('#txt-service-description').val("")
                visibleSuccessMessage();
            }
        },
        error: function (e) {
            closeLoadingPage();
           // redirectToErrorPage();
        }
    });
}

function addQtyRejectedPaperInfo() {
    openLoadingPage(); 
    hiddenSuccessMessage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-qty-rejected-paper-info",
        data: "qty=" + $('#txt-qty').val(),
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            if (response.data == 'true') {
                $('#txt-qty').val("")
                visibleSuccessMessage();
            }
        },
        error: function (e) {
        closeLoadingPage();
        //    redirectToErrorPage();
        }
    });
}

function addLoadPaperTickerInfo() {

    hiddenSuccessMessage();
    var datajsonArr = new Object();
    var paperTicketQtyEntity = new Array();

    var txtFieldsCount = $('#txt-ticket-qty-unloaded').val();
    for (var counter = 1; counter <= txtFieldsCount; counter++) {
        var fromVal = $('#txt-lpt-from-' + counter).val();
        var toVal = $('#txt-lpt-to-' + counter).val();

        paperTicketQtyEntity.push({
            fromNumber: fromVal,
            toNumber: toVal
        })
    }

    var qtyunloaded = $('#txt-ticket-qty-unloaded').val();
    var qtyloaded = $('#txt-ticket-qty-loaded').val();
    var startno = $('#txt-ticket-start-no').val();
    var endno = $('#txt-ticket-end-no').val();

    datajsonArr['qtyUnloaded'] = qtyunloaded;
    datajsonArr['qtyLoaded'] = qtyloaded;
    datajsonArr['startTicketNo'] = startno;
    datajsonArr['endTicketNo'] = endno;
    datajsonArr['loadPaperTicketQty'] = paperTicketQtyEntity;
    
    openLoadingPage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/add-load-paper-ticket-info",
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(datajsonArr),
        headers : {
            'Store-Api-Channel' : channel
        },

        success: function (response) {
            closeLoadingPage();
            if (response.data == 'true') {
                var counter = $('#txt-ticket-qty-unloaded').val();
                $('#txt-ticket-qty-unloaded').val("");
                $('#txt-ticket-qty-loaded').val("");
                $('#txt-ticket-start-no').val("");
                $('#txt-ticket-end-no').val("");
                $('#txt-lpt-from-1').val("");
                $('#txt-lpt-to-1').val("");

                for (var counter; counter > 1; counter--) {
                    $("#unloaded-fields" + counter).remove();
                }
                visibleSuccessMessage();
            }
        },
        error: function (e) {
         closeLoadingPage();
         //redirectToErrorPage();
        }
    });
}

function removeTextFieldsForLoadTicketPapaer(index) {
    //  $("#txt-ticket-qty-unloaded").change(function () {
    var counter = index;
    for (var counter; counter > 1; counter--) {
        $("#unloaded-fields" + counter).remove();
    }

}

function populateTextFieldsForLoadTicketPapaer() {
    var index;
  //  $("#txt-ticket-qty-unloaded").change(function () {
        var counter = index;
        for (var counter; counter > 1; counter--) {
            $("#unloaded-fields" + counter).remove();
        }

        var txtFieldsCount = $('#txt-ticket-qty-unloaded').val();
        if (txtFieldsCount == "" || txtFieldsCount == 0) {
            var counter = index;
            for (var counter; counter > 1; counter--) {
                $("#unloaded-fields" + counter).remove();
            }
        } else {
            for (var counter = 2; counter <= txtFieldsCount; counter++) {
                var newTextBoxDiv = $(document.createElement('div'))
                    .attr({class: 'field-container unloaded-fields', id: 'unloaded-fields' + counter});
                newTextBoxDiv.after().html('<span class="index-number" >' + counter + '</span>&nbsp; <input type="text" id="txt-lpt-from-' + counter + '" class="number" onclick="adminInputGotFocus(this)"  /> &nbsp;<span>to</span>&nbsp;<input type="text" id="txt-lpt-to-' + counter + '" class="number" onclick="adminInputGotFocus(this)"/>');
                newTextBoxDiv.appendTo("#unloaded-tickets-list");
                index = counter;
            }
        }
 //   });
}

function adminMaintenance() {
    openLoadingPage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/check-operation-mode",
        data: "value=down",
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();      
            $(".btn-maintenance").attr("disabled", "disabled");
            $(".btn-resume").removeAttr("disabled");

            var classnameM = document.getElementsByClassName("btn-maintenance");
            classnameM[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-maintenance-disabled.png')";

            var classnameR = document.getElementsByClassName("btn-resume");
            classnameR[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-resume-operation-active.png')"

        },
        error: function (e) {
            closeLoadingPage();
            // $('.error-msg').html("You are not authorized to perform this operation");
        //    redirectToErrorPage();
        }
    });
}

function adminResumeOperation() {
    openLoadingPage();
    $.ajax({
        type: "post",
        url: "/.store-kiosk/ops-data/check-operation-mode",
        data: "value=resume",
        headers : {
            'Store-Api-Channel' : channel
        },
        success: function (response) {
            closeLoadingPage();
            $(".btn-resume").attr("disabled", "disabled");
            $(".btn-maintenance").removeAttr("disabled");

            var classnameR = document.getElementsByClassName("btn-resume");
            classnameR[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-resume-operation-disabled.png')";

            var classnameM = document.getElementsByClassName("btn-maintenance");
            classnameM[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-maintenance-active.png')"

        },
        error: function (e) {
            closeLoadingPage();
            //$('.error-msg').html("You are not authorized to perform this operation");
         //   redirectToErrorPage();
        }
    });
}

function getKioskMaintenanceMode() {

    $.ajax({
        url: '/.store-kiosk/ops-data/get-operation-mode',
        data: 'mode=operation',
        headers : {
            'Store-Api-Channel' : channel
        },
        type: 'POST',
        cache: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function (result) {
            if (result.data == 'false') {

                $(".btn-maintenance").attr("disabled", "disabled");
                $(".btn-resume").removeAttr("disabled");

                var classnameM = document.getElementsByClassName("btn-maintenance");
                classnameM[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-maintenance-disabled.png')";

                var classnameR = document.getElementsByClassName("btn-resume");
                classnameR[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-resume-operation-active.png')"

            } else {

                $(".btn-resume").attr("disabled", "disabled");
                $(".btn-maintenance").removeAttr("disabled");

                var classnameR = document.getElementsByClassName("btn-resume");
                classnameR[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-resume-operation-disabled.png')";

                var classnameM = document.getElementsByClassName("btn-maintenance");
                classnameM[0].style.backgroundImage = "url('"+themePath+"/images/admin/btn-maintenance-active.png')"
            }
        },
        error: function () {
         //   redirectToErrorPage();
        }
    });
}

