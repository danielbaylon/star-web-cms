/**
 * Created by lavanya on 8/9/16.
 */
var $pop = {};
$pop.paPopDetailTemp = '';
$pop.bounded = 0;

$pop.DistributionModel = function(data){
    var s = this;
    s.percentage = ko.observable(data.percentage);
    s.countryId = ko.observable(data.countryId);
    s.countryNm = ko.observable(data.countryNm);
    s.adminId = ko.observable(data.adminId);
    s.adminNm = ko.observable(data.adminNm);
};


$pop.PartnerModel = function(pa){
    var s = this;
    s.Data = {};
    s.id = ko.observable();
    s.orgName = ko.observable();
    s.accountCode = ko.observable();
    s.branchName = ko.observable();
    s.uen = ko.observable();
    s.licenseNum = ko.observable();
    s.licenseExpDate = ko.observable();
    s.registrationYear = ko.observable();
    s.contactPerson = ko.observable();
    s.contactDesignation = ko.observable();
    s.address = ko.observable();
    s.postalCode = ko.observable();
    s.city = ko.observable();
    s.correspondenceAddress = ko.observable();
    s.correspondencePostalCode = ko.observable();
    s.correspondenceCity = ko.observable();
    s.telNum = ko.observable();
    s.mobileNum = ko.observable();
    s.faxNum = ko.observable();
    s.email = ko.observable();
    s.website = ko.observable();
    s.languagePreference = ko.observable();
    s.mainDestinations = ko.observable();
    s.username =  ko.computed(function() {
        return (this.accountCode()?this.accountCode():'') + "_ADMIN";
    }, this);
    s.countryName = ko.observable();
    s.orgType = ko.observable();
    s.paDocs = ko.observableArray([]);
    s.excluProds = ko.observableArray([]);
    s.tierLabel = ko.observable();
    s.remarks  = ko.observable();
    s.revalFeeStr = ko.observable();
    s.accountManager = ko.observable();
    s.revalPeriodMonths = ko.observable();
    s.dailyTransCapStr = ko.observable();
    s.onlinePaymentEnabled = ko.observable(true);
    s.offlinePaymentEnabled = ko.observable(false);
    s.depoistEnabled = ko.observable(false);
    s.wotReservationEnabled = ko.observable();
    s.subAccountEnabled = ko.observable(false);
    s.canApprove  = ko.observable(($pop.canApprove === true));
    s.showRemarks = ko.observable(($pop.showRemarks === true));
    s.showRejectRemarks = ko.observable(($pop.showRejectRemarks === "true"));
    s.paHist = ko.observableArray([]);
    s.distributionModels = ko.observableArray([]);
    s.createdBy = ko.observable();
    s.createdDateStr = ko.observable();

    s.customerGroupId = ko.observable();
    s.customerGroupLabel = ko.observable();
    s.capacityGroupId = ko.observable();
    s.capacityGroupLabel = ko.observable();
    s.wotReservationCap = ko.observable();
    s.wotReservationValidUntil = ko.observable();
    
    if($pop.applogVmsJson){
        var applogVmsJsonLength = $pop.applogVmsJson.length;
        console.log("$pop.applogVmsJson.length:"+$pop.applogVmsJson.length);
        for (var i = 0; i < applogVmsJsonLength; i++) {
            var tem = $pop.applogVmsJson[i];
            var o = new $pop.PaHistModel(tem);
            s.paHist.push(o);
        }
    }
    else{
        s.paHist([]);
    };
    s.update = function(pa){
        for(var k in pa) s.Data[k]=pa[k];
        s.id(pa.id);
        s.orgName(pa.orgName);
        s.orgType(pa.orgTypeName);
        s.accountCode(pa.accountCode);
        s.branchName(pa.branchName);
        s.uen(pa.uen);
        s.licenseNum(pa.licenseNum);
        s.licenseExpDate(pa.licenseExpDate);
        s.contactPerson(pa.contactPerson);
        s.contactDesignation(pa.contactDesignation);
        s.address(pa.address);
        s.postalCode(pa.postalCode);
        s.city(pa.city);
        s.telNum(pa.telNum);
        s.mobileNum(pa.mobileNum);
        s.faxNum(pa.faxNum);
        s.email(pa.email);
        s.website(pa.website);
        s.languagePreference(pa.languagePreference);
        s.mainDestinations(pa.mainDestinations);
        s.countryName(pa.countryName);
        s.tierLabel(pa.tierLabel);
        s.createdBy(pa.createdBy);
        s.createdDateStr(pa.createdDateStr);
        s.remarks(pa.remarks);
        s.revalFeeStr(pa.revalFeeStr);
        s.accountManager(pa.accountManager);
        s.revalPeriodMonths(pa.revalPeriodMonths);
        s.dailyTransCapStr(pa.dailyTransCapStr);
        s.subAccountEnabled(pa.subAccountEnabled);
        s.offlinePaymentEnabled(pa.offlinePaymentEnabled);
        var exts = pa['extension'];
        if (exts != undefined && exts != null && exts.length > 0) {
            var extlen = exts.length;
            for (var i = 0; i < extlen; i++) {
                var ext = exts[i];
                if (ext != undefined && ext != null && ext != '') {
                    var name = ext['attrName'];
                    var attrVal = ext['attrValue'];
                    if ('onlinePaymentEnabled' == name) {
                        s.onlinePaymentEnabled(attrVal == 'true');
                    }
                    if ('offlinePaymentEnabled' == name) {
                        s.offlinePaymentEnabled(attrVal == 'true');
                    }
                    if ('depoistEnabled' == name) {
                        s.depoistEnabled(attrVal == 'true');
                    }
                    if ('wotReservationEnabled' == name) {
                        s.wotReservationEnabled(attrVal == 'true');
                    }
                    if ('wotReservationCap' == name) {
                        s.wotReservationCap(attrVal);
                    }
                    if ('wotReservationValidUntil' == name) {
                        s.wotReservationValidUntil(attrVal);
                    }
                    if ('correspondenceAddress' == name) {
                        s.correspondenceAddress(attrVal);
                    }
                    if ('correspondencePostalCode' == name) {
                        s.correspondencePostalCode(attrVal);
                    }
                    if ('correspondenceCity' == name) {
                        s.correspondenceCity(attrVal);
                    }
                    if ('customerGroup' == name) {
                        s.customerGroupId(attrVal);
                        s.customerGroupLabel(ext['attrValueLabel']);
                    }
                    if ('capacityGroupId' == name) {
                        s.capacityGroupId(attrVal);
                        s.capacityGroupLabel(ext['attrValueLabel']);
                    }

                }
            }
        }
        s.showRemarks(($pop.showRemarks === true));
        if($pop.applogVmJson){s.createdBy($pop.applogVmJson.createdBy);}else{s.createdBy('');}
        if($pop.applogVmJson){s.createdDateStr($pop.applogVmJson.createdDateStr);}else{s.createdDateStr('')}
        s.paDocs([]);
        if(pa.paDocs){
            var paDocsLength = pa.paDocs.length;
            for (var i = 0; i < paDocsLength; i++) {
                var tem = pa.paDocs[i];
                var o = new $pop.PaDocModel(tem);
                s.paDocs.push(o);
            }
        }else{
            s.paDocs([]);
        }
        s.excluProds([]);
        if(pa.excluProds){
            var excluProdsLength = pa.excluProds.length;
            for (var i = 0; i < excluProdsLength; i++) {
                var tem = pa.excluProds[i];
                var o = new $pop.ProdModel(tem);
                s.excluProds.push(o);
            }
        }else{
            s.excluProds([]);
        }
        s.distributionModels([]);
        if(pa.distributionMapping){
            var pdMappingLength = pa.distributionMapping.length;
            for (var i = 0; i < pdMappingLength; i++) {
                if(pa.distributionMapping[i].percentage){
                    var idata = {percentage:pa.distributionMapping[i].percentage,
                        countryId:pa.distributionMapping[i].id,
                        countryNm:pa.distributionMapping[i].countryNm,
                        adminId:pa.distributionMapping[i].adminId,
                        adminNm:pa.distributionMapping[i].adminNm};
                    var distribution = new $pop.DistributionModel(idata);
                    s.distributionModels.push(distribution);
                }
            }
        }else{
            s.distributionModels([]);
        }
    };
    
};
$pop.PaDocModel = function(data){
    var s = this;
    s.id = ko.observable(data.id);
    s.fileName = ko.observable(data.fileName);
    s.fileTypeLabel = ko.observable(data.fileTypeLabel);
    s.url = ko.observable(nvx.API_PREFIX + '/partner-management/attachment/download?id='+data.id);
};
$pop.PaHistModel = function(data){
    var s = this;
    s.id = ko.observable(data.id);
    s.createdDateStr = ko.observable(data.createdDateStr);
    s.createdBy = ko.observable(data.createdBy);
    s.description = ko.observable(data.description);
};

$pop.ProdModel = function(data){
    var s = this;
    s.displayTitle = ko.observable(data.name);
}

$pop.paModel = new $pop.PartnerModel();
$pop.initPopPage = function(appId){
    if($pop.bounded == 0){
        $('#paDetailDv').html('');
        var tempHtml = $('#paPopDetailTemp').html();
        $pop['paPopDetailTemp'] = kendo.template(tempHtml);
        $('#paDetailDv').html($pop.paPopDetailTemp({}));
        ko.applyBindings($pop.paModel,$('#paDetailDv')[0]);

    }
    if($pop.bounded == 0) {
        $pop.bounded = 1;
    }
    $pop.initPageDisplay(appId);
};

$pop.initPageDisplay = function(appId){
    if(appId != undefined && appId != null && appId != ''){
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/partner-management/get-previous-partner-history',
            data : {
                appId : appId
            },
            success:  function(data, textStatus, jqXHR)
            {

                if(data.success)
                {
                    $pop.bindingPageWithData(data);
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert('Unable to process your request. Please try again in a few moments.');
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    }
};

$pop.bindingPageWithData = function(data){
    data = JSON.parse(data.data);
    $pop.paVmJson = data['partnerVM'];
    $pop.canApprove = data['canApprove'];
    $pop.showRemarks = data['showRemarks'];
    $pop.showRejectRemarks = data['showRejectRemarks'];
    $pop.applogVmJson = data['approvalLogVM'];
    $pop.reasonVmsJson = data['reasonVM'];
    // $pop.paVmJson = paVmJson;
    // if(canApprove){$pop.canApprove = canApprove;};
    // if(showRemarks){$pop.showRemarks = showRemarks;};
    // if(showRejectRemarks){$pop.showRejectRemarks = showRejectRemarks;};
    // if(applogVmJson){$pop.applogVmJson = JSON.parse(applogVmJson);};
    // if(applogVmsJson){$pop.applogVmsJson = JSON.parse(applogVmsJson);};


    $pop.paModel.update($pop.paVmJson);
    $pop.showPopupWindowWithContent();
};


$pop.showPopupWindowWithContent = function(){
    if($pop.detailWindow == null) {
        $pop.detailWindow = $('#paDetailDv').kendoWindow({
            width: '800px',
            modal: true,
            resizable: false,
            title: "Detail",
            actions: ["Close"],
            viewable: false,

        }).data('kendoWindow');
    }
    $pop.detailWindow.center().open();
};

