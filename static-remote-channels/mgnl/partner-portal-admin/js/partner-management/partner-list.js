$(document).ready(function() {
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    var $page = {};
    $page.basePartnerUrl = nvx.API_PREFIX + '/partner-management/get-partner-list';
    $page.thePartnerUrl = $page.basePartnerUrl;

    $page.getFilterJson = function(isQuery){
        var filter = {};
        if(isQuery){
            var pf = $page.pafilter;
            filter.paCode = pf.paCode();
            filter.orgName = pf.orgName();
            filter.contactPerson = pf.contactPerson();
            filter.status = pf.status();
            return JSON.stringify(filter);
        }else{
            filter.paCode = null;
            filter.orgName = null;
            filter.contactPerson = null;
            filter.status = null;
            return null;
        }
    };
    $page.getAdminUserPermissionList = function(){
        var lst = [];
        $.ajax({
            type: "POST",
            async : false,
            url: nvx.API_PREFIX + '/admin-func/get-admin-access-and-approval-rights',
            success:  function(permissionResponse, textStatus, jqXHR)
            {
                if (permissionResponse.success) {
                    var permissions = JSON.parse(permissionResponse['data']);
                    if(permissions != null && permissions.length > 0){
                        lst = permissions;
                    }
                }
            },
            error: function(jqXHR, textStatus){}
        });
        return lst;
    };
    $page.populateUserAccessRights = function(responseList){
        var hasRights = false;
        var permissions = $page.getAdminUserPermissionList();
        for(var i = 0; i < permissions.length ; i ++){
            var item = permissions[i];
            if(item != null && item != '' && item == 'pp-slm-user-accounts'){
                hasRights = true;
                break;
            }
        }
        for(var i = 0 ; i < responseList.length ; i++) {
            responseList[i]['VerifyRight'] = hasRights ? 'Y' : 'N';
        }
        return responseList;
    };
    $page.partnerDS = new kendo.data.DataSource({
        transport: {
            read: {
                url: $page.basePartnerUrl,
                type: 'POST',
                cache: false,
                data : { 'ptfilterJson' :  $page.getFilterJson(false) }
            }
        },
        schema: {
            data:function(response) {
                if(response){
                    var obj = response['data'];
                    if(obj != undefined && obj != null && obj != ''){
                        return $page.populateUserAccessRights(JSON.parse(obj));
                    }
                }
                return [];
            },
            total: 'total',
            model: {
                id: 'id',
                fields: {
                    id: {type: "number"},
                    orgName: {type: 'string'},
                    accountCode: {type: 'string'},
                    email: {type: 'string'},
                    status: {type: 'string'},
                    statusLabel: {type: 'string'},
                    contactPerson: {type: 'string'}
                }
            }
        }, pageSize: 10, serverSorting: true, serverPaging: true
    });
    $page.partnerGrid = $("#partnerGrid").kendoGrid({
        dataSource: $page.partnerDS,
        columns: [{
            title: "Partner Code",
            template:"<a href='javascript:nvxpage.viewDetail(#=id#)'>#=accountCode#</a>",
            sortable: false,
            width: 40
        },{
            field: "orgName",
            title: "Company Name",
            width: 100
        },{
            field: "contactPerson",
            title: "Contact Person",
            width: 40
        },{
            field: "email",
            title: "Email",
            width: 60
        },{
            field: "status",
            template:"#if (statusLabel){##=statusLabel##}#",
            title: "Status",
            width: 40
        },{
            title: "Action",
            template: '#if (status == "PendingVerify" && VerifyRight != null && VerifyRight != "" && VerifyRight == "Y")' +
                      '{#<div style="padding-left: 10px;"><a class="btn btn-success" href="'+(nvx.APP_PREFIX)+'/partner-management/partner-verify.html?paId=#=id#">Verify</a></div>#}' +
                      'else if(status == "Active" || status == "Suspended")' +
                      '{#<div style="padding-left: 10px;"><a class="btn btn-success" href="'+(nvx.APP_PREFIX)+'/partner-management/partner-edit.html?paId=#=id#">Detail</a></div>#}' +
                      'else' +
                      '{#<div style="padding-left: 10px;"><a class="btn btn-success" href="javascript:nvxpage.viewDetail(#=id#)">View</a></div># }#',
            sortable: false,
            width: 40
        }
        ],
        sortable: true, pageable: true
    });
    $page.viewDetail = function(paId){
        $pop.initPopPage(paId);
    };
    $page.PaFilterModel = function(){
        var s = this;
        s.paCode = ko.observable();
        s.orgName = ko.observable();
        s.contactPerson = ko.observable();
        s.status = ko.observable();
        s.search = function(){
            $page.refreshPa(s);
        };
        s.reset = function(){
            s.paCode('');
            s.orgName('');
            s.contactPerson('');
            s.status('');
        };
    }
    $page.pafilter = new $page.PaFilterModel();
    ko.applyBindings($page.pafilter,$('#filterDv')[0]);
    $page.refreshPa = function(pafilter){
        $page.partnerDS.options.transport.read.url = $page.basePartnerUrl;
        $page.partnerDS.options.transport.read.data = { 'ptfilterJson' : $page.getFilterJson(true) };
        $page.partnerDS.read();
    };

    window.nvxpage = $page;
});