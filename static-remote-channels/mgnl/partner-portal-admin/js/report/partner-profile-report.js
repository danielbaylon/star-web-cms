$(document).ready(function() {
    var $page = {};
    $page.basePaProfileUrl = nvx.API_PREFIX+'/report/partner-profile-rpt/search';
    $page.AdminModel = function(data){
        var s = this;
        s.id = ko.observable(data.id);
        s.username = ko.observable(data.username);
    };
    $page.PaStatusModel = function(value,display){
        var self = this;
        self.optionValue = ko.observable(value);
        self.optionText = ko.observable(display);
    };

    
    $page.columns = [{
        field: "rowNumber",
        title: "No.",
        template: "<span class='row-number'></span>",
        sortable: false,
        width: 40
    },{field: "orgName", title: "Organization Name",width: 120},
        {field: "orgTypeName",title: "Nature of <BR/> Business",width: 100},
        {field: "status",title: "Account Status",width: 100},
        {field: "uen", title: "UEN",width: 100},
        {field: "licenseNum",title: "License<BR/>Number",width: 100},
        {field: "licenseExpDate",title: "License<BR/>Expiry<BR/>Date",width: 100},
        {field: "contactPerson",title: "Contact Person (Designation)",template: "#=contactDesignation# #=contactPerson#",width: 200},
        {field: "address",title: "Address",width: 200},
        {field: "postalCode",title: "Postal Code",width: 100},
        {field: "city",title: "City",width: 100},
        {field: "telNum",title: "Telephone",width: 100},
        {field: "faxNum",title: "Fax",width: 100},
        {field: "email",title: "Email",width: 200},
        {field: "website",title: "Website",width: 100},
        {field: "accountManager",title: "Account Manager",width: 120},
        {field: "subAccCount",template: "#=subAccCount#",title: "Number Of Sub Account",width: 150}
    ];
    $page.PartnerModel = function(data){
        var self = this;
        self.id = data.id;
        self.orgName = data.orgName;
    };
    $page.FilterModel = function(){
        var s = this;
        s.orgName = ko.observable();
        s.accStatus = ko.observable();
        s.accMgr = ko.observable();
        s.accMgrs = ko.observableArray([]);

        s.showExport = ko.observable(false);
        s.paStatusOptions = ko.observableArray([
            new $page.PaStatusModel('Active','Active'),
            new $page.PaStatusModel('PendingVerify','PendingVerify'),
            new $page.PaStatusModel('Suspended','Suspended'),
            new $page.PaStatusModel('Pending','Pending'),
            new $page.PaStatusModel('Rejected','Rejected'),
            new $page.PaStatusModel('Ceased','Ceased')
        ]);

        if($page.adminVmsJson){
            var adminsLength = $page.adminVmsJson.length;
            for (var i = 0; i < adminsLength; i++){
                var tem = $page.adminVmsJson[i];
                var o = new $page.AdminModel(tem);
                s.accMgrs.push(o);
            };
        };

        s.selPaVms = $("#selPaVms").kendoMultiSelect({
            dataSource: $page.allPaVmsJson,
            dataTextField: "orgName",
            dataValueField: "id"
        }).data("kendoMultiSelect");

        s.reset = function(){
            s.orgName('');
            s.accStatus('');
            s.accMgr('');
            s.selPaVms.value('');
        };
        s.doExport = function(){
            if(!s.validate()){
                return;
            }
            window.open(nvx.API_PREFIX+'/report/partner-profile-rpt/export'+"?filterJson="+s.groupParam());
        };
        s.groupParam = function(){
            var filterJson = {};
            if(s.orgName()){
                filterJson.orgName = s.orgName();
            }
            if(s.accStatus()){
                filterJson.paStatus = s.accStatus().optionValue();
            }
            if(s.accMgr()){
                filterJson.accMgrId = s.accMgr().id();
                filterJson.accMgrName = s.accMgr().username();
            }
            filterJson.paIds = s.selPaVms.value();
            return JSON.stringify(filterJson);
        };
        s.refreshUrl = function(){
            console.log("refreshUrl.....");
            if(!s.validate()){
                return;
            }
            s.theUrl = $page.basePaProfileUrl;
            $page.paProfileDS.options.transport.read.url = s.theUrl;
            $page.paProfileDS.options.transport.read.data = { 'filterJson' : s.groupParam()};
            $page.paProfileDS.read();
            $page.paProfileDS.page(1);
            if(!$page.resultDv){
                $page.resultDv = $("#resultDv").kendoGrid({
                    dataSource: $page.paProfileDS,
                    dataBound: dataBound,
                    columns: $page.columns ,
                    sortable: true, pageable: true
                });
            }
        };

        s.validate = function(){
            return true;
        }
    };
    function dataBound(e){
        var grid = $("#resultDv").data("kendoGrid");
        var currentPage = $page.paProfileDS.page();
        var pageSize = $page.paProfileDS.pageSize();
        var rows = this.items();
        $(rows).each(function () {
            var index = $(this).index() + 1+(currentPage-1)*pageSize;
            var rowLabel = $(this).find(".row-number");
            $(rowLabel).html(index);
        });
    };

    $page.initDS =function () {
        $page.paProfileUrl = $page.basePaProfileUrl;
        $page.paProfileDS = new kendo.data.DataSource({
            transport: {read: {url: $page.paProfileUrl, cache: false, type: "POST"}},
            schema: {
                data:function(response) {
                    $page.filter.showExport(false);
                    if(response.data.viewModel){
                        var paVms= response.data.viewModel;
                        if(paVms&&paVms.length > 0){
                            $page.filter.showExport(true);
                        }else{
                            $page.filter.showExport(false);
                        }
                        return paVms;
                    }else{
                        return [];
                    }
                },
                total: 'data.total',
                model: {
                    id: 'id',
                    fields: {
                        id: {type: "number"},
                        orgName: {type: 'string'},
                        orgTypeName: {type: 'string'},
                        status: {type: 'string'},
                        uen: {type: 'string'},
                        licenseNum: {type: 'string'},
                        licenseExpDate: {type: 'string'},
                        contactDesignation: {type: 'string'},
                        contactPerson: {type: 'string'},
                        address: {type: 'string'},
                        postalCode: {type: 'number'},
                        city: {type: 'string'},
                        telNum: {type: 'string'},
                        mobileNum: {type: 'string'},
                        faxNum: {type: 'string'},
                        email: {type: 'string'},
                        website: {type: 'string'},
                        accountManager: {type: 'string'},
                        subAccCount: {type: 'string'},
                    }
                }
            }, pageSize: 10, serverSorting: true, serverPaging: true
        });  
    };

    $page.loadPageData = function(data, status, jqXHR) {
        if (data != undefined) {
            var newData = JSON.parse(data);
            $page.adminVmsJson = newData['adminAccountVMs'];
            $page.allPaVmsJson =newData['allPartnerVMs'];
            // $page.sysSettingJson = newData['sysMap'];
            // $page.approverJson = newData['approverVm'];
            // $page.candidatesJson = newData['candidatesList'];
            $page.initDS();
            $page.filter = new $page.FilterModel();
            ko.applyBindings($page.filter,$('#filter')[0]);
        }
    };

    $page.initPageData = function () {
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/report/partner-profile-rpt/init-page',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $page.loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });

    };
    //$($page.tabstrip.items()[2]).attr("style", "display:none");
    window.nvxpage = $page;
    /** Page Loading ajax request, beginning **/
    window.nvxpage.initPageData();
    /** Page Loading ajax request, finished **/
});