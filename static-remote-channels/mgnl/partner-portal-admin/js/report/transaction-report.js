(function(nvx, $) {

    /* Initialization */

    $(document).ready(function() {

        nvx.initHackKendoGridSortForStruts();
        initPageData();
    });

    nvx.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    nvx.GenericErrorMsg = nvx.ErrorSpan.replace('THE_MSG', 'Unable to process your request. Please try again in a few moments or refresh the page.');
    nvx.pageSize = 10;
    nvx.filterView = {};
    nvx.resultView = {};
    nvx.FilterView = function(pf) {
        var s = this;

        /* Main filters */

        s.ktdf = {};
        s.ktdt = {};

        s.filterTdf = ko.observable('');
        s.filterTdt = ko.observable('');

        s.filterPaymentType = ko.observable('');
        s.filterStatus = ko.observable('');
        s.reInitFilterStatus = function(type){
            s.filterStatus('');
            $('#filterStatusDrpList > option').remove();
            $('#filterStatusDrpList').append(new Option('All', ''));
            if(type != undefined && type != null && type != ''){
                if('Offline' == type){
                    $('#filterStatusDrpList').append(new Option('Approved', 'Approved'));
                    $('#filterStatusDrpList').append(new Option('Pending Approval', 'Pending_Approval'));
                    $('#filterStatusDrpList').append(new Option('Pending Submit', 'Pending_Submit'));
                    $('#filterStatusDrpList').append(new Option('Cancelled', 'Cancelled'));
                    $('#filterStatusDrpList').append(new Option('Rejected', 'Rejected'));
                    return;
                }else if('Online' == type){
                    $('#filterStatusDrpList').append(new Option('Success', 'Successful'));
                    $('#filterStatusDrpList').append(new Option('Failed', 'Failed'));
                    $('#filterStatusDrpList').append(new Option('Refunded', 'Refunded'));
                    $('#filterStatusDrpList').append(new Option('Incomplete', 'Incomplete'));
                    return;
                }
            }
        };
        s.filterPaymentType.subscribe(function(item){
            s.reInitFilterStatus(item);
        });

        s.filterStatus = ko.observable('');
        s.filterReceiptNo = ko.observable('');
        s.filterName = ko.observable('');

        /* Additional filters */

        s.pf = pf;
        s.filterProds = ko.observableArray([]);
        s.includeRevals = ko.observable(false);

        /* Actions */

        s.doReset = function() {
            s.reInitFilterStatus();
            s.filterTdf('');
            s.filterTdt('');
            s.filterStatus('');
            s.filterReceiptNo('');
            s.filterName('');

            s.filterPaymentType('');
            s.filterStatus('');

            s.selPaVms.value('');
            $.each(s.filterProds(), function(idx, prod) {
                $.each(prod.items(), function(idx, item) {
                    item.checked(false);
                });
            });
        };

        var isGenerating = false;
        s.genIsErr = ko.observable(false);
        s.genErrMsg = ko.observable('');

        s.showExport = ko.observable(false);

        s.doExport = function() {
            if (s.pkg == null) {
                return;
            }
            window.open(nvx.API_PREFIX + '/report/transaction-rpt/export');
        };

        s.pkg = null;

        s.doGenerate = function() {
            if (isGenerating) {
                return;
            }
            s.showExport(false);
            isGenerating = true;
            s.genIsErr(false);

            var errMsg = '';

            if (s.ktdf.value() == null || s.ktdt.value() == null) {
                errMsg += '<li>Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
            } else if (s.ktdf.value() > s.ktdt.value()) {
                errMsg += '<li>Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
            }

            if (errMsg != '') {
                s.genIsErr(true);
                s.genErrMsg(errMsg);
                isGenerating = false;
                return;
            }

            var itemNames = [];
            $.each(s.filterProds(), function(idx, obj) {
                if (obj.checked()) {
                    $.each(obj.items(), function(idx2, item) {
                        itemNames.push(item.name);
                    });
                } else {
                    $.each(obj.items(), function(idx2, item) {
                        if (item.checked()) {
                            hasProd = true;
                            itemNames.push(item.name);
                        }
                    });
                }
            });

            var inputStatus = null;
            var inputTransMegaStatus = null;
            if('Online' == s.filterPaymentType()){
                inputStatus = null;
                inputTransMegaStatus = s.filterStatus() != undefined && s.filterStatus() != null && s.filterStatus() != '' ? s.filterStatus() : null;
            }else if('Offline' == s.filterPaymentType()){
                inputStatus = s.filterStatus() != undefined && s.filterStatus() != null && s.filterStatus() != '' ? s.filterStatus() : null;
                inputTransMegaStatus = null;
            }

            s.pkg = {
                fromDate: s.filterTdf(),
                toDate: s.filterTdt(),
                orgName: s.filterName(),
                receiptNum: s.filterReceiptNo(),
                status : inputStatus,
                transMegaStatus: inputTransMegaStatus,
                itemNames: itemNames,
                revalItem: s.includeRevals() ? 'include-reval' : '',
                paIds: s.selPaVms.value(),
                paymentType: s.filterPaymentType()
            };

            nvx.resultView.init(s.pkg);
            s.showExport(true);
            isGenerating = false;
        };

        /* Initialization */

        function init() {
            s.ktdf = $('#filterTdf').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');
            s.ktdt = $('#filterTdt').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');

            s.selPaVms = $("#selPaVms").kendoMultiSelect({
                dataSource: nvx.allPaVmsJson,
                dataTextField: "orgName",
                dataValueField: "id"
            }).data("kendoMultiSelect");

            $.each(pf, function(idx, obj) {
                var prod = new nvx.MainOpt(obj);
                s.filterProds.push(prod);
            });
        }
        init();
    };

    nvx.ResultView = function(recsPerPage) {
        var s = this;

        s.recsPerPage = recsPerPage ? recsPerPage : nvx.pageSize;

        s.theGrid = null;

        s.init = function(pkg) {
            if (s.theGrid != null) {
                s.theGrid.destroy();
                $('#resultGrid').remove();
                s.theGrid = null;
            }

            var $anchor = $('#resultMarker');
            $anchor.after('<div class="data_grid grid-wrapper" id="resultGrid"></div>');
            var initialLoad = true;
            s.theGrid = $('#resultGrid').kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: nvx.API_PREFIX + '/report/transaction-rpt/search',
                            data: {
                                filterJson: JSON.stringify(pkg)
                            },
                            method: 'post',
                            cache: false
                        }
                    },
                    schema: {
                        data: 'data.viewModel',
                        total: 'data.total',
                        model: {
                            id: 'id',
                            fields: {
                                orgName: {type: 'string'},
                                receiptNum: {type: 'string'},
                                itemDesc: {type: 'string'},
                                qty: {type: 'int'},
                                purchasedBy: {type: 'string'},
                                revalidateReference: {type: 'string'},
                                transStatusText: {type: 'string'},
                                remarks: {type: 'string'},
                                txnDateText: {type: 'string'},
                                expiryDateText: {type: 'string'},
                                revalExpiryDateText: {type: 'string'},
                                priceText: {type: 'string'},
                                totalAmountText: {type: 'string'},
                                paymentType: {type: 'string'},
                                offlinePaymentStatus: {type: 'string'},
                                offlinePaymentId: {type: 'string'},
                                paymentDateText : {type : 'string'},
                                offlinePayApproveDateText : {type : 'string'}
                            }
                        }
                    }, pageSize: s.recsPerPage, serverSorting: true, serverPaging: true,
                    requestStart: function () {
                        if (initialLoad){
                            nvx.spinner.start();
                        }
                    },
                    requestEnd: function (e) {
                        if(e){
                            if(e.response){
                                if(e.response.data.viewModel){
                                    var lst = e.response.data.viewModel;
                                    if(lst != undefined && lst != null && lst.length > 0){
                                        var len = lst.length;
                                        for(var i  = 0  ; i < len ; i++){
                                            var item = lst[i];
                                            if(item){
                                                var offlinePaymentStatus = item['offlinePaymentStatus'];
                                                var offlinePaymentId = item['offlinePaymentId'];
                                                if(offlinePaymentId != undefined && offlinePaymentId != null && offlinePaymentId != '' ){
                                                    if(offlinePaymentStatus != undefined && offlinePaymentStatus != null && offlinePaymentStatus != ''){
                                                        offlinePaymentStatus = offlinePaymentStatus.replace('_', ' ');
                                                        item['transStatusText'] = offlinePaymentStatus;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(initialLoad){
                            nvx.spinner.stop();
                            initialLoad = false;
                        }
                    }
                },
                columns: [
                    { field: 'orgName', title: 'Organization Name', width: 150, sortable: true },
                    { field: 'receiptNum', title: 'Receipt No.', width: 150, sortable: true },
                    { field: 'txnDateText', title: 'Transaction Date', width: 150, sortable: true },
                    { field: 'transStatusText', title: 'Transaction Status', width: 150, sortable: true },
                    { field: "paymentType", title: "Payment Type", width: 150, sortable: false },
                    { field: 'paymentDateText', title: 'Payment Date (Offline)', width: 150, sortable: false },
                    { field: 'referenceNum', title: 'Payment Reference Number', width: 200, sortable: false },
                    { field: 'offlinePayApproveDateText', title: 'Approval Date (Offline)', width: 150, sortable: false },
                    { field: 'expiryDateText', title: 'Expiry Date', width: 150, sortable: true },
                    { field: 'revalExpiryDateText', title: 'Revalidated Expiry Date', width: 150, sortable: true },
                    { field: 'itemDesc', title: 'Item Description', width: 150, sortable: true },
                    { field: 'qty', title: 'Qty. Purchased', width: 150, sortable: false },
                    { field: 'priceText', title: 'Unit Price (S$)', width: 150, sortable: false },
                    { field: 'totalAmountText', title: 'Total Amount (S$)', width: 150, sortable: false },
                    { field: 'purchasedBy', title: 'Purchased By', width: 150, sortable: true },
                    { field: 'revalidateReference', title: 'Revalidation Ref. No.', width: 150, sortable: true },
                    { field: 'remarks', title: 'Remarks', width: 150, sortable: false },
                    { title: 'Actions', width: 150 }
                ],
                dataBound: nvx.gridNoDataDisplay, sortable: true, pageable: true, resizable: true
            }).data('kendoGrid');
        };
    };

    nvx.MainOpt = function(theBase) {
        var s = this;

        s.fromChild = false;
        s.childAffected = true;
        s.checked = ko.observable(false);
        s.id = theBase.productId;
        s.name = theBase.prodName;
        s.type = ko.observable(theBase.type);

        s.items = ko.observableArray([]);
        if (theBase.itemNames) {
            $.each(theBase.itemNames, function(idx, obj) {
                var item = new nvx.ItemOpt(s, obj);
                s.items.push(item);
            });
        }

        s.checked.subscribe(function(newValue) {
            if (!s.fromChild && s.items().length > 0) {
                s.childAffected = false;
                $.each(s.items(), function(idx, item) {
                    item.checked(newValue);
                });
                s.childAffected = true;
            }
        });
    };

    nvx.ItemOpt = function(parent, itemName) {
        var s = this;

        s.parent = parent;
        s.checked = ko.observable(false);
        s.name = itemName;

        s.checked.subscribe(function(newValue) {
            if (!newValue && s.parent.childAffected) {
                s.parent.fromChild = true;
                s.parent.checked(false);
                s.parent.fromChild = false;
            }
        });
    };
    loadPageData = function(data, status, jqXHR) {
        if (data != undefined) {
            var newData = JSON.parse(data);
            nvx.allPaVmsJson =newData['allPartnerVMs'];
            var recsPerPage = newData['recsPerPage'];
            var pf = newData['itemsList'];
            nvx.resultView = new nvx.ResultView(recsPerPage);
            nvx.filterView = new nvx.FilterView(pf);
            ko.applyBindings(nvx.filterView, document.getElementById('filterView'));
        }
    };

    initPageData = function () {
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/report/transaction-rpt/init-page',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                   loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };

})(window.nvx = window.nvx || {}, jQuery);