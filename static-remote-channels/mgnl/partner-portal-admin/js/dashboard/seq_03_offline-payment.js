var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

nvx.OfflinePaymentModel = function(){
    var s = this;
    s.id = ko.observable();
    s.transId = ko.observable();
    s.status = ko.observable();
    s.receiptNumber = ko.observable();
    s.paymentTypes = ko.observableArray([]);
    s.countries = ko.observableArray([]);
    s.txnPaymentType = ko.observable();
    s.validityStartDateStr = ko.observable();
    s.validityEndDateStr = ko.observable();
    s.displayStatus = ko.observable();
    s.tmStatus = ko.observable(' - ');

    s.paymentType = ko.observable();
    s.bankName = ko.observable().extend({ maxlength: 200 });
    s.bankCountry = ko.observable();
    s.referenceNum = ko.observable().extend({ maxlength: 200 });
    s.paymentDateStr = ko.observable();
    s.remarks = ko.observable('').extend({ maxlength: 1000 });
    s.hasChatHists = ko.observable(false);
    s.totalAmountStr = ko.observable();
    s.createDtStr = ko.observable();
    s.offlinePaymentApprovalDateText = ko.observable();
    s.chatHists  = ko.observableArray([]);
    s.nonEmptyString = function(str){
        if(str != undefined && str != null && str != ''){
            return str;
        }else{
            return '';
        }
    };

    s.reset = function(){
        s.id('');
        s.transId('');
        s.status('');
        s.receiptNumber('');
        s.paymentTypes([]);
        s.countries([]);
        s.paymentType('');
        s.bankName('');
        s.bankCountry('');
        s.referenceNum('');
        s.paymentDateStr('');
        s.remarks('');
        s.totalAmountStr('');
        s.createDtStr('');
        s.offlinePaymentApprovalDateText('');
        s.txnPaymentType('');
        s.validityStartDateStr('');
        s.validityEndDateStr('');
        s.displayStatus('');
        s.chatHists([]);
    };

    s.initOfflinePaymentModel = function(data){
        s.id(s.nonEmptyString(data['id']));
        s.transId(s.nonEmptyString(data['transId']));
        s.status(s.nonEmptyString(data['status']));
        s.receiptNumber(s.nonEmptyString(data['receiptNum']));
        if('Pending_Submit' == s.status()){
            s.paymentTypes(data['paymentTypes']);
            s.countries(data['countries']);
        }
        s.paymentType(s.nonEmptyString(data['paymentType']));
        s.bankName(s.nonEmptyString(data['bankName']));
        s.bankCountry(s.nonEmptyString(data['bankCountry']));
        s.referenceNum(s.nonEmptyString(data['referenceNum']));
        s.paymentDateStr(s.nonEmptyString(data['paymentDateStr']));
        s.remarks(s.nonEmptyString(data['remarks']));
        s.totalAmountStr(s.nonEmptyString(data['totalAmountStr']));
        s.createDtStr(s.nonEmptyString(data['createDtStr']));
        s.offlinePaymentApprovalDateText(s.nonEmptyString(data['approvalDateText']));
        s.txnPaymentType(data['txnPaymentType']);
        s.validityStartDateStr(data['validityStartDateStr']);
        s.validityEndDateStr(data['validityEndDateStr']);
        s.displayStatus(data['displayStatus']);

        s.chatHists.removeAll();
        if(data != null && data.chatHists != null){
            $.each(data.chatHists, function(key, item) {
                s.hasChatHists(true);
                s.chatHists.push(new nvx.ChatHist(item));
            });
        }else{
            s.hasChatHists(false);
        }
    };
};

nvx.ChatHist = function(data){
    var s = this;
    s.username = ko.observable(data.username);
    s.content = ko.observable(data.content);
    s.createDtStr = ko.observable(data.createDtStr);
    s.usertype = ko.observable(data.usertype);
};