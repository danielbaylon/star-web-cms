(function(nvx, $) {
    /*
     * Cart
     */
    $(document).ready(function() {

        if(document.getElementById('cartItemNum') != null) {
            nvx.cartViewModel = new nvx.CartViewModel();
            ko.applyBindings(nvx.cartViewModel, document.getElementById('cartItemNum'));
            nvx.cartViewModel.getCart();
        }

    });

    nvx.cartViewModel = null;

    nvx.CartViewModel = function() {
        var s = this;

        s.baseModel = null; //This base model is not guaranteed to be updated.

        s.totalQty = ko.observable(0);
        s.totalMainQty = ko.observable(0);

        s.getCart = function() {
            $.ajax({
                url: nvx.API_PREFIX + '/secured/store-partner/get-cart-size',
                data: {}, headers: { 'Store-Api-Channel' : 'partner-portal-slm' },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        s.totalQty(data.data);
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };
    };

})(window.nvx = window.nvx || {}, jQuery);