(function(nvx, $) {
    nvx.API_PREFIX = "/.store/ppslm";
    nvx.API_CHANNEL = "partner-portal-slm";
    nvx.ROOT_PATH = "/partner-portal-slm";
})(window.nvx = window.nvx || {}, jQuery);