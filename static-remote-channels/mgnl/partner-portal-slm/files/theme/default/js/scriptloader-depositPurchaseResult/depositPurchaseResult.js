(function(nvx, $, ko) {
    $(document).ready(function() {
        $('#btnDepositAgain').click(function(){
            window.location.href = "/partner-portal-slm/deposit-purchase";
        });
        nvx.rsModel = new nvx.DepositPurchaseResultModel();
        ko.applyBindings(nvx.rsModel, document.getElementById('resultDiv'));
        nvx.rsModel.syncHistory();
    });

    nvx.DepositPurchaseResultModel = function() {
        var s = this;
        s.errVisible = ko.observable(false);
        s.errMsg = ko.observable('');
        s.rsMsgVisible = ko.observable(false);
        s.rsMsg = ko.observable('');

        s.reset = function(){
            s.errVisible(false);
            s.errMsg('');
            s.rsMsgVisible(false);
            s.rsMsg('');
            $('#depositBalanceDiv').hide();
        }

        s.syncHistory = function(){
            s.reset();
            $.ajax({
                type: "POST",
                url:  nvx.API_PREFIX + '/secured/partner-deposit/post-deposit-purchase-process',
                headers: { 'Store-Api-Channel' : 'partner-portal-slm' },
                cache: false, dataType: 'json', contentType: 'application/json',
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success) {
                        s.rsMsg(data['data']);
                        s.rsMsgVisible(true);
                        $('#depositBalanceDiv').show();
                    } else {
                        s.errMsg(data['message']);
                        s.errVisible(true);
                    }
                },
                error: function(jqXHR, textStatus) {
                    s.errMsg('Unable to process get deposit balance. Please try again in a few moments.');
                    s.errVisible(true);
                    console.log('Unable to process get deposit balance. Please try again in a few moments.');
                },
                complete: function() {
                }
            });
        }
    };
})(window.nvx = window.nvx || {}, jQuery, ko);
