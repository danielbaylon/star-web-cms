(function(nvx, $) {

    /* Initialization */

    $(document).ready(function() {
        nvx.initHackKendoGridSortForStruts();

       initPageData();
    });

    nvx.ErrorSpan = '<span style="color:#D30707;">THE_MSG</span>';
    nvx.GenericErrorMsg = nvx.ErrorSpan.replace('THE_MSG', 'Unable to process your request. Please try again in a few moments or refresh the page.');
    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';
    nvx.filterView = {};
    nvx.resultView = {};

    nvx.FilterView = function(pf) {
        var s = this;

        /* Main filters */

        s.ktdf = {};
        s.ktdt = {};

        s.filterTdf = ko.observable('');
        s.filterTdt = ko.observable('');
        s.filterTicketMedia = ko.observable('');
        /* Additional filters */

        s.pf = pf;
        s.filterProds = ko.observableArray([]);

        /* Actions */

        s.doReset = function() {
            s.filterTdf('');
            s.filterTdt('');
            s.filterTicketMedia('');
            $.each(s.filterProds(), function(idx, prod) {
                $.each(prod.items(), function(idx, item) {
                    item.checked(false);
                });
            });
        };

        var isGenerating = false;
        s.genIsErr = ko.observable(false);
        s.genErrMsg = ko.observable('');

        s.showExport = ko.observable(false);

        s.doExport = function() {
            if (s.pkg == null) {
                return;
            }
            window.open(nvx.API_PREFIX + '/secured/report/package-rpt/export');
        };

        s.pkg = null;

        s.doGenerate = function() {
            if (isGenerating) {
                return;
            }
            s.showExport(false);
            isGenerating = true;
            s.genIsErr(false);

            var errMsg = '';

            if (s.ktdf.value() == null || s.ktdt.value() == null) {
                errMsg += '<li>Both Transaction Date (From) and (To) fields are required and must be valid dates.</li>';
            } else if (s.ktdf.value() > s.ktdt.value()) {
                errMsg += '<li>Transaction Date To date must be equal to or later than the Transaction Date From date.</li>';
            }

            if (errMsg != '') {
                s.genIsErr(true);
                s.genErrMsg(errMsg);
                isGenerating = false;
                return;
            }

            var itemNames = [];
            $.each(s.filterProds(), function(idx, obj) {
                if (obj.checked()) {
                    $.each(obj.items(), function(idx2, item) {
                        itemNames.push(item.name);
                    });
                } else {
                    $.each(obj.items(), function(idx2, item) {
                        if (item.checked()) {
                            hasProd = true;
                            itemNames.push(item.name);
                        }
                    });
                }
            });

            s.pkg = {
                fromDate: s.filterTdf(),
                toDate: s.filterTdt(),
                ticketMedia: s.filterTicketMedia(),
                itemNames: itemNames
            };

            nvx.resultView.init(s.pkg);
            s.showExport(true);
            isGenerating = false;
        };

        /* Initialization */

        function init() {
            s.ktdf = $('#filterTdf').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');
            s.ktdt = $('#filterTdt').kendoDatePicker({
                format: nvx.DATE_FORMAT
            }).data('kendoDatePicker');

            $.each(pf, function(idx, obj) {
                var prod = new nvx.MainOpt(obj);
                s.filterProds.push(prod);
            });
        }
        init();
    };

    nvx.ResultView = function(recsPerPage) {
        var s = this;

        s.recsPerPage = recsPerPage ? recsPerPage : nvx.pageSize;

        s.theGrid = null;

        s.init = function(pkg) {
            if (s.theGrid != null) {
                s.theGrid.destroy();
                $('#resultGrid').remove();
                s.theGrid = null;
            }

            var $anchor = $('#resultMarker');
            $anchor.after('<div class="data_grid grid-wrapper" id="resultGrid"></div>');
            var initialLoad = true;
            s.theGrid = $('#resultGrid').kendoGrid({
                dataSource: {
                    transport: {read: {
                        url: nvx.API_PREFIX + '/secured/report/package-rpt/search',
                        data: {
                            filterJson: JSON.stringify(pkg)
                        },
                        method: 'post',
                        cache: false
                    }},
                    schema: {
                        data: 'data.viewModel',
                        total: 'data.total',
                        model: {
                            id: 'id',
                            fields: {
                                pinCode: {type: 'string'},
                                packageName: {type: 'string'},
                                packageDesc: {type: 'string'},
                                pinCodeQty: {type: 'int'},
                                qtyRedeemed: {type: 'int'},
                                indivItems: {type: 'string'},
                                indivReceipts: {type: 'string'},
                                ticketMedia: {type: 'string'},
                                ticketGeneratedDateText: {type: 'string'},
                                lastRedemptionDateText: {type: 'string'},
                                expiryDateText: {type: 'string'}
                            }
                        }
                    }, pageSize: s.recsPerPage, serverSorting: true, serverPaging: true,
                    requestStart: function () {
                        if (initialLoad){
                            nvx.spinner.start();
                        }
                    },
                    requestEnd: function () {
                        if(initialLoad){
                            nvx.spinner.stop();
                            initialLoad = false;
                        }
                    }
                },
                columns: [
                    { field: 'pinCode', title: 'Ticket Media', width: 150, sortable: false, template: function(data) {
                        if(data.ticketMedia == 'Pincode') {
                            return 'PINCODE <br/>' + data.pinCode + '';
                        }else if (data.ticketMedia == 'ETicket') {
                            return 'E-Ticket';
                        }else if (data.ticketMedia == 'ExcelFile') {
                            return 'Excel File';
                        }
                    } },
                    { field: 'ticketGeneratedDateText', title: 'Ticket Generation Date', width: 150, sortable: true },
                    { field: 'expiryDateText', title: 'Expiry Date', width: 150, sortable: true },
                    { field: 'packageName', title: 'Package Name', width: 150, sortable: false },
                    { field: 'packageDesc', title: 'Package Description', width: 150, sortable: false },
                    { field: 'indivItems', title: 'Individual Product(s)', width: 150, sortable: false,
                        template: '<div>#=indivItems#</div>'},
                    { field: 'indivReceipts', title: 'Sales Receipt No(s).', width: 150, sortable: false },
                    { field: 'itemQty', title: 'Package Quantity', width: 150, sortable: false },
                    { field: 'itemQtyRedeemed', title: 'Quantity Redeemed', width: 150, sortable: false },
                    { field: 'lastRedemptionDateText', title: 'Last Redeemed Date', width: 150, sortable: true }
                ],
                dataBound: nvx.gridNoDataDisplay, sortable: true, pageable: true, resizable: true
            }).data('kendoGrid');
        };
    };

    nvx.MainOpt = function(theBase) {
        var s = this;

        s.fromChild = false;
        s.childAffected = true;
        s.checked = ko.observable(false);
        s.id = theBase.productId;
        s.name = theBase.prodName;
        s.type = ko.observable(theBase.type);

        s.items = ko.observableArray([]);
        if (theBase.itemNames) {
            $.each(theBase.itemNames, function(idx, obj) {
                var item = new nvx.ItemOpt(s, obj);
                s.items.push(item);
            });
        }

        s.checked.subscribe(function(newValue) {
            if (!s.fromChild && s.items().length > 0) {
                s.childAffected = false;
                $.each(s.items(), function(idx, item) {
                    item.checked(newValue);
                });
                s.childAffected = true;
            }
        });
    };

    nvx.ItemOpt = function(parent, itemName) {
        var s = this;

        s.parent = parent;
        s.checked = ko.observable(false);
        s.name = itemName;

        s.checked.subscribe(function(newValue) {
            if (!newValue && s.parent.childAffected) {
                s.parent.fromChild = true;
                s.parent.checked(false);
                s.parent.fromChild = false;
            }
        });
    };

    loadPageData = function(data, status, jqXHR) {
        if (data != undefined) {
            var newData = JSON.parse(data);
            nvx.allPaVmsJson =newData['allPartnerVMs'];
            var recsPerPage = newData['recsPerPage'];
            var pf = newData['itemsList'];
            nvx.resultView = new nvx.ResultView(recsPerPage);
            nvx.filterView = new nvx.FilterView(pf);
            ko.applyBindings(nvx.filterView, document.getElementById('filterView'));
        }
    };

    initPageData = function () {
        $.ajax({
            type: "GET",
            url: nvx.API_PREFIX + '/secured/report/package-rpt/init-page',
            data: {},
            beforeSend: function () {
                nvx.spinner.start();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    loadPageData(data['data']);
                } else {
                    if (data.message != null && data.message != '') {
                        var errorMsg = nvx.getDefinedMsg(data.message);
                        if (errorMsg != undefined && errorMsg != '') {
                            alert(errorMsg);
                        } else {
                            alert(data.message);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus) {
                alert(ERROR_MSG);
            },
            complete: function () {
                nvx.spinner.stop();
            }
        });
    };

})(window.nvx = window.nvx || {}, jQuery);