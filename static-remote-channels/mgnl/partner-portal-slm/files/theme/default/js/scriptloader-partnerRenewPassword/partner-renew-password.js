$(document).ready(function() {
    nvx.renew = new nvx.RenewModel(nvx.getPreferDefinedMsg(warnMsg), nvx.getPreferDefinedMsg(errMsg));
    ko.applyBindings(nvx.renew, document.getElementById('renew-password-form'));
});

(function(nvx, $) {
    nvx.RenewModel = function(warnMsg, errMsg) {
        var s = this;

        s.pw = ko.observable();
        s.newPw = ko.observable();
        s.newPw2 = ko.observable();

        s.warnMsg = ko.observable((warnMsg && warnMsg !== '') ? warnMsg + '<br>' : '');
        s.isErr = ko.observable(errMsg ? true : false);
        s.errMsg = ko.observable(errMsg);

        s.doRenew = function() {
            var pw = s.pw();
            var newPw = s.newPw();
            var newPw2 = s.newPw2();
            if (nvx.isEmpty(pw) || nvx.isEmpty(newPw) || nvx.isEmpty(newPw2)) {
                s.errMsg(nvx.getPreferDefinedMsg('AllFieldsRequired'));
                s.isErr(true);
                return false;
            }
            if (newPw != newPw2) {
                s.errMsg(nvx.getPreferDefinedMsg('RenewPassNotMatch'));
                s.isErr(true);
                return false;
            }
            if (/^\s|\s$/.test(newPw)) {
                s.errMsg(nvx.getPreferDefinedMsg('PasswordLeadingTrailing'));
                s.isErr(true);
                return false;
            }
            if (!nvx.isLengthWithin(newPw, 8, 150, true)
                || !nvx.hasUppercase(newPw) || !nvx.hasLowercase(newPw)
                || !nvx.hasNumber(newPw) || !nvx.hasNonAlphanumeric(newPw)) {
                s.errMsg(nvx.getPreferDefinedMsg('PasswordPolicyNotMet'));
                s.isErr(true);
                return false;
            }

            return true;
        };

        s.doCancel = function() {
            window.location.href = nvx.ROOT_PATH + '/dashboard';
        };

        s.doLogout = function() {
            window.location.href = nvx.API_PREFIX + '/publicity/logout';
        };
    };
})(window.nvx = window.nvx || {}, jQuery);