(function(nvx, $) {
	ko.bindingHandlers.fadeVisible = {
		init: function(element, valueAccessor) {
			// Initially set the element to be instantly visible/hidden depending on the value
			var value = valueAccessor();
			$(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
		},
		update: function(element, valueAccessor) {
			// Whenever the value subsequently changes, slowly fade the element in or out
			var value = valueAccessor();
			ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).hide();
		}
	};

	ko.bindingHandlers.slideVisible = {
		init: function(element, valueAccessor) {
			// Initially set the element to be instantly visible/hidden depending on the value
			var value = valueAccessor();
			$(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
		},
		update: function(element, valueAccessor) {
			// Whenever the value subsequently changes, slowly fade the element in or out
			var value = valueAccessor();
			ko.utils.unwrapObservable(value) ? $(element).slideDown() : $(element).fadeOut(100);
		}
	};

	nvx.DATE_FORMAT = 'dd/MM/yyyy';

	nvx.theNav = {};
	nvx.initNav = function(elemId) {
		nvx.theNav = new nvx.NavModel(nvx.navData);
		ko.applyBindings(nvx.theNav, document.getElementById(elemId));
	};

	nvx.NavModel = function(navData) {
		var s = this;

		s.rights = navData != undefined && navData != null ? navData['rights'] : null;

		s.currNav = ko.observable(navData != undefined && navData != null ? navData.currNav : null);
		s.currSubNav = ko.observable(navData != undefined && navData != null ? currSubNav : null);

		s.hasNavRights = function(right) {
			var hasRight = false;
			$.each(s.rights, function(idx, val) {
				hasRight = (right == val);
				if (hasRight) return false;
			});
			return hasRight;
		};

		s.stickied = ko.observable('');

		s.stickyThis = function(right) {
			s.stickied(s.stickied() == right ? '' : right);
		};

		s.switchNav = function(navToSwitch) {
			s.currNav(navToSwitch);
			$('#' +  navToSwitch +' > li:first').click();
		};

		s.navigateTo = function(link) {
			window.location.href = link;
		};
	};

	nvx.toggleAlert = function(toggle, elemId, text, isSuccess) {
		if (toggle) {
			$('#' + elemId).text(text).addClass(isSuccess ? 'alert-success' : 'alert-error').show();
		} else {
			$('#' + elemId).hide().removeClass('alert-success').removeClass('alert-error').text('');
		}
	};

	nvx.spinnerOpts = {
		lines: 13, // The number of lines to draw
		length: 16, // The length of each line
		width: 4, // The line thickness
		radius: 10, // The radius of the inner circle
		rotate: 0, // The rotation offset
		color: '#000', // #rgb or #rrggbb
		speed: 1.2, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: true, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: 'auto', // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};

	nvx.spinner = new function() {
		var s = this;
		s.spinner = Spinner(nvx.spinnerOpts);
		s.elemId = 'spinner-overlay';
		s.start = function() {
			$('#' + s.elemId).show();
			s.spinner.spin(document.getElementById(s.elemId));
		};
		s.stop = function() {
			s.spinner.stop();
			$('#' + s.elemId).hide();
		};
	};

	nvx.MultiInputModel = function(elemId) {
		var s = this;
		s.elemId = elemId;

		s.showMsg = ko.observable(false);
		s.isErr = ko.observable(false);
		s.theMsg = ko.observable('');

		s.toggleMsg = function(showMsg, isErr, theMsg) {
			if (!showMsg) {
				s.showMsg(false); return;
			}
			s.isErr(isErr);
			s.theMsg(theMsg);
			s.showMsg(true);
		};

		s.title = ko.observable('');

		s.rows = ko.observableArray([]);
		s.RowModel = function() {
			var s2 = this;
			s2.rowVal = ko.observable('');
			s2.rowIdx = ko.observable(0);
			s2.init = function(val, idx) {
				s2.rowVal(val);
				s2.rowIdx(idx);
			};
		};

		s.addRow = function() {
			var rm = new s.RowModel();
			rm.rowIdx(s.rows().length);
			s.rows.push(rm);
		};

		s.removeRow = function() {
			s.rows.remove(this);
			$.each(s.rows(), function(idx, val) {
				val.rowIdx(idx);
			});
		};

		s.init = function(title, valsArr) {
			s.title(title);
			if (valsArr.length == 0) {
				var rm = new s.RowModel();
				s.rows.push(rm);
			}
			$.each(valsArr, function(idx, val) {
				var rm = new s.RowModel();
				rm.init(val, idx);
				s.rows.push(rm);
			});
		};

		s.doUpdate = function() {
			//Replace with a function of your choosing.
		};
	};

	/* Hacks and Fixes for various compatible libraries */
	nvx.initHackKendoGridSortForStruts = function() {
		$(document).ajaxSend(function(e, xhr, settings) {
			settings.url = settings.url.replace(/%5Bfield%5D/g, '.field').replace(/%5Bdir%5D/g, '.dir');
			if (settings.data) {
				settings.data = settings.data.replace(/%5Bfield%5D/g,".field").replace(/%5Bdir%5D/g,".dir");
			}
		});
	};

	nvx.gridNoDataDisplay = function(e) {
		var grid = e.sender;
		var gridHasData = grid.dataSource.data().length > 0;
		if (!gridHasData) {
			var columnCount = $(grid.thead.get(0)).children("tr").first().children("th").length;
			$(grid.tbody.get(0)).append(
				kendo.format("<tr class='custom-no-data-row'><td colspan='{0}'>No records found.</td></tr>", columnCount)
			);
		}
	};


	nvx.Grid = function(gridId, modalId, formId) {
		var s = this;
		s.gridId = gridId;
		s.modalId = modalId;
		s.formId = formId;
		s.kGrid = {};

		initGrid();
		function initGrid() {

		}

		s.modal = null;
		s.model = null;

		s.showModal = function(id) {

		};

		s.cleanupModal = function() {

		};
	};

	nvx.Model = function(kWin, kGrid) {
		var s = this;
		s.kWin = kWin;
		s.kGrid = kGrid;

		s.init = function(data) {

		};

		s.clear = function() {

		};

		s.clearErrors = function() {

		};

		s.doCancel = function() {

		};

		s.doSave = function() {

		};

		s.validate = function() {

		};
	};

	nvx.ShowConfirmDialog = function(title, msg, callback, cancelCallback) {
		if($("#confirmDIVContainer").size() == 0)
		{
			var html = "<div id='confirmDIVContainer'>";
			html += "<table style='width:80%; margin: 0 auto;'><tr><td style='text-align:center;'><div id = 'confirmDIVMSGContiner' style='height:150px;'></div></td></tr>";
			html += "<tr><td style='text-align:center;'><a class='btn btn-primary' id='confirm_yes_btn' style='margin-right:10px;'>Confirm</a><a class='btn' id='confirm_no_btn'>Cancel</a></td></tr>";
			html += "</table></div>";

			var div = $(html);
			$('body').append(div);
		}
		$("#confirmDIVMSGContiner").html(msg);

		if(!$("#confirmDIVContainer").data("kendoWindow"))
		{
			$("#confirmDIVContainer").kendoWindow({
				width: "400px",
				height: "220px",
				visible: false,
				modal: true,
				actions: []
			}).data("kendoWindow").open().center().title(title);
			$("#confirmDIVContainer").parent().find(".k-window-action").hide();
			$("#confirmDIVContainer_wnd_title").css('height', '25px');
		}
		else
		{
			var confirmWindow = $("#confirmDIVContainer").data("kendoWindow");
			confirmWindow.open().center().title(title);
			$("#confirm_yes_btn").unbind('click');
			$("#confirm_no_btn").unbind('click');
		}
		$("#confirm_no_btn").click(function(){
			$("#confirmDIVContainer").data("kendoWindow").close();
			if (cancelCallback != undefined || cancelCallback != null) {
				cancelCallback();
			}
		});
		$("#confirm_yes_btn").click(function(){
			$("#confirmDIVContainer").data("kendoWindow").close();
			if (callback != undefined || callback != null) {
				callback();
			}
		});
	};

	nvx.ShowInfoDialog = function(title, msg, callback) {
		if($("#infoDIVContainer").size() == 0)
		{
			var html = "<div id='infoDIVContainer'>";
			html += "<table style='width:80%; margin: 0 auto;'><tr><td style='text-align:center;'><div id = 'infoDIVMSGContiner' style='height:150px;'></div></td></tr>";
			html += "<tr><td style='text-align:center;'><a class='btn' id='info_close_btn' style='margin-right:10px;'>Close</a></td></tr>";
			html += "</table></div>";

			var div = $(html);
			$('body').append(div);
		}
		$("#infoDIVMSGContiner").html(msg);

		if(!$("#infoDIVContainer").data("kendoWindow"))
		{
			$("#infoDIVContainer").kendoWindow({
				width: "400px",
				height: "220px",
				visible: false,
				modal: true,
				actions: []
			}).data("kendoWindow").open().center().title(title);
			$("#info_close_btn").click(function(){
				$("#infoDIVContainer").data("kendoWindow").close();
				if (callback != undefined || callback != null) {
					callback();
				}
			});
			$("#infoDIVContainer").parent().find(".k-window-action").hide();
			$("#infoDIVContainer_wnd_title").css('height', '25px');
		}
		else
		{
			var confirmWindow = $("#infoDIVContainer").data("kendoWindow");
			confirmWindow.open().center().title(title);
		}
	};

	ko.bindingHandlers.fadeVisible = {
		init: function(element, valueAccessor) {
			// Initially set the element to be instantly visible/hidden depending on the value
			var value = valueAccessor();
			$(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
		},
		update: function(element, valueAccessor) {
			// Whenever the value subsequently changes, slowly fade the element in or out
			var value = valueAccessor();
			ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
		}
	};

	nvx.ClearWordCharacters = function(str)
	{
		str = str.replace(/<style>[\s\S]*<\/style>/, "");
		str = str.replace(/<xml>[\s\S]*<\/xml>/,"");
		str = str.replace(/<o:p>\s*<\/o:p>/g, "") ;
		str = str.replace(/<o:p>.*?<\/o:p>/g, "&nbsp;");
		str = str.replace( /\s*mso-[^:]+:[^;"]+;?/gi, "" );
		str = str.replace( /\s*MARGIN: 0cm 0cm 0pt\s*;/gi, "" ) ;
		str = str.replace( /\s*MARGIN: 0cm 0cm 0pt\s*"/gi, "\"" ) ;
		str = str.replace( /\s*TEXT-INDENT: 0cm\s*;/gi, "" ) ;
		str = str.replace( /\s*TEXT-INDENT: 0cm\s*"/gi, "\"" ) ;
		str = str.replace( /\s*TEXT-ALIGN: [^\s;]+;?"/gi, "\"" ) ;
		str = str.replace( /\s*PAGE-BREAK-BEFORE: [^\s;]+;?"/gi, "\"" ) ;
		str = str.replace( /\s*FONT-VARIANT: [^\s;]+;?"/gi, "\"" ) ;
		str = str.replace( /\s*tab-stops:[^;"]*;?/gi, "" ) ;
		str = str.replace( /\s*tab-stops:[^"]*/gi, "" ) ;
		str = str.replace( /\s*face="[^"]*"/gi, "" ) ;
		str = str.replace( /\s*face=[^ >]*/gi, "" ) ;
		str = str.replace( /\s*FONT-FAMILY:[^;"]*;?/gi, "" ) ;
		str = str.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi, "<$1$3") ;
		str = str.replace( /<(\w[^>]*) style="([^\"]*)"([^>]*)/gi, "<$1$3" ) ;
		str = str.replace( /\s*style="\s*"/gi, '' ) ;
		str = str.replace(/HYPERLINK \\0022http\\;"/gi, '');
		str = str.replace( /<SPAN\s*[^>]*>\s*&nbsp;\s*<\/SPAN>/gi, '&nbsp;' ) ;
		str = str.replace( /<SPAN\s*[^>]*><\/SPAN>/gi, '' ) ;
		str = str.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi, "<$1$3") ;
		str = str.replace( /<SPAN\s*>(.*?)<\/SPAN>/gi, '$1' ) ;
		str = str.replace( /<FONT\s*>(.*?)<\/FONT>/gi, '$1' ) ;
		str = str.replace(/<\\?\?xml[^>]*>/gi, "") ;
		str = str.replace(/<\/?\w+:[^>]*>/gi, "") ;
		str = str.replace( /<H\d>\s*<\/H\d>/gi, '' ) ;
		str = str.replace( /<H1([^>]*)>/gi, '' ) ;
		str = str.replace( /<H2([^>]*)>/gi, '' ) ;
		str = str.replace( /<H3([^>]*)>/gi, '' ) ;
		str = str.replace( /<H4([^>]*)>/gi, '' ) ;
		str = str.replace( /<H5([^>]*)>/gi, '' ) ;
		str = str.replace( /<H6([^>]*)>/gi, '' ) ;
		str = str.replace( /<\/H\d>/gi, '<br>' ) ; //remove this to take out breaks where Heading tags were
		str = str.replace( /<(U|I|STRIKE)>&nbsp;<\/\1>/g, '&nbsp;' ) ;
		str = str.replace( /<(B|b)>&nbsp;<\/\b|B>/g, '' ) ;
		str = str.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;
		str = str.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;
		str = str.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;
		str = str.replace( /<!-*[^-]*-*>/g, '' ) ;
		//some RegEx code for the picky browsers
		var re = new RegExp("(<P)([^>]*>.*?)(<\/P>)","gi") ;
		str = str.replace( re, "<div$2</div>" ) ;
		var re2 = new RegExp("(<font|<FONT)([^*>]*>.*?)(<\/FONT>|<\/font>)","gi") ;
		str = str.replace( re2, "<div$2</div>") ;
		str = str.replace( /size|SIZE = ([\d]{1})/g, '' ) ;
		return str;
	};

})(window.nvx = window.nvx || {}, jQuery);