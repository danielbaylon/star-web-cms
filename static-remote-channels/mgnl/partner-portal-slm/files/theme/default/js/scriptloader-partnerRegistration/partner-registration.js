$(document).ready(function() {
    function isAllowedFileType(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'doc':
            case 'docx':
            case 'rtf':
            case 'txt':
            case 'jpeg':
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'tiff':
            case 'pdf':
                //etc
                return true;
        }
        return false;
    }
    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }
    function addFormParam(key , value){
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value",value);
        return hiddenField;
    }

    var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

    var $page = {};

    $page.paTypeVmsJson = null;
    $page.ctyVmsJson = null;
    $page.docTypeJson = null;

    // $("#uploadfileinput").kendoUpload();
    $page.isBlank = function(str) {
        return (!str || /^\s*$/.test(str));
    }
    $page.appendSpace = function(str){
        var nstr = str.trim();
        return !(nstr.length == str.length);
    };
    $page.hasWhiteSpace = function(s){
        return /\s/g.test(s);
    };
    $page.isValidEmailAddress = function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };
    $page.DropdownModel = function(value,display){
        var self = this;
        self.optionValue = ko.observable(value);
        self.optionText = ko.observable(display);
    };
    $page.PartnerModel = function(){
        var s = this;
        s.id = ko.observable();
        s.orgName = ko.observable();
        s.accountCode = ko.observable();
        s.accountCode.subscribe(function(newValue) {
            s.usernameMsg("");
            s.usernameErrMsg("");
        });
        /*s.branchName = ko.observable(); */
        s.uen = ko.observable();
        s.licenseNum = ko.observable();
        s.licenseExpDate = ko.observable();
        s.contactPerson = ko.observable();
        s.contactDesignation = ko.observable();
        s.address = ko.observable();
        s.postalCode = ko.observable();
        s.city = ko.observable();
        s.correspondenceAddress = ko.observable();
        s.correspondencePostalCode = ko.observable();
        s.correspondenceCity = ko.observable();
        s.useBizAddrAsCorrespAddr = ko.observable(false);
        s.isCorrespondenceAddrEnabled = ko.computed(function() {
            return !(s.useBizAddrAsCorrespAddr() == true);
        }, this);
        s.tnc = ko.observable(false);
        s.telNum = ko.observable();
        s.mobileNum = ko.observable();
        s.faxNum = ko.observable();
        s.email = ko.observable();
        s.reconfirmEmail = ko.observable();
        s.website = ko.observable();
        s.mainDestinations = ko.observable();
        s.username =  ko.computed(function() {
            return (this.accountCode()?this.accountCode():'') + "_ADMIN";
        }, this);
        // - Company code alrady existed
        s.usernameMsg = ko.observable("");
        s.usernameErrMsg =  ko.observable("");
        s.selCountry = ko.observable();
        s.selCountry.subscribe(function(newValue) {
            if(newValue.ctyName() == 'Singapore'){
                s.city('Singapore');
                if(s.useBizAddrAsCorrespAddr()){
                    s.correspondenceCity('Singapore');
                }
            }
        });
        s.ctys = ko.observableArray([]);
        var ctyLength = $page.ctyVmsJson.length;
        for (var i = 0; i < ctyLength; i++) {
            var o = new $page.CtyModel($page.ctyVmsJson[i]);
            s.ctys.push(o);
        };

        s.docTypes = ko.observableArray([]);
        var docTypesLength = $page.docTypeJson.length;
        for (var i = 0; i < docTypesLength; i++) {
            var o = new $page.DropdownModel($page.docTypeJson[i].code,$page.docTypeJson[i].label);
            s.docTypes.push(o);
        };


        s.orgType = ko.observable();
        s.paTypes = ko.observableArray([]);
        var paTypeLength = $page.paTypeVmsJson.length;
        for (var i = 0; i < paTypeLength; i++) {
            var o = new $page.PaTypeModel($page.paTypeVmsJson[i]);
            s.paTypes.push(o);
        };
        s.isTa = ko.computed(function() {
            /**
             * if(!s.orgType()){
                    return false;
                }
                 if('Travel Agent' == s.orgType().label()){
                    return true;
                }else{
                    return false;
                }
             */
            return true;
        }, this);
        s.file1Type = ko.observable();
        s.file2Type = ko.observable('PL');
        s.savePartner = function(){
            $page.savePartner(s);
        };
        s.checkPaCode = function(){
            $page.checkPaCode(s);
        };
        s.applyBizAddrAsCorrespAddr = function(){
            if(s.useBizAddrAsCorrespAddr()){
                s.correspondenceAddress(s.address());
                s.correspondencePostalCode(s.postalCode());
                s.correspondenceCity(s.city());
            }else{
                s.correspondenceAddress('');
                s.correspondencePostalCode('');
                s.correspondenceCity('');
            }
            return true;
        };
    };
    $page.initModel = function(o){
        var s = $page.partner;
        s.id(o['id']);
        s.orgName(o['orgName']);
        s.accountCode(o['accountCode']);
        s.uen(o['uen']);
        s.licenseNum(o['licenseNum']);
        $('#licenseExpDate').data('kendoDatePicker').value(kendo.parseDate(o['licenseExpDate'],'dd/MM/yyyy'));
        s.licenseExpDate(kendo.toString($('#licenseExpDate').data('kendoDatePicker').value(),'dd/MM/yyyy'));
        s.contactPerson(o['contactPerson']);
        s.contactDesignation(o['contactDesignation']);
        s.address(o['address']);
        s.postalCode(o['postalCode']);
        s.telNum(o['telNum']);
        s.mobileNum(o['mobileNum']);
        s.faxNum(o['faxNum']);
        s.email(o['email']);
        s.reconfirmEmail('');
        s.website(o['website']);
        s.usernameErrMsg('');
        s.usernameMsg('');
        s.mainDestinations(o['mainDestinations']);
        var exts = o['extension'];
        if(exts != undefined && exts != null && exts.length > 0){
            var extlen = exts.length;
            for(var i = 0 ; i < extlen ; i++){
                var ext = exts[i];
                if(ext != undefined && ext != null && ext != ''){
                    var name = ext['attrName'];
                    var attrVal = ext['attrValue'];
                    if('correspondenceAddress' == name){
                        s.correspondenceAddress(attrVal);
                    }
                    if('correspondencePostalCode' == name){
                        s.correspondencePostalCode(attrVal);
                    }
                    if('correspondenceCity' == name){
                        s.correspondenceCity(attrVal);
                    }
                    if('useOneAddress' == name){
                        if('true' == ext['attrValue']){
                            s.useBizAddrAsCorrespAddr(true);
                        }else{
                            s.useBizAddrAsCorrespAddr(false);
                        }
                    }
                }
            }
        }
        $.each(s.paTypes(), function(idx, item){
            if(item['id']() == o['orgTypeCode']){
                s.orgType(item);
            }
        });

        $.each(s.ctys(), function(idx, item){
            if(item['id']() == o['countryCode']){
                s.selCountry(item);
            }
        });
        s.city(o['city']);

        var paDocs = o['paDocs'];
        if(paDocs != null && paDocs.length > 0){
            $.each(paDocs, function(idx, item){
                $('#prevFileList').append('<tr><td><span id="prevFileType1Label" prevFileId="'+(item['id'])+'" prevFileType="'+(item['fileType'])+'">'+(item['fileTypeLabel'])+'</span></td><td><a id="prevFileType1Link" target="partner_file_download_frame" href="'+(nvx.API_PREFIX + '/publicity/partner-registration/attachment?id='+item['id'])+'"><span id="prevFileType1Name">'+(item['fileName'])+'</span></a></td><td><span></span></td></tr>');
            });
            $('#partnerExistingDocSection').show();
        }
        var isResubmitRequired = o['resubmitRequired'];
        if(isResubmitRequired != undefined && isResubmitRequired != null && isResubmitRequired != '' && (isResubmitRequired == true || isResubmitRequired == 'true')){
            var html = '<div><b>Remarks:</b></div>';
            var resubmitReason = o['resubmitReason'];
            if(resubmitReason != undefined && resubmitReason != null && resubmitReason != ''){
                html += '<div>'+( resubmitReason )+'</div>';
            }
            var resubmitRemarks=  o['resubmitRemarks'];
            if(resubmitRemarks != undefined && resubmitRemarks != null && resubmitRemarks != ''){
                html += '<div>'+( resubmitRemarks )+'</div>';
            }
            $('#errordv').html('<div>'+ html +'</div>');
            $('#errordv').show();
        }
    };

    $page.checkPaCode= function(pa){
        //var paCodeReg = /^[A-Za-z0-9]*$/;
        var paCodeReg = /^[A-Za-z0-9]*$/;
        if($page.isBlank(pa.accountCode()) || !paCodeReg.test(pa.accountCode())){
            pa.usernameErrMsg("Please refer to above note for a valid Partner Username format.");
            return;
        }
        if(pa.accountCode().length < 6 || pa.accountCode().length > 8){
            pa.usernameErrMsg("Partner Username should between 6 to 8 characters.");
            return;
        }
        if($page.hasWhiteSpace(pa.accountCode())){
            pa.usernameErrMsg("Please remove white space in the Partner Username.");
            return;
        }

        var _pa_account_code = pa.accountCode() != undefined && pa.accountCode() != null ? pa.accountCode() : null;
        var _pa_id = pa.id() != undefined && pa.id() != null ? pa.id() : null;

        nvx.spinner.start();
        $.ajax({
            type: "POST",
            url: nvx.API_PREFIX + '/publicity/partner-registration/check',
            data: {
                code : _pa_account_code,
                id : _pa_id
            },
            success:  function(data, textStatus, jqXHR)
            {
                pa.usernameMsg('');
                pa.usernameErrMsg('');
                if (data.success) {
                    var msg = nvx.getDefinedMsg(data.message);
                    if(msg != undefined && msg != null && msg != ''){
                        pa.usernameMsg(msg);
                    }else{
                        pa.usernameMsg(data.message);
                    }
                } else {
                    var msg = nvx.getDefinedMsg(data.message);
                    if(msg != undefined && msg != null && msg != ''){
                        pa.usernameErrMsg(msg);
                    }else{
                        pa.usernameErrMsg(data.message);
                    }
                }
            },
            error: function(jqXHR, textStatus)
            {
                alert(ERROR_MSG);
            },
            complete: function() {
                nvx.spinner.stop();
            }
        });
    };
    $page.CtyModel = function(data){
        var s = this;
        s.id = ko.observable('');
        s.ctyName = ko.observable('');
        s.update = function(data){
            s.id(data.ctyCode);
            s.ctyName(data.ctyName);
        };
        s.update(data);
    };
    $page.PaTypeModel = function(data){
        var s = this;
        s.id = ko.observable('');
        s.label = ko.observable('');
        s.update = function(data){
            s.id(data.id);
            s.label(data.label);
        };
        s.update(data);
    };
    $page.PaFileModel = function(data){
        var s = this;
        s.fileName = ko.observable('');
        s.detail = ko.observable('');
        s.update = function(data){
            s.fileName(data[0].files[0].name);
            s.detail(data);
        };
        s.update(data);
    };
    $page.savePartner = function(pa){
        if($page.isBlank(pa.orgName())){
            alert("Please fill in Organization Name.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.orgName())){
            alert("Special characters found on Organization Name.");
            return;
        }
        var paCodeReg = /^[A-Za-z0-9]*$/;
        if($page.isBlank(pa.accountCode()) || !paCodeReg.test(pa.accountCode())){
            alert("Please fill in a valid Partner Username.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.accountCode())){
            alert("Special characters found on Partner Username.");
            return;
        }

        if(pa.accountCode().length < 6 || pa.accountCode().length > 8){
            alert("Partner Username should between 6 to 8 characters.");
            return;
        }

        if($page.hasWhiteSpace(pa.accountCode())){
            alert("Please remove white space in the Partner Username.");
            return;
        }

        if($page.isBlank(pa.selCountry())){
            alert("Please choose a Registered Country.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.selCountry())){
            alert("Special characters found on Registered Country.");
            return;
        }

        if($page.isBlank(pa.orgType())){
            alert('Please fill in Nature of Business.');
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.orgType())){
            alert("Special characters found on Nature of Business.");
            return;
        }

        if($page.isBlank(pa.uen())){
            alert("Please fill in Unique Entity Number(UEN).");
            return;
        }

        if(!nvx.isRequiredValidInputText(true, pa.uen())){
            alert("Special characters found on Unique Entity Number(UEN).");
            return;
        }


        if(pa.isTa()) {
            if ($page.isBlank(pa.licenseNum())) {
                alert("Please fill in Travel Agent License No.");
                return;
            }

            if(!nvx.isRequiredValidInputText(true, pa.licenseNum())){
                alert("Special characters found on Travel Agent License No.");
                return;
            }

        }

        if(pa.isTa()){
            var datepicker = $("#licenseExpDate").data("kendoDatePicker");
            if(datepicker.value() == null){
                alert("Please fill in Business/Travel Agent License Expiry Date.");
                return;
            }
            var isFeatureDate = datepicker.value() > (new Date());
            if(!isFeatureDate){
                alert("Travel Agent License Expiry Date must be a valid feature date.");
                return;
            }
        }
        var file1 = $('[name="file1"]');
        var file2 = $('[name="file2"]');
        if(!file1.val()){
            if(!($('span[prevFileType="'+pa.file1Type()+'"]').size() > 0)){
                var attachmentLabelName = $($('option[value="'+ pa.file1Type() +'"]')[0]).text();
                alert("Please select an attachment for "+attachmentLabelName+".");
                return;
            }
        }
        if(!file2.val()){
            if(!($('span[prevFileType="'+pa.file2Type()+'"]').size() > 0)){
                var attachmentLabelName = $($('option[value="'+ pa.file2Type() +'"]')[0]).text();
                alert("Please select an attachment for "+attachmentLabelName+".");
                return;
            }
        }

        var files = [file1,file2];
        var filesLength = files.length;
        for(var i = 0; i < filesLength; i++){
            if(!files[i]&&!isAllowedFileType(files[i].val())){
                alert("File Type is not allowed.");
                return;
            }
        }
        if($page.isBlank(pa.address())){
            alert("Please fill in Business Registered Address.");
            return;
        }

        if(!nvx.isRequiredValidInputText(true, pa.address())){
            alert("Special characters found on Business Registered Address.");
            return;
        }
        if($page.isBlank(pa.postalCode())){
            alert("Please fill in Business Registered Postal Code.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.postalCode())){
            alert("Special characters found on Business Registered Postal Code.");
            return;
        }
        if(!nvx.isRequiredValidInputText(false, pa.city())){
            alert("Special characters found on Business Registered City.");
            return;
        }
        if($page.isBlank(pa.correspondenceAddress())){
            alert("Please fill in Correspondence Address.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.correspondenceAddress())){
            alert("Special characters found on Correspondence Address.");
            return;
        }
        if($page.isBlank(pa.correspondencePostalCode())){
            alert("Please fill in Correspondence Postal Code.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.correspondencePostalCode())){
            alert("Special characters found on Correspondence Postal Code.");
            return;
        }
        if(!nvx.isRequiredValidInputText(false, pa.correspondenceCity())){
            alert("Special characters found on Correspondence City.");
            return;
        }
        if($page.isBlank(pa.contactPerson())){
            alert("Please fill in Contact Person.");
            return;
        }

        if(!nvx.isRequiredValidInputText(true, pa.contactPerson())){
            alert("Special characters found on Contact Person.");
            return;
        }
        if($page.isBlank(pa.contactDesignation())){
            alert("Please fill in Designation or Position.");
            return;
        }

        if(!nvx.isRequiredValidInputText(true, pa.contactDesignation())){
            alert("Special characters found on Designation or Position.");
            return;
        }
        if($page.isBlank(pa.telNum())){
            alert("Please fill in Office No..");
            return;
        }
        if(!nvx.isValidPhoneNumber(pa.telNum())){
            alert("Please fill in a valid Office No., only '+' and digits are accepted. ");
            return;
        }
        if($page.isBlank(pa.mobileNum())){
            alert("Please fill in Mobile No..");
            return;
        }
        if(!nvx.isValidPhoneNumber(pa.mobileNum())){
            alert("Please fill in a valid Mobile No., only '+' and digits are accepted.");
            return;
        }
        if(!$page.isBlank(pa.faxNum())){
            if(!nvx.isValidPhoneNumber(pa.faxNum())){
                alert("Please fill in a valid Fax No., only '+' and digits are accepted.");
                return;
            }
        }
        if(!$page.isValidEmailAddress(pa.email())){
            alert("Please fill in valid email address.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.email())){
            alert("Special characters found on email address.");
            return;
        }
        if(!$page.isValidEmailAddress(pa.reconfirmEmail())){
            alert("Please fill in valid confirm email address.");
            return;
        }
        if(!nvx.isRequiredValidInputText(true, pa.reconfirmEmail())){
            alert("Special characters found on confirm email address.");
            return;
        }
        if(pa.email().toUpperCase() != pa.reconfirmEmail().toUpperCase()){
            alert("The reconfirm email is not the same with the email.");
            return;
        }
        if(!nvx.isRequiredValidInputText(false, pa.website())){
            alert("Special characters found on Website.");
            return;
        }

        if(!nvx.isRequiredValidInputText(true, pa.mainDestinations())){
            alert("Special characters found on Key market / Country.");
            return;
        }

        if(pa.isTa()){
            if($page.isBlank(pa.mainDestinations())){
                alert("Please fill in Key market / Country.");
                return;
            }
        }
        if(!(pa.tnc() == true)){
            alert("Please agree with the T&C.");
            return;
        }

        nvx.spinner.start();
        if(!$('[name="tempIframe"]').length){
            var iframe=document.createElement('iframe');
            $(iframe).attr('name', 'tempIframe');
            $(iframe).attr('id', 'tempIframe');
            $(iframe).hide();
            $('body').append(iframe);
        }
        $('[name="tempIframe"]').load(function() {
            var content = $('[name="tempIframe"]').contents().find("html").text();
            try {
                var data = JSON.parse(content);
                if(data.success){
                    alert("Submited Successfully.");
                    $('#registerDv').hide();
                    $('#successdv').show();
                }else{
                    var errorMsg = nvx.getDefinedMsg(data.message);
                    if(errorMsg != undefined && errorMsg != ''){
                        alert(errorMsg);
                    }else{
                        alert(data.message);
                    }
                }
            }catch(err) {
                console.log(err);
                alert(ERROR_MSG);
            }
            nvx.spinner.stop();
            Recaptcha.reload();
            $('[name="tempIframe"]').remove();
        });

        var idata = {};
        idata.id = pa.id();
        idata.orgName = pa.orgName().toUpperCase();
        idata.accountCode = pa.accountCode();
        /*
        if(pa.branchName()){
            idata.branchName = pa.branchName().toUpperCase();
        }else{
            idata.branchName = '';
        }*/

        idata.countryCode = pa.selCountry().id();
        idata.orgTypeCode = pa.orgType().id();
        idata.uen = pa.uen().toUpperCase();
        idata.licenseNum = pa.licenseNum().toUpperCase();
        idata.licenseExpDate = pa.licenseExpDate().toUpperCase();
        idata.contactPerson = pa.contactPerson().toUpperCase();
        idata.contactDesignation = pa.contactDesignation().toUpperCase();
        idata.address = pa.address().toUpperCase();
        idata.postalCode = pa.postalCode().toUpperCase();
        if(pa.city()){
            idata.city = pa.city().toUpperCase();
        }else{
            idata.city = '';
        }
        idata.extension = [];
        idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceAddress', 'attrValue' : pa.correspondenceAddress().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false};
        idata.extension[idata.extension.length] = { 'attrName' : 'correspondencePostalCode', 'attrValue' : pa.correspondencePostalCode().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

        if(pa.correspondenceCity()){
            idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceCity', 'attrValue' : pa.correspondenceCity().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : false, 'status' : 'A', 'approvalRequired' : false };
        }else{
            idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceCity', 'attrValue' : '', 'attrValueType' : 'STRING', 'mandatoryInd' : false, 'status' : 'A', 'approvalRequired' : false };
        }
        idata.extension[idata.extension.length] = { 'attrName' : 'termAndCondition', 'attrValue' : 'Agree', 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

        if(pa.useBizAddrAsCorrespAddr() == true){
            idata.extension[idata.extension.length] = { 'attrName' : 'useOneAddress', 'attrValue' : 'true', 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

        }else{
            idata.extension[idata.extension.length] = { 'attrName' : 'useOneAddress', 'attrValue' : 'false', 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };
        }

        idata.telNum = pa.telNum().toUpperCase();
        idata.mobileNum = pa.mobileNum().toUpperCase();
        if(pa.faxNum()){
            idata.faxNum = pa.faxNum().toUpperCase();
        }else{
            idata.faxNum = '';
        }
        idata.email = pa.email().toUpperCase();
        if(pa.website()){
            idata.website = pa.website().toUpperCase();
        }else{
            idata.website = '';
        }
        idata.mainDestinations = pa.mainDestinations().toUpperCase();
        idata.file1Type = pa.file1Type();
        idata.file2Type = pa.file2Type();
        var partnerVmJson = JSON.stringify(idata);

        var frm = document.getElementById('partnerForm');
        frm.setAttribute('enctype','multipart/form-data');
        var param = addFormParam("partnerVmJson",partnerVmJson);
        frm.appendChild(param);
        frm.target = 'tempIframe';
        frm.submit();
        frm.removeChild(param);

    };

    $page.loadPartnerDetailsIfNeeded = function(){
        var url = window.location.href;
        if(url != null && url != '' && url.indexOf('accountCode') > 0){
            var accountCodeIdx = url.indexOf('accountCode') + 12;
            var accountCode = url.substr(accountCodeIdx);
            if(accountCode != null && accountCode.trim().length > 0){
                accountCode = accountCode.trim();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/publicity/partner-registration/details',
                    data : {
                        code : accountCode
                    },
                    beforeSend : function(){
                        nvx.spinner.start();
                    },
                    success:  function(data, textStatus, jqXHR) {
                        if(data.success){
                            $page.initModel(data['data']);
                        }else{
                            if(data.message != null && data.message != ''){
                                var errorMsg = nvx.getDefinedMsg(data.message);
                                if(errorMsg != undefined && errorMsg != ''){
                                    alert(errorMsg);
                                }else{
                                    alert(data.message);
                                }
                            }
                        }
                    },
                    error: function(jqXHR, textStatus) {
                        alert(ERROR_MSG);
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }
        }
    };

    $('#licenseExpDate').kendoDatePicker({
        format: nvx.DATE_FORMAT
    }).data('kendoDatePicker');

    $.ajax({
        type: "POST",
        url: nvx.API_PREFIX + '/publicity/partner-registration/params',
        beforeSend : function(){
            nvx.spinner.start();
        },
        success:  function(data, textStatus, jqXHR) {
            if(data.success){
                // init
                $page.paTypeVmsJson = data.data.partnerTypes;
                $page.ctyVmsJson = data.data.countries;
                $page.docTypeJson = data.data.partnerDocTypes;

                // init page
                $page.partner = new $page.PartnerModel();
                ko.applyBindings($page.partner,$('#partnerForm')[0]);
                window.nvxpage = $page;
                $page.loadPartnerDetailsIfNeeded();

            }else{
                if(data.message != null && data.message != ''){
                    alert(data.message);
                }
            }
        },
        error: function(jqXHR, textStatus) {
            alert(ERROR_MSG);
        },
        complete: function() {
            nvx.spinner.stop();
        }
    });
});
