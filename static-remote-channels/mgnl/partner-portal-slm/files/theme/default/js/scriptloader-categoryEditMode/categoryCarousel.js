(function(nvx, $) {

    $(document).ready(function() {

        var onlyOneImage = $('.highlight-section').find('.promotion-1').length == 1;
        if (onlyOneImage) {
            $('.highlight-section').find('.slider-arrow').hide();
        }
        theSlider = $('.highlight-section').sequence({
            autoPlay: true,
            autoPlayDelay: 5000,
            nextButton: !onlyOneImage,
            prevButton: !onlyOneImage
        }).data('sequence');
        $('.slideshow-slides').sequence({
            autoPlay: true,
            autoPlayDelay: 5000,
            nextButton: '.icon-left-arrow',
            prevButton: '.icon-right-arrow'
        }).data('sequence');

    });

})(window.nvx = window.nvx || {}, jQuery);
