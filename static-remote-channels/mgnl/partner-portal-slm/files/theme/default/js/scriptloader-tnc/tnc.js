(function(nvx, $, ko) {

    $(document).ready(function() {
        $.ajax({
            url: nvx.API_PREFIX + '/secured/store-partner/get-tnc',
            headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    //console.log("data.data.cmsProducts:"+data.data.cmsProducts.length);
                    nvx.tncModel = new nvx.TncModel(data.data);
                    ko.applyBindings(nvx.tncModel, document.getElementById('tncDiv'));
                } else {
                    alert("System Error, please try again later.");
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
    });


    nvx.tncModel = null;

    nvx.TncModel = function(data) {
        var s = this;
        s.tncItem = ko.observableArray([]);

        $.each(data, function(idx, obj) {
            s.tncItem.push(new nvx.TncItemModel(obj));
        });
    }

    nvx.TncItemModel = function(data) {
        var s = this;

        s.title = ko.observable(data.title);
        s.content = ko.observable(data.content);

    }

})(window.nvx = window.nvx || {}, jQuery, ko);