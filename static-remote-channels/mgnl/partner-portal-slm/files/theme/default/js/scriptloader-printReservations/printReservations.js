(function(nvx, $, ko) {
    $(document).ready(function() {
        nvx.initPage();
    });
    nvx.initPage = function() {
        nvx.spinner.start();
        nvx.printReservationModel = new nvx.PrintReservationModel();
        ko.applyBindings(nvx.printReservationModel, document.getElementById('printingReservationContentDiv'));
        nvx.spinner.stop();
        var id = parseInt(window.location.href.substring(window.location.href.indexOf('=')+1));
        nvx.printReservationModel.showPrintConent(id);
    };

    nvx.printReservationModel =  null;

    nvx.PrintReservationModel = function(base) {
        var s = this;

        s.id = ko.observable();
        s.receiptNo = ko.observable();
        s.pinCode = ko.observable();
        s.pinCodeImage = ko.observable();
        s.status = ko.observable();
        s.qty = ko.observable();
        s.rdm = ko.observable();
        s.unr = ko.observable();
        s.remarks = ko.observable();
        s.createMode = ko.observable(false);
        s.updateMode = ko.observable(false);
        s.splitMode = ko.observable(false);
        s.createdBy = ko.observable();
        s.createdByUsername = ko.observable();
        s.createdDate = ko.observable();
        s.notifInfo = ko.observable("");
        s.showTime = ko.observable('');
        s.partnerName = ko.observable('');
        s.showDate = ko.observable('');
        s.productShowScheduleList = ko.observableArray([]);
        s.showTimeList = ko.observableArray([]);
        s.showTimeDisplay = ko.observable("");
        s.eventLineId = ko.observable("");
        s.productId = ko.observable("");
        s.penaltyCharge = ko.observable("");
        s.eventDate = ko.observable(-1);
        s.cutOffDateTime = ko.observable(-1);
        s.now = ko.observable(-1);
        s.baseUrl = ko.observable("");
        s.eventStartTime = ko.observable("");
        s.eventEndTime = ko.observable("");
        s.eventCapacityId = ko.observable("");

        s.itemId = ko.observable();
        s.mediaTypeId = ko.observable();
        s.eventGroupId = ko.observable();
        s.openValidityEndDate = ko.observable();
        s.openValidityStartDate = ko.observable();

        s.qtyFromDB = ko.observable();
        s.showDateFromDB = ko.observable();
        s.remarksFromDB = ko.observable();
        s.showTimeDisplayFromDB = ko.observable();

        s.showDateDisplayForPrint = ko.observable();

        s.showTimeDisplayForPrint = ko.observable();

        s.isEditable = ko.computed(function(){
            if(s.id() != undefined && s.id() != null && s.id() != '' && s.id() != -1 && s.id() != '-1'){
                if(s.status() != undefined && s.status() != null && s.status() != ''){
                    if(s.status() == 'Confirmed') {
                        if(s.now() < s.cutOffDateTime()){
                            return true;
                        }
                    }
                }
            }else{
                return true;
            }
            return false;
        });

        s.isAnExistingOrder = ko.computed(function(){
            if(s.status() != undefined && s.status() != null && s.status() != ''){
                if(s.id() != undefined && s.id() != null && s.id() != ''){
                    return true;
                }
            }
            return false;
        });

        s.reset = function(){
            s.createMode(false);
            s.updateMode(false);
            s.splitMode(false);
            s.id(-1);
            s.receiptNo('');
            s.pinCodeImage('');
            s.pinCode('');
            s.showTime('');
            s.showTimeDisplay('');
            s.showTimeDisplayFromDB('');
            s.eventLineId('');
            s.productId('');
            s.showDate('');
            s.showDateFromDB('');
            s.status('');
            s.qty(0);
            s.qtyFromDB(0);
            s.rdm(0);
            s.unr(0);
            s.remarks('');
            s.remarksFromDB('');
            s.createdBy('');
            s.createdByUsername('');
            s.createdDate('');
            s.itemId('');
            s.partnerName('');
            s.mediaTypeId('');
            s.eventGroupId('');
            s.openValidityEndDate('');
            s.openValidityStartDate('');
            s.eventDate(-1);
            s.cutOffDateTime(-1)
            s.now(-1);
            s.baseUrl('');

            s.eventStartTime('');
            s.eventEndTime('');
            s.eventCapacityId('');

            s.showDateDisplayForPrint('');

            s.showTimeDisplayForPrint('');
        };


        s.initPrintReservationModel = function(data) {
            s.reset();
            if(data.id != undefined && data.id != null && data.id != '' && data.id != -1 && data.id != -1){
                s.updateMode(true);
            }else{
                s.createMode(true);
            }
            s.id(data.id);
            s.receiptNo(data.receiptNo);
            s.pinCode(data.pinCode);
            var barcode = data['pinCodeImage'];
            if(barcode != undefined && barcode != null && barcode != ''){
                s.pinCodeImage('data:image/png;base64,'+barcode);
            }
            s.showTime(data.productId+'-'+data.eventLineId);
            s.showTimeDisplay(data.showTimeDisplay);
            s.showTimeDisplayFromDB(data.showTimeDisplay);
            s.eventLineId(data.eventLineId);
            s.productId(data.productId);
            s.showDate(data.showDate);
            s.showDateFromDB(data.showDate);
            s.status('');
            if(data['status'] != undefined && data['status'] != null && data['status'] != ''){
                if('FullyPurchased' == data['status'] || 'PartiallyPurchased' == data['status']){
                    s.status('Reserved');
                }else{
                    s.status(data['status']);
                }
            }
            s.qty(parseInt(data.qty) - parseInt(data.rdm));
            s.qtyFromDB(data.qty);
            s.rdm(data.rdm);
            s.baseUrl(data.baseUrl);
            s.unr(data.unr);
            s.partnerName(data.partnerName);
            s.remarks(data.remarks);
            s.remarksFromDB(data.remarks);
            s.createdBy(data.createdBy);
            s.createdByUsername(data.createdByUsername);
            s.createdDate(data.createdDate);
            s.itemId(data.itemId);
            s.mediaTypeId(data.mediaTypeId);
            s.eventGroupId(data.eventGroupId);
            s.openValidityEndDate(data.openValidityEndDate);
            s.openValidityStartDate(data.openValidityStartDate);

            s.eventDate(data.eventDate);
            s.cutOffDateTime(data.cutOffDateTime)
            s.now(data.now);

            s.eventStartTime(data.eventStartTime);
            s.eventEndTime(data.eventEndTime);
            s.eventCapacityId(data.eventCapacityId);

            s.showDateDisplayForPrint(kendo.toString(new Date(s.eventDate()), 'dddd, dd MMM yyyy'));

            s.showTimeDisplayForPrint((
                kendo.toString(new Date( s.eventDate() + ( s.eventStartTime() * 1000) ), ' hh:mm tt')
                + ' to ' +
                kendo.toString(new Date( s.eventDate() + ( s.eventEndTime() * 1000) ), ' hh:mm tt')
                + ' - ' +
                s.showTimeDisplay()
            ));
        };

        s.printingTicket = function(){
            window.print();
        };

        s.closeModel = function(){
            window.close();
        };

        s.showPrintConent = function(id){
            try{
                nvx.spinner.start();
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + '/secured/partner-wot/get-reservation-printing-details/' + id,
                    headers: { 'Store-Api-Channel' : 'partner-portal-slm' },
                    success:  function(data, textStatus, jqXHR)
                    {
                        if (data['message'] != undefined && data['message'] != null && data['message'] != '') {
                            nvx.spinner.stop();
                            alert(data['message']);
                            console.log('ERRORS: ' + data['message']);
                        }else{
                            nvx.printReservationModel.initPrintReservationModel(data.data);
                            nvx.spinner.stop();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        nvx.spinner.stop();
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }
                });
            }catch(e){
                console.log('Show ticket printing window failed : '+e);
            }
        };
    };
})(window.nvx = window.nvx || {}, jQuery, ko);
