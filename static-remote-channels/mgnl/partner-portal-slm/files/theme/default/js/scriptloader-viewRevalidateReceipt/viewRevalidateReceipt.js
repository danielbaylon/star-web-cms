(function(nvx, $, ko) {

    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        var receiptNum = nvx.getUrlParam("txn");

        if(receiptNum != "") {
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/get-revalidated-receipt-by-receiptnum/" + receiptNum,
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        nvx.receiptView = new nvx.ReceiptView(data.data);
                        ko.applyBindings(nvx.receiptView , document.getElementById('revalidateReceiptView'));

                        $("#successdv > h2").html(data.message);
                        $("#successdv").show();
                        $("#revalidateReceiptView").show();
                    }
                    else
                    {
                        $("#errordv > span").html(data.message);
                        $("#errordv").show();
                        $("#successdv").hide();
                        $("#revalidateReceiptView").hide();
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }

            });
        }else {
            nvx.spinner.start();
            var counts = 0;
            function checkStatus() {
                $.ajax({
                    type: "POST",
                    url: nvx.API_PREFIX + "/secured/partner-inventory/check-reval-status",
                    headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                    success:  function(data, textStatus, jqXHR)
                    {
                        if(data.success)
                        {
                            if(data.data == null) {
                                checkStatusCallBack(data.data);
                            }else {
                                window.location.href = "view-revalidate-receipt?txn=" + data.data;
                            }
                        }else {
                            $("#revalLink").attr("href", "revalidate-receipt?transId=" + data.data);
                            $("#errordv").show();
                            $("#successdv").hide();
                            $("#revalidateReceiptView").hide();
                        }
                    },
                    error: function(jqXHR, textStatus)
                    {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete: function() {
                        nvx.spinner.stop();
                    }

                });
            }

            checkStatus();

            function checkStatusCallBack(data){
                //success,message,statusUpdated
                if(counts >= 3){
                    console.log("Max connect times exceed.");
                    alert("Max connect times exceed, please check your network connection.");
                    nvx.spinner.stop();
                }else{
                    if(data == null){
                        nvx.spinner.stop();
                    }else{
                        setTimeout(checkStatus, 7000);
                    }
                }
            }
            //check the status of the telemoney


        }

        $('#btnRegen').click(function(event) {
            var result = confirm("Are you going to regenerate the revalidate receipt?");
            if (result == false) {
                return;
            }
            //        var data = new FormData();
            //        data.append('transId', transId);
            //        $.submitFormRequest('/regenerateRevalReceipt',data,regenRevalCallBack,true);
            nvx.spinner.start();
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/send-email-reval-ticket/" + nvx.receiptView.transId(),
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        regenRevalCallBack(data);
                    }
                    else
                    {
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        });

        function regenRevalCallBack(data){
            nvx.spinner.stop();
            alert(data.message);
        }


        //$('#revalReceiptDo').click(function() {
        //
        //    if (!$('#agreeTnc').is(':checked')) {
        //        alert('Please read and agree to the Terms and Conditions first.');
        //        return;
        //    }
        //
        //    if (nvx.btnGuard) {return;}
        //
        //    nvx.btnGuard = true;
        //    nvx.spinner.start();
        //    $.ajax({
        //        url: nvx.API_PREFIX + '/secured/partner-inventory/finalise-reval-trans/' + transId,
        //        headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
        //        type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
        //        success: function (data) {
        //            if (data.success) {
        //                var receiptNum = data.data;
        //                //TODO should actually redirect to TELEMONEY....
        //                window.location.replace(nvx.ROOT_PATH + '/view-revalidate-receipt?txn='+receiptNum);
        //            } else {
        //                alert("System Error, please try again later.");
        //            }
        //        },
        //        error: function () {
        //        },
        //        complete: function () {
        //            nvx.spinner.stop();
        //        }
        //    });
        //});
    });


    nvx.receiptView = null;

    nvx.ReceiptView = function(data) {
        var s = this;

        s.transId = ko.observable(data.transId);
        s.status = ko.observable(data.status);
        s.revalReceiptNum = ko.observable(data.revalRecepitNum);
        s.accountCode = ko.observable(data.accountCode);
        s.address = ko.observable(data.address);
        s.orgName = ko.observable(data.orgName);
        s.validateStartDateStr = ko.observable(data.validateStartDateStr);
        s.validityEndDateStr = ko.observable(data.validityEndDateStr);
        s.extendRevalMonth = ko.observable(data.extendRevalMonth);
        s.newvalidateStartDateStr = ko.observable(data.newvalidateStartDateStr);
        s.newvalidityEndDateStr = ko.observable(data.newvalidityEndDateStr);
        s.normalRevFeeStr = ko.observable(data.normalRevFeeStr);
        s.topupRevFeeStr = ko.observable(data.topupRevFeeStr);
        s.revalidatedBy = ko.observable(data.revalidatedBy);
        s.tmStatus = ko.observable(data.tmStatus);
        s.revalPaymentType = ko.observable(data.revalPaymentType);
        s.revalidateDtTmStr = ko.observable(data.revalidateDtTmStr);

        s.receiptitems = ko.observableArray([]);

        $.each(data.receiptitems , function(idx, obj) {
            s.receiptitems.push(new nvx.ReceiptItemView(obj));
        });

        s.revalGSTRateStr = ko.observable(data.revalGSTRateStr);
        s.currency = ko.observable(data.currency);
        s.revalGSTStr = ko.observable(data.revalGSTStr);
        s.revalExclGSTStr = ko.observable(data.revalExclGSTStr);
        s.totalRevalFeeStr = ko.observable(data.totalRevalFeeStr);

        s.mainReceiptLink = nvx.ROOT_PATH + "/view-inventory-receipt?transId=" + s.transId();
    }

    nvx.ReceiptItemView = function(data) {
        var s = this;
        s.itemTopup = ko.observable(data.itemTopup);
        s.displayName = ko.observable(data.displayName);
        s.singleRevalFeeFullStr = ko.observable(data.singleRevalFeeFullStr);
        s.unpackedQty = ko.observable(data.unpackagedQty);
        s.revalFeeFullStr = ko.observable(data.revalFeeFullStr);
    }

})(window.nvx = window.nvx || {}, jQuery, ko);