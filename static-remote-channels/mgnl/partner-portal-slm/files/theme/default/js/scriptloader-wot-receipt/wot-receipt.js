$(document).ready(function() {
    $page = {};
    $page.WoTReceiptGenModel = function(){
        var s = this;
        s.receiptType = ko.observable('D');
        s.receiptDate = ko.observable();
        s.receiptMonth = ko.observable();
        s.receiptTypeList = ko.observableArray([
            {code : 'D', name : 'Daily'},
            {code : 'M', name : 'Monthly'}
        ]);

        s.filter = function(event){
            if(event != undefined && event != null && event != ''){
                try{
                    event.preventDefault();
                }catch(e){}
            }
            if('D' == s.receiptType()){
                s.processGenDailyReceipt();
            }else if('M' == s.receiptType()){
                s.processGenMonthlyReceipt();
            }
        };

        s.processGenDailyReceipt = function(){
            var date = $('#receiptDate').data('kendoDatePicker').value();
            if(date == undefined || date == null || date == ''){
                alert('Please select a date to generate receipt.');
                return false;
            }
            var dateString = '';
            try{
                dateString = kendo.toString($('#receiptDate').data('kendoDatePicker').value(), 'dd/MM/yyyy');
            }catch(e){
                alert('Please select a date to generate receipt.');
                return false;
            }
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/secured/partner-wot/verifyDailyReservations',
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                data: {
                    'date' : dateString
                },
                type: 'POST', cache: false, dataType: 'json',
                success:  function(data, textStatus, jqXHR){
                    nvx.spinner.stop();
                    if (data.success) {
                        s.genDailyReceipt(dateString);
                    } else {
                        alert(data['message']);
                    }
                },
                error: function(jqXHR, textStatus){
                    console.log(textStatus);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.genDailyReceipt = function(param){
            $('#_receipt_generation_form_param').val('');
            $('#_receipt_generation_form_param').val(param);
            $('#_receipt_generation_form').attr('action','');
            var url = nvx.API_PREFIX + '/secured/partner-wot/genDailyReservations';
            $('#_receipt_generation_form').attr('action',url);
            $('#_receipt_generation_form').submit();
        };

        s.processGenMonthlyReceipt = function(){
            var date = $('#receiptMonth').data('kendoDatePicker').value();
            if(date == undefined || date == null || date == ''){
                alert('Please select a month to generate receipt.');
                return false;
            }
            var dateString = '';
            try{
                dateString = kendo.toString($('#receiptMonth').data('kendoDatePicker').value(), 'MM/yyyy');
            }catch(e){
                alert('Please select a month to generate receipt.');
                return false;
            }
            dateString = '01/'+dateString;
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/secured/partner-wot/verifyMonthlyReservations',
                headers: { 'Store-Api-Channel' :  nvx.API_CHANNEL },
                data: {
                    'date' : dateString
                },
                type: 'POST', cache: false, dataType: 'json',
                success:  function(data, textStatus, jqXHR){
                    nvx.spinner.stop();
                    if (data.success) {
                        s.genMonthlyReceipt(dateString);
                    } else {
                        alert(data['message']);
                    }
                },
                error: function(jqXHR, textStatus){
                    console.log(textStatus);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.genMonthlyReceipt = function(param){
            $('#_receipt_generation_form_param').val('');
            $('#_receipt_generation_form_param').val(param);
            $('#_receipt_generation_form').attr('action','');
            var url = nvx.API_PREFIX + '/secured/partner-wot/genMonthlyReservations';
            $('#_receipt_generation_form').attr('action',url);
            $('#_receipt_generation_form').submit();
        };


        s.init = function(){
            try{
                nvx.spinner.start();
                $('#receiptDate').kendoDatePicker({
                    format: 'dd/MM/yyyy'
                });
                $('#receiptMonth').kendoDatePicker({
                    start: 'year',
                    depth: 'year',
                    format: 'MMMM yyyy'
                });
                s.receiptType('D');
                nvx.spinner.stop();
            }catch(e){
                nvx.spinner.stop();
            }
        };
    };
    $page.wotReceiptModel = new $page.WoTReceiptGenModel();
    $page.wotReceiptModel.init();
    ko.applyBindings($page.wotReceiptModel ,$('#wot-receipt-generation')[0]);
});