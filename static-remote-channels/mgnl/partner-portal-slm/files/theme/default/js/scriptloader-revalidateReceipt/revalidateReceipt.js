(function(nvx, $, ko) {

    $(document).ready(function() {

        nvx.theNav.currNav('Inventory');
        nvx.theNav.currSubNav('Inventory');

        var transId = nvx.getUrlParam("transId");

        if(transId != "") {
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + "/secured/partner-inventory/revalidate-receipt/" + transId,
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                success:  function(data, textStatus, jqXHR)
                {
                    if(data.success)
                    {
                        nvx.receiptView = new nvx.ReceiptView(data.data);
                        ko.applyBindings(nvx.receiptView , document.getElementById('revalidateReceiptView'));
                    }
                    else
                    {
                        alert(data.error);
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete: function() {
                    nvx.spinner.stop();
                }

            });
        }


        $('#revalReceiptDo').click(function() {

            if (!$('#agreeTnc').is(':checked')) {
                alert('Please read and agree to the Terms and Conditions first.');
                return;
            }

            if (nvx.btnGuard) {return;}

            nvx.btnGuard = true;
            nvx.spinner.start();
            $.ajax({
                url: nvx.API_PREFIX + '/secured/partner-inventory/finalise-reval-trans/' + transId,
                headers: { 'Store-Api-Channel' : nvx.API_CHANNEL },
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        if (data.data.tmRedirect) {
                            window.location.replace(data.data.redirectUrl);
                        } else {
                            alert("System Error, please try again later.");
                        }
                    } else {
                        alert("System Error, please try again later.");
                    }
                },
                error: function () {
                },
                complete: function () {
                    nvx.spinner.stop();
                }
            });
        });
    });


    nvx.receiptView = null;

    nvx.ReceiptView = function(data) {
        var s = this;

        s.transId = ko.observable(data.transId);
        s.status = ko.observable(data.status);
        s.receiptNum = ko.observable(data.recepitNum);
        s.accountCode = ko.observable(data.accountCode);
        s.address = ko.observable(data.address);
        s.orgName = ko.observable(data.orgName);
        s.validateStartDateStr = ko.observable(data.validateStartDateStr);
        s.validityEndDateStr = ko.observable(data.validityEndDateStr);
        s.extendRevalMonth = ko.observable(data.extendRevalMonth);
        s.newvalidateStartDateStr = ko.observable(data.newvalidateStartDateStr);
        s.newvalidityEndDateStr = ko.observable(data.newvalidityEndDateStr);
        s.normalRevFeeStr = ko.observable(data.normalRevFeeStr);
        s.topupRevFeeStr = ko.observable(data.topupRevFeeStr);

        s.receiptitems = ko.observableArray([]);

        $.each(data.receiptitems , function(idx, obj) {
            s.receiptitems.push(new nvx.ReceiptItemView(obj));
        });

        s.revalGSTRateStr = ko.observable(data.revalGSTRateStr);
        s.currency = ko.observable(data.currency);
        s.revalGSTStr = ko.observable(data.revalGSTStr);
        s.revalExclGSTStr = ko.observable(data.revalExclGSTStr);
        s.totalRevalFeeStr = ko.observable(data.totalRevalFeeStr);

        s.mainReceiptLink = nvx.ROOT_PATH + "/view-inventory-receipt?transId=" + s.transId();
    }

    nvx.ReceiptItemView = function(data) {
        var s = this;
        s.itemTopup = ko.observable(data.itemTopup);
        s.displayName = ko.observable(data.displayName);
        s.singleRevalFeeFullStr = ko.observable(data.singleRevalFeeFullStr);
        s.unpackedQty = ko.observable(data.unpackagedQty);
        s.revalFeeFullStr = ko.observable(data.revalFeeFullStr);
    }

})(window.nvx = window.nvx || {}, jQuery, ko);