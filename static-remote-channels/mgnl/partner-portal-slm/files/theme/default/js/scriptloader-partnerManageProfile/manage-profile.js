/**
 * Created by houtao on 3/11/16.
 */
var ERROR_MSG = 'Unable to process your request. Please try again in a few moments.';

$(document).ready(function() {
    var $page = {};
    $page.ppmm = new nvx.PartnerProfileManagementModel();
    ko.applyBindings($page.ppmm,$('#partnerProfileManagementDiv')[0]);
    $page.ppmm.init();
    nvx.$page = $page;
});

(function(nvx, $) {

    nvx.PartnerDocument = function(){
        var s = this;
        s.fileType = ko.observable();
        s.fileTypeName = ko.observable();
        s.fileId = ko.observable();
        s.fileName = ko.observable();
    };

    nvx.PartnerProfileManagementModel = function() {
        var s = this;
        s.isErr = ko.observable();
        s.errMsg = ko.observable();
        s.username = ko.observable();
        s.usernameMsg = ko.observable();
        s.usernameErrMsg = ko.observable();
        s.lineOfBusinessLabel = ko.observable();
        s.countryRegionLabel = ko.observable();
        s.status = ko.observable();
        s.orgName = ko.observable();
        s.uen = ko.observable();
        s.licenseNum = ko.observable();
        s.licenseExpDate = ko.observable();
        s.partnerDocs = ko.observable([]);

        s.address = ko.observable();
        s.postalCode = ko.observable();
        s.city = ko.observable();

        s.useBizAddrAsCorrespAddr = ko.observable(false);
        s.isCorrespondenceAddrEnabled = ko.computed(function() {
            return !(s.useBizAddrAsCorrespAddr() == true);
        }, this);

        s.correspondenceAddress = ko.observable();
        s.correspondencePostalCode = ko.observable();
        s.correspondenceCity = ko.observable();
        s.contactPerson = ko.observable();
        s.contactDesignation = ko.observable();
        s.telNum = ko.observable();
        s.mobileNum = ko.observable();
        s.faxNum = ko.observable();
        s.email = ko.observable();
        s.website = ko.observable();
        s.mainDestinations = ko.observable();
        s.applyBizAddrAsCorrespAddr = function(){
            if(s.useBizAddrAsCorrespAddr()){
                s.correspondenceAddress(s.address());
                s.correspondencePostalCode(s.postalCode());
                s.correspondenceCity(s.city());
            }else{
                s.correspondenceAddress('');
                s.correspondencePostalCode('');
                s.correspondenceCity('');
            }
            return true;
        };
        s.isBlank = function(str) {
            return (!str || /^\s*$/.test(str));
        };
        s.hasWhiteSpace = function(s){
            return /\s/g.test(s);
        };

        s.doSave = function() {
            if(s.isBlank(s.address())){
                alert("Please fill in Business Registered Address.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.address())){
                alert("Special characters found on Business Registered Address.");
                return;
            }
            if(s.isBlank(s.postalCode())){
                alert("Please fill in Business Registered Postal Code.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.postalCode())){
                alert("Special characters found on Business Registered Postal Code.");
                return;
            }
            if(!nvx.isRequiredValidInputText(false, s.city())){
                alert("Special characters found on Business Registered City.");
                return;
            }
            if(s.isBlank(s.correspondenceAddress())){
                alert("Please fill in Correspondence Address.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.correspondenceAddress())){
                alert("Special characters found on Correspondence Address.");
                return;
            }
            if(s.isBlank(s.correspondencePostalCode())){
                alert("Please fill in Correspondence Postal Code.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.correspondencePostalCode())){
                alert("Special characters found on Correspondence Postal Code.");
                return;
            }
            if(!nvx.isRequiredValidInputText(false, s.correspondenceCity())){
                alert("Special characters found on Correspondence City.");
                return;
            }
            if(s.isBlank(s.contactPerson())){
                alert("Please fill in Contact Person.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.contactPerson())){
                alert("Special characters found on Contact Person.");
                return;
            }
            if(s.isBlank(s.contactDesignation())){
                alert("Please fill in Designation or Position.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.contactDesignation())){
                alert("Special characters found on Designation or Position.");
                return;
            }
            if(s.isBlank(s.telNum())){
                alert("Please fill in Office No..");
                return;
            }
            if(!nvx.isValidPhoneNumber(s.telNum())){
                alert("Please fill in a valid Office No., only '+' and digits are accepted. ");
                return;
            }
            if(s.isBlank(s.mobileNum())){
                alert("Please fill in Mobile No..");
                return;
            }
            if(!nvx.isValidPhoneNumber(s.mobileNum())){
                alert("Please fill in a valid Mobile No., only '+' and digits are accepted.");
                return;
            }
            if(!s.isBlank(s.faxNum())){
                if(!nvx.isValidPhoneNumber(s.faxNum())){
                    alert("Please fill in a valid Fax No., only '+' and digits are accepted.");
                    return;
                }
            }
            if(!nvx.isRequiredValidInputText(false, s.website())){
                alert("Special characters found on Website.");
                return;
            }
            if(s.isBlank(s.mainDestinations())){
                alert("Please fill in Key market / Country.");
                return;
            }
            if(!nvx.isRequiredValidInputText(true, s.mainDestinations())){
                alert("Special characters found on Key market / Country.");
                return;
            }

            var idata = {};
            idata.contactPerson = s.contactPerson().toUpperCase();
            idata.contactDesignation = s.contactDesignation().toUpperCase();
            idata.address = s.address().toUpperCase();
            idata.postalCode = s.postalCode().toUpperCase();
            if(s.city()){
                idata.city = s.city().toUpperCase();
            }else{
                idata.city = '';
            }
            idata.extension = [];
            idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceAddress', 'attrValue' : s.correspondenceAddress().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false};
            idata.extension[idata.extension.length] = { 'attrName' : 'correspondencePostalCode', 'attrValue' : s.correspondencePostalCode().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

            if(s.correspondenceCity()){
                idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceCity', 'attrValue' : s.correspondenceCity().toUpperCase(), 'attrValueType' : 'STRING', 'mandatoryInd' : false, 'status' : 'A', 'approvalRequired' : false };
            }else{
                idata.extension[idata.extension.length] = { 'attrName' : 'correspondenceCity', 'attrValue' : '', 'attrValueType' : 'STRING', 'mandatoryInd' : false, 'status' : 'A', 'approvalRequired' : false };
            }
            idata.extension[idata.extension.length] = { 'attrName' : 'termAndCondition', 'attrValue' : 'Agree', 'attrValueType' : 'STRING', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

            if(s.useBizAddrAsCorrespAddr() == true){
                idata.extension[idata.extension.length] = { 'attrName' : 'useOneAddress', 'attrValue' : 'true', 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };

            }else{
                idata.extension[idata.extension.length] = { 'attrName' : 'useOneAddress', 'attrValue' : 'false', 'attrValueType' : 'boolean', 'mandatoryInd' : true, 'status' : 'A', 'approvalRequired' : false };
            }

            idata.telNum = s.telNum().toUpperCase();
            idata.mobileNum = s.mobileNum().toUpperCase();
            if(s.faxNum()){
                idata.faxNum = s.faxNum().toUpperCase();
            }else{
                idata.faxNum = '';
            }
            if(s.website()){
                idata.website = s.website().toUpperCase();
            }else{
                idata.website = '';
            }
            idata.mainDestinations = s.mainDestinations().toUpperCase();
            var partnerVmJson = JSON.stringify(idata);
            $.ajax({
                type: "POST",
                url: nvx.API_PREFIX + '/secured/partner-account/update-partner-profile',
                data : {
                    partner : partnerVmJson
                },
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        alert('Profile updated successfully.');
                    }else{
                        if(data.message != null && data.message != ''){
                            var errorMsg = nvx.getDefinedMsg(data.message);
                            if(errorMsg != undefined && errorMsg != ''){
                                alert(errorMsg);
                            }else{
                                alert(data.message);
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };

        s.doCancel = function() {
            s.init();
        };

        s.populateFormData = function(t){
            if(!(t != undefined && t != null && t != '')){
                return;
            }
            s.username(t['username']);
            s.lineOfBusinessLabel(t['orgTypeName']);
            s.countryRegionLabel(t['countryName']);
            s.status(t['statusLabel']);
            s.orgName(t['orgName']);
            s.uen(t['uen']);
            s.licenseNum(t['licenseNum']);
            s.licenseExpDate(t['licenseExpDate']);
            s.partnerDocs(t['paDocs']);
            s.address(t['address']);
            s.postalCode(t['postalCode']);
            s.city(t['city']);
            s.contactPerson(t['contactPerson']);
            s.contactDesignation(t['contactDesignation']);
            s.telNum(t['telNum']);
            s.mobileNum(t['mobileNum']);
            s.faxNum(t['faxNum']);
            s.email(t['email']);
            s.website(t['website']);
            s.mainDestinations(t['mainDestinations']);

            var exts = t['extension'];
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        if('useOneAddress' == name){
                            if('true' == ext['attrValue']){
                                s.useBizAddrAsCorrespAddr(true);
                            }else{
                                s.useBizAddrAsCorrespAddr(false);
                            }
                        }
                    }
                }
            }
            if(exts != undefined && exts != null && exts.length > 0){
                var extlen = exts.length;
                for(var i = 0 ; i < extlen ; i++){
                    var ext = exts[i];
                    if(ext != undefined && ext != null && ext != ''){
                        var name = ext['attrName'];
                        var attrVal = ext['attrValue'];
                        if('correspondenceAddress' == name){
                            s.correspondenceAddress(attrVal);
                        }
                        if('correspondencePostalCode' == name){
                            s.correspondencePostalCode(attrVal);
                        }
                        if('correspondenceCity' == name){
                            s.correspondenceCity(attrVal);
                        }
                    }
                }
            }
        };

        s.init = function(){
            $.ajax({
                type: 'POST',
                url: nvx.API_PREFIX + '/secured/partner-account/get-partner-profile',
                beforeSend : function(){
                    nvx.spinner.start();
                },
                success:  function(data, textStatus, jqXHR) {
                    if(data.success){
                        s.populateFormData(data['data']);
                    }else{
                        if(data.message != null && data.message != ''){
                            alert(data.message);
                        }
                    }
                },
                error: function(jqXHR, textStatus) {
                    alert(ERROR_MSG);
                },
                complete: function() {
                    nvx.spinner.stop();
                }
            });
        };
    };
})(window.nvx = window.nvx || {}, jQuery);
