[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]

[#assign title = cmsfn.decode(content).title!""]
[#assign message = cmsfn.decode(content).message!""]

<style type="text/css">
    strong {
        font-family: 'Gotham-Medium', sans-serif;
    }
    /* http://stackoverflow.com/questions/4098195/can-ordered-list-produce-result-that-looks-like-1-1-1-2-1-3-instead-of-just-1 */
    ol {
        counter-reset: item;
    }
    ol li {
        display: block;
        position: relative;
        padding: 5px 0;
    }
    ol li:before {
        content: counters(item, ".")".";
        counter-increment: item;
        position: absolute;
        margin-right: 100%;
        right: 10px; /* space between number and text */
    }
</style>

<div id="main-div" class="main-content-section-container">
    <h1 class="heading">${title}</h1>
    <div style="text-align:justify;">${message}</div>
</div>