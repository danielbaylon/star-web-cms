[#-------------- ASSIGNMENTS --------------]
[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]

[#-- Page's model & definition, based on the rendering hierarchy and not the node hierarchy --]
[#assign urlVersion = "?ver=1.0r003"]
[#assign site = sitefn.site()!]

[#-------------- RENDERING --------------]
<title>${cmsfn.page(content).title!content.windowTitle!content.title!}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="${content.description!""}" />
<meta name="keywords" content="${content.keywords!""}" />
<meta name="author" content="Enovax Pte Ltd." />
<meta name="generator" content="Powered by Enovax - Intuitive Opensource CMS" />

<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script>window.html5 || document.write('<script src="${ctx.contextPath}/.resources/${channel}/theme/js/html5shiv.min.js"><\/script>')</script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
[#assign requestUri = ctx.getRequest().getRequestURI()]
[#assign requestUriArr = "${requestUri}"?split("/")]
[#assign lastUri = requestUriArr[requestUriArr?size-1]!""]
[#assign lastSecUri = requestUriArr[requestUriArr?size-2]!""]
[#assign lang = sitefn.site().getI18n().getLocale().toString()]
<script language="javascript" type="text/javascript">
    var CURR_LANG = '${lang}';
</script>
<link href="${ctx.contextPath}/resources/${channel}/theme/default/css/styleloader.css${urlVersion}" rel="stylesheet">

<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-lib.js${urlVersion}"></script>


[#if requestUri?index_of("/dashboard",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-dashboard.js${urlVersion}"></script>
[#elseif requestUri?index_of("/product",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-product.js${urlVersion}"></script>
[#elseif requestUri?index_of("/category",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-category.js${urlVersion}"></script>
[#elseif requestUri?index_of("/category",0) > -1 && cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-categoryEditMode.js${urlVersion}"></script>
[#elseif requestUri?index_of("/cart",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-purchaseCart.js${urlVersion}"></script>
[#elseif requestUri?index_of("/confirm-package",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-confirmPackage.js${urlVersion}"></script>
[#elseif requestUri?index_of("/confirm",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-purchaseConfirm.js${urlVersion}"></script>
[#elseif requestUri?index_of("/offlinePay-confirm",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-offlinePayConfirm.js${urlVersion}"></script>
[#elseif requestUri?index_of("/offlinePay-form",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-offlinePayForm.js${urlVersion}"></script>
[#elseif requestUri?index_of("/offlinePay-list",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-viewOfflinePayment.js${urlVersion}"></script>
[#elseif requestUri?index_of("/receipt",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-purchaseReceipt.js${urlVersion}"></script>
[#elseif requestUri?index_of("/view-inventory-receipt",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-viewInventoryReceipt.js${urlVersion}"></script>
[#elseif requestUri?index_of("/view-inventory",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-viewInventory.js${urlVersion}"></script>
[#elseif requestUri?index_of("/gen-package",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-genETicket.js${urlVersion}"></script>
[#elseif requestUri?index_of("/view-packages",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-viewPackages.js${urlVersion}"></script>
[#elseif requestUri?index_of("/print-reservations",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-printReservations.js${urlVersion}"></script>
[#elseif requestUri?index_of("/view-reservations",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-viewReservations.js${urlVersion}"></script>
[#elseif requestUri?index_of("/deposit-management",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-depositManagement.js${urlVersion}"></script>
[#elseif requestUri?index_of("/deposit-purchase-result",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-depositPurchaseResult.js${urlVersion}"></script>
[#elseif requestUri?index_of("/deposit-purchase",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-depositPurchase.js${urlVersion}"></script>
[#elseif requestUri?index_of("/partner-registration",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerRegistration.js${urlVersion}"></script>
[#elseif requestUri?index_of("/forgot-password",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerForgotPassword.js${urlVersion}"></script>
[#elseif requestUri?index_of("/renew-password",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerRenewPassword.js${urlVersion}"></script>
[#elseif requestUri?index_of("/manage-profile",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerManageProfile.js${urlVersion}"></script>
[#elseif requestUri?index_of("/manage-subaccounts",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerManageSubAccount.js${urlVersion}"></script>
[#elseif requestUri?index_of("/manage-subaccount-access",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-partnerManageSubAccountAccess.js${urlVersion}"></script>
[#elseif requestUri?index_of("/tnc",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-tnc.js${urlVersion}"></script>
[#elseif requestUri?index_of("/revalidate-receipt",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-revalidateReceipt.js${urlVersion}"></script>
[#elseif requestUri?index_of("/view-revalidate-receipt",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-viewRevalidateReceipt.js${urlVersion}"></script>
[#elseif requestUri?index_of("/wot-receipt",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/partner-portal-slm/theme/default/js/scriptloader-wot-receipt.js${urlVersion}"></script>
[#elseif requestUri?index_of("/transaction-report",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-transactionReport.js${urlVersion}"></script>
[#elseif requestUri?index_of("/inventory-report",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-inventoryReport.js${urlVersion}"></script>
[#elseif requestUri?index_of("/package-report",0) > -1 && !cmsfn.editMode]
<script src="${ctx.contextPath}/resources/${channel}/theme/default/js/scriptloader-packageReport.js${urlVersion}"></script>
[/#if]

[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
[#assign isEditMode = cmsfn.editMode]
[#assign isAuthorPreview = cmsfn.authorInstance && cmsfn.previewMode]

[#assign isMaintenanceMode = starfn.isMaintenanceMode("${channel}") && !isOnPreview && !isEditMode && !isAuthorPreview]

[#assign templateName = state.templateName!""]
[#if isMaintenanceMode && templateName != "${channel}:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/${channel}/maintenance")}
[#elseif !isMaintenanceMode && templateName == "${channel}:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/${channel}")}
[/#if]
