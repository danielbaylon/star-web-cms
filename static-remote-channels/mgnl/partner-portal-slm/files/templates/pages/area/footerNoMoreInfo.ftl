[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
[#include "/partner-portal-slm/templates/macros/staticText/footer.ftl"]

<div class="clearfix"></div>
<div class="footer">
    <div class="footer-content">
        <div class="footer-main-content">
            <div class="more-information">
                &nbsp;
            </div>
            <div class="contact-us">
                <h4 style="text-transform: uppercase">We Are Here To Serve You</h4>
                <div class="office-info">
                    <img class="contact-icon" src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-phone.png"/>
                    <p>
                        Our Guest Services Officers are available at<br>
                        <span class="office-no"><s:text name="sentosa.contact.info" /></span><br>
                    ${staticText[lang]["contactInfo"]!"Hotline: 9618 1078 / Fax Number: 6274 9570"}
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="email-info">
                    <img class="contact-icon" src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-email.png" />
                    <p>You may reach us via email at <a href="mailto:partners@sentosa.com.sg" target="_blank" class="link-text">partners@sentosa.com.sg</a></p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>Supported Browsers: Explorer 8 and above, Firefox, Chrome, Safari<br>Copyright&copy;2014 Sentosa. All rights reserved.</p>
    </div>
</div>