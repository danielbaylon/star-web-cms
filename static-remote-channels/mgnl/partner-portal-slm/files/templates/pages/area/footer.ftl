[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
[#include "/partner-portal-slm/templates/macros/staticText/footer.ftl"]

<div class="clearfix"></div>
<div class="footer">
    <div class="footer-content">
        <div class="footer-main-content">
            <div class="more-information">
                <h4 style="text-transform: uppercase">More Information</h4>
                <ul>
                    <li><a href="http://www.sentosa.com.sg" target="_blank">Sentosa.com</a></li>
                    <li><a href="http://www.sentosa.com.sg/en/getting-to-around-sentosa/getting-to-sentosa" target="_blank">Getting to &amp; Around Sentosa</a></li>
                    <li><a href="http://www.sentosa.com.sg/en/about-us/sentosa-island" target="_blank">About Us</a></li>
                    <li><a href="/faq" target="_blank">FAQ</a></li>
                    <li><a href="http://www.sentosa.com.sg/en/legal-notices/" target="_blank">Legal Notices</a></li>
                    <li><a href="/data-protection-policy" target="_blank">Data Protection Policy</a></li>
                </ul>
            </div>
            <div class="contact-us">
                <h4 style="text-transform: uppercase">We Are Here To Serve You</h4>
                <div class="office-info">
                    <img class="contact-icon" src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-phone.png"/>
                    <p>
                        Our Guest Services Officers are available at<br>
                        <span class="office-no"><s:text name="sentosa.contact.info" /></span><br>
                    ${staticText[lang]["contactInfo"]!"Hotline: 9618 1078 / Fax Number: 6274 9570"}
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="email-info">
                    <img class="contact-icon" src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-email.png" />
                    <p>You may reach us via email at <a href="mailto:partners@sentosa.com.sg" target="_blank" class="link-text">partners@sentosa.com.sg</a></p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>Supported Browsers: Explorer 8 and above, Firefox, Chrome, Safari<br>Copyright&copy;2014 Sentosa. All rights reserved.</p>
    </div>
</div>