<#function objectToJsonFunction object>
    <#local json = "">
    <#if object?is_hash || object?is_hash_ex>
        <#local first="true">
        <#local json = json + '{'>
        <#assign keys = object?keys>
        <#list keys as key>
            <#if first="false"><#local json = json + ','></#if>
            <#local value = object[key] >
            <#local json = json + '"${key}": "${value?trim}"'>
            <#local first="false">
        </#list>
        <#local json = json + '}'>
    <#elseif object?is_enumerable>
        <#local first="true">
        <#local json = json + '['>
        <#list object as item>
            <#if first="false">
                <#local json = json + ','>
            </#if>
            <#local value = objectToJsonFunction(item) >
            <#local json = json + '${value?trim}'>
            <#local first="false">
        </#list>
        <#local json = json + ']'>
    <#else>
        <#local json = json + '"${object?trim}"'>
    </#if>
    <#return json>
</#function>