[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">
    <div class="heading-bar">
        <h2 class="heading">Offline Payment Submit</h2>
        <p class="countdown"><span class="time"><span id="confirmTimer"></span></span></p>
        <div class="clearfix"></div>
    </div>

        <div class="main-content-section" id="checkoutView">
    
            <!-- ko if:products().length != 0 -->
            <!-- ko foreach:products  -->
            <div class="items-details">
                <h3 class="heading" data-bind="text: title"></h3>
                <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                    <tr class="primary-multi-heading-bar">
                        <td width="40%">Product</td>
                        <td width="30%" class="center">Price</td>
                        <td width="10%" class="center">Quantity</td>
                        <td width="15%"class="center">Amount</td>
                        <td width="5%" class="center"> </td>
                    </tr>
                    <tbody data-bind="foreach: items">
                    <tr data-bind="css:{'main-items':!isTopup,'top-up-items':isTopup}">
                        <td width="40%">
                            <!-- ko if:isTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                            <span data-bind="text: title" class="product-item-name"></span>
                            <span data-bind="html: description"></span>
                        </td>
                        <td width="30%" class="center" data-bind="text: ticketType + ' - S$ ' + priceText"></td>
                        <td width="10%" class="center">
                            <!-- ko if:isTopup -->
                            <span data-bind="text: qty"></span>
                            <!-- /ko --><!-- ko ifnot:isTopup -->
                            <span data-bind="text: qty"></span>
                            <!-- /ko -->
                            <!--  <input type="text" class="input-textarea center" data-bind="value: qty, valueUpdate: 'keyup'">-->
                        </td>
                        <td width="15%" class="center" data-bind="text: 'S$ ' + totalText()"></td>
                        <td width="5%"></td>
                    </tr>
                    </tbody>
    
                    <tr class="subtotal-items main-inner-items">
                        <td width="70%" colspan="2">
                            <a class="link-text" style="cursor: pointer;" data-bind="visible: additionalTopups().length > 0, click: doToggleTopupsToAdd">
                                <i class="fa fa-plus-circle"></i> <span data-bind="text: topupsToAddText"></span>
                            </a>
                        </td>
                        <td class="subtotal-items-title" width="10%">Subtotal:</td>
                        <td width="15%" class="center" data-bind="text: 'S$ ' + subtotalText()"></td>
                        <td width="5%" class="center">&nbsp;</td>
                    </tr>
                </table>
            </div>
            <!-- /ko -->
    
            <!-- TOTALS -->
            <table cellpadding="0" cellspacing="0" width="100%" class="total-account-section-container">
                <tr class="subtotal-items main-inner-items">
                    <td width="40%">&nbsp;</td>
                    <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'Total Amount (excl. ' + gstRateStr() +'):'"></td>
                    <td class="center" width="30%" data-bind="text: 'S$ ' + totalNoGstText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
                <tr class="total-items subtotal-items">
                    <td class="no-border"width="40%">&nbsp;</td>
                    <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'GST ' + gstRateStr()"></td>
                    <td class="center" width="20%" data-bind="text: 'S$ ' + gstText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
                <tr class="main-items subtotal-items">
                    <td class="no-border" width="40%">&nbsp;</td>
                    <td class="subtotal-items-title highlight-title" colspan="2" width="35%">Grand Total:</td>
                    <td class="center highlight-attribute" width="20%"  data-bind="text: 'S$ ' + grandTotalText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
            </table>
            <!-- /ko -->
        </div>
    
        <div id="offlinePaymentForm">
            <div id="offlinePaymentFormSection1n2">
            <div id="offlinePaymentFormSection1">
                <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                    <tr class="primary-multi-heading-bar">
                        <td width="100%">Offline Payment Detail</td>
                    </tr>
                </table>
                <table  cellpadding="0" cellspacing="0" width="100%" class="main-items">
                    <tbody>
                    <tr>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="25%" >Order Reference number:</td>
                        <td style="border-bottom:none;"  width="25%" colspan="5"><span data-bind="text: receiptNumber"></span></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="25%" >Name of Company:</td>
                        <td style="border-bottom:none;"  width="25%" colspan="5">SENTOSA DEVELOPMENT CORPORATION</td>
                    </tr>
                    <tr>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="15%">Bank Code:</td>
                        <td style="border-bottom:none;"  width="25%">7339</td>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="20%"></td>
                        <td style="border-bottom:none;"  width="25%"></td>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="15%" >Bank Address:</td>
                        <td style="border-bottom:none;"  width="25%" colspan="7">65 CHULIA STREET OCBC CENTRE SINGAPORE 049513</td>
                    </tr>
                    <tr>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="15%" >Bank Account No:</td>
                        <td style="border-bottom:none;"  width="25%">501559827002</td>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"  width="20%">Branch Code:</td>
                        <td style="border-bottom:none;"  width="25%">501</td>
                        <td style="border-bottom:none;"  width="5%">&nbsp;</td>
                    </tr>
                    <tr style="border-bottom: 1px solid green;">
                        <td width="5%">&nbsp;</td>
                        <td width="15%" >Branch Account Currency:</td>
                        <td width="25%">SGD</td>
                        <td width="5%">&nbsp;</td>
                        <td width="20%">Swift Code:</td>
                        <td width="25%">OCBCSGSG</td>
                        <td width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Payment Type:</td>
                        <td style="border-bottom:none;"   width="25%"><select style="width: 220px;" data-bind="options: paymentTypes,
                           optionsText: 'name',
                           optionsValue: 'code',
                           value: paymentType,
                           optionsCaption: 'Choose...'"></select>
                        </td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Status:</td>
                        <td style="border-bottom:none;"   width="25%" data-bind="text: status" > </td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Bank:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%"><input type='text' class="input-textarea" maxlength="200" data-bind="value: bankName"/></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Bank Country:</td>
                        <td style="border-bottom:none;"   width="25%">
                            <select data-bind="options: countries,
                           optionsText: 'name',
                           optionsValue: 'name',
                           value: bankCountry,
                           optionsCaption: 'Choose...'"></select>
                        </td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;" width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Payment Reference Number:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%" ><input type='text' class="input-textarea" maxlength="200" data-bind="value: referenceNum"/></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Payment Date:</td>
                        <td style="border-bottom:none;"   width="25%"><input type="text" id="paymentDatePicker" name="paymentDateStr" maxlength="10" style="width: 140px;" data-bind="value:paymentDateStr"/></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Amount:</td>
                        <td style="border-bottom:none;"   width="25%">S$ <span data-bind="text: totalAmountStr"></span></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Order Created Date:</td>
                        <td style="border-bottom:none;"   width="25%"><span data-bind="text: createDtStr"></span></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Remarks:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%"><textarea rows="4" class="input-textarea" data-bind="value: remarks" maxlength="1000" ></textarea></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="25%"></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  == 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%"><span style="color:red;">Important Notes:</span></td>
                        <td style="border-bottom:none;"   width="25%" colspan="5"><span style="color:red;">Thank you for your order.  If there is bank transfer fee incurred for your remittance, please remember to include the fee in your payment to us.</span></td>
                    </tr>
                    <tr data-bind="visible: status()  != 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Payment Type:</td>
                        <td style="border-bottom:none;"   width="25%"  data-bind="text: paymentTypeText" > </td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Status:</td>
                        <td style="border-bottom:none;"   width="25%" data-bind="text: status" > </td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  != 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Bank:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%" data-bind="text: bankName"></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td  style="border-bottom:none;"   width="20%">Bank Country:</td>
                        <td style="border-bottom:none;"   width="25%" data-bind="text: bankCountryText"></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  != 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Payment Reference Number:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%"  data-bind="text: referenceNum"></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Payment Date:</td>
                        <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;"   width="25%"  data-bind="text: paymentDateStr"></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: status()  != 'Pending_Submit'">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Amount:</td>
                        <td style="border-bottom:none;"   width="25%">S$ <span data-bind="text: totalAmountStr"></span></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">Order Created Date:</td>
                        <td style="border-bottom:none;"   width="25%"><span data-bind="text: createDtStr"></span></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
                    <tr data-bind="visible: (status() == 'Approved' || status() == 'Rejected')  ">
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="15%">Approved/Rejected Date:</td>
                        <td style="border-bottom:none;"   width="25%"><span data-bind="text: offlinePaymentApprovalDateText"></span></td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="20%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="25%">&nbsp;</td>
                        <td style="border-bottom:none;"   width="5%">&nbsp;</td>
                    </tr>
    
                    <tr data-bind="visible: status()  != 'Pending_Submit'">
                        <td style="border-bottom:none;" width="5%">&nbsp;</td>
                        <td style="border-bottom:none;" width="15%"><span style="color:red;">Important Notes:</span></td>
                        <td style="border-bottom:none;" width="25%" colspan="5"><span style="color:red;">Thank you for your order.  If there is bank transfer fee incurred for your remittance, please remember to include the fee in your payment to us.</span></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="padding:5px;"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="offlinePaymentFormSection2">
                <div data-bind="visible: status() != 'Pending_Submit'">
                    <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                        <tr class="primary-multi-heading-bar">
                            <td width="100%">Transaction Remarks</td>
                        </tr>
                    </table>
                    <table  cellpadding="0" cellspacing="0" width="100%" class="main-items">
                        <!-- ko if: hasChatHists -->
                        <tbody data-bind="foreach: chatHists">
                        <tr>
                            <td style="border-bottom:none;" width="5%;"></td>
                            <td style="border-bottom:none;" width="23%;">
                                <div >
                                    <span data-bind="text: username"></span>
                                </div>
                                <div>
                                    @<span data-bind="text: createDtStr"></span>
                                </div>
                            </td>
                            <td style="border-bottom:none; word-wrap: break-word; word-break: break-all;" width="69%;">
                                <div >
                                    <span style=" word-wrap: break-word; word-break: break-all;" data-bind="html: content"></span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                        <!-- /ko -->
                        <!-- ko ifnot: hasChatHists -->
                        <tbody>
                        <tr>
                            <td style="border-bottom:none;" width="5%;"></td>
                            <td style="border-bottom:none;" width="23%;">
                                <div >
                                    No Transaction Remarks
                                </div>
                            </td>
                            <td style="border-bottom:none;" width="69%;">
                            </td>
                        </tr>
                        </tbody>
                        <!-- /ko -->
                    </table>
                </div>
            </div>
            <div id="offlinePaymentFormSection3">
                <div class="action-container">
                    <a class="btn btn-third"  id="btnCancel">Back To List Page</a>
                    <a class="btn btn-secondary" id="btnSubmit" data-bind="visible: status() == 'Pending_Submit'">Submit For Approval</a>
                    <a class="btn btn-secondary" id="btnSubmitUserComments" data-bind="visible: status() == 'Pending_Approval'">Send Notification</a>
                </div>
                <div class="action-container">
                    <a  class="btn btn-primary"  id="btnIncomplete"  data-bind="visible: status()  == 'Pending_Submit'">Withdraw Payment Request</a>
                </div>
            </div>
            </div>
            <div id="offlinePaymentFormSection4">
                <div id="PartnerNotificationWindowDiv" style="display: none;">
                    <div style="width: 100%;">
                        <div><br/></div>
                        <table style="width: 95%">
                            <tr>
                                <td>
                                    <span>Content of Notification</span>&nbsp;<span style="font-size: 9px;color: grey;">(Content of notification must be within minimum 20 characters and maximum 1000 characters)</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <textarea rows="4" class="input-textarea" data-bind="value: notificationText" maxlength="1000"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 100%;">
                        <div class="action-container">
                            <a  class="btn btn-third" data-bind="click: cancelNotification" >Cancel</a>
                            <a  class="btn btn-primary" data-bind="click: sendNotification" >Send Notification</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div>
    [@cms.area name="adBanner"/]
    </div>
</div>