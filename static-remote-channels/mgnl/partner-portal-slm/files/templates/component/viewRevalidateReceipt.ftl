<div id="successdv" class="breadcrumb" style="display:none">
    <h2 class="heading" style="text-align: center">Loading Transaction Status.....</h2>
</div>

<div id="errordv" class="content" style="display: none;">
    <div class="heading-bar">
        <h2 class="heading" style="float: none; text-align: center;">Revalidation Transaction Status</h2>
        <div class="clearfix"></div>
    </div>
    <span style="text-align: center; display: block"></span>
    <br>
    <strong style="text-align: center; display: block"><a href="#" id="revalLink">Click here to proceed to revalidate again.</a></strong>
</div>


<div class="main-content-section-container" id="revalidateReceiptView" style="display:none">
    <div class="receipt-section">
        <h3 class="heading center" data-bind="text: 'Receipt Number: ' + revalReceiptNum()"></h3>
    </div>

    <div class="main-content-section">
        <div class="information-section-container">
            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   class="information-section">
                <tr>
                    <td width="20%">Partner Code:</td>
                    <td width="25%" data-bind="text: accountCode"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Validity Start Date:</td>
                    <td data-bind="text: validateStartDateStr"></td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td data-bind="text: orgName"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Validity End Date:</td>
                    <td data-bind="text: validityEndDateStr"></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td data-bind="text: address"></td>
                    <td width="5%">&nbsp;</td>
                    <td>Revalidation Date/Time:</td>
                    <td data-bind="text: revalidateDtTmStr"></td>
                </tr>
                <tr>
                    <td>Username:</td>
                    <td data-bind="text: revalidatedBy"></td>
                    <td width="5%">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Transaction Status:</td>
                    <td data-bind="text: tmStatus"></td>
                    <td width="5%">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Payment Type:</td>
                    <td data-bind="text: revalPaymentType"></td>
                    <td width="5%">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%"
               class="items-tickets-content-container package-items"
               border="0">
            <tr class="forth-multi-heading-bar">
                <td width="25%">Product</td>
                <td width="25%" class="center">Revalidation Fee Per Ticket</td>
                <td width="25%" class="center">Quantity</td>
                <td width="25%" class="center">Amount</td>
            </tr>

            <!-- ko foreach: receiptitems -->
            <tr data-bind="css: {'main-items': !itemTopup(), 'top-up-items': itemTopup()}">
                <td width="25%">
                    <!-- ko if:itemTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                    <span data-bind="text: displayName"></span>
                </td>
                <td width="25%" class="center" data-bind="text: singleRevalFeeFullStr"></td>
                <td width="25%" class="center" data-bind="text: unpackedQty"></td>
                <td width="25%" class="center" data-bind="text: revalFeeFullStr"></td>
            </tr>
            <!-- /ko -->
        </table>
        <table cellpadding="0" cellspacing="0" width="100%"
               class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="30%" data-bind="text: 'GST ' + revalGSTRateStr() + ':'"></td>
                <td class="center" width="20%" data-bind="text: currency() + '&nbsp;' + revalGSTStr()"></td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2"
                    width="30%" data-bind="text: 'Total Amount (excl. GST ' + revalGSTRateStr() + '):'"></td>
                <td class="center" width="20%" data-bind="text: currency() + '&nbsp;' + revalExclGSTStr()"></td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" colspan="2" width="50%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title"
                    colspan="2" width="30%">Grand Total:</td>
                <td class="center highlight-attribute" width="20%" data-bind="text: currency() + '&nbsp;' + totalRevalFeeStr()"></td>
            </tr>
        </table>

        <#--<span data-txn-id="true" style="display: none;">${transId}</span>-->

        <div class="action-container">
            <a class="btn btn-primary" id="btnRegen">Regenerate Revalidation Receipt</a>
        </div>
    </div>
</div>

<div class=" clearfix"></div>