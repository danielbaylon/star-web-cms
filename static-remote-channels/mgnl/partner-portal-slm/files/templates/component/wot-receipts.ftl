
<div class="main-content-section-container pendingInitElementIndicator">
    <h2 class="heading">Wings of Time Receipt</h2>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section">
        <div class="filter-section background">
            <div class="pendingInitElementIndicator" style="width: 100%;" id="wot-receipt-generation">
                <table cellpadding="0" cellspacing="10" width="100%" class="filter-section-content">
                    <tr>
                        <td>
                            <div>
                                <div>
                                    <div data-bind="visible: 'D' == receiptType()">
                                        <span style="font-weight: bold; text-decoration: underline;">Daily Receipt</span>
                                    </div>
                                    <div data-bind="visible: 'M' == receiptType()">
                                        <span style="font-weight: bold; text-decoration: underline;">Monthly Receipt</span>
                                    </div>
                                </div>
                                <div><br/></div>
                                <div>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 100px;"><span>Receipt Type: </span></td>
                                            <td>
                                                <div data-bind="foreach: receiptTypeList">
                                                    <input type="radio" name="receiptType" data-bind="value: code, checked: $page.wotReceiptModel.receiptType"/>&nbsp;<span data-bind="text: name"></span>&nbsp;&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div><br/></div>
                                <div>
                                    <div data-bind="visible: 'D' == receiptType()">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;"><span>Select Date: </span></td>
                                                <td>
                                                    <input type="text" value="" name="receiptDate" id="receiptDate" data-bind="value : receiptDate"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div data-bind="visible: 'M' == receiptType()">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;"><span>Select Month: </span></td>
                                                <td>
                                                    <input type="text" value="" name="receiptMonth" id="receiptMonth" data-bind="value : receiptMonth"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div><br/></div>
                                <div>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 100px;">&nbsp;</td>
                                            <td>
                                                <input class="btn btn-primary thin-btn" data-bind="click: filter" type="button" value="Generate"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
    <form id="_receipt_generation_form" target="_receipt_generation_frame" method="post" action="">
        <input type="hidden" name="param" id="_receipt_generation_form_param" value=""/>
    </form>
    <iframe id="_receipt_generation_frame" name="_receipt_generation_frame"></iframe>
</div>
<div class=" clearfix"></div>
</div>