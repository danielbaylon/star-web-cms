<div class="main-content-section-container" id="receiptView">

    <div class="general-info-section" style="padding-bottom: 2em; display: none;" id="transactionAbnormalSection">
        <div class="heading-bar">
            <h2 class="heading">Purchase Transaction Status</h2>
            <div class="clearfix"></div>
        </div>

        <p class="content" id="pendingMsg"><img src="/img/ajax-loader.gif">We are still processing your transaction. Please wait a few moments.</p>
        <p class="content" id ="tasMsg">
            Your transaction is unsuccessful. Please try again.<br/>
            For enquiries, please contact our Guest Services Officers at Hotline: 9618 1078 / Fax Number: 6274 9570, or email us at partners@sentosa.com.sg.
            <br/>
            <br/>
            <strong>
                Your cart still contains the items you have selected.
                <a href="cart">Click here to proceed to checkout again.</a>
            </strong>
        </p>

    </div>

    <div class="general-info-section" style="padding-bottom: 2em; display: none;" id="transactionOkSection">
        <div class="breadcrumb">
            <h2 class="heading" style="text-align: center">Transaction is successful!</h2>
        </div>
    </div>


    <div class="receipt-section">
        <h3 class="heading center">Receipt Number: <span data-bind="text: receiptNumber"></span></h3>
        <p>Date Of Purchase: <span data-bind="text: dateOfPurchase"></span></p>
    </div>

    <div class="main-content-section" >
        <div class="information-section-container">
            <h4 class="primary-heading-bar">Receipt Information</h4>
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                <tr>
                    <td width="20%">Partner Code:</td>
                    <td width="25%" data-bind="text: accountCode"></td>
                    <td width="5%">&nbsp;</td>
                    <td width="20%">&nbsp;</td>
                    <td width="30%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td data-bind="text: orgName"></td>
                    <td>&nbsp;</td>
                    <td>Validity Start Date:</td>
                    <td data-bind="text: validityStartDate"></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td data-bind="text: address"></td>
                    <td>&nbsp;</td>
                    <td>Validity End Date:</td>
                    <td data-bind="text: validityEndDate"></td>
                </tr>
                <tr>
                    <td>Username:</td>
                    <td data-bind="text: username"></td>
                    <td>&nbsp;</td>
                    <td>Receipt Status:</td>
                    <td>Available</td>
                </tr>
                <tr>
                    <td>Transaction Status:</td>
                    <td><span data-bind="text: transStatus"></span></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Payment Type:</td>
                    <td>VISA</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <!-- ko if:products().length != 0 -->
        <!-- ko foreach:products  -->
        <div class="items-details">
            <h3 class="heading" data-bind="text: title"></h3>
            <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                <tr class="primary-multi-heading-bar">
                    <td width="40%">Product</td>
                    <td width="30%" class="center">Price</td>
                    <td width="10%" class="center">Quantity</td>
                    <td width="15%"class="center">Amount</td>
                    <td width="5%" class="center"> </td>
                </tr>
                <tbody data-bind="foreach: items">
                <tr data-bind="css:{'main-items':!isTopup,'top-up-items':isTopup}">
                    <td width="40%">
                        <!-- ko if:isTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                        <span data-bind="text: title" class="product-item-name"></span>
                        <span data-bind="html: description"></span>
                    </td>
                    <td width="30%" class="center" data-bind="text: ticketType + ' - S$ ' + priceText"></td>
                    <td width="10%" class="center">
                        <!-- ko if:isTopup -->
                        <span data-bind="text: qty"></span>
                        <!-- /ko --><!-- ko ifnot:isTopup -->
                        <span data-bind="text: qty"></span>
                        <!-- /ko -->
                        <!--  <input type="text" class="input-textarea center" data-bind="value: qty, valueUpdate: 'keyup'">-->
                    </td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + totalText()"></td>
                    <td width="5%"></td>
                </tr>
                </tbody>

                <tr class="subtotal-items main-inner-items">
                    <td width="70%" colspan="2">
                        <a class="link-text" style="cursor: pointer;" data-bind="visible: additionalTopups().length > 0, click: doToggleTopupsToAdd">
                            <i class="fa fa-plus-circle"></i> <span data-bind="text: topupsToAddText"></span>
                        </a>
                    </td>
                    <td class="subtotal-items-title" width="10%">Subtotal:</td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + subtotalText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!-- /ko -->

        <!-- TOTALS -->
        <table cellpadding="0" cellspacing="0" width="100%" class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'Total Amount (excl. GST ' + gstRateStr() +'):'"></td>
                <td class="center" width="30%" data-bind="text: 'S$ ' + totalNoGstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border"width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'GST ' + gstRateStr()"></td>
                <td class="center" width="20%" data-bind="text: 'S$ ' + gstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" width="40%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title" colspan="2" width="35%">Grand Total:</td>
                <td class="center highlight-attribute" width="20%"  data-bind="text: 'S$ ' + grandTotalText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <div class="action-container">
            <a class="btn btn-secondary" href="view-inventory">View Inventory</a>
        </div>
    </div>
    <div>
        [@cms.area name="adBanner"/]
    </div>
</div>