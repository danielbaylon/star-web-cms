[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
[#include "/partner-portal-slm/templates/function/objectToJsonFunction.ftl"]

<div id="main-content-section" class="main-content-section-container">
    <h2 class="heading">Mix &amp; Match Package</h2>
    <div class="main-content-section">
        <div class="information-section-container">
            <h4 class="primary-heading-bar">Confirm Package</h4>
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                <tr>
                    <td width="20%">New Package Name:</td>
                    <td width="80%" data-bind="text: name"></td>
                </tr>
                <tr>
                    <td width="20%">Description:</td>
                    <td width="80%" data-bind="text: description"></td>
                </tr>
                <tr>
                    <td width="20%">Expiry Date:</td>
                    <td width="80%" data-bind="text: expiryDateStr"></td>
                </tr>
                <tr>
                    <td width="20%">Quantity Per Product:</td>
                    <td width="80%" data-bind="text: qty"></td>
                </tr>
            </table>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="items-tickets-content-container package-items" border="0">
            <tr class="forth-multi-heading-bar">
                <td width="50%">Product</td>
                <td width="20%">Quantity</td>
                <td width="30%" class="center">Receipt Number</td>
            </tr>
            <!-- ko foreach: transItems -->
            <tr class="main-items" data-bind="css: {odd: $index()%2==1, even: $index()%2==0}">
                <td width="50%">
                    <span data-bind="text: displayName" class="product-item-name"></span>
                    <span data-bind="html: description"></span>
                </td>
                <td width="20%" data-bind="text: pkgQty"></td>
                <td width="30%" data-bind="text: receiptNum" class="center"></td>
            </tr>
                 <!-- ko foreach: topupItems -->
                    <tr>
                        <td><i class="fa fa-plus-circle"></i><span data-bind="text: displayName"></span></td>
                        <td style="padding: 0.8em" data-bind="text: pkgQty"></td>
                        <td >&nbsp;</td>
                    </tr>

                <!-- /ko -->
            <!-- /ko -->
        </table>

        <div class="error-section"><span class="error-note"><i class="fa fa-exclamation-circle"></i> Important Note:</span> <BR>-Generated Ticket will not be eligible for refund.
            <BR>-Please note that the expiry date of your created package is decided by the earliest expiry date of your selected ticket(s).</div>

        <div class="terms-conditions">
            <label for="agreeTnc">
                <input type="checkbox" id="agreeTnc"> <span class="star">*</span>
                I have read and agree to both the <a href="/tnc" target="_blank" class="link-text">Terms and Conditions</a> as set out in the Sentosa Online Store
                and Sentosa Leisure Management Pte Ltd's ("SLM") <a href="/data-protection-policy" target="_blank" class="link-text">Data Protection Policy</a>,
                including as to how my personal data may be collected, used, disclosed and processed, and confirm that
                all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself,
                I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents,
                to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out
                in the Data Protection Policy.
            </label>
        </div>
        <div class="action-container">
            <input id="editPkg" type="button" class="btn btn-secondary" value="Edit" />
            <input id="genETicket" type="button" class="btn btn-primary" value="Generate E-Ticket" data-bind="visible: pkgTktMedia() == 'ETicket', click: genETicket"/>
            <input id="genPinCode" type="button" class="btn btn-primary" value="Generate PINCODE"  data-bind="visible: pkgTktMedia() == 'Pincode', click: genPinCode"/>
            <input id="genExcel" type="button" class="btn btn-primary" value="Generate Excel" data-bind="visible: pkgTktMedia() == 'ExcelFile', click: genExcel"/>
        </div>
    </div>
</div>
<div class=" clearfix"></div>