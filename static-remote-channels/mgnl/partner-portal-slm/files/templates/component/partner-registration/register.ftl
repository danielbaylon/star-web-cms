[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<style>
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }
    .note {
        max-width: 750px;
        width: 100%;
    }

    .notetop {
        background-color: #26b67c;
        border-color: #638c9c;
        border-style: solid;
        border-width: 1px 1px 0 1px;
        color: #000066;
        padding: 5px;
    }

    .notebody {
        background: none repeat scroll 0 0 #f5faff;
        border-color: #638c9c;
        border-style: solid;
        border-width: 0 1px 1px 1px;
        color: #323232;
        font-style: italic;
        margin: 0 auto 8px;
        padding: 4px;
        font-size: 0.9em;
    }

    #partnerForm .input-textarea{
        text-transform: uppercase;
    }
</style>
<div class="main-content-section-container">
    <div id="successdv" class="breadcrumb" style='display:none;'>
        <h2 class="heading" style="text-align: center">Thank you for your registration! You will receive an email after your account has been approved!</h2>
    </div>
    <div id='registerDv'>
        <h2 class="heading">Partner Registration</h2>
        <div class="main-content-section">
            <div class="items-records-container">
                <h4 class="primary-heading-bar">Partner Information</h4>
                <div>
                    <br/>
                </div>
                <div id="errordv" class="error-section" style="display: none;"></div>
                <form method="POST" action="${API_PREFIX!""}/publicity/partner-registration/register" enctype="multipart/form-data" id="partnerForm">
                    <table cellpadding="0" cellspacing="0" width="100%"
                           class="information-section">
                        <tr>
                            <td>Organization Name<span class="star">*</span>:
                            </td>
                            <td><input type="text"  data-bind="value: orgName"
                                       class="input-textarea" maxlength="100"></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="30%">&nbsp;</td>
                            <td width="54%">
                                <div class="note">
                                    <div class="notetop">Note:</div>
                                    <div class="notebody">
                                        The Partner Username will be used as your login name with
                                        suffix of "_ADMIN", please refer to below when you create Partner Username.<br>
                                        -Between six to eight characters in length.<br>
                                        -English uppercase characters (A through Z) or lowercase characters (a through z)<br>
                                        -Numerals (0 through to 9)<br>
                                    </div>
                                </div>
                            </td>
                            <td width="16%">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="30%">Partner Username<span class="star">*</span>:</td>
                            <td width="54%"><input type="text"
                                                   maxlength="8"
                                                   data-bind="value: accountCode,valueUpdate: 'afterkeydown'"
                                                   class="input-textarea"
                                                   style="width: 120px;"> <input
                                    type="button" value="Check" data-bind="click: checkPaCode"
                                    class="btn btn-secondary thin-btn">
                                <span  style="font-size: 0.8em; font-weight: bold;">Is Partner Username in use?</span></td>
                            <td width="16%">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="30%">Main Account Login
                                Name:</td>
                            <td >
                                <div style="display: inline-block;width: 150px;">
                                    <span data-bind="text: username" style="text-transform: uppercase;"></span>
                                </div>
                                <span style="color:#17bb17;" data-bind="text: usernameMsg"></span>
                                <span style="color:red;" data-bind="text: usernameErrMsg"></span>
                            </td>
                            <td width="16%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Registered Country<span class="star">*</span>:
                            </td>
                            <td><select
                                    data-bind="
                                         options: ctys,
                                         optionsText: 'ctyName',
                                         value: selCountry,
                                     optionsCaption: 'Choose...'">
                            </select></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>Nature of Business<span class="star">*</span>:</td>
                            <td><select
                                    data-bind="
                                         options: paTypes,
                                         optionsText: 'label',
                                         value: orgType,
                                     optionsCaption: 'Choose...'">
                            </select></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Unique Entity Number(UEN)<span
                                    class="star">*</span>:
                            </td>
                            <td><input type="text"  data-bind="value: uen"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>Travel Agent License Number.<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: licenseNum" class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Travel Agent License Expiry Date<span data-bind="visible: isTa"  class="star">*</span>:</td>
                            <td><input type="text" id='licenseExpDate' data-bind="value: licenseExpDate" class="input-textarea" maxlength="10"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <!--address, begin -->
                        <tr>
                            <td>Business Registered Address<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: address" class="input-textarea" maxlength="60"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Business Registered Postal Code<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: postalCode" class="input-textarea" maxlength="10"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Business Registered City:</td>
                            <td><input type="text"  data-bind="value: city" class="input-textarea" maxlength="60"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Correspondence Address<span class="star">*</span>:</td>
                            <td>
                                <div><input type="checkbox" data-bind="checked: useBizAddrAsCorrespAddr, click : applyBizAddrAsCorrespAddr"/>&nbsp;Same as business registered address</div>
                                <div>
                                    <input type="text"  data-bind="value: correspondenceAddress, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="60" />
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Correspondence Postal Code<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: correspondencePostalCode, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="10"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Correspondence City:</td>
                            <td><input type="text"  data-bind="value: correspondenceCity, enable: isCorrespondenceAddrEnabled" class="input-textarea" maxlength="60"></td>
                            <td>&nbsp;</td>
                        </tr>

                        <!--address, end-->
                        <tr id="partnerExistingDocSection" style="display:none;">
                            <td>
                                Existing Document Attachment<span class="star">*</span>:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%" class="information-section" id="prevFileList"></table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <!-- Upload File part Start -->
                        <tr>
                            <td colspan="3" class="column-multi-attribute">
                                <table cellpadding="0" cellspacing="0" width="100%"
                                       class="information-section">
                                    <tr>
                                        <td width="30%" valign="top">
                                            Document Attachment<span class="star">*</span>:
                                        </td>
                                        <td width="54%"><span class="doc-note">1. Maximum file size 4MB.<br>2. Supported file format for attachment are PDF, Microsoft Word (DOC, Docx, RTF, TXT), image files (JPEG, JPG, BMP, PNG, TIFF).</span></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="column-multi-attribute">
                                            <table cellpadding="0" cellspacing="0"
                                                   width="100%" class="information-section">
                                                <tr>
                                                    <td width="30%">
                                                        <select  style='width: 260px;' data-bind="
                                                                options: docTypes,
                                                                optionsText: 'optionText',
                                                                optionsValue: 'optionValue',
                                                                value: file1Type">
                                                        </select>
                                                    </td>
                                                    <td width="38%"><input
                                                            name="file1" type="file" class="input-file"></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">
                                                        <select  style='width: 260px;' data-bind="
                                                                options: docTypes,
                                                                optionsText: 'optionText',
                                                                optionsValue: 'optionValue',
                                                                value: file2Type">
                                                        </select>
                                                    </td>
                                                    <td width="38%"><input
                                                            name="file2" type="file" class="input-file"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- Upload File part End -->
                        <tr>
                            <td>Contact Person<span class="star">*</span>:
                            </td>
                            <td><input type="text" data-bind="value: contactPerson"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Designation or Position<span
                                    class="star">*</span>:
                            </td>
                            <td><input type="text" data-bind="value: contactDesignation"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Office No.<span class="star">*</span>:
                            </td>
                            <td><div>
                                <input type="text"  data-bind="value: telNum"
                                       class="input-textarea" maxlength="50">
                            </div>
                                <div>+(country code)(area code)(phone number)</div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Mobile No.<span class="star">*</span>:
                            </td>
                            <td><div>
                                <input type="text"  data-bind="value: mobileNum"
                                       class="input-textarea" maxlength="50">
                            </div>
                            <div>+(country code)(area code)(mobile number)</div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Fax No.:</td>
                            <td><div>
                                <input type="text"  data-bind="value: faxNum"
                                       class="input-textarea" maxlength="20"></div>
                                <div>+(country code)(area code)(fax number)</div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Email<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: email"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Reconfirm Email<span class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: reconfirmEmail"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Website (if Available):</td>
                            <td><input type="text"  data-bind="value: website"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <!--
                        <tr>
                            <td>Language Preference:</td>
                            <td><input type="text" data-bind="value: languagePreference"
                                       class="input-textarea"></td>
                            <td>&nbsp;</td>
                        </tr>
                        -->
                        <tr>
                            <td>Key market / Country<span data-bind="visible: isTa" class="star">*</span>:</td>
                            <td><input type="text"  data-bind="value: mainDestinations"
                                       class="input-textarea" maxlength="50"></td>
                            <td>&nbsp;</td>
                        </tr>

                        [#assign recaptchaEnabled = starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.enabled")]
                        [#if recaptchaEnabled == "Y"]
                            <tr>
                                <td>Verification:</td>
                                <td width="70%">
                                    <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=${starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.public.key")!}">
                                    </script>
                                    <noscript>
                                        <iframe src="https://www.google.com/recaptcha/api/noscript?k=${starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.public.key")!}"
                                                height="300" width="500" frameborder="0"></iframe><br>
                                    <textarea name="recaptcha_challenge_field" id="recaptcha_challenge_field" rows="3" cols="40">
                                        </textarea>
                                        <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
                                    </noscript>
                                </td>
                            </tr>
                        [/#if]

                        <tr>
                            <td></td>
                            <td>
                                <input type="checkbox" data-bind="checked: tnc"/>
                                I agree with the <a href="${rootPath!""}/partner-registration-tnc">T & C</a>.
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="inner-action-container">
                            <td>&nbsp;</td>
                            <td><input  data-bind="click: savePartner"
                                        class="btn btn-primary" type="button"
                                        value="Submit" > <input
                                    class="btn btn-third" type="reset"
                                    value="Cancel"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
    <iframe style="display: none;" name="partner_file_download_frame"></iframe>
</div>
