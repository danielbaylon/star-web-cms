[#-------------- INCLUDE AND ASSIGN PART --------------]
[#include "/mtk/templates/includes/init.ftl"]
[#-- Image --]
[#-- Basic positioning of an image below or above the text --]
[#assign imagePosition = content.imagePosition!"below"]

[#-- CSS default --]
[#if !divClass?has_content]
    [#assign divClass = "text-section"]
[/#if]

[#-- Image css classes --]
[#assign hasImage = false]
[#assign imageHtml = ""]

[#if content.image?has_content]
    [#assign hasImage = true]
    [#assign divClass = "${divClass} text-image-section"]
    [#assign imageClass = "content-image-${imagePosition}"]
    [#assign rendition = damfn.getRendition(content.image, "original")]
    [#include "/partner-portal-slm/templates/macros/imageResponsive.ftl"]
    [#assign imageHtml][@imageResponsive rendition content imageClass false def.parameters /][/#assign]
    [#assign imageHyperlink = content.url!"javascript:void(0);"]
[/#if]


[#-------------- RENDERING PART --------------]

[#assign pageNode = cmsfn.page(content)!"unknown"]
<a href="${imageHyperlink}" target="_blank">
    <img src="${imageLink}" alt="${imageAlt}">
</a>
<br>
<br>
