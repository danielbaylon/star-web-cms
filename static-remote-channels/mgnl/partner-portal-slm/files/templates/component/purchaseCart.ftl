[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">
    <div class="heading-bar">
        <h2 class="heading">Shopping Cart</h2>
        <div class="clearfix"></div>
    </div>

    <div class="main-content-section" id="checkoutView">
        <!-- PRODUCTS -->
        <!-- ko if:products().length == 0 -->
        <div class="empty-category">
            <h1 class="title" style="text-align: center;">Your shopping cart is empty.</h1>
        </div>
        <!-- /ko -->

        <div class="alert alert-error" data-bind="text: errorMessage, visible: errorMessage() != ''" style="display:none"></div>

        <!-- ko if:products().length != 0 -->
        <!-- ko foreach:products  -->
        <div class="items-details">
            <h3 class="heading" data-bind="text: title"></h3>
            <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                <tr class="primary-multi-heading-bar">
                    <td width="40%">Product</td>
                    <td width="30%" class="center">Price</td>
                    <td width="10%" class="center">Quantity</td>
                    <td width="15%"class="center">Amount</td>
                    <td width="5%" class="center">Remove</td>
                </tr>
                <tbody data-bind="foreach: items">
                <tr data-bind="css:{'main-items':!isTopup,'top-up-items':isTopup}">
                    <td width="40%">
                        <!-- ko if:isTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                        <span data-bind="text: title" class="product-item-name"></span>
                        <span data-bind="html: description"></span>
                    </td>
                    <td width="30%" class="center" data-bind="text: ticketType + ' - S$ ' + priceText"></td>
                    <td width="10%" class="center">
                        <!-- ko if:isTopup -->
                        <span data-bind="text: qty"></span>
                        <!-- /ko --><!-- ko ifnot:isTopup -->
                        <input type="text" class="input-textarea center" data-bind="value: qty, valueUpdate: 'keyup'">
                        <!-- /ko -->
                    </td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + totalText()"></td>
                    <td width="5%">
                        <a style="cursor: pointer" class="icon-remove"
                           data-bind="click: $parent.doRemoveItem, attr: {'data-cart-id': cartId}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
                </tbody>
                <tbody data-bind="foreach: additionalTopups, visible: showTopupsToAdd">
                <tr>
                    <td colspan="5" class="column-multi-attribute">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="booking-tickets-top-up-items-container">
                            <tr class="main-inner-items">
                                <td width="37%"><i class="fa fa-plus-circle"></i> <span data-bind="text: title"></span></td>
                                <td width="30%" class="center" data-bind="text: ticketType + ' - S$ ' + priceText"></td>
                                <td width="10%" class="center" data-bind="text: qty"></td>
                                <td width="15%" class="center" data-bind="text: 'S$ ' + totalText()"></td>
                                <td width="*" class="icon-remove">
                                    <input type="checkbox" class="input-checkbox"
                                           data-bind="checked: includeInCart, visible: topupSelectable">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
                <tr class="subtotal-items main-inner-items">
                    <td width="70%" colspan="2">
                        <a class="link-text" style="cursor: pointer;" data-bind="visible: additionalTopups().length > 0, click: doToggleTopupsToAdd">
                            <i class="fa fa-plus-circle"></i> <span data-bind="text: topupsToAddText"></span>
                        </a>
                    </td>
                    <td class="subtotal-items-title" width="10%">Subtotal:</td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + subtotalText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!-- /ko -->

        <!-- TOTALS -->
        <table cellpadding="0" cellspacing="0" width="100%" class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'Total Amount (excl. ' + gstRateStr() +'):'"></td>
                <td class="center" width="30%" data-bind="text: 'S$ ' + totalNoGstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border"width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'GST ' + gstRateStr()"></td>
                <td class="center" width="20%" data-bind="text: 'S$ ' + gstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" width="40%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title" colspan="2" width="35%">Grand Total:</td>
                <td class="center highlight-attribute" width="20%"  data-bind="text: 'S$ ' + grandTotalText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
        </table>


        <!-- ko if:hasEdit -->
        <div style="text-align: center; color: #B10000; padding-top: 1em; font-weight: bold; font-size: 1.5em;">
            Please save your changes first before proceeding to payment.
        </div>
        <div class="action-container">
            <a class="btn btn-secondary" data-bind="click: doClearChanges">Clear Changes</a>
            <a class="btn btn-primary" data-bind="click: doSaveChanges">Save Changes</a>
        </div>
        <div class="action-container">
            <a class="btn btn-third" style="cursor: default; color: #9B9B9B;">Proceed to Pay</a>
        </div>
        <!-- /ko -->

        <!-- ko ifnot:hasEdit -->
        <div class="terms-conditions">
            <label for="agreeTnc">
                <input type="checkbox" id="agreeTnc" data-bind="checked: agreeTnc"> <span class="star">*</span>
                I have read and agree to both the <a href="/${channel}/tnc" target="_blank" class="link-text">Terms and Conditions</a> as set out in the Sentosa Online Store
                and Sentosa Leisure Management Pte Ltd's ("SLM") <a href="/${channel}/data-protection-policy" target="_blank" class="link-text">Data Protection Policy</a>,
                including as to how my personal data may be collected, used, disclosed and processed, and confirm that
                all information and details of myself are true, correct and complete. Where I have provided personal data of individuals other than myself,
                I warrant and represent that I am validly acting on behalf of each of these individuals, and have obtained their individual consents,
                to disclose their personal data to SLM and for SLM to collect, use, disclose and process their personal data for such purposes as out
                in the Data Protection Policy.
            </label>
        </div>

        <div class="action-container">
            <a class="btn btn-primary" data-bind="click: doCheckout">Pay By Credit Card</a>
            <a class="btn btn-secondary" data-bind="click: doOfflinePayment, visible: offlinePaymentEnabled">Other Payment Mode</a>
        </div>
        <!-- /ko -->
        <!-- /ko -->
    </div>
    <div>
        [@cms.area name="adBanner"/]
    </div>
</div>