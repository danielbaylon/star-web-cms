[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book', sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content > table > tbody > tr {
        font-family: 'Gotham-Book', sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content > table > tbody > .k-alt {
        font-family: 'Gotham-Book', sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar {
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions {
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action {
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action > .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }

    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }

    .k-i-close {
        display: inline-block;
        background-image: url("/resources/${channel}/theme/default/img/close.png");

    }

    .k-grid td {
        word-wrap: break-word;
    }

    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }

    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }

    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }

    .picker {
        z-index: 100000;
    }

    .depositBalance {
        float: right;
        border: 2px solid #13669b;
        padding: 10px;
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: 10px;
    }

    @media print {
        .no-print, .no-print * {
            display: none !important;
        }

        .no-element-print {
            display: none !important;
        }
    }
</style>
<script type="application/javascript">
    $('a.logo').children('img').css('padding-left', '100px');
</script>
<div id="printingReservationContentDiv">
    <center>
        <table border="0" cellpadding="2" cellspacing="0"
               style="border-color: #002569; border-collapse: collapse; width: 591px; height: 0;" bgcolor="#CCD4E1"
               summary="">
            <tr>
                <td bgcolor="#002569" width="40%">
                    <img src="/resources/partner-portal-slm/theme/default/img/logo-sentosaB2B.png?ver=1.0r002"
                         style="border: 0;"/>
                </td>
                <td bgcolor="#002569" width="60%" style="text-align: right;">
						<span lang="en-us"
                              style="font-family: Arial, Helvetica, sans-serif; color: #FFFFFF; font-weight: bold;">Wings 					of Time

							<br/>
							<span style="font-size: x-small;">Travel 						Agent Online Reservation System&nbsp;</span>
						</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="style5"
                    style="margin: 5px; padding: 10px; text-align: left; height: 20px; font-family: Tahoma; font-size: 10pt;"
                    valign="bottom">
						<span lang="en-us">Dear&nbsp;

							<b><span data-bind="text: partnerName"></span></b>,

							<br/>
							<br/> You're set to experience an award-winning visual 					spectacular at Sentosa's Wings of Time.

							<br/>  					This is to confirm your online reservation via <span data-bind="text:baseUrl"></span> is successful.

							<br/>
							<br/>
							<br/>
							<table style="border: 1px solid #CCCCCC; width: 100%; font-family: Arial; font-size: 10pt; border-collapse: collapse;"
                                   border="1" cellpadding="2">
								<tr>
									<td colspan="2" style="background-color: #CCCCCC">
										<b>Reservation 									Details</b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">Redemption Code:</td>
									<td>
										<b><span data-bind="text: pinCode"></span></b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">Show Date:</td>
									<td>
										<b><span data-bind="text: showDateDisplayForPrint"></span></b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">Show Time:</td>
									<td>
										<b><span data-bind="text: showTimeDisplayForPrint"></span></b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">Quantity:</td>
									<td>
										<b><span data-bind="text: qty"></span></b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">Status:</td>
									<td>
										<b><span data-bind="text: status"></span></b>
									</td>
								</tr>
								<tr>
									<td style="width: 136px;">
										<span lang="en-us">Remarks:</span>
									</td>
									<td>
										<span lang="en-us" style="font-weight: bold"><span
                                                data-bind="text: remarks"></span></span>
									</td>
								</tr>
							</table>
						</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="font-family: tahoma; font-size: 10pt;">
                    <div>
                        <img data-bind="attr : { 'src' : pinCodeImage }" width="300" height="75"/>
                    </div>
                    <div>
                        <b><span data-bind="text: pinCode"></span></b>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="style5"
                    style="margin: 5px; padding: 10px; text-align: left; height: 20px; font-family: Tahoma; font-size: 10pt;"
                    valign="bottom">
						<span lang="en-us">Please print out this 					email to redeem your Wings of Time Ticket(s) on the day of the show 					at the following locations:

							<ul style="margin-left: 20px; list-style-type: disc; list-style-position: inside;">
								<li>
									<b>Sentosa Station @ VivoCity (Lobby L, Level 3)</b>
								</li>
								<li>
									<b>Imbiah Lookout Ticketing Counter or Kiosk</b>
								</li>
								<li>
									<b>Beach Station Ticketing Counter or Kiosk</b>
								</li>
								<li>
									<b>Waterfront Station Ticketing Counter</b>
								</li>
								<li>
									<b>Merlion Plaza Ticketing Counter</b>
								</li>
							</ul>
						</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px; margin: 5px; font-family: Arial; font-size: 10pt"> Thank you for
                    using the Wings of Time Travel Agent Online Reservation System!
                    <br/>
                    <br/>
                    <b>Sentosa Development Corporation </b>
                    <br/> br /> For Enquiries,
                    <br/> Call 1800 - SENTOSA (736 8672)
                    <br/> 9am - 6.00pm daily
                    <br/> or email
                    <a href="mailto:travelagent@sentosa.com.sg">travelagent@sentosa.com.sg</a>
                    <br/>
                    <br/>
                    <a data-bind="attr : {'href' : baseUrl }"><span data-bind="text: baseUrl"></span></a>
                </td>
            </tr>
            <tr style="display: inline;">
                <td colspan="2" align="center" style="border-top-style: solid; border-width: 1px; border-color: #002569"
                    width="100%">
                    <span lang="en-us" style="font-family: tahoma; font-size: 10pt; color: #FF0000; font-weight: bold"> 					Terms and Conditions </span>
                </td>
            </tr>
            <tr style="display: inline;">
                <td colspan="2" align="left" style="padding: 5px; font-family: Arial; font-size: 8pt; width: 100%">
                    <ul style="list-style-type: disc; list-style-position: outside; margin-left: 20px; margin-right: 5px; width: 97%;">
                        <li>This voucher is valid on the above mentioned date &amp; time and can be used for one-time
                            redemption only.
                        </li>
                        <li>Please be advised to make your redemption at least an hour before the show time.</li>
                        <li>This is an outdoor, rain or shine show; it is not refundable &amp; exchangeable for cash.
                        </li>
                        <li>Please contact your travel agent if the redemption code provided is deemed to be invalid or
                            if you require any changes in show date or time. Any changes will be subjected to
                            availability.
                        </li>
                        <li>There will be no refund for unused printed ticket either in part or full. All printed
                            tickets are non-transferable.
                        </li>
                        <li>If more tickets are required at the point of redemption, we recommend that you contact your
                            travel agent or simply purchase them at published rates over at our ticketing counters.
                        </li>
                        <li>Cancellation of booked tickets will incur an administrative charge of $2 and $1 for 1st and
                            2nd show respectively. Please note that no cancellation/amendment can be made one hour
                            before the show time.
                        </li>
                        <li>Tickets are not to be used for touting purpose.</li>
                        <li>10% of unredeemed seats (i.e. price of 1 ticket can be refunded for every 10 tickets
                            purchased) will be returned back into your online credit pool on the day of the show. This
                            is only applicable if redemption is made at least an hour before the show time (i.e. before
                            6.40pm for 7.40pm show).
                        </li>
                        <li>Sentosa reserves the right to make changes without further notice to information herein.
                        </li>
                        <li>Island Admission &amp; Transport charges payable separately.</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-family: tahoma; font-size: 3px; background-color: #002569; width: 591;">
                    &nbsp;</td>
            </tr>
        </table>
    </center>
    <div class="clearfix no-print"></div>
    <div style="width: 100%;"><br/></div>
    <div class="clearfix  no-print"></div>
    <div class="action-container  no-print" style="padding-top: 10px; padding-bottom: 10px">
        <input type="button" class="btn btn-primary" value="Print" data-bind="click: printingTicket"/>
        <input type="button" class="btn btn-third" value="Close" data-bind="click : closeModel"/>
    </div>
</div>

<div id="adminNav" style="display: none;"></div>