<!-- Generate package--->

[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
[#include "/partner-portal-slm/templates/function/objectToJsonFunction.ftl"]

<!--TODO Alert a message here-->

<div id="main-content-section" class="main-content-section-container">
    <h2 class="heading">Mix &amp; Match Package</h2>
    <!-- TODO check the success -->
    <div id="successdv" class="breadcrumb">
        <h2 class="heading" style="text-align: center">Package Creation is Successful!</h2>
    </div>
    <div class="main-content-section">
        <div class="information-section-container">
            <h4 class="primary-heading-bar">Package Detail</h4>

            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                <tr>
                    <td width="20%">Status</td>
                    <td width="80%"><p data-bind="text: status"></p></td>
                </tr>
                <tr data-bind="visible: ticketMedia() == 'Pincode'" style="display:none">
                    <td width="20%">PIN CODE</td>
                    <td width="80%"><p data-bind="text: pinCode"></p></td>
                </tr>
                <tr data-bind="visible: ticketMedia() == 'ETicket'" style="display:none">
                    <td>
                        <a href="" id="downloadTicket" type="button" class="btn btn-secondary">Download E-Ticket</a>
                    </td>
                </tr>
                <tr data-bind="visible: ticketMedia() == 'ExcelFile'" style="display:none">
                    <td>
                        <a href="" id="downloadExcelFile" type="button" class="btn btn-secondary">Download Excel File</a>
                    </td>
                </tr>
            </table>
            <div class="receipt-section">
            </div>
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                <tr>
                    <td width="20%">New Package Name:</td>
                    <td width="80%" data-bind="text: name"></td>
                </tr>
                <tr>
                    <td width="20%">Booking Reference No:</td>
                    <td width="80%" data-bind="text: id"></td>
                </tr>
                <tr>
                    <td width="20%">Description:</td>
                    <td width="80%" data-bind="text: description"></td>
                </tr>
                <tr>
                    <td width="20%">Expiry Date:</td>
                    <td width="80%" data-bind="text: expiryDateStr"></td>
                </tr>
                <tr>
                    <td width="20%">Quantity Per Product:</td>
                    <td width="80%" data-bind="text: qty"></td>
                </tr>
            </table>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="items-tickets-content-container package-items" border="0">
            <tr class="forth-multi-heading-bar">
                <td width="50%">Product</td>
                <td width="20%">Quantity</td>
                <td width="30%" class="center">Receipt Number</td>
            </tr>
            <!-- ko foreach: transItems -->
            <tr class="main-items" data-bind="css: {odd: $index()%2==1, even: $index()%2==0}">
                <td width="50%">
                    <span data-bind="text: displayName" class= "product-item-name"></span>
                    <span data-bind="html: description"></span>
                </td>
                <td width="20%" data-bind="text: pkgQty"></td>
                <td width="30%" data-bind="text: receiptNum" class="center"></td>
            </tr>
                <!-- ko foreach: topupItems -->
                <tr>
                    <td><i class="fa fa-plus-circle"></i><span data-bind="text: displayName"></span></td>
                    <td style="padding: 0.8em" data-bind="text: pkgQty"></td>
                    <td >&nbsp;</td>
                </tr>
                <!-- /ko -->
            <!-- /ko -->
        </table>

        <div class="error-section"><span class="error-note"><i class="fa fa-exclamation-circle"></i> Important Note:</span> <BR>-Generated Ticket / Pincode will not be eligible for refund.
            <BR>-Please note that the expiry date of your created package is decided by the earliest expiry date of your selected ticket(s).</div>

        <div class="action-container">
            <a href="/${channel}/view-packages" id="viewPkgs" type="button" class="btn btn-secondary">View Packages</a>
            <a href="/${channel}/view-inventory" id="viewInventory" type="button" class="btn btn-primary" >Create Another Package</a>
        </div>
    </div>
</div>
<div class=" clearfix"></div>