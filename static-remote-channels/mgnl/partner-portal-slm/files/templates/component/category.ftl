[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
[#include "/partner-portal-slm/templates/function/objectToJsonFunction.ftl"]


[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = false]
[#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
    [#assign isOnPreview = true]
[/#if]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign cat = "Package"]
[#if params[0]?has_content]
    [#assign catName = params[0]]
[/#if]
[#assign nodeId = ""]
[#if params[1]?has_content]
    [#assign nodeId = params[1]]
[/#if]
[#if nodeId?has_content]
    [#assign catNode = starfn.getCMSProductCategory(channel,nodeId)]
    [#if catNode.hasProperty("name")]
        [#assign catName = catNode.getProperty("name").getString()]
        [#if isOnPreview]
            [#assign subProdlist = starfn.getCMSProductByCategoryForPartnerPreview(channel, nodeId)]
        [#else]
            [#assign randomNumber = starfn.genRandomNumber()]
            [#assign subProdlist = starfn.getCMSProductByCategoryForPartner(channel, nodeId, randomNumber)]
        [/#if]
    [/#if]

    [#--[#if catNode.hasProperty("name")]--]
        [#--[#assign subCatNodes = starfn.getCMSProductSubCategories(catNode)]--]
    [#--[/#if]--]
    [#--[#list subCatNodes as subCatNode]--]
        [#--[#if subCatNode?has_content]--]
            [#--[#assign subCatNode = subCatNode]--]
        [#--[/#if]--]
    [#--[/#list]--]

[/#if]
<script>
    var categoryName = "${nodeId}";
    var prodsJson = [];
    var tmpProd = {};
    [#if subProdlist?has_content]
        [#list subProdlist as prod]
            [#if prod?has_content]
                [#assign prod = prod]
            tmpProd = {};
                [#assign prodId = prod.getName()]
            tmpProd.prodId = '${prodId}';
                [#if prod.hasProperty("name")]
                    [#assign prodName = prod.getProperty("name").getString()]
                        tmpProd.name = '${prodName?js_string!""}';
                [/#if]
                [#--[#if prod.hasProperty("shortDescription")]--]
                    [#--[#assign shortDescription = prod.getProperty("shortDescription").getString()]--]
                [#--tmpProd.shortDescription = '${shortDescription}';--]
                [#--[/#if]--]
                [#if prod.hasProperty("description")]
                    [#assign description = prod.getProperty("description").getString()]
                    tmpProd.description = [@compress single_line=true]"${description}"[/@compress];
                [/#if]
                [#if prod.hasProperty("productLevel")]
                    [#assign productLevel = prod.getProperty("productLevel").getString()]
                    [#if productLevel == 'Exclusive']
                        tmpProd.isExclusiveDeal = true;
                    [#else]
                        tmpProd.isExclusiveDeal = false;
                    [/#if]
                [/#if]
                [#assign axPrdList = starfn.getAXProductsByCMSProductNode(prod)]
                [#assign axPrdDetailsMap = starfn.getAXProductsDetailByCMSProductNode(channel, prod)]
            tmpProd.axProducts = []
                [#list axPrdList as axProd]
                var tmpAxProduct = {};
                    [#assign axPrdDetail = axPrdDetailsMap[axProd.getProperty("relatedAXProductUUID").getString()]]
                    [#if axPrdDetail?has_content]
                        [#if axPrdDetail.hasProperty("itemName")]
                            [#assign itemName = axPrdDetail.getProperty("itemName").getString()]
                        tmpAxProduct.itemName = '${itemName}';
                        [/#if]
                        [#if axPrdDetail.hasProperty("ticketType")]
                            [#assign ticketType = axPrdDetail.getProperty("ticketType").getString()]
                        tmpAxProduct.ticketType = '${ticketType}';
                        [/#if]

                        [#if axPrdDetail.hasProperty("productPrice")]
                            [#assign productPrice = axPrdDetail.getProperty("productPrice").getString()]
                        tmpAxProduct.productPrice = '${productPrice}';
                        [/#if]

                        [#if axPrdDetail.hasProperty("publishedPrice")]
                            [#assign publishPrice = axPrdDetail.getProperty("publishedPrice").getString()]
                        tmpAxProduct.publishPrice = '${publishPrice}';
                        [#else]
                        tmpAxProduct.publishPrice =  '';
                        [/#if]

                        [#if axPrdDetail.hasProperty("displayProductNumber")]
                            [#assign productCode = axPrdDetail.getProperty("displayProductNumber").getString()]
                        tmpAxProduct.productCode = '${productCode}';
                        [/#if]
                        [#if axPrdDetail.hasProperty("productListingId")]
                            [#assign listingId = axPrdDetail.getProperty("productListingId").getString()]
                        tmpAxProduct.listingId = '${listingId}';
                        [/#if]

                    tmpAxProduct.crossSells = [];
                        [#assign crossSells = starfn.getAXProductCrossSells(axPrdDetail)]
                        [#if crossSells?has_content]
                            [#list crossSells as cs]
                            var crossSell = {};
                                [#if cs.hasProperty("crossSellItemName")]
                                    [#assign csItemName = cs.getProperty("crossSellItemName").getString()]
                                crossSell.itemName = '${csItemName}';
                                [/#if]
                                [#if cs.hasProperty("crossSellProductCode")]
                                    [#assign csProductCode = cs.getProperty("crossSellProductCode").getString()]
                                crossSell.productCode = '${csProductCode}';
                                [/#if]
                                [#if cs.hasProperty("crossSellProductListingId")]
                                    [#assign csListingId = cs.getProperty("crossSellProductListingId").getString()]
                                crossSell.listingId = '${csListingId}';
                                [/#if]
                                [#if cs.hasProperty("crossSellItemImage")]
                                    [#assign csItemImageUUID = cs.getProperty("crossSellItemImage").getString()]
                                    [#assign csItemImageThumbnailLink = damfn.getAssetLink(csItemImageUUID)]
                                crossSell.imageLink = '${csItemImageThumbnailLink}';
                                [/#if]
                                [#assign crossSellRelatedAxProduct = starfn.getAxProductByProductCode(channel, csProductCode)]
                                [#if crossSellRelatedAxProduct?has_content]
                                    [#if crossSellRelatedAxProduct.hasProperty("ticketType")]
                                        [#assign csItemTicketType = crossSellRelatedAxProduct.getProperty("ticketType").getString()]
                                    crossSell.ticketType = '${csItemTicketType}';
                                    [/#if]
                                    [#if crossSellRelatedAxProduct.hasProperty("productPrice")]
                                        [#assign csItemProductPrice = crossSellRelatedAxProduct.getProperty("productPrice").getString()]
                                    crossSell.ticketPrice = '${csItemProductPrice}';
                                    [/#if]
                                    [#if crossSellRelatedAxProduct.hasProperty("publishPrice")]
                                        [#assign csItemProductPublishPrice = crossSellRelatedAxProduct.getProperty("publishPrice").getString()]
                                    crossSell.ticketPublishPrice = '${csItemProductPublishPrice}';
                                    [#else]
                                    crossSell.ticketPublishPrice = crossSell.ticketPrice;
                                    [/#if]
                                [/#if]
                            crossSell.parentAxProductListingId = tmpAxProduct.listingId;
                            crossSell.parentAxProductProductCode = tmpAxProduct.productCode;
                            tmpAxProduct.crossSells.push(crossSell);
                            [/#list]
                        [/#if]
                    [/#if]
                tmpProd.axProducts.push(tmpAxProduct);
                [/#list]
                [#assign cmsProductImages = starfn.getCMSProductImagesUUID(prod, "")!]
            tmpProd.images = []
                [#list cmsProductImages as cmsProductImage]
                    [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)]
                var tmpImg = {};
                tmpImg.url = "${ctx.contextPath}${thumbnailAssetLink}";
                tmpProd.images.push(tmpImg);
                [/#list]

            prodsJson.push(tmpProd);
            [/#if]
        [/#list]
    [/#if]

    [#--[#if !isOnPreview && !cmsfn.editMode]--]

        [#--$(document).ready(function() {--]
            [#--nvx.retrieveProducts();--]
        [#--});--]

    [#--[#else]--]

        [#--$(document).ready(function() {--]
            [#--var categJson = {products:prodsJson};--]
            [#--nvx.mdl = new nvx.MainModel(categJson);--]
            [#--ko.applyBindings(nvx.mdl,$('#main-div')[0]);--]
            [#--//$page.mdl.initProdExtData();--]
        [#--});--]
    [#--[/#if]--]

</script>

<style>
    img.banner-exclusive {
        position: absolute;
        z-index: 99;
        top: 1em;
        left: 1em;
    }
    .summary *{
        display: inline-block;
    }
    .summary p{
        display:block;
    }
</style>
<div id='main-div' class="main-content-section-container">
    <h2 class="heading" style='text-transform: capitalize;'>${catName!""}</h2>
    [@cms.area name="categoryCarousel"/]
    <div class="clearfix"></div>

    <div class="main-content-section">
        <h3 class="heading">Buy Tickets Now</h3>
        <div data-bind="foreach: products" class="product-list-content-container items-spacing">
            <div class="product-cell">
                <a class="product-content product-size-medium" data-bind="click: $parent.changeProduct" >
                    <img data-bind="attr: { src: imagePath}" class="main-image"/>
                    <span class="product-title"  data-bind="text: name">Test Prod</span>
                    <!-- ko if: isExclusiveDeal -->
                    <img class="icon-exclusive"  src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-exclusive.png"/>
                    <!-- /ko -->
                </a>
            </div>
        </div>
        <div id="formsContainer" data-bind="visible: showProduct">
            <div class="expand-product-details-container">
                <div class="close-section">
                    <div class="icon-close"  data-bind="click: closeProduct"><i class="fa fa-times close"></i></div>
                </div>
                <div class="expand-product-details">
                    <div class="sidebar-slideshow " data-bind="attr: { id : 'sidebar-slideshow-' + currentProd().prodId() }">
                        <div class="slideshow-slides" data-bind="attr: { id : 'slideshow-slides-' + currentProd().prodId() }">
                            <ul class="sequence-canvas"  data-bind="attr: { id : 'sequence-canvas-' + currentProd().prodId() }, foreach: currentProd().images" >
                                <li><div class="promotion-1">
                                    <img data-bind="attr: { src:  url()}"  width="100%" height="100%" />
                                </div></li>
                            </ul>
                            <a class="icon-left-arrow"></a>
                            <a class="icon-right-arrow"></a>
                        </div>
                    </div>
                    <div class="sidebar-product-details">
                        <div class="add-successful-pop-up" style="display: none;">
                            <img src="${ctx.contextPath}/resources/${channel}/theme/default/img/icon-successful.png" width="100%"/>
                            <span>Item(s) added to Cart</span>
                            <div class="clearfix"></div>
                        </div>
                        <h2 class="heading" data-bind="text: currentProd().name" >Display Title</h2>
                        <div class="summaryDiv" style='position: relative;overflow:hidden;height:100px;display:inline-block;'>
                            <div class="summary" style="width:620px"><span data-bind="html: currentProd().description" ></span></div>
                            <div style="position: absolute;right: 0px;bottom: 0px;background-color: white;">
                                <a class="link-text moretext" >[More]</a>
                                <a class="link-text lesstext" style="display:none;">[Less]</a>
                            </div>
                        </div>
                        <div class="error-section" style="display:none;" data-bind="text: errorMessage, visible: errorMessage() != ''">Error</div>

                        <!-- GENERAL -->
                        <div class="main-tickets-section">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr class="primary-multi-heading-bar">
                                    <td width="5%" class="icon-expand-collapse"></td>
                                    <td width="30%" colspan="2">Main Ticket(s)</td>
                                    <td width="24%">Published Rate</td>
                                    <td width="24%">Price</td>
                                    <td width="12%">Qty.</td>
                                    <td width="5%">&nbsp;</td>
                                </tr>
                            </table>

                            <table data-bind="visible: showProductTable" cellpadding="0" cellspacing="0" width="100%" border="0" class="items-tickets-content-container" >
                                <tbody data-bind="foreach: currentProd().axProducts">
                                <tr class="main-items" data-bind="css: {odd: $index()%2==1, even: $index()%2==0}">
                                    <td colspan="7" class="column-multi-attribute">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr class="main-inner-items">
                                                <td width="35%" colspan="3"  data-bind="text: itemName()"</td>
                                                <td width="24%"  data-bind="text: ticketType() +' - ' + publishPriceFormat()"></td>
                                                <td width="24%"  data-bind="text: ticketType() +' - ' + productPriceFormat()"></td>
                                                <td width="12%">
                                                    <span data-bind="attr: { id: 'soldOut' + listingId()}" style="display:none; color: red">Sold Out</span>
                                                    <input type="text" class="input-textarea" data-bind="value: qty, attr { 'std-id': listingId(), 'std-code': productCode(), id: 'qty' + listingId()}" >
                                                    <a style="display:none" class="sebs-btn-select" href="javascript:void(0)" data-bind="attr: { id: 'ebSelect' + listingId() }">Select</a>
                                                    <a style="display:none" class="sebs-btn-cancel" href="javascript:void(0)" data-bind="attr: { id: 'ebCancel' + listingId() }">Cancel</a>
                                                </td>
                                                <td width="5%">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="main-items even" data-bind="attr: { id: 'eventBar' + listingId()}, css: {odd: $index()%2==1, even: $index()%2==0}" style="display: none;">
                                    <td colspan="4" style="padding: 0">
                                        <div style="padding: 5px 7px;">
                                            <div class="star-event-booking-section">
                                                <div class="seb-subsection seb-subsection-date">
                                                    <div class="sebs-label">Choose Date of Visit</div>
                                                    <div>
                                                        <input data-bind="attr: { id: 'ebDate' + listingId()}" type="text" class="sebs-input" style="width: 70%;">
                                                        <label data-bind="attr: {for: 'ebDate' + listingId()}">
                                                            <i class="fw-icon-calendar"
                                                               style="vertical-align:middle; font-size:1.2em; color: #1ca9ea; padding: 0 0.3em;"
                                                               data-profile-dov-cal-icon="1"></i>
                                                        </label>
                                                    </div>
                                                </div><div class="seb-subsection seb-subsection-session">
                                                <div class="sebs-label">Choose Session</div>
                                                <div>
                                                    <select style="width: 95%" data-bind="attr: {id: 'ebSession' + listingId()}">
                                                    </select>
                                                </div>
                                            </div><div class="seb-subsection seb-subsection-qty">
                                                <input type="text" class="input-textarea" data-bind="value: qty, attr: {id: 'ebQty' + listingId()}">
                                            </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <a class="btn btn-primary layout" data-bind="click: addToCart,visible: !showViewCart()" >Add to Cart</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="top-up-tickets-section" data-topup-section="true" style="display: none;" data-bind="visible: showTopupTable">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="">
                                <tr class="secondary-multi-heading-bar">
                                    <td width="5%" class="icon-expand-collapse"></td>
                                    <td width="32%">Top-up Ticket(s)</td>
                                    <td width="20%">Published Rate</td>
                                    <td width="20%">Price</td>
                                    <td width="10%">Qty.</td>
                                    <td width="5%">&nbsp;</td>
                                </tr>
                            </table>
                            <div class="top-up-tickets-items-container" data-topups-body="true">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="items-tickets-content-container">
                                    <tbody data-bind="foreach: topups">
                                    <tr class="top-up-items" data-bind="attr: {'topup-listing-id': parentAxProductListingId, 'topup-product-code': parentAxProductProductCode, 'data-topup-id': $index()}">
                                        <td width="5%"><i class="fa fa-plus-circle"></i></td>
                                        <td class="top-up-items-name" width="15%"><img width="100%" alt="name" data-bind="attr: {src: imageLink}"/></td>
                                        <td class="top-up-items-name" width="17%" data-bind="text: itemName"></td>
                                        <td width="24%"  data-bind="text: ticketType() +' - ' + ticketPublishPriceFormat()"></td>
                                        <td width="24%"  data-bind="text: ticketType() +' - ' + ticketPriceFormat()"></td>
                                        <td width="10%" data-bind="attr: {'data-topup-qty': $index()}">
                                            <span data-bind="visible: !addedInCart() && !addToTopUp()">0</span>
                                            <span data-bind="text: parentItemQty, visible: addToTopUp"></span>
                                            <span data-bind="text: qty, visible: addedInCart"></span>
                                        </td>
                                        <td width="5%">
                                            <input type="checkbox" data-bind="visible: !addedInCart(), checked: addToTopUp, attr: { 'data-topup-chk' : $index()}">
                                            <span><i style="color: green; font-size: 1.3em;display:none" class="fa fa-check-square-o" data-bind="visible: addedInCart"></i></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <a class="btn btn-primary layout" data-bind="visible: showAddTopups, click: addTopupToCart">Add Topups</a>
                        </div>
                        <!-- END GENERAL -->

                        <div class="main-tickets-section" data-end-of-flow-actions="true" data-bind="visible: showViewCart">
                            <a class="btn btn-secondary layout" data-btn-view-cart="true" href="${ctx.contextPath}/${channel}/cart">View Cart</a>
                            <a class="btn btn-secondary layout" data-form-reset="true" data-bind="click: addMore">Add More Items</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
