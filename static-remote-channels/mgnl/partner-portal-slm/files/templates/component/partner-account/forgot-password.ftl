[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]

<div class="main-content-section-container">
    <div class="forgot-pwd-panel">
        <div class="background heading-panel">
            <h2 class="heading center">Forgot Password</h2>
        </div>
        <div class="panel background user-info-panel">
            <h5 class="note">Please enter following fields to reset your password</h5>
            <div class="error-section" style="display: none;" id="alert"></div>
            <form method="post" action="${API_PREFIX}/publicity/partner-account/reset-password" id="forgot-password-reset-form">
            <table cellpadding="0" cellspacing="0" width="100%" class="user-main-info">
                <tr>
                    <td width="30%">
                        <label class="login-bold-text">Username:</label>
                    </td>
                    <td width="70%">
                        <input type="text" id="un" name="un" class="input-textarea"  placeholder="Username" >
                    </td>
                </tr>
                <tr>
                    <td width="30%">
                        <label class="login-bold-text">Registered Email:</label>
                    </td>
                    <td width="70%">
                        <input type="text" id="email" name="email" class="input-textarea" placeholder="Email">
                    </td>
                </tr>
                <tr>
                    <td width="30%" class="note-section">&nbsp;</td>
                    <td width="70%" class="note-section"><p>* This must be an email registered with Sentosa</p></td>
                </tr>

                [#assign recaptchaEnabled = starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.enabled")]
                [#if recaptchaEnabled == "Y"]
                    <tr>
                        <td width="30%">
                            <label class="login-bold-text">Verification:</label>
                        </td>
                        <td width="70%">
                            <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=${starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.public.key")!}"></script>
                            <noscript>
                                <iframe src="https://www.google.com/recaptcha/api/noscript?k=${starfn.getSystemParamValueByKey(channel, "sys.param.recapcha.public.key")!}" height="300" width="500" frameborder="0"></iframe><br>
                                <textarea name="recaptcha_challenge_field" id="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                                <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
                            </noscript>
                        </td>
                    </tr>
                [/#if]
                <tr>
                    <td width="30%">&nbsp;</td>
                    <td width="70%">
                        <button type="button" class="btn btn-primary btn-submit" id="btnReset">Reset Password</button>
                        <a href="${sitePath}" class="btn btn-third btn-cancel">Back</a>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>