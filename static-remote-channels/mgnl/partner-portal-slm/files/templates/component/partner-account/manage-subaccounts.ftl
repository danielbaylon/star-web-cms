[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">

    <style>
        .k-grid th.k-header,
        .k-grid-header {
            background: #4d4d4d;
            padding: 0.8em;
            color: #fff;
            border: 1px solid #4d4d4d;
            font-size: 11.9px;
            font-family: 'Gotham-Book',sans-serif;
            font-weight: 700;
            text-align: center;
        }

        .k-grid-content>table>tbody>tr{
            font-family: 'Gotham-Book',sans-serif;
            font-size: 11.9px;
            font-weight: 400;
            text-align: center;
        }

        .k-grid-content>table>tbody>.k-alt{
            font-family: 'Gotham-Book',sans-serif;
            font-size: 11.9px;
            font-weight: 400;
            text-align: center;
            background: #F5F5F5;
        }

        .k-window-titlebar{
            background: #26b67c;
            padding: 0.8em;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            color: #ffffff;
            margin: 0;
            width: 100%;
            position: relative;
            font-size: 1.2em;
            height: 50px;
            border-left-width: 0px;
            border-right-width: 0px;

        }

        .k-window-titlebar .k-window-actions{
            right: -0.1em;
            top: -7px;
        }

        .k-window-titlebar .k-window-action{
            width: 40px;
            height: 46px;
            background-color: transparent;
            opacity: 1;
        }

        .k-icon,.k-sprite,.k{
            width: 40px;
            height: 50px;
            background-image: none;
        }

        .k-icon,.k-sprite{
            background: #0c9d62;
            background-repeat: no-repeat;
            background-position:8px 15px;
        }

        .k-i-close{
            display: inline-block;
            background-image: url("${themePath!""}/img/close.png");

        }

        .k-link:not(.k-state-disabled):hover>.k-i-close,.k-link:not(.k-state-disabled):hover>.k-delete,.k-link:not(.k-state-disabled):hover>.k-group-delete,.k-state-hover .k-i-close,.k-state-hover .k-delete,.k-state-hover .k-group-delete,.k-button:hover .k-i-close,.k-button:hover .k-delete,.k-button:hover .k-group-delete,.k-textbox:hover .k-i-close,.k-textbox:hover .k-delete,.k-textbox:hover .k-group-delete,.k-button:active .k-i-close,.k-button:active .k-delete,.k-button:active .k-group-delete{
            background-position:8px 15px;
        }


        .k-grid-content {
            overflow-y: hidden;
        }



    </style>


    <h2 class="heading">Account Management</h2>

    <div class="main-content-section">

        <div class="items-records-container">
            <h4 class="primary-heading-bar">Manage Sub User</h4>
            <div class="add-sub-section">
                <a href="javascript:void(0)" class="btn btn-primary" data-forbind="new-acc">Add Sub User</a>
            </div>
            <div  id="accountsGrid"></div>
            <div class="add-sub-section">
                <a href="javascript:void(0)" class="btn btn-primary" data-forbind="new-acc" >Add Sub User</a>
            </div>
        </div>


        <div id="accountsModal" style="display: none;">
            <form class="form-horizontal">
                <div class="error-section" style="display: none;" data-bind="visible: isErr, html: errMsg"></div>
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                    <tr>
                        <td width="30%">Partner Unique Code:</td>
                        <td width="70%"><span data-bind="text: accCode"></span></td>
                    </tr>
                    <tr>
                        <td width="30%">Name<span class="star" data-bind="visible: isNew()">*</span>:</td>
                        <td width="70%">
                            <input type="text" placeholder="Name" maxlength="10" data-bind="visible: isNew(), value: name, valueUpdate: 'input'">
                            <span class="static" data-bind="visible: !isNew(), text: name"></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%">User Name:</td>
                        <td width="70%">
                            <span class="static" data-bind="visible: isNew()"><span data-bind="text: accCode"></span>_</span><span data-bind="visible: isNew(), text: name"></span>
                            <span class="static" data-bind="visible: !isNew(), text: username"></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%">Designation<span class="star">*</span>:</td>
                        <td width="70%">
                            <input type="text" placeholder="Designation" data-bind="value: title">
                        </td>
                    </tr>
                    <tr>
                        <td width="30%">Email Address<span class="star">*</span>:</td>
                        <td width="70%">
                            <input type="text" placeholder="Email" data-bind="value: email">
                        </td>
                    </tr>

                    <tr data-bind="visible: isNotSelf">
                        <td width="30%" >Status:</td>
                        <td width="70%">
                            <select data-bind="value: status, visible: !isNew()">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                                <option value="Locked">Locked</option>
                            </select>
                            <span class="static" data-bind="visible: isNew()">Active</span>
                            <input type="hidden" data-bind="visible: isNew(), value: status" value="Active"/>
                        </td>
                    </tr>
                    <tr data-bind="visible: !isNew()">
                        <td width="30%"><span class="static" >Reset Password:</span></td>
                        <td width="70%">
                            <a class="btn btn-secondary" data-bind="click: doResetPassword">Reset</a>
                        </td>
                    </tr>
                </table>

                <div data-bind="visible: isNotSelf">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                        <tr>
                            <td width="28%">
                                Access Rights:
                            </td>
                            <td width="71%">
                                <div style="width: 100%;">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                                        <tbody data-bind="foreach: accountAccessGroups">
                                            <tr>
                                                <td width="85%"><span data-bind="text: description"></span></td>
                                                <td width="15%"><input type="checkbox" data-bind="checked: checked"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
                    <tr class="inner-action-container">
                        <td align="center">
                            <a href="#" class="btn btn-primary" data-bind="click: doSave">Save</a>
                            <a href="#" class="btn btn-third" data-bind="click: doCancel">Cancel</a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>