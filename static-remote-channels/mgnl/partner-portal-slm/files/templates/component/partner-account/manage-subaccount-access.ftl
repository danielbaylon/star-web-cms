[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">
    <h2 class="heading">Account Management</h2>
    <div class="main-content-section">
        <div class="items-records-container">
            <h4 class="primary-heading-bar">Access Rights Management</h4>
                <div style="display:none;width: 100%;"  id="accountMgtDiv">
                    <table cellpadding="0" cellspacing="0" width="100%" class="items-records-content-container">
                        <thead>
                        <tr data-bind="foreach: subAccountRights" class="forth-multi-heading-bar">
                            <!-- ko if: $index() === 0 -->
                            <th>User ID</th>
                            <!-- /ko -->
                            <th><span data-bind="text: description"></span></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: subAccounts">
                        <tr data-bind="foreach: rights">
                            <!-- ko if: $index() === 0 -->
                            <td><span data-bind="text: $parent.name"></span></td>
                            <!-- /ko -->
                            <td>
                                <input type="checkbox" data-bind="checked: checked"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="action-container">
                        <input class="btn btn-primary" type="button" data-bind="click: doSave" value="Save">
                        <input class="btn btn-third" type="button" data-bind="click: doReset" value="Cancel">
                    </div>
                </div>
        </div>
    </div>
</div>