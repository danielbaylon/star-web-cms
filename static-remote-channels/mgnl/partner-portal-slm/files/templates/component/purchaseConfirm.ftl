[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<div class="main-content-section-container">
    <div class="heading-bar">
        <h2 class="heading">Confirmation</h2>
        <p class="countdown">Please complete booking in: <span class="time"><span id="confirmTimer">00:00</span> minutes.</span></p>
        <div class="clearfix"></div>
    </div>

    <div class="main-content-section" id="checkoutView">

        <!-- ko if:products().length != 0 -->
        <!-- ko foreach:products  -->
        <div class="items-details">
            <h3 class="heading" data-bind="text: title"></h3>
            <table cellpadding="0" cellspacing="0" width="100%" class="booking-tickets-content-container" border="0">
                <tr class="primary-multi-heading-bar">
                    <td width="40%">Product</td>
                    <td width="30%" class="center">Price</td>
                    <td width="10%" class="center">Quantity</td>
                    <td width="15%"class="center">Amount</td>
                    <td width="5%" class="center"> </td>
                </tr>
                <tbody data-bind="foreach: items">
                <tr data-bind="css:{'main-items':!isTopup,'top-up-items':isTopup}">
                    <td width="40%">
                        <!-- ko if:isTopup --><i class="fa fa-plus-circle"></i><!-- /ko -->
                        <span data-bind="text: title" class="product-item-name"></span>
                        <span data-bind="html: description"></span>
                    </td>
                    <td width="30%" class="center" data-bind="text: ticketType + ' - S$ ' + priceText"></td>
                    <td width="10%" class="center">
                        <!-- ko if:isTopup -->
                        <span data-bind="text: qty"></span>
                        <!-- /ko --><!-- ko ifnot:isTopup -->
                        <span data-bind="text: qty"></span>
                        <!-- /ko -->
                        <!--  <input type="text" class="input-textarea center" data-bind="value: qty, valueUpdate: 'keyup'">-->
                    </td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + totalText()"></td>
                    <td width="5%"></td>
                </tr>
                </tbody>

                <tr class="subtotal-items main-inner-items">
                    <td width="70%" colspan="2">
                        <a class="link-text" style="cursor: pointer;" data-bind="visible: additionalTopups().length > 0, click: doToggleTopupsToAdd">
                            <i class="fa fa-plus-circle"></i> <span data-bind="text: topupsToAddText"></span>
                        </a>
                    </td>
                    <td class="subtotal-items-title" width="10%">Subtotal:</td>
                    <td width="15%" class="center" data-bind="text: 'S$ ' + subtotalText()"></td>
                    <td width="5%" class="center">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!-- /ko -->

        <!-- TOTALS -->
        <table cellpadding="0" cellspacing="0" width="100%" class="total-account-section-container">
            <tr class="subtotal-items main-inner-items">
                <td width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'Total Amount (excl. ' + gstRateStr() +'):'"></td>
                <td class="center" width="30%" data-bind="text: 'S$ ' + totalNoGstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="total-items subtotal-items">
                <td class="no-border"width="40%">&nbsp;</td>
                <td class="subtotal-items-title" colspan="2" width="35%" data-bind="text: 'GST ' + gstRateStr()"></td>
                <td class="center" width="20%" data-bind="text: 'S$ ' + gstText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
            <tr class="main-items subtotal-items">
                <td class="no-border" width="40%">&nbsp;</td>
                <td class="subtotal-items-title highlight-title" colspan="2" width="35%">Grand Total:</td>
                <td class="center highlight-attribute" width="20%"  data-bind="text: 'S$ ' + grandTotalText()"></td>
                <td width="5%" class="center">&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <div class="action-container">
            <a class="btn btn-third" href="${ctx.contextPath}/${channel}/cart" data-bind="click: doCheckout" id="btnCancel">Back to Cart</a>
            <a class="btn btn-primary" data-bind="click: doPayment" id="btnPay">Pay Now</a>
        </div>
    </div>
    <div>
        [@cms.area name="adBanner"/]
    </div>
</div>