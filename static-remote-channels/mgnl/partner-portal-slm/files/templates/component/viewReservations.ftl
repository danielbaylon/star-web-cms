[#include "/partner-portal-slm/templates/macros/pageInit.ftl"]
<style>

    .k-grid-header .k-header .k-link {
        display: block;
        min-height: 18px;
        line-height: 18px;
        margin: -0.5em -0.6em -0.4em -0.6em;
        padding: .5em .6em .4em .6em;
        color: white;
    }

    .k-grid th.k-header,
    .k-grid-header {
        background: #4d4d4d;
        padding: 0em;
        color: #fff;
        border: 1px solid #4d4d4d;
        font-size: 11px;
        font-family: 'Gotham-Book',sans-serif;
        font-weight: 700;
        text-align: center;
    }

    .k-grid-content>table>tbody>tr{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
    }

    .k-grid-content>table>tbody>.k-alt{
        font-family: 'Gotham-Book',sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-align: center;
        background: #F5F5F5;
    }

    .k-window-titlebar{
        background: #26b67c;
        padding: 0.8em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        color: #ffffff;
        margin: 0;
        width: 100%;
        position: relative;
        font-size: 1.2em;
        height: 50px;
        border-left-width: 0px;
        border-right-width: 0px;

    }

    .k-window-titlebar .k-window-actions{
        right: -0.1em;
        top: -7px;
    }

    .k-window-titlebar .k-window-action{
        width: 40px;
        height: 46px;
        background-color: transparent;
        opacity: 1;
    }

    .k-window-action> .k-icon {
        height: 50px;
        width: 40px;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-window-action > .k-icon {
        background: no-repeat scroll 8px 15px #0c9d62;
        background-image: url("/resources/${channel}/theme/default/img/close.png");
    }
    .k-i-close{
        display: inline-block;
        background-image: url("/resources/${channel}/theme/default/img/close.png");

    }
    .k-grid td {
        word-wrap: break-word;
    }
    .action-container {
        font-size: 1.4em;
        margin: 0;
        text-align: center;
    }
    .k-grid-content {
        overflow-y: hidden;
    }

    .filter-section-content {
        margin-bottom: 0em;
    }
    .thin-btn {
        padding: 0.3em 0.6em;
        margin-top: 2px;
        margin-bottom: 10px;
        font-size: 14px;
    }

    .picker {
        z-index: 100000;
    }

    .depositBalance {
        float: right;
        border: 2px solid #13669b;
        padding: 10px;
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: 10px;
    }

</style>
<div class="main-content-section-container pendingInitElementIndicator" style="display: none;">
    <h2 class="heading">Reservations</h2>
    <div id="depositBalanceDiv" class="pendingInitElementIndicator" style="display: none;">
        <div class="depositBalance">
            <span>
                Deposit Balance:&nbsp;&nbsp;&nbsp;
            </span>
            <span style="color: red" data-bind="text: depositBalance">
            </span>
        </div>

        <div style="clear:both"></div>
    </div>
    <div id="errordv" class="error-section" style="display: none;"></div>
    <div class="main-content-section">
        <div class="filter-section background">
            <div class="pendingInitElementIndicator" style="display: none; width: 100%;" id="filterSectionContentDiv">
                <table cellpadding="0" cellspacing="0" class="filter-section-content" style="width: 95%">
                    <tr>
                        <th colspan="4" width="45%">Show Date(s):</th>
                        <th colspan="4" width="45%">Reservation Date(s):</th>
                        <th  width="10%">Status:</th>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><input id="startDate" name="startDateStr" type="text" style="width: 135px;"></td>
                        <td>To</td>
                        <td><input id="endDate" name="endDateStr" type="text" style="width: 135px;"></td>
                        <td>From</td>
                        <td><input id="reservationStartDate" name="reservationStartDateStr" type="text" style="width: 135px;"></td>
                        <td>To</td>
                        <td><input id="reservationEndDate" name="reservationEndDateStr" type="text" style="width: 135px;"></td>
                        <td><input id="wotReservationStatus" name="wotReservationStatus" type="text" style="width: 165px;"/></td>
                    </tr>
                    <tr>
                        <th colspan="9">Show Time(s):</th>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <div style="width: 100%; height: 100%">
                                <div data-bind="foreach: showTimeSearchCriterialList">
                                    <input type="radio" name="showTimes" data-bind="value: value"/>&nbsp;<span data-bind="text: text"></span>&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="action-container">
                    <input id="btnfilter" class="btn btn-primary thin-btn"
                           type="button" value="Filter">
                    <input id="btnfilterReset" class="btn btn-third thin-btn" type="reset"
                           value="Reset Filters">
                </div>
            </div>
        </div>
        <div class="grid-action-section pendingInitElementIndicator" style="display: none;">
            <input id="btnNewReservation" type="button" class="btn btn-primary" value="New Reservation" />
            <input id="btnExportAsExcel" type="button" class="btn btn-secondary" value="Export to Excel" />
        </div>
        <div class="items-records-container pendingInitElementIndicator" style="display: none;">
            <div id="reservationGrid">
            </div>
        </div>
    </div>
</div>



<div class=" clearfix"></div>

<div id="purchaseReservationWindow" style="display: none;">
    <div id="purchaseReservationContentDiv" style="width: 100%">
        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
            <tr>
                <td width="20%">Agent/Customer:</td>
                <td width="30%" data-bind="text: partnerName"></td>
                <td width="10%">Show:</td>
                <td width="40%" data-bind="text: showTimeDisplayFromDB"></td>
            </tr>
            <tr>
                <td width="20%">Date:</td>
                <td width="30%" data-bind="text: showDate"></td>
                <td width="10%">Quantity:</td>
                <td width="40%" data-bind="text: qtyFromDB"></td>
            </tr>
            <tr>
                <td width="20%">Receipt No.:</td>
                <td width="30%" data-bind="text: receiptNo"></td>
                <td width="10%">Remarks:</td>
                <td width="40%" data-bind="text: remarks"></td>
            </tr>
        </table>
        <div class=" clearfix"></div>
        <div style="width: 100%;"><hr style="width: 100%"/></div>
        <div class=" clearfix"></div>
        <div style="width: 100%;">
            <div style="width: 100%;">
                <!-- html kendo style grid begin -->
                <div class="k-grid k-widget">
                    <div class="k-grid-header" style="padding-right: 0px;">
                        <div class="k-grid-header-wrap k-auto-scrollable">
                            <table role="grid" style="width: 100%;">
                                <colgroup>
                                    <col style="width:90px"/>
                                    <col style="width:80px"/>
                                    <col style="width:60px"/>
                                    <col style="width:50px"/>
                                    <col style="width:70px"/>
                                </colgroup>
                                <thead role="rowgroup">
                                <tr role="row">
                                    <th data-field="splitListReservationCode" class="k-header" data-index="0">Reservation Code</th>
                                    <th data-field="splitListShowDate" class="k-header" data-index="1">Show Date</th>
                                    <th data-field="splitListName" class="k-header" data-index="2">Name</th>
                                    <th data-field="splitListQty" class="k-header" data-index="3">Qty</th>
                                    <th data-field="splitListType" class="k-header" data-index="4">&nbsp;</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="k-grid-content k-auto-scrollable">
                        <table style="width: 100%;" id="purchaseReservationDetailsDiv" data-role="grid" role="grid">
                            <colgroup>
                                <col style="width:90px"/>
                                <col style="width:80px"/>
                                <col style="width:60px"/>
                                <col style="width:50px"/>
                                <col style="width:70px"/>
                            </colgroup>
                            <tbody role="rowgroup">
                            <tr role="row" style="height: 36px;">
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseOriginalItem().receiptNo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseOriginalItem().showDateInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseOriginalItem().showTimeInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseOriginalItem().pinCodeQty"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseOriginalItem().pinCodeType"></span>
                                </td>
                            </tr>
                            <tr class="k-alt" role="row" style="height: 36px;" data-bind="visible : pendingPurchaseQty() > 0 ">
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseNewItem().receiptNo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseNewItem().showDateInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseNewItem().showTimeInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingPurchaseNewItem().pinCodeQty"></span>
                                </td>
                                <td role="gridcell">
                                    <input id="btnClearPurchaseQty" type="button" class="btn btn-primary" value="Delete" data-bind="click: clearPurchaseQty, enable: isNotInPurchase() "/>
                                </td>
                            </tr>
                            <tr class="k-alt" role="row" style="height: 36px;" data-bind="visible : pendingPurchaseQty() <= 0 ">
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <input type="text" data-bind="value : inputPendingPurchaseQty" maxlength="4" style="width: 50px;text-align: center;"/>
                                </td>
                                <td role="gridcell">
                                    <input id="btnApplyPurchaseQty" type="button" class="btn btn-primary" value="Purchase" data-bind="click: applyPurchaseQty, enable: isNotInPurchase() "/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- html kendo style grid end -->
            </div>
        </div>
        <div class=" clearfix"></div>
        <div style="width: 100%;"><br/></div>
        <div class=" clearfix"></div>
        <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
            <input type="button" class="btn btn-primary" data-bind="visible: isEditable && purchaseModel && pendingPurchaseQty() > 0, click: doPurchaseReservationValidation, enable: isNotInPurchase()" value="Confirm Purchase"/>
            <input type="button" class="btn btn-third" data-bind="click: closeModal" value="Cancel"/>
        </div>
        <div class=" clearfix"></div>
    </div>
</div>



<div class=" clearfix"></div>

<div id="splitReservationWindow" style="display: none;">
    <div id="splitReservationContentDiv" style="width: 100%">
        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
            <tr>
                <td width="20%">Agent/Customer:</td>
                <td width="30%" data-bind="text: partnerName"></td>
                <td width="10%">Show:</td>
                <td width="40%" data-bind="text: showTimeDisplayFromDB"></td>
            </tr>
            <tr>
                <td width="20%">Date:</td>
                <td width="30%" data-bind="text: showDate"></td>
                <td width="10%">Quantity:</td>
                <td width="40%" data-bind="text: qtyFromDB"></td>
            </tr>
            <tr>
                <td width="20%">Receipt No.:</td>
                <td width="30%" data-bind="text: receiptNo"></td>
                <td width="10%">Remarks:</td>
                <td width="40%" data-bind="text: remarks"></td>
            </tr>
        </table>
        <div class=" clearfix"></div>
        <div style="width: 100%;"><hr style="width: 100%"/></div>
        <div class=" clearfix"></div>
        <div style="width: 100%;">
            <div style="width: 100%;">
                <!-- html kendo style grid begin -->
                <div class="k-grid k-widget">
                    <div class="k-grid-header" style="padding-right: 0px;">
                        <div class="k-grid-header-wrap k-auto-scrollable">
                            <table role="grid" style="width: 100%;">
                                <colgroup>
                                    <col style="width:90px"/>
                                    <col style="width:80px"/>
                                    <col style="width:60px"/>
                                    <col style="width:50px"/>
                                    <col style="width:70px"/>
                                </colgroup>
                                <thead role="rowgroup">
                                <tr role="row">
                                    <th data-field="splitListReservationCode" class="k-header" data-index="0">Reservation Code</th>
                                    <th data-field="splitListShowDate" class="k-header" data-index="1">Show Date</th>
                                    <th data-field="splitListName" class="k-header" data-index="2">Name</th>
                                    <th data-field="splitListQty" class="k-header" data-index="3">Qty</th>
                                    <th data-field="splitListType" class="k-header" data-index="4">&nbsp;</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="k-grid-content k-auto-scrollable">
                        <table style="width: 100%;" id="splitReservationDetailsDiv" data-role="grid" role="grid">
                            <colgroup>
                                <col style="width:90px"/>
                                <col style="width:80px"/>
                                <col style="width:60px"/>
                                <col style="width:50px"/>
                                <col style="width:70px"/>
                            </colgroup>
                            <tbody role="rowgroup">
                            <tr role="row" style="height: 36px;">
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitOriginalItem().receiptNo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitOriginalItem().showDateInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitOriginalItem().showTimeInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitOriginalItem().pinCodeQty"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitOriginalItem().pinCodeType"></span>
                                </td>
                            </tr>
                            <tr class="k-alt" role="row" style="height: 36px;" data-bind="visible : pendingSplitQty() > 0 ">
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitNewItem().receiptNo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitNewItem().showDateInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitNewItem().showTimeInfo"></span>
                                </td>
                                <td role="gridcell">
                                    <span data-bind="text: pendingSplitNewItem().pinCodeQty"></span>
                                </td>
                                <td role="gridcell">
                                    <input id="btnClearSplitQty" type="button" class="btn btn-primary" value="Delete" data-bind="click: clearSplitQty, enable: isNotInSplitting() "/>
                                </td>
                            </tr>
                            <tr class="k-alt" role="row" style="height: 36px;" data-bind="visible : pendingSplitQty() <= 0 ">
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <span></span>
                                </td>
                                <td role="gridcell">
                                    <input type="text" data-bind="value : inputPendingSplitQty" maxlength="4" style="width: 50px;text-align: center;"/>
                                </td>
                                <td role="gridcell">
                                    <input id="btnApplySplitQty" type="button" class="btn btn-primary" value="Add New" data-bind="click: applySplitQty, enable: isNotInSplitting() "/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- html kendo style grid end -->
            </div>
        </div>
        <div class=" clearfix"></div>
        <div style="width: 100%;"><br/></div>
        <div class=" clearfix"></div>
        <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
            <input type="button" class="btn btn-primary" data-bind="visible: isEditable && splitMode && (pendingSplitQty() > 0), click: doSplitReservationValidation, enable: isNotInSplitting()" value="Apply Split"/>
            <input type="button" class="btn btn-third" data-bind="click: closeModal" value="Cancel"/>
        </div>
        <div class=" clearfix"></div>
    </div>
</div>

<div id="viewReservationWindow" style="display: none;">
    <div style="width: 100%">
        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section">
            <tr data-bind="visible: isAnExistingOrder() ">
                <td width="20%">Agent/Customer:</td>
                <td width="30%" data-bind="text: partnerName"></td>
                <td width="10%">Show:</td>
                <td width="40%" data-bind="text: showTimeDisplayFromDB"></td>
            </tr>
            <tr data-bind="visible: isAnExistingOrder()">
                <td width="20%">Date:</td>
                <td width="30%" data-bind="text: showDate"></td>
                <td width="10%">Quantity:</td>
                <td width="40%" data-bind="text: qtyFromDB"></td>
            </tr>
            <tr data-bind="visible: isAnExistingOrder()">
                <td width="20%">Receipt No.:</td>
                <td width="30%" data-bind="text: receiptNo"></td>
                <td width="10%">Remarks:</td>
                <td width="40%" data-bind="text: remarks"></td>
            </tr>
        </table>
        <div class=" clearfix"></div>
        <div style="width: 100%;"><hr data-bind="visible: isAnExistingOrder() && isEditable()"/></div>
        <div style="width: 100%;"><br data-bind="visible: isAnExistingOrder() && isEditable()"/></div>
        <div class=" clearfix"></div>
        <div style="width: 100%;">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="information-section" data-bind="visible: isEditable">
                <tr>
                    <td width="30%"><b>Step 1:</b> Select Show Date</td>
                    <td width="70%">
                        <div data-bind="visible: createMode() || updateMode() ">
                            <input type="text" class="input-text" style="width: 30%;" data-bind="value: showDate" id="showDate">
                            <label for="showDate"><i class="fw-icon-calendar"
                                   style="vertical-align:middle; font-size:1.2em; color: #1ca9ea; padding: 0 0.3em;"
                                   data-profile-dov-cal-icon="1"></i></label>
                        </div>
                        <div data-bind="visible: splitMode">
                            <span data-bind=" text: showDate"></span>
                        </div>
                    </td>
                </tr>
                <tr data-bind="visible: showTimeList().length > 0">
                    <td width="30%"><b>Step 2:</b> Select Show Time</td>
                    <td width="70%">
                        <!--  <select data-bind="options: showTimeList, optionsText: 'text', optionsValue: 'value', value: showTime"></select> -->
                        <div data-bind="foreach: showTimeList, visible: createMode() || updateMode() ">
                            <div>
                                <input type="radio" name="showTime" data-bind="value:value, checked:$parent.showTime"/>&nbsp;&nbsp;&nbsp;<span data-bind="text: text"></span>
                            </div>
                        </div>
                        <div data-bind="visible: splitMode()"><span data-bind="text: showTimeDisplay"></span></div>
                    </td>
                </tr>
                <tr data-bind="visible: showTimeList().length > 0">
                    <td width="30%"><b>Step 3:</b> Select Quantity</td>
                    <td width="70%">
                        <input type="text" class="input-text" data-bind="value: qty" style="width: 10%"></input>
                    </td>
                </tr>
                <tr data-bind="visible: showTimeList().length > 0">
                    <td width="30%"><b>Step 4:</b> Fill-in Remarks</td>
                    <td width="70%">
                        <input type="text" class="input-text" data-bind="value: remarks" style="width: 95%"/>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 100%;"><div style="width: 100%;" class="reservation-charge-info" data-bind="visible: isEditable(), text: notifInfo"></div></div>
        <div class=" clearfix"></div>
        <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
            <a type="button" id="btnSaveNew" class="btn btn-primary" data-bind="visible: isEditable() && createMode, click: verifyNewReservation">Save New Reservation</a>
            <a type="button" id="btnUpdateReservation" class="btn btn-primary" data-bind="visible: isEditable() && updateMode, click: verifyUpdateReservation">Update Reservation</a>
            <a type="button" id="btnClose" class="btn btn-third" data-bind="click: closeModal">Close</a>
        </div>
        <div class=" clearfix"></div>
    </div>
</div>

<div id="viewReservationConfirmWindow" style="display: none;">

    <div style="text-align: center">

        <div><br/></div>

        <div data-bind="visible: depositToBeCharged() == 0 && depositToBeRefunded() == 0">
            You will be charged S$0.0 for this booking modification.
        </div>

        <div data-bind="visible: depositToBeCharged() > 0 && !isNew()" style="color: red">
            You will be charged <span data-bind="text: depositToBeChargedText"></span> for this booking modification.
        </div>

        <div data-bind="visible: depositToBeCharged() > 0 && isNew()" style="color: red">
            You will be charged <span data-bind="text: depositToBeChargedText"></span> for your reservation.
        </div>

        <div data-bind="visible: depositToBeRefunded() > 0 && !isNew()" style="color: blue">
            We will refund <span data-bind="text: depositToBeRefundedText"></span> to your deposit.
        </div>

        <div><br/></div>

        <div style="text-align: center">
            <div style="text-align: left;margin-left: 25%;">
                <div><span>Show Date:</span>&nbsp;<span data-bind="text: showDateDisplay" style="font-weight: bold;"></span></div>
                <div><span>Show Time:</span>&nbsp;<span data-bind="text: showTimeDisplay" style="font-weight: bold;"></span></div>
            </div>
            <div style="width: 100%" data-bind="visible: confirmType() == 'S' || confirmType() == 'CP' ">
                <table cellpadding="0" cellspacing="0" width="50%" class="information-section" data-bind="visible: showComputation" style="border: 2px solid black; margin-left: 25%">
                    <tr data-bind="visible: ticketPurchased() > 0">
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Price:</div>
                            <div>
                                <span data-bind="text: ticketPriceText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: totalQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: ticketPurchasedText" style="text-align:right"></td>
                    </tr>
                    <tr style="border-top: 1px solid black">
                        <td width="60%" style="border-right: 1px solid black;text-align:right; font-weight: bold;">Total Amount:</td>
                        <td width="40%" data-bind="text: totalAmountText" style="text-align:right"></td>
                    </tr>
                </table>
            </div>

            <div style="width: 100%" data-bind="visible: confirmType() == 'U'">
                <table cellpadding="0" cellspacing="0" width="50%" class="information-section" data-bind="visible: showComputation" style="border: 2px solid black; margin-left: 25%">

                    <tr>
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Refund:</div>
                            <div>
                                <span data-bind="text: originalTicketPriceText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: originalTotalQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: originalRefundAmountText" style="text-align:right"></td>
                    </tr>

                    <tr>
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Price:</div>
                            <div>
                                <span data-bind="text: ticketPriceText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: totalQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: ticketPurchasedText" style="text-align:right"></td>
                    </tr>
                    <tr data-bind="visible: penaltyCharge() > 0">
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Administration Charge:</div>
                            <div>
                                <span data-bind="text: penaltyChargeCfgText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: penaltyChargeQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: penaltyChargeText" style="text-align:right"></td>
                    </tr>
                    <tr style="border-top: 1px solid black">
                        <td width="60%" style="border-right: 1px solid black; text-align:right; font-weight: bold;">Total Amount:</td>
                        <td width="40%" data-bind="text: totalAmountText" style="text-align:right"></td>
                    </tr>
                </table>
            </div>

            <div style="width: 100%" data-bind="visible: confirmType() == 'CC'">
                <table cellpadding="0" cellspacing="0" width="50%" class="information-section" data-bind="visible: showComputation" style="border: 2px solid black; margin-left: 25%">
                    <tr>
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Refund:</div>
                            <div>
                                <span data-bind="text: ticketPriceText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: totalQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: refundedAmountText" style="text-align:right"></td>
                    </tr>
                    <tr data-bind="visible: penaltyCharge() > 0">
                        <td width="60%" style="border-right: 1px solid black;text-align:right">
                            <div style="font-weight: bold;">Administration Charge:</div>
                            <div>
                                <span data-bind="text: penaltyChargeCfgText"></span>&nbsp;<span>X</span>&nbsp;<span data-bind="text: totalQtyText"></span>
                            </div>
                        </td>
                        <td width="40%" data-bind="text: penaltyChargeText" style="text-align:right"></td>
                    </tr>
                    <tr style="border-top: 1px solid black">
                        <td width="60%" style="border-right: 1px solid black;text-align:right; font-weight: bold;">Total Amount:</td>
                        <td width="40%" data-bind="text: totalAmountText" style="text-align:right"></td>
                    </tr>
                </table>
            </div>
        </div>

        <h4>
            Do you want to proceed?
        </h4>

        <div class=" clearfix"></div>
        <div style="width: 100%;"><br/></div>
        <div class=" clearfix"></div>
        <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
            <a type="button" id="btnProceedCancel"
               class="btn btn-primary" data-bind="visible: confirmType() == 'CC', click: proceedCancel">Proceed</a>

            <a type="button" id="btnProceedSave"
               class="btn btn-primary" data-bind="visible: confirmType() == 'S', click: proceedSave">Proceed</a>

            <a type="button" id="btnProceedUpdate"
               class="btn btn-primary" data-bind="visible: confirmType() == 'U', click: proceedUpdate">Proceed</a>

            <a type="button" id="btnProceedPurchase"
               class="btn btn-primary" data-bind="visible: confirmType() == 'CP', click: proceedPurchase">Proceed</a>

            <a type="button" id="btnCancel"
               class="btn btn-third" data-bind="click: closeModal">Cancel</a>
        </div>

    </div>

    <div class=" clearfix"></div>
</div>

<div id="errorWindow" style="display: none;">
    <div><br/></div>
    <div style="text-align: center" data-bind="text: errorMessage"></div>
    <div><br/></div>
    <div><br/></div>
    <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
        <a type="button" id="btnCancel"
           class="btn btn-primary" data-bind="click: closeModal">&nbsp;&nbsp;OK&nbsp;&nbsp;</a>
    </div>
    <div class=" clearfix"></div>
</div>

<div id="confirmMsgWindow" style="display: none;">
    <div><br/></div>
    <div style="text-align: center" data-bind="text: confirmMessage"></div>
    <div><br/></div>
    <div><br/></div>
    <div class="action-container" style="padding-top: 10px; padding-bottom: 10px">
        <a type="button" id="btnDoubleConfirmPerform"
           class="btn btn-primary" data-bind="click: confirmModal">&nbsp;&nbsp;Confirm&nbsp;&nbsp;</a>
        <a type="button" id="btnDoubleConfirmCancel"
           class="btn btn-third" data-bind="click: cancelModal">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</a>
    </div>
    <div class=" clearfix"></div>
</div>

<div style="display:none;">
    <form id="wotExportForm" method="post" action="${ctx.contextPath}${API_PREFIX!""}/secured/partner-wot/export-to-excel" target="excel_export_iframe_target">
        <input type="hidden" name="startDateStr"   id="wotExportForm_startDateStr"   value="" />
        <input type="hidden" name="endDateStr"     id="wotExportForm_endDateStr"     value="" />
        <input type="hidden" name="reservationStartDateStr"   id="wotExportForm_reservationStartDateStr"   value="" />
        <input type="hidden" name="reservationEndDateStr"     id="wotExportForm_reservationEndDateStr"     value="" />
        <input type="hidden" name="filterStatus"   id="wotExportForm_filterStatus"   value="" />
        <input type="hidden" name="showTime"       id="wotExportForm_showTime"       value="" />
    </form>
</div>