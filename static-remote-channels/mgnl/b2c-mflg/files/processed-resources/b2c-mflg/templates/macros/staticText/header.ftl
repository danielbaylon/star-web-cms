[#assign staticText =
{"en":
{
"signUp": "Sign Up",
"cartItems": "item(s)",
"yourCart": "YOUR CART",
"cartEmpty": "Your Cart is empty",
"total": "total",
"checkout": "Check out"
},
"zh_CN":
{
"signUp": "报名",
"cartItems": "件商品",
"yourCart": "您的预订单",
"cartEmpty": "您的预订单是空的",
"total": "总计",
"checkout": "结算"
}
}
]
