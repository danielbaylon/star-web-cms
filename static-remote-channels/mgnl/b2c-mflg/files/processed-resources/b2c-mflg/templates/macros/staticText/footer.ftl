[#assign staticText =
    {
        "en" :
        {
            "supportedBrowsersLabel" : "Supported Browsers: Explorer 8 and above, Firefox, Chrome, Safari",
            "aboutUsLabel" : "About Us",
            "tosLabel" : "Terms of Use",
            "dppLabel" : "Data Protection Policy",
            "faqLabel" : "FAQ",
            "copyrightLabel" : "Copyright &copy; 2016 Mount Faber Leisure Group Pte. Ltd."
        }
    }
]
