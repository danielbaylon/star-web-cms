[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<div class="container contain" id="confirmPage">
    <div class="booking-process-step3"></div>
    <div class="clearfix"></div>

    <div class="single-col-content" data-bind="visible: !isForConfirm() & !${cmsfn.isEditMode()?c}">
        <div class="main-error">
            <div class="main-error-msg">You may have refreshed the page or pressed back while in the middle of a transaction.
                Your transaction has been invalidated. Please checkout again. <a href="${sitePath}"
                                                                                 style="text-decoration: underline">Let's take
                    you back to the home page.</a></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="basic-content" data-bind="visible: isForConfirm() | ${cmsfn.isEditMode()?c}">
        <div class="checkout">
            <h2>Confirmation</h2>
            <span class="confirm-messages">
                <span class="timer-msg">Please confirm your order in <span class="confirm-timer"
                                                                           id="confirmTimer">00:00</span> minutes.</span>
            </span>
        </div>

        <!-- ko foreach: {data: cmsProducts, as: 'cmsProduct'} -->
        <table class="checkout-summary">
            <colgroup>
                <col span="1" style="width: 35%"/>
                <col span="1" style="width: 37%"/>
                <col span="1" style="width: 10%"/>
                <col span="1" style="width: 5%"/>
                <col span="1" style="width: 13%"/>
            [#--<col span="1" style="width: 5%"/>--]
            </colgroup>
            <tr class="row-title">
                <th colspan="6">
                    <span data-bind="text: cmsProduct.name"></span>
                </th>
            </tr>
            <tr class="row-header">
                <th class="left_row">Item</th>
                <th class="left_row">Details</th>
                <th>Unit Price</th>
                <th>Qty.</th>
                <th>Amount</th>
            [#--<th>&nbsp;</th>--]
            </tr>
            <tbody class="section-main" data-bind="foreach: {data: cmsProduct.items, as: 'cmsItem'}">
            <tr class="row-main" data-bind="ifnot: cmsItem.isTopup">
                <td class="cell-item">
                  <span data-bind="text: cmsItem.name"></span>
                  <!-- ko if: cmsItem.hasDiscount -->
                  <br><span style="font-style: italic;" data-bind="text: cmsItem.discountName"></span>
                  <!-- /ko -->
                </td>
                <td class="cell-desc">
                    <div class="cell-desc-div">
                        <span data-bind="html: cmsItem.description"></span>
                    </div>
                    <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                        <span data-bind="text: cmsItem.discountLabel"></span>
                    </div>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: cmsItem.priceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: qty"></span>
                </td>
                <td class="cell-sub">
                    <div data-bind="visible: !cmsItem.hasDiscount()">
                        <span data-bind="text: cmsItem.totalText"></span>
                    </div>
                    <div data-bind="visible: cmsItem.hasDiscount">
                        <div>
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div>
                            <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                        </div>
                    </div>
                </td>
            [#--<td class="cell-remove">&nbsp;</td>--]
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.isTopup">
                <td class="cell-item">
                    <i class="icon-plus-sign"></i>
                    <span data-bind="text: cmsItem.name"></span>
                </td>
                <td class="cell-desc">
                    <div class="cell-desc-div">
                        <span data-bind="text: cmsItem.description"></span>
                    </div>
                    <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                        <span data-bind="text: cmsItem.discountLabel"></span>
                    </div>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: cmsItem.priceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: qty"></span>
                </td>
                <td class="cell-sub">
                    <div data-bind="visible: !cmsItem.hasDiscount()">
                        <span data-bind="text: cmsItem.totalText"></span>
                    </div>
                    <div data-bind="visible: cmsItem.hasDiscount">
                        <div>
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div>
                            <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                        </div>
                    </div>
                </td>
            [#--<td class="cell-remove">&nbsp;</td>--]
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.hasMainCourse">
                <td class="cell-item"><i class="icon-food"></i>&nbsp;Main Course Selections</td>
                <td class="cell-desc">
                    <div class="cell-desc-div" data-bind="html: cmsItem.mainCourseText"></div>
                </td>
                <td class="cell-unit">&nbsp;</td>
                <td class="cell-qty">&nbsp;</td>
                <td class="cell-sub">&nbsp;</td>
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.hasRemarks">
                <td class="cell-item">Special Requests</td>
                <td class="cell-desc" colspan="4">
                    <div class="cell-desc-div" data-bind="html: cmsItem.remarks"></div>
                </td>
            </tr>
            </tbody>
            <tr class="row-subtotal">
              <td colspan="4" class="cell-subtotal-lbl">Sub Total:
                <br/>Service Charge(<span data-bind="text: cmsProduct.serviceCharge"></span>%):
                <br/>GST(<span data-bind="text: cmsProduct.gst"></span>%):
                <br/>Additional Items:
              </td>
              <td class="cell-subtotal-amt"><span data-bind="text: cmsProduct.subtotalText"></span>
                <br/><span data-bind="text: cmsProduct.serviceChargeTotalText"></span>
                <br/><span data-bind="text: cmsProduct.gstTotalText"></span>
                <br/><span data-bind="text: cmsProduct.topUpPriceText"></span>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <!-- /ko -->

        <table class="checkout-summary" data-bind="if: hasBookingFee">
            <colgroup>
                <col span="1" style="width: 67%"/>
                <col span="1" style="width: 10%"/>
                <col span="1" style="width: 5%"/>
                <col span="1" style="width: 13%"/>
                <col span="1" style="width: 5%"/>
            </colgroup>
            <tr class="row-title">
                <th colspan="5">Booking Fee</th>
            </tr>
            <tr class="row-header">
                <th class="left_row">Booking Fee</th>
                <th>Unit Price</th>
                <th>Qty.</th>
                <th>Amount</th>
                <th>&nbsp;</th>
            </tr>
            <tbody class="section-main">
            <tr class="row-main">
                <td class="cell-item cell-book-fee">
                    <span data-bind="text: bookFeeMode"></span>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: bookPriceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: bookFeeQty"></span>
                </td>
                <td class="cell-sub">
                    <span data-bind="text: bookFeeSubTotalText"></span>
                </td>
                <td class="">&nbsp;</td>
            </tr>
            </tbody>
            <tr class="row-subtotal">
                <td class="cell-subtotal-waive">&nbsp;</td>
                <td colspan="2" class="cell-subtotal-lbl">Subtotal:</td>
                <td class="cell-subtotal-amt">
                    <span data-bind="text: bookFeeSubTotalText"></span>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>

        <div class="grand-total">
      <span class="grand-total-text">Grand Total:<span class="grand-total-amt" id="grandTotal"
                                                       data-bind="text: totalText"></span></span>
        </div>

        <div class="checkout-details" data-bind="with: customer">
            <h3><i class="icon-pencil"></i>Checkout Information</h3>

            <div class="checkout-form">
                <div class="form-field" style="display:block;">
                    <span class="field-title">Email:</span><span class="static-field" data-bind="text: email"></span>
                </div>
                <div class="form-field" style="display:block;">
                    <span class="field-title">Name:</span>
                    <span class="static-field">
                    	<span data-bind="text: salutation"></span>
                    	<span data-bind="text: name"></span>
                    </span>
                </div>
                <div class="form-field">
                    <span class="field-title">ID:</span><span class="static-field" data-bind="text: idDisplay"></span>
                </div>
                <div class="form-field">
                    <span class="field-title">Company Name:</span><span class="static-field" data-bind="text: companyName"></span>
                </div>
                <div class="form-field">
                    <span class="field-title">Mobile Number:</span><span class="static-field" data-bind="text: mobile"></span>
                </div>
                <div class="form-field">
                    <span class="field-title">Payment Type:</span><span class="static-field" data-bind="text: paymentType"></span>
                </div>
                <div class="form-field" style="margin: 0.3em 0 0 15px" data-bind="visible: subscribed">
                    &nbsp;&nbsp;<strong><i class="icon-check"></i> Subscribed to Mt. Faber Newsletter</strong>
                </div>
                <div class="button-panel">
                    <a href="javascript:void(0)" class="btn-mflg-gray" id="btnCancel">Cancel</a>&nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0)" class="btn-mflg-green" id="btnPay">Pay Now</a>
                </div>
            </div>
        </div>

        [@cms.area name="adBanner"/]

        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>

</div>