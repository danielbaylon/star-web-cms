[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<script>
  var cmsBookingModel = null;
  [#assign params = ctx.aggregationState.getSelectors()]
  [#if params[0]?has_content]
    [#assign packageName = params[0]]
  [/#if]
  [#assign rootPath = "/b2c-mflg/"]
  [#assign cmsProductUrl = "javascript:void(0);"]
  [#assign previewMode = ctx.getParameter("preview")!]
  [#assign proceedShowingProduct = true]
  [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance && packageName?has_content]
    [#assign proceedShowingProduct = true]
  [/#if]
  [#if proceedShowingProduct]
    [#assign sdPackage = sdfn.getSkyDiningPackage(packageName)!]
    [#if sdPackage?has_content]
      [#assign ccPromos = sdfn.getPromotions(packageName,"CreditCard")!]
      [#assign jcPromos = sdfn.getPromotions(packageName,"JewelCard")!]
      [#assign pcPromos = sdfn.getPromotions(packageName,"PromoCode")!]
      [#assign themes = sdfn.getThemes(packageName)]
      [#assign topUps = sdfn.getTopUps(packageName)]
      [#assign hasPromo = ccPromos?has_content || jcPromos?has_content || pcPromos?has_content]
    cmsBookingModel = {
      "bookingModel": {
        "blackoutDates": null,
        "blackoutDatesArr": [],
        "cableCarClosures": null,
        "ccTickets": [],
        "chosenSchedule": 0,
        "dateOfVisit": null,
        "descUrl": null,
        "displayDate": true,
        "displayDescription": "${sdPackage.description?js_string!""}",
        "displayImagePath": "${sdPackage.imageHash?js_string!""}",
        "displayTitle": "${sdPackage.name?js_string!""}",
        "hasCableCarClosure": false,
        [#if ccPromos?has_content]
          "hasCcPromo": true,
        [#else]
          "hasCcPromo": false,
        [/#if]
        [#if jcPromos?has_content]
          "hasJcPromo": true,
        [#else]
          "hasJcPromo": false,
        [/#if]
        "hasMainCourses": false,
        [#if sdPackage.notes?has_content]
          "hasNotes": true,
        [#else]
          "hasNotes": false,
        [/#if]
        [#if pcPromos?has_content]
          "hasPcPromo": true,
        [#else]
          "hasPcPromo": false,
        [/#if]
        [#if hasPromo]
          "hasPromotion": true,
        [#else]
          "hasPromotion": false,
        [/#if]
        [#if themes?has_content]
          "hasTheme": true,
        [#else]
          "hasTheme": false,
        [/#if]
        [#if topUps?has_content]
          "hasTopup": true,
        [#else]
          "hasTopup": false,
        [/#if]
        "isActive": true,
        "jcExpiry": null,
        "jcNumber": null,
        "jcTickets": [],
        "mainCourses": null,
        "mainTickets": [],
        "maxDate": "",
        "maxDateArr": [],
        "maxPax":${sdPackage.maxPax},
        "minBookingDays":${sdPackage.salesCutOffDays},
        "minDate": "",
        "minDateArr": [],
        "minPax":${sdPackage.minPax},
        "nonTicketed": true,
        "notes": "${sdPackage.notes?js_string!""}",
        "paxPerTicket": 1,
        "pcTickets": [],
        "pcValue": null,
        "productId":${sdPackage.@name},
        "remarks": null,
        "schedules": [],
        "sdCcTickets": [
          [#list ccPromos as ccPromo]
            [#assign menus = sdfn.getPromoMenus(ccPromo.@name)]
            [#if menus?has_content]
              [#list menus as menu]
                [#assign newPrice = sdfn.getDiscountedPrice(menu.price, ccPromo.priceType, ccPromo.discountPrice)]
                {
                  "discountId": ${ccPromo.@name!'null'},
                  "discountItem": true,
                  "discountPrice": ${ccPromo.discountPrice!'null'},
                "displayDesc": [@compress single_line=true]"${menu.description?js_string!""}"[/@compress],
                  "displayImagePath": null,
                  "displayName": "${menu.name?js_string!""}",
                  "displayPrice": "${sdfn.formatPrice(sdPackage.minPax * newPrice)!0.00}",
                  "gst":${menu.gst},
                  "hasMainCourses": true,
                  "id":${menu.@name},
                  "itemCode": 0,
                  "link": null,
                  "merchantId": "${ccPromo.merchantId!0}",
                  "priceType": ${ccPromo.priceType!'null'},
                  "qty": 0,
                  "sdMainCourses": [
                    [#assign mcs = sdfn.getMainCourses(menu.@name)]
                    [#list mcs as mc]
                      {
                        "id": ${mc.@name},
                      "name": [@compress single_line=true]"${mc.description?js_string!""}"[/@compress],
                        "qty": 0
                      },
                    [/#list]
                  ],
                  "serviceCharge":${menu.serviceCharge},
                  "subName": "${ccPromo.name?js_string!""}",
                  "ticketType": "Standard",
                  "unitPrice":${sdfn.formatPrice(newPrice)},
                  "usualPrice": "${sdfn.formatPrice(menu.price)}",
                },
              [/#list]
            [/#if]
          [/#list]
        ],
        "sdJcTickets": [],
        [#assign isEarlyBirdPeriod = sdfn.isEarlyBirdPeriod(sdPackage.earlyBirdEndDate)]
        [#if isEarlyBirdPeriod]
          [#assign ebPromos = sdfn.getPromotions(packageName,"EarlyBird")!]
          "sdMenus": [
            [#list ebPromos as ebPromo]
              [#assign menus = sdfn.getPromoMenus(ebPromo.@name)]
              [#if menus?has_content]
                [#list menus as menu]
                  [#assign newPrice = sdfn.getDiscountedPrice(menu.price, ebPromo.priceType, ebPromo.discountPrice)]
                  {
                    "discountId": ${ebPromo.@name!'null'},
                    "discountItem": true,
                    "discountPrice": ${ebPromo.discountPrice!'null'},
                  "displayDesc": [@compress single_line=true]"${menu.description?js_string!""}"[/@compress],
                    "displayImagePath": null,
                    "displayName": "${menu.name?js_string!""}",
                    "displayPrice": "${sdfn.formatPrice(sdPackage.minPax * newPrice)!0.00}",
                    "gst":${menu.gst},
                    "hasMainCourses": true,
                    "id":${menu.@name},
                    "itemCode": 0,
                    "link": null,
                    "merchantId": "${ebPromo.merchantId!0}",
                    "priceType": ${ebPromo.priceType!'null'},
                    "qty": 0,
                    "sdMainCourses": [
                      [#assign mcs = sdfn.getMainCourses(menu.@name)]
                      [#list mcs as mc]
                        {
                          "id": ${mc.@name},
                        "name": [@compress single_line=true]"${mc.description?js_string!""}"[/@compress],
                          "qty": 0
                        },
                      [/#list]
                    ],
                    "serviceCharge":${menu.serviceCharge},
                    "subName": "${ebPromo.name?js_string!""}",
                    "ticketType": "Standard",
                    "unitPrice":${sdfn.formatPrice(newPrice)},
                    "usualPrice": "${sdfn.formatPrice(menu.price)}",
                  },
                [/#list]
              [/#if]
            [/#list]
          ],
        [#else]
          "sdMenus": [
            [#assign menus = sdfn.getMenus(packageName)]
            [#list menus as menu]
              {
                "discountId": ${menu.discountId!'null'},
                "discountItem": ${menu.discountItem!'null'},
                "discountPrice": ${menu.discountPrice!'null'},
              "displayDesc": [@compress single_line=true]"${menu.description?js_string!""}"[/@compress],
                "displayImagePath": null,
                "displayName": "${menu.name?js_string!""}",
                "displayPrice": "${sdfn.formatPrice(sdPackage.minPax * menu.price)!0.00}",
                "gst":${menu.gst},
                "hasMainCourses": true,
                "id":${menu.@name},
                "itemCode": 0,
                "link": null,
                "merchantId": "${menu.merchantId!0}",
                "priceType": ${menu.priceType!'null'},
                "qty": 0,
                "sdMainCourses": [
                  [#assign mcs = sdfn.getMainCourses(menu.@name)]
                  [#list mcs as mc]
                    {
                      "id": ${mc.@name},
                    "name": [@compress single_line=true]"${mc.description?js_string!""}"[/@compress],
                      "qty": 0
                    },
                  [/#list]
                ],
                "serviceCharge":${menu.serviceCharge},
                "subName": null,
                "ticketType": "Standard",
                "unitPrice":${sdfn.formatPrice(menu.price)},
                "usualPrice": "${menu.usualPrice!""}",
              },
            [/#list]
          ],
        [/#if]

        "sdPcTickets": [],
        "sdThemes": [
          [#list themes as theme]
            {
              "discountId": null,
              "discountItem": false,
              "discountPrice": null,
            "displayDesc": [@compress single_line=true]"${theme.description?js_string!""}"[/@compress],
              "displayImagePath": "${theme.imageHash?js_string!""}",
              "displayName": null,
              "displayPrice": null,
              "gst": null,
              "hasMainCourses": false,
              "id": ${theme.@name},
              "itemCode": 0,
              "link": null,
              "merchantId": null,
              "originalPrice": null,
              "priceType": null,
              "qty": 0,
              "sdMainCourses": [],
              "serviceCharge": null,
              "subName": null,
              "ticketType": null,
              "unitPrice": null,
              "usualPrice": null
            },
          [/#list]
        ],
        "sdTopUps": [
          [#list topUps as topUp]
            {
              "discountId": null,
              "discountItem": false,
              "discountPrice": null,
            "displayDesc": [@compress single_line=true]"${topUp.description?js_string!""}"[/@compress],
              "displayImagePath": "${topUp.imageHash?js_string!""}",
              "displayName": "${topUp.name?js_string!""}",
              "displayPrice": "${sdfn.formatPrice(topUp.price)}",
              "gst": null,
              "hasMainCourses": false,
              "id": ${topUp.@name},
              "itemCode": 0,
              "link": null,
              "merchantId": null,
              "originalPrice": null,
              "priceType": null,
              "qty": 0,
              "sdMainCourses": [],
              "serviceCharge": null,
              "subName": null,
              "ticketType": "Standard",
              "unitPrice": ${sdfn.formatPrice(topUp.price)},
              "usualPrice": null
            },
          [/#list]
        ],
        "showRemarks": true,
        "topupLimitType": "None",
        "topups": [],
        "type": "General",
        "usualPrices": {
          "Standard": "",
          "Child": "",
          "Adult": ""
        },
        "validFrom": null,
        "validUntil": null,
        "validityDays": null,
        "validityType": 0
      },
      "message": null,
      "success": true
    };
    [/#if]
  [/#if]


  var checkThemeModel = null;
  checkThemeModel = {
    "message": null,
    "success": true
  };

  var checkSessionModel = null;
  checkSessionModel = {
    "message": null,
    "success": true
  };

</script>

<div id="productDiv" class="form-panel" data-template="bookingFormGeneral">
  <div data-remarketing="true"></div>
  <div class="item-details">
    <h2 data-bind="text: displayTitle">DISPLAY_TITLE</h2>

    <div id="error-section" class="display-msg msg-error" data-errorsection="true" style="display: none;"
         data-bind="visible: isErr, html: errMsg"></div>
    <div data-bind="visible: showDateSelection">
      <div class="item-content">
        <div class="item-date-visit">
          <div class="item-step" style="margin-top: 0">Choose a date<label for="dov-field"><i class="icon-calendar"></i></label>
          </div>
          <div class="item-date-display picker-holder--inline-fixed">
            Date of Visit <input id="dov-field" type="text" data-datepicker="true" class="field date-of-visit"
                                 data-bind="value: dateOfVisit, style: {'border-color': dateOfVisit() == '' ? '#D83939' : '#bbbbbb'}">
          </div>
          <div class="item-step" style="margin-top: 0">Choose a session</div>
          <div data-bind="foreach: availableSessions" style="background-color: #DAEEF8; font-weight: bolder;">
            <label>
              <input type="radio" name="sessionRadio" data-bind="attr: {value: id}, checked: $root.skyDiningSession"/>
              <span data-bind="text: time"></span>
            </label>
            <br>
          </div>
          <div class="field-nothing" data-bind="fadeVisible: showNoSessions()"><span
            data-bind="text: noSessionsText"></span></div>
        </div>

        <div class="item-intro">
          <img class="product-image" data-bind="attr: {'src':displayImagePath}"/>

          <div class="item-desc">
            <p data-bind="html: displayDescription">DISPLAY_DESCRIPTION</p>
            <!-- ko if:descUrl != '' -->
            <a target="_blank" class="more-info" data-bind="attr:{'href':descUrl}">More Info</a>
            <!-- /ko -->
          </div>
        </div>
      </div>
      <div class="btn-field">
        <div class="submit-btn">
          <a class="addToCart" href="javascript:void(0)" data-bind="click: nextStep">Next</a>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div data-bind="visible: showBookingSummary">
    <div class="item-step" style="margin-top: 10px;">Booking Summary</div>
    <table class="booking-summary" cellpadding="0" cellspacing="0">
      <tr>
        <td class="label">Date of Visit</td>
        <td width="text"><span data-bind="text: bookingSummaryDateText"></span></td>
      </tr>
      <tr data-bind="visible: showThemeSummary">
        <td class="label">Theme</td>
        <td width="text"><span data-bind="text: bookingSummaryThemeText"></span></td>
      </tr>
      <tr data-bind="visible: showMenuSummary">
        <td class="label">Menu</td>
        <td width="text"><span data-bind="text: bookingSummaryMenuText"></span></td>
      </tr>
      <tr data-bind="visible: showMenuSummary">
        <td class="label">Main Course(s)</td>
        <td width="text"><span data-bind="text: bookingSummaryMainCourseText"></span></td>
      </tr>
    </table>
  </div>

  <div data-bind="visible: showThemeSelection">
    <div class="item-step" style="margin-top: 10px;">Select Your Theme</div>
    <table class="ticket-form" cellpadding="0" cellspacing="0">
      <tr>
        <th width="85%" class="ticket-category">Themes</th>
        <th width="15%">Select</th>
      </tr>
      <tbody
        data-bind="foreach: themes">
      <tr data-bind="css:{'first-record':$index()==0}">
        <td class="ticket-category">
          <div class="topup-data">
            <!-- ko if:displayImagePath != '' && displayImagePath != null -->
            <img
              style="float: left; width: 50%; height: 50%; margin-right: 5px"
              data-bind="attr: {'src':displayImagePath}"/>
            <!-- /ko -->
            <!-- ko if:displayDesc != '' -->
            <span class="topup-desc" data-bind="text: displayDesc">TOPUP_DESC</span>
            <!-- /ko -->
          </div>
        </td>
        <td><input type="radio" name="selectedTheme"
                   data-bind="click: selectTheme"/></td>
      </tr>
      </tbody>
    </table>
    <div class="btn-field" style="margin-top: 10px">
      <div class="submit-btn">
        <a class="back" href="javascript:void(0)" data-bind="click: previousStep">Back</a>
        <a class="addToCart" href="javascript:void(0)" data-bind="click: nextStep">Next</a>
      </div>
    </div>
  </div>

  <div data-bind="visible: showMenuSelection">
    <div class="item-step" style="margin-top: 10px;">Select Your Menu</div>
    <table class="ticket-form" cellpadding="0" cellspacing="0">
      <tr id="std-header">
        <th width="85%" colspan="2" class="ticket-category">Menus</th>
        <th width="15%">Select</th>
      </tr>
      <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
        <tr class="header-clickable" data-bind="click: function(){changeActiveSection('std')}">
          <th colspan="3">
            <img src="${themePath}/img/icon-promotion.png"/>Standard Menus
            <i data-bind="css:{'icon-expand-alt':activeSection()!='std','icon-collapse-alt':activeSection()=='std'}"
               style="float:right;font-size:1.3em;color:#3ca7ba;"></i>
          </th>
        </tr>
        <tbody
          data-bind="foreach: mainTickets, fadeVisible: activeSection()=='std'">
        <tr data-bind="css:{'first-record':$index()==0}">
          <td width="85%" colspan="2" class="ticket-category">
            <div class="book-item-data">
              <a href="javascript:void(0)" data-bind="click: toggleCollapse">
                <i data-bind="css:{'icon-expand-alt':collapsed(),'icon-collapse-alt':!collapsed()}"
                   style="float:left;font-size:1.3em;color:#3ca7ba;margin-right: 5px"></i>
              </a>
              <span class="book-item-name" data-bind="text: displayName">TICKET_NAME</span>
            </div>
            <!-- ko if:subName != '' -->
            <span class="book-item-sub-name" data-bind="html: subName">SUB_NAME</span>
            <!-- /ko -->
            <!-- ko if:usualPrice!=''||pax!='' -->
              <span class="booking-item-addendum">
                  <!--ko if:usualPrice!=''-->
                  (Usual price: <span data-bind="text: usualPrice"></span>)
                  <!-- /ko -->
                  <!--ko if:pax!=''-->
                  (For <span data-bind="text: pax"></span> pax)
                  <!-- /ko -->
              </span>
            <!-- /ko -->
          </td>
          <td width="15%"><input type="radio" name="selectedMenu"
                                 data-bind="click: selectMenu"/></td>
        </tr>
        <tr>
          <td class="book-item-data" colspan="3" data-bind="fadeVisible: !collapsed()">
            <!-- ko if:displayDesc != '' -->
            <span class="book-item-desc" data-bind="html: displayDesc">TICKET_DESC</span>
            <!-- /ko -->
            <!-- ko if: link != '' && link != null -->
                    <span class="book-item-link"><a target="_blank"
                                                    data-bind="attr:{'href':link}">Read more</a></span>
            <!-- /ko -->
          </td>
        </tr>
        </tbody>
      </table>
    </table>

    <div style="display: none" data-bind="visible: hasPromotion">
      <table class="ticket-form" cellpadding="0" cellspacing="0" border="0">
        <tr id="promo-header">
          <th width="85%" colspan="2" class="ticket-category">
            Promotions
            <div style="position: relative; margin-left: 5px; display: inline-block;">&nbsp;
              <img style="position:absolute;top:0;margin-top:-15px;" src="${themePath}/img/other/great-deals-en.png"/>
            </div>
          </th>
          <th width="15%">Select.</th>
        </tr>
        <tbody data-bind="if: hasCcPromo">
        <tr>
          <td colspan="3" class="ticket-sub-form">
            <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
              <tr id="cc-header" class="header-clickable" data-bind="click: function(){changeActiveSection('cc')}">
                <th colspan="3">
                  <img src="${themePath}/img/icon-promotion.png"/>Holding one of our partners' credit cards? Click here
                  for great savings!
                  <i data-bind="css:{'icon-expand-alt':activeSection()!='cc','icon-collapse-alt':activeSection()=='cc'}"
                     style="float:right;font-size:1.3em;color:#3ca7ba;"></i>
                </th>
              </tr>
              <tbody data-bind="foreach: ccTickets, fadeVisible: activeSection()=='cc'">
              <tr data-bind="css:{'first-record':$index()==0}">
                <td class="ticket-category" width="85%" colspan="2">
                  <div class="book-item-data">
                    <a href="javascript:void(0)" data-bind="click: toggleCollapse">
                      <i data-bind="css:{'icon-expand-alt':collapsed(),'icon-collapse-alt':!collapsed()}"
                         style="float:left;font-size:1.3em;color:#3ca7ba;margin-right: 5px"></i>
                    </a>
                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                    <!-- ko if:subName != '' -->
                    <span class="book-item-sub-name" data-bind="html: subName">SUB_NAME</span>
                    <!-- /ko -->
                    <!-- ko if:usualPrice!=''||pax!='' -->
                      <span class="booking-item-addendum">
                          <!--ko if:pax!=''-->
                          (For <span data-bind="text: pax"></span> pax)
                          <!-- /ko -->
                      </span>
                    <!-- /ko -->
                  </div>
                </td>
                <td width="15%"><input type="radio" name="selectedMenu"
                                       data-bind="click: selectMenu"/></td>
              </tr>
              <tr>
                <td class="book-item-data" colspan="3" data-bind="fadeVisible: !collapsed()">
                  <!-- ko if:displayDesc != '' -->
                  <span class="book-item-desc" data-bind="html: displayDesc">DISCOUNT_DESC</span>
                  <!-- /ko -->
                  <!-- ko if: link != '' && link != null -->
                  <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                  <!-- /ko -->
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>

        <tbody data-bind="if: hasJcPromo">
        <tr>
          <td colspan="3" class="ticket-sub-form">
            <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
              <tr id="jc-header">
                <th colspan="3">
                  <img src="${themePath}/img/icon-promotion.png"/>Are you a Faber Licence member? Get special deals!
                  <div style="padding-left: 20px;font-weight: normal;font-size: 0.9em;">Please key in your membership ID
                    and expiry date.
                  </div>
                  <div style="padding-left: 24px; margin-top: 5px;">
                    <span style="font-weight: normal; font-size: 0.9em;">Card No.</span>
                    <input type="text" class="field card-no" placeholder="Faber Licence No."
                           data-bind="value: jcNumber">
                    <span style="font-weight: normal; font-size: 0.9em;">Expiry Date</span>&nbsp;&nbsp;
                    <select class="field card-no" data-bind="value: jcExpiryMon">
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select> /
                    <select class="field card-no" data-bind="value: jcExpiryYr">
                      <option value="2013">2013</option>
                      <option value="2014">2014</option>
                      <option value="2015">2015</option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                      <option value="2018">2018</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                    </select>
                    <button class="mflg-cyan" style="font-size:1em;width: 100px;" data-bind="click: checkJc">Apply
                    </button>
                  </div>
                </th>
              </tr>
              <tbody data-bind="foreach: jcTickets, fadeVisible: activeSection()=='jc'">
              <tr data-bind="css:{'first-record':$index()==0}">
                <td class="ticket-category" width="85%" colspan="2">
                  <div class="book-item-data">
                    <a href="javascript:void(0)" data-bind="click: toggleCollapse">
                      <i data-bind="css:{'icon-expand-alt':collapsed(),'icon-collapse-alt':!collapsed()}"
                         style="float:left;font-size:1.3em;color:#3ca7ba;margin-right: 5px"></i>
                    </a>
                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                    <!-- ko if:subName != '' -->
                    <span class="book-item-sub-name" data-bind="html: subName">SUB_NAME</span>
                    <!-- /ko -->
                    <!-- ko if:usualPrice!=''||pax!='' -->
                            <span class="booking-item-addendum">
                                <!--ko if:pax!=''-->
                                (For <span data-bind="text: pax"></span> pax)
                                <!-- /ko -->
                            </span>
                    <!-- /ko -->
                  </div>
                </td>
                <td width="15%"><input type="radio" name="selectedMenu"
                                       data-bind="click: selectMenu"/></td>
              </tr>
              <tr>
                <td class="book-item-data" colspan="3" data-bind="fadeVisible: !collapsed()">
                  <!-- ko if:displayDesc != '' -->
                  <span class="book-item-desc" data-bind="html: displayDesc">DISCOUNT_DESC</span>
                  <!-- /ko -->
                  <!-- ko if: link != '' && link != null -->
                  <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                  <!-- /ko -->
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>


        <tbody id="promo-code-tbody" data-bind="if: hasPcPromo">
        <tr>
          <td colspan="3" class="ticket-sub-form">
            <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
              <tr id="pc-header">
                <th colspan="3">
                  <img src="${themePath}/img/icon-promotion.png"/>Have a promo code? Enter it here for sweet
                  discounts!<br/>

                  <div style="padding-left: 24px; margin-top: 5px;">
                    <span style="font-weight: normal; font-size: 0.9em;">Promo Code</span>
                    <input id="promo-code-bottom" type="text" maxlength="100" class="field card-no"
                           placeholder="Promo Code" data-bind="value: pcValue">
                    <button class="mflg-cyan" style="font-size:1em;width: 100px;" data-bind="click: checkPc">Apply
                    </button>
                  </div>
                </th>
              </tr>
              <tbody data-bind="foreach: pcTickets, fadeVisible: activeSection()=='pc'">
              <tr data-bind="css:{'first-record':$index()==0}">
                <td class="ticket-category" width="85%" colspan="2">
                  <div class="book-item-data">
                    <a href="javascript:void(0)" data-bind="click: toggleCollapse">
                      <i data-bind="css:{'icon-expand-alt':collapsed(),'icon-collapse-alt':!collapsed()}"
                         style="float:left;font-size:1.3em;color:#3ca7ba;margin-right: 5px"></i>
                    </a>
                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                    <!-- ko if:subName != '' -->
                    <span class="book-item-sub-name" data-bind="html: subName">SUB_NAME</span>
                    <!-- /ko -->
                    <!-- ko if:usualPrice!=''||pax!='' -->
                            <span class="booking-item-addendum">
                                <!--ko if:pax!=''-->
                                (For <span data-bind="text: pax"></span> pax)
                                <!-- /ko -->
                            </span>
                    <!-- /ko -->
                  </div>
                </td>
                <td width="15%"><input type="radio" name="selectedMenu"
                                       data-bind="click: selectMenu"/></td>
              </tr>
              <tr>
                <td class="book-item-data" colspan="3" data-bind="fadeVisible: !collapsed()">
                  <!-- ko if:displayDesc != '' -->
                  <span class="book-item-desc" data-bind="html: displayDesc">DISCOUNT_DESC</span>
                  <!-- /ko -->
                  <!-- ko if: link != '' && link != null -->
                  <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                  <!-- /ko -->
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>

      </table>
    </div>

    <div data-bind="if: hasMainCourses">
      <div class="item-step">Select Main Courses</div>
      <table class="ticket-form" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="70%" class="ticket-category">Main Course Selections</th>
          <th width="15%">&nbsp;</th>
          <th width="15%">Qty.</th>
        </tr>
        <tr>
          <td colspan="3" class="main-course-diners" data-bind="visible: minPax() == maxPax()">
            Please choose a main course for each of the diners. (No. of diners: <span data-bind="text: minPax">0</span>)
          </td>
          <td colspan="3" class="main-course-diners" data-bind="visible: minPax() != maxPax()">
            Please choose a main course for each of the diners. (No. of diners: <span data-bind="text: minPax">0</span>
            to <span data-bind="text: maxPax">0</span>)
          </td>
        </tr>
        <tbody data-bind="foreach: mainCourses">
        <tr>
          <td class="ticket-category" data-bind="text: name">MAIN_COURSE_DISPLAY</td>
          <td>&nbsp;</td>
          <td>
            <select class="field" data-bind="options: $parent.mainCourseQtyOptions, value: qty"></select>
          </td>
        </tr>
        </tbody>
      </table>
      <div class="ticket-total">
        Sub Total:<span class="total-price" data-bind="text: subTotalAmount">TOTAL_AMOUNT</span><br>
        Service Charge(<span data-bind="text: serviceCharge"></span>%):<span class="total-price"
                                                                             data-bind="text: serviceChargeAmount">TOTAL_AMOUNT</span><br>
        GST(<span data-bind="text: gst"></span>%):<span class="total-price"
                                                        data-bind="text:gstAmount">TOTAL_AMOUNT</span><br>
      [#--Additional Items:<span class="total-price" data-bind="text: topUpAmount">TOTAL_AMOUNT</span>--]
      </div>
      <div class="ticket-total">Total (SGD):<span class="total-price" data-bind="text: totalAmount">TOTAL_AMOUNT</span>
      </div>
    </div>
    <div class="btn-field" style="margin-top: 10px">
      <div class="submit-btn">
        <a class="back" href="javascript:void(0)" data-bind="click: previousStep">Back</a>
        <a class="addToCart" href="javascript:void(0)" data-bind="click: nextStep">Next</a>
      </div>
    </div>
  </div>

  <div data-bind="visible: showTopUpSelection">
    <div data-bind="visible: hasTopup">
      <div class="item-step">Add top ups</div>
      <table class="ticket-form" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="70%" class="ticket-category">Top up</th>
          <th width="15%">Price (SGD)</th>
          <th width="15%">Qty.</th>
        </tr>
        <tbody data-bind="foreach: topups">
        <tr data-bind="css:{'first-record':$index()==0}">
          <td class="ticket-category">
            <div class="topup-data">
              <!-- ko if:displayImagePath != '' && displayImagePath != null -->
              <img class="sky-dining-topup-img" data-bind="attr: {'src':displayImagePath}"/>
              <!-- /ko -->
              <span class="topup-name" data-bind="text: displayName">TOPUP_DISPLAY</span>
              <!-- ko if:displayDesc != '' -->
              <span class="topup-desc" data-bind="text: displayDesc">TOPUP_DESC</span>
              <!-- /ko -->
              <!-- ko if: link != '' && link != null -->
              <span class="topup-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
              <!-- /ko -->
            </div>
          </td>
          <td data-bind="text: displayPrice">TOPUP_PRICE</td>
          <td>
            <select class="field" data-bind="value: qty">
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
            </select>
          </td>
        </tr>
        </tbody>
      </table>
    </div>

    <div class="item-step">Specify other details</div>

    <table class="ticket-form" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <th class="ticket-category">Do you have any special requirements?</th>
      </tr>
      <tr>
        <td colspan="3" class="main-course-diners">
          If you have any special requests, please let us know in advance and we will try our best to serve you.
        </td>
      </tr>
      <tbody>
      <tr>
        <td class="ticket-category" style="text-align: center;">
          <textarea data-bind="value: remarks" class="field" style="width: 90%"></textarea>
        </td>
      </tr>
      </tbody>
    </table>

    <div class="ticket-total">
      Sub Total:<span class="total-price" data-bind="text: subTotalAmount">TOTAL_AMOUNT</span><br>
      Service Charge(<span data-bind="text: serviceCharge"></span>%):<span class="total-price"
                                                                           data-bind="text: serviceChargeAmount">TOTAL_AMOUNT</span><br>
      GST(<span data-bind="text: gst"></span>%):<span class="total-price"
                                                      data-bind="text: gstAmount">TOTAL_AMOUNT</span><br>
      Additional Items:<span class="total-price" data-bind="text: topUpAmount">TOTAL_AMOUNT</span>
    </div>
    <div class="ticket-total">Total (SGD):<span class="total-price" data-bind="text: totalAmount">TOTAL_AMOUNT</span>
    </div>
    <div class="btn-field">
      <div class="submit-btn">
        <a class="back" href="javascript:void(0)" data-bind="click: previousStep">Back</a>
        <a class="addToCart" href="javascript:void(0)" data-bind="click: addToCart">Checkout</a>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="item-notes">
    <div data-bind="if: hasNotes">
      <h6>Notes:</h6>

      <div class="notes" data-bind="html: notes">
      </div>
      <span style="display: block; height: 10px;">&nbsp;</span>
    </div>
    <h6>Terms &amp; Conditions:</h6>
    <ul class="notes">
      <li><a style="color:#099DD5" target="_blank" href="${sitePath + "/tnc-sky-dining~" + packageName + "~.html"}">Click
        here to view.</a></li>
    </ul>

  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>