[#include "/b2c-mflg/templates/macros/pageInit.ftl"]
[#assign params = ctx.aggregationState.getSelectors()]
[#assign generalTncs = starfn.getGeneralTncContent("b2c-mflg")]
[#if params[0]?has_content]
    [#assign prodId = params[0]]
    [#assign productTnc = sdfn.getPackageTncContent(prodId)!]
[/#if]
[#assign hasTnc = generalTncs?has_content || productTnc?has_content]


<div class="container contain">
  <div class="single-col-content">
    <div class="static-heading"><h2>Terms &amp; Conditions</h2></div>
  [#if hasTnc]
      [#if generalTncs?has_content]
          [#list generalTncs as generalTnc]
            <h3>
            ${generalTnc.title?html!""}
            </h3>

            <div class="the-tnc">
            ${generalTnc.content!""}
            </div>
          [/#list]
      [/#if]
      [#if productTnc?has_content]
        <h3>
        ${productTnc.title?html!""}
        </h3>

        <div class="the-tnc">
        ${productTnc.tncContent!""}
        </div>
      [/#if]
  [#else]
    <div style="text-align:center;font-size:3em;margin-top:100px;font-style:italic">
      No Terms &amp; Conditions available.
    </div>
  [/#if]
    <div style="margin:20px 0">&nbsp;</div>
    <div class="clearfix"></div>
  </div>
</div>