<div class="container contain">
    <div class="single-col-content">
        <div class="static-contact-us">
            <h1>Contact Us</h1>
            <h3>For enquiries, please contact:</h3>
            <p>Tel: (+65) 6377 9688</p>
            <p>Email: guestrelations@mountfaber.com.sg</p>
            <h3>For corporate sales/bulk purchases, please contact:</h3>
            <p>Email: enquiry@mountfaber.com.sg</p>
            <h3>Address:</h3>
            <p>
                Mount Faber Leisure Group<br />
                109 Mount Faber Road<br />
                Faber Peak<br />
                Singapore 099203

            </p>
        </div>

        <div class="clearfix" style="margin-top: 30px;"></div>
    </div>
</div>