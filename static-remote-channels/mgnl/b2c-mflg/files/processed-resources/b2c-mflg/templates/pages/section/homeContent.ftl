<div class="container contain">

    <div class="leftside_bar">
    [@cms.area name="chooseTickets"/]
    </div>

    <div class="rightside_bar">
    [@cms.area name="whatsOn"/]
    </div>
    <div class="clearfix"></div>

    [@cms.area name="highlights"/]

</div>