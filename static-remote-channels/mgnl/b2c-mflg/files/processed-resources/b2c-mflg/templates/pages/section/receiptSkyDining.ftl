[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<div class="container contain" id="receiptPage">

    <div class="single-col-content" data-bind="visible: !isForReceipt() & !${cmsfn.isEditMode()?c}">
        <div class="main-error">
            <div class="main-error-msg">You may have refreshed the page or pressed back while in the middle of a transaction.
                Your transaction has been invalidated. Please checkout again. <a href="${sitePath}"
                                                                                 style="text-decoration: underline">Let's take
                    you back to the home page.</a></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="basic-content" data-bind="visible: isForReceipt() | ${cmsfn.isEditMode()?c}">
        <div class="checkout">
            <h2>Order Successful</h2>
        </div>

        <p class="order-message">
            <span>Your order has been completed. An email will be sent to you soon with your receipt.</span>
        </p>

    [#--<p class="order-print">--]
    [#--<a href="/.store/store/generate-ticket/b2c-mflg"><i class="icon-print"></i> Download tickets.</a>--]
    [#--</p>--]

        <p class="order-print">
            <a href="javascript:window.print();"><i class="icon-print"></i> Print this page.</a>
        </p>

        <div class="receipt-wrapper">
            <div class="show-logos"><img src="${themePath}/img/logo/receipt-logo-${imgSuffix}.png"/></div>
            <div class="receipt-header">
                <div class="receipt-part">
                    <span class="receipt-title">Receipt Number</span>
                    <span class="receipt-number" data-bind="text: receiptNumber"></span>
                </div>
            [#--<div class="pin-part">--]
            [#--<span class="pin-title">PIN Code</span>--]
            [#--<span class="pincode">123456</span>--]
            [#--</div>--]
            [#--<div class="barcode">--]
            [#--<img title="Barcode" alt="barcode" src="" />--]
            [#--</div>--]
            </div>

            <div class="receipt-intro" data-bind="with: customer">
                <p>Dear <span data-bind="text: salutation"></span> <span data-bind="text: name"></span>,</p>

                <p>Thank you for your booking! For instructions on how to redeem, please refer to the redemption mechanics
                    stated in the Terms &amp; Conditions at the bottom of the page.</p>

                <p>Your order details are summarized below:</p>
            </div>

            <div class="receipt-deets">
        <span class="deets-label">Date of Purchase: <span class="deets-value"
                                                          data-bind="text: dateOfPurchase"></span></span>
                <span class="deets-label">Payment Type: <span class="deets-value" data-bind="text: paymentType"></span></span>

                <div data-bind="with: customer">
                    <span class="deets-label">Email: <span class="deets-value" data-bind="text: email"></span></span>
          <span class="deets-label">Name: <span class="deets-value" data-bind="text: salutation"></span> <span
                  class="deets-value" data-bind="text: name"></span></span>
                    <span class="deets-label">Company Name: <span class="deets-value" data-bind="text: companyName"></span></span>
                    <span class="deets-label">Mobile: <span class="deets-value" data-bind="text: mobile"></span></span>
                </div>
                <span class="deets-label">Last 4 Digits of Credit Card: <span class="deets-value">1234</span></span>
            </div>

            <!-- ko foreach: {data: cmsProducts, as: 'cmsProduct'} -->
            <table class="checkout-summary">
                <colgroup>
                    <col span="1" style="width: 37%"/>
                    <col span="1" style="width: 36%"/>
                    <col span="1" style="width: 10%"/>
                    <col span="1" style="width: 5%"/>
                    <col span="1" style="width: 13%"/>
                </colgroup>
                <tr class="row-title">
                    <th colspan="5">
                        <span data-bind="text: cmsProduct.name"></span>
                    </th>
                </tr>
                <tr class="row-header">
                    <th class="left_row">Item</th>
                    <th class="left_row">Details</th>
                    <th>Unit Price</th>
                    <th>Qty.</th>
                    <th>Amount</th>
                </tr>
                <tbody class="section-main" data-bind="foreach: {data: cmsProduct.items, as: 'cmsItem'}">
                <tr class="row-main" data-bind="ifnot: cmsItem.isTopup">
                    <td class="cell-item">
                      <span data-bind="text: cmsItem.name"></span>
                      <!-- ko if: cmsItem.hasDiscount -->
                      <br><span style="font-style: italic;" data-bind="text: cmsItem.discountName"></span>
                      <!-- /ko -->
                    </td>
                    <td class="cell-desc">
                      <div class="cell-desc-div" data-bind="html: cmsItem.description"></div>
                        <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                            <span data-bind="text: cmsItem.discountLabel"></span>
                        </div>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: cmsItem.priceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: qty"></span>
                    </td>
                    <td class="cell-sub">
                        <div data-bind="visible: !cmsItem.hasDiscount()">
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div data-bind="visible: cmsItem.hasDiscount">
                            <div>
                                <span data-bind="text: cmsItem.totalText"></span>
                            </div>
                            <div>
                                <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="row-main row-topup" data-bind="if: cmsItem.isTopup">
                    <td class="cell-item">
                        <i class="icon-plus-sign"></i>
                        <span data-bind="text: cmsItem.name"></span>
                    </td>
                    <td class="cell-desc">
                        <div class="cell-desc-div">
                            <span data-bind="text: cmsItem.description"></span>
                        </div>
                        <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                            <span data-bind="text: cmsItem.discountLabel"></span>
                        </div>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: cmsItem.priceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: qty"></span>
                    </td>
                    <td class="cell-sub">
                        <div data-bind="visible: !cmsItem.hasDiscount()">
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div data-bind="visible: cmsItem.hasDiscount">
                            <div>
                                <span data-bind="text: cmsItem.totalText"></span>
                            </div>
                            <div>
                                <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="row-main row-topup" data-bind="if: cmsItem.hasMainCourse">
                    <td class="cell-item"><i class="icon-food"></i>&nbsp;Main Course Selections</td>
                    <td class="cell-desc">
                        <div class="cell-desc-div" data-bind="html: cmsItem.mainCourseText"></div>
                    </td>
                    <td class="cell-unit">&nbsp;</td>
                    <td class="cell-qty">&nbsp;</td>
                    <td class="cell-sub">&nbsp;</td>
                </tr>
                <tr class="row-main row-topup" data-bind="if: cmsItem.hasRemarks">
                    <td class="cell-item">Special Requests</td>
                    <td class="cell-desc" colspan="4">
                        <div class="cell-desc-div" data-bind="html: cmsItem.remarks"></div>
                    </td>
                </tr>
                </tbody>
                <tr class="row-subtotal">
                  <td colspan="4" class="cell-subtotal-lbl">Sub Total:
                    <br/>Service Charge(<span data-bind="text: cmsProduct.serviceCharge"></span>%):
                    <br/>GST(<span data-bind="text: cmsProduct.gst"></span>%):
                    <br/>Additional Items:
                  </td>
                  <td class="cell-subtotal-amt"><span data-bind="text: cmsProduct.subtotalText"></span>
                    <br/><span data-bind="text: cmsProduct.serviceChargeTotalText"></span>
                    <br/><span data-bind="text: cmsProduct.gstTotalText"></span>
                    <br/><span data-bind="text: cmsProduct.topUpPriceText"></span>
                  </td>
                </tr>
            </table>
            <!-- /ko -->

            <table class="checkout-summary" data-bind="if: hasBookingFee">
                <colgroup>
                    <col span="1" style="width: 67%"/>
                    <col span="1" style="width: 10%"/>
                    <col span="1" style="width: 5%"/>
                    <col span="1" style="width: 13%"/>
                </colgroup>
                <tr class="row-title">
                    <th colspan="4">Booking Fee</th>
                </tr>
                <tr class="row-header">
                    <th class="left_row">Booking Fee</th>
                    <th>Unit Price</th>
                    <th>Qty.</th>
                    <th>Amount</th>
                </tr>
                <tbody class="section-main">
                <tr class="row-main">
                    <td class="cell-item cell-book-fee">
                        <span data-bind="text: bookFeeMode"></span>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: bookPriceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: bookFeeQty"></span>
                    </td>
                    <td class="cell-sub">
                        <span data-bind="text: bookFeeSubTotalText"></span>
                    </td>
                </tr>
                </tbody>
                <tr class="row-subtotal">
                    <td class="cell-subtotal-waive">&nbsp;</td>
                    <td colspan="2" class="cell-subtotal-lbl">Subtotal:</td>
                    <td class="cell-subtotal-amt">
                        <span data-bind="text: bookFeeSubTotalText"></span>
                    </td>
                </tr>
            </table>

            <div class="grand-total">
        <span class="grand-total-text">Grand Total:<span class="grand-total-amt" id="grandTotal"
                                                         data-bind="text: totalText"></span></span>
            </div>

            <div style="margin: 5px 10px; padding: 5px; color: #666666; font-size: 11px">
                <h3 style="margin: 5px 0">Notes</h3>
                <h4>Please note that all completed and confirmed transactions cannot be cancelled, refunded or amended under any
                    circumstances.</h4>
                <h4>For a proxy to collect on your behalf, he/she needs to present:</h4>
                <ol>
                    <li>The printed confirmation letter</li>
                    <li>A <a href="/doc/mflg-letter-of-authorisation.pdf" target="_blank">letter of authorisation</a> signed by
                        you (the purchaser)
                    </li>
                    <li>His/her original NRIC/Passport/FIN Card</li>
                </ol>
                <h4>For general enquiries, please email us at guestrelations@mountfaber.com.sg or call (65) 6377 9688.</h4>
                <h4>For corporate enquiries, please email us at enquiry@mountfaber.com.sg or call (65) 6377 9648/9.</h4>
                <h4>We reserve the right to disallow redemption if required documents are inadequately furnished.</h4>
            </div>

        </div>
    </div>

    [@cms.area name="adBanner"/]

    <div class="clearfix"></div>

</div>