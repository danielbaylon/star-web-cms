[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<div class="news_heading">What's On</div>
<div class="news_list">
[#assign systemAnnouncements = starfn.getAnnouncements("b2c-mflg")]
[#if systemAnnouncements?has_content]
  <ul>
    [#list systemAnnouncements as sa]
      <li class="newsbite">
        <h2 class="heading">${sa.title?html}</h2>
        [#if sa.image?has_content]
          [#assign imageAssest = damfn.getAsset(sa.image)]
          [#if imageAssest?has_content]
            <a class="promotion_img"><img src="${imageAssest.getLink()!}" alt="${imageAssest.getCaption()!}"/></a>
          [/#if]
        [/#if]
        <p class="description">${sa.content!}</p>
        [#if sa.relatedLink?has_content]
          <a href="${sa.relatedLink}" target="_blank" class="more-info">More<br/>Info</a>
        [/#if]
      </li>
    [/#list]
  </ul>
[/#if]
[#if content.announcements?has_content]
  [#assign announceIndex = 0]
  [#assign announceImageIndex = 0]
  [#assign announcements = cmsfn.children(cmsfn.contentByPath(content.@handle + "/announcements"))]
    <ul>
      [#list announcements as announcement]
        [#if announcement?has_content]
          [#assign isCorrectLang = false]
          [#if announcement?index_of(lang,0) > -1]
            [#assign isCorrectLang = true]
          [#elseif lang = "en" && announcement?index_of("_",0) == -1]
            [#assign isCorrectLang = true]
          [/#if]
          [#if isCorrectLang == true]

            [#assign rendition = ""]
            [#if announcements[announceIndex].image?has_content]
              [#assign rendition = damfn.getRendition(announcements[announceIndex].image, "original")]
            [/#if]
            [#assign assetTitle = announceImageIndex + 1]
            [#if rendition != ""]
              [#if rendition.asset?? && rendition.asset.title?has_content]
                [#assign assetTitle = rendition.asset.title]
              [/#if]
            [/#if]

            [#assign imageAlt = announcements[announceIndex].title!assetTitle!]
            [#assign imageTitle = announcements[announceIndex].title!assetTitle!]

            [#assign imageLink = ""]
            [#if rendition != ""]
              [#assign imageLink = rendition.link]
            [/#if]
            [#assign imageHyperlink = announcements[announceIndex].url!"javascript:void(0);"]

            [#assign imageContent = announcements[announceIndex].content!""]
              <li class="newsbite">
                  <h2 class="heading">${imageTitle}</h2>
                [#if imageLink != ""]
                    <a class="promotion_img"><img src="${imageLink}" alt="${imageAlt}" /></a>
                [/#if]
                  <p class="description">${imageContent}</p>
                [#if imageHyperlink != "javascript:void(0);"]
                    <a href="${imageHyperlink}" target="_blank" class="more-info">More<br/>Info</a>
                [/#if]
              </li>
            [#assign announceImageIndex = announceImageIndex + 1]
          [/#if]
        [/#if]
        [#assign announceIndex = announceIndex + 1]
      [/#list]
    </ul>

[#else]
    <span>Check back for promotions.</span>
[/#if]
    <div class="clearfix"></div>
</div>