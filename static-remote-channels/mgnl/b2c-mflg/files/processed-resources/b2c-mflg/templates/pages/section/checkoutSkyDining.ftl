[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<div class="container contain" id="checkoutPage">
    <div class="booking-process-step2"></div>
    <div class="clearfix"></div>

    <div class="basic-content-msg" data-bind="visible: message, html: message"></div>

    <div class="basic-content" data-bind="visible: cmsProducts().length < 1">
        <div class="albuquerque cart-empty-splash">
            <div class="maint-title">Your shopping cart is empty.</div>
            <div class="maint-text">How about checking out our <a href="${sitePath}">catalog</a>?</div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="basic-content" data-bind="visible: cmsProducts().length > 0">
        <div class="checkout">
            <h2>Checkout</h2>
            <a href="${sitePath}" class="btn-mflg-green btn-continue">Continue Browsing</a>
        </div>

    [#--<a href="javascript:void(0)" title="Clear Cart" class="clear-cart" id="btnClearCart">--]
    [#--<i class="icon-trash icon-large"></i> Clear Cart--]
    [#--</a>--]

        <!-- ko foreach: {data: cmsProducts, as: 'cmsProduct'} -->
        <table class="checkout-summary">
            <colgroup>
                <col span="1" style="width: 35%" />
                <col span="1" style="width: 37%" />
                <col span="1" style="width: 10%" />
                <col span="1" style="width: 5%" />
                <col span="1" style="width: 13%" />
            [#--<col span="1" style="width: 5%" />--]
            </colgroup>
            <tr class="row-title">
                <th colspan="6">
                    <span data-bind="text: cmsProduct.name"></span>
                </th>
            </tr>
            <tr class="row-header">
                <th class="left_row">Item</th>
                <th class="left_row">Details</th>
                <th>Unit Price</th>
                <th>Qty.</th>
                <th>Amount</th>
            [#--<th>&nbsp;</th>--]
            </tr>
            <tbody class="section-main" data-bind="foreach: {data: cmsProduct.items, as: 'cmsItem'}">
            <tr class="row-main" data-bind="ifnot: cmsItem.isTopup">
                <td class="cell-item">
                  <span data-bind="text: cmsItem.name"></span>
                  <!-- ko if: cmsItem.hasDiscount -->
                  <br><span style="font-style: italic;" data-bind="text: cmsItem.discountName"></span>
                  <!-- /ko -->
                </td>
                <td class="cell-desc">
                    <div class="cell-desc-div">
                        <span data-bind="html: cmsItem.description"></span>
                    </div>
                    <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                        <span data-bind="text: cmsItem.discountLabel"></span>
                    </div>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: cmsItem.priceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: qty"></span>
                </td>
              `
                <td class="cell-sub">
                    <div data-bind="visible: !cmsItem.hasDiscount()">
                        <span data-bind="text: cmsItem.totalText"></span>
                    </div>
                    <div data-bind="visible: cmsItem.hasDiscount">
                        <div>
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div>
                            <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                        </div>
                    </div>
                </td>
            [#--<td class="cell-remove">--]
            [#--<a href="#" title="Remove" data-bind="click: removeItem"><i class="icon-remove icon-large"></i></a>--]
            [#--</td>--]
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.isTopup">
                <td class="cell-item">
                    <i class="icon-plus-sign"></i>
                    <span data-bind="text: cmsItem.name"></span>
                </td>
                <td class="cell-desc">
                    <div class="cell-desc-div">
                        <span data-bind="text: cmsItem.description"></span>
                    </div>
                    <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                        <span data-bind="text: cmsItem.discountLabel"></span>
                    </div>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: cmsItem.priceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: qty"></span>
                </td>
                <td class="cell-sub">
                    <div data-bind="visible: !cmsItem.hasDiscount()">
                        <span data-bind="text: cmsItem.totalText"></span>
                    </div>
                    <div data-bind="visible: cmsItem.hasDiscount">
                        <div>
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div>
                            <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                        </div>
                    </div>
                </td>
            [#--<td class="cell-remove">--]
            [#--<a href="javascript:mflg.removeItem('')" title="Remove"><i class="icon-remove icon-large"></i></a>--]
            [#--</td>--]
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.hasMainCourse">
                <td class="cell-item"><i class="icon-food"></i>&nbsp;Main Course Selections</td>
                <td class="cell-desc"><div class="cell-desc-div" data-bind="html: cmsItem.mainCourseText"></div></td>
                <td class="cell-unit">&nbsp;</td>
                <td class="cell-qty">&nbsp;</td>
                <td class="cell-sub">&nbsp;</td>
            </tr>
            <tr class="row-main row-topup" data-bind="if: cmsItem.hasRemarks">
                <td class="cell-item">Special Requests</td>
                <td class="cell-desc" colspan="4"><div class="cell-desc-div" data-bind="html: cmsItem.remarks"></div></td>
            </tr>
            </tbody>
            <tr class="row-subtotal">
              <td colspan="4" class="cell-subtotal-lbl">Sub Total:
                <br/>Service Charge(<span data-bind="text: cmsProduct.serviceCharge"></span>%):
                <br/>GST(<span data-bind="text: cmsProduct.gst"></span>%):
                <br/>Additional Items:
              </td>
              <td class="cell-subtotal-amt"><span data-bind="text: cmsProduct.subtotalText"></span>
                <br/><span data-bind="text: cmsProduct.serviceChargeTotalText"></span>
                <br/><span data-bind="text: cmsProduct.gstTotalText"></span>
                <br/><span data-bind="text: cmsProduct.topUpPriceText"></span>
              </td>
            </tr>

        </table>
        <!-- /ko -->

        <table class="checkout-summary" data-bind="if: hasBookingFee">
            <colgroup>
                <col span="1" style="width: 67%" />
                <col span="1" style="width: 10%" />
                <col span="1" style="width: 5%" />
                <col span="1" style="width: 13%" />
                <col span="1" style="width: 5%" />
            </colgroup>
            <tr class="row-title"><th colspan="5">Booking Fee</th></tr>
            <tr class="row-header">
                <th class="left_row">Booking Fee</th>
                <th>Unit Price</th>
                <th>Qty.</th>
                <th>Amount</th>
                <th>&nbsp;</th>
            </tr>
            <tbody class="section-main">
            <tr class="row-main">
                <td class="cell-item cell-book-fee">
                    <span data-bind="text: bookFeeMode"></span>
                </td>
                <td class="cell-unit">
                    <span data-bind="text: bookPriceText"></span>
                </td>
                <td class="cell-qty">
                    <span data-bind="text: bookFeeQty"></span>
                </td>
                <td class="cell-sub">
                    <span data-bind="text: bookFeeSubTotalText"></span>
                </td>
                <td class="">&nbsp;</td>
            </tr>
            </tbody>
            <tr class="row-subtotal">
                <td class="cell-subtotal-waive" data-bind="if: bookFeeWaiverCode">
                    <span id="waiveControls">
	                    <span style="display:block; float: left; margin: 0 10px 0 0;">
	                        <span style="display: block;">Enter Merchant Code</span>
	                        <span style="display: block;font-weight: normal; font-size: 0.85em; text-align: right;">(if applicable)</span>
	                    </span>
	                    <span style="display:block; float: left;">
	                        <input type="text" maxlength="100" class="field field-xs" id="fldWaive" />
	                        <button class="mflg-cyan" style="font-size: 0.85em; width: 80px; padding: 5px 0" id="btnWaive">Apply</button>
	                    </span>
                    </span>
                    <span id="msgWaive"></span>
                </td>
                <td colspan="2" class="cell-subtotal-lbl">Subtotal:</td>
                <td class="cell-subtotal-amt" id="bookFeeSubtotal">
                    <span data-bind="text: bookFeeSubTotalText"></span>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>

        <div id="waivedTotal" style="display:none;">S$ <span data-bind="text: waivedTotalText"></span></div>

        <div class="grand-total">
            [#--<div style="text-align: left">--]
                [#--<span>Have a promo code?&nbsp;</span>--]
                [#--<input type="text" class="field" data-bind="value: promoCode" placeholder="Promo Code"/>--]
                [#--<button class="mflg-cyan" style="width: auto; font-size: 14px; padding-left: 2px; padding-right: 2px" data-bind="click: applyPromoCode">Apply</button>--]
            [#--</div>--]
            <div>
              <span class="grand-total-text">Grand Total:<span class="grand-total-amt" id="grandTotal"><span
                data-bind="text: totalText"></span></span></span>
            </div>
        </div>

        <form id="checkoutForm" data-bind="with: customer">
            <input type="hidden" value="" name="bfw" id="bfw" />
            <div class="checkout-details">
                <h3><i class="icon-pencil"></i>Checkout Information</h3>
                <div class="checkout-form">
                    <div class="display-msg msg-error" style="display:none;" data-bind="visible: isErr, html: errMsg"></div>
                    <div class="form-field">
                        <span class="field-title">Email <span class="required-ind">*</span></span>
                        <input type="text" class="field" name="cust.email" data-bind="value: email"/>
                    </div>
                    <div class="form-field">
                        <span class="field-title">Name <span class="required-ind">*</span></span>
                        <select class="field" style="width: 65px;" data-bind="value: salutation" name="cust.salutation">
                            <option value="">&nbsp;</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Ms.">Ms.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Dr.">Dr.</option>
                        </select>
                        <input type="text" class="field" name="cust.name" data-bind="value: name"/>
                    </div>
                    <div class="form-field">
                        <span class="field-title">Confirm Email <span class="required-ind">*</span></span>
                        <input type="text" class="field" data-bind="value: email2"/>
                    </div>
                    <div class="form-field">
                        <span class="field-title">ID <span class="required-ind">*</span></span>
                    <span class="fieldset-nric">
                        <label><input type="radio" name="cust.idType" value="NricFin" data-bind="checked: idType" /> NRIC^/FIN</label>
                        <label><input type="radio" name="cust.idType" value="Passport" data-bind="checked: idType" /> Passport</label>
                    </span>
                        <input type="text" class="field field-s" name="cust.idNo" data-bind="value: idNo"/>
                        <span class="checkout-note">&nbsp;&nbsp;^For Singaporeans and PRs only</span>
                    </div>
                    <div class="form-field">
                        <span class="field-title">Payment Type <span class="required-ind">*</span></span>
                        <select class="field field-s" data-bind="value: paymentType" name="cust.paymentType">
                            <option value="">&nbsp;</option>
                            <option value="VISA">VISA</option>
                            <option value="Mastercard">Mastercard</option>
                            <option value="China UnionPay">China UnionPay</option>
                        </select>
                    </div>
                    <div class="form-field">
                        <span class="field-title">Company Name</span>
                        <input type="text" class="field" name="cust.companyName" data-bind="value: companyName"/>
                    </div>
                    <div class="form-field">
                    <span class="field-title" style="width: 100%;color: #808080; font-style: italic;">
                    We accept:
                    <img src="${themePath}/img/visa-10032.gif" alt="VISA" height="20" style="border: 1px solid #c2c2c2;border-radius:3px;padding: 5px 5px 5px 2px;background:#ffffff" />
                    <img src="${themePath}/img/mastercard-8050.gif" alt="Mastercard" height="32" style="border: 1px solid #c2c2c2;border-radius:3px;padding: 1px;background:#ffffff" />
                    <img src="${themePath}/img/unionpay.gif" alt="China UnionPay" height="32" style="border: 1px solid #c2c2c2;border-radius:3px;padding: 1px;background:#ffffff" />
                    </span>
                    </div>
                    <div class="form-field">
                      <span class="field-title">Mobile Number <span class="required-ind">*</span></span>
                        <input type="text" class="field" name="cust.mobile" data-bind="value: mobile"/>
                    </div>
                    <!-- <div class="form-field">
                        - DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT -
                        <table width=110 border=0 cellspacing=0 cellpadding=0 title="CLICK TO VERIFY: This site uses a GlobalSign SSL Certificate to secure your personal information." ><tr><td><span id="ss_img_wrapper_gmogs_image_110-45_en_black"><a href="https://www.globalsign.com/" target=_blank title="SSL"><img alt="SSL" border=0 id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gmogs_image_110-45_en_black.png"></a></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_110-45_en_black.js"></script><br /><a href="https://www.globalsign.com/" target=_blank  style="color:#000000; text-decoration:none; display: block;text-indent:-9999px;font:bold 8px arial; margin:0px;padding:0px;">Get SSL Certificates</a></td></tr></table>
                        - DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT -
                    </div> -->
                    <div class="form-field" style="margin: 10px 0 0 0; padding:10px 0 0 0;border-top:1px dotted #bbb; width:100%;">
                        &nbsp;&nbsp;<input type="checkbox" value="true" name="cust.subscribed" data-bind="checked: subscribed"/> I wish to receive the latest promotions and updates from Mount Faber Leisure Group.<br/>
                      &nbsp;&nbsp;<input type="checkbox" data-bind="checked: tnc"/> <span class="required-ind">*</span>
                      I have read and agree to the <a target="_blank" href="${sitePath + "/tnc-sky-dining~-2~.html"}">Terms
                      &amp; Conditions</a>, including as to how my personal data may be collected, used, disclosed and
                      processed, and confirm that all information and details of myself are true, correct and complete.
                      Where I have provided personal data of individuals other than myself, I warrant and represent that
                      I am validly acting on behalf of each of these individuals, and have obtained their individual
                      consents, to disclose their personal data to MFLG and for MFLG to collect, use, disclose and
                      process their personal data for the Purposes and in accordance to the terms stated in MFLG's Data
                      Protection Policy.
                    </div>
                </div>
            </div>
            <div class="button-panel">
                <a href="javascript:void(0)" class="btn-mflg-green btn-checkout" data-bind="click: doCheckout">Checkout</a>
            </div>
        </form>
    </div>
</div>