<div class="container contain">
    <div class="single-col-content">
        <div class="static-heading"><h2>About Mount Faber Leisure Group</h2></div>
        <p class="static-banner"><img src="/resources/b2c-mflg/theme/default/img/about-us.jpg" /></p>
        <div class="brand-introduction">
            <div class="brand-logo"><img src="/resources/b2c-mflg/theme/default/img/logo/mountfaber-logo-big-en.png"></div>
            <div class="brand-history">
                <p>Established in 1974, Mount Faber Leisure Group operates the Singapore Cable Car, which spans Mount Faber Park,
                    the HarbourFront cruise bay and the resort island of Sentosa.</p>

                <p>Its key businesses encompass attraction management, guided tours for Sentosa attractions, coach services,
                    lifestyle merchandise as well as F&amp;B dining on the hill at Mount Faber.</p>

                <p>Mount Faber Leisure Group strives to create happy experiences for all its guests with friendly and thoughtful service
                    from its passionate team of ambassadors. </p>

            </div>
            <div class="clearfix"></div>
            <div style="height: 100px;"></div>
        </div>
    </div>
</div>