[#-------------- ASSIGNMENTS --------------]
[#-- Page's model & definition, based on the rendering hierarchy and not the node hierarchy --]
[#assign urlVersion = "?ver=1.0r001"]
[#assign site = sitefn.site()!]

[#-------------- RENDERING --------------]
<title>${cmsfn.page(content).title!content.windowTitle!content.title!}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="${content.description!""}" />
<meta name="keywords" content="${content.keywords!""}" />
<meta name="author" content="Enovax Pte Ltd." />
<meta name="generator" content="Powered by Enovax - Intuitive Opensource CMS" />
<link rel="shortcut icon" href="${ctx.contextPath}/resources/b2c-mflg/theme/default/img/favicon.ico${urlVersion}"/>

<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script>window.html5 || document.write('<script src="${ctx.contextPath}/.resources/b2c-mflg/theme/js/html5shiv.min.js"><\/script>')</script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

[#assign requestUri = ctx.getRequest().getRequestURI()]
[#assign requestUriArr = "${requestUri}"?split("/")]
[#assign lastUri = requestUriArr[requestUriArr?size-1]!""]
[#assign lastSecUri = requestUriArr[requestUriArr?size-2]!""]

<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-common.css${urlVersion}" rel="stylesheet">
[#if requestUri?index_of("/sky-dining",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-sky-dining.css${urlVersion}" rel="stylesheet">
[#elseif requestUri?index_of("/product",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-booking.css${urlVersion}" rel="stylesheet">
[#elseif requestUri?index_of("/checkout",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-checkout.css${urlVersion}" rel="stylesheet">
[#elseif requestUri?index_of("/confirm",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-confirm.css${urlVersion}" rel="stylesheet">
[#elseif requestUri?index_of("/receipt",0) > -1]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-receipt.css${urlVersion}" rel="stylesheet">
[#else]
<link href="${ctx.contextPath}/resources/b2c-mflg/theme/default/css/styleloader-landing.css${urlVersion}" rel="stylesheet">
[/#if]

<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-lib.js${urlVersion}"></script>
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-common.js${urlVersion}"></script>
[#if requestUri?index_of("/checkout-sky-dining",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-checkout-sd.js${urlVersion}"></script>
[#elseif requestUri?index_of("/confirm-sky-dining",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-confirm-sd.js${urlVersion}"></script>
[#elseif requestUri?index_of("/receipt-sky-dining",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-receipt-sd.js${urlVersion}"></script>
[#elseif requestUri?index_of("/sky-dining",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-sky-dining.js${urlVersion}"></script>
[#elseif requestUri?index_of("/product",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-booking.js${urlVersion}"></script>
[#elseif requestUri?index_of("/checkout",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-checkout.js${urlVersion}"></script>
[#elseif requestUri?index_of("/confirm",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-confirm.js${urlVersion}"></script>
[#elseif requestUri?index_of("/receipt",0) > -1]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-receipt.js${urlVersion}"></script>
[#else]
<script src="${ctx.contextPath}/resources/b2c-mflg/theme/default/js/scriptloader-landing.js${urlVersion}"></script>
[/#if]