<style type="text/css">
    ul.book-steps-info {
        list-style:none;
        float:left;
    }
    ul.book-steps-info li {
        float:left;
        width:154px;
        height:210px;
        margin-right:4px;
        display:inline;
        padding:8px;
        border:1px solid #ccc;
    }
    ul.book-steps-info li h3 {
        font-size:14px;
        font-weight:600;
        padding:0;
        margin:0;
        margin-bottom:12px;
    }
    ul.book-steps-info p {
        text-align:left;
        font-size:12px;
        display:block;
        padding:0;
        margin:0 0 8px 0;
        float:left;
    }
    ul.book-steps-info p i {
        float:left;
        margin-top:3px;
    }
    ul.book-steps-info p span {
        float:left;
        width:138px;
    }
    .main-list li {
        margin:4px 0 0 0;
    }

</style>

<div class="container contain">
    <div class="single-col-content">
        <div class="static-heading"><h2>Frequently Asked Questions</h2></div>

        <div style="text-align:justify;">
            <h3>1. Need tips on how to book online?</h3>
            <p>Follow our 5 simple steps below to complete your online booking.</p>
            <ul class="book-steps-info">
                <li>
                    <h3>Step 1:<br />Booking</h3>
                    <p>
                        <i class="icon-caret-right"></i><span>Choose  the experience you want to enjoy</span></p>
                    <p><i class="icon-caret-right"></i><span>Pick the date of your visit</span></p>
                    <p><i class="icon-caret-right"></i><span>Indicate number of tickets*</span></p>
                    <p>*Opt for promotional tickets if you are eligible.</p>
                </li>
                <li>
                    <h3>Step 2:<br />Checkout/<br />'Continue Browsing'</h3>
                    <p><i class="icon-caret-right"></i><span>Fill in your personal details, check out then proceed to payment</span></p>
                    <p>OR</p>
                    <p><i class="icon-caret-right"></i><span>'Continue Browsing'</span>
                    </p>
                </li>
                <li>
                    <h3>Step 3:<br />Payment</h3>
                    <p>
                        Key in your credit card details and proceed to pay.
                    </p>
                </li>
                <li>
                    <h3>Step 4:<br />Email Receipt</h3>
                    <p>An e-receipt will be sent to your personal email within 24 hours. If you do not receive it, please contact our Guest Relations officers at (+65) 63779688.
                    </p>
                </li>
                <li>
                    <h3>Step 5:<br />Redemption</h3>
                    <p>Redeem your ticket(s) on the day you visit. Refer to your e-receipt for details.</p>
                    <p>Have a great time!</p>
                </li>
            </ul>
            <div class="clearfix"></div>
            <h3>2. Can I amend my booking after I have proceeded to check out and made payment?</h3>
            <p>We are sorry, but changes cannot be facilitated once a booking is confirmed and payment completed. We encourage you to check through your booking details on the ‘Confirmation’ page before you proceed to pay.</p>
            <h3>3. Where do I redeem my purchase?</h3>
            <p>An e-receipt will be sent to your personal email within 24 hours. If you do not receive it, please contact our Guest Relations officers at (+65) 63779688. Your e-receipt will contain information on where to redeem your purchase on the day of your arrival. </p>
            <p>Redemption points:</p>
            <ul class="main-list" style="padding-left: 20px;list-style: disc;">
                <li>Cable car tickets and tours → Singapore Cable Car ticketing counters </li>
                <li>F&amp;B bookings → Restaurants</li>
            </ul>
            <p>Lastly, do remember to bring along your e-receipt and Identification Card for ticket redemption.</p>
            <h3>4. Where can I find more information on the F&amp;B offerings and cable car ride?</h3>
            <p>As this portal is dedicated to assist you in booking your tickets and reservations online, please visit <a href="http://www.faberpeaksingapore.com" target="_blank">www.faberpeaksingapore.com</a> to find out more about cable car joyrides,other dining, events and leisure experiences at Faber Peak Singapore.</p>

            <div class="the-tnc">
                <h3>5. Any other information I need to note for my online booking?</h3>
                <p class="main-para">Please note the following terms and conditions (T&amp;Cs) for your e-purchases. By confirming your booking, you have agreed to the T&amp;Cs.</p>
                <ul class="main-list">
                    <li>Child is defined as 3 to 12 years old. Free admission for children below 3 years old, unless otherwise stated. Senior citizen is defined as 60 years and above. We reserve the right to request for presentation of documents for verification of age.</li>
                    <li>Local Rate for certain packages may be available to Singaporeans, Permanent Residents and holders of Employment Passes, S-Passes, Work Permits or Dependent Passes. Please be ready to present proof of eligibility for Local Rate tickets at point of ticketing and random checks may also be conducted at the point of entry into the attraction. Visitors unable to present proof of eligibility will be required to upgrade to standard full price tickets.</li>
                    <li>Bookings made are non-refundable and not for resale.</li>
                    <li>Please note that all completed and confirmed transactions CANNOT BE CANCELLED, REFUNDED OR ADMENDED under any circumstances.</li>
                    <li>Any resale of tickets/vouchers is strictly prohibited unless authorized in writing by MFLG. MFLG reserves the right to invalidate tickets/vouchers in connection with any fraudulent/unauthorized resale transaction, without refund or other compensation. Tickets/Vouchers only allows for a one (1) - time use only. If it is determined by MFLG that there are multiple copies/usages of the ticket, usage of the ticket will be denied. In the event of any dispute, a final decision shall be made based on our electronic record. </li>
                    <li>Redemption must be made in person at the redemption points specified on your receipt. For verification purpose, please bring along your e-receipt and identification card.</li>
                    <li>Please note the following if you are purchasing promotional tickets:<br/>
                        <p><i class="icon-chevron-right"></i>&nbsp;For credit card promotions, the same credit card must be used for your online payment.<br /><i class="icon-chevron-right"></i>&nbsp;For Faber Licence promotions, the Faber Licence Card must be presented for purpose of verification during redemption.  </p>
                    </li>
                    <li>Should you be unable to make the redemption personally, you may appoint someone to collect on your behalf. The appointed person shall bring along the signed authorisation form (please download from confirmation email), a printed copy of the e-receipt sent to your email and his/her identification card.</li>
                    <li>We reserve the right to disallow redemption if required documents are inadequately furnished.</li>
                    <li>The respective prevailing operating hours, ticketing, admission, payment and programme policies apply.</li>
                    <li>Booking made is valid on the day of visit stipulated only, unless otherwise stated. Any unredeemed bookings may not be carried over to another day.</li>
                    <li>Prices displayed are for one (1) pax only (unless otherwise stated). All prices are inclusive of the prevailing GST.</li>
                    <li>Blackout dates may apply for certain products, promotions or discounts.</li>
                    <li>Any typographical, clerical or other error/omission in price or offer shall be subject to correction without any liability on the part of Mount Faber Leisure Group.</li>
                    <li>The management reserves the right to cancel or amend the product offerings at its discretion, without any refunds or compensation, in the event of unfavourable weather conditions and/or unforeseen circumstances.</li>
                    <li>All contents on this site are correct at the time of booking but subject to change or withdrawal, without prior notice.</li>
                    <li>By accepting admission, you agree to release Mount Faber Leisure Group from all liabilities and responsibilities for any personal injury, loss or damage, which you may incur.</li>
                    <li>Mount Faber Leisure Group reserves the right to make changes at its discretion and amend or add to the T&Cs at any time without prior notice.</li>
                    <li>Usage of Personal Data</li>
                    <ul>
                        <li>In order to process, administer and/or manage your booking with MFLG and/or to achieve one or more of the Purposes (as defined below), MFLG will necessarily need to collect, use, disclose and/or process your personal data or personal information about you. Such personal data includes information set out in this form and any other personal information relating to you that is provided by you or possessed by MFLG, from time to time.</li>
                        <li>MFLG will/may collect, use, disclose and/or process your personal data for one or more of the following purposes:</li>
                        <ol type="a">
                            <li>evaluating and/or processing your booking;</li>
                            <li>administering, dealing with, managing and/or maintaining your booking with MFLG including but not limited to administering and dealing with your booking, sharing your personal data with MFLG Partners to administer and deal with your booking, contacting you through various modes of communication such as via phone/voice call, text message and/or fax message, email and/or postal mail for such purposes, dealing in any matters relating to your booking (including the mailing of correspondence, statements, information, invoices, or notices to you, which could involve disclosure of certain personal data about you to bring about delivery of the same), performing internal administrative, operational and technology tasks for the said purposes;</li>
                            <li>providing and sending you marketing, advertising and/or promotional information and materials relating to your booking as well as products, attractions, events and/or services that MFLG or MFLG Partners may be selling, marketing, offering and/or promoting, on Sentosa island and Faber Peak Singapore, via various modes of communication such as (a) postal mail to your postal address(es) and/or electronic transmission to your email address(es) (based on our records that we have of your postal address(es) and email address(es) or any new or amended postal or email address(es) that you may provide us with from time to time) and (b) via telephone/voice calls, SMS/MMS and/or facsimile to your telephone number(s) (based on our records that we have of your telephone number(s) or any new telephone number(s) that you may provide us with from time to time) with such telephone/voice calls, SMS/MMS and/or facsimile being sent in accordance with Singapore's Personal Data Protection Act;</li>
                            <li>dealing with or facilitating customer service, carrying out your instructions, or dealing with or responding to any enquiries given by you or on your behalf;</li>
                            <li>complying with or as required by any applicable law, governmental or regulatory requirements of any relevant jurisdiction, including meeting the requirements to make disclosure under the requirements of any law binding on MFLG or on MFLG's related corporations or associated companies (such as the Sentosa Development Corporation) and for the purposes of any guidelines issued by regulatory or other authorities, whether in Singapore or elsewhere, with which MFLG or MFLG's related corporations or associated companies is/are expected to comply;</li>
                            <li>producing statistics and research for internal and statutory reporting and/or record-keeping requirements; and</li>
                            <li>conducting research, analysis and development activities (including but not limited to data analytics, surveys, product and service development and/or profiling) to improve MFLG's services or products via various modes of communication such as (a) postal mail to your postal address(es) and/or electronic transmission to your email address(es) (based on our records that we have of your postal address(es) and email address(es) or any new or amended postal or email address(es) that you may provide us with from time to time) and (b) via telephone/voice calls, SMS/MMS and/or facsimile to your telephone number(s) (based on our records that we have of your telephone number(s) or any new telephone number(s) that you may provide us with from time to time) with such telephone/voice calls, SMS/MMS and/or facsimile being sent in accordance with Singapore's Personal Data Protection Act.
                                (collectively the "Purposes").</li>
                        </ol>
                        <li>Your personal data may/will be disclosed by MFLG to any of its third party service providers or agents or MFLG's related corporations' or associated companies' third party service providers or agents, which may be sited within or outside of Singapore, for one or more of the above Purposes, as such third party service providers or agents if engaged by MFLG or by MFLG's related corporations or associated companies, as the case may be, would be processing your personal data for one or more of the Purposes.</li>
                        <li>Where you have provided personal data of individuals other than yourself (such as personal data of your spouse or your family members), you warrant and represent that the personal data of these individuals are legitimate and accurate, that you are valid acting on behalf of and have the authority from each of these individuals, and that you have obtained each of these individuals' consent, to disclose their personal data to MFLG and for MFLG to collect, use, disclose and process their personal data for the Purposes and in accordance to the terms herein.</li>
                    </ul>
            </div>
        </div>

        <div style="margin:20px 0">&nbsp;</div>

        <div class="clearfix"></div>
    </div>
</div>