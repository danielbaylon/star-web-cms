[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

[#assign maintMessage = starfn.getMaintenanceMessage("b2c-mflg")!""]

<div class="container contain">

  <div class="single-col-content">
    <div class="albuquerque">
    [#if maintMessage?has_content]
      <div class="maint-title">${maintMessage.getProperty("name").getString()}</div>
      <div class="maint-text">${maintMessage.getProperty("message").getString()}</div>
    [#else]
      <div class="maint-title">We will be back soon.</div>
      <div class="maint-text">Our system is currently undergoing maintenance. Please check back shortly.</div>
    [/#if]
    </div>
    <div class="clearfix"></div>
  </div>

</div>