[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<script>
    var cmsBookingModel = null;
    [#assign params = ctx.aggregationState.getSelectors()]
    [#if params[2]?has_content]
        [#assign cat = params[2]]
    [/#if]
    [#if params[3]?has_content]
        [#assign prd = params[3]]
    [/#if]
    [#assign rootPath = "/b2c-mflg/"]
    [#assign cmsProductUrl = "javascript:void(0);"]
    [#assign previewMode = ctx.getParameter("preview")!]
    [#assign proceedShowingProduct = true]
    [#if previewMode?has_content && previewMode == "true" && cmsfn.authorInstance && prd?has_content]
        [#assign proceedShowingProduct = true]
    [/#if]
    [#if cat?has_content && prd?has_content]
        [#assign proceedShowingProduct = true]
    [/#if]
    [#if proceedShowingProduct]
        [#assign catUrlPath = ""]
        [#assign catNode = ""]
        [#if cat?has_content]
            [#assign catNode = starfn.getCMSProductCategory("b2c-mflg", cat)!]
        [/#if]
        [#assign cmsProductNode = starfn.getCMSProduct("b2c-mflg", prd)!]
        [#if cmsProductNode?has_content]
            [#assign cmsProductName = ""]
            [#assign cmsProductDesc = ""]
            [#assign cmsProductUrl = "javascript:void(0);"]
            [#assign cmsProductUUID = cmsProductNode.getName()]
            [#if cmsProductNode.hasProperty("name")]
                [#assign cmsProductName = cmsProductNode.getProperty("name").getString()!]
            [/#if]
            [#if cmsProductNode.hasProperty("description")]
                [#assign cmsProductDesc = cmsProductNode.getProperty("description").getString()!]
            [/#if]
            [#if cmsProductNode.hasProperty("notes")]
                [#assign cmsProductNotes = cmsProductNode.getProperty("notes").getString()!]
            [/#if]
            [#if cat?has_content]
                [#if catNode.hasProperty("generatedURLPath")]
                    [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()!]
                [/#if]
                [#if cmsProductNode.hasProperty("generatedURLPath")]
                    [#assign cmsProductUrl = sitePath + "/category/product~" + cmsfn.asContentMap(cmsProductNode).generatedURLPath?lower_case + "~${lang}~${cat}~${cmsProductNode.name}~.html?category=${catUrlPath?lower_case}"]
                [/#if]
            [/#if]

        cmsBookingModel = {
            "bookingModel":{
                "blackoutDates":null,
                "blackoutDatesArr":null,
                "cableCarClosures":null,
                "ccTickets":[

                ],
                "chosenSchedule":0,
                "dateOfVisit":null,
                "descUrl":"",
                "displayDate":false,
                "displayDescription":"",
                "displayImagePath":null,
                "displayTitle": "${cmsProductName?js_string!""}",
                "hasCableCarClosure":false,
                "hasCcPromo":false,
                "hasJcPromo":false,
                "hasMainCourses":false,
                [#if cmsProductNode.hasProperty("notes")]
                    "hasNotes":true,
                [#else]
                    "hasNotes":false,
                [/#if]
                [#assign hasPcPromo = starfn.hasActivePromoCode(cmsProductNode)!false]
                [#if hasPcPromo]
                    "hasPcPromo": true,
                [#else]
                    "hasPcPromo": false,
                [/#if]
                [#if hasPcPromo]
                    "hasPromotion": true,
                [#else]
                    "hasPromotion": false,
                [/#if]
                "hasTopup":false,
                "isActive":true,
                "jcExpiry":null,
                "jcNumber":null,
                "jcTickets":[

                ],
                "mainCourses":null,
                "mainTickets":[
                    [#assign axPrdList = starfn.getAXProductsByCMSProductNode(cmsProductNode)!]
                    [#assign axPrdDetailsMap = starfn.getAXProductsDetailByCMSProductNode(cmsProductNode)!]
                    [#list axPrdList as axPrd]
                        [#assign itemName = ""]
                        [#assign description = ""]
                        [#assign ticketType = ""]
                        [#assign relatedAXProductUUID = ""]
                        [#assign productPriceUnitId = ""]
                        [#assign displayProductNumber = ""]
                        [#assign productPrice = ""]
                        [#assign axPrdDetail = axPrdDetailsMap[axPrd.getProperty("relatedAXProductUUID").getString()!]]
                        [#if axPrdDetail.hasProperty("itemName")]
                            [#assign itemName = axPrdDetail.getProperty("itemName").getString()!]
                        [/#if]
                        [#if axPrdDetail.hasProperty("description")]
                            [#assign description = axPrdDetail.getProperty("description").getString()!]
                        [/#if]
                        [#if axPrdDetail.hasProperty("ticketType")]
                            [#assign ticketType = axPrdDetail.getProperty("ticketType").getString()!]
                        [/#if]
                        [#if axPrdDetail.hasProperty("status")]
                            [#assign status = axPrdDetail.getProperty("status").getString()]
                        [/#if]
                        [#assign inGroup = "false"]
                        [#assign displayGroupName = "true"]
                        [#assign groupSize = "1"]
                        [#if axPrdDetail.hasProperty("inGroup")]
                            [#assign inGroup = axPrdDetail.getProperty("inGroup").getString()!"false"]
                        [/#if]
                        [#if axPrdDetail.hasProperty("displayGroupName")]
                            [#assign displayGroupName = axPrdDetail.getProperty("displayGroupName").getString()!"true"]
                        [/#if]
                        [#if axPrdDetail.hasProperty("groupSize")]
                            [#assign groupSize = axPrdDetail.getProperty("groupSize").getString()!"1"]
                        [/#if]
                        [#if status?lower_case == "active"]
                            [#if axPrdDetail.hasProperty("productListingId")]
                                [#assign productListingId = axPrdDetail.getProperty("productListingId").getString()!]
                            [/#if]
                            [#if axPrdDetail.hasProperty("displayProductNumber")]
                                [#assign displayProductNumber = axPrdDetail.getProperty("displayProductNumber").getString()!]
                            [/#if]
                            [#if axPrdDetail.hasProperty("productPrice")]
                                [#assign productPrice = axPrdDetail.getProperty("productPrice").getString()!]
                            [/#if]
                            {
                                "discountId":null,
                                "discountCode": null,
                                "discountPrice":null,
                              "displayDesc": "${description?js_string!""}",
                                "displayName": "${itemName?js_string!""}",
                                "displayPrice":"${productPrice}",
                                "id":${productListingId},
                                "itemCode":${displayProductNumber},
                                "link":"",
                                "merchantId":null,
                                "originalPrice":null,
                                "priceType":null,
                                "qty":0,
                              "groupSize":${groupSize},
                              "displayGroupName":${displayGroupName},
                              "inGroup":${inGroup},
                                "ticketType":"${ticketType}",
                                "unitPrice":${productPrice},
                                "promotionContent": [
                                    [#assign promotionList = starfn.getAXProductPromotions(axPrdDetail)]
                                    [#list promotionList as promotion]
                                        [#assign promotionIconUUID = promotion.getProperty("promotionIcon").getString()!]
                                        [#assign promotionIconImageURL = damfn.getAssetLink(promotionIconUUID)!]
                                        [#assign promotionText = promotion.getProperty("promotionText").getString()!]
                                        [#assign additionalInfo = promotion.getProperty("promotionAdditionalInfo").getString()!]
                                        [#assign cachedDiscountPrice = promotion.getProperty("cachedDiscountPrice").getDouble()!]
                                        {
                                            "promotionIconImageURL": "${promotionIconImageURL?js_string!""}",
                                            "promotionText": "${promotionText?js_string!""}",
                                        "additionalInfo": [@compress single_line=true]"${additionalInfo?js_string!""}"[/@compress],
                                            "cachedDiscountPrice": "Promo Price: S$ ${cachedDiscountPrice?string["0.00"]}"
                                        },
                                    [/#list]
                                ],
                                "crossSellContent": [
                                    [#assign crossSellList = starfn.getAXProductCrossSells(axPrdDetail)]
                                    [#list crossSellList as crossSell]
                                        [#assign csNumber = crossSell.getProperty("crossSellProductCode").getString()!]
                                        [#assign crossSellProduct = starfn.getAxProductByProductCode("b2c-mflg",csNumber)]
                                        [#if crossSellProduct?has_content]
                                            [#assign csDesc = ""]
                                            [#if crossSell.hasProperty("crossSellItemDesc")]
                                                [#assign csDesc = crossSell.getProperty("crossSellItemDesc").getString()!]
                                            [/#if]
                                            [#assign csName = crossSell.getProperty("crossSellItemName").getString()!]
                                            [#assign csPrice = crossSellProduct.getProperty("productPrice").getString()!]
                                            [#assign csId = crossSellProduct.getProperty("productListingId").getString()!]
                                            [#assign csType = crossSellProduct.getProperty("productType").getString()!]
                                            {
                                                "displayDesc": "${csDesc?js_string!""}",
                                                "displayName": "${csName?js_string!""}",
                                                "displayPrice": "${csPrice}",
                                                "id":${csId},
                                                "itemCode":${csNumber},
                                                "link": "",
                                                "merchantId": null,
                                                "originalPrice": null,
                                                "priceType": null,
                                                "qty": 0,
                                                "ticketType": "${csType}",
                                                "unitPrice":${csPrice}},
                                        [/#if]
                                    [/#list]
                                ]
                            },
                        [/#if]
                    [/#list]
                ],
                "maxDate":"",
                "maxDateArr":[
                    2016,
                    7,
                    21
                ],
                "minBookingDays":1,
                "minDate":"",
                "minDateArr":[
                    0,
                    0,
                    0
                ],
                "nonTicketed":null,
                "notes":"",
                "paxPerTicket":1,
                "pcTickets":[

                ],
                "pcValue":null,
                "productId":"${cmsProductUUID}",
                "remarks":null,
                "schedules":[

                ],
                "showRemarks":false,
                "topupLimitType":"None",
                "topups":[

                ],
                "type":"General",
                "usualPrices":{
                    "Standard":"8.00",
                    "Child":"",
                    "Adult":""
                },
                "validFrom":null,
                "validUntil":"21\/07\/2016",
                "validityDays":90,
                "validityType":0
            },
            "message":null,
            "success":true
        };
        [/#if]
    [/#if]
</script>

<script>
    [#if params[4]?has_content]
    var urlPromoCode = "${params[4]?js_string!""}";
    var loadUrlPromoCode = true;
    [#else]
    var urlPromoCode = "";
    var loadUrlPromoCode = false;
    [/#if]
</script>

<div id="productDiv" class="form-panel">
    <div data-remarketing="true"></div>
    <div class="item-details" >
        <h2 data-bind="text: displayTitle">DISPLAY_TITLE</h2>
        <div class="display-msg msg-error" data-errorsection="true" style="display: none;" data-bind="visible: isErr, html: errMsg"></div>
        <div data-bind="if: displayDate || displayValiditySection()">
            <div class="item-content">
                <div class="item-date-visit">
                    <div data-bind="if: displayDate">
                        <div class="item-step" style="margin-top: 0">Choose a date<i class="icon-calendar"></i></div>
                        <!-- <div class="booking-form-note">Applicable for bookings for the following day, and up to 3 months later.</div> -->
                        <div class="item-date-display picker-holder--inline-fixed">
                            Date of Visit <input type="text" data-datepicker="true" class="field date-of-visit"
                                                 data-bind="value: dateOfVisit, style: {'border-color': dateOfVisit() == '' ? '#D83939' : '#bbbbbb'}">
                        </div>
                    </div>
                    <div data-bind="if: displayValiditySection">
                        <div class="item-step" style="margin-top: 0">Validity Period<i class="icon-calendar"></i></div>
                        <p><span data-bind="text: validityText"></span></p>
                    </div>
                </div>
                <div class="item-intro">
                [#if cmsProductNode?has_content]
                    [#assign cmsProductImages = starfn.getCMSProductImagesUUID(cmsProductNode, lang)!]
                    [#if cmsProductImages?has_content]
                        [#assign cmsProductImage = cmsProductImages[0]]
                        [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)]
                        <img class="product-image" src=${thumbnailAssetLink} />
                    [/#if]
                [/#if]
                    <div class="item-desc">
                    [#if cmsProductNode?has_content]
                        <p>${cmsProductDesc}</p>
                    [#else]
                        <p></p>
                    [/#if]
                        <!-- ko if:descUrl != '' -->
                        <a target="_blank" class="more-info" data-bind="attr:{'href':descUrl}">More Info</a>
                        <!-- /ko -->
                    </div>
                </div>
            </div>
        </div>
        <div data-bind="ifnot: displayValiditySection() || displayDate">
            <div class="item-content-dinner" data-bind="ifnot: displayDate">
                <div class="item-description">
                [#if cmsProductNode?has_content]
                    <p>${cmsProductDesc}</p>
                [#else]
                    <p></p>
                [/#if]

                    <!-- ko if:descUrl != '' -->
                    <a target="_blank" class="more-info" data-bind="attr:{'href':descUrl}">More Info</a>
                    <!-- /ko -->
                </div>
                <div class="item-intro-dinner">
                [#if cmsProductNode?has_content]
                    [#assign cmsProductImages = starfn.getCMSProductImagesUUID(cmsProductNode, lang)!]
                    [#if cmsProductImages?has_content]
                        [#assign cmsProductImage = cmsProductImages[0]]
                        [#assign thumbnailAssetLink = damfn.getAssetLink(cmsProductImage)]
                        <img class="dinner-product-image" src=${thumbnailAssetLink} />
                    [/#if]
                [/#if]
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="item-step" style="margin-top: 10px;" data-bind="css:{'ticket-sel':hasPromotion}">Select your tickets</div>
    <div class="ticket-sel-desc" data-bind="visible: hasPromotion">
        Choose standard tickets/bookings OR one of the promotions featured below.
    </div>
    <table class="ticket-form" cellpadding="0" cellspacing="0" data-bind="if: hasMainTickets">
        <tr>
          <th width="60%" class="ticket-category">Packages</th>
          <th width="25%">Price (SGD)</th>
            <th width="15%">Qty.</th>
        </tr>
        <tr>
            <td colspan="3" class="ticket-sub-form">
                <table  class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
                    <tr class="header-clickable" data-bind="click: function(){changeActiveSection('std')}">
                        <th colspan="3">
                            <img src="${themePath}/img/icon-promotion.png"/>Standard Tickets
                            <i data-bind="css:{'icon-expand-alt':activeSection()!='std','icon-collapse-alt':activeSection()=='std'}"
                               style="float:right;font-size:1.3em;color:#3ca7ba;"></i>
                        </th>
                    </tr>
                    <tbody data-bind="foreach: mainTickets, fadeVisible: activeSection()=='std'">
                    <tr data-bind="css:{'first-record':$index()==0}">
                      <!-- ko if:!hideDisplayName -->
                      <td width="60%" class="ticket-category" data-bind="attr: {rowspan: groupSize}">
                            <div class="book-item-data">
                                <span class="book-item-name" data-bind="text: displayName">TICKET_NAME</span>
                                <!-- ko if:displayDesc != '' -->
                                <span class="book-item-desc" data-bind="text: displayDesc">TICKET_DESC</span>
                                <!-- /ko -->
                                <!-- ko if: link != '' && link != null -->
                                <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                                <!-- /ko -->
                            </div>
                        </td>
                      <!-- /ko -->
                      <td width="25%" data-bind="text: typeAndPrice">TICKET_PRICE</td>
                        <td width="15%">
                            <select class="field" data-bind="value: qty, visible: !isEvent()">
                                <option value="0">0</option>
                                <option value="1">1</option><option value="2">2</option>
                                <option value="3">3</option><option value="4">4</option>
                                <option value="5">5</option><option value="6">6</option>
                                <option value="7">7</option><option value="8">8</option>
                            </select>
                            <button class="mflg-cyan" style="font-size:1em;width:50px;" data-bind="visible: isEvent() && !showSelectDate(), click: toggleSelectDate">Select</button>
                            <button class="mflg-cyan" style="font-size:1em;width:50px;" data-bind="visible: isEvent() && showSelectDate(), click: toggleSelectDate">Cancel</button>
                        </td>
                    </tr>
                    <tr class="first-record" data-bind="visible: showSelectDate">
                      <!-- ko if:!hideDisplayName -->
                      <td width="60%" data-bind="attr: {rowspan: groupSize}" colspan="2">
                            <table width="100%" class="select-event-details">
                                <tr>
                                    <td width="50%">Choose Date of Visit</td>
                                    <td width="50%">Choose Session</td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <div>
                                            <input style="width: 85%" class="field" width="100%"
                                                   data-bind="value: eventDate, attr: {id: 'event_date_' + id}"/>
                                            <span>&nbsp;</span>
                                            <label data-bind="attr: {for: 'event_date_' + id}">
                                                <i class="icon-calendar"></i>
                                            </label>
                                        </div>
                                    </td>
                                    <td width="50%">
                                        <select style="width: 85%" class="field" width="100%" data-bind="options: eventLines,
                       optionsText: 'eventName',
                       value: eventSession">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                      <!-- /ko -->
                        <td width="15%">
                            <select class="field" data-bind="value: qty">
                                <option value="0">0</option>
                                <option value="1">1</option><option value="2">2</option>
                                <option value="3">3</option><option value="4">4</option>
                                <option value="5">5</option><option value="6">6</option>
                                <option value="7">7</option><option value="8">8</option>
                            </select>
                        </td>
                    </tr>
                    <tr
                      class="first-record"
                      data-bind="visible: hasPromotionContent() && showPromoRows(), click: toggleShowPromoRows"
                      style="border-right: none">
                      <td colspan="3">
                        <div class="view-promotions-header">Hide Promotions
                          <div class="promo-arrow-up"></div>
                        </div>
                      </td>
                    </tr>
                    <tr
                      class="first-record"
                      data-bind="visible: hasPromotionContent() && !showPromoRows(), click: toggleShowPromoRows"
                      style="border-right: none">
                      <td colspan="3">
                        <div class="view-promotions-header">View Promotions
                          <div class="promo-arrow-down"></div>
                        </div>
                      </td>
                    </tr>
                    <tr class="first-record" data-bind="visible: hasPromotionContent() && showPromoRows()"
                        style="text-align: left">
                      <td width="80%" colspan="2">
                            <div data-bind="foreach: {data: promotionContent, as: 'promoContent'}">
                                <div class="promotion-content">
                                    <img data-bind="attr:{src: promoContent.promotionIconImageURL}"/>
                                    <span class="promotion-text" data-bind="text: promoContent.promotionText"></span>
                                    <div class="additional-info" data-bind="html: promoContent.additionalInfo"></div>
                                </div>
                                <div class="cached-discount-price">
                                    <span data-bind="text: promoContent.cachedDiscountPrice"></span>
                                </div>
                            </div>
                        </td>
                        <td width="15%"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>

    <div data-bind="visible: hasPromotion">
        <table class="ticket-form" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 20px;">
            <tr>
              <th width="60%" class="ticket-category">
                    Promotions
                    <div style="position: relative; margin-left: 5px; display: inline-block;">&nbsp;
                        <img style="position:absolute;top:0;margin-top:-15px;" src="${themePath}/img/other/great-deals-en.png"/>
                    </div>
                </th>
              <th width="25%">Price (SGD)</th>
                <th width="15%">Qty.</th>
            </tr>
            <tbody data-bind="if: hasCcPromo">
            <tr>
                <td colspan="3" class="ticket-sub-form">
                    <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
                        <tr class="header-clickable" data-bind="click: function(){changeActiveSection('cc')}">
                            <th colspan="3">
                                <img src="${themePath}/img/icon-promotion.png"/>Holding one of our partners' credit cards? Click here
                                for great savings!
                                <i data-bind="css:{'icon-expand-alt':activeSection()!='cc','icon-collapse-alt':activeSection()=='cc'}"
                                   style="float:right;font-size:1.3em;color:#3ca7ba;"></i>
                            </th>
                        </tr>
                        <tbody data-bind="foreach: ccTickets, fadeVisible: activeSection()=='cc'">
                        <tr data-bind="css:{'first-record':$index()==0}">
                          <td class="ticket-category" width="60%">
                                <div class="book-item-data">
                                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                                    <!-- ko if:displayDesc != '' -->
                                    <span class="book-item-desc" data-bind="text: displayDesc">DISCOUNT_DESC</span>
                                    <!-- /ko -->
                                    <!-- ko if: link != '' && link != null -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                                    <!-- /ko -->
                                    <!-- ko if:tncId > 0 -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':tncUrl}">* Terms and
                    Conditions Apply</a></span>
                                    <!-- /ko -->
                                </div>
                            </td>
                          <td width="25%" data-bind="text: displayPrice">DISCOUNT_PRICE</td>
                            <td width="15%">
                                <select class="field" data-bind="value: qty">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>

            <tbody data-bind="if: hasJcPromo">
            <tr>
                <td colspan="3" class="ticket-sub-form">
                    <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <th colspan="3">
                                <img src="${themePath}/img/icon-promotion.png"/>Are you a Faber Licence member? Get special deals!
                                <div style="padding-left: 20px;font-weight: normal;font-size: 0.9em;">Please key in your membership ID
                                    and expiry date.
                                </div>
                                <div style="padding-left: 24px; margin-top: 5px;">
                                    <span style="font-weight: normal; font-size: 0.9em;">Card No.</span>
                                    <input type="text" class="field card-no" placeholder="Faber Licence No." data-bind="value: jcNumber">
                                    <span style="font-weight: normal; font-size: 0.9em;">Expiry Date</span>&nbsp;&nbsp;
                                    <select class="field card-no" data-bind="value: jcExpiryMon">
                                        <option value="01">Jan</option>
                                        <option value="02">Feb</option>
                                        <option value="03">Mar</option>
                                        <option value="04">Apr</option>
                                        <option value="05">May</option>
                                        <option value="06">Jun</option>
                                        <option value="07">Jul</option>
                                        <option value="08">Aug</option>
                                        <option value="09">Sep</option>
                                        <option value="10">Oct</option>
                                        <option value="11">Nov</option>
                                        <option value="12">Dec</option>
                                    </select> /
                                    <select class="field card-no" data-bind="value: jcExpiryYr">
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                      <option value="2022">2022</option>
                                      <option value="2023">2023</option>
                                      <option value="2024">2024</option>
                                    </select>
                                    <button class="mflg-cyan" style="font-size:1em;width: 100px;" data-bind="click: checkJc">Apply
                                    </button>
                                </div>
                            </th>
                        </tr>
                        <tbody data-bind="foreach: jcTickets, fadeVisible: activeSection()=='jc'">
                        <tr data-bind="css:{'first-record':$index()==0}">
                          <td class="ticket-category" width="60%">
                                <div class="book-item-data">
                                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                                    <!-- ko if:displayDesc != '' -->
                                    <span class="book-item-desc" data-bind="text: displayDesc">DISCOUNT_DESC</span>
                                    <!-- /ko -->
                                    <!-- ko if: link != '' && link != null -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                                    <!-- /ko -->
                                    <!-- ko if:tncId > 0 -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':tncUrl}">* Terms and
                    Conditions Apply</a></span>
                                    <!-- /ko -->
                                </div>
                            </td>
                          <td width="25%" data-bind="text: displayPrice">DISCOUNT_PRICE</td>
                            <td width="15%">
                                <select class="field" data-bind="value: qty">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="first-record" data-bind="visible: hasPromotionContent"
                            style="text-align: left">
                          <td width="80%" colspan="2">
                            <div data-bind="foreach: {data: promotionContent, as: 'promoContent'}">
                              <div class="promotion-content">
                                <img data-bind="attr:{src: promoContent.promotionIconImageURL}"/>
                                <span class="promotion-text" data-bind="text: promoContent.promotionText"></span>

                                <div class="additional-info" data-bind="html: promoContent.additionalInfo"></div>
                              </div>
                              <div class="cached-discount-price">
                                Promo Price: S$ <span data-bind="text: promoContent.cachedDiscountPrice"></span>
                              </div>
                            </div>
                          </td>
                          <td width="15%"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>

            <tbody id="promo-code-tbody" data-bind="if: hasPcPromo">
            <tr>
                <td colspan="3" class="ticket-sub-form">
                    <table class="promotion-sub-category" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <th colspan="3">
                                <img src="${themePath}/img/icon-promotion.png"/>Have a promo code? Enter it here for sweet
                                discounts!<br/>

                                <div style="padding-left: 24px; margin-top: 5px;">
                                    <span style="font-weight: normal; font-size: 0.9em;">Promo Code</span>
                                    <input id="promo-code-bottom" type="text" maxlength="100" class="field card-no"
                                           placeholder="Promo Code" data-bind="value: pcValue">
                                    <button class="mflg-cyan" style="font-size:1em;width: 100px;" data-bind="click: checkPc">Apply
                                    </button>
                                </div>
                            </th>
                        </tr>
                        <tbody data-bind="foreach: pcTickets, fadeVisible: activeSection()=='pc'">
                        <tr data-bind="css:{'first-record':$index()==0}">
                          <td class="ticket-category" width="60%">
                                <div class="book-item-data">
                                    <span class="book-item-name" data-bind="text: displayName">DISCOUNT_DISPLAY</span>
                                    <!-- ko if:displayDesc != '' -->
                                    <span class="book-item-desc" data-bind="text: displayDesc">DISCOUNT_DESC</span>
                                    <!-- /ko -->
                                    <!-- ko if: link != '' && link != null -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                                    <!-- /ko -->
                                    <!-- ko if:tncId > 0 -->
                                    <span class="book-item-link"><a target="_blank" data-bind="attr:{'href':tncUrl}">* Terms and
                    Conditions Apply</a></span>
                                    <!-- /ko -->
                                </div>
                            </td>
                          <td width="25%" data-bind="text: displayPrice">DISCOUNT_PRICE</td>
                            <td width="15%">
                                <select class="field" data-bind="value: qty">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="first-record" data-bind="visible: hasPromotionContent"
                            style="text-align: left">
                          <td width="80%" colspan="2">
                            <div data-bind="foreach: {data: promotionContent, as: 'promoContent'}">
                              <div class="promotion-content">
                                <img data-bind="attr:{src: promoContent.promotionIconImageURL}"/>
                                <span class="promotion-text" data-bind="text: promoContent.promotionText"></span>

                                <div class="additional-info" data-bind="html: promoContent.additionalInfo"></div>
                              </div>
                              <div class="cached-discount-price">
                                Promo Price: S$ <span data-bind="text: promoContent.cachedDiscountPrice"></span>
                              </div>
                            </div>
                          </td>
                          <td width="15%"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div data-bind="visible: hasTopup">
        <div class="item-step">Add top ups</div>
        <table class="ticket-form" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <th width="60%" class="ticket-category">Top up</th>
              <th width="25%">Price (SGD)</th>
                <th width="15%">Qty.</th>
            </tr>
            <tbody data-bind="foreach: topups">
            <tr data-bind="css:{'first-record':$index()==0}, visible: displayItem">
                <td class="ticket-category">
                    <div class="topup-data">
                        <span class="topup-name" data-bind="text: displayName">TOPUP_DISPLAY</span>
                        <!-- ko if:displayDesc != '' -->
                        <span class="topup-desc" data-bind="text: displayDesc">TOPUP_DESC</span>
                        <!-- /ko -->
                        <!-- ko if: link != '' && link != null -->
                        <span class="topup-link"><a target="_blank" data-bind="attr:{'href':link}">Read more</a></span>
                        <!-- /ko -->
                    </div>
                </td>
                <td data-bind="text: displayPrice">TOPUP_PRICE</td>
                <td>
                    <select class="field" data-bind="value: qty">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ticket-total">Total (SGD):<span class="total-price" data-bind="text: totalAmount">TOTAL_AMOUNT</span></div>
    <div class="btn-field">
        <div class="submit-btn">
            <a class="addToCart" href="javascript:void(0)" data-bind="click: addToCart">Add To Cart</a>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="item-notes">
        <div data-bind="if: hasCableCarClosure">
            <h6>This product/package is not available on the following dates:</h6>
            <ul class="notes" data-bind="foreach: cableCarClosures">
                <li><span data-bind="text: dateRange"></span></li>
            </ul>
            <span style="display: block; height: 10px;">&nbsp;</span>
        </div>
        [#if cmsProductNotes?has_content]
          <div>
            <h6>Notes:</h6>

            <div class="notes">${cmsProductNotes}</div>

            <span style="display: block; height: 10px;">&nbsp;</span>
        </div>
        [/#if]
        <h6>Terms &amp; Conditions:</h6>
        <ul class="notes">
          <li><a style="color:#099DD5" target="_blank" href="${sitePath + "/tnc~" + prd + "~"}">Click here to view.</a>
          </li>
        </ul>

    </div>

    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>