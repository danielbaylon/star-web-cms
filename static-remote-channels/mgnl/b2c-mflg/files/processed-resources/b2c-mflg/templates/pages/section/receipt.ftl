[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<script>
  var jsLang = "${lang?js_string!}";
</script>

<div class="container contain" id="receiptPage">

    <div class="single-col-content" data-bind="visible: !isForReceipt() && !${cmsfn.isEditMode()?c}">
        <div class="main-error">
            <div class="main-error-msg" data-bind="text: abnormalMsg">You may have refreshed the page or pressed back while in the middle of a transaction. Your transaction has been invalidated. Please checkout again. <a href="${sitePath}" style="text-decoration: underline">Let's take you back to the home page.</a></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="basic-content" data-bind="visible: isForReceipt() | ${cmsfn.isEditMode()?c}">
        <div class="checkout">
            <h2>Order Successful</h2>
        </div>

        <p class="order-message">
            <span>Your order has been completed. An email will be sent to you soon with your receipt.</span>
        </p>

        <p class="order-print">
            <a href="javascript:window.print();"><i class="icon-print"></i> Print this page.</a>
        </p>

        <div class="receipt-wrapper">
            <div class="show-logos"><img src="${themePath}/img/logo/receipt-logo-${imgSuffix}.png"/></div>
            <div style="margin-top: 10px;margin-left: 110px;">
                <span>GST No.: M2-0010940-6</span>
            </div>

            <div class="receipt-header">
                <div class="receipt-part">
                    <span class="receipt-title">Receipt Number</span>
                    <span class="receipt-number" data-bind="text: receiptNumber"></span>
                </div>
            </div>

            <div class="receipt-intro" data-bind="with: customer">
                <p>Dear <span data-bind="text: salutation"></span> <span data-bind="text: name"></span>,</p>
                <p>Thank you for your booking! For instructions on how to redeem, please refer to the redemption mechanics
                    stated in the Terms &amp; Conditions at the bottom of the page.</p>
                <p>Your order details are summarized below:</p>
            </div>

            <div class="receipt-deets">
                <span class="deets-label">Date of Purchase: <span class="deets-value" data-bind="text: dateOfPurchase"></span></span>
                <span class="deets-label">Payment Type: <span class="deets-value" data-bind="text: paymentType"></span></span>
                <div data-bind="with: customer">
                    <span class="deets-label">Email: <span class="deets-value" data-bind="text: email"></span></span>
                    <span class="deets-label">Name: <span class="deets-value" data-bind="text: salutation"></span> <span class="deets-value" data-bind="text: name"></span></span>
                    <span class="deets-label">Company Name: <span class="deets-value" data-bind="text: companyName"></span></span>
                    <span class="deets-label">Mobile: <span class="deets-value" data-bind="text: mobile"></span></span>
                </div>
                <span class="deets-label">Last 4 Digits of Credit Card: <span class="deets-value">1234</span></span>
            </div>

            <!-- ko foreach: {data: cmsProducts, as: 'cmsProduct'} -->
            <table class="checkout-summary">
                <colgroup>
                    <col span="1" style="width: 37%" />
                    <col span="1" style="width: 36%" />
                    <col span="1" style="width: 10%" />
                    <col span="1" style="width: 5%" />
                    <col span="1" style="width: 13%" />
                </colgroup>
                <tr class="row-title">
                    <th colspan="5">
                        <span data-bind="text: cmsProduct.name"></span>
                    </th>
                </tr>
                <tr class="row-header">
                    <th class="left_row">Item</th>
                    <th class="left_row">Details</th>
                    <th>Unit Price</th>
                    <th>Qty.</th>
                    <th>Amount</th>
                </tr>
                <tbody class="section-main" data-bind="foreach: {data: cmsProduct.items, as: 'cmsItem'}">
                <tr class="row-main" data-bind="ifnot: cmsItem.isTopup">
                    <td class="cell-item">
                        <span data-bind="text: cmsItem.name"></span><br/>
                        <span data-bind="text: cmsItem.productDescription" style="color: gray"></span>
                    </td>
                    <td class="cell-desc">
                        <div class="cell-desc-div">
                            <span data-bind="text: cmsItem.description"></span>
                        </div>
                        <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                            <span data-bind="text: cmsItem.discountLabel"></span>
                        </div>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: cmsItem.priceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: qty"></span>
                    </td>
                    <td class="cell-sub">
                        <div data-bind="visible: !cmsItem.hasDiscount()">
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div data-bind="visible: cmsItem.hasDiscount">
                            <div>
                                <span data-bind="text: cmsItem.totalText"></span>
                            </div>
                            <div>
                                <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="row-main row-topup" data-bind="if: cmsItem.isTopup">
                    <td class="cell-item">
                        <i class="icon-plus-sign"></i>
                        <span data-bind="text: cmsItem.name"></span><br/>
                        <span data-bind="text: cmsItem.productDescription" style="color: gray"></span>
                    </td>
                    <td class="cell-desc">
                        <div class="cell-desc-div">
                            <span data-bind="text: cmsItem.description"></span>
                        </div>
                        <div class="cell-desc-div discount-description" data-bind="visible: cmsItem.hasDiscount">
                            <span data-bind="text: cmsItem.discountLabel"></span>
                        </div>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: cmsItem.priceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: qty"></span>
                    </td>
                    <td class="cell-sub">
                        <div data-bind="visible: !cmsItem.hasDiscount()">
                            <span data-bind="text: cmsItem.totalText"></span>
                        </div>
                        <div data-bind="visible: cmsItem.hasDiscount">
                            <div>
                                <span data-bind="text: cmsItem.totalText"></span>
                            </div>
                            <div>
                                <span class="discount-original-price" data-bind="text: cmsItem.originalTotalText"></span>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
                <tr class="row-subtotal">
                    <td colspan="4" class="cell-subtotal-lbl">Subtotal:</td>
                    <td class="cell-subtotal-amt">
                        <span data-bind="text: cmsProduct.subtotalText"></span>
                    </td>
                </tr>
            </table>
            <!-- /ko -->

            <table class="checkout-summary" data-bind="if: hasBookingFee">
                <colgroup>
                    <col span="1" style="width: 67%" />
                    <col span="1" style="width: 10%" />
                    <col span="1" style="width: 5%" />
                    <col span="1" style="width: 13%" />
                </colgroup>
                <tr class="row-title"><th colspan="4">Booking Fee</th></tr>
                <tr class="row-header">
                    <th class="left_row">Booking Fee</th>
                    <th>Unit Price</th>
                    <th>Qty.</th>
                    <th>Amount</th>
                </tr>
                <tbody class="section-main">
                <tr class="row-main">
                    <td class="cell-item cell-book-fee">
                        <span data-bind="text: bookFeeMode"></span>
                    </td>
                    <td class="cell-unit">
                        <span data-bind="text: bookPriceText"></span>
                    </td>
                    <td class="cell-qty">
                        <span data-bind="text: bookFeeQty"></span>
                    </td>
                    <td class="cell-sub">
                        <span data-bind="text: bookFeeSubTotalText"></span>
                    </td>
                </tr>
                </tbody>
                <tr class="row-subtotal">
                    <td class="cell-subtotal-waive">&nbsp;</td>
                    <td colspan="2" class="cell-subtotal-lbl">Subtotal:</td>
                    <td class="cell-subtotal-amt">
                        <span data-bind="text: bookFeeSubTotalText"></span>
                    </td>
                </tr>
            </table>

            <div class="grand-total">
                <span class="grand-total-text">GRAND TOTAL (Inclusive of GST): <span class="grand-total-amt" id="grandTotal" data-bind="text: totalText"></span></span>
            </div>

            <div style="margin: 5px 10px; padding: 5px; color: #666666; font-size: 11px">
                <h3 style="margin: 5px 0">Notes</h3>
                <h4>Please note that all completed and confirmed transactions cannot be cancelled, refunded or amended under any circumstances.</h4>

                <h4>For verification purposes, please present the following upon redemption:</h4>
                <ol>
                    <li>Email Confirmation</li>
                    <li>Credit card used for purchase</li>
                </ol>

                <h4>For a proxy to collect on your behalf, he/she needs to presents:</h4>
                <ol>
                    <li>Email Confirmation</li>
                    <li>This <a href="/dam/jcr:74ef58ab-b3dd-43b9-b85b-f343794efba9/letter-of-auth-mflg.pdf"
                                target="_blank">Letter of Authorisation</a> (duly completed and signed by you)
                    </li>
                    <li>Clear photocopy of your photo identification card such as NRIC/Passport/FIN Card</li>
                    <li>Proxy's original photo identification</li>
                </ol>
                <h4>For general enquiries, please email us at guestrelations@mountfaber.com.sg or call (65) 6377 9688.</h4>
                <h4>For corporate enquiries, please email us at enquiry@mountfaber.com.sg or call (65) 6377 9648/9.</h4>
                <h4>We reserve the right to disallow redemption if required documents are inadequately furnished.</h4>
            </div>

          <!-- ko if:hasTnc -->
          <div class="tnc">
            <h3>Terms &amp; Conditions</h3>
            <!-- ko foreach:tncs -->
            <div class="tnc-nugget">
              <h4 data-bind="text: title"></h4>

              <div class="the-tnc" data-bind="html: content"></div>
            </div>
            <!-- /ko -->
          </div>
          <!-- /ko -->

        </div>
    </div>

[@cms.area name="adBanner"/]

    <div class="clearfix"></div>

</div>