[#include "/b2c-mflg/templates/macros/pageInit.ftl"]
[#include "/b2c-mflg/templates/macros/staticText/header.ftl"]

[#assign previewMode = ctx.getParameter("preview")!]
[#assign isOnPreview = previewMode?has_content && previewMode == "true" && cmsfn.authorInstance]
[#assign isEditMode = cmsfn.editMode]
[#assign isAuthorPreview = cmsfn.authorInstance && cmsfn.previewMode]

[#assign isMaintenanceMode = starfn.isMaintenanceMode("b2c-mflg") && !isOnPreview && !isEditMode && !isAuthorPreview]

[#assign templateName = state.templateName!""]
[#if isMaintenanceMode && templateName != "b2c-mflg:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/b2c-mflg/maintenance")}
[#elseif !isMaintenanceMode && templateName == "b2c-mflg:pages/maintenance"]
${ctx.response.setStatus(301)}
${ctx.response.sendRedirect("/b2c-mflg")}
[/#if]

<script>
  var jsSitePath = "${sitePath?js_string!}";
  var jsLang = "${lang?js_string!}"
</script>

<div class="navbar">
  <div class="navbar-inner contain">
    <div class="section-1">
      <h1 class="navbar-logo">
        <a href="${sitePath}"><img src="${themePath}/img/logo/faber-peak-booking-logo-${imgSuffix}.png" alt="${staticText[lang]["siteLabel"]!"Faber Peak Online Booking"}"></a>
      </h1>
    </div>
    <div class="section-2">
      <div class="follow-us">
        ${staticText[lang]["followUsLabel"]!"Follow Us"}
        <a href="http://www.facebook.com/SingaporeCableCar" target="_blank" class="social-media"><img src="${themePath}/img/facebook-icon.png" alt="${staticText[lang]["facebookLabel"]!"Facebook"}"></a>
        <a href="http://instagram.com/singaporecablecar" target="_blank" class="social-media"><img src="${themePath}/img/instagram-icon.png" alt="${staticText[lang]["instagramLabel"]!"Instagram"}"></a>
        <a href="http://www.tripadvisor.com.sg/Attraction_Review-g294265-d7221059-Reviews-Faber_Peak_Singapore-Singapore.html" target="_blank" class="social-media"><img src="${themePath}/img/tripadvisor-icon.png" alt="${staticText[lang]["tripAdvisorLabel"]!"TripAdvisor"}"></a>
      </div>
    [#if !isMaintenanceMode]
      <div class="clearfix"></div>
      <div class="cart-btn">
        <div id="btnCart" style="display: inline; float: right;">
          <a id="default-cart" class="shpping-cart" href="${sitePath}/checkout">
            <i class="icon-shopping-cart icon-large"></i><span
            id="cartQty">0</span> ${staticText[lang]["cartItemsLabel"]!"item(s)"}
          </a>
        </div>
        <div style="display: inline; float: right; margin-top: 5px;">
          <span>${staticText[lang]["havePromoCodeLabel"]!"Have a promo code?"}</span>
          <input id="promo-code-header" class="field" type="text" maxlength="100"
                 placeholder="${staticText[lang]["promoCodeLabel"]!"Promo Code"}"
                 style="width: 100px; font-size: 14px;"/>
          <button id="promo-btn-header" class="mflg-cyan"
                  style="width: auto; font-size: 14px; padding-left: 2px; padding-right: 2px">${staticText[lang]["applyLabel"]!"Apply"}</button>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="navbar-nav">
        <ul>
          <li class="tabs">
            <a href="http://www.faberpeaksingapore.com/" class="faber-peak"><img
              src="${themePath}/img/tab/tab-faber-peak-${imgSuffix}.png"
              alt="${staticText[lang]["faberPeakLabel"]!"Faber Peak"}"></a>
          </li>

          <li class="tabs">
            <a href="${sitePath}" title="Home" class="icon_home"><img
              src="${themePath}/img/tab/tab-home-${imgSuffix}.png" alt="${staticText[lang]["homeLabel"]!"Home"}"></a>
          </li>

            [#assign cats = starfn.getCMSProductCategories("b2c-mflg")!]
            [#if cats?has_content]
              <li class="tabs" id="navOtherTickets">
                <a href="#"><img src="${themePath}/img/tab/tab-booking-${imgSuffix}.png"
                                 alt="${staticText[lang]["bookingLabel"]!"Booking"}"></a>

                <div class="navbar-nav-subnav">
                  <ul>
                      [#list cats as catNode]
                          [#if catNode?has_content]
                              [#assign catName = ""]
                              [#if catNode.hasProperty("name")]
                                  [#assign catName = catNode.getProperty("name").getString()]
                              [/#if]
                              [#assign catUrlPath = ""]
                              [#if catNode.hasProperty("generatedURLPath")]
                                  [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
                              [/#if]
                              [#assign subCats = starfn.getCMSProductSubCategories(catNode)!]
                              [#if catName = "Sky Dining"]
                                  [#assign hasPackages = sdfn.hasAvailablePackages()!false]
                                  [#if hasPackages]
                                      [#assign sdPackage = sdfn.getFirstAvailablePackage()!]
                                    <li class=""><a
                                      href="${sitePath}/category/sky-dining~${sdPackage.@name}~${lang}~.html?category=${catUrlPath}">Sky
                                      Dining</a>
                                    </li>
                                  [/#if]
                              [#elseif subCats?has_content]
                                  [#assign subCatNode = subCats?first]
                                  [#assign prods = starfn.getCMSProductBySubCategory(subCatNode)!]
                                  [#if prods?has_content]
                                      [#assign prodNode = prods?first]
                                      [#assign cmsProductName = ""]
                                      [#assign cmsProductShortDesc = ""]
                                      [#assign cmsProductUrl = "javascript:void(0);"]
                                      [#if prodNode.hasProperty("name")]
                                          [#assign cmsProductName = prodNode.getProperty("name").getString()]
                                      [/#if]
                                      [#if prodNode.hasProperty("shortDescription")]
                                          [#assign cmsProductShortDesc = prodNode.getProperty("shortDescription").getString()]
                                      [/#if]
                                      [#if prodNode.hasProperty("generatedURLPath")]
                                          [#assign cmsProductUrl = sitePath + "/category/product~" + prodNode.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${prodNode.getName()}~.html?category=${catUrlPath}"]
                                      [/#if]
                                    <li class=""><a href="${cmsProductUrl}">${catName}</a></li>
                                  [#else]
                                    <li class=""><a
                                      href="${sitePath}/category~${catUrlPath}~${catNode.getName()!}~">${catName}</a>
                                    </li>
                                  [/#if]
                              [#else]
                                <li class=""><a href="${sitePath}/category~${catUrlPath}~${catNode.getName()!}~">${catName}</a></li>
                              [/#if]
                          [/#if]
                      [/#list]
                  </ul>
                </div>
              </li>
            [/#if]

          <li class="tabs"><a href="${sitePath}/faq" title="Home"><img
            src="${themePath}/img/tab/tab-how-to-book-${imgSuffix}.png"
            alt="${staticText[lang]["howToBookLabel"]!"How to Book"}"></a></li>

          <li class="tabs"><a href="${sitePath}/contact"><img src="${themePath}/img/tab/tab-contact-${imgSuffix}.png"
                                                              alt="${staticText[lang]["contactUsLabel"]!"Contact Us"}"></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
    [/#if]
    </div>
    <div class="clearfix"></div>
  </div>
</div>

[#if isOnPreview]
<div class="nav" style="text-align: center;background-color: red"><h3>This is for preview only</h3></div>
<br/>
[/#if]