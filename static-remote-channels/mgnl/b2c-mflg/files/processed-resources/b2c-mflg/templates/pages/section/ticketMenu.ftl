[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

[#assign params = ctx.aggregationState.getSelectors()]
[#assign channel = "b2c-mflg"]
[#assign requestUri = ctx.getRequest().getRequestURI()]

<div class="item-panel">
[#if requestUri?index_of("/sky-dining",0) > -1]
    [#assign sdPackages = sdfn.getSkyDiningPackages()]
    [#assign prd = params[0]!""]
  <h3 class="item-category-title">Sky Dining</h3>
  <ul class="item-category-list" id="ticketMenu">
    <li><span class="item-category"><img src="${themePath}/img/icon-ticket.png">Sky Dining</span>
      <ul class="item-title-list">
          [#list sdPackages as sdPackage]
              [#if sdPackage.@name == prd]
                <li data-menu="12" class="selected"><a
                  href="${sitePath}/category/sky-dining~${sdPackage.@name}~${lang}~.html?category=sky-dining">
                  <i class="icon-chevron-right"></i><span class="item-title">${sdPackage.name}</span></a></li>
              [#else]
                <li data-menu="12" class=""><a
                  href="${sitePath}/category/sky-dining~${sdPackage.@name}~${lang}~.html?category=sky-dining">
                  <i class="icon-chevron-right"></i><span class="item-title">${sdPackage.name}</span></a></li>
              [/#if]

          [/#list]
      </ul>
    </li>
  </ul>
[#elseif params[2]?has_content]
    [#assign cat = params[2]]
    [#assign catNode = starfn.getCMSProductCategory(channel, cat)!]
    [#if params[3]?has_content]
        [#assign prd = params[3]]
        [#assign prdNode = starfn.getCMSProduct("b2c-mflg", prd)!]
    [/#if]
    [#if catNode?has_content]
        [#assign catName = ""]
        [#assign catUrlPath = ""]
        [#if catNode.hasProperty("name")]
            [#assign catName = catNode.getProperty("name").getString()]
        [/#if]
        [#if catNode.hasProperty("generatedURLPath")]
            [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
        [/#if]
        <h3 class="item-category-title">${catName}</h3>
        <ul class="item-category-list" id="ticketMenu">
            [#assign subCats = starfn.getCMSProductSubCategories(catNode)!]
            [#if subCats?has_content]
                [#list subCats as subCatNode]
                    [#assign subCatName = ""]
                    [#if subCatNode.hasProperty("name")]
                        [#if subCatNode.getName() == "default"]
                            [#assign subCatName = catName]
                        [#else]
                            [#assign subCatName = subCatNode.getProperty("name").getString()]
                        [/#if]
                    [/#if]
                    <li>
                        <span class="item-category"><img src=""${themePath}/img/icon-ticket.png"/>${subCatName}</span>
                        [#assign prods = starfn.getCMSProductBySubCategory(subCatNode)!]
                        [#if prods?has_content]
                            <ul class="item-title-list">
                                [#list prods as prodNode]
                                    [#assign cmsProductName = ""]
                                    [#assign cmsProductShortDesc = ""]
                                    [#assign cmsProductUrl = "javascript:void(0);"]
                                    [#if prodNode.hasProperty("name")]
                                        [#assign cmsProductName = prodNode.getProperty("name").getString()]
                                    [/#if]
                                    [#if prodNode.hasProperty("shortDescription")]
                                        [#assign cmsProductShortDesc = prodNode.getProperty("shortDescription").getString()]
                                    [/#if]
                                    [#if prodNode.hasProperty("generatedURLPath")]
                                        [#assign cmsProductUrl = sitePath + "/category/product~" + prodNode.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${prodNode.getName()}~.html?category=${catUrlPath}"]
                                    [/#if]
                                    [#assign prdName = ""]
                                    [#if prdNode?has_content]
                                        [#if prdNode.hasProperty("name")]
                                            [#assign prdName = prdNode.getProperty("name").getString()]
                                        [/#if]
                                    [/#if]
                                    [#if prdName = cmsProductName]
                                        <li class="selected">
                                            <a href="${cmsProductUrl}">
                                                <i class="icon-chevron-right"></i>
                                                <span class="item-title">${cmsProductName}</span>
                                            </a>
                                        </li>
                                    [#else]
                                        <li>
                                            <a href="${cmsProductUrl}">
                                                <i class="icon-chevron-right"></i>
                                                <span class="item-title">${cmsProductName}</span>
                                            </a>
                                        </li>
                                    [/#if]
                                [/#list]
                            </ul>
                        [/#if]
                    </li>
                [/#list]
            [/#if]
        </ul>
    [/#if]
[#elseif params[1]?has_content]
    [#assign cat = params[1]]
    [#assign catNode = starfn.getCMSProductCategory(channel, cat)!]
    [#if catNode?has_content]
        [#assign catName = ""]
        [#assign catUrlPath = ""]
        [#if catNode.hasProperty("name")]
            [#assign catName = catNode.getProperty("name").getString()]
        [/#if]
        [#if catNode.hasProperty("generatedURLPath")]
            [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
        [/#if]
        <h3 class="item-category-title">${catName}</h3>
    [/#if]
[/#if]

    <div class="clearfix"></div>
    <div class="help-panel">
        <h3>Need help booking ?</h3>
        <div class="contact-phone">
            <i class="icon-phone"></i>
            <p>Call our Guest Relations officers at<span class="help-number">(65) 6377 9688</span></p>
        </div>
        <p class="divider-or">or</p>
        <div class="contact-email">
            <i class="icon-envelope-alt" ></i>
            <p>Email us at <a href="mailto:guestrelations@mountfaber.com.sg" class="help-email">guestrelations@mountfaber.com.sg</a></p>
        </div>
    </div>

    [@cms.area name="adBannerL"/]

    <div class="clearfix" style="margin-top: 15px;"></div>
</div>

