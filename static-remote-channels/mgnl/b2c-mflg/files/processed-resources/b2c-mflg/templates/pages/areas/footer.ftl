[#include "/b2c-mflg/templates/macros/pageInit.ftl"]
[#include "/b2c-mflg/templates/macros/staticText/footer.ftl"]

<div class="footer">
  <div class="contain">
    <div style="display:block; width: 100%">
      <p class="copyright">
        ${staticText[lang]["supportedBrowsersLabel"]!"Supported Browsers: Explorer 8 and above, Firefox, Chrome, Safari"}
      </p>
      <ul class="footer-menu">
        <li><a href="${sitePath}/about">${staticText[lang]["aboutUsLabel"]!"About Us"}</a></li>
        <li><a href="${sitePath}/tos">${staticText[lang]["tosLabel"]!"Terms of Use"}</a></li>
        <li><a href="${sitePath}/data-protection-policy">${staticText[lang]["dppLabel"]!"Data Protection Policy"}</a></li>
        <li><a href="${sitePath}/faq">${staticText[lang]["faqLabel"]!"FAQ"}</a></li>
      </ul>
    </div>
    <p style="display: block; width: 100%;margin: 0 auto;margin-bottom: 10px;font-size: .95em;text-align: center; color:#009edb;">
      ${staticText[lang]["copyrightLabel"]!"Copyright &copy; 2016 Mount Faber Leisure Group Pte. Ltd."}
    </p>
  </div>
  <div class="clearfix"></div>
</div>