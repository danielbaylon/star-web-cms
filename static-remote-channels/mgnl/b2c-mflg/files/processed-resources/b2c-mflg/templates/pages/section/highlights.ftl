[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

[#if content.images?has_content]
  [#assign imageIndex = 0]
  [#assign displayImageIndex = 0]
  [#assign images = cmsfn.children(cmsfn.contentByPath(content.@handle + "/images"))]

<div class="slideshow">
    <div class="slideshow-heading">Featured Highlights</div>
    <div class="the-show">
        <ul id="roundabout">
          [#list images as image]
            [#if image?has_content]
              [#assign isCorrectImage = false]
              [#if image?index_of(lang,0) > -1]
                [#assign isCorrectImage = true]
              [#elseif lang = "en" && image?index_of("_",0) == -1]
                [#assign isCorrectImage = true]
              [/#if]
              [#if isCorrectImage == true]

                [#assign rendition = ""]
                [#if images[imageIndex].image?has_content]
                  [#assign rendition = damfn.getRendition(images[imageIndex].image, "original")]
                [/#if]
                [#assign assetTitle = displayImageIndex + 1]
                [#if rendition != ""]
                  [#if rendition.asset?? && rendition.asset.title?has_content]
                    [#assign assetTitle = rendition.asset.title]
                  [/#if]
                [/#if]

                [#assign imageAlt = images[imageIndex].title!assetTitle!]
                [#assign imageTitle = images[imageIndex].title!assetTitle!]

                [#assign imageLink = ""]
                [#if rendition != ""]
                  [#assign imageLink = rendition.link]
                [/#if]
                [#assign imageHyperlink = images[imageIndex].url!"javascript:void(0);"]

                  <li>
                      <a href="${imageHyperlink}" target="_blank">
                          <img src="${imageLink}" alt="${imageAlt}" />
                      </a>
                  </li>

                [#assign displayImageIndex = displayImageIndex + 1]
              [/#if]
            [/#if]
            [#assign imageIndex = imageIndex + 1]
          [/#list]
        </ul>
    </div>
    <div class="slide-nav">
        <ul>
            <li><a href="javascript:void(0)" class="roundPrev"><i class="icon-backward"></i></a></li>
            <li><a href="javascript:void(0)" id="roundToggle"><i id="roundToggleIcon" class="icon-pause"></i></a></li>
            <li><a href="javascript:void(0)" class="roundNext"><i class="icon-forward"></i></a></li>
        </ul>
    </div>
</div>

[/#if]