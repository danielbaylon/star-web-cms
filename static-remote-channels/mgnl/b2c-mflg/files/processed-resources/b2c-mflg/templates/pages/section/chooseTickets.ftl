[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

<div class="tickets_heading">Choose Tickets Now!</div>
[#assign cats = starfn.getCMSProductCategories("b2c-mflg")]
[#if cats?has_content]
<ul>
    [#list cats as catNode]
        [#if catNode?has_content]
            [#assign catName = ""]
            [#if catNode.hasProperty("name")]
                [#assign catName = catNode.getProperty("name").getString()]
            [/#if]
            [#assign assetLink = ""]
            [#if catNode.hasProperty("iconImage")]
                [#assign iconImage = catNode.getProperty("iconImage").getString()]
                [#assign assetLink = damfn.getAssetLink(iconImage)]
            [/#if]
            [#assign catUrlPath = ""]
            [#if catNode.hasProperty("generatedURLPath")]
                [#assign catUrlPath = catNode.getProperty("generatedURLPath").getString()]
            [/#if]
            [#assign subCats = starfn.getCMSProductSubCategories(catNode)!]
            [#if catName == "Sky Dining"]
                [#assign hasPackages = sdfn.hasAvailablePackages()!false]
                [#if hasPackages]
                    [#assign sdPackage = sdfn.getFirstAvailablePackage()!]
                  <li class="">
                    <a class="category"
                       href="${sitePath}/category/sky-dining~${sdPackage.@name}~${lang}~.html?category=${catUrlPath}">
                      <img src="${assetLink}">

                      <h3>${catName}</h3>
                    </a>
                  </li>
                [/#if]
            [#elseif subCats?has_content]
                [#assign subCatNode = subCats?first]
                [#assign prods = starfn.getCMSProductBySubCategory(subCatNode)!]
                [#if prods?has_content]
                    [#assign prodNode = prods?first]
                    [#assign cmsProductName = ""]
                    [#assign cmsProductShortDesc = ""]
                    [#assign cmsProductUrl = "javascript:void(0);"]
                    [#if prodNode.hasProperty("name")]
                        [#assign cmsProductName = prodNode.getProperty("name").getString()]
                    [/#if]
                    [#if prodNode.hasProperty("shortDescription")]
                        [#assign cmsProductShortDesc = prodNode.getProperty("shortDescription").getString()]
                    [/#if]
                    [#if prodNode.hasProperty("generatedURLPath")]
                        [#assign cmsProductUrl = sitePath + "/category/product~" + prodNode.getProperty("generatedURLPath").getString() + "~${lang}~${catNode.getName()}~${prodNode.getName()}~.html?category=${catUrlPath}"]
                    [/#if]
                    <li class="">
                        <a class="category" href="${cmsProductUrl}">
                            <img src="${assetLink}">
                            <h3>${catName}</h3>
                        </a>
                    </li>
                [#else]
                    <li class="">
                        <a class="category" href="${sitePath}/category~${catUrlPath}~${catNode.getName()!}~">
                            <img src="${assetLink}">
                            <h3>${catName}</h3>
                        </a>
                    </li>
                [/#if]
            [#else]
                <li class="">
                    <a class="category" href="${sitePath}/category~${catUrlPath}~${catNode.getName()!}~">
                        <img src="${assetLink}">
                        <h3>${catName}</h3>
                    </a>
                </li>
            [/#if]
        [/#if]
    [/#list]
</ul>
[/#if]