[#include "/b2c-mflg/templates/macros/pageInit.ftl"]

[#assign title = cmsfn.decode(content).title!""]
[#assign message = cmsfn.decode(content).message!""]

<div class="container contain">
    <div class="single-col-content">
        <div class="static-heading"><h2>${title}</h2></div>
        <div style="text-align:justify;">${message}</div>
        <br>
    </div>
</div>