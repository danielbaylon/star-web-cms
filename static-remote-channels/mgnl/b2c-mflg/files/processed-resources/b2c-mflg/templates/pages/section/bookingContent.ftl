<div class="container contain">

    <div class="booking-process">
    </div>
    <div class="clearfix"></div>

    [@cms.area name="ticketMenu"/]

    [@cms.area name="category"/]

    [@cms.area name="product"/]

    [@cms.area name="skyDining"/]

</div>
<div class="clearfix"></div>