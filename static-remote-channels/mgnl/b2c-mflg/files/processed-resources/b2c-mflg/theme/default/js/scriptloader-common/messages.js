(function (msgs) {

    //Login + User-related
    msgs.IncompleteLoginDetails = 'Both username and password cannot be blank.';
    msgs.AllFieldsRequired = 'All fields must be filled up.';
    msgs.RenewPassNotMatch = 'New passwords do not match. Please check your input.';
    msgs.PasswordPolicyNotMet = 'Password does not match the policy. Please see the password guidelines.';
    msgs.UsernameInvalid = 'Username is required and must be less than 100 characters.';
    msgs.EmailInvalid = 'Email is invalid.';

    //Ads
    msgs.ImageRequired = 'An image is required.';
    msgs.OrderingInvalid = 'Order must be a number. Please leave it blank if you want to add it to the end.';

    //News
    msgs.TitleInvalid = 'Title is required and must be less than 200 characters.';
    msgs.NewsInvalid = 'Content is required and must be less than 4000 characters.';
    msgs.ValidityRequired = 'Dates of validity are required.';
    msgs.ValidityInvalid = 'Valid (From) must be earlier or equal to the Valid (To) date.';

    msgs.PasswordLeadingTrailing = 'Password must not contain leading or trailing spaces.';

    //Booking

})(window.msgs = window.msgs || {});