$(document).ready(function() {
    mflg.checkoutModel = new mflg.CheckoutModel();
    mflg.getCheckoutDisplay();
    mflg.initCheckout();

    //	$('.cross-sell-view').slick({
    //		infinite : true,
    //		slidesToShow : 3,
    //		slidesToScroll : 1,
    //		dots: true
    //	});

    mflg.updateCartQty();
});

(function(mflg, $) {

    mflg.getCheckoutDisplay = function() {
        $
            .ajax({
                url : '/.store/store/get-checkout-display',
                data : JSON.stringify({
                    sessionId : ''
                }),
                headers : {
                    'Store-Api-Channel': 'b2c-mflg',
                    'Store-Api-Locale': jsLang
                },
                type : 'POST',
                cache : false,
                dataType : 'json',
                contentType : 'application/json',
                success : function(data) {
                    if (data.success) {
                        mflg.checkoutModel.init(data.data);
                    } else {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                },
                error : function() {
                    alert('Unable to process your request. Please try again in a few moments.');
                },
                complete : function() {

                }
            });
    };

    mflg.CheckoutModel = function() {
        var s = this;

        s.cartId = ko.observable("");
        s.total = ko.observable(0);
        s.totalText = ko.observable("");
        s.totalQty = ko.observable(0);
        s.totalMainQty = ko.observable(0);
        s.payMethod = ko.observable("");

        s.cmsProducts = ko.observableArray([]);

        s.hasBookingFee = ko.observable(false);
        s.bookFeeMode = ko.observable('');
        s.bookPriceInCents = ko.observable(0);
        s.bookPriceText = ko.observable("");
        s.bookFeeWaiverCode = ko.observable("");
        s.bookFeeQty = ko.observable(0);
        s.bookFeeSubTotal = ko.observable(0);
        s.bookFeeSubTotalText = ko.observable("");

        s.waivedTotalText = ko.observable("");

        s.dateOfPurchase = ko.observable("");
        s.receiptNumber = ko.observable("");
        s.pin = ko.observable("");
        s.barcodeUrl = ko.observable("");
        s.baseUrl = ko.observable("");
        s.paymentType = ko.observable("");
        s.paymentTypeLabel = ko.observable("");
        s.customerName = ko.observable("");

        s.hasTnc = ko.observable(false);

        s.hasAd = ko.observable(false);
        s.hasAdLink = ko.observable(false);
        s.adLink = ko.observable("");
        s.adUrl = ko.observable("");

        s.transStatus = ko.observable("");
        s.isError = ko.observable(false);
        s.errorCode = ko.observable("");
        s.errorMessage = ko.observable("");
        s.message = ko.observable("");

        s.custServEmail = ko.observable("");

        s.ccDigits = ko.observable("");

        s.emailMsgStyle = ko.observable("");

        s.totalTotal = ko.observable(0);
        s.totalTotalText = ko.observable("");

        s.customer = new mflg.CheckoutDeets('checkoutForm');
        s.customerJson = ko.observable("");

        s.init = function(data) {
            s.cartId(data.cartId);
            s.total(data.total);
            s.totalText(data.totalText);
            s.totalQty(data.totalQty);
            s.totalMainQty(data.totalMainQty);
            s.payMethod(data.payMethod);

            s.hasBookingFee(data.hasBookingFee);
            s.bookFeeMode(data.bookFeeMode);
            s.bookPriceInCents(data.bookPriceInCents);
            s.bookPriceText(data.bookPriceText);
            s.bookFeeWaiverCode(data.bookFeeWaiverCode);
            s.bookFeeQty(data.bookFeeQty);
            s.bookFeeSubTotal(data.bookFeeSubTotal);
            s.bookFeeSubTotalText(data.bookFeeSubTotalText);

            s.waivedTotalText(data.waivedTotalText);

            s.dateOfPurchase(data.dateOfPurchase);
            s.receiptNumber(data.receiptNumber);
            s.pin(data.pin);
            s.barcodeUrl(data.barcodeUrl);
            s.baseUrl(data.baseUrl);
            s.paymentType(data.paymentType);
            s.paymentTypeLabel(data.paymentTypeLabel);
            s.customerName(data.customerName);

            s.hasTnc(data.hasTnc);

            s.hasAd(data.hasAd);
            s.hasAdLink(data.hasAdLink);
            s.adLink(data.adLink);
            s.adUrl(data.adLink);

            s.transStatus(data.transStatus);
            s.isError(data.customerName);
            s.errorCode(data.errorCode);
            s.errorMessage(data.errorCode);
            s.message(data.message);

            s.custServEmail(data.custServEmail);

            s.ccDigits(data.ccDigits);

            s.emailMsgStyle(data.emailMsgStyle);

            s.totalTotal(data.totalTotal);
            s.totalTotalText(data.totalTotalText);

            s.customerJson(data.customerJson);

            $.each(data.cmsProducts, function(index, element) {
                var cmsProduct = new mflg.CmsProduct();
                cmsProduct.init(element);
                s.cmsProducts.push(cmsProduct);
            });

            if (data.hasPromoMerchant) {
                s.customer.hasPromoMerchant(data.hasPromoMerchant);
                s.customer.promoMerchants(data.promoMerchants);
            }

            if (data.hasAffiliations) {
                s.customer.hasAffiliations(data.hasAffiliations);
                s.customer.affiliations(data.affiliations);
            }

            if (data.customer) {
                s.customer.unpackDeets(data.customer);
            }
        }
    };

    mflg.CmsProduct = function() {
        var s = this;

        s.id = ko.observable(0);
        s.name = ko.observable("");
        s.subtotal = ko.observable(0);
        s.subtotalText = ko.observable("");

        s.items = ko.observableArray([]);

        s.init = function(data) {
            s.id(data.id);
            s.name(data.name);
            s.subtotal(data.subtotal);
            s.subtotalText(data.subtotalText);

            $.each(data.items, function(index, element) {
                var cmsItem = new mflg.CmsItem();
                cmsItem.init(element);
                s.items.push(cmsItem);
            });
        }
    };

    mflg.CmsItem = function() {
        var s = this;

        s.cartItemId = ko.observable("");
        s.listingId = ko.observable("");
        s.productCode = ko.observable("");
        s.type = ko.observable("");
        s.name = ko.observable("");
        s.description = ko.observable("");
        s.qty = ko.observable(0);
        s.price = ko.observable(0);
        s.priceText = ko.observable("");
        s.total = ko.observable(0);
        s.totalText = ko.observable("");
        s.isTopup = ko.observable(false);

        s.discountTotal = ko.observable(0);
        s.discountTotalText = ko.observable("");
        s.discountLabel = ko.observable("");
        s.hasDiscount = ko.computed(function() {
           return s.discountTotal() != 0;
        });
        s.originalTotalText = ko.computed(function() {
            if(s.hasDiscount()) {
                var originalPrice = s.total() + s.discountTotal();
                return "S$ " + originalPrice.toFixed(2);
            }

            return s.totalText();
        });

        s.promoLabels = ko.observableArray([]);
        s.hasPromoLabel = ko.computed(function () {
            return s.promoLabels().length > 0;
        });


        s.init = function(data) {
            s.cartItemId(data.cartItemId);
            s.listingId(data.listingId);
            s.productCode(data.productCode);
            s.type(data.type);
            s.name(data.name);
            s.description(data.description);
            s.qty(data.qty);
            s.price(data.price);
            s.priceText(data.priceText);
            s.total(data.total);
            s.totalText(data.totalText);
            s.isTopup(data.topup);

            s.discountTotal(data.discountTotal);
            s.discountTotalText(data.discountTotalText);
            s.discountLabel(data.discountLabel);

            $.each(data.promoLabels, function (index, element) {
                var promoLabel = new mflg.PromoLabel();
                promoLabel.init(element);
                s.promoLabels.push(promoLabel);
            });
        };

        s.removeItem = function() {
            mflg.removeItem(s.cartItemId);
        }
    };

    mflg.PromoLabel = function () {
        var s = this;

        s.text = ko.observable('');

        s.init = function (data) {
            s.text(data);
        };
    };

    mflg.logCrossSellView = function(productId, productUrl) {
        // mflg.spinner.start();
        $.ajax({
            url : '/checkout/log-cross-sell?productId=' + productId,
            type : 'POST',
            cache : false,
            dataType : 'json',
            success : function(data) {
                // window.location.replace(productUrl);
            },
            error : function() {
                // alert('Unable to process your request. Please try again in a few moments.');
            },
            complete : function() {
                // mflg.spinner.stop();
            }
        });
    };

    mflg.removeItem = function(cartId) {
        if (confirm('Are you sure you want to remove this item?')) {
            mflg.spinner.start();
            $
                .ajax({
                    url : '/.store/store/remove-item-from-cart/' + cartId(),
                    data : JSON.stringify({
                        sessionId : '',
                    }),
                    headers : {
                        'Store-Api-Channel' : 'b2c-mflg'
                    },
                    type : 'POST',
                    cache : false,
                    dataType : 'json',
                    contentType : 'application/json',
                    success : function(data) {
                        if (data.success) {
                            location.reload(true);
                        } else {
                            alert(data.message);
                        }
                    },
                    error : function() {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete : function() {
                        mflg.spinner.stop();
                    }
                });
        }
    };

    mflg.initCheckout = function() {
        $('#btnClearCart')
            .click(
            function() {
                if (confirm('Are you sure you want to remove all the items from your cart?')) {
                    mflg.spinner.start();
                    $
                        .ajax({
                            url : '/.store/store/clear-cart',
                            data : JSON.stringify({
                                sessionId : ''
                            }),
                            headers : {
                                'Store-Api-Channel' : 'b2c-mflg'
                            },
                            type : 'POST',
                            cache : false,
                            dataType : 'json',
                            contentType : 'application/json',
                            success : function(data) {
                                if (data.success) {
                                    location.reload(true);
                                } else {
                                    alert(data.message);
                                }
                            },
                            error : function() {
                                alert('Unable to process your request. Please try again in a few moments.');
                            },
                            complete : function() {
                                mflg.spinner.stop();
                            }
                        });
                }
            });

        $('#btnWaive').click(function() {
            var waiveCode = $('#fldWaive').val();
            var $msgWaive = $('#msgWaive');
            if (waiveCode == '') {
                $msgWaive.text('Please enter a promo code.');
                return;
            }
            $msgWaive.text('Please wait...');
            $.ajax({
                url : '/checkout/check-booking-fee-waiver?bfw=' + waiveCode,
                type : 'POST',
                cache : false,
                dataType : 'json',
                success : function(data) {
                    if (data.success) {
                        $('#bfw').val(waiveCode);
                        $('#grandTotal').text($('#waivedTotal').text());
                        $('#bookFeeSubtotal').text('0.00');
                        $('#waiveControls').hide();
                        $msgWaive.text('Booking fee has been waived.');
                    } else {
                        $msgWaive.text('Invalid. Please try again.');
                    }
                },
                error : function() {
                    $msgWaive.text('Invalid. Please try again.');
                },
                complete : function() {
                    mflg.spinner.stop();
                }
            });
        });


        ko.applyBindings(mflg.checkoutModel, document
            .getElementById('checkoutPage'));
    };

    mflg.CheckoutDeets = function(formId) {
        var s = this;

        s.formId = formId;

        s.email = ko.observable();
        s.email2 = ko.observable();
        s.idType = ko.observable('NricFin');
        s.idNo = ko.observable();
        s.idDisplay = ko.computed(function() {
            return (s.idType() === 'NricFin' ? '(NRIC/FIN) ' : '(Passport) ')
                + s.idNo();
        });
        s.name = ko.observable();
        s.mobile = ko.observable();
        s.paymentType = ko.observable('');
        s.subscribed = ko.observable(true);
        s.tnc = ko.observable(false);
        s.salutation = ko.observable('');
        s.companyName = ko.observable('');

        s.hasPromoMerchant = ko.observable(false);
        s.promoMerchants = ko.observableArray([]);
        s.selectedMerchant = ko.observable('');
        s.merchantId = ko.observable('');

        s.affiliations = ko.observableArray([]);
        s.hasAffiliations = ko.observable(true);

        s.activeAffiliation = ko.observable('');
        s.oldMerchant = ko.observable('');
        s.oldPaymentType = ko.observable('');
        s.waitForAffUpdate = ko.observable(false);

        s.promoCode = new ko.observable("");
        s.applyPromoCode = function () {
            $.ajax({
                url: '/.store/store/apply-promo-code',
                data: s.promoCode(), headers: {'Store-Api-Channel': 'b2c-mflg'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        window.alert(data.message);
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };

        s.changeMerchant = function (data, event) {
            var nextAction = s.checkMerchantAffiliation(s.oldMerchant(), s.merchantId());
            if (nextAction > 0) {
                s.updateAffiliation(nextAction, true);
            }
        };

        s.changePaymentType = function (data, event) {
            // console.log(s.affiliations());
            var nextAction = s.checkPaymentTypeAffiliation(s.oldPaymentType(), s.paymentType());
            if (nextAction > 0) {
                s.updateAffiliation(nextAction, false);
            }
        };

        s.updateAffiliation = function (affiliationAction, isBankAffiliation) {
            if (s.waitForAffUpdate()) {
                return;
            }

            s.waitForAffUpdate(true);

            var data = {
                isRemove: affiliationAction == 2,
                isBankAffiliation: isBankAffiliation,
                affiliationToAdd: {
                    affiliationId: s.activeAffiliation()
                },
                deets: s.packageDeets()
            };

            $.ajax({
                url: '/.store/store/update-affiliation',
                data: JSON.stringify(data), headers: {'Store-Api-Channel': 'b2c-mflg'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    // console.log(data);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        s.errMsg(data.message);
                        s.isErr(true);
                    }
                },
                error: function () {
                    s.errMsg('System encountered an error. Please try again.');
                    s.isErr(true);
                },
                complete: function () {
                    s.waitForAffUpdate(false)
                }
            });
        };

        // 0 - no need to update
        // 1 - add affiliation
        // 2 - remove affiliation
        s.checkMerchantAffiliation = function (oldId, newId) {
            if (oldId == newId) {
                return 0;
            }
            if (s.hasAffiliations()) {
                var isOldAffiliated = false;
                if (oldId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.creditCard == oldId) {
                            s.activeAffiliation(value.affiliationId);
                            isOldAffiliated = true;
                        }
                    });
                }
                var isNewAffiliated = false;
                if (newId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.creditCard == newId) {
                            s.activeAffiliation(value.affiliationId);
                            isNewAffiliated = true;
                        }
                    });
                }
                if (isOldAffiliated && !isNewAffiliated) {
                    return 2;
                } else if (!isOldAffiliated && isNewAffiliated) {
                    return 1;
                } else if (isOldAffiliated && isNewAffiliated) {
                    return 1;
                }
            }
            return 0;
        };

        // 0 - no need to update
        // 1 - add affiliation
        // 2 - remove affiliation
        s.checkPaymentTypeAffiliation = function (oldId, newId) {
            if (oldId == newId) {
                return 0;
            }
            if (s.hasAffiliations()) {
                var isOldAffiliated = false;
                if (oldId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.paymentType == oldId) {
                            s.activeAffiliation(value.affiliationId);
                            isOldAffiliated = true;
                        }
                    });
                }
                var isNewAffiliated = false;
                if (newId) {
                    $.each(s.affiliations(), function (index, value) {
                        if (value.paymentType == newId) {
                            s.activeAffiliation(value.affiliationId);
                            isNewAffiliated = true;
                        }
                    });
                }
                if (isOldAffiliated && !isNewAffiliated) {
                    return 2;
                } else if (!isOldAffiliated && isNewAffiliated) {
                    return 1;
                } else if (isOldAffiliated && isNewAffiliated) {
                    return 1;
                }
            }
            return 0;
        };

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();

        s.btnGuard = false;
        s.selectedMerchant = ko.observable('');

        s.doCheckout = function() {
            s.isErr(false);
            if (!s.validate()) {
                return false;
            }

            if (!s.btnGuard) {
                s.btnGuard = true;
                $.ajax({
                    url : '/.store/store/checkout',
                    data : JSON.stringify(s.packageDeets()),
                    headers : {
                        'Store-Api-Channel' : 'b2c-mflg',
                        'Store-Api-Locale': jsLang
                    },
                    type : 'POST',
                    cache : false,
                    dataType : 'json',
                    contentType : 'application/json',
                    success : function(data) {
                        if (data.success) {
                            window.location.href = "confirm";
                        } else {
                            alert('Unable to process your request. Please try again in a few moments.');
                        }
                    },
                    error : function() {
                        alert('Unable to process your request. Please try again in a few moments.');
                    },
                    complete : function() {
                        s.btnGuard = false;
                    }
                });
            }
        };

        s.validate = function() {
            var errs = '';

            s.name($('[name="cust.name"]').val());
            s.paymentType($('[name="cust.paymentType"]').val());
            s.email($('[name="cust.email"]').val());
            s.idNo($('[name="cust.idNo"]').val());

            // nvx.isEmpty(s.idType()) || nvx.isEmpty(s.idNo()) ||
            if (nvx.isEmpty(s.idType()) || nvx.isEmpty(s.idNo())
                || nvx.isEmpty(s.email()) || nvx.isEmpty(s.name())
                || nvx.isEmpty(s.paymentType()) || !s.tnc()
                || nvx.isEmpty(s.email2())) {
                errs += 'Please fill in all required fields denoted by an asterisk (*).<br>';
            } else {
                if (!nvx.isValidEmail(s.email())) {
                    errs += 'Please enter a valid email address.<br/>';
                } else if (!nvx.isLengthWithin(s.email(), 0, 500, true)) {
                    errs += 'Email can only have a maximum of 500 characters.<br/>';
                } else if (s.email() != s.email2()) {
                    errs += 'The emails you have entered do not match. Please check if you have entered your correct email address.<br/>';
                } else if (s.email() == 'andrew@gmail.com') {
                    errs += 'For UAT purposes, please use your own email address instead.<br/>';
                }

                if ('NricFin' === s.idType()
                    && !(nvx.nric.isNricValid(s.idNo().toUpperCase()) || nvx.nric
                        .isFinValid(s.idNo().toUpperCase()))) {
                    errs += 'Please enter a valid NRIC/FIN, e.g. S1234567G, G1234567G.<br/>';
                } else if (!nvx.isLengthWithin(s.idNo(), 0, 100, true)) {
                    errs += 'ID No. can only have a maximum of 100 characters.<br/>';
                } else if ('Passport' === s.idType() && nvx.hasNonAlphanumeric(s.idNo())) {
                    errs += 'Please enter a valid Passport No.<br/>';
                }

                if (!nvx.isLengthWithin(s.name(), 0, 200, true)) {
                    errs += 'Name can only have a maximum of 200 characters.<br/>';
                }
                if (nvx.isNotEmpty(s.mobile())
                    && !nvx.isLengthWithin(s.mobile(), 0, 100, true)) {
                    errs += 'Mobile Number can only have a maximum of 100 characters.<br/>';
                }
                if(nvx.isNotEmpty(s.mobile())
                    && !nvx.isValidPhoneNumber(s.mobile())) {
                    errs += 'Please enter a valid Mobile Number.<br/>';
                }


                // TODO form validations
            }

            if (errs != '') {
                s.errMsg(errs);
                s.isErr(true);
                return false;
            }
            return true;
        };

        s.packageDeets = function() {
            return {
                email : s.email(),
                idType : s.idType(),
                idNo : s.idNo(),
                name : s.name(),
                mobile : s.mobile(),
                paymentType : s.paymentType(),
                subscribed : s.subscribed(),
                salutation : s.salutation(),
                companyName : s.companyName(),
                selectedMerchant: s.getMerchantName(s.merchantId()),
                merchantId: s.merchantId(),
                dob : "",
                referSource : "",
                nationality : "",
                referSourceLabel : "",
                nationalityLabel : "",
                paymentTypeLabel : "",
                trafficSource : "",
                ip : ""
            };
        };

        s.getMerchantName = function (merchantId) {
            for (var i = 0; i < s.promoMerchants().length; i++) {
                var promoMerchant = s.promoMerchants()[i];
                if (promoMerchant.merchantId == merchantId) {
                    return promoMerchant.name;
                }
            }
            return "";
        };

        s.unpackDeets = function(d) {
            s.email(d.email);
            s.email2(d.email);
            s.idType(d.idType);
            s.idNo(d.idNo);
            s.name(d.name);
            s.mobile(d.mobile);
            s.paymentType(d.paymentType);
            s.oldPaymentType(d.paymentType);
            s.subscribed(d.subscribed);
            s.salutation(d.salutation);
            s.companyName(d.companyName);
            s.selectedMerchant(d.selectedMerchant);
            s.merchantId(d.merchantId);
            s.oldMerchant(d.merchantId);
            s.isErr(d.isErr);
            s.errMsg(d.errMsg);
        };

        s.getProductIdList = function () {
            var idList = "";
            $.each(mflg.checkoutModel.cmsProducts(), function (k, v) {
                idList += v.id();
                idList += "~";
            });
            return idList;
        };

        s.goToTnc = function (data, event) {
            event.stopPropagation();
            window.location.href = './tnc~' + s.getProductIdList();
        }
    };

})(window.mflg = window.mflg || {}, jQuery);