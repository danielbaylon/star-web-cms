$(document).ready(function () {
    mflg.initBase();

    /*
     $('#skyDiningNav')
     .attr('href', 'http://www.faberpeaksingapore.com/index.php?option=com_content&view=article&id=28&Itemid=16')
     .attr('target', '_blank');
     $('#skyDiningLink')
     .attr('href', 'http://www.faberpeaksingapore.com/index.php?option=com_content&view=article&id=28&Itemid=16')
     .attr('target', '_blank');
     */
});

(function (mflg, $) {

    mflg.spinnerOpts = {
        lines: 13, // The number of lines to draw
        length: 16, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        rotate: 0, // The rotation offset
        color: '#000', // #rgb or #rrggbb
        speed: 1.2, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };

    mflg.spinner = new function () {
        var s = this;
        s.spinner = new Spinner(mflg.spinnerOpts);
        s.elemId = 'spinner-overlay';
        s.start = function () {
            $('#' + s.elemId).show();
            s.spinner.spin(document.getElementById(s.elemId));
        };
        s.stop = function () {
            s.spinner.stop();
            $('#' + s.elemId).hide();
        };
    };

    mflg.goCache = true;

    ko.bindingHandlers.fadeVisible = {
        init: function (element, valueAccessor) {
            // Initially set the element to be instantly visible/hidden depending on the value
            var value = valueAccessor();
            $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
        },
        update: function (element, valueAccessor) {
            // Whenever the value subsequently changes, slowly fade the element in or out
            var value = valueAccessor();
            ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).hide();
        }
    };

    mflg.enableCartClick = true;

    mflg.initCartToggle = function () {
        if (mflg.enableCartClick) {
//            $('#btnCart').hover(function() {
//                $('#default-cart').css('background-position', '0 -103px');
//                $('#default-cart').css('height', '217px');
//                $('#cartPreview').show();
//            }, function() {
//                $('#default-cart').css('background-position', '0 0');
//                $('#default-cart').css('height', '80px');
//                $('#cartPreview').hide();
//            });
            $('#btnCart').click(function () {
//                window.location.href= '/checkout';
            }).css('cursor', 'pointer');
            $('#promo-btn-header').click(function () {
                mflg.checkPromoCodeFromHeader($('#promo-code-header').val());
            }).css('cursor', 'pointer');
        } else {
            $('#btnCart').hide();
        }
    };


    mflg.checkPromoCodeFromHeader = function (code) {
        if (code) {
            $
                .ajax({
                    url: '/.store/store/search-promo-code',
                    data: JSON.stringify({
                        fromHeader: true,
                        promoCode: code,
                        language: jsLang
                    }),
                    headers: {'Store-Api-Channel': 'b2c-mflg'},
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            window.location.href = jsSitePath + data.data.redirectUrl;
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function () {
                        alert('Unable to process your request. Please try again in a few moments.');
                    }
                });
        }
    };

    mflg.initBase = function () {
        /* TODO: Configure Timeout*/
        var timeoutSecs = 600;
        var doTimeoutDetection = false;

        mflg.initCartToggle();
        if (doTimeoutDetection == true) {
            mflg.heyTimeout = setTimeout(heyDoIt, timeoutSecs * 1000);
        }

        function heyDoIt() {
            var beforeAlert = (new Date()).getTime() / 1000;
            alert('This session will expire in 60 seconds. To stay on this page, click \'OK\'. If not, your session will be closed and your bookings will not be saved.');
            if ((new Date()).getTime() / 1000 - beforeAlert > 60) {
                alert('You have taken too long to click OK. Your session has expired and your bookings have not been saved. Please try again.');
                window.location.reload();
            }
            $.ajax({
                url: '/',
                type: 'GET', cache: false, dataType: 'json',
                success: function () {
                },
                error: function () {
                },
                complete: function () {
                }
            });
            mflg.heyTimeout = setTimeout(heyDoIt, timeoutSecs * 1000);
        }
    };

    function setCartText(qtyText, bodyHtml, totalText) {
        $('#cartQty').html(qtyText);
        $('#cartBody').html('');
        $('#cartTotal').html(totalText);
    }

    mflg.ValidationResult = function (success, html) {
        var self = this;
        self.success = success;
        self.html = html;
    };

    mflg.MsgBox = function ($msg) {
        var s = this;
        s.$msg = $msg;
        s.hide = function () {
            s.$msg.hide();
        };
        s.showError = function (html) {
            s.$msg.html(html).removeClass('msg-success').addClass('msg-error').show();
        };
        s.showSuccess = function (html) {
            s.$msg.html(html).removeClass('msg-error').addClass('msg-success').show();
        };
    };

    mflg.updateCartQty = function () {
        $.ajax({
            url: '/.store/store/get-cart',
            data: JSON.stringify({
                sessionId: '',
            }), headers: {'Store-Api-Channel': 'b2c-mflg'},
            type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    if (data.data) {
                        $('#cartQty').html(data.data.totalQty);
                    }
                } else {
                    $('#cartQty').html('0');
                }
            },
            error: function () {
                $('#cartQty').html('0');
            }
        });
    }
})(window.mflg = window.mflg || {}, jQuery);