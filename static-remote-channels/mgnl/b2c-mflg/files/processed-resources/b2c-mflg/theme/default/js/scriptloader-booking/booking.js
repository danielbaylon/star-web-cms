$(document).ready(function () {

    mflg.mainPath = window.location.pathname;

    if (cmsBookingModel != null) {
        mflg.bm = new mflg.BookingModel();
        mflg.bm.initialize(cmsBookingModel.bookingModel);
        ko.applyBindings(mflg.bm, document.getElementById('productDiv'));
    }

    mflg.updateCartQty();
});

(function (mflg, $) {

    mflg.FORM_ID = 'bookingForm_';
    mflg.DATE_ID = 'dateOfVisit_';

    mflg.REMARKETING = {
        36: '<img src="https://avn.innity.com/rt/?cb=1418367393&a=1450&e=8760" width="0" height="0" border="0"/>'
    };

    mflg.BookingModel = function () {
        var s = this;

        s.formId = '';
        s.dateId = '';
        s.productId = -1;
        s.type = '';

        s.displayDate = true;
        s.displayTitle = '';
        s.displayDescription = '';
        s.displayImagePath = '';
        s.descUrl = '';

        s.hasCableCarClosure = false;
        s.cableCarClosures = [];

        s.hasMainCourses = false;
        s.mainCourses = ko.observableArray([]);
        s.paxPerTicket = 1;
        s.topupLimitType = '';

        s.hasNotes = false;
        s.notes = '';

        s.tncUrl = '';

        s.usualPrices = {};

        s.mainTickets = ko.observableArray([]);
        s.hasMainTickets = ko.computed(function () {
            return s.mainTickets().length > 0;
        });
        s.hasPromotion = false;
        s.hasCcPromo = false;
        s.hasJcPromo = false;
        s.hasPcPromo = false;
        s.ccTickets = ko.observableArray([]);
        s.jcTickets = ko.observableArray([]);
        s.pcTickets = ko.observableArray([]);
        s.topups = ko.observableArray([]);
        s.hasTopup = ko.observable(false);
        s.isFnb = ko.observable(false);

        s.schedules = ko.observableArray([]);

        s.jcNumber = ko.observable('');
        s.jcExpiryMon = ko.observable('');
        s.jcExpiryYr = ko.observable('');
        s.jcExpiry = ko.computed(function () {
            return s.jcExpiryMon() + '/' + s.jcExpiryYr();
        });
        s.pcValue = ko.observable('');
        s.dateOfVisit = ko.observable('');

        s.showRemarks = ko.observable(false);
        s.remarks = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable('');

        s.minBookingDays = '';
        s.blackoutDatesArr = null;
        s.maxDateArr = null;
        s.minDateArr = null;

        s.currentSched = ko.observable(-1);

        s.SECTS = {
            STD: 'std',
            CC: 'cc',
            PC: 'pc',
            JC: 'jc'
        };
        s.activeSection = ko.observable(s.SECTS.STD);
        s.changeActiveSection = function (theSect) {
            s.activeSection(theSect);
        };
        s.activeSection.subscribe(function (newVal) {
            if (newVal != s.SECTS.STD) {
                $.each(s.mainTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.JC) {
                $.each(s.jcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.CC) {
                $.each(s.ccTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.PC) {
                $.each(s.pcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
        });

        s.computeTotalPax = ko.computed(function () {
            var total = 0;
            if (s.type == 'General') {
                $.each(s.mainTickets(), function (idx, tix) {
                    total += Number(tix.qty());
                });
                $.each(s.ccTickets(), function (idx, tix) {
                    total += Number(tix.qty());
                });
                $.each(s.jcTickets(), function (idx, tix) {
                    total += Number(tix.qty());
                });
                $.each(s.pcTickets(), function (idx, tix) {
                    total += Number(tix.qty());
                });
            } else {
                $.each(s.schedules(), function (idx, sc) {
                    $.each(sc.mainTickets(), function (idx, tix) {
                        total += Number(tix.qty());
                    });
                    $.each(sc.ccTickets(), function (idx, tix) {
                        total += Number(tix.qty());
                    });
                    $.each(sc.jcTickets(), function (idx, tix) {
                        total += Number(tix.qty());
                    });
                    $.each(sc.pcTickets(), function (idx, tix) {
                        total += Number(tix.qty());
                    });
                });
            }
            return total * s.paxPerTicket;
        });

        s.totalAmount = null;

        s.validityType = ko.observable(0);
        s.validityDays = ko.observable(0);
        s.validFrom = ko.observable('');
        s.validUntil = ko.observable('');

        s.displayValiditySection = ko.computed(function () {
            return (s.validityType() == 1) || (s.validityType() == 2)
                || (s.validityType() == 3);
        });

        s.validityText = ko.computed(function () {
            switch (s.validityType()) {
                case 1:
                    var text = s.validityDays() + " days from the booking date";
                    text += " - expires on " + s.validUntil();
                    return text;
                case 2:
                    return "Expires on " + s.validUntil();
                case 3:
                    return "Valid from " + s.validFrom() + " until "
                        + s.validUntil();
                default:
                    return "";
            }
        });

        function getAmt(qty, unitPrice) {
            if (qty > 0) {
                return Number(qty) * Number(unitPrice);
            }
            return 0;
        }

        s.isEvent = function () {
            if (s.type != null) {
                return s.type.slice(0, 'Event'.length) == 'Event';
            }
            return false;
        };

        s.checkJc = function () {
            s.isErr(false);
            if (nvx.isEmpty(s.jcNumber())) {
                s.isErr(true);
                s.errMsg('Please enter a Faber Licence number.');
                s.scrollToErr();
                return;
            }
            var isEventMan = s.type.slice(0, 'Event'.length) == 'Event';
            var schedId = isEventMan ? s.currentSched() : -1;
            mflg.spinner.start();
            $
                .ajax({
                    url: '/booking/check-jc',
                    data: {
                        productId: s.productId,
                        jcNumber: s.jcNumber(),
                        jcExpiry: s.jcExpiry(),
                        schedId: schedId
                    },
                    type: 'GET',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshJcTickets(data.jcTickets);
                            s.activeSection(s.SECTS.JC);
                        } else {
                            s.refreshJcTickets([]);
                            s.isErr(true);
                            s.errMsg(data.message);
                            s.scrollToErr();
                        }
                    },
                    error: function () {
                        s.refreshJcTickets([]);
                        s.isErr(true);
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.scrollToErr();
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.checkPcFromTopGeneral = function () {
            return s.checkPc(true);
        };

        s.checkPcFromTopEvent = function () {
            if (s.currentSched() == -1) {
                return false;
            } else {
                var theSched = null;
                $.each(s.schedules(), function (idx, sc) {
                    if (sc.id == s.currentSched()) {
                        theSched = sc;
                        return false;
                    }
                });
                if (theSched) {
                    theSched.pcValue(s.pcValue());
                    return theSched.checkPc(true);
                }
                return false;
            }
        };

        s.checkPc = function (fromTop) {
            s.isErr(false);
            if (s.displayDate && nvx.isEmpty(s.dateOfVisit())) {
                s.isErr(true);
                s.errMsg('Please specify a date of visit.');
                s.scrollToErr(fromTop);
                return;
            }
            if (nvx.isEmpty(s.pcValue())) {
                s.isErr(true);
                s.errMsg('Please enter a promo code.');
                s.scrollToErr(fromTop);
                return;
            }
            var isEventMan = s.type.slice(0, 'Event'.length) == 'Event';
            var schedId = isEventMan ? s.currentSched() : -1;
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/store/check-promo-code',
                    data: JSON.stringify({
                        fromHeader: false,
                        productId: s.productId,
                        promoCode: s.pcValue(),
                        isEvent: isEventMan,
                        schedId: schedId,
                        dateOfVisit: s.dateOfVisit()
                    }),
                    headers: {'Store-Api-Channel': 'b2c-mflg'},
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshPcTickets(data.data);
                            s.activeSection(s.SECTS.PC);
                            if (fromTop == true) {
                                var topOfErr = $('#promo-code-tbody')
                                    .offset().top;
                                $('html, body').animate({
                                    scrollTop: topOfErr
                                }, 500); // account
                                // for
                                // the
                                // height
                                // of
                                // navbar
                            }
                        } else {
                            s.refreshPcTickets(null);
                            s.isErr(true);
                            s.errMsg(data.message);
                            s.scrollToErr(fromTop);
                        }
                    },
                    error: function () {
                        s.refreshPcTickets(null);
                        s.isErr(true);
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.scrollToErr(fromTop);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.refreshPcTickets = function (data) {
            var pcArr = [];
            if (data) {
                console.log(data);
                $.each(data.updatedTickets, function (idx, tix) {
                    // TODO: Implement update tickets
                });
                $.each(data.unlockedTickets, function (idx, tix) {
                    var bookingItemModel = s.convertAxProductToBookingItem(tix);
                    bookingItemModel.discountCode = data.promoCode;
                    pcArr.push(new mflg.BookingItem(bookingItemModel, s.usualPrices,
                        s.paxPerTicket));
                });
            }
            s.pcTickets(pcArr);
        };

        s.convertAxProductToBookingItem = function (axProduct) {
            var bookingItem = {
                "discountId": null,
                "discountPrice": null,
                "displayDesc": axProduct.productDesc,
                "displayName": axProduct.productName,
                "displayPrice": axProduct.productPriceText,
                "id": axProduct.productListingId,
                "itemCode": axProduct.displayProductNumber,
                "link": "",
                "merchantId": null,
                "originalPrice": null,
                "priceType": null,
                "qty": 0,
                "ticketType": axProduct.getTicketType,
                "unitPrice": axProduct.productPrice,
                "promotionContent": [{
                    "promotionIconImageURL": axProduct.promotionIconImageURL,
                    "promotionText": axProduct.promotionText,
                    "additionalInfo": axProduct.additionalInfo,
                    "cachedDiscountPrice": axProduct.cachedDiscountPrice
                }],
            };
            return bookingItem;
        };

        s.refreshJcTickets = function (jcTickets) {
            var jcArr = [];
            $.each(jcTickets, function (idx, tix) {
                jcArr.push(new mflg.BookingItem(tix, s.usualPrices));
            });
            s.jcTickets(jcArr);
        };

        s.scrollToErr = function (fromTop) {
            var topOfErr = $('#productDiv').find('div[data-errorsection]')
                .offset().top;
            if (fromTop) {
                if (topOfErr > 1000) {
                    topOfErr = 330;
                }
            }
            $('html, body').animate({
                scrollTop: topOfErr - 85
            }, 500); // account
            // for
            // the
            // height
            // of
            // navbar
        };

        s.acceptsWarnings = function () {
            if (s.topupLimitType == 'LimitToMainTicket') {
                var qtyMap = {
                    'Adult': 0,
                    'Child': 0,
                    'Standard': 0
                };
                var topupQtyMap = {
                    'Adult': 0,
                    'Child': 0,
                    'Standard': 0
                };

                function iterAndAddTicketsTyped(idx, tix) {
                    qtyMap[tix.ticketType] = qtyMap[tix.ticketType]
                        + Number(tix.qty());
                }

                if (s.type == 'General') {
                    $.each(s.mainTickets(), iterAndAddTicketsTyped);
                    $.each(s.ccTickets(), iterAndAddTicketsTyped);
                    $.each(s.pcTickets(), iterAndAddTicketsTyped);
                    $.each(s.jcTickets(), iterAndAddTicketsTyped);
                    $
                        .each(
                        s.topups(),
                        function (idx, tix) {
                            topupQtyMap[tix.ticketType] = topupQtyMap[tix.ticketType]
                                + Number(tix.qty());
                        });
                } else {
                    var theSched = null;
                    $.each(s.schedules(), function (idx, sc) {
                        if (sc.id == s.currentSched()) {
                            theSched = sc;
                            return false;
                        }
                    });
                    $.each(theSched.mainTickets(), iterAndAddTicketsTyped);
                    $.each(theSched.ccTickets(), iterAndAddTicketsTyped);
                    $.each(theSched.pcTickets(), iterAndAddTicketsTyped);
                    $.each(theSched.jcTickets(), iterAndAddTicketsTyped);
                    $
                        .each(
                        theSched.topups(),
                        function (idx, tix) {
                            topupQtyMap[tix.ticketType] = topupQtyMap[tix.ticketType]
                                + Number(tix.qty());
                        });
                }

                var xAdult = qtyMap.Adult > topupQtyMap.Adult, xChild = qtyMap.Child > topupQtyMap.Child, xStd = qtyMap.Standard > topupQtyMap.Standard;
                if (!xAdult && !xChild && !xStd) {
                    return true;
                }
                return confirm('You have selected '
                    + (xAdult ? qtyMap.Adult
                    + ' adult main ticket(s) but only '
                    + topupQtyMap.Adult + ' adult topup(s); ' : '')
                    + (xChild ? qtyMap.Child
                    + ' child main ticket(s) but only '
                    + topupQtyMap.Child + ' child topup(s); ' : '')
                    + (xStd ? qtyMap.Standard
                    + ' standard main ticket(s) but only '
                    + topupQtyMap.Standard + ' standard topup(s); '
                        : '') + 'is this correct?');
            }
            return true;
        };

        s.addToCart = function () {
            s.isErr(false);
            if (!s.validate()) {
                s.isErr(true);
                s.scrollToErr();
                return false;
            }
            if (!s.acceptsWarnings()) {
                return false;
            }
            var mainItems = [];
            $.each(mflg.bm.mainTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    if (tix.isEvent()) {
                        var mainItem = {
                            cmsProductName: mflg.bm.displayTitle,
                            cmsProductId: mflg.bm.productId,
                            listingId: tix.id,
                            productCode: tix.itemCode,
                            name: tix.displayName,
                            type: tix.ticketType,
                            qty: tix.qty(),
                            price: tix.unitPrice,
                            eventGroupId: tix.eventGroupId(),
                            eventLineId: tix.eventSession().eventLineId,
                            eventSessionName: tix.eventSession().eventName,
                            selectedEventDate: tix.eventDate(),
                            discountId: tix.discountId,
                            discountCode: tix.discountCode(),
                        };
                        mainItems.push(mainItem);
                    } else {
                        var mainItem = {
                            cmsProductName: mflg.bm.displayTitle,
                            cmsProductId: mflg.bm.productId,
                            listingId: tix.id,
                            productCode: tix.itemCode,
                            name: tix.displayName,
                            type: tix.ticketType,
                            qty: tix.qty(),
                            price: tix.unitPrice,
                            eventGroupId: "",
                            eventLineId: "",
                            eventSessionName: "",
                            selectedEventDate: "",
                            discountId: tix.discountId,
                            discountCode: tix.discountCode(),
                        };
                        mainItems.push(mainItem);
                    }
                }
            });
            console.log(mflg.bm.pcTickets());
            $.each(mflg.bm.pcTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    if (tix.isEvent()) {
                        var mainItem = {
                            cmsProductName: mflg.bm.displayTitle,
                            cmsProductId: mflg.bm.productId,
                            listingId: tix.id,
                            productCode: tix.itemCode,
                            name: tix.displayName,
                            type: tix.ticketType,
                            qty: tix.qty(),
                            price: tix.unitPrice,
                            eventGroupId: tix.eventGroupId(),
                            eventLineId: tix.eventSession().eventLineId,
                            eventSessionName: tix.eventSession().eventName,
                            selectedEventDate: tix.eventDate(),
                            discountId: tix.discountId,
                            discountCode: tix.discountCode(),
                        };
                        mainItems.push(mainItem);
                    } else {
                        var mainItem = {
                            cmsProductName: mflg.bm.displayTitle,
                            cmsProductId: mflg.bm.productId,
                            listingId: tix.id,
                            productCode: tix.itemCode,
                            name: tix.displayName,
                            type: tix.ticketType,
                            qty: tix.qty(),
                            price: tix.unitPrice,
                            eventGroupId: "",
                            eventLineId: "",
                            eventSessionName: "",
                            selectedEventDate: "",
                            discountId: tix.discountId,
                            discountCode: tix.discountCode(),
                        };
                        mainItems.push(mainItem);
                    }
                }
            });
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/store/add-to-cart',
                    data: JSON.stringify({
                        sessionId: '',
                        items: mainItems
                    }),
                    headers: {
                        'Store-Api-Channel': 'b2c-mflg'
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.addTopUpsToCart();
                        } else {
                            s.errMsg(data.message);
                            s.isErr(true);
                            s.scrollToErr();
                            mflg.spinner.stop();
                        }
                    },
                    error: function () {
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                        var topOfErr = $('#productDiv').find(
                            'div[data-errorsection]').offset().top;
                        $('html, body').animate({
                            scrollTop: topOfErr - 85
                        }, 500); // account
                        // for
                        // the
                        // height
                        // of
                        // navbar
                        mflg.spinner.stop();
                    }
                });
        };

        s.addTopUpsToCart = function () {
            var topups = [];
            $.each(mflg.bm.topups(), function (idx, tix) {
                if (tix.qty() > 0) {

                    var topup = {
                        cmsProductName: mflg.bm.displayTitle,
                        cmsProductId: mflg.bm.productId,
                        parentCmsProductId: mflg.bm.productId,
                        parentListingId: tix.parentId(),
                        listingId: tix.id,
                        productCode: tix.itemCode,
                        name: tix.displayName,
                        type: tix.ticketType,
                        qty: tix.qty(),
                        price: tix.unitPrice,
                        isTopup: true
                    };
                    topups.push(topup);
                }
            });
            if (topups.length > 0) {
                mflg.spinner.start();
                $
                    .ajax({
                        url: '/.store/store/add-topup-to-cart',
                        data: JSON.stringify({
                            sessionId: '',
                            items: topups
                        }),
                        headers: {
                            'Store-Api-Channel': 'b2c-mflg'
                        },
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            if (data.success) {
                                window.location.href = "../checkout";
                            } else {
                                s.errMsg(data.message);
                                s.isErr(true);
                                s.scrollToErr();
                                mflg.spinner.stop();
                            }
                        },
                        error: function () {
                            s
                                .errMsg('Unable to process your request. Please try again in a few moments.');
                            s.isErr(true);
                            var topOfErr = $('#productDiv').find(
                                'div[data-errorsection]').offset().top;
                            $('html, body').animate({
                                scrollTop: topOfErr - 85
                            }, 500); // account
                            // for
                            // the
                            // height
                            // of
                            // navbar
                            mflg.spinner.stop();
                        }
                    });
            } else {
                window.location.href = "../checkout";
            }

        };

        s.constructDataObj = function () {
            if (s.type == 'General') {
                var data = {
                    productId: s.productId,
                    jcNumber: s.jcNumber(),
                    jcExpiry: s.jcExpiry(),
                    pcValue: s.pcValue(),
                    dateOfVisit: s.dateOfVisit(),
                    topups: constructTicketArr(s.topups()),
                    mainTickets: [],
                    jcTickets: [],
                    ccTickets: [],
                    pcTickets: []
                };
                switch (s.activeSection()) {
                    case s.SECTS.STD:
                        data.mainTickets = constructTicketArr(s.mainTickets());
                        break;
                    case s.SECTS.JC:
                        data.jcTickets = constructTicketArr(s.jcTickets());
                        break;
                    case s.SECTS.CC:
                        data.ccTickets = constructTicketArr(s.ccTickets());
                        break;
                    case s.SECTS.PC:
                        data.pcTickets = constructTicketArr(s.pcTickets());
                        break;
                }
                return data;
            }

            // Event
            var theSched = null;
            $.each(s.schedules(), function (idx, sc) {
                if (sc.id == s.currentSched()) {
                    theSched = sc;
                    return false;
                }
            });

            var data = {
                productId: s.productId,
                jcNumber: theSched.jcNumber(),
                jcExpiry: theSched.jcExpiry(),
                pcValue: theSched.pcValue(),
                chosenSchedule: theSched.id,
                mainCourses: constructMainCourses(),
                remarks: s.remarks(),
                mainTickets: [],
                jcTickets: [],
                ccTickets: [],
                pcTickets: [],
                topups: constructTicketArr(theSched.topups())
            };

            switch (theSched.activeSection()) {
                case theSched.SECTS.STD:
                    data.mainTickets = constructTicketArr(theSched.mainTickets());
                    break;
                case theSched.SECTS.JC:
                    data.jcTickets = constructTicketArr(theSched.jcTickets());
                    break;
                case theSched.SECTS.CC:
                    data.ccTickets = constructTicketArr(theSched.ccTickets());
                    break;
                case theSched.SECTS.PC:
                    data.pcTickets = constructTicketArr(theSched.pcTickets());
                    break;
            }
            return data;
        };

        function constructMainCourses() {
            var mcArr = [];
            $.each(s.mainCourses(), function (idx, mc) {
                if (mc.qty() > 0) {
                    mcArr.push({
                        id: mc.id,
                        name: mc.name,
                        qty: Number(mc.qty())
                    });
                }
            });
            return mcArr;
        }

        function constructTicketArr(listToCheck) {
            var tixArr = [];
            $.each(listToCheck, function (idx, tix) {
                if (tix.qty() > 0) {
                    tixArr.push({
                        id: tix.id,
                        itemCode: tix.itemCode,
                        qty: tix.qty(),
                        discountId: tix.discountId
                    });
                }
            });
            return tixArr;
        }

        s.validate = function () {
            var message = '<ul>';
            if (s.type == 'General') {
                // Check date of visit
                if (s.displayDate && nvx.isEmpty(s.dateOfVisit())) {
                    message += '<li>Please specify a date of visit.</li>';
                }
                // Check that there is at least one quantity selected for
                // main/discounted tickets.
                if (!hasQty()) {
                    message += '<li>No tickets have been selected. Please select at least one ticket.</li>';
                } else {
                    if (checkHasQty(s.jcTickets())
                        && (nvx.isEmpty(s.jcNumber()) || nvx.isEmpty(s
                            .jcExpiry()))) {
                        message += '<li>Please enter your Faber Licence details to avail of Faber Licence promotions.</li>';
                    }
                    if (checkHasQty(s.pcTickets())
                        && (nvx.isEmpty(s.pcValue()))) {
                        message += '<li>Please enter a Promo Code to avail of promotional tickets.</li>';
                    }
                }
            } else {
                var theSched = null;
                $.each(s.schedules(), function (idx, sc) {
                    if (sc.id == s.currentSched()) {
                        theSched = sc;
                        return false;
                    }
                });
                if (theSched == null) {
                    message += '<li>Please select a valid Schedule.</li>';
                } else {
                    if (!theSched.hasQty()) {
                        message += '<li>No tickets have been selected. Please select at least one ticket.</li>';
                    } else {
                        if (s.hasMainCourses) {
                            var totalPax = s.computeTotalPax();
                            var totalMc = 0;
                            $.each(s.mainCourses(), function (idx, mc) {
                                totalMc += Number(mc.qty());
                            });
                            if ((totalPax != 0) && (totalMc != totalPax)) {
                                message += '<li>Please choose main courses for '
                                    + totalPax + ' diner/s.</li>';
                            }
                        }
                        if (checkHasQty(theSched.jcTickets())
                            && (nvx.isEmpty(theSched.jcNumber()) || nvx
                                .isEmpty(theSched.jcExpiry()))) {
                            message += '<li>Please enter your Faber Licence details to avail of Faber Licence promotions.</li>';
                        }
                        if (checkHasQty(theSched.pcTickets())
                            && (nvx.isEmpty(theSched.pcValue()))) {
                            message += '<li>Please enter a Promo Code to avail of promotional tickets.</li>';
                        }
                    }
                }
            }
            if (s.showRemarks()
                && !nvx.isLengthWithin(s.remarks(), 0, 500, true)) {
                message += '<li>Additional remarks cannot exceed 500 characters.</li>';
            }
            if (message == '<ul>') {
                return true;
            }
            s.errMsg(message + '</ul>');
            return false;
        };

        function hasQty() {
            return checkHasQty(s.mainTickets()) || checkHasQty(s.ccTickets())
                || checkHasQty(s.jcTickets()) || checkHasQty(s.pcTickets());
        }

        function checkHasQty(listToCheck) {
            var hasQty = false;
            $.each(listToCheck, function (idx, tix) {
                if (tix.qty() > 0) {
                    hasQty = true;
                    return false;
                }
            });
            return hasQty;
        }

        s.initialize = function (bm) {
            s.formId = mflg.FORM_ID + bm.productId;
            s.dateId = mflg.DATE_ID + bm.productId;
            s.productId = bm.productId;
            s.type = bm.type;
            s.isFnb(s.type == 'EventFestiveDining');
            s.displayDate = bm.displayDate;
            s.displayTitle = bm.displayTitle;
            s.displayDescription = bm.displayDescription;
            s.displayImagePath = bm.displayImagePath;

            s.usualPrices = bm.usualPrices;

            if (bm.descUrl != null) {
                s.descUrl = bm.descUrl;
            }

            s.minBookingDays = bm.minBookingDays;
            if ((bm.blackoutDatesArr != null)
                && (bm.blackoutDatesArr != undefined)
                && (bm.blackoutDatesArr.length > 0)) {
                s.blackoutDatesArr = bm.blackoutDatesArr;
            }
            if ((bm.maxDateArr != null) && (bm.maxDateArr[0] > 0)) {
                s.maxDateArr = bm.maxDateArr;
            }
            if ((bm.minDateArr != null) && (bm.minDateArr[0] > 0)) {
                s.minDateArr = bm.minDateArr;
            }

            s.hasCableCarClosure = bm.hasCableCarClosure;
            if (s.hasCableCarClosure) {
                s.cableCarClosures = bm.cableCarClosures;
            }
            s.hasNotes = bm.hasNotes;
            if (s.hasNotes) {
                s.notes = bm.notes;
            }
            s.tncUrl = '/tnc?id=' + s.productId;

            s.paxPerTicket = bm.paxPerTicket;
            s.topupLimitType = bm.topupLimitType;
            s.hasMainCourses = bm.hasMainCourses;
            if (s.hasMainCourses) {
                $.each(bm.mainCourses, function (idx, mc) {
                    s.mainCourses.push(new mflg.BookingMainCourse(mc));
                });
            }

            s.showRemarks(bm.showRemarks);

            s.validityType(bm.validityType);
            s.validityDays(bm.validityDays);
            s.validFrom(bm.validFrom);
            s.validUntil(bm.validUntil);

            if (s.type == 'General') {
                initAsGeneral(bm);
            } else {
                initAsEvent(bm);
            }

            initProdExtData();
            initTopUps();

            s.totalAmount = ko.computed(function () {
                var total = 0;
                if (s.type == 'General') {
                    $.each(s.mainTickets(), function (idx, tix) {
                        total += getAmt(tix.qty(), tix.unitPrice);
                    });
                    $.each(s.ccTickets(), function (idx, tix) {
                        total += getAmt(tix.qty(), tix.unitPrice);
                    });
                    $.each(s.jcTickets(), function (idx, tix) {
                        total += getAmt(tix.qty(), tix.unitPrice);
                    });
                    $.each(s.pcTickets(), function (idx, tix) {
                        total += getAmt(tix.qty(), tix.unitPrice);
                    });
                    $.each(s.topups(), function (idx, tix) {
                        total += getAmt(tix.qty(), tix.unitPrice);
                    });
                } else {
                    $.each(s.schedules(), function (idx, sc) {
                        $.each(sc.mainTickets(), function (idx, tix) {
                            total += getAmt(tix.qty(), tix.unitPrice);
                        });
                        $.each(sc.ccTickets(), function (idx, tix) {
                            total += getAmt(tix.qty(), tix.unitPrice);
                        });
                        $.each(sc.jcTickets(), function (idx, tix) {
                            total += getAmt(tix.qty(), tix.unitPrice);
                        });
                        $.each(sc.pcTickets(), function (idx, tix) {
                            total += getAmt(tix.qty(), tix.unitPrice);
                        });
                        $.each(sc.topups(), function (idx, tix) {
                            total += getAmt(tix.qty(), tix.unitPrice);
                        });
                    });
                }
                return total.toFixed(2);
            });
        };

        function initTopUps() {
            $.each(s.mainTickets(), function (mainTixIndex, mainTix) {
                if (mainTix.hasCrossSellContent()) {
                    $.each(mainTix.crossSellContent(), function (csIndex, cs) {
                        var alreadyExists = false;
                        $.each(s.topups(), function (topupIndex, topUp) {
                            if (topUp.id == cs.id && topUp.parentId() == mainTix.id) {
                                alreadyExists = true;
                                return false;
                            }
                        });
                        if (!alreadyExists) {
                            var newTopUpItem = new mflg.BookingItem(cs);
                            newTopUpItem.isTopup(true);
                            newTopUpItem.parentId(mainTix.id);
                            s.topups.push(newTopUpItem);
                        }
                    });
                }

            });

            s.organizeTopups();
        }

        s.organizeTopups = function () {
            var hasTopup = false;
            // Remove all top-ups with no main tickets
            $.each(s.topups(), function (topupIndex, topup) {
                var displayTopUp = false;
                $.each(s.mainTickets(), function (mainTixIndex, mainTix) {
                    if (mainTix.hasCrossSellContent() && mainTix.qty() > 0) {
                        $.each(mainTix.crossSellContent(), function (csIndex, cs) {
                            if (topup.id == cs.id && topup.parentId() == mainTix.id) {
                                displayTopUp = true;
                                return false;
                            }
                        });
                    }
                    if (displayTopUp) {
                        return false;
                    }
                });
                if (displayTopUp) {
                    hasTopup = true;
                } else {
                    topup.qty(0);
                }
                topup.displayItem(displayTopUp);
            });

            s.hasTopup(hasTopup);
            //console.log(s.topups());
        };

        s.prodExtData = [];

        var extRetrievalFailCount = 0;
        function initProdExtData() {
            if (extRetrievalFailCount > 3) {
                console.log('Error retrieving extended data final.');

                if (loadUrlPromoCode) {
                    s.pcValue(urlPromoCode);
                    s.checkPc();
                }

                return;
            }

            var prods = [];
            $.each(s.mainTickets(), function (idx, elem) {
                var anItem = {
                    productId: elem.id,
                    itemId: elem.itemCode
                };
                prods.push(anItem);
            });

            $.ajax({
                url: '/.store/store/products-ext',
                data: JSON.stringify({
                    products: prods
                }), headers: {'Store-Api-Channel': 'b2c-mflg'},
                type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        $.each(data.data.products, function (idx, product) {
                            s.prodExtData.push(product);
                        });
                        initEventControls();
                    } else {
                        console.log('Error retrieving extended data. Retrying. ' + data);
                        extRetrievalFailCount++;
                        initProdExtData();
                    }
                },
                error: function () {
                    console.log('Error retrieving extended data. Retrying. [General]');
                    extRetrievalFailCount++;
                    initProdExtData();
                },
                complete: function () {
                }
            });
        }

        function initEventControls() {
            $.each(s.prodExtData, function (idx, prod) {
                var isSelectDate = prod.event && (!prod.openDate || prod.singleEvent);
                if (isSelectDate) {
                    $.each(s.mainTickets(), function (idx2, ticket) {
                        if (prod.itemId == ticket.itemCode && prod.productId == ticket.id) {
                            ticket.eventGroupId(prod.eventGroupId);
                            ticket.isEvent(true);
                            var ebDate = $('#event_date_' + ticket.id);
                            ebDate.pickadate({
                                format: 'dd/mm/yyyy'
                            });
                            $.each(prod.eventLines, function (idx3, eventLine) {
                                ticket.eventLines().push(eventLine);
                                if (eventLine.eventLineId == prod.defaultEventLineId) {
                                    ticket.eventSession(eventLine);
                                }
                            });
                            if (prod.defaultEventLineId == "" && ticket.eventLines().length > 0) {
                                ticket.eventSession(ticket.eventLines()[0]);
                            }
                        }
                    });
                }
            });

            if (loadUrlPromoCode) {
                s.pcValue(urlPromoCode);
                s.checkPc();
            }
        }


        function initAsGeneral(bm) {
            $.each(bm.mainTickets, function (idx, tix) {
                s.mainTickets.push(new mflg.BookingItem(tix));
            });

            s.hasPromotion = bm.hasPromotion;
            if (s.hasPromotion) {
                s.hasCcPromo = bm.hasCcPromo;
                s.hasJcPromo = bm.hasJcPromo;
                s.hasPcPromo = bm.hasPcPromo;
                if (s.hasCcPromo) {
                    $.each(bm.ccTickets, function (idx, tix) {
                        s.ccTickets.push(new mflg.BookingItem(tix,
                            s.usualPrices));
                    });
                }
                s.jcTickets = ko.observableArray([]);
                s.pcTickets = ko.observableArray([]);
            }

            //s.hasTopup = bm.hasTopup;
            //if (s.hasTopup) {
            //    $.each(bm.topups, function(idx, topup) {
            //        s.topups.push(new mflg.BookingItem(topup));
            //    });
            //}
        }

        function initAsEvent(bm) {
            $.each(bm.schedules, function (idx, sched) {
                var bsModel = new mflg.BookingSchedule(s,
                    bm.paxPerTicket == 1 ? '' : bm.paxPerTicket);
                bsModel.init(s.productId, sched);
                s.schedules.push(bsModel);
            });
            if (s.schedules.length > 0) {
                s.currentSched(s.schedules()[0].id);
            }
        }

        s.displayPromoCodeTop = ko.computed(function () {
            if (s.currentSched() == -1) {
                return s.hasPcPromo;
            } else {
                var theSched = null;
                $.each(s.schedules(), function (idx, sc) {
                    if (sc.id == s.currentSched()) {
                        theSched = sc;
                        return false;
                    }
                });
                if (theSched) {
                    return theSched.hasPcPromo;
                }
                return false;
            }
        });

    };

    mflg.BookingSchedule = function (mainForm, paxPerTicket) {
        var s = this;

        s.productId = -1;
        s.id = -1;
        s.scheduleDate = null;
        s.scheduleName = '';
        s.dateNA = false;

        s.paxPerTicket = paxPerTicket;

        s.isActive = ko.observable(false);

        s.mainTickets = ko.observableArray([]);
        s.hasPromotion = false;
        s.hasCcPromo = false;
        s.hasJcPromo = false;
        s.hasPcPromo = false;
        s.ccTickets = ko.observableArray([]);
        s.jcTickets = ko.observableArray([]);
        s.pcTickets = ko.observableArray([]);
        s.hasTopup = ko.observable(false);
        s.topups = ko.observableArray([]);

        s.usualPrices = {};

        s.parent = mainForm;
        s.SECTS = s.parent.SECTS;
        s.activeSection = ko.observable(s.SECTS.STD);
        s.changeActiveSection = function (theSect) {
            s.activeSection(theSect);
        };
        s.activeSection.subscribe(function (newVal) {
            if (newVal != s.SECTS.STD) {
                $.each(s.mainTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.JC) {
                $.each(s.jcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.CC) {
                $.each(s.ccTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.PC) {
                $.each(s.pcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
        });

        s.jcNumber = ko.observable('');
        s.jcExpiryMon = ko.observable('');
        s.jcExpiryYr = ko.observable('');
        s.jcExpiry = ko.computed(function () {
            return s.jcExpiryMon() + '/' + s.jcExpiryYr();
        });
        s.pcValue = ko.observable('');

        s.checkJc = function () {
            s.parent.isErr(false);
            if (nvx.isEmpty(s.jcNumber())) {
                s.parent.isErr(true);
                s.parent.errMsg('Please enter a Faber Licence number.');
                s.parent.scrollToErr();
                return;
            }
            mflg.spinner.start();
            $
                .ajax({
                    url: '/booking/check-jc',
                    data: {
                        productId: s.productId,
                        jcNumber: s.jcNumber(),
                        jcExpiry: s.jcExpiry(),
                        schedId: s.id
                    },
                    type: 'GET',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshJcTickets(data.jcTickets);
                            s.activeSection(s.SECTS.JC);
                        } else {
                            s.refreshJcTickets([]);
                            s.parent.isErr(true);
                            s.parent.errMsg(data.message);
                            s.parent.scrollToErr();
                        }
                    },
                    error: function () {
                        s.refreshJcTickets([]);
                        s.parent.isErr(true);
                        s.parent
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.parent.scrollToErr();
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.refreshJcTickets = function (jcTickets) {
            var jcArr = [];
            $.each(jcTickets, function (idx, tix) {
                jcArr.push(new mflg.BookingItem(tix, s.usualPrices,
                    s.paxPerTicket));
            });
            s.jcTickets(jcArr);
        };

        s.checkPc = function (fromTop) {
            s.parent.isErr(false);
            if (nvx.isEmpty(s.pcValue())) {
                s.parent.isErr(true);
                s.parent.errMsg('Please enter a promo code.');
                s.parent.scrollToErr(fromTop);
                return;
            }

            // WARNING: Unused
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/store/check-promo-code',
                    data: JSON.stringify({
                        fromHeader: false,
                        productId: s.productId,
                        promoCode: s.pcValue(),
                        isEvent: true,
                        schedId: s.id
                    }),
                    headers: {'Store-Api-Channel': 'b2c-mflg'},
                    type: 'POST', cache: false, dataType: 'json', contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshPcTickets(data);
                            s.activeSection(s.SECTS.PC);
                            if (fromTop == true) {
                                var topOfErr = $('#promo-code-tbody')
                                    .offset().top;
                                $('html, body').animate({
                                    scrollTop: topOfErr
                                }, 500); // account
                                // for
                                // the
                                // height
                                // of
                                // navbar
                            }
                        } else {
                            s.refreshPcTickets([]);
                            s.parent.isErr(true);
                            s.parent.errMsg(data.message);
                            s.parent.scrollToErr(fromTop);
                        }
                    },
                    error: function () {
                        s.refreshPcTickets([]);
                        s.parent.isErr(true);
                        s.parent
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.parent.scrollToErr(fromTop);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        // WARNING: Unused
        s.refreshPcTickets = function (data) {
            var pcArr = [];
            $.each(data.updatedTickets, function (idx, tix) {
                // TODO: Implement update tickets
            });
            $.each(data.unlockedTickets, function (idx, tix) {
                var bookingItemModel = s.convertAxProductToBookingItem(tix);
                bookingItemModel.discountCode(data.promoCode);
                pcArr.push(new mflg.BookingItem(bookingItemModel, s.usualPrices,
                    s.paxPerTicket));
            });
            s.pcTickets(pcArr);
        };

        // WARNING: Unused
        s.convertAxProductToBookingItem = function (axProduct) {
            var bookingItem = {
                "discountId": null,
                "discountPrice": null,
                "displayDesc": axProduct.productDesc,
                "displayName": axProduct.productName,
                "displayPrice": axProduct.productPrice,
                "id": axProduct.productListingId,
                "itemCode": axProduct.displayProductNumber,
                "link": "",
                "merchantId": null,
                "originalPrice": null,
                "priceType": null,
                "qty": 0,
                "ticketType": axProduct.getTicketType,
                "unitPrice": axProduct.productPrice,
                "promotionContent": [{
                    "promotionIconImageURL": axProduct.promotionIconImageURL,
                    "promotionText": axProduct.promotionText,
                    "additionalInfo": axProduct.additionalInfo,
                    "cachedDiscountPrice": axProduct.cachedDiscountPrice
                }],
            };
            return bookingItem;
        };

        s.hasQty = function () {
            return checkHasQty(s.mainTickets()) || checkHasQty(s.ccTickets())
                || checkHasQty(s.jcTickets()) || checkHasQty(s.pcTickets());
        };
        function checkHasQty(listToCheck) {
            var hasQty = false;
            $.each(listToCheck, function (idx, tix) {
                if (tix.qty() > 0) {
                    hasQty = true;
                    return false;
                }
            });
            return hasQty;
        }

        s.init = function (productId, bs) {
            s.productId = productId;
            s.id = bs.id;
            s.scheduleDate = bs.scheduleDate;
            s.scheduleName = bs.scheduleName;
            s.dateNA = bs.dateNA;

            s.usualPrices = bs.usualPrices;

            s.isActive((bs.mainTickets != null) && (bs.mainTickets.length > 0));
            if (!s.isActive()) {
                return;
            }

            $.each(bs.mainTickets, function (idx, tix) {
                s.mainTickets.push(new mflg.BookingItem(tix, undefined,
                    s.paxPerTicket));
            });

            s.hasPromotion = bs.hasPromotion;
            if (s.hasPromotion) {
                s.hasCcPromo = bs.hasCcPromo;
                s.hasJcPromo = bs.hasJcPromo;
                s.hasPcPromo = bs.hasPcPromo;
                if (s.hasCcPromo) {
                    $.each(bs.ccTickets, function (idx, tix) {
                        s.ccTickets.push(new mflg.BookingItem(tix,
                            s.usualPrices, s.paxPerTicket));
                    });
                }
                s.jcTickets = ko.observableArray([]);
                s.pcTickets = ko.observableArray([]);
            }

            //s.hasTopup = bs.hasTopup;
            //if (s.hasTopup) {
            //    $.each(bs.topups, function(idx, topup) {
            //        s.topups.push(new mflg.BookingItem(topup));
            //    });
            //}
        };
    };

    mflg.BookingMainCourse = function (bmc) {
        var s = this;

        s.id = bmc.id;
        s.name = bmc.name;
        s.qty = ko.observable('');
    };

    mflg.BookingItem = function (bi, usualPrices, pax) {
        var s = this;

        s.id = bi.id;
        s.itemCode = bi.itemCode;
        s.ticketType = bi.ticketType;
        s.displayName = bi.displayName;
        s.displayPrice = bi.displayPrice;
        s.displayDesc = bi.displayDesc;
        s.link = bi.link;
        s.unitPrice = bi.unitPrice;
        s.usualPrice = '';
        if (usualPrices) {
            s.usualPrice = usualPrices[s.ticketType];
        }
        s.pax = '';
        if (pax) {
            s.pax = pax;
        }
        s.discountId = bi.discountId;
        s.discountCode = ko.observable('');
        s.discountCode(bi.discountCode);

        s.priceType = bi.priceType;
        s.discountPrice = bi.discountPrice;
        s.originalPrice = bi.originalPrice;

        s.tncId = 0;

        s.qty = ko.observable(0);

        s.groupSize = bi.groupSize != null ? bi.groupSize : 1;
        s.displayGroupName = bi.displayGroupName != null ? bi.displayGroupName : true;
        s.inGroup = bi.inGroup != null ? bi.inGroup : false;
        s.typeAndPrice = s.ticketType + ' - ' + s.displayPrice;
        s.hideDisplayName = s.inGroup && !s.displayGroupName;

        s.isEvent = ko.observable(false);
        s.eventGroupId = ko.observable('');
        s.showSelectDate = ko.observable(false);
        s.eventDate = ko.observable('');
        s.eventSession = ko.observable();
        s.eventLines = ko.observableArray();

        s.parentId = ko.observable("");
        s.isTopup = ko.observable(false);
        s.displayItem = ko.observable(true);

        s.showPromoRows = ko.observable(false);
        s.toggleShowPromoRows = function () {
            if (s.showPromoRows()) {
                s.showPromoRows(false);
            } else {
                s.showPromoRows(true);
            }
        };

        s.toggleSelectDate = function () {
            if (s.showSelectDate()) {
                s.qty(0);
                s.showSelectDate(false);
            } else {
                console.log(s.eventSession());
                console.log(s.eventLines());
                s.showSelectDate(true);
            }
        };

        s.qty.subscribe(function (newValue) {
            if (!s.isTopup()) {
                mflg.bm.organizeTopups();
            }
        });

        s.eventSession.subscribe(function (newValue) {
            var eventDates = newValue.eventDates;
            var enableDates = [];
            enableDates.push(true);
            $.each(eventDates, function (idx, eventDate) {
                var dtArr = eventDate.date.split('/');
                enableDates.push([Number(dtArr[2]), Number(dtArr[1]) - 1, Number(dtArr[0])]);
            });
            var ebDate = $('#event_date_' + s.id);
            ebDate.pickadate().pickadate('picker').stop();
            setTimeout(function () {
                ebDate.pickadate({
                    onStart: function() {
                        if(eventDates.length == 1) {
                            this.set('select', enableDates[1]);
                        }
                    },
                    format: 'dd/mm/yyyy', disable: enableDates
                });
            }, 600);
        });

        s.promotionContent = ko.observableArray();
        s.hasPromotionContent = ko.computed(function () {
            return s.promotionContent().length > 0;
        });

        if (bi.promotionContent) {
            $.each(bi.promotionContent, function (idx, pc) {
                s.promotionContent.push(pc);
            });
        }


        s.crossSellContent = ko.observableArray();
        s.hasCrossSellContent = ko.computed(function () {
            return s.crossSellContent().length > 0;
        });

        if (bi.crossSellContent) {
            $.each(bi.crossSellContent, function (idx, pc) {
                s.crossSellContent.push(pc);
            });
        }

        s.visible = ko.observable(false);
    };

})(window.mflg = window.mflg || {}, jQuery);