$(document).ready(
    function () {

        mflg.mainPath = window.location.pathname;

        if (cmsBookingModel != null) {
            mflg.bm = new mflg.BookingModel();
            mflg.bm.initialize(cmsBookingModel.bookingModel);
            ko.applyBindings(mflg.bm, document.getElementById('productDiv'));
        }

        mflg.updateCartQty();
    });

(function (mflg, $) {

    mflg.FORM_ID = 'bookingForm_';
    mflg.DATE_ID = 'dateOfVisit_';

    mflg.REMARKETING = {
        36: '<img src="https://avn.innity.com/rt/?cb=1418367393&a=1450&e=8760" width="0" height="0" border="0"/>'
    };

    mflg.BookingModel = function () {
        var s = this;

        s.formId = '';
        s.dateId = '';
        s.productId = -1;
        s.type = '';

        s.displayDate = true;
        s.displayTitle = '';
        s.displayDescription = '';
        s.displayImagePath = '';
        s.descUrl = '';

        s.hasCableCarClosure = false;
        s.cableCarClosures = [];

        s.hasMainCourses = ko.observable(false);
        s.mainCourses = ko.observableArray([]);
        s.paxPerTicket = 1;
        s.minPax = ko.observable(0);
        s.maxPax = ko.observable(0);
        s.topupLimitType = '';

        s.hasNotes = false;
        s.notes = '';

        s.tncUrl = '';

        s.usualPrices = {};

        s.mainTickets = ko.observableArray([]);
        s.hasPromotion = false;
        s.hasCcPromo = false;
        s.hasJcPromo = false;
        s.hasPcPromo = false;
        s.ccTickets = ko.observableArray([]);
        s.jcTickets = ko.observableArray([]);
        s.pcTickets = ko.observableArray([]);
        s.hasTopup = false;
        s.topups = ko.observableArray([]);
        s.isFnb = ko.observable(false);

        s.dateOfVisit = ko.observable('');
        s.dateOfVisit.subscribe(function (newValue) {
            getAvailableSessions(s.productId, newValue);
        });
        s.timeOfVisit = ko.observable('');
        s.skyDiningSession = ko.observable(0);
        s.hasSessions = ko.observable(false);
        s.availableSessions = ko.observableArray([]);

        s.showRemarks = ko.observable(false);
        s.remarks = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable('');

        s.minBookingDays = '';
        s.blackoutDatesArr = null;
        s.maxDateArr = null;
        s.minDateArr = null;

        s.hasTheme = false;
        s.themes = ko.observableArray([]);
        s.themeId = ko.observable(0);

        s.gst = ko.observable(0);
        s.serviceCharge = ko.observable(0);
        s.menuPrice = ko.observable(0);
        s.menuId = ko.observable(0);
        s.discountId = ko.observable(0);
        s.merchantId = ko.observable(0);

        s.pcValue = ko.observable('');
        s.jcNumber = ko.observable('');
        s.jcExpiryMon = ko.observable('');
        s.jcExpiryYr = ko.observable('');
        s.jcExpiry = ko.computed(function () {
            return s.jcExpiryMon() + '/' + s.jcExpiryYr();
        });
        s.SECTS = {
            STD: 'std',
            CC: 'cc',
            PC: 'pc',
            JC: 'jc'
        };
        s.activeSection = ko.observable(s.SECTS.STD);
        s.changeActiveSection = function (theSect) {
            s.activeSection(theSect);
            s.scrollToActiveMenu(theSect == 'std' ? theSect : 'promo');
        };
        s.activeSection.subscribe(function (newVal) {
            if (newVal != s.SECTS.STD) {
                $.each(s.mainTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.JC) {
                $.each(s.jcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.CC) {
                $.each(s.ccTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
            if (newVal != s.SECTS.PC) {
                $.each(s.pcTickets(), function (idx, itm) {
                    itm.qty(0);
                });
            }
        });

        s.showSelectSession = ko.computed(function () {
            return s.availableSessions().length > 0;
        });

        s.showNoSessions = ko.computed(function () {
            return !s.showSelectSession();
        });

        s.noSessionsText = ko
            .computed(function () {
                if (s.dateOfVisit() == '') {
                    return "Please select a date.";
                }
                return "No sessions available for this date. Please select a different date.";
            });

        s.totalAmount = null;

        function getAmt(qty, unitPrice) {
            if (qty > 0) {
                return Number(qty) * Number(unitPrice);
            }
            return 0;
        }

        s.scrollToActiveMenu = function (theSect) {
            var activeMenu;
            if (theSect) {
                activeMenu = $('#' + theSect + '-header').offset().top;
            } else {
                activeMenu = $('#' + s.activeSection() + '-header').offset().top;
            }
            $('html, body').animate({
                scrollTop: activeMenu
            }, 0);
        };

        s.scrollToErr = function (fromTop) {
            var topOfErr = $('#error-section').offset().top;
            if (fromTop) {
                if (topOfErr > 1000) {
                    topOfErr = 330;
                }
            }
            $('html, body').animate({
                scrollTop: topOfErr - 85
            }, 500); // account
            // for
            // the
            // height
            // of
            // navbar
        };

        s.acceptsWarnings = function () {
            if (s.topupLimitType == 'LimitToMainTicket') {
                var qtyMap = {
                    'Adult': 0,
                    'Child': 0,
                    'Standard': 0
                };
                var topupQtyMap = {
                    'Adult': 0,
                    'Child': 0,
                    'Standard': 0
                };

                function iterAndAddTicketsTyped(idx, tix) {
                    qtyMap[tix.ticketType] = qtyMap[tix.ticketType]
                        + Number(tix.qty());
                }

                $.each(s.mainTickets(), iterAndAddTicketsTyped);
                $.each(s.topups(), function (idx, tix) {
                    topupQtyMap[tix.ticketType] = topupQtyMap[tix.ticketType]
                        + Number(tix.qty());
                });

                var xAdult = qtyMap.Adult > topupQtyMap.Adult, xChild = qtyMap.Child > topupQtyMap.Child, xStd = qtyMap.Standard > topupQtyMap.Standard;
                if (!xAdult && !xChild && !xStd) {
                    return true;
                }
                return confirm('You have selected '
                    + (xAdult ? qtyMap.Adult
                    + ' adult main ticket(s) but only '
                    + topupQtyMap.Adult + ' adult topup(s); ' : '')
                    + (xChild ? qtyMap.Child
                    + ' child main ticket(s) but only '
                    + topupQtyMap.Child + ' child topup(s); ' : '')
                    + (xStd ? qtyMap.Standard
                    + ' standard main ticket(s) but only '
                    + topupQtyMap.Standard + ' standard topup(s); '
                        : '') + 'is this correct?');
            }
            return true;
        };

        s.addToCart = function () {
            s.isErr(false);
            if (!s.validate()) {
                s.isErr(true);
                s.scrollToErr();
                return false;
            }
            if (!s.acceptsWarnings()) {
                return false;
            }
            var mainCourses = [];
            $.each(mflg.bm.mainCourses(), function (idx, mc) {
                if (mc.qty() > 0) {
                    var mainCourse = {
                        menuId: mc.id,
                        menuName: mc.name,
                        qty: mc.qty()
                    };
                    mainCourses.push(mainCourse);
                }
            });
            var topUps = [];
            $.each(mflg.bm.topups(), function (idx, tu) {
                if (tu.qty() > 0) {
                    var topUp = {
                        topUpId: tu.id,
                        topUpName: tu.displayName,
                        qty: tu.qty()
                    };
                    topUps.push(topUp);
                }
            });
            var themeName = "";
            if (mflg.bm.hasTheme) {
                $.each(mflg.bm.themes(), function (idx, theme) {
                    if (theme.selected()) {
                        themeName = theme.displayDesc;
                    }
                });
            }
            var sessionName = "";
            $.each(mflg.bm.availableSessions(), function (idx, session) {
                if (session.id = mflg.bm.skyDiningSession()) {
                    sessionName = session.time;
                }
            });
            var mainItems = [];
            $.each(mflg.bm.mainTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    var mainItem = {
                        cmsProductName: mflg.bm.displayTitle,
                        cmsProductId: mflg.bm.productId,
                        listingId: tix.id,
                        productCode: tix.itemCode,
                        name: tix.displayName,
                        subName: tix.subName,
                        type: tix.ticketType,
                        qty: tix.qty(),
                        price: tix.unitPrice,
                        eventGroupId: mflg.bm.skyDiningSession(),
                        eventLineId: mflg.bm.skyDiningSession(),
                        eventSessionName: sessionName,
                        selectedEventDate: mflg.bm.dateOfVisit(),
                        mainCourses: mainCourses,
                        topUps: topUps,
                        remarks: mflg.bm.remarks(),
                        themeId: mflg.bm.themeId(),
                        themeName: themeName,
                        merchantId: mflg.bm.merchantId(),
                        promoCode: mflg.bm.pcValue(),
                        jewelCard: mflg.bm.jcNumber(),
                        jewelCardExpiry: mflg.bm.jcExpiry(),
                        discountId: tix.discountId
                    };
                    mainItems.push(mainItem);
                }
            });
            $.each(mflg.bm.pcTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    var mainItem = {
                        cmsProductName: mflg.bm.displayTitle,
                        cmsProductId: mflg.bm.productId,
                        listingId: tix.id,
                        productCode: tix.itemCode,
                        name: tix.displayName,
                        subName: tix.subName,
                        type: tix.ticketType,
                        qty: tix.qty(),
                        price: tix.unitPrice,
                        eventGroupId: mflg.bm.skyDiningSession(),
                        eventLineId: mflg.bm.skyDiningSession(),
                        eventSessionName: sessionName,
                        selectedEventDate: mflg.bm.dateOfVisit(),
                        mainCourses: mainCourses,
                        topUps: topUps,
                        remarks: mflg.bm.remarks(),
                        themeId: mflg.bm.themeId(),
                        themeName: themeName,
                        merchantId: mflg.bm.merchantId(),
                        promoCode: mflg.bm.pcValue(),
                        jewelCard: mflg.bm.jcNumber(),
                        jewelCardExpiry: mflg.bm.jcExpiry(),
                        discountId: tix.discountId
                    };
                    mainItems.push(mainItem);
                }
            });
            $.each(mflg.bm.jcTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    var mainItem = {
                        cmsProductName: mflg.bm.displayTitle,
                        cmsProductId: mflg.bm.productId,
                        listingId: tix.id,
                        productCode: tix.itemCode,
                        name: tix.displayName,
                        subName: tix.subName,
                        type: tix.ticketType,
                        qty: tix.qty(),
                        price: tix.unitPrice,
                        eventGroupId: mflg.bm.skyDiningSession(),
                        eventLineId: mflg.bm.skyDiningSession(),
                        eventSessionName: sessionName,
                        selectedEventDate: mflg.bm.dateOfVisit(),
                        mainCourses: mainCourses,
                        topUps: topUps,
                        remarks: mflg.bm.remarks(),
                        themeId: mflg.bm.themeId(),
                        themeName: themeName,
                        merchantId: mflg.bm.merchantId(),
                        promoCode: mflg.bm.pcValue(),
                        jewelCard: mflg.bm.jcNumber(),
                        jewelCardExpiry: mflg.bm.jcExpiry(),
                        discountId: tix.discountId
                    };
                    mainItems.push(mainItem);
                }
            });
            $.each(mflg.bm.ccTickets(), function (idx, tix) {
                if (tix.qty() > 0) {
                    var mainItem = {
                        cmsProductName: mflg.bm.displayTitle,
                        cmsProductId: mflg.bm.productId,
                        listingId: tix.id,
                        productCode: tix.itemCode,
                        name: tix.displayName,
                        subName: tix.subName,
                        type: tix.ticketType,
                        qty: tix.qty(),
                        price: tix.unitPrice,
                        eventGroupId: mflg.bm.skyDiningSession(),
                        eventLineId: mflg.bm.skyDiningSession(),
                        eventSessionName: sessionName,
                        selectedEventDate: mflg.bm.dateOfVisit(),
                        mainCourses: mainCourses,
                        topUps: topUps,
                        remarks: mflg.bm.remarks(),
                        themeId: mflg.bm.themeId(),
                        themeName: themeName,
                        merchantId: mflg.bm.merchantId(),
                        promoCode: mflg.bm.pcValue(),
                        jewelCard: mflg.bm.jcNumber(),
                        jewelCardExpiry: mflg.bm.jcExpiry(),
                        discountId: tix.discountId
                    };
                    mainItems.push(mainItem);
                }
            });
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/sky-dining/add-to-cart',
                    data: JSON.stringify({
                        sessionId: '',
                        skyDiningItems: mainItems
                    }),
                    headers: {
                        'Store-Api-Channel': 'b2c-mflg'
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            window.location.href = "../checkout-sky-dining";
                        } else {
                            s.errMsg(data.message);
                            s.isErr(true);
                            s.scrollToErr();
                            mflg.spinner.stop();
                        }
                    },
                    error: function () {
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                        s.scrollToErr();
                        mflg.spinner.stop();
                    }
                });
        };

        s.checkSessionCapacity = function () {
            if (checkSessionModel.success) {
                mflg.spinner.stop();
                s.showDateSelection(false);
                if (s.hasTheme) {
                    s.showThemeSelection(true);
                    s.showMenuSelection(false);
                    s.showTopUpSelection(false);
                    s.bookingStage(1);
                } else {
                    s.showThemeSelection(false);
                    s.showMenuSelection(true);
                    s.showTopUpSelection(false);
                    s.bookingStage(2);
                }
            } else {
                s.errMsg(checkSessionModel.message);
                s.isErr(true);
                s.scrollToErr();
                mflg.spinner.stop();
            }
        };
        s.checkThemeCapacity = function () {
            if (checkThemeModel.success) {
                mflg.spinner.stop();
                s.showDateSelection(false);
                s.showThemeSelection(false);
                s.showMenuSelection(true);
                s.showTopUpSelection(false);
                s.bookingStage(2);
            } else {
                s.errMsg(checkThemeModel.message);
                s.isErr(true);
                s.scrollToErr();
                mflg.spinner.stop();
            }
        };
        s.constructDataObj = function () {
            var data = {
                id: 0,
                dateText: s.dateOfVisit(),
                packageId: s.productId,
                themeId: s.hasTheme ? s.themeId() : null,
                sessionId: s.skyDiningSession(),
                discountId: s.discountId(),
                menuId: s.menuId(),
                specialRequest: s.remarks(),
                mainCourseIds: getMainCourseIds(),
                mainCourseQuantities: getMainCourseQuantities(),
                topUpIds: getTopUpIds(),
                topUpQuantities: getTopUpQuantities(),
                merchantId: s.merchantId(),
                promoCode: s.pcValue(),
                jewelCard: s.jcNumber(),
                jewelCardExpiry: s.jcExpiry()
            };
            return data;
        };

        function getMainCourseIds() {
            var idArr = [];
            $.each(s.mainCourses(), function (idx, mc) {
                if (mc.qty() > 0) {
                    idArr.push(mc.id);
                }
            });
            return idArr;
        }

        function getMainCourseQuantities() {
            var idArr = [];
            $.each(s.mainCourses(), function (idx, mc) {
                if (mc.qty() > 0) {
                    idArr.push(mc.qty());
                }
            });
            return idArr;
        }

        function getTopUpIds() {
            var idArr = [];
            $.each(s.topups(), function (idx, mc) {
                if (mc.qty() > 0) {
                    idArr.push(mc.id);
                }
            });
            return idArr;
        }

        function getTopUpQuantities() {
            var idArr = [];
            $.each(s.topups(), function (idx, mc) {
                if (mc.qty() > 0) {
                    idArr.push(mc.qty());
                }
            });
            return idArr;
        }

        function getAvailableSessions(productId, sessionDate) {
            mflg.spinner.start();
            $.ajax({
                url: '/.store/sky-dining/get-available-sessions',
                data: JSON.stringify({
                    packageId: productId,
                    dateOfVisitText: sessionDate
                }),
                headers: {
                    'Store-Api-Channel': 'b2c-mflg'
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        s.hasSessions(data.total > 0);
                        s.availableSessions.removeAll();
                        $.each(data.data,
                            function (index, value) {
                                s.availableSessions.push(value);
                            });
                        s.skyDiningSession(0);
                        s.errMsg('');
                        s.isErr(false);
                    } else {
                        s.hasSessions(false);
                        s.errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                        s.scrollToErr();
                    }
                },
                error: function () {
                    s.errMsg('Unable to process your request. Please try again in a few moments.');
                    s.isErr(true);
                    s.scrollToErr();
                },
                complete: function () {
                    mflg.spinner.stop();
                }
            });

        }

        s.validate = function () {
            var message = '<ul>';
            // Check date of visit
            if (s.displayDate && nvx.isEmpty(s.dateOfVisit())) {
                message += '<li>Please specify a date of visit.</li>';
            }

            if (nvx.isEmpty(s.skyDiningSession())) {
                message += '<li>Please specify a sky dining session.</li>';
            }

            if (s.hasTheme && !isThemeSelected()) {
                message += '<li>No theme has been selected. Please select a theme.</li>';
            }
            // Check that there is at least one quantity selected for
            // main/discounted tickets.
            if (!hasQty()) {
                message += '<li>No menu has been selected. Please select a menu.</li>';
            } else {
                var selectedMc = 0;
                $.each(s.mainCourses(), function (idx, itm) {
                    selectedMc += Number(itm.qty());
                });
                if (selectedMc < s.minPax()) {
                    if (s.minPax() == 1) {
                        message += '<li>Please select at least one main course.</li>';
                    } else {
                        message += '<li>Please select at least ' + s.minPax()
                            + ' main courses.</li>';
                    }
                } else if (selectedMc > s.maxPax()) {
                    if (s.maxPax() == 1) {
                        message += '<li>Please select no more than one main course.</li>';
                    } else {
                        message += '<li>Please select no more than '
                            + s.maxPax() + ' main courses.</li>';
                    }
                }
            }

            if (s.showRemarks()
                && !nvx.isLengthWithin(s.remarks(), 0, 500, true)) {
                message += '<li>Additional remarks cannot exceed 500 characters.</li>';
            }
            if (message == '<ul>') {
                return true;
            }
            s.errMsg(message + '</ul>');
            return false;
        };

        function hasQty() {
            return checkHasQty(s.mainTickets()) || checkHasQty(s.ccTickets())
                || checkHasQty(s.jcTickets()) || checkHasQty(s.pcTickets());
        }

        function checkHasQty(listToCheck) {
            var hasQty = false;
            $.each(listToCheck, function (idx, tix) {
                if (tix.qty() > 0) {
                    hasQty = true;
                    return false;
                }
            });
            return hasQty;
        }

        s.initialize = function (bm) {
            s.formId = mflg.FORM_ID + bm.productId;
            s.dateId = mflg.DATE_ID + bm.productId;
            s.productId = bm.productId;
            s.type = bm.type;
            s.isFnb(s.type == 'EventFestiveDining');
            s.displayDate = bm.displayDate;
            s.displayTitle = bm.displayTitle;
            s.displayDescription = bm.displayDescription;
            s.displayImagePath = bm.displayImagePath;

            s.usualPrices = bm.usualPrices;

            if (bm.descUrl != null) {
                s.descUrl = bm.descUrl;
            }

            s.minBookingDays = bm.minBookingDays;
            if (((((bm.blackoutDatesArr != null))))
                && ((((bm.blackoutDatesArr != undefined))))
                && ((((bm.blackoutDatesArr.length > 0))))) {
                s.blackoutDatesArr = bm.blackoutDatesArr;
            }
            if (((((bm.maxDateArr != null)))) && ((((bm.maxDateArr[0] > 0))))) {
                s.maxDateArr = bm.maxDateArr;
            }
            if (((((bm.minDateArr != null)))) && ((((bm.minDateArr[0] > 0))))) {
                s.minDateArr = bm.minDateArr;
            }

            s.hasCableCarClosure = bm.hasCableCarClosure;
            if (s.hasCableCarClosure) {
                s.cableCarClosures = bm.cableCarClosures;
            }
            s.hasNotes = bm.hasNotes;
            if (s.hasNotes) {
                s.notes = bm.notes;
            }
            s.tncUrl = '/sky-dining-tnc?id=' + s.productId;

            s.paxPerTicket = bm.paxPerTicket;
            s.minPax(bm.minPax);
            s.maxPax(bm.maxPax);
            initMainCourseQtyOptions(bm.maxPax);
            s.topupLimitType = bm.topupLimitType;
            s.hasMainCourses(bm.hasMainCourses);
            if (s.hasMainCourses()) {
                $.each(bm.mainCourses, function (idx, mc) {
                    s.mainCourses.push(new mflg.BookingMainCourse(mc));
                });
            }

            s.showRemarks(bm.showRemarks);

            initAsGeneral(bm);
            s.totalAmount = ko.computed(function () {
                var total = s.menuPrice();
                var selectedMc = 0;
                $.each(s.mainCourses(), function (idx, itm) {
                    selectedMc += Number(itm.qty());
                });
                if (selectedMc < s.minPax()) {
                    total *= s.minPax();
                } else {
                    total *= selectedMc;
                }
                var menuTotal = total;
                var scTotal = menuTotal * (s.serviceCharge() / 100);
                var gstTotal = (menuTotal + scTotal) * (s.gst() / 100);

                total += scTotal;
                total += gstTotal;

                $.each(s.topups(), function (idx, tix) {
                    total += getAmt(tix.qty(), tix.unitPrice);
                });
                return total.toFixed(2);
            });

            s.subTotalAmount = ko.computed(function () {
                var total = s.menuPrice();
                var selectedMc = 0;
                $.each(s.mainCourses(), function (idx, itm) {
                    selectedMc += Number(itm.qty());
                });
                if (selectedMc < s.minPax()) {
                    total *= s.minPax();
                } else {
                    total *= selectedMc;
                }
                return total.toFixed(2);
            });

            s.serviceChargeAmount = ko.computed(function () {
                var total = 0;
                total = s.subTotalAmount() * (s.serviceCharge() / 100);
                return total.toFixed(2);
            });

            s.gstAmount = ko.computed(function () {
                var total = 0;
                total = (Number(s.subTotalAmount()) + Number(s
                        .serviceChargeAmount()))
                    * (s.gst() / 100);
                return total.toFixed(2);
            });

            s.topUpAmount = ko.computed(function () {
                var total = 0;
                $.each(s.topups(), function (idx, tix) {
                    total += getAmt(tix.qty(), tix.unitPrice);
                });
                return total.toFixed(2);
            });

            initCalendar(bm);
        };

        function initAsGeneral(bm) {
            $.each(bm.sdMenus, function (idx, tix) {
                s.mainTickets.push(new mflg.BookingItem(tix));
            });

            s.hasPromotion = bm.hasPromotion;
            if (s.hasPromotion) {
                s.hasCcPromo = bm.hasCcPromo;
                s.hasJcPromo = bm.hasJcPromo;
                s.hasPcPromo = bm.hasPcPromo;
                if (s.hasCcPromo) {
                    $.each(bm.sdCcTickets, function (idx, tix) {
                        s.ccTickets.push(new mflg.BookingItem(tix,
                            s.usualPrices));
                    });
                }
                s.jcTickets = ko.observableArray([]);
                s.pcTickets = ko.observableArray([]);
            }

            s.hasTopup = bm.hasTopup;
            if (s.hasTopup) {
                $.each(bm.sdTopUps, function (idx, topup) {
                    s.topups.push(new mflg.BookingItem(topup));
                });
            }

            s.hasTheme = bm.hasTheme;
            if (s.hasTheme) {
                $.each(bm.sdThemes, function (idx, theme) {
                    s.themes.push(new mflg.Theme(theme));
                });
            }
        }

        function isThemeSelected() {
            var selected = false;
            $.each(s.themes(), function (idx, tix) {
                var test = tix.selected();
                if (tix.selected()) {
                    selected = true;
                }
            });
            return selected;
        }

        s.checkJc = function () {
            s.isErr(false);
            if (nvx.isEmpty(s.jcNumber())) {
                s.isErr(true);
                s.errMsg('Please enter a Faber Licence number.');
                s.scrollToErr();
                return;
            }
            var isEventMan = s.type.slice(0, 'Event'.length) == 'Event';
            var schedId = isEventMan ? s.currentSched() : -1;
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/sky-dining/check-sky-dining-jc',
                    data: JSON.stringify({
                        productId: s.productId,
                        jcNumber: s.jcNumber(),
                        jcExpiry: s.jcExpiry()
                    }),
                    headers: {
                        'Store-Api-Channel': 'b2c-mflg'
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshJcTickets(data.data.tickets);
                            s.changeActiveSection(s.SECTS.JC);
                        } else {
                            s.refreshJcTickets([]);
                            s.isErr(true);
                            s.errMsg(data.message);
                            s.scrollToErr();
                        }
                    },
                    error: function () {
                        s.refreshJcTickets([]);
                        s.isErr(true);
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.scrollToErr();
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.checkPc = function (fromTop) {
            s.isErr(false);
            if (s.displayDate && nvx.isEmpty(s.dateOfVisit())) {
                s.isErr(true);
                s.errMsg('Please specify a date of visit.');
                s.scrollToErr(fromTop);
                return;
            }
            if (nvx.isEmpty(s.pcValue())) {
                s.isErr(true);
                s.errMsg('Please enter a promo code.');
                s.scrollToErr(fromTop);
                return;
            }
            var isEventMan = s.type.slice(0, 'Event'.length) == 'Event';
            var schedId = isEventMan ? s.currentSched() : -1;
            mflg.spinner.start();
            $
                .ajax({
                    url: '/.store/sky-dining/check-sky-dining-pc',
                    data: JSON.stringify({
                        productId: s.productId,
                        promoCode: s.pcValue(),
                        dateOfVisitText: s.dateOfVisit()
                    }),
                    headers: {
                        'Store-Api-Channel': 'b2c-mflg'
                    },
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            s.refreshPcTickets(data.data.tickets);
                            s.changeActiveSection(s.SECTS.PC);
                            if (fromTop == true) {
                                var topOfErr = $('#promo-code-tbody')
                                    .offset().top;
                                $('html, body').animate({
                                    scrollTop: topOfErr
                                }, 500); // account
                                // for
                                // the
                                // height
                                // of
                                // navbar
                            }
                        } else {
                            s.refreshPcTickets([]);
                            s.isErr(true);
                            s.errMsg(data.message);
                            s.scrollToErr(fromTop);
                        }
                    },
                    error: function () {
                        s.refreshPcTickets([]);
                        s.isErr(true);
                        s
                            .errMsg('Unable to process your request. Please try again in a few moments.');
                        s.scrollToErr(fromTop);
                    },
                    complete: function () {
                        mflg.spinner.stop();
                    }
                });
        };

        s.refreshPcTickets = function (pcTickets) {
            var pcArr = [];
            $.each(pcTickets, function (idx, tix) {
                pcArr.push(new mflg.BookingItem(tix, s.usualPrices));
            });
            s.pcTickets(pcArr);
        };

        s.refreshJcTickets = function (jcTickets) {
            var jcArr = [];
            $.each(jcTickets, function (idx, tix) {
                jcArr.push(new mflg.BookingItem(tix, s.usualPrices));
            });
            s.jcTickets(jcArr);
        };

        s.bookingStage = ko.observable(0);
        s.showDateSelection = ko.observable(true);
        s.showThemeSelection = ko.observable(false);
        s.showMenuSelection = ko.observable(false);
        s.showTopUpSelection = ko.observable(false);

        s.showBookingSummary = ko.computed(function () {
            return s.bookingStage() > 0;
        });

        s.showThemeSummary = ko.computed(function () {
            return (((s.bookingStage() > 1))) && s.hasTheme;
        });

        s.showMenuSummary = ko.computed(function () {
            return s.bookingStage() > 2;
        });

        function getSessionTimeText(sessionId) {
            var sessions = s.availableSessions();
            for (var i = 0; i < sessions.length; i++) {
                if (sessions[i].id == sessionId) {
                    return ', ' + sessions[i].time;
                }
            }
            return '';
        }

        s.bookingSummaryDateText = ko.computed(function () {
            var text = '';
            text += s.dateOfVisit();
            text += getSessionTimeText(s.skyDiningSession());
            return text;
        });

        function getThemeText(themeId) {
            var themes = s.themes();
            for (var i = 0; i < themes.length; i++) {
                if (themes[i].id == themeId) {
                    return themes[i].displayDesc;
                }
            }
            return '';
        }

        s.bookingSummaryThemeText = ko.computed(function () {
            return getThemeText(s.themeId());
        });

        function getMenuNameTextFromList(listToCheck) {
            $.each(listToCheck, function (idx, tix) {
                if (tix.qty() > 0) {
                    s.bookingSummaryMenuText(tix.displayName);
                }
            });
        }

        function getMenuNameText() {
            getMenuNameTextFromList(s.mainTickets());
            getMenuNameTextFromList(s.ccTickets());
            getMenuNameTextFromList(s.jcTickets());
            getMenuNameTextFromList(s.pcTickets())
        }

        function getMainCourseText() {
            var mcText = '';
            $.each(s.mainCourses(), function (idx, itm) {
                if (itm.qty() > 0) {
                    var text = itm.qty() + 'x ';
                    text += itm.name;
                    if (mcText != '') {
                        mcText += ', ';
                    }
                    mcText += text;
                }
            });
            s.bookingSummaryMainCourseText(mcText);
        }

        s.bookingSummaryMenuText = ko.observable('');
        s.bookingSummaryMainCourseText = ko.observable('');

        s.validateStage = function () {
            var message = '<ul>';
            switch (s.bookingStage()) {
                case 0:
                    // Check date of visit
                    if (s.displayDate && nvx.isEmpty(s.dateOfVisit())) {
                        message += '<li>Please specify a date of visit.</li>';
                    }

                    if (nvx.isEmpty(s.skyDiningSession())) {
                        message += '<li>Please specify a sky dining session.</li>';
                    }
                    break;
                case 1:
                    // Check theme
                    if (s.hasTheme && !isThemeSelected()) {
                        message += '<li>No theme has been selected. Please select a theme.</li>';
                    }
                    break;
                case 2:
                    // Check that there is at least one quantity selected for
                    // main/discounted tickets.
                    if (!hasQty()) {
                        message += '<li>No menu has been selected. Please select a menu.</li>';
                    } else {
                        var selectedMc = 0;
                        $.each(s.mainCourses(), function (idx, itm) {
                            selectedMc += Number(itm.qty());
                        });
                        if (selectedMc < s.minPax()) {
                            if (s.minPax() == 1) {
                                message += '<li>Please select at least one main course.</li>';
                            } else {
                                message += '<li>Please select at least '
                                    + s.minPax() + ' main courses.</li>';
                            }
                        } else if (selectedMc > s.maxPax()) {
                            if (s.minPax() == 1) {
                                message += '<li>Please select no more than one main course.</li>';
                            } else {
                                message += '<li>Please select no more than '
                                    + s.maxPax() + ' main courses.</li>';
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            if (message == '<ul>') {
                return true;
            }
            s.errMsg(message + '</ul>');
            return false;
        };
        s.nextStep = function () {
            if (s.validateStage()) {
                s.isErr(false);
                s.errMsg('');
            } else {
                s.isErr(true);
                return false;
            }

            switch (s.bookingStage()) {
                case 0:
                    s.checkSessionCapacity();
                    break;
                case 1:
                    s.checkThemeCapacity();
                    break;
                case 2:
                    s.showDateSelection(false);
                    s.showThemeSelection(false);
                    s.showMenuSelection(false);
                    s.showTopUpSelection(true);
                    getMenuNameText();
                    getMainCourseText();
                    s.bookingStage(3);
                    break;
                default:
                    s.showDateSelection(true);
                    s.showThemeSelection(false);
                    s.showMenuSelection(false);
                    s.showTopUpSelection(false);
                    s.bookingStage(0);
                    break;
            }

            s.scrollToErr();
        };
        s.previousStep = function () {
            switch (s.bookingStage()) {
                case 2:
                    s.showDateSelection(false);
                    if (s.hasTheme) {
                        s.showThemeSelection(true);
                        s.showMenuSelection(false);
                        s.showTopUpSelection(false);
                        s.bookingStage(1);
                    } else {
                        s.showDateSelection(true);
                        s.showThemeSelection(false);
                        s.showMenuSelection(false);
                        s.showTopUpSelection(false);
                        s.bookingStage(0);
                    }
                    break;
                case 3:
                    s.showDateSelection(false);
                    s.showThemeSelection(false);
                    s.showMenuSelection(true);
                    s.showTopUpSelection(false);
                    s.bookingStage(2);
                    break;
                default:
                    s.showDateSelection(true);
                    s.showThemeSelection(false);
                    s.showMenuSelection(false);
                    s.showTopUpSelection(false);
                    s.bookingStage(0);
                    break;
            }

            s.scrollToErr();
        };
        s.mainCourseQtyOptions = ko.observableArray([]);

        function initMainCourseQtyOptions(maxPax) {
            var options = [];
            for (var i = 0; i <= maxPax; i++) {
                options.push(i);
            }
            s.mainCourseQtyOptions(options);
        }

        function initCalendar(bm) {
            mflg.spinner.start();
            $.ajax({
                url: '/.store/sky-dining/get-calendar-params',
                data: JSON.stringify({
                    packageId: bm.productId
                }),
                headers: {
                    'Store-Api-Channel': 'b2c-mflg'
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.success) {
                        console.log(data);

                        var result = data.data;
                        bm.minBookingDays = result.minBookingDays;
                        bm.minDateArr = result.minDateArr;
                        bm.maxDateArr = result.maxDateArr;
                        bm.blackoutDatesArr = result.blackoutDatesArr;

                        s.errMsg('');
                        s.isErr(false);
                    } else {
                        s.errMsg('Unable to process your request. Please try again in a few moments.');
                        s.isErr(true);
                        s.scrollToErr();
                    }
                },
                error: function () {
                    s.errMsg('Unable to process your request. Please try again in a few moments.');
                    s.isErr(true);
                    s.scrollToErr();
                },
                complete: function () {
                    var dateOfVisitField = $('#dov-field');
                    var dovProps = {
                        format: 'dd/mm/yyyy',
                        min: 1,
                        selectMonths: true,
                        selectYears: true,
                        today: false,
                        clear: false
                    };
                    if (bm.minDateArr != null) {
                        dovProps['min'] = bm.minDateArr;
                    } else {
                        dovProps['max'] = bm.minBookingDays;
                    }
                    var allDisabled = false;
                    if (bm.maxDateArr != null) {
                        dovProps['max'] = bm.maxDateArr;
                        var dateObj = new Date();
                        var month = dateObj.getUTCMonth();
                        var day = dateObj.getUTCDate();
                        var year = dateObj.getUTCFullYear();
                        if ((bm.maxDateArr[0] == year)
                            && (bm.maxDateArr[1] == month + 1)
                            && (bm.maxDateArr[2] == day)) {
                            dovProps['max'] = 0;
                            allDisabled = true;
                        }
                    }
                    if (!allDisabled && (bm.blackoutDatesArr != null)) {
                        dovProps['disable'] = bm.blackoutDatesArr;
                    }
                    console.log(dovProps);
                    /*
                     * TODO This is problematic. For IE8, it doesn't work if you
                     * pickadate without setting a timeout.
                     */
                    if ($('html').hasClass('lt-ie9')) {
                        setTimeout(function () {
                            dateOfVisitField.pickadate(dovProps);
                        }, 500);
                    } else {
                        dateOfVisitField.pickadate(dovProps);
                    }

                    mflg.spinner.stop();
                }
            });
        }
    };

    mflg.BookingMainCourse = function (bmc) {
        var s = this;

        s.id = bmc.id;
        s.name = bmc.name;
        s.qty = ko.observable('');
    };

    mflg.BookingItem = function (bi, usualPrices, pax) {
        var s = this;

        s.id = bi.id;
        s.discountId = bi.discountId;
        s.itemCode = bi.itemCode;
        s.ticketType = bi.ticketType;
        s.displayName = bi.displayName;
        s.subName = bi.subName;
        s.displayPrice = bi.displayPrice;
        s.displayDesc = bi.displayDesc;
        s.link = bi.link;
        s.unitPrice = bi.unitPrice;
        s.usualPrice = bi.usualPrice ? bi.usualPrice : '';
        s.pax = '';
        if (pax) {
            s.pax = pax;
        }

        s.hasMainCourses = ko.observable();
        s.hasMainCourses(bi.hasMainCourses);
        s.mainCourses = ko.observableArray([]);
        if (s.hasMainCourses()) {
            $.each(bi.sdMainCourses, function (idx, mc) {
                s.mainCourses.push(new mflg.BookingMainCourse(mc));
            });
        }

        s.qty = ko.observable(0);
        s.displayImagePath = bi.displayImagePath;

        s.gst = bi.gst;
        s.serviceCharge = bi.serviceCharge;
        s.merchantId = bi.merchantId;

        s.selectMenu = function () {
            $.each(mflg.bm.mainTickets(),
                function (idx, tix) {
                    tix.qty(0);
                });
            $.each(mflg.bm.jcTickets(), function (idx, itm) {
                itm.qty(0);
            });
            $.each(mflg.bm.ccTickets(), function (idx, itm) {
                itm.qty(0);
            });
            $.each(mflg.bm.pcTickets(), function (idx, itm) {
                itm.qty(0);
            });
            mflg.bm.hasMainCourses(s.hasMainCourses());
            mflg.bm.mainCourses(s.mainCourses());
            mflg.bm.gst(s.gst);
            mflg.bm.serviceCharge(s.serviceCharge);
            mflg.bm.menuPrice(s.unitPrice);
            mflg.bm.menuId(s.id);
            mflg.bm.discountId(s.discountId);
            mflg.bm.merchantId(s.merchantId);
            s.qty(1);
            return true;
        };
        s.collapsed = ko.observable(true);
        s.toggleCollapse = function () {
            if (s.collapsed()) {
                s.collapsed(false);
            } else {
                s.collapsed(true);
            }
        }
    };

    mflg.Theme = function (bi) {
        var s = this;

        s.selected = ko.observable(false);

        s.id = bi.id;
        s.displayDesc = bi.displayDesc;
        s.displayImagePath = bi.displayImagePath;

        s.selectTheme = function () {
            $.each(mflg.bm.themes(), function (idx, tix) {
                tix.selected(false);
            });
            mflg.bm.themeId(s.id);
            s.selected(true);
            return true;
        }
    }

})(window.mflg = window.mflg || {}, jQuery);