$(document).ready(function() {
    mflg.updateCartQty();

    mflg.checkoutModel = new mflg.CheckoutModel();
    mflg.getCheckoutDisplay();
    mflg.initReceipt();
});

(function(mflg, $) {

    mflg.initReceipt = function() {
        ko.applyBindings(mflg.checkoutModel, document
            .getElementById('receiptPage'));
    };

    mflg.getCheckoutDisplay = function() {
        $
            .ajax({
                url: '/.store/store/get-finalised-receipt',
                data : JSON.stringify({
                    sessionId : ''
                }),
                headers : {
                    'Store-Api-Channel': 'b2c-mflg',
                    'Store-Api-Locale': jsLang
                },
                type : 'POST',
                cache : false,
                dataType : 'json',
                contentType : 'application/json',
                success : function(data) {
                    if (data.success) {
                        if (data.data == null) {
                            mflg.checkoutModel.abnormalMsg('We are still processing your transaction. Please wait a few moments.');

                            setTimeout(function() {
                                window.location.reload();
                            }, 3000);
                        } else {
                            mflg.checkoutModel.init(data.data);
                        }
                    } else {
                        mflg.checkoutModel.abnormalMsg(data.message);
                    }
                },
                error : function() {
                    mflg.checkoutModel.abnormalMsg('You may have refreshed the page or pressed back while in the middle of a transaction. Your transaction has been invalidated. Please checkout again.');
                },
                complete : function() {

                }
            });
    };

    mflg.CheckoutModel = function() {
        var s = this;

        s.cartId = ko.observable("");
        s.total = ko.observable(0);
        s.totalText = ko.observable("");
        s.totalQty = ko.observable(0);
        s.totalMainQty = ko.observable(0);
        s.payMethod = ko.observable("");

        s.cmsProducts = ko.observableArray([]);

        s.hasBookingFee = ko.observable(false);
        s.bookFeeMode = ko.observable('');
        s.bookPriceInCents = ko.observable(0);
        s.bookPriceText = ko.observable("");
        s.bookFeeWaiverCode = ko.observable("");
        s.bookFeeQty = ko.observable(0);
        s.bookFeeSubTotal = ko.observable(0);
        s.bookFeeSubTotalText = ko.observable("");

        s.waivedTotalText = ko.observable("");

        s.dateOfPurchase = ko.observable("");
        s.receiptNumber = ko.observable("");
        s.pin = ko.observable("");
        s.barcodeUrl = ko.observable("");
        s.baseUrl = ko.observable("");
        s.paymentType = ko.observable("");
        s.paymentTypeLabel = ko.observable("");
        s.customerName = ko.observable("");

        s.hasTnc = ko.observable(false);
        s.tncs = ko.observableArray([]);

        s.hasAd = ko.observable(false);
        s.hasAdLink = ko.observable(false);
        s.adLink = ko.observable("");
        s.adUrl = ko.observable("");

        s.transStatus = ko.observable("");
        s.isError = ko.observable(false);
        s.errorCode = ko.observable("");
        s.errorMessage = ko.observable("");
        s.message = ko.observable("");

        s.custServEmail = ko.observable("");

        s.ccDigits = ko.observable("");

        s.emailMsgStyle = ko.observable("");

        s.totalTotal = ko.observable(0);
        s.totalTotalText = ko.observable("");

        s.customer = new mflg.CheckoutDeets('checkoutForm');
        s.customerJson = ko.observable("");

        s.hasPinCode = ko.observable(false);
        s.pinCode = ko.observable('');

        s.isForReceipt = ko.computed(function() {
            return s.cmsProducts().length > 0 && s.transStatus() == "Success";
        });

        s.abnormalMsg = ko.observable('You may have refreshed the page or pressed back while in the middle of a transaction. Your transaction has been invalidated. Please checkout again.');

        s.init = function(data) {
            s.cartId(data.cartId);
            s.total(data.total);
            s.totalText(data.totalText);
            s.totalQty(data.totalQty);
            s.totalMainQty(data.totalMainQty);
            s.payMethod(data.payMethod);

            s.hasBookingFee(data.hasBookingFee);
            s.bookFeeMode(data.bookFeeMode);
            s.bookPriceInCents(data.bookPriceInCents);
            s.bookPriceText(data.bookPriceText);
            s.bookFeeWaiverCode(data.bookFeeWaiverCode);
            s.bookFeeQty(data.bookFeeQty);
            s.bookFeeSubTotal(data.bookFeeSubTotal);
            s.bookFeeSubTotalText(data.bookFeeSubTotalText);

            s.waivedTotalText(data.waivedTotalText);

            s.dateOfPurchase(data.dateOfPurchase);
            s.receiptNumber(data.receiptNumber);
            s.pin(data.pin);
            s.barcodeUrl(data.barcodeUrl);
            s.baseUrl(data.baseUrl);
            s.paymentType(data.paymentType);
            s.paymentTypeLabel(data.paymentTypeLabel);
            s.customerName(data.customerName);

            s.pinCode(data.pinCode == null ? '' : data.pinCode);
            s.hasPinCode(s.pinCode() != '');

            s.hasTnc(data.hasTnc);
            s.tncs([]);
            if (data.tncs) {
                for (var i = 0; i < data.tncs.length; i++) {
                    var tnc = data.tncs[i];
                    s.tncs.push(new mflg.TncViewModel(tnc));
                }
            }

            s.hasAd(data.hasAd);
            s.hasAdLink(data.hasAdLink);
            s.adLink(data.adLink);
            s.adUrl(data.adLink);

            s.transStatus(data.transStatus);
            s.isError(data.customerName);
            s.errorCode(data.errorCode);
            s.errorMessage(data.errorCode);
            s.message(data.message);

            s.custServEmail(data.custServEmail);

            s.ccDigits(data.ccDigits);

            s.emailMsgStyle(data.emailMsgStyle);

            s.totalTotal(data.totalTotal);
            s.totalTotalText(data.totalTotalText);

            s.customerJson(data.customerJson);

            $.each(data.cmsProducts, function(index, element) {
                var cmsProduct = new mflg.CmsProduct();
                cmsProduct.init(element);
                s.cmsProducts.push(cmsProduct);
            });

            if (data.customer) {
                s.customer.unpackDeets(data.customer);
            }
        }
    };

    mflg.TncViewModel = function (model) {
        var s = this;

        s.title = model.title;
        s.content = model.content;
    };

    mflg.CmsProduct = function() {
        var s = this;

        s.id = ko.observable(0);
        s.name = ko.observable("");
        s.subtotal = ko.observable(0);
        s.subtotalText = ko.observable("");

        s.items = ko.observableArray([]);

        s.init = function(data) {
            s.id(data.id);
            s.name(data.name);
            s.subtotal(data.subtotal);
            s.subtotalText(data.subtotalText);

            $.each(data.items, function(index, element) {
                var cmsItem = new mflg.CmsItem();
                cmsItem.init(element);
                s.items.push(cmsItem);
            });
        }
    };

    mflg.CmsItem = function() {
        var s = this;

        s.listingId = ko.observable("");
        s.productCode = ko.observable("");
        s.type = ko.observable("");
        s.name = ko.observable("");
        s.productDescription = ko.observable("");
        s.description = ko.observable("");
        s.qty = ko.observable(0);
        s.price = ko.observable(0);
        s.priceText = ko.observable("");
        s.total = ko.observable(0);
        s.totalText = ko.observable("");
        s.isTopup = ko.observable(false);

        s.discountTotal = ko.observable(0);
        s.discountTotalText = ko.observable("");
        s.discountLabel = ko.observable("");
        s.hasDiscount = ko.computed(function() {
            return s.discountTotal() != 0;
        });
        s.originalTotalText = ko.computed(function() {
            if(s.hasDiscount()) {
                var originalPrice = s.total() + s.discountTotal();
                return "S$ " + originalPrice.toFixed(2);
            }

            return s.totalText();
        });

        s.init = function(data) {
            s.listingId(data.listingId);
            s.productCode(data.productCode);
            s.type(data.type);
            s.name(data.name);
            s.productDescription(data.productDescription);
            s.description(data.description);
            s.qty(data.qty);
            s.price(data.price);
            s.priceText(data.priceText);
            s.total(data.total);
            s.totalText(data.totalText);
            s.isTopup(data.topup);

            s.discountTotal(data.discountTotal);
            s.discountTotalText(data.discountTotalText);
            s.discountLabel(data.discountLabel);
        };

        s.removeItem = function() {
            mflg.removeItem(s.listingId);
        }
    };

    mflg.CheckoutDeets = function(formId) {
        var s = this;

        s.formId = formId;

        s.email = ko.observable();
        s.email2 = ko.observable();
        s.idType = ko.observable('NricFin');
        s.idNo = ko.observable();
        s.idDisplay = ko.computed(function() {
            return (s.idType() === 'NricFin' ? '(NRIC/FIN) ' : '(Passport) ')
                + s.idNo();
        });
        s.name = ko.observable();
        s.mobile = ko.observable();
        s.paymentType = ko.observable('');
        s.subscribed = ko.observable(false);
        s.tnc = ko.observable(false);
        s.salutation = ko.observable('');
        s.companyName = ko.observable('');

        s.isErr = ko.observable(false);
        s.errMsg = ko.observable();

        s.packageDeets = function() {
            return {
                email : s.email(),
                idType : s.idType(),
                idNo : s.idNo(),
                name : s.name(),
                mobile : s.mobile(),
                paymentType : s.paymentType(),
                subscribed : s.subscribed(),
                salutation : s.salutation(),
                companyName : s.companyName(),
                dob : "",
                referSource : "",
                nationality : "",
                referSourceLabel : "",
                nationalityLabel : "",
                paymentTypeLabel : "",
                trafficSource : "",
                ip : ""
            };
        };

        s.unpackDeets = function(d) {
            s.email(d.email);
            s.email2(d.email);
            s.idType(d.idType);
            s.idNo(d.idNo);
            s.name(d.name);
            s.mobile(d.mobile);
            s.paymentType(d.paymentType);
            s.subscribed(d.subscribed);
            s.salutation(d.salutation);
            s.companyName(d.companyName);
            s.isErr(d.isErr);
            s.errMsg(d.errMsg);
        };
    };

})(window.mflg = window.mflg || {}, jQuery);