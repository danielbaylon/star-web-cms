$(document).ready(function() {

    //Roundabout http://fredhq.com/projects/roundabout
    $('#roundabout').roundabout({
        btnNext: '.roundNext',
        btnPrev: '.roundPrev',
        minScale: 0.3,
        autoplay: true,
        autoplayDuration: 5000,
        autoplayPauseOnHover: true,
        enableDrag: false,
        clickToFocus: false,
        shape: 'figure8',
        minOpacity: 0.01
    });

    var autoplay = true;
    $('#roundToggle').click(function() {
        if (autoplay) {
            $('#roundabout').roundabout('stopAutoplay');
            $('#roundToggleIcon').removeClass('icon-pause').addClass('icon-play');
        } else {
            $('#roundabout').roundabout('startAutoplay');
            $('#roundToggleIcon').removeClass('icon-play').addClass('icon-pause');
        }
        autoplay = !autoplay;
    });

    $('#linkSd').click(function() {
        redir(this, 'You are being redirected to the Sky Dining reservation page. Please wait...');
        return false;
    });
    $('#linkJc').click(function() {
        redir(this, 'You are being redirected to the Faber Licence membership page. Please wait...');
        return false;
    });

    function redir(elem, msg) {
        $('.spinner-overlay').show();
        $('#redirectDialog').html(msg).fadeIn();
        setTimeout(function() {
            window.location.href = $(elem).attr('href');
        }, 3000);
    }

    mflg.updateCartQty();
});