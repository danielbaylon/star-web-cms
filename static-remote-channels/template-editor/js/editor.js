(function(nvx, $, ko) {

    /*
     TODO list:
     / Small font
     - Escape text (&amp;)
     / Bold / italic
     - Shorter barcode (length)
     - Should exceed bounds
     - Issue with printing too many text
     */

    $(document).ready(function() {
        nvx.editorWidth = 100;;
        nvx.tokenLine = 1;
        nvx.editorView = new nvx.EditorView();
        ko.applyBindings(nvx.editorView, $('#ticket')[0]);

        var paramMode = $.urlParam('mode');
        if(paramMode != null) {
            nvx.editorView.editorMode(paramMode);

            if(nvx.editorView.editorMode() == 'S') {
                var paramType = $.urlParam('type');
                if(paramType != null) {
                    nvx.editorView.ticketType(paramType);
                    if(nvx.editorView.ticketType() == 'ETICKET') {
                        CKEDITOR.replace( 'editor1', {
                            allowedContent: true,
                            customValues: { mode: nvx.editorView.editorMode(),
                                sid: '' },
                            toolbar: 'StandaloneToolbar'
                        });
                    }
                }
            }
            if (nvx.editorView.editorMode() == 'I') {
                nvx.editorView.axsessionId($.urlParam('sid'));
                //nvx.editorView.addTemplate();
                nvx.editorView.loadTemplate();
            }
        }
        // if(nvx.editorView.editorMode() == 'I') {
        //     //nvx.editorView.addTemplate();
        //     nvx.editorView.loadTemplate();

        // }
        //alert(nvx.editorView.editorMode()+" "+nvx.editorView.ticketType());



    });

    $.urlParam = function(name) {
        var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
        if(results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

    nvx.editorView = {};



    nvx.EditorView = function() {
        var s = this;

        s.ticketType = ko.observable('PAPER');
        s.editorMode = ko.observable('S');
        s.axsessionId = ko.observable('');

        /* Element Initialisation */


        $("#cppsPosX").change(function() {
            var x =this.value;
            var ticketComponent = s.selectedComponent();
            if($("#"+ticketComponent.id)[0] == undefined)
                return;
            $("#"+ticketComponent.id).css({left: x+'px'});
            var info = $("#"+ticketComponent.id)[0].getBoundingClientRect();
            if(x == "" || parseInt(info.left) < 245 || parseInt(info.right) > 826 ) {
                $("#cppsPosX").val(nvx.prevX);
                $("#"+ticketComponent.id).css({left: nvx.prevX+'px'});
            }else {
                nvx.prevX = x;
            }
        });

        $("#cppsPosY").change(function() {
            var y =this.value;
            var ticketComponent = s.selectedComponent();
            if($("#"+ticketComponent.id)[0] == undefined)
                return;
            $("#"+ticketComponent.id).css({top: y+'px'});
            var info = $("#"+ticketComponent.id)[0].getBoundingClientRect();
            if(y == ""|| parseInt(info.top) < 83 || parseInt(info.bottom) > 585) {
                $("#cppsPosY").val(nvx.prevY);
                $("#"+ticketComponent.id).css({top: nvx.prevY+'px'});
            }else{
                nvx.prevY = y;
            }
        });

        $("#cppsPosX").focusin(function () {
            nvx.prevX = this.value;
        });

        $("#cppsPosY").focusin(function () {
            nvx.prevY = this.value;
        });

        $(document).on("keyup","#cppsText",function () {
            var ticketComponent = s.selectedComponent();
            var text = this.value;
            if(text == "") {
                this.value = "Text";
                ticketComponent.text("Text");
            }
            if($("#"+ticketComponent.id)[0] == undefined){
                return;
            }


            var maxLen = 35;
            var editorArea = $("#editorDrawingAreaActual")[0].getBoundingClientRect();
            var componentArea = $("#"+ticketComponent.id)[0].getBoundingClientRect();

            switch(ticketComponent.textOrientation()) {
                case 'NORMAL':
                    if(Math.floor(componentArea.right) >= Math.floor(editorArea.right-7)) {
                        return false;
                    }
                    break;
                case 'CLOCKWISE_90':
                    if(Math.floor(componentArea.bottom) >= Math.floor(editorArea.bottom-7)) {
                        return false;
                    }
                    break;
                case 'COUNTERCLOCKWISE_90':
                    if(Math.floor(componentArea.top) <= Math.floor(editorArea.top+7)) {
                        return false;
                    }
                    break;
                case 'UPSIDE_DOWN':
                    if(Math.floor(componentArea.left) <= Math.floor(editorArea.left+7)) {
                        return false;
                    }
                    break;
            }



            // alert("this"+ this.value +"text" +ticketComponent.text());
        });

        // $("#cppsTextOrientation").change(function () {
        //     var ticketComponent = s.selectedComponent();
        //     var textOrientation = this.value;
        //     if($("#"+ticketComponent.id)[0] == undefined)
        //         return;
        //     var editorArea = $("#editorDrawingAreaActual")[0].getBoundingClientRect();
        //     var componentArea = $("#"+ticketComponent.id)[0].getBoundingClientRect();
        //
        //     switch(textOrientation) {
        //         case 'NORMAL':
        //             s.truncText(ticketComponent,componentArea,editorArea);
        //             break;
        //         /*case 'CLOCKWISE_90':
        //             if(Math.floor(componentArea.bottom) >= Math.floor(editorArea.bottom-7)) {
        //                 return false;
        //             }
        //             break;
        //         case 'COUNTERCLOCKWISE_90':
        //             if(Math.floor(componentArea.top) <= Math.floor(editorArea.top+7)) {
        //                 return false;
        //             }
        //             break;
        //         case 'UPSIDE_DOWN':
        //             if(Math.floor(componentArea.left) <= Math.floor(editorArea.left+7)) {
        //                 return false;
        //             }
        //             break;*/
        //     }
        // });

        $(document).on("change","#cppsTextOrientation",function () {
            var ticketComponent = s.selectedComponent();
            var textOrientation = this.value;
            if($("#"+ticketComponent.id)[0] == undefined)
                return;
            var editorArea = $("#editorDrawingAreaActual")[0].getBoundingClientRect();
            var componentArea = $("#" + ticketComponent.id)[0].getBoundingClientRect();
            if(ticketComponent.componentType == 'Text') {
                s.truncText(ticketComponent, componentArea, editorArea, textOrientation);
            }else if(ticketComponent.componentType == 'Token') {
                var ret =  s.realignComponent(ticketComponent, $("#" + ticketComponent.id), $("#editorDrawingAreaActual"), textOrientation);
                return ret;
            }
            return true;
        });

        // $("#cppsTextSize").focus(function () {
        //     // Store the current value on focus, before it changes
        //     alert("old"+this.value);
        //     previous = this.value;
        // }).change(function() {
        //     // Do something with the previous value after the change
        //     //alert(previous);
        // });

        function onOpen(e) {
           nvx.prevTextSize = s.selectedComponent().textSize();
        }

        $(document).on("change","#cppsTextSize",function (e) {
            var ticketComponent = s.selectedComponent();
            if($("#"+ticketComponent.id)[0] == undefined)
                return;
            var textOrientation = ticketComponent.textOrientation();
            if(ticketComponent.componentType == 'Text') {
                var editorArea = $("#editorDrawingAreaActual")[0].getBoundingClientRect();
                var componentArea = $("#" + ticketComponent.id)[0].getBoundingClientRect();
                s.truncText(ticketComponent, componentArea, editorArea, textOrientation);
            }else if(ticketComponent.componentType == 'Token') {
                var ret =  s.realignComponent(ticketComponent, $("#" + ticketComponent.id), $("#editorDrawingAreaActual"), textOrientation);
                if(!ret) {
                    ticketComponent.textSize(nvx.prevTextSize);
                    //e.preventDefault();
                    return ret;
                }
            }
            return true;
        });



        $(document).on("change","#cppsToken",function () {
            var ticketComponent = s.selectedComponent();
            if($("#"+ticketComponent.id)[0] == undefined)
                return;
            var textOrientation = ticketComponent.textOrientation();
            if(ticketComponent.componentType == 'Token') {
                return s.realignComponent(ticketComponent, $("#" + ticketComponent.id), $("#editorDrawingAreaActual"), textOrientation);
            }
            return true;
        });

        s.realignComponent = function(component, hintElement, enclosingElement, textOrientation) {
            var posX, posY;

            var bodyInfo = enclosingElement[0].getBoundingClientRect();
            var info = hintElement[0].getBoundingClientRect();
            posX = component.posX();
            posY = component.posY();
            if (component.textOrientation() == 'NORMAL') {
                // maxX = $('.drawing-area-actual')[0].getBoundingClientRect().width - 10;
                // if (posX > maxX) posX = Math.round(maxX);
                // if (posX <= 0) posX = 1;
                rewrapToken(component, posX, posY);
                // maxY = (($('.drawing-area-actual')[0].getBoundingClientRect().height) - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                // if (maxY < 1) {
                //     return false;
                // }
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY <= 0) posY = 1;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var comp = $("#" + component.id);
                var offset = comp.offset();
                comp.css("width", nvx.editorWidth / 1.5 + "px"); // reduce scale
                comp.css("height", nvx.tokenBaseHeight * nvx.tokenLine + "px");
                // comp.css("top",posY+"px");
                // comp.css("left", posX+"px");

                //s.realignComponent(component,$("#"+component.id),$("#editorDrawingAreaActual"),component.textOrientation());

            } else if (component.textOrientation() == 'UPSIDE_DOWN') {

                // maxX = bodyInfo.width;
                // minX = 10;
                // maxY = bodyInfo.height;
                //
                // if (posX >= maxX) posX = Math.round(maxX);
                // if (posY >= maxY) posY = Math.round(maxY);
                // if (posX <= minX) posX = Math.round(minX);

                rewrapToken(component, posX, posY);
                // minY = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                // if (posY < minY) posY = Math.round(minY);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var comp = $("#" + component.id);
                var offset = comp.offset();
                comp.css("width", editorWidth / 1.5 + "px"); // reduce scale
                comp.css("height", nvx.tokenBaseHeight * tokLine + "px");

            }else if(component.textOrientation() == 'CLOCKWISE_90') {

                // maxX = bodyInfo.width;
                // minY = 1;
                // maxY = bodyInfo.height-10;
                //
                // if (posX > maxX) posX = Math.round(maxX);
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY < minY) posY = Math.round(minY);

                rewrapToken(component, posX, posY);
                var comp = $("#"+component.id);


                // minX = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                // if(minX > bodyInfo.width) {
                //     return false;
                // }
                // if (posX < minX) posX = Math.round(minX);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var offset = comp.offset();
                comp.css("height",nvx.tokenBaseHeight * tokLine+"px");
                comp.css("width",editorWidth/1.5+"px"); // reduce scale

            }else if(component.textOrientation() == 'COUNTERCLOCKWISE_90') {
                // minX = 1;
                // minY = 10;
                // maxY = bodyInfo.height;
                //
                // if (posX < minX) posX = Math.round(minX);
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY < minY) posY = Math.round(minY);

                rewrapToken(component, posX, posY);
                var comp = $("#"+component.id);
                // maxX = (bodyInfo.width - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                // if (posX > maxX) posX = Math.round(maxX);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var offset = comp.offset();
                comp.css("height",nvx.tokenBaseHeight * tokLine+"px");
                comp.css("width",editorWidth/1.5+"px"); // reduce scale

            }
            return true;

        };
        s.realignComponentOld = function(component, hintElement, enclosingElement, textOrientation) {
            var posX, posY;

            var bodyInfo = enclosingElement[0].getBoundingClientRect();
            var info = hintElement[0].getBoundingClientRect();

            // if(parseInt(info.left) < parseInt(bodyInfo.left+1) || parseInt(info.right) > parseInt(bodyInfo.right-1) || parseInt(info.top) < parseInt(bodyInfo.top-8) || parseInt(info.bottom) > parseInt(bodyInfo.bottom))
            //     return false;

            posX = component.posX();
            posY = component.posY();
            //posX = Math.floor(parseInt($(hintElement).css('left').replace('px','')) - enclosingElement.offset().left) + '';
            // posY = Math.floor(parseInt($(hintElement).css('top').replace('px','')) - enclosingElement.offset().top) + '';
            // if(posX<=0) posX = 1;
            // if(posY<=0) posY = 1;
            // console.log($hintElement.css('left'),$hintElement.css('top'));
            // console.log($enclosingElement.offset().left,$enclosingElement.offset().top);
            var maxX, maxY;

            if (!s.selectedComponent().unselected) {
                switch (textOrientation) {
                    case 'CLOCKWISE_90':
                        maxX = bodyInfo.width;
                        maxY = bodyInfo.height - info.height;
                        if (posX >= maxX) posX = Math.round(maxX);
                        if (posY >= maxY) posY = Math.round(maxY);
                        if (posX <= info.width) posX = Math.round(info.width);
                        if (posY <= 0) posY = 1;
                        break;
                    case 'COUNTERCLOCKWISE_90':
                        maxX = bodyInfo.width - info.width;
                        maxY = bodyInfo.height;
                        if (posX >= maxX) posX = Math.round(maxX);
                        if (posY >= maxY) posY = Math.round(maxY);
                        if (posX <= 1) posX = 1;
                        if (posY <= info.height) posY = Math.round(info.height);
                        break;
                    case 'UPSIDE_DOWN':
                        maxX = bodyInfo.width;
                        maxY = bodyInfo.height;
                        if (posX >= maxX) posX = Math.round(maxX);
                        if (posY >= maxY) posY = Math.round(maxY);
                        if (posX <= info.width) posX = Math.round(info.width);
                        if (posY <= info.height) posY = Math.round(info.height);
                        break;
                    default:
                        // NORMAL
                        maxX = bodyInfo.width - info.width;
                        maxY = bodyInfo.height - info.height;
                        if (posX >= maxX) posX = Math.round(maxX);
                        if (posY >= maxY) posY = Math.round(maxY);
                        if (posX <= 0) posX = 1;
                        if (posY <= 0) posY = 1;
                        break;
                }
            }
            component.posX(posX);
            component.posY(posY);
            updateKendoElements(component);
        };

        s.truncText = function (component, componentArea, editorArea, textOrientation) {
            switch (textOrientation) {
                case 'NORMAL':
                    if(Math.round(componentArea.right) >= Math.round(editorArea.right)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'CLOCKWISE_90':
                    if(Math.round(componentArea.bottom) >= Math.round(editorArea.bottom)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'COUNTERCLOCKWISE_90':
                    if(Math.round(componentArea.top) <= Math.round(editorArea.top)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'UPSIDE_DOWN':
                    if(Math.round(componentArea.left) <= Math.round(editorArea.left)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
            }
        };

        s.wrapText = function (component, componentArea, editorArea, textOrientation) {
            switch (textOrientation) {
                case 'NORMAL':
                    if(Math.round(componentArea.right) >= Math.round(editorArea.right)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'CLOCKWISE_90':
                    if(Math.round(componentArea.bottom) >= Math.round(editorArea.bottom)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'COUNTERCLOCKWISE_90':
                    if(Math.round(componentArea.top) <= Math.round(editorArea.top)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
                case 'UPSIDE_DOWN':
                    if(Math.round(componentArea.left) <= Math.round(editorArea.left)) {
                        var str = component.text();
                        component.text(str.substring(0, str.length - 1));
                        var newComponentArea = $("#"+component.id)[0].getBoundingClientRect();
                        s.truncText(component,newComponentArea,editorArea,textOrientation);
                    }
                    else{
                        return;
                    }
                    break;
            }
        };

        s.addTemplate = function() {

            var templateXML = "<TicketPackage><FontData><Path>VeraMono.ttf</Path></FontData><Ticket><Text><X>187</X><Y>175</Y><Font>VeraMono.ttf</Font><Orientation>NORMAL</Orientation><FontSize>8</FontSize><Data>sample</Data></Text></Ticket></TicketPackage>";
            $.ajax({
                url: '/.templateeditor/templateeditor/init-template',
                data: {
                    sid: s.axsessionId(),
                    isNew: true,
                    ticketType: 'PAPER',
                    templateXML: templateXML
                },
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        s.loadTemplate();
                        //window.location.reload();
                    } else {
                        alert('Error encountered loading template');
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };

        var i = 0, count = 3;
        s.loadTemplate = function() {
            $.ajax({
                url: '/.templateeditor/templateeditor/get-template',
                data: {
                    sid: s.axsessionId()
                },
                type: 'GET', cache: false, dataType: 'json',
                success: function (data) {
                    var inputXML;
                    if (data.success) {
                        if(!data.data.modified) {
                            inputXML = data.data.originalTemplateXML;
                        }
                        else {
                            inputXML = data.data.modifiedTemplateXML;
                        }

                        //inputXML = '<p>This is my textarea to be replaced with CKEditor.</p> <p><img alt="" border="0" data-template="BarCode" hspace="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAADLUlEQVR4Xu2dzyt0URjHv3eBnQ0aSkiJIRtRykZsJCXZ2NhoFpRMsrNRFsJCFGXjn7Agv/KjKTU1G+VHYyEJKys7FqNz3nfuO2fvWbz1uTvXmcc9n3l67jn3fO4RFQqFgjhMCETANeHqgwLXji1wDdkCF7iWBAxjU3OBa0jAMDSZC1xDAoahyVzgGhIwDE3mAteQgGHoqL29vXB3d6fn52c1NDSosrJSFRUVur+/V3V1tebn57W3t6f19XVNT09rbGxMZ2dnwSXlcjm1tLSoqqpK39/fyufzqq2tjdscHBxoYmJCvb29Oj4+Dj67uLio7e3t4NzOzo4mJyeVyWQ0PDysjo4OXV9fB21WV1e1srISnFtbW9PMzEx8zj3wc3/T9aV4lJWV6ePjQ29vb2pra1MikdDj46NOTk40Pj6u/v5+7e/v++adnZ2ey8XFhbq6uuIYrn/d3d2qr6+XY3d1daWRkZH495+fn5qdnVXU2NhYcAGenp7U1NSkKIo83JeXF9XU1PiL3d3d1dbWlubm5jQ0NKSjo6OgUw8PD2ptbVV5ebmH6y68rq4ubuMudnR0VH19fR5Y6bGwsKCNjY3gnPsyp6amdHl56TvrOnlzcxO0WV5e1tLSUnBuc3NT6XQ6gOs+e3t7G8D9+vrS6+urh+OS4P39XYeHh/6LHBwc1OnpqW/f3NzsuWSzWfX09MQxXH+TyaRPRsfu/PxcAwMDwbWkUingApfMpSyU1gXKAjWXGxqjBUYLkhvnMhRjnPvn/sgkgkmETwTKAtPff0NmygJlgbLgCPBUrGQiTVmgLFAWKAusRLAS4dbQWOZhDY0FyuIAiQVKSZQFltbxFvAWkEIwbtCZ0Jlwxbw1iIiH5Yjl6GZKKKR/54uUBeRn5OfiwxPKAmWBdyJ4J4IXTnibp3hPQNtH20fbR9tH28fPRcQrfaENEQ8RDxEPEQ8RDxEP4wbjBuMG4wbjBuMG4wbjBuMG44Z9xdhXLH5e9F8srbPdoOF2g/wTjmCXwF/9gS1efxVnGAy4wDUkYBiazAWuIQHD0GQucA0JGIYmc4FrSMAwNJkLXEMChqHJXEO4P+e0WWu9Nev4AAAAAElFTkSuQmCC" style="border:0px solidblack; float:right; height:100px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:150px" vspace="0" />text here&nbsp;</p>';

                        s.ticketType(data.data.ticketType);
                        if(s.ticketType() == 'ETICKET') {
                            CKEDITOR.replace( 'editor1', {
                                allowedContent: true,
                                customValues: { mode: s.editorMode(),
                                    sid: s.axsessionId(),
                                },
                                toolbar: 'IntegratedToolbar'
                            });
                            CKEDITOR.instances.editor1.setData( inputXML, function()
                            {
                                this.checkDirty();  // true
                            });
                        }
                        else {
                            var doc = $.parseXML(inputXML);
                            if (doc == null) {
                                alert('Input is invalid, please try again.');
                            } else {
                                s.doImport(doc);
                            }
                        }
                        //alert(template.originalTemplateXML);
                        //window.location.reload();
                    } else {
                        i++;
                        if( i < count ){
                            setTimeout( s.loadTemplate, 3000 );
                        }
                        else {
                            alert('Error encountered loading template');
                            $("#overlay").fadeIn(100);
                        }
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        };




        var kEditorSplitter = $('#editorView').kendoSplitter({
            orientation: 'horizontal',
            panes: [
                { collapsible: false },
                { collapsible: true, size: '350px' }
            ]
        }).data('kendoSplitter');

        var kControlPanel = $('#editorControlPanel').kendoPanelBar({
            expandMode: 'multiple'
        }).data('kendoPanelBar').expand($('[id^="cp"]'), false);

        var kPosX = $('#cppsPosX').kendoNumericTextBox({
            format: 'n0',
            min: 1
        }).data('kendoNumericTextBox');

        var kPosY = $('#cppsPosY').kendoNumericTextBox({
            format: 'n0',
            min: 1
        }).data('kendoNumericTextBox');

        var kToken = $('#cppsToken').kendoDropDownList({
            dataSource: nvx.Tokens,
            dataTextField: 'displayText',
            dataValueField: 'token'
        }).data('kendoDropDownList');

        var kFont = $('#cppsFont').kendoDropDownList({
            dataSource: nvx.Fonts,
            dataTextField: 'displayText',
            dataValueField: 'id'
        }).data('kendoDropDownList');

        var kTextSize = $('#cppsTextSize').kendoDropDownList({
            dataSource: nvx.TextSizes,
            dataTextField: 'displayText',
            dataValueField: 'size',
            select: onOpen
        }).data('kendoDropDownList');

        // var kTextWeight = $('#cppsTextWeight').kendoDropDownList({
        //     dataSource: nvx.TextWeights,
        //     dataTextField: 'displayText',
        //     dataValueField: 'id'
        // }).data('kendoDropDownList');

        var kTextStyle = $('#cppsTextStyle').kendoDropDownList({
            dataSource: nvx.TextStyles,
            dataTextField: 'displayText',
            dataValueField: 'id'
        }).data('kendoDropDownList');


        var kBarcodeHeight = $('#cppsBarcodeHeight').kendoDropDownList({
            dataSource: nvx.BarcodeHeights,
            dataTextField: 'displayText',
            dataValueField: 'id'
        }).data('kendoDropDownList');

        var kBarcodeOrientation = $('#cppsBarcodeOrientation').kendoDropDownList({
            dataSource: nvx.BarcodeOrientations,
            dataTextField: 'displayText',
            dataValueField: 'id'
        }).data('kendoDropDownList');

        var kTextOrientation = $('#cppsTextOrientation').kendoDropDownList({
            dataSource: nvx.TextOrientations,
            dataTextField: 'displayText',
            dataValueField: 'id'
        }).data('kendoDropDownList');

        // var kLogo = $('#cppsLogo').kendoUpload( {
        //     multiple: false,
        //     localization: {
        //         select: "Image"
        //     },
        //     select: onSelect
        // }).data('kendoUpload');

        var kExportWindow = $('#exportWindow').kendoWindow({
            width: '600px',
            title: 'Export Template',
            visible: false,
            actions: ['Close']
        }).data('kendoWindow');

        var kImportWindow = $('#importWindow').kendoWindow({
            width: '600px',
            title: 'Import Template',
            visible: false,
            actions: ['Close']
        }).data('kendoWindow');


        $('#editorDrawingAreaActual').kendoDropTarget({

                drop: dropelement
        });


function dropelement(e,ui) {
    var $enclosingElement = $('#editorDrawingAreaActual');
    var $hintElement = $('.draggable-hint-element');
    var posX, posY;

    var bodyInfo = $enclosingElement[0].getBoundingClientRect();
    var info = $hintElement[0].getBoundingClientRect();
    // if(parseInt(info.left) < parseInt(bodyInfo.left+1) || parseInt(info.right) > parseInt(bodyInfo.right-1) || parseInt(info.top) < parseInt(bodyInfo.top-8) || parseInt(info.bottom) > parseInt(bodyInfo.bottom))
    //     return false;

    posX = Math.floor(parseInt($($hintElement).css('left').replace('px', '')) - $enclosingElement.offset().left) + '';
    posY = Math.floor(parseInt($($hintElement).css('top').replace('px', '')) - $enclosingElement.offset().top) + '';
    // if(posX<=0) posX = 1;
    // if(posY<=0) posY = 1;
    // console.log($hintElement.css('left'),$hintElement.css('top'));
    // console.log($enclosingElement.offset().left,$enclosingElement.offset().top);
    var maxX, maxY, minX, minY;
    var $target = $(e.draggable.currentTarget);
    var targetId = $target.attr('id');
    if (!s.selectedComponent().unselected && s.selectedComponent().id == targetId) {
        var component = s.selectedComponent();
        if (component.componentType == 'Token') {
            if (component.textOrientation() == 'NORMAL') {
                // maxX = $('.drawing-area-actual')[0].getBoundingClientRect().width - 10;
                // if (posX > maxX) posX = Math.round(maxX);
                // if (posX <= 0) posX = 1;
                rewrapToken(component, posX, posY);
                // maxY = (($('.drawing-area-actual')[0].getBoundingClientRect().height) - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                // if (maxY < 1) {
                //     e.preventDefault();
                //     return false;
                // }
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY <= 0) posY = 1;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var comp = $("#" + component.id);
                var offset = comp.offset();
                comp.css("width", nvx.editorWidth / 1.5 + "px"); // reduce scale
                comp.css("height", nvx.tokenBaseHeight * nvx.tokenLine + "px");
                return;


            } else if (component.textOrientation() == 'UPSIDE_DOWN') {

                // maxX = bodyInfo.width;
                // minX = 10;
                // maxY = bodyInfo.height;
                //
                // if (posX >= maxX) posX = Math.round(maxX);
                // if (posY >= maxY) posY = Math.round(maxY);
                // if (posX <= minX) posX = Math.round(minX);

                rewrapToken(component, posX, posY);
                // minY = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                // if (posY < minY) posY = Math.round(minY);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var comp = $("#" + component.id);
                var offset = comp.offset();
                comp.css("width", editorWidth / 1.5 + "px"); // reduce scale
                comp.css("height", nvx.tokenBaseHeight * tokLine + "px");
                return;
            }else if(component.textOrientation() == 'CLOCKWISE_90') {

                // maxX = bodyInfo.width;
                // minY = 1;
                // maxY = bodyInfo.height-10;
                //
                // if (posX > maxX) posX = Math.round(maxX);
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY < minY) posY = Math.round(minY);

                rewrapToken(component, posX, posY);
                var comp = $("#"+component.id);


                // minX = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                // if(minX > bodyInfo.width) {
                //     e.preventDefault();
                //     return false;
                // }
                // if (posX < minX) posX = Math.round(minX);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var offset = comp.offset();
                comp.css("height",nvx.tokenBaseHeight * tokLine+"px");
                comp.css("width",editorWidth/1.5+"px"); // reduce scale
                return;
            }else if(component.textOrientation() == 'COUNTERCLOCKWISE_90') {
                // minX = 1;
                // minY = 10;
                // maxY = bodyInfo.height;
                //
                // if (posX < minX) posX = Math.round(minX);
                // if (posY > maxY) posY = Math.round(maxY);
                // if (posY < minY) posY = Math.round(minY);

                rewrapToken(component, posX, posY);
                var comp = $("#"+component.id);
                // maxX = (bodyInfo.width - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                // if (posX > maxX) posX = Math.round(maxX);
                var tokLine = nvx.tokenLine;
                var editorWidth = nvx.editorWidth;
                s.selectedComponent().posX(posX);
                s.selectedComponent().posY(posY);
                updateKendoElements(s.selectedComponent());
                var offset = comp.offset();
                comp.css("height",nvx.tokenBaseHeight * tokLine+"px");
                comp.css("width",editorWidth/1.5+"px"); // reduce scale
                return;
            }


        } else {
            switch (s.selectedComponent().textOrientation()) {
                case 'CLOCKWISE_90':
                    maxX = bodyInfo.width;
                    maxY = bodyInfo.height - info.height;
                    if (posX >= maxX) posX = Math.round(maxX);
                    if (posY >= maxY) posY = Math.round(maxY);
                    if (posX <= info.width) posX = Math.round(info.width);
                    if (posY <= 0) posY = 1;
                    break;
                case 'COUNTERCLOCKWISE_90':
                    maxX = bodyInfo.width - info.width;
                    maxY = bodyInfo.height;
                    if (posX >= maxX) posX = Math.round(maxX);
                    if (posY >= maxY) posY = Math.round(maxY);
                    if (posX <= 1) posX = 1;
                    if (posY <= info.height) posY = Math.round(info.height);
                    break;
                case 'UPSIDE_DOWN':
                    maxX = bodyInfo.width;
                    maxY = bodyInfo.height;
                    if (posX >= maxX) posX = Math.round(maxX);
                    if (posY >= maxY) posY = Math.round(maxY);
                    if (posX <= info.width) posX = Math.round(info.width);
                    if (posY <= info.height) posY = Math.round(info.height);
                    break;
                default:
                    // NORMAL
                    maxX = bodyInfo.width - info.width;
                    maxY = bodyInfo.height - info.height;
                    if (posX >= maxX) posX = Math.round(maxX);
                    if (posY >= maxY) posY = Math.round(maxY);
                    if (posX <= 0) posX = 1;
                    if (posY <= 0) posY = 1;
                    break;
            }
        }

    }

    if (!(s.selectedComponent().unselected) && (s.selectedComponent().componentType != 'Token')) {
        if (s.selectedComponent().id == targetId) {
            s.selectedComponent().posX(posX);
            s.selectedComponent().posY(posY);
            updateKendoElements(s.selectedComponent());

            return;
        }
    }


    $.each(s.components(), function (idx, mdl) {
        if (mdl.id == targetId) {
            mdl.isSelected(true);
            mdl.posX(posX);
            mdl.posY(posY);
            updateKendoElements(mdl);

            if (s.selectedComponent() instanceof nvx.TicketComponent) {
                s.selectedComponent().isSelected(false);
            }
            s.selectedComponent(mdl);
            return false;
        }
    });
}
        function rewrapToken(component,posX,posY) {
            var toke = component.token();

            $.each(nvx.Tokens, function(idx, mdl) {
                if (mdl.token == toke) {
                        var token = "";
                        for (var i = 0; i < mdl.maxLength; i++) {
                            token = token + '1';
                        }
                        var orientation = component.textOrientation();
                        if(orientation == 'NORMAL' ) {
                            var totalArea = $('.drawing-area-actual')[0].getBoundingClientRect().width-posX;
                            var computeLessWidth = false;
                            if(token.length < mdl.displayText.length) {
                                computeLessWidth = true;
                            }
                            var textSize  = visualLengthHorizontal(token,component.id,computeLessWidth);
                            var foo =  $("#"+component.id).find("#foo")[0];
                            var len = foo.offsetWidth;
                            var span = document.createElement('span');
                            foo.appendChild(span);
                            span.innerHTML = '';
                            for(var i = 0; span.getBoundingClientRect().width <= totalArea; i++)
                                span.innerHTML += 'd';
                            span.innerHTML = span.innerHTML.substring(0, span.innerHTML.length - 1);
                            var chars  =span.innerHTML.length;
                            foo.removeChild(span);
                            component.tokenMaxLength(Math.ceil(chars));
                            var lines = Math.ceil(textSize/totalArea);

                            if(computeLessWidth) {
                                nvx.editorWidth = textSize;
                            }else {
                                if(textSize<totalArea) nvx.editorWidth = textSize;
                                else nvx.editorWidth = totalArea;
                            }

                            nvx.tokenLine = Math.ceil(textSize/totalArea);
                            console.log("Lines:" + Math.ceil(textSize/totalArea));
                        } else if(orientation == 'UPSIDE_DOWN') {
                            var totalArea = posX;
                            var textSize  = visualLengthHorizontal(token,component.id);
                            var foo =  $("#"+component.id).find("#foo")[0];
                            var len = foo.offsetWidth;
                            var span = document.createElement('span');
                            foo.appendChild(span);
                            span.innerHTML = '';
                            for(var i = 0; span.getBoundingClientRect().width <= totalArea; i++)
                                span.innerHTML += 'd';
                            span.innerHTML = span.innerHTML.substring(0, span.innerHTML.length - 1);
                            var chars  =span.innerHTML.length;
                            foo.removeChild(span);
                            component.tokenMaxLength(Math.ceil(chars));
                            var lines = Math.ceil(textSize/totalArea);

                            nvx.editorWidth = totalArea;
                            nvx.tokenLine = Math.ceil(textSize/totalArea);
                            console.log("Lines:" + Math.ceil(textSize/totalArea));
                        } else if(orientation == 'CLOCKWISE_90') {
                            var totalArea = $('.drawing-area-actual')[0].getBoundingClientRect().height-posY;
                            nvx.editorWidth = totalArea;
                            var textSize  = visualLengthVertical(token,component.id);
                            var foo =  $("#"+component.id).find("#foo")[0];
                            var len = foo.offsetWidth;
                            var span = document.createElement('span');
                            foo.appendChild(span);
                            span.innerHTML = '';
                            for(var i = 0; span.getBoundingClientRect().height <= totalArea; i++)
                                span.innerHTML += 'd';
                            span.innerHTML = span.innerHTML.substring(0, span.innerHTML.length - 1);
                            var chars  =span.innerHTML.length;
                            foo.removeChild(span);
                            component.tokenMaxLength(Math.ceil(chars));
                            nvx.tokenLine = Math.ceil(textSize/totalArea);
                            console.log("Lines:" + Math.ceil(textSize/totalArea));
                        } else if(orientation == 'COUNTERCLOCKWISE_90') {
                            var totalArea = posY;
                            nvx.editorWidth = totalArea;
                            var textSize  = visualLengthVertical(token,component.id);
                            var foo =  $("#"+component.id).find("#foo")[0];
                            var len = foo.offsetWidth;
                            var span = document.createElement('span');
                            foo.appendChild(span);
                            span.innerHTML = '';
                            for(var i = 0; span.getBoundingClientRect().height <= totalArea; i++)
                                span.innerHTML += 'd';
                            span.innerHTML = span.innerHTML.substring(0, span.innerHTML.length - 1);
                            var chars  =span.innerHTML.length;
                            foo.removeChild(span);
                            component.tokenMaxLength(Math.ceil(chars));
                            nvx.tokenLine = Math.ceil(textSize/totalArea);
                            console.log("Lines:" + Math.ceil(textSize/totalArea));
                        }
                    return false;
                    }

            });

            //s.realignComponent(component,$("#"+component.id),$("#editorDrawingAreaActual"),component.textOrientation());
        }


        // TODO Phase2
        $("#cppsLogo[type=file]").change(function ()
        {
            if(this.files.length == 0)
                return false;

            if(this.files[0].type.indexOf("image")==-1) {
                alert("Invalid Image File.")
                return false;
            }

            //frm.removeChild(param);
            if(!$('[name="tempIframe"]').length){
                var iframe=document.createElement('iframe');
                $(iframe).attr('name', 'tempIframe');
                $(iframe).attr('id', 'tempIframe');
                $(iframe).hide();
                $('body').append(iframe);
            }
            $('[name="tempIframe"]').load(function(data) {
                var content = $('[name="tempIframe"]').contents().find("body").text();
                try {
                    var data = JSON.parse(content);
                    var result = data.data;

                    if(data.success) {
                        //alert("Uploaded Successfully.");
                        //alert(data.data.base64BW);
                        //var blob = b64toBlob(result.base64BW,'image/bmp');
                        //var blobUrl = URL.createObjectURL(blob);
                        //document.getElementById("imageLogo").src = blobUrl;
                        var totalLogoSize = 0;
                        $.each(s.components(), function (idx, mdl) {

                            totalLogoSize = parseInt(totalLogoSize + mdl.logoSize);

                        });
                        totalLogoSize += parseInt(result.logoSize);
                        if (totalLogoSize > 102400) {
                            alert("Total size of Logos should be less than 100KB");
                            var component = s.selectedComponent();
                            s.components.remove(component);

                        }
                        else {

                            s.selectedComponent().imgurl("data:image/bmp;base64," + result.base64BW);
                            s.selectedComponent().text(result.base64BWInverse);
                            s.selectedComponent().logoSize = parseInt(result.logoSize);
                        }
                    }else{
                        alert(data.message);
                    }
                }catch(err) {
                    console.log(err);
                    alert("Error loading Logo.");
                }

                $('[name="tempIframe"]').remove();
            });

            var frm = document.getElementById('fileUpload');
            var param = addFormParam("logoSize",0);
            frm.appendChild(param);
            frm.target = 'tempIframe';
            frm.submit();



        });

        function addFormParam(key , value){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value",value);
            return hiddenField;
        }

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }

        /* Business Logic */
        s.components = ko.observableArray([]);

        var noComponentSelected = {
            unselected: true
        };

        s.selectedComponent = ko.observable(noComponentSelected);

        s.chooseNewComponent = function(componentType) {
            var prevComponent = s.selectedComponent();
            if (!prevComponent.unselected) {
                if (!prevComponent.isNew()) {
                    prevComponent.isSelected(false);
                }
            }

            var component = new nvx.TicketComponent(componentType);

            s.selectedComponent(component);

            updateKendoElements(component);
        };


        //noinspection JSAnnotator

        s.addNewComponentToView = function(data, event) {
            var component = s.selectedComponent();
            component.isNew(false);
            s.components.push(component);

            setTimeout(function() {
                component.initDraggable();
            }, 500);

            $('#componentPicker' + component.componentType).find('.k-link').removeClass('k-state-selected');
            if($("#"+component.id)[0] == undefined)
                return;
            else{
                if(component.componentType == 'Text') {
                    var editorArea = $("#editorDrawingAreaActual")[0].getBoundingClientRect();
                    var componentArea = $("#"+component.id)[0].getBoundingClientRect();
                    s.truncText(component,componentArea,editorArea,component.textOrientation());
                }else if(component.componentType == 'Token'){
                    if(component.textOrientation() == 'UPSIDE_DOWN') {
                        var $enclosingElement = $('#editorDrawingAreaActual');
                        var $hintElement =  $("#" + component.id);
                        var posX, posY;
                        var bodyInfo = $enclosingElement[0].getBoundingClientRect();
                        var info = $hintElement[0].getBoundingClientRect();
                        posX = bodyInfo.width;
                        posY = 1;
                        maxX = bodyInfo.width;
                        minX = 10;
                        maxY = bodyInfo.height;

                        if (posX > maxX) posX = Math.round(maxX);
                        if (posY > maxY) posY = Math.round(maxY);
                        if (posX < minX) posX = Math.round(minX);

                        rewrapToken(component, posX, posY);
                        var comp = $("#"+component.id);
                        var height = comp[0].offsetHeight;
                        nvx.tokenBaseHeight = height;
                        minY = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                        if (posY < minY) posY = Math.round(minY);
                        var tokLine = nvx.tokenLine;
                        var editorWidth = nvx.editorWidth;
                        s.selectedComponent().posX(posX);
                        s.selectedComponent().posY(posY);
                        updateKendoElements(s.selectedComponent());
                        var offset = comp.offset();
                        comp.css("height",height * tokLine+"px");
                        comp.css("width",editorWidth/1.5+"px"); // reduce scale


                    }else if(component.textOrientation() == 'NORMAL') {
                        /*var comp = $("#"+component.id);
                         var height = comp[0].offsetHeight;
                         nvx.tokenBaseHeight = height;
                         var offset = comp.offset();
                         comp.css("height",height * nvx.tokenLine+"px");
                         comp.css("width",nvx.editorWidth/1.5+"px"); // reduce scale*/
                        var $enclosingElement = $('#editorDrawingAreaActual');
                        var $hintElement =  $("#" + component.id);
                        var posX, posY;
                        var bodyInfo = $enclosingElement[0].getBoundingClientRect();
                        var info = $hintElement[0].getBoundingClientRect();
                        posX = 1;
                        posY = 1;
                        maxX = bodyInfo.width - 10;
                        minX = 1;
                        minY = 1;
                        maxY = bodyInfo.height;

                        if (posX > maxX) posX = Math.round(maxX);
                        if (posX < minX) posX = Math.round(minX);

                        rewrapToken(component, posX, posY);
                        maxY = (($('.drawing-area-actual')[0].getBoundingClientRect().height) - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                        if (maxY < 1) {
                            return;
                        }
                        if (posY > maxY) posY = Math.round(maxY);
                        if (posY < minY) posY = minY;
                        var comp = $("#" + component.id);
                        var height = comp[0].offsetHeight;
                        nvx.tokenBaseHeight = height;


                        var tokLine = nvx.tokenLine;
                        var editorWidth = nvx.editorWidth;
                        s.selectedComponent().posX(posX);
                        s.selectedComponent().posY(posY);
                        updateKendoElements(s.selectedComponent());
                        var offset = comp.offset();
                        comp.css("height", height * tokLine + "px");
                        comp.css("width", editorWidth / 1.5 + "px"); // reduce scale
                    }else if(component.textOrientation() == 'CLOCKWISE_90') {
                        var $enclosingElement = $('#editorDrawingAreaActual');
                        var $hintElement =  $("#" + component.id);
                        var posX, posY;
                        var bodyInfo = $enclosingElement[0].getBoundingClientRect();
                        var info = $hintElement[0].getBoundingClientRect();
                        posX = bodyInfo.width;
                        posY = 1;
                        maxX = bodyInfo.width;
                        minY = 1;
                        maxY = bodyInfo.height-10;

                        if (posX > maxX) posX = Math.round(maxX);
                        if (posY > maxY) posY = Math.round(maxY);
                        if (posY < minY) posY = Math.round(minY);

                        rewrapToken(component, posX, posY);
                        var comp = $("#"+component.id);
                        var height = comp[0].offsetHeight;
                        nvx.tokenBaseHeight = height;
                        minX = (nvx.tokenBaseHeight * nvx.tokenLine * 1.8);
                        if (posX < minX) posX = Math.round(minX);
                        var tokLine = nvx.tokenLine;
                        var editorWidth = nvx.editorWidth;
                        s.selectedComponent().posX(posX);
                        s.selectedComponent().posY(posY);
                        updateKendoElements(s.selectedComponent());
                        var offset = comp.offset();
                        comp.css("height",height * tokLine+"px");
                        comp.css("width",editorWidth/1.5+"px"); // reduce scale
                    }else if(component.textOrientation() == 'COUNTERCLOCKWISE_90') {
                        var $enclosingElement = $('#editorDrawingAreaActual');
                        var $hintElement =  $("#" + component.id);
                        var posX, posY;
                        var bodyInfo = $enclosingElement[0].getBoundingClientRect();
                        var info = $hintElement[0].getBoundingClientRect();
                        posX = 1;
                        posY = bodyInfo.height;
                        minX = 1;
                        minY = 10;
                        maxY = bodyInfo.height;

                        if (posX < minX) posX = Math.round(minX);
                        if (posY > maxY) posY = Math.round(maxY);
                        if (posY < minY) posY = Math.round(minY);

                        rewrapToken(component, posX, posY);
                        var comp = $("#"+component.id);
                        var height = comp[0].offsetHeight;
                        nvx.tokenBaseHeight = height;
                        maxX = (bodyInfo.width - (nvx.tokenBaseHeight * nvx.tokenLine * 1.8));
                        if (posX > maxX) posX = Math.round(maxX);
                        var tokLine = nvx.tokenLine;
                        var editorWidth = nvx.editorWidth;
                        s.selectedComponent().posX(posX);
                        s.selectedComponent().posY(posY);
                        updateKendoElements(s.selectedComponent());
                        var offset = comp.offset();
                        comp.css("height",height * tokLine+"px");
                        comp.css("width",editorWidth/1.5+"px"); // reduce scale
                    }

                }
                else if(component.componentType == 'Barcode'){
                    var comp = $("#"+component.id).find(".component-barcode img");
                    comp.css("display","inline-block");
                }

            }

        };


        s.removeComponentFromView = function(data, event) {
            var component = s.selectedComponent();
            s.components.remove(component);
            s.selectedComponent(noComponentSelected);
        };



        s.selectThisComponent = function(data, event) {
            var prevComponent = s.selectedComponent();
            if (!prevComponent.unselected) {
                if (prevComponent.isNew()) {
                    $('#componentPicker' + prevComponent.componentType).find('.k-link').removeClass('k-state-selected');
                } else {
                    prevComponent.isSelected(false);
                }
            }

            updateKendoElements(data);
            data.isSelected(true);
            s.selectedComponent(data);
        };

        function updateKendoElements(data) {
            kPosX.value(data.posX());
            kPosY.value(data.posY());
            kToken.value(data.token());
            //kFont.value(data.font());
            kTextSize.value(data.textSize());
            //kTextWeight.value(data.textWeight());
            kTextStyle.value(data.textStyle());
            kTextOrientation.value(data.textOrientation());
            kBarcodeHeight.value(data.barcodeHeight());
            kBarcodeOrientation.value(data.barcodeOrientation());
        }


        s.uploadImageold = function(imgfile) {
            var fd = new FormData($('form')[0]);
            //fd.append('imagePath',imgfile);
            $.ajax({
                url: '/.templateeditor/templateeditor/upload-image1',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST', cache: false,
                success: function (data) {
                    if (data.success) {

                        alert("uploaded image");

                    } else {
                        alert('Error encountered saving template');
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        }

        s.getModifiedTemplate = function() {
            $.ajax({
                url: '/.templateeditor/templateeditor/get-original-xml',
                data: {
                    sid: s.axsessionId()
                },
                type: 'GET', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {

                        alert(data.data);

                    } else {
                        window.alert('Error encountered saving template');
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });
        }

        s.saveTemplate = function () {


            var xmlData = nvx.PaperTicketXmlHandler.exportXml(s.components());

            $.ajax({
                url: '/.templateeditor/templateeditor/save-template',
                data: {
                    sid: s.axsessionId(),
                    templateXML: xmlData
                },
                type: 'POST', cache: false, dataType: 'json',
                success: function (data) {
                    if (data.success) {

                        alert("Template is saved successfully.");

                    } else {
                        alert(data.message);
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            });

        };

        s.openExportWindow = function() {
            var xmlData = nvx.PaperTicketXmlHandler.exportXml(s.components());
            $('#exportWindowInput').val(xmlData);
            kExportWindow.center().open();
        };



        var initialisedImportButton = false;
        s.openImportWindow = function() {
            if (!initialisedImportButton) {
                initialisedImportButton = true;
                $('#importWindowButton').click(function() {
                    var inputXml = $('#importWindowInput').val();
                    try {

                        var doc = $.parseXML(inputXml);
                        if (doc == null) {
                            alert('Input is invalid, please try again.');
                        } else {
                            s.doImport(doc);
                        }
                    } catch (e) {
                        console.log('Error parsing input.' + e);
                        alert('Input is invalid, please try again.');
                        s.components([]);
                        s.selectedComponent(noComponentSelected);
                    }
                });
            }
            kImportWindow.center().open();
        };

        s.doImport = function(xmlDom) {
            s.components([]);
            s.selectedComponent(noComponentSelected);

            var comps = nvx.PaperTicketXmlHandler.importXml(xmlDom);

            $.each(comps, function (idx, mdl) {
                s.components.push(mdl);
                if(mdl.componentType == 'Token') {
                    rewrapToken(mdl, mdl.posX(), mdl.posY());
                    var comp = $("#" + mdl.id);
                    comp.css("width", nvx.editorWidth / 1.5 + "px"); // reduce scale
                    comp.css("height", nvx.tokenBaseHeight * nvx.tokenLine + "px");
                }
            });

            setTimeout(function() {
                $.each(s.components(), function(idx, mdl) {
                    mdl.initDraggable();
                });
            }, 500);

        };
    };


    nvx.TicketComponent = function(componentType) {
        var s = this;

        s.id = nvx.generateRandomId();
        s.componentType = componentType;

        //TODO bold italic
        //TODO z-index implementation
        //TODO issue with wrapping

        s.isSelected = ko.observable(true);
        s.isNew = ko.observable(true);
        s.posX = ko.observable('1');
        s.posY = ko.observable('1');
        s.font = ko.observable('VeraMono');
        s.text = ko.observable('Text'); //TODO Force to have value even if user removes all value. Or auto-delete.
        s.imgurl =  ko.observable('');
        //s.imgfile = ko.observable('');
        s.token = ko.observable('TicketNumber');
        s.tokenMaxLength = ko.observable('1');
        s.textSize = ko.observable(8);
        //s.textWeight = ko.observable('Normal');
        s.textStyle = ko.observable('Normal');
        s.textOrientation = ko.observable('NORMAL');
        s.barcodeHeight = ko.observable('Normal');
        s.barcodeOrientation = ko.observable('Vertical');
        s.logoSize = 0;

        s.initDraggable = function() {
            var $elem = $('#' + s.id);
            var topOffset,leftOffset;
            if($elem.hasClass("rotate-90")) {
                topOffset = $elem.width();
                leftOffset = 0;
            }
            else if($elem.hasClass("rotate90")) {
                topOffset = 0;
                leftOffset = $elem.height();
            }
            else if ($elem.hasClass("rotate180")) {
                topOffset = $elem.height();
                leftOffset = $elem.width();
            }
            else {
                topOffset = 0;
                leftOffset = 0;
            }
            $elem.kendoDraggable({
                //container: $('#editorDrawingAreaActual'),
                hint: function() {
                    //$elem.css("width", "100px");
                    return $elem.clone().addClass('draggable-hint-element');
                },
                cursorOffset: { top: topOffset, left: leftOffset}
            });
        };

        // s.tokenMaxLength = ko.pureComputed(function() {
        //    var toke = s.token();
        //     var maxLength;
        //     $.each(nvx.Tokens, function(idx, mdl) {
        //         if (mdl.token == toke) {
        //             maxLength = mdl.maxLength;
        //             return false;
        //         }
        //
        //     });
        //     return maxLength;
        // });
        s.tokenDisplay = ko.pureComputed(function() {
            var toke = s.token();
            var displayText = '';
            $.each(nvx.Tokens, function(idx, mdl) {
                if (mdl.token == toke) {
                        if(toke == 'Quantity') {
                        displayText = '{{Qty}}'+'<BR/>'+'next line';
                    }
                    else {
                        var token = mdl.displayText;

                        // for (var i = token.length; i < mdl.maxLength; i++) {
                        //     token = token + '1';
                        // }
                        //
                        // var orientation = s.textOrientation();
                        // if(orientation == 'NORMAL' || orientation == 'UPSIDE_DOWN') {
                        //     var totalArea = $('.drawing-area-actual')[0].getBoundingClientRect().width-s.posX();
                        //     nvx.editorWidth = totalArea;
                        //     //var info = $("#"+s.selectedComponent().id)[0].getBoundingClientRect().width;
                        //     var textSize  = visualLengthHorizontal(token,s.id);
                        //     nvx.tokenLine = Math.ceil(textSize/totalArea);
                        //     console.log("Lines:" + Math.ceil(textSize/totalArea));
                        // }else if(orientation == 'CLOCKWISE_90' || orientation == 'COUNTERCLOCKWISE_90') {
                        //     var totalArea = $('.drawing-area-actual')[0].getBoundingClientRect().height-s.posY();
                        //     nvx.editorWidth = totalArea;
                        //     //var info = $("#"+s.selectedComponent().id)[0].getBoundingClientRect().width;
                        //     var textSize  = visualLengthVertical(token,s.id);
                        //     nvx.tokenLine = Math.ceil(textSize/totalArea);
                        //     console.log("Lines:" + Math.ceil(textSize/totalArea));
                        // }
                        
                        //var token1 = wordWrap1(token,20);
                        //token1 = replaceAll(token1,' ','&nbsp;');

                        // $("#"+s.id).css("maxWidth",$('.drawing-area-actual')[0].getBoundingClientRect().width-s.posX()+"px");
                        displayText = mdl.displayText;
                    }
                    return false;
                }
            });
            return displayText;
        });

      visualLengthHorizontal = function(text,element,computeLessWidth)
        {

            var ruler = $("#"+element).find("#ruler")[0];
            var length;
            if(computeLessWidth) {
                $("#"+element).find("#ruler").empty();
                $("#"+element).find("#ruler").css("display","inline");
            }
            else {
                $("#"+element).find("#ruler").css("display","block");
            }
            ruler.innerHTML = text;
            $("#"+element).css("width",""); // reduce scale
           // $("#"+element).find("#ruler").css("width","");

            // if(fontsize=='6'){
            //     $("#ruler").addClass('text-size-small');
            //     length = ruler.getBoundingClientRect().width;
            //     console.log("width"+ruler.getBoundingClientRect().width);
            //     $("#ruler").removeClass('text-size-small');
            // }else if(fontsize=='8'){
            //     $("#ruler").addClass('text-size-normal');
            //     length = ruler.getBoundingClientRect().width;
            //     console.log("width"+ruler.getBoundingClientRect().width);
            //     $("#ruler").removeClass('text-size-normal');
            // }else if(fontsize=='14'){
            //     $("#ruler").addClass('text-size-large');
            //     length = ruler.getBoundingClientRect().width;
            //     console.log("width"+ruler.getBoundingClientRect().width);
            //     $("#ruler").removeClass('text-size-large');
            // }else if(fontsize =='18') {
            //     $("#ruler").addClass('text-size-xlarge');
            //     length = ruler.getBoundingClientRect().width;
            //     console.log("width"+ruler.getBoundingClientRect().width);
            //     $("#ruler").removeClass('text-size-xlarge');
            // }
            var height = ruler.offsetHeight;
            nvx.tokenBaseHeight = height;
            length = ruler.getBoundingClientRect().width;
            $("#"+element).find("#ruler").css("display","none");
            console.log("width"+length);
            return length;
        };

        visualLengthVertical = function(text,element)
        {
            var ruler = $("#"+element).find("#ruler")[0];
            var length;
            ruler.innerHTML = text;
            $("#"+element).css("width","");
            $("#"+element).find("#ruler").css("display","block");
            var height = ruler.offsetHeight;
            nvx.tokenBaseHeight = height;
            length = ruler.getBoundingClientRect().height;
            $("#"+element).find("#ruler").css("display","none");
            console.log("width"+length);
            return length;
        };

        window.getWidthOfText = function(txt, fontname, fontsize){
            // Create a dummy canvas (render invisible with css)
            var c=document.createElement('canvas');
            // Get the context of the dummy canvas
            var ctx=c.getContext('2d');
            // Set the context.font to the font that you are using
            ctx.font = fontsize + 'px' + fontname;
            // Measure the string
            // !!! <CRUCIAL>  !!!
            var length = ctx.measureText(txt).width;
            // !!! </CRUCIAL> !!!
            // Return width
            return length;
        };

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(find, 'g'), replace);
        }

        function wordwrap( str, width, brk, cut ) {

            brk = brk || 'n';
            width = width || 75;
            cut = cut || false;

            if (!str) { return str; }

            var regex = '.{1,' +width+ '}(\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\S+?(\s|$)');

            return str.match( RegExp(regex, 'g') ).join( brk );

        }

        function wordWrap1(str, maxWidth) {
            var newLineStr = "<br/>"; done = false; res = '';
            do {
                found = false;
                // Inserts new line at first whitespace of the line
                for (i = maxWidth - 1; i >= 0; i--) {
                    if (testWhite(str.charAt(i))) {orientation == 'NORMAL' || orientation == 'UPSIDE_DOWN'
                        res = res + [str.slice(0, i), newLineStr].join('');
                        str = str.slice(i + 1);
                        found = true;
                        break;
                    }
                }
                // Inserts new line at maxWidth position, the word is too long to wrap
                if (!found) {
                    res += [str.slice(0, maxWidth), newLineStr].join('');
                    str = str.slice(maxWidth);
                }

                if (str.length < maxWidth)
                    done = true;
            } while (!done);

            return res+str;
        }

        function testWhite(x) {
            var white = new RegExp(/^\s$/);
            return white.test(x.charAt(0));
        };

        s.componentCss = ko.pureComputed(function() {
            var classes = '';

            if(s.componentType == 'Text' || s.componentType == 'FilledText' || s.componentType == 'Token') {
                switch (s.textSize()) {
                    case '6': classes = 'text-size-small'; break;
                    case '8': classes = 'text-size-normal'; break;
                    case '14': classes = 'text-size-large'; break;
                    case '18': classes = 'text-size-xlarge'; break;
                    default: classes = 'text-size-normal';
                }
            }


            if(s.componentType == 'Barcode') {
                switch (s.barcodeHeight()) {
                    case 'Normal':
                        classes += ' barcode-height-normal';
                        break;
                    case 'Short':
                        classes += ' barcode-height-small';
                        break;
                }

                switch (s.barcodeOrientation()) {
                    case 'Horizontal':
                        classes += ' barcode-rotate90 barcode-width-small';

                }
            }

            // switch (s.textWeight()) {
            //     case 'Bold': classes += ' text-weight-bold';
            // }

            switch (s.textStyle()) {
                case 'Bold': classes += ' text-weight-bold'; break;
                case 'Italic': classes += ' text-style-italic'; break;
                case 'BoldItalic': classes += ' text-weight-bold text-style-italic'; break;
            }

            switch (s.textOrientation()) {
                case 'CLOCKWISE_90': classes += ' rotate90'; break;
                case 'COUNTERCLOCKWISE_90': classes += ' rotate-90'; break;
                case 'UPSIDE_DOWN': classes += ' rotate180'; break;
            }

            if (s.isSelected()) {
                classes += ' component-selected';
            }
            // if(s.font() == 'Sans Serif') {
            //     classes += ' font-serif';
            // }
            // else {
            //     classes += ' font-vera';
            // }



            return classes;
        });
    };

    nvx.PosOffsetAdjustment = 10;

    nvx.TextSizes = [{
        id: '1,1',
        displayText: 'Small',
        size: 6
    }, {
        id: '1,2',
        displayText: 'Normal',
        size: 8
    }, {
        id: '2,4',
        displayText: 'Large',
        size: 14
    }, {
        id: '3,6',
        displayText: 'X-Large',
        size: 18
    }];

    nvx.BarcodeHeights = [{
        id: 'Normal',
        displayText: 'Normal',
        lines: '10'
    }, {
        id: 'Short',
        displayText: 'Short',
        lines: '5'
    }];

    nvx.BarcodeOrientations = [{
        id: 'Vertical',
        displayText: 'Vertical'
    }, {
        id: 'Horizontal',
        displayText: 'Horizontal'
    }];

    nvx.Fonts = [{
        id: 'VeraMono',
        displayText: 'VeraMono'
    }, {
        id: 'Sans Serif',
        displayText: 'Sans Serif'
    }];

    nvx.TextWeights = [{
        id: 'Normal',
        displayText: 'Normal'
    }, {
        id: 'Bold',
        displayText: 'Bold'
    }];

    nvx.TextStyles = [{
        id: 'Normal',
        displayText: 'Normal'
    }, {
        id: 'Bold',
        displayText: 'Bold'
    }, {
        id: 'Italic',
        displayText: 'Italic'
    }, {
        id: 'BoldItalic',
        displayText: 'Bold+Italic'
    }];

    nvx.TextOrientations = [{
        id: 'NORMAL',
        displayText: 'Normal'
    }, {
        id: 'CLOCKWISE_90',
        displayText: 'Rotate-Clockwise'
    },{
        id: 'COUNTERCLOCKWISE_90',
        displayText: 'Rotate-CounterClockwise'
    },{
        id: 'UPSIDE_DOWN',
        displayText: 'Upside Down'
    }];

    nvx.Tokens = [{
        token: 'TicketNumber',
        displayText: '{{Ticket Number}}',
        maxLength: '60'
    }, {
        token: 'TicketCode',
        displayText: '{{Ticket Code}}',
        maxLength: '60'
    }, {
        token: 'PaxPerTicket',
        displayText: '{{Pax per Ticket}}',
        maxLength: '1'
    }, {
        token: 'TicketType',
        displayText: '{{Ticket Type}}',
        maxLength: '20'
    }, {
        token: 'PublishPrice',
        displayText: '{{Publish Price}}',
        maxLength: '12'
    }, {
        token: 'UsageValidity',
        displayText: '{{Usage Validity}}',
        maxLength: '20'
    }, {
        token: 'TicketValidity',
        displayText: '{{Ticket Validity}}',
        maxLength: '20'
    }, {
        token: 'ValidityStartDate',
        displayText: '{{Valid From}}',
        maxLength: '23'
    }, {
        token: 'ValidityEndDate',
        displayText: '{{Valid To}}',
        maxLength: '23'
    },{
        token: 'LegalEntity',
        displayText: '{{Legal Entity}}',
        maxLength: '4'
    },{
        token: 'CustomerName',
        displayText: '{{Customer Name}}',
        maxLength: '100'
    },{
        token: 'ProductImage',
        displayText: '{{Product Image - CMS}}',
        maxLength: '60'
    }, {
        token: 'ItemNumber',
        displayText: '{{Item Number}}',
        maxLength: '20'
    },{
        token: 'DisplayName',
        displayText: '{{Product Name}}',
        maxLength: '60'
    },  {
        token: 'TicketLineDesc',
        displayText: '{{Ticket Line Description}}',
        maxLength: '60'
    },  {
        token: 'EventName',
        displayText: '{{Event Name}}',
        maxLength: '60'
    },{
        token: 'PrintLabel',
        displayText: '{{Print Label}}',
        maxLength: '60'
    }, {
        token: 'Description',
        displayText: '{{Product Ticket Description}}',
        maxLength: '1000'
    },  {
        token: 'ProductSearchName',
        displayText: '{{Product Search Name}}',
        maxLength: '20'
    },  {
        token: 'ProductVarConfig',
        displayText: '{{Product Variant (configuration)}}',
        maxLength: '10'
    }, {
        token: 'ProductVarSize',
        displayText: '{{Product Variant (Size)}}',
        maxLength: '10'
    }, {
        token: 'ProductVarColor',
        displayText: '{{Product Variant (Color)}}',
        maxLength: '10'
    }, {
        token: 'ProductVarStyle',
        displayText: '{{(Product Variant (Style)}}',
        maxLength: '10'
    }, {
        token: 'PackageItem',
        displayText: '{{Package ItemId}}',
        maxLength: '20'
    }, {
        token: 'PackageItemName',
        displayText: '{{Package Item Name}}',
        maxLength: '60'
    }, {
        token: 'PackagePrintLabel',
        displayText: '{{Package Print Labe}}',
        maxLength: '1999'
    }, {
        token: 'MixMatchPackageName',
        displayText: '{{Mix & Match Package Name}}',
        maxLength: '60'
    },{
        token: 'MixMatchDescription',
        displayText: '{{Mix & Match Description}}',
        maxLength: '60'
    },{
        token: 'EventDate',
        displayText: '{{Event date}}',
        maxLength: '12'
    },{
        token: 'Facilities',
        displayText: '{{Facility}}',
        maxLength: '60'
    },{
        token: 'FacilityAction',
        displayText: '{{Facility Action}}',
        maxLength: '10'
    },{
        token: 'OperationIds',
        displayText: '{{Facility Operation}}',
        maxLength: '60'
    },{
        token: 'DiscountApplied',
        displayText: '{{Discount Applied}}',
        maxLength: '60'
    },{
        token: 'TAC',
        displayText: '{{Terms and Condition}}',
        maxLength: '1999'
    }, {
        token: 'Disclaimer',
        displayText: '{{Disclaimer}}',
        maxLength: '1999'
    }];

    nvx.ComponentTypes = {
        QrCode: {
            type: 'QrCode'
        },
        Barcode: {
            type: 'Barcode'
        },
        Text: {
            type: 'Text'
        },
        Token: {
            type: 'Token'
        },
        FilledText: {
            type: 'FilledText'
        },
        Logo: {
            type: 'Logo'
        }
    };

    nvx.generateRandomId = function() {
        return Math.random().toString(36).substring(7);
    };

})(window.nvx = window.nvx || {}, jQuery, ko);