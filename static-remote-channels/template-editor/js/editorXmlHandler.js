(function(nvx, $, ko) {

    nvx.PaperTicketXmlHandler = {
        importXml: function(xmlDom) {
            var tokenMap = {};
            $.each(nvx.Tokens, function(idx, mdl) {
                tokenMap[mdl.token] = true;
            });

            var comps = [];
            var $dom = $(xmlDom);
            $.each($dom.find('Ticket').children(), function(idx, node) {
                console.log(node);
                var $node = $(node);
                var component = null;
                var nodeName = node.nodeName;
                switch (nodeName) {
                    case 'QRCode':
                        component = new nvx.TicketComponent(nvx.ComponentTypes.QrCode.type);
                        break;
                    case 'BarCode':
                        component = new nvx.TicketComponent(nvx.ComponentTypes.Barcode.type);
                        var bcHeight = $node.find('Height')[0].textContent;
                        switch (bcHeight) {
                            case '5': component.barcodeHeight('Short'); break;
                            default: component.barcodeHeight('Normal');
                        }
                        var isHorizontal = $node.find('Orientation').length > 0;
                        if(isHorizontal)
                            component.barcodeOrientation('Horizontal');
                        else
                            component.barcodeOrientation('Vertical');
                        break;
                    case 'Text':
                        var textDataWithDel = $node.find('Data')[0].textContent;
                        var textData = textDataWithDel.slice(2,textDataWithDel.length-2);
                        var isToken = tokenMap[textData] != null;
                        component = new nvx.TicketComponent(isToken ? nvx.ComponentTypes.Token.type : nvx.ComponentTypes.Text.type);
                        if (isToken) {
                            component.token(textData);
                        } else {
                            component.text(textDataWithDel.replace('&amp;', '&'));
                        }

                        var isBold = $node.find('Bold').length > 0;
                        if (isBold) {
                            component.textStyle('Bold');
                        }
                        var isItalic = $node.find('Italic').length > 0;
                        if (isItalic) {
                            component.textStyle('Italic');
                        }
                        var isBoldItalic = ($node.find('Bold').length > 0) &&  ($node.find('Italic').length > 0);
                        if(isBoldItalic) {
                            component.textStyle('BoldItalic');
                        }
                        var tn = $node.find('Orientation')[0].textContent;
                        component.textOrientation(tn);
                        //var fh = $node.find('FontHeight')[0].innerHTML;
                        //var fw = $node.find('FontWidth')[0].innerHTML;

                        //component.textSize(fw + ',' + fh);
                        var fs = $node.find('FontSize')[0].textContent;
                        component.textSize(fs);
                        break;
                    case 'FilledText':
                        var textData = $node.find('Data')[0].textContent;

                        component = new nvx.TicketComponent(nvx.ComponentTypes.FilledText.type);
                        component.text(textData.replace('&amp;', '&'));

                        var isBold = $node.find('Bold').length > 0;
                        if (isBold) {
                            component.textStyle('Bold');
                        }
                        var isItalic = $node.find('Italic').length > 0;
                        if (isItalic) {
                            component.textStyle('Italic');
                        }
                        var isBoldItalic = ($node.find('Bold').length > 0) &&  ($node.find('Italic').length > 0);
                        if(isBoldItalic) {
                            component.textStyle('BoldItalic');
                        }
                        // var fh = $node.find('FontHeight')[0].innerHTML;
                        // var fw = $node.find('FontWidth')[0].innerHTML;
                        // component.textSize(fw + ',' + fh);
                        var fs = $node.find('FontSize')[0].textContent;
                        component.textSize(fs);
                        break;
                    case 'Image':
                        var imageData = $node.find('Data')[0].textContent;
                        component = new nvx.TicketComponent(nvx.ComponentTypes.Logo.type);
                        component.text(imageData);
                        var urlData = $node.find('URLData')[0].textContent;
                        component.imgurl(urlData);
                        break;
                }
                if (component != null) {
                    var posX = Math.floor(Number($node.find('X')[0].textContent));
                    var posY = Math.floor(Number($node.find('Y')[0].textContent));
                    component.isNew(false);
                    component.isSelected(false);
                    component.posX(posX);
                    component.posY(posY);

                    comps.push(component);
                }
            });

            return comps;
        },
        exportXml: function(components) {
            var xml = [];

            xml.push(
                '<TicketPackage>' +
                '<FontData>' +
                '<Path>VeraMono.ttf</Path>' +
                '</FontData>' +
                '<Ticket>'
            );

            $.each(components, function(idx, component) {
                switch (component.componentType) {
                    case nvx.ComponentTypes.QrCode.type:
                        xml.push(
                            '<QRCode>' +
                            '<Version>11</Version><DotSize>3</DotSize><ErrorCorrectionLevel>1</ErrorCorrectionLevel>'
                        );
                        xml.push(
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            '<Data>{{TicketDataQRCode}}</Data>'
                        );
                        xml.push(
                            '</QRCode>'
                        );
                        break;
                    case nvx.ComponentTypes.Barcode.type:
                        var bcHeight = '10';
                        switch (component.barcodeHeight()) {
                            case 'Short': bcHeight = '5'; break;
                        }
                        xml.push(
                            '<BarCode>' +
                            '<Symbology>CODE_128</Symbology><Code>A</Code>' +
                            '<Height>' + bcHeight + '</Height>' +
                            '<LinesDimension>2</LinesDimension>'
                        );
                        xml.push(
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            '<Data>{{TicketDataQRCode}}</Data>'
                        );
                        if (component.barcodeOrientation() == 'Horizontal') {
                            xml.push('<Orientation>HORIZONTAL</Orientation>');
                        }
                        xml.push(
                            '</BarCode>'
                        );
                        break;
                    case nvx.ComponentTypes.Text.type:
                        var ts = component.textSize();
                        var additionalFormatting = '';
                        // if (component.textWeight() == 'Bold') {
                        //     additionalFormatting += '<Bold>true</Bold>';
                        // }
                        if (component.textStyle() == 'Italic') {
                            additionalFormatting += '<Italic>true</Italic>';
                        }
                        else if(component.textStyle() == 'Bold') {
                            additionalFormatting += '<Bold>true</Bold>';
                        }
                        else if(component.textStyle() == 'BoldItalic') {
                            additionalFormatting += '<Bold>true</Bold><Italic>true</Italic>';
                        }
                        additionalFormatting += '<Font>' + component.font() + '.ttf</Font>';

                        additionalFormatting += '<Orientation>' + component.textOrientation() + '</Orientation>';
                        xml.push(
                            '<Text>'
                        );
                        xml.push(
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            additionalFormatting +
                            '<FontSize>' + ts + '</FontSize>' +

                            '<Data>' + component.text().replace('&', '&amp;') + '</Data>'
                        );
                        xml.push(
                            '</Text>'
                        );
                        break;
                    case nvx.ComponentTypes.Token.type:
                        var ts = component.textSize();
                        var additionalFormatting = '';
                        if (component.textStyle() == 'Italic') {
                            additionalFormatting += '<Italic>true</Italic>';
                        }
                        else if(component.textStyle() == 'Bold') {
                            additionalFormatting += '<Bold>true</Bold>';
                        }
                        else if(component.textStyle() == 'BoldItalic') {
                            additionalFormatting += '<Bold>true</Bold><Italic>true</Italic>';
                        }
                        additionalFormatting += '<Font>' + component.font() + '.ttf</Font>';
                        additionalFormatting += '<Orientation>' + component.textOrientation() + '</Orientation>';
                        additionalFormatting += '<MaxLength>' + component.tokenMaxLength() + '</MaxLength>';
                        xml.push(
                            '<Text>'
                        );
                        xml.push(
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            additionalFormatting +
                            '<FontSize>' + ts + '</FontSize>' +

                            '<Data>{{' + component.token() + '}}</Data>'
                        )
                        xml.push(
                            '</Text>'
                        );
                        break;
                    case nvx.ComponentTypes.FilledText.type:
                        var ts = component.textSize();
                        var additionalFormatting = '';
                        if (component.textStyle() == 'Italic') {
                            additionalFormatting += '<Italic>true</Italic>';
                        }
                        else if(component.textStyle() == 'Bold') {
                            additionalFormatting += '<Bold>true</Bold>';
                        }
                        else if(component.textStyle() == 'BoldItalic') {
                            additionalFormatting += '<Bold>true</Bold><Italic>true</Italic>';
                        }
                        additionalFormatting += '<Font>' + component.font() + '.ttf</Font>';
                        additionalFormatting += '<Orientation>' + component.textOrientation() + '</Orientation>';
                        xml.push(
                            '<FilledText>'
                        );
                        xml.push(
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            additionalFormatting +
                            '<FontSize>' + ts + '</FontSize>' +

                            '<Data>' + component.text().replace('&', '&amp;') + '</Data>'
                        );
                        xml.push(
                            '</FilledText>'
                        );
                        break;
                    case nvx.ComponentTypes.Logo.type:
                        xml.push(
                            '<Image>' +
                            '<X>' + component.posX() + '</X>' +
                            '<Y>' + component.posY() + '</Y>' +
                            '<Id>5</Id>' +
                            '<Data>' + component.text() + '</Data>' +
                            '<URLData>' + component.imgurl() + '</URLData>' +
                            '</Image>'
                        );
                }
            });

            xml.push(
                '</Ticket>' +
                '</TicketPackage>'
            );

            return xml.join('');
        }


    };

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    }
})(window.nvx = window.nvx || {}, jQuery, ko);