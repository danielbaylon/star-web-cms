#!/bin/sh

export MAVEN_REPO_DIR=/Users/houtao/Tools/Installed/maven/repo
export APP_BASE_DIR=/Users/houtao/Enovax/Star/Gold/svn/sdc_star/branches/applications/star-cms

export MAVEN_ENOVAX_DIR=$MAVEN_REPO_DIR/com/enovax
export MAVEN_STAR_DIR=$MAVEN_ENOVAX_DIR/star
export APP_LIB_DIR=$APP_BASE_DIR/star-cms-magnolia/star-cms-magnolia-webapp/target/star-cms-magnolia-webapp-1.0/WEB-INF/lib

## enovax-jcr-utils
cd $APP_BASE_DIR/enovax-jcr-utils/
rm -rf $MAVEN_ENOVAX_DIR/util/jcr/enovax-jcr-utils/1.0/enovax-jcr-utils-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
    echo "compile enovax-jcr-utils failed" 1>&2
    exit 1
fi
rm -rf $APP_LIB_DIR/enovax-jcr-utils-1.0.jar
cp $MAVEN_ENOVAX_DIR/util/jcr/enovax-jcr-utils/1.0/enovax-jcr-utils-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""


## star-project-shared
cd $APP_BASE_DIR/star-project-shared/
rm -rf $MAVEN_STAR_DIR/star-project-shared/1.0/star-project-shared-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-project-shared failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-project-shared-1.0.jar
cp $MAVEN_STAR_DIR/star-project-shared/1.0/star-project-shared-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-shared
cd $APP_BASE_DIR/star-module-shared/
rm -rf $MAVEN_STAR_DIR/star-module-shared/1.0/star-module-shared-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-shared failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-shared-1.0.jar
cp $MAVEN_STAR_DIR/star-module-shared/1.0/star-module-shared-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-api-template-editor
cd $APP_BASE_DIR/star-module-api-template-editor/
rm -rf $MAVEN_STAR_DIR/star-module-api-template-editor/1.0/star-module-api-template-editor-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
    echo "compile star-module-api-template-editor failed" 1>&2
    exit 1
fi
rm -rf $APP_LIB_DIR/star-module-api-template-editor-1.0.jar
cp $MAVEN_STAR_DIR/star-module-api-template-editor/1.0/star-module-api-template-editor-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-templating-kit
cd $APP_BASE_DIR/star-module-templating-kit/
rm -rf $MAVEN_STAR_DIR/star-module-templating-kit/1.0/star-module-templating-kit-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
    echo "compile star-module-templating-kit failed" 1>&2
    exit 1
fi
rm -rf $APP_LIB_DIR/star-module-templating-kit-1.0.jar
cp $MAVEN_STAR_DIR/star-module-templating-kit/1.0/star-module-templating-kit-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""



## star-module-partner-portal-shared
cd $APP_BASE_DIR/star-module-partner-portal-shared/
rm -rf $MAVEN_STAR_DIR/star-module-partner-portal-shared/1.0/star-module-partner-portal-shared-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-partner-portal-shared failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-partner-portal-shared-1.0.jar
cp $MAVEN_STAR_DIR/star-module-partner-portal-shared/1.0/star-module-partner-portal-shared-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-api-store
cd $APP_BASE_DIR/star-module-api-store
rm -rf $MAVEN_STAR_DIR/star-module-api-store/1.0/star-module-api-store-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-api-store failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-api-store-1.0.jar
cp $MAVEN_STAR_DIR/star-module-api-store/1.0/star-module-api-store-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""


## star-module-product-categories
cd $APP_BASE_DIR/star-module-product-categories/
rm -rf $MAVEN_STAR_DIR/star-module-product-categories/1.0/star-module-product-categories-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-product-categories failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-product-categories-1.0.jar
cp $MAVEN_STAR_DIR/star-module-product-categories/1.0/star-module-product-categories-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-product-sync
cd $APP_BASE_DIR/star-module-product-sync/
rm -rf $MAVEN_STAR_DIR/star-module-product-sync/1.0/star-module-product-sync-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-product-sync failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-product-sync-1.0.jar
cp $MAVEN_STAR_DIR/star-module-product-sync/1.0/star-module-product-sync-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""


## star-module-products
cd $APP_BASE_DIR/star-module-products/
rm -rf $MAVEN_STAR_DIR/star-module-products/1.0/star-module-products-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-products failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-products-1.0.jar
cp $MAVEN_STAR_DIR/star-module-products/1.0/star-module-products-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""



## star-module-partner-portal-admin
cd $APP_BASE_DIR/star-module-partner-portal-admin
rm -rf $MAVEN_STAR_DIR/star-module-partner-admin/1.0/star-module-partner-admin-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
    echo "compile star-module-partner-portal-admin failed" 1>&2
    exit 1
fi
rm -rf $APP_LIB_DIR/star-module-partner-admin-1.0.jar
cp $MAVEN_STAR_DIR/star-module-partner-admin/1.0/star-module-partner-admin-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""


## star-module-admin-config
cd $APP_BASE_DIR/star-module-admin-config
rm -rf $MAVEN_STAR_DIR/star-module-admin-config/1.0/star-module-admin-config-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-admin-config failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-admin-config-1.0.jar
cp $MAVEN_STAR_DIR/star-module-admin-config/1.0/star-module-admin-config-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""


## star-module-sky-dining-shared
cd $APP_BASE_DIR/star-module-sky-dining-shared
rm -rf $MAVEN_STAR_DIR/star-module-sky-dining-shared/1.0/star-module-sky-dining-shared-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-sky-dining-shared failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-sky-dining-shared-1.0.jar
cp $MAVEN_STAR_DIR/star-module-sky-dining-shared/1.0/star-module-sky-dining-shared-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-sky-dining-admin
cd $APP_BASE_DIR/star-module-sky-dining-admin
rm -rf $MAVEN_STAR_DIR/star-module-sky-dining-admin/1.0/star-module-sky-dining-admin-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-sky-dining-admin failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-sky-dining-admin-1.0.jar
cp $MAVEN_STAR_DIR/star-module-sky-dining-admin/1.0/star-module-sky-dining-admin-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-kiosk-info
cd $APP_BASE_DIR/star-module-kiosk-info
rm -rf $MAVEN_STAR_DIR/star-module-kiosk-info/1.0/star-module-kiosk-info-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-kiosk-info failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-kiosk-info-1.0.jar
cp $MAVEN_STAR_DIR/star-module-kiosk-info/1.0/star-module-kiosk-info-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

## star-module-kiosk-group
cd $APP_BASE_DIR/star-module-kiosk-group
rm -rf $MAVEN_STAR_DIR/star-module-kiosk-group/1.0/star-module-kiosk-group-1.0.jar
mvn clean install -Dmaven.test.skip=true
if [ "$?" -ne "0" ]; then
echo "compile star-module-kiosk-group failed" 1>&2
exit 1
fi
rm -rf $APP_LIB_DIR/star-module-kiosk-group-1.0.jar
cp $MAVEN_STAR_DIR/star-module-kiosk-group/1.0/star-module-kiosk-group-1.0.jar $APP_LIB_DIR/
rm -rf target
echo ""
echo ""
echo ""

echo "compile all module successfully" 1>&2
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
exit 0



