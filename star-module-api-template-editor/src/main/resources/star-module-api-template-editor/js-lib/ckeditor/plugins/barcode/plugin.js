/**
 * Created by lavanya on 2/8/16.
 */
CKEDITOR.plugins.add( 'barcode', {
    icons: 'barcode',
    init: function( editor ) {
        editor.ui.addButton( 'Barcode', {
            label: 'Insert Barcode',
            command: 'insertBarcode',
            toolbar: 'insert'
        });
        editor.addContentsCss('js-lib/ckeditor/plugins/barcode/styles/example.css' );
        editor.addCommand( 'insertBarcode', {
            exec: function( editor ) {
                function convertImgToBase64(url, callback, outputFormat){
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function () {
                        var url = URL.createObjectURL(this.response), img = new Image();
                        img.onload = function () {
                            var canvas = document.createElement('CANVAS');
                                var ctx = canvas.getContext('2d');
                                canvas.height = img.height;
                                canvas.width = img.width;
                                ctx.drawImage(img,0,0);
                                var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                                callback(dataURL);
                                canvas = null;
                            URL.revokeObjectURL(url);
                        };
                        img.src = url;
                    };
                    xhr.open('GET', url, true);
                    xhr.responseType = 'blob';
                    xhr.send();
                }
                convertImgToBase64('js-lib/ckeditor/plugins/barcode/icons/barcode.png',function(base64Img){
                    console.log('IMAGE:',base64Img);
                    editor.insertHtml( '<img data-template="BarCode" src="' +base64Img+'" width="150" height="100" data-cke-filter="off"/>');
                });
            }

        });
    }
});