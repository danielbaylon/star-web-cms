/**
 * Created by lavanya on 4/8/16.
 */
CKEDITOR.plugins.add( 'export', {

    init: function (editor) {
        editor.ui.addButton('export', {
            label: 'Export',
            command: 'export',
            toolbar: 'document,99'
        });
        //editor.addContentsCss('js-lib/ckeditor/plugins/barcode/styles/example.css' );
        editor.addCommand('export', new CKEDITOR.dialogCommand('exportDialog'));
        CKEDITOR.dialog.add('exportDialog', function (editor) {
            return {
                title: 'Export Template',
                minWidth: 400,
                minHeight: 500,

                contents: [
                    {
                        id: 'tab-basic',
                        label: 'Basic Settings',
                        elements: [
                            {
                                id: 'textarea_html',
                                type: 'textarea',
                                label: 'HTML',

                                rows: 10,
                                cols: 50
                                //validate: CKEDITOR.dialog.validate.notEmpty( "Abbreviation field cannot be empty." )
                            }
                        ]
                    }
                ]
            };
        });

        CKEDITOR.dialog.on( 'resize', function( evt )
        {
            var data = evt.data;
            var height = 100;
            //alert(this);
            //var dialog = this.getDialog();
            //alert(dialog);
            evt.data.getTarget().getContentElement('textarea_html').style.height = height + 'px';
            //this.getContentElement('textarea_html').style.height = height + 'px';
            //document.getElementById('textarea_html').style.height = height + 'px' ;
            //document.getElementById('cke_117_textarea').style.height = height + 'px' ;
        });
    }
});