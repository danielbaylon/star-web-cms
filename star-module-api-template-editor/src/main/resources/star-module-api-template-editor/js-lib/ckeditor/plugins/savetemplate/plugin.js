/**
 * Created by lavanya on 4/8/16.
 */
CKEDITOR.plugins.add( 'savetemplate', {
    icons: 'save',
    init: function( editor ) {
        editor.ui.addButton( 'SaveTemplate', {
            label: 'Save Template',
            command: 'saveTemplate',
            toolbar: 'document,2',
            icon: this.path + 'icons/save.png'
        });
        editor.addContentsCss('js-lib/ckeditor/plugins/barcode/styles/example.css' );
        editor.addCommand( 'saveTemplate', {
            exec: function( editor ) {
                var xmlData = editor.document.getBody().getHtml();
                //alert(editor.config.customValues.sid);
                $.ajax({
                    url: '/.templateeditor/templateeditor/save-template',
                    data: {
                        sid: editor.config.customValues.sid,
                        templateXML: xmlData
                    },
                    type: 'POST', cache: false, dataType: 'json',
                    success: function (data) {
                        if (data.success) {

                            alert("Template is saved successfully.");

                        } else {
                            alert(data.message);
                        }
                    },
                    error: function () {
                    },
                    complete: function () {
                    }
                });
            }

        });
    }
});