/**
 * Created by lavanya on 2/8/16.
 */
/**
 * Created by lavanya on 2/8/16.
 */
CKEDITOR.plugins.add( 'qrcode', {
    icons: 'qrcode',
    init: function( editor ) {
        editor.ui.addButton( 'QRCode', {
            label: 'Insert QRCode',
            command: 'insertQRCode',
            toolbar: 'insert'
        });

        editor.addCommand( 'insertQRCode', {
            exec: function( editor ) {
                function convertImgToBase64(url, callback, outputFormat){
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function () {
                        var url = URL.createObjectURL(this.response), img = new Image();
                        img.onload = function () {
                            var canvas = document.createElement('CANVAS');
                            var ctx = canvas.getContext('2d');
                            canvas.height = img.height;
                            canvas.width = img.width;
                            ctx.drawImage(img,0,0);
                            var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                            callback(dataURL);
                            canvas = null;
                            URL.revokeObjectURL(url);
                        };
                        img.src = url;
                    };
                    xhr.open('GET', url, true);
                    xhr.responseType = 'blob';
                    xhr.send();
                }
                convertImgToBase64('js-lib/ckeditor/plugins/qrcode/icons/qrcode.png',function(base64Img){
                    console.log('IMAGE:',base64Img);
                    editor.insertHtml( '<img  data-template="QRCode" src="' +base64Img+'" width="150" height="150" data-cke-filter="off"/>');
                });
            }

        });
    }
});