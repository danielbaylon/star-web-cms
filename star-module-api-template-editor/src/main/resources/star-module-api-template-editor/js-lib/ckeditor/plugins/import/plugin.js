/**
 * Created by lavanya on 4/8/16.
 */
CKEDITOR.plugins.add( 'import', {
    //icons: 'barcode',
    init: function( editor ) {
        editor.ui.addButton( 'import', {
            label: 'Import',
            command: 'import',
            toolbar: 'document,100'
        });
        //editor.addContentsCss('js-lib/ckeditor/plugins/barcode/styles/example.css' );
        editor.addCommand( 'import', {
            exec: function( editor ) {
                function convertImgToBase64(url, callback, outputFormat){
                    var img = new Image();
                    img.crossOrigin = 'Anonymous';
                    img.onload = function(){
                        var canvas = document.createElement('CANVAS');
                        var ctx = canvas.getContext('2d');
                        canvas.height = this.height;
                        canvas.width = this.width;
                        ctx.drawImage(this,0,0);
                        var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                        callback(dataURL);
                        canvas = null;
                    };
                    img.src = url;
                }
                convertImgToBase64('js-lib/ckeditor/plugins/barcode/icons/barcode.png',function(base64Img){
                    console.log('IMAGE:',base64Img);
                    editor.insertHtml( '<img data-template="BarCode" src="' +base64Img+'" width="150" height="100" data-cke-filter="off"/>');
                });
            }

        });
    }
});