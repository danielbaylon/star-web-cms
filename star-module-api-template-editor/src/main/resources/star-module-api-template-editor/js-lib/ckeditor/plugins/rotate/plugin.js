/**
 * Created by lavanya on 2/8/16.
 */
CKEDITOR.plugins.add( 'rotate', {
    icons: 'barcode',
    init: function( editor ) {
        editor.ui.addButton( 'Rotate', {
            label: 'Rotate Barcode',
            command: 'rotateBarcode',
            toolbar: 'insert'
        });
        editor.addContentsCss('js-lib/ckeditor/plugins/rotate/styles/example.css' );
        editor.addCommand( 'rotateBarcode', {
            exec: function( editor ) {
                function convertImgToBase64(url, callback, outputFormat){
                    var img = new Image();
                    img.crossOrigin = 'Anonymous';
                    img.onload = function(){
                        var canvas = document.createElement('CANVAS');
                        var ctx = canvas.getContext('2d');
                        canvas.height = this.height;
                        canvas.width = this.width;
                        ctx.drawImage(this,0,0);
                        var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                        callback(dataURL);
                        canvas = null;
                    };
                    img.src = url;
                }
                //alert(editor.getSelection().getStartElement().getOuterHtml());
                var selection = editor.getSelection();
                var element = selection.getStartElement();
                var imageElement = element.getAscendant('div', true);
                var domImageElement = imageElement.$;
                if(domImageElement.hasAttribute("class")) {
                    var rotation = domImageElement.getAttribute("class");
                    if(rotation == "rotate90") {
                        domImageElement.setAttribute("class", "rotate180");
                    }
                    else if(rotation == "rotate180") {
                        domImageElement.setAttribute("class", "rotate270");
                    }
                    else if(rotation == "rotate270") {
                        domImageElement.removeAttribute("class");
                    }
                }
                else {
                    domImageElement.setAttribute("class", "rotate90");
                }

                //editor.getSelection().getStartElement().className = '.barcode-rotate90';
                // convertImgToBase64('js-lib/ckeditor/plugins/barcode/icons/barcode.png',function(base64Img){
                //     console.log('IMAGE:',base64Img);
                //     editor.insertHtml( '<img data-template="BarCode" src="' +base64Img+'" width="150" height="100" data-cke-filter="off"/>');
                // });
            }

        });
    }
});