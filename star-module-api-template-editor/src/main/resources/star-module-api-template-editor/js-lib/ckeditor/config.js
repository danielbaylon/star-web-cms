/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    // Remove multiple plugins.
    //config.toolbar = 'StandaloneToolbar';

config.font_names = 'Arial;Courier New;Georgia;Times New Roman;';
config.font_defaultLabel = 'Arial';
config.fontSize_defaultLabel = '12px';
    config.toolbar_StandaloneToolbar =
        [
            { name: 'document', items : [ 'Source' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll' ] },
            { name: 'insert', items : [ 'Table','base64image','Barcode','QRCode','token','imagerotate' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize','TextColor','BGColor' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
            '/',
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','CreateDiv','-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
            { name: 'links', items : [ 'Link','Unlink' ] },
            { name: 'tools', items : [ 'ShowBlocks' ] }
        ];

    config.toolbar_IntegratedToolbar =
        [
            { name: 'document', items : [ 'Source','-','SaveTemplate' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll' ] },
            { name: 'insert', items : [ 'Table','base64image','Barcode','QRCode','token','imagerotate' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize','TextColor','BGColor' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
            '/',
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','CreateDiv','-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
            { name: 'links', items : [ 'Link','Unlink' ] },
            { name: 'tools', items : [ 'ShowBlocks' ] }
        ];
    config.removePlugins = 'placeholder,placeholder_select,image,about,qrc,save,newpage,forms';
    config.width = 800;
    config.height = 500;

    config.placeholder_select = {
        placeholders:['Pax per Ticket','Ticket Type','Publish Price','Usage Validity','Ticket Validity','Valid From','Valid To','Legal Entity',
            'Ticket Number','Ticket Code','Customer Name','Product Image - CMS','Item Number','Product Name','Ticket Line Description','Event Name','Print Label','Product Search Name',
            'Product Ticket Description','Product Variant (configuration)','Product Variant (Size)','Product Variant (Color)','Product Variant (Style)','Package ItemId',
            'Package Item Name','Package Print Label','Mix & Match Package Name','Mix & Match Description','Event date','Facility','Facility Action','Facility Operation',
            'Discount Applied','Terms and Condition','Disclaimer'],
        format: '{{%placeholder%}}',

        label: 'Insert Tokens'
    };

    //config.stylesSet = 'default';


    config.extraPlugins = 'barcode,qrcode,imagerotate,savetemplate,token';
};
