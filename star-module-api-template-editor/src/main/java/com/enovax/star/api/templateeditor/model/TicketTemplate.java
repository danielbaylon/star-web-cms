package com.enovax.star.api.templateeditor.model;

/**
 * Created by lavanya on 21/7/16.
 */
public class TicketTemplate {

    private String sessionId;
    private String ticketType;
    private boolean isNew = false;
    private boolean isModified = false;
    private String originalTemplateXML;
    private String modifiedTemplateXML;

    public String getSessionId() { return sessionId; }

    public void setSessionId(String sessionId) { this.sessionId = sessionId; }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getModifiedTemplateXML() {
        return modifiedTemplateXML;
    }

    public void setModifiedTemplateXML(String modifiedTemplateXML) {
        this.modifiedTemplateXML = modifiedTemplateXML;
    }

    public String getOriginalTemplateXML() {
        return originalTemplateXML;
    }

    public void setOriginalTemplateXML(String originalTemplateXML) {
        this.originalTemplateXML = originalTemplateXML;
    }

    public String getTicketType() { return ticketType; }

    public void setTicketType(String ticketType) { this.ticketType = ticketType; }

}
