package com.enovax.star.api.templateeditor.controller;

import com.enovax.star.api.templateeditor.constant.ApiErrorCodes;
import com.enovax.star.api.templateeditor.model.TicketTemplate;
import com.enovax.star.api.templateeditor.model.api.ApiResult;
import com.enovax.star.api.templateeditor.service.DefaultTemplateEditorService;
import com.enovax.star.cms.commons.util.JsonUtil;
import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.cms.beans.runtime.MultipartForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * Created by lavanya on 21/7/16.
 */
@Controller
@RequestMapping("/templateeditor/")
public class TemplateEditorController extends BaseTemplateEditorController {

    @Autowired
    private HttpSession session;

    @Autowired
    private DefaultTemplateEditorService templateEditorService;


    @RequestMapping(value = "init-template", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<TicketTemplate>> apiInitTemplate(
            @RequestParam(value = "sid") String sid,
            @RequestParam(value = "isNew") boolean isNew,
            @RequestParam(value = "ticketType") String ticketType,
            @RequestParam(value = "templateXML") String templateXML) {
        log.info("Entered apiInitTemplate...");
        try {
            final ApiResult<TicketTemplate> result = templateEditorService.addTemplate(sid, isNew, ticketType, templateXML);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiInitTemplate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, TicketTemplate.class, ""), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "get-template", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<TicketTemplate>> apiGetTemplate(@RequestParam(value = "sid") String sid) {
        log.info("Entered apiGetTemplate...");
        try {

            final ApiResult<TicketTemplate> result = templateEditorService.getTemplate(sid);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTemplate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, TicketTemplate.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-updated-xml", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiGetModifiedTemplateXML(@RequestParam(value = "sid") String sid) {
        log.info("Entered apiGetTemplate...");
        try {

            final ApiResult<String> result = templateEditorService.getModifiedTemplateXML(sid);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTemplate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-original-xml", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiGetOriginalTemplateXML(@RequestParam(value = "sid") String sid) {
        log.info("Entered apiGetTemplate...");
        try {

            final ApiResult<String> result = templateEditorService.getOriginalTemplateXML(sid);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTemplate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save-template", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<TicketTemplate>> apiSaveTemplate(@RequestParam(value = "sid") String sid,
                                                                     @RequestParam(value = "templateXML") String templateXML) {
        log.info("Entered apiGetTemplate...");
        try {
            final ApiResult<TicketTemplate> result = templateEditorService.saveTemplate(sid, templateXML);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetTemplate] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, TicketTemplate.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "upload-image", method = RequestMethod.POST,  headers = {"content-type=multipart/form-data"})
    public ResponseEntity<String> apiUploadImage(HttpServletRequest request) {
        log.info("Entered apiGetTemplate...");
        try {

            MultipartForm mpf = ((MultipartForm) request.getAttribute("multipartform"));
            Document doc1 = mpf.getDocument("cppsLogo");

            File file = doc1.getFile();
            log.info("******"+doc1.getFile());
            if(file!=null) {
                final ApiResult<Map<String,String>> result = templateEditorService.uploadImage(file);
                return new ResponseEntity<>(JsonUtil.jsonify(result), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(JsonUtil.jsonify(new ApiResult<>(ApiErrorCodes.UnableToUploadLogo)),HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiUploadImage] !!!");
            return new ResponseEntity<>(JsonUtil.jsonify(handleUncaughtExceptionForMap(e, String.class,String.class, "")), HttpStatus.OK);
        }
    }

}
