package com.enovax.star.api.templateeditor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

/**
 * Created by lavanya on 20/7/16.
 */
@Configuration
@ComponentScan(
        basePackages = {"com.enovax.star.api.templateeditor"},
        excludeFilters = {
                @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
                @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION)
        }
)
@EnableScheduling
public class StarModuleApiTemplateEditorAppConfig {
}
