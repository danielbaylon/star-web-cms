package com.enovax.star.api.templateeditor.constant;

/**
 * Created by lavanya on 21/7/16.
 */
public enum ApiErrorCodes {
    //General
    UnexpectedSystemException("UnexpectedSystemException", "Unexpected system error encountered. "),
    General("GeneralError", "A general system error occurred."),
    TemplateNotFound("TemplateNotFound", "Template is not found."),
    UnableToUploadLogo("UnableToUploadLogo","Unable to upload Logo."),
    InvalidLogoDim("InvalidLogoSize","Logo dimension should be less than 512x480"),
    TotalLogoSizeExceeded("TotalLogoSize","Total size of Logos should be less than 100KB")
    ;
    public final String code;
    public final String message;

    ApiErrorCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
