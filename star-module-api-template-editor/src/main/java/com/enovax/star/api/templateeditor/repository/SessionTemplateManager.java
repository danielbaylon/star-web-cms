package com.enovax.star.api.templateeditor.repository;

import com.enovax.star.api.templateeditor.model.TicketTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by lavanya on 21/7/16.
 */

@Service
public class SessionTemplateManager implements ITemplateManager {

    private Map<String, TicketTemplate> ticketTemplates = new ConcurrentHashMap<>();
    @Override
    public void saveTicketTemplate(String sessionId, TicketTemplate ticketTemplate) {
        ticketTemplates.replace(sessionId,ticketTemplate);
    }

    @Override
    public TicketTemplate getTicketTemplate(String sessionId) {
        return ticketTemplates.get(sessionId);
    }

    @Override
    public void addTicketTemplate(String sessionId, TicketTemplate ticketTemplate) {
        ticketTemplates.put(sessionId,ticketTemplate);
    }
}
