package com.enovax.star.api.templateeditor.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

/**
 * Created by lavanya on 26/7/16.
 */
public class ImageTool {
    public static void toBlackAndWhite(BufferedImage img) {
        toBlackAndWhite(img, img, 50);
    }
    public static void toBlackAndWhiteInverse(BufferedImage img) {
        toBlackAndWhiteInverse(img, img, 50);
    }
    public static void toBlackAndWhite(BufferedImage orig, BufferedImage bwimg, int precision) {
        int w = orig.getWidth();
        int h = orig.getHeight();

        precision = (0 <= precision && precision <= 100) ? precision : 50;

        int limit = 255 * precision / 100;

        for(int i = 0, j; i < w; ++i) {
            for(j = 0; j < h; ++j) {
                Color color = new Color(orig.getRGB(i, j));
                if(limit <= color.getRed() || limit <= color.getGreen() || limit <= color.getBlue()) {
                    bwimg.setRGB(i, j, Color.WHITE.getRGB());
                } else {
                    bwimg.setRGB(i, j, Color.BLACK.getRGB());
                }
            }
        }
    }

    public static void copyImage(BufferedImage orig, BufferedImage bwimg) {
        int w = orig.getWidth();
        int h = orig.getHeight();
        for(int i = 0, j; i < w; ++i) {
            for (j = 0; j < h; ++j) {
                Color c = new Color(orig.getRGB(i,j));
                Color newCol = new Color(c.getRed(),c.getGreen(),c.getBlue());
                bwimg.setRGB(i, j, newCol.getRGB());
            }
        }

    }

    public static void invertImage(BufferedImage orig, BufferedImage bwimg) {
        int w = orig.getWidth();
        int h = orig.getHeight();
        for(int i = 0, j; i < w; ++i) {
            for (j = 0; j < h; ++j) {
                Color c = new Color(orig.getRGB(i,j));
                Color newCol = new Color(255-c.getRed(),255-c.getGreen(),255-c.getBlue());
                bwimg.setRGB(i, j, newCol.getRGB());

            }
        }

    }
    public static void toBlackAndWhiteInverse(BufferedImage orig, BufferedImage bwimg, int precision) {
        int w = orig.getWidth();
        int h = orig.getHeight();

        precision = (0 <= precision && precision <= 100) ? precision : 50;

        int limit = 255 * precision / 100;

        for(int i = 0, j; i < w; ++i) {
            for(j = 0; j < h; ++j) {
                Color color = new Color(orig.getRGB(i, j));
                if(limit <= color.getRed() || limit <= color.getGreen() || limit <= color.getBlue()) {
                    bwimg.setRGB(i, j, Color.BLACK.getRGB());
                } else {
                    bwimg.setRGB(i, j, Color.WHITE.getRGB());
                }
            }
        }
    }

    public static BufferedImage convertToBW(BufferedImage img) {
        return floydSteinbergDithering(img);

    }


private static class C3{
    int r, g, b;

    public C3(int c) {
        Color color = new Color(c);
        r = color.getRed();
        g = color.getGreen();
        b = color.getBlue();
    }

    public C3(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public C3 add(C3 o) {
        return new C3(r + o.r, g + o.g, b + o.b);
    }

    public int clamp(int c) {
        return Math.max(0, Math.min(255, c));
    }

    public int diff(C3 o) {
        int Rdiff = o.r - r;
        int Gdiff = o.g - g;
        int Bdiff = o.b - b;
        int distanceSquared = Rdiff * Rdiff + Gdiff * Gdiff + Bdiff * Bdiff;
        return distanceSquared;
    }

    public C3 mul(double d) {
        return new C3((int) (d * r), (int) (d * g), (int) (d * b));
    }

    public C3 sub(C3 o) {
        return new C3(r - o.r, g - o.g, b - o.b);
    }

    public Color toColor() {
        return new Color(clamp(r), clamp(g), clamp(b));
    }

    public int toRGB() {
        return toColor().getRGB();
    }
}

    private static C3 findClosestPaletteColor(C3 c, C3[] palette) {
        C3 closest = palette[0];

        for (C3 n : palette) {
            if (n.diff(c) < closest.diff(c)) {
                closest = n;
            }
        }

        return closest;
    }

    private static BufferedImage floydSteinbergDithering(BufferedImage img) {
        C3[] palette = new C3[] {
                new C3(  0,   0,   0), // black
                new C3(255, 255, 255)  // white
        };

        int w = img.getWidth();
        int h = img.getHeight();

        C3[][] d = new C3[h][w];

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                d[y][x] = new C3(img.getRGB(x, y));
            }
        }

        for (int y = 0; y < img.getHeight(); y++) {
            for (int x = 0; x < img.getWidth(); x++) {

                C3 oldColor = d[y][x];
                C3 newColor = findClosestPaletteColor(oldColor, palette);
                img.setRGB(x, y, newColor.toColor().getRGB());

                C3 err = oldColor.sub(newColor);

                if (x + 1 < w) {
                    d[y][x + 1] = d[y][x + 1].add(err.mul(7. / 16));
                }

                if (x - 1 >= 0 && y + 1 < h) {
                    d[y + 1][x - 1] = d[y + 1][x - 1].add(err.mul(3. / 16));
                }

                if (y + 1 < h) {
                    d[y + 1][x] = d[y + 1][x].add(err.mul(5. / 16));
                }

                if (x + 1 < w && y + 1 < h) {
                    d[y + 1][x + 1] = d[y + 1][x + 1].add(err.mul(1. / 16));
                }
            }
        }

        return img;
    }
}
