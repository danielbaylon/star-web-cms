package com.enovax.star.api.templateeditor.repository;

import com.enovax.star.api.templateeditor.model.TicketTemplate;

/**
 * Created by lavanya on 21/7/16.
 */
public interface ITemplateManager {
    void saveTicketTemplate(String sessionId,TicketTemplate ticketTemplate);

    TicketTemplate getTicketTemplate(String sessionId);

    void addTicketTemplate(String sessionId,TicketTemplate ticketTemplate);

}
