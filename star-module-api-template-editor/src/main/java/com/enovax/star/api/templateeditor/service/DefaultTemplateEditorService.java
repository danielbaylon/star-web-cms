package com.enovax.star.api.templateeditor.service;

import com.enovax.star.api.templateeditor.constant.ApiErrorCodes;
import com.enovax.star.api.templateeditor.model.TicketTemplate;
import com.enovax.star.api.templateeditor.model.api.ApiResult;
import com.enovax.star.api.templateeditor.repository.ITemplateManager;
import com.enovax.star.api.templateeditor.util.ImageTool;
import com.sun.javafx.iio.common.ImageTools;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.collections4.map.HashedMap;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

/**
 * Created by lavanya on 21/7/16.
 */
@SuppressWarnings("Duplicates")
@Service
public class DefaultTemplateEditorService implements  ITemplateEditorService{

    @Autowired
    ITemplateManager templateManager;

    @Override
    public ApiResult<TicketTemplate> addTemplate(String sessionId, boolean isNew, String ticketType, String templateXML) {
        TicketTemplate ticketTemplate = new TicketTemplate();
        ticketTemplate.setOriginalTemplateXML(templateXML);
        ticketTemplate.setModified(false);
        ticketTemplate.setNew(isNew);
        ticketTemplate.setTicketType(ticketType);
        templateManager.addTicketTemplate(sessionId,ticketTemplate);
        return new ApiResult<>(true, "", "", ticketTemplate);
    }


    @Override
    public ApiResult<TicketTemplate> getTemplate(String sessionId) {
        if(templateManager.getTicketTemplate(sessionId) != null) {
            return new ApiResult<>(true, "", "", templateManager.getTicketTemplate(sessionId));
        }
        else {
            return new ApiResult<>(ApiErrorCodes.TemplateNotFound);
        }
    }

    @Override
    public ApiResult<String> getModifiedTemplateXML(String sessionId) {
        if(templateManager.getTicketTemplate(sessionId) != null) {
            return new ApiResult<>(true, "", "", templateManager.getTicketTemplate(sessionId).getModifiedTemplateXML());
        }
        else {
            return new ApiResult<>(ApiErrorCodes.TemplateNotFound);
        }
    }

    @Override
    public ApiResult<String> getOriginalTemplateXML(String sessionId) {
        if(templateManager.getTicketTemplate(sessionId) != null) {
            return new ApiResult<>(true, "", "", templateManager.getTicketTemplate(sessionId).getOriginalTemplateXML());
        }
        else {
            return new ApiResult<>(ApiErrorCodes.TemplateNotFound);
        }
    }

    @Override
    public ApiResult<TicketTemplate> saveTemplate(String sessionId, String templateXML) {
        if(templateManager.getTicketTemplate(sessionId) != null) {
            TicketTemplate ticketTemplate = templateManager.getTicketTemplate(sessionId);
            ticketTemplate.setModified(true);
            ticketTemplate.setModifiedTemplateXML(templateXML);
            return new ApiResult<>(true, "", "", ticketTemplate);
        }
        else {
            return new ApiResult<>(ApiErrorCodes.TemplateNotFound);
        }
    }



    @Override
    public ApiResult<Map<String,String>> uploadImage(File file) throws IOException{

        String base64blackWhite="",base64blackWhiteInverse="";

        Map<String,String> result=new HashedMap<String,String>();
        int precision = 70;
        //for(Integer i : new Integer[] {0, 30, 70, 100}) {
            BufferedImage img = ImageIO.read(file);

            int origWidth = img.getWidth();
            int origHeight = img.getHeight();
            if(origWidth>512 || origHeight>480) {
                return new ApiResult<>(ApiErrorCodes.InvalidLogoDim);
            }


            //find the nearest width multiple of 32
            int newWidth = 32*(Math.round(img.getWidth()/32));

            //scale the image to the new width maintaining the aspect ratio
            BufferedImage img2 = toBufferedImage(img.getScaledInstance(newWidth,-1,Image.SCALE_DEFAULT));

            //BufferedImage convImg = img2;
            ImageTool.convertToBW(img2);

            //convert to monochrome bitmap & invert B&W
            BufferedImage blackWhiteInverse = new BufferedImage(img2.getWidth(),img2.getHeight(),BufferedImage.TYPE_BYTE_BINARY);
            ImageTool.invertImage(img2, blackWhiteInverse);

            // Flip the image vertically
            AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
            tx.translate(0, -blackWhiteInverse.getHeight(null));
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            blackWhiteInverse = op.filter(blackWhiteInverse, null);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImageIO.write(blackWhiteInverse, "bmp",baos);
            baos.close();
            int currLogoSize = baos.size();

            //convert the image to monochrome bitmap without inverting B&W
            BufferedImage blackWhite = new BufferedImage(img2.getWidth(),img2.getHeight(),BufferedImage.TYPE_BYTE_BINARY);
            ImageTool.copyImage(img2, blackWhite);

            //convert the inverted image to base64
            base64blackWhiteInverse = imgToBase64String(blackWhiteInverse,"bmp");
            base64blackWhite = imgToBase64String(blackWhite,"bmp");

        result.put("base64BW",base64blackWhite);
        result.put("base64BWInverse",base64blackWhiteInverse);
        result.put("logoSize",Integer.toString(currLogoSize));
        return new ApiResult<>(true, "", "", result);
    }

    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    private static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static String imgToBase64String(final RenderedImage img, final String formatName) {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, formatName, os);
            return Base64.getEncoder().encodeToString(os.toByteArray());
            //return os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }


    public static BufferedImage base64StringToImg(final String base64String) {
        try {
            return ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(base64String)));
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }
}
