package com.enovax.star.api.templateeditor;

import com.enovax.star.api.templateeditor.config.StarModuleApiTemplateEditorAppConfig;
import com.enovax.star.api.templateeditor.config.StarModuleApiTemplateEditorWebConfig;
import com.enovax.star.api.templateeditor.filter.SimpleCorsFilter;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
/**
 * Created by lavanya on 20/7/16.
 */
public class StarModuleApiTemplateEditor implements ModuleLifecycle {
    /**
     * URL mapping for the dispatcherServlet with a default value. To configure in AdminCentral,
     * go to: Configuration > modules > kando-event-reservation-module, and create a folder called "config"
     * with a property "urlMappings" and value "whatever".
     *
     * Changing this property might require a server restart to take effect.
     */
    private String urlMappings = "/.templateeditor/*";

    private ContextLoader contextLoader;
    private DispatcherServlet dispatcherServlet;

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
            //Retrieve serlvet context.
            ServletContext servletContext = getServletContext();

            //Initialize root context.

            //Initialize dispatcher servlet.

            AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
            webCtx.register(StarModuleApiTemplateEditorAppConfig.class,  StarModuleApiTemplateEditorWebConfig.class);

            dispatcherServlet = new DispatcherServlet(webCtx);

            final ServletRegistration.Dynamic dispatcherRegistration = servletContext.addServlet("templateeditor-api-dispatcher", dispatcherServlet);
            dispatcherRegistration.setLoadOnStartup(1);
            dispatcherRegistration.setInitParameter("dispatchOptionsRequest", "true");
            dispatcherRegistration.setAsyncSupported(true);

            final String[] urlMappingArray = urlMappings.split(",");
            dispatcherRegistration.addMapping(urlMappingArray);

            final FilterRegistration.Dynamic corsRegistration = servletContext.addFilter("templatEditorCorsFilter", SimpleCorsFilter.class);
            corsRegistration.addMappingForServletNames(null, false, "templateeditor-api-dispatcher");
            corsRegistration.setInitParameter("dispatchOptionsRequest", "true");
            corsRegistration.setAsyncSupported(true);
        }
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
            if (dispatcherServlet != null) {
                dispatcherServlet.destroy();
            }
            if (contextLoader != null) {
                contextLoader.closeWebApplicationContext(getServletContext());
            }
        }
    }

    protected ServletContext getServletContext() {
        return Components.getComponent(ServletContext.class);
    }

    /*
    Getters and Setters
     */

    public String getUrlMappings() {
        return urlMappings;
    }

    public void setUrlMappings(String urlMappings) {
        this.urlMappings = urlMappings;
    }
}
