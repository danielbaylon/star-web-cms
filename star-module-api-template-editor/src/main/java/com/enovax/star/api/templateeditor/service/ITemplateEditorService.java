package com.enovax.star.api.templateeditor.service;

import com.enovax.star.api.templateeditor.model.TicketTemplate;
import com.enovax.star.api.templateeditor.model.api.ApiResult;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by lavanya on 21/7/16.
 */
public interface ITemplateEditorService {

        ApiResult<TicketTemplate> addTemplate(String sessionId, boolean isNew, String ticketType, String templateXML);
        ApiResult<TicketTemplate> getTemplate(String sessionId);
        ApiResult<String> getModifiedTemplateXML(String sessionId);
        ApiResult<String> getOriginalTemplateXML(String sessionId);
        ApiResult<TicketTemplate> saveTemplate(String sessionId,String templateXML);
        ApiResult<Map<String,String>> uploadImage(File file) throws IOException;
}
