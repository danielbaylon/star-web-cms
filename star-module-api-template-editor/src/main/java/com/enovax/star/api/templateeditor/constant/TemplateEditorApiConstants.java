package com.enovax.star.api.templateeditor.constant;

/**
 * Created by lavanya on 21/7/16.
 */
public final class TemplateEditorApiConstants {
    public static final String REQUEST_HEADER_CHANNEL = "Template-Editor-Api-Channel";
}
