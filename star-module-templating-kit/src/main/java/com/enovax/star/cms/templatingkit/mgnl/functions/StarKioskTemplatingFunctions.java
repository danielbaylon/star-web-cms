package com.enovax.star.cms.templatingkit.mgnl.functions;

import static com.enovax.star.cms.commons.mgnl.definition.AXProductProperties.ProductPrice;
import static com.enovax.star.cms.commons.mgnl.definition.AXProductProperties.Status;
import static com.enovax.star.cms.commons.mgnl.definition.AXProductProperties.TicketType;
import static com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties.RecommendedProducts;
import static com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties.RelatedAXProducts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.spi.commons.value.QValueValue;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enovax.star.cms.commons.common.CommonConstant;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.AXProductStatus;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductSubCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskGroupProperties;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import info.magnolia.cms.core.AggregationState;
import info.magnolia.cms.i18n.I18nContentSupport;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.rendering.template.type.TemplateTypeHelper;

/**
 * Created by JC on 5/8/16.
 */
public class StarKioskTemplatingFunctions extends StarTemplatingFunctions {

    private static final Logger log = LoggerFactory.getLogger(StarKioskTemplatingFunctions.class);

    public static final String functionName = "kioskfn";

    private static final String PRODUCT_FILTER = KioskGroupProperties.ProductFilter.getPropertyName();

    private static final String ADMISSION_INCLUDED = CMSProductProperties.AdmissionIncluded.getPropertyName();

    @Inject
    public StarKioskTemplatingFunctions(Provider<AggregationState> aggregationStateProvider, TemplateTypeHelper templateTypeFunctions,
            Provider<I18nContentSupport> i18nContentSupport) {
        super(aggregationStateProvider, templateTypeFunctions, i18nContentSupport);
    }

    private void setCategoryLayoutCssProperty(Node node, String value) throws RepositoryException {
        node.setProperty("categoryLayoutCSS", "menu-category-" + value);
    }

    private void setCategoryScreenLayout(List<Node> categories) throws RepositoryException {

        if (categories.size() == 1) {
            setCategoryLayoutCssProperty(categories.get(0), "0");
        } else if (categories.size() == 2) {
            setCategoryLayoutCssProperty(categories.get(0), "1");
            setCategoryLayoutCssProperty(categories.get(1), "2");
        } else if (categories.size() == 3) {
            setCategoryLayoutCssProperty(categories.get(0), "3");
            setCategoryLayoutCssProperty(categories.get(1), "1");
            setCategoryLayoutCssProperty(categories.get(2), "2");

        } else if (categories.size() == 4) {
            setCategoryLayoutCssProperty(categories.get(0), "3");
            setCategoryLayoutCssProperty(categories.get(1), "1");
            setCategoryLayoutCssProperty(categories.get(2), "2");
            setCategoryLayoutCssProperty(categories.get(3), "4");
        } else if (categories.size() == 5) {
            setCategoryLayoutCssProperty(categories.get(0), "3");
            setCategoryLayoutCssProperty(categories.get(1), "5");
            setCategoryLayoutCssProperty(categories.get(2), "6");
            setCategoryLayoutCssProperty(categories.get(3), "7");
            setCategoryLayoutCssProperty(categories.get(4), "8");

        } else if (categories.size() == 6) {
            setCategoryLayoutCssProperty(categories.get(0), "3");
            setCategoryLayoutCssProperty(categories.get(1), "5");
            setCategoryLayoutCssProperty(categories.get(2), "6");
            setCategoryLayoutCssProperty(categories.get(3), "7");
            setCategoryLayoutCssProperty(categories.get(4), "8");
            setCategoryLayoutCssProperty(categories.get(5), "4");
        }

    }

    private Node getKiosk(String channel) throws RepositoryException {
        Node kiosk = null;

        Node parentNode = JcrRepository.getParentNode(JcrWorkspace.KioskInfo.getWorkspaceName(), "/" + channel);
        List<Node> kioskInfoList = NodeUtil.asList(NodeUtil.getNodes(parentNode, JcrWorkspace.KioskInfo.getNodeType()));
        if (kioskInfoList.size() > 0) {
            kiosk = kioskInfoList.get(0);
        } else {
            throw new RepositoryException("NO KIOSK INFO FOUND");
        }

        return kiosk;
    }

    private Node getKioskGroup(String channel, Node kiosk) throws RepositoryException {
        Node kioskGroupNode = null;
        String sourceKioskUuid = kiosk.getIdentifier();
        Boolean match = Boolean.FALSE;
        Node kioskGroupParentNode = JcrRepository.getParentNode(JcrWorkspace.KioskGroup.getWorkspaceName(), "/" + channel);
        List<Node> kioskGroupList = NodeUtil.asList(NodeUtil.getNodes(kioskGroupParentNode, JcrWorkspace.KioskGroup.getNodeType()));
        for (Node kioskGroup : kioskGroupList) {
            if (kioskGroup.hasProperty("status") && "Active".equals(kioskGroup.getProperty("status").getString())) {
                List<Node> kioskInfoConfig = NodeUtil.asList(NodeUtil.getNodes(kioskGroup, JcrWorkspace.KioskInfo.getNodeType()));
                for (Node kioskInfo : kioskInfoConfig) {
                    List<Node> grantedList = NodeUtil.asList(NodeUtil.getNodes(kioskInfo, JcrWorkspace.KioskGroupKioskInfo.getNodeType()));
                    for (Node granted : grantedList) {
                        Property uuid = granted.getProperty(KioskGroupProperties.RelatedKioskInfoUUID.getPropertyName());
                        if (uuid != null && sourceKioskUuid.equals(uuid.getString())) {
                            if (match) {
                                log.error("more than one kiosk group found for kiosk :" + kiosk.getName());
                            } else {
                                kioskGroupNode = kioskGroup;
                                match = Boolean.TRUE;
                            }
                        }
                    }
                }
            }
        }
        if (!match) {
            throw new RepositoryException("NO KIOSK GROUP INFO FOUND for KIOSK :" + kiosk.getName());
        }
        return kioskGroupNode;
    }

    public Node getTransportLayoutCategory(String channel) {
        Node transportProductCategory = null;
        try {
            Node kiosk = getKiosk(channel);
            Node group = getKioskGroup(channel, kiosk);
            List<Node> categories = NodeUtil.asList(NodeUtil.getNodes(group, JcrWorkspace.ProductCategories.getNodeType()));
            for (Node category : categories) {
                List<Node> grantedCategoryList = NodeUtil.asList(NodeUtil.getNodes(category, JcrWorkspace.KioskGroupProductCategory.getNodeType()));
                for (Node grantedCategory : grantedCategoryList) {
                    Property categoryUuid = grantedCategory.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName());
                    Node sourceProductCategory = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), categoryUuid.getString());
                    String transportLayout = CMSProductCategoriesProperties.TransportLayout.getPropertyName();
                    if (sourceProductCategory.hasProperty(transportLayout) && "true".equals(sourceProductCategory.getProperty(transportLayout).getString())) {
                        transportProductCategory = sourceProductCategory;
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return transportProductCategory;
    }

    public List<Node> getIslandAdmissionCategoryList(String channel) {
        List<Node> islandAdmissionCategoryNodeList = new ArrayList<>();
        ;
        try {
            Node kiosk = getKiosk(channel);
            Node group = getKioskGroup(channel, kiosk);
            List<String> subCategoryProductUuidList = getSubCategoryProductUuidList(group);
            List<Node> categories = NodeUtil.asList(NodeUtil.getNodes(group, JcrWorkspace.ProductCategories.getNodeType()));

            for (Node category : categories) {
                List<Node> grantedCategoryList = NodeUtil.asList(NodeUtil.getNodes(category, JcrWorkspace.KioskGroupProductCategory.getNodeType()));
                for (Node grantedCategory : grantedCategoryList) {
                    Boolean match = Boolean.FALSE;
                    Property categoryUuid = grantedCategory.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName());
                    Node sourceProductCategory = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), categoryUuid.getString());
                    List<Node> subCategories = getCMSProductSubCategories(sourceProductCategory);
                    for (Node subCategory : subCategories) {
                        List<Node> relatedProducts = getCMSProductBySubCategory(subCategory);
                        for (Node related : relatedProducts) {
                            if (!match && related.hasProperty(ADMISSION_INCLUDED)) {
                                if (related.getProperty(ADMISSION_INCLUDED).getBoolean()
                                        && subCategoryProductUuidList.contains(subCategory.getIdentifier() + "-" + related.getIdentifier())) {
                                    islandAdmissionCategoryNodeList.add(sourceProductCategory);
                                    match = Boolean.TRUE;
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return islandAdmissionCategoryNodeList;
    }

    private List<String> getSubCategoryProductUuidList(Node kioskGroupNode) throws RepositoryException {
        List<String> subCategoryProductUuidList = new ArrayList<>();
        if (kioskGroupNode.hasProperty(PRODUCT_FILTER)) {
            for (Value value : kioskGroupNode.getProperty(PRODUCT_FILTER).getValues()) {
                subCategoryProductUuidList.add(((QValueValue) value).getQValue().getString());
            }
        }
        return subCategoryProductUuidList;
    }

    public Node getTransportLayoutProduct(String channel) {
        Node product = null;
        try {
            Node category = this.getTransportLayoutCategory(channel);
            if (category != null) {
                List<Node> subCategories = getCMSProductSubCategories(category);
                for (Node subCategory : subCategories) {
                    List<Node> relatedProducts = getCMSProductBySubCategory(subCategory);
                    for (Node related : relatedProducts) {
                        product = related;
                        List<Node> axProducts = getAXProductsByCMSProductNode(product);
                        if (axProducts.size() > 0) {
                            Node relatedAxProduct = axProducts.get(0);
                            String uuid = relatedAxProduct.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                            Node axProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);
                            String listingId = AXProductProperties.ProductListingId.getPropertyName();
                            String prodNum = AXProductProperties.DisplayProductNumber.getPropertyName();
                            String itemName = AXProductProperties.ItemName.getPropertyName();
                            String ticketType = AXProductProperties.TicketType.getPropertyName();
                            String productPrice = AXProductProperties.ProductPrice.getPropertyName();
                            product.setProperty(listingId, axProduct.getProperty(listingId).getString());
                            product.setProperty(prodNum, axProduct.getProperty(prodNum).getString());
                            product.setProperty(itemName, axProduct.getProperty(itemName).getString());
                            product.setProperty(ticketType, axProduct.getProperty(ticketType).getString());
                            product.setProperty(productPrice, axProduct.getProperty(productPrice).getString());
                        }
                        break;
                    }
                }

            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return product;
    }

    /**
     * Get the CMS Product Categories Nodes given the Channel for Kiosk.
     *
     * @param channel
     * @param excludeNoCMSProducts
     * @param excludeDefaultSub
     * @return
     * @throws RepositoryException
     * @throws PathNotFoundException
     */

    public List<Node> getCMSProductCategories(String channel, boolean excludeNoCMSProducts, boolean excludeDefaultSub) {
        List<Node> categories = new ArrayList<>();
        try {
            Node kiosk = this.getKiosk(channel);
            Node kioskGroup = this.getKioskGroup(channel, kiosk);

            List<Node> categoryConfig = NodeUtil.asList(NodeUtil.getNodes(kioskGroup, JcrWorkspace.ProductCategories.getNodeType()));
            for (Node category : categoryConfig) {
                List<Node> grantedCategoryList = NodeUtil.asList(NodeUtil.getNodes(category, JcrWorkspace.KioskGroupProductCategory.getNodeType()));
                for (Node grantedCategory : grantedCategoryList) {
                    Property categoryUuid = grantedCategory.getProperty(KioskGroupProperties.RelatedProductCategoryUUID.getPropertyName());
                    Node sourceProductCategory = NodeUtil.getNodeByIdentifier(JcrWorkspace.ProductCategories.getWorkspaceName(), categoryUuid.getString());
                    categories.add(sourceProductCategory);
                }
            }
            List<Node> categoryListInI18n = new ArrayList<>();
            for (Node category : categories) {
                if (!excludeNoCMSProducts || this.hasSubCategoriesWithProducts(category, excludeDefaultSub)) {
                    categoryListInI18n.add(this.wrapForI18n(category));
                    categories = categoryListInI18n;
                }
            }
            setCategoryScreenLayout(categories);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return categories;
    }

    private String getIncludedProductUuid(String channel) throws RepositoryException {
        String included = "";

        Node kiosk = this.getKiosk(channel);
        String sourceKioskInfoUuid = kiosk.getIdentifier();
        Node kioskGroup = this.getKioskGroup(channel, kiosk);

        List<Node> kioskInfoConfig = NodeUtil.asList(NodeUtil.getNodes(kioskGroup, JcrWorkspace.KioskInfo.getNodeType()));
        for (Node kioskInfo : kioskInfoConfig) {
            List<Node> grantedList = NodeUtil.asList(NodeUtil.getNodes(kioskInfo, JcrWorkspace.KioskGroupKioskInfo.getNodeType()));
            for (Node granted : grantedList) {
                Property uuid = granted.getProperty(KioskGroupProperties.RelatedKioskInfoUUID.getPropertyName());
                if (uuid != null && sourceKioskInfoUuid.equals(uuid.getString()) && kioskGroup.hasProperty(PRODUCT_FILTER)) {
                    StringBuffer productFilter = new StringBuffer();
                    Value[] values = (Value[]) kioskGroup.getProperty(KioskGroupProperties.ProductFilter.getPropertyName()).getValues();
                    for (Value value : values) {
                        productFilter.append(value.getString());
                    }
                    included = productFilter.toString();
                }

            }
        }

        return included;
    }

    public List<Node> getCMSProductListByName(String channel, String name) {
        List<Node> productList = new ArrayList<>();
        try {
            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel);
            List<Node> allProducts = NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.CMSProducts.getNodeType()));

            Node kioskGroup = this.getKioskGroup(channel, getKiosk(channel));
            if (StringUtils.isNotBlank(name) && name.length() > 2 && kioskGroup.hasProperty(PRODUCT_FILTER)) {
                Value[] values = (Value[]) kioskGroup.getProperty(KioskGroupProperties.ProductFilter.getPropertyName()).getValues();
                for (Node product : allProducts) {
                    if (product.getProperty("name").getString().toUpperCase().contains(name.toUpperCase())) {
                        for (Value value : values) {
                            if (value.getString().contains(product.getIdentifier())) {
                                productList.add(product);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return productList;
    }

    private void sortCMSProduct(String sort, List<Node> productList) {
        if (StringUtils.isNotBlank(sort) && CollectionUtils.isNotEmpty(productList)) {
            Collections.sort(productList, new Comparator<Node>() {

                private BigDecimal getPrice(Node node) {
                    BigDecimal price = BigDecimal.ZERO;
                    try {
                        if (node.hasNode(RelatedAXProducts.getPropertyName())) {
                            Node relatedAXProductParentNode = node.getNode(RelatedAXProducts.getPropertyName());
                            List<Node> relatedAxProducts = NodeUtil.asList(NodeUtil.getNodes(relatedAXProductParentNode));
                            for (Node axProductUUID : relatedAxProducts) {
                                if (axProductUUID.hasProperty(RelatedAXProducts.getPropertyName())) {
                                    String uuid = axProductUUID.getProperty(RelatedAXProducts.getPropertyName()).getString();
                                    Node axProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);
                                    if (AXProductStatus.Active.name().equals(axProduct.getProperty(Status.getPropertyName()).getString())) {
                                        try {
                                            String type = "";
                                            if (axProduct.hasProperty(TicketType.getPropertyName())) {
                                                type = axProduct.getProperty(TicketType.getPropertyName()).getString();
                                            }
                                            Property priceProperty = axProduct.getProperty(ProductPrice.getPropertyName());
                                            if ("Adult".equals(type)) {
                                                price = priceProperty.getDecimal();
                                                break;
                                            } else {
                                                price = priceProperty.getDecimal();
                                                continue;
                                            }
                                        } catch (PathNotFoundException e) {
                                            log.error(e.getMessage(), e);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    return price;
                }

                @Override
                public int compare(Node o1, Node o2) {
                    int result = 0;
                    try {
                        if ("name".equals(sort)) {
                            result = o1.getProperty(sort).getString().compareTo(o2.getProperty(sort).getString());
                        } else {
                            result = this.getPrice(o1).compareTo(this.getPrice(o2));
                        }
                    } catch (RepositoryException e) {
                    }
                    return result;
                }
            });
        }
    }

    /**
     * Get CMS Products with sort and filter.
     *
     * @param subCategoryNode
     * @param excludeNoActiveAXProduct
     * @param sort
     * @param filter
     * @return
     */
    public List<Node> getCMSProductBySubCategory(String channel, Node subCategoryNode, boolean excludeNoActiveAXProduct, String sort, String filter) {
        List<Node> productList = new ArrayList<>();
        try {

            List<Node> relatedCMSProductList = NodeUtil.asList(NodeUtil.getNodes(subCategoryNode, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
            for (Node relatedCMSProduct : relatedCMSProductList) {
                if (relatedCMSProduct.hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
                    String uuid = relatedCMSProduct.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();
                    String key = subCategoryNode.getIdentifier() + "-" + uuid;
                    String granted = getIncludedProductUuid(channel);
                    if (granted == null || (StringUtils.isNotBlank(granted) && granted.contains(key))) {
                        Node cmsProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);
                        if (cmsProduct != null && (!excludeNoActiveAXProduct || hasActiveAXProducts(cmsProduct))) {
                            if (StringUtils.isBlank(filter) || filter.length() < 3) {
                                productList.add(this.wrapForI18n(cmsProduct));
                            } else if (StringUtils.isNotBlank(filter) && filter.length() > 2
                                    && cmsProduct.getProperty("name").getString().toUpperCase().contains(filter.toUpperCase())) {
                                productList.add(this.wrapForI18n(cmsProduct));
                            }
                        }
                    }
                }
            }
            this.sortCMSProduct(sort, productList);
        } catch (RepositoryException e1) {
            log.error(e1.getMessage(), e1);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return productList;
    }

    /**
     * Can pass the product node name
     *
     * @param channel
     * @param productNodeName
     * @return
     */
    public List<ContentMap> getRecommendProducts(String channel, String productNodeName) {
        List<ContentMap> recommendProducts = new ArrayList<>();
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productNodeName);

            if (productNode != null && productNode.hasNode(RecommendedProducts.getPropertyName())) {
                Node recommendedProductParentNode = productNode.getNode(RecommendedProducts.getPropertyName());

                List<Node> recommendedProductList = NodeUtil.asList(NodeUtil.getNodes(recommendedProductParentNode));

                for (Node recommendedProduct : recommendedProductList) {
                    String uuid = PropertyUtil.getString(recommendedProduct, RecommendedProducts.getPropertyName());
                    Node recProductNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);

                    if (recProductNode.hasNode(RelatedAXProducts.getPropertyName())) {
                        Node relatedAXProductParentNode = recProductNode.getNode(RelatedAXProducts.getPropertyName());
                        List<Node> relatedAxProducts = NodeUtil.asList(NodeUtil.getNodes(relatedAXProductParentNode));
                        BigDecimal lowestPrice = BigDecimal.ZERO;
                        for (Node axProductUUID : relatedAxProducts) {
                            if (axProductUUID.hasProperty(RelatedAXProducts.getPropertyName())) {
                                String axUUID = axProductUUID.getProperty(RelatedAXProducts.getPropertyName()).getString();
                                Node axProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), axUUID);
                                BigDecimal axPrice = axProduct.getProperty(ProductPrice.getPropertyName()).getDecimal();
                                if (lowestPrice == BigDecimal.ZERO) {
                                    lowestPrice = axPrice;
                                } else if (axPrice.compareTo(lowestPrice) == -1) {
                                    lowestPrice = axPrice;
                                }

                            }
                        }
                        recProductNode.setProperty(ProductPrice.getPropertyName(), lowestPrice);

                    }
                    ContentMap recProductContentMap = asContentMap(wrapForI18n(recProductNode));
                    if (recommendProducts.size() - 5 < 0) {
                        recommendProducts.add(recProductContentMap);
                    }
                }

            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return recommendProducts;
    }

    public List<ContentMap> getAnnouncementsByProduct(String channel, Node productNodeName) {
        try {

            String uuid = productNodeName.getIdentifier();
            String query = "/jcr:root/" + channel + "//element(*, mgnl:announcement)[@product= '" + uuid + "']";

            Iterable<Node> announcementIterable = JcrRepository.query("announcement", "mgnl:announcement", query);
            Iterator<Node> announcementNodeIterator = announcementIterable.iterator();

            List<Node> announcementList = IteratorUtils.toList(announcementNodeIterator);
            List<ContentMap> announcementContentMap = new ArrayList<>();

            for (Node announcement : announcementList) {
                if (isValidDateRange(announcement)) {
                    ContentMap recProductContentMap = asContentMap(wrapForI18n(announcement));
                    announcementContentMap.add(recProductContentMap);
                }
            }

            return announcementContentMap;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /* fetching and setting zode details and from date/ to date */
    public Node getOtherInformationByProduct(Node productNode) {
        try {
            productNode.setProperty(CommonConstant.ZONE, getZoneByUUID(productNode));
            return formatDate(productNode);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
    
    public String isProductClosed(Node productNode) {
        Boolean closed = Boolean.FALSE;
        String closingTimeLabel = this.getClosingTime(productNode);

        if (StringUtils.isNotBlank(closingTimeLabel)) {
            if (DateTime.now().toString("hh:mm").compareTo(closingTimeLabel) > 0) {
                closed = Boolean.TRUE;
            }
        }
        return closed.toString();
    }

    public String getClosingTime(Node productNode) {
        String closingTimeLabel = null;
        try {
            String query = "/jcr:root/kiosk-cms-product-closing-time//element(*, mgnl:cms-config)";

            Iterable<Node> closingTimeIterable = JcrRepository.query("cms-config", "mgnl:cms-config", query);

            List<Node> closingTimeList = IteratorUtils.toList(closingTimeIterable.iterator());

            if (productNode.hasProperty(CMSProductProperties.ClosingTime.getPropertyName())) {
                String closingTimeIdentity = productNode.getProperty(CMSProductProperties.ClosingTime.getPropertyName()).getString();

                for (Node closingTime : closingTimeList) {
                    if (closingTime.getIdentifier().equals(closingTimeIdentity)) {
                        closingTimeLabel = closingTime.getProperty("label").getString();
                    }
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return closingTimeLabel;
    }

    public String getZoneByUUID(Node productNode) {
        try {
            String queryGetZones = "/jcr:root/cmsProductZones//element(*, mgnl:cms-config)";

            Iterable<Node> cmsProductZonesIterable = JcrRepository.query("cms-config", "mgnl:cms-config", queryGetZones);
            Iterator<Node> cmsProductZonesIterator = cmsProductZonesIterable.iterator();
            List<Node> productZonesList = IteratorUtils.toList(cmsProductZonesIterator);

            if (productNode.hasProperty(CommonConstant.ZONE)) {
                String zoneUUID = productNode.getProperty(CommonConstant.ZONE).getString();

                for (Node productZones : productZonesList) {
                    if (productZones.getIdentifier().equals(zoneUUID)) {
                        return productZones.getProperty("value").getString();
                    }
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public Node formatDate(Node productNode) {
        Calendar date = null;
        try {
            if (productNode.hasProperty(CommonConstant.VALID_FROM_DATE)) {
                if (productNode.getProperty(CommonConstant.VALID_FROM_DATE) != null) {
                    date = PropertyUtil.getDate(productNode, CommonConstant.VALID_FROM_DATE);
                    if (date != null) {
                        String fromDate = NvxDateUtils.formatDate(date.getTime(), NvxDateUtils.PRODUCT_DETAILS_DATE_FORMAT_DISPLAY);
                        productNode.setProperty(CommonConstant.VALID_FROM_DATE, fromDate);
                    }
                }
            }
            if (productNode.hasProperty(CommonConstant.VALID_TO_DATE)) {
                if (productNode.getProperty(CommonConstant.VALID_TO_DATE) != null) {
                    date = PropertyUtil.getDate(productNode, CommonConstant.VALID_TO_DATE);
                    if (date != null) {
                        String toDate = NvxDateUtils.formatDate(date.getTime(), NvxDateUtils.PRODUCT_DETAILS_DATE_FORMAT_DISPLAY);
                        productNode.setProperty(CommonConstant.VALID_TO_DATE, toDate);
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return productNode;
    }

    public List<ContentMap> getTopUpInfo(Node productNode) {

        Node relatedAXProductParentNode = null;

        try {
            if (productNode.hasNode(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                relatedAXProductParentNode = productNode.getNode(CMSProductProperties.RelatedAXProducts.getPropertyName());

                List<Node> relatedAxProducts = NodeUtil.asList(NodeUtil.getNodes(relatedAXProductParentNode));
                List<ContentMap> topUpsContentMap = new ArrayList<>();

                for (Node axProductUUID : relatedAxProducts) {
                    if (axProductUUID.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                        String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                        Node axProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);
                        List<Node> axTopupsList = getAXProductCrossSells(axProduct);
                        for (Node axTopups : axTopupsList) {
                            ContentMap axProductContentMap = asContentMap(wrapForI18n(axTopups));
                            topUpsContentMap.add(axProductContentMap);
                        }
                    }
                }
                return topUpsContentMap;
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public List<Node> getAXProductCrossSells(Node axProductNode) {
        List<Node> crossSellList = new ArrayList<>();
        try {

            if (axProductNode.hasNode(CommonConstant.AX_PRODUCT_CROSS_SELL)) {
                if (axProductNode.getNode(CommonConstant.AX_PRODUCT_CROSS_SELL) != null) {

                    Node crossSellNode = axProductNode.getNode(CommonConstant.AX_PRODUCT_CROSS_SELL);
                    Iterable<Node> crossSellIterable = NodeUtil.getNodes(crossSellNode);
                    Iterator<Node> crossSellIterator = crossSellIterable.iterator();

                    while (crossSellIterator.hasNext()) {
                        Node crossSellChildNode = crossSellIterator.next();

                        if ("show".equals(PropertyUtil.getString(crossSellChildNode, "crossSellDisplay"))
                                && "Active".equals(PropertyUtil.getString(crossSellChildNode, "systemStatus"))) {
                            crossSellList.add(crossSellChildNode);
                        }

                    }
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return crossSellList;
    }

    public boolean isValidDateRange(Node announcement) {
        Calendar fromDate = null;
        Calendar toDate = null;
        Calendar toDay = Calendar.getInstance();
        toDay.setTime(new Date());

        try {
            if (announcement.hasProperty(CommonConstant.VALID_FROM_DATE) & announcement.hasProperty(CommonConstant.VALID_TO_DATE)) {
                if (announcement.getProperty(CommonConstant.VALID_FROM_DATE) != null & announcement.getProperty(CommonConstant.VALID_TO_DATE) != null) {
                    fromDate = PropertyUtil.getDate(announcement, CommonConstant.VALID_FROM_DATE);
                    toDate = PropertyUtil.getDate(announcement, CommonConstant.VALID_TO_DATE);
                    if ((toDay.after(fromDate) || checkDateEquals(toDay, fromDate)) && (toDay.before(toDate) || checkDateEquals(toDay, toDate))) {
                        return true;
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean checkDateEquals(Calendar today, Calendar otherDay) {

        return NvxDateUtils.datesEqualDayField(today.getTime(), otherDay.getTime());
    }
    
    public Node getAXProductByDisplayProductNumber(String channel, String productCode) {
        try {
            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/ax-products/" + channel);
            List<Node> allProducts = NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.AXProducts.getNodeType()));

            if (StringUtils.isNotBlank(productCode) && productCode.length() > 2) {
                for (Node product : allProducts) {
                    if (product.getProperty("displayProductNumber").getString().toUpperCase().contains(productCode.toUpperCase())) {
                        return product;
                     }
                    }
                }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    
    
    public Node getReceiptTemplateParams(String channel) {
    	Node receiptTemplate = null;
        try {
        	receiptTemplate = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), "/kiosk-receipt-templates-params/" + channel);

            if (receiptTemplate == null) {
                throw new RepositoryException("NO RECEIPT FOUND");
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return receiptTemplate;
    }
}
