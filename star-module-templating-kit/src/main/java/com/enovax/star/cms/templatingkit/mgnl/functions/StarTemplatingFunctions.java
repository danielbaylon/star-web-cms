package com.enovax.star.cms.templatingkit.mgnl.functions;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.*;
import com.enovax.star.cms.commons.model.axstar.AxProductPrice;
import com.enovax.star.cms.commons.model.axstar.AxProductPriceMap;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.util.MagnoliaConfigUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import info.magnolia.cms.core.AggregationState;
import info.magnolia.cms.i18n.DefaultI18nContentSupport;
import info.magnolia.cms.i18n.I18nContentSupport;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.rendering.template.type.TemplateTypeHelper;
import info.magnolia.templating.functions.TemplatingFunctions;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jennylynsze on 4/4/16.
 */
public class StarTemplatingFunctions extends TemplatingFunctions {

    private static final Logger log = LoggerFactory.getLogger(StarTemplatingFunctions.class);

    public static final String functionName = "starfn";

    @Inject
    public StarTemplatingFunctions(Provider<AggregationState> aggregationStateProvider, TemplateTypeHelper templateTypeFunctions, Provider<I18nContentSupport> i18nContentSupport) {
        super(aggregationStateProvider, templateTypeFunctions, i18nContentSupport);
    }

    public String escapeHtml(String value) {
        return StringEscapeUtils.escapeHtml4(value);
    }

    public BigDecimal getPropertyBigDecimal(Node node, String propertyName, String language) {
        Property value = getProperty(node, propertyName, language);
        try {
            return value == null ? null : value.getDecimal();
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    //gets value by whatever the current locale is
    public String getPropertyValue(Node node, String propertyName) {
        try {
            Property value = node.getProperty(propertyName);
            return value == null ? null : value.getString();
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    public String getPropertyValue(Node node, String propertyName, String language) {
        Property value = getProperty(node, propertyName, language);
        try {
            return value == null ? null : value.getString();
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    public Property getProperty(Node node, String propertyName, String language) {
        try {
            Node nativeNode = NodeUtil.unwrap(node);
            if (Locale.ENGLISH.toString().equals(language)) {
                return PropertyUtil.getPropertyOrNull(node, propertyName);
            } else {
                DefaultI18nContentSupport contentSupport = new DefaultI18nContentSupport();
                if (StringUtils.isNotBlank(language)) {
                    String lang = null;
                    String country = null;
                    if (StringUtils.isNotBlank(language)) {
                        String[] langArr = language.split("_");
                        lang = langArr[0];
                        if (langArr.length > 1) {
                            country = langArr[1];
                        } else {
                            country = null;
                        }
                    }

                    Locale locale;
                    if (country == null) {
                        locale = new Locale(lang);
                    } else {
                        locale = new Locale(lang, country);
                    }
                    Property property = contentSupport.getProperty(nativeNode, propertyName, locale);
                    if (property == null) {
                        return PropertyUtil.getPropertyOrNull(node, propertyName);
                    }
                    return property;
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the CMS Product Categories Nodes given the Channel.
     * Return the Product Categories even if there's no product to be found
     * <p>
     * The nodes are already in i18n enabled.
     *
     * @param channel
     * @return
     */
    public List<Node> getCMSProductCategories(String channel) {
        return getCMSProductCategories(channel, false, false);
    }

    /**
     * Get the CMS Product Categories Nodes given the Channel.
     * <p>
     * There's an option to hide a categories if it doesn't have any products at all
     * <p>
     * The nodes are already in i18n enabled.
     *
     * @param channel
     * @return
     */
    public List<Node> getCMSProductCategories(String channel, boolean excludeNoCMSProducts, boolean excludeDefaultSub) {
        try {
            Node parentNode = JcrRepository.getParentNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/" + channel);
            if (parentNode == null) {
                log.info("No parent node found for channel: " + channel);
                return null;
            }
            List<Node> categoryList = NodeUtil.asList(NodeUtil.getNodes(parentNode, JcrWorkspace.ProductCategories.getNodeType()));
            List<Node> categoryListInI18n = new ArrayList<>();
            for (Node category : categoryList) {
                if (!excludeNoCMSProducts || hasSubCategoriesWithProducts(category, excludeDefaultSub)) {
                    categoryListInI18n.add(this.wrapForI18n(category));
                }
            }
            return categoryListInI18n;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Get the CMS Product Category node given the category node name
     *
     * @param channel
     * @param categoryNodeName
     * @return
     */
    public Node getCMSProductCategory(String channel, String categoryNodeName) {
        try {

            Node node = JcrRepository.getParentNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/" + channel + "/" + categoryNodeName);
            return this.wrapForI18n(node);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    //Just get the first one lo
    public Node getCMSProductCategoryByCMSProduct(String channel, String cmsProductUUID) {
        try {
            Iterable<Node> relatedProductIterable = JcrRepository.query(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(), JcrWorkspace.RelatedCMSProductForCategories.getNodeType(), "/jcr:root/" + channel + "//element(*, mgnl:cms-product-for-category)[@relatedCMSProductUUID = '" + cmsProductUUID + "']");
            Iterator<Node> relatedProductIterator = relatedProductIterable.iterator();

            if (relatedProductIterator.hasNext()) {
                Node relatedProductNode = relatedProductIterator.next();
                Node categoryNode = NodeUtil.getNearestAncestorOfType(relatedProductNode, JcrWorkspace.ProductCategories.getNodeType());

                if (categoryNode != null) {
                    return categoryNode;
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected boolean hasSubCategoriesWithProducts(Node categoryNode, boolean excludeDefaultSub) {
        try {

            if (!excludeDefaultSub) {
                List<Node> defaultSubCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductDefaultSubCategories.getNodeType()));
                for (Node cat : defaultSubCategoryList) {
                    if (hasCMSProductForSubCategory(cat, false)) {
                        return true;
                    }
                }
            }

            List<Node> subCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));
            for (Node cat : subCategoryList) {
                if (hasCMSProductForSubCategory(cat, false)) {
                    return true;
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * check if the sub categories of the category has any active ax product
     * check if
     *
     * @param categoryNode
     * @param excludeNoCMSProducts
     * @param excludesNoActiveAXProduct
     * @param excludeDefault
     * @return
     */
    private boolean hasSubCategoriesWithProducts(Node categoryNode, boolean excludeNoCMSProducts, boolean excludesNoActiveAXProduct, boolean excludeDefault) {
        try {
            //if default is allowed
            if (!excludeDefault) {
                List<Node> defaultSubCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductDefaultSubCategories.getNodeType()));

                if (defaultSubCategoryList != null && !excludeNoCMSProducts) {
                    return true;
                }
                for (Node cat : defaultSubCategoryList) {
                    if (hasCMSProductForSubCategory(cat, excludesNoActiveAXProduct)) {
                        return true;
                    }
                }
            }


            List<Node> subCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));
            for (Node cat : subCategoryList) {
                if (hasCMSProductForSubCategory(cat, excludesNoActiveAXProduct)) {
                    return true;
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * Get all the sub categories of a given category node
     * If there's no active ax product, it will not be return
     *
     * @param categoryNode
     * @return
     */
    public List<Node> getCMSProductSubCategories(Node categoryNode) {
        return getCMSProductSubCategories(categoryNode, true);
    }


    /**
     * Get all the sub categories of a given category node
     * There's an option to still show the sub categories even if it doesnt have a product with active ax product
     *
     * @param categoryNode
     * @return
     */
    public List<Node> getCMSProductSubCategories(Node categoryNode, boolean excludeNoActiveAxProduct) {
        return getCMSProductSubCategories(categoryNode, excludeNoActiveAxProduct, false);
    }


    /**
     * Get all the sub categories of a given category node
     * here's an option to still show the sub categories even if it doesnt have a product with active ax product
     * There's an option to exclude the default sub category
     *
     * @param categoryNode
     * @return
     */
    public List<Node> getCMSProductSubCategories(Node categoryNode, boolean excludeNoActiveAxProduct, boolean excludeDefault) {
        try {
            //get the default one first
            List<Node> finalSubCategoryList = new ArrayList<>();
            List<Node> subCategoryList = null;
            try {
                subCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductSubCategories.getNodeType()));
            } catch (Exception e) {
                log.error(e.getMessage());
            }

            if (!excludeDefault) {
                List<Node> defaultSubCategoryList = NodeUtil.asList(NodeUtil.getNodes(categoryNode, JcrWorkspace.ProductDefaultSubCategories.getNodeType()));
                for (Node cat : defaultSubCategoryList) {
                    if (hasCMSProductForSubCategory(cat, excludeNoActiveAxProduct)) {
                        finalSubCategoryList.add(cat);
                    }
                }
            }

            if (subCategoryList != null) {
                for (Node cat : subCategoryList) {
                    if (hasCMSProductForSubCategory(cat, excludeNoActiveAxProduct)) {
                        finalSubCategoryList.add(cat);
                    }
                }
            }

            return finalSubCategoryList;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    private boolean hasCMSProductForSubCategory(Node subCategoryNode, boolean excludeNoActiveAXProduct) {
        try {
            List<Node> relatedCMSProductList = NodeUtil.asList(NodeUtil.getNodes(subCategoryNode, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
            for (Node relatedCMSProduct : relatedCMSProductList) {
                if (relatedCMSProduct.hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
                    String uuid = relatedCMSProduct.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();

                    try {
                        Node cmsProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);


                        if (cmsProduct != null && !excludeNoActiveAXProduct) {
                            return true;

                        }

                        if (cmsProduct != null && excludeNoActiveAXProduct && hasActiveAXProducts(cmsProduct)) {
                            return true;
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }

                }
            }
            return false;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return false;
    }

    /**
     * Get all the Related CMS product of the Sub Category
     *
     * @param
     * @return
     */
    public List<Node> getCMSProductByCategory(String channel, String categoryNodeName) {

        try {

            List<Node> relatedCMSProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(),
                JcrWorkspace.RelatedCMSProductForCategories.getNodeType(),
                "/jcr:root/" + channel + "/" + categoryNodeName + "//element(*, mgnl:cms-product-for-category)"));

            List<Node> relatedCMSProductMainNodeList = getCMSProductsByRelatedCMSProductList(relatedCMSProductList, true);

            return relatedCMSProductMainNodeList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get all the Related CMS product of the Sub Category
     *
     * @param
     * @return
     */
    public List<Node> getCMSProductByCategoryForPartnerPreview(String channel, String categoryNodeName) {

        try {
            List<Node> relatedCMSProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(),
                JcrWorkspace.RelatedCMSProductForCategories.getNodeType(),
                "/jcr:root/" + channel + "/" + categoryNodeName + "//element(*, mgnl:cms-product-for-category)"));

            List<Node> relatedCMSProductMainNodeList = getCMSProductsByRelatedCMSProductList(relatedCMSProductList, true);

            return relatedCMSProductMainNodeList;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String genRandomNumber() {
        return NvxUtil.generateRandomAlphaNumeric(5, true);
    }

    public List<Node> getCMSProductByCategoryForPartner(String channel, String categoryNodeName, String randomNumber) {
        return getCMSProductByCategoryForPartner(channel, categoryNodeName);
    }

    public List<Node> getCMSProductByCategoryForPartner(String channel, String categoryNodeName) {
        log.info("Entered the getCMSProductByCategoryForPartner for channel = " + channel + " and category node name = " + categoryNodeName);
        List<Node> relatedCMSProductList = new ArrayList<>();
        List<Node> partnerCMSProductList = new ArrayList<>();
        List<String> partnerCMSProductName = new ArrayList<>();
        List<Node> orderedPartnerCMSProductList = new ArrayList<>();

        try {
            relatedCMSProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(),
                JcrWorkspace.RelatedCMSProductForCategories.getNodeType(),
                "/jcr:root/" + channel + "/" + categoryNodeName + "//element(*, mgnl:cms-product-for-category)"));

            List<Node> relatedCMSProductMainNodeList = getCMSProductsByRelatedCMSProductList(relatedCMSProductList, true);

            String partnerId = MgnlContext.getAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_ID, Context.SESSION_SCOPE) + "";
            //log.info("Partner ID = " + partnerId);

            Map<String, Node> productIdNodeMapping = new HashMap<>();
            for (Node relCmsProd : relatedCMSProductMainNodeList) {
                try {
                    productIdNodeMapping.put(relCmsProd.getName(), relCmsProd);
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }

            if (StringUtils.isEmpty(partnerId)) {
                return null;
            }

            //get the tier products
            try {
                //TODO: re-evaluate how to make sure to update the TierID in case it changed...
                String tierId = MgnlContext.getAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_PARTNER_TIER_ID, Context.SESSION_SCOPE);
                log.info("Tier ID = " + tierId);
                List<Node> tierProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.TierProductMappings.getWorkspaceName(), JcrWorkspace.TierProductMappings.getNodeType(),
                    "/jcr:root/" + channel + "//element(*, " + JcrWorkspace.TierProductMappings.getNodeType() + ")[@tierId = '" + tierId + "']"));

                for (Node tierProduct : tierProductList) {
                    String productId = PropertyUtil.getString(tierProduct, "productId");
                    if (productIdNodeMapping.containsKey(productId)) {
                        partnerCMSProductList.add(productIdNodeMapping.get(productId));
                        partnerCMSProductName.add(productId);
                    }
                }
            } catch (RepositoryException e) {
                log.error(e.getMessage(), e);
                log.info("No Partner with id = " + partnerId);
            }

            //get the exclusive products
            try {
                Node exclProdsMapping = JcrRepository.getParentNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), "/" + channel + "/" + partnerId);
                String exclProdsList = PropertyUtil.getString(exclProdsMapping, "exclProdIds");
                if (StringUtils.isNotEmpty(exclProdsList)) {
                    String exclProdsArr[] = exclProdsList.split(",");
                    for (String exclProd : exclProdsArr) {
                        if (productIdNodeMapping.containsKey(exclProd)) {
                            partnerCMSProductList.add(productIdNodeMapping.get(exclProd));
                            partnerCMSProductName.add(exclProd);
                        }
                    }

                }
            } catch (RepositoryException e) {
                log.error(e.getMessage(), e);
                log.info("No exclusive product specified for Partner with id = " + partnerId);
            }


            for (Node relCmsProd : relatedCMSProductMainNodeList) {
                if(partnerCMSProductName.contains(relCmsProd.getName())) {
                    orderedPartnerCMSProductList.add(relCmsProd);
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
            log.info("Error encountered in getCMSProductByCategoryForPartner");
        }
        return orderedPartnerCMSProductList;
    }


    /**
     * Get all the Related CMS product of the Sub Category
     *
     * @param subCategoryNode
     * @return
     */
    public List<Node> getCMSProductBySubCategory(Node subCategoryNode) {
        return getCMSProductBySubCategory(subCategoryNode, true);
    }

    /**
     * Get all the Related CMS product of the Sub Category
     *
     * @param subCategoryNode
     * @return
     */
    public List<Node> getCMSProductBySubCategory(Node subCategoryNode, boolean excludeNoActiveAXProduct) {
        try {
            List<Node> relatedCMSProductList = NodeUtil.asList(NodeUtil.getNodes(subCategoryNode, JcrWorkspace.RelatedCMSProductForCategories.getNodeType()));
            return getCMSProductsByRelatedCMSProductList(relatedCMSProductList, excludeNoActiveAXProduct);

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Get all the Related CMS product of the RelatedCMSProduct Nodes
     *
     * @param
     * @return
     */
    public List<Node> getCMSProductsByRelatedCMSProductList(List<Node> relatedCMSProductList, boolean excludeNoActiveAXProduct) {
        try {
            List<Node> cmsProductList = new ArrayList<>();
            for (Node relatedCMSProduct : relatedCMSProductList) {
                if (relatedCMSProduct.hasProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName())) {
                    String uuid = relatedCMSProduct.getProperty(CMSProductSubCategoriesProperties.RelatedCMSProducts.getPropertyName()).getString();
                    try {
                        Node cmsProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);
                        if (cmsProduct != null && (!excludeNoActiveAXProduct || hasActiveAXProducts(cmsProduct))) {
                            cmsProductList.add(this.wrapForI18n(cmsProduct));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            return cmsProductList;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }


    /**
     * Get the CMS Product Node given the productNodeName
     *
     * @param channel
     * @param productNodeName
     * @return
     */
    public Node getCMSProduct(String channel,
                              String productNodeName) {
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productNodeName);
            return this.wrapForI18n(node);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Get the UUID of the list of related product images
     *
     * @param productNode
     * @param language
     * @return
     */
    public List<String> getCMSProductImagesUUID(Node productNode, String language) {
        try {
            Node productImageParentNode;

            if (productNode.hasNode(CMSProductProperties.ProductImages.getPropertyName())) {
                productImageParentNode = productNode.getNode(CMSProductProperties.ProductImages.getPropertyName());
            } else {
                return null;
            }

            if (productImageParentNode == null) {
                return null;
            }

            List<Node> relatedCMSProductImagesList = NodeUtil.asList(NodeUtil.getNodes(productImageParentNode));
            List<String> relatedCMSProductImagesListUUID = new ArrayList<>();
            boolean hasEnglish = false;
            boolean hasOtherLang = false;

            for (Node relatedCMSProductImage : relatedCMSProductImagesList) {
                //TODO check if this is ok
                if (Locale.ENGLISH.toString().equals(language) && relatedCMSProductImage.getName().indexOf("_") == -1) {
                    if (relatedCMSProductImage.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
                        relatedCMSProductImagesListUUID.add(relatedCMSProductImage.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString());
                        hasEnglish = true;
                    }
                } else if (relatedCMSProductImage.getName().endsWith(language)) {
                    if (relatedCMSProductImage.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
                        relatedCMSProductImagesListUUID.add(relatedCMSProductImage.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString());
                        hasOtherLang = true;
                    }
                }
            }

            //fallback to english if no other language
            if (!Locale.ENGLISH.toString().equals(language) && !hasOtherLang) {
                for (Node relatedCMSProductImage : relatedCMSProductImagesList) {
                    //TODO check if this is ok
                    if (relatedCMSProductImage.getName().indexOf("_") == -1) {
                        if (relatedCMSProductImage.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
                            relatedCMSProductImagesListUUID.add(relatedCMSProductImage.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString());
                        }
                    }
                }
            }

            return relatedCMSProductImagesListUUID;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }


    /**
     * Get the list of Related AX Product
     *
     * @param channel
     * @return
     */
    public List<Node> getAllAXProducts(String channel) {
        try {
            List<Node> axProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.AXProducts.getWorkspaceName(), JcrWorkspace.AXProducts.getNodeType(), "/jcr:root/ax-products/" + channel + "//element(*, mgnl:ax-product)[@status='Active']"));
            return axProductList;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    /**
     * Get the list of  AX Product by Product Name
     *
     * @param channel
     * @return
     */
    public List<Node> getAXProductsByProductName(String channel, String prodName) {
        try {
            List<Node> axProductList = NodeUtil.asList(JcrRepository.query(JcrWorkspace.AXProducts.getWorkspaceName(), JcrWorkspace.AXProducts.getNodeType(),
                "/jcr:root/ax-products/" + channel + "//element(*, mgnl:ax-product)[@status='Active' and jcr:like(fn:lower-case(@productName), '%" + prodName.toLowerCase() + "%')]"));
            return axProductList;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    /**
     * Get the list of Related AX Product
     *
     * @param productNode
     * @return
     */
    public List<Node> getAXProductsByCMSProductNode(Node productNode) {
        return getAXProductsByCMSProductNode(productNode, false);
    }

    public List<Node> getAXProductsByCMSProductNode(Node productNode, boolean showAll) {
        Node relatedAXProductParentNode = null;

        try {
            if (productNode.hasNode(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                relatedAXProductParentNode = productNode.getNode(CMSProductProperties.RelatedAXProducts.getPropertyName());
            } else {
                return null;
            }

            if (relatedAXProductParentNode == null) {
                return null;
            }

            List<Node> relatedAxProducts = NodeUtil.asList(NodeUtil.getNodes(relatedAXProductParentNode));
            List<Node> activeRelatedAxProducts = new ArrayList<>();
            for (Node axProductUUID : relatedAxProducts) {
                if (axProductUUID.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                    String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                    Node axProduct = this.wrapForI18n(NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid));
                    if (isValidAxProduct(axProduct)) {
                        boolean displayAxProduct = true;
                        // Additional validation if a ticket will be displayed.
                        if (!showAll) {
                            List<Node> promotions = getAXProductPromotions(axProduct, true);
                            if (promotions != null) {
                                for (Node promotion : promotions) {
                                    if (PropertyUtil.getBoolean(promotion, AXProductPromotionProperties.UsedAsUnlocker.getPropertyName(), false)) {
                                        displayAxProduct = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (displayAxProduct) {
                            activeRelatedAxProducts.add(axProductUUID);
                        }
                    }
                }
            }

            return activeRelatedAxProducts;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public boolean isValidAxProduct(Node axProductNode) {

        String status = PropertyUtil.getString(axProductNode, AXProductProperties.Status.getPropertyName());

        if (status != null && !AXProductStatus.Active.name().equals(status)) {
            return false;
        }

        Date now = new Date();
        Date nowDateOnly = NvxDateUtils.clearTime(now);

        Calendar validFrom = PropertyUtil.getDate(axProductNode, AXProductProperties.ValidFrom.getPropertyName());
        Calendar validTo = PropertyUtil.getDate(axProductNode, AXProductProperties.ValidTo.getPropertyName());

        if (validFrom == null || validTo == null) {
            return true;
        }

        return validFrom != null && validTo != null && validFrom.getTime().compareTo(nowDateOnly) <= 0 && validTo.getTime().compareTo(nowDateOnly) >= 0;

    }


    public boolean hasActiveAXProducts(Node productNode) {
        List<Node> relatedAXProductList = getAXProductsByCMSProductNode(productNode);
        return relatedAXProductList != null && relatedAXProductList.size() > 0;
    }

    /**
     * Get related ax product details
     *
     * @param productNode
     * @return
     */

    public Map<String, Node> getAXProductsDetailByCMSProductNode(String channel, Node productNode) {
        return getAXProductsDetailByCMSProductNode(channel, productNode, false);
    }


    public Map<String, Node> getAXProductsDetailByCMSProductNode(String channel, Node productNode, boolean showAll) {
        try {

            AxProductPriceMap priceMap = null;
            Map<String, List<AxProductPrice>> listingIdPriceMap = new HashMap<>();
            if (StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channel) || StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(channel)) {
                priceMap = MgnlContext.getAttribute(PartnerPortalConst.CACHE_PARTNER_PRODUCT_PRICE_MAP, Context.SESSION_SCOPE);
                if (priceMap != null) {
                    listingIdPriceMap = priceMap.getProductPriceMap();
                }
            }
            List<Node> relatedAXProductList = getAXProductsByCMSProductNode(productNode, showAll);
            Map<String, Node> axProductDetail = new HashMap<>();
            for (Node axProductUUID : relatedAXProductList) {
                if (axProductUUID.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                    String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                    Node axProduct = this.wrapForI18n(NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid));
                    if (AXProductStatus.Active.name().equals(axProduct.getProperty(AXProductProperties.Status.getPropertyName()).getString())) {

                        if (priceMap != null) {
                            String listingId = PropertyUtil.getString(axProduct, AXProductProperties.ProductListingId.getPropertyName());
                            if (listingIdPriceMap.containsKey(listingId)) {
                                AxProductPrice productPrice = listingIdPriceMap.get(listingId).get(0);
                                axProduct.setProperty(AXProductProperties.ProductPrice.getPropertyName(), productPrice.getCustomerContextualPrice());
                                //relCmsProd.setProperty("publishPrice", productPrice.getBasePrice()); //only get the first one loh
                                axProduct.setProperty("publishPrice", productPrice.getBasePrice()); //only get the first one loh
                            }
                        }
                        axProductDetail.put(uuid, axProduct);
                    }
                }
            }
            assignTicketGroupings(relatedAXProductList, axProductDetail);
            return axProductDetail;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    private void assignTicketGroupings(List<Node> relatedAXProductList, Map<String, Node> axProductDetail) throws RepositoryException {
        Iterator<Node> i = relatedAXProductList.iterator();
        if (i.hasNext()) {
            Node axProductUUID = i.next();
            String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
            Node axProduct = axProductDetail.get(uuid);

            if (axProduct != null) {
                Node currentItem = null;
                Node nextItem = axProduct;
                Node groupStart = null;

                nextItem.setProperty("inGroup", false);
                nextItem.setProperty("displayGroupName", true);
                nextItem.setProperty("groupSize", 1);
                int groupSize = 1;

                while (i.hasNext()) {
                    currentItem = nextItem;

                    axProductUUID = i.next();
                    uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                    axProduct = axProductDetail.get(uuid);
                    nextItem = axProduct;

                    while (nextItem == null && i.hasNext()) {
                        axProductUUID = i.next();
                        uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                        axProduct = axProductDetail.get(uuid);
                        nextItem = axProduct;
                    }

                    if (nextItem != null) {
                        if (isSameGroup(currentItem, nextItem)) {
                            if (groupSize == 1) {
                                currentItem.setProperty("inGroup", true);
                                currentItem.setProperty("displayGroupName", true);
                                groupStart = currentItem;
                            }
                            nextItem.setProperty("inGroup", true);
                            nextItem.setProperty("displayGroupName", false);
                            nextItem.setProperty("groupSize", 1);
                            groupSize++;
                            groupStart.setProperty("groupSize", groupSize);
                        } else {
                            if (groupSize > 1) {
                                groupSize = 1;
                            }
                            nextItem.setProperty("inGroup", false);
                            nextItem.setProperty("displayGroupName", true);
                            nextItem.setProperty("groupSize", 1);
                        }
                    }

                }
            }

        }
    }

    private boolean isSameGroup(Node a, Node b) {
        return PropertyUtil.getString(a, AXProductProperties.ProductName.getPropertyName(), "").equals(
                PropertyUtil.getString(b, AXProductProperties.ProductName.getPropertyName(), ""));
    }

    public Map<String, Node> getAXProductsDetailByCMSProductNode(Node productNode) {
        return getAXProductsDetailByCMSProductNode(productNode, false); //dont pass the channel
    }

    public Map<String, Node> getAXProductsDetailByCMSProductNode(Node productNode, boolean showAll) {
        return getAXProductsDetailByCMSProductNode("", productNode, showAll); //dont pass the channel;
//    try {
//      List<Node> relatedAXProductList = getAXProductsByCMSProductNode(productNode, showAll);
//      Map<String, Node> axProductDetail = new HashMap<>();
//      for (Node axProductUUID : relatedAXProductList) {
//        if (axProductUUID.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
//          String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
//          Node axProduct = this.wrapForI18n(NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid));
//          if (AXProductStatus.Active.name().equals(axProduct.getProperty(AXProductProperties.Status.getPropertyName()).getString())) {
//            axProductDetail.put(uuid, axProduct);
//          }
//        }
//      }
//      return axProductDetail;
//    } catch (RepositoryException e) {
//      log.error(e.getMessage(), e);
//    }
//
//    return null;
    }

    /**
     * Retrieve the lowest price for the CMS product
     *
     * @param productNode
     * @return
     */
    public String getCMSProductLowestTicketPrice(Node productNode) {
        try {
            List<Node> relatedAXProductList = getAXProductsByCMSProductNode(productNode);
            BigDecimal minPrice = null;
            String minPriceString = null;
            for (Node relatedAXProduct : relatedAXProductList) {
                if (relatedAXProduct.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                    String uuid = relatedAXProduct.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                    Node axProduct = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);
                    if (axProduct != null && AXProductStatus.Active.name().equals(axProduct.getProperty(AXProductProperties.Status.getPropertyName()).getString())) {
                        BigDecimal price = axProduct.getProperty(AXProductProperties.ProductPrice.getPropertyName()).getDecimal();
                        if (minPrice == null || minPrice.compareTo(price) > 0) {
                            minPrice = price;
                            minPriceString = axProduct.getProperty(AXProductProperties.ProductPrice.getPropertyName()).getString();
                        }
                    }
                }
            }

            return minPriceString == null ? null : minPriceString;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Getting the first category
     *
     * @param channel
     * @return
     */
    public Node getFirstCategory(String channel) {
        try {
            Node parentNode = JcrRepository.getParentNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/" + channel);
            Iterable<Node> categoryNodes = NodeUtil.getNodes(parentNode, JcrWorkspace.ProductCategories.getNodeType());

            if (categoryNodes != null) {
                Iterator<Node> categoryNodesIterator = categoryNodes.iterator();

                if (categoryNodesIterator.hasNext()) {
                    return this.wrapForI18n(categoryNodesIterator.next());
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    /**
     * Getting the first CMS product
     *
     * @param channel
     * @return
     */
    public Node getFirstProduct(String channel) {
        try {
            Node parentNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel);

            Iterable<Node> productNodes = NodeUtil.getNodes(parentNode, JcrWorkspace.CMSProducts.getNodeType());

            if (productNodes != null) {
                Iterator<Node> productNodesIterator = productNodes.iterator();

                if (productNodesIterator.hasNext()) {
                    return this.wrapForI18n(productNodesIterator.next());
                }
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Getting the first CMS product
     *
     * @param channel
     * @return
     */
    public Node getFirstProduct(String channel, String categoryNodeName) {
        try {
            Node parentNode = JcrRepository.getParentNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/" + channel + "/" + categoryNodeName);

            List<Node> subCategories = getCMSProductSubCategories(parentNode);

            if (subCategories != null) {
                for (Node sub : subCategories) {
                    List<Node> products = getCMSProductBySubCategory(sub);
                    if (products != null) {
                        return products.get(0);
                    }
                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    /**
     * Get the cross sell of the product
     *
     * @param axProductNode should be the node of the actual ax product
     * @return
     */
    public List<Node> getAXProductCrossSells(Node axProductNode) {
        List<Node> crossSellList = new ArrayList<>();
        try {

            if (axProductNode.hasNode("cross-sell")) {
                Node crossSellNode = axProductNode.getNode("cross-sell");
                Iterable<Node> crossSellIterable = NodeUtil.getNodes(crossSellNode);
                Iterator<Node> crossSellIterator = crossSellIterable.iterator();

                while (crossSellIterator.hasNext()) {
                    Node crossSellChildNode = crossSellIterator.next();

                    if ("show".equals(PropertyUtil.getString(crossSellChildNode, "crossSellDisplay")) &&
                        "Active".equals(PropertyUtil.getString(crossSellChildNode, "systemStatus"))) {
                        crossSellList.add(crossSellChildNode);
                    }

                }
            }

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return crossSellList;
    }

    /**
     * Get the cross sell of the product
     *
     * @param uuid of the ax product
     * @return
     */
    public List<Node> getAXProductCrossSellsByAXProductUUID(String uuid) {
        try {
            Node axProductNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);
            return getAXProductCrossSells(axProductNode);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Get the cross sell of the product
     *
     * @param listingId of the ax product
     * @return
     */
    public List<Node> getAXProductCrossSellsByListingId(String channel, String listingId) {
        try {
            Node axProductNode = JcrRepository.getParentNode(JcrWorkspace.AXProducts.getWorkspaceName(), "/ax-products/" + channel + "/" + listingId);
            return getAXProductCrossSells(axProductNode);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Get all promotions given a channel
     *
     * @param channel
     * @return
     */
    public List<Node> getAllPromotions(String channel) {

        try {
            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.Promotions.getWorkspaceName(), "/" + channel);
            return NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.Promotions.getNodeType()));
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }


    /**
     * Get the promotions of the product
     *
     * @param axProductNode should be the node of the actual ax product
     * @return
     */
    public List<Node> getAXProductPromotions(Node axProductNode) {
        return getAXProductPromotions(axProductNode, false);
    }


    public List<Node> getAXProductPromotions(Node axProductNode, boolean showAll) {
        List<Node> promotionList = new ArrayList<>();

        try {
            if (axProductNode.hasNode(AXProductProperties.Promotion.getPropertyName())) {
                Node promotionNode = axProductNode.getNode(AXProductProperties.Promotion.getPropertyName());
                Iterable<Node> promotionIterable = NodeUtil.getNodes(promotionNode);
                Iterator<Node> promotionIterator = promotionIterable.iterator();

                while (promotionIterator.hasNext()) {
                    Node promotionChildNode = promotionIterator.next();

                    String uuid = PropertyUtil.getString(promotionChildNode, AXProductProperties.RelatedPromotionUUID.getPropertyName());
                    Node promotionMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Promotions.getWorkspaceName(), uuid);


                    Calendar today = Calendar.getInstance();

                    if (PropertyUtil.getDate(promotionMainNode, "validFrom").compareTo(today) <= 0 &&
                            PropertyUtil.getDate(promotionMainNode, "validTo").compareTo(today) >= 0 &&
                            "Active".equals(PropertyUtil.getString(promotionMainNode, "systemStatus"))) {

                        if (showAll ||
                                "show".equals(PropertyUtil.getString(promotionMainNode, AXProductPromotionProperties.PromotionDisplay.getPropertyName()))) {
                            String estimatedPriceString = PropertyUtil.getString(promotionChildNode, AXProductPromotionProperties.CachedDiscountPrice.getPropertyName());

                            if (StringUtils.isNotEmpty(estimatedPriceString)) {
                                promotionMainNode.setProperty(AXProductPromotionProperties.CachedDiscountPrice.getPropertyName(), new BigDecimal(estimatedPriceString));  //TESTING!!!!
                            }
                            promotionList.add(promotionMainNode);
                        }

                    }


                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return promotionList;
    }

    /**
     * Get the promotions of the product
     *
     * @param uuid of the ax product
     * @return
     */
    public List<Node> getAXProductPromotionsByAxProductUUID(String uuid) {
        try {
            Node axProductNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid);
            return getAXProductPromotions(axProductNode);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param productCode refers to the displayProductNumber in AX
     * @return the AxProductNode with this productCode
     */
    public Node getAxProductByProductCode(String channel, String productCode) {

        AxProductPriceMap priceMap = null;
        Map<String, List<AxProductPrice>> listingIdPriceMap = new HashMap<>();
        //TODO reevaluate if need to separate...
        if (StoreApiChannels.PARTNER_PORTAL_SLM.code.equals(channel) || StoreApiChannels.PARTNER_PORTAL_MFLG.code.equals(channel)) {
            priceMap = MgnlContext.getAttribute(PartnerPortalConst.CACHE_PARTNER_PRODUCT_PRICE_MAP, Context.SESSION_SCOPE);
            if (priceMap != null) {
                listingIdPriceMap = priceMap.getProductPriceMap();
            }
        }

        String query = "/jcr:root/ax-products/" + channel + "//element(*, mgnl:ax-product)[@displayProductNumber = '" + productCode + "']";
        try {
            Iterable<Node> axProductNodeIterable = JcrRepository.query(JcrWorkspace.AXProducts.getWorkspaceName(), JcrWorkspace.AXProducts.getNodeType(), query);
            Iterator<Node> axProductNodeIterator = axProductNodeIterable.iterator();

            Node axProductNode = null;
            if (axProductNodeIterator.hasNext()) {
                axProductNode = axProductNodeIterator.next();

                if (priceMap != null) {
                    String listingId = PropertyUtil.getString(axProductNode, AXProductProperties.ProductListingId.getPropertyName());
                    if (listingIdPriceMap.containsKey(listingId)) {
                        AxProductPrice productPrice = listingIdPriceMap.get(listingId).get(0);
                        axProductNode.setProperty(AXProductProperties.ProductPrice.getPropertyName(), productPrice.getCustomerContextualPrice());
                        //relCmsProd.setProperty("publishPrice", productPrice.getBasePrice()); //only get the first one loh
                        axProductNode.setProperty("publishPrice", productPrice.getBasePrice()); //only get the first one loh
                    }
                }
            }

            return wrapForI18n(axProductNode);
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }


    public String getTermsAndConditions(String channel) {
        return getTermsAndConditions(channel, null);
    }

    /**
     * [#assign tnc = starfn.getTermsAndConditions("b2c-slm", prd)]
     * <div>
     * ${tnc}
     * </div>
     * Gets the Terms and Conditions, both the General and Product specific ones
     *
     * @param channel
     * @param productNodeName
     * @return
     */
    public String getTermsAndConditions(String channel, String productNodeName) {
        try {
            StringBuilder tncContent = new StringBuilder();
            String query = "/jcr:root/" + channel + "//element(*, mgnl:tnc)[@type='General']";
            //Get the general tnc
            Iterable<Node> generalTncIterable = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(), "mgnl:tnc", query);
            Iterator<Node> generalTncIterator = generalTncIterable.iterator();

            while (generalTncIterator.hasNext()) {
                if (tncContent.length() > 0) {
                    tncContent.append("<br/>");
                }

                tncContent.append(PropertyUtil.getString(wrapForI18n(generalTncIterator.next()), "content"));
            }

            if (StringUtils.isNotEmpty(productNodeName)) {
                Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productNodeName);
                String tncUUID = PropertyUtil.getString(productNode, CMSProductProperties.TNC.getPropertyName());

                if (!StringUtils.isEmpty(tncUUID)) {
                    Node tncNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.TNC.getWorkspaceName(), tncUUID);

                    if (tncContent.length() > 0) {
                        tncContent.append("<br/>");
                    }

                    tncContent.append(PropertyUtil.getString(wrapForI18n(tncNode), "content"));
                }
            }

            return tncContent.toString();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * [#assign tnc = starfn.getTermsAndConditions("b2c-slm", prd)]
     * <div>
     * ${tnc}
     * </div>
     * Gets the Terms and Conditions, both the General and Product specific ones
     *
     * @param channel
     * @param productIds
     * @return
     */
    public String getTermsAndConditions(String channel, List<String> productIds, String language) {
        try {
            StringBuilder tncContent = new StringBuilder();
            String query = "/jcr:root/" + channel + "//element(*, mgnl:tnc)[@type='General']";
            //Get the general tnc
            Iterable<Node> generalTncIterable = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(), "mgnl:tnc", query);
            Iterator<Node> generalTncIterator = generalTncIterable.iterator();

            while (generalTncIterator.hasNext()) {
                if (tncContent.length() > 0) {
                    tncContent.append("<br/>");
                }

                Property content = getProperty(generalTncIterator.next(), "content", language);
                tncContent.append(content == null ? "" : content.getString());
            }

            if (productIds != null && productIds.size() > 0) {

                for (String productId : productIds) {
                    Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productId);
                    String tncUUID = PropertyUtil.getString(productNode, CMSProductProperties.TNC.getPropertyName());

                    if (!StringUtils.isEmpty(tncUUID)) {
                        Node tncNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.TNC.getWorkspaceName(), tncUUID);

                        if (tncContent.length() > 0) {
                            tncContent.append("<br/>");
                        }

                        Property content = getProperty(tncNode, "content", language);
                        tncContent.append(content == null ? "" : content.getString());
                    }
                }

            }

            return tncContent.toString();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<ContentMap> getGeneralTncContent(String channel) {
        try {
            String query = "/jcr:root/" + channel + "//element(*, mgnl:tnc)[@type='General']";
            //Get the general tnc
            Iterable<Node> generalTncIterable = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(), "mgnl:tnc", query);
            Iterator<Node> generalTncIterator = generalTncIterable.iterator();
            List<ContentMap> tncMaps = new ArrayList<>();
            while (generalTncIterator.hasNext()) {
                Node tncNode = generalTncIterator.next();
                boolean active = "Active".equals(PropertyUtil.getString(tncNode, "status", "Inactive"));
                if (active) {
                    ContentMap tncMap = asContentMap(wrapForI18n(tncNode));
                    tncMaps.add(tncMap);
                }
            }
            return tncMaps;
        } catch (RepositoryException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ContentMap getProductTncContent(String channel, String productId) {
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productId);
            String tncUUID = PropertyUtil.getString(productNode, CMSProductProperties.TNC.getPropertyName());
            if (!StringUtils.isEmpty(tncUUID)) {
                Node tncNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.TNC.getWorkspaceName(), tncUUID);
                boolean active = "Active".equals(PropertyUtil.getString(tncNode, "status", "Inactive"));
                if (active) {
                    ContentMap tncMap = asContentMap(wrapForI18n(tncNode));
                    return tncMap;
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ContentMap> getProductTncContent(String channel, List<String> productIds) {
        if (productIds != null && productIds.size() > 0) {
            List<ContentMap> tncMaps = new ArrayList<>();
            for (String productId : productIds) {
                ContentMap tncMap = getProductTncContent(channel, productId);
                if (tncMap != null) {
                    tncMaps.add(tncMap);
                }
            }
            return tncMaps;
        }
        return null;
    }

    /**
     * Getting the Terms and Conditions for the General (to be used by ajax call)
     *
     * @param channel
     * @param language
     * @return
     */
    public List<TncVM> getGeneralTermsAndConditions(String channel, String language) {
        try {
            String query = "/jcr:root/" + channel + "//element(*, mgnl:tnc)[@type='General']";
            //Get the general tnc
            Iterable<Node> generalTncIterable = JcrRepository.query(JcrWorkspace.TNC.getWorkspaceName(), "mgnl:tnc", query);
            Iterator<Node> generalTncIterator = generalTncIterable.iterator();
            List<TncVM> tncVMs = new ArrayList<>();

            while (generalTncIterator.hasNext()) {
                Node tncNode = generalTncIterator.next();
                Property content = getProperty(tncNode, "content", language);
                Property title = getProperty(tncNode, "title", language);

                TncVM tnc = new TncVM();
                tnc.setTitle(PropertyUtil.getValueString(title));
                tnc.setContent(PropertyUtil.getValueString(content));
                tnc.setType(TNCType.General.toString());
                tncVMs.add(tnc);
            }

            return tncVMs;

        } catch (RepositoryException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<TncVM> getProductTermsAndConditions(String channel, List<String> productIds, String language) {
        try {
            if (productIds != null && productIds.size() > 0) {
                List<TncVM> tncVMs = new ArrayList<>();

                for (String productId : productIds) {
                    Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productId);
                    String tncUUID = PropertyUtil.getString(productNode, CMSProductProperties.TNC.getPropertyName());

                    if (!StringUtils.isEmpty(tncUUID)) {
                        Node tncNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.TNC.getWorkspaceName(), tncUUID);
                        Property content = getProperty(tncNode, "content", language);
                        Property title = getProperty(tncNode, "title", language);

                        TncVM tnc = new TncVM();
                        tnc.setTitle(PropertyUtil.getValueString(title));
                        tnc.setContent(PropertyUtil.getValueString(content));
                        tnc.setType(TNCType.Product.toString());
                        tncVMs.add(tnc);
                    }
                }

                return tncVMs;
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Can pass the product node name
     *
     * @param channel
     * @param productNodeName
     * @return
     */
    public List<ContentMap> getRecommendProducts(String channel, String productNodeName, String language) {
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productNodeName);
            return getRecommendProducts(channel, productNode, language);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * [#assign recommendedProductsMap =  starfn.getRecommendProducts("b2c-slm", cmsProductNode)]
     * [#if recommendedProductsMap?has_content]
     * [#list recommendedProductsMap as recommendedProduct]
     * Note: can access the property of the cms product directly by a . property name
     * The categoryId (PK of the category) and categoryGeneratedURLPath (this one is shown in the url category=packages#!/).
     * <div>${recommendedProduct.name}</div>
     * <div>${recommendedProduct.generatedURLPath}</div>
     * <div>${recommendedProduct.categoryId}</div>
     * <div>${recommendedProduct.categoryGeneratedURLPath}</div>
     * [/#list]
     * [/#if]
     *
     * @param channel
     * @param productNode
     * @return
     */
    public List<ContentMap> getRecommendProducts(String channel, Node productNode, String language) {
        try {
            Node recommendedProductParentNode;

            if (productNode.hasNode(CMSProductProperties.RecommendedProducts.getPropertyName())) {
                recommendedProductParentNode = productNode.getNode(CMSProductProperties.RecommendedProducts.getPropertyName());
            } else {
                return null;
            }

            if (recommendedProductParentNode == null) {
                return null;
            }


            List<Node> recommendedProductList = NodeUtil.asList(NodeUtil.getNodes(recommendedProductParentNode));
            List<ContentMap> recommendedProductContentMap = new ArrayList<>();
            for (Node recommendedProduct : recommendedProductList) {
                String uuid = PropertyUtil.getString(recommendedProduct, CMSProductProperties.RecommendedProducts.getPropertyName());
                Node recProductNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.CMSProducts.getWorkspaceName(), uuid);
                //get the category here
                String query = "//element(*, mgnl:cms-product-for-category)[@relatedCMSProductUUID='" + recProductNode.getIdentifier() + "']";
                Iterable<Node> cmsProductNodeIterable = JcrRepository.query(JcrWorkspace.ProductCategories.getWorkspaceName(), "mgnl:cms-product-for-category", query);
                Iterator<Node> cmsProductNodeIterator = cmsProductNodeIterable.iterator();

                if (cmsProductNodeIterator.hasNext()) {
                    Node cmsProductNode = cmsProductNodeIterator.next();
                    Node categoryNode = cmsProductNode.getParent().getParent();
                    recProductNode.setProperty("categoryId", categoryNode.getName());
                    recProductNode.setProperty("categoryGeneratedURLPath", PropertyUtil.getString(categoryNode, CMSProductCategoriesProperties.GeneratedUrlPath.getPropertyName()));

                    String productUrl = getProductUrl(categoryNode, recProductNode, channel, language);
                    recProductNode.setProperty("productUrl", productUrl);

                    if (recProductNode.hasNode("relatedProductImage")) {
                        NodeIterator imageIterator = recProductNode.getNode("relatedProductImage").getNodes();
                        if (imageIterator.hasNext()) {
                            String imageUuid = PropertyUtil.getString(imageIterator.nextNode(), "relatedProductImage");
                            recProductNode.setProperty("imageUuid", imageUuid);
                        }
                    }
                }


                ContentMap recProductContentMap = asContentMap(wrapForI18n(recProductNode));
                recommendedProductContentMap.add(recProductContentMap);

            }

            return recommendedProductContentMap;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    protected String getProductUrl(Node categoryNode, Node productNode, String channel, String language) throws RepositoryException {
        final String catId = categoryNode.getName();
        final String catUrlPath = PropertyUtil.getString(categoryNode, CMSProductCategoriesProperties.GeneratedUrlPath.getPropertyName());
        final String prodId = productNode.getName();
        final String prodName = getConvertedProductName(PropertyUtil.getString(productNode, "name"));

        StringBuilder sb = new StringBuilder();
        sb.append("/");
        sb.append(language);
        sb.append("/");
        sb.append(channel);
        sb.append("/");
        sb.append("category");
        sb.append("/");
        sb.append("product");
        sb.append("~");
        sb.append(prodName);
        sb.append("~");
        sb.append(language);
        sb.append("~");
        sb.append(catId);
        sb.append("~");
        sb.append(prodId);
        sb.append("~.html?category=");
        sb.append(catUrlPath);

        return sb.toString();
    }

    protected String getConvertedProductName(String productName) {
        if (StringUtils.isNotBlank(productName)) {
            return productName.replace(' ', '-').toLowerCase();
        }
        return "";
    }

    public boolean hasActivePromoCode(Node productNode) {
        Node relatedAXProductParentNode = null;

        try {
            if (productNode.hasNode(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                relatedAXProductParentNode = productNode.getNode(CMSProductProperties.RelatedAXProducts.getPropertyName());
            } else {
                return false;
            }

            if (relatedAXProductParentNode == null) {
                return false;
            }

            List<Node> relatedAxProducts = NodeUtil.asList(NodeUtil.getNodes(relatedAXProductParentNode));
            for (Node axProductUUID : relatedAxProducts) {
                if (axProductUUID.hasProperty(CMSProductProperties.RelatedAXProducts.getPropertyName())) {
                    String uuid = axProductUUID.getProperty(CMSProductProperties.RelatedAXProducts.getPropertyName()).getString();
                    Node axProduct = this.wrapForI18n(NodeUtil.getNodeByIdentifier(JcrWorkspace.AXProducts.getWorkspaceName(), uuid));
                    if (AXProductStatus.Active.name().equals(axProduct.getProperty(AXProductProperties.Status.getPropertyName()).getString())) {
                        List<Node> promotions = getAXProductPromotions(axProduct, true);
                        if (promotions != null) {
                            for (Node promotion : promotions) {
                                if (AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName())) &&
                                    promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                                    NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                                    while (discountCodes.hasNext()) {
                                        final Node discountCode = discountCodes.nextNode();
                                        if (AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return false;
    }


    public String getDefaultMerchant(String channel) {
        try {
            List<Node> defaultMerchant = NodeUtil.asList(JcrRepository.query(JcrWorkspace.MerchantDefault.getWorkspaceName(), JcrWorkspace.MerchantDefault.getNodeType(), "/jcr:root/" + channel + "//element(*, " + JcrWorkspace.MerchantDefault.getNodeType() + ")"));

            if (defaultMerchant != null && defaultMerchant.size() > 0) {
                return PropertyUtil.getString(defaultMerchant.get(0), "merchantId");
            }


        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return "";
    }

    //TODO found out how to search by dates loh
    public boolean isMaintenanceMode(String channel) {
        try {
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());

            List<Node> schedules = NodeUtil.asList(JcrRepository.query(JcrWorkspace.MaintenanceSchedule.getWorkspaceName(),
                JcrWorkspace.MaintenanceSchedule.getNodeType(),
                "/jcr:root/" + channel + "//element(*, " + JcrWorkspace.MaintenanceSchedule.getNodeType() + ")"));

            for (Node sched : schedules) {
                if (PropertyUtil.getDate(sched, "startDate").compareTo(now) <= 0 &&
                    PropertyUtil.getDate(sched, "endDate").compareTo(now) > 0) {
                    return true;
                }
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return false;
    }

    public Node getMaintenanceMessage(String channel) {
        try {
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());

            List<Node> schedules = NodeUtil.asList(JcrRepository.query(JcrWorkspace.MaintenanceSchedule.getWorkspaceName(),
                JcrWorkspace.MaintenanceSchedule.getNodeType(),
                "/jcr:root/" + channel + "//element(*, " + JcrWorkspace.MaintenanceSchedule.getNodeType() + ")"));

            for (Node sched : schedules) {
                if (PropertyUtil.getDate(sched, "startDate").compareTo(now) <= 0 &&
                    PropertyUtil.getDate(sched, "endDate").compareTo(now) > 0) {
                    return wrapForI18n(sched);
                }
            }

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<ContentMap> getAnnouncements(String channel) {
        try {
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MILLISECOND, 0);


            List<Node> announcements = NodeUtil.asList(JcrRepository.query(JcrWorkspace.Announcement.getWorkspaceName(),
                JcrWorkspace.Announcement.getNodeType(),
                "/jcr:root/" + channel + "//element(*, " + JcrWorkspace.Announcement.getNodeType() + ")"));
            List<Node> availableAnnouncements = new ArrayList<>();

            for (Node announcement : announcements) {
                if (PropertyUtil.getDate(announcement, "validFrom").compareTo(today) <= 0 &&
                    PropertyUtil.getDate(announcement, "validTo").compareTo(today) >= 0) {
                    availableAnnouncements.add(this.wrapForI18n(announcement));
                }
            }

            return asContentMapList(availableAnnouncements);

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Node> getPromoSearchProducts(Node category, String channel, String promoCode) {
        List<Node> results = new ArrayList<>();
        if (StringUtils.isBlank(promoCode)) {
            return results;
        }
        try {
            List<Node> cmsProducts = getCMSProductByCategory(channel, category.getName());
            if (cmsProducts != null) {
                for (Node cmsProduct : cmsProducts) {
                    boolean hasMatchingPromoCode = cmsProductHasPromoCode(cmsProduct, promoCode);
                    if (hasMatchingPromoCode) {
                        results.add(cmsProduct);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Unable to get promotion search categories.", e);
        }
        return results;
    }

    private boolean cmsProductHasPromoCode(Node cmsProduct, String promoCode) throws RepositoryException {
        boolean hasMatchingPromoCode = false;
        Map<String, Node> axProducts = getAXProductsDetailByCMSProductNode(cmsProduct, true);
        for (Node axProduct : axProducts.values()) {
            List<Node> promotions = getAXProductPromotions(axProduct, true);
            if (promotions != null) {
                for (Node promotion : promotions) {
                    if (AxProductPromotionStatus.Active.name().equals(PropertyUtil.getString(promotion, AXProductPromotionProperties.SystemStatus.getPropertyName())) &&
                        PropertyUtil.getBoolean(promotion, AXProductPromotionProperties.UsedAsUnlocker.getPropertyName(), false) &&
                        promotion.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
                        NodeIterator discountCodes = promotion.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName()).getNodes();
                        while (discountCodes.hasNext()) {
                            final Node discountCode = discountCodes.nextNode();
                            if (AxProductDiscountCodeStatus.Active.name().equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.SystemStatus.getPropertyName()))
                                && promoCode.equals(PropertyUtil.getString(discountCode, AXProductDiscountCodeProperties.Code.getPropertyName()))) {
                                hasMatchingPromoCode = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (hasMatchingPromoCode) {
                break;
            }
        }
        return hasMatchingPromoCode;
    }

    public String getSystemParamValueByKey(String appKey, String key) {
        if (StringUtils.isEmpty(appKey) || StringUtils.isEmpty(key)) {
            return null;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.AppConfigs.getWorkspaceName(), "/" + appKey +"/" + key.trim());
            if (node == null) {
                return null;
            }
            String str = PropertyUtil.getString(node, "value");
            if (!StringUtils.isEmpty(str)) {
                return str.trim();
            }
            return null;
        } catch (RepositoryException e) {
            log.error("App-Config [/" + appKey + "/" + key + "] not found : " + e.getMessage());
        }
        return null;
    }

    public boolean hasRights(String functionName) {

       if(MagnoliaConfigUtil.isAuthorInstance() && !MagnoliaConfigUtil.isDevMode()) {
            return true;
       }

       List<String> accessRights =  MgnlContext.getAttribute(PartnerPortalConst.SESSION_ATTR_LOGGED_IN_ACCESS_RIGHTS, Context.SESSION_SCOPE);
       if(accessRights.contains(functionName)) {
           return true;
       }
       return false;
    }
}
