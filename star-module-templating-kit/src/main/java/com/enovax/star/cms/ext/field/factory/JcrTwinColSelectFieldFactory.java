package com.enovax.star.cms.ext.field.factory;

import com.enovax.star.cms.ext.field.definition.JcrTwinColSelectFieldDefinition;
import com.vaadin.data.Item;
import info.magnolia.cms.util.QueryUtil;
import info.magnolia.jcr.iterator.FilteringPropertyIterator;
import info.magnolia.jcr.predicate.JCRMgnlPropertyHidingPredicate;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.SelectFieldOptionDefinition;
import info.magnolia.ui.form.field.factory.TwinColSelectFieldFactory;
import info.magnolia.ui.form.field.transformer.Transformer;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JcrTwinColSelectFieldFactory extends TwinColSelectFieldFactory<JcrTwinColSelectFieldDefinition> {

    private static class Code {
        public String key;
        public String name;
        public Code(String key, String name) {
            this.key = key;
            this.name = name;
        }
    }

    private static final Logger log = LoggerFactory.getLogger(JcrTwinColSelectFieldFactory.class);
    private ComponentProvider componentProvider;

    @Inject
    public JcrTwinColSelectFieldFactory(JcrTwinColSelectFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, ComponentProvider componentProvider) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, componentProvider);
        definition.setOptions(getSelectFieldOptionDefinition());
        this.componentProvider = componentProvider;
    }

    @Deprecated
    public JcrTwinColSelectFieldFactory(JcrTwinColSelectFieldDefinition definition, Item relatedFieldItem, ComponentProvider componentProvider) {
        this(definition, relatedFieldItem, null, componentProvider.getComponent(I18NAuthoringSupport.class), componentProvider);
    }

    @Override
    public List<SelectFieldOptionDefinition> getSelectFieldOptionDefinition() {
        List<SelectFieldOptionDefinition> options = new ArrayList<SelectFieldOptionDefinition>();
        List<Code> allOptionList = getOptionList();
        Set<String> selectedOptionList = getSelectedOptionList();
        for(Code cd : allOptionList){
            SelectFieldOptionDefinition option = new SelectFieldOptionDefinition();
            option.setLabel(cd.name);
            option.setValue(cd.key);
            if(selectedOptionList.contains(cd.key)){
                option.setSelected(true);
            }
            options.add(option);
        }
        return options;
    }

    private List<Code> getOptionList() {
        List<Code> list = new ArrayList<Code>();
        try{
            String keyFieldName = getFieldDefinition().getValueProperty();
            String labelFieldName = getFieldDefinition().getLabelProperty();
            NodeIterator ni = QueryUtil.search(getFieldDefinition().getRepository(), populateQuery());
            while(ni.hasNext()){
                Node n = ni.nextNode();
                if(n == null){
                    continue;
                }
                list.add(new Code(n.getProperty(keyFieldName).getString(), n.getProperty(labelFieldName).getString()));
            }
        }catch (Exception ex){
            log.error("Cannot read roles from the [ "+ (getFieldDefinition().getRepository()) +" ] workspace.", ex);
        }
        return list;
    }

    private String populateQuery() {
        String query = String.format("select * from [nt:base] as node where ISDESCENDANTNODE([%s]) and node.%s is not null and node.%s is not null",
                getFieldDefinition().getPath(),
                getFieldDefinition().getValueProperty(),
                getFieldDefinition().getLabelProperty());
        return query;
    }

    private Set<String> getSelectedOptionList(){
        Set<String> list = new HashSet<String>();
        try {
            Node mainNode = ((JcrNodeAdapter)item).getJcrItem();
            String fieldName = getFieldDefinition().getName();
            if(mainNode.hasNode(fieldName)){
                Node subNode = mainNode.getNode(fieldName);
                if(subNode == null){
                    return list;
                }
                for (PropertyIterator iter = new FilteringPropertyIterator(subNode.getProperties(), new JCRMgnlPropertyHidingPredicate()); iter.hasNext();) {
                    list.add(iter.nextProperty().getString());
                }
            }
        }catch (Exception ex){
            log.error("Reading current value from selected item failed", ex);
        }
        return list;
    }

    @Override
    protected Transformer<?> initializeTransformer(Class<? extends Transformer<?>> transformerClass) {
        return this.componentProvider.newInstance(transformerClass, item, definition, HashSet.class, getSelectedOptionList(), getFieldDefinition().getName());
    }
}
