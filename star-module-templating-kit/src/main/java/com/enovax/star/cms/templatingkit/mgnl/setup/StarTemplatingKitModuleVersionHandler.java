package com.enovax.star.cms.templatingkit.mgnl.setup;

import com.enovax.star.cms.templatingkit.mgnl.functions.SkyDiningTemplatingFunctions;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.Task;
import info.magnolia.rendering.module.setup.InstallRendererContextAttributeTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 4/4/16.
 */
public class StarTemplatingKitModuleVersionHandler extends DefaultModuleVersionHandler {
    @Override
    protected List<Task> getExtraInstallTasks(InstallContext installContext) {
        List<Task> extraInstallTasks = new ArrayList<Task>(super.getExtraInstallTasks(installContext));
        extraInstallTasks.addAll(getFunctionsInstallerTask());
        return extraInstallTasks;
    }

    private List<Task> getFunctionsInstallerTask() {
        List<Task> tasks = new ArrayList<>();
        tasks.add(new InstallRendererContextAttributeTask("rendering", "freemarker", StarTemplatingFunctions.functionName, StarTemplatingFunctions.class.getName()));
        tasks.add(new InstallRendererContextAttributeTask("rendering", "freemarker", StarKioskTemplatingFunctions.functionName, StarKioskTemplatingFunctions.class.getName()));
        tasks.add(new InstallRendererContextAttributeTask("rendering", "freemarker", SkyDiningTemplatingFunctions.functionName, SkyDiningTemplatingFunctions.class.getName()));
        return tasks;
    }
}
