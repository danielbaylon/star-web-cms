package com.enovax.star.cms.ext.field.definition;

import com.enovax.star.cms.ext.field.transformer.JcrTwinColSelectFieldTransformer;
import info.magnolia.ui.form.field.definition.TwinColSelectFieldDefinition;
import info.magnolia.ui.form.field.transformer.Transformer;

public class JcrTwinColSelectFieldDefinition extends TwinColSelectFieldDefinition {

    public JcrTwinColSelectFieldDefinition() {
        setTransformerClass((Class<? extends Transformer<?>>) (Object) JcrTwinColSelectFieldTransformer.class);
    }
}
