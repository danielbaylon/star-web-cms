package com.enovax.star.cms.templatingkit.mgnl.functions;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.datamodel.*;
import com.enovax.star.cms.skydining.repository.jcr.*;
import info.magnolia.cms.core.AggregationState;
import info.magnolia.cms.i18n.I18nContentSupport;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.rendering.template.type.TemplateTypeHelper;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;

/**
 * Created by jace on 10/8/16.
 */
public class SkyDiningTemplatingFunctions extends StarTemplatingFunctions {

    private static final Logger log = LoggerFactory.getLogger(SkyDiningTemplatingFunctions.class);

    public static final String functionName = "sdfn";

    protected ISkyDiningPackageRepository packageRepo = new DefaultSkyDiningPackageRepository();

    protected ISkyDiningSessionRepository sessionRepo = new DefaultSkyDiningSessionRepository();

    protected ISkyDiningDiscountRepository discountRepo = new DefaultSkyDiningDiscountRepository();

    protected ISkyDiningMenuRepository menuRepo = new DefaultSkyDiningMenuRepository();

    protected ISkyDiningMainCourseRepository mainCourseRepo = new DefaultSkyDiningMainCourseRepository();

    protected ISkyDiningThemeRepository themeRepo = new DefaultSkyDiningThemeRepository();

    protected ISkyDiningTopUpRepository topUpRepo = new DefaultSkyDiningTopUpRepository();

    @Inject
    public SkyDiningTemplatingFunctions(Provider<AggregationState> aggregationStateProvider, TemplateTypeHelper templateTypeFunctions,
                                        Provider<I18nContentSupport> i18nContentSupport) {
        super(aggregationStateProvider, templateTypeFunctions, i18nContentSupport);
    }

    private List<SkyDiningPackage> findForSalePackages(boolean sort) {
        Date now = new Date();
        Set<Integer> packageIds = new HashSet<>();
        List<SkyDiningPackage> forSalePackages = new ArrayList<>();
        List<SkyDiningSession> sessions = sessionRepo.findByProperty("active", true);
        for (SkyDiningSession session : sessions) {
            if (NvxDateUtils.inDateRange(session.getSalesStartDate(), session.getSalesEndDate(), now, null, true, true)) {
                List<SkyDiningPackage> packages = session.getPackages();
                if (packages != null) {
                    for (SkyDiningPackage sdPackage : packages) {
                        if (!packageIds.contains(sdPackage.getId()) &&
                            NvxDateUtils.inDateRange(sdPackage.getStartDate(), sdPackage.getEndDate(), now, sdPackage.getSalesCutOffDays(), true, true)) {
                            packageIds.add(sdPackage.getId());
                            forSalePackages.add(sdPackage);
                        }
                    }
                }
            }
        }
        if (sort) {
            Collections.sort(forSalePackages, new Comparator<SkyDiningPackage>() {
                @Override
                public int compare(SkyDiningPackage o1, SkyDiningPackage o2) {
                    return (o1.getDisplayOrder() == null ? 0 : o1.getDisplayOrder()) - (o2.getDisplayOrder() == null ? 0 : o2.getDisplayOrder());
                }
            });
        }
        return forSalePackages;
    }

    public List<ContentMap> getSkyDiningPackages() {
        List<SkyDiningPackage> packages = findForSalePackages(true);
        if (packages != null && !packages.isEmpty()) {
            List<ContentMap> packageMaps = new ArrayList<>();
            for (SkyDiningPackage sdPackage : packages) {
                ContentMap packageMap = packageRepo.findContent(sdPackage.getId());
                packageMaps.add(packageMap);
            }
            return packageMaps;
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public List<ContentMap> getPromotions(String packageId, String promotionType) {
        List<ContentMap> promotions = new ArrayList<>();
        SkyDiningPackage sdPackage = packageRepo.find(packageId);
        if (sdPackage != null) {
            List<SkyDiningDiscount> discounts = sdPackage.getDiscounts();
            if (discounts != null && !discounts.isEmpty()) {
                Date now = new Date();
                for (SkyDiningDiscount discount : discounts) {
                    if (BooleanUtils.isTrue(discount.getActive() &&
                        NvxDateUtils.inDateRangeFlexible(discount.getValidFrom(), discount.getValidTo(), now, true, true) &&
                        StringUtils.equals(discount.getType(), promotionType) &&
                        discount.getMenus() != null && !discount.getMenus().isEmpty())) {
                        promotions.add(discountRepo.findContent(discount.getId()));
                    }
                }
            }
        }
        return promotions;
    }

    public List<ContentMap> getMainCourses(String menuId) {
        List<ContentMap> mcMaps = new ArrayList<>();
        SkyDiningMenu menu = menuRepo.find(menuId);
        if (menu != null) {
            List<SkyDiningMainCourse> mcs = menu.getMainCourses();
            if (mcs != null && !mcs.isEmpty()) {
                for (SkyDiningMainCourse mc : mcs) {
                    mcMaps.add(mainCourseRepo.findContent(mc.getId()));
                }
            }
        }
        return mcMaps;
    }

    public boolean isEarlyBirdPeriod(Date earlyBirdEndDate) {
        if (earlyBirdEndDate != null) {
            return NvxDateUtils.inDateRangeFlexible(null, earlyBirdEndDate, new Date(), true, true);
        }
        return false;
    }

    public List<ContentMap> getMenus(String packageId) {
        List<ContentMap> menuMaps = new ArrayList<>();
        SkyDiningPackage sdPackage = packageRepo.find(packageId);
        if (sdPackage != null && sdPackage.getMenus() != null) {
            Date now = new Date();
            for (SkyDiningMenu menu : sdPackage.getMenus()) {
                if (NvxDateUtils.inDateRange(menu.getStartDate(), menu.getEndDate(), now, menu.getSalesCutOffDays(), true, true)) {
                    menuMaps.add(menuRepo.findContent(menu.getId()));
                }
            }
        }

        return menuMaps;
    }

    public List<ContentMap> getPromoMenus(String promoId) {
        List<ContentMap> menuMaps = new ArrayList<>();

        SkyDiningDiscount discount = discountRepo.find(promoId);
        if (discount != null && discount.getMenus() != null) {
            Date now = new Date();
            for (SkyDiningMenu menu : discount.getMenus()) {
                if (NvxDateUtils.inDateRange(menu.getStartDate(), menu.getEndDate(), now, menu.getSalesCutOffDays(), true, true)) {
                    menuMaps.add(menuRepo.findContent(menu.getId()));
                }
            }
        }

        return menuMaps;
    }

    public List<ContentMap> getThemes(String packageId) {
        List<ContentMap> menuMaps = new ArrayList<>();
        SkyDiningPackage sdPackage = packageRepo.find(packageId);
        if (sdPackage != null) {
            List<SkyDiningTheme> themes = sdPackage.getThemes();
            if (themes != null && !themes.isEmpty()) {
                for (SkyDiningTheme theme : themes) {
                    menuMaps.add(themeRepo.findContent(theme.getId()));
                }
            }
        }
        return menuMaps;
    }

    public List<ContentMap> getTopUps(String packageId) {
        List<ContentMap> menuMaps = new ArrayList<>();
        SkyDiningPackage sdPackage = packageRepo.find(packageId);
        if (sdPackage != null) {
            List<SkyDiningTopUp> topUps = sdPackage.getTopUps();
            if (topUps != null && !topUps.isEmpty()) {
                for (SkyDiningTopUp topUp : topUps) {
                    menuMaps.add(topUpRepo.findContent(topUp.getId()));
                }
            }
        }
        return menuMaps;
    }

    public List<ContentMap> getPromotionMenus(String packageId, String promotionId) {
        return null;
    }

    public ContentMap getSkyDiningPackage(String packageId) {
        return packageRepo.findContent(packageId);
    }

    public boolean hasAvailablePackages() {
        return !findForSalePackages(false).isEmpty();
    }

    public ContentMap getFirstAvailablePackage() {
        List<SkyDiningPackage> packages = findForSalePackages(true);
        if (packages != null && !packages.isEmpty()) {
            return packageRepo.findContent(packages.get(0).getId());
        } else {
            return null;
        }
    }

    public String formatPrice(Integer priceInCents) {
        return NvxNumberUtils.formatToCurrency(NvxNumberUtils.centsToMoney(priceInCents));
    }

    public ContentMap getPackageTncContent(String packageId) {
        ContentMap tncMap = packageRepo.getPackageTnc(packageId);
        return tncMap;
    }

    public List<ContentMap> getPackageTncContent(List<String> packageIds) {
        if (packageIds != null && packageIds.size() > 0) {
            List<ContentMap> tncMaps = new ArrayList<>();
            for (String packageId : packageIds) {
                ContentMap tncMap = getPackageTncContent(packageId);
                if (tncMap != null) {
                    tncMaps.add(tncMap);
                }
            }
            return tncMaps;
        }
        return null;
    }

    public Integer getDiscountedPrice(Integer originalPrice, Integer priceType, Integer discountPrice) {
        if (priceType != null && discountPrice != null) {
            switch (priceType) {
                case 1:
                    originalPrice = NvxNumberUtils.percentOffInCents(originalPrice,
                        discountPrice);
                    break;
                case 2:
                    originalPrice = originalPrice - discountPrice;
                    break;
                case 3:
                    originalPrice = discountPrice;
                    break;
                default:
                    break;
            }
        }
        return originalPrice;
    }
}
