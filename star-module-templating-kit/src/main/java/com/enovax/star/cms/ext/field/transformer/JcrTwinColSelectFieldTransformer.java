package com.enovax.star.cms.ext.field.transformer;

import com.vaadin.data.Item;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.wrapper.JCRMgnlPropertiesFilteringNodeWrapper;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.basic.BasicTransformer;
import info.magnolia.ui.vaadin.integration.jcr.DefaultProperty;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import java.util.*;

public class JcrTwinColSelectFieldTransformer<T> extends BasicTransformer<T> {

    private Logger log = LoggerFactory.getLogger(JcrTwinColSelectFieldTransformer.class);

    private String childNodeType = NodeTypes.ContentNode.NAME;
    private Set<String> assignedEntity;
    private String entityName;

    @Inject
    public JcrTwinColSelectFieldTransformer(Item parent, ConfiguredFieldDefinition definition, Class<?> type, Set<String> assignedEntity, String entityName) {
        super(parent, definition, (Class<T>) type);
        this.assignedEntity = assignedEntity;
        this.entityName = entityName;
    }

    @Override
    public void writeToItem(T newValue) {
        if (newValue == null) {
            newValue = (T) new LinkedHashSet();
        }
        LinkedHashSet<String> list = (LinkedHashSet<String>) newValue;
        writeToRepo(list);
    }

    private void writeToRepo(LinkedHashSet<String> list) {
        // i18n support
        String childNodeName = definePropertyName();
        try {
            // get the child item
            JcrNodeAdapter child = getOrCreateChildItem((JcrNodeAdapter) relatedFormItem, childNodeName);
            // Remove all old properties
            for (Object id : child.getItemPropertyIds()) {
                if(id != null){
                    child.removeItemProperty(id);
                }
            }
            // add all the new properties
            if (list != null && list.size() > 0) {
                Iterator<String> it = list.iterator();
                int index = 0;
                DefaultProperty prop = null;
                while (it.hasNext()) {
                    prop = new DefaultProperty(it.next());
                    child.addItemProperty(index++, prop);
                }
            }
        } catch (RepositoryException re) {
            log.warn("Not able to access the child node of '{}'", NodeUtil.getName(((JcrNodeAdapter) relatedFormItem).getJcrItem()));
        }
    }

    private JcrNodeAdapter getOrCreateChildItem(JcrNodeAdapter parent, String childNodeName) throws RepositoryException {
        JcrNodeAdapter child = null;
        Node rootNode = parent.getJcrItem();
        if (rootNode.hasNode(childNodeName)) {
            child = new JcrNodeAdapter(rootNode.getNode(childNodeName));
            Node childNode = new JCRMgnlPropertiesFilteringNodeWrapper(rootNode.getNode(childNodeName));
            PropertyIterator iterator = childNode.getProperties();
            while (iterator.hasNext()) {
                child.getItemProperty(iterator.nextProperty().getName());
            }
        } else {
            child = new JcrNewNodeAdapter(rootNode, childNodeType, childNodeName);
        }
        parent.addChild(child);
        return child;
    }

    @Override
    public T readFromItem() {
        LinkedHashSet<String> list = readFromRepo();
        return (T)list;
    }

    private LinkedHashSet<String> readFromRepo() {
        LinkedHashSet<String> list = new LinkedHashSet<String>();
        String childNodeName = definePropertyName();
        try {
            JcrNodeAdapter child = getOrCreateChildItem((JcrNodeAdapter) relatedFormItem, childNodeName);
            if (!(child instanceof JcrNewNodeAdapter)) {
                List<Object> ids = new ArrayList<Object>(child.getItemPropertyIds());
                if(ids != null && ids.size() > 0){
                    Iterator<Object> iter = ids.iterator();
                    while(iter.hasNext()){
                        com.vaadin.data.Property prop = child.getItemProperty(iter.next());
                        if(prop != null && prop.getValue() != null){
                            String value = prop.getValue().toString();
                            if(!list.contains(value.trim())){
                                list.add(value.trim());
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException re) {
            log.warn("Not able to access the child node of '{}'", ((JcrNodeAdapter) relatedFormItem).getNodeName());
        }
        return list;
    }
}
