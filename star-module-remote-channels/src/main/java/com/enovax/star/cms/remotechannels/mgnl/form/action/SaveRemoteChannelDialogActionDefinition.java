package com.enovax.star.cms.remotechannels.mgnl.form.action;

import info.magnolia.ui.admincentral.dialog.action.SaveDialogActionDefinition;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class SaveRemoteChannelDialogActionDefinition extends SaveDialogActionDefinition {

    public SaveRemoteChannelDialogActionDefinition() {
        setImplementationClass(SaveRemoteChannelDialogAction.class);
    }
}
