package com.enovax.star.cms.remotechannels.mgnl.form.action;

import com.enovax.star.cms.remotechannels.mgnl.util.RemoteChannelUtils;
import com.vaadin.data.Item;
import info.magnolia.ui.admincentral.dialog.action.SaveDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class SaveRemoteChannelDialogAction extends SaveDialogAction<SaveRemoteChannelDialogActionDefinition> {

    protected final Logger log = LoggerFactory.getLogger(SaveRemoteChannelDialogAction.class);

    public SaveRemoteChannelDialogAction(SaveRemoteChannelDialogActionDefinition definition, Item item, EditorValidator validator, EditorCallback callback) {
        super(definition, item, validator, callback);
    }

    @Override
    public void execute() throws ActionExecutionException {
        this.validator.showValidation(true);
        if(this.validator.isValid()) {
            JcrNodeAdapter itemChanged = (JcrNodeAdapter)this.item;

            try {
                Node e = itemChanged.applyChanges();
                this.setNodeName(e, itemChanged);

                Property folderName = e.getProperty("folderName");

                RemoteChannelUtils.createChannel(folderName.getString());

                e.getSession().save();
            } catch (RepositoryException var3) {
                throw new ActionExecutionException(var3);
            }

            this.callback.onSuccess(getDefinition().getName());
        } else {
            log.info("Validation error(s) occurred. No save performed.");
        }

    }
}
