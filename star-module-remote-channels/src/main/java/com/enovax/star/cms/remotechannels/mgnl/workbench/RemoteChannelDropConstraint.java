package com.enovax.star.cms.remotechannels.mgnl.workbench;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.vaadin.data.Item;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.workbench.tree.drop.BaseDropConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class RemoteChannelDropConstraint extends BaseDropConstraint {

    protected final Logger log = LoggerFactory.getLogger(RemoteChannelDropConstraint.class);

    private final String nodeType;

    public RemoteChannelDropConstraint() {
        super(JcrWorkspace.RemoteChannels.getNodeType());
        this.nodeType = JcrWorkspace.RemoteChannels.getNodeType();
    }

    @Override
    public boolean allowedAsChild(Item sourceItem, Item targetItem) {
        try {
            JcrNodeAdapter e = (JcrNodeAdapter)sourceItem;
            JcrNodeAdapter target = (JcrNodeAdapter)targetItem;
            String sourceNodeType = e.applyChanges().getPrimaryNodeType().getName();
            String targetNodeType = target.applyChanges().getPrimaryNodeType().getName();
            if(this.nodeType.equals(sourceNodeType) && this.nodeType.equals(targetNodeType)) {
                log.debug("Could not move a node type \'{}\' under a node type \'{}\'", targetNodeType, this.nodeType);
                return false;
            }

        } catch (RepositoryException var8) {
            log.warn("Could not check if child is allowed. ", var8);
        }

        return true;
    }

    @Override
    public boolean allowedBefore(Item sourceItem, Item targetItem) {
        return true;
    }

    @Override
    public boolean allowedAfter(Item sourceItem, Item targetItem) {
        return true;
    }

    @Override
    public boolean allowedToMove(Item sourceItem) {
        return true;
    }
}
