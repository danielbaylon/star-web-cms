package com.enovax.star.cms.remotechannels.mgnl.util;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;

import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 3/18/16.
 */
public class RemoteChannelUtils {

    public static final String CMS_PRODUCTS_FOLDER = "/cms-products/";
    public static final String AX_PRODUCTS_FOLDER = "/ax-products/";

    //TODO how to inject

    public static void createChannel(String channelName) throws RepositoryException {
        //create folder to "cms-categories"
        JcrRepository.createNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/", channelName,JcrWorkspace.RemoteChannelsFolder.getNodeType());

        //create forlder to "dam"
        JcrRepository.createNode(JcrWorkspace.Dam.getWorkspaceName(), "/", channelName, JcrWorkspace.RemoteChannelsFolder.getNodeType());

        //create folder to resources
        JcrRepository.createNode(JcrWorkspace.Resources.getWorkspaceName(), "/", channelName, JcrWorkspace.RemoteChannelsFolder.getNodeType());

        //create folder to cms-products/cms-products
        JcrRepository.createNode(JcrWorkspace.CMSProducts.getWorkspaceName(), CMS_PRODUCTS_FOLDER, channelName,JcrWorkspace.RemoteChannelsFolder.getNodeType());

        JcrRepository.createNode(JcrWorkspace.AXProducts.getWorkspaceName(), AX_PRODUCTS_FOLDER, channelName, JcrWorkspace.RemoteChannelsFolder.getNodeType());

    }
}
