package com.enovax.star.cms.cmsapi.model;

import java.util.Date;

/**
 * Created by jennylynsze on 11/1/16.
 */
public class AxSyncLogs implements Comparable<AxSyncLogs> {
    private String axSyncLogNodeName;
    private Date axProductSyncDate;
    private String axProductSyncDateStr;
    private String axProductSyncLog;

    public String getAxSyncLogNodeName() {
        return axSyncLogNodeName;
    }

    public void setAxSyncLogNodeName(String axSyncLogNodeName) {
        this.axSyncLogNodeName = axSyncLogNodeName;
    }

    public Date getAxProductSyncDate() {
        return axProductSyncDate;
    }

    public void setAxProductSyncDate(Date axProductSyncDate) {
        this.axProductSyncDate = axProductSyncDate;
    }

    public String getAxProductSyncLog() {
        return axProductSyncLog;
    }

    public void setAxProductSyncLog(String axProductSyncLog) {
        this.axProductSyncLog = axProductSyncLog;
    }

    public String getAxProductSyncDateStr() {
        return axProductSyncDateStr;
    }

    public void setAxProductSyncDateStr(String axProductSyncDateStr) {
        this.axProductSyncDateStr = axProductSyncDateStr;
    }

    @Override
    public int compareTo(AxSyncLogs o) {
        return this.axProductSyncDate.compareTo(o.axProductSyncDate);
    }
}
