package com.enovax.star.cms.cmsapi.model;

import java.util.Date;

/**
 * Created by jennylynsze on 11/1/16.
 */
public class AxDailySyncDates implements Comparable<AxDailySyncDates> {
    private String syncDateStr;
    private Date syncDate;
    private String syncDateNodeName;

    public String getSyncDateStr() {
        return syncDateStr;
    }

    public void setSyncDateStr(String syncDateStr) {
        this.syncDateStr = syncDateStr;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }

    public String getSyncDateNodeName() {
        return syncDateNodeName;
    }

    public void setSyncDateNodeName(String syncDateNodeName) {
        this.syncDateNodeName = syncDateNodeName;
    }

    @Override
    public int compareTo(AxDailySyncDates o) {
        return this.syncDate.compareTo(o.syncDate);
    }
}
