package com.enovax.star.cms.cmsapi.controller;

import com.enovax.star.cms.cmsapi.model.AxDailySyncDates;
import com.enovax.star.cms.cmsapi.model.AxSyncLogs;
import com.enovax.star.cms.cmsapi.service.IAXScheduledSyncService;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jennylynsze on 10/30/16.
 */
@Controller
@RequestMapping("/scheduled-sync/")
public class AXScheduledSyncApiController extends BaseCmsApiController {

    @Autowired
    IAXScheduledSyncService scheduledSyncService;

    //get the sync config
    @RequestMapping(value = "get-sync-config", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiGetSyncConfig(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetSyncConfig...");
        try {
            String syncOption = scheduledSyncService.getSyncConfig(channelCode);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", syncOption), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetAllSyncDailySchedules] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }


    //save the sync config
    @RequestMapping(value = "save-sync-config/{option}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiSaveSyncConfig(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @PathVariable("option") String syncOption) {
        log.info("Entered apiSaveSyncConfig...");
        try {
            boolean success = scheduledSyncService.saveSyncConfig(channelCode, syncOption);
            return new ResponseEntity<>(new ApiResult<>(success, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSyncConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-daily-sync-dates", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List<AxDailySyncDates>>> apiGetDailySyncDates(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetDailySyncDates...");
        try {
            List<AxDailySyncDates> syncDates = scheduledSyncService.getDailySyncDates(channelCode);
            ApiResult res = new ApiResult<>(true, "", "", syncDates);
            res.setTotal(syncDates.size());

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetDailySyncDates] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, AxDailySyncDates.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-daily-sync-date", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiAddDailySyncDate(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @RequestBody AxDailySyncDates syncDate) {
        log.info("Entered apiSaveSyncConfig...");
        try {
            boolean success = scheduledSyncService.addDailySyncDate(channelCode, syncDate.getSyncDate());
            return new ResponseEntity<>(new ApiResult<>(success, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSyncConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-daily-sync-date/{id}", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiRemoveDailySyncDate(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @PathVariable("id") String syncDateNodeName) {
        log.info("Entered apiSaveSyncConfig...");
        try {
            boolean success = scheduledSyncService.removeDailySyncDate(channelCode, syncDateNodeName);
            return new ResponseEntity<>(new ApiResult<>(success, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSyncConfig] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-sync-logs", method = {RequestMethod.GET})
         public ResponseEntity<ApiResult<List<AxSyncLogs>>> apiGetSyncLogs(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiSaveSyncConfig...");
        try {
            List<AxSyncLogs> syncLogs = scheduledSyncService.getLogs(channelCode);
            ApiResult res = new ApiResult<>(true, "", "", syncLogs);
            res.setTotal(syncLogs.size());
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiSaveSyncConfig] !!!");
            return new ResponseEntity<>(handleUncaughtExceptionForList(e, AxSyncLogs.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-sync-log/{id}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<AxSyncLogs>> apiGetSyncLog(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @PathVariable("id") String syncLogNodeName) {
        log.info("Entered apiGetSyncLog...");
        try {
            AxSyncLogs log = scheduledSyncService.getLog(channelCode, syncLogNodeName);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", log), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [ apiGetSyncLog] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxSyncLogs.class, ""), HttpStatus.OK);
        }
    }

    //get all the sync daily schedules
//    @RequestMapping(value = "get-all-sync-daily-schedules", method = {RequestMethod.GET})
//    public ResponseEntity<ApiResult<List<ProductTierVM>>> apiGetAllSyncDailySchedules() {
//        log.info("Entered apiGetAllTiers...");
//        try {
//            List<ProductTierVM> tierVms = new ArrayList<>();
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", tierVms), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiGetAllSyncDailySchedules] !!!");
//            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ProductTierVM.class, ""), HttpStatus.OK);
//        }
//    }
//
//    //add the schedule
//    @RequestMapping(value = "get-all-sync-daily-schedules", method = {RequestMethod.GET})
//    public ResponseEntity<ApiResult<List<ProductTierVM>>> apiAddSyncDailySchedules() {
//        log.info("Entered apiGetAllTiers...");
//        try {
//            List<ProductTierVM> tierVms = new ArrayList<>();
//            return new ResponseEntity<>(new ApiResult<>(true, "", "", tierVms), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("!!! System exception encountered [apiGetAllSyncDailySchedules] !!!");
//            return new ResponseEntity<>(handleUncaughtExceptionForList(e, ProductTierVM.class, ""), HttpStatus.OK);
//        }
//    }



    //remove the schedule


    //get the logs (need pagination)


    // view the log
}
