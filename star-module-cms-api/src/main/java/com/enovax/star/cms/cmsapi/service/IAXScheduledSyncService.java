package com.enovax.star.cms.cmsapi.service;

import com.enovax.star.cms.cmsapi.model.AxDailySyncDates;
import com.enovax.star.cms.cmsapi.model.AxSyncLogs;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 10/31/16.
 */
public interface IAXScheduledSyncService {
    String getSyncConfig(String channel);
    boolean saveSyncConfig(String channel, String syncOption);

    //daily sync dates
    List<AxDailySyncDates> getDailySyncDates(String channel);
    boolean addDailySyncDate(String channel, Date syncDate);
    boolean removeDailySyncDate(String channel, String syncDateNodeName);

    //logs
    List<AxSyncLogs> getLogs(String channel);
    AxSyncLogs getLog(String channel, String syncLogNodeName);

}
