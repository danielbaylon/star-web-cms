package com.enovax.star.cms.cmsapi.config;

import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.objectfactory.Components;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * Created by jennylynsze on 10/30/16.
 */
@Configuration
@ComponentScan(
        basePackages = {"com.enovax.star.cms.commons", "com.enovax.star.cms.cmsapi"},
        excludeFilters = {
                @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
                @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION)
        }
)
public class StarModuleCmsApiAppConfig {

    protected Logger log = LoggerFactory.getLogger(StarModuleCmsApiAppConfig.class);

    @Bean
    public StarTemplatingFunctions starfn() { return Components.getComponent(StarTemplatingFunctions.class); }

    @Bean
    public DamTemplatingFunctions damfn() { return Components.getComponent(DamTemplatingFunctions.class); }


}

