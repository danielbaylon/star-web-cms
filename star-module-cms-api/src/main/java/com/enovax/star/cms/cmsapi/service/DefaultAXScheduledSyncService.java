package com.enovax.star.cms.cmsapi.service;

import com.enovax.star.cms.cmsapi.model.AxDailySyncDates;
import com.enovax.star.cms.cmsapi.model.AxSyncLogs;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.AXSyncConfigProperties;
import com.enovax.star.cms.commons.mgnl.definition.AXSyncLogsProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.qom.Constraint;
import javax.jcr.query.qom.Ordering;
import javax.jcr.query.qom.QueryObjectModelFactory;
import javax.jcr.query.qom.Source;
import java.util.*;

/**
 * Created by jennylynsze on 10/31/16.
 */
@Service
public class DefaultAXScheduledSyncService implements  IAXScheduledSyncService {

    public static final String SYNC_CONFIG_PATH = "/scheduled-sync";
    public static final String DAILY_SYNC_PATH = SYNC_CONFIG_PATH + "/" + "dailySync";

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String getSyncConfig(String channel) {
        try {
           Node syncConfig =  JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel  + SYNC_CONFIG_PATH);

           String syncOption = PropertyUtil.getString(syncConfig, AXSyncConfigProperties.SyncOption.getPropertyName());

           return syncOption;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return "";
    }

    @Override
    public boolean saveSyncConfig(String channel, String syncOption) {
        try {
            Node syncConfig =  JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel  + SYNC_CONFIG_PATH);

            syncConfig.setProperty(AXSyncConfigProperties.SyncOption.getPropertyName(), syncOption);

            if("noSync".equals(syncOption)) {
                //remove all in the dailySync
                try {
                    Node dailySync =  JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel + DAILY_SYNC_PATH);
                    Iterable<Node> dailySyncDatesIterable = NodeUtil.getNodes(dailySync);
                    Iterator<Node> dailySyncDatesIterator = dailySyncDatesIterable.iterator();
                    while(dailySyncDatesIterator.hasNext()) {
                        dailySyncDatesIterator.next().remove();
                    }
                    dailySync.getSession().save();
                }catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

            }

            syncConfig.getSession().save();

            return true;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return false;
    }

    @Override
    public List<AxDailySyncDates> getDailySyncDates(String channel) {
        try {
            Node dailySync =  JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel + DAILY_SYNC_PATH);

            Iterable<Node> dailySyncDatesIterable = NodeUtil.getNodes(dailySync);
            Iterator<Node> dailySyncDatesIterator = dailySyncDatesIterable.iterator();

            List<AxDailySyncDates> axDailySyncDates = new ArrayList<>();

            while(dailySyncDatesIterator.hasNext()) {
                AxDailySyncDates syncDate = new AxDailySyncDates();
                Node dailySyncDate = dailySyncDatesIterator.next();
                String nodeName = dailySyncDate.getName();
                String nodeNameArr[] = nodeName.split("_");
                String HH = nodeNameArr[0];
                String mm = nodeNameArr[1];

                if(HH.length() == 1) {
                    HH = "0" + HH;
                }

                if(mm.length() == 1) {
                    mm = "0" + mm;
                }

                syncDate.setSyncDateNodeName(nodeName);

                try {
                    Date syncDateDate = NvxDateUtils.parseDate(HH + ":" + mm, "HH:mm");
                    syncDate.setSyncDate(syncDateDate);

                    String syncDateStr = NvxDateUtils.formatDate(syncDateDate, "hh:mm a");
                    syncDate.setSyncDateStr(syncDateStr);

                    axDailySyncDates.add(syncDate);

                }catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }

            Collections.sort(axDailySyncDates);

            return axDailySyncDates;

        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return new ArrayList<>();
    }

    @Override
    public boolean addDailySyncDate(String channel, Date syncDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(syncDate);
        int hh = cal.get(Calendar.HOUR);
        int mm = cal.get(Calendar.MINUTE);
        int ap = cal.get(Calendar.AM_PM);

        String syncDateNodeName = "";

        if(ap == Calendar.AM) {
            syncDateNodeName = hh + "_" + mm;
        }else {
            syncDateNodeName = (12 + hh) + "_" + mm;
        }

        //check if syncDateNodeName exists
        try {
            if(JcrRepository.nodeExists(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel + DAILY_SYNC_PATH + "/" + syncDateNodeName)) {
                return false;
            }

            JcrRepository.createNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel + DAILY_SYNC_PATH, syncDateNodeName, "mgnl:content");
            return true;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean removeDailySyncDate(String channel, String syncDateNodeName) {
        try {
            Node syncDateNode = JcrRepository.getParentNode(JcrWorkspace.AXProductSyncConfig.getWorkspaceName(), "/" + channel + DAILY_SYNC_PATH  +"/" + syncDateNodeName);
            syncDateNode.remove();
            syncDateNode.getSession().save();

            return true;

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<AxSyncLogs> getLogs(String channel) {
        try {
            final String selector = "product-sync-logs";
            QueryObjectModelFactory factory = JcrRepository.getQOMFactory(JcrWorkspace.AXSyncLogs.getWorkspaceName());
            Source source = factory.selector(JcrWorkspace.AXSyncLogs.getNodeType(), selector);
            Constraint constraint = factory.descendantNode(selector, "/ax-sync-logs/" + channel);
            Iterable<Node> parentNodeIterable =      JcrRepository.execute(factory,
                    JcrWorkspace.AXSyncLogs.getNodeType(),
                    source,
                    constraint,
                    new Ordering[]{
                            factory.descending(factory.propertyValue(selector, AXSyncLogsProperties.SyncDate.getPropertyName()))
                    },
                    null); //TODO how to set the limit ah

            Iterator<Node> parentNodeIterator = parentNodeIterable.iterator();
            List<AxSyncLogs> syncLogs = new ArrayList<>();

            while(parentNodeIterator.hasNext()) {
                AxSyncLogs syncLog = new AxSyncLogs();
                Node log = parentNodeIterator.next();
                Calendar syncDate = PropertyUtil.getDate(log, AXSyncLogsProperties.SyncDate.getPropertyName());
                syncLog.setAxProductSyncDate(syncDate.getTime());
                syncLog.setAxProductSyncDateStr(NvxDateUtils.formatDate(syncLog.getAxProductSyncDate(), NvxDateUtils.DEFAULT_DATE_TIME_FORMAT_DISPLAY_READABLE));
                syncLog.setAxSyncLogNodeName(log.getName());
                syncLog.setAxProductSyncLog(PropertyUtil.getString(log, AXSyncLogsProperties.SyncLog.getPropertyName()));

                syncLogs.add(syncLog);
            }

            return syncLogs;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return new ArrayList<>();
    }

    @Override
    public AxSyncLogs getLog(String channel, String syncLogNodeName) {
        try {
            Node log =  JcrRepository.getParentNode(JcrWorkspace.AXSyncLogs.getWorkspaceName(), "/ax-sync-logs/" + channel + "/" + syncLogNodeName);
            AxSyncLogs syncLog = new AxSyncLogs();
            Calendar syncDate = PropertyUtil.getDate(log, AXSyncLogsProperties.SyncDate.getPropertyName());
            syncLog.setAxProductSyncDate(syncDate.getTime());
            syncLog.setAxSyncLogNodeName(log.getName());
            syncLog.setAxProductSyncLog(PropertyUtil.getString(log, AXSyncLogsProperties.SyncLog.getPropertyName()));

            return syncLog;
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return new AxSyncLogs();
    }
}
