package com.enovax.star.cms.kiosk.api.store.param;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

public class ExecutionResponse<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4150273399910293504L;

	private T details;

	private T callbacks;

	private boolean result;

	private String approvalCode;

	private String cardTypeDescription;

	private String cardTypeValue;

	private String terminalId;

	private String merchantId;

	private String transactionStart;

	private String transactionEnd;

	private String type;

	private String receipt;

	private boolean signatureRequired;

	private List<ExecutionResult> results;

	public ExecutionResponse() {
	}

	public T getDetails() {

		return details;
	}

	public void setDetails(T details) {

		this.details = (T) new Gson().toJson(details);
	}

	public T getCallbacks() {

		return callbacks;
	}

	public void setCallbacks(T callbacks) {

		this.callbacks = (T) new Gson().toJson(callbacks);
	}

	public boolean isResult() {

		return result;
	}

	public void setResult(boolean result) {

		this.result = result;
	}

	public String getApprovalCode() {

		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {

		this.approvalCode = approvalCode;
	}

	public String getCardTypeDescription() {

		return cardTypeDescription;
	}

	public void setCardTypeDescription(String cardTypeDescription) {

		this.cardTypeDescription = cardTypeDescription;
	}

	public String getCardTypeValue() {

		return cardTypeValue;
	}

	public void setCardTypeValue(String cardTypeValue) {

		this.cardTypeValue = cardTypeValue;
	}

	public String getTerminalId() {

		return terminalId;
	}

	public void setTerminalId(String terminalId) {

		this.terminalId = terminalId;
	}

	public String getMerchantId() {

		return merchantId;
	}

	public void setMerchantId(String merchantId) {

		this.merchantId = merchantId;
	}

	public String getTransactionStart() {

		return transactionStart;
	}

	public void setTransactionStart(String transactionStart) {

		this.transactionStart = transactionStart;
	}

	public String getTransactionEnd() {

		return transactionEnd;
	}

	public void setTransactionEnd(String transactionEnd) {

		this.transactionEnd = transactionEnd;
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	public String getReceipt() {

		return receipt;
	}

	public void setReceipt(String receipt) {

		this.receipt = receipt;
	}

	public void setSignatureRequired(boolean signatureRequired) {

		this.signatureRequired = signatureRequired;
	}

	public boolean isSignatureRequired() {

		return signatureRequired;
	}


	public List<ExecutionResult> getResults() {
        return results;
    }

	public void setResults(List<ExecutionResult> results) {
        this.results = results;
    }
	
}
