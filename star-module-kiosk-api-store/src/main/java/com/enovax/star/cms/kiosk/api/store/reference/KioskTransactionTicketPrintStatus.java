package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskTransactionTicketPrintStatus {

    NOT_PRINTED("0", "NOT PRINTED"), //
    PRINTED_SUCCESS("1", "PRINTED SUCCESS"), //
    PRINTED_FAILED("2", "PRINTED FAILED"); //
    
	private final String code;
    public final String message;

    public static String getTransactionStatus(String code) {
        String transactionStatus = null;
        if (NOT_PRINTED.getCode().equals(code)) {
            transactionStatus = NOT_PRINTED.getCode();
        } else if (PRINTED_SUCCESS.getCode().equals(code)) {
            transactionStatus = PRINTED_SUCCESS.getCode();
        } else if (PRINTED_FAILED.getCode().equals(code)) {
            transactionStatus = PRINTED_FAILED.getCode();
        } 
        return transactionStatus;
    }

    private KioskTransactionTicketPrintStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return message;
    }
}
