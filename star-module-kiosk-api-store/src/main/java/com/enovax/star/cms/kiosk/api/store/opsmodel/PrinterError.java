package com.enovax.star.cms.kiosk.api.store.opsmodel;

public class PrinterError {
    private long errorCode;
    private String autoCutterError;
    private String ramError;
    private String eepromError;
    private String printHeadOverheated;
    private String irregularEject;
    private String voltageError;
    private String printHeadUp;
    private String coverUp;
    private String motorRunning;
    private String feedKeyPressed;
    private String printKeyPressed;
    private String noPaper;
    private String nearPaperEnd;
    private String paperJam;

    private boolean unknownError;

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getAutoCutterError() {
        return autoCutterError;
    }

    public void setAutoCutterError(String autoCutterError) {
        this.autoCutterError = autoCutterError;
    }

    public String getRamError() {
        return ramError;
    }

    public void setRamError(String ramError) {
        this.ramError = ramError;
    }

    public String getEepromError() {
        return eepromError;
    }

    public void setEepromError(String eepromError) {
        this.eepromError = eepromError;
    }

    public String getPrintHeadOverheated() {
        return printHeadOverheated;
    }

    public void setPrintHeadOverheated(String printHeadOverheated) {
        this.printHeadOverheated = printHeadOverheated;
    }

    public String getIrregularEject() {
        return irregularEject;
    }

    public void setIrregularEject(String irregularEject) {
        this.irregularEject = irregularEject;
    }

    public String getVoltageError() {
        return voltageError;
    }

    public void setVoltageError(String voltageError) {
        this.voltageError = voltageError;
    }

    public String getPrintHeadUp() {
        return printHeadUp;
    }

    public void setPrintHeadUp(String printHeadUp) {
        this.printHeadUp = printHeadUp;
    }

    public String getCoverUp() {
        return coverUp;
    }

    public void setCoverUp(String coverUp) {
        this.coverUp = coverUp;
    }

    public String getMotorRunning() {
        return motorRunning;
    }

    public void setMotorRunning(String motorRunning) {
        this.motorRunning = motorRunning;
    }

    public String getFeedKeyPressed() {
        return feedKeyPressed;
    }

    public void setFeedKeyPressed(String feedKeyPressed) {
        this.feedKeyPressed = feedKeyPressed;
    }

    public String getPrintKeyPressed() {
        return printKeyPressed;
    }

    public void setPrintKeyPressed(String printKeyPressed) {
        this.printKeyPressed = printKeyPressed;
    }

    public String getNoPaper() {
        return noPaper;
    }

    public void setNoPaper(String noPaper) {
        this.noPaper = noPaper;
    }

    public String getNearPaperEnd() {
        return nearPaperEnd;
    }

    public void setNearPaperEnd(String nearPaperEnd) {
        this.nearPaperEnd = nearPaperEnd;
    }

    public String getPaperJam() {
        return paperJam;
    }

    public void setPaperJam(String paperJam) {
        this.paperJam = paperJam;
    }

    public boolean isUnknownError() {
        return unknownError;
    }

    public void setUnknownError(boolean unknownError) {
        this.unknownError = unknownError;
    }
}
