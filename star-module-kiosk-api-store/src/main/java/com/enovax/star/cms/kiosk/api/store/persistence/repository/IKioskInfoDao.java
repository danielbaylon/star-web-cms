package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

@Repository
public interface IKioskInfoDao extends JpaRepository<KioskInfoEntity, Long> {

    @Query("SELECT o FROM KioskInfoEntity o WHERE o.jcrNodeUuid = :jcrNodeUuid")
    public KioskInfoEntity getByJcrNodeUuid(@Param("jcrNodeUuid") String jcrNodeUuid);

}