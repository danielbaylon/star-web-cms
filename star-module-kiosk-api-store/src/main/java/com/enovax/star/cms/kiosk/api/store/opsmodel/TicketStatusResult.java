package com.enovax.star.cms.kiosk.api.store.opsmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/3/16.
 */
public class TicketStatusResult implements Serializable {

    private Integer status;
    private String errorCode;
    private String errorMessage;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
