package com.enovax.star.cms.kiosk.api.store.service.arm;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixRoboticArmService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class DefaultKioskRoboticArmService implements IKioskRoboticArmService {

    private static final Logger log = Logger.getLogger(DefaultKioskRoboticArmService.class);

    @Value("${kiosk.api.arm.service.url}")
    private String serviceURL;

    private RestTemplate template = new RestTemplate();
    
    @Autowired
    private IKioskZabbixRoboticArmService kioskZabbixRoboticArmService;

    private Boolean getResponse(String action, KioskInfoEntity kiosk, String channel, String...details) {
        Boolean result = Boolean.FALSE;
        try {
            if (StringUtils.isBlank(serviceURL)) {
                result = Boolean.TRUE;
            } else {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> requestEntity = new HttpEntity<>(headers);

                String postResponse = template.postForObject(serviceURL + action, requestEntity, String.class);
                JsonParser parser = new JsonParser();
                JsonObject response = (JsonObject) parser.parse(postResponse);
                log.info(response.toString());
                if ("true".equals(response.get("success").getAsString())) {
                    result = Boolean.TRUE;
                } else {
                    if(action.equalsIgnoreCase("reject")){
                        kioskZabbixRoboticArmService.sendRoboticArmRejectError(kiosk, channel, details);
                    } else if (action.equalsIgnoreCase("collect")) {
                        kioskZabbixRoboticArmService.sendRoboticArmReleaseError(kiosk, channel, details);
                    }
                }
            }
        } catch (Exception e) {
            if(action.equalsIgnoreCase("reject")){
                kioskZabbixRoboticArmService.sendRoboticArmRejectError(kiosk, channel, details);
            } else if (action.equalsIgnoreCase("collect")) {
                kioskZabbixRoboticArmService.sendRoboticArmReleaseError(kiosk, channel, details);
            }
            log.error("action : " + action + " " + e.getMessage(), e);
        }
        return result;
    }

    @Override
    public Boolean init(KioskInfoEntity kiosk, String channel, String...details) {
        return this.getResponse("init", kiosk, channel);
    }

    @Override
    public Boolean issueTicket(KioskInfoEntity kiosk, String channel, String...details) {
        return this.getResponse("collect", kiosk, channel);
    }

    @Override
    public Boolean rejectTicket(KioskInfoEntity kiosk, String channel, String...details) {
        return this.getResponse("reject", kiosk, channel);
    }

    @Override
    public Boolean isReady(KioskInfoEntity kiosk, String channel, String...details) {
        return this.getResponse("getStatus", kiosk, channel);
    }

}
