package com.enovax.star.cms.kiosk.api.store.util;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

@Component
public class KioskDateUtil {

    public static final SimpleDateFormat DF_HH_MM_SS = new SimpleDateFormat("HH:mm:ss");
}
