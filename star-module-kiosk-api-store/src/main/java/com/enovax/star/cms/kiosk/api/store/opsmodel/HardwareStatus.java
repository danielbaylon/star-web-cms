package com.enovax.star.cms.kiosk.api.store.opsmodel;

/**
 * Created by Win_PC on 12/10/2016.
 */
public class HardwareStatus {
    String appServerStatus;
    String receiptPrinterStatus;
    String ticketPrinterStatus;
    String paymentTerminalStatus;
    String roboticArmStatus;
    String appServerMsg;
    String receiptPrinterMsg;
    String ticketPrinterMsg;
    String paymentTerminalMsg;
    String roboticArmMsg;

    public String getAppServerStatus() {
        return appServerStatus;
    }

    public void setAppServerStatus(String appServerStatus) {
        this.appServerStatus = appServerStatus;
    }

    public String getReceiptPrinterStatus() {
        return receiptPrinterStatus;
    }

    public void setReceiptPrinterStatus(String receiptPrinterStatus) {
        this.receiptPrinterStatus = receiptPrinterStatus;
    }

    public String getTicketPrinterStatus() {
        return ticketPrinterStatus;
    }

    public void setTicketPrinterStatus(String ticketPrinterStatus) {
        this.ticketPrinterStatus = ticketPrinterStatus;
    }

    public String getPaymentTerminalStatus() {
        return paymentTerminalStatus;
    }

    public void setPaymentTerminalStatus(String paymentTerminalStatus) {
        this.paymentTerminalStatus = paymentTerminalStatus;
    }

    public String getRoboticArmStatus() {
        return roboticArmStatus;
    }

    public void setRoboticArmStatus(String roboticArmStatus) {
        this.roboticArmStatus = roboticArmStatus;
    }

    public String getAppServerMsg() {
        return appServerMsg;
    }

    public void setAppServerMsg(String appServerMsg) {
        this.appServerMsg = appServerMsg;
    }

    public String getReceiptPrinterMsg() {
        return receiptPrinterMsg;
    }

    public void setReceiptPrinterMsg(String receiptPrinterMsg) {
        this.receiptPrinterMsg = receiptPrinterMsg;
    }

    public String getTicketPrinterMsg() {
        return ticketPrinterMsg;
    }

    public void setTicketPrinterMsg(String ticketPrinterMsg) {
        this.ticketPrinterMsg = ticketPrinterMsg;
    }

    public String getPaymentTerminalMsg() {
        return paymentTerminalMsg;
    }

    public void setPaymentTerminalMsg(String paymentTerminalMsg) {
        this.paymentTerminalMsg = paymentTerminalMsg;
    }

    public String getRoboticArmMsg() {
        return roboticArmMsg;
    }

    public void setRoboticArmMsg(String roboticArmMsg) {
        this.roboticArmMsg = roboticArmMsg;
    }
}
