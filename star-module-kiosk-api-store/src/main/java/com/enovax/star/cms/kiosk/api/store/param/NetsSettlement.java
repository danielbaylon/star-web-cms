package com.enovax.star.cms.kiosk.api.store.param;

/**
 * @author danielbaylon
 *
 */
public class NetsSettlement {

	private String result;

	private String merchantId;

	private String terminalId;

	private String commandId;

	private String responseCode;

	private String rawData;

	public NetsSettlement() {
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getResult() {
		return result;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public String getCommandId() {
		return commandId;
	}

	public String getRawData() {
		return rawData;
	}

}
