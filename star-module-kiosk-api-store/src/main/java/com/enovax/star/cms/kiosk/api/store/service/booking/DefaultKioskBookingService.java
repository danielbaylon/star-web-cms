package com.enovax.star.cms.kiosk.api.store.service.booking;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxStarCart;
import com.enovax.star.cms.commons.model.axstar.AxStarCartNested;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.booking.CheckoutDisplay;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.commons.model.booking.FunCartItem;
import com.enovax.star.cms.commons.model.booking.KioskFunCart;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.booking.StoreTransactionItem;
import com.enovax.star.cms.commons.model.kiosk.odata.AxAddCartLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCart;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketListCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdatePrintStatusCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDiscountLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxFacilityClassEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintHistory;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintTokenEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrder;
import com.enovax.star.cms.commons.model.kiosk.odata.AxTenderLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdateRetailTicketStatusCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdatedTicketStatusEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUseDiscountCounter;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskAddCartLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskAddTenderLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskBlacklistTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutCompleteRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartValidateAndReserveRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartValidateQuantityRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetCartsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetReceiptsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchProductExtRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdateCartLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdatePrintStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidCartLinesRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidCartsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidTenderLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutCompleteResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdatePrintStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateAndReserveResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateQuantityResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetReceiptsResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchProductExtResponse;
import com.enovax.star.cms.commons.model.ticket.Facility;
import com.enovax.star.cms.commons.model.ticket.TicketData;
import com.enovax.star.cms.commons.service.axchannel.AxChannelTransactionService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.axcrt.kiosk.AxStarKioskService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.api.store.jcr.repository.product.IProductJcrRepository;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionItemDao;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketStatus;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketTokenList;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketToken;
import com.enovax.star.cms.kiosk.api.store.reference.KioskDiscountStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTenderType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTicketPrintStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.repository.cart.IKioskCartManager;
import com.enovax.star.cms.kiosk.api.store.service.arm.IKioskRoboticArmService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixPrinterService;
import com.enovax.star.cms.kiosk.api.store.service.sync.IKioskDBSyncService;

@SuppressWarnings("Duplicates")
@Service
public class DefaultKioskBookingService implements IKioskBookingService {

    private Logger log = LoggerFactory.getLogger(DefaultKioskBookingService.class);

    @Autowired
    private AxChannelTransactionService axChannelTransactionService;
    @Autowired
    private AxStarKioskService axStarKioskService;
    @Autowired
    private AxStarService axStarService;
    @Autowired
    private IKioskTransactionDao transactionDao;

    @Autowired
    private IKioskTransactionItemDao transactionItemDao;

    @Autowired
    private IProductJcrRepository productRepository;

    @Autowired
    private IKioskCartManager cartManager;

    @Autowired
    private IKioskManagementService kioskManagementService;

    @Autowired
    private IKioskRoboticArmService armService;
    
    @Autowired
    private IKioskZabbixPrinterService zabbixPrinterService;
    
    @Autowired
    private IKioskDBSyncService syncService;

    private void saveTransactionInSession(StoreApiChannels channel, HttpSession session, KioskTransactionEntity txn) {
        session.setAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN, txn);
    }

    private KioskTransactionEntity getTransactionInSession(StoreApiChannels channel, HttpSession session) {
        return (KioskTransactionEntity) session.getAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }

    private void clearTransactionInSession(StoreApiChannels channel, HttpSession session) {
        session.removeAttribute(channel.code + StoreApiConstants.SESSION_ATTR_STORE_TXN);
    }

    // TODO AX Retail CRT API integration
    // TODO Business rules validation for all steps in the booking process

    private ApiResult<KioskCartValidateQuantityResponse> checkCartQuantity(StoreApiChannels channel, FunCart cartToAdd, KioskInfoEntity kioskInfo) throws Exception {
        ApiResult<KioskCartValidateQuantityResponse> apiResult = new ApiResult<>(ApiErrorCodes.General);

        KioskCartValidateQuantityRequest request = new KioskCartValidateQuantityRequest();
        List<AxCartTicketListCriteria> criteriaList = new ArrayList<>();
        for (FunCartItem funCartItem : cartToAdd.getItems()) {
            if (funCartItem.isCapacity()) {
                AxCartTicketListCriteria criteria = new AxCartTicketListCriteria();
                if (funCartItem.isCapacity()) {
                    Date eventDate = NvxDateUtils.parseAxDate(funCartItem.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    criteria.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                    criteria.setEventGroupId(funCartItem.getEventGroupId());
                    criteria.setEventLineId(funCartItem.getEventLineId());
                } else {
                    criteria.setEventDate(NvxDateUtils.formatDate(Calendar.getInstance(TimeZone.getDefault()).getTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                }
                criteria.setIsUpdateCapacity("0");
                criteria.setProductId(funCartItem.getListingId());
                criteria.setQty(funCartItem.getQty().toString());
                criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                criteria.setStaffId(kioskInfo.getUserId());
                criteria.setTransactionId("CartValidate");
                criteriaList.add(criteria);
            }
        }
        request.setAxCartTicketListCriteria(criteriaList);

        AxStarServiceResult<KioskCartValidateQuantityResponse> cartValidateQuantityResult = this.axStarKioskService.apiKioskCartValidateQuantity(channel, request);
        if (cartValidateQuantityResult.isSuccess()) {
            KioskCartValidateQuantityResponse response = cartValidateQuantityResult.getData();
            if (response != null && "0".equals(response.getErrorStatus()) && StringUtils.isBlank(response.getErrorMessage())) {
                apiResult = new ApiResult<>(Boolean.TRUE, "", "", response);
            } else {
                String itemIds = cartToAdd.getItems().stream().map(i -> i.getProductCode()).collect(Collectors.joining(","));
                log.error("cart quantity check failed for item ids : " + itemIds + ".");
                // AX original : Insufficient quantity for selected product -
                // 1102103001 Available Qty: 0
                apiResult.setMessage(ApiErrorCodes.InsufficientQty.message);
                apiResult.setErrorCode(ApiErrorCodes.InsufficientQty.code);
            }
        }
        return apiResult;
    }

    private AxCart getAxCart(StoreApiChannels channel, KioskInfoEntity kioskInfo, String sessionId) throws Exception {
        AxCart axCart = null;
        KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (StringUtils.isBlank(kioskInfo.getCurrentCartId())) {
            kioskManagementService.getNextTransactionId(channel, kioskInfo);
            String transactionId = this.kioskManagementService.getTransactionId(kioskInfo);
            kioskInfo.setCurrentCartId(transactionId);
            kioskManagementService.updateKioskInfo(kioskInfo);
            KioskGetCartsRequest request = new KioskGetCartsRequest();
            request.setCartId(kioskInfo.getCurrentCartId());

            AxStarServiceResult<AxCart> getCartsResult = this.axStarKioskService.apiKioskGetCarts(channel, request);
            if (getCartsResult.isSuccess()) {
                axCart = getCartsResult.getData();
            } else {
                throw new Exception("Fail to get cart from AX " + request.getCartId());
            }
        } else {
            if (savedCart != null) {
                axCart = savedCart.getKioskAxCart();
            }
        }
        return axCart;

    }

    private Boolean updateDiscountLines(StoreApiChannels channel, KioskInfoEntity kioskInfo, List<AxCartLine> axCartLines, KioskDiscountStatus status) {
        Boolean updated = Boolean.TRUE;
        for (AxCartLine axCartLine : axCartLines) {
            for (AxDiscountLine discountLine : axCartLine.getDiscountLineList()) {
                AxUseDiscountCounter useDiscountCounter = new AxUseDiscountCounter();
                useDiscountCounter.setInventTransId(kioskInfo.getCurrentCartId());
                useDiscountCounter.setOfferId(discountLine.getOfferId());
                useDiscountCounter.setTransactoinId(kioskInfo.getCurrentCartId());
                useDiscountCounter.setStatus(status.getCode());
                AxStarServiceResult<AxUseDiscountCounter> useDiscountCounterResult = this.axStarKioskService.apiKioskUseDiscountCounter(channel, useDiscountCounter);

                if (!useDiscountCounterResult.isSuccess()) {
                    updated = Boolean.FALSE;
                    log.error("apiKioskUseDiscountCounter failed" + useDiscountCounterResult.getError().getMessage() + useDiscountCounterResult.getRawData());
                }
            }
        }
        return updated;
    }

    private AxAddCartLine createAxAddCartLine(FunCartItem item) throws ParseException {
        AxAddCartLine axAddCartLine = new AxAddCartLine();
        Boolean isEvent = StringUtils.isNotEmpty(item.getEventGroupId());
        if (isEvent) {
            String selectedEventDate = item.getSelectedEventDate();
            if (StringUtils.isNotBlank(selectedEventDate)) {
                Date eventDate = NvxDateUtils.parseAxDate(selectedEventDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                selectedEventDate = NvxDateUtils.formatDate(eventDate, "yyyy-MM-dd'T'") + "00:00:00.000";
                axAddCartLine.setComment(item.getEventGroupId() + ";" + item.getEventLineId() + ";" + selectedEventDate + ";");
            } else {
                axAddCartLine.setComment(item.getEventGroupId() + ";" + item.getEventLineId() + ";");
            }

        }
        if (item.isTopup()) {
            axAddCartLine.setComment(item.getParentListingId());
        }
        axAddCartLine.setDescription(item.getName());

        axAddCartLine.setItemId(item.getProductCode());
        axAddCartLine.setProductId(item.getListingId());
        axAddCartLine.setQuantity(item.getQty().toString());
        axAddCartLine.setUnitOfMeasureSymbol("ea");

        return axAddCartLine;
    }

    private AxStarServiceResult<AxCart> addCartLines(StoreApiChannels channel, FunCart cartToAdd, AxCart remoteAxCart, KioskInfoEntity kioskInfo) throws ParseException {
        List<AxAddCartLine> axCartLineListToAdd = new ArrayList<>();
        for (FunCartItem item : cartToAdd.getItems()) {
            axCartLineListToAdd.add(this.createAxAddCartLine(item));
        }
        KioskAddCartLineRequest request = new KioskAddCartLineRequest();
        request.setCartId(kioskInfo.getCurrentCartId());
        request.setAxCartLineList(axCartLineListToAdd);
        AxStarServiceResult<AxCart> addCartLineResult = this.axStarKioskService.apiKioskAddCartLine(channel, request);
        return addCartLineResult;
    }

    private KioskFunCart getCurrentFunCart(StoreApiChannels channel, String sessionId) {
        KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            savedCart = new KioskFunCart();
            savedCart.setCartId(sessionId);
            savedCart.setChannelCode(channel.code);
        }
        return savedCart;
    }

    private FunCartItem createNewFunCartItem(FunCartItem itemToAdd, AxCartLine axCartLine) {
        FunCartItem item = new FunCartItem();
        final Boolean isEvent = itemToAdd.isCapacity();

        item.setIsTopup(itemToAdd.isTopup());
        item.setParentListingId(itemToAdd.getParentListingId());
        item.setParentCmsProductId(itemToAdd.getParentCmsProductId());
        item.setAxLineCommentId(itemToAdd.getAxLineCommentId());

        item.setCmsProductId(itemToAdd.getCmsProductId());
        item.setCmsProductName(itemToAdd.getCmsProductName());
        item.setListingId(itemToAdd.getListingId());
        item.setName(itemToAdd.getName());
        item.setPrice(itemToAdd.getPrice());
        item.setProductCode(itemToAdd.getProductCode());
        item.setQty(new BigDecimal(axCartLine.getQuantity()).intValue());
        item.setType(itemToAdd.getType());

        item.setEventGroupId(isEvent ? itemToAdd.getEventGroupId() : "");
        item.setEventLineId(isEvent ? itemToAdd.getEventLineId() : "");
        item.setSelectedEventDate(isEvent ? itemToAdd.getSelectedEventDate() : "");
        item.setEventSessionName(isEvent ? itemToAdd.getEventSessionName() : "");
        item.setLineId(axCartLine.getLineId());

        item.setCapacity(itemToAdd.isCapacity());
        item.setOpenDate(itemToAdd.isOpenDate());

        item.setCartItemId(item.generateCartItemId());
        item.setParentCmsProductId(itemToAdd.getParentCmsProductId());
        item.setParentListingId(itemToAdd.getParentListingId());
        return item;
    }

    private String formatSelectedEventDate(String date) {
        // yyyy-MM-dd'T'HH:mm:ss.SSS"
        String axDate = "";
        String dates[] = date.split("/"); // dd/MM/yyyy
        if (dates.length == 3) {
            axDate = dates[2] + "-" + dates[1] + "-" + dates[0] + "T00:00:00.000";
        }
        return axDate;
    }

    private void mergeAxCartIntoFunCart(AxCart axCart, FunCart currentFunCart, FunCart cartToAdd) {
        if (currentFunCart.getItems() == null) {
            currentFunCart.setItems(new ArrayList<>());
        }
        for (AxCartLine axCartLine : axCart.getAxCartLineList()) {
            // find if ax cart line in current cart , if not then add otherwise
            // update
            FunCartItem existingItem = currentFunCart.getItems().stream().filter(item -> axCartLine.getLineId().equals(item.getLineId())).findAny().orElse(null);
            if (existingItem == null) {

                FunCartItem newItem = null;
                for (FunCartItem item : cartToAdd.getItems()) {
                    if (item.isCapacity()) {
                        // comment is group id + line id + event date from AX
                        String key = axCartLine.getProductId() + axCartLine.getComment();
                        FunCartItem itemToAdd = cartToAdd.getItems().stream().filter(
                                i -> key.equals(i.getListingId() + i.getEventGroupId() + ";" + i.getEventLineId() + ";" + formatSelectedEventDate(i.getSelectedEventDate()) + ";"))
                                .findAny().orElse(null);
                        if (itemToAdd != null) {
                            newItem = itemToAdd;
                        }
                    } else if (item.isTopup()) {
                        // comment is parentListingId
                        String key = axCartLine.getProductId() + axCartLine.getComment();
                        FunCartItem itemToAdd = cartToAdd.getItems().stream().filter(i -> key.equals(i.getListingId() + i.getParentListingId())).findAny().orElse(null);
                        if (itemToAdd != null) {
                            newItem = itemToAdd;
                        }
                    } else {
                        String key = axCartLine.getProductId();
                        FunCartItem itemToAdd = cartToAdd.getItems().stream().filter(i -> key.equals(i.getListingId())).findAny().orElse(null);
                        if (itemToAdd != null) {
                            newItem = itemToAdd;
                        }
                    }

                }

                if (newItem != null) {
                    currentFunCart.getItems().add(createNewFunCartItem(newItem, axCartLine));
                } else {
                    log.error("product added to AX can't be found in the passed in FUN CART.");
                }

            } else {
                existingItem.setQty(new Integer(axCartLine.getQuantity()));
            }

        }

    }

    private AxStarServiceResult<KioskCartValidateAndReserveResponse> reserveTicket(StoreApiChannels channel, FunCart cartToAdd, List<AxCartLine> axCartLineList,
            KioskInfoEntity kioskInfo, Boolean add) {

        AxStarServiceResult<KioskCartValidateAndReserveResponse> cartValidateAndReserveResult = new AxStarServiceResult<>();
        try {
            KioskCartValidateAndReserveRequest validateAndReserveRequest = new KioskCartValidateAndReserveRequest();
            for (AxCartLine line : axCartLineList) {
                List<FunCartItem> items = cartToAdd.getItems().stream().filter(i -> line.getItemId().equals(i.getProductCode())).collect(Collectors.toList());
                for (FunCartItem item : items) {
                    AxCartTicketListCriteria criteria = new AxCartTicketListCriteria();

                    if (item.isCapacity()) {
                        Date eventDate = NvxDateUtils.parseAxDate(item.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                        criteria.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                        criteria.setEventGroupId(item.getEventGroupId());
                        criteria.setEventLineId(item.getEventLineId());
                    } else {
                        criteria.setEventDate(NvxDateUtils.formatDate(Calendar.getInstance(TimeZone.getDefault()).getTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                    }
                    criteria.setIsUpdateCapacity(add ? "0" : "1");
                    criteria.setItemId(line.getItemId());
                    criteria.setLineId(line.getLineId());
                    criteria.setQty(line.getQuantity());
                    criteria.setProductId(line.getProductId());
                    criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                    criteria.setStaffId(kioskInfo.getUserId());
                    criteria.setTransactionId(kioskInfo.getCurrentCartId());
                    validateAndReserveRequest.getAxCartTicketListCriteria().add(criteria);
                }
            }

            cartValidateAndReserveResult = this.axStarKioskService.apiKioskCartValidateAndReserve(channel, validateAndReserveRequest);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return cartValidateAndReserveResult;
    }

    private List<AxInventTableExtEntity> getProductExtList(StoreApiChannels channel, FunCart cartToAdd) {
        List<AxInventTableExtEntity> productExtList = new ArrayList<>();
        for (FunCartItem item : cartToAdd.getItems()) {
            AxInventTableExtCriteria axInventTableExtCriteria = new AxInventTableExtCriteria();
            axInventTableExtCriteria.setItemId(item.getProductCode());

            KioskSearchProductExtRequest kioskSearchProductExtRequest = new KioskSearchProductExtRequest();
            kioskSearchProductExtRequest.setAxInventTableExtCriteria(axInventTableExtCriteria);

            AxStarServiceResult<KioskSearchProductExtResponse> searchProductExtResult = this.axStarKioskService.apiKioskSearchProductExt(channel, kioskSearchProductExtRequest);
            if (searchProductExtResult.isSuccess()) {
                productExtList.addAll(searchProductExtResult.getData().getAxInventTableExtEntityList());
            }
        }

        return productExtList;

    }

    @Override
    public ApiResult<FunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId) {

        ApiResult<FunCartDisplay> apiResult = new ApiResult<>(ApiErrorCodes.General);
        ApiResult<KioskCartValidateQuantityResponse> checkCartQuantityResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            KioskInfoEntity kioskInfo = kioskManagementService.getKioskInfo(channel);

            // List<AxInventTableExtEntity> productExtList =
            // this.getProductExtList(channel, cartToAdd);

            checkCartQuantityResult = checkCartQuantity(channel, cartToAdd, kioskInfo);
            if (checkCartQuantityResult.isSuccess()) {
                AxCart axCart = getAxCart(channel, kioskInfo, sessionId);
                AxStarServiceResult<AxCart> addCartLinesResult = this.addCartLines(channel, cartToAdd, axCart, kioskInfo);

                AxCart currentAxCart = addCartLinesResult.getData();
                reserveTicket(channel, cartToAdd, currentAxCart.getAxCartLineList(), kioskInfo, Boolean.TRUE);

                KioskFunCart currentFunCart = this.getCurrentFunCart(channel, sessionId);
                mergeAxCartIntoFunCart(currentAxCart, currentFunCart, cartToAdd);

                currentFunCart.setKioskAxCart(currentAxCart);
                cartManager.saveCart(channel, currentFunCart);
                final FunCartDisplay funCartDisplay = constructCartDisplay(channel, currentFunCart);
                apiResult = new ApiResult<>(true, "", "", funCartDisplay);
            } else {
                apiResult.setErrorCode(checkCartQuantityResult.getErrorCode());
                apiResult.setMessage(checkCartQuantityResult.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    @Override
    public ApiResult<FunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, KioskFunCart topupCart) {

        ApiResult<FunCartDisplay> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            Boolean hasParent = Boolean.TRUE;
            if (savedCart != null) {
                for (FunCartItem topupFunCartItem : topupCart.getItems()) {

                    List<FunCartItem> parentFunCartItems = savedCart.getItems().stream().filter(i -> topupFunCartItem.getParentListingId().equals(i.getListingId()))
                            .collect(Collectors.toList());
                    if (parentFunCartItems == null || parentFunCartItems.size() == 0) {
                        log.error("Can't buy cross sell (child) product without buying original (parent) product.");
                        hasParent = Boolean.FALSE;
                    } else {
                        // parent and child should always the same QTY
                        Integer parentQty = parentFunCartItems.stream().mapToInt(i -> i.getQty()).sum();
                        if (topupFunCartItem.getQty() - parentQty > 0) {
                            topupFunCartItem.setQty(parentQty);
                        }
                    }
                }

                ApiResult<KioskCartValidateQuantityResponse> checkCartQuantityResult = checkCartQuantity(channel, topupCart, kioskInfo);
                if (hasParent && checkCartQuantityResult.isSuccess()) {
                    AxStarServiceResult<AxCart> addAxCartLineResult = this.addCartLines(channel, topupCart, savedCart.getKioskAxCart(), kioskInfo);
                    if (addAxCartLineResult.isSuccess()) {
                        KioskFunCart currentFunCart = this.getCurrentFunCart(channel, sessionId);
                        mergeAxCartIntoFunCart(addAxCartLineResult.getData(), currentFunCart, topupCart);

                        currentFunCart.setKioskAxCart(addAxCartLineResult.getData());
                        cartManager.saveCart(channel, currentFunCart);
                        final FunCartDisplay funCartDisplay = constructCartDisplay(channel, currentFunCart);
                        apiResult = new ApiResult<>(true, "", "", funCartDisplay);
                    } else {
                        apiResult.setApiErrorCode(ApiErrorCodes.General);
                        apiResult.setMessage(addAxCartLineResult.getError().getMessage());
                    }
                } else {
                    apiResult.setErrorCode(checkCartQuantityResult.getErrorCode());
                    apiResult.setMessage(checkCartQuantityResult.getMessage());
                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    private AxStarServiceResult<AxCart> voidAxCartLine(StoreApiChannels channel, List<AxCartLine> axCartLineList, KioskFunCart funCart, KioskInfoEntity kioskInfo) {
        AxStarServiceResult<AxCart> voidCartLineResult = new AxStarServiceResult<>();
        try {
            KioskVoidCartLinesRequest voidCartLinesRequest = new KioskVoidCartLinesRequest();
            voidCartLinesRequest.setCartId(kioskInfo.getCurrentCartId());
            for (AxCartLine line : axCartLineList) {
                line.setIsVoided(Boolean.TRUE.toString());
                for (AxDiscountLine discountLine : line.getDiscountLineList()) {
                    AxUseDiscountCounter useDiscountCounter = new AxUseDiscountCounter();
                    useDiscountCounter.setInventTransId(kioskInfo.getCurrentCartId());
                    useDiscountCounter.setOfferId(discountLine.getOfferId());
                    useDiscountCounter.setTransactoinId(kioskInfo.getCurrentCartId());
                    useDiscountCounter.setIsUpdate("1");
                    useDiscountCounter.setStatus(KioskDiscountStatus.CANCELLED.getCode());
                    AxStarServiceResult<AxUseDiscountCounter> useDiscountCounterResult = axStarKioskService.apiKioskUseDiscountCounter(channel, useDiscountCounter);
                    if (!useDiscountCounterResult.isSuccess()) {
                        log.error("use discount counter failed during void cart line" + useDiscountCounterResult.getError().getMessage());
                    }
                }
            }
            voidCartLinesRequest.getAxCartLineList().addAll(axCartLineList);
            voidCartLineResult = this.axStarKioskService.apiKioskVoidCartLines(channel, voidCartLinesRequest);

            this.reserveTicket(channel, funCart, axCartLineList, kioskInfo, Boolean.FALSE);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return voidCartLineResult;
    }

    private AxStarServiceResult<AxCart> voidAxCartLine(StoreApiChannels channel, AxCartLine axCartLine, KioskFunCart funCart, KioskInfoEntity kioskInfo) {
        AxStarServiceResult<AxCart> voidCartLineResult = new AxStarServiceResult<>();
        try {
            KioskVoidCartLinesRequest voidCartLinesRequest = new KioskVoidCartLinesRequest();
            voidCartLinesRequest.setCartId(kioskInfo.getCurrentCartId());
            axCartLine.setIsVoided(Boolean.TRUE.toString());
            voidCartLinesRequest.getAxCartLineList().add(axCartLine);

            for (AxDiscountLine discountLine : axCartLine.getDiscountLineList()) {
                AxUseDiscountCounter useDiscountCounter = new AxUseDiscountCounter();
                useDiscountCounter.setInventTransId(kioskInfo.getCurrentCartId());
                useDiscountCounter.setOfferId(discountLine.getOfferId());
                useDiscountCounter.setTransactoinId(kioskInfo.getCurrentCartId());
                useDiscountCounter.setIsUpdate("1");
                useDiscountCounter.setStatus(KioskDiscountStatus.CANCELLED.getCode());
                AxStarServiceResult<AxUseDiscountCounter> useDiscountCounterResult = axStarKioskService.apiKioskUseDiscountCounter(channel, useDiscountCounter);
                if (!useDiscountCounterResult.isSuccess()) {
                    log.error("use discount counter failed during void cart line" + useDiscountCounterResult.getError().getMessage());
                }
            }

            voidCartLineResult = this.axStarKioskService.apiKioskVoidCartLines(channel, voidCartLinesRequest);
            List<AxCartLine> axCartLineList = new ArrayList<>();
            axCartLineList.add(axCartLine);
            this.reserveTicket(channel, funCart, axCartLineList, kioskInfo, Boolean.FALSE);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return voidCartLineResult;
    }

    private ApiResult<FunCartDisplay> updateCartLines(StoreApiChannels channel, KioskFunCart cartInSession, KioskFunCart cartToAdd, AxCart remoteAxCart, KioskInfoEntity kioskInfo,
            String sessionId) throws Exception {

        KioskUpdateCartLineRequest request = new KioskUpdateCartLineRequest();
        request.setCartId(kioskInfo.getCurrentCartId());

        List<FunCartItem> toBeRemoved = new ArrayList<>();
        List<AxCartLine> lines = new ArrayList<>();
        for (AxCartLine line : remoteAxCart.getAxCartLineList()) {
            FunCartItem existing = cartInSession.getItems().stream().filter(i -> line.getLineId().equals(i.getLineId())).findAny().orElse(new FunCartItem());
            if (!existing.isTopup()) {
                lines.add(line);
                toBeRemoved.add(existing);
            }
        }

        if (lines.size() > 0) {
            AxStarServiceResult<AxCart> voidAxCartLineResult = this.voidAxCartLine(channel, lines, cartInSession, kioskInfo);
            if (voidAxCartLineResult.isSuccess()) {
                // remove the ones that has been voided from cart
                cartInSession.setKioskAxCart(voidAxCartLineResult.getData());
                cartInSession.getItems().removeAll(toBeRemoved);
                cartManager.saveCart(channel, cartInSession);
            }
        }

        return this.cartAdd(channel, cartToAdd, sessionId);

    }

    @Override
    public ApiResult<FunCartDisplay> updateShoppingCart(StoreApiChannels channel, KioskFunCart cartToUpdate, String sessionId) {
        ApiResult<FunCartDisplay> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
            if (savedCart == null) {
                return new ApiResult<>(ApiErrorCodes.NoCartFound);
            }
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            AxCart axCart = this.getAxCart(channel, kioskInfo, sessionId);
            apiResult = this.updateCartLines(channel, savedCart, cartToUpdate, axCart, kioskInfo, sessionId);
        } catch (Exception e) {
            apiResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    @Override
    public ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode) {
        final FunCart savedCart = cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        final AxStarCart axCart = savedCart.getAxCart();
        final AxStarServiceResult<String> axResult = axStarService.apiCartApplyPromoCode(channel, axCart.getId(),
                StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId(), promoCode);
        if (!axResult.isSuccess()) {
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddPromoCode);
        }

        AxStarServiceResult<AxStarCartNested> getCartResult = axStarService.apiCartGet(channel, axCart.getId(),
                StringUtils.isEmpty(savedCart.getAxCustomerId()) ? "" : savedCart.getAxCustomerId());
        if (!getCartResult.isSuccess()) {
            log.error("Error getting updated cart."); // TODO Further handling
            return new ApiResult<>(ApiErrorCodes.AxStarErrorCartAddPromoCode);
        }

        savedCart.setAxCart(getCartResult.getData().getCart());
        cartManager.saveCart(channel, savedCart);

        return new ApiResult<>(true, "", "", axResult.getData());
    }

    @Override
    public ApiResult<FunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId) {

        KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        AxCart axCart = savedCart.getKioskAxCart();

        List<FunCartItem> removedFunCartItem = new ArrayList<>();
        List<FunCartItem> savedFunCartItems = savedCart.getItems().stream().filter(i -> cartItemId.equals(i.getCartItemId())).collect(Collectors.toList());

        for (FunCartItem savedFunCartItem : savedFunCartItems) {

            // find topup and void topup ax cart line first if any
            Boolean allTopupRemoved = Boolean.TRUE;
            for (FunCartItem topupItem : savedCart.getItems()) {
                if (savedFunCartItem.getListingId().equals(topupItem.getParentListingId())) {
                    AxCartLine axCartLine = axCart.getAxCartLineList().stream().filter(l -> l.getLineId().equals(topupItem.getLineId())).findAny().orElse(null);
                    if (axCartLine != null) {
                        AxStarServiceResult<AxCart> voidResult = this.voidAxCartLine(channel, axCartLine, savedCart, kioskInfo);
                        if (voidResult.isSuccess()) {
                            removedFunCartItem.add(topupItem);
                        } else {
                            allTopupRemoved = false;
                            log.error("failed to remove topup " + savedFunCartItem.getLineId());
                        }
                    }
                }
            }

            // void current line
            if (allTopupRemoved) {
                AxCartLine axCartLine = axCart.getAxCartLineList().stream().filter(l -> l.getLineId().equals(savedFunCartItem.getLineId())).findAny().orElse(null);
                if (axCartLine != null) {
                    AxStarServiceResult<AxCart> voidResult = this.voidAxCartLine(channel, axCartLine, savedCart, kioskInfo);
                    if (voidResult.isSuccess()) {
                        removedFunCartItem.add(savedFunCartItem);
                    }
                }
            }
        }

        for (int i = 0; i < savedCart.getItems().size(); i++) {
            FunCartItem item = savedCart.getItems().get(i);
            FunCartItem toRemoveItem = removedFunCartItem.stream().filter(toRemove -> toRemove.getCartItemId().equals(item.getCartItemId())).findAny().orElse(null);
            if (toRemoveItem != null) {
                savedCart.getItems().remove(i);
                i--;
            }
        }

        final FunCartDisplay funCartDisplay = constructCartDisplay(channel, savedCart);
        return new ApiResult<>(true, "", "", funCartDisplay);
    }

    @Override
    public boolean cartClear(StoreApiChannels channel, String sessionId) {
        Boolean cleared = Boolean.FALSE;
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (savedCart != null && savedCart.getKioskAxCart() != null) {
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            KioskVoidCartsRequest request = new KioskVoidCartsRequest();
            request.setCartId(savedCart.getKioskAxCart().getId());

            AxStarServiceResult<AxSalesOrder> voidCartsResult = axStarKioskService.apiKioskVoidCarts(channel, request);
            AxCart axCart = savedCart.getKioskAxCart();
            for (AxCartLine axCartLine : axCart.getAxCartLineList()) {
                for (AxDiscountLine discountLine : axCartLine.getDiscountLineList()) {
                    AxUseDiscountCounter useDiscountCounter = new AxUseDiscountCounter();
                    useDiscountCounter.setInventTransId(request.getCartId());
                    useDiscountCounter.setOfferId(discountLine.getOfferId());
                    useDiscountCounter.setTransactoinId(request.getCartId());
                    useDiscountCounter.setIsUpdate("1");
                    useDiscountCounter.setStatus(KioskDiscountStatus.CANCELLED.getCode());
                    AxStarServiceResult<AxUseDiscountCounter> useDiscountCounterResult = axStarKioskService.apiKioskUseDiscountCounter(channel, useDiscountCounter);
                    if (!useDiscountCounterResult.isSuccess()) {
                        log.error("use discount counter failed during void cart line" + useDiscountCounterResult.getError().getMessage());
                    }
                }
                this.reserveTicket(channel, savedCart, axCart.getAxCartLineList(), kioskInfo, Boolean.FALSE);
            }
            if (!voidCartsResult.isSuccess()) {
                log.error("VOID CART " + request.getCartId() + "FAILED");
            }
            cleared = Boolean.TRUE;
            cartManager.removeCart(channel, sessionId);
            kioskInfo.setCurrentCartId("");
            kioskManagementService.updateKioskInfo(kioskInfo);
        }
        return cleared;
    }

    @Override
    public FunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId) {
        final KioskFunCart funCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            FunCartDisplay emptyCart = new FunCartDisplay();
            emptyCart.setCartId(sessionId);
            return emptyCart;
        }

        return constructCartDisplay(channel, funCart);
    }

    @Override
    public FunCartDisplay constructCartDisplay(StoreApiChannels channel, KioskFunCart cart) {
        final FunCartDisplay cd = new FunCartDisplay();
        cd.setCartId(cart.getCartId());

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;
        List<AxCartLine> cartLines = new ArrayList<>();
        if (cart.getKioskAxCart() != null) {
            cartLines = cart.getKioskAxCart().getAxCartLineList();
        }

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();
        for (FunCartItem item : cart.getItems()) {
            final boolean isTopup = item.isTopup();
            final String productId = isTopup ? item.getParentCmsProductId() : item.getCmsProductId();
            // final String key = productId + item.getEventGroupId() +
            // item.getEventLineId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getCmsProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            // final List<AxCartLine> relatedLines =
            // cartLineMap.get(item.getListingId());

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();
            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();

            itemDisplay.setListingId(item.getListingId());
            itemDisplay.setProductCode(item.getProductCode());
            itemDisplay.setIsTopup(isTopup);
            itemDisplay.setCapacity(item.isCapacity());
            itemDisplay.setOpenDate(item.isOpenDate());
            if (itemDisplay.isTopup()) {
                itemDisplay.setParentListingId(item.getParentListingId());
                itemDisplay.setParentCmsProductId(item.getParentCmsProductId());
            }
            itemDisplay.setType(item.getType());
            itemDisplay.setName(item.getName());

            final boolean isEvent = item.isCapacity();

            itemDisplay.setDescription(
                    "Ticket Type : " + item.getType() + (isEvent ? "<br>Event Session : " + item.getEventSessionName() + "<br>Event Date : " + item.getSelectedEventDate() : ""));

            itemDisplay.setEventGroupId(item.getEventGroupId());
            itemDisplay.setEventLineId(item.getEventLineId());
            itemDisplay.setSelectedEventDate(item.getSelectedEventDate());
            itemDisplay.setEventSessionName(item.getEventSessionName());

            itemDisplay.setCartItemId(item.getCartItemId());

            AxCartLine line = cartLines.stream().filter(l -> l.getLineId().equals(item.getLineId())).findAny().orElse(new AxCartLine());
            itemDisplay.setQty(new BigDecimal(line.getQuantity()).intValue());
            itemDisplay.setLineId(item.getLineId());
            itemDisplay.setPrice(new BigDecimal(line.getPrice()));
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(itemDisplay.getPrice(), "S$ "));
            itemDisplay.setTotal(new BigDecimal(line.getTotalAmount()));
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getTotal(), "S$ "));
            itemDisplay.setDiscountTotal(new BigDecimal(line.getDiscountAmount()));
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getDiscountTotal(), "S$ "));
            itemDisplay.setDiscountLabel("");
            for (AxDiscountLine discount : line.getDiscountLineList()) {
                if (StringUtils.isBlank(itemDisplay.getDiscountLabel())) {
                    itemDisplay.setDiscountLabel(discount.getOfferName());
                } else {
                    itemDisplay.setDiscountLabel(itemDisplay.getDiscountLabel() + " , " + discount.getOfferName());
                }

            }

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            mergeFunCartDisplayItems(productDisplay);
            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));

            String admission = productRepository.getAdmssionInfo(channel.code, productDisplay.getId());
            if ("true".equals(admission)) {
                productDisplay.setIsAdmission(Boolean.TRUE);
                cd.setHasAdmission(Boolean.TRUE);
            }
            productDisplay.setIsTransportLayout(productRepository.isProductCategoryTransportLayout(channel.code, productDisplay.getId()));
        }

        cd.setCmsProducts(cmsProducts);
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        return cd;
    }

    private void mergeFunCartDisplayItems(FunCartDisplayCmsProduct productDisplay) {

        List<FunCartDisplayItem> mergedItems = new ArrayList<>();
        Map<String, List<FunCartDisplayItem>> mergedTopupItems = new HashMap<>();
        List<FunCartDisplayItem> displayItems = productDisplay.getItems();
        for (FunCartDisplayItem item : displayItems) {

            String key = item.getListingId() + item.getParentListingId() + item.getEventLineId() + item.getEventGroupId();
            // check if this item is merged already
            FunCartDisplayItem existing = mergedItems.stream().filter(m -> key.equals(m.getListingId() + m.getParentListingId() + m.getEventLineId() + m.getEventGroupId()))
                    .findAny().orElse(null);
            if (existing == null) {
                // not merged then find all items
                List<FunCartDisplayItem> sameItemList = productDisplay.getItems().stream()
                        .filter(i -> key.equals(i.getListingId() + i.getParentListingId() + i.getEventLineId() + i.getEventGroupId())).collect(Collectors.toList());

                if (sameItemList.size() == 1) {
                    FunCartDisplayItem merged = sameItemList.get(0);
                    if (merged.isTopup()) {
                        if (mergedTopupItems.get(merged.getParentListingId()) == null) {
                            List<FunCartDisplayItem> topups = new ArrayList<>();
                            topups.add(merged);
                            mergedTopupItems.put(merged.getParentListingId(), topups);
                        } else {
                            mergedTopupItems.get(merged.getParentListingId()).add(merged);
                        }

                    } else {
                        mergedItems.addAll(sameItemList);
                    }
                } else if (sameItemList.size() > 1) {
                    FunCartDisplayItem merged = sameItemList.get(0);
                    for (int i = 1; i < sameItemList.size(); i++) {
                        merged.setQty(merged.getQty() + sameItemList.get(i).getQty());
                        merged.setTotal(merged.getTotal().add(sameItemList.get(i).getTotal()));
                        if (!merged.getDiscountLabel().equals(sameItemList.get(i).getDiscountLabel())) {
                            merged.setDiscountLabel(merged.getDiscountLabel() + " " + sameItemList.get(i).getDiscountLabel());
                        }
                        merged.setDiscountTotal(merged.getDiscountTotal().add(sameItemList.get(i).getDiscountTotal()));
                    }
                    if (merged.isTopup()) {
                        if (mergedTopupItems.get(merged.getParentListingId()) == null) {
                            List<FunCartDisplayItem> topups = new ArrayList<>();
                            topups.add(merged);
                            mergedTopupItems.put(merged.getParentListingId(), topups);
                        } else {
                            mergedTopupItems.get(merged.getParentListingId()).add(merged);
                        }
                    } else {
                        mergedItems.add(merged);
                    }
                } else {
                    log.error("ERROR MERGING , item not found for " + key);
                }
            }
        }

        productDisplay.setItems(new ArrayList<>());

        for (FunCartDisplayItem merged : mergedItems) {
            productDisplay.getItems().add(merged);
            List<FunCartDisplayItem> mergedTopup = mergedTopupItems.get(merged.getListingId());
            if (mergedTopup != null) {
                productDisplay.getItems().addAll(mergedTopup);
            }
        }
    }

    @Override
    public CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session) {
        final FunCartDisplay cartVm = constructCartDisplay(channel, session.getId());
        final CheckoutDisplay checkout = new CheckoutDisplay();

        checkout.setCartId(cartVm.getCartId());
        checkout.setTotalQty(cartVm.getTotalQty());
        checkout.setTotalMainQty(cartVm.getTotalMainQty());
        checkout.setCmsProducts(cartVm.getCmsProducts());
        checkout.setPayMethod(cartVm.getPayMethod());
        checkout.setPaymentTypeLabel("");
        if (StringUtils.isNotEmpty(checkout.getPayMethod())) {
            checkout.setMessage("");
        }

        final boolean hasBookingFee = false;
        checkout.setHasBookingFee(hasBookingFee);
        checkout.setBookFeeMode("");

        BigDecimal grandTotal = cartVm.getTotal();

        // TODO remove this
        checkout.setWaivedTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        checkout.setTotal(grandTotal);
        checkout.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal));

        return checkout;
    }

    @Override
    @Transactional
    public ApiResult<KioskTransactionEntity> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets) {
        final String sessionId = session.getId();
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            return new ApiResult<>(ApiErrorCodes.NoCartFound);
        }

        ApiResult<KioskTransactionEntity> result = new ApiResult<>(ApiErrorCodes.General);
        if (armService.isReady(null, channel.code, savedCart.getCartId()) && armService.init(null, channel.code, savedCart.getCartId())) {
            result = doCheckout(channel, savedCart, sessionId);
        } else {
            log.error("init robotic arm failed");
        }

        if (result.isSuccess()) {
            final KioskTransactionEntity txn = result.getData();
            saveTransactionInSession(channel, session, txn);
        }

        return result;
    }

    @Transactional
    private ApiResult<KioskTransactionEntity> doCheckout(StoreApiChannels channel, KioskFunCart cart, String sessionId) {

        ApiResult<KioskTransactionEntity> apiResult = new ApiResult<>(ApiErrorCodes.General);

        try {
            final Date now = new Date();
            final String receiptNumber = ProjectUtils.generateReceiptNumber(now);
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            AxCart axCart = this.getAxCart(channel, kioskInfo, sessionId);

            List<KioskTransactionEntity> tranList = transactionDao.getTransactionByAxCartIdByOrderByIdDesc(axCart.getId());
            KioskTransactionEntity tran = null;
            if (tranList.size() > 0) {
                tran = tranList.get(0);
            }
            if (tran == null) {
                tran = new KioskTransactionEntity();
                tran.setKiosk(kioskInfo);
                tran.setReceiptNumber(receiptNumber);

                tran.setCurrency("SGD");
                tran.setAxReceiptNumber("");
                tran.setAxCartId(axCart.getId());
                tran.setAxReceiptId(kioskInfo.getCurrentReceiptSequence());

            } else {
                tran.setReceiptNumber(receiptNumber);
                for (KioskTransactionItemEntity item : tran.getItems()) {
                    this.transactionItemDao.delete(item);
                }
                tran.setItems(new HashSet<>());
                tran.setAxReceiptId(kioskInfo.getCurrentReceiptSequence());
            }
            if (tran.getItems().size() > 0) {
                for (KioskTransactionItemEntity item : tran.getItems()) {
                    this.transactionItemDao.delete(item);
                }
                tran.setItems(new HashSet<>());
            }

            BigDecimal gstAmount = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;

            for (AxCartLine axCartLine : axCart.getAxCartLineList()) {
                KioskTransactionItemEntity item = new KioskTransactionItemEntity();
                FunCartItem funCartItem = cart.getItems().stream().filter(i -> axCartLine.getLineId().equals(i.getLineId())).findAny().orElse(new FunCartItem());

                item.setListingId(axCartLine.getProductId());
                item.setQuantity(axCartLine.getQuantity());
                if (axCartLine.getDiscountLineList() != null && axCartLine.getDiscountLineList().size() > 0) {
                    item.setAmount(new BigDecimal(axCartLine.getExtendedPrice()));
                } else {
                    item.setAmount(new BigDecimal(axCartLine.getPrice()));
                }
                item.setItemId(axCartLine.getItemId());
                item.setListingId(axCartLine.getProductId());
                item.setParentProductId(funCartItem.getParentListingId());
                item.setTran(tran);
                item.setProductName(funCartItem.getCmsProductName());
                item.setAxProductName(funCartItem.getName());
                item.setEventLineId(funCartItem.getEventLineId());
                item.setEventGroupId(funCartItem.getEventGroupId());
                item.setSelectedEventDate(funCartItem.getSelectedEventDate());
                item.setSubTotal(new BigDecimal(axCartLine.getTotalAmount()));

                gstAmount = gstAmount.add(new BigDecimal(axCartLine.getTaxAmount()));
                totalAmount = totalAmount.add(new BigDecimal(axCartLine.getTotalAmount()));
                tran.getItems().add(item);
            }

            tran.setGstAmount(gstAmount);
            tran.setTotalAmount(totalAmount);
            tran.setBeginDateTime(new Date());
            tran.setEndDateTime(new Date());
            transactionDao.save(tran);
            apiResult = new ApiResult<>(true, "", "", tran);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            apiResult = new ApiResult<>(ApiErrorCodes.General);
        }

        return apiResult;
    }

    private AxCartTicketListCriteria getAxCartTicketListCriteria(KioskInfoEntity kioskInfo, FunCartItem item, AxCartLine line) throws ParseException {
        AxCartTicketListCriteria criteria = new AxCartTicketListCriteria();
        criteria = new AxCartTicketListCriteria();

        if (item.isCapacity()) {
            Date eventDate = NvxDateUtils.parseAxDate(item.getSelectedEventDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            criteria.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
            criteria.setEventGroupId(item.getEventGroupId());
            criteria.setEventLineId(item.getEventLineId());
        } else {
            criteria.setEventDate(NvxDateUtils.formatDate(Calendar.getInstance(TimeZone.getDefault()).getTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
            criteria.setEventGroupId("");
            criteria.setEventLineId("");
        }

        criteria.setItemId(item.getProductCode());
        criteria.setProductId(item.getListingId());
        criteria.setLineId(item.getLineId());
        criteria.setQty(item.getQty().toString());
        criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
        criteria.setStaffId(kioskInfo.getUserId());
        criteria.setTransactionId(kioskInfo.getCurrentCartId());

        return criteria;
    }

    /**
     * @Override public ApiResult<String> cancelCheckout(StoreApiChannels
     *           channel, HttpSession session) { final KioskTransactionEntity
     *           txnInSession = getTransactionInSession(channel, session); if
     *           (txnInSession == null) { return new
     *           ApiResult<>(ApiErrorCodes.NoTransactionFoundInSession); }
     *
     *           final KioskTransactionEntity txn =
     *           this.bookingRepository.getStoreTransactionByReceipt(channel,
     *           txnInSession.getReceiptNumber()); if (txn == null) { return new
     *           ApiResult<>(ApiErrorCodes.NoTransactionFound); }
     *
     *           txn.setStage("Canceled"); // TODO Perform additional
     *           cancelation logic: release tickets, etc.
     *
     *           clearTransactionInSession(channel, session);
     *
     *           return new ApiResult<>(true, "", "", ""); }
     **/

    @Override
    public KioskTransactionEntity getTransactionByReceipt(StoreApiChannels channel, String receiptNumber) {
        return transactionDao.getTransactionByReceipt(receiptNumber);
    }

    @Override
    public void saveTransaction(StoreApiChannels channel, KioskTransactionEntity tran) {
        this.transactionDao.save(tran);
    }

    @Override
    public KioskTransactionEntity getTransactionBySession(StoreApiChannels channel, HttpSession session) {
        return getTransactionInSession(channel, session);
    }

    @Override
    public ReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session) {
        final KioskTransactionEntity txn = getTransactionInSession(channel, session);
        if (txn == null) {
            return null;
        }

        return null; // getReceiptForDisplay(txn);
    }

    @Override
    public ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber) {
        final StoreTransaction txn = null; // bookingRepository.getStoreTransactionByReceipt(channel,
        // receiptNumber);
        if (txn == null) {
            return null;
        }

        return getReceiptForDisplay(txn);
    }

    @Override
    public ReceiptDisplay getReceiptForDisplay(StoreTransaction txn) {
        final ReceiptDisplay rd = new ReceiptDisplay();

        Integer totalMainQty = 0;
        Integer totalQty = 0;
        BigDecimal total = BigDecimal.ZERO;

        final Map<String, FunCartDisplayCmsProduct> cmsProductMap = new HashMap<>();
        for (StoreTransactionItem item : txn.getItems()) {
            final String productId = item.getCmsProductId();
            FunCartDisplayCmsProduct productDisplay = cmsProductMap.get(productId);
            if (productDisplay == null) {
                productDisplay = new FunCartDisplayCmsProduct();
                productDisplay.setName(item.getCmsProductName());
                productDisplay.setId(productId);
                cmsProductMap.put(productId, productDisplay);
            }

            final List<FunCartDisplayItem> itemsForProduct = productDisplay.getItems();
            final FunCartDisplayItem itemDisplay = new FunCartDisplayItem();
            itemDisplay.setListingId(item.getListingId());
            itemDisplay.setProductCode(item.getProductCode());
            itemDisplay.setIsTopup(item.isTopup());
            itemDisplay.setParentListingId(item.getParentListingId());
            itemDisplay.setType(item.getType());
            itemDisplay.setName(item.getName());
            itemDisplay.setDescription(item.getDisplayDetails());
            itemDisplay.setQty(item.getQty());
            itemDisplay.setPrice(item.getUnitPrice());
            itemDisplay.setPriceText(NvxNumberUtils.formatToCurrency(item.getUnitPrice(), "S$ "));
            itemDisplay.setTotal(item.getSubtotal());
            itemDisplay.setTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getTotal(), "S$ "));
            itemDisplay.setDiscountTotal(item.getDiscountTotal());
            itemDisplay.setDiscountTotalText(NvxNumberUtils.formatToCurrency(itemDisplay.getDiscountTotal(), "S$ "));
            itemDisplay.setDiscountLabel(item.getDiscountLabel());

            totalMainQty += itemDisplay.getQty();
            totalQty += itemDisplay.getQty();
            total = total.add(itemDisplay.getTotal());

            itemsForProduct.add(itemDisplay);
        }

        final List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();
        for (FunCartDisplayCmsProduct productDisplay : cmsProductMap.values()) {
            productDisplay.reorganiseItems();
            cmsProducts.add(productDisplay);

            BigDecimal subtotal = BigDecimal.ZERO;
            for (FunCartDisplayItem itemDisplay : productDisplay.getItems()) {
                subtotal = subtotal.add(itemDisplay.getTotal());
            }

            productDisplay.setSubtotal(subtotal);
            productDisplay.setSubtotalText(NvxNumberUtils.formatToCurrency(subtotal, "S$ "));
        }

        rd.setCmsProducts(cmsProducts);
        rd.setTotalMainQty(totalMainQty);
        rd.setTotalQty(totalQty);
        rd.setTotal(total);
        rd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        final CustomerDetails customer = new CustomerDetails();
        customer.setDob(txn.getCustDob());
        customer.setEmail(txn.getCustEmail());
        customer.setMobile(txn.getCustMobile());
        customer.setName(txn.getCustName());
        customer.setNationality(txn.getCustNationality());
        customer.setPaymentType(txn.getPaymentType());
        customer.setReferSource(txn.getCustReferSource());
        customer.setSubscribed(txn.isCustSubscribe());

        customer.setIdType(txn.getCustIdType());
        customer.setIdNo(txn.getCustIdNo());
        customer.setSalutation(txn.getCustSalutation());
        customer.setCompanyName(txn.getCustCompanyName());

        rd.setCustomer(customer);

        rd.setDateOfPurchase(NvxDateUtils.formatDate(txn.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        rd.setReceiptNumber(txn.getReceiptNumber());
        rd.setPaymentType(txn.getPaymentType());
        rd.setTransStatus(txn.getStage());
        rd.setTotalTotal(rd.getTotal());
        rd.setTotalTotalText(rd.getTotalText());

        return rd;
    }

    @Override
    @Transactional
    public ApiResult<KioskTransactionEntity> startCartCheckout(StoreApiChannels channel, HttpSession session, String signature) {
        // TODO This is simplified validation of sale completion. May need to
        // add more complex validation.
        KioskInfoEntity kiosk = this.kioskManagementService.getKioskInfo(channel);
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(kiosk.getCurrentCartId());
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        } else {
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final String sid = session.getId();
        final KioskFunCart cart = (KioskFunCart) cartManager.getCart(channel, sid);

        final ApiResult<KioskTransactionEntity> result = startCartCheckout(channel, tran, cart, sid, signature);
        if (result.isSuccess()) {
            final KioskTransactionEntity txn = result.getData();
            saveTransactionInSession(channel, session, txn);
        }

        return result;
    }

    @Transactional
    private ApiResult<KioskTransactionEntity> startCartCheckout(StoreApiChannels channel, KioskTransactionEntity tran, KioskFunCart funCart, String sessionId, String signature) {
        DateTime now = DateTime.now();
        tran.setAxCartCheckoutDateTime(now.toDate());
        Set<KioskTransactionPaymentEntity> payments = tran.getPayments();
        if (payments != null && payments.size() > 0) {
            KioskTransactionPaymentEntity payment = payments.stream().filter(p -> "00".equals(p.getResponseCode())).findAny().orElse(null);
            if (payment != null) {
                if (StringUtils.isBlank(signature)) {
                    payment.setSignatureRequired(Boolean.FALSE);
                } else {
                    payment.setSignatureRequired(Boolean.TRUE);
                    payment.setSignature(signature);
                }
            }
        } else {
            log.error("no payment data found for transactioin " + tran.getId());
        }
        try {

            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            String cartId = kioskInfo.getCurrentCartId();

            AxCart axCart = this.getAxCart(channel, kioskInfo, sessionId);
            for (AxTenderLine axTenderLine : axCart.getAxTenderLineList()) {

                KioskVoidTenderLineRequest voidRequest = new KioskVoidTenderLineRequest();
                voidRequest.setTenderLineId(axTenderLine.getTenderLineId());
                voidRequest.setCartId(cartId);
                this.axStarKioskService.apiKioskVoidTenderLine(channel, voidRequest);
            }
            addTenderLine(channel, tran, funCart);
            AxStarServiceResult<KioskCartCheckoutStartResponse> startCheckoutResult = this.startCheckout(channel, kioskInfo, funCart, axCart);
            if (startCheckoutResult.isSuccess()) {
                log.debug("AX returned tickets" + startCheckoutResult.getData().getAxCartRetailTicketTableEntityList().size());
                for (AxCartRetailTicketTableEntity axTicket : startCheckoutResult.getData().getAxCartRetailTicketTableEntityList()) {
                    tran.getTickets().add(this.getTicket(axTicket, tran));
                }
                KioskCartCheckoutRequest cartCheckoutRequest = new KioskCartCheckoutRequest();
                cartCheckoutRequest.setCartId(cartId);
                cartCheckoutRequest.setReceiptNumberSequence(kioskInfo.getCurrentReceiptSequence().toString());
                cartCheckoutRequest.setReceiptEmail("");

                AxStarServiceResult<AxSalesOrder> cartCheckoutResult = axStarKioskService.apiKioskCartCheckout(channel, cartCheckoutRequest);
                if (cartCheckoutResult.isSuccess()) {

                    tran.setAxReceiptNumber(cartCheckoutResult.getData().getReceiptId());

                }
                tran.setBeginDateTime(new Date());
                this.transactionDao.save(tran);
            }
        } catch (Exception e) {
            log.error("Error encountered generating eTickets.", e);
            tran.setStatus(KioskTransactionStatus.AX_CHECKOUT_FAILED.getCode());
            tran.setBeginDateTime(new Date());
            tran.setEndDateTime(new Date());
            this.transactionDao.save(tran);
            return new ApiResult<>(ApiErrorCodes.General);
        }

        return new ApiResult<>(true, "", "", tran);
    }

    @Override
    @Transactional
    public ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, HttpSession session) {
        // TODO This is simplified validation of sale completion. May need to
        // add more complex validation.
        KioskInfoEntity kiosk = this.kioskManagementService.getKioskInfo(channel);
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(kiosk.getCurrentCartId());
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        } else {
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final String sid = session.getId();
        final KioskFunCart cart = (KioskFunCart) cartManager.getCart(channel, sid);

        final ApiResult<KioskTransactionEntity> result = completeSale(channel, tran, cart, sid);
        if (result.isSuccess()) {
            // axStarKioskService.apiCartDeleteEntireCart(channel,
            // cart.getAxCart().getId(), cart.getAxCustomerId());

            cartManager.removeCart(channel, sid);
            session.invalidate();
        }

        return result;
    }

    private AxStarServiceResult<AxCart> addTenderLine(StoreApiChannels channel, KioskTransactionEntity tran, KioskFunCart cart) throws Exception {
        AxStarServiceResult<AxCart> addTenderLineResult = null;
        KioskTransactionPaymentEntity payment = tran.getPayments().stream().filter(p -> "00".equals(p.getResponseCode())).findAny().orElse(null);
        if (payment != null) {
            KioskAddTenderLineRequest addTenderLineRequest = new KioskAddTenderLineRequest();
            addTenderLineRequest.setCartId(tran.getAxCartId());
            addTenderLineRequest.getAxCartTenderLine().setAmount(this.constructCartDisplay(channel, cart).getTotal().toPlainString());
            addTenderLineRequest.getAxCartTenderLine().setCurrency("SGD");
            addTenderLineRequest.getAxCartTenderLine().setTenderLineId("");
            String hostLabel = payment.getHostLabel();
            log.debug("host label : " + hostLabel);
            KioskTenderType tenderType = KioskTenderType.getTenderType(hostLabel);
            if (tenderType != null) {
                addTenderLineRequest.getAxCartTenderLine().setTenderTypeId(tenderType.getValue());
            }

            addTenderLineResult = axStarKioskService.apiKioskAddTenderLine(channel, addTenderLineRequest);
            if (!addTenderLineResult.isSuccess()) {
                log.error("add tender line failed" + addTenderLineResult.getError().getMessage());
                throw new Exception("add tender line failed" + addTenderLineResult.getError().getMessage());
            } else {
                cart.setKioskAxCart(addTenderLineResult.getData());
            }
        }

        return addTenderLineResult;
    }

    private Date formatAxDate(String inputDate) throws ParseException {
        Date date = null;
        if (!StringUtils.isBlank(inputDate) && inputDate.length() > 19) {
            date = NvxDateUtils.parseAxDate(inputDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_MSEC_FORMAT);
        } else {
            // "yyyy-MM-hhThh:mm:ss"
            date = NvxDateUtils.parseAxDate(inputDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);
        }

        return date;
    }

    private String getTokenData(AxCartRetailTicketTableEntity axTicket) {
        String tokenData = "";
        KioskTicketTokenList tokenList = new KioskTicketTokenList();
        for (AxPrintTokenEntity token : axTicket.getTokenList()) {
            String key = token.getTokenKey();
            String value = token.getValue();
            KioskAxTicketToken ticketToken = KioskAxTicketToken.toEnum(key);
            if (ticketToken != null) {
                switch (ticketToken) {
                case BarcodeEndDate:
                    tokenList.setBarcodeEndDate(value);
                    break;
                case BarcodePLU:
                    tokenList.setBarcodePLU(value);
                    break;
                case BarcodeStartDate:
                    tokenList.setBarcodeStartDate(value);
                    break;
                case CustomerName:
                    tokenList.setCustomerName(value);
                    break;
                case Description:
                    tokenList.setDescription(value);
                    break;
                case DiscountApplied:
                    tokenList.setDiscountApplied(value);
                    break;
                case DisplayName:
                    tokenList.setDisplayName(value);
                    break;
                case EventDate:
                    tokenList.setEventDate(value);
                    break;
                case EventName:
                    tokenList.setEventName(value);
                    break;
                case Facilities:
                    tokenList.setFacilities(value);
                    break;
                case FacilityAction:
                    tokenList.setFacilityAction(value);
                    break;
                case ItemNumber:
                    tokenList.setItemNumber(value);
                    break;
                case LegalEntity:
                    tokenList.setLegalEntity(value);
                    break;
                case MixMatchDescription:
                    tokenList.setMixMatchDescription(value);
                    break;
                case MixMatchPackageName:
                    tokenList.setMixMatchPackageName(value);
                    break;
                case OperationIds:
                    tokenList.setOperationIds(value);
                    break;
                case PackageItem:
                    tokenList.setPackageItem(value);
                    break;
                case PackageItemName:
                    tokenList.setPackageItemName(value);
                    break;
                case PaxPerTicket:
                    tokenList.setPaxPerTicket(value);
                    break;
                case PriceType:
                    tokenList.setPriceType(value);
                    break;
                case PrintLabel:
                    tokenList.setPrintLabel(value);
                    break;
                case ProductImage:
                    tokenList.setProductImage(value);
                    break;
                case ProductOwner:
                    tokenList.setProductOwner(value);
                    break;
                case ProductSearchName:
                    tokenList.setProductSearchName(value);
                    break;
                case ProductVarColor:
                    tokenList.setProductVarColor(value);
                    break;
                case ProductVarConfig:
                    tokenList.setProductVarConfig(value);
                    break;
                case ProductVarSize:
                    tokenList.setProductVarSize(value);
                    break;
                case ProductVarStyle:
                    tokenList.setProductVarStyle(value);
                    break;
                case PublishPrice:
                    tokenList.setPublishPrice(value);
                    break;
                case PackagePrintLabel:
                    tokenList.setPackagePrintLabel(value);
                    break;
                case TicketCode:
                    tokenList.setTicketCode(value);
                    break;
                case TicketNumber:
                    tokenList.setTicketNumber(value);
                    break;
                case TicketType:
                    tokenList.setTicketType(value);
                    break;
                case TicketValidity:
                    tokenList.setTicketValidity(value);
                    break;
                case TicketLineDesc:
                    tokenList.setTicketLineDesc(value);
                    break;
                case UsageValidity:
                    tokenList.setUsageValidity(value);
                    break;
                case ValidityEndDate:
                    tokenList.setValidityEndDate(value);
                    break;
                case ValidityStartDate:
                    tokenList.setValidityStartDate(value);
                    break;
                case TermAndCondition:
                    tokenList.setTermAndCondition(value);
                    break;
                case Disclaimer:
                    tokenList.setDisclaimer(value);
                    break;
                default:
                    break;
                }
            }
        }
        tokenData = JsonUtil.jsonify(tokenList);

        return tokenData;
    }

    private KioskTicketEntity getTicket(AxCartRetailTicketTableEntity axTicket, KioskTransactionEntity tran) throws Exception {

        KioskTicketEntity ticket = tran.getTickets().stream().filter(t -> t.getTicketCode().equals(axTicket.getTicketCode())).findAny().orElse(new KioskTicketEntity());
        log.debug("ticket item id : " + ticket.getItemId());
        KioskTransactionItemEntity item = tran.getItems().stream().filter(i -> i.getItemId().equals(axTicket.getItemId())).findAny().orElse(new KioskTransactionItemEntity());
        ticket.setCreatedDateTime(this.formatAxDate(axTicket.getCreatedDateTime()));
        ticket.setEventDate(this.formatAxDate(axTicket.getEventDate()));
        ticket.setStartDate(this.formatAxDate(axTicket.getStartDate()));
        ticket.setEndDate(this.formatAxDate(axTicket.getEndDate()));

        ticket.setEventGroupId(axTicket.getEventGroupId());
        ticket.setEventLineId(axTicket.getEventLineId());
        ticket.setItemId(axTicket.getItemId());
        ticket.setModifiedDateTime(this.formatAxDate(axTicket.getModifiedDateTime()));
        ticket.setOpenValidityId(axTicket.getOpenValidityId());
        ticket.setProductName(item.getProductName());
        ticket.setAxProductName(axTicket.getProductName());
        ticket.setQrigTicketCode(axTicket.getOrigTicketCode());
        ticket.setQuantity(String.valueOf(new BigDecimal(axTicket.getQtyGroup()).intValue()));
        ticket.setReceiptId(axTicket.getReceiptId());
        ticket.setShiftId(axTicket.getShiftId());

        ticket.setStartDate(this.formatAxDate(axTicket.getStartDate()));
        ticket.setTicketCode(axTicket.getTicketCode());
        ticket.setTicketTableId(axTicket.getTicketTableId());
        ticket.setTransactionId(axTicket.getTransactionId());
        ticket.setTransDate(this.formatAxDate(axTicket.getTransDate()));
        ticket.setUsageValidityId(axTicket.getUsageValidityId());
        ticket.setTicketNumber(axTicket.getTicketTableId());
        ticket.setTemplateName(axTicket.getTemplateName());
        ticket.setStatus(KioskTicketPrintStatus.INIT.getCode());
        ticket.setTokenData(this.getTokenData(axTicket));

        if (axTicket.isNeedsActivation()) {
            final TicketData rawData = new TicketData();
            rawData.setFacilities(new ArrayList<>());
            for (AxFacilityClassEntity axFacility : axTicket.getFacilities()) {
                if (StringUtils.isBlank(axFacility.getFacilityAction()) || StringUtils.isBlank(axFacility.getFacilityId())) {
                    log.error("empty falicty action and facility id");
                }
                Facility facility = new Facility();
                facility.setFacilityAction(new Integer(Optional.ofNullable(axFacility.getFacilityAction()).orElse("0")));
                facility.setFacilityId(axFacility.getFacilityId());
                facility.setOperationIds(axFacility.getOperationIds());
                rawData.getFacilities().add(facility);

            }
            rawData.setQuantity(1);
            rawData.setStartDate(NvxDateUtils.formatDate(ticket.getStartDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
            rawData.setEndDate(NvxDateUtils.formatDate(ticket.getEndDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
            rawData.setTicketCode(axTicket.getTicketCode());
            ticket.setCodeData(JsonUtil.jsonify(rawData));
            ticket.setBarcodeBase64(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)));
        } else {
            ticket.setThirdParty(Boolean.TRUE);
            ticket.setCodeData(axTicket.getTicketCode());
        }
        ticket.setTran(tran);
        return ticket;
    }

    private AxStarServiceResult<KioskCartCheckoutStartResponse> startCheckout(StoreApiChannels channel, KioskInfoEntity kioskInfo, KioskFunCart funCart, AxCart axCart)
            throws ParseException {
        KioskCartCheckoutStartRequest cartCheckoutStartRequest = new KioskCartCheckoutStartRequest();

        for (AxCartLine cartLine : axCart.getAxCartLineList()) {
            FunCartItem item = funCart.getItems().stream().filter(i -> i.getLineId().equals(cartLine.getLineId())).findAny().orElse(null);
            if (item != null) {
                cartCheckoutStartRequest.getAxCartTicketListCriteria().add(getAxCartTicketListCriteria(kioskInfo, item, cartLine));
            } else {
                this.voidAxCartLine(channel, cartLine, funCart, kioskInfo);
            }

        }

        AxStarServiceResult<KioskCartCheckoutStartResponse> cartCheckoutStartResult = axStarKioskService.apiKioskCartCheckoutStart(channel, cartCheckoutStartRequest);
        return cartCheckoutStartResult;
    }

    @Transactional
    private ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, KioskTransactionEntity tran, KioskFunCart funCart, String sessionId) {
        DateTime now = DateTime.now();
        tran.setAxCartCheckoutDateTime(now.toDate());

        try {

            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            String cartId = kioskInfo.getCurrentCartId();

            AxCart axCart = funCart.getKioskAxCart();// this.getAxCart(channel,
            // kioskInfo, sessionId);
            this.updateDiscountLines(channel, kioskInfo, axCart.getAxCartLineList(), KioskDiscountStatus.ISSUED);

            KioskGetReceiptsRequest kioskGetReceiptsRequest = new KioskGetReceiptsRequest();
            kioskGetReceiptsRequest.setTransactionId(cartId);
            kioskGetReceiptsRequest.setShiftId(kioskInfo.getCurrentShiftSequence().toString());

            AxStarServiceResult<KioskGetReceiptsResponse> getReceiptsResult = axStarKioskService.apiKioskGetReceipts(channel, kioskGetReceiptsRequest);

            KioskCartCheckoutCompleteRequest cartCheckoutCompleteRequest = new KioskCartCheckoutCompleteRequest();
            cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setDataLevelValue("4");
            cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setTransactionId(cartId);
            AxStarServiceResult<KioskCartCheckoutCompleteResponse> cartCheckoutCompleteResult = axStarKioskService.apiKioskCartCheckoutComplete(channel,
                    cartCheckoutCompleteRequest);

            if (getReceiptsResult.isSuccess() && cartCheckoutCompleteResult.isSuccess()) {
                tran.setStatus(KioskTransactionStatus.AX_CHECKOUT_SUCCESSFUL.getCode());
            } else {
                tran.setStatus(KioskTransactionStatus.AX_CHECKOUT_FAILED.getCode());
            }
            tran.setEndDateTime(new Date());
            this.transactionDao.save(tran);
            kioskInfo.setCurrentReceiptSequence(kioskInfo.getCurrentReceiptSequence() + 1);
            kioskInfo.setCurrentCartId("");
            this.kioskManagementService.updateKioskInfo(kioskInfo);
            syncService.publishTransaction(tran);
        } catch (Exception e) {
            log.error("Error encountered generating eTickets.", e);
            tran.setStatus(KioskTransactionStatus.AX_CHECKOUT_FAILED.getCode());
            tran.setEndDateTime(new Date());
            this.transactionDao.save(tran);
            return new ApiResult<>(ApiErrorCodes.General);
        }

        return new ApiResult<>(true, "", "", tran);
    }

    @Override
    public KioskTransactionEntity updateTransactionTicketPrintingStatus(String receiptNumber, Boolean ticketPrinted) {
        KioskTransactionEntity tran = this.transactionDao.getTransactionByReceipt(receiptNumber);
        if (tran == null) {
            log.error("updateTransactionTicketPrintingStatus failed transaction not found with receipt no " + receiptNumber);
        } else {
            tran.setStatus(KioskTransactionStatus.TICKET_ISSUED.getCode());
            if (!ticketPrinted) {
                tran.setStatus(KioskTransactionStatus.TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode());
            }
            transactionDao.save(tran);
        }
        return tran;
    }

    @Override
    public Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, KioskInfoEntity kiosk) {

        Boolean updated = Boolean.FALSE;

        final String sessionId = session.getId();
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = null;
        AxCart axCart = savedCart.getKioskAxCart();
        if (axCart != null) {

            tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(axCart.getId());
            if (tranList.size() > 0) {
                tran = tranList.get(0);
            }
            if (tran != null) {
                tran.setAllTicketsPrinted(Boolean.TRUE);
                if (printTicketResult.hasErrors()) {
                    tran.setAllTicketsPrinted(Boolean.FALSE);
                }
                tran.setEndDateTime(new Date());

            }
        }

        KioskUpdatePrintStatusRequest request = new KioskUpdatePrintStatusRequest();
        AxCartTicketUpdatePrintStatusCriteria criteria = new AxCartTicketUpdatePrintStatusCriteria();
        request.setAxCartTicketUpdatePrintStatusCriteria(criteria);

        KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
        criteria.setDataAreaId("sdc");
        criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
        criteria.setStaffId(kioskInfo.getUserId());
        criteria.setStoreId(kioskInfo.getStoreNumber());
        criteria.setTerminalId(kioskInfo.getTerminalNumber());
        criteria.setTransactionId(savedCart.getKioskAxCart().getId());

        AxPrintHistory history = null;
        for (KioskTicketStatus ticket : printTicketResult.getPrintHistory()) {
            history = new AxPrintHistory();
            history.setErrorMessage(ticket.getErrorMessage());
            history.setId(ticket.getId());
            if (CollectionUtils.isNotEmpty(ticket.getStatusCodes())) {
                history.setStatusCodes(StringUtils.join(ticket.getStatusCodes()));
            }
            history.setSuccess(ticket.getSuccess());
            criteria.getAxPrintHistory().add(history);
            KioskTicketEntity entity = tran.getTickets().stream().filter(t -> t.getTicketCode().equals(ticket.getId())).findAny().orElse(null);
            if (ticket.getSuccess()) {
                entity.setStatus(KioskTicketPrintStatus.PRINTED.getCode());
            } else {
                entity.setStatus(KioskTicketPrintStatus.NOTPRINTED.getCode());
            }
        }

        AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> updatePrintedStatusResult = this.axStarKioskService.apiKioskCartTicketUpdatePrintStatus(channel, request);
        if (updatePrintedStatusResult.isSuccess()) {
            if (StringUtils.isBlank(updatePrintedStatusResult.getData().getErrorMessage())) {
                updated = Boolean.TRUE;

            } else {
                log.error("apiKioskCartTicketUpdatePrintStatus failed" + updatePrintedStatusResult.getData().getErrorMessage());
            }
        }

        if (printTicketResult.hasErrors()) {
            KioskBlacklistTicketRequest blacklistRequest = null;
            for (KioskTicketStatus ticket : printTicketResult.getPrintHistory()) {
                if (ticket.getSuccess()) {
                    blacklistRequest = new KioskBlacklistTicketRequest();
                    AxUpdateRetailTicketStatusCriteria blacklistCriteria = blacklistRequest.getBlacklistTicketCriteria();
                    blacklistCriteria.setDataLevelValue("4");
                    blacklistCriteria.setReasonCode("BlackList");
                    blacklistCriteria.setStaffId(kioskInfo.getUserId());
                    blacklistCriteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                    blacklistCriteria.setStoreId(kioskInfo.getStoreNumber());
                    blacklistCriteria.setTerminalId(kioskInfo.getTerminalNumber());
                    blacklistCriteria.setTicketCode(ticket.getId());
                    blacklistCriteria.setTicketStatus(KioskAxTicketStatus.INACTIVE.getCode());
                    blacklistCriteria.setTransactionId(axCart.getId());
                    AxStarServiceResult<AxUpdatedTicketStatusEntity> blacklistResult = this.axStarKioskService.apiKioskBlacklistTickets(channel, blacklistRequest);
                    if (!blacklistResult.isSuccess() || (blacklistResult.isSuccess() && "1".equals(blacklistResult.getData().getIsError()))) {
                        log.error("failed black list ticket for ticket code " + ticket.getId());
                    } else {
                        KioskTicketEntity entity = tran.getTickets().stream().filter(t -> t.getTicketCode().equals(ticket.getId())).findAny().orElse(null);
                        entity.setStatus(KioskTicketPrintStatus.BLACKLISTED.getCode());
                    }
                }
            }
            if (!armService.rejectTicket(kioskInfo, channel.code, tranList.stream().findFirst().get().getReceiptNumber())) {
                log.error("robotic arm failed to reject tickets");
            }
        } else {
            if (!armService.issueTicket(kioskInfo, channel.code, tranList.stream().findFirst().get().getReceiptNumber())) {
                log.error("robotic arm failed to issue tickets");
            }
        }
        this.transactionDao.save(tran);
        
        zabbixPrinterService.checkTicketPrinterStatus(kiosk, channel.code);
        
        return updated;
    }

    @Override
    public ApiResult<FunCartDisplay> updateCartTopup(StoreApiChannels channel, KioskFunCart topupCartToUpdate, String sessionId) {
        ApiResult<FunCartDisplay> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
            if (savedCart == null) {
                return new ApiResult<>(ApiErrorCodes.NoCartFound);
            }
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);

            // update child product same QTY as parent
            for (FunCartItem topupFunCartItem : topupCartToUpdate.getItems()) {
                // topupFunCartItem.setQty(0);
                List<FunCartItem> parentFunCartItems = savedCart.getItems().stream().filter(i -> topupFunCartItem.getParentListingId().equals(i.getListingId()))
                        .collect(Collectors.toList());
                if (parentFunCartItems != null && parentFunCartItems.size() > 0) {

                    Integer parentQty = parentFunCartItems.stream().mapToInt(i -> i.getQty()).sum();
                    if (topupFunCartItem.getQty() - parentQty > 0) {
                        topupFunCartItem.setQty(parentQty);
                    }
                }
            }

            // find corresponding topup item to void
            List<FunCartItem> toBeRemoved = new ArrayList<>();
            List<AxCartLine> lines = new ArrayList<>();
            for (AxCartLine line : savedCart.getKioskAxCart().getAxCartLineList()) {
                FunCartItem existing = savedCart.getItems().stream().filter(i -> line.getLineId().equals(i.getLineId())).findAny().orElse(null);
                if (existing != null && existing.isTopup()) {
                    FunCartItem matched = topupCartToUpdate.getItems().stream()
                            .filter(i -> (i.getListingId() + i.getParentListingId()).equals(existing.getListingId() + existing.getParentListingId())).findAny().orElse(null);
                    if (matched != null) {
                        lines.add(line);
                        toBeRemoved.add(existing);
                    }
                }
            }

            if (lines.size() > 0) {
                AxStarServiceResult<AxCart> voidAxCartLineResult = this.voidAxCartLine(channel, lines, savedCart, kioskInfo);
                if (voidAxCartLineResult.isSuccess()) {
                    // remove the ones that has been voided from cart

                    savedCart.getItems().removeAll(toBeRemoved);
                    savedCart.setKioskAxCart(voidAxCartLineResult.getData());
                    cartManager.saveCart(channel, savedCart);
                }
            }

            apiResult = this.cartAddTopup(channel, sessionId, topupCartToUpdate);

        } catch (Exception e) {
            apiResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        }
        return apiResult;

    }

}
