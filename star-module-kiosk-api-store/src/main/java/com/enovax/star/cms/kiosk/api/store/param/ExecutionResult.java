package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExecutionResult<T> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1067996413694361149L;

    @JsonProperty
    private String processType;

    @JsonProperty
    private T result;

    @JsonProperty
    private boolean success;

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T results) {
        this.result = results;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
