package com.enovax.star.cms.kiosk.api.store.opsmodel;

import java.util.ArrayList;

/**
 * Created by tharaka on 24/09/2016.
 */
public class LoadPaperTicket {

    private String qtyUnloaded;
    private String qtyLoaded;
    private String startTicketNo;
    private String endTicketNo;

    private ArrayList<LoadPaperTicketQty> loadPaperTicketQty;

    public String getQtyUnloaded() {
        return qtyUnloaded;
    }

    public void setQtyUnloaded(String qtyUnloaded) {
        this.qtyUnloaded = qtyUnloaded;
    }

    public String getQtyLoaded() {
        return qtyLoaded;
    }

    public void setQtyLoaded(String qtyLoaded) {
        this.qtyLoaded = qtyLoaded;
    }

    public String getStartTicketNo() {
        return startTicketNo;
    }

    public void setStartTicketNo(String startTicketNo) {
        this.startTicketNo = startTicketNo;
    }

    public String getEndTicketNo() {
        return endTicketNo;
    }

    public void setEndTicketNo(String endTicketNo) {
        this.endTicketNo = endTicketNo;
    }

    public ArrayList<LoadPaperTicketQty> getLoadPaperTicketQty() {
        return loadPaperTicketQty;
    }

    public void setLoadPaperTicketQty(ArrayList<LoadPaperTicketQty> loadPaperTicketQty) {
        this.loadPaperTicketQty = loadPaperTicketQty;
    }
}
