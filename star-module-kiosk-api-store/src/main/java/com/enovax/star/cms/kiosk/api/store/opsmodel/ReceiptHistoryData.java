package com.enovax.star.cms.kiosk.api.store.opsmodel;

/**
 * Created by tharaka on 28/09/2016.
 */
public class ReceiptHistoryData {
    private int index;
    private String transId;
    private String printTime;
    private String isTicketFail;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getPrintTime() {
        return printTime;
    }

    public void setPrintTime(String printTime) {
        this.printTime = printTime;
    }

    public String getIsTicketFail() {
        return isTicketFail;
    }

    public void setIsTicketFail(String isTicketFail) {
        this.isTicketFail = isTicketFail;
    }
}
