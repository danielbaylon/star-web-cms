package com.enovax.star.cms.kiosk.api.store.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_info", schema = "dbo")
public class KioskInfoEntity {

    @Id
    @Column(name = "oid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "channel_id")
    private String channelId;

    @Column(name = "channel")
    private String channel;

    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "device_no")
    private String deviceNumber;

    @Column(name = "device_status")
    private Boolean isDeviceActivated;

    @Column(name = "store_no")
    private String storeNumber;

    @Column(name = "terminal_no")
    private String terminalNumber;

    @Column(name = "hardware_profile_id")
    private String hardwareProfileId;

    @Column(name = "cash_drawer")
    private String cashDrawer;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "jcr_node_uuid")
    private String jcrNodeUuid;

    @Column(name = "jcr_node_name")
    private String jcrNodeName;

    @Column(name = "tran_seq")
    private Integer currentTransactionSequence;

    @Column(name = "shift_seq")
    private Integer currentShiftSequence;

    @Column(name = "receipt_seq")
    private Integer currentReceiptSequence;

    @Column(name = "terminal_merchant_info")
    private String terminalMerchantNameAndLocation;

    /**
     * store id + device id + transaction sequence
     */
    @Column(name = "cart_id")
    private String currentCartId;

    @Column(name = "location")
    private String location;

    @Column(name = "placement")
    private String placement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public Boolean getIsDeviceActivated() {
        return isDeviceActivated;
    }

    public void setIsDeviceActivated(Boolean isDeviceActivated) {
        this.isDeviceActivated = isDeviceActivated;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getTerminalNumber() {
        return terminalNumber;
    }

    public void setTerminalNumber(String terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getJcrNodeUuid() {
        return jcrNodeUuid;
    }

    public void setJcrNodeUuid(String jcrNodeUuid) {
        this.jcrNodeUuid = jcrNodeUuid;
    }

    public String getJcrNodeName() {
        return jcrNodeName;
    }

    public void setJcrNodeName(String jcrNodeName) {
        this.jcrNodeName = jcrNodeName;
    }

    public Integer getCurrentTransactionSequence() {
        return currentTransactionSequence;
    }

    public void setCurrentTransactionSequence(Integer currentTransactionSequence) {
        this.currentTransactionSequence = currentTransactionSequence;
    }

    public Integer getCurrentShiftSequence() {
        return currentShiftSequence;
    }

    public void setCurrentShiftSequence(Integer currentShiftSequence) {
        this.currentShiftSequence = currentShiftSequence;
    }

    public String getHardwareProfileId() {
        return hardwareProfileId;
    }

    public void setHardwareProfileId(String hardwareProfileId) {
        this.hardwareProfileId = hardwareProfileId;
    }

    public String getCashDrawer() {
        return cashDrawer;
    }

    public void setCashDrawer(String cashDrawer) {
        this.cashDrawer = cashDrawer;
    }

    public Integer getCurrentReceiptSequence() {
        return currentReceiptSequence;
    }

    public void setCurrentReceiptSequence(Integer currentReceiptSequence) {
        this.currentReceiptSequence = currentReceiptSequence;
    }

    public String getCurrentCartId() {
        return currentCartId;
    }

    public void setCurrentCartId(String currentCartId) {
        this.currentCartId = currentCartId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTerminalMerchantNameAndLocation() {
        return terminalMerchantNameAndLocation;
    }

    public void setTerminalMerchantNameAndLocation(String terminalMerchantNameAndLocation) {
        this.terminalMerchantNameAndLocation = terminalMerchantNameAndLocation;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }
    
    

}
