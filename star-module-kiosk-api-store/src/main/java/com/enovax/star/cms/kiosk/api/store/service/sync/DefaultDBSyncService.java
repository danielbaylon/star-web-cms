package com.enovax.star.cms.kiosk.api.store.service.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketQtyEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskEventDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskInfoDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskPaperTicketEventDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixKioskManagementService;

@Service
public class DefaultDBSyncService implements IKioskDBSyncService {

    private static final Logger log = Logger.getLogger(DefaultDBSyncService.class);

    @Value("${kiosk.api.db.sync.url}")
    private String cmsAuthorServiceURL;

    @Autowired
    private IKioskInfoDao kioskDao;

    @Autowired
    private IKioskTransactionDao tranDao;

    @Autowired
    private IKioskEventDao eventDao;

    @Autowired
    private IKioskPaperTicketEventDao loadTicketDao;
    
    @Autowired
    private IKioskZabbixKioskManagementService kioskZabbixKioskManagementService;
    
    @Autowired
    private IKioskManagementService kioskManagementService;

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private RestTemplate template = new RestTemplate();

    private KioskInfoEntity getKioskInfo(KioskTransactionEntity tran) {
        KioskInfoEntity kioskInfo = null;

        if (tran.getKiosk() != null && StringUtils.isNotBlank(tran.getKiosk().getJcrNodeUuid())) {
            log.info("kiosk UUID : " + tran.getKiosk().getJcrNodeUuid());
            kioskInfo = kioskDao.getByJcrNodeUuid(tran.getKiosk().getJcrNodeUuid());
            if (kioskInfo == null) {
                kioskInfo = tran.getKiosk();
                kioskDao.saveAndFlush(kioskInfo);
            }
        } else {
            log.error("no kiosk info passed in");
        }
        return kioskInfo;
    }

    private void reset(KioskTransactionEntity tran) {
        tran.setId(null);
        if (tran.getKiosk() != null) {
            tran.getKiosk().setId(null);
        }
        for (KioskTransactionItemEntity item : tran.getItems()) {
            item.setId(null);
            item.setTran(tran);
        }
        for (KioskTicketEntity ticket : tran.getTickets()) {
            ticket.setId(null);
            ticket.setTran(tran);
        }

        for (KioskTransactionPaymentEntity payment : tran.getPayments()) {
            payment.setId(null);
            payment.setTran(tran);
        }
    }

    private void reset(KioskEventEntity event) {
        event.setId(null);
    }

    private void reset(KioskLoadPaperTicketInfoEntity event) {
        event.setId(null);

        for (KioskLoadPaperTicketQtyEntity qty : event.getTicketQtyEntitySet()) {
            qty.setId(null);
            qty.setTicketInfoEntity(event);
        }
    }

    @Override
    @Transactional
    public KioskTransactionEntity saveTransaction(KioskTransactionEntity tran) {
        reset(tran);
        KioskInfoEntity kiosk = this.getKioskInfo(tran);
        if (kiosk != null) {
            tran.setKiosk(kiosk);
            KioskTransactionEntity existing = tranDao.getTransaction(tran.getReceiptNumber(), tran.getAxCartId());
            if (existing == null) {
                tranDao.saveAndFlush(tran);
            } else {
                // should persist to another table
                log.error("duplicate transaction found with star receipt num : " + tran.getReceiptNumber() + " and ax cart id : " + tran.getAxCartId());
            }
        } else {
            log.error("transaction dropped as no kiosk info found");
        }
        return tran;
    }

    @Override
    @Transactional
    public KioskEventEntity saveEvent(KioskEventEntity event) {
        reset(event);
        return eventDao.saveAndFlush(event);
    }

    @Override
    @Transactional
    public KioskLoadPaperTicketInfoEntity saveLoadTicketEvent(KioskLoadPaperTicketInfoEntity event) {
        reset(event);
        return loadTicketDao.saveAndFlush(event);
    }

    @Override
    public void publishTransaction(KioskTransactionEntity tran) {
        final String url = this.cmsAuthorServiceURL + "/persist/tran";
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<KioskTransactionEntity> requestEntity = new HttpEntity<>(tran, headers);

                    ResponseEntity<ApiResult> response = template.postForEntity(url, requestEntity, ApiResult.class);
                    if (!response.getBody().isSuccess()) {
                        kioskZabbixKioskManagementService.sendTransDataSyncError(tran.getKiosk(), tran.getKiosk().getChannel());
                        log.error("transaction sync up failed");
                    }
                } catch (Exception e) {
                    kioskZabbixKioskManagementService.sendTransDataSyncError(tran.getKiosk(), tran.getKiosk().getChannel());
                    log.error(e.getMessage(), e);
                }
            }
        });

    }

    @Override
    public void publishEvent(KioskEventEntity event) {
        final String url = this.cmsAuthorServiceURL + "/persist/event";
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<KioskEventEntity> requestEntity = new HttpEntity<>(event, headers);

                    ResponseEntity<ApiResult> response = template.postForEntity(url, requestEntity, ApiResult.class);
                    if (!response.getBody().isSuccess()) {
                        kioskZabbixKioskManagementService.sendNonTransDataSyncError(kioskManagementService.getKioskInfo(StoreApiChannels.fromCode(event.getApiChannel())), event.getApiChannel());
                        log.error("event sync up failed");
                    }
                } catch (Exception e) {
                    kioskZabbixKioskManagementService.sendNonTransDataSyncError(kioskManagementService.getKioskInfo(StoreApiChannels.fromCode(event.getApiChannel())), event.getApiChannel());
                    log.error(e.getMessage(), e);
                }
            }
        });

    }

    @Override
    public void publishLoadTicketEvent(KioskLoadPaperTicketInfoEntity event) {
        final String url = this.cmsAuthorServiceURL + "/persist/load-ticket-event";
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<KioskLoadPaperTicketInfoEntity> requestEntity = new HttpEntity<>(event, headers);

                    ResponseEntity<ApiResult> response = template.postForEntity(url, requestEntity, ApiResult.class);
                    if (!response.getBody().isSuccess()) {
                        log.error("load ticket event sync up failed");
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        });

    }

}
