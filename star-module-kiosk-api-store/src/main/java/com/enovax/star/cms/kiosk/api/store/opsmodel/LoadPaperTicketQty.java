package com.enovax.star.cms.kiosk.api.store.opsmodel;

/**
 * Created by Win_PC on 24/09/2016.
 */
public class LoadPaperTicketQty {

    private String fromNumber;
    private String toNumber;

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }
}
