package com.enovax.star.cms.kiosk.api.store.jcr.repository.product;

import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductCategoriesProperties;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;

import info.magnolia.jcr.util.NodeUtil;

/**
 * Created by JC on 5/13/16.
 */
@Repository
public class DefaultProductJcrRepository implements IProductJcrRepository {

    private static final Logger log = LoggerFactory.getLogger(DefaultProductJcrRepository.class);

    private static final String ADMISSION_INCLUDED = CMSProductProperties.AdmissionIncluded.getPropertyName();

    @Override
    public String getAdmssionInfo(String channel, String cmsNodeNameInJcr) {
        String admission = "";
        try {
            Node prodNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + cmsNodeNameInJcr);
            if (prodNode != null) {
                if (prodNode.hasProperty(ADMISSION_INCLUDED)) {
                    admission = prodNode.getProperty(ADMISSION_INCLUDED).getString();
                }
            } else {
                log.error("NO node for CMS product with name :" + cmsNodeNameInJcr);
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return admission;
    }

    @Override
    public Boolean isProductCategoryTransportLayout(String channel, String productNodeName) {
        Boolean isTransportLayoutCategory = Boolean.FALSE;
        try {
            Node productNode = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/" + channel + "/" + productNodeName);
            Iterable<Node> relatedProductIterable = JcrRepository.query(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(),
                    JcrWorkspace.RelatedCMSProductForCategories.getNodeType(),
                    "/jcr:root/" + channel + "//element(*, mgnl:cms-product-for-category)[@relatedCMSProductUUID = '" + productNode.getIdentifier() + "']");
            Iterator<Node> relatedProductIterator = relatedProductIterable.iterator();

            if (relatedProductIterator.hasNext()) {
                Node relatedProductNode = relatedProductIterator.next();
                Node categoryNode = NodeUtil.getNearestAncestorOfType(relatedProductNode, JcrWorkspace.ProductCategories.getNodeType());

                if (categoryNode != null) {
                    String transportLayout = CMSProductCategoriesProperties.TransportLayout.getPropertyName();
                    if (categoryNode.hasProperty(transportLayout) && "true".equals(categoryNode.getProperty(transportLayout).getString())) {
                        isTransportLayoutCategory = Boolean.TRUE;
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return isTransportLayoutCategory;
    }
}
