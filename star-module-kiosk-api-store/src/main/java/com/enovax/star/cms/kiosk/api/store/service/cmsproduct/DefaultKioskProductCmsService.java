package com.enovax.star.cms.kiosk.api.store.service.cmsproduct;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CrossSellCartItem;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.kiosk.api.store.service.product.KioskServiceException;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DefaultKioskProductCmsService implements IKioskProductCmsService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private StarTemplatingFunctions starfn;
    @Autowired
    private DamTemplatingFunctions damfn;

    @Override
    public ApiResult<List<CrossSellCartItem>> getAllCrossSellCartItemsFor(FunCartDisplay kioskTypeCart, String channelCode, String productNodeName, String locale) throws KioskServiceException {

        log.info("Calling DefaultProductCmsService.getAllCrossSellCartItemsFor() .......");

        List<CrossSellCartItem> crossSellCartItems = new ArrayList<>();

        List<FunCartDisplayCmsProduct> cmsProducts = kioskTypeCart.getCmsProducts();

        for(FunCartDisplayCmsProduct cmsProduct : cmsProducts) {

            if(cmsProduct.getId().equals(productNodeName)) {

                List<FunCartDisplayItem> cmsProductItems = cmsProduct.getItems();

                Map<String, FunCartDisplayItem> axProductItems = new HashMap<>();
                Map<String, FunCartDisplayItem> topupProductItems = new HashMap<>();

                for(FunCartDisplayItem cmsProductItem : cmsProductItems) {

                    if(cmsProductItem.isTopup()) {
                        topupProductItems.put(cmsProductItem.getParentListingId(), cmsProductItem);
                    }
                }

                for(FunCartDisplayItem cmsProductItem : cmsProductItems) {

                    if(cmsProductItem.isTopup()) {
                        continue;
                    }

                    axProductItems.put(cmsProductItem.getListingId(), cmsProductItem);
                }

                for(FunCartDisplayItem cmsProductItem : axProductItems.values()) {

                    List<Node> crossSellItemNodes = starfn.getAXProductCrossSellsByListingId(channelCode, cmsProductItem.getListingId());

                    for(Node itemNode : crossSellItemNodes) {

                        CrossSellCartItem crossSellCartItem = new CrossSellCartItem();

                        crossSellCartItem.setCrossSellProductListingId(starfn.getPropertyValue(itemNode, "crossSellProductListingId", locale));
                        crossSellCartItem.setCrossSellItemImage(starfn.getPropertyValue(itemNode, "crossSellItemImage", locale));
                        crossSellCartItem.setCrossSellItemName(starfn.getPropertyValue(itemNode, "crossSellItemName", locale));
                        crossSellCartItem.setCrossSellItemDesc(starfn.getPropertyValue(itemNode, "crossSellItemDesc", locale));
                        crossSellCartItem.setCrossSellProductCode(starfn.getPropertyValue(itemNode, "crossSellProductCode", locale));

                        Node productNode = starfn.getAxProductByProductCode(channelCode, starfn.getPropertyValue(itemNode, "crossSellProductCode", locale));

                        crossSellCartItem.setProductPrice(starfn.getPropertyValue(productNode, "productPrice", locale));
                        crossSellCartItem.setTicketType(starfn.getPropertyValue(productNode, "ticketType", locale));

                        String imageIconPath = damfn.getAssetLink(starfn.getPropertyValue(itemNode, "crossSellItemImage", locale));

                        crossSellCartItem.setImageIconPath(imageIconPath);

                        FunCartDisplayItem funCartDisplayItem = topupProductItems.get(crossSellCartItem.getCrossSellProductListingId());

                        if(funCartDisplayItem != null) {
                            crossSellCartItem.setQuantity(funCartDisplayItem.getQty());
                        }
                        crossSellCartItem.setCmsProductName(cmsProduct.getName());
                        crossSellCartItem.setParentListingId(cmsProductItem.getListingId());
                        crossSellCartItem.setParentCmsProductId(cmsProduct.getId());

                        crossSellCartItems.add(crossSellCartItem);
                    }
                }
            }
        }

        return new ApiResult<>(true, "", "", crossSellCartItems);
    }
}