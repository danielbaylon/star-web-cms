package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskTransactionType {

    PURCHASE("1"), //
    B2B_REDEMPTION("2"), //
    B2C_REDEMPTION("3"), //
    RECOVERY("4"), //
    CONTINUE_PURCHASE("5"), //
    SETTLEMENT("6");

    private String code;

    KioskTransactionType(String code) {
        this.code = code;

    }

    public String getCode() {
        return this.code;
    }

}
