package com.enovax.star.cms.kiosk.api.store.service.redemption;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.KioskFunCartRedemptionDisplayItem;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;

public interface IKioskPINRedemptionService {

    ApiResult<KioskTransactionEntity> startRedemption(StoreApiChannels channel, String sessionId, String pinCode);

    FunCartDisplay constructRedeemCartDisplay(StoreApiChannels channel, String sessionId);
    
    ApiResult<KioskTransactionEntity>  checkout(StoreApiChannels channel, String sessionId, List<KioskFunCartRedemptionDisplayItem> items);
    
    ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, HttpSession session);
    
    ApiResult<KioskTransactionEntity> unlockPin(StoreApiChannels channel, HttpSession session);
    
    Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, KioskInfoEntity kiosk);
}
