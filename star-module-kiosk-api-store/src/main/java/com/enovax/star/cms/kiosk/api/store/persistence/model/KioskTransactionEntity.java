package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_tran", schema = "dbo")
public class KioskTransactionEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9103478435777708002L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oid")
    private Long id;

    @Column(name = "receipt_no")
    private String receiptNumber;

    @Column(name = "ax_receipt_id")
    private Integer axReceiptId;

    @Column(name = "ax_receipt_no")
    private String axReceiptNumber;

    @Column(name = "ax_cart_id")
    private String axCartId;

    @Column(name = "checkout_datetime")
    private Date axCartCheckoutDateTime;

    @Column(name = "begin_datetime")
    private Date beginDateTime;

    @Column(name = "end_datetime")
    private Date endDateTime;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @Column(name = "gst_amount")
    private BigDecimal gstAmount;

    @Column(name = "currency")
    private String currency = "SGD";

    @Column(name = "tran_type")
    private String type = KioskTransactionType.PURCHASE.getCode();

    @Column(name = "tickets_printed")
    private Boolean allTicketsPrinted = Boolean.FALSE;

    @Column(name = "rdp_allow_partial")
    private Boolean allowPartiallRedemption = Boolean.FALSE;

    @Column(name = "rdp_pin_code")
    private String redemptionPinCode;

    @Column(name = "status")
    private String status = KioskTransactionStatus.TRANSACTION_CREATED.getCode();

    @OneToMany(mappedBy = "tran", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<KioskTransactionItemEntity> items = new HashSet<>();

    @OneToMany(mappedBy = "tran", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<KioskTicketEntity> tickets = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "kiosk_oid")
    private KioskInfoEntity kiosk;

    @OneToMany(mappedBy = "tran", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<KioskTransactionPaymentEntity> payments = new HashSet<>();

    public KioskTransactionEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getAxReceiptNumber() {
        return axReceiptNumber;
    }

    public void setAxReceiptNumber(String axReceiptNumber) {
        this.axReceiptNumber = axReceiptNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getAllowPartiallRedemption() {
        return allowPartiallRedemption;
    }

    public void setAllowPartiallRedemption(Boolean allowPartiallRedemption) {
        this.allowPartiallRedemption = allowPartiallRedemption;
    }

    public Set<KioskTransactionItemEntity> getItems() {
        return items;
    }

    public void setItems(Set<KioskTransactionItemEntity> items) {
        this.items = items;
    }

    public KioskInfoEntity getKiosk() {
        return kiosk;
    }

    public void setKiosk(KioskInfoEntity kiosk) {
        this.kiosk = kiosk;
    }

    public String getAxCartId() {
        return axCartId;
    }

    public Boolean getAllTicketsPrinted() {
        return allTicketsPrinted;
    }

    public void setAllTicketsPrinted(Boolean allTicketsPrinted) {
        this.allTicketsPrinted = allTicketsPrinted;
    }

    public void setAxCartId(String axCartId) {
        this.axCartId = axCartId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<KioskTicketEntity> getTickets() {
        return tickets;
    }

    public void setTickets(Set<KioskTicketEntity> tickets) {
        this.tickets = tickets;
    }

    public void setPayments(Set<KioskTransactionPaymentEntity> payments) {
        this.payments = payments;
    }

    public Set<KioskTransactionPaymentEntity> getPayments() {
        return payments;
    }

    public Date getAxCartCheckoutDateTime() {
        return axCartCheckoutDateTime;
    }

    public void setAxCartCheckoutDateTime(Date axCartCheckoutDateTime) {
        this.axCartCheckoutDateTime = axCartCheckoutDateTime;
    }

    public String getRedemptionPinCode() {
        return redemptionPinCode;
    }

    public void setRedemptionPinCode(String redemptionPinCode) {
        this.redemptionPinCode = redemptionPinCode;
    }

    public Integer getAxReceiptId() {
        return axReceiptId;
    }

    public void setAxReceiptId(Integer axReceiptId) {
        this.axReceiptId = axReceiptId;
    }

    public BigDecimal getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(BigDecimal gstAmount) {
        this.gstAmount = gstAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(Date beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

}
