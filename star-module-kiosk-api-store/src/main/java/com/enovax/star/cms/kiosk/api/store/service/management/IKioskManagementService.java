package com.enovax.star.cms.kiosk.api.store.service.management;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEmployee;
import com.enovax.star.cms.commons.model.kiosk.odata.AxShift;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ScheduledMaintenance;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

import java.text.ParseException;

public interface IKioskManagementService {

    KioskInfoEntity openShift(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception;
    
    KioskInfoEntity activateDevice(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception;

    KioskInfoEntity logonForDeviceActivation(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception;

    ApiResult<AxEmployee> logon(StoreApiChannels channel);

    Boolean noneTransactionalLogon(StoreApiChannels channel, KioskInfoEntity kioskInfo);

    ApiResult<String> logoff(StoreApiChannels channel);

    Boolean noneTransactionalLogoff(StoreApiChannels channel);

    ApiResult<AxShift> closeShift(StoreApiChannels channel);

    String getTransactionId(KioskInfoEntity kioskInfo);

    String getNextTransactionId(StoreApiChannels channel, KioskInfoEntity kioskInfo);

    KioskInfoEntity getKioskInfo(StoreApiChannels channel);

    KioskInfoEntity updateKioskInfo(KioskInfoEntity kioskInfo);

    ScheduledMaintenance getOperationModeFromCMS(String channel) throws ParseException;
    
    String getTikectTemplateXMLByName(String templateName);
}
