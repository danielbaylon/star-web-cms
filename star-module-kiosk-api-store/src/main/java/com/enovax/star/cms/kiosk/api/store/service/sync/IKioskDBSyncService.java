package com.enovax.star.cms.kiosk.api.store.service.sync;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;

public interface IKioskDBSyncService {

    KioskTransactionEntity saveTransaction(KioskTransactionEntity tran);

    KioskEventEntity saveEvent(KioskEventEntity event);

    KioskLoadPaperTicketInfoEntity saveLoadTicketEvent(KioskLoadPaperTicketInfoEntity event);

    void publishTransaction(KioskTransactionEntity tran);

    void publishEvent(KioskEventEntity event);

    void publishLoadTicketEvent(KioskLoadPaperTicketInfoEntity event);

}
