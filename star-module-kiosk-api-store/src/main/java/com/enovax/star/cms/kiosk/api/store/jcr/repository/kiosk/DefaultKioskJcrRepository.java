package com.enovax.star.cms.kiosk.api.store.jcr.repository.kiosk;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.kiosk.jcr.JcrKioskInfo;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ScheduledMaintenance;

import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;

/**
 * Repositry for kiosk related data in JCR.
 *
 * @author Justin
 * @since 7 SEP 16
 */
@Repository
public class DefaultKioskJcrRepository implements IKioskJcrRepository {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskJcrRepository.class);

    private static final String PROPERTY_DEVICE_NUMBER = "deviceNumber";
    private static final String PROPERTY_TERMINAL_NUMBER = "terminalNumber";
    private static final String PROPERTY_USER_ID = "userId";
    private static final String PROPERTY_USER_PASSWORD = "userPassword";
    private static final String PROPERTY_LOCATION = "location";
    private static final String PROPERTY_SHUTDOWN_HOUR = "shutdownHour";
    private static final String PROPERTY_SHUTDOWN_MIN = "shutdownMin";

    private static final String SCHEDULE_KIOSK_NAME = "name";
    private static final String SCHEDULE_UUID = "jcr:uuid";
    private static final String SCHEDULE_KIOSK_START_TIME = "startDate";
    private static final String SCHEDULE_KIOSK_END_TIME = "endDate";

    private String getValue(Node node, String name) throws RepositoryException {
        String value = "";
        if (node.hasProperty(name)) {
            value = node.getProperty(name).getString();
        }

        return value;
    }

    @Override
    public JcrKioskInfo get(String channel) {

        JcrKioskInfo kioskInfo = null;
        try {
            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.KioskInfo.getWorkspaceName(), "/" + channel);
            if (channelNode != null) {
                List<Node> kioskInfoList = NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.KioskInfo.getNodeType()));
                if (kioskInfoList.size() > 0) {
                    Node kioskInfoNode = kioskInfoList.get(0);
                    kioskInfo = new JcrKioskInfo();
                    kioskInfo.setDeviceNumber(getValue(kioskInfoNode, PROPERTY_DEVICE_NUMBER));
                    kioskInfo.setJcrName(kioskInfoNode.getName());
                    kioskInfo.setTerminalNumber(getValue(kioskInfoNode, PROPERTY_TERMINAL_NUMBER));
                    kioskInfo.setUserId(getValue(kioskInfoNode, PROPERTY_USER_ID));
                    kioskInfo.setUserPassword(getValue(kioskInfoNode, PROPERTY_USER_PASSWORD));
                    kioskInfo.setJcrNodeUuid(kioskInfoNode.getIdentifier());
                    kioskInfo.setLocation(getValue(kioskInfoNode, PROPERTY_LOCATION));
                    kioskInfo.setShutdownHour(getValue(kioskInfoNode, PROPERTY_SHUTDOWN_HOUR));
                    kioskInfo.setShutdownMin(getValue(kioskInfoNode, PROPERTY_SHUTDOWN_MIN));
                } else {
                    log.error("NO kiosk info node found for channel " + channel);
                }
            } else {
                log.error("NO channel node " + channel + " found for kiosk info.");
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return kioskInfo;
    }

    @Override
    public List<JcrKioskInfo> getAll(String channel) {
        List<JcrKioskInfo> kioskInfoList = new ArrayList<>();
        try {
            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.KioskInfo.getWorkspaceName(), "/" + channel);
            if (channelNode != null) {
                List<Node> kioskInfoNodeList = NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.KioskInfo.getNodeType()));
                for (Node kioskInfoNode : kioskInfoNodeList) {
                    JcrKioskInfo kioskInfo = new JcrKioskInfo();
                    kioskInfo.setDeviceNumber(getValue(kioskInfoNode, PROPERTY_DEVICE_NUMBER));
                    kioskInfo.setJcrName(kioskInfoNode.getName());
                    kioskInfo.setTerminalNumber(getValue(kioskInfoNode, PROPERTY_TERMINAL_NUMBER));
                    kioskInfo.setUserId(getValue(kioskInfoNode, PROPERTY_USER_ID));
                    kioskInfo.setUserPassword(getValue(kioskInfoNode, PROPERTY_USER_PASSWORD));
                    kioskInfo.setJcrNodeUuid(kioskInfoNode.getIdentifier());
                    kioskInfo.setLocation(getValue(kioskInfoNode, PROPERTY_LOCATION));
                    kioskInfoList.add(kioskInfo);
                }
            } else {
                log.error("NO channel node " + channel + " found for kiosk info.");
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }
        return kioskInfoList;
    }

    @Override
    public ScheduledMaintenance getSchedule(String channel) throws ParseException {
        ScheduledMaintenance schedule = new ScheduledMaintenance(true, 0);
        try {

            Node channelNode = JcrRepository.getParentNode(JcrWorkspace.MaintenanceSchedule.getWorkspaceName(), "/" + channel);
            if (channelNode != null) {
                List<Node> kioskInfoList = NodeUtil.asList(NodeUtil.getNodes(channelNode, JcrWorkspace.MaintenanceSchedule.getNodeType()));

                JcrKioskInfo kioskInfo = get(channel);

                for (Node n : kioskInfoList) {
                    List<Node> kioskInfoConfig = NodeUtil.asList(NodeUtil.getNodes(n, "mgnl:contentNode"));
                    for (Node kiosk : kioskInfoConfig) {
                        List<Node> datalist = NodeUtil.asList(NodeUtil.getNodes(kiosk, "mgnl:contentNode"));
                        if (datalist != null && datalist.size() > 0) {
                            for (Node granted : datalist) {
                                // kiosk from schedule
                                String kioskName = granted.getProperty("relatedKioskInfoUUID").getString();
                                if (kioskInfo != null & (kioskInfo.getJcrName() != null && !kioskInfo.getJcrName().equals(""))) {

                                    if (kioskInfo.getJcrNodeUuid().equals(kioskName)) {
                                        String sDate = NvxDateUtils.formatDate(PropertyUtil.getDate(n, SCHEDULE_KIOSK_START_TIME).getTime(),
                                                NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                                        String eDate = NvxDateUtils.formatDate(PropertyUtil.getDate(n, SCHEDULE_KIOSK_END_TIME).getTime(), NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                                        String now = NvxDateUtils.formatDate(new Date(), NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);

                                        Date dateStart = NvxDateUtils.parseDate(sDate, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                                        Date dateEnd = NvxDateUtils.parseDate(eDate, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                                        Date nowDate = NvxDateUtils.parseDate(now, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);

                                        boolean status = nowDate.after(dateStart) && nowDate.before(dateEnd);
                                        if (status) {
                                            schedule.setStatus(false);
                                            schedule.setNumberOfSeconds(dateEnd.getTime() - nowDate.getTime());
                                            // return schedule;
                                        }
                                    }
                                }
                            }
                        } else {
                            String sDate = NvxDateUtils.formatDate(PropertyUtil.getDate(n, SCHEDULE_KIOSK_START_TIME).getTime(), NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                            String eDate = NvxDateUtils.formatDate(PropertyUtil.getDate(n, SCHEDULE_KIOSK_END_TIME).getTime(), NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                            String now = NvxDateUtils.formatDate(new Date(), NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);

                            Date dateStart = NvxDateUtils.parseDate(sDate, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                            Date dateEnd = NvxDateUtils.parseDate(eDate, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);
                            Date nowDate = NvxDateUtils.parseDate(now, NvxDateUtils.DATE_TIME_FORMAT_SCHEDULE);

                            boolean status = nowDate.after(dateStart) && nowDate.before(dateEnd);
                            if (status) {
                                schedule.setStatus(false);
                                schedule.setNumberOfSeconds(dateEnd.getTime() - nowDate.getTime());
                                return schedule;
                            }
                        }
                    }

                }
                // else {
                // log.error("NO Schedule node found for channel " + channel);
                // }
            } else {
                schedule.setStatus(true);
                schedule.setNumberOfSeconds(0);
                log.error("NO channel node " + channel + " found for Schedule.");
            }
        } catch (RepositoryException e) {
            log.error(e.getMessage(), e);
        }

        return schedule;
    }

}
