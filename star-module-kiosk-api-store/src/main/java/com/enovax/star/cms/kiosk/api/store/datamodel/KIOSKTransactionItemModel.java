package com.enovax.star.cms.kiosk.api.store.datamodel;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "KIOSKTransactionItem", schema = "dbo")
public class KIOSKTransactionItemModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9103478435777708002L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "transaction_date")
	private String transactionDate;

	@Column(name = "transaction_number")
	private String transactionNumber;

	@Column(name = "total_amount")
	private String totalAmount;

	@Column(name = "pamyent_type")
	private String paymentType;

	@Column(name = "quantity")
	private String quantity;

	@Column(name = "item_code")
	private String itemCode;

	@Column(name = "item_name")
	private String itemName;

	@Column(name = "item_type")
	private String itemType;

	@Column(name = "kiosk_id")
	private String kioskId;

	public KIOSKTransactionItemModel() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getKioskId() {
		return kioskId;
	}

	public void setKioskId(String kioskId) {
		this.kioskId = kioskId;
	}

}
