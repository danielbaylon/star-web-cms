package com.enovax.star.cms.kiosk.api.store.service.job;

public interface IKioskJobSchedulerService {

    void scheduleKioskShutdown(String channel);

}
