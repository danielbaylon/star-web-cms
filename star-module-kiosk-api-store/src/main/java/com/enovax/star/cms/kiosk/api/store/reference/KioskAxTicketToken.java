package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskAxTicketToken {

    PrintLabel("PrintLabel", "{{PrintLabel}}"),

    PublishPrice("PublishPrice", "{{PublishPrice}}"),

    TicketType("TicketType", "{{TicketType}}"),

    PriceType("PriceType", "{{PriceType}}"),

    ProductOwner("ProductOwner", "{{ProductOwner}}"),

    PaxPerTicket("PaxPerTicket", "{{PaxPerTicket}}"),

    UsageValidity("UsageValidity", "{{UsageValidity}}"),

    TicketValidity("TicketValidity", "{{TicketValidity}}"),

    ValidityStartDate("ValidityStartDate", "{{ValidityStartDate}}"),

    ValidityEndDate("ValidityEndDate", "{{ValidityEndDate}}"),

    LegalEntity("LegalEntity", "{{LegalEntity}}"),

    TicketNumber("TicketNumber", "{{TicketNumber}}"),

    TicketCode("TicketCode", "{{TicketCode}}"),

    TicketLineDesc("TicketLineDesc", "{{TicketLineDesc}}"),

    ProductImage("ProductImage", "{{ProductImage}}"),

    ItemNumber("ItemNumber", "{{ItemNumber}}"),

    CustomerName("CustomerName", "{{CustomerName}}"),

    DisplayName("DisplayName", "{{DisplayName}}"),

    Description("Description", "{{Description}}"),

    ProductSearchName("ProductSearchName", "{{ProductSearchName}}"),

    ProductVarConfig("ProductVarConfig", "{{ProductVarConfig}}"),

    ProductVarSize("ProductVarSize", "{{ProductVarSize}}"),

    ProductVarColor("ProductVarColor", "{{ProductVarColor}}"),

    ProductVarStyle("ProductVarStyle", "{{ProductVarStyle}}"),

    PackageItem("PackageItem", "{{PackageItem}}"),

    PackageItemName("PackageItemName", "{{PackageItemName}}"),

    PackagePrintLabel("PackagePrintLabel", "{{PackagePrintLabel}}"),

    MixMatchPackageName("MixMatchPackageName", "{{MixMatchPackageName}}"),

    MixMatchDescription("MixMatchDescription", "{{MixMatchDescription}}"),

    EventDate("EventDate", "{{EventDate}}"),
    
    EventName("EventName", "{{EventName}}"),

    Facilities("Facilities", "{{Facilities}}"),

    FacilityAction("FacilityAction", "{{FacilityAction}}"),

    OperationIds("OperationIds", "{{OperationIds}}"),

    DiscountApplied("DiscountApplied", "{{DiscountApplied}}"),

    BarcodePLU("BarcodePLU", "{{BarcodePLU}}"),

    BarcodeStartDate("BarcodeStartDate", "{{BarcodeStartDate}}"),

    BarcodeEndDate("BarcodeEndDate", "{{BarcodeEndDate}}"),

    TermAndCondition("TAC", "{{TAC}}"),

    Disclaimer("Disclaimer", "{{Disclaimer}}");

    private final String token;

    private final String replacement;

    private KioskAxTicketToken(String token, String replacement) {
        this.token = token;
        this.replacement = replacement;
    }

    public String getToken() {
        return this.token;
    }

    public String getReplacement() {
        return this.replacement;
    }

    public static KioskAxTicketToken toEnum(String token) {
        KioskAxTicketToken ticketToken = null;
        if (PrintLabel.getToken().equals(token)) {
            ticketToken = PrintLabel;
        } else if (PublishPrice.getToken().equals(token)) {
            ticketToken = PublishPrice;
        } else if (TicketType.getToken().equals(token)) {
            ticketToken = TicketType;
        } else if (PriceType.getToken().equals(token)) {
            ticketToken = PriceType;
        } else if (ProductOwner.getToken().equals(token)) {
            ticketToken = ProductOwner;
        } else if (PaxPerTicket.getToken().equals(token)) {
            ticketToken = PaxPerTicket;
        } else if (UsageValidity.getToken().equals(token)) {
            ticketToken = UsageValidity;
        } else if (TicketValidity.getToken().equals(token)) {
            ticketToken = TicketValidity;
        } else if (ValidityStartDate.getToken().equals(token)) {
            ticketToken = ValidityStartDate;
        } else if (ValidityEndDate.getToken().equals(token)) {
            ticketToken = ValidityEndDate;
        } else if (LegalEntity.getToken().equals(token)) {
            ticketToken = LegalEntity;
        } else if (TicketNumber.getToken().equals(token)) {
            ticketToken = TicketNumber;
        } else if (TicketCode.getToken().equals(token)) {
            ticketToken = TicketCode;
        } else if (TicketLineDesc.getToken().equals(token)) {
            ticketToken = TicketLineDesc;
        } else if (ProductImage.getToken().equals(token)) {
            ticketToken = ProductImage;
        } else if (ItemNumber.getToken().equals(token)) {
            ticketToken = ItemNumber;
        } else if (CustomerName.getToken().equals(token)) {
            ticketToken = CustomerName;
        } else if (DisplayName.getToken().equals(token)) {
            ticketToken = DisplayName;
        } else if (Description.getToken().equals(token)) {
            ticketToken = Description;
        } else if (ProductSearchName.getToken().equals(token)) {
            ticketToken = ProductSearchName;
        } else if (ProductVarConfig.getToken().equals(token)) {
            ticketToken = ProductVarConfig;
        } else if (ProductVarSize.getToken().equals(token)) {
            ticketToken = ProductVarSize;
        } else if (ProductVarColor.getToken().equals(token)) {
            ticketToken = ProductVarColor;
        } else if (ProductVarStyle.getToken().equals(token)) {
            ticketToken = ProductVarStyle;
        } else if (PackageItem.getToken().equals(token)) {
            ticketToken = PackageItem;
        } else if (PackageItemName.getToken().equals(token)) {
            ticketToken = PackageItemName;
        } else if (PackagePrintLabel.getToken().equals(token)) {
            ticketToken = PackagePrintLabel;
        } else if (MixMatchPackageName.getToken().equals(token)) {
            ticketToken = MixMatchPackageName;
        } else if (MixMatchDescription.getToken().equals(token)) {
            ticketToken = MixMatchDescription;
        } else if (Facilities.getToken().equals(token)) {
            ticketToken = Facilities;
        } else if (FacilityAction.getToken().equals(token)) {
            ticketToken = FacilityAction;
        } else if (OperationIds.getToken().equals(token)) {
            ticketToken = OperationIds;
        } else if (DiscountApplied.getToken().equals(token)) {
            ticketToken = DiscountApplied;
        } else if (BarcodePLU.getToken().equals(token)) {
            ticketToken = BarcodePLU;
        } else if (BarcodeStartDate.getToken().equals(token)) {
            ticketToken = BarcodeStartDate;
        } else if (BarcodeEndDate.getToken().equals(token)) {
            ticketToken = BarcodeEndDate;
        } else if (TermAndCondition.getToken().equals(token)) {
            ticketToken = TermAndCondition;
        } else if (Disclaimer.getToken().equals(token)) {
            ticketToken = Disclaimer;
        }
        return ticketToken;
    }

}