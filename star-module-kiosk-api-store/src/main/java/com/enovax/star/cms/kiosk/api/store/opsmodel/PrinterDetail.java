package com.enovax.star.cms.kiosk.api.store.opsmodel;

public class PrinterDetail {
    private long gsi;
    private String modelName;
    private long model;
    private String firmwareVersion;
    private String voltage;
    private String printHeadTemperature;
    private long totalPaperPrinted;
    private long cutCounter;

    public long getGsi() {
        return gsi;
    }

    public void setGsi(long gsi) {
        this.gsi = gsi;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public long getModel() {
        return model;
    }

    public void setModel(long model) {
        this.model = model;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getPrintHeadTemperature() {
        return printHeadTemperature;
    }

    public void setPrintHeadTemperature(String printHeadTemperature) {
        this.printHeadTemperature = printHeadTemperature;
    }

    public long getTotalPaperPrinted() {
        return totalPaperPrinted;
    }

    public void setTotalPaperPrinted(long totalPaperPrinted) {
        this.totalPaperPrinted = totalPaperPrinted;
    }

    public long getCutCounter() {
        return cutCounter;
    }

    public void setCutCounter(long cutCounter) {
        this.cutCounter = cutCounter;
    }
}
