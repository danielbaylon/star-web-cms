package com.enovax.star.cms.kiosk.api.store.service.recovery;

import javax.servlet.http.HttpSession;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;

public interface IKioskRecoveryService {
	
	ApiResult<KioskTransactionEntity> startRecoveryFlowTransaction(StoreApiChannels channel, HttpSession sessions, String receiptNumber);
	
	FunCartDisplay constructRecoveryCartDisplayDetails(StoreApiChannels channel, String sessionId);
	
	ApiResult<KioskTransactionEntity> getTransactionByAxReceiptNumber(StoreApiChannels channel, String receiptNumber);
	
	ApiResult<KioskTransactionEntity> cartTicketReprintStart(StoreApiChannels channel, HttpSession sessionId,  String receiptNumber);
	
	public Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, String axCartId, KioskInfoEntity kiosk);
	

}
