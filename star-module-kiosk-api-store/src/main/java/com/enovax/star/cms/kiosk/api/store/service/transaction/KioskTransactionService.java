package com.enovax.star.cms.kiosk.api.store.service.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.service.sync.IKioskDBSyncService;

@Service
public class KioskTransactionService implements IKioskTransactionService {

    @Autowired
    private IKioskTransactionDao tranDao;

    @Override
    public KioskTransactionEntity getTransactionByAxCartId(String axCartId) {
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = this.tranDao.getTransactionByAxCartIdByOrderByIdDesc(axCartId);
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        }
        return tran;

    }

    @Override
    public KioskTransactionEntity getTranByAxReceiptNo(String axReceiptNo) {
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = tranDao.getTransactionByAxReceipt(axReceiptNo);
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        }
        return tran;
    }

}
