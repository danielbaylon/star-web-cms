package com.enovax.star.cms.kiosk.api.store.service.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.model.kiosk.jcr.JcrKioskInfo;
import com.enovax.star.cms.kiosk.api.store.jcr.repository.kiosk.IKioskJcrRepository;
import com.enovax.star.cms.kiosk.api.store.service.ops.IKioskOpsService;

@Service
public class DefaultKioskJobSchedulerService implements IKioskJobSchedulerService {

    private static final Logger log = Logger.getLogger(DefaultKioskJobSchedulerService.class);

    @Autowired
    private IKioskOpsService opsService;

    @Autowired
    private IKioskJcrRepository kioskJcrRepository;

    @Autowired(required = false)
    private TaskScheduler scheduler;

    private static List<ScheduledFuture<?>> scheduledFutures = new ArrayList<>();

    private static final Integer DEFAULT_SHUTDOWN_HOUR = 21;

    private static final Integer DEFAULT_SHUTDOWN_MIN_AND_SEC = 0;

    private static final Integer DEFAULT_NEXT_SHUTDOWN_HOUR = 2;

    private static final Integer DEFAULT_NEXT_SHUTDOWN_MIN_AND_SEC = 30;

    private Boolean cancelAll() {
        Boolean cancelled = Boolean.TRUE;
        for (ScheduledFuture<?> scheduledFuture : scheduledFutures) {
            cancelled = cancelled && scheduledFuture.cancel(Boolean.TRUE);
        }
        scheduledFutures.clear();

        return cancelled;
    }

    private Boolean isAfter9PM() {
        DateTime now = DateTime.now();
        DateTime nightPM = now.withHourOfDay(21).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);

        return now.isAfter(nightPM);
    }

    @Override
    public void scheduleKioskShutdown(String channel) {
        log.info("channel :" + channel);
        cancelAll();
        try {
            Optional<JcrKioskInfo> kiosk = Optional.ofNullable(kioskJcrRepository.get(channel));
            DateTime now = DateTime.now();
            DateTime date = now;
            Integer hour = DEFAULT_SHUTDOWN_HOUR;
            Integer min = DEFAULT_SHUTDOWN_MIN_AND_SEC;
            if (kiosk.isPresent() && StringUtils.isNotBlank(kiosk.get().getShutdownHour()) && StringUtils.isNotBlank(kiosk.get().getShutdownMin())) {
                hour = Integer.valueOf(kiosk.get().getShutdownHour());
                min = Integer.valueOf(kiosk.get().getShutdownMin());
            }

            if (this.isAfter9PM()) {
                hour = DEFAULT_NEXT_SHUTDOWN_HOUR;
                min = DEFAULT_NEXT_SHUTDOWN_MIN_AND_SEC;
                now = now.plusDays(1);
            }
            date = date.withHourOfDay(hour);
            date = date.withMinuteOfHour(min);
            date = date.withSecondOfMinute(DEFAULT_SHUTDOWN_MIN_AND_SEC);
            date = date.withMillisOfSecond(DEFAULT_SHUTDOWN_MIN_AND_SEC);

            if (date.isAfter(now)) {
                this.scheduleKioskShutdown(channel, date.toDate());
            } else {
                log.error("schedule request skipped as next scheduled time " + date + "is before system date " + now);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void scheduleKioskShutdown(String channel, Date date) {
        try {
            Runnable job = () -> {
                log.info("shutdown is requested by scheduled job......");
                opsService.shutdown(channel, Boolean.TRUE);
            };
            scheduledFutures.add(scheduler.schedule(job, date));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

}
