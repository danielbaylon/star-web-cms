package com.enovax.star.cms.kiosk.api.store.print.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.ticketgen.KioskTicketsPackage;
import com.enovax.star.cms.commons.model.ticketgen.TicketXmlRecord;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.api.store.controller.OnlineStoreKioskController;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.service.booking.IKioskBookingService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixPrinterService;
import com.enovax.star.cms.kiosk.api.store.service.recovery.IKioskRecoveryService;
import com.enovax.star.cms.kiosk.api.store.service.redemption.IKioskPINRedemptionService;
import com.enovax.star.cms.kiosk.api.store.service.transaction.IKioskTransactionService;
import com.google.zxing.WriterException;

/**
 * Created by cornelius on 14/6/16.
 */

public abstract class KioskPrintApiController {

    private final static Logger logger = LoggerFactory.getLogger(KioskPrintApiController.class.getName());

    @Value("${kiosk.api.printer.service.url:http://localhost:9100/}")
    private String printerServiceURL;

    private String PRINT_TICKET_URL = "";
    private String PRINT_RECEIPT_URL = "";

    private String PAYMENT_MODE_NETS_PIN_DEBIT = KioskPaymentCardType.NETSPINDEBIT.getDescription();
    private String PAYMENT_MODE_NETS_FLASHPAY = KioskPaymentCardType.NFP.getDescription();
    private String PAYMENT_MODE_OTHER = "CARD";

    @Autowired
    private OnlineStoreKioskController storeController;

    @Autowired
    private IKioskBookingService bookingService;

    @Autowired
    private IKioskManagementService kioskService;

    @Autowired
    private IKioskTransactionService tranService;

    @Autowired
    private IKioskPINRedemptionService redemptionService;

    @Autowired
    private HttpSession session;

    @Autowired
    private IKioskTransactionDao kioskTransactionDao;

    @Autowired
    private IKioskRecoveryService recoveryService;

    @Autowired
    private IKioskZabbixPrinterService kioskZabbixPrinterService;
    
    @PostConstruct
    private void intServiceURL() {
        this.PRINT_TICKET_URL = this.printerServiceURL + "/api/print-ticket/";
        this.PRINT_RECEIPT_URL = this.printerServiceURL + "/api/print-receipt/";
        logger.info(this.PRINT_TICKET_URL);
        logger.info(this.PRINT_RECEIPT_URL);
    }

    @RequestMapping(value = "printTicketsAndReceipt", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<?>> printTicketsAndReceipt(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @RequestBody String receiptNumber) {
        KioskInfoEntity kiosk = new KioskInfoEntity();
        try {
            if (StringUtils.isEmpty(receiptNumber)) {
                logger.error("Empty parameter value supplied when calling API.");
                return generateExceptionResponse(ApiErrorCodes.General, "");
            }
            logger.info("receiptNumber : " + receiptNumber);
            KioskTicketsPackage ticketPackage = this.getTicketsByReceipt(receiptNumber);
            for (TicketXmlRecord xml : ticketPackage.getTicketXmls()) {
                logger.info("TicketXmlRecord : " + xml.getTemplateXml());
            }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            kiosk = this.kioskService.getKioskInfo(channel);
            KioskTransactionEntity tran = tranService.getTransactionByAxCartId(kiosk.getCurrentCartId());
            
            if(null == tran) {
                tran = kioskTransactionDao.getTransactionByReceipt(receiptNumber);
            }
            
            kioskZabbixPrinterService.checkTicketPaperAvailability(ticketPackage.getTicketXmls().size(), kiosk, channel.code);
            
            ResponseEntity<KioskPrintTicketResult> printTicketResponse = printTickets(ticketPackage.getTicketXmls());
            logger.info(" printTicketResponse.getBody().hasErrors() : " + printTicketResponse.getBody().hasErrors());
            if (ticketPackage.getTicketXmls() == null || ticketPackage.getTicketXmls().size() == 0) {
                List<KioskTicketStatus> history = new ArrayList<>();
                KioskTicketStatus status = new KioskTicketStatus();
                status.setSuccess(Boolean.FALSE);
                history.add(status);
                printTicketResponse.getBody().setPrintHistory(history);
                printTicketResponse.getBody().setSuccess(Boolean.FALSE);
                printTicketResponse.getBody().setErrorMessage("no tickets");
            }

            KioskPrintTicketResult printTicketResult = printTicketResponse.getBody();

            String receiptXml = ticketPackage.getReceiptXml();
            if (tran.getType().equalsIgnoreCase(KioskTransactionType.RECOVERY.getCode())) {

            } else if (StringUtils.isBlank(tran.getRedemptionPinCode())) {
                receiptXml = getPaymentModeRow(receiptXml);
            }

            if (printTicketResult.hasErrors()) {

                bookingService.updateTransactionTicketPrintingStatus(receiptNumber, Boolean.FALSE);
                logPrintErrors("Tickets", printTicketResult.getErrorMessages());

                KioskTransactionEntity transactionEntity = kioskTransactionDao.getTransactionByReceipt(receiptNumber);

                if (transactionEntity.getType().equalsIgnoreCase(KioskTransactionType.RECOVERY.getCode())) {
                    receiptXml = getReceiptXMLForRecovery(receiptNumber, transactionEntity.getAxReceiptNumber());
                } else if (StringUtils.isBlank(transactionEntity.getRedemptionPinCode())) {
                    receiptXml = getReceiptXMLWithQRCode(receiptNumber, transactionEntity.getAxReceiptNumber());
                    receiptXml = getPaymentModeRow(receiptXml);
                } else {
                    receiptXml = getReceiptXMLForRedemption(receiptNumber, transactionEntity.getAxReceiptNumber());
                }

            } else {
                bookingService.updateTransactionTicketPrintingStatus(receiptNumber, Boolean.TRUE);
            }

            if (StringUtils.isNotBlank(tran.getRedemptionPinCode())) {
                redemptionService.updatePrintStatus(channel, session, printTicketResult, kiosk);
            } else if (tran.getType().equals(KioskTransactionType.RECOVERY.getCode())) {
                recoveryService.updatePrintStatus(channel, session, printTicketResult, tran.getAxCartId(), kiosk);
            } else {
                bookingService.updatePrintStatus(channel, session, printTicketResult, kiosk);
            }

            if (!skipReceipt(tran, printTicketResult)) {
                ResponseEntity<KioskPrintTicketResult> printReceiptResponse = printReceipt(receiptXml);

                KioskPrintTicketResult printReceiptResult = printReceiptResponse.getBody();

                if (printReceiptResult.hasErrors()) {
                    kioskZabbixPrinterService.sendReceiptError(kiosk, channel.code, new String[]{receiptNumber});
                    logPrintErrors("Receipt", printReceiptResult.getErrorMessages());
                }

                if (printTicketResult.hasErrors() && !printReceiptResult.hasErrors()) {
                    if(StringUtils.isNotBlank(tran.getRedemptionPinCode())){
                        kioskZabbixPrinterService.sendTicketRedemptionError(kiosk, channel.code, new String[]{tran.getRedemptionPinCode(), receiptNumber});
                    } else {
                        kioskZabbixPrinterService.sendTicketAndReceiptError(kiosk, channel.code, new String[]{receiptNumber});
                    }
                    
                    return generateExceptionResponse(ApiErrorCodes.TicketPrintError,
                            "Please bring this slip and print your tickets <br /> at other ticketing kiosks or counters. <br /><span class=\"sub-line\">We are sorry for any inconvenience caused.</span>");
                }

                if (printTicketResult.hasErrors() && printReceiptResult.hasErrors()) {
                    kioskZabbixPrinterService.sendTicketAndReceiptError(kiosk, channel.code, new String[]{receiptNumber});
                    throw new Exception();
                }
            }

            return generateSuccessResponse("Printing Completed");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generateExceptionResponse(ApiErrorCodes.TicketAndReceiptPrintError, "Please note down the receipt number (" + receiptNumber
                    + ") and print your tickets at other ticketing kiosks or counters.<br /> We are sorry for any inconvenience caused.");
        }
    }

    /**
     * if ticket printed failed for B2C then skip receipt
     * 
     * @param tran
     * @param printTicketResult
     * @return
     */
    private boolean skipReceipt(KioskTransactionEntity tran, KioskPrintTicketResult printTicketResult) {

        Boolean skip = false;
        if (!printTicketResult.hasErrors() && !StringUtils.isBlank(tran.getRedemptionPinCode()) && !tran.getAllowPartiallRedemption()) {
            skip = true;
            logger.info("receipt skiped");
        }

        return skip;
    }

    @RequestMapping(value = "printReceipt", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<?>> printReceiptForOps(HttpServletRequest request, HttpServletResponse response) {

        String receiptNumber = Optional.ofNullable((String) request.getParameter("receipt-number")).orElse("");
        String ticketPrintingStatus = Optional.ofNullable((String) request.getParameter("ticket-status")).orElse("");
        String apiChannel =  Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
        try {

            if (StringUtils.isBlank(receiptNumber)) {
                logger.error("Empty parameter value supplied when calling API.");
                return generateExceptionResponse(ApiErrorCodes.General, "");
            }

            KioskTicketsPackage ticketPackage = this.getTicketsByReceipt(receiptNumber);

            String receiptXml = ticketPackage.getReceiptXml();
            receiptXml = getPaymentModeRow(receiptXml);

            if ((StringUtils.isBlank(ticketPrintingStatus)) && ticketPrintingStatus.equalsIgnoreCase("true")) {
                logPrintErrors("Tickets", "Unknown printer error(s).");

                List<KioskTransactionEntity> receiptDetailsByTranId = kioskTransactionDao.getReceiptDetailsByTranId(receiptNumber, apiChannel);
                if (receiptDetailsByTranId != null && receiptDetailsByTranId.size() > 0) {
                    KioskTransactionEntity transactionEntity = receiptDetailsByTranId.get(0);
                    receiptXml = getReceiptXMLWithQRCode(receiptNumber, transactionEntity.getAxReceiptNumber());
                }

            }

            ResponseEntity<KioskPrintTicketResult> printReceiptResponse = printReceipt(receiptXml);

            KioskPrintTicketResult printReceiptResult = printReceiptResponse.getBody();

            if (printReceiptResult.hasErrors()) {
                logPrintErrors("Receipt", printReceiptResult.getErrorMessages());
            }

            if (ticketPrintingStatus.equalsIgnoreCase("true") && !printReceiptResult.hasErrors()) {
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError,
                        "Please bring this slip and print your tickets <br /> at other ticketing kiosks or counters. <br /><span class=\"sub-line\">We are sorry for any inconvenience caused.</span>");
            }

            if (ticketPrintingStatus.equalsIgnoreCase("true") && printReceiptResult.hasErrors()) {
                throw new Exception();
            }

            return generateSuccessResponse("Printing Completed");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generateExceptionResponse(ApiErrorCodes.TicketAndReceiptPrintError, "Please note down the receipt number (" + receiptNumber
                    + ") and print your tickets at other ticketing kiosks or counters.<br /> We are sorry for any inconvenience caused.");
        }
    }

    @RequestMapping(value = "printTicketsForRecoveryFlow", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<?>> printTicketsForRecoveryFlow(@RequestBody String receiptNumber) {

        try {
            if (StringUtils.isEmpty(receiptNumber)) {
                logger.error("Empty parameter value supplied when calling API.");
                return generateExceptionResponse(ApiErrorCodes.General, "");
            }

            StoreTransaction storeTransaction = getStoreTransaction(receiptNumber);

            if (storeTransaction == null) {
                logPrintErrors("Tickets", "Invalid Transaction ID");
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "Invalid Transaction ID.");
            }

            if (storeTransaction.isRecoveryFlowTicketsPrinted()) {
                logPrintErrors("Tickets", "Receipt number has been used and no longer valid");
                return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "ALL tickets of the transaction have been issued.");
            }

            // Save Store Transaction to prevent further printing of the same
            // receipt
            updateRecoveryFlowExecuted(storeTransaction);

            KioskTicketsPackage ticketPackage = retrieveTicketsFromSession();

            ResponseEntity<KioskPrintTicketResult> printTicketResponse = printTickets(ticketPackage.getTicketXmls());

            KioskPrintTicketResult printTicketResult = printTicketResponse.getBody();

            if (printTicketResult.hasErrors()) {
                logPrintErrors("Tickets", printTicketResult.getErrorMessages());
                throw new Exception();
            }

            return generateSuccessResponse("Printing Completed");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generateExceptionResponse(ApiErrorCodes.TicketPrintError, "Please print your tickets at counters.<br /> We are sorry for any inconvenience caused.");
        }
    }

    protected void updateRecoveryFlowExecuted(StoreTransaction storeTransaction) {

        storeTransaction.setRecoveryFlowTicketsPrinted(true);
        // bookingRepository.saveStoreTransaction(getChannel(),
        // storeTransaction);
    }

    protected StoreTransaction getStoreTransaction(String receiptNumber) {
        return null; // bookingRepository.getStoreTransactionByReceipt(getChannel(),
                     // receiptNumber);
    }

    protected String generateQRCode(String ticketData) throws WriterException, IOException {
        return QRCodeGenerator.toBase64(ticketData, 150, 150);
    }

    protected String getReceiptXMLWithQRCode(String receiptNumber, String axReceiptNumber) throws WriterException, IOException {

        String qrCode = generateQRCode(axReceiptNumber);

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperReceiptWithQRCodePackage(getChannel().code, qrCode, receiptNumber);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage.getReceiptXml();
    }

    protected String getReceiptXMLForRedemption(String receiptNumber, String axReceiptNumber) throws WriterException, IOException {

        String qrCode = generateQRCode(axReceiptNumber);

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperReceiptWithoutQRCodePackage(getChannel().code, qrCode, receiptNumber);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage.getReceiptXml();
    }

    protected String getReceiptXMLForRecovery(String receiptNumber, String axReceiptNumber) throws WriterException, IOException {

        String qrCode = generateQRCode(axReceiptNumber);

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperReceiptWithoutQRCodePackage(getChannel().code, qrCode, receiptNumber);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage.getReceiptXml();
    }

    protected KioskTicketsPackage getTicketsByReceipt(String receiptNumber) {

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperTicketsPackage(getChannel().code, receiptNumber);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage;
    }

    protected KioskTicketsPackage retrieveTicketsFromSession() {

        ResponseEntity<ApiResult<KioskTicketsPackage>> ticketResultResponse = storeController.apiGeneratePaperTicketsPackage(getChannel().code);
        KioskTicketsPackage ticketPackage = ticketResultResponse.getBody().getData();

        return ticketPackage;
    }

    protected ResponseEntity<KioskPrintTicketResult> requestPrinterRESTAPI(String url, String requestJSONString) throws Exception {

        HttpEntity<String> requestEntity = new HttpEntity<>(requestJSONString, getJSONHttpHeaders());

        RestTemplate template = new RestTemplate();
        ResponseEntity<KioskPrintTicketResult> printResult = template.postForEntity(url, requestEntity, KioskPrintTicketResult.class);

        return printResult;
    }

    protected ResponseEntity<KioskPrintTicketResult> printTickets(List<TicketXmlRecord> ticketsXml) throws Exception {

        logger.info("Ticket Printing started .....");

        logger.info("Total tickets to print: " + ticketsXml.size());

        String requestJSONString = convertToJSONString("tickets", ticketsXml);

        ResponseEntity<KioskPrintTicketResult> printResult = requestPrinterRESTAPI(PRINT_TICKET_URL, requestJSONString);

        logger.info(printResult.toString());

        logger.info(printResult.getBody().toString());

        logger.info("Ticket Printing ended .....");

        return printResult;
    }

    protected ResponseEntity<KioskPrintTicketResult> printReceipt(String receiptXml) throws Exception {

        logger.info("Receipt Printing started ....." + receiptXml);

        String requestJSONString = convertToJSONString("templateXml", receiptXml);

        ResponseEntity<KioskPrintTicketResult> printResult = requestPrinterRESTAPI(PRINT_RECEIPT_URL, requestJSONString);

        logger.info("Receipt Printing ended .....");

        return printResult;
    }

    protected String convertToJSONString(String rootName, Object content) {
        return "{ \"" + rootName + "\": " + JsonUtil.jsonify(content, false) + " }";
    }

    protected HttpHeaders getJSONHttpHeaders() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    protected ResponseEntity<ApiResult<?>> generateExceptionResponse(ApiErrorCodes errorCode, String message) {
        return new ResponseEntity<>(new ApiResult<>(errorCode, message, ""), HttpStatus.OK);
    }

    protected ResponseEntity<ApiResult<?>> generateSuccessResponse(String message) {
        return new ResponseEntity<>(new ApiResult<>(true, "", message, ""), HttpStatus.OK);
    }

    protected void logPrintErrors(String type, String errorMessages) {

        logger.info("Printing " + type + " encounters the following error(s):");
        logger.info(errorMessages);
    }

    protected abstract StoreApiChannels getChannel();

    public String getPaymentModeRow(String xml) throws IOException, SAXException, TransformerException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setByteStream(new ByteArrayInputStream(xml.getBytes()));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("Row");

            NodeList lines = doc.getElementsByTagName("Line");
            Element line = (Element) lines.item(14);
            String receiptStage = line.getElementsByTagName("Text").item(0).getFirstChild().getFirstChild().getTextContent();

            if (receiptStage != null && receiptStage.equals("Payment Receipt")) {
                NodeList removeLines = doc.getElementsByTagName("Line");
                Element removeLine = (Element) removeLines.item(0);
                removeElementFromXML(removeLine);
                // get payment mode row(row index 21)
                Element paymentMode = (Element) nodes.item(22);
                Element name = (Element) paymentMode.getElementsByTagName("Text").item(1);
                String mode = name.getTextContent();

                if (PAYMENT_MODE_NETS_PIN_DEBIT != null && PAYMENT_MODE_NETS_PIN_DEBIT.equals(mode)) {
                    
                    // set new values
                    
                    Element netsTrace = (Element) nodes.item(12);
                    Element netsTraceText = (Element) netsTrace.getElementsByTagName("Text").item(0);
                    netsTraceText.setTextContent("NETS Trace#:");
                    
                    Element dateTime = (Element) nodes.item(13);
                    Element dateTimeText = (Element) dateTime.getElementsByTagName("Text").item(0);
                    dateTimeText.setTextContent("Date/Time:");

                    Element netsIssuerName = (Element) nodes.item(24);
                    Element netsIssuerNameText = (Element) netsIssuerName.getElementsByTagName("Text").item(0);
                    netsIssuerNameText.setTextContent("NETS Issuer :");
                    
                    
                    // remove card no row(row index 24)
                    //Element cardNo = (Element) nodes.item(24);
                    //removeElementFromXML(cardNo);
                    // remove TC row(row index 21)
                    Element tc = (Element) nodes.item(21);
                    removeElementFromXML(tc);
                    // remove App Label row(row index 20)
                    Element appLabel = (Element) nodes.item(20);
                    removeElementFromXML(appLabel);
                    // remove AID row(row index 19)
                    Element aid = (Element) nodes.item(19);
                    removeElementFromXML(aid);
                    // remove TVR row(row index 18)
                    Element tvr = (Element) nodes.item(18);
                    removeElementFromXML(tvr);
                    // remove entryMode row(row index 17)
                    Element entryMode = (Element) nodes.item(17);
                    removeElementFromXML(entryMode);

                    Element rrn = (Element) nodes.item(15);
                    removeElementFromXML(rrn);
                    
                    //Element invoice = (Element) nodes.item(13);
                    //removeElementFromXML(invoice);
                    
                   


                } else if (PAYMENT_MODE_NETS_FLASHPAY != null && PAYMENT_MODE_NETS_FLASHPAY.equals(mode)) {
                    
                    // set new values
                    Element originalBalance = (Element) nodes.item(13);
                    Element originalBalanceText = (Element) originalBalance.getElementsByTagName("Text").item(0);
                    originalBalanceText.setTextContent("Original Balance:");

                    Element transAmount = (Element) nodes.item(14);
                    Element transAmountText = (Element) transAmount.getElementsByTagName("Text").item(0);
                    transAmountText.setTextContent("Trans Amount:");

                    Element dummyItem = (Element) nodes.item(15);
                    Element dummyItemText = (Element) dummyItem.getElementsByTagName("Text").item(0);
                    dummyItemText.setTextContent("         ");

                    Element remainBalance = (Element) nodes.item(16);
                    Element remainBalanceText = (Element) remainBalance.getElementsByTagName("Text").item(0);
                    remainBalanceText.setTextContent("Remain Balance:");
                    
                    
                    Element dateTime = (Element) nodes.item(17);
                    Element dateTimeText = (Element) dateTime.getElementsByTagName("Text").item(0);
                    dateTimeText.setTextContent("Date/Time:");
                    
                    
                    // remove card no row(row index 24)
                    // Element cardNo = (Element) nodes.item(24);
                    // removeElementFromXML(cardNo);
                    // remove TC row(row index 21)
                    Element tc = (Element) nodes.item(21);
                    removeElementFromXML(tc);
                    // remove App Label row(row index 20)
                    Element appLabel = (Element) nodes.item(20);
                    removeElementFromXML(appLabel);
                    // remove AID row(row index 19)
                    Element aid = (Element) nodes.item(19);
                    removeElementFromXML(aid);
                    // remove TVR row(row index 18)
                    Element tvr = (Element) nodes.item(18);
                    removeElementFromXML(tvr);
                    // remove entryMode row(row index 17)
                    //Element entryMode = (Element) nodes.item(17);
                    //removeElementFromXML(entryMode);



                } else {
                    return xml;
                }

                DOMSource domSource = new DOMSource(doc);
                StringWriter writer = new StringWriter();
                StreamResult result = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, result);
                nodes.getLength();
                return writer.toString();

            } else {
                return xml;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return xml;
    }

    private void removeElementFromXML(Element elm) {
        elm.getParentNode().removeChild(elm);
    }
}
