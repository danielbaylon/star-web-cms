package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;

@Service
public class DefaultKioskZabbixKioskManagmentService implements IKioskZabbixKioskManagementService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskZabbixKioskManagmentService.class);

    @Autowired
    private IKioskZabbixAdapterService kioskZabbixAdapter;
    
    @Override
    public void sendCloseShiftError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("kiosk.management.close.shift.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

    @Override
    public void sendTransDataSyncError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("kiosk.management.data.sync.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

    @Override
    public void sendNonTransDataSyncError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("kiosk.management.non.ticketing.data.sync.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

}
