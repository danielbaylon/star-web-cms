package com.enovax.star.cms.kiosk.api.store.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.ticketgen.ReceiptMain;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.kiosk.api.store.controller.exception.LoginControllerException;
import com.enovax.star.cms.kiosk.api.store.opsmodel.HardwareStatus;
import com.enovax.star.cms.kiosk.api.store.opsmodel.LoadPaperTicket;
import com.enovax.star.cms.kiosk.api.store.opsmodel.LoadPaperTicketQty;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ReceiptDetailsResponse;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ReceiptHistoryData;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ReceiptStatusResult;
import com.enovax.star.cms.kiosk.api.store.opsmodel.Result;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ScheduledMaintenance;
import com.enovax.star.cms.kiosk.api.store.opsmodel.TicketStatusResult;
import com.enovax.star.cms.kiosk.api.store.opsmodel.TransactionStatusData;
import com.enovax.star.cms.kiosk.api.store.opsmodel.TransactionStatusResponse;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketQtyEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.IKioskOpsService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by tharaka on 7/9/16.
 */

@Controller
@RequestMapping("/ops-data/")
public class OnlineStoreKioskOpsController extends BaseStoreKioskApiController {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    public static final String OPERATION_MODE_VALUE_OPEN = "resume";
    public static final String OPERATION_MODE_VALUE_CLOSE = "down";
    public static final String OPERATION_MODE_EVENT_NAME = "operation mode";
    public static final String OTHER_INFO_EVENT_NAME = "other info";
    public static final String SERVICE_INFO_EVENT_NAME = "service info";
    public static final String QTY_REJECTED_PAPER_INFO_EVENT_NAME = "rejected ticket info";
    public static final String LOAD_PAPER_TICKET_INFO_EVENT_NAME = "load paper ticket info";
    public static final String TRANSACTION_STATUS_INFO_EVENT_NAME = "transaction status info";
    public static final String RECEIPT_PRINTER_INFO_EVENT_NAME = "receipt printer info";
    public static final String HARDWARE_STATUS_INFO_EVENT_NAME = "hardware status info";
    public static final String SHUTDOWN_WITH_OUT_SETTLEMENT = "SWOS";
    public static final String SHUTDOWN_WITH_SETTLEMENT = "SWS";
    public static final String SHUTDOWN_OR_SETTLEMENT_INFO_EVENT_NAME = "shutdown/settlement info";
    public static final String OPERATION_MODE_EVENT_TYPE = "OPS";
    public static final String STATUS_ACTIVE = "Active";
    public static final String STATUS_INACTIVE = "Inactive";

    @Value("${kiosk.api.printer.service.url://http://localhost:9100/}")
    private String printerServiceURL;

    @Value("${kiosk.api.payment.service.url://http://localhost:8081/}")
    private String paymentServiceURL;

    @Value("${kiosk.api.payment.adapter://http://localhost:8081/}")
    private String paymentAdapterURL;

    private String PRINT_RECEIPT_STATUS_URL = "";

    private String PRINT_TICKET_STATUS_URL = "";

    private String PRINT_RECEIPT_DETAILS_URL = "";

    private String PAYMENT_ADAPTER_URL = "";

    private String PAYMENT_ADAPTER_STATUS_URL = "";
    
    @Autowired
    private IKioskOpsService defaultKioskOpsService;

    @Autowired
    private IKioskManagementService defaultKioskManagementService;

    @PostConstruct
    private void intServiceURL() {
        this.PRINT_RECEIPT_STATUS_URL = this.printerServiceURL + "/api/print-receipt-connection-status/";
        this.PRINT_TICKET_STATUS_URL = this.printerServiceURL + "/api/print-ticket-connection-status/";
        this.PRINT_RECEIPT_DETAILS_URL = this.printerServiceURL + "/api/print-receipt-details/";
        this.PAYMENT_ADAPTER_URL = this.paymentAdapterURL + "settlement/";
        this.PAYMENT_ADAPTER_STATUS_URL = this.paymentAdapterURL + "/status-check/";
    }

    @RequestMapping(value = "check-operation-mode", method = {RequestMethod.GET, RequestMethod.POST})
    public void checkOperationMode(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {

        log.info("check-operation-mode ...");

        String operationMode = this.getStringValue(request, "value", "");
        String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
       
        KioskEventEntity eventEntity = new KioskEventEntity();
        KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
        
        if(StringUtils.isNotBlank(operationMode)){
            if (OPERATION_MODE_VALUE_OPEN.equalsIgnoreCase(operationMode)) {
                eventEntity.setValue(operationMode);
                eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
                eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
                eventEntity.setApiChannel(apiChannel);
                defaultKioskOpsService.resumeOperationMode(eventEntity);
            
            } else if (OPERATION_MODE_VALUE_CLOSE.equals(operationMode)) {
                eventEntity.setValue(operationMode);
                eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
                eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
                eventEntity.setApiChannel(apiChannel);
                defaultKioskOpsService.downOperationMode(eventEntity);
            }
        }

    }

    @RequestMapping(value = "get-operation-mode", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> getOperationMode(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {
        try {
            log.info("get-operation-mode ...");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            boolean status = defaultKioskOpsService.getOperationMode(apiChannel);
            
            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getOperationMode] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-other-info", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> addOtherInfo(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {
        try {
            log.info("add other info...");
            
            String activity = this.getStringValue(request, "activity", "");
            String description = this.getStringValue(request, "description", "");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setValue(activity);
            eventEntity.setDescription(description);
            eventEntity.setApiChannel(apiChannel);
            
            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());

            Boolean status = defaultKioskOpsService.addOtherInfo(eventEntity);
           
            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
            
        } catch (Exception e) {
            log.error("!!! System exception encountered [addOtherInfo] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-service-info", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> addServicingInfo(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {
        try {
            log.info("add servicing info...");

            String serviceName = this.getStringValue(request, "servicename", "");
            String description = this.getStringValue(request, "description", "");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setValue(serviceName);
            eventEntity.setDescription(description);
            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setApiChannel(apiChannel);

            boolean status = defaultKioskOpsService.addServiceInfo(eventEntity);
  
            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [addOtherInfo] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-qty-rejected-paper-info", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> addQtyRejectedPaperInfo(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {
        try {
            log.info("add qty rejected paper info info...");
            
            String qty = this.getStringValue(request, "qty", "0");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setValue(qty);
            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel)); 
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setApiChannel(apiChannel);

            boolean status = defaultKioskOpsService.addQtyRejectedPaperInfo(eventEntity);
            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [addOtherInfo] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-load-paper-ticket-info", method = {RequestMethod.GET,
            RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResult<Boolean>> addLoadPaperTicketInfo(@RequestBody LoadPaperTicket loadPaperTicket, HttpServletRequest request, HttpServletResponse response)
            throws LoginControllerException {
        
        boolean status = false;
        try {
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            log.info("add LoadPaperTicket info...");
            if (loadPaperTicket != null) {
                KioskLoadPaperTicketInfoEntity infoEntity = new KioskLoadPaperTicketInfoEntity();

                KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
                infoEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
                infoEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
                infoEntity.setApiChannel(apiChannel);

                infoEntity.setQtyUnloaded(loadPaperTicket.getQtyUnloaded());
                infoEntity.setQtyLoaded(loadPaperTicket.getQtyLoaded());
                infoEntity.setStartTicketNo(loadPaperTicket.getStartTicketNo());
                infoEntity.setEndTicketNo(loadPaperTicket.getEndTicketNo());

                List<LoadPaperTicketQty> ticketQty = loadPaperTicket.getLoadPaperTicketQty();
                HashSet<KioskLoadPaperTicketQtyEntity> ticketQtyEntitySet = new HashSet<KioskLoadPaperTicketQtyEntity>();

                for (LoadPaperTicketQty qty : ticketQty) {
                    KioskLoadPaperTicketQtyEntity ticketQtyEntity = new KioskLoadPaperTicketQtyEntity();
                    ticketQtyEntity.setFromNumber(qty.getFromNumber());
                    ticketQtyEntity.setToNumber(qty.getToNumber());
                    ticketQtyEntity.setTicketInfoEntity(infoEntity);
                    ticketQtyEntitySet.add(ticketQtyEntity);
                }
                infoEntity.setTicketQtyEntitySet(ticketQtyEntitySet);

                status = defaultKioskOpsService.addLoadPaperTicketInfo(infoEntity);
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [addOtherInfo] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-transaction-status", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<TransactionStatusResponse>> adminSearchTransactionStatus(HttpServletRequest request, HttpServletResponse response)
            throws LoginControllerException {

        log.info("search-transaction-status ...");
        try {

            String selectedDate = this.getStringValue(request, "selectdate", NvxDateUtils.formatDate(new Date(), NvxDateUtils.KIOSK_TICKET_DATE_PRINT_FORMAT));
            String startTime = this.getStringValue(request, "starttime", "00:00");
            String endTime = this.getStringValue(request, "endtime", "23:59");
            String transId = this.getStringValue(request, "tranid", "");
            String pinCode = this.getStringValue(request, "pincode", "");
            String apiChannel = Optional.of((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            KioskEventEntity eventEntity = new KioskEventEntity();

            eventEntity.setDescription(selectedDate + "/" + startTime.substring(0, 5) + "/" + endTime.substring(0, 5));

            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel)); 
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setApiChannel(apiChannel);

            eventEntity.setDescription(eventEntity.getDescription() + "/" + transId + "/" + pinCode);

            defaultKioskOpsService.addTransactionStatus(eventEntity);

            String startDate = selectedDate + " " + startTime.substring(0, 5);
            String endDate = selectedDate + " " + endTime.substring(0, 5);

            List<KioskTransactionEntity> transactions = defaultKioskOpsService.getTransactionStatus(startDate, endDate, transId, pinCode, apiChannel);
            if (transactions.isEmpty()) {
                return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
            }

            TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
            ArrayList<TransactionStatusData> statusDatas = new ArrayList<>();

            for (KioskTransactionEntity transactionEntity : transactions) {
                TransactionStatusData statusData = new TransactionStatusData();
                statusData.setTransId(transactionEntity.getReceiptNumber());
                for (KioskTransactionStatus status : KioskTransactionStatus.values()) {
                    if (status.getCode().equals(transactionEntity.getStatus())) {
                        statusData.setStatus(status.getMessage());
                        break;
                    }
                }
                statusDatas.add(statusData);
            }
            transactionStatusResponse.setStatusDataArrayList(statusDatas);
            return new ResponseEntity(new ApiResult<>(true, "", "", transactionStatusResponse), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [adminSearchTransactionStatus] !!!");
            return new ResponseEntity<ApiResult<TransactionStatusResponse>>(handleUncaughtException(e, TransactionStatusResponse.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "search-receipt-details", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptDetailsResponse>> adminSearchReceiptDetails(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {

        log.info("search-ReceiptDetailsResponse-status ...");
        try {
            
            String selectedDate = this.getStringValue(request, "selectdate", NvxDateUtils.formatDate(new Date(), NvxDateUtils.KIOSK_TICKET_DATE_PRINT_FORMAT));
            String startTime = this.getStringValue(request, "starttime", "00:00");
            String endTime = this.getStringValue(request, "endtime", "23:59");
            String apiChannel = Optional.of((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
        
            KioskEventEntity eventEntity = new KioskEventEntity();

            eventEntity.setDescription(selectedDate + "/" + startTime.substring(0,5) + "/" + endTime.substring(0,5));

            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setApiChannel(apiChannel);

            defaultKioskOpsService.addReceiptHistoryStatus(eventEntity);

            String startDate = selectedDate + " " + startTime.substring(0,5);
            String endDate = selectedDate + " " + endTime.substring(0,5);

            List<KioskTransactionEntity> transactions = defaultKioskOpsService.getReceiptDetails(startDate, endDate, apiChannel);
            if (transactions.isEmpty()) {
                return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
            }

            ReceiptDetailsResponse receiptDetailsResponse = new ReceiptDetailsResponse();
            List<ReceiptHistoryData> statusDatas = new ArrayList<>();

            int x = 1;
            for (KioskTransactionEntity transactionEntity : transactions) {
                ReceiptHistoryData statusData = new ReceiptHistoryData();
                statusData.setTransId(transactionEntity.getReceiptNumber());
                statusData.setPrintTime(NvxDateUtils.formatDate(transactionEntity.getAxCartCheckoutDateTime(), NvxDateUtils.KIOSK_TICKET_TIME_PRINT_FORMAT));

                if (KioskTransactionStatus.TICKET_NOT_ISSUED.getCode().equals(transactionEntity.getStatus())
                        || KioskTransactionStatus.TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode().equals(transactionEntity.getStatus())) {
                    statusData.setIsTicketFail("true");
                } else {
                    statusData.setIsTicketFail("false");
                }
                statusData.setIndex(x++);
                statusDatas.add(statusData);
            }

            receiptDetailsResponse.setStatusDataArrayList(statusDatas);

            return new ResponseEntity(new ApiResult<>(true, "", "", receiptDetailsResponse), HttpStatus.OK);
          
        } catch (Exception e) {
            log.error("!!! System exception encountered [adminSearchReceiptDetails] !!!");
            return new ResponseEntity<ApiResult<ReceiptDetailsResponse>>(handleUncaughtException(e, ReceiptDetailsResponse.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-hardware-status", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<HardwareStatus>> getHardwareStatus(HttpServletRequest request, HttpServletResponse response) {
        try {
            log.info("Entered get-hardware-status...");
            HardwareStatus hardwareStatus = new HardwareStatus();
            HttpEntity<String> requestEntity = new HttpEntity<>(null, getJSONHttpHeaders());
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            KioskEventEntity eventEntity = new KioskEventEntity();
            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setApiChannel(apiChannel);

            boolean statusS = defaultKioskOpsService.addHardwareStatus(eventEntity);

            RestTemplate template = new RestTemplate();
            ResponseEntity<ReceiptStatusResult> printerReceiptStatus = template.getForEntity(PRINT_RECEIPT_STATUS_URL, ReceiptStatusResult.class);
            ResponseEntity<TicketStatusResult> printerTicketStatus = template.getForEntity(PRINT_TICKET_STATUS_URL, TicketStatusResult.class);
            String paymentTerminalStatus = template.postForObject(PAYMENT_ADAPTER_STATUS_URL, requestEntity, String.class);
            JsonParser parser = new JsonParser();
            JsonObject result = (JsonObject) parser.parse(paymentTerminalStatus);
            log.info(result.toString());
            if ("true".equals(result.get("result").getAsString())) {
                String responseCode = result.getAsJsonArray("callbacks").get(0).getAsJsonObject().getAsJsonPrimitive("responseCode").getAsString();
                if ("00".equals(responseCode)) {
                    hardwareStatus.setPaymentTerminalStatus("false");
                    hardwareStatus.setPaymentTerminalMsg("Not Connected");
                }
            }

            hardwareStatus.setReceiptPrinterStatus(printerReceiptStatus.getBody().getStatus() == 1 ? "false" : "true");
            hardwareStatus.setReceiptPrinterMsg(printerReceiptStatus.getBody().getErrorMessage());

            hardwareStatus.setTicketPrinterStatus(printerTicketStatus.getBody().getStatus() == 1 ? "false" : "true");
            hardwareStatus.setTicketPrinterMsg(printerReceiptStatus.getBody().getErrorMessage());


            // if(printResult==null | printResult.getBody()==null ){
            // return new ResponseEntity(new ApiResult<>(false, "", "", ""),
            // HttpStatus.OK);
            // }else if(printResult.getBody().getErrorCode()!=0 &&
            // printResult.getBody().getErrorCode()!=0 &&
            // printResult.getBody().getSystemError()==0) {
            // return new ResponseEntity(new ApiResult<>(false, "", "", ""),
            // HttpStatus.OK);
            // }
            return new ResponseEntity(new ApiResult<>(true, "", "", hardwareStatus), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getPrinterDetails] !!!");
            return new ResponseEntity<ApiResult<HardwareStatus>>(handleUncaughtException(e, HardwareStatus.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-receipt-details-by-transid", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ReceiptMain>> getReceiptDetailsByTransId(HttpServletRequest request, HttpServletResponse response) {
        try {
            log.info("Entered  getReceiptDetailsByTransId...");
            
            String transId = this.getStringValue(request, "tranid", "");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
            
            ReceiptMain receiptMain = defaultKioskOpsService.getTransactionDetails(transId, apiChannel);
            
            if(null != receiptMain){
                return new ResponseEntity(new ApiResult<>(true, "", "", receiptMain), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("!!! System exception encountered [getReceiptDetailsByTransId] !!!");
            return new ResponseEntity<ApiResult<ReceiptMain>>(handleUncaughtException(e, ReceiptMain.class, ""), HttpStatus.OK);
        }
        return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
    }
    
    @RequestMapping(value = "get-company-receipt-details", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<ReceiptMain>> getCompanyDetails(HttpServletRequest request, HttpServletResponse response) {
        try {
            log.info("Entered  getCompanyDetails...");

            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            ReceiptMain companyReceipt = defaultKioskOpsService.getCompanyReceiptDetails(apiChannel);
            if (null != companyReceipt) {
                return new ResponseEntity(new ApiResult<>(true, "", "", companyReceipt), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("!!! System exception encountered [getPrinterDetails] !!!");
            return new ResponseEntity<ApiResult<ReceiptMain>>(handleUncaughtException(e, ReceiptMain.class, ""), HttpStatus.OK);
        }
        return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
    }

    @RequestMapping(value = "get-Settlement-info", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> getSettlementInfo(HttpServletRequest request, HttpServletResponse response) {
        try {
            log.info("get-Settlement ...");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
            // if value is 0 = two buttons need to enable
            // if value is 1 = with button need to disable
            KioskInfoEntity kioskInfoEntity = this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel));
            String status = defaultKioskOpsService.getSettlementInfo(apiChannel, kioskInfoEntity);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", status), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getOperationMode] !!!");
            return new ResponseEntity<ApiResult<String>>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "add-settlement-info", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Boolean>> addSettlementInfo(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {

        try {
            log.info("check-settlement ...");
            String settlement = this.getStringValue(request, "value", "SWS");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");
         
            String msg = "";
            Boolean result = Boolean.FALSE;
            if (StringUtils.isNotBlank(apiChannel)){
                if (SHUTDOWN_WITH_SETTLEMENT.equals(settlement)) {
                    result  = defaultKioskOpsService.processDeviceSettlement(PAYMENT_ADAPTER_URL, this.defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(apiChannel)), apiChannel);
                }
            }
            return new ResponseEntity<>(new ApiResult<>(true, "", msg, result), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [addSettlementInfo] !!!");
            return new ResponseEntity<ApiResult<Boolean>>(handleUncaughtException(e, Boolean.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-operation-mode-cms", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<ScheduledMaintenance>> getOperationModeFromCMS(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {
        try {
            ScheduledMaintenance maintenance = new ScheduledMaintenance();

            log.info("get-operation-mode-cms ...");
            String apiChannel = Optional.ofNullable((String) request.getHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL)).orElse("");

            maintenance = getScheduleMaintenanceFromCMS(apiChannel);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", maintenance), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getOperationMode] !!!");
            return new ResponseEntity<ApiResult<ScheduledMaintenance>>(handleUncaughtException(e, ScheduledMaintenance.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "print-receipt-details", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ApiResult<Result>> getPrinterDetails(@RequestBody String body) {
        try {
            log.info("Entered  getPrinterDetails...");
            HttpEntity<String> requestEntity = new HttpEntity<>(null, getJSONHttpHeaders());

            RestTemplate template = new RestTemplate();//PRINT_RECEIPT_DETAILS_URL //http://127.0.0.1:9100/api/print-receipt-details
            ResponseEntity<Result> printResult = template.postForEntity(PRINT_RECEIPT_DETAILS_URL, requestEntity, Result.class);

            if (printResult == null | printResult.getBody() == null) {
                return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
            } else if (printResult.getBody().getErrorCode() != 0 && printResult.getBody().getErrorCode() != 0 && printResult.getBody().getSystemError() == 0) {
                return new ResponseEntity(new ApiResult<>(false, "", "", ""), HttpStatus.OK);
            }
            return new ResponseEntity(new ApiResult<>(true, "", "", printResult), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getPrinterDetails] !!!");
            return new ResponseEntity<ApiResult<Result>>(handleUncaughtException(e, Result.class, ""), HttpStatus.OK);
        }
    }

    protected HttpHeaders getJSONHttpHeaders() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    private ScheduledMaintenance getScheduleMaintenanceFromCMS(String channel) throws ParseException {
        return defaultKioskManagementService.getOperationModeFromCMS(channel);
    }

    public void shutdownKiosk() throws RuntimeException, IOException {
        String shutdownCommand;
        String operatingSystem = System.getProperty("os.name");

        if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
            shutdownCommand = "shutdown -h now";
        } else if ("Windows 7".startsWith(operatingSystem)) {
            shutdownCommand = "shutdown.exe -s -t 5";
        } else if ("Windows 8".startsWith(operatingSystem)) {
            shutdownCommand = "shutdown.exe -s -t 5";
        } else {
            throw new RuntimeException("Unsupported operating system.");
        }

        Runtime.getRuntime().exec(shutdownCommand);

    }
    
    private String getStringValue(ServletRequest request, String paramName, String defaultValue){
        
        if(!request.getParameter(paramName).isEmpty()){
            return (String) request.getParameter(paramName);
        } else{
            return defaultValue;
        }
        
        
    }
}
