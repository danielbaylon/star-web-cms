package com.enovax.star.cms.kiosk.api.store.controller;

import com.enovax.star.cms.kiosk.api.store.service.booking.IKioskBookingService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CrossSellCart;
import com.enovax.star.cms.commons.model.booking.CrossSellCartItem;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.kiosk.api.store.service.cmsproduct.IKioskProductCmsService;
import com.enovax.star.cms.kiosk.api.store.service.cmsproduct.IKioskProductService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/store-data/")
public class OnlineStoreKioskDataController extends BaseStoreKioskApiController {

    @Autowired
    private HttpSession session;

    @Autowired
    private IKioskBookingService bookingService;
    @Autowired
    private IKioskProductCmsService productCmsService;
    @Autowired
    private IKioskProductService productService;

    @Autowired
    private IKioskManagementService kioskService;

    @RequestMapping(value = "get-products", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<AxProductRelatedLinkPackage>> apiGetProducts(@RequestBody String channelCode) {

        log.info("Entered apiGetCrossSellItems...");

        try {
            ApiResult<AxProductRelatedLinkPackage> result = productService.getValidRelatedAxProducts(StoreApiChannels.fromCode(channelCode),
                    kioskService.getKioskInfo(StoreApiChannels.fromCode(channelCode)));

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxProductRelatedLinkPackage.class, ""), HttpStatus.OK);
        }
    }

    // @RequestMapping(value = "get-promotions", method = {RequestMethod.GET,
    // RequestMethod.POST})
    // public ResponseEntity<ApiResult<PromotionLinkPackage>> apiGetPromotions(
    // @RequestBody String channelCode) {
    //
    //
    // log.info("Entered apiGetCrossSellItems...");
    //
    // try {
    // ApiResult<PromotionLinkPackage> result =
    // productService.getFullDiscountData(StoreApiChannels.fromCode(channelCode));
    //
    // return new ResponseEntity<>(result, HttpStatus.OK);
    //
    // } catch (Exception e) {
    // log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
    // return new ResponseEntity<>(handleUncaughtException(e,
    // PromotionLinkPackage.class, ""), HttpStatus.OK);
    // }
    // }

    @RequestMapping(value = "get-cs", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<CrossSellLinkPackage>> apiGetCS(@RequestBody String channelCode) {

        log.info("Entered apiGetCrossSellItems...");

        try {
            ApiResult<CrossSellLinkPackage> result = productService.getFullUpCrossSellData(StoreApiChannels.fromCode(channelCode));

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CrossSellLinkPackage.class, ""), HttpStatus.OK);
        }
    }

    // TODO tharaka move to new module @@@
    @RequestMapping(value = "get-cross-sell-items/{locale}", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<CrossSellCart>> apiGetCrossSellItems(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @PathVariable("locale") String locale, @RequestBody String productNodeName) {

        log.info("Entered apiGetCrossSellItems...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            String sessionId = session.getId();

            FunCartDisplay funCartDisplay = bookingService.constructCartDisplay(channel, sessionId);

            ApiResult<List<CrossSellCartItem>> result = productCmsService.getAllCrossSellCartItemsFor(funCartDisplay, channelCode, productNodeName, locale);

            CrossSellCart crossSellCart = new CrossSellCart();
            crossSellCart.setCrossSellCartItems(result.getData());

            return new ResponseEntity<>(new ApiResult<>(true, "", "", crossSellCart), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCrossSellItems] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CrossSellCart.class, ""), HttpStatus.OK);
        }
    }
}
