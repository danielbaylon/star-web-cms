package com.enovax.star.cms.kiosk.api.store.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.enovax.star.cms.kiosk.api.store.controller.exception.LoginControllerException;
import com.enovax.star.cms.kiosk.api.store.service.ops.IKioskOpsService;

import info.magnolia.cms.security.auth.callback.CredentialsCallbackHandler;
import info.magnolia.cms.security.auth.callback.PlainTextCallbackHandler;
import info.magnolia.cms.security.auth.login.FormLogin;
import info.magnolia.cms.security.auth.login.LoginResult;

@Controller
@RequestMapping("/ops/")
public class OnlineStoreKioskOpsLoginController extends FormLogin {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private IKioskOpsService defaultKioskOpsService;

    @RequestMapping(value = "login", method = { RequestMethod.GET, RequestMethod.POST })
    public String login(HttpServletRequest request, HttpServletResponse response) throws LoginControllerException {

        log.info("login ...");

        Optional<String> mgnlUserId_ = Optional.ofNullable((String) request.getParameter("mgnlUserId"));
        Optional<String> mgnlUserPSWD_ = Optional.ofNullable((String) request.getParameter("mgnlUserPSWD"));
        Optional<String> channel = Optional.ofNullable((String) request.getParameter("channel"));
        String mgnlRealm = "mgnlRealm";

        log.info("mgnlUserId: " + mgnlUserId_.get());
        log.info("mgnlUserPSWD: " + mgnlUserPSWD_.get());

        String realm = StringUtils.defaultString(request.getParameter(mgnlRealm));
        CredentialsCallbackHandler callbackHandler = new PlainTextCallbackHandler(mgnlUserId_.get(), mgnlUserPSWD_.get().toCharArray(), realm);
        LoginResult result = null;
        try {
            result = authenticate(callbackHandler, super.getJaasChain());

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        if (result.getStatus() != LoginResult.STATUS_SUCCEEDED) {
            throw new LoginControllerException("Unauthorized user");
        } else {
            defaultKioskOpsService.login(channel.get());
        }
        return "redirect:/kiosk-ops/detail";
    }

}
