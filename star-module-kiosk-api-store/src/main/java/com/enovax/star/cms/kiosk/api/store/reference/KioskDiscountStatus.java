package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskDiscountStatus {

    NONE("0"), //
    RESERVED("1"), //
    SOLD("2"), //
    ISSUED("9"), //
    CANCELLED("10");
    private final String code;

    private KioskDiscountStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
