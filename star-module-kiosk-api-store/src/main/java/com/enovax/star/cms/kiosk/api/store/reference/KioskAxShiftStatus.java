package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskAxShiftStatus {

    NONE("0"), //
    OPEN("1"), //
    COLSE("2"), //
    BLIND_CLOSED("3"), //
    SUSPENDED("4"); //

    private final String code;

    private KioskAxShiftStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
