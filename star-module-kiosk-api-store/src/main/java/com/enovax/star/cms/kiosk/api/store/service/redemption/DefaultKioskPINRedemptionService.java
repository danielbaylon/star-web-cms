
package com.enovax.star.cms.kiosk.api.store.service.redemption;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.TicketType;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxStarCart;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.commons.model.booking.KioskFunCart;
import com.enovax.star.cms.commons.model.booking.KioskFunCartRedemptionDisplayItem;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCart;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartPinTicketListCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketListCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdatePrintStatusCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventDate;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroup;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxFacilityClassEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartTransactionData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintHistory;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintTokenEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdateRetailTicketStatusCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdatedTicketStatusEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskBlacklistTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutCancelRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutCompleteRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutPinStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartPinValidateAndReserveRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartValidateQuantityRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetCartsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetRedemptionPinStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskPINRedemptionUnlockRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSendRedeemTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdatePrintStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutCompleteResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutStartPINResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdatePrintStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateAndReserveResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateQuantityResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetPINRedemptionStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSendRedeemTicketToAXResponse;
import com.enovax.star.cms.commons.model.ticket.Facility;
import com.enovax.star.cms.commons.model.ticket.TicketData;
import com.enovax.star.cms.commons.service.axcrt.kiosk.AxStarKioskService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketStatus;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketTokenList;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketToken;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTicketPrintStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.repository.cart.IKioskCartManager;
import com.enovax.star.cms.kiosk.api.store.service.arm.IKioskRoboticArmService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixPrinterService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;

@Service
public class DefaultKioskPINRedemptionService implements IKioskPINRedemptionService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskPINRedemptionService.class);

    @Autowired
    private AxStarKioskService axStarService;

    @Autowired
    private IKioskManagementService kioskManagementService;

    @Autowired
    private IKioskCartManager cartManager;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;

    @Autowired
    private IKioskTransactionDao transactionDao;

    @Autowired
    private IKioskRoboticArmService armService;
    
    @Autowired
    private IKioskZabbixPrinterService zabbixPrinterService;

    private AxStarServiceResult<KioskCartValidateAndReserveResponse> reserveTicket(StoreApiChannels channel, AxPINRedemptionStartData redemptionData, KioskInfoEntity kioskInfo,
            Boolean completed) {

        AxStarServiceResult<KioskCartValidateAndReserveResponse> cartValidateAndReserveResult = new AxStarServiceResult<>();
        try {

            KioskCartPinValidateAndReserveRequest validateAndReserveRequest = new KioskCartPinValidateAndReserveRequest();
            for (AxPINRedemptionStartTransactionData data : redemptionData.getAxPINRedemptionStartTransactionDataList()) {
                // 3 is barcode
                if (("1".equals(data.getIsCapacity()) && "1".equals(data.getAllowChangeEventDate())) || "3".equals(data.getPrintingBehavior())) {
                    AxCartPinTicketListCriteria criteria = new AxCartPinTicketListCriteria();

                    if (StringUtils.isNotBlank(data.getEventDate())) {
                        Date eventDate = NvxDateUtils.parseAxDate(data.getEventDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);
                        criteria.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                        criteria.setEventGroupId(data.getEventGroupId());
                        // override default event goup if event line belongs to
                        // different group , wired NEC API !
                        if ("1".equals(data.getAllowChangeEventDate())) {
                            if (data.getAxEventGroupList() != null && data.getAxEventGroupList().getAxEventGroups() != null) {
                                for (AxEventGroup eventGroup : data.getAxEventGroupList().getAxEventGroups()) {
                                    criteria.setEventGroupId(eventGroup.getEventGroupIdOption());
                                }
                            }
                        }
                        criteria.setEventLineId(data.getEventLineId());
                    }
                    if (completed != null) {
                        criteria.setIsUpdateCapacity("1");
                    } else {
                        criteria.setIsUpdateCapacity("0");
                    }
                    criteria.setItemId(data.getItemId());
                    criteria.setLineId(data.getLineID());
                    if (completed != null && !completed) {
                        criteria.setQty("0");
                    } else {
                        criteria.setQty(data.getQty());
                    }

                    criteria.setAccountNum(redemptionData.getCustAccount());
                    if ("1".equals(data.getIsCapacity()) && "1".equals(data.getAllowChangeEventDate())) {
                        criteria.setIsCapacityReserved("0");
                    } else {
                        criteria.setIsCapacityReserved("1");
                    }

                    if ("0".equals(data.getIsCapacity())) {
                        criteria.setOpenStartDateTime(data.getTicketStartDateTime());
                        criteria.setOpenEndDateTime(data.getTicketEndDateTime());

                    }
                    criteria.setProductId("0");
                    criteria.setNoOfPax(data.getNoOfPax());
                    criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                    criteria.setStaffId(kioskInfo.getUserId());
                    criteria.setTransactionId(kioskInfo.getCurrentCartId());
                    criteria.setLineId(data.getInventTransId());
                    criteria.setRefTransactionId(data.getSalesId());
                    criteria.setDescriptioin(redemptionData.getPackageName());
                    criteria.setPackageName(redemptionData.getPackageName());
                    criteria.setPinCode(redemptionData.getpINCode());
                    validateAndReserveRequest.getAxCartTicketListCriteria().add(criteria);
                }
            }
            if (validateAndReserveRequest.getAxCartTicketListCriteria().size() != 0) {
                cartValidateAndReserveResult = this.axStarService.apiKioskCartPinValidateAndReserve(channel, validateAndReserveRequest);
            } else {
                cartValidateAndReserveResult.setSuccess(Boolean.TRUE);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return cartValidateAndReserveResult;
    }

    private String getTokenData(AxCartRetailTicketTableEntity axTicket) {
        String tokenData = "";
        KioskTicketTokenList tokenList = new KioskTicketTokenList();
        for (AxPrintTokenEntity token : axTicket.getTokenList()) {
            String key = token.getTokenKey();
            String value = token.getValue();
            KioskAxTicketToken ticketToken = KioskAxTicketToken.toEnum(key);
            if (ticketToken != null) {
                switch (ticketToken) {
                case BarcodeEndDate:
                    tokenList.setBarcodeEndDate(value);
                    break;
                case BarcodePLU:
                    tokenList.setBarcodePLU(value);
                    break;
                case BarcodeStartDate:
                    tokenList.setBarcodeStartDate(value);
                    break;
                case CustomerName:
                    tokenList.setCustomerName(value);
                    break;
                case Description:
                    tokenList.setDescription(value);
                    break;
                case DiscountApplied:
                    tokenList.setDiscountApplied(value);
                    break;
                case DisplayName:
                    tokenList.setDisplayName(value);
                    break;
                case EventDate:
                    tokenList.setEventDate(value);
                    break;
                case EventName:
                    tokenList.setEventName(value);
                    break;
                case Facilities:
                    tokenList.setFacilities(value);
                    break;
                case FacilityAction:
                    tokenList.setFacilityAction(value);
                    break;
                case ItemNumber:
                    tokenList.setItemNumber(value);
                    break;
                case LegalEntity:
                    tokenList.setLegalEntity(value);
                    break;
                case MixMatchDescription:
                    tokenList.setMixMatchDescription(value);
                    break;
                case MixMatchPackageName:
                    tokenList.setMixMatchPackageName(value);
                    break;
                case OperationIds:
                    tokenList.setOperationIds(value);
                    break;
                case PackageItem:
                    tokenList.setPackageItem(value);
                    break;
                case PackageItemName:
                    tokenList.setPackageItemName(value);
                    break;
                case PaxPerTicket:
                    tokenList.setPaxPerTicket(value);
                    break;
                case PriceType:
                    tokenList.setPriceType(value);
                case PrintLabel:
                    tokenList.setPrintLabel(value);
                    break;
                case ProductImage:
                    tokenList.setProductImage(value);
                    break;
                case ProductOwner:
                    tokenList.setProductOwner(value);
                    break;
                case ProductSearchName:
                    tokenList.setProductSearchName(value);
                    break;
                case ProductVarColor:
                    tokenList.setProductVarColor(value);
                    break;
                case ProductVarConfig:
                    tokenList.setProductVarConfig(value);
                    break;
                case ProductVarSize:
                    tokenList.setProductVarSize(value);
                    break;
                case ProductVarStyle:
                    tokenList.setProductVarStyle(value);
                    break;
                case PublishPrice:
                    tokenList.setPublishPrice(value);
                    break;
                case PackagePrintLabel:
                    tokenList.setPackagePrintLabel(value);
                    break;
                case TicketCode:
                    tokenList.setTicketCode(value);
                    break;
                case TicketNumber:
                    tokenList.setTicketNumber(value);
                    break;
                case TicketType:
                    tokenList.setTicketType(value);
                    break;
                case TicketValidity:
                    tokenList.setTicketValidity(value);
                    break;
                case TicketLineDesc:
                    tokenList.setTicketLineDesc(value);
                    break;
                case UsageValidity:
                    tokenList.setUsageValidity(value);
                    break;
                case ValidityEndDate:
                    tokenList.setValidityEndDate(value);
                    break;
                case ValidityStartDate:
                    tokenList.setValidityStartDate(value);
                    break;
                case TermAndCondition:
                    tokenList.setTermAndCondition(value);
                    break;
                case Disclaimer:
                    tokenList.setDisclaimer(value);
                    break;
                }
            }
        }
        tokenData = JsonUtil.jsonify(tokenList);

        return tokenData;
    }

    private KioskTicketEntity getTicket(AxCartRetailTicketTableEntity axTicket, KioskTransactionEntity tran) throws Exception {

        KioskTicketEntity ticket = new KioskTicketEntity();

        KioskTransactionItemEntity item = tran.getItems().stream().filter(i -> i.getItemId().equals(axTicket.getItemId())).findAny().orElse(new KioskTransactionItemEntity());
        ticket.setCreatedDateTime(NvxDateUtils.parseAxDate(axTicket.getCreatedDateTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        if (StringUtils.isNotBlank(axTicket.getEventDate()) && axTicket.getEventDate().contains("1900-01-01")) {
            ticket.setEventDate(DateTime.now().toDate());
        } else {
            ticket.setEventDate(NvxDateUtils.parseAxDate(axTicket.getEventDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        }
        ticket.setEndDate(NvxDateUtils.parseAxDate(axTicket.getEndDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setEventGroupId(axTicket.getEventGroupId());
        ticket.setEventLineId(axTicket.getEventLineId());
        ticket.setItemId(axTicket.getItemId());
        ticket.setModifiedDateTime(NvxDateUtils.parseAxDate(axTicket.getModifiedDateTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setOpenValidityId(axTicket.getOpenValidityId());
        ticket.setProductName(item.getProductName());
        ticket.setAxProductName(item.getAxProductName());
        ticket.setQrigTicketCode(axTicket.getOrigTicketCode());
        ticket.setQuantity(String.valueOf(new BigDecimal(axTicket.getQtyGroup()).intValue()));
        ticket.setReceiptId(axTicket.getReceiptId());
        ticket.setShiftId(axTicket.getShiftId());
        ticket.setStartDate(NvxDateUtils.parseAxDate(axTicket.getStartDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setTicketCode(axTicket.getTicketCode());
        ticket.setTicketTableId(axTicket.getTicketTableId());
        ticket.setTransactionId(axTicket.getTransactionId());
        ticket.setTransDate(NvxDateUtils.parseAxDate(axTicket.getTransDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setUsageValidityId(axTicket.getUsageValidityId());
        ticket.setTicketNumber(axTicket.getTicketTableId());
        ticket.setTemplateName(axTicket.getTemplateName());
        ticket.setStatus("0");
        ticket.setTokenData(this.getTokenData(axTicket));
        if (axTicket.isNeedsActivation()) {
            final TicketData rawData = new TicketData();
            rawData.setFacilities(new ArrayList<>());
            for (AxFacilityClassEntity axFacility : axTicket.getFacilities()) {
                if (StringUtils.isBlank(axFacility.getFacilityAction()) || StringUtils.isBlank(axFacility.getFacilityId())) {
                    log.error("empty falicty action and facility id");
                }
                Facility facility = new Facility();
                facility.setFacilityAction(new Integer(Optional.ofNullable(axFacility.getFacilityAction()).orElse("0")));
                facility.setFacilityId(axFacility.getFacilityId());
                facility.setOperationIds(axFacility.getOperationIds());
                rawData.getFacilities().add(facility);
            }
            rawData.setQuantity(1);
            rawData.setStartDate(NvxDateUtils.formatDate(ticket.getStartDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
            rawData.setEndDate(NvxDateUtils.formatDate(ticket.getEndDate(), NvxDateUtils.AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT));
            rawData.setTicketCode(axTicket.getTicketCode());
            ticket.setCodeData(JsonUtil.jsonify(rawData));
            ticket.setBarcodeBase64(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)));
        } else {
            ticket.setThirdParty(Boolean.TRUE);
            ticket.setCodeData(axTicket.getTicketCode());
        }
        ticket.setTran(tran);
        return ticket;
    }

    @Override
    public FunCartDisplay constructRedeemCartDisplay(StoreApiChannels channel, String sessionId) {
        KioskFunCart funCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            funCart = new KioskFunCart();
            funCart.setCartId(sessionId);
            funCart.setChannelCode(channel.code);
            AxStarCart axCart = new AxStarCart();
            axCart.setCartLines(new ArrayList<>());
            funCart.setAxCart(axCart);
        }
        FunCartDisplay display = this.constructRedeemCartDisplay(channel, funCart);
        return display;
    }

    private AxStarServiceResult<KioskGetPINRedemptionStartResponse> getPin(StoreApiChannels channel, KioskInfoEntity kioskInfo, String pinCode) {
        KioskGetRedemptionPinStartRequest kioskGetPINRedemptionStartRequest = new KioskGetRedemptionPinStartRequest();
        kioskGetPINRedemptionStartRequest.setRetailStoreId(kioskInfo.getStoreNumber());
        kioskGetPINRedemptionStartRequest.setRetailTerminalId(kioskInfo.getTerminalNumber());
        kioskGetPINRedemptionStartRequest.setSessionId(kioskInfo.getCurrentShiftSequence().toString());
        kioskGetPINRedemptionStartRequest.setUserId(kioskInfo.getUserId());

        kioskGetPINRedemptionStartRequest.setError(true);
        kioskGetPINRedemptionStartRequest.setPinCode(pinCode);

        AxStarServiceResult<KioskGetPINRedemptionStartResponse> getPinResult = axStarService.apiKioskGetPINRedemptionStart(channel, kioskGetPINRedemptionStartRequest);

        return getPinResult;
    }

    private AxStarServiceResult<KioskCartCheckoutStartPINResponse> checkoutPin(StoreApiChannels channel, AxPINRedemptionStartData redemptionData, KioskInfoEntity kioskInfo,
            Integer retry) {
        KioskCartCheckoutPinStartRequest checkoutPinStartRequest = new KioskCartCheckoutPinStartRequest();
        retry = retry--;
        for (AxPINRedemptionStartTransactionData data : redemptionData.getAxPINRedemptionStartTransactionDataList()) {
            AxCartPinTicketListCriteria criteria = new AxCartPinTicketListCriteria();
            criteria.setDataLevelValue("4");
            criteria.setDescriptioin(data.getSourceRecId());
            criteria.setEventDate(data.getEventDate());
            if ("1".equals(data.getIsCapacity())) {
                criteria.setEventGroupId(data.getEventGroupId());
                // override default event goup if event line belongs to
                // different group , wired NEC API !
                if ("1".equals(data.getAllowChangeEventDate())) {
                    if (data.getAxEventGroupList() != null && data.getAxEventGroupList().getAxEventGroups() != null) {
                        for (AxEventGroup eventGroup : data.getAxEventGroupList().getAxEventGroups()) {
                            criteria.setEventGroupId(eventGroup.getEventGroupIdOption());
                        }
                    }
                }
                criteria.setEventLineId(data.getEventLineId());

            } else {
                criteria.setOpenStartDateTime(data.getTicketStartDateTime());
                criteria.setOpenEndDateTime(data.getTicketEndDateTime());
                criteria.setEventGroupId("");
                criteria.setEventLineId("");
            }
            criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
            criteria.setStaffId(kioskInfo.getUserId());
            criteria.setIsCombined(data.getIsCombined());
            criteria.setIsReturn("0");
            criteria.setIsUpdateCapacity("1");
            criteria.setItemId(data.getItemId());
            criteria.setLineId(redemptionData.getPackageName());
            criteria.setNoOfPax(data.getNoOfPax());
            criteria.setPackageName(redemptionData.getPackageName());
            criteria.setPinCode(redemptionData.getpINCode());
            criteria.setProductId("0");
            criteria.setQty(String.valueOf(new BigDecimal(data.getQty()).intValue()));
            criteria.setReturnLineNumber("0");
            criteria.setSourceRecId(data.getSourceRecId());
            criteria.setSourceTable(redemptionData.getSourceTable());
            criteria.setTableGroupAll("0");
            criteria.setTransactionId(kioskInfo.getCurrentCartId());
            criteria.setRefTransactionId(data.getSalesId());
            criteria.setAccountNum(redemptionData.getCustAccount());
            criteria.setTicketCodePackage("");
            if ("1".equals(data.getIsCapacity()) && "1".equals(data.getAllowChangeEventDate())) {
                criteria.setIsCapacityReserved("0");
            } else {
                criteria.setIsCapacityReserved("1");
            }

            checkoutPinStartRequest.getAxCartTicketListCriterias().add(criteria);
            data.setQty(String.valueOf(new BigDecimal(data.getQty()).toBigInteger()));
            data.setOriginalQty(String.valueOf(new BigDecimal(data.getOriginalQty()).toBigInteger()));
            data.setQtyRedeemed(String.valueOf(new BigDecimal(data.getQtyRedeemed()).toBigInteger()));

            checkoutPinStartRequest.getAxPINRedemptionStartTransactionList().add(data);

        }
        checkoutPinStartRequest.setStoreId(kioskInfo.getStoreNumber());
        checkoutPinStartRequest.setTerminalId(kioskInfo.getTerminalNumber());
        checkoutPinStartRequest.setTransactionId(kioskInfo.getCurrentCartId());
        AxStarServiceResult<KioskCartCheckoutStartPINResponse> pinCheckoutStartResult = axStarService.apiKioskCartCheckoutStartPIN(channel, checkoutPinStartRequest);

        if (retry != 0 && !pinCheckoutStartResult.isSuccess() && pinCheckoutStartResult.getRawData() != null
                && pinCheckoutStartResult.getRawData().contains("CriticalStorageError")) {
            this.kioskManagementService.logoff(channel);
            this.kioskManagementService.logon(channel);

            kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            String tranId = this.kioskManagementService.getTransactionId(kioskInfo);
            kioskInfo.setCurrentCartId(tranId);
            this.kioskManagementService.updateKioskInfo(kioskInfo);
            checkoutPinStartRequest.setTransactionId(kioskInfo.getCurrentCartId());
            pinCheckoutStartResult = axStarService.apiKioskCartCheckoutStartPIN(channel, checkoutPinStartRequest);
        }
        return pinCheckoutStartResult;
    }

    private KioskTransactionEntity createTransaction(KioskInfoEntity kioskInfo, AxPINRedemptionStartData redemptionData) {
        KioskTransactionEntity tran = new KioskTransactionEntity();
        Date now = DateTime.now().toDate();
        tran.setAxCartCheckoutDateTime(now);
        tran.setAxCartId(kioskInfo.getCurrentCartId());
        tran.setAxReceiptNumber(kioskInfo.getCurrentCartId());
        tran.setAxReceiptId(kioskInfo.getCurrentReceiptSequence());
        tran.setRedemptionPinCode(redemptionData.getpINCode());
        tran.setKiosk(kioskInfo);
        tran.setReceiptNumber(ProjectUtils.generateReceiptNumber(now));
        tran.setStatus(KioskTransactionStatus.RECOVERY_FLOW_STARTED.getCode());
        tran.setTotalAmount(BigDecimal.ZERO);
        if ("1".equals(redemptionData.getAllowPartialRedemption())) {
            tran.setType(KioskTransactionType.B2B_REDEMPTION.getCode());
            tran.setAllowPartiallRedemption(Boolean.TRUE);
        } else {
            tran.setType(KioskTransactionType.B2C_REDEMPTION.getCode());
            tran.setAllowPartiallRedemption(Boolean.FALSE);
        }

        BigDecimal gstAmount = tran.getTotalAmount().multiply(new BigDecimal(1.07)).divide(new BigDecimal(0.07)).setScale(2, RoundingMode.HALF_UP);
        tran.setGstAmount(gstAmount);
        return tran;
    }

    private void getTransactionItem(StoreApiChannels channel, List<AxPINRedemptionStartTransactionData> pinLineList, KioskTransactionEntity tran) {

        KioskTransactionItemEntity item = null;
        for (AxPINRedemptionStartTransactionData pinLine : pinLineList) {
            item = new KioskTransactionItemEntity();
            item.setTran(tran);
            tran.getItems().add(item);
            item.setAxProductName(pinLine.getItemName());
            item.setSelectedEventDate(pinLine.getEventDate());
            item.setEventGroupId(pinLine.getEventGroupId());
            // override default event goup if event line belongs to
            // different group , wired NEC API !

            if ("1".equals(pinLine.getAllowChangeEventDate())) {
                if (pinLine.getAxEventGroupList() != null && pinLine.getAxEventGroupList().getAxEventGroups() != null) {
                    for (AxEventGroup eventGroup : pinLine.getAxEventGroupList().getAxEventGroups()) {
                        item.setEventGroupId(eventGroup.getEventGroupIdOption());
                    }
                }
            }
            item.setEventLineId(pinLine.getEventLineId());
            item.setQuantity(String.valueOf(new BigDecimal(pinLine.getOriginalQty()).intValue()));
            item.setItemId(pinLine.getItemId());
            item.setListingId(pinLine.getSourceRecId());
            item.setOriginalQty(new BigDecimal(pinLine.getOriginalQty()).intValue());
            item.setRedeemedQty(new BigDecimal(pinLine.getQty()).intValue());
            item.setRemainingQty(item.getOriginalQty() - new BigDecimal(pinLine.getQtyRedeemed()).intValue() - item.getRedeemedQty());
            item.setSubTotal(item.getAmount());
        }
    }

    @Override
    public ApiResult<KioskTransactionEntity> checkout(StoreApiChannels channel, String sessionId, List<KioskFunCartRedemptionDisplayItem> items) {

        ApiResult<KioskTransactionEntity> result = new ApiResult(ApiErrorCodes.General);

        Map<String, List<AxPINRedemptionStartTransactionData>> toBeRedeemData = new HashMap<>();
        List<AxPINRedemptionStartTransactionData> toBeRedeemList = new ArrayList<>();
        List<AxPINRedemptionStartTransactionData> fullPinLinesList = new ArrayList<>();
        try {
            KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
            if (savedCart == null) {
                savedCart = new KioskFunCart();
            }
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            AxPINRedemptionStartData redemptionData = savedCart.getAxPINRedemptionStartData();

            // update QTY for partial
            if ("1".equals(redemptionData.getAllowPartialRedemption())) {
                for (AxPINRedemptionStartTransactionData data : redemptionData.getAxPINRedemptionStartTransactionDataList()) {
                    KioskFunCartRedemptionDisplayItem item = items.stream().filter(i -> i.getListingId().equals(data.getItemId())).findAny().orElse(null);
                    if (item != null) {
                        String key = item.getListingId() + (StringUtils.isBlank(item.getEventGroupId()) ? "" : item.getEventGroupId())
                                + (StringUtils.isBlank(item.getEventLineId()) ? "" : item.getEventLineId());
                        if (toBeRedeemData.get(key) == null) {
                            List<AxPINRedemptionStartTransactionData> dataList = new ArrayList<>();
                            dataList.add(data);
                            toBeRedeemData.put(key, dataList);
                        } else {
                            toBeRedeemData.get(key).add(data);
                        }
                    }
                }

                for (Map.Entry<String, List<AxPINRedemptionStartTransactionData>> entry : toBeRedeemData.entrySet()) {
                    String key = entry.getKey();
                    List<AxPINRedemptionStartTransactionData> dataList = entry.getValue();
                    KioskFunCartRedemptionDisplayItem item = items.stream().filter(i -> key.equals(i.getListingId()
                            + (StringUtils.isBlank(i.getEventGroupId()) ? "" : i.getEventGroupId()) + (StringUtils.isBlank(i.getEventLineId()) ? "" : i.getEventLineId())))
                            .findAny().orElse(null);
                    if (dataList.size() > 1) {
                        Integer toBeRedeemedQty = item.getQty();
                        for (AxPINRedemptionStartTransactionData data : dataList) {
                            if (toBeRedeemedQty != 0) {
                                Integer remainingQty = new BigDecimal(data.getOriginalQty()).subtract(new BigDecimal(data.getQtyRedeemed())).intValue();
                                if (remainingQty <= toBeRedeemedQty) {
                                    toBeRedeemedQty = toBeRedeemedQty - remainingQty;
                                } else {
                                    data.setQty(toBeRedeemedQty.toString());
                                    toBeRedeemedQty = 0;
                                }
                                toBeRedeemList.add(data);
                            } else {
                                data.setQty("0"); // existing pin lines already
                                                  // have enough QTY to redeem
                            }
                            fullPinLinesList.add(data);
                        }

                    } else if (dataList.size() == 1) {
                        AxPINRedemptionStartTransactionData data = dataList.get(0);
                        Integer remainingQty = new BigDecimal(data.getOriginalQty()).subtract(new BigDecimal(data.getQtyRedeemed())).intValue();
                        if (item.getQty() < remainingQty) {
                            data.setQty(item.getQty().toString());

                        }
                        toBeRedeemList.add(data);
                        fullPinLinesList.add(data);
                    } else {
                        log.error("no redeptioin data found in the map for " + key);
                    }

                }
                redemptionData.setAxPINRedemptionStartTransactionDataList(toBeRedeemList);
            } else {
                fullPinLinesList.addAll(redemptionData.getAxPINRedemptionStartTransactionDataList());
            }
            // update event line and date for open dated

            for (AxPINRedemptionStartTransactionData data : redemptionData.getAxPINRedemptionStartTransactionDataList()) {
                String key = data.getItemId() + (StringUtils.isBlank(data.getEventGroupId()) ? "" : data.getEventGroupId())
                        + (StringUtils.isBlank(data.getEventLineId()) ? "" : data.getEventLineId());
                KioskFunCartRedemptionDisplayItem item = items.stream().filter(i -> key.equals(i.getListingId()
                        + (StringUtils.isBlank(data.getEventGroupId()) ? "" : i.getEventGroupId()) + (StringUtils.isBlank(i.getEventLineId()) ? "" : data.getEventLineId())))
                        .findAny().orElse(null);
                if (data.getEventDate() == null && !StringUtils.isBlank(data.getEventGroupId())) {
                    data.setEventLineId(item.getEventLineId());
                    String eventDate = item.getSelectedEventDate(); // "dd/MM/yyyy"
                    Date axEventDate = NvxDateUtils.parseAxDate(eventDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    if (axEventDate != null) {
                        String axEventDateInString = NvxDateUtils.formatDate(axEventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);
                        data.setEventDate(axEventDateInString);
                    } else {
                        log.error("ax event date parse failed for " + item.getSelectedEventDate());
                    }
                }
            }
            ApiResult<KioskCartValidateQuantityResponse> validateQtyResult = this.checkCartQuantity(channel, redemptionData, kioskInfo);
            if (!validateQtyResult.isSuccess() || (validateQtyResult.getData() != null && "1".equalsIgnoreCase(validateQtyResult.getData().getErrorStatus()))) {
                result.setErrorCode(validateQtyResult.getErrorCode());
                result.setMessage(validateQtyResult.getMessage());
            } else {
                AxStarServiceResult<KioskCartValidateAndReserveResponse> reserveTicketResult = this.reserveTicket(channel, redemptionData, kioskInfo, null);
                if (reserveTicketResult.isSuccess()) {
                    AxStarServiceResult<KioskCartCheckoutStartPINResponse> pinCheckoutStartResult = this.checkoutPin(channel, redemptionData, kioskInfo, 3);
                    if (pinCheckoutStartResult.isSuccess()) {
                        KioskFunCart currentFunCart = getCurrentFunCart(channel, sessionId);
                        currentFunCart.setAxPINRedemptionStartData(redemptionData);
                        cartManager.saveCart(channel, currentFunCart);

                        KioskTransactionEntity tran = this.createTransaction(kioskInfo, redemptionData);
                        this.getTransactionItem(channel, fullPinLinesList, tran);
                        for (AxCartRetailTicketTableEntity axTicket : pinCheckoutStartResult.getData().getAxCartRetailTicketTableEntityList()) {

                            List<AxPINRedemptionStartTransactionData> dataList = redemptionData.getAxPINRedemptionStartTransactionDataList().stream()
                                    .filter(d -> d.getItemId().equals(axTicket.getItemId())).collect(Collectors.toList());
                            for (AxPINRedemptionStartTransactionData data : dataList) {
                                data.setExpiryDate(axTicket.getEndDate());
                            }
                            tran.getTickets().add(this.getTicket(axTicket, tran));
                        }

                        transactionDao.save(tran);
                        cartManager.saveCart(channel, savedCart);
                        result = new ApiResult<>(true, "", "", tran);
                    } else {
                        result.setMessage(pinCheckoutStartResult.getError().getMessage());
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    private ApiResult<KioskCartValidateQuantityResponse> checkCartQuantity(StoreApiChannels channel, AxPINRedemptionStartData redemptionData, KioskInfoEntity kioskInfo)
            throws Exception {
        ApiResult<KioskCartValidateQuantityResponse> apiResult = new ApiResult<>(ApiErrorCodes.General);

        KioskCartValidateQuantityRequest request = new KioskCartValidateQuantityRequest();
        List<AxCartTicketListCriteria> criteriaList = new ArrayList<>();
        for (AxPINRedemptionStartTransactionData data : redemptionData.getAxPINRedemptionStartTransactionDataList()) {

            AxCartTicketListCriteria criteria = new AxCartTicketListCriteria();

            if (data.getEventDate() == null && "0".equals(data.getIsCapacity())) {
                criteria.setEventDate(NvxDateUtils.formatDate(DateTime.now().toDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
            } else {
                Date eventDate = NvxDateUtils.parseAxDate(data.getEventDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);
                criteria.setEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
            }
            criteria.setEventGroupId(data.getEventGroupId());

            // override default event goup if event line belongs to
            // different group , wired NEC API !
            if ("1".equals(data.getAllowChangeEventDate())) {
                if (data.getAxEventGroupList() != null && data.getAxEventGroupList().getAxEventGroups() != null) {
                    for (AxEventGroup eventGroup : data.getAxEventGroupList().getAxEventGroups()) {
                        criteria.setEventGroupId(eventGroup.getEventGroupIdOption());
                    }
                }
            }
            criteria.setEventLineId(data.getEventLineId());
            criteria.setItemId(data.getItemId());
            criteria.setLineId(data.getLineID());
            criteria.setIsUpdateCapacity("0");
            criteria.setProductId(data.getSourceRecId());
            criteria.setQty(data.getQty());
            criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
            criteria.setStaffId(kioskInfo.getUserId());
            criteria.setTransactionId("CartValidate");
            criteriaList.add(criteria);

        }
        request.setAxCartTicketListCriteria(criteriaList);

        if (criteriaList.size() == 0) {
            apiResult.setSuccess(Boolean.TRUE);
        } else {
            AxStarServiceResult<KioskCartValidateQuantityResponse> cartValidateQuantityResult = this.axStarService.apiKioskCartValidateQuantity(channel, request);
            if (cartValidateQuantityResult.isSuccess()) {
                KioskCartValidateQuantityResponse response = cartValidateQuantityResult.getData();
                if (response != null && "0".equals(response.getErrorStatus()) && StringUtils.isBlank(response.getErrorMessage())) {
                    apiResult = new ApiResult<>(Boolean.TRUE, "", "", response);
                } else {
                    String itemIds = redemptionData.getAxPINRedemptionStartTransactionDataList().stream().map(d -> d.getItemId()).collect(Collectors.joining(","));
                    log.error("cart quantity check failed for item ids : " + itemIds + ".");
                    // AX original : Insufficient quantity for selected product
                    // -
                    // 1102103001 Available Qty: 0
                    apiResult.setMessage(ApiErrorCodes.InsufficientQty.message);
                    apiResult.setErrorCode(ApiErrorCodes.InsufficientQty.code);
                }
            }
        }
        return apiResult;
    }

    @Override
    public ApiResult<KioskTransactionEntity> startRedemption(StoreApiChannels channel, String sessionId, String pinCode) {
        ApiResult<KioskTransactionEntity> result = new ApiResult(ApiErrorCodes.General);
        try {

            if (armService.isReady(null, channel.code, pinCode) && armService.init(null, channel.code, pinCode)) {
                KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
                if (savedCart == null) {
                    savedCart = new KioskFunCart();
                }
                KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);

                kioskManagementService.getNextTransactionId(channel, kioskInfo);
                String transactionId = this.kioskManagementService.getTransactionId(kioskInfo);
                kioskInfo.setCurrentCartId(transactionId);
                kioskManagementService.updateKioskInfo(kioskInfo);

                KioskGetCartsRequest request = new KioskGetCartsRequest();
                request.setCartId(kioskInfo.getCurrentCartId());

                AxStarServiceResult<AxCart> getCartsResult = this.axStarService.apiKioskGetCarts(channel, request);
                if (getCartsResult.isSuccess()) {
                    AxPINRedemptionStartData redemptionData = null;
                    AxStarServiceResult<KioskGetPINRedemptionStartResponse> getPinResult = this.getPin(channel, kioskInfo, pinCode);

                    if (getPinResult.isSuccess()) {
                        redemptionData = getPinResult.getData().getAxPINRedemptionStartDataList().stream().findAny().orElse(null);
                        if (redemptionData == null) {
                            result.setMessage(getPinResult.getError().getMessage());
                        } else {
                            result.setSuccess(true);
                            KioskFunCart currentFunCart = getCurrentFunCart(channel, sessionId);
                            currentFunCart.setAxPINRedemptionStartData(redemptionData);
                            cartManager.saveCart(channel, currentFunCart);
                        }
                    } else {
                        result.setErrorCode(getPinResult.getError().getExceptionType());
                        result.setMessage(getPinResult.getError().getExceptionMessage());
                    }

                } else {
                    log.error("GET CART FAILED " + request.getCartId());
                }
            } else {
                log.error("init robotic arm failed");
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return result;

    }

    // private BigDecimal getAxProductPrice(StoreApiChannels channel, String
    // itemId) {
    // BigDecimal price = BigDecimal.ZERO;
    // try {
    // Node product = kioskfn.getAXProductByDisplayProductNumber(channel.code,
    // itemId);
    // if (product != null &&
    // product.hasProperty(AXProductProperties.ProductPrice.getPropertyName()))
    // {
    // price = new
    // BigDecimal(product.getProperty(AXProductProperties.ProductPrice.getPropertyName()).getString());
    // }
    // } catch (Exception e) {
    // log.error(e.getMessage(), e);
    // }
    // return price;
    // }

    private String getAxProductTicketType(StoreApiChannels channel, String itemId) {
        String type = TicketType.Standard.display;
        try {
            Node product = kioskfn.getAXProductByDisplayProductNumber(channel.code, itemId);
            if (product != null && product.hasProperty(AXProductProperties.TicketType.getPropertyName())) {
                type = product.getProperty(AXProductProperties.TicketType.getPropertyName()).getString();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return type;
    }

    private FunCartDisplay constructRedeemCartDisplay(StoreApiChannels channel, KioskFunCart cart) {

        final FunCartDisplay cd = new FunCartDisplay();
        try {
            cd.setCartId(cart.getCartId());
            AxPINRedemptionStartData pinData = cart.getAxPINRedemptionStartData();
            final List<AxPINRedemptionStartTransactionData> items = cart.getAxPINRedemptionStartData().getAxPINRedemptionStartTransactionDataList();
            // BigDecimal totalAmount = BigDecimal.ZERO;
            Integer totalQty = 0;
            FunCartDisplayCmsProduct cmsProduct = null;
            KioskFunCartRedemptionDisplayItem item = null;
            Boolean hasExistingItem = Boolean.FALSE;
            for (AxPINRedemptionStartTransactionData data : items) {

                KioskFunCartRedemptionDisplayItem existingItem = null;
                // find if this pin line already in fun cart
                for (FunCartDisplayCmsProduct cms : cd.getCmsProducts()) {
                    for (FunCartDisplayItem existing : cms.getItems()) {
                        String existingKey = item.getListingId() + (item.getEventLineId() == null ? "" : item.getEventLineId())
                                + (item.getEventGroupId() == null ? "" : item.getEventGroupId());
                        String pinKey = data.getItemId() + (data.getEventLineId() == null ? "" : data.getEventLineId())
                                + (data.getEventGroupId() == null ? "" : data.getEventGroupId());
                        if (pinKey.equals(existingKey)) {
                            hasExistingItem = Boolean.TRUE;
                            existingItem = (KioskFunCartRedemptionDisplayItem) existing;
                        }
                    }
                }

                if (hasExistingItem) {
                    Integer qty = new BigDecimal(data.getQty()).toBigInteger().intValue();
                    Integer remainingQty = new BigDecimal(data.getOriginalQty()).subtract(new BigDecimal(data.getQtyRedeemed())).toBigInteger().intValue();
                    existingItem.setQty(existingItem.getQty() + qty);
                    existingItem.setRemainingQty(existingItem.getRemainingQty() + remainingQty);
                    totalQty += qty;

                } else {
                    cmsProduct = new FunCartDisplayCmsProduct();
                    item = new KioskFunCartRedemptionDisplayItem();
                    cd.getCmsProducts().add(cmsProduct);
                    cmsProduct.getItems().add(item);
                    item.setName(data.getItemName());
                    item.setQty(new BigDecimal(data.getQty()).toBigInteger().intValue());
                    totalQty += item.getQty();

                    Date expiryDate = NvxDateUtils.parseAxDate(data.getExpiryDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);

                    item.setExpiryDate(NvxDateUtils.formatDate(expiryDate, NvxDateUtils.PRODUCT_DETAILS_DATE_FORMAT_DISPLAY));
                    // BigDecimal price = this.getAxProductPrice(channel,
                    // data.getItemId());
                    // item.setPrice(price);
                    // totalAmount = totalAmount.add(price.multiply(new
                    // BigDecimal(item.getQty())));

                    if (StringUtils.isNotBlank(data.getEventDate())) {
                        Date eventDate = NvxDateUtils.parseAxDate(data.getEventDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT);

                        item.setSelectedEventDate(NvxDateUtils.formatDate(eventDate, NvxDateUtils.PRODUCT_DETAILS_DATE_FORMAT_DISPLAY));
                    }
                    item.setDiscountLabel("");
                    item.setRemainingQty(new BigDecimal(data.getOriginalQty()).subtract(new BigDecimal(data.getQtyRedeemed())).toBigInteger().intValue());
                    item.setListingId(data.getItemId());
                    item.setEventGroupId(data.getEventGroupId());
                    // override default event goup if event line belongs to
                    // different group , wired NEC API !
                    if ("1".equals(data.getAllowChangeEventDate())) {
                        if (data.getAxEventGroupList() != null && data.getAxEventGroupList().getAxEventGroups() != null) {
                            for (AxEventGroup eventGroup : data.getAxEventGroupList().getAxEventGroups()) {
                                item.setEventGroupId(eventGroup.getEventGroupIdOption());
                            }
                        }
                    }
                    item.setEventLineId(data.getEventLineId());
                    item.setType(this.getAxProductTicketType(channel, data.getItemId()));
                    item.setIsCombined("1".equals(data.getIsCombined()) ? Boolean.TRUE : Boolean.FALSE);
                    item.setIsOpenDated("1".equals(data.getAllowChangeEventDate()) ? Boolean.TRUE : Boolean.FALSE);
                    item.setIsPartial("1".equals(pinData.getAllowPartialRedemption()) ? Boolean.TRUE : Boolean.FALSE);
                    DateTime endOfToday = DateTime.now().withHourOfDay(23).withSecondOfMinute(59);
                    item.setIsExpired((new DateTime(expiryDate).isAfter(endOfToday)) ? Boolean.TRUE : Boolean.FALSE);
                    item.setAllowChangeEventDate("1".equals(data.getAllowChangeEventDate()) ? Boolean.TRUE : Boolean.FALSE);
                    if (StringUtils.isBlank(data.getEventDate()) && data.getAxEventGroupList() != null) {

                        for (AxEventGroup group : data.getAxEventGroupList().getAxEventGroups()) {
                            List<String> eventDates = null;
                            if (group.getAxEventLineList() != null) {
                                for (AxEventLine line : group.getAxEventLineList().getAxEventLines()) {
                                    eventDates = new ArrayList<>();
                                    if (line.getEventDateList() != null) {
                                        for (AxEventDate date : line.getEventDateList().getAxEventDates()) {
                                            eventDates.add(date.getEventDateOption());
                                        }
                                    }
                                    if (eventDates.size() != 0) {
                                        item.getEventLines().add(line.getEventLineIdOption());
                                        item.getEventLineDates().put(line.getEventLineIdOption(), eventDates);
                                    }
                                }
                            }
                        }
                    }

                }

                hasExistingItem = Boolean.FALSE;
            }
            // cd.setTotal(totalAmount);
            cd.setTotalQty(totalQty);
            // cd.setTotalText(NvxNumberUtils.formatToCurrency(totalAmount, "S$
            // "));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return cd;
    }

    private KioskFunCart getCurrentFunCart(StoreApiChannels channel, String sessionId) {

        KioskFunCart funCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            funCart = new KioskFunCart();
            funCart.setCartId(sessionId);
            funCart.setChannelCode(channel.code);
        }
        AxStarCart axCart = new AxStarCart();
        axCart.setCartLines(new ArrayList<>());
        funCart.setAxCart(axCart);

        return funCart;
    }

    @Transactional
    private ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, KioskTransactionEntity tran, KioskFunCart funCart, HttpSession session) {
        DateTime now = DateTime.now();
        tran.setAxCartCheckoutDateTime(now.toDate());
        ApiResult<KioskTransactionEntity> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            tran.setStatus(KioskTransactionStatus.AX_REDEMPTION_FAILED.getCode());
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            String cartId = kioskInfo.getCurrentCartId();

            AxPINRedemptionStartData redemptionData = funCart.getAxPINRedemptionStartData();
            if (tran.getAllTicketsPrinted()) {

                KioskCartCheckoutCompleteRequest cartCheckoutCompleteRequest = new KioskCartCheckoutCompleteRequest();
                cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setDataLevelValue("4");
                cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setTransactionId(cartId);
                cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setIsPINRedemption("1");
                cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setAccountNum(redemptionData.getCustAccount());
                cartCheckoutCompleteRequest.getAxCartCheckoutCompleteCriteria().setSourceTable(redemptionData.getSourceTable());
                AxStarServiceResult<KioskCartCheckoutCompleteResponse> cartCheckoutCompleteResult = this.axStarService.apiKioskCartCheckoutComplete(channel,
                        cartCheckoutCompleteRequest);
                if (cartCheckoutCompleteResult.isSuccess()) {
                    KioskSendRedeemTicketRequest redeemTicketRequest = new KioskSendRedeemTicketRequest();
                    redeemTicketRequest.getCriteria().setLastRedemptionDate(NvxDateUtils.formatDate(now.toDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_ZONE_FORMAT));
                    redeemTicketRequest.getCriteria().setLastRedemptionLoc(kioskInfo.getPlacement());
                    redeemTicketRequest.getCriteria().setPinCode(redemptionData.getpINCode());
                    redeemTicketRequest.getCriteria().setReferenceId(redemptionData.getReferenceId());
                    redeemTicketRequest.getCriteria().setSessionId(redemptionData.getSessionNo());
                    redeemTicketRequest.getCriteria().setSourceTable(redemptionData.getSourceTable());
                    redeemTicketRequest.getCriteria().setTransactionId(kioskInfo.getCurrentCartId());
                    redeemTicketRequest.getCriteria().setUserId(kioskInfo.getUserId());
                    AxStarServiceResult<KioskSendRedeemTicketToAXResponse> sendRedeemTicketToAXResult = this.axStarService.apiKioskSendRedeemTicketToAX(channel,
                            redeemTicketRequest);
                    if (sendRedeemTicketToAXResult.isSuccess()) {
                        tran.setStatus(KioskTransactionStatus.AX_REDEMPTION_SUCCESSFUL.getCode());
                        apiResult = this.unlockPin(channel, session);
                        apiResult = new ApiResult<>(true, "", "", tran);

                    }
                }
            } else {
                KioskCartCheckoutCancelRequest cancelRequest = new KioskCartCheckoutCancelRequest();
                cancelRequest.getCriteria().setIsPINRedemption("1");
                cancelRequest.getCriteria().setTransactionId(kioskInfo.getCurrentCartId());
                cancelRequest.getCriteria().setStoreId(kioskInfo.getStoreNumber());
                cancelRequest.getCriteria().setStaffId(kioskInfo.getUserId());
                cancelRequest.getCriteria().setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                cancelRequest.getCriteria().setTerminalId(kioskInfo.getTerminalNumber());
                cancelRequest.getCriteria().setAccountNum(redemptionData.getCustAccount());
                cancelRequest.getCriteria().setSourceTable(redemptionData.getSourceTable());
                AxStarServiceResult<KioskCartCheckoutCompleteResponse> cartCheckoutCancelResult = this.axStarService.apiKioskCartCheckoutCancel(channel, cancelRequest);
                if (!cartCheckoutCancelResult.isSuccess()) {
                    log.error("Redemption apiKioskCartCheckoutCancel failed");
                } else {
                    apiResult = this.unlockPin(channel, session);
                }
            }

            this.transactionDao.save(tran);
            kioskInfo.setCurrentReceiptSequence(kioskInfo.getCurrentReceiptSequence() + 1);
            kioskInfo.setCurrentCartId("");
            this.kioskManagementService.updateKioskInfo(kioskInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            tran.setStatus(KioskTransactionStatus.AX_REDEMPTION_FAILED.getCode());
            this.transactionDao.save(tran);
            apiResult = new ApiResult<>(ApiErrorCodes.General);
        }

        return apiResult;
    }

    @Override
    public ApiResult<KioskTransactionEntity> unlockPin(StoreApiChannels channel, HttpSession session) {
        ApiResult<KioskTransactionEntity> apiResult = new ApiResult<>(ApiErrorCodes.General);
        KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
        List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(kioskInfo.getCurrentCartId());
        KioskTransactionEntity tran = null;
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        }

        final String sid = session.getId();
        final KioskFunCart cart = (KioskFunCart) cartManager.getCart(channel, sid);
        AxPINRedemptionStartData data = cart.getAxPINRedemptionStartData();

        KioskPINRedemptionUnlockRequest unlockRequest = new KioskPINRedemptionUnlockRequest();
        unlockRequest.getAxPINRedmeptionUnlockCriteria().setPinCode(data.getpINCode());
        unlockRequest.getAxPINRedmeptionUnlockCriteria().setSessionId(data.getSessionNo());
        unlockRequest.getAxPINRedmeptionUnlockCriteria().setUserId(data.getUserId());
        unlockRequest.getAxPINRedmeptionUnlockCriteria().setSourceTable(data.getSourceTable());

        if (this.axStarService.apiKioskPINRedemptionUnlock(channel, unlockRequest).isSuccess()) {
            apiResult = new ApiResult<>(true, "", "", tran);
        } else {
            log.error("UNLOCK PIN " + cart.getAxPINRedemptionStartData().getpINCode() + "FAILED");
        }

        return apiResult;

    }

    @Override
    public ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, HttpSession session) {
        KioskInfoEntity kiosk = this.kioskManagementService.getKioskInfo(channel);
        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(kiosk.getCurrentCartId());
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        } else {
            return new ApiResult<>(ApiErrorCodes.General);
        }

        final String sid = session.getId();
        final KioskFunCart cart = (KioskFunCart) cartManager.getCart(channel, sid);

        final ApiResult<KioskTransactionEntity> result = completeSale(channel, tran, cart, session);
        if (result.isSuccess()) {
            cartManager.removeCart(channel, sid);
            session.invalidate();
        }

        return result;
    }

    @Override
    public Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, KioskInfoEntity kiosk) {
        Boolean updated = Boolean.FALSE;

        final String sessionId = session.getId();
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        log.info("updatePrintStatus savedCart.getCartId() : " + savedCart.getCartId());
        KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
        log.info("updatePrintStatus kioskInfo.getCurrentCartId() : " + kioskInfo.getCurrentCartId());

        KioskTransactionEntity tran = null;
        List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(kioskInfo.getCurrentCartId());
        if (tranList.size() > 0) {
            tran = tranList.get(0);
        }
        tran.setAllTicketsPrinted(Boolean.TRUE);
        if (printTicketResult.hasErrors()) {
            tran.setAllTicketsPrinted(Boolean.FALSE);
        }
        this.transactionDao.save(tran);

        AxStarServiceResult<KioskCartValidateAndReserveResponse> reserveTicketResult = this.reserveTicket(channel, savedCart.getAxPINRedemptionStartData(), kioskInfo,
                tran.getAllTicketsPrinted());
        if (!reserveTicketResult.isSuccess() || (reserveTicketResult.getData() != null && "1".equals(reserveTicketResult.getData().getErrorStatus()))) {
            log.error("updatePrintStatus reserveTicket failed" + reserveTicketResult.getRawData());
        }

        KioskUpdatePrintStatusRequest request = new KioskUpdatePrintStatusRequest();
        AxCartTicketUpdatePrintStatusCriteria criteria = new AxCartTicketUpdatePrintStatusCriteria();
        request.setAxCartTicketUpdatePrintStatusCriteria(criteria);

        criteria.setDataAreaId("sdc");
        criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
        criteria.setStaffId(kioskInfo.getUserId());
        criteria.setStoreId(kioskInfo.getStoreNumber());
        criteria.setTerminalId(kioskInfo.getTerminalNumber());
        criteria.setTransactionId(kioskInfo.getCurrentCartId());
        criteria.setPinCode(savedCart.getAxPINRedemptionStartData().getpINCode());
        AxPrintHistory history = null;
        for (KioskTicketStatus ticket : printTicketResult.getPrintHistory()) {
            history = new AxPrintHistory();
            history.setErrorMessage(ticket.getErrorMessage());
            history.setId(ticket.getId());
            if (CollectionUtils.isNotEmpty(ticket.getStatusCodes())) {
                history.setStatusCodes(StringUtils.join(ticket.getStatusCodes()));
            }
            history.setSuccess(ticket.getSuccess());
            criteria.getAxPrintHistory().add(history);
            
            KioskTicketEntity entity = tran.getTickets().stream().filter(t -> t.getTicketCode().equals(ticket.getId())).findAny().orElse(null);
            if (ticket.getSuccess()) {
                entity.setStatus(KioskTicketPrintStatus.PRINTED.getCode());
            } else {
                entity.setStatus(KioskTicketPrintStatus.NOTPRINTED.getCode());
            }
        }

        AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> updatePrintedStatusResult = this.axStarService.apiKioskCartTicketUpdatePrintStatus(channel, request);
        if (updatePrintedStatusResult.isSuccess()) {
            if (StringUtils.isBlank(updatePrintedStatusResult.getData().getErrorMessage())) {
                updated = Boolean.TRUE;
            } else {
                log.error("apiKioskCartTicketUpdatePrintStatus failed" + updatePrintedStatusResult.getData().getErrorMessage());
            }
        }
        if (printTicketResult.hasErrors()) {
            KioskBlacklistTicketRequest blacklistRequest = null;
            for (KioskTicketStatus ticket : printTicketResult.getPrintHistory()) {
                if (ticket.getSuccess()) {
                    blacklistRequest = new KioskBlacklistTicketRequest();
                    AxUpdateRetailTicketStatusCriteria blacklistCriteria = blacklistRequest.getBlacklistTicketCriteria();
                    blacklistCriteria.setDataLevelValue("4");
                    blacklistCriteria.setReasonCode("BlackList");
                    blacklistCriteria.setStaffId(kioskInfo.getUserId());
                    blacklistCriteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                    blacklistCriteria.setStoreId(kioskInfo.getStoreNumber());
                    blacklistCriteria.setTerminalId(kioskInfo.getTerminalNumber());
                    blacklistCriteria.setTicketCode(ticket.getId());
                    blacklistCriteria.setTicketStatus(KioskAxTicketStatus.INACTIVE.getCode());
                    blacklistCriteria.setTransactionId(kioskInfo.getCurrentCartId());
                    AxStarServiceResult<AxUpdatedTicketStatusEntity> blacklistResult = this.axStarService.apiKioskBlacklistTickets(channel, blacklistRequest);
                    if (!blacklistResult.isSuccess() || (blacklistResult.isSuccess() && "1".equals(blacklistResult.getData().getIsError()))) {
                        log.error("failed black list ticket for ticket code " + ticket.getId());
                    }
                }
            }

            if (!armService.rejectTicket(kioskInfo, channel.code, tranList.stream().findFirst().get().getReceiptNumber())) {
                log.error("robotic arm failed to reject tickets");
            }
        } else {
            if (!armService.issueTicket(kioskInfo, channel.code, tranList.stream().findFirst().get().getReceiptNumber())) {
                log.error("robotic arm failed to issue tickets");
            }
        }
        
        zabbixPrinterService.checkTicketPrinterStatus(kiosk, channel.code);
       
        return updated;
    }

}
