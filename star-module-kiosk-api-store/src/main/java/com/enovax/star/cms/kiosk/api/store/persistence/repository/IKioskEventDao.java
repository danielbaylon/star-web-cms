package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IKioskEventDao extends JpaRepository<KioskEventEntity, Long> {

    //
//    @Query(value = "select TOP 1 * from kiosk_event order by id desc",)
//    public List<KioskEventEntity> getAllKioskEvent();
//
//
//    @Query(value = "SELECT o FROM KioskEventEntity o order by o.id desc")
//    public List<KioskEventEntity> getAllKioskEvents();
//
    @Query("SELECT o FROM KioskEventEntity o WHERE o.status = :status and o.value = :value and o.eventName = :eventName and o.apiChannel = :channelName")
    public  List<KioskEventEntity> getOperationBystatus(@Param("status") String status,@Param("value") String value,@Param("eventName") String eventName,@Param("channelName") String channelName);

    @Query("SELECT o FROM KioskEventEntity o WHERE o.status = :status and o.eventName = :eventName and o.apiChannel = :channelName")
    public  List<KioskEventEntity> getTopActiveOperationsByType(@Param("status") String status,@Param("eventName") String eventName, @Param("channelName") String channelName);

    public List<KioskEventEntity> findTop1ByApiChannelAndEventNameOrderByIdDesc(String apiChannel,String eventName);

    public List<KioskEventEntity> findTop1ByApiChannelAndEventNameAndValueOrderByIdDesc(String apiChannel,String eventName,String value);

}