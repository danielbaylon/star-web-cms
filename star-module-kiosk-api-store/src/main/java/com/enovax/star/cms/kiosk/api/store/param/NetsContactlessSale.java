package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;

/**
 * @author danielbaylon
 *
 */
public class NetsContactlessSale implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO: Check for the following fields: result transaction merchantId
     * terminalId commandId
     */
    private String result;

    private String transaction;

    private String merchantId;

    private String terminalId;

    private String commandId;

    private String responseCode;

    private String cardNumber;

    private Long transactionAmount;

    private String hostLabel;

    private String approvalCode;

    private String cardLabel;

    private String cardTypeValue;

    private String cardTypeDescription;

    private String hostTypeValue;

    private String hostTypeDescription;

    private String transactionInfoValue;

    private long transactionNetsOriginalBalance;

    private long transactionNetsTransactionAmount;

    private long transactionNetsNewBalance;

    private String batchNumber;

    private String netsPurchaseFee;

    private String netsClessSignedCert;

    private String netsClessTransCounter;

    private String netsClessCEPASVersion;

    private String entryModeValue;

    private String dateTime;

    private String rawData;

    public NetsContactlessSale() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(String hostLabel) {
        this.hostLabel = hostLabel;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardTypeValue() {
        return cardTypeValue;
    }

    public void setCardTypeValue(String cardTypeValue) {
        this.cardTypeValue = cardTypeValue;
    }

    public String getCardTypeDescription() {
        return cardTypeDescription;
    }

    public void setCardTypeDescription(String cardTypeDescription) {
        this.cardTypeDescription = cardTypeDescription;
    }

    public String getHostTypeValue() {
        return hostTypeValue;
    }

    public void setHostTypeValue(String hostTypeValue) {
        this.hostTypeValue = hostTypeValue;
    }

    public String getHostTypeDescription() {
        return hostTypeDescription;
    }

    public void setHostTypeDescription(String hostTypeDescription) {
        this.hostTypeDescription = hostTypeDescription;
    }

    public String getTransactionInfoValue() {
        return transactionInfoValue;
    }

    public void setTransactionInfoValue(String transactionInfoValue) {
        this.transactionInfoValue = transactionInfoValue;
    }

    public long getTransactionNetsOriginalBalance() {
        return transactionNetsOriginalBalance;
    }

    public void setTransactionNetsOriginalBalance(long transactionNetsOriginalBalance) {
        this.transactionNetsOriginalBalance = transactionNetsOriginalBalance;
    }

    public long getTransactionNetsTransactionAmount() {
        return transactionNetsTransactionAmount;
    }

    public void setTransactionNetsTransactionAmount(long transactionNetsTransactionAmount) {
        this.transactionNetsTransactionAmount = transactionNetsTransactionAmount;
    }

    public long getTransactionNetsNewBalance() {
        return transactionNetsNewBalance;
    }

    public void setTransactionNetsNewBalance(long transactionNetsNewBalance) {
        this.transactionNetsNewBalance = transactionNetsNewBalance;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getNetsPurchaseFee() {
        return netsPurchaseFee;
    }

    public void setNetsPurchaseFee(String netsPurchaseFee) {
        this.netsPurchaseFee = netsPurchaseFee;
    }

    public String getNetsClessSignedCert() {
        return netsClessSignedCert;
    }

    public void setNetsClessSignedCert(String netsClessSignedCert) {
        this.netsClessSignedCert = netsClessSignedCert;
    }

    public String getNetsClessTransCounter() {
        return netsClessTransCounter;
    }

    public void setNetsClessTransCounter(String netsClessTransCounter) {
        this.netsClessTransCounter = netsClessTransCounter;
    }

    public String getNetsClessCEPASVersion() {
        return netsClessCEPASVersion;
    }

    public void setNetsClessCEPASVersion(String netsClessCEPASVersion) {
        this.netsClessCEPASVersion = netsClessCEPASVersion;
    }

    public String getEntryModeValue() {
        return entryModeValue;
    }

    public void setEntryModeValue(String entryModeValue) {
        this.entryModeValue = entryModeValue;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getRawData() {
        return rawData;
    }

}
