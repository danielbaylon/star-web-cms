package com.enovax.star.cms.kiosk.api.store.config;

public class StarModuleApiStoreKioskConfig {

	private String kioskPaymentAdapter;
	
	private String kioskPaymentWebSocket;
	
	private String kioskScannerAdapter;
	
	private String kioskScannerWebSocket;

	public StarModuleApiStoreKioskConfig() {
	}
	
	public StarModuleApiStoreKioskConfig(String kioskPaymentAdapter, String kioskPaymentWebSocket,
			String kioskScannerAdapter, String kioskScannerWebSocket) {
		super();
		this.kioskPaymentAdapter = kioskPaymentAdapter;
		this.kioskPaymentWebSocket = kioskPaymentWebSocket;
		this.kioskScannerAdapter = kioskScannerAdapter;
		this.kioskScannerWebSocket = kioskScannerWebSocket;
	}

	public String getKioskPaymentAdapter() {
		return kioskPaymentAdapter;
	}

	public String getKioskPaymentwebSocket() {
		return kioskPaymentWebSocket;
	}

	public String getKioskScannerAdapter() {
		return kioskScannerAdapter;
	}

	public String getKioskScannerWebSocket() {
		return kioskScannerWebSocket;
	}

}
