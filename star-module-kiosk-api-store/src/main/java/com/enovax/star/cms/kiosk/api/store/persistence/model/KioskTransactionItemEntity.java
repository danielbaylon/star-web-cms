package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_tran_item", schema = "dbo")
public class KioskTransactionItemEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9103478435777708002L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oid")
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "product_id")
    private String listingId;

    @Column(name = "item_id")
    private String itemId;

    @Column(name = "cms_prod_name")
    private String productName;

    @Column(name = "ax_prod_name")
    private String axProductName;

    @Column(name = "selected_event_date")
    private String selectedEventDate;

    @Column(name = "event_group_id")
    private String eventGroupId;

    @Column(name = "event_line_id")
    private String eventLineId;

    @Column(name = "sub_total")
    private BigDecimal subTotal;

    @Column(name = "rdp_original_qty")
    private Integer originalQty;

    @Column(name = "rdp_redeemed_qty")
    private Integer redeemedQty;

    @Column(name = "rdp_remaining_qty")
    private Integer remainingQty;

    @Column(name = "parent_product_id")
    private String parentProductId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_tran_oid")
    private KioskTransactionEntity tran;

    public KioskTransactionItemEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public KioskTransactionEntity getTran() {
        return tran;
    }

    public void setTran(KioskTransactionEntity tran) {
        this.tran = tran;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSelectedEventDate() {
        return selectedEventDate;
    }

    public void setSelectedEventDate(String selectedEventDate) {
        this.selectedEventDate = selectedEventDate;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getAxProductName() {
        return axProductName;
    }

    public void setAxProductName(String axProductName) {
        this.axProductName = axProductName;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public Integer getOriginalQty() {
        return originalQty;
    }

    public void setOriginalQty(Integer originalQty) {
        this.originalQty = originalQty;
    }

    public Integer getRedeemedQty() {
        return redeemedQty;
    }

    public void setRedeemedQty(Integer redeemedQty) {
        this.redeemedQty = redeemedQty;
    }

    public Integer getRemainingQty() {
        return remainingQty;
    }

    public void setRemainingQty(Integer remainingQty) {
        this.remainingQty = remainingQty;
    }

    public String getParentProductId() {
        return parentProductId == null ? "" : parentProductId;
    }

    public void setParentProductId(String parentProductId) {
        this.parentProductId = parentProductId;
    }

}
