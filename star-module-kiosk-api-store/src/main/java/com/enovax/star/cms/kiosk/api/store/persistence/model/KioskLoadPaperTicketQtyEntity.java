package com.enovax.star.cms.kiosk.api.store.persistence.model;

import javax.persistence.*;

/**
 * Created by tharaka 23/09/2016.
 */
@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_paper_ticket_qty", schema = "dbo")
public class KioskLoadPaperTicketQtyEntity {

    @Id
    @Column(name = "id" )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_ticket_no")
    private String fromNumber;

    @Column(name = "end_ticket_no")
    private String toNumber;


    @ManyToOne(cascade=CascadeType.ALL )
    @JoinColumn(name = "paper_ticket_info_id")
    private KioskLoadPaperTicketInfoEntity ticketInfoEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public KioskLoadPaperTicketInfoEntity getTicketInfoEntity() {
        return ticketInfoEntity;
    }

    public void setTicketInfoEntity(KioskLoadPaperTicketInfoEntity ticketInfoEntity) {
        this.ticketInfoEntity = ticketInfoEntity;
    }
}
