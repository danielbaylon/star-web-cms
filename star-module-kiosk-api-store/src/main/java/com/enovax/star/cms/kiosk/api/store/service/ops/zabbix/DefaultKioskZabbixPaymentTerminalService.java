package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;

@Service
public class DefaultKioskZabbixPaymentTerminalService implements IKioskZabbixPaymentTerminalService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskZabbixPaymentTerminalService.class);

    @Autowired
    private IKioskZabbixAdapterService kioskZabbixAdapter;
    
    @Override
    public void sendNetsLogonError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("nets.logon.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }



}
