package com.enovax.star.cms.kiosk.api.store.service.product;

@Deprecated
public class KioskServiceException extends Exception {

    public KioskServiceException() {
    }

    public KioskServiceException(String message) {
        super(message);
    }

    public KioskServiceException(Throwable t) {
        super(t);
    }

    public KioskServiceException(String message, Throwable t) {
        super(message, t);
    }

}
