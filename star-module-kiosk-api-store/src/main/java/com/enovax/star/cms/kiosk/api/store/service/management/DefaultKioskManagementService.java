package com.enovax.star.cms.kiosk.api.store.service.management;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.kiosk.jcr.JcrKioskInfo;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDeviceActivationResult;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDeviceConfiguration;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEmployee;
import com.enovax.star.cms.commons.model.kiosk.odata.AxHardwareProfile;
import com.enovax.star.cms.commons.model.kiosk.odata.AxHardwareProfileCashDrawer;
import com.enovax.star.cms.commons.model.kiosk.odata.AxNumberSequenceSeedData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxShift;
import com.enovax.star.cms.commons.model.kiosk.odata.AxTemplateXMLEntityResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KiioskGetHardwareProfileByIdRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskActivationConnectionRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCloseShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskDeviceActivationRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetShiftsByStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskLogoffRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskLogonRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskOpenShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchXmlTemplateRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUseShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetLatestNumberSequenceResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetShiftsByStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchXmlTemplateResponse;
import com.enovax.star.cms.commons.service.axcrt.kiosk.AxStarKioskService;
import com.enovax.star.cms.kiosk.api.store.jcr.repository.kiosk.IKioskJcrRepository;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ScheduledMaintenance;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskInfoDao;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxSequence;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxShiftStatus;
import com.enovax.star.cms.kiosk.api.store.service.job.IKioskJobSchedulerService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixKioskManagementService;

/**
 *
 * @author Justin
 * @since 6 SEP 16
 */
@Service
public class DefaultKioskManagementService implements IKioskManagementService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskManagementService.class);

    private static final String LOGON_AUTH_PROVIDER = "UserAuthentication";

    private static final String LOGON_TYPE = "3";

    private static final String NONE_TRANSACTIOINAL_LOGON_ID = "-1";

    @Autowired
    private AxStarKioskService axStarService;
    
    @Autowired
    private IKioskJobSchedulerService scheduler;

    @Autowired
    private IKioskJcrRepository kioskJcrRepository;

    @Autowired
    private IKioskInfoDao kioskInfoDao;
    
    private Map<String, String> ticketTemplateMap = new HashMap<>();
    
    @Autowired
    private IKioskZabbixKioskManagementService kioskZabbixManagementService;
    
    

    private AxStarServiceResult<String> logoff(StoreApiChannels channel, KioskLogoffRequest request) {
        return this.axStarService.apiKioskLogoff(channel, request);
    }

    @Transactional
    public KioskInfoEntity activateDevice(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {
        if (StringUtils.isBlank(kioskInfo.getDeviceToken())) {
            logonForDeviceActivation(channel, kioskInfo);

            KioskDeviceActivationRequest axActivationRequest = new KioskDeviceActivationRequest();
            axActivationRequest.setDeviceNumber(kioskInfo.getDeviceNumber());
            axActivationRequest.setTerminalId(kioskInfo.getTerminalNumber());
            AxStarServiceResult<AxDeviceActivationResult> axDeviceActivationResult = axStarService.apiKioskActivation(channel, axActivationRequest);
            log.info("device activation result " + axDeviceActivationResult.isSuccess());

            AxDeviceActivationResult data = axDeviceActivationResult.getData();
            if (axDeviceActivationResult.isSuccess()) {

                kioskInfo.setChannelId(data.getDevice().getChannelId());
                kioskInfo.setChannel(channel.code);
                kioskInfo.setDeviceToken(data.getDevice().getToken());
                kioskInfo.setIsDeviceActivated(Boolean.TRUE);

                kioskInfoDao.saveAndFlush(kioskInfo);

                this.noneTransactionalLogoff(channel);
            } else {
                log.error("device activation failed for kiosk" + kioskInfo.getJcrNodeName());
                throw new Exception("device activation failed for kiosk " + kioskInfo.getJcrNodeName());
            }
        }
        return kioskInfo;
    }

    public KioskInfoEntity logonForDeviceActivation(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {

        KioskActivationConnectionRequest axActivationConnectionRequest = new KioskActivationConnectionRequest();

        axActivationConnectionRequest.getActivationConnectionRequest().setAuthenticationProvider(LOGON_AUTH_PROVIDER);
        axActivationConnectionRequest.getActivationConnectionRequest().setLogOnKey(kioskInfo.getUserId());
        axActivationConnectionRequest.getActivationConnectionRequest().setLogOnType(LOGON_TYPE);
        axActivationConnectionRequest.getActivationConnectionRequest().setPassword(kioskInfo.getUserPassword());
        axActivationConnectionRequest.getActivationConnectionRequest().setTransactionId(NONE_TRANSACTIOINAL_LOGON_ID);
        axActivationConnectionRequest.getActivationConnectionRequest().setUserId(kioskInfo.getUserId());
        AxStarServiceResult<AxEmployee> apiKioskActivationLogonResult = axStarService.apiKioskActivationLogon(channel, axActivationConnectionRequest);
        if (apiKioskActivationLogonResult.isSuccess()) {
            AxEmployee employee = apiKioskActivationLogonResult.getData();
            if (Boolean.FALSE.toString().toUpperCase().equals(employee.getChangePassword().toUpperCase())) {

            } else {
                log.error("logon for device activation failed as password required to be reset.");
                throw new Exception("kiosk user password expired");
            }
        }

        return kioskInfo;
    }

    private KioskLogonRequest getLogonRequest(KioskInfoEntity kioskInfo) {

        KioskLogonRequest axConnectionRequest = new KioskLogonRequest();
        axConnectionRequest.getConnectionRequest().setAuthenticationProvider(LOGON_AUTH_PROVIDER);
        axConnectionRequest.getConnectionRequest().setDeviceNumber(kioskInfo.getDeviceNumber());
        axConnectionRequest.getConnectionRequest().setDeviceToken(kioskInfo.getDeviceToken());
        axConnectionRequest.getConnectionRequest().setLogOnKey(kioskInfo.getUserId());
        axConnectionRequest.getConnectionRequest().setLogOnType(LOGON_TYPE);
        axConnectionRequest.getConnectionRequest().setPassword(kioskInfo.getUserPassword());
        axConnectionRequest.getConnectionRequest().setTransactionId(this.getTransactionId(kioskInfo));
        axConnectionRequest.getConnectionRequest().setUserId(kioskInfo.getUserId());
        axConnectionRequest.setStoreNumber(kioskInfo.getStoreNumber());
        return axConnectionRequest;
    }

    private void init(KioskInfoEntity kioskInfo) {
        // should reset cart id to blank on a new logon
        kioskInfo.setCurrentCartId("");
        // if receipt sequence not init then set init value
        if (kioskInfo.getCurrentReceiptSequence() == null) {
            kioskInfo.setCurrentReceiptSequence(0);
        }
    }

    @Transactional
    private KioskInfoEntity logon(StoreApiChannels channel, KioskInfoEntity kioskInfo, ApiResult<AxEmployee> apiResult) throws Exception {
        if (StringUtils.isNotBlank(kioskInfo.getDeviceToken())) {

            KioskLogonRequest logonRequest = this.getLogonRequest(kioskInfo);
            logonRequest.getConnectionRequest().setTransactionId(NONE_TRANSACTIOINAL_LOGON_ID);
            axStarService.apiKioskLogon(channel, logonRequest);

            getDeviceConfiguration(channel, kioskInfo);
            getHardwareProfile(channel, kioskInfo);
            getXmlTemplate(channel, kioskInfo);
            updateSequnceNumber(channel, kioskInfo);
            logoff(channel, new KioskLogoffRequest());

            AxStarServiceResult<AxEmployee> apiKioskLogonResult = axStarService.apiKioskLogon(channel, getLogonRequest(kioskInfo));

            if (apiKioskLogonResult.isSuccess()) {
                openShift(channel, kioskInfo);
                init(kioskInfo);
                this.kioskInfoDao.saveAndFlush(kioskInfo);
                
                scheduler.scheduleKioskShutdown(channel.code);
                AxEmployee employee = apiKioskLogonResult.getData();
                if (Boolean.FALSE.toString().toUpperCase().equals(employee.getChangePassword().toUpperCase())) {
                    apiResult.setSuccess(Boolean.TRUE);
                    apiResult.setData(employee);
                    apiResult.setErrorCode("");
                    apiResult.setMessage("");
                } else {
                    log.error("logon for device activation failed as password required to be reset.");
                    throw new Exception("kiosk user password expired");
                }

            }
        }
        return kioskInfo;
    }

    private KioskInfoEntity updateShiftSequnceNumber(StoreApiChannels channel, KioskInfoEntity kioskInfo) {
        AxStarServiceResult<KioskGetLatestNumberSequenceResponse> getLatestNumberSequenceResult = this.axStarService.apiKioskGetLatestNumberSequence(channel);
        if (getLatestNumberSequenceResult.isSuccess() && getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList() != null) {
            List<AxNumberSequenceSeedData> axNoSeqSeedDataList = getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList();
            AxNumberSequenceSeedData shiftSequence = axNoSeqSeedDataList.stream().filter(s -> KioskAxSequence.SHIFT.getCode().equals(s.getDataTypeValue())).findAny().orElse(null);
            if (shiftSequence != null) {
                kioskInfo.setCurrentShiftSequence(new Integer(shiftSequence.getDataValue()));
            }
            this.kioskInfoDao.saveAndFlush(kioskInfo);
        }
        return kioskInfo;
    }

    private KioskInfoEntity updateSequnceNumber(StoreApiChannels channel, KioskInfoEntity kioskInfo) {
        AxStarServiceResult<KioskGetLatestNumberSequenceResponse> getLatestNumberSequenceResult = this.axStarService.apiKioskGetLatestNumberSequence(channel);
        if (getLatestNumberSequenceResult.isSuccess() && getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList() != null) {
            List<AxNumberSequenceSeedData> axNoSeqSeedDataList = getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList();
            AxNumberSequenceSeedData transactionSequence = axNoSeqSeedDataList.stream().filter(s -> KioskAxSequence.TRANSACTION.getCode().equals(s.getDataTypeValue())).findAny()
                    .orElse(null);
            if (transactionSequence != null) {
                kioskInfo.setCurrentTransactionSequence(new Integer(transactionSequence.getDataValue()));
            }
            this.kioskInfoDao.saveAndFlush(kioskInfo);
        }
        return kioskInfo;
    }

    private KioskInfoEntity getDeviceConfiguration(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {

        AxStarServiceResult<AxDeviceConfiguration> getDeviceConfigurationResult = this.axStarService.apiKioskGetDeviceConfiguration(channel);

        if (getDeviceConfigurationResult.isSuccess()) {
            AxDeviceConfiguration config = getDeviceConfigurationResult.getData();
            kioskInfo.setStoreNumber(config.getStoreNumber());
            kioskInfo.setHardwareProfileId(config.getHardwareProfile());
            kioskInfo.setPlacement(config.getPlacement());
            this.kioskInfoDao.saveAndFlush(kioskInfo);
        } else {
            log.error("not able to get kiosk device configuration from AX.");
            throw new Exception("not able to get kiosk device configuration from AX.");
        }

        return kioskInfo;
    }

    private KioskInfoEntity getHardwareProfile(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {

        KiioskGetHardwareProfileByIdRequest request = new KiioskGetHardwareProfileByIdRequest();
        request.setHardwareProfileId(kioskInfo.getHardwareProfileId());
        AxStarServiceResult<AxHardwareProfile> getHardwareProfileByIdResult = this.axStarService.apiKiioskGetHardwareProfileById(channel, request);

        if (getHardwareProfileByIdResult.isSuccess()) {
            AxHardwareProfile profile = getHardwareProfileByIdResult.getData();
            List<AxHardwareProfileCashDrawer> axHardwareProfileCashDrawerList = profile.getAxHardwareProfileCashDrawerList();
            if (axHardwareProfileCashDrawerList != null && axHardwareProfileCashDrawerList.size() > 0) {
                kioskInfo.setCashDrawer(axHardwareProfileCashDrawerList.get(0).getDeviceName());
                this.kioskInfoDao.saveAndFlush(kioskInfo);
            }

        } else {
            log.error("not able to get kiosk hardware profile from AX.");
            throw new Exception("not able to get kiosk hardware profile from AX.");
        }

        return kioskInfo;
    }

    private KioskInfoEntity getXmlTemplate(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {

        AxStarServiceResult<KioskSearchXmlTemplateResponse> searchXmlTemplateResult = this.axStarService.apiKioskSearchXmlTemplate(channel, new KioskSearchXmlTemplateRequest());

        if (searchXmlTemplateResult.isSuccess() && searchXmlTemplateResult.getData().getEntityResponse() != null) {

            for (AxTemplateXMLEntityResponse ticketTemplate : searchXmlTemplateResult.getData().getEntityResponse()) {
                this.ticketTemplateMap.put(ticketTemplate.getTemplateName(), ticketTemplate.getXmlDocument());
            }

        } else {
            log.error("not able to get kiosk device configuration from AX.");
            throw new Exception("not able to get kiosk device configuration from AX.");
        }

        return kioskInfo;
    }

    @Override
    @Transactional
    public KioskInfoEntity getKioskInfo(StoreApiChannels channel) {
        KioskInfoEntity kioskInfo = null;
        JcrKioskInfo jcrKioskInfo = kioskJcrRepository.get(channel.code);
        if (jcrKioskInfo != null) {
            kioskInfo = kioskInfoDao.getByJcrNodeUuid(jcrKioskInfo.getJcrNodeUuid());
            if (kioskInfo == null) {
                kioskInfo = new KioskInfoEntity();
                kioskInfo.setJcrNodeName(jcrKioskInfo.getJcrName());
                kioskInfo.setJcrNodeUuid(jcrKioskInfo.getJcrNodeUuid());
            }
            kioskInfo.setChannel(channel.code);
            kioskInfo.setDeviceNumber(jcrKioskInfo.getDeviceNumber());
            kioskInfo.setTerminalNumber(jcrKioskInfo.getTerminalNumber());
            kioskInfo.setUserId(jcrKioskInfo.getUserId());
            kioskInfo.setUserPassword(jcrKioskInfo.getUserPassword());
            kioskInfo.setLocation(jcrKioskInfo.getLocation());
            kioskInfoDao.saveAndFlush(kioskInfo);
        }
        return kioskInfo;
    }

    public KioskInfoEntity openShift(StoreApiChannels channel, KioskInfoEntity kioskInfo) throws Exception {
        KioskGetShiftsByStatusRequest getShiftsByStatusRequest = new KioskGetShiftsByStatusRequest();
        getShiftsByStatusRequest.setStatusValue(KioskAxShiftStatus.OPEN.getCode());
        AxStarServiceResult<KioskGetShiftsByStatusResponse> getShiftsByStatusResponse = this.axStarService.apiKiioskGetShiftsByStatus(channel, getShiftsByStatusRequest);
        if (getShiftsByStatusResponse.isSuccess()) {
            List<AxShift> axShiftList = getShiftsByStatusResponse.getData().getAxShiftList();
            if (axShiftList.size() == 0) {
                // open new shift
                KioskOpenShiftRequest kioskOpenShiftRequest = new KioskOpenShiftRequest();
                this.updateShiftSequnceNumber(channel, kioskInfo);
                kioskOpenShiftRequest.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
                kioskOpenShiftRequest.setCashDrawer(kioskInfo.getCashDrawer());
                AxStarServiceResult<AxShift> kioskOpenShiftResult = this.axStarService.apiKioskOpenShift(channel, kioskOpenShiftRequest);
                if (kioskOpenShiftResult.isSuccess()) {
                    AxShift axShift = kioskOpenShiftResult.getData();
                    log.debug("shift " + axShift.getShiftId() + " opened.");
                } else {
                    new Exception("system failed to open shift " + kioskOpenShiftResult.getRawData());
                }
            } else {
                // use existing shift
                KioskUseShiftRequest kioskUseShiftRequest = new KioskUseShiftRequest();
                AxShift existingAxShift = axShiftList.get(0);
                kioskUseShiftRequest.setShiftId(existingAxShift.getShiftId());
                kioskUseShiftRequest.setTerminalId(kioskInfo.getTerminalNumber());
                kioskInfo.setCurrentShiftSequence(new Integer(existingAxShift.getShiftId()));
                AxStarServiceResult<AxShift> kioskUseShiftResult = this.axStarService.apiKioskUseShift(channel, kioskUseShiftRequest);
                if (!kioskUseShiftResult.isSuccess()) {
                    throw new Exception("system is not able to resume current shift " + kioskUseShiftResult.getRawData());
                }
            }
        } else {
            log.error("system is not able to get shift info");
            throw new Exception("system is not able to get shift info" + getShiftsByStatusResponse.getRawData());
        }
        return kioskInfo;
    }

    @Override
    public ApiResult<AxEmployee> logon(StoreApiChannels channel) {

        ApiResult<AxEmployee> apiResult = new ApiResult<>(ApiErrorCodes.General);

        try {
            KioskInfoEntity kioskInfo = this.getKioskInfo(channel);

            if (kioskInfo != null) {
                activateDevice(channel, kioskInfo);
                logon(channel, kioskInfo, apiResult);
            } else {
                apiResult.setMessage("logon failed no kiosk found");
                log.error("logon failed no kiosk found");
            }
        } catch (Exception e) {
            apiResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    @Override
    public String getTransactionId(KioskInfoEntity kioskInfo) {

        // store number - device number - sequence
        return kioskInfo.getStoreNumber() + "-" + kioskInfo.getDeviceNumber() + "-" + kioskInfo.getCurrentTransactionSequence();
    }

    @Override
    public ApiResult<AxShift> closeShift(StoreApiChannels channel) {
        ApiResult<AxShift> apiResult = new ApiResult<>(ApiErrorCodes.General);
        KioskInfoEntity kioskInfo = this.getKioskInfo(channel);
        try {
           KioskGetShiftsByStatusRequest getShiftsByStatusRequest = new KioskGetShiftsByStatusRequest();
            getShiftsByStatusRequest.setStatusValue(KioskAxShiftStatus.OPEN.getCode());
            AxStarServiceResult<KioskGetShiftsByStatusResponse> getShiftsByStatusResponse = this.axStarService.apiKiioskGetShiftsByStatus(channel, getShiftsByStatusRequest);
            if (getShiftsByStatusResponse.isSuccess()) {
                List<AxShift> axShiftList = getShiftsByStatusResponse.getData().getAxShiftList();
                if (axShiftList.size() > 0) {
                    AxShift axShift = axShiftList.get(0);
                    KioskCloseShiftRequest kioskCloseShiftRequest = new KioskCloseShiftRequest();
                    kioskCloseShiftRequest.setShiftId(axShift.getShiftId());
                    kioskCloseShiftRequest.setTerminalId(kioskInfo.getTerminalNumber());
                    kioskCloseShiftRequest.setTransactionId(this.getNextTransactionId(channel, kioskInfo));
                    AxStarServiceResult<AxShift> kioskCloseShiftResult = this.axStarService.apiKioskCloseShift(channel, kioskCloseShiftRequest);
                    if (kioskCloseShiftResult.isSuccess()) {
                        apiResult.setSuccess(Boolean.TRUE);
                        apiResult.setErrorCode("");
                        apiResult.setMessage("");
                        apiResult.setData(kioskCloseShiftResult.getData());
                    }
                }
            }

        } catch (Exception e) {
            kioskZabbixManagementService.sendCloseShiftError(kioskInfo, channel.code);
            apiResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    @Override
    @Transactional
    public ApiResult<String> logoff(StoreApiChannels channel) {
        ApiResult<String> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            KioskInfoEntity kioskInfo = this.getKioskInfo(channel);

            KioskLogoffRequest kioskLogoffRequest = new KioskLogoffRequest();

            kioskLogoffRequest.setTransactionId(this.getNextTransactionId(channel, kioskInfo));
            AxStarServiceResult<String> kioskLogoffResult = this.axStarService.apiKioskLogoff(channel, kioskLogoffRequest);
            if (kioskLogoffResult.isSuccess()) {
                apiResult.setSuccess(Boolean.TRUE);
                apiResult.setErrorCode("");
                apiResult.setMessage("");
            }
        } catch (Exception e) {
            apiResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        }
        return apiResult;

    }

    @Override
    public String getNextTransactionId(StoreApiChannels channel, KioskInfoEntity kioskInfo) {
        updateSequnceNumber(channel, kioskInfo);
        return getTransactionId(kioskInfo);
    }

    @Override
    @Transactional
    public KioskInfoEntity updateKioskInfo(KioskInfoEntity kioskInfo) {
        return this.kioskInfoDao.save(kioskInfo);
    }

    @Override
    @Transactional
    public ScheduledMaintenance getOperationModeFromCMS(String channel) throws ParseException {
        return kioskJcrRepository.getSchedule(channel);
    }

    @Override
    public Boolean noneTransactionalLogon(StoreApiChannels channel, KioskInfoEntity kioskInfo) {
        KioskLogonRequest logonRequest = this.getLogonRequest(kioskInfo);
        logonRequest.getConnectionRequest().setTransactionId(NONE_TRANSACTIOINAL_LOGON_ID);
        AxStarServiceResult<AxEmployee> logonResult = axStarService.apiKioskLogon(channel, logonRequest);
        return logonResult.isSuccess();
    }

    @Override
    public Boolean noneTransactionalLogoff(StoreApiChannels channel) {

        return this.logoff(channel, new KioskLogoffRequest()).isSuccess();
    }

    @Override
    public String getTikectTemplateXMLByName(String templateName) {

        return this.ticketTemplateMap.get(templateName);
    }

}
