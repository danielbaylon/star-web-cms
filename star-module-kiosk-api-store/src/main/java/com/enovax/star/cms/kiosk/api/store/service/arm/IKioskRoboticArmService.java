package com.enovax.star.cms.kiosk.api.store.service.arm;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskRoboticArmService {

    Boolean init(KioskInfoEntity kiosk, String channel, String...details);

    Boolean issueTicket(KioskInfoEntity kiosk, String channel, String...details);

    Boolean rejectTicket(KioskInfoEntity kiosk, String channel, String...details);

    Boolean isReady(KioskInfoEntity kiosk, String channel, String...details);

}
