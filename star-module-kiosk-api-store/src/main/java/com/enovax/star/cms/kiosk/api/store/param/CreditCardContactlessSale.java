package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;

/**
 * @author danielbaylon
 *
 */
public class CreditCardContactlessSale implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO: Check for the following fields: result transaction merchantId
     * terminalId commandId
     */
    private String result;

    private String transaction;

    private String merchantId;

    private String terminalId;

    private String commandId;

    private String responseCode;

    private String cardNumber;

    private Long transactionAmount;

    private String dateTime;

    private String expiryDate;

    private String retrievalReferenceNumber;

    private String approvalCode;

    private String emvDataValue;

    private String emvDataTVR;

    private String emvDataAID;

    private String emvDataTSI;

    private String emvDataAppLabel;

    private String emvDataTC;

    private String cardLabel;

    private String cardTypeValue;

    private String cardTypeDescription;

    private String hostTypeValue;

    private String hostTypeDescription;

    private String customData2;

    private String customData3;

    private String ecrUniqueTraceNumber;

    private String invoiceNumber;

    private String transactionInfoValue;

    private long transactionNetsOriginalBalance;

    private long transactionNetsTransactionAmount;

    private long transactionNetsNewBalance;

    private String additionalPrintingFlag;

    private String cardHolderName;

    private String batchNumber;

    private String entryModeValue;

    private String hostLabel;

    private String rawData;

    public CreditCardContactlessSale() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getEmvDataValue() {
        return emvDataValue;
    }

    public void setEmvDataValue(String emvDataValue) {
        this.emvDataValue = emvDataValue;
    }

    public String getEmvDataTVR() {
        return emvDataTVR;
    }

    public void setEmvDataTVR(String emvDataTVR) {
        this.emvDataTVR = emvDataTVR;
    }

    public String getEmvDataAID() {
        return emvDataAID;
    }

    public void setEmvDataAID(String emvDataAID) {
        this.emvDataAID = emvDataAID;
    }

    public String getEmvDataTSI() {
        return emvDataTSI;
    }

    public void setEmvDataTSI(String emvDataTSI) {
        this.emvDataTSI = emvDataTSI;
    }

    public String getEmvDataAppLabel() {
        return emvDataAppLabel;
    }

    public void setEmvDataAppLabel(String emvDataAppLabel) {
        this.emvDataAppLabel = emvDataAppLabel;
    }

    public String getEmvDataTC() {
        return emvDataTC;
    }

    public void setEmvDataTC(String emvDataTC) {
        this.emvDataTC = emvDataTC;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardTypeValue() {
        return cardTypeValue;
    }

    public void setCardTypeValue(String cardTypeValue) {
        this.cardTypeValue = cardTypeValue;
    }

    public String getCardTypeDescription() {
        return cardTypeDescription;
    }

    public void setCardTypeDescription(String cardTypeDescription) {
        this.cardTypeDescription = cardTypeDescription;
    }

    public String getHostTypeValue() {
        return hostTypeValue;
    }

    public void setHostTypeValue(String hostTypeValue) {
        this.hostTypeValue = hostTypeValue;
    }

    public String getHostTypeDescription() {
        return hostTypeDescription;
    }

    public void setHostTypeDescription(String hostTypeDescription) {
        this.hostTypeDescription = hostTypeDescription;
    }

    public String getCustomData2() {
        return customData2;
    }

    public void setCustomData2(String customData2) {
        this.customData2 = customData2;
    }

    public String getCustomData3() {
        return customData3;
    }

    public void setCustomData3(String customData3) {
        this.customData3 = customData3;
    }

    public String getEcrUniqueTraceNumber() {
        return ecrUniqueTraceNumber;
    }

    public void setEcrUniqueTraceNumber(String ecrUniqueTraceNumber) {
        this.ecrUniqueTraceNumber = ecrUniqueTraceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getTransactionInfoValue() {
        return transactionInfoValue;
    }

    public void setTransactionInfoValue(String transactionInfoValue) {
        this.transactionInfoValue = transactionInfoValue;
    }

    public long getTransactionNetsOriginalBalance() {
        return transactionNetsOriginalBalance;
    }

    public void setTransactionNetsOriginalBalance(long transactionNetsOriginalBalance) {
        this.transactionNetsOriginalBalance = transactionNetsOriginalBalance;
    }

    public long getTransactionNetsTransactionAmount() {
        return transactionNetsTransactionAmount;
    }

    public void setTransactionNetsTransactionAmount(long transactionNetsTransactionAmount) {
        this.transactionNetsTransactionAmount = transactionNetsTransactionAmount;
    }

    public long getTransactionNetsNewBalance() {
        return transactionNetsNewBalance;
    }

    public void setTransactionNetsNewBalance(long transactionNetsNewBalance) {
        this.transactionNetsNewBalance = transactionNetsNewBalance;
    }

    public String getAdditionalPrintingFlag() {
        return additionalPrintingFlag;
    }

    public void setAdditionalPrintingFlag(String additionalPrintingFlag) {
        this.additionalPrintingFlag = additionalPrintingFlag;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEntryModeValue() {
        return entryModeValue;
    }

    public void setEntryModeValue(String entryModeValue) {
        this.entryModeValue = entryModeValue;
    }

    public String getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(String hostLabel) {
        this.hostLabel = hostLabel;
    }

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public String getRawData() {
		return rawData;
	}
}
