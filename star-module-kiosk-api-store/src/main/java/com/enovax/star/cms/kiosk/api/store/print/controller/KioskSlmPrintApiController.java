package com.enovax.star.cms.kiosk.api.store.print.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

/**
 * Created by cornelius on 24/6/16.
 */

@RestController
@RequestMapping("/kiosk-slm/")
public class KioskSlmPrintApiController extends KioskPrintApiController {

    protected StoreApiChannels getChannel() {
        return StoreApiChannels.KIOSK_SLM;
    }
}