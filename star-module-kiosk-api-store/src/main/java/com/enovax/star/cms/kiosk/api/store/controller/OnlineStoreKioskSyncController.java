package com.enovax.star.cms.kiosk.api.store.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.service.sync.IKioskDBSyncService;

@Controller
@RequestMapping("/store-sync/")
public class OnlineStoreKioskSyncController {

    private static final Logger log = Logger.getLogger(OnlineStoreKioskSyncController.class);

    @Autowired
    private IKioskDBSyncService syncService;

    @RequestMapping(value = "persist/tran", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> saveTransaction(@RequestBody KioskTransactionEntity tran) {

        log.info("store sync save transaction");
        ApiResult<KioskTransactionEntity> result = new ApiResult<>();
        ResponseEntity<ApiResult<KioskTransactionEntity>> resposne = new ResponseEntity<>(result, HttpStatus.OK);
        try {
            KioskTransactionEntity saved = syncService.saveTransaction(tran);
            result.setSuccess(Boolean.TRUE);
            result.setData(saved);
        } catch (Exception e) {
            result.setSuccess(Boolean.FALSE);
            resposne = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            log.error(e.getMessage(), e);
        }
        return resposne;
    }

    @RequestMapping(value = "persist/event", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskEventEntity>> saveEvent(@RequestBody KioskEventEntity event) {

        log.info("store sync save event");
        ApiResult<KioskEventEntity> result = new ApiResult<>();
        ResponseEntity<ApiResult<KioskEventEntity>> resposne = new ResponseEntity<>(result, HttpStatus.OK);
        try {
            KioskEventEntity saved = syncService.saveEvent(event);
            result.setSuccess(Boolean.TRUE);
            result.setData(saved);
        } catch (Exception e) {
            result.setSuccess(Boolean.FALSE);
            resposne = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            log.error(e.getMessage(), e);
        }
        return resposne;
    }

    @RequestMapping(value = "persist/load-ticket-event", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskLoadPaperTicketInfoEntity>> saveLoadTicketEvent(@RequestBody KioskLoadPaperTicketInfoEntity event) {

        log.info("store sync save transaction");
        ApiResult<KioskLoadPaperTicketInfoEntity> result = new ApiResult<>();
        ResponseEntity<ApiResult<KioskLoadPaperTicketInfoEntity>> resposne = new ResponseEntity<>(result, HttpStatus.OK);
        try {
            KioskLoadPaperTicketInfoEntity saved = syncService.saveLoadTicketEvent(event);
            result.setSuccess(Boolean.TRUE);
            result.setData(saved);
        } catch (Exception e) {
            result.setSuccess(Boolean.FALSE);
            resposne = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            log.error(e.getMessage(), e);
        }
        return resposne;
    }

}
