package com.enovax.star.cms.kiosk.api.store.repository.cart;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.FunCart;

public interface IKioskCartManager {

    void saveCart(StoreApiChannels channel, FunCart cartToSave);

    FunCart getCart(StoreApiChannels channel, String cartId);

    void removeCart(StoreApiChannels channel, String cartId);
}
