package com.enovax.star.cms.kiosk.api.store.service.ticketgen;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.KioskReceiptTemplate;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.model.ticketgen.KioskPaymentItem;
import com.enovax.star.cms.commons.model.ticketgen.ReceiptItem;
import com.enovax.star.cms.commons.model.ticketgen.ReceiptMain;
import com.enovax.star.cms.commons.model.ticketgen.RedemptionReceiptItem;
import com.enovax.star.cms.commons.model.ticketgen.TicketIssuanceStatus;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.kiosk.api.store.param.NetsContactlessSale;
import com.enovax.star.cms.kiosk.api.store.param.NetsSale;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketTokenList;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketToken;
import com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.lowagie.text.DocumentException;

@Service
public class SimpleKioskTicketGenerationService implements IKioskTicketGenerationService {

    final Logger log = LoggerFactory.getLogger(SimpleKioskTicketGenerationService.class);

    @Autowired
    private IKioskManagementService kioskService;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;

    private String ticketXMLTemplateName = "kiosk-receipt-template";

    private String DUMMY_SIGNATURE = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    private String NO_SIGNATURE = "NO SIGNATURE REQUIRED";

    private String TICKET_ISSURANCE_COMPLETED = "COMPLETED";
    private String TICKET_ISSURANCE_NOT_ISSUED = "NOT ISSUED";
    private String TICKET_ISSURANCE_COMPLETED_MSG_1 = "All tickets are";
    private String TICKET_ISSURANCE_COMPLETED_MSG_2 = "Successfully issued";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_1 = "All tickets are NOT issued";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_2 = "You may processed to other";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_3 = "ticketing kiosk to print";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_4 = "your ticket";
    private String PAYMENT_MODE_NETS_PIN_DEBIT = KioskPaymentCardType.NETSPINDEBIT.getDescription();
    private String PAYMENT_MODE_NETS_FLASHPAY = KioskPaymentCardType.NFP.getDescription();

    private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_1 = "Please look for ticketing";
    private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_2 = "staff for assistance";
    private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_3 = "Redemption failed";
    private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_4 = "Service recovery required";

    private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_1 = "We apologize that we are not";
    private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_2 = "able to print your ticket here.";
    private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_3 = "Please proceed to our";
    private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_4 = "ticketing counter";

    @PostConstruct
    public void init() {
        String oldValue = System.getProperty("java.protocol.handler.pkgs");
        if (oldValue == null) {
            System.setProperty("java.protocol.handler.pkgs", "org.xhtmlrenderer.protocols");
        } else if (!oldValue.contains("org.xhtmlrenderer.protocols")) {
            System.setProperty("java.protocol.handler.pkgs", oldValue + "|org.xhtmlrenderer.protocols");
        }
    }

    @Override
    public byte[] generateTickets(ETicketDataCompiled compiledTicketData) throws IOException {
        final String template = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/eticket-template.html"));
        final Handlebars hb = new Handlebars();
        final Template hbTemplate = hb.compileInline(template);
        final Context hbContext = Context.newContext(compiledTicketData);
        final String generatedContent = hbTemplate.apply(hbContext);

        ByteArrayOutputStream output = null;
        try {

            output = new ByteArrayOutputStream();

            final ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(generatedContent);
            renderer.layout();
            renderer.createPDF(output, true);

            return output.toByteArray();
        } catch (DocumentException e) {
            log.error("Error encountered generating eTicket PDF", e);
            throw new IOException("Error encountered generating eTicket PDF", e);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private String getReceiptXmlTemplateByName(String templateName) {
        String xml = "";
        try {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/" + templateName);
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + templateName + ".xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", templateName, "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                xml = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    xml = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    xml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + templateName + ".xml"));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return xml;
    }

    @Override
    public String generateReceiptXmlWithQRCode(KioskTransactionEntity txn, String inputReceiptTemplateXml, String qrCode, StoreApiChannels channel) throws IOException {

        if (StringUtils.isEmpty(qrCode)) {
            return generateReceiptXml(txn, inputReceiptTemplateXml, channel);
        }

        if (null == txn.getRedemptionPinCode()) {
            inputReceiptTemplateXml = this.getReceiptXmlTemplateByName("kiosk-receipt-with-qrcode-template");
        } else {
            inputReceiptTemplateXml = this.getReceiptXmlTemplateByName("kiosk-redemption-receipt-with-qrcode-template");
        }

        String receiptXMLString = generateReceiptXml(txn, inputReceiptTemplateXml, channel);

        return receiptXMLString.replace("<Data></Data>", "<Data>" + qrCode + "</Data>");
    }

    private ReceiptItem createReceiptItem(KioskTransactionItemEntity tranItem, Integer i) {
        // only assign cms product name to first receipt item in same cms
        // product group
        ReceiptItem receiptItem = new ReceiptItem();
        receiptItem.setQty(tranItem.getQuantity());
        receiptItem.setAxProductName(getStringValue(tranItem.getAxProductName()));
        receiptItem.setAmount((NvxNumberUtils.formatToCurrency(Optional.ofNullable(tranItem.getAmount()).orElse(BigDecimal.ZERO), "$ ")));
        receiptItem.setSubtotalText(NvxNumberUtils.formatToCurrency(Optional.ofNullable(tranItem.getSubTotal()).orElse(BigDecimal.ZERO), "$ "));
        if (i == 0) {
            receiptItem.setProductName(tranItem.getProductName());
        }
        return receiptItem;
    }

    @Override
    public String generateReceiptXml(KioskTransactionEntity txn, final String inputReceiptTemplateXml, StoreApiChannels channel) throws IOException {
        String receiptTemplateXml;
        if (StringUtils.isEmpty(inputReceiptTemplateXml)) {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/" + ticketXMLTemplateName);
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + ticketXMLTemplateName + ".xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", ticketXMLTemplateName, "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                receiptTemplateXml = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    receiptTemplateXml = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    receiptTemplateXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + ticketXMLTemplateName + ".xml"));
                }
            }
        } else {
            receiptTemplateXml = inputReceiptTemplateXml;
        }


        final ReceiptMain receipt = new ReceiptMain();
        receipt.setPrintedDateText(NvxDateUtils.formatDate(txn.getAxCartCheckoutDateTime(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        receipt.setTransId(getStringValue(txn.getReceiptNumber()));
        receipt.setReceiptNumber(getStringValue(txn.getAxReceiptNumber() + ""));
        receipt.setReceiptSeqNumber(getStringValue(txn.getAxReceiptId() + ""));
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(txn.getTotalAmount(), "$ "));
        receipt.setLocation(getStringValue(txn.getKiosk().getLocation()));
        receipt.setKioskId(getStringValue(txn.getKiosk().getJcrNodeName()));
        receipt.setGst(NvxNumberUtils.formatToCurrency(Optional.ofNullable(txn.getGstAmount()).orElse(new BigDecimal(0)), "$ "));

        KioskInfoEntity kioskInfo = this.kioskService.getKioskInfo(channel);
        Node receiptTemplate = kioskfn.getReceiptTemplateParams(kioskInfo.getChannel());

        try {
            receipt.setLogo(receiptTemplate.getProperty(KioskReceiptTemplate.Logo.getPropertyName()).getString());
            receipt.setCompanyName(receiptTemplate.getProperty(KioskReceiptTemplate.CompanyName.getPropertyName()).getString());
            receipt.setAddress(receiptTemplate.getProperty(KioskReceiptTemplate.Address.getPropertyName()).getString());
            receipt.setPostalCode(receiptTemplate.getProperty(KioskReceiptTemplate.PostalCode.getPropertyName()).getString());
            receipt.setContactDetails(receiptTemplate.getProperty(KioskReceiptTemplate.ContactDetails.getPropertyName()).getString());
            receipt.setGstDetails(receiptTemplate.getProperty(KioskReceiptTemplate.GSTDetails.getPropertyName()).getString());
        } catch (RepositoryException e) {
            log.error("No Details Available");
        }

        final List<ReceiptItem> items = new ArrayList<>();

        Map<String, List<KioskTransactionItemEntity>> itemGroup = new HashMap<>();

        itemGroup = txn.getItems().stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getProductName));

        Map<String, List<KioskTransactionItemEntity>> topupItemMap = new HashMap<>();
        List<KioskTransactionItemEntity> axProductItems;
        List<List<KioskTransactionItemEntity>> axProductItemGroup = new ArrayList<>();

        for (Map.Entry<String, List<KioskTransactionItemEntity>> entry : itemGroup.entrySet()) {
            axProductItems = new ArrayList<>();
            for (KioskTransactionItemEntity item : entry.getValue()) {
                if (StringUtils.isBlank(item.getParentProductId())) {
                    axProductItems.add(item);
                } else {
                    if (topupItemMap.get(item.getParentProductId()) == null) {
                        topupItemMap.put(item.getParentProductId(), new ArrayList<>());
                    }
                    topupItemMap.get(item.getParentProductId()).add(item);
                }
            }
            axProductItemGroup.add(axProductItems);
        }

        for (List<KioskTransactionItemEntity> entries : axProductItemGroup) {
            int i = 0;
            for (KioskTransactionItemEntity tranItem : entries) {

                ReceiptItem existingReceiptItem = items.stream()
                        .filter(item -> item.getAxProductName().equals(tranItem.getAxProductName()) && StringUtils.isNotBlank(tranItem.getProductName())).findAny().orElse(null);

                if (null != existingReceiptItem) {
                    String existingQty = existingReceiptItem.getQty();
                    int totalQty = Integer.parseInt(existingQty) + Integer.parseInt(tranItem.getQuantity());

                    String existingTotal = existingReceiptItem.getSubtotalText().replaceAll("[^\\d.]+", "");
                    BigDecimal totalSubtotal = new BigDecimal(existingTotal).add(Optional.ofNullable(tranItem.getSubTotal()).orElse(BigDecimal.ZERO));

                    String existingAmount = existingReceiptItem.getAmount().replaceAll("[^\\d.]+", "");
                    BigDecimal totaAmount = new BigDecimal(existingAmount).add(Optional.ofNullable(tranItem.getAmount()).orElse(BigDecimal.ZERO));

                    existingReceiptItem.setQty(totalQty + "");
                    existingReceiptItem.setAmount(NvxNumberUtils.formatToCurrency(totaAmount, "$ "));
                    existingReceiptItem.setSubtotalText(NvxNumberUtils.formatToCurrency(totalSubtotal, "$ "));
                } else {
                    items.add(this.createReceiptItem(tranItem, i));
                    i++;
                    List<KioskTransactionItemEntity> tops = topupItemMap.get(tranItem.getListingId());
                    if (tops != null) {
                        for (KioskTransactionItemEntity topup : tops) {
                            items.add(this.createReceiptItem(topup, i));
                            i++;
                        }
                    }
                }
            }
        }

        receipt.setItems(items);

        final List<KioskPaymentItem> kioskPaymentItem = new ArrayList<>();
        for (KioskTransactionPaymentEntity tranItem : txn.getPayments()) {
            final KioskPaymentItem item = new KioskPaymentItem();
            item.setTerminalId(getStringValue(tranItem.getTerminalId()));
            item.setMerchantId(getStringValue(tranItem.getMerchantId()));

            String paymentMode = "";
            // get a payment mode and Entry mode
            if (tranItem != null & (tranItem.getEntryMode() != null && !tranItem.getEntryMode().equals(""))) {
                String entryMode = KioskCardEntryMode.toEnum(tranItem.getEntryMode()).getDescription();
                item.setEntryMode(getStringValue(entryMode));
            }
            if (tranItem != null & (tranItem.getPaymentMode() != null && !tranItem.getPaymentMode().equals(""))) {
                paymentMode = KioskPaymentCardType.toEnum(tranItem.getPaymentMode()).getDescription();
                item.setPaymentMode(getStringValue(paymentMode));
            }

            // set the NETS Trace(BatchNumber) and NETS Receipt(InvoiceNumber)
            // details for NETS PIN
            // set the Original Balance(InvoiceNumber) and Trans Amount
            // (ApprovalCode) and Empty line (RetrivalReferenceData) and Remain
            // Balance(Merchant) details for NETS FLASH
            if (PAYMENT_MODE_NETS_PIN_DEBIT != null && PAYMENT_MODE_NETS_PIN_DEBIT.equals(paymentMode)) {
                item.setApprovalCode(tranItem.getApprovalCode());
                item.setBatchNumber(getStringValue(getStringValue(tranItem.getNetsTraceNumber())));
                item.setInvoiceNumber(JsonUtil.fromJson(tranItem.getPaymentData(), NetsSale.class).getDateTime());
                item.setMerchantName(txn.getKiosk().getTerminalMerchantNameAndLocation());
                item.setCardNumber(JsonUtil.fromJson(tranItem.getPaymentData(), NetsSale.class).getNetsIssuerName());
            } else if (PAYMENT_MODE_NETS_FLASHPAY != null && PAYMENT_MODE_NETS_FLASHPAY.equals(paymentMode)) {
                item.setBatchNumber(tranItem.getBatchNumber());
                item.setInvoiceNumber(tranItem.getOriginalBalance() == null ? NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO)
                        : NumberFormat.getCurrencyInstance().format(tranItem.getOriginalBalance()));
                item.setApprovalCode(getStringValue("-     " + NumberFormat.getCurrencyInstance().format(tranItem.getAmount()) + ""));
                item.setRetrivalReferenceData(getStringValue("==========="));
                item.setMerchantName(getStringValue(tranItem.getRemainingBalance() + ""));
                item.setMerchantName(tranItem.getRemainingBalance() == null ? NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO)
                        : NumberFormat.getCurrencyInstance().format(tranItem.getRemainingBalance()));
                item.setCardNumber(tranItem.getCardNumber());
                item.setEntryMode(JsonUtil.fromJson(tranItem.getPaymentData(), NetsContactlessSale.class).getDateTime());
            } else {
                item.setBatchNumber(getStringValue(tranItem.getBatchNumber()));
                item.setInvoiceNumber(getStringValue(tranItem.getInvoiceNumber()));
                item.setApprovalCode(getStringValue(tranItem.getApprovalCode()));
                item.setRetrivalReferenceData(getStringValue(tranItem.getRetrivalReferenceData()));
                item.setAppLabel(getStringValue(tranItem.getAppLabel()));
                item.setMerchantName(getStringValue(txn.getKiosk().getTerminalMerchantNameAndLocation()));
                item.setCardNumber(tranItem.getCardNumber());
            }

            item.setTerminalVerificationResponse(tranItem.getTerminalVerificationResponse());
            item.setApplicationIdentifier(tranItem.getApplicationIdentifier());
            item.setTransactionCertificate(tranItem.getTransactionCertificate());
            item.setCardLabel(tranItem.getCardLabel());
           
            // show signature or write signature
            if (tranItem.getSignatureRequired()) {
                item.setSignature(resizeBase64Image(tranItem.getSignature()));
                item.setCardHolderName(tranItem.getCardHolderName());
                item.setNoSignature("Sign x______________________________");
            } else {
                item.setSignature(DUMMY_SIGNATURE);
                item.setCardHolderName("");
                item.setNoSignature(NO_SIGNATURE);
            }

            kioskPaymentItem.add(item);
        }

        receipt.setKioskPaymentItems(kioskPaymentItem);

        TicketIssuanceStatus ticketIssuanceStatus = new TicketIssuanceStatus();

        if (KioskTransactionStatus.TICKET_NOT_ISSUED.getCode().equals(txn.getStatus())
                || KioskTransactionStatus.TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode().equals(txn.getStatus())) {
            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_NOT_ISSUED);
            ticketIssuanceStatus.setTicketIssuranceMsg1(TICKET_ISSURANCE_NOT_ISSUED_MSG_1);
            ticketIssuanceStatus.setTicketIssuranceMsg2(TICKET_ISSURANCE_NOT_ISSUED_MSG_2);
            ticketIssuanceStatus.setTicketIssuranceMsg3(TICKET_ISSURANCE_NOT_ISSUED_MSG_3);
            ticketIssuanceStatus.setTicketIssuranceMsg4(TICKET_ISSURANCE_NOT_ISSUED_MSG_4);
        } else {
            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_COMPLETED);
            ticketIssuanceStatus.setTicketIssuranceMsg2(TICKET_ISSURANCE_COMPLETED_MSG_1);
            ticketIssuanceStatus.setTicketIssuranceMsg3(TICKET_ISSURANCE_COMPLETED_MSG_2);
        }

        receipt.setTicketIssuanceStatus(ticketIssuanceStatus);

        final Handlebars hb = new Handlebars();
        final Template hbTemplate = hb.compileInline(receiptTemplateXml);
        final Context hbContext = Context.newContext(receipt);
        final String generatedContent = hbTemplate.apply(hbContext);

        return generatedContent.replace("\n", "");
    }

    public String resizeBase64Image(String base64image) {
        BufferedImage bufImg = null;
        byte[] imageInByte = null;
        try {

            byte[] imgBytes = Base64.decodeBase64(base64image);

            bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
            bufImg = resizeImage(bufImg, bufImg.getType());

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufImg, "jpg", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();

        } catch (IOException s) {

        }
        return Base64.encodeBase64String(imageInByte);
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(originalImage.getWidth() / 5, originalImage.getHeight() / 5, type);
        Graphics2D g = resizedImage.createGraphics();

        g.drawImage(originalImage, 0, 0, originalImage.getWidth() / 5, originalImage.getHeight() / 5, null);
        g.dispose();
        return resizedImage;
    }

    private String replace(String xml, KioskAxTicketToken token, String value) {
        return xml.replace(token.getReplacement(), StringUtils.isBlank(value) ? "-" : value);
    }

    @Override
    public Map<String, String> generatePaperTicketXml(ETicketDataCompiled compiledTicketData, final String inputTemplateXmlQr, final String inputTemplateXmlBarcode)
            throws IOException {

        final Map<String, String> xmlMap = new HashMap<>();

        for (ETicketData td : compiledTicketData.getTickets()) {
            String xml = kioskService.getTikectTemplateXMLByName(td.getTicketTemplateName());
            log.info("before : " + xml);
            if (StringUtils.isBlank(xml)) {
                log.error("NO template found for ticket from AX with tempalte name " + td.getTicketTemplateName() + "for ticket " + td.getTicketNumber());
            } else {

                String tokenData = td.getTokenData();

                log.info("token data : " + tokenData);

                KioskTicketTokenList tokenList = JsonUtil.fromJson(tokenData, KioskTicketTokenList.class);

                xml = replace(xml, KioskAxTicketToken.BarcodeEndDate, tokenList.getBarcodeEndDate());
                xml = replace(xml, KioskAxTicketToken.BarcodePLU, tokenList.getBarcodePLU());
                xml = replace(xml, KioskAxTicketToken.BarcodeStartDate, tokenList.getBarcodeStartDate());
                xml = replace(xml, KioskAxTicketToken.CustomerName, tokenList.getCustomerName());
                xml = replace(xml, KioskAxTicketToken.Description, tokenList.getDescription());
                xml = replace(xml, KioskAxTicketToken.DiscountApplied, tokenList.getDiscountApplied());
                xml = replace(xml, KioskAxTicketToken.DisplayName, tokenList.getDisplayName());
                xml = replace(xml, KioskAxTicketToken.EventDate, tokenList.getEventDate());
                xml = replace(xml, KioskAxTicketToken.EventName, tokenList.getEventName());
                xml = replace(xml, KioskAxTicketToken.Facilities, tokenList.getFacilities());
                xml = replace(xml, KioskAxTicketToken.FacilityAction, tokenList.getFacilityAction());
                xml = replace(xml, KioskAxTicketToken.ItemNumber, tokenList.getItemNumber());
                xml = replace(xml, KioskAxTicketToken.LegalEntity, tokenList.getLegalEntity());
                xml = replace(xml, KioskAxTicketToken.MixMatchDescription, tokenList.getMixMatchDescription());
                xml = replace(xml, KioskAxTicketToken.MixMatchPackageName, tokenList.getMixMatchPackageName());
                xml = replace(xml, KioskAxTicketToken.OperationIds, tokenList.getOperationIds());
                xml = replace(xml, KioskAxTicketToken.PackageItem, tokenList.getPackageItem());
                xml = replace(xml, KioskAxTicketToken.PackageItemName, tokenList.getPackageItemName());
                xml = replace(xml, KioskAxTicketToken.PackagePrintLabel, tokenList.getPackagePrintLabel());
                xml = replace(xml, KioskAxTicketToken.PaxPerTicket, tokenList.getPaxPerTicket());
                xml = replace(xml, KioskAxTicketToken.PriceType, tokenList.getPriceType());
                xml = replace(xml, KioskAxTicketToken.PrintLabel, tokenList.getPrintLabel());
                xml = replace(xml, KioskAxTicketToken.ProductImage, tokenList.getProductImage());
                xml = replace(xml, KioskAxTicketToken.ProductOwner, tokenList.getProductOwner());
                xml = replace(xml, KioskAxTicketToken.ProductSearchName, tokenList.getProductSearchName());
                xml = replace(xml, KioskAxTicketToken.ProductVarColor, tokenList.getProductVarColor());
                xml = replace(xml, KioskAxTicketToken.ProductVarConfig, tokenList.getProductVarConfig());
                xml = replace(xml, KioskAxTicketToken.ProductVarSize, tokenList.getProductVarSize());
                xml = replace(xml, KioskAxTicketToken.ProductVarStyle, tokenList.getProductVarStyle());
                xml = replace(xml, KioskAxTicketToken.PublishPrice, tokenList.getPublishPrice());
                xml = replace(xml, KioskAxTicketToken.TicketCode, tokenList.getTicketCode());
                xml = replace(xml, KioskAxTicketToken.TicketLineDesc, tokenList.getTicketLineDesc());
                xml = replace(xml, KioskAxTicketToken.TicketNumber, tokenList.getTicketNumber());
                xml = replace(xml, KioskAxTicketToken.TicketType, tokenList.getTicketType());
                xml = replace(xml, KioskAxTicketToken.TicketValidity, tokenList.getTicketValidity());
                xml = replace(xml, KioskAxTicketToken.UsageValidity, tokenList.getUsageValidity());
                xml = replace(xml, KioskAxTicketToken.ValidityEndDate, tokenList.getValidityEndDate());
                xml = replace(xml, KioskAxTicketToken.ValidityStartDate, tokenList.getValidityStartDate());
                xml = replace(xml, KioskAxTicketToken.TermAndCondition, tokenList.getTermAndCondition());
                xml = replace(xml, KioskAxTicketToken.Disclaimer, tokenList.getDisclaimer());

                if (td.isThirdParty()) {
                    xml = xml.replace("{{TicketDataQRCode}}", td.getCodeData());
                } else {
                    xml = xml.replace("{{TicketDataQRCode}}", td.getBarcodeBase64());
                }
                xmlMap.put(td.getTicketNumber(), xml.replace("\n", "").replace("&", "&amp;"));
                log.info("after : " + xml);
            }
        }

        return xmlMap;
    }

    private String getStringValue(String string) {

        if (string == null || string.equals("null")) {
            return "";
        }
        return string;
    }

    @Override
    public String generateReceiptXmlWithoutQRCode(KioskTransactionEntity txn, String inputReceiptTemplateXml, StoreApiChannels channel) throws IOException {
        return generateRedemptionXML(txn, inputReceiptTemplateXml, "kiosk-redemption-unsuccessful-receipt-template", channel);
    }

    private String generateRedemptionXML(KioskTransactionEntity txn, String inputTemplateXml, String templateName, StoreApiChannels channel) throws IOException {

        String receiptTemplateXml;

        if (StringUtils.isEmpty(inputTemplateXml)) {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/" + templateName);
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + templateName + ".xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", templateName, "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                receiptTemplateXml = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    receiptTemplateXml = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    receiptTemplateXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + templateName + ".xml"));
                }
            }
        } else {
            receiptTemplateXml = inputTemplateXml;
        }

        final Date now = new Date();
        final ReceiptMain receipt = new ReceiptMain();
        receipt.setPrintedDateText(NvxDateUtils.formatDate(now, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        receipt.setTransId(txn.getReceiptNumber());
        receipt.setReceiptNumber(getStringValue(txn.getAxReceiptNumber() + ""));
        receipt.setReceiptSeqNumber(getStringValue(txn.getAxReceiptId() + ""));
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(txn.getTotalAmount(), "$ "));
        receipt.setLocation(getStringValue(txn.getKiosk().getLocation()));
        receipt.setKioskId(getStringValue(txn.getKiosk().getJcrNodeName()));
        receipt.setGst(NvxNumberUtils.formatToCurrency(Optional.ofNullable(txn.getGstAmount()).orElse(new BigDecimal(0)), "$ "));

        KioskInfoEntity kioskInfo = this.kioskService.getKioskInfo(channel);
        Node receiptTemplate = kioskfn.getReceiptTemplateParams(kioskInfo.getChannel());

        try {
            receipt.setLogo(receiptTemplate.getProperty(KioskReceiptTemplate.Logo.getPropertyName()).getString());
            receipt.setCompanyName(receiptTemplate.getProperty(KioskReceiptTemplate.CompanyName.getPropertyName()).getString());
            receipt.setAddress(receiptTemplate.getProperty(KioskReceiptTemplate.Address.getPropertyName()).getString());
            receipt.setPostalCode(receiptTemplate.getProperty(KioskReceiptTemplate.PostalCode.getPropertyName()).getString());
            receipt.setContactDetails(receiptTemplate.getProperty(KioskReceiptTemplate.ContactDetails.getPropertyName()).getString());
            receipt.setGstDetails(receiptTemplate.getProperty(KioskReceiptTemplate.GSTDetails.getPropertyName()).getString());
        } catch (RepositoryException e) {
            log.error("No Details Available");
        }

        final List<ReceiptItem> items = new ArrayList<>();
        Map<String, List<KioskTransactionItemEntity>> group = txn.getItems().stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getItemId));

        if (txn.getType().equalsIgnoreCase(KioskTransactionType.RECOVERY.getCode())) {
            for (Entry<String, List<KioskTransactionItemEntity>> itemGroup : group.entrySet()) {
                final ReceiptItem receiptItem = new ReceiptItem();
                int qty = 0;
                for (KioskTransactionItemEntity item : itemGroup.getValue()) {
                    qty += Integer.parseInt(item.getQuantity());
                    receiptItem.setAxProductName(item.getAxProductName());
                }
                receiptItem.setQty(qty + "");
                items.add(receiptItem);
            }

        } else {
            for (Entry<String, List<KioskTransactionItemEntity>> itemGroup : group.entrySet()) {
                final RedemptionReceiptItem redemptionReceiptItem = new RedemptionReceiptItem();
                String axProductName = null;
                Integer totalRedeemQty = 0;
                Integer totalRedeemRemaining = 0;

                for (KioskTransactionItemEntity transItem : itemGroup.getValue()) {
                    axProductName = getStringValue(transItem.getAxProductName());
                    totalRedeemQty = totalRedeemQty + transItem.getRedeemedQty();
                    totalRedeemRemaining = totalRedeemRemaining + transItem.getRemainingQty();
                }

                redemptionReceiptItem.setRedeemedQty(totalRedeemQty.toString());
                redemptionReceiptItem.setRemainingQty(totalRedeemRemaining.toString());
                redemptionReceiptItem.setAxProductName(axProductName);

                items.add(redemptionReceiptItem);
            }
        }

        receipt.setItems(items);

        TicketIssuanceStatus ticketIssuanceStatus = new TicketIssuanceStatus();

        if (KioskTransactionStatus.TICKET_NOT_ISSUED.getCode().equals(txn.getStatus())
                || KioskTransactionStatus.TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode().equals(txn.getStatus())) {

            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_NOT_ISSUED);

            if (txn.getType().equalsIgnoreCase(KioskTransactionType.RECOVERY.getCode())) {
                ticketIssuanceStatus.setTicketIssuranceMsg1(RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_1);
                ticketIssuanceStatus.setTicketIssuranceMsg2(RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_2);
                ticketIssuanceStatus.setTicketIssuranceMsg3(RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_3);
                ticketIssuanceStatus.setTicketIssuranceMsg4(RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_4);
            } else {
                ticketIssuanceStatus.setTicketIssuranceMsg1(REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_1);
                ticketIssuanceStatus.setTicketIssuranceMsg2(REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_2);
                ticketIssuanceStatus.setTicketIssuranceMsg3(REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_3);
                ticketIssuanceStatus.setTicketIssuranceMsg4(REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_4);
            }

        } else {
            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_COMPLETED);
            ticketIssuanceStatus.setTicketIssuranceMsg2(TICKET_ISSURANCE_COMPLETED_MSG_1);
            ticketIssuanceStatus.setTicketIssuranceMsg3(TICKET_ISSURANCE_COMPLETED_MSG_2);
        }

        receipt.setTicketIssuanceStatus(ticketIssuanceStatus);

        final Handlebars hb = new Handlebars();
        final Template hbTemplate = hb.compileInline(receiptTemplateXml);
        final Context hbContext = Context.newContext(receipt);
        final String generatedContent = hbTemplate.apply(hbContext);

        return generatedContent.replace("\n", "");
    }

    @Override
    public String generateRedemptionReceiptXML(KioskTransactionEntity txn, String inputTemplateXml, StoreApiChannels channel) throws IOException {
        return generateRedemptionXML(txn, inputTemplateXml, "kiosk-redemption-receipt-template", channel);
    }

    @Override
    public String generateRecoveryReceiptXML(KioskTransactionEntity txn, String inputTemplateXml, StoreApiChannels channel) throws IOException {
        return generateRedemptionXML(txn, inputTemplateXml, "kiosk-recovery-receipt-template", channel);
    }

    @Override
    public String generateRecoveryReceiptWithoutQRCode(KioskTransactionEntity txn, String inputTemplateXml, StoreApiChannels channel) throws IOException {
        return generateRedemptionXML(txn, inputTemplateXml, "kiosk-recovery-unsuccessful-receipt-template", channel);
    }
}
