package com.enovax.star.cms.kiosk.api.store.redemption.param;

public class PINRedemptionParam {

	private String pinCode;

	public PINRedemptionParam() {
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

}
