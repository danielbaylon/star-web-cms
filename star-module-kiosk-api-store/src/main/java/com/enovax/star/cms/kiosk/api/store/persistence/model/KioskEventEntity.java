package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by tharaka on 16/9/16.
 */

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_event", schema = "dbo")
public class KioskEventEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "event_type")
    private String eventType;

    @Column(name = "value")
    private String value;

    @Column(name = "description")
    private String description;

    @Column(name = "device_no")
    private String deviceNo;

    @Column(name = "kiosk_name")
    private String kioskName;

    @Column(name = "createts")
    private Date createts;

    @Column(name = "logonuser")
    private String logonuser;

    @Column(name = "api_channel")
    private String apiChannel;

    public KioskEventEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getKioskName() {
        return kioskName;
    }

    public void setKioskName(String kioskName) {
        this.kioskName = kioskName;
    }

    public Date getCreatets() {
        return createts;
    }

    public void setCreatets(Date createts) {
        this.createts = createts;
    }

    public String getLogonuser() {
        return logonuser;
    }

    public void setLogonuser(String logonuser) {
        this.logonuser = logonuser;
    }

    public String getApiChannel() {
        return apiChannel;
    }

    public void setApiChannel(String apiChannel) {
        this.apiChannel = apiChannel;
    }
}
