package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskEventDao;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
@PropertySource("classpath:zabbix-event/kiosk-api-store-application-zabbix.properties")
public class DefaultKioskZabbixAdapterService implements IKioskZabbixAdapterService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskZabbixAdapterService.class);

    private HttpHeaders headers;

    @Value("${kiosk.api.zabbix.uri}")
    private String ZABBIX_API;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IKioskEventDao kioskEventDao;
    
    @Autowired
    private Environment env;

    @PostConstruct
    private void createThreadPool() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Override
    public void sendRequest(KioskZabbixEventEntity entity, String channel) {
        log.info("send Zabbix Request");
  
        entity.setRequestDateTime(new Timestamp(new Date().getTime()));
        
        Runnable runnable = () -> {
            log.info("start Zabbix Request");
            final HttpEntity<String> request = new HttpEntity<>(new Gson().toJson(entity), headers);
            String response = restTemplate.postForObject(ZABBIX_API, request, String.class);
            entity.setRequest(request.getBody());
            entity.setResponse(response);
            entity.setResponseDateTime(new Timestamp(new Date().getTime()));

            this.saveEvent(entity, channel);
            
            log.info(response);
            log.info("end Zabbix Request");
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
    
    private void saveEvent(KioskZabbixEventEntity entity, String channel){
        KioskEventEntity event = new KioskEventEntity();
        if(this.getJsonObject(entity.getResponse()).get("success").getAsBoolean()){
            event.setStatus("Active");
        } else {
            event.setStatus("Deactivate");
        }
        event.setEventType("ZABBIX");
        event.setEventName(this.getJsonObject(entity.getRequest()).get("event").getAsString());
        event.setValue(this.getJsonObject(entity.getRequest()).get("eventId").getAsString());
        event.setDescription(this.getJsonObject(entity.getRequest()).get("message").getAsString());
        event.setCreatets(entity.getRequestDateTime());
        event.setApiChannel(channel);
        
        kioskEventDao.save(event);
        
    }
    
    private JsonObject getJsonObject(String jsonString) {
        JsonParser parser = new JsonParser();
        return parser.parse(jsonString).getAsJsonObject();
    }
    
    @Override
    public KioskZabbixEventEntity constructEvent(String eventItem, KioskInfoEntity kiosk, boolean isFullParam, String...messageParams) {
        KioskZabbixEventEntity eventEntity = new KioskZabbixEventEntity();

        String[] eventItems = this.env.getProperty(eventItem).split("|");

        if (isFullParam) {
            eventEntity.setHostName(kiosk.getJcrNodeName());
        }
        
        if (isFullParam && !ArrayUtils.isEmpty(messageParams)) {
            MessageFormat messageFormat = new MessageFormat(eventItems[3]);
            eventEntity.setMessage(messageFormat.format(messageParams));
        } else {
            eventEntity.setMessage(eventItems[3]);
        }

        eventEntity.setEventId(eventItems[0]);
        eventEntity.setCategory(eventItems[1]);
        eventEntity.setEvent(eventItems[2]);

        return eventEntity;
    }
 

}
