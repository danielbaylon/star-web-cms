package com.enovax.star.cms.kiosk.api.store.service.booking;

import javax.servlet.http.HttpSession;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutDisplay;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.KioskFunCart;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;

public interface IKioskBookingService {

    ApiResult<FunCartDisplay> cartAdd(StoreApiChannels channel, FunCart cartToAdd, String sessionId);

    ApiResult<FunCartDisplay> cartAddTopup(StoreApiChannels channel, String sessionId, KioskFunCart topupCart);

    ApiResult<FunCartDisplay> updateCartTopup(StoreApiChannels channel, KioskFunCart cartToUpdate, String sessionId);
    
    ApiResult<FunCartDisplay> updateShoppingCart(StoreApiChannels channel, KioskFunCart cartToUpdate, String sessionId);

    ApiResult<String> cartApplyPromoCode(StoreApiChannels channel, String sessionId, String promoCode);

    ApiResult<FunCartDisplay> cartRemoveItem(StoreApiChannels channel, String sessionId, String cartItemId);

    boolean cartClear(StoreApiChannels channel, String sessionId);

    FunCartDisplay constructCartDisplay(StoreApiChannels channel, String sessionId);

    FunCartDisplay constructCartDisplay(StoreApiChannels channel, KioskFunCart cart);

    CheckoutDisplay constructCheckoutDisplayFromCart(StoreApiChannels channel, HttpSession session);

    ApiResult<KioskTransactionEntity> startCartCheckout(StoreApiChannels channel, HttpSession session, String signature);

    ApiResult<KioskTransactionEntity> doCheckout(StoreApiChannels channel, HttpSession session, CustomerDetails deets);

    // ApiResult<KioskTransactionEntity> doCheckout(StoreApiChannels channel,
    // KioskFunCart cart, CustomerDetails deets);

    // ApiResult<String> cancelCheckout(StoreApiChannels channel, HttpSession
    // session);

    KioskTransactionEntity getTransactionByReceipt(StoreApiChannels channel, String receiptNumber);

    KioskTransactionEntity getTransactionBySession(StoreApiChannels channel, HttpSession session);

    ReceiptDisplay getReceiptForDisplayBySession(StoreApiChannels channel, HttpSession session);

    ApiResult<KioskTransactionEntity> completeSale(StoreApiChannels channel, HttpSession session);

    ReceiptDisplay getReceiptForDisplayByReceipt(StoreApiChannels channel, String receiptNumber);

    ReceiptDisplay getReceiptForDisplay(StoreTransaction txn);

    void saveTransaction(StoreApiChannels channel, KioskTransactionEntity tran);

    KioskTransactionEntity updateTransactionTicketPrintingStatus(String receiptNumber, Boolean ticketPrinted);

    Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, KioskInfoEntity kiosk);
    
}
