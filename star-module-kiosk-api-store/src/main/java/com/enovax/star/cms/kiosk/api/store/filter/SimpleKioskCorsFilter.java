package com.enovax.star.cms.kiosk.api.store.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SimpleKioskCorsFilter extends OncePerRequestFilter {

    private static void setCorsHeaders(final HttpServletRequest request, final HttpServletResponse response) {
        String origin = request.getHeader("Origin");
        if (StringUtils.isBlank(origin)|| !"*".equals(origin)) {
            response.addHeader("Access-Control-Allow-Origin", "*");
        } 

        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X_Developer, Store-Api-Channel");
        response.addHeader("Access-Control-Max-Age", "1728000");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    	// need this for static launcher in production
        
        if ("OPTIONS".equals(request.getMethod())) {
        	setCorsHeaders(request, response);
            return;
        }

        filterChain.doFilter(request, response);
    }
}
