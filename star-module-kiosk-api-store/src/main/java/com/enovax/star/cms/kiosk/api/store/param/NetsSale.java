package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;

/**
 * @author danielbaylon
 *
 */
public class NetsSale implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO: Check for the following fields: result transaction merchantId
     * terminalId commandId
     */
    private String result;

    private String transaction;

    private String merchantId;

    private String terminalId;

    private String commandId;

    private String responseCode;

    private Long transactionAmount;

    private String retrievalReferenceNumber;

    private String approvalCode;

    private String hostLabel;

    private String cardLabel;

    private String cardTypeValue;

    private String cardTypeDescription;

    private String hostTypeValue;

    private String hostTypeDescription;

    private String netsReferenceNumber;

    private String netsSystemTrace;

    private String bankIdentificationNumber;

    private String netsAccountTypeValue;

    private String netsAccountTypeDescription;

    private String netsIssuerName;

    private String netsReceiptNumber;

    private String netsPurchaseFee;

    private String entryModeValue;

    private String dateTime;

    private String rawData;

    public NetsSale() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(String hostLabel) {
        this.hostLabel = hostLabel;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardTypeValue() {
        return cardTypeValue;
    }

    public void setCardTypeValue(String cardTypeValue) {
        this.cardTypeValue = cardTypeValue;
    }

    public String getCardTypeDescription() {
        return cardTypeDescription;
    }

    public void setCardTypeDescription(String cardTypeDescription) {
        this.cardTypeDescription = cardTypeDescription;
    }

    public String getHostTypeValue() {
        return hostTypeValue;
    }

    public void setHostTypeValue(String hostTypeValue) {
        this.hostTypeValue = hostTypeValue;
    }

    public String getHostTypeDescription() {
        return hostTypeDescription;
    }

    public void setHostTypeDescription(String hostTypeDescription) {
        this.hostTypeDescription = hostTypeDescription;
    }

    public String getNetsReferenceNumber() {
        return netsReferenceNumber;
    }

    public void setNetsReferenceNumber(String netsReferenceNumber) {
        this.netsReferenceNumber = netsReferenceNumber;
    }

    public String getNetsSystemTrace() {
        return netsSystemTrace;
    }

    public void setNetsSystemTrace(String netsSystemTrace) {
        this.netsSystemTrace = netsSystemTrace;
    }

    public String getBankIdentificationNumber() {
        return bankIdentificationNumber;
    }

    public void setBankIdentificationNumber(String bankIdentificationNumber) {
        this.bankIdentificationNumber = bankIdentificationNumber;
    }

    public String getNetsAccountTypeValue() {
        return netsAccountTypeValue;
    }

    public void setNetsAccountTypeValue(String netsAccountTypeValue) {
        this.netsAccountTypeValue = netsAccountTypeValue;
    }

    public String getNetsAccountTypeDescription() {
        return netsAccountTypeDescription;
    }

    public void setNetsAccountTypeDescription(String netsAccountTypeDescription) {
        this.netsAccountTypeDescription = netsAccountTypeDescription;
    }

    public String getNetsIssuerName() {
        return netsIssuerName;
    }

    public void setNetsIssuerName(String netsIssuerName) {
        this.netsIssuerName = netsIssuerName;
    }

    public String getNetsReceiptNumber() {
        return netsReceiptNumber;
    }

    public void setNetsReceiptNumber(String netsReceiptNumber) {
        this.netsReceiptNumber = netsReceiptNumber;
    }

    public String getNetsPurchaseFee() {
        return netsPurchaseFee;
    }

    public void setNetsPurchaseFee(String netsPurchaseFee) {
        this.netsPurchaseFee = netsPurchaseFee;
    }

    public String getEntryModeValue() {
        return entryModeValue;
    }

    public void setEntryModeValue(String entryModeValue) {
        this.entryModeValue = entryModeValue;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getRawData() {
        return rawData;
    }
}
