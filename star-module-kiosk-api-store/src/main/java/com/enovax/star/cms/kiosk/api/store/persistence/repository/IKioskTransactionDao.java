package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;

@Repository
public interface IKioskTransactionDao extends JpaRepository<KioskTransactionEntity, Long> {

    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.receiptNumber = :receiptNumber")
    public KioskTransactionEntity getTransactionByReceipt(@Param("receiptNumber") String receiptNumber);
    
    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.receiptNumber = :receiptNumber and t.axCartId = :axCartId")
    public KioskTransactionEntity getTransaction(@Param("receiptNumber") String receiptNumber, @Param("axCartId")String axCartId);

    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.axCartId = :cartId order by id desc")
    public List<KioskTransactionEntity> getTransactionByAxCartIdByOrderByIdDesc(@Param("cartId") String cartId);

    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k where k.channel = :channel  AND  t.axCartCheckoutDateTime >= :axCartCheckoutStartDateTime AND t.axCartCheckoutDateTime <= :axCartCheckoutEndDateTime AND t.redemptionPinCode = :redemptionPinCode")
    public ArrayList<KioskTransactionEntity> getTransactionsByPinCode(@Param("axCartCheckoutStartDateTime") Date axCartCheckoutStartDateTime , @Param("axCartCheckoutEndDateTime") Date axCartCheckoutEndDateTime, @Param("redemptionPinCode") String redemptionPinCode,@Param("channel") String channel);

    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k  where k.channel = :channel AND  t.axCartCheckoutDateTime >= :axCartCheckoutStartDateTime AND t.axCartCheckoutDateTime <= :axCartCheckoutEndDateTime")
    public ArrayList<KioskTransactionEntity> getTransactionByDate(@Param("axCartCheckoutStartDateTime") Date axCartCheckoutStartDateTime , @Param("axCartCheckoutEndDateTime") Date axCartCheckoutEndDateTime,@Param("channel") String channel);

    //@Query("SELECT t FROM KioskTransactionEntity t WHERE t.receiptNumber = :receiptNumber")
    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k where k.channel = :channel  AND  t.axCartCheckoutDateTime >= :axCartCheckoutStartDateTime AND t.axCartCheckoutDateTime <= :axCartCheckoutEndDateTime AND t.receiptNumber = :receiptNumber")
    public ArrayList<KioskTransactionEntity> getTransactionByTranID(@Param("axCartCheckoutStartDateTime") Date axCartCheckoutStartDateTime , @Param("axCartCheckoutEndDateTime") Date axCartCheckoutEndDateTime,@Param("receiptNumber") String receiptNumber, @Param("channel") String channel);

    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k where k.channel = :channel AND t.axCartId = :cartId AND t.receiptNumber = :receiptNumber AND t.axCartCheckoutDateTime >= :axCartCheckoutStartDateTime AND t.axCartCheckoutDateTime <= :axCartCheckoutEndDateTime")
    public ArrayList<KioskTransactionEntity> getTransactionByAll(@Param("receiptNumber") String receiptNumber, @Param("axCartCheckoutStartDateTime") Date axCartCheckoutStartDateTime , @Param("axCartCheckoutEndDateTime") Date axCartCheckoutEndDateTime,@Param("cartId") String cartId,@Param("channel") String channel );

    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k where k.channel = :channel AND t.axCartCheckoutDateTime >= :axCartCheckoutStartDateTime AND t.axCartCheckoutDateTime <= :axCartCheckoutEndDateTime")
    public ArrayList<KioskTransactionEntity> getReceiptDetailsByAll(@Param("axCartCheckoutStartDateTime") Date axCartCheckoutStartDateTime , @Param("axCartCheckoutEndDateTime") Date axCartCheckoutEndDateTime,@Param("channel") String channel );

    @Query("SELECT t FROM KioskTransactionEntity t inner join t.kiosk k where k.channel = :channel AND t.receiptNumber = :receiptNumber")
    public ArrayList<KioskTransactionEntity> getReceiptDetailsByTranId(@Param("receiptNumber") String receiptNumber,@Param("channel") String channel);

    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.axReceiptNumber = :axReceiptNumber order by id desc")
    public ArrayList<KioskTransactionEntity> getTransactionByAxReceipt(@Param("axReceiptNumber") String axReceiptNumber);
    
    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.endDateTime >= :endDateTime order by id desc")
    public List<KioskTransactionEntity> getTransactionByEndDate(@Param("endDateTime") Date endDateTime);
    
    @Query("SELECT t FROM KioskTransactionEntity t WHERE t.axCartCheckoutDateTime BETWEEN :startDateTime AND :endDateTime")
    public List<KioskTransactionEntity> getTransactionsByStartDateTimeAndEndDateTime(@Param("startDateTime") Date startDateTime, @Param("endDateTime") Date endDateTime);

}
