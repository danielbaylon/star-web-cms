package com.enovax.star.cms.kiosk.api.store.datamodel;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "KIOSKTransaction", schema = "dbo")
public class KIOSKTransactionModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9103478435777708002L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "result")
	private boolean result;

	@Column(name = "receipt_number")
	private String receiptNumber;

	@Column(name = "transaction_date")
	private String transactionDate;

	@Column(name = "transaction_number")
	private String transactionNumber;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "pamyent_type")
	private String paymentType;

	@Column(name = "card_last_four_digits")
	private String cardLast4Digits;

	@Column(name = "bank_approval_code")
	private String bankApprovalCode;

	@Column(name = "traffic_source")
	private String trafficSource; // TODO: Validate the details regarding this field

	@Column(name = "browsing_printed_date_time")
	private String browsingPrintedDateTime; // TODO: Validate if not the same as the transcationDate.

	@Column(name = "kiosk_id")
	private String kioskId;

	@Column(name = "transaction_start")
	private String transactionStart;

	@Column(name = "transaction_end")
	private String transactionEnd;

	public KIOSKTransactionModel() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardLast4Digits() {
		return cardLast4Digits;
	}

	public void setCardLast4Digits(String cardLast4Digits) {
		this.cardLast4Digits = cardLast4Digits;
	}

	public String getBankApprovalCode() {
		return bankApprovalCode;
	}

	public void setBankApprovalCode(String bankApprovalCode) {
		this.bankApprovalCode = bankApprovalCode;
	}

	public String getTrafficSource() {
		return trafficSource;
	}

	public void setTrafficSource(String trafficSource) {
		this.trafficSource = trafficSource;
	}

	public String getBrowsingPrintedDateTime() {
		return browsingPrintedDateTime;
	}

	public void setBrowsingPrintedDateTime(String browsingPrintedDateTime) {
		this.browsingPrintedDateTime = browsingPrintedDateTime;
	}

	public String getKioskId() {
		return kioskId;
	}

	public void setKioskId(String kioskId) {
		this.kioskId = kioskId;
	}

	public String getTransactionStart() {
		return transactionStart;
	}

	public void setTransactionStart(String transactionStart) {
		this.transactionStart = transactionStart;
	}

	public String getTransactionEnd() {
		return transactionEnd;
	}

	public void setTransactionEnd(String transactionEnd) {
		this.transactionEnd = transactionEnd;
	}

}
