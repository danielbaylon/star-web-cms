package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_tran_payment", schema = "dbo")
public class KioskTransactionPaymentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oid")
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "payment_data")
    private String paymentData;

    @Column(name = "callback_data")
    private String callbackData;

    @Column(name = "approval_code")
    private String approvalCode;

    @Column(name = "card_type_description")
    private String cardTypeDescription;

    @Column(name = "terminal_id")
    private String terminalId;

    @Column(name = "merchant_id")
    private String merchantId;

    @Column(name = "transaction_start")
    private String transactionStart;

    @Column(name = "transaction_end")
    private String transactionEnd;

    @Column(name = "batch_no")
    private String batchNumber;

    @Column(name = "invoice_no")
    private String invoiceNumber;

    @Column(name = "rrn")
    private String retrivalReferenceData;

    @Column(name = "entry_mode")
    private String entryMode;

    @Column(name = "emv_tvr")
    private String terminalVerificationResponse;

    @Column(name = "emv_aid")
    private String applicationIdentifier;

    @Column(name = "emv_app_label")
    private String appLabel;

    @Column(name = "emv_tr")
    private String transactionCertificate;

    @Column(name = "card_type_value")
    private String paymentMode;

    @Column(name = "card_label")
    private String cardLabel;

    @Column(name = "card_no")
    private String cardNumber;

    @Column(name = "cardholder_name")
    private String cardHolderName;

    @Column(name = "host_label")
    private String hostLabel;

    @Column(name = "signature_required")
    private Boolean signatureRequired = Boolean.FALSE;

    @Column(name = "signature")
    private String signature;

    @Column(name = "original_bal")
    private BigDecimal originalBalance;

    @Column(name = "remaining_bal")
    private BigDecimal remainingBalance;

    @Column(name = "nets_trace_no")
    private String netsTraceNumber;

    @Column(name = "nets_receipt_no")
    private String netsReceiptNumber;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "result")
    private Boolean status;

    @Column(name = "raw_data")
    private String rawData;

    @Column(name = "payment_type")
    private String type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_tran_oid")
    private KioskTransactionEntity tran;

    public KioskTransactionPaymentEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(String paymentData) {
        this.paymentData = paymentData;
    }

    public String getCallbackData() {
        return callbackData;
    }

    public void setCallbackData(String callbackData) {
        this.callbackData = callbackData;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getCardTypeDescription() {
        return cardTypeDescription;
    }

    public void setCardTypeDescription(String cardTypeDescription) {
        this.cardTypeDescription = cardTypeDescription;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTransactionStart() {
        return transactionStart;
    }

    public void setTransactionStart(String transactionStart) {
        this.transactionStart = transactionStart;
    }

    public String getTransactionEnd() {
        return transactionEnd;
    }

    public void setTransactionEnd(String transactionEnd) {
        this.transactionEnd = transactionEnd;
    }

    public void setTran(KioskTransactionEntity tran) {
        this.tran = tran;
    }

    public KioskTransactionEntity getTran() {
        return tran;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getRetrivalReferenceData() {
        return retrivalReferenceData;
    }

    public void setRetrivalReferenceData(String retrivalReferenceData) {
        this.retrivalReferenceData = retrivalReferenceData;
    }

    public String getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getTerminalVerificationResponse() {
        return terminalVerificationResponse;
    }

    public void setTerminalVerificationResponse(String terminalVerificationResponse) {
        this.terminalVerificationResponse = terminalVerificationResponse;
    }

    public String getApplicationIdentifier() {
        return applicationIdentifier;
    }

    public void setApplicationIdentifier(String applicationIdentifier) {
        this.applicationIdentifier = applicationIdentifier;
    }

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    public String getTransactionCertificate() {
        return transactionCertificate;
    }

    public void setTransactionCertificate(String transactionCertificate) {
        this.transactionCertificate = transactionCertificate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Boolean getSignatureRequired() {
        return signatureRequired;
    }

    public void setSignatureRequired(Boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(String hostLabel) {
        this.hostLabel = hostLabel;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public BigDecimal getOriginalBalance() {
        return originalBalance;
    }

    public void setOriginalBalance(BigDecimal originalBalance) {
        this.originalBalance = originalBalance;
    }

    public BigDecimal getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(BigDecimal remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public String getNetsTraceNumber() {
        return netsTraceNumber;
    }

    public void setNetsTraceNumber(String netsTraceNumber) {
        this.netsTraceNumber = netsTraceNumber;
    }

    public String getNetsReceiptNumber() {
        return netsReceiptNumber;
    }

    public void setNetsReceiptNumber(String netsReceiptNumber) {
        this.netsReceiptNumber = netsReceiptNumber;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getRawData() {
        return rawData;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Double getAmountInDouble() {
        return this.amount.doubleValue();
    }

}
