package com.enovax.star.cms.kiosk.api.store.jcr.repository.product;

/**
 * Created by JC on 5/13/16.
 */
public interface IProductJcrRepository {
    /**
     * Get admissionIncluded property of CMS product by channel and node name.
     * 
     * @param channel
     * @param cmsNodeNameInJcr
     * @return
     */
    public String getAdmssionInfo(String channel, String cmsNodeNameInJcr);

    /**
     * Check if product below category that is transport layout.
     * 
     * 
     * @param channel
     * @param productNodeName
     * @return
     */
    public Boolean isProductCategoryTransportLayout(String channel, String productNodeName);

}
