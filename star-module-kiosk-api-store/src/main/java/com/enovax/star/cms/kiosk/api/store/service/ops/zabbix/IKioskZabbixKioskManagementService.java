package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskZabbixKioskManagementService {

    public void sendCloseShiftError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendTransDataSyncError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendNonTransDataSyncError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
}