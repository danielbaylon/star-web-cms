package com.enovax.star.cms.kiosk.api.store.controller.exception;

public class LoginControllerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3434320074433046213L;

	public LoginControllerException(String message) {
		super(message);
	}
	
	

}
