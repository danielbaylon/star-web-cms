package com.enovax.star.cms.kiosk.api.store.service.recovery;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.jcr.Node;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ppslm.TicketType;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axstar.AxStarCart;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.commons.model.booking.KioskFunCart;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartDeactivateTicketCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketReprintCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketSearchCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdatePrintStatusCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdateReprintSuccessCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintHistory;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintTokenEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrderSearch;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrdersSearchCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxTransactionSearchCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartDeactivateTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketReprintStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketSearchCriteriaRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketUpdateReprintSuccessRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSalesOrdersSearchRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchJournalTransactionsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdatePrintStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartDeactivateTicketResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketReprintStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketSearchResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdatePrintStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdateReprintSuccessResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSalesOrdersSearchResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchJournalTransactionsResponse;
import com.enovax.star.cms.commons.model.ticket.TicketData;
import com.enovax.star.cms.commons.service.axcrt.kiosk.AxStarKioskService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;
//import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTicketDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskPrintTicketResult;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketStatus;
import com.enovax.star.cms.kiosk.api.store.print.model.KioskTicketTokenList;
import com.enovax.star.cms.kiosk.api.store.reference.KioskAxTicketToken;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.repository.cart.IKioskCartManager;
import com.enovax.star.cms.kiosk.api.store.service.arm.IKioskRoboticArmService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixPrinterService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;

@Service
public class DefaultKioskRecoveryService implements IKioskRecoveryService {

    private Logger log = LoggerFactory.getLogger(DefaultKioskRecoveryService.class);

    @Autowired
    private AxStarKioskService axStarKioskService;

    @Autowired
    private IKioskTransactionDao transactionDao;

    @Autowired
    private IKioskCartManager cartManager;

    @Autowired
    private IKioskManagementService kioskManagementService;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;

    @Autowired
    private IKioskRoboticArmService armService;
    
    @Autowired
    private IKioskZabbixPrinterService zabbixPrinterService;

    private String getAxProductTicketType(StoreApiChannels channel, String itemId) {
        String type = TicketType.Standard.display;
        try {
            Node product = kioskfn.getAXProductByDisplayProductNumber(channel.code, itemId);
            if (product != null && product.hasProperty(AXProductProperties.TicketType.getPropertyName())) {
                type = product.getProperty(AXProductProperties.TicketType.getPropertyName()).getString();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return type;
    }

    private void getTransactionItem(StoreApiChannels channel, AxSalesOrderSearch axSalesOrderSearch, KioskTransactionEntity tran) {
        KioskTransactionItemEntity item = null;

        for (AxSalesLine salesLine : axSalesOrderSearch.getSalesLines()) {
            item = new KioskTransactionItemEntity();
            item.setTran(tran);
            tran.getItems().add(item);
            item.setAxProductName(this.getAxProductItemName(channel, salesLine.getItemId()));
            item.setQuantity(String.valueOf(new BigDecimal(salesLine.getQuantity()).intValue()));
            item.setItemId(salesLine.getItemId());
            item.setListingId(salesLine.getListingId());
            item.setSubTotal(new BigDecimal(salesLine.getTotalAmount()));
        }
    }

    private KioskTransactionEntity createTransaction(KioskInfoEntity kioskInfo, AxSalesOrderSearch axSalesOrderSearch, String axCartId) {
        KioskTransactionEntity tran = new KioskTransactionEntity();
        Date now = DateTime.now().toDate();
        tran.setAxCartCheckoutDateTime(now);
        tran.setAxCartId(axCartId);
        tran.setAxReceiptId(kioskInfo.getCurrentReceiptSequence());
        tran.setAxReceiptNumber(axSalesOrderSearch.getReceiptId());
        tran.setKiosk(kioskInfo);
        tran.setReceiptNumber(ProjectUtils.generateReceiptNumber(now));
        tran.setStatus(KioskTransactionStatus.RECOVERY_FLOW_STARTED.getCode());
        tran.setTotalAmount(new BigDecimal(axSalesOrderSearch.getTotalAmount()));
        BigDecimal gstAmount = tran.getTotalAmount().multiply(new BigDecimal(1.07)).divide(new BigDecimal(0.07), 2, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
        tran.setGstAmount(gstAmount);
        tran.setType(KioskTransactionType.RECOVERY.getCode());

        return tran;
    }

    private AxStarServiceResult<KioskCartTicketSearchResponse> getCartTicketByAxReceiptNumber(StoreApiChannels channel, String axReceiptNumber, String transactionId) {
        AxCartTicketSearchCriteria criteria = new AxCartTicketSearchCriteria();
        criteria.setReceiptNumber(axReceiptNumber);
        KioskCartTicketSearchCriteriaRequest request = new KioskCartTicketSearchCriteriaRequest();
        request.setAxCartTicketSearchCriteria(criteria);

        return axStarKioskService.apiKioskCartTicketSearch(channel, request);
    }

    private KioskFunCart getCurrentFunCart(StoreApiChannels channel, String sessionId) {
        KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (savedCart == null) {
            savedCart = new KioskFunCart();
            savedCart.setCartId(sessionId);
            savedCart.setChannelCode(channel.code);
        }
        return savedCart;
    }

    private KioskTicketEntity getTicket(AxCartRetailTicketTableEntity axTicket, KioskTransactionEntity tran) throws Exception {
        KioskTicketEntity ticket = new KioskTicketEntity();

        if (axTicket.getCreatedDateTime().equalsIgnoreCase("0001-01-01T00:00:00")) {
            ticket.setCreatedDateTime(NvxDateUtils.parseAxDate(axTicket.getTransDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        } else {
            ticket.setCreatedDateTime(NvxDateUtils.parseAxDate(axTicket.getCreatedDateTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        }

        if (axTicket.getModifiedDateTime().equalsIgnoreCase("0001-01-01T00:00:00")) {
            ticket.setCreatedDateTime(NvxDateUtils.parseAxDate(axTicket.getTransDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        } else {
            ticket.setModifiedDateTime(NvxDateUtils.parseAxDate(axTicket.getModifiedDateTime(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        }

        ticket.setEventDate(NvxDateUtils.parseAxDate(axTicket.getEventDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setEndDate(NvxDateUtils.parseAxDate(axTicket.getEndDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setEventGroupId(axTicket.getEventGroupId());
        ticket.setEventLineId(axTicket.getEventLineId());
        ticket.setItemId(axTicket.getItemId());
        ticket.setOpenValidityId(axTicket.getOpenValidityId());
        ticket.setAxProductName(axTicket.getProductName());
        ticket.setQrigTicketCode(axTicket.getOrigTicketCode());
        ticket.setQuantity(String.valueOf(new BigDecimal(axTicket.getQtyGroup()).intValue()));
        ticket.setReceiptId(axTicket.getReceiptId());
        ticket.setShiftId(axTicket.getShiftId());
        ticket.setStartDate(NvxDateUtils.parseAxDate(axTicket.getStartDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setTicketCode(axTicket.getTicketCode());
        ticket.setTicketTableId(axTicket.getTicketTableId());
        ticket.setTransactionId(axTicket.getTransactionId());
        ticket.setTransDate(NvxDateUtils.parseAxDate(axTicket.getTransDate(), NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        ticket.setUsageValidityId(axTicket.getUsageValidityId());
        ticket.setTicketNumber(axTicket.getTicketTableId());
        ticket.setTemplateName(Optional.ofNullable(axTicket.getTemplateName()).orElse("Paper"));
        ticket.setStatus("0");

        if (axTicket.isNeedsActivation()) {
            final TicketData rawData = new TicketData();
            rawData.setFacilities(new ArrayList<>());
            rawData.setQuantity(1);
            rawData.setStartDate(NvxDateUtils.formatDate(ticket.getStartDate(), "MM/dd/yyyy"));
            rawData.setEndDate(NvxDateUtils.formatDate(ticket.getEndDate(), "MM/dd/yyyy"));
            rawData.setTicketCode(axTicket.getTicketCode());
            ticket.setCodeData(JsonUtil.jsonify(rawData));
            ticket.setBarcodeBase64(QRCodeGenerator.compressAndEncode(JsonUtil.jsonify(rawData)));
        } else {
            ticket.setThirdParty(Boolean.TRUE);
            ticket.setCodeData(axTicket.getTicketCode());
            ticket.setBarcodeBase64(BarcodeGenerator.toBase64(axTicket.getTicketCode(), 300, 75));
        }

        ticket.setTran(tran);
        return ticket;
    }

    private String getAxProductItemName(StoreApiChannels channel, String itemId) {
        String productName = null;
        try {
            Node product = kioskfn.getAXProductByDisplayProductNumber(channel.code, itemId);
            if (product != null && product.hasProperty(AXProductProperties.ProductName.getPropertyName())) {
                productName = product.getProperty(AXProductProperties.ProductName.getPropertyName()).getString();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return productName;
    }

    private String getTokenData(AxCartRetailTicketTableEntity axTicket) {
        String tokenData = "";
        KioskTicketTokenList tokenList = new KioskTicketTokenList();
        for (AxPrintTokenEntity token : axTicket.getTokenList()) {
            String key = token.getTokenKey();
            String value = token.getValue();
            KioskAxTicketToken ticketToken = KioskAxTicketToken.toEnum(key);
            if (ticketToken != null) {
                switch (ticketToken) {
                case BarcodeEndDate:
                    tokenList.setBarcodeEndDate(value);
                    break;
                case BarcodePLU:
                    tokenList.setBarcodePLU(value);
                    break;
                case BarcodeStartDate:
                    tokenList.setBarcodeStartDate(value);
                    break;
                case CustomerName:
                    tokenList.setCustomerName(value);
                    break;
                case Description:
                    tokenList.setDescription(value);
                    break;
                case DiscountApplied:
                    tokenList.setDiscountApplied(value);
                    break;
                case DisplayName:
                    tokenList.setDisplayName(value);
                    break;
                case EventDate:
                    tokenList.setEventDate(value);
                    break;
                case EventName:
                    tokenList.setEventName(value);
                    break;
                case Facilities:
                    tokenList.setFacilities(value);
                    break;
                case FacilityAction:
                    tokenList.setFacilityAction(value);
                    break;
                case ItemNumber:
                    tokenList.setItemNumber(value);
                    break;
                case LegalEntity:
                    tokenList.setLegalEntity(value);
                    break;
                case MixMatchDescription:
                    tokenList.setMixMatchDescription(value);
                    break;
                case MixMatchPackageName:
                    tokenList.setMixMatchPackageName(value);
                    break;
                case OperationIds:
                    tokenList.setOperationIds(value);
                    break;
                case PackageItem:
                    tokenList.setPackageItem(value);
                    break;
                case PackageItemName:
                    tokenList.setPackageItemName(value);
                    break;
                case PaxPerTicket:
                    tokenList.setPaxPerTicket(value);
                    break;
                case PriceType:
                    tokenList.setPriceType(value);
                    break;
                case PrintLabel:
                    tokenList.setPrintLabel(value);
                    break;
                case ProductImage:
                    tokenList.setProductImage(value);
                    break;
                case ProductOwner:
                    tokenList.setProductOwner(value);
                    break;
                case ProductSearchName:
                    tokenList.setProductSearchName(value);
                    break;
                case ProductVarColor:
                    tokenList.setProductVarColor(value);
                    break;
                case ProductVarConfig:
                    tokenList.setProductVarConfig(value);
                    break;
                case ProductVarSize:
                    tokenList.setProductVarSize(value);
                    break;
                case ProductVarStyle:
                    tokenList.setProductVarStyle(value);
                    break;
                case PublishPrice:
                    tokenList.setPublishPrice(value);
                    break;
                case TicketCode:
                    tokenList.setTicketCode(value);
                    break;
                case TicketNumber:
                    tokenList.setTicketNumber(value);
                    break;
                case TicketType:
                    tokenList.setTicketType(value);
                    break;
                case TicketValidity:
                    tokenList.setTicketValidity(value);
                    break;
                case UsageValidity:
                    tokenList.setUsageValidity(value);
                    break;
                case ValidityEndDate:
                    tokenList.setValidityEndDate(value);
                    break;
                case ValidityStartDate:
                    tokenList.setValidityStartDate(value);
                    break;
                case TermAndCondition:
                    tokenList.setTermAndCondition(value);
                    break;
                case Disclaimer:
                    tokenList.setDisclaimer(value);
                    break;
                default:
                    break;
                }
            }
        }
        tokenData = JsonUtil.jsonify(tokenList);

        return tokenData;
    }

    @Override
    public ApiResult<KioskTransactionEntity> startRecoveryFlowTransaction(StoreApiChannels channel, HttpSession session, String receiptNumber) {
        ApiResult<KioskTransactionEntity> result = new ApiResult(ApiErrorCodes.General);

        if (armService.isReady(null, channel.code, receiptNumber) && armService.init(null, channel.code, receiptNumber)) {
            KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, session.getId());
            if (savedCart == null) {
                savedCart = new KioskFunCart();
            }

            KioskInfoEntity kioskInfo = kioskManagementService.getKioskInfo(channel);

            kioskManagementService.getNextTransactionId(channel, kioskInfo);
            String transactionId = kioskManagementService.getTransactionId(kioskInfo);
            kioskInfo.setCurrentCartId(transactionId);
            kioskManagementService.updateKioskInfo(kioskInfo);

            AxStarServiceResult<KioskSearchJournalTransactionsResponse> response = this.getJournalTransaction(channel, receiptNumber, kioskInfo);

            if (response.isSuccess()) {
                String id = response.getData().getAxJournalTransactionList().stream().findFirst().get().getId();

                if (null != id) {
                    AxSalesOrderSearch salesOrderSearch = null;

                    AxStarServiceResult<KioskSalesOrdersSearchResponse> responseSearch = this.getAxSalesOrderByReceiptNumber(channel, kioskInfo, id);
                    if (responseSearch.isSuccess()) {
                        salesOrderSearch = responseSearch.getData().getAxSalesOrderSearch().stream().findAny().get();
                        KioskFunCart currentFunCart = getCurrentFunCart(channel, session.getId());

                        if (null != salesOrderSearch) {
                            AxStarServiceResult<KioskCartTicketSearchResponse> ticketSearchResponse = this.getCartTicketByAxReceiptNumber(channel, receiptNumber, transactionId);
                            List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntities = null;

                            if (ticketSearchResponse.isSuccess()) {
                                axCartRetailTicketTableEntities = ticketSearchResponse.getData().getAxCartRetailTicketTableEntityList();

                                long printedCount = axCartRetailTicketTableEntities.stream().findFirst().get().getExtensionProperties().stream()
                                        .filter(property -> property.getKey().equalsIgnoreCase("SDC_NOTPRINTED")).map(property -> property.getValue())
                                        .filter(value -> value.getIntegerValue().equalsIgnoreCase("2")).count();

                                // 0 = Normal Process
                                // 1 = Not Printed
                                // 2 = Printed
                                if (printedCount <= 0) {
                                    result.setSuccess(true);
                                    currentFunCart.setAxCartRetailTicketTableEntities(axCartRetailTicketTableEntities);
                                    currentFunCart.setAxSalesOrderSearch(salesOrderSearch);
                                    cartManager.saveCart(channel, currentFunCart);
                                } else {
                                    result.setMessage("All Tickets were printed successfully.");
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private AxStarServiceResult<KioskSearchJournalTransactionsResponse> getJournalTransaction(StoreApiChannels channel, String receiptNumber, KioskInfoEntity kioskInfo) {
        KioskSearchJournalTransactionsRequest request = new KioskSearchJournalTransactionsRequest();

        AxTransactionSearchCriteria criteria = new AxTransactionSearchCriteria();
        criteria.setReceiptId(receiptNumber);
        criteria.setStoreId(kioskInfo.getStoreNumber());

        request.setAxTransactionSearchCriteria(criteria);
        return axStarKioskService.apiKioskSearchJournalTransactions(channel, request);
    }

    private AxStarServiceResult<KioskSalesOrdersSearchResponse> getAxSalesOrderByReceiptNumber(StoreApiChannels channel, KioskInfoEntity kioskInfo, String id) {
        KioskSalesOrdersSearchRequest request = new KioskSalesOrdersSearchRequest();

        AxSalesOrdersSearchCriteria criteria = new AxSalesOrdersSearchCriteria();
        criteria.setIncludeDetails(true);

        List<Integer> salesTransactionValueTypes = new ArrayList<>();
        salesTransactionValueTypes.add(2);
        criteria.setSalesTransactionTypeValues(salesTransactionValueTypes);

        List<String> transactionIds = new ArrayList<>();
        transactionIds.add(id);

        criteria.setTransactionIds(transactionIds);

        request.setAxSalesOrdersSearchCriteria(criteria);
        return axStarKioskService.apiKioskSalesOrdersSearch(channel, request);
    }

    @Override
    public FunCartDisplay constructRecoveryCartDisplayDetails(StoreApiChannels channel, String sessionId) {
        KioskFunCart funCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        if (funCart == null) {
            funCart = new KioskFunCart();
            funCart.setCartId(sessionId);
            funCart.setChannelCode(channel.code);
            AxStarCart axCart = new AxStarCart();
            axCart.setCartLines(new ArrayList<>());
            funCart.setAxCart(axCart);
        }
        FunCartDisplay display = this.constructRecoveryCartDisplayDetails(channel, funCart);
        saveRecoveryDetails(funCart, channel);
        return display;
    }

    private void saveRecoveryDetails(KioskFunCart funCart, StoreApiChannels channel) {
        try {
            KioskFunCart savedCart = (KioskFunCart) funCart;
            if (savedCart == null) {
                savedCart = new KioskFunCart();
            }

            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);

            String axTransactionId = funCart.getAxCartRetailTicketTableEntities().stream().findFirst().get().getTransactionId();

            KioskTransactionEntity tran = this.createTransaction(kioskInfo, funCart.getAxSalesOrderSearch(), axTransactionId);

            for (AxCartRetailTicketTableEntity axTicket : funCart.getAxCartRetailTicketTableEntities()) {
                tran.getTickets().add(this.getTicket(axTicket, tran));
            }

            this.getTransactionItem(channel, funCart.getAxSalesOrderSearch(), tran);

            transactionDao.saveAndFlush(tran);
            cartManager.saveCart(channel, savedCart);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private FunCartDisplay constructRecoveryCartDisplayDetails(StoreApiChannels channel, KioskFunCart cart) {
        final FunCartDisplay cd = new FunCartDisplay();

        try {
            cd.setCartId(cart.getCartId());

            FunCartDisplayCmsProduct cmsProduct = null;
            FunCartDisplayItem item = null;
            BigDecimal total = BigDecimal.ZERO;
            Integer totalQty = 0;
            Boolean hasExistingItem = Boolean.FALSE;

            AxSalesOrderSearch salesOrderSearch = null;
            if (null != cart.getAxSalesOrderSearch()) {
                salesOrderSearch = cart.getAxSalesOrderSearch();
            }

            for (AxSalesLine salesLine : salesOrderSearch.getSalesLines()) {

                FunCartDisplayItem existingItem = null;
                for (FunCartDisplayCmsProduct cms : cd.getCmsProducts()) {
                    for (FunCartDisplayItem existing : cms.getItems()) {
                        String existingKey = existing.getListingId();
                        String key = salesLine.getListingId();

                        if (key.equals(existingKey)) {
                            hasExistingItem = Boolean.TRUE;
                            existingItem = (FunCartDisplayItem) existing;
                        }
                    }
                }

                if (hasExistingItem) {
                    Integer qty = new BigDecimal(salesLine.getQuantity()).toBigInteger().intValue();
                    existingItem.setQty(existingItem.getQty() + qty);
                    totalQty += qty;

                    BigDecimal amount = new BigDecimal(salesLine.getTotalAmount());
                    existingItem.setTotal(existingItem.getTotal().add(amount));

                    if (!salesLine.getDiscountLines().isEmpty()) {
                        existingItem.setDiscountTotal(new BigDecimal(salesLine.getDiscountLines().get(0).getEffectiveAmount()));
                        existingItem.setDiscountTotalText(NvxNumberUtils.formatToCurrency(new BigDecimal(salesLine.getDiscountLines().get(0).getEffectiveAmount()), "S$ "));
                        existingItem.setDiscountLabel(salesLine.getDiscountLines().get(0).getOfferName());
                    }

                } else {
                    cmsProduct = new FunCartDisplayCmsProduct();
                    cmsProduct.setName(this.getAxProductItemName(channel, salesLine.getItemId()));
                    item = new FunCartDisplayItem();
                    cd.getCmsProducts().add(cmsProduct);
                    cmsProduct.getItems().add(item);

                    item.setQty(new BigDecimal(salesLine.getQuantity()).toBigInteger().intValue());
                    item.setListingId(salesLine.getListingId());
                    item.setName(this.getAxProductItemName(channel, salesLine.getItemId()));

                    item.setQty(new BigDecimal(salesLine.getQuantity()).intValue());
                    item.setLineId(salesLine.getLineId());
                    item.setPrice(new BigDecimal(salesLine.getPrice()));
                    item.setTotal(new BigDecimal(salesLine.getTotalAmount()));
                    item.setType((this.getAxProductTicketType(channel, salesLine.getItemId())));

                    final boolean isEvent = StringUtils.isNotBlank(salesLine.getComment());

                    if (isEvent) {
                        String[] comment = salesLine.getComment().split(";");
                        item.setEventSessionName(comment[1]);
                        item.setEventLineId(comment[1]);
                        Calendar selectedEventDate;
                        if (comment.length == 3) {
                            selectedEventDate = DateUtils.toCalendar(NvxDateUtils.parseAxDate(comment[2], NvxDateUtils.AX_DEFAULT_DATE_TIME_MSEC_FORMAT));
                        } else {
                            selectedEventDate = DateUtils.toCalendar(new Date());
                        }
                        item.setSelectedEventDate(NvxDateUtils.formatDate(selectedEventDate.getTime(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));

                        item.setDescription("Ticket Type : " + item.getType()
                                + (isEvent ? "<br>Event Session : " + item.getEventSessionName() + "<br>Event Date : " + item.getSelectedEventDate() : ""));
                    }

                    if (!salesLine.getDiscountLines().isEmpty()) {
                        item.setDiscountTotal(new BigDecimal(salesLine.getDiscountLines().get(0).getEffectiveAmount()));
                        item.setDiscountTotalText(NvxNumberUtils.formatToCurrency(new BigDecimal(salesLine.getDiscountLines().get(0).getEffectiveAmount()), "S$ "));
                        item.setDiscountLabel(salesLine.getDiscountLines().get(0).getOfferName());
                    } else {
                        item.setDiscountLabel("");
                    }

                    total = total.add(item.getTotal());
                    totalQty += item.getQty();

                }
            }
            cd.setTotalQty(totalQty);
            cd.setTotal(total);
            cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return cd;

    }

    @Override
    public ApiResult<KioskTransactionEntity> getTransactionByAxReceiptNumber(StoreApiChannels channel, String axReceiptNumber) {
        ApiResult<KioskTransactionEntity> result = new ApiResult(ApiErrorCodes.General);

        if (axReceiptNumber.contains("\"")) {
            axReceiptNumber = axReceiptNumber.replace("\"", "");
        }

        List<KioskTransactionEntity> entityList = this.transactionDao.getTransactionByAxReceipt(axReceiptNumber);

        if (!entityList.isEmpty()) {
            result.setData(entityList.stream().findFirst().get());
            result.setSuccess(true);
        }

        return result;
    }

    @Override
    public ApiResult<KioskTransactionEntity> cartTicketReprintStart(StoreApiChannels channel, HttpSession session, String receiptNumber) {
        ApiResult<KioskTransactionEntity> result = new ApiResult(ApiErrorCodes.General);
        KioskTransactionEntity transactionEntity = this.transactionDao.getTransactionByReceipt(receiptNumber);

        if (null != transactionEntity) {
            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);

            for (KioskTicketEntity ticket : transactionEntity.getTickets()) {
                List<AxCartTicketReprintCriteria> criteriaList = new ArrayList<>();

                AxCartTicketReprintCriteria criteria = new AxCartTicketReprintCriteria();

                criteria.setIsRealTime(0);
                criteria.setItemId(ticket.getItemId());
                criteria.setReasonCode("Reprint");
                criteria.setShiftId(kioskInfo.getCurrentShiftSequence() + "");
                criteria.setStaffId(kioskInfo.getUserId());
                criteria.setStoreId(kioskInfo.getStoreNumber());
                criteria.setTerminalId(kioskInfo.getDeviceNumber());
                criteria.setTicketCode(ticket.getTicketCode());
                criteria.setTicketTableId(ticket.getTicketCode());
                criteria.setTransactionId(ticket.getTransactionId());
                criteriaList.add(criteria);

                KioskCartTicketReprintStartRequest request = new KioskCartTicketReprintStartRequest();
                request.setAxCartTicketReprintCriteria(criteriaList);

                AxStarServiceResult<KioskCartTicketReprintStartResponse> cartTicketReprintStartResponse = this.axStarKioskService.apiKioskCartTicketReprintStart(channel, request);
                if (cartTicketReprintStartResponse.isSuccess()) {
                    if (StringUtils.isBlank(cartTicketReprintStartResponse.getData().getErrorMessage())) {
                        AxCartRetailTicketTableEntity axTicket = cartTicketReprintStartResponse.getData().getAxCartRetailTicketTableEntityList().stream().findFirst().get();
                        ticket.setTokenData(this.getTokenData(axTicket));
                        transactionEntity.getTickets().add(ticket);
                    } else {
                        log.error("apiKioskCartTicketReprintStart failed: " + cartTicketReprintStartResponse.getData().getErrorMessage());
                        result.setMessage(cartTicketReprintStartResponse.getData().getErrorMessage());
                        return result;
                    }
                }
            }

            transactionDao.saveAndFlush(transactionEntity);
            result.setSuccess(true);
        }
        return result;
    }

    @Override
    public Boolean updatePrintStatus(StoreApiChannels channel, HttpSession session, KioskPrintTicketResult printTicketResult, String axCartId, KioskInfoEntity kiosk) {
        Boolean updated = Boolean.FALSE;
        
        if (printTicketResult.hasErrors()) {
            cartDeactivateTicket(channel, axCartId);
            
            if (!armService.rejectTicket(kiosk, channel.code, axCartId)) {
                log.error("robotic arm failed to reject tickets");
            }
            
        } else {
            cartTicketReprintSuccess(channel, axCartId);

            KioskUpdatePrintStatusRequest request = new KioskUpdatePrintStatusRequest();
            AxCartTicketUpdatePrintStatusCriteria criteria = new AxCartTicketUpdatePrintStatusCriteria();
            request.setAxCartTicketUpdatePrintStatusCriteria(criteria);

            KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
            criteria.setDataAreaId("sdc");
            criteria.setShiftId(kioskInfo.getCurrentShiftSequence().toString());
            criteria.setStaffId(kioskInfo.getUserId());
            criteria.setStoreId(kioskInfo.getStoreNumber());
            criteria.setTerminalId(kioskInfo.getTerminalNumber());
            criteria.setTransactionId(axCartId);

            AxPrintHistory history = null;
            for (KioskTicketStatus ticket : printTicketResult.getPrintHistory()) {
                history = new AxPrintHistory();
                history.setErrorMessage(ticket.getErrorMessage());
                history.setId(ticket.getId());
                if (CollectionUtils.isNotEmpty(ticket.getStatusCodes())) {
                    history.setStatusCodes(StringUtils.join(ticket.getStatusCodes()));
                }
                history.setSuccess(ticket.getSuccess());
                criteria.getAxPrintHistory().add(history);
            }

            AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> updatePrintedStatusResult = this.axStarKioskService.apiKioskCartTicketUpdatePrintStatus(channel, request);
            if (updatePrintedStatusResult.isSuccess()) {
                if (StringUtils.isBlank(updatePrintedStatusResult.getData().getErrorMessage())) {
                    updated = Boolean.TRUE;
                } else {
                    log.error("apiKioskCartTicketUpdatePrintStatus failed" + updatePrintedStatusResult.getData().getErrorMessage());
                }
            }

            if (!armService.issueTicket(kioskInfo, channel.code, axCartId)) {
                log.error("robotic arm failed to issue tickets");
            }
        }

        final String sessionId = session.getId();
        final KioskFunCart savedCart = (KioskFunCart) cartManager.getCart(channel, sessionId);
        AxSalesOrderSearch searchResult = savedCart.getAxSalesOrderSearch();
        if (null != searchResult) {
            KioskTransactionEntity tran = null;
            List<KioskTransactionEntity> tranList = this.transactionDao.getTransactionByAxCartIdByOrderByIdDesc(axCartId);
            if (!tranList.isEmpty()) {
                tran = tranList.stream().findFirst().get();
            }
            if (tran != null) {
                tran.setAllTicketsPrinted(Boolean.TRUE);
                if (printTicketResult.hasErrors()) {
                    tran.setAllTicketsPrinted(Boolean.FALSE);
                }
                this.transactionDao.saveAndFlush(tran);
            }
        }

        zabbixPrinterService.checkTicketPrinterStatus(kiosk, channel.code);
       
        return updated;
    }

    private void cartTicketReprintSuccess(StoreApiChannels channel, String axCartId) {
        AxCartTicketUpdateReprintSuccessCriteria criteria = new AxCartTicketUpdateReprintSuccessCriteria();
        criteria.setTransactionId(axCartId);

        KioskCartTicketUpdateReprintSuccessRequest request = new KioskCartTicketUpdateReprintSuccessRequest();
        request.setAxCartTicketUpdateReprintSuccessCriteria(criteria);

        AxStarServiceResult<KioskCartTicketUpdateReprintSuccessResponse> updatePrintedStatusResult = this.axStarKioskService.apiKioskUpdateReprintSuccess(channel, request);
        if (updatePrintedStatusResult.isSuccess()) {
            if (StringUtils.isBlank(updatePrintedStatusResult.getData().getErrorMessage())) {
                KioskInfoEntity kioskInfo = this.kioskManagementService.getKioskInfo(channel);
                kioskInfo.setCurrentReceiptSequence(kioskInfo.getCurrentReceiptSequence() + 1);
                kioskInfo.setCurrentCartId("");
                this.kioskManagementService.updateKioskInfo(kioskInfo);

            } else {
                log.error("apiKioskUpdateReprintSuccess failed " + updatePrintedStatusResult.getData().getErrorMessage());
            }
        }
    }

    private void cartDeactivateTicket(StoreApiChannels channel, String axCartId) {
        AxCartDeactivateTicketCriteria deactivateTicketCriteria = new AxCartDeactivateTicketCriteria();
        deactivateTicketCriteria.setTransactionId(axCartId);

        KioskCartDeactivateTicketRequest request = new KioskCartDeactivateTicketRequest();
        request.setAxCartDeactivateTicketCriteria(deactivateTicketCriteria);

        AxStarServiceResult<KioskCartDeactivateTicketResponse> deactivateResponse = this.axStarKioskService.apiKioskDeactivateTicket(channel, request);

        if (!deactivateResponse.isSuccess()) {
            log.error("apiKioskDeactivateTicket failed " + deactivateResponse.getData().getErrorMessage());
        }
    }
}
