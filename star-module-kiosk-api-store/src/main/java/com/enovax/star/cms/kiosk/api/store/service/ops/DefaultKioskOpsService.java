package com.enovax.star.cms.kiosk.api.store.service.ops;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.mgnl.definition.KioskReceiptTemplate;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.ticketgen.KioskPaymentItem;
import com.enovax.star.cms.commons.model.ticketgen.ReceiptItem;
import com.enovax.star.cms.commons.model.ticketgen.ReceiptMain;
import com.enovax.star.cms.commons.model.ticketgen.TicketIssuanceStatus;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.kiosk.api.store.controller.OnlineStoreKioskOpsController;
import com.enovax.star.cms.kiosk.api.store.param.ExecutionResponse;
import com.enovax.star.cms.kiosk.api.store.param.ExecutionResult;
import com.enovax.star.cms.kiosk.api.store.persistence.model.*;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskCustomEventDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskEventDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionPaymentDao;
import com.enovax.star.cms.kiosk.api.store.reference.KioskCardEntryMode;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentCardType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentTransactionType;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionStatus;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.sync.IKioskDBSyncService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import info.magnolia.context.MgnlContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by tharaka on 16/9/16.
 */
@Service
public class DefaultKioskOpsService implements IKioskOpsService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskOpsService.class);

    /**
     * NOTE: Ticket and Payment Library Response
     *  =======================================================================================
     *  private final String HOST_CONNECTION_PROBLEM_RESPONSE = "HS";
     *  private final String HOST_NO_RESPONSE = "HR";
     *  private final String LOGON_RESPONSE = "LR";
     *  private final String HOST_INVALID_RESPONSE = "HI";
     *  
     *  private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_1 = "Please look for ticketing";
     *  private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_2 = "staff for assistance";
     *  private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_3 = "Redemption failed";
     *  private String REDEMPTION_TICKET_ISSUANCE_NOT_ISSUED_MSG_4 = "Service recovery required";
     *
     *  private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_1 = "We apologize that we are not";
     *  private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_2 = "able to print your ticket here.";
     *  private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_3 = "Please proceed to our";
     *  private String RECOVERY_TICKET_ISSUANCE_NOT_ISSUED_MSG_4 = "ticketing counter";
     *  =======================================================================================
     */
    
    private String DUMMY_SIGNATURE = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    private String NO_SIGNATURE = "NO SIGNATURE REQUIRED";

    private String TICKET_ISSURANCE_COMPLETED = "COMPLETED";
    private String TICKET_ISSURANCE_NOT_ISSUED = "NOT ISSUED";
    private String TICKET_ISSURANCE_COMPLETED_MSG_1 = "All tickets are";
    private String TICKET_ISSURANCE_COMPLETED_MSG_2 = "Successfully issued";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_1 = "All tickets are NOT issued";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_2 = "You may processed to other";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_3 = "ticketing kiosk to print";
    private String TICKET_ISSURANCE_NOT_ISSUED_MSG_4 = "your ticket";
    private String PAYMENT_MODE_NETS_PIN_DEBIT = KioskPaymentCardType.NETSPINDEBIT.getDescription();
    private String PAYMENT_MODE_NETS_FLASHPAY = KioskPaymentCardType.NFP.getDescription();

    private final int MAX_SETTLEMENT_EXECUTION = 5;

    @Value("${kiosk.api.payment.adapter://http://localhost:8081/}")
    private String paymentAdapterURL;
    
    @Autowired
    private IKioskEventDao kioskEventDao;

    @Autowired
    private IKioskCustomEventDao kioskCustomEventDao;

    @Autowired
    private IKioskTransactionDao kioskTransactionDao;

    @Autowired
    private IKioskManagementService defaultKioskManagementService;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;

    @Autowired
    private IKioskTransactionPaymentDao kioskTransactionPaymentDao;
    
    @Autowired
    private IKioskDBSyncService syncService;

    @Override
    public void resumeOperationMode(KioskEventEntity kioskEventEntity) {
        List<KioskEventEntity> allEvents = kioskEventDao.getOperationBystatus(OnlineStoreKioskOpsController.STATUS_ACTIVE, OnlineStoreKioskOpsController.OPERATION_MODE_VALUE_CLOSE,
                OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_NAME, kioskEventEntity.getApiChannel());
       if(!allEvents.isEmpty()){
           KioskEventEntity eventEntity = allEvents.stream().findFirst().get();
           if(null != kioskEventEntity){
               
               //update the previous record
               eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_INACTIVE);
               kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
               syncService.publishEvent(eventEntity);
               //create new record
               this.constructEvent(kioskEventEntity);
           }
       }
    }
    
    private void constructEvent(KioskEventEntity eventEntity){
        eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
        kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
        syncService.publishEvent(eventEntity);
    }

    @Override
    public void downOperationMode(KioskEventEntity kioskEventEntity) {
        List<KioskEventEntity> allEvents = kioskEventDao.getOperationBystatus(OnlineStoreKioskOpsController.STATUS_ACTIVE, OnlineStoreKioskOpsController.OPERATION_MODE_VALUE_OPEN, OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_NAME, kioskEventEntity.getApiChannel());
        
        if(!allEvents.isEmpty()){
            KioskEventEntity eventEntity = allEvents.stream().findFirst().get();
            if (eventEntity != null) {
                if (OnlineStoreKioskOpsController.STATUS_ACTIVE.equals(eventEntity.getStatus())
                        && OnlineStoreKioskOpsController.OPERATION_MODE_VALUE_OPEN.equals(eventEntity.getValue())) {
                    eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_INACTIVE);
                    kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
                    syncService.publishEvent(eventEntity);
                }
                this.constructEvent(kioskEventEntity);
               
                
            }
        } else {
            List<KioskEventEntity> allActiveOpesEvent = kioskEventDao.getTopActiveOperationsByType(OnlineStoreKioskOpsController.STATUS_ACTIVE, OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_NAME, kioskEventEntity.getApiChannel());
            if (allActiveOpesEvent.isEmpty()) {
                kioskEventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
                kioskEventEntity.setEventName(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_NAME);
                kioskEventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
                kioskEventDao.saveAndFlush(setUserAndTimeStamp(kioskEventEntity));
                syncService.publishEvent(kioskEventEntity);
            }
        }
       

 
    }

    @Override
    public boolean getOperationMode(String channel) {
        List<KioskEventEntity> allEvents = kioskEventDao.findTop1ByApiChannelAndEventNameOrderByIdDesc(channel, OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_NAME);

        if (allEvents != null & allEvents.size() > 0) {
            KioskEventEntity eventEntity = allEvents.get(0);
            if (eventEntity != null) {// & eventEntity.getValue()!=null
                if (OnlineStoreKioskOpsController.OPERATION_MODE_VALUE_OPEN.equals(eventEntity.getValue())) {
                    return true;
                } else if (OnlineStoreKioskOpsController.OPERATION_MODE_VALUE_CLOSE.equals(eventEntity.getValue())) {
                    return false;
                }
            }
        } else if (allEvents != null & allEvents.size() == 0) {
            return true;
        }
        return true;
    }

    @Override
    public boolean addOtherInfo(KioskEventEntity kioskEventEntity) {
        if (null != kioskEventEntity) {
            kioskEventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            kioskEventEntity.setEventName(OnlineStoreKioskOpsController.OTHER_INFO_EVENT_NAME);
            kioskEventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            KioskEventEntity eventEntity = kioskEventDao.saveAndFlush(setUserAndTimeStamp(kioskEventEntity));
            if (null != eventEntity) {
                 syncService.publishEvent(eventEntity);
                return true;
            } else {
                log.error("Error saving in the database :", kioskEventEntity.getDescription());
            }
        }
        return false;
    }

    @Override
    public boolean addServiceInfo(KioskEventEntity kioskEventEntity) {

        if ((kioskEventEntity.getValue() != null && !kioskEventEntity.getValue().equals(""))
                && (kioskEventEntity.getDescription() != null && !kioskEventEntity.getDescription().equals(""))) {

            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            eventEntity.setEventName(OnlineStoreKioskOpsController.SERVICE_INFO_EVENT_NAME);
            eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            eventEntity.setValue(kioskEventEntity.getValue());
            eventEntity.setDescription(kioskEventEntity.getDescription());
            eventEntity.setDeviceNo(kioskEventEntity.getDeviceNo());
            eventEntity.setKioskName(kioskEventEntity.getKioskName());
            eventEntity.setApiChannel(kioskEventEntity.getApiChannel());
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
            syncService.publishEvent(eventEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addQtyRejectedPaperInfo(KioskEventEntity kioskEventEntity) {
        if (kioskEventEntity.getValue() != null && !kioskEventEntity.getValue().equals("")) {

            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            eventEntity.setEventName(OnlineStoreKioskOpsController.QTY_REJECTED_PAPER_INFO_EVENT_NAME);
            eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            eventEntity.setValue(kioskEventEntity.getValue());
            eventEntity.setDeviceNo(kioskEventEntity.getDeviceNo());
            eventEntity.setKioskName(kioskEventEntity.getKioskName());
            eventEntity.setApiChannel(kioskEventEntity.getApiChannel());
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
            syncService.publishEvent(eventEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addLoadPaperTicketInfo(KioskLoadPaperTicketInfoEntity paperTicketInfoEntity) {
        if (paperTicketInfoEntity != null) {

            // KioskLoadPaperTicketInfoEntity eventEntity = new
            // KioskLoadPaperTicketInfoEntity();
            paperTicketInfoEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            paperTicketInfoEntity.setEventName(OnlineStoreKioskOpsController.LOAD_PAPER_TICKET_INFO_EVENT_NAME);
            paperTicketInfoEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);

            kioskCustomEventDao.saveAndFlush(setUserAndTimeStamp(paperTicketInfoEntity));
            syncService.publishLoadTicketEvent(paperTicketInfoEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addTransactionStatus(KioskEventEntity kioskEventEntity) {
        if (kioskEventEntity.getDescription() != null && !kioskEventEntity.getDescription().equals("")) {

            kioskEventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            kioskEventEntity.setEventName(OnlineStoreKioskOpsController.TRANSACTION_STATUS_INFO_EVENT_NAME);
            kioskEventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(kioskEventEntity));
            syncService.publishEvent(kioskEventEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addReceiptHistoryStatus(KioskEventEntity kioskEventEntity) {
        if (kioskEventEntity.getDescription() != null && !kioskEventEntity.getDescription().equals("")) {
            kioskEventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            kioskEventEntity.setEventName(OnlineStoreKioskOpsController.RECEIPT_PRINTER_INFO_EVENT_NAME);
            kioskEventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(kioskEventEntity));
            syncService.publishEvent(kioskEventEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addHardwareStatus(KioskEventEntity kioskEventEntity) {
        if (kioskEventEntity.getDescription() != null && !kioskEventEntity.getDescription().equals("")) {

            kioskEventEntity.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            kioskEventEntity.setEventName(OnlineStoreKioskOpsController.HARDWARE_STATUS_INFO_EVENT_NAME);
            kioskEventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(kioskEventEntity));
            syncService.publishEvent(kioskEventEntity);
            return true;
        }
        return false;
    }

    @Override
    public String getSettlementInfo(String channel, KioskInfoEntity kioskinfo) {
        List<KioskEventEntity> allEvents = kioskEventDao.findTop1ByApiChannelAndEventNameOrderByIdDesc(channel,
                OnlineStoreKioskOpsController.SHUTDOWN_OR_SETTLEMENT_INFO_EVENT_NAME);
        // if value is 0 = two buttons need to enable
        // if value is 1 = with button need to disable

        if (allEvents != null & allEvents.size() > 0) {
            KioskEventEntity eventEntity = allEvents.get(0);
            if (eventEntity != null) {// & eventEntity.getValue()!=null
                String shift = kioskinfo.getCurrentShiftSequence() + "";
                if (OnlineStoreKioskOpsController.SHUTDOWN_WITH_SETTLEMENT.equals(eventEntity.getValue()) && shift.equals(eventEntity.getDescription())) {
                    return "1";
                } else if (OnlineStoreKioskOpsController.SHUTDOWN_WITH_SETTLEMENT.equals(eventEntity.getValue()) && !shift.equals(eventEntity.getDescription())) {
                    return "0";
                }
            }
        } else if (allEvents != null & allEvents.size() == 0) {
            return "0";
        }
        return "0";
    }

    private boolean addSettlementEvent(KioskInfoEntity kioskInfoEntity) {
        boolean status = true;
        try {
            KioskEventEntity eventEntity = new KioskEventEntity();
            eventEntity.setDeviceNo(kioskInfoEntity.getDeviceNumber());
            eventEntity.setKioskName(kioskInfoEntity.getJcrNodeName());
            eventEntity.setDescription(kioskInfoEntity.getCurrentShiftSequence() + "");
            eventEntity.setEventName(OnlineStoreKioskOpsController.SHUTDOWN_OR_SETTLEMENT_INFO_EVENT_NAME);
            eventEntity.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            eventEntity.setValue(OnlineStoreKioskOpsController.SHUTDOWN_WITH_SETTLEMENT);
            kioskEventDao.saveAndFlush(setUserAndTimeStamp(eventEntity));
            syncService.publishEvent(eventEntity);
        } catch (Exception e) {
          log.error("Event Persistent Exception: ", e);
          status = false;
        }

        return status;
    }

    @Override
    public List<KioskTransactionEntity> getTransactionStatus(String startD, String endD, String tranId, String pinCode, String apiChannel) {
        List<KioskTransactionEntity> transactions = new ArrayList<>();
        
        try {
            Date startDate = NvxDateUtils.parseDate(startD, NvxDateUtils.DATE_TIME_FORMAT);
            Date endDate = NvxDateUtils.parseDate(endD, NvxDateUtils.DATE_TIME_FORMAT);
            Calendar calendarStartDate = Calendar.getInstance();
            calendarStartDate.setTime(startDate);
            Calendar calendarEndDate = Calendar.getInstance();
            calendarEndDate.setTime(endDate);

            if(StringUtils.isBlank(tranId) && StringUtils.isBlank(pinCode)){
                transactions = kioskTransactionDao.getTransactionByDate(calendarStartDate.getTime(), calendarEndDate.getTime(), apiChannel);
            } else if (StringUtils.isBlank(tranId) && StringUtils.isNotBlank(pinCode)){
                transactions = kioskTransactionDao.getTransactionsByPinCode(calendarStartDate.getTime(), calendarEndDate.getTime(), pinCode, apiChannel);
            } else if (StringUtils.isNotBlank(tranId) && StringUtils.isBlank(pinCode)) {
                transactions = kioskTransactionDao.getTransactionByTranID(calendarStartDate.getTime(), calendarEndDate.getTime(), tranId, apiChannel);
            } else  if (StringUtils.isNotBlank(tranId) && StringUtils.isNotBlank(pinCode)) {
                transactions = kioskTransactionDao.getTransactionByAll(pinCode, calendarStartDate.getTime(), calendarEndDate.getTime(), tranId, apiChannel);
            }
        } catch (ParseException e) {
            log.error("Date Parsing Exception: ", e);
        }
        return transactions;
    }

    @Override
    public List<KioskTransactionEntity> getReceiptDetails(String startD, String endD, String apiChannel) {
        List<KioskTransactionEntity> transactions = new ArrayList<>();

        try {
            Date startDate = NvxDateUtils.parseDate(startD, NvxDateUtils.DATE_TIME_FORMAT);
            Date endDate = NvxDateUtils.parseDate(endD, NvxDateUtils.DATE_TIME_FORMAT);
            Calendar calendarStartDate = Calendar.getInstance();
            calendarStartDate.setTime(startDate);
            Calendar calendarEndDate = Calendar.getInstance();
            calendarEndDate.setTime(endDate);

            transactions = kioskTransactionDao.getTransactionsByStartDateTimeAndEndDateTime(calendarStartDate.getTime(), calendarEndDate.getTime());

        } catch (ParseException e) {
            log.error("Date Parsing Exception: ", e);
        }
        return transactions;
    }

    @Override
    public KioskTransactionEntity getReceiptDetailsByTranId(String trandId, String channel) {
        KioskTransactionEntity transaction = kioskTransactionDao.getReceiptDetailsByTranId(trandId, channel).stream().findFirst().orElse(null);
        return transaction;
    }

    @Override
    public void addResponseSettlementData(JsonObject result, KioskInfoEntity deviceInfo) {
        KioskTransactionEntity transactionEntity = new KioskTransactionEntity();

        Set<KioskTransactionPaymentEntity> kioskPaymentItems = new HashSet<KioskTransactionPaymentEntity>();
        KioskTransactionPaymentEntity paymentEntity = new KioskTransactionPaymentEntity();

        JsonObject responseCode = result.getAsJsonObject("data").getAsJsonObject();
        paymentEntity.setCallbackData(responseCode.toString());
        paymentEntity.setTerminalId(result.getAsJsonObject("data").getAsJsonPrimitive("terminalId").getAsString());
        paymentEntity.setMerchantId(result.getAsJsonObject("data").getAsJsonPrimitive("merchantId").getAsString());
        paymentEntity.setCardTypeDescription(result.getAsJsonObject("data").getAsJsonPrimitive("type").getAsString());
        kioskPaymentItems.add(paymentEntity);

        transactionEntity.setKiosk(deviceInfo);
        transactionEntity.setPayments(kioskPaymentItems);
 
        
        kioskTransactionDao.saveAndFlush(transactionEntity);
        syncService.publishTransaction(transactionEntity);
    }

    private KioskEventEntity setUserAndTimeStamp(KioskEventEntity eventEntity) {
        if (eventEntity.getLogonuser() == null) {
            info.magnolia.cms.security.User user = MgnlContext.getUser();
            eventEntity.setLogonuser(user.getName());
        }
        if (eventEntity.getCreatets() == null) {
            java.util.Date today = new java.util.Date();
            eventEntity.setCreatets(new java.sql.Timestamp(today.getTime()));
        }
        return eventEntity;
    }

    private KioskLoadPaperTicketInfoEntity setUserAndTimeStamp(KioskLoadPaperTicketInfoEntity eventEntity) {
        if (eventEntity.getLogonuser() == null) {
            info.magnolia.cms.security.User user = MgnlContext.getUser();
            eventEntity.setLogonuser(user.getName());
        }
        if (eventEntity.getCreatets() == null) {
            java.util.Date today = new java.util.Date();
            eventEntity.setCreatets(new java.sql.Timestamp(today.getTime()));
        }
        return eventEntity;
    }

    @Override
    public boolean processDeviceSettlement(String url, KioskInfoEntity kioskInfoEntity, String apiChannel) {
        boolean status = false;
        if (this.addSettlementEvent(kioskInfoEntity)) {
            HttpEntity<String> requestEntity = new HttpEntity<>(null, getJSONHttpHeaders());
            RestTemplate template = new RestTemplate();
            KioskTransactionEntity trans = this.constructTransaction();

            for (int i = 0; i < MAX_SETTLEMENT_EXECUTION; i++) {
                ExecutionResponse response = null;
                String settlementResponse = template.postForObject(url, requestEntity, String.class);
                try {
                    response = this.getExecutionResponse(settlementResponse);
                    if (null != response) {
                        for (ExecutionResult result : (List<ExecutionResult>) response.getResults()) {
                            this.constructTransactionPayment(result, trans);
                        }
                    }
                } catch (Exception e) {
                    log.error("Process Settlement Service", e);
                }

                if (response.isResult()) {
                    status = true;
                    break;
                }
            }
        }
        return status;
    }
    
    private KioskTransactionEntity constructTransaction(){
        KioskTransactionEntity trans = new KioskTransactionEntity();
        trans.setType(KioskTransactionType.SETTLEMENT.getCode());
        return kioskTransactionDao.save(trans);
    }


    private KioskTransactionPaymentEntity constructTransactionPayment(ExecutionResult result, KioskTransactionEntity trans){
        KioskTransactionPaymentEntity entity = new KioskTransactionPaymentEntity();

        String settlementType = KioskPaymentTransactionType.SETTLEMENT_CREDIT.getCode();

        if (result.getProcessType().equalsIgnoreCase("NetsSettlement")) {
            settlementType = KioskPaymentTransactionType.SETTLEMENT_NETS.getCode();
        }
        
        String jsonString = new Gson().toJson(result.getResult());

        entity.setType(settlementType);
        entity.setCallbackData(jsonString.toString());
        entity.setStatus(result.isSuccess());
        entity.setResponseCode(this.getJsonObject(jsonString).get("responseCode").getAsString());
        entity.setTran(trans);

        kioskTransactionPaymentDao.save(entity);

        return entity;
    }
    
    private JsonObject getJsonObject(String jsonString) {
        JsonParser parser = new JsonParser();
        return parser.parse(jsonString).getAsJsonObject();
    }
    
    private ExecutionResponse getExecutionResponse(String settlementResponse) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            ApiResult response = mapper.readValue(settlementResponse, ApiResult.class);

            String jsonObject = new Gson().toJson(response.getData()).toString();

            return mapper.readValue(jsonObject, ExecutionResponse.class);
        } catch (IOException e) {
           log.error("getExecutionResponse: ", e);
        }
        
        return null;
    }

    // TODO: Validate if needs restart
    /*
     * private Boolean logonPaymentTerminal() { Boolean logon = Boolean.FALSE;
     * try { HttpHeaders headers = new HttpHeaders();
     * headers.setContentType(MediaType.APPLICATION_JSON); HttpEntity<String>
     * requestEntity = new HttpEntity<>(headers); RestTemplate template = new
     * RestTemplate(); String logonResponse =
     * template.postForObject(paymentServiceURL + "/payment/logon/",
     * requestEntity, String.class); JsonParser parser = new JsonParser();
     * JsonObject result = (JsonObject) parser.parse(logonResponse);
     * log.info(result.toString()); if
     * ("true".equals(result.get("success").getAsString())) { logon =
     * Boolean.TRUE; String merchantInfo =
     * result.getAsJsonObject("data").getAsJsonObject("details").get(
     * "netsMerchantNameLoc").getAsString();
     * 
     * KioskInfoEntity kioskInfo = getDeviceInfo();
     * kioskInfo.setTerminalMerchantNameAndLocation(merchantInfo);
     * defaultKioskManagementService.updateKioskInfo(kioskInfo); }
     * 
     * } catch (Exception e) { log.error(e.getMessage(), e); } return logon; }
     */

    protected HttpHeaders getJSONHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Override
    public List<KioskLoadPaperTicketInfoEntity> getPaperTicketLoadEvent(String channel, String date, String kioskId) {
        Date beginDate = new Date();
        try {
            beginDate = NvxDateUtils.parseDate(date + " 00:00:00", NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        Date endDate = new DateTime(beginDate.getTime()).plusDays(1).toDate();
        List<KioskLoadPaperTicketInfoEntity> events = new ArrayList<>();
        // this.ticketDeventDao.getPaperTicketEvent(beginDate, endDate, channel,
        // kioskId);
        return events;
    }

    @Override
    public ReceiptMain getCompanyReceiptDetails(String channel) {
        Node receiptTemplate = kioskfn.getReceiptTemplateParams(channel);

        final ReceiptMain receipt = new ReceiptMain();

        try {
            receipt.setLogo(receiptTemplate.getProperty(KioskReceiptTemplate.Logo.getPropertyName()).getString());
            receipt.setCompanyName(receiptTemplate.getProperty(KioskReceiptTemplate.CompanyName.getPropertyName()).getString());
            receipt.setAddress(receiptTemplate.getProperty(KioskReceiptTemplate.Address.getPropertyName()).getString());
            receipt.setPostalCode(receiptTemplate.getProperty(KioskReceiptTemplate.PostalCode.getPropertyName()).getString());
            receipt.setContactDetails(receiptTemplate.getProperty(KioskReceiptTemplate.ContactDetails.getPropertyName()).getString());
            receipt.setGstDetails(receiptTemplate.getProperty(KioskReceiptTemplate.GSTDetails.getPropertyName()).getString());
        } catch (RepositoryException e) {
            log.error("No Details Available");
        }
        return receipt;
    }

    @Override
    public ReceiptMain getTransactionDetails(String transactionId, String channel) {

        KioskTransactionEntity txn = kioskTransactionDao.getReceiptDetailsByTranId(transactionId, channel).stream().findFirst().orElse(null);

        final Date now = new Date();
        final ReceiptMain receipt = new ReceiptMain();
        receipt.setPrintedDateText(NvxDateUtils.formatDate(now, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        receipt.setTransId(getStringValue(txn.getReceiptNumber()));
        receipt.setReceiptNumber(getStringValue(txn.getAxReceiptNumber() + ""));
        receipt.setReceiptSeqNumber(getStringValue(txn.getAxReceiptId() + ""));
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(txn.getTotalAmount(), "$ "));
        receipt.setLocation(getStringValue(txn.getKiosk().getLocation()));
        receipt.setKioskId(getStringValue(txn.getKiosk().getJcrNodeName()));
        receipt.setGst(NvxNumberUtils.formatToCurrency(Optional.ofNullable(txn.getGstAmount()).orElse(new BigDecimal(0)), "$ "));

        Node receiptTemplate = kioskfn.getReceiptTemplateParams(channel);

        try {
            receipt.setLogo(receiptTemplate.getProperty(KioskReceiptTemplate.Logo.getPropertyName()).getString());
            receipt.setCompanyName(receiptTemplate.getProperty(KioskReceiptTemplate.CompanyName.getPropertyName()).getString());
            receipt.setAddress(receiptTemplate.getProperty(KioskReceiptTemplate.Address.getPropertyName()).getString());
            receipt.setPostalCode(receiptTemplate.getProperty(KioskReceiptTemplate.PostalCode.getPropertyName()).getString());
            receipt.setContactDetails(receiptTemplate.getProperty(KioskReceiptTemplate.ContactDetails.getPropertyName()).getString());
            receipt.setGstDetails(receiptTemplate.getProperty(KioskReceiptTemplate.GSTDetails.getPropertyName()).getString());
        } catch (RepositoryException e) {
            log.error("No Details Available");
        }

        final List<ReceiptItem> items = new ArrayList<>();

        Map<String, List<KioskTransactionItemEntity>> itemGroup = new HashMap<>();

        itemGroup = txn.getItems().stream().collect(Collectors.groupingBy(KioskTransactionItemEntity::getProductName));

        Map<String, List<KioskTransactionItemEntity>> topupItemMap = new HashMap<>();
        List<KioskTransactionItemEntity> axProductItems;
        List<List<KioskTransactionItemEntity>> axProductItemGroup = new ArrayList<>();

        for (Map.Entry<String, List<KioskTransactionItemEntity>> entry : itemGroup.entrySet()) {
            axProductItems = new ArrayList<>();
            for (KioskTransactionItemEntity item : entry.getValue()) {
                if (StringUtils.isBlank(item.getParentProductId())) {
                    axProductItems.add(item);
                } else {
                    if (topupItemMap.get(item.getParentProductId()) == null) {
                        topupItemMap.put(item.getParentProductId(), new ArrayList<>());
                    }
                    topupItemMap.get(item.getParentProductId()).add(item);
                }
            }
            axProductItemGroup.add(axProductItems);
        }

        for (List<KioskTransactionItemEntity> entries : axProductItemGroup) {
            int i = 0;
            for (KioskTransactionItemEntity tranItem : entries) {

                ReceiptItem existingReceiptItem = items.stream()
                        .filter(item -> item.getAxProductName().equals(tranItem.getAxProductName()) && StringUtils.isNotBlank(tranItem.getProductName())).findAny().orElse(null);

                if (null != existingReceiptItem) {
                    String existingQty = existingReceiptItem.getQty();
                    int totalQty = Integer.parseInt(existingQty) + Integer.parseInt(tranItem.getQuantity());

                    String existingTotal = existingReceiptItem.getSubtotalText().replaceAll("[^\\d.]+", "");
                    BigDecimal totalSubtotal = new BigDecimal(existingTotal).add(Optional.ofNullable(tranItem.getSubTotal()).orElse(BigDecimal.ZERO));

                    String existingAmount = existingReceiptItem.getAmount().replaceAll("[^\\d.]+", "");
                    BigDecimal totaAmount = new BigDecimal(existingAmount).add(Optional.ofNullable(tranItem.getAmount()).orElse(BigDecimal.ZERO));

                    existingReceiptItem.setQty(totalQty + "");
                    existingReceiptItem.setAmount(NvxNumberUtils.formatToCurrency(totaAmount, "$ "));
                    existingReceiptItem.setSubtotalText(NvxNumberUtils.formatToCurrency(totalSubtotal, "$ "));
                } else {
                    items.add(this.createReceiptItem(tranItem, i));
                    i++;
                    List<KioskTransactionItemEntity> tops = topupItemMap.get(tranItem.getListingId());
                    if (tops != null) {
                        for (KioskTransactionItemEntity topup : tops) {
                            items.add(this.createReceiptItem(topup, i));
                            i++;
                        }
                    }
                }
            }
        }

        receipt.setItems(items);

        final List<KioskPaymentItem> kioskPaymentItem = new ArrayList<>();
        for (KioskTransactionPaymentEntity tranItem : txn.getPayments()) {
            final KioskPaymentItem item = new KioskPaymentItem();
            item.setTerminalId(getStringValue(tranItem.getTerminalId()));
            item.setMerchantId(getStringValue(tranItem.getMerchantId()));

            String paymentMode = "";
            // get a payment mode and Entry mode
            if (tranItem != null & (tranItem.getEntryMode() != null && !tranItem.getEntryMode().equals(""))) {
                String entryMode = KioskCardEntryMode.toEnum(tranItem.getEntryMode()).getDescription();
                item.setEntryMode(getStringValue(entryMode));
            }
            if (tranItem != null & (tranItem.getPaymentMode() != null && !tranItem.getPaymentMode().equals(""))) {
                paymentMode = KioskPaymentCardType.toEnum(tranItem.getPaymentMode()).getDescription();
                item.setPaymentMode(getStringValue(paymentMode));
            }

            // set the NETS Trace(BatchNumber) and NETS Receipt(InvoiceNumber)
            // details for NETS PIN
            // set the Original Balance(InvoiceNumber) and Trans Amount
            // (ApprovalCode) and Empty line (RetrivalReferenceData) and Remain
            // Balance(Merchant) details for NETS FLASH
            if (PAYMENT_MODE_NETS_PIN_DEBIT != null && PAYMENT_MODE_NETS_PIN_DEBIT.equals(paymentMode)) {
                item.setApprovalCode(tranItem.getApprovalCode());
                item.setBatchNumber(getStringValue(getStringValue(tranItem.getNetsTraceNumber())));
                item.setInvoiceNumber(getStringValue(getStringValue(tranItem.getNetsReceiptNumber())));
                item.setMerchantName(txn.getKiosk().getTerminalMerchantNameAndLocation());
            } else if (PAYMENT_MODE_NETS_FLASHPAY != null && PAYMENT_MODE_NETS_FLASHPAY.equals(paymentMode)) {
                item.setInvoiceNumber(tranItem.getOriginalBalance() == null ? NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO)
                        : NumberFormat.getCurrencyInstance().format(tranItem.getOriginalBalance()));
                item.setApprovalCode(getStringValue("-     " + NumberFormat.getCurrencyInstance().format(tranItem.getAmount()) + ""));
                item.setRetrivalReferenceData(getStringValue("==========="));
                item.setMerchantName(getStringValue(tranItem.getRemainingBalance() + ""));
                item.setMerchantName(tranItem.getRemainingBalance() == null ? NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO)
                        : NumberFormat.getCurrencyInstance().format(tranItem.getRemainingBalance()));
            } else {
                item.setBatchNumber(getStringValue(tranItem.getBatchNumber()));
                item.setInvoiceNumber(getStringValue(tranItem.getInvoiceNumber()));
                item.setApprovalCode(getStringValue(tranItem.getApprovalCode()));
                item.setRetrivalReferenceData(getStringValue(tranItem.getRetrivalReferenceData()));
                item.setAppLabel(getStringValue(tranItem.getAppLabel()));
                item.setMerchantName(getStringValue(txn.getKiosk().getTerminalMerchantNameAndLocation()));
            }

            item.setTerminalVerificationResponse(tranItem.getTerminalVerificationResponse());
            item.setApplicationIdentifier(tranItem.getApplicationIdentifier());
            item.setTransactionCertificate(tranItem.getTransactionCertificate());

            item.setCardLabel(tranItem.getCardLabel());
            item.setCardNumber(tranItem.getCardNumber());

            // show signature or write signature
            if (tranItem.getSignatureRequired()) {
                item.setSignature(resizeBase64Image(tranItem.getSignature()));
                item.setCardHolderName(tranItem.getCardHolderName());
                item.setNoSignature("Sign x______________________________");
            } else {
                item.setSignature(DUMMY_SIGNATURE);
                item.setCardHolderName("");
                item.setNoSignature(NO_SIGNATURE);
            }

            kioskPaymentItem.add(item);
        }

        receipt.setKioskPaymentItems(kioskPaymentItem);

        TicketIssuanceStatus ticketIssuanceStatus = new TicketIssuanceStatus();

        if (KioskTransactionStatus.TICKET_NOT_ISSUED.getCode().equals(txn.getStatus()) || KioskTransactionStatus.TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode().equals(txn.getStatus())) {
            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_NOT_ISSUED);
            ticketIssuanceStatus.setTicketIssuranceMsg1(TICKET_ISSURANCE_NOT_ISSUED_MSG_1);
            ticketIssuanceStatus.setTicketIssuranceMsg2(TICKET_ISSURANCE_NOT_ISSUED_MSG_2);
            ticketIssuanceStatus.setTicketIssuranceMsg3(TICKET_ISSURANCE_NOT_ISSUED_MSG_3);
            ticketIssuanceStatus.setTicketIssuranceMsg4(TICKET_ISSURANCE_NOT_ISSUED_MSG_4);
        } else {
            ticketIssuanceStatus.setTicketIssuranceStatus(TICKET_ISSURANCE_COMPLETED);
            ticketIssuanceStatus.setTicketIssuranceMsg2(TICKET_ISSURANCE_COMPLETED_MSG_1);
            ticketIssuanceStatus.setTicketIssuranceMsg3(TICKET_ISSURANCE_COMPLETED_MSG_2);
        }

        receipt.setTicketIssuanceStatus(ticketIssuanceStatus);

        return receipt;
    }

    private String getStringValue(String string) {

        if (string == null || string.equals("null")) {
            return "";
        }
        return string;
    }

    private ReceiptItem createReceiptItem(KioskTransactionItemEntity tranItem, Integer i) {
        // only assign cms product name to first receipt item in same cms
        // product group
        ReceiptItem receiptItem = new ReceiptItem();
        receiptItem.setQty(tranItem.getQuantity());
        receiptItem.setAxProductName(getStringValue(tranItem.getAxProductName()));
        receiptItem.setAmount((NvxNumberUtils.formatToCurrency(Optional.ofNullable(tranItem.getAmount()).orElse(BigDecimal.ZERO), "$ ")));
        receiptItem.setSubtotalText(NvxNumberUtils.formatToCurrency(Optional.ofNullable(tranItem.getSubTotal()).orElse(BigDecimal.ZERO), "$ "));
        if (i == 0) {
            receiptItem.setProductName(tranItem.getProductName());
        }
        return receiptItem;
    }

    private String resizeBase64Image(String base64image) {
        BufferedImage bufImg = null;
        byte[] imageInByte = null;
        try {

            byte[] imgBytes = Base64.decodeBase64(base64image);

            bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
            bufImg = resizeImage(bufImg, bufImg.getType());

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufImg, "jpg", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();

        } catch (IOException s) {

        }
        return Base64.encodeBase64String(imageInByte);
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(originalImage.getWidth() / 5, originalImage.getHeight() / 5, type);
        Graphics2D g = resizedImage.createGraphics();

        g.drawImage(originalImage, 0, 0, originalImage.getWidth() / 5, originalImage.getHeight() / 5, null);
        g.dispose();
        return resizedImage;
    }

    private KioskInfoEntity getDeviceInfo(String channel) {
        KioskInfoEntity kioskInfo = defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(channel));
        return kioskInfo;
    }

    @Override
    public KioskEventEntity login(String channel) {
        KioskInfoEntity kioskInfo = this.getDeviceInfo(channel);
        KioskEventEntity event = null;
        if (kioskInfo != null && MgnlContext.getUser() != null) {
            event = new KioskEventEntity();
            event.setApiChannel(kioskInfo.getChannel());
            event.setCreatets(new Date());
            event.setDescription("");
            event.setDeviceNo(kioskInfo.getDeviceNumber());
            event.setEventName("LOGIN");
            event.setEventType(OnlineStoreKioskOpsController.OPERATION_MODE_EVENT_TYPE);
            event.setKioskName(kioskInfo.getJcrNodeName());
            event.setLogonuser(MgnlContext.getUser().getName());
            event.setStatus(OnlineStoreKioskOpsController.STATUS_ACTIVE);
            event.setValue("");
            event = kioskEventDao.saveAndFlush(event);
            syncService.publishEvent(event);
        }

        return event;
    }
    
    public void shudownOS() throws RuntimeException, IOException {
        String shutdownCommand;
        String operatingSystem = System.getProperty("os.name");
        if ("Windows 8".startsWith(operatingSystem)) {
            shutdownCommand = "shutdown.exe -s -t 5";
        } else {
            throw new RuntimeException("Unsupported operating system.");
        }
        Runtime.getRuntime().exec(shutdownCommand);
    }

    @Override
    public void shutdown(String channel, Boolean doSettlement) {
        try {
            KioskInfoEntity kiosk = defaultKioskManagementService.getKioskInfo(StoreApiChannels.fromCode(channel));
            if (doSettlement != null && doSettlement) {
                boolean status = this.processDeviceSettlement(paymentAdapterURL + "settlement", kiosk, channel);
                log.info("settlement status : " + status);
                if(status){
                    this.shudownOS();
                } else {
                    log.error("Settlement Failed: ");
                }
            } else {
                this.shudownOS();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
