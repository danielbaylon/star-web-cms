package com.enovax.star.cms.kiosk.api.store.service.payment;

import com.enovax.star.cms.kiosk.api.store.param.ExecutionResponse;

public interface IKioskPaymentService {
	
	  void savePaymentTransaction(String channelCode, ExecutionResponse executionResponse);
}
