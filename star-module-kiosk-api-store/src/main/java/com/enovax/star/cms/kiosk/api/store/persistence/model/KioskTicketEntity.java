package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_ticket", schema = "dbo")
public class KioskTicketEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oid")
    private Long id;

    @Column(name = "event_date")
    private Date eventDate;

    @Column(name = "event_group_id")
    private String eventGroupId;

    @Column(name = "event_line_id")
    private String eventLineId;

    @Column(name = "item_id")
    private String itemId;

    @Column(name = "qr_ticket_code")
    private String qrigTicketCode;

    @Column(name = "qty")
    private String quantity;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "code")
    private String ticketCode;

    @Column(name = "table_id")
    private String ticketTableId;

    @Column(name = "tran_date")
    private Date transDate;

    @Column(name = "updated_datetime")
    private Date modifiedDateTime;

    @Column(name = "create_datetime")
    private Date createdDateTime;

    @Column(name = "tran_id")
    private String transactionId;

    @Column(name = "usage_val_id")
    private String usageValidityId;

    @Column(name = "open_val_id")
    private String openValidityId;

    @Column(name = "cms_prod_name")
    private String productName;

    @Column(name = "ax_prod_name")
    private String axProductName;

    @Column(name = "receipt_id")
    private String receiptId;

    @Column(name = "shift_id")
    private String shiftId;

    @Column(name = "third_party")
    private Boolean thirdParty = Boolean.FALSE;

    @Column(name = "code_data")
    private String codeData;

    @Column(name = "token_data")
    private String tokenData;

    @Column(name = "barcode")
    private String barcodeBase64;

    @Column(name = "status")
    private String status;

    @Column(name = "ticket_no")
    private String ticketNumber;

    @Column(name = "template_name")
    private String templateName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_tran_oid")
    private KioskTransactionEntity tran;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQrigTicketCode() {
        return qrigTicketCode;
    }

    public void setQrigTicketCode(String qrigTicketCode) {
        this.qrigTicketCode = qrigTicketCode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getTicketTableId() {
        return ticketTableId;
    }

    public void setTicketTableId(String ticketTableId) {
        this.ticketTableId = ticketTableId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUsageValidityId() {
        return usageValidityId;
    }

    public void setUsageValidityId(String usageValidityId) {
        this.usageValidityId = usageValidityId;
    }

    public String getOpenValidityId() {
        return openValidityId;
    }

    public void setOpenValidityId(String openValidityId) {
        this.openValidityId = openValidityId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public Boolean getThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(Boolean thirdParty) {
        this.thirdParty = thirdParty;
    }

    public String getCodeData() {
        return codeData;
    }

    public void setCodeData(String codeData) {
        this.codeData = codeData;
    }

    public String getBarcodeBase64() {
        return barcodeBase64;
    }

    public void setBarcodeBase64(String barcodeBase64) {
        this.barcodeBase64 = barcodeBase64;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public KioskTransactionEntity getTran() {
        return tran;
    }

    public void setTran(KioskTransactionEntity tran) {
        this.tran = tran;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getAxProductName() {
        return axProductName;
    }

    public void setAxProductName(String axProductName) {
        this.axProductName = axProductName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTokenData() {
        return tokenData;
    }

    public void setTokenData(String tokenData) {
        this.tokenData = tokenData;
    }


}
