package com.enovax.star.cms.kiosk.api.store.opsmodel;

import com.enovax.star.cms.kiosk.api.store.opsmodel.PrinterDetail;
import com.enovax.star.cms.kiosk.api.store.opsmodel.PrinterError;

public class Result {
    private long lastError;
    private long errorCode;
    private long systemError;
    private PrinterDetail printerDetail;
    private PrinterError printerError;

    public long getLastError() {
        return lastError;
    }

    public void setLastError(long lastError) {
        this.lastError = lastError;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public long getSystemError() {
        return systemError;
    }

    public void setSystemError(long systemError) {
        this.systemError = systemError;
    }

    public PrinterDetail getPrinterDetail() {
        return printerDetail;
    }

    public void setPrinterDetail(PrinterDetail printerDetail) {
        this.printerDetail = printerDetail;
    }

    public PrinterError getPrinterError() {
        return printerError;
    }

    public void setPrinterError(PrinterError printerError) {
        this.printerError = printerError;
    }
}
