package com.enovax.star.cms.kiosk.api.store.service.transaction;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;

public interface IKioskTransactionService {

    public KioskTransactionEntity getTransactionByAxCartId(String axCartId);

    public KioskTransactionEntity getTranByAxReceiptNo(String axReceiptNo);

}
