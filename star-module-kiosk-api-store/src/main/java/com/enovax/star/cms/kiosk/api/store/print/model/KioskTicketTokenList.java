package com.enovax.star.cms.kiosk.api.store.print.model;

public class KioskTicketTokenList {

    private String printLabel;

    private String publishPrice;

    private String ticketType;

    private String priceType;

    private String productOwner;

    private String paxPerTicket;

    private String usageValidity;

    private String ticketValidity;

    private String ticketLineDesc;

    private String validityStartDate;

    private String validityEndDate;

    private String legalEntity;

    private String ticketNumber;

    private String ticketCode;

    private String productImage;

    private String itemNumber;

    private String customerName;

    private String displayName;

    private String description;

    private String productSearchName;

    private String productVarConfig;

    private String productVarSize;

    private String productVarColor;

    private String productVarStyle;

    private String packageItem;

    private String packageItemName;

    private String packagePrintLabel;

    private String mixMatchPackageName;

    private String mixMatchDescription;

    private String eventDate;

    private String eventName;

    private String facilities;

    private String facilityAction;

    private String operationIds;

    private String discountApplied;

    private String barcodePLU;

    private String barcodeStartDate;

    private String barcodeEndDate;

    private String termAndCondition;

    private String disclaimer;

    public String getPrintLabel() {
        return printLabel;
    }

    public void setPrintLabel(String printLabel) {
        this.printLabel = printLabel;
    }

    public String getPublishPrice() {
        return publishPrice;
    }

    public void setPublishPrice(String publishPrice) {
        this.publishPrice = publishPrice;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(String productOwner) {
        this.productOwner = productOwner;
    }

    public String getPaxPerTicket() {
        return paxPerTicket;
    }

    public void setPaxPerTicket(String paxPerTicket) {
        this.paxPerTicket = paxPerTicket;
    }

    public String getUsageValidity() {
        return usageValidity;
    }

    public void setUsageValidity(String usageValidity) {
        this.usageValidity = usageValidity;
    }

    public String getTicketValidity() {
        return ticketValidity;
    }

    public void setTicketValidity(String ticketValidity) {
        this.ticketValidity = ticketValidity;
    }

    public String getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(String validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public String getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(String validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductSearchName() {
        return productSearchName;
    }

    public void setProductSearchName(String productSearchName) {
        this.productSearchName = productSearchName;
    }

    public String getProductVarConfig() {
        return productVarConfig;
    }

    public void setProductVarConfig(String productVarConfig) {
        this.productVarConfig = productVarConfig;
    }

    public String getProductVarSize() {
        return productVarSize;
    }

    public void setProductVarSize(String productVarSize) {
        this.productVarSize = productVarSize;
    }

    public String getProductVarColor() {
        return productVarColor;
    }

    public void setProductVarColor(String productVarColor) {
        this.productVarColor = productVarColor;
    }

    public String getProductVarStyle() {
        return productVarStyle;
    }

    public void setProductVarStyle(String productVarStyle) {
        this.productVarStyle = productVarStyle;
    }

    public String getPackageItem() {
        return packageItem;
    }

    public void setPackageItem(String packageItem) {
        this.packageItem = packageItem;
    }

    public String getPackageItemName() {
        return packageItemName;
    }

    public void setPackageItemName(String packageItemName) {
        this.packageItemName = packageItemName;
    }

    public String getMixMatchPackageName() {
        return mixMatchPackageName;
    }

    public void setMixMatchPackageName(String mixMatchPackageName) {
        this.mixMatchPackageName = mixMatchPackageName;
    }

    public String getMixMatchDescription() {
        return mixMatchDescription;
    }

    public void setMixMatchDescription(String mixMatchDescription) {
        this.mixMatchDescription = mixMatchDescription;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getFacilityAction() {
        return facilityAction;
    }

    public void setFacilityAction(String facilityAction) {
        this.facilityAction = facilityAction;
    }

    public String getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(String operationIds) {
        this.operationIds = operationIds;
    }

    public String getDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(String discountApplied) {
        this.discountApplied = discountApplied;
    }

    public String getBarcodePLU() {
        return barcodePLU;
    }

    public void setBarcodePLU(String barcodePLU) {
        this.barcodePLU = barcodePLU;
    }

    public String getBarcodeStartDate() {
        return barcodeStartDate;
    }

    public void setBarcodeStartDate(String barcodeStartDate) {
        this.barcodeStartDate = barcodeStartDate;
    }

    public String getBarcodeEndDate() {
        return barcodeEndDate;
    }

    public void setBarcodeEndDate(String barcodeEndDate) {
        this.barcodeEndDate = barcodeEndDate;
    }

    public String getTermAndCondition() {
        return termAndCondition;
    }

    public void setTermAndCondition(String termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getTicketLineDesc() {
        return ticketLineDesc;
    }

    public void setTicketLineDesc(String ticketLineDesc) {
        this.ticketLineDesc = ticketLineDesc;
    }

    public String getPackagePrintLabel() {
        return packagePrintLabel;
    }

    public void setPackagePrintLabel(String packagePrintLabel) {
        this.packagePrintLabel = packagePrintLabel;
    }

}
