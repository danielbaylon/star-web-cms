package com.enovax.star.cms.kiosk.api.store.opsmodel;

import java.util.ArrayList;

/**
 * Created by tharaka on 28/09/2016.
 */
public class TransactionStatusResponse {

    private ArrayList<TransactionStatusData> statusDataArrayList;

    public ArrayList<TransactionStatusData> getStatusDataArrayList() {
        return statusDataArrayList;
    }

    public void setStatusDataArrayList(ArrayList<TransactionStatusData> statusDataArrayList) {
        this.statusDataArrayList = statusDataArrayList;
    }
}
