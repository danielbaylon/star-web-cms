package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

@Repository
public interface IKioskCustomEventDao extends JpaRepository<KioskLoadPaperTicketInfoEntity, Long> {
     
    @Query("FROM KioskLoadPaperTicketInfoEntity order by id desc")
    public List<KioskLoadPaperTicketInfoEntity> geTicketInfoEntityOrderByIdDesc();
}