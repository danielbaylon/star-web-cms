package com.enovax.star.cms.kiosk.api.store.persistence.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tharaka on 16/9/16.
 */

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_paper_ticket_info", schema = "dbo")
public class KioskLoadPaperTicketInfoEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "event_type")
    private String eventType;

    @Column(name = "device_no")
    private String deviceNo;

    @Column(name = "kiosk_name")
    private String kioskName;

    @Column(name = "createts")
    private Timestamp createts;

    @Column(name = "logonuser")
    private String logonuser;

    @Column(name = "api_channel")
    private String apiChannel;

    @Column(name = "qty_unloaded")
    private String qtyUnloaded;

    @Column(name = "qty_loaded")
    private String qtyLoaded;

    @Column(name = "start_ticket_no")
    private String startTicketNo;

    @Column(name = "end_ticket_no")
    private String endTicketNo;


    @OneToMany(mappedBy = "ticketInfoEntity" , cascade = CascadeType.ALL)
    private Set<KioskLoadPaperTicketQtyEntity> ticketQtyEntitySet = new HashSet<>(0);

    public KioskLoadPaperTicketInfoEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getKioskName() {
        return kioskName;
    }

    public void setKioskName(String kioskName) {
        this.kioskName = kioskName;
    }

    public Timestamp getCreatets() {
        return createts;
    }

    public void setCreatets(Timestamp createts) {
        this.createts = createts;
    }

    public String getLogonuser() {
        return logonuser;
    }

    public void setLogonuser(String logonuser) {
        this.logonuser = logonuser;
    }

    public Set<KioskLoadPaperTicketQtyEntity> getTicketQtyEntitySet() {
        return ticketQtyEntitySet;
    }

    public void setTicketQtyEntitySet(Set<KioskLoadPaperTicketQtyEntity> ticketQtyEntitySet) {
        this.ticketQtyEntitySet = ticketQtyEntitySet;
    }

    public String getQtyUnloaded() {
        return qtyUnloaded;
    }

    public void setQtyUnloaded(String qtyUnloaded) {
        this.qtyUnloaded = qtyUnloaded;
    }

    public String getQtyLoaded() {
        return qtyLoaded;
    }

    public void setQtyLoaded(String qtyLoaded) {
        this.qtyLoaded = qtyLoaded;
    }

    public String getStartTicketNo() {
        return startTicketNo;
    }

    public void setStartTicketNo(String startTicketNo) {
        this.startTicketNo = startTicketNo;
    }

    public String getEndTicketNo() {
        return endTicketNo;
    }

    public void setEndTicketNo(String endTicketNo) {
        this.endTicketNo = endTicketNo;
    }

    public String getApiChannel() {
        return apiChannel;
    }

    public void setApiChannel(String apiChannel) {
        this.apiChannel = apiChannel;
    }
}
