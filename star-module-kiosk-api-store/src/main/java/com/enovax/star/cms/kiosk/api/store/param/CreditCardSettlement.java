package com.enovax.star.cms.kiosk.api.store.param;

/**
 * @author danielbaylon
 *
 */
public class CreditCardSettlement{
	
    private String result;

    private String transaction;

    private String merchantId;

    private String terminalId;

    private String commandId;

    private String responseCode;

	private String dateTimeOfTransaction;

	private String customData2;

	private String customData3;

	private String ecrUniqueTraceNumber;
	
	private String rawData;

	public CreditCardSettlement() {
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getDateTimeOfTransaction() {
		return dateTimeOfTransaction;
	}

	public String getCustomData2() {
		return customData2;
	}

	public String getCustomData3() {
		return customData3;
	}

	public String getEcrUniqueTraceNumber() {
		return ecrUniqueTraceNumber;
	}

	public String getResult() {
		return result;
	}

	public String getTransaction() {
		return transaction;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public String getCommandId() {
		return commandId;
	}

	public String getRawData() {
		return rawData;
	}
	
	

}
