package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskAxSequence {

    SHIFT("7"), //
    TRANSACTION("8");

    private final String code;

    private KioskAxSequence(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
