package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionItemEntity;

@Repository
public interface IKioskTransactionItemDao extends JpaRepository<KioskTransactionItemEntity, Long> {

}
