package com.enovax.star.cms.kiosk.api.store.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.WebRequest;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.api.StoreApiConstants;
import com.enovax.star.cms.commons.mgnl.definition.AXProductProperties;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CheckoutDisplay;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.FunCart;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.KioskFunCart;
import com.enovax.star.cms.commons.model.booking.KioskFunCartRedemptionDisplayItem;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEmployee;
import com.enovax.star.cms.commons.model.kiosk.odata.AxShift;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.ProductExtViewModelWrapper;
import com.enovax.star.cms.commons.model.product.ProductItemCriteria;
import com.enovax.star.cms.commons.model.product.ProductItemCriteriaWrapper;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.commons.model.ticketgen.KioskTicketsPackage;
import com.enovax.star.cms.commons.model.ticketgen.TicketXmlRecord;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.cms.commons.util.barcode.BarcodeGenerator;
import com.enovax.star.cms.commons.util.barcode.QRCodeGenerator;
import com.enovax.star.cms.kiosk.api.store.config.StarModuleApiStoreKioskConfig;
import com.enovax.star.cms.kiosk.api.store.param.ExecutionResponse;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTicketEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.redemption.param.PINRedemptionParam;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTransactionType;
import com.enovax.star.cms.kiosk.api.store.service.arm.IKioskRoboticArmService;
import com.enovax.star.cms.kiosk.api.store.service.booking.IKioskBookingService;
import com.enovax.star.cms.kiosk.api.store.service.cmsproduct.IKioskProductService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.ops.zabbix.IKioskZabbixPaymentTerminalService;
import com.enovax.star.cms.kiosk.api.store.service.payment.IKioskPaymentService;
import com.enovax.star.cms.kiosk.api.store.service.recovery.IKioskRecoveryService;
import com.enovax.star.cms.kiosk.api.store.service.redemption.IKioskPINRedemptionService;
import com.enovax.star.cms.kiosk.api.store.service.ticketgen.IKioskTicketGenerationService;
import com.enovax.star.cms.kiosk.api.store.service.transaction.IKioskTransactionService;
import com.enovax.star.cms.templatingkit.mgnl.functions.StarKioskTemplatingFunctions;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
@RequestMapping("/store/")
public class OnlineStoreKioskController extends BaseStoreKioskApiController {

    @Value("${kiosk.api.payment.service.url://http://localhost:9100/}")
    private String paymentServiceURL;

    @Autowired
    private HttpSession session;

    @Autowired
    private IKioskBookingService kioskBookingService;

    @Autowired
    private IKioskPaymentService kioskPaymentService;

    @Autowired
    private IKioskTicketGenerationService ticketGenerationService;

    @Autowired
    private IKioskPINRedemptionService kioskPINRedemptionService;

    @Autowired
    private IKioskProductService productService;

    @Autowired
    private IKioskManagementService kioskManagementService;

    @Autowired
    private IKioskTransactionService tranService;

    @Autowired
    private StarModuleApiStoreKioskConfig starModuleApiStoreKioskConfig;

    @Autowired
    private IKioskRecoveryService kioskRecoveryService;
    
    @Autowired
    private IKioskRoboticArmService armService;

    @Autowired
    private StarKioskTemplatingFunctions kioskfn;
    
    @Autowired
    private IKioskZabbixPaymentTerminalService zabbixPaymentTerminalService;

    private Boolean logon(WebRequest request) {
        Boolean logon = Boolean.TRUE;

        String date = (String) request.getAttribute("lastLogon", WebRequest.SCOPE_SESSION);
        if (StringUtils.isNoneBlank(date)) {
            logon = DateTime.now().isAfter(DateTime.parse(date, ISODateTimeFormat.dateTime()).plusHours(6));
        } else {
            request.setAttribute("lastLogon", ISODateTimeFormat.dateTime().print(DateTime.now()), WebRequest.SCOPE_SESSION);
        }
        return logon;
    }

    private Boolean logonPaymentTerminal(StoreApiChannels channel) {
        Boolean logon = Boolean.FALSE;
        JsonObject result = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(headers);
            RestTemplate template = new RestTemplate();
            String logonResponse = template.postForObject(paymentServiceURL + "/payment/logon/", requestEntity, String.class);
            JsonParser parser = new JsonParser();
            result = (JsonObject) parser.parse(logonResponse);
            log.info(result.toString());
            if ("true".equals(result.get("success").getAsString())) {
                logon = Boolean.TRUE;
                String merchantInfo = result.getAsJsonObject("data").getAsJsonObject("details").get("netsMerchantNameLoc").getAsString();

                KioskInfoEntity kioskInfo = kioskManagementService.getKioskInfo(channel);
                kioskInfo.setTerminalMerchantNameAndLocation(merchantInfo);
                kioskManagementService.updateKioskInfo(kioskInfo);
            } else {
                zabbixPaymentTerminalService.sendNetsLogonError(null, channel.code, new String[]{"", "", result.toString()});
            }

        } catch (Exception e) {
            zabbixPaymentTerminalService.sendNetsLogonError(null, channel.code, new String[]{"", "", result.toString()});
            log.error(e.getMessage(), e);
        }
        return logon;
    }


    @CrossOrigin
    @RequestMapping(value = "echo", method = { RequestMethod.OPTIONS, RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskInfoEntity>> apiEcho(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, WebRequest request) {
        log.info("Entered apiEcho...");

        ApiResult<KioskInfoEntity> result = new ApiResult<>();
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            KioskInfoEntity kiosk = kioskManagementService.getKioskInfo(channel);
            result = new ApiResult<>(true, "", "", kiosk);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiEcho] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskInfoEntity.class, ""), HttpStatus.OK);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "logon-all", method = { RequestMethod.OPTIONS, RequestMethod.POST })
    public ResponseEntity<ApiResult<AxEmployee>> apiLogonAll(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, WebRequest request) {
        log.info("Entered apiLogon...");

        ApiResult<AxEmployee> result = new ApiResult<>();
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            result = kioskManagementService.logon(channel);

            result.setSuccess(logonPaymentTerminal(channel));

            result.setSuccess(armService.init(null, channel.code, ""));
            
            //TODO QR scanner/ticket printer/receipt printer
            
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiLogon] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxEmployee.class, ""), HttpStatus.OK);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "logon", method = { RequestMethod.OPTIONS, RequestMethod.POST })
    public ResponseEntity<ApiResult<AxEmployee>> apiLogon(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, WebRequest request) {
        log.info("Entered apiLogon...");

        ApiResult<AxEmployee> result = new ApiResult<>();
        try {
            // if (logon(request)) {
            // final StoreApiChannels channel =
            // StoreApiChannels.fromCode(channelCode);
            // result = kioskManagementService.logon(channel);
            // return new ResponseEntity<>(result, HttpStatus.OK);
            // } else {
            // result.setSuccess(true);
            // return new ResponseEntity<>(result, HttpStatus.OK);
            // }

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            result = kioskManagementService.logon(channel);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiLogon] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxEmployee.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "logoff", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiLogoff(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiLogoff...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<String> result = kioskManagementService.logoff(channel);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiLogoff] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "close-shift", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<AxShift>> apiCloseShift(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCloseShift...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final ApiResult<AxShift> result = kioskManagementService.closeShift(channel);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCloseShift] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, AxShift.class, ""), HttpStatus.OK);
        }
    }

    // TODO Major issue: AX cart merges the products together. Have to see how
    // to handle this.
    // TODO i18n functionality
    @RequestMapping(value = "products-ext", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<ProductExtViewModelWrapper>> apiGetProductsExt(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody ProductItemCriteriaWrapper data) {
        log.info("Entered apiGetProductsExt...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final List<String> itemIds = new ArrayList<>();
            for (ProductItemCriteria crit : data.getProducts()) {
                itemIds.add(crit.getItemId());
            }

            final ApiResult<List<ProductExtViewModel>> result = productService.getDataForProducts(channel, itemIds);
            if (result.isSuccess()) {
                return new ResponseEntity<>(new ApiResult<>(true, "", "", new ProductExtViewModelWrapper(result.getData())), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(false, result.getErrorCode(), result.getMessage(), null), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetProductsExt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ProductExtViewModelWrapper.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-to-cart", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiAddToCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @RequestBody FunCart cart) {
        log.info("Entered apiAddToCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<FunCartDisplay> result = kioskBookingService.cartAdd(channel, cart, sessionId);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-cart", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiUpdateCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @RequestBody KioskFunCart cart) {
        log.info("Entered apiUpdateCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<FunCartDisplay> result = kioskBookingService.updateShoppingCart(channel, cart, sessionId);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiUpdateCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "remove-item-from-cart/{id}", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiRemoveItemFromCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @PathVariable("id") String id) {
        log.info("Entered apiRemoveItemFromCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<FunCartDisplay> result = kioskBookingService.cartRemoveItem(channel, sessionId, id);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiRemoveItemFromCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "update-topup-to-cart", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiUpdateTopupToCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody KioskFunCart cart) {
        log.info("Entered apiUpdateTopupToCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<FunCartDisplay> result = kioskBookingService.updateCartTopup(channel, cart, sessionId);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiUpdateTopupToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "add-topup-to-cart", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiAddTopupToCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody KioskFunCart cart) {
        log.info("Entered apiAddTopupToCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<FunCartDisplay> result = kioskBookingService.cartAddTopup(channel, sessionId, cart);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddTopupToCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "apply-promo-code", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiApplyPromoCode(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode, @RequestBody String promoCode) {
        log.info("Entered apiApplyPromoCode...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final ApiResult<String> result = kioskBookingService.cartApplyPromoCode(channel, sessionId, promoCode);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiApplyPromoCode] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "clear-cart", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiClearCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiClearCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            kioskBookingService.cartClear(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiClearCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-cart", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiGetCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final FunCartDisplay funCartDisplay = kioskBookingService.constructCartDisplay(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", funCartDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-cart-by-receipt-number", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiGetCartByReceiptNumber(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody String receiptNumber) {

        // TODO Need to return a proper transaction formatted for display, or a
        // new API should return this
        log.info("Entered apiGetCartByReceiptNumber ...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            KioskTransactionEntity tran = kioskBookingService.getTransactionByReceipt(channel, receiptNumber);

            if (tran == null) {
                return new ResponseEntity<>(new ApiResult<>(ApiErrorCodes.NoKioskTransactionFound, "", ""), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", tran), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCartByReceiptNumber] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTransactionEntity.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-checkout-display", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<CheckoutDisplay>> apiGetCheckoutDisplay(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetCheckoutDisplay...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final CheckoutDisplay checkoutDisplay = kioskBookingService.constructCheckoutDisplayFromCart(channel, session);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", checkoutDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCheckoutDisplay] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, CheckoutDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "checkout", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiCheckout(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody CustomerDetails deets) {
        log.info("Entered apiCheckout...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ApiResult<KioskTransactionEntity> result = kioskBookingService.doCheckout(channel, session, deets);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCheckout] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTransactionEntity.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "start-checkout", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiStartCheckout(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody(required = false) String signature) {
        log.info("Entered apiStartCheckout...");
        try {
            ApiResult<KioskTransactionEntity> result = null;
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            KioskInfoEntity kiosk = this.kioskManagementService.getKioskInfo(channel);
            KioskTransactionEntity tran = tranService.getTransactionByAxCartId(kiosk.getCurrentCartId());
            if (tran == null || StringUtils.isBlank(tran.getRedemptionPinCode())) {
                result = kioskBookingService.startCartCheckout(channel, session, signature);
            } else {
                result = new ApiResult<>(true, "", "", tran);

            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCompleteSale] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTransactionEntity.class, ""), HttpStatus.OK);
        }
    }

    /**
     * @RequestMapping(value = "checkout-cancel", method = { RequestMethod.POST
     *                       }) public ResponseEntity<ApiResult<String>>
     *                       apiCheckoutCancel(
     * @RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String
     *                                                          channelCode) {
     *                                                          log.info("Entered
     *                                                          apiCheckoutCancel...");
     *                                                          try {
     * 
     *                                                          final
     *                                                          StoreApiChannels
     *                                                          channel =
     *                                                          StoreApiChannels.fromCode(channelCode);
     * 
     *                                                          final
     *                                                          ApiResult<String>
     *                                                          result =
     *                                                          kioskBookingService.cancelCheckout(channel,
     *                                                          session);
     * 
     *                                                          return new
     *                                                          ResponseEntity<>(result,
     *                                                          HttpStatus.OK);
     *                                                          } catch
     *                                                          (Exception e) {
     *                                                          log.error("!!!
     *                                                          System exception
     *                                                          encountered
     *                                                          [apiCheckoutCancel]
     *                                                          !!!"); return
     *                                                          new
     *                                                          ResponseEntity<>(handleUncaughtException(e,
     *                                                          String.class,
     *                                                          ""),
     *                                                          HttpStatus.OK);
     *                                                          } }
     **/

    @RequestMapping(value = "checkout-complete-sale", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiCompleteSale(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiCompleteSale...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            ApiResult<KioskTransactionEntity> result = null;

            KioskInfoEntity kiosk = this.kioskManagementService.getKioskInfo(channel);
            KioskTransactionEntity tran = tranService.getTransactionByAxCartId(kiosk.getCurrentCartId());
            if (StringUtils.isBlank(tran.getRedemptionPinCode())) {
                result = kioskBookingService.completeSale(channel, session);
            } else {

                result = this.kioskPINRedemptionService.completeSale(channel, session);

            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiCompleteSale] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTransactionEntity.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-receipt", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<ReceiptDisplay>> apiGetReceipt(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiGetReceipt...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final ReceiptDisplay receipt = kioskBookingService.getReceiptForDisplayBySession(channel, session);

            if (receipt == null) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "No transaction found", null), HttpStatus.OK);
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", receipt), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetReceipt] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, ReceiptDisplay.class, ""), HttpStatus.OK);
        }
    }

    private String getAxProductTicketType(StoreApiChannels channel, String itemId) {
        // empty string would cause error;
        String tickeType = " ";
        try {
            Node product = kioskfn.getAXProductByDisplayProductNumber(channel.code, itemId);
            if (product != null && product.hasProperty(AXProductProperties.TicketType.getPropertyName())) {
                tickeType = product.getProperty(AXProductProperties.TicketType.getPropertyName()).getString();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return tickeType;
    }

    @RequestMapping(value = "generate-paper-tickets-package-receipt/{channel}/{receiptNumber}", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperTicketsPackage(@PathVariable("channel") String channelCode,
            @PathVariable("receiptNumber") String receiptNumber) {
        log.info("Entered apiGeneratePaperTicketsPackage..." + channelCode);
        try {
            String format = "dd MMM yyyy";
            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final KioskTransactionEntity tran = kioskBookingService.getTransactionByReceipt(channel, receiptNumber);
            log.info("no of tickets for transaction " + tran.getId() + "is : " + tran.getTickets());
            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            List<ETicketData> ticketDataList = new ArrayList<>();
            for (KioskTicketEntity ticket : tran.getTickets()) {
                ETicketData data = new ETicketData();
                data.setDisplayName(ticket.getAxProductName());
                data.setEventDate(new DateTime(ticket.getEventDate()).toString(format));
                data.setEventSession(ticket.getEventLineId());
                data.setTicketPersonType(this.getAxProductTicketType(channel, ticket.getItemId()));
                data.setShortDisplayName(ticket.getAxProductName());
                data.setThirdParty(ticket.getThirdParty());
                data.setTicketNumber(ticket.getTicketCode());
                data.setValidityEndDate(new DateTime(ticket.getStartDate()).toString(format));
                data.setValidityStartDate(new DateTime(ticket.getEndDate()).toString(format));
                data.setTotalPrice(tran.getTotalAmount().toPlainString());
                data.setTicketTemplateName(ticket.getTemplateName());
                data.setCodeData(ticket.getCodeData());
                data.setTokenData(ticket.getTokenData());
                data.setBarcodeBase64(ticket.getBarcodeBase64());
                ticketDataList.add(data);
            }
            ticketDataCompiled.setTickets(ticketDataList);

            final Map<String, String> ticketXmlMap = ticketGenerationService.generatePaperTicketXml(ticketDataCompiled, "", "");

            final KioskTicketsPackage pkg = new KioskTicketsPackage();
            final List<TicketXmlRecord> ticketXmls = new ArrayList<>();
            for (Map.Entry<String, String> entry : ticketXmlMap.entrySet()) {
                final TicketXmlRecord rec = new TicketXmlRecord();
                rec.setId(entry.getKey());
                rec.setTemplateXml(entry.getValue());
                ticketXmls.add(rec);
            }
            pkg.setTicketXmls(ticketXmls);

            if (StringUtils.isNotBlank(tran.getRedemptionPinCode())) {
                pkg.setReceiptXml(ticketGenerationService.generateRedemptionReceiptXML(tran, "", channel));
            } else if (KioskTransactionType.RECOVERY.getCode().equalsIgnoreCase(tran.getType())) {
                pkg.setReceiptXml(ticketGenerationService.generateRecoveryReceiptXML(tran, "", channel));
            } else {
                pkg.setReceiptXml(ticketGenerationService.generateReceiptXml(tran, "", channel));
            }
            log.info(pkg.getReceiptXml());
            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGeneratePaperTicketsPackage] !!!" + e.getMessage(), e);
            return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "generate-paper-tickets-package/{channel}", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperTicketsPackage(@PathVariable("channel") String channelCode) {
        log.info("Entered apiGeneratePaperTicketsPackage...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final KioskTransactionEntity tran = kioskBookingService.getTransactionBySession(channel, session);

            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            // ticketDataCompiled.setTickets(tran.getTickets());

            final Map<String, String> ticketXmlMap = ticketGenerationService.generatePaperTicketXml(ticketDataCompiled, "", "");

            final KioskTicketsPackage pkg = new KioskTicketsPackage();
            final List<TicketXmlRecord> ticketXmls = new ArrayList<>();
            for (Map.Entry<String, String> entry : ticketXmlMap.entrySet()) {
                final TicketXmlRecord rec = new TicketXmlRecord();
                rec.setId(entry.getKey());
                rec.setTemplateXml(entry.getValue());
                ticketXmls.add(rec);
            }
            pkg.setTicketXmls(ticketXmls);

            // pkg.setReceiptXml(ticketGenerationService.generateReceiptXml(storeTransaction,
            // ""));

            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGeneratePaperTicketsPackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
        }
    }

    public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperReceiptWithQRCodePackage(String channelCode, String qrCode, String receiptNumber) {

        log.info("Entered apiGeneratePaperReceiptWithQRCodePackage...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            KioskTransactionEntity tran = this.kioskBookingService.getTransactionByReceipt(channel, receiptNumber);

            KioskTicketsPackage pkg = new KioskTicketsPackage();

            pkg.setReceiptXml(ticketGenerationService.generateReceiptXmlWithQRCode(tran, "", qrCode, channel));

            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGeneratePaperReceiptWithQRCodePackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
        }
    }

    public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperReceiptWithoutQRCodePackage(String channelCode, String qrCode, String receiptNumber) {

        log.info("Entered apiGeneratePaperReceiptWithoutQRCodePackage...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            KioskTransactionEntity tran = this.kioskBookingService.getTransactionByReceipt(channel, receiptNumber);

            KioskTicketsPackage pkg = new KioskTicketsPackage();

            if (tran.getType().equalsIgnoreCase(KioskTransactionType.RECOVERY.getCode())) {
                pkg.setReceiptXml(ticketGenerationService.generateRecoveryReceiptWithoutQRCode(tran, "", channel));
            } else {
                pkg.setReceiptXml(ticketGenerationService.generateReceiptXmlWithoutQRCode(tran, "", channel));
            }

            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);

        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGeneratePaperReceiptWithQRCodePackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
        }
    }

    public ResponseEntity<ApiResult<KioskTicketsPackage>> apiGeneratePaperReceiptPackage(String channelCode, String qrCode, String receiptNumber) {

        log.info("Entered apiGeneratePaperReceiptWithQRCodePackage...");

        try {
            StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            KioskTransactionEntity tran = this.kioskBookingService.getTransactionByReceipt(channel, receiptNumber);

            KioskTicketsPackage pkg = new KioskTicketsPackage();

            pkg.setReceiptXml(ticketGenerationService.generateReceiptXmlWithQRCode(tran, "", qrCode, channel));

            return new ResponseEntity<>(new ApiResult<>(true, "", "", pkg), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGeneratePaperReceiptWithQRCodePackage] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, KioskTicketsPackage.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "generate-ticket/{channel}", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<ApiResult<String>> apiGenerateTicket(@PathVariable("channel") String channelCode, final HttpServletResponse response) {
        log.info("Entered apiGenerateTicket...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

            final StoreTransaction storeTransaction = null;// kioskBookingService.getStoreTransactionBySession(channel,session);

            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(storeTransaction.geteTickets());

            final byte[] outputBytes = ticketGenerationService.generateTickets(ticketDataCompiled);

            final String fileName = "etickets.pdf";
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setHeader("X-Frame-Options", "SAMEORIGIN");

            ServletOutputStream output = response.getOutputStream();
            output.write(outputBytes);
            output.flush();

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGenerateTicket] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "generate-ticket-sample", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<ApiResult<String>> apiGenerateTicketSample(final HttpServletResponse response) {
        log.info("Entered apiGenerateTicketSample...");
        try {

            final List<ETicketData> tickets = new ArrayList<>();

            ETicketData td = new ETicketData();
            td.setBarcodeBase64(BarcodeGenerator.toBase64("helloworld", 250, 75));
            td.setDisplayName("Sample Name");
            td.setTicketNumber(ProjectUtils.generateReceiptNumber(new Date()));
            td.setTicketPersonType("Adult");
            td.setTotalPrice("S$ 10.00");
            td.setValidityStartDate("29/03/2016");
            td.setValidityEndDate("29/03/2016");
            tickets.add(td);

            td = new ETicketData();
            td.setBarcodeBase64(QRCodeGenerator.toBase64("helloworld qr", 200, 200));
            td.setDisplayName("Sample Name QR");
            td.setTicketNumber(ProjectUtils.generateReceiptNumber(new Date()));
            td.setTicketPersonType("Adult");
            td.setTotalPrice("S$ 10.00");
            td.setValidityStartDate("29/03/2016");
            td.setValidityEndDate("29/03/2016");
            tickets.add(td);

            final ETicketDataCompiled ticketDataCompiled = new ETicketDataCompiled();
            ticketDataCompiled.setTickets(tickets);

            final byte[] outputBytes = ticketGenerationService.generateTickets(ticketDataCompiled);

            final String fileName = "sentosa-tickets.pdf";
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setHeader("X-Frame-Options", "SAMEORIGIN");

            ServletOutputStream output = response.getOutputStream();
            output.write(outputBytes);
            output.flush();

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGenerateTicketSample] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "get-config", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<StarModuleApiStoreKioskConfig>> getAdapterURL() {
        log.info("Get Adapter URL");
        try {
            return new ResponseEntity<>(new ApiResult<>(true, "", "", starModuleApiStoreKioskConfig), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [getAdapterURL] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, StarModuleApiStoreKioskConfig.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "redemption/start", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> startRedemption(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody PINRedemptionParam param) {
        log.info("Entered startRedemption...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
        final String sessionId = session.getId();

        final ApiResult<KioskTransactionEntity> result = kioskPINRedemptionService.startRedemption(channel, sessionId, param.getPinCode());

        log.info("startRedemption Result: " + result);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "redemption/checkout", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> checkoutRedeption(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody List<KioskFunCartRedemptionDisplayItem> items) {
        log.info("Entered checkoutRedeption...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
        final String sessionId = session.getId();

        final ApiResult<KioskTransactionEntity> result = kioskPINRedemptionService.checkout(channel, sessionId, items);

        log.info("checkoutRedeption Result: " + result);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "redemption/get-cart", method = { RequestMethod.GET })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiRedemptionGetCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiRedemptionGetCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final FunCartDisplay funCartDisplay = kioskPINRedemptionService.constructRedeemCartDisplay(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", funCartDisplay), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "redemption/unlock-pin", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> unlockRedemptionPin(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered unlockRedemptionPin...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
        final ApiResult<KioskTransactionEntity> result = kioskPINRedemptionService.unlockPin(channel, session);

        log.info("unlockRedemptionPin Result: " + result);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "recovery/start", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiStartRecovery(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody String axReceiptNumber) {
        log.info("Entered apiStartRecovery...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

        final ApiResult<KioskTransactionEntity> result = kioskRecoveryService.startRecoveryFlowTransaction(channel, session, axReceiptNumber);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "recovery/get-transaction", method = { RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiGetRecoveryTransaction(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody String axReceiptNumber) {
        log.info("Entered apiStartRecovery...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

        final ApiResult<KioskTransactionEntity> result = kioskRecoveryService.getTransactionByAxReceiptNumber(channel, axReceiptNumber);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "recovery/get-cart", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<FunCartDisplay>> apiRecoveryGetCart(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode) {
        log.info("Entered apiRecoveryGetCart...");
        try {

            final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);
            final String sessionId = session.getId();

            final FunCartDisplay funCartDisplayRecovery = kioskRecoveryService.constructRecoveryCartDisplayDetails(channel, sessionId);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", funCartDisplayRecovery), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiGetCart] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, FunCartDisplay.class, ""), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "recovery/reprint", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<KioskTransactionEntity>> apiRecoveryReprint(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody String receiptNumber) {
        log.info("Entered apiRecoveryReprint...");
        final StoreApiChannels channel = StoreApiChannels.fromCode(channelCode);

        final ApiResult<KioskTransactionEntity> result = kioskRecoveryService.cartTicketReprintStart(channel, session, receiptNumber);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "add-payment", method = { RequestMethod.GET, RequestMethod.POST })
    public ResponseEntity<ApiResult<String>> apiAddPayment(@RequestHeader(StoreApiConstants.REQUEST_HEADER_CHANNEL) String channelCode,
            @RequestBody ExecutionResponse executionResponse) {
        log.info("Entered apiAddTransaction...");
        try {

            kioskPaymentService.savePaymentTransaction(channelCode, executionResponse);

            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            log.error("!!! System exception encountered [apiAddPayment] !!!");
            return new ResponseEntity<>(handleUncaughtException(e, String.class, ""), HttpStatus.OK);
        }
    }

}