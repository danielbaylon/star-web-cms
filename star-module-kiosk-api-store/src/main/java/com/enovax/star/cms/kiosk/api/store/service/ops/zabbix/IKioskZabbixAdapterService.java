package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;


public interface IKioskZabbixAdapterService {
    
   public void sendRequest(KioskZabbixEventEntity entity, String channel);
   
   public KioskZabbixEventEntity constructEvent(String eventItem, KioskInfoEntity kiosk, boolean isFullParam, String...messageParams);
    
}