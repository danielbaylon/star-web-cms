package com.enovax.star.cms.kiosk.api.store.opsmodel;

/**
 * Created by Win_PC on 26/10/2016.
 */
public class ScheduledMaintenance {

    private boolean status;
    private long numberOfSeconds;

    public ScheduledMaintenance() {
    }

    public ScheduledMaintenance(boolean status, long numberOfSeconds) {
        this.status = status;
        this.numberOfSeconds = numberOfSeconds;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getNumberOfSeconds() {
        return numberOfSeconds;
    }

    public void setNumberOfSeconds(long numberOfSeconds) {
        this.numberOfSeconds = numberOfSeconds;
    }
}
