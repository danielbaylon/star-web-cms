package com.enovax.star.cms.kiosk.api.store.transformer.inventtable;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.*;
import com.enovax.star.cms.commons.ws.axretail.inventtable.*;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class KioskProductExtResponseTransformer {

    public static ApiResult<List<AxRetailProductExtData>> fromWs(SDCInventTableResponse wsResponse) {
        if (wsResponse == null) {
            return new ApiResult<>();
        }

        final ArrayOfResponseError arrayOfResponseError = wsResponse.getErrors().getValue();
        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), null);
        }

        final ArrayOfSDCInventTableExt arrayOfSDCInventTableExt = wsResponse.getInventTableExtCollection().getValue();
        if (arrayOfSDCInventTableExt == null) {
            return new ApiResult<>(true, "", "Successful but no records returned.", new ArrayList<>());
        }

        final List<SDCInventTableExt> wsProducts = arrayOfSDCInventTableExt.getSDCInventTableExt();
        final List<AxRetailProductExtData> prods = new ArrayList<>();
        for (SDCInventTableExt wsProduct : wsProducts) {
            final AxRetailProductExtData prod = new AxRetailProductExtData();

            prod.setAbsoluteCapacityId(wsProduct.getAbsoluteCapacityId().getValue());
            prod.setAccessId(wsProduct.getAccessId().getValue());
            prod.setCapacity(wsProduct.getIsCapacity() == 1);
            prod.setDataAreaId(wsProduct.getDataAreaId().getValue());
            prod.setDefaultEventLineId(wsProduct.getDefaultEventLineId().getValue());
            prod.setDefineOpenDate(wsProduct.getDefineOpenDate() == 1);
            prod.setEventGroupId(wsProduct.getEventGroupId().getValue());
            prod.setFacilityId(wsProduct.getFacilityId().getValue());
            prod.setItemId(wsProduct.getItemId().getValue());
            prod.setMediaTypeDescription(wsProduct.getMediaTypeDescription().getValue());
            prod.setMediaTypeId(wsProduct.getMediaTypeId().getValue());
            prod.setMember(wsProduct.getNecIsMember() == 1);
            prod.setMembershipTypeCode(wsProduct.getNecMembershipTypeCode().getValue());
            prod.setNonReturnable(wsProduct.getNonReturnable() == 1);

            final XMLGregorianCalendar wsProductOpenEndDateTime = wsProduct.getOpenEndDateTime();
            prod.setOpenEndDateTime(wsProductOpenEndDateTime == null ? null : wsProductOpenEndDateTime.toGregorianCalendar().getTime());


            final XMLGregorianCalendar wsProductOpenStartDateTime = wsProduct.getOpenStartDateTime();
            prod.setOpenStartDateTime(wsProductOpenStartDateTime == null ? null : wsProductOpenStartDateTime.toGregorianCalendar().getTime());

            prod.setOpenValidityRuleId(wsProduct.getOpenValidityRuleId().getValue());
            prod.setOperationId(wsProduct.getOperationId().getValue());
            prod.setOwnAttraction(wsProduct.getOwnAttraction() == 1);
            prod.setPackage(wsProduct.getIsPackage() == 1);
            prod.setPackageId(wsProduct.getPackageId().getValue());
            prod.setPrinterType(wsProduct.getPrinterType());
            prod.setPrinting(wsProduct.getPrinting());
            prod.setProductId(wsProduct.getRecId().toString());
            prod.setProductName(wsProduct.getProductName().getValue());
            prod.setRecId(wsProduct.getRecId().toString());
            prod.setTemplateDescription(wsProduct.getTemplateDescription().getValue());
            prod.setTemplateName(wsProduct.getTemplateName().getValue());
            prod.setTicketing(wsProduct.getIsTicketing() == 1);
            prod.setTransport(wsProduct.getIsTransport() == 1);
            prod.setUsageValidityRuleId(wsProduct.getUsageValidityRuleId().getValue());

            final List<EventGroupTableEntity> wsEventGroups = wsProduct.getEventGroupTableCollection().getValue().getEventGroupTableEntity();
            final List<AxRetailProductExtEventGroup> eventGroups = new ArrayList<>();
            for (EventGroupTableEntity wsEventGroup : wsEventGroups) {
                final AxRetailProductExtEventGroup eventGroup = new AxRetailProductExtEventGroup();
                eventGroup.setCapacityCalendarId(wsEventGroup.getCapacityCalendarId().getValue());
                eventGroup.setDataAreaId(wsEventGroup.getDataAreaID().getValue());

                final XMLGregorianCalendar wsEventGroupEventDate = wsEventGroup.getEventDate();
                eventGroup.setEventDate(wsEventGroupEventDate == null ? null : wsEventGroupEventDate.toGregorianCalendar().getTime());

                eventGroup.setEventGroupId(wsEventGroup.getEventGroupId().getValue());
                eventGroup.setRecId(wsEventGroup.getRecID().toString());
                eventGroup.setRecVersion(wsEventGroup.getRecVersion().toString());
                eventGroup.setSingleEvent(wsEventGroup.getIsSingleEvent() == 1);

                final List<EventGroupLineEntity> wsGroupLines = wsEventGroup.getEventGroupLinesCollection().getValue().getEventGroupLineEntity();
                final List<AxRetailProductExtEventGroupLine> groupLines = new ArrayList<>();
                for (EventGroupLineEntity wsGroupLine : wsGroupLines) {
                    final AxRetailProductExtEventGroupLine groupLine = new AxRetailProductExtEventGroupLine();
                    groupLine.setDataAreaId(wsGroupLine.getDataAreaID().getValue());
                    groupLine.setEventCapacityId(wsGroupLine.getEventCapacityId().getValue());
                    groupLine.setEventEndTime(wsGroupLine.getEventEndTime());
                    groupLine.setEventGroupId(wsGroupLine.getEventGroupId().getValue());
                    groupLine.setEventLineId(wsGroupLine.getEventLineId().getValue());
                    groupLine.setEventName(wsGroupLine.getEventName().getValue());
                    groupLine.setEventStartTime(wsGroupLine.getEventStartTime());
                    groupLine.setRecId(wsGroupLine.getRecID().toString());
                    groupLine.setRecVersion(wsGroupLine.getRecVersion().toString());

                    final List<EventAllocation> wsEventAllocations = wsGroupLine.getEventAllocationCollection().getValue().getEventAllocation();
                    final List<AxRetailProductExtEventAllocation> eventAllocations = new ArrayList<>();
                    for (EventAllocation wsEventAllocation : wsEventAllocations) {
                        final AxRetailProductExtEventAllocation allocation = new AxRetailProductExtEventAllocation();
                        allocation.setEventGroupId(wsEventAllocation.getEventGroupId().getValue());
                        allocation.setEventLineId(wsEventAllocation.getEventLineId().getValue());

                        final XMLGregorianCalendar wsEventAllocationEventDate = wsEventAllocation.getEventDate();
                        allocation.setEventDate(wsEventAllocationEventDate == null ? null : wsEventAllocationEventDate.toGregorianCalendar().getTime());

                        eventAllocations.add(allocation);
                    }
                    groupLine.setEventAllocations(eventAllocations);

                    groupLines.add(groupLine);
                }
                eventGroup.setEventGroupLines(groupLines);

                eventGroups.add(eventGroup);
            }
            prod.setEventGroups(eventGroups);

            final List<PackageLine> wsPackageLines = wsProduct.getPackageLinesCollection().getValue().getPackageLine();
            final List<AxRetailProductExtPackageLine> packageLines = new ArrayList<>();
            for (PackageLine wsPackageLine : wsPackageLines) {
                final AxRetailProductExtPackageLine packageLine = new AxRetailProductExtPackageLine();
                packageLine.setAbsoluteCapacityId(wsPackageLine.getAbsoluteCapacityId().getValue());
                packageLine.setAllowRevisit(wsPackageLine.getAllowRevisit() == 1);
                packageLine.setCost(wsPackageLine.getCost());

                final XMLGregorianCalendar wsPackageLineCreatedDateTime = wsPackageLine.getCreatedDateTime();
                packageLine.setCreatedDate(wsPackageLineCreatedDateTime == null ? null : wsPackageLineCreatedDateTime.toGregorianCalendar().getTime());

                packageLine.setDataAreaId(wsPackageLine.getDataAreaId().getValue());
                packageLine.setDescription(wsPackageLine.getDescription().getValue());
                packageLine.setEntryWeight(wsPackageLine.getEntryWeight());
                packageLine.setItemId(wsPackageLine.getItemId().getValue());
                packageLine.setLineGroup(wsPackageLine.getLineGroup().getValue());
                packageLine.setLineNumber(wsPackageLine.getLineNum());

                final XMLGregorianCalendar wsPackageLineModifiedDateTime = wsPackageLine.getModifiedDateTime();
                packageLine.setModifiedDate(wsPackageLineModifiedDateTime == null ? null : wsPackageLineModifiedDateTime.toGregorianCalendar().getTime());

                packageLine.setPackageId(wsPackageLine.getPackageId().getValue());
                packageLine.setPrintingBehavior(wsPackageLine.getPrintingBehavior());
                packageLine.setRecId(wsPackageLine.getRecId().toString());
                packageLine.setRetailVariantId(wsPackageLine.getRetailVariantId().getValue());
                packageLine.setRevenue(wsPackageLine.getRevenue());

                packageLines.add(packageLine);
            }
            prod.setPackageLines(packageLines);

            wsProduct.getInventTableVariantExtCollection().getValue();
            prod.setProductVariants(new ArrayList<>()); //TODO

            prods.add(prod);
        }

        return new ApiResult<>(true, "", "", prods);
    }

}
