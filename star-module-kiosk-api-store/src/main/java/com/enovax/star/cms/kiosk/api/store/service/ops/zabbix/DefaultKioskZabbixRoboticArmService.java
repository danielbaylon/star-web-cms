package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;

@Service
public class DefaultKioskZabbixRoboticArmService implements IKioskZabbixRoboticArmService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskZabbixRoboticArmService.class);

    @Autowired
    private IKioskZabbixAdapterService kioskZabbixAdapter;

    @Override
    public void sendRoboticArmReleaseError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("robotic.arm.release.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

    @Override
    public void sendRoboticArmRejectError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("robotic.arm.reject.error", kioskInfoEntity, false, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

}
