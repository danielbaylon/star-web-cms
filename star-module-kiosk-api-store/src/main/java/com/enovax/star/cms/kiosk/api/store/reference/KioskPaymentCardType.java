
package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskPaymentCardType {

		VISA("V", "VISA"), 
		MASTERCARD("M", "MASTERCARD"), 
		AMERICANEXPRESS("A", "AMERICAN EXPRESS"), 
		JCB("J", "JCB"), 
		DINERS("D", "DINERS"), 
		CUP("C", "CUP"), 
		EZLINK("E", "EZ-LINK"), 
		STAFFDISCOUNT("F", "STAFF DISCOUNT"), 
		NETSPINDEBIT("S", "NETS PIN DEBIT"), 
		NFP("N", "NETS FLASHPAY (NFP)"); 

	String value;
	String description;

	KioskPaymentCardType(String value, String description) {
		this.value = value;
		this.description = description;
	}

	public String getValue() {

		return value;
	}

	public String getDescription() {

		return description;
	}

	public static KioskPaymentCardType toEnum(String value) {

		if (VISA.getValue().equals(value))
			return VISA;
		if (MASTERCARD.getValue().equals(value))
			return MASTERCARD;
		if (AMERICANEXPRESS.getValue().equals(value))
			return AMERICANEXPRESS;
		if (JCB.getValue().equals(value))
			return JCB;
		if (DINERS.getValue().equals(value))
			return DINERS;
		if (CUP.getValue().equals(value))
			return CUP;
		if (EZLINK.getValue().equals(value))
			return EZLINK;
		if (STAFFDISCOUNT.getValue().equals(value))
			return STAFFDISCOUNT;
		if (NETSPINDEBIT.getValue().equals(value))
			return NETSPINDEBIT;
		if (NFP.getValue().equals(value))
			return NFP;
		return null;
	}

}
