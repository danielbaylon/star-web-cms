package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskZabbixEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskCustomEventDao;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionDao;
import com.enovax.star.cms.kiosk.api.store.reference.KioskTicketPrintStatus;

@Service
public class DefaultKioskZabbixPrinterService implements IKioskZabbixPrinterService {

    private static final Logger log = LoggerFactory.getLogger(DefaultKioskZabbixPrinterService.class);

    @Autowired
    private IKioskCustomEventDao kioskCustoEventDao;

    @Autowired
    private IKioskTransactionDao kioskTransactionDao;
    
    @Autowired
    private IKioskZabbixAdapterService kioskZabbixAdapter;

    //Post Check of Ticket Printer Status
    @Override
    public void checkTicketPrinterStatus(KioskInfoEntity kioskInfoEntity, String channel) {
        KioskZabbixEventEntity event = null;

        KioskLoadPaperTicketInfoEntity entity = this.getPaperTicketEntityInfo();

        long count = this.getTicketPrintedCount(entity);
        
        if((Long.parseLong(entity.getEndTicketNo()) - count) <= 20){
            event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.very.few", kioskInfoEntity, true, new String[]{"20","RFID/Paper ticket"});
        }

        /*if (count == Long.parseLong(entity.getEndTicketNo())) {
            //event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.run.out", kioskInfoEntity, true);
        } else if ((Long.parseLong(entity.getEndTicketNo()) - count) <= 20) {
            event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.very.few", kioskInfoEntity, true);
        } else if ((Long.parseLong(entity.getEndTicketNo()) - count) <= 10) {
            //event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.paper.near.end", kioskInfoEntity, true);
        }*/

        if (null != event) {
            kioskZabbixAdapter.sendRequest(event, channel);
        }

    }

    //Pre Check of Ticket Printer Status
    @Override
    public void checkTicketPaperAvailability(int ticketSize, KioskInfoEntity kioskInfoEntity, String channel) {
        KioskZabbixEventEntity event = null;

        KioskLoadPaperTicketInfoEntity entity = this.getPaperTicketEntityInfo();

        long count = this.getTicketPrintedCount(entity);

        if (count == Long.parseLong(entity.getEndTicketNo())) {
            event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.paper.end", kioskInfoEntity, true);
        } else if ((Long.parseLong(entity.getEndTicketNo()) - count) <= 20) {
            event = this.kioskZabbixAdapter.constructEvent("kiosk.print.ticket.running.low", kioskInfoEntity, true);
        }

        if (null != event) {
            kioskZabbixAdapter.sendRequest(event, channel);
        }

    }
    
    @Override
    public void sendTicketAndReceiptError(KioskInfoEntity kioskInfoEntity, String channel, String...params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("ticket.not.issued.no.receipt.failed", kioskInfoEntity, true, params);
        if (null != event) {
            kioskZabbixAdapter.sendRequest(event, channel);
        } else {
            log.error("No event triggered. Encountered error during sending.");
        }
    }
    
   

    private long getTicketPrintedCount(KioskLoadPaperTicketInfoEntity entity) {
        long count = 0;
        try {
            List<KioskTransactionEntity> transactions = kioskTransactionDao.getTransactionByEndDate(this.formatStringToDate(this.getDateStringFormat(entity.getCreatets())));
            for (KioskTransactionEntity transaction : transactions) {
                count = transaction.getTickets().stream().filter(ticket -> ticket.getStatus().equals(KioskTicketPrintStatus.PRINTED.getCode())).count() + count;
                count = transaction.getTickets().stream().filter(ticket -> ticket.getStatus().equals(KioskTicketPrintStatus.BLACKLISTED.getCode())).count() + count;
            }

        } catch (ParseException e) {
            log.error("Parsing Exception : ", e);
        }
        return count;
    }

    private KioskLoadPaperTicketInfoEntity getPaperTicketEntityInfo() {
        return kioskCustoEventDao.geTicketInfoEntityOrderByIdDesc().stream().findFirst().get();
    }

    private String getDateStringFormat(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return format.format(date);
    }

    private Date formatStringToDate(String inputDate) throws ParseException {
        return NvxDateUtils.parseAxDate(inputDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME_MS);
    }

    
    @Override
    public void sendGenericErrorRequest(KioskInfoEntity kioskInfoEntity, String channel) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("kiosk.print.error", kioskInfoEntity, true);
        kioskZabbixAdapter.sendRequest(event, channel);
    }
    
    @Override
    public void sendTicketError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("ticket.issuance.error", kioskInfoEntity, true, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }
    
    @Override
    public void sendReceiptError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("receipt.issuance.error", kioskInfoEntity, true, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }
    
    @Override
    public void sendTicketRedemptionError(KioskInfoEntity kioskInfoEntity, String channel, String... params) {
        KioskZabbixEventEntity event = this.kioskZabbixAdapter.constructEvent("redemption.ticket.printing.failed", kioskInfoEntity, true, params);
        kioskZabbixAdapter.sendRequest(event, channel);
    }

}
