package com.enovax.star.cms.kiosk.api.store.service.ticketgen;

import java.io.IOException;
import java.util.Map;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;

public interface IKioskTicketGenerationService {

    byte[] generateTickets(ETicketDataCompiled compiledTicketData) throws IOException;

    String generateReceiptXmlWithQRCode(KioskTransactionEntity txn, String inputReceiptTemplateXml, String qrCode, StoreApiChannels channel) throws IOException;
    String generateReceiptXml(KioskTransactionEntity txn, String inputReceiptTemplateXml, StoreApiChannels channel) throws IOException;

    Map<String, String> generatePaperTicketXml(
            ETicketDataCompiled compiledTicketData,
            String inputTemplateXmlQr, String inputTemplateXmlBarcode) throws IOException;
    
    String generateRedemptionReceiptXML(KioskTransactionEntity txn, final String inputTemplateXml, StoreApiChannels channel) throws IOException;
    
    String generateRecoveryReceiptXML(KioskTransactionEntity txn, final String inputTemplateXml, StoreApiChannels channel) throws IOException;
    
    String generateRecoveryReceiptWithoutQRCode(KioskTransactionEntity txn, final String inputTemplateXml, StoreApiChannels channel) throws IOException;
    
    String generateReceiptXmlWithoutQRCode(KioskTransactionEntity txn, String inputReceiptTemplateXml, StoreApiChannels channel) throws IOException;
}
