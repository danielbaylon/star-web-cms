package com.enovax.star.cms.kiosk.api.store.service.cmsproduct;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.CrossSellCartItem;
import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.kiosk.api.store.service.product.KioskServiceException;

import java.util.List;

public interface IKioskProductCmsService {

    ApiResult<List<CrossSellCartItem>> getAllCrossSellCartItemsFor(FunCartDisplay kioskTypeCart, String channelCode, String productNodeName, String locale) throws  KioskServiceException;
}