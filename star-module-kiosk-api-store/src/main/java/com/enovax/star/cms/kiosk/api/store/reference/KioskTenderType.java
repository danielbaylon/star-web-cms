package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskTenderType {

    DBSH("DBSH", "59"), //
    DBS("DBS", "58"), //
    AMEX("AMEX", "51"), //
    DINER("DINER", "55"), //
    CUP("CUP", "52"), //
    JCB("JCB", "53"), //
    NFP("NFP", "57"), // nets flash pay
    NETS("NETS", "56");

    private final String code;

    private final String value;

    private KioskTenderType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return this.code;
    }

    public String getValue() {
        return this.value;
    }

    public static KioskTenderType getTenderType(String code) {
        KioskTenderType tenderType = null;
        if (DBSH.getCode().equals(code)) {
            tenderType = DBSH;
        } else if (DBS.getCode().equals(code)) {
            tenderType = DBS;
        } else if (AMEX.getCode().equals(code)) {
            tenderType = AMEX;
        } else if (DINER.getCode().equals(code)) {
            tenderType = DINER;
        } else if (CUP.getCode().equals(code)) {
            tenderType = CUP;
        } else if (JCB.getCode().equals(code)) {
            tenderType = JCB;
        } else if (NFP.getCode().equals(code)) {
            tenderType = NFP;
        } else if (NETS.getCode().equals(code)) {
            tenderType = NETS;
        }
        return tenderType;
    }

}
