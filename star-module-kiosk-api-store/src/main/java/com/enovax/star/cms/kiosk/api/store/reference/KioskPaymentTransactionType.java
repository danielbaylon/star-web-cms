package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskPaymentTransactionType {

    SALES("1"), //
    VOID("2"), //
    REVERSAL("3"), //
    REFUND("4"), //
    SETTLEMENT_NETS("11"), //
    SETTLEMENT_CREDIT("12");

    private String code;

    KioskPaymentTransactionType(String code) {
        this.code = code;

    }

    public String getCode() {
        return this.code;
    }
}
