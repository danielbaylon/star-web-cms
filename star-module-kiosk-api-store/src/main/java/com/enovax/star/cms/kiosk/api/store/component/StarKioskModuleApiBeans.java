package com.enovax.star.cms.kiosk.api.store.component;


import com.enovax.star.cms.kiosk.api.store.service.cmsproduct.IKioskProductService;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;

/**
 * Created by jennylynsze on 7/20/16.
 */
public class StarKioskModuleApiBeans {


    public StarKioskModuleApiBeans() {
    }

    public IKioskProductService getProductService() {
        return AppContextManager.getAppContext().getBean(IKioskProductService.class);
    }
    
    public IKioskManagementService getKioskManagementService() {
        return AppContextManager.getAppContext().getBean(IKioskManagementService.class);
    }
}

