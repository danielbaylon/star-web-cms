package com.enovax.star.cms.kiosk.api.store.service.payment;

import java.io.IOException;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.kiosk.api.store.param.CreditCardContactlessSale;
import com.enovax.star.cms.kiosk.api.store.param.CreditCardSale;
import com.enovax.star.cms.kiosk.api.store.param.ExecutionResponse;
import com.enovax.star.cms.kiosk.api.store.param.NetsContactlessSale;
import com.enovax.star.cms.kiosk.api.store.param.NetsSale;
import com.enovax.star.cms.kiosk.api.store.param.TransactionType;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionPaymentEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.repository.IKioskTransactionPaymentDao;
import com.enovax.star.cms.kiosk.api.store.reference.KioskPaymentTransactionType;
import com.enovax.star.cms.kiosk.api.store.service.booking.IKioskBookingService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DefaultKioskPaymentService implements IKioskPaymentService {
    
    protected final Logger log = LoggerFactory.getLogger(DefaultKioskPaymentService.class);

    @Autowired
    private IKioskTransactionPaymentDao transactionPaymentDao;

    @Autowired
    private IKioskBookingService kioskBookingService;

    @Override
    public void savePaymentTransaction(String channelCode, ExecutionResponse response) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            KioskTransactionPaymentEntity payment = new KioskTransactionPaymentEntity();
            KioskTransactionEntity tran = getTransaction(channelCode, response.getReceipt());
            payment.setPaymentData(response.getDetails().toString());
            payment.setCallbackData(response.getCallbacks().toString());
            payment.setCardTypeDescription(response.getCardTypeDescription());
            payment.setPaymentMode(response.getCardTypeValue());
            payment.setTerminalId(response.getTerminalId());
            payment.setMerchantId(response.getMerchantId());
            payment.setTransactionStart(response.getTransactionStart());
            payment.setTransactionEnd(response.getTransactionEnd());

            if (TransactionType.NETSSALE.getCode().equalsIgnoreCase(response.getType())) {
                NetsSale sale = mapper.readValue(response.getDetails().toString(), NetsSale.class);
                payment.setType(KioskPaymentTransactionType.SALES.getCode());
                payment.setAmount(tran.getTotalAmount());
                payment.setRetrivalReferenceData(sale.getRetrievalReferenceNumber());
                payment.setEntryMode(sale.getEntryModeValue());
                payment.setHostLabel(sale.getHostLabel());
                payment.setCardTypeDescription(sale.getCardTypeDescription());
                payment.setCardLabel(sale.getCardLabel());
                payment.setResponseCode(sale.getResponseCode());
                payment.setStatus("00".equals(payment.getResponseCode()) ? Boolean.TRUE : Boolean.FALSE);
                payment.setApprovalCode(sale.getApprovalCode());
                payment.setNetsReceiptNumber(sale.getNetsReceiptNumber());
                payment.setNetsTraceNumber(sale.getNetsSystemTrace());
                payment.setRawData(sale.getRawData());

            } else if (TransactionType.CCSALE.getCode().equalsIgnoreCase(response.getType())) {
                CreditCardSale sale = mapper.readValue(response.getDetails().toString(), CreditCardSale.class);
                payment.setType(KioskPaymentTransactionType.SALES.getCode());
                payment.setAmount(tran.getTotalAmount());
                payment.setBatchNumber(sale.getBatchNumber());
                payment.setInvoiceNumber(sale.getInvoiceNumber());
                payment.setRetrivalReferenceData(sale.getRetrievalReferenceNumber());
                payment.setEntryMode(sale.getEntryModeValue());
                payment.setHostLabel(sale.getHostLabel());
                payment.setTerminalVerificationResponse(sale.getEmvDataTVR());
                payment.setApplicationIdentifier(sale.getEmvDataAID());
                payment.setAppLabel(sale.getEmvDataAppLabel());
                payment.setTransactionCertificate(sale.getEmvDataTC());
                payment.setCardTypeDescription(sale.getCardTypeDescription());
                payment.setCardLabel(sale.getCardLabel());
                payment.setCardNumber(sale.getCardNumber());
                payment.setResponseCode(sale.getResponseCode());
                payment.setStatus("00".equals(payment.getResponseCode()) ? Boolean.TRUE : Boolean.FALSE);
                payment.setApprovalCode(sale.getApprovalCode());
                payment.setCardHolderName(sale.getCardHolderName());
                payment.setRawData(sale.getRawData());
            } else if (TransactionType.NETSCONTACTLESSSALE.getCode().equalsIgnoreCase(response.getType())) {
                NetsContactlessSale sale = mapper.readValue(response.getDetails().toString(), NetsContactlessSale.class);
                payment.setType(KioskPaymentTransactionType.SALES.getCode());
                payment.setAmount(tran.getTotalAmount());
                payment.setBatchNumber(sale.getBatchNumber());
                payment.setEntryMode(sale.getEntryModeValue());
                payment.setHostLabel(sale.getHostLabel());
                payment.setCardTypeDescription(sale.getCardTypeDescription());
                payment.setCardLabel(sale.getCardLabel());
                payment.setCardNumber(sale.getCardNumber());
                payment.setResponseCode(sale.getResponseCode());
                payment.setStatus("00".equals(payment.getResponseCode()) ? Boolean.TRUE : Boolean.FALSE);
                payment.setApprovalCode(sale.getApprovalCode());
                payment.setOriginalBalance(new BigDecimal(sale.getTransactionNetsOriginalBalance()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
                payment.setRemainingBalance(new BigDecimal(sale.getTransactionNetsNewBalance()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
                payment.setRawData(sale.getRawData());

            } else if (TransactionType.CCCONTACTLESSSALE.getCode().equalsIgnoreCase(response.getType())) {
                CreditCardContactlessSale sale = mapper.readValue(response.getDetails().toString(), CreditCardContactlessSale.class);
                payment.setType(KioskPaymentTransactionType.SALES.getCode());
                payment.setAmount(tran.getTotalAmount());
                payment.setBatchNumber(sale.getBatchNumber());
                payment.setInvoiceNumber(sale.getInvoiceNumber());
                payment.setRetrivalReferenceData(sale.getRetrievalReferenceNumber());
                payment.setEntryMode(sale.getEntryModeValue());
                payment.setHostLabel(sale.getHostLabel());
                payment.setTerminalVerificationResponse(sale.getEmvDataTVR());
                payment.setApplicationIdentifier(sale.getEmvDataAID());
                payment.setAppLabel(sale.getEmvDataAppLabel());
                payment.setTransactionCertificate(sale.getEmvDataTC());
                payment.setCardTypeDescription(sale.getCardTypeDescription());
                payment.setCardLabel(sale.getCardLabel());
                payment.setCardNumber(sale.getCardNumber());
                payment.setResponseCode(sale.getResponseCode());
                payment.setStatus("00".equals(payment.getResponseCode()) ? Boolean.TRUE : Boolean.FALSE);
                payment.setApprovalCode(sale.getApprovalCode());
                payment.setCardHolderName(sale.getCardHolderName());
                payment.setRawData(sale.getRawData());
            }

            payment.setTran(tran);
            transactionPaymentDao.save(payment);
            
        } catch (JsonParseException e) {
            log.error("JSONParseExeception [savePaymentTransaction]: ", e);
        } catch (JsonMappingException e) {
            log.error("JsonMappingException [savePaymentTransaction]: ", e);
        } catch (IOException e) {
            log.error("IOException [savePaymentTransaction]: ", e);
        }
       
    }

    private KioskTransactionEntity getTransaction(String channelCode, String receipt) {
        return kioskBookingService.getTransactionByReceipt(StoreApiChannels.fromCode(channelCode), receipt);
    }

}
