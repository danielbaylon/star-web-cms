package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskCardEntryMode {

    MANUALENTRY("E", "MANUAL ENTRY"), //

    MAGNETICSTRIPE("M", "MAGNETIC STRIPE"), //
    FALLBACKMAGNETICSTRIPE("F", "FALLBACK MAGNETIC STRIPE"), //

    CHIP("C", "CHIP (EMV CONTACT)"), //

    CONTACTLESS("P", "CONTACTLESS (PAYPASS/PAYWAVE/EXPRESSPAY)"), //

    NETS("NETS", "NETS"), //

    NETS_FLASHPAY("NFC", "NETS FLASHPAY");

    String value;
    String description;

    KioskCardEntryMode(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public static KioskCardEntryMode toEnum(String value) {
        KioskCardEntryMode entryMode = null;
        if (MANUALENTRY.getValue().equals(value)) {
            entryMode = MANUALENTRY;
        } else if (MAGNETICSTRIPE.getValue().equals(value)) {
            entryMode = MAGNETICSTRIPE;
        } else if (FALLBACKMAGNETICSTRIPE.getValue().equals(value)) {
            entryMode = FALLBACKMAGNETICSTRIPE;
        } else if (CHIP.getValue().equals(value)) {
            entryMode = CHIP;
        } else if (CONTACTLESS.getValue().equals(value)) {
            entryMode = CONTACTLESS;
        } else if (NETS.getValue().equals(value)) {
            entryMode = NETS;
        } else if (NETS_FLASHPAY.getValue().equals(value)) {
            entryMode = NETS_FLASHPAY;
        }
        return entryMode;
    }
}
