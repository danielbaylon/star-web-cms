package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskTicketPrintStatus {

    INIT("0"), //
    PRINTED("1"), //
    NOTPRINTED("2"), //
    BLACKLISTED("3");

    private String code;

    KioskTicketPrintStatus(String code) {
        this.code = code;

    }

    public String getCode() {
        return this.code;
    }
}
