package com.enovax.star.cms.kiosk.api.store.repository.cart;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.FunCart;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SessionKioskCartManager implements IKioskCartManager {

    private Map<StoreApiChannels, Map<String, FunCart>> carts = new ConcurrentHashMap<>();

    //TODO no stale cart cleanup mechanism

    @PostConstruct
    public void init() {
        this.carts.put(StoreApiChannels.B2C_MFLG, new ConcurrentHashMap<>(50, 0.75f));
        this.carts.put(StoreApiChannels.B2C_SLM, new ConcurrentHashMap<>(50, 0.75f));
        this.carts.put(StoreApiChannels.PARTNER_PORTAL_SLM, new ConcurrentHashMap<>(50, 0.75f));
        this.carts.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new ConcurrentHashMap<>(50, 0.75f));
        this.carts.put(StoreApiChannels.KIOSK_SLM, new ConcurrentHashMap<>(50, 0.75f));
        this.carts.put(StoreApiChannels.KIOSK_MFLG, new ConcurrentHashMap<>(50, 0.75f));
    }

    @Override
    public void saveCart(StoreApiChannels channel, FunCart cartToSave) {
        final Map<String, FunCart> cartsForChannel = this.carts.get(channel);
        cartsForChannel.put(cartToSave.getCartId(), cartToSave);
    }

    @Override
    public FunCart getCart(StoreApiChannels channel, String cartId) {
        return this.carts.get(channel).get(cartId);
    }

    @Override
    public void removeCart(StoreApiChannels channel, String cartId) {
        this.carts.get(channel).remove(cartId);
    }
}
