package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;

public class BaseExecution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2243972600587335371L;

	protected Object result;

	protected Transaction transaction;

	protected String merchantId;

	protected String terminalId;

	protected int commandId;

	public BaseExecution() {
	}

	public String getMerchantId() {
		return merchantId;
	}

	public Object getResult() {
		return result;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public int getCommandId() {
		return commandId;
	}
}
