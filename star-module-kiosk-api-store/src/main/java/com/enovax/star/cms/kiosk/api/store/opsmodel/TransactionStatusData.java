package com.enovax.star.cms.kiosk.api.store.opsmodel;

/**
 * Created by tharaka on 28/09/2016.
 */
public class TransactionStatusData {
    private String transId;
    private String status;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
