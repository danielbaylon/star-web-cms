package com.enovax.star.cms.kiosk.api.store.param;

import java.io.Serializable;

public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4150273399910293504L;

	private boolean result;

	private String approvalCode;

	private String cardTypeDescription;

	private String cardTypeValue;

	private String terminalId;

	private String merchantId;

	private String transactionStart;

	private String transactionEnd;
	
	private String type;

	public Transaction() {
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public String getCardTypeDescription() {
		return cardTypeDescription;
	}

	public void setCardTypeDescription(String cardTypeDescription) {
		this.cardTypeDescription = cardTypeDescription;
	}

	public String getCardTypeValue() {
		return cardTypeValue;
	}

	public void setCardTypeValue(String cardTypeValue) {
		this.cardTypeValue = cardTypeValue;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTransactionStart() {
		return transactionStart;
	}

	public void setTransactionStart(String transactionStart) {
		this.transactionStart = transactionStart;
	}

	public String getTransactionEnd() {
		return transactionEnd;
	}

	public void setTransactionEnd(String transactionEnd) {
		this.transactionEnd = transactionEnd;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "[ " + " " + "result: " + result + ", " + "approvalCode: " + approvalCode + ", " + "cardTypeValue: "
				+ cardTypeValue + ", " + "cardTypeDescription: " + cardTypeDescription + ", " + "merchant: "
				+ merchantId + ", " + "terminal: " + terminalId + ", " + "baseExecution: " + " ]";
	}
}
