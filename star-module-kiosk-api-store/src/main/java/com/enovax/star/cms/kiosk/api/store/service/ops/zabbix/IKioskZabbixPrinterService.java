package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskZabbixPrinterService {

    public void checkTicketPrinterStatus(KioskInfoEntity kioskInfoEntity, String channel);
    
    public void checkTicketPaperAvailability(int ticketSize, KioskInfoEntity kioskInfoEntity, String channel);
    
    public void sendGenericErrorRequest(KioskInfoEntity kioskInfoEntity, String channel);
    
    public void sendTicketAndReceiptError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendTicketError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendReceiptError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendTicketRedemptionError(KioskInfoEntity kioskInfoEntity, String channel, String...params);

}