package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskZabbixPaymentTerminalService {

    public void sendNetsLogonError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
}