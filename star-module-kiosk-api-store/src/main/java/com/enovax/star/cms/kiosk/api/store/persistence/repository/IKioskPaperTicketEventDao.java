package com.enovax.star.cms.kiosk.api.store.persistence.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;

@Repository
public interface IKioskPaperTicketEventDao extends JpaRepository<KioskLoadPaperTicketInfoEntity, Long> {

    @Query("SELECT o FROM KioskLoadPaperTicketInfoEntity o WHERE (o.createts between :beginDate and :endDate) and o.apiChannel = :channel and o.kioskName = :kioskId ")
    public List<KioskLoadPaperTicketInfoEntity> getPaperTicketEvent(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate, @Param("channel") String channel,
            @Param("kioskId") String kioskId);

}