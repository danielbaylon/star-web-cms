package com.enovax.star.cms.kiosk.api.store.param;

public enum TransactionType {

	NETSSALE("1", "NetsSale"), 
	NETSCONTACTLESSSALE("2", "NetsContactLessSale"), 
	CCSALE("3",	"CCSale"), 
	CCCONTACTLESSSALE("4", "CCContactlessSale"), 
	SETTLEMENT_NETS("5", "Settlement"),
	SETTLEMENT_CREDITCARD("56", "Settlement"),
	NETSLOGON("7", "Logon"),
	CCSALEVOID("8", "CCSaleVoid"),
	STATUSCHECK("9", "StatusCheck");
	
	
	private final String code;
	public final String message;

	private TransactionType(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return this.code;
	}

	public String getMessage() {
		return message;
	}
}
