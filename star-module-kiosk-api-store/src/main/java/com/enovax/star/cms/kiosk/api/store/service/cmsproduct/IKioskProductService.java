package com.enovax.star.cms.kiosk.api.store.service.cmsproduct;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.product.AxProductLinkPackage;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkPackage;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskProductService {

    ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels channel, List<String> productIds);

    ApiResult<AxProductLinkPackage> getValidAxProducts(StoreApiChannels channel, KioskInfoEntity kioskInfo);

    ApiResult<AxProductRelatedLinkPackage> getValidRelatedAxProducts(StoreApiChannels channel, KioskInfoEntity kioskInfo);

    ApiResult<PromotionLinkPackage> getFullDiscountData(StoreApiChannels channel, Map<Long, BigDecimal> mainProductPriceMap);

    ApiResult<CrossSellLinkPackage> getFullUpCrossSellData(StoreApiChannels channel) throws AxChannelException;
}
