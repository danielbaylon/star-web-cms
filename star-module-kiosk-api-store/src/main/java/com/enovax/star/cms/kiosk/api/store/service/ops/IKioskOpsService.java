package com.enovax.star.cms.kiosk.api.store.service.ops;

import java.util.List;

import com.enovax.star.cms.commons.model.ticketgen.ReceiptMain;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskEventEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskLoadPaperTicketInfoEntity;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskTransactionEntity;
import com.google.gson.JsonObject;

public interface IKioskOpsService {

    KioskEventEntity login(String channel);

    void resumeOperationMode(KioskEventEntity kioskEventEntity);

    void downOperationMode(KioskEventEntity kioskEventEntity);

    boolean getOperationMode(String channel);

    boolean addOtherInfo(KioskEventEntity kioskEventEntity);

    boolean addServiceInfo(KioskEventEntity kioskEventEntity);

    boolean addQtyRejectedPaperInfo(KioskEventEntity kioskEventEntity);

    boolean addLoadPaperTicketInfo(KioskLoadPaperTicketInfoEntity paperTicketInfoEntity);

    boolean addTransactionStatus(KioskEventEntity kioskEventEntity);

    boolean addReceiptHistoryStatus(KioskEventEntity kioskEventEntity);

    boolean addHardwareStatus(KioskEventEntity kioskEventEntity);

    String getSettlementInfo(String channel, KioskInfoEntity kioskinfo);

    //boolean addSettlementW(KioskEventEntity kioskEventEntity);

    List<KioskTransactionEntity> getTransactionStatus(String startDate, String endDate, String tranId, String pinCode, String apiChannel);

    //
    List<KioskTransactionEntity> getReceiptDetails(String startDate, String endDate, String apiChannel);

    //
    KioskTransactionEntity getReceiptDetailsByTranId(String trandId, String channel);

    //
    ReceiptMain getCompanyReceiptDetails(String channel);

    //
    ReceiptMain getTransactionDetails(String transactionId, String channel);

    void addResponseSettlementData(JsonObject result, KioskInfoEntity deviceInfo);

    boolean processDeviceSettlement(String url, KioskInfoEntity kioskInfoEntity, String apiChannel);

    List<KioskLoadPaperTicketInfoEntity> getPaperTicketLoadEvent(String channel, String date, String kioskId);

    void shutdown(String channel, Boolean doSettlement);
}