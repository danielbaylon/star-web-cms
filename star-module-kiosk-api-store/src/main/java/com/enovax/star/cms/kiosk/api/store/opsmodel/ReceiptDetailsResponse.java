package com.enovax.star.cms.kiosk.api.store.opsmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tharaka on 28/09/2016.
 */
public class ReceiptDetailsResponse {

    private List<ReceiptHistoryData> statusDataArrayList = new ArrayList<>();

    public List<ReceiptHistoryData> getStatusDataArrayList() {
        return statusDataArrayList;
    }

    public void setStatusDataArrayList(List<ReceiptHistoryData> statusDataArrayList) {
        this.statusDataArrayList = statusDataArrayList;
    }
}
