package com.enovax.star.cms.kiosk.api.store.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "STAR_DB_KIOSK", name = "kiosk_zabbix_event", schema = "dbo")
public class KioskZabbixEventEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8707237519534267085L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oid")
    private Long id;

    @Column(name = "host_name")
    private String hostName;

    @Column(name = "event_id")
    private String eventId;

    @Column(name = "category")
    private String category;

    @Column(name = "event")
    private String event;

    @Column(name = "message")
    private String message;

    @Column(name = "request")
    private String request;

    @Column(name = "response")
    private String response;

    @Column(name = "request_datetime")
    private Timestamp requestDateTime;

    @Column(name = "response_datetime")
    private Timestamp responseDateTime;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Timestamp getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(Timestamp requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public Timestamp getResponseDateTime() {
        return responseDateTime;
    }

    public void setResponseDateTime(Timestamp responseDateTime) {
        this.responseDateTime = responseDateTime;
    }

}
