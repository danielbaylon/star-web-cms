package com.enovax.star.cms.kiosk.api.store.service.ops.zabbix;

import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;

public interface IKioskZabbixRoboticArmService {

    public void sendRoboticArmReleaseError(KioskInfoEntity kioskInfoEntity, String channel, String...params);
    
    public void sendRoboticArmRejectError(KioskInfoEntity kioskInfoEntity, String channel, String...params);

}