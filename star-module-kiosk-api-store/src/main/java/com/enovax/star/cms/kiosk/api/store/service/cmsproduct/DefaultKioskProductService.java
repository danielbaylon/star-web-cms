package com.enovax.star.cms.kiosk.api.store.service.cmsproduct;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enovax.star.cms.commons.constant.AxProductPropertiesKey;
import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellLine;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellProduct;
import com.enovax.star.cms.commons.model.axstar.AxStarAffiliation;
import com.enovax.star.cms.commons.model.axstar.AxStarPriceGroup;
import com.enovax.star.cms.commons.model.axstar.AxStarProduct;
import com.enovax.star.cms.commons.model.axstar.AxStarProductCatalog;
import com.enovax.star.cms.commons.model.axstar.AxStarRetailDiscount;
import com.enovax.star.cms.commons.model.axstar.AxStarRetailDiscountDiscountCode;
import com.enovax.star.cms.commons.model.axstar.AxStarRetailDiscountLine;
import com.enovax.star.cms.commons.model.axstar.AxStarRetailDiscountMultibuyQuantityTier;
import com.enovax.star.cms.commons.model.axstar.AxStarRetailDiscountPriceGroup;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProduct;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProductProperty;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProductPropertyTranslation;
import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellLineEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetUpCrossSellRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchProductExtRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetProductsResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetUpCrossSellResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchProductExtResponse;
import com.enovax.star.cms.commons.model.product.AxMainProduct;
import com.enovax.star.cms.commons.model.product.AxProductLinkPackage;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.KioskProductExtViewModel;
import com.enovax.star.cms.commons.model.product.ProductExtDataCacheObject;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkMainProduct;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkRelatedProduct;
import com.enovax.star.cms.commons.model.product.promotions.ProductRelatedPromotion;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkMainProduct;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkMainPromotion;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkPackage;
import com.enovax.star.cms.commons.model.product.promotions.PromotionRelatedAffiliation;
import com.enovax.star.cms.commons.model.product.promotions.PromotionRelatedDiscountCode;
import com.enovax.star.cms.commons.model.product.promotions.PromotionRelatedProduct;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelProductsService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.service.axcrt.kiosk.AxStarKioskService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.kiosk.api.store.persistence.model.KioskInfoEntity;
import com.enovax.star.cms.kiosk.api.store.service.management.IKioskManagementService;
import com.enovax.star.cms.kiosk.api.store.service.product.KioskUpCrossSellService;
import com.google.gson.reflect.TypeToken;

import info.magnolia.jcr.util.PropertyUtil;

@Service
public class DefaultKioskProductService implements IKioskProductService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxStarKioskService axStarKioskService;

    @Autowired
    private AxStarService axStarService;

    @Autowired
    private AxChannelProductsService axChannelProductsService;

    @Autowired
    private IKioskManagementService kioskManagementServcie;

    @Autowired
    private KioskUpCrossSellService kioskUpCrossSellService;

    public static final long CACHE_MINS = 30L;

    private Map<StoreApiChannels, ProductExtDataCacheObject> productExtDataCache = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        productExtDataCache.put(StoreApiChannels.B2C_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.B2C_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new ProductExtDataCacheObject());
    }

    // public boolean updateProductExtDataCache(StoreApiChannels channel) throws
    // KioskServiceException {
    // final LocalDateTime now = LocalDateTime.now();
    // final ProductExtDataCacheObject channelCacheObject =
    // productExtDataCache.get(channel);
    // final LocalDateTime cacheTs = channelCacheObject.getCacheTs();
    // final boolean shouldUpdate = cacheTs == null ||
    // cacheTs.plusMinutes(CACHE_MINS).isBefore(now);
    //
    // if (shouldUpdate) {
    // final ApiResult<List<AxRetailProductExtData>> result =
    // kioskProductExtService.apiSearchProductExt(channel, true, "", 0L);
    // if (!result.isSuccess()) {
    // log.error("Error updating product ext data cache for " + channel + ".
    // Result: " + JsonUtil.jsonify(result));
    // return false;
    // }
    // final Map<String, ProductExtViewModel> vmMap =
    // channelCacheObject.getViewModelMap();
    // vmMap.clear();
    // final Map<String, AxRetailProductExtData> dataMap =
    // channelCacheObject.getDataMap();
    // dataMap.clear();
    // for (AxRetailProductExtData data : result.getData()) {
    // dataMap.put(data.getProductId(), data);
    // vmMap.put(data.getProductId(), ProductExtViewModel.fromExtData(data));
    // }
    // channelCacheObject.setCacheTs(LocalDateTime.now());
    //
    // log.info("Updated product ext data cache.");
    // return true;
    // }
    //
    // log.info("No update required.");
    // return true;
    // }

    @Override
    public ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels channel, List<String> itemIdList) {

        ApiResult<List<ProductExtViewModel>> apiResult = new ApiResult<>(ApiErrorCodes.General);
        final List<ProductExtViewModel> viewModels = new ArrayList<>();

        for (String itemId : itemIdList) {

            AxInventTableExtCriteria axInventTableExtCriteria = new AxInventTableExtCriteria();
            axInventTableExtCriteria.setItemId(itemId);

            KioskSearchProductExtRequest kioskSearchProductExtRequest = new KioskSearchProductExtRequest();
            kioskSearchProductExtRequest.setAxInventTableExtCriteria(axInventTableExtCriteria);

            AxStarServiceResult<KioskSearchProductExtResponse> searchProductExtResult = this.axStarKioskService.apiKioskSearchProductExt(channel, kioskSearchProductExtRequest);

            if (searchProductExtResult.isSuccess()) {
                List<AxInventTableExtEntity> axInventTableExtEntityList = searchProductExtResult.getData().getAxInventTableExtEntityList();
                Integer axItems = axInventTableExtEntityList.size();
                if (axItems == 0) {
                    log.error("no product found with product item ids : " + itemIdList);
                } else if (axInventTableExtEntityList.size() > 0) {
                    for (AxInventTableExtEntity prod : axInventTableExtEntityList) {
                        viewModels.add(KioskProductExtViewModel.fromExtData(prod));
                    }
                    apiResult = new ApiResult<>(true, "", "", viewModels);
                }
            }
        }

        return apiResult;
    }

    private Boolean logon(StoreApiChannels channel, KioskInfoEntity kioskInfo) {
        Boolean logon = Boolean.TRUE;
        try {
            logon = kioskManagementServcie.noneTransactionalLogon(channel, kioskInfo);
            // kioskInfo = kioskManagementServcie.openShift(channel, kioskInfo);
        } catch (Exception e) {
            logon = Boolean.FALSE;
            log.error(e.getMessage(), e);
        }
        return logon;
    }

    @Override
    public ApiResult<AxProductRelatedLinkPackage> getValidRelatedAxProducts(StoreApiChannels channel, KioskInfoEntity kioskInfo) {

        ApiResult<AxProductRelatedLinkPackage> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            AxProductRelatedLinkPackage pkg = new AxProductRelatedLinkPackage();
            pkg.setChannel(channel);
            if (this.logon(channel, kioskInfo)) {

                ApiResult<AxProductLinkPackage> axProductResult = getValidAxProducts(channel, kioskInfo);

                if (!axProductResult.isSuccess()) {
                    log.error("Error in getting the Products...");
                    apiResult.setErrorCode(axProductResult.getErrorCode());
                    apiResult.setMessage(axProductResult.getMessage());
                }
                AxProductLinkPackage axProductData = axProductResult.getData();
                pkg.setAxProductLinkPackage(axProductData);

                Map<Long, BigDecimal> mainProductPriceMap = new HashMap<>();

                for (AxMainProduct mainProduct : axProductData.getMainProductList()) {
                    mainProductPriceMap.put(mainProduct.getRecordId(), mainProduct.getPrice());
                }

                ApiResult<PromotionLinkPackage> promotionResult = getFullDiscountData(channel, mainProductPriceMap);

                if (!promotionResult.isSuccess()) {
                    log.error("Error in getting the Promotions...");
                    return new ApiResult<>(false, promotionResult.getErrorCode(), promotionResult.getMessage(), null);
                }

                PromotionLinkPackage promotionData = promotionResult.getData();
                pkg.setPromotionLinkPackage(promotionData);

                ApiResult<CrossSellLinkPackage> crossSellResult = getFullUpCrossSellData(channel);

                if (!crossSellResult.isSuccess()) {
                    return new ApiResult<>(false, crossSellResult.getErrorCode(), crossSellResult.getMessage(), null);
                }

                pkg.setCrossSellLinkPackage(crossSellResult.getData());
                /**
                 * CrossSellLinkPackage crossSell = new CrossSellLinkPackage();
                 * crossSell.setAxRetailUpCrossSellProductResultJson("");
                 * crossSell.setSuccess(Boolean.TRUE);
                 * pkg.setCrossSellLinkPackage(crossSell);
                 **/
                apiResult = new ApiResult<>(true, "", "", pkg);
                kioskManagementServcie.noneTransactionalLogoff(channel);
            } else {
                apiResult.setErrorCode("ERR_KIOSK_LOGON");
                apiResult.setMessage("kiosk logon failed");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return apiResult;
    }

    public ApiResult<AxProductLinkPackage> getValidAxProducts(StoreApiChannels channel, KioskInfoEntity kioskInfo) {

        ApiResult<AxProductLinkPackage> apiResult = new ApiResult<>(ApiErrorCodes.General);
        try {
            AxProductLinkPackage pkg = new AxProductLinkPackage();
            pkg.setChannel(channel);
            AxStarServiceResult<KioskGetProductsResponse> kioskGetProductsResult = axStarKioskService.apiKioskGetProducts(channel);
            if (!kioskGetProductsResult.isSuccess()) {
                log.error("Error in getting the Products...");
                apiResult = new ApiResult<>(false, "ERR_PRODUCT_DATA_RETRIEVAL", "Error retrieving the product data.", null);
            }
            AxStarServiceResult<Map<String, AxStarProductCatalog>> productCategoryResult = this.axStarService.apiProductCatalogGet(channel);
            Map<String, AxStarProductCatalog> productCatalogMap = new HashMap<>();

            if (productCategoryResult.isSuccess()) {
                productCatalogMap = productCategoryResult.getData();
            }
            List<AxProduct> axProductsData = kioskGetProductsResult.getData().getProductList();
            List<AxMainProduct> axMainProductList = new ArrayList<>();

            // Get the key name from CMS CONFIG
            String ticketTypeKey;
            try {
                Node ticketTypeKeyNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), AxProductPropertiesKey.TICKET_TYPE_KEY_JCR_PATH);
                ticketTypeKey = PropertyUtil.getString(ticketTypeKeyNode, "value");
            } catch (RepositoryException e) {
                ticketTypeKey = AxProductPropertiesKey.TICKET_TYPE; // default
                                                                    // if
                                                                    // nothing
                                                                    // is set
                log.error("No cms config found for path: " + AxProductPropertiesKey.TICKET_TYPE_KEY_JCR_PATH + ". Will use the default key: " + AxProductPropertiesKey.TICKET_TYPE);
            }

            String publishPriceKey;
            try {
                Node publishPriceKeyNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), AxProductPropertiesKey.PUBLISH_PRICE_KEY_JCR_PATH);
                publishPriceKey = PropertyUtil.getString(publishPriceKeyNode, "value");
            } catch (RepositoryException e) {
                publishPriceKey = AxProductPropertiesKey.PUBLISHED_PRICE; // default
                                                                          // if
                                                                          // nothing
                                                                          // is
                                                                          // set
                log.error("No cms config found for path: " + AxProductPropertiesKey.PUBLISH_PRICE_KEY_JCR_PATH + ". Will use the default key: "
                        + AxProductPropertiesKey.PUBLISHED_PRICE);
            }

            for (AxProduct axProdData : axProductsData) {
                AxMainProduct product = new AxMainProduct();
                product.setItemId(axProdData.getItemId());
                product.setProductName(axProdData.getProductName());
                product.setRecordId(new Long(axProdData.getRecordId()));
                product.setPrice(new BigDecimal(axProdData.getPrice()));
                product.setDescription(axProdData.getDescription());
                product.setSearchName(axProdData.getSearchName());
                axMainProductList.add(product);

                AxStarProductCatalog productCatalog = productCatalogMap.get(axProdData.getRecordId());
                if (productCatalog != null) {
                    product.setValidFrom(parseValidityDate(productCatalog.getValidFrom()));
                    product.setValidTo(parseValidityDate(productCatalog.getValidTo()));
                }

                for (AxProductPropertyTranslation translateion : axProdData.getProductProperties()) {
                    if (AxProductPropertyTranslation.EN.equals(translateion.getTranslationLanguage())) {

                        for (AxProductProperty prop : translateion.getAxProductPropertyList()) {
                            if (ticketTypeKey.equals(prop.getKeyName())) {
                                String ticketType = prop.getValueString();
                                // everything that is not adult or child will
                                // automatically become Standard
                                if (!AxProductPropertiesKey.TICKET_TYPE_ADULT.equals(ticketType) && !AxProductPropertiesKey.TICKET_TYPE_CHILD.equals(ticketType)) {
                                    ticketType = AxProductPropertiesKey.TICKET_TYPE_STANDARD;
                                }

                                product.setTicketType(ticketType);
                            } else if (publishPriceKey.equals(prop.getKeyName())) {
                                product.setPublishedPrice(new BigDecimal(prop.getValueString()));
                            }
                        }
                    }
                }
            }

            AxStarServiceResult<KioskSearchProductExtResponse> searchProductExtResult = axStarKioskService.apiKioskSearchProductExt(channel, new KioskSearchProductExtRequest());
            if (searchProductExtResult.isSuccess()) {
                for (AxInventTableExtEntity extData : searchProductExtResult.getData().getAxInventTableExtEntityList()) {
                    AxMainProduct axMainProduct = axMainProductList.stream().filter(p -> p.getItemId().equals(extData.getItemId())).findAny().orElse(null);
                    if (axMainProduct != null) {
                        axMainProduct.setMediaTypeDescription(extData.getMediaTypeDescription());
                        axMainProduct.setMediaTypeId(extData.getMediaTypeId());
                        axMainProduct.setIsCapacity(extData.isCapacity());
                        axMainProduct.setOwnAttraction("1".equals(extData.getOwnAttraction()) ? Boolean.TRUE : Boolean.FALSE);
                        axMainProduct.setTemplateName(extData.getTemplateName());
                    }
                }
            }
            pkg.setSuccess(true);
            pkg.setMainProductList(axMainProductList);
            pkg.setAxProductsResultJson(kioskGetProductsResult.getRawData());
            pkg.setMessage("Successful");

            apiResult = new ApiResult<>(true, "", "", pkg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            apiResult.setErrorCode("ERR_PRODUCT_DATA_RETRIEVAL");
            apiResult.setMessage(e.getMessage());
        }
        return apiResult;
    }

    public ApiResult<AxProductLinkPackage> getValidAxProducts(StoreApiChannels channel) {
        AxProductLinkPackage pkg = new AxProductLinkPackage();
        pkg.setChannel(channel);

        AxStarServiceResult<List<AxStarProduct>> axProducts = axStarService.apiProductsGet(channel);
        if (!axProducts.isSuccess()) {
            log.error("Error in getting the Products...");
            return new ApiResult<>(false, "ERR_PRODUCT_DATA_RETRIEVAL", "Error retrieving the product data.", null);
        }

        List<AxStarProduct> axProductsData = axProducts.getData();
        List<AxMainProduct> axMainProductList = new ArrayList<>();

        for (AxStarProduct axProdData : axProductsData) {
            AxMainProduct product = new AxMainProduct();
            product.setItemId(axProdData.getItemId());
            product.setProductName(axProdData.getProductName());
            product.setRecordId(axProdData.getRecordId());
            product.setPrice(axProdData.getPrice());
            product.setDescription(axProdData.getDescription());
            product.setSearchName(axProdData.getSearchName());
            axMainProductList.add(product);
        }

        pkg.setSuccess(true);
        pkg.setMainProductList(axMainProductList);
        pkg.setAxProductsResultJson(axProducts.getRawData());
        pkg.setMessage("Successful");

        return new ApiResult<>(true, "", "", pkg);
    }

    @Override
    public ApiResult<PromotionLinkPackage> getFullDiscountData(StoreApiChannels channel, Map<Long, BigDecimal> mainProductPriceMap) {

        PromotionLinkPackage pkg = new PromotionLinkPackage();
        pkg.setChannel(channel);

        AxStarServiceResult<List<AxStarRetailDiscount>> axDiscounts = axStarService.apiRetailDiscountsGet(channel);

        if (!axDiscounts.isSuccess()) {
            log.error("Error in getting the RetailDiscounts...");
            return new ApiResult<>(false, "ERR_DISCOUNT_DATA_RETRIEVAL", "Error retrieving the discount data.", null);
        }

        AxStarServiceResult<List<AxStarAffiliation>> axAffs = axStarService.apiAffiliationsGet(channel);
        if (!axAffs.isSuccess()) {
            log.error("Error in getting the Affiliations...");
            return new ApiResult<>(false, "ERR_AFFILIATION_RETRIEVAL", "Error retrieving the affiliations data.", null);
        }

        // Process the affiliations and store by PriceGroupId
        List<AxStarAffiliation> affiliations = axAffs.getData();
        Map<Long, List<PromotionRelatedAffiliation>> affPriceGroupMap = new HashMap<>(); // TODO
                                                                                         // check
                                                                                         // if
                                                                                         // one
                                                                                         // priceGroup
                                                                                         // can
                                                                                         // belong
                                                                                         // to
                                                                                         // 2
                                                                                         // different
                                                                                         // aff
        if (affiliations != null && affiliations.size() > 0) {
            for (AxStarAffiliation aff : affiliations) {
                List<AxStarPriceGroup> priceGroupList = aff.getPriceGroups();
                for (AxStarPriceGroup priceGroup : priceGroupList) {
                    PromotionRelatedAffiliation relatedAff = new PromotionRelatedAffiliation();
                    relatedAff.setRecordId(aff.getRecordId().toString());
                    relatedAff.setSystemName(aff.getName());
                    relatedAff.setSystemDescription(aff.getDescription());

                    List<PromotionRelatedAffiliation> relatedAffList = affPriceGroupMap.get(priceGroup.getPriceGroupId());
                    if (relatedAffList == null) {
                        relatedAffList = new ArrayList<>();
                    }
                    relatedAffList.add(relatedAff); // add in the new one
                    affPriceGroupMap.put(priceGroup.getPriceGroupId(), relatedAffList);
                }
            }
        }

        // Process the discounts
        Map<Long, List<ProductRelatedPromotion>> promotionProductMap = new HashMap<>(); // map
                                                                                        // of
                                                                                        // promotion
                                                                                        // with
                                                                                        // product
        List<AxStarRetailDiscount> retailDiscount = axDiscounts.getData();

        if (retailDiscount != null && retailDiscount.size() > 0) {

            List<PromotionLinkMainPromotion> mainPromotionList = new ArrayList<>();

            for (AxStarRetailDiscount rd : retailDiscount) {

                ProductRelatedPromotion promotion;
                List<PromotionRelatedAffiliation> relatedAffiliationList = new ArrayList<>();
                List<PromotionRelatedDiscountCode> relatedDiscountCodeList = new ArrayList<>();
                Date now = NvxDateUtils.clearTime(new Date());
                ProductRelatedPromotion mainPromotion = new ProductRelatedPromotion();
                PromotionLinkMainPromotion mainPromotionLink = new PromotionLinkMainPromotion();

                mainPromotionList.add(mainPromotionLink);

                // for the MAIN PROMOTION, ideally for those without a related
                // item....
                mainPromotion.setOfferId(rd.getOfferId());
                mainPromotion.setSystemName(rd.getName());

                Date mainPromotionValidFrom = parseValidityDate(rd.getValidFromDate());
                Date mainPromotionValidTo = parseValidityDate(rd.getValidToDate());

                if (mainPromotionValidFrom != null && mainPromotionValidTo != null && now.compareTo(mainPromotionValidFrom) >= 0 && now.compareTo(mainPromotionValidTo) <= 0) {
                    mainPromotion.setSystemActive(true); // TODO check if this
                                                         // is the case
                } else {
                    mainPromotion.setSystemActive(false);
                }

                mainPromotion.setValidFrom(mainPromotionValidFrom);
                mainPromotion.setValidTo(mainPromotionValidTo);
                mainPromotion.setCurrencyCode(rd.getCurrencyCode());
                mainPromotion.setEstimatedDiscountedUnitPrice(BigDecimal.ZERO);

                mainPromotionLink.setPromotion(mainPromotion);

                // TODO
                // TODO
                // TODO For now no need to check if the groupId is SLM or KIOSK,
                // assume that the API will return to me the correct Channel
                // Related Affiliation
                List<AxStarRetailDiscountPriceGroup> priceGroupList = rd.getPriceGroups();
                for (AxStarRetailDiscountPriceGroup priceGroup : priceGroupList) {
                    // check if it has related aff
                    if (affPriceGroupMap.containsKey(priceGroup.getPriceGroupId())) {
                        relatedAffiliationList.addAll(affPriceGroupMap.get(priceGroup.getPriceGroupId())); // TODO:
                                                                                                           // for
                                                                                                           // now
                                                                                                           // just
                                                                                                           // add
                                                                                                           // everything,
                                                                                                           // even
                                                                                                           // if
                                                                                                           // there's
                                                                                                           // only
                                                                                                           // one
                                                                                                           // aff
                    }
                }

                mainPromotion.setAffiliations(relatedAffiliationList);

                // Related DiscountCode
                List<AxStarRetailDiscountDiscountCode> discountCodeList = rd.getDiscountCodes();
                for (AxStarRetailDiscountDiscountCode discountCode : discountCodeList) {
                    PromotionRelatedDiscountCode relatedDiscountCode = new PromotionRelatedDiscountCode();
                    relatedDiscountCode.setRecordId(discountCode.getRecordId().toString());
                    relatedDiscountCode.setRelatedOfferId(discountCode.getOfferId());
                    relatedDiscountCode.setBarcode(discountCode.getBarcode());
                    relatedDiscountCode.setDiscountCode(discountCode.getCode());
                    relatedDiscountCode.setSystemName(discountCode.getName());
                    Date validFrom = parseValidityDate(discountCode.getValidFrom());
                    Date validTo = parseValidityDate(discountCode.getValidTo());

                    if (validFrom != null && validTo != null && now.compareTo(validFrom) >= 0 && now.compareTo(validTo) <= 0) {
                        relatedDiscountCode.setSystemActive(true); // TODO check
                                                                   // if this is
                                                                   // the case
                    } else {
                        relatedDiscountCode.setSystemActive(false);
                    }
                    relatedDiscountCode.setValidFrom(validFrom);
                    relatedDiscountCode.setValidTo(validTo);
                    relatedDiscountCodeList.add(relatedDiscountCode);
                }

                mainPromotion.setDiscountCodes(relatedDiscountCodeList);

                // For Promotion Sake

                // For Main Product Sake
                Map<String, PromotionRelatedProduct> relatedProductMap = new HashMap<>();
                mainPromotionLink.setRelatedItems(relatedProductMap);

                List<AxStarRetailDiscountLine> discountLineList = rd.getDiscountLines();
                for (AxStarRetailDiscountLine discountLine : discountLineList) {

                    // TODO if product id is 0, just ignore for now, evaluate
                    // later what needs to do
                    if (discountLine.getProductId() == 0) {
                        continue;
                    }

                    promotion = new ProductRelatedPromotion(); // new promotion
                                                               // for
                                                               // discountLine
                    promotion.setOfferId(rd.getOfferId());
                    promotion.setSystemName(rd.getName());

                    List<ProductRelatedPromotion> relatedPromotionList = promotionProductMap.get(discountLine.getProductId()); // get
                                                                                                                               // from
                                                                                                                               // the
                                                                                                                               // mapping
                                                                                                                               // for
                                                                                                                               // the
                                                                                                                               // promotion
                                                                                                                               // and
                                                                                                                               // product
                                                                                                                               // that
                                                                                                                               // we
                                                                                                                               // stored
                                                                                                                               // so
                                                                                                                               // far
                    if (relatedPromotionList == null) {
                        relatedPromotionList = new ArrayList<>();
                    }
                    // TODO do the computation here
                    BigDecimal relProductPrice = mainProductPriceMap.get(discountLine.getProductId());
                    promotion.setEstimatedDiscountedUnitPrice(computeEstimatedPrice(relProductPrice, rd, discountLine, rd.getMultibuyQuantityTiers())); // TODO
                                                                                                                                                        // compute
                                                                                                                                                        // this
                                                                                                                                                        // one
                    relatedPromotionList.add(promotion);

                    PromotionRelatedProduct relatedProduct = new PromotionRelatedProduct();
                    relatedProduct.setItemId(discountLine.getProductId() + "");
                    relatedProduct.setEstimatedDiscountedUnitPrice(promotion.getEstimatedDiscountedUnitPrice());
                    relatedProductMap.put(relatedProduct.getItemId(), relatedProduct);

                    promotionProductMap.put(discountLine.getProductId(), relatedPromotionList);
                }
            }

            List<PromotionLinkMainProduct> promotionMainProductList = new ArrayList<>();
            for (Long productId : promotionProductMap.keySet()) {
                PromotionLinkMainProduct mainProduct = new PromotionLinkMainProduct();
                mainProduct.setItemId(productId.toString());
                mainProduct.setRelatedPromotions(promotionProductMap.get(productId));
                promotionMainProductList.add(mainProduct);
            }

            pkg.setMainPromotionList(mainPromotionList);
            pkg.setMainProductList(promotionMainProductList);
        }

        pkg.setMessage("Success");
        pkg.setSuccess(true);

        pkg.setRetailDiscountResultJson(axDiscounts.getRawData());
        pkg.setAffiliationResultJson(axAffs.getRawData());

        return new ApiResult<>(true, "", "", pkg);
    }

    private BigDecimal computeEstimatedPrice(BigDecimal productPrice, AxStarRetailDiscount retailDiscount, AxStarRetailDiscountLine discountLine,
            List<AxStarRetailDiscountMultibuyQuantityTier> quantityTier) {

        if (productPrice == null) {
            return BigDecimal.ZERO;
        }

        // (1) DiscountPercent, 10% off
        if (BigDecimal.ZERO.compareTo(discountLine.getDiscountPercent()) != 0) {
            return productPrice.multiply(new BigDecimal(100).subtract(discountLine.getDiscountPercent())).divide(new BigDecimal(100));
        }

        // (2) Buy X for $Y.00, dealPrice divide by the number of items
        if (BigDecimal.ZERO.compareTo(retailDiscount.getMixAndMatchDealPrice()) != 0) {
            return retailDiscount.getMixAndMatchDealPrice().divide(new BigDecimal(discountLine.getMixAndMatchLineNumberOfItemsNeeded()));
        }

        // (3) OfferPriceIncTax > 0, this is the amount
        if (BigDecimal.ZERO.compareTo(discountLine.getOfferPriceIncludingTax()) != 0) {
            return discountLine.getOfferPriceIncludingTax();
        }

        // (4) Buy 1 Merlion with USS $60
        if (BigDecimal.ZERO.compareTo(new BigDecimal(discountLine.getDiscountLinePercentOrValue())) != 0) {
            // discount type = 4, is actually the actual value
            if (retailDiscount.getDiscountType().intValue() == 4) {
                return new BigDecimal(discountLine.getDiscountLinePercentOrValue()).divide(new BigDecimal(discountLine.getMixAndMatchLineNumberOfItemsNeeded()));
            } else {
                // else it means that probably it is percent i guess
                return productPrice.multiply(new BigDecimal(100).subtract(new BigDecimal(discountLine.getDiscountLinePercentOrValue()))).divide(new BigDecimal(100));
            }

        }

        // (5) Buy minimum 5 get 20% off and Buy minimum 3 get discounted price
        // of $3 each
        if (quantityTier != null && quantityTier.size() > 0) {
            // TODO for now get the first one only
            AxStarRetailDiscountMultibuyQuantityTier tier = quantityTier.get(0);
            // if discount type = 1, use % off
            if (retailDiscount.getDiscountType().intValue() == 1) {
                return productPrice.multiply(new BigDecimal(100).subtract(tier.getDiscountPriceOrPercent())).divide(new BigDecimal(100));
            } else if (retailDiscount.getDiscountType().intValue() == 0) {
                return tier.getDiscountPriceOrPercent();
            }
        }

        // (6) Amount off, 5$ off
        if (BigDecimal.ZERO.compareTo(discountLine.getDiscountAmount()) != 0) {
            return productPrice.subtract(discountLine.getDiscountAmount());
        }

        return productPrice;
    }

    private Date parseValidityDate(String validity) {
        Date validDate;
        try {
            String validToDateClean = validity.substring(0, 10) + validity.substring(11, 19) + validity.substring(19, 22) + validity.substring(23);
            validDate = NvxDateUtils.parseDate(validToDateClean, "yyyy-MM-ddHH:mm:ssZ");
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
            return null;
        }
        return validDate;
    }

    @Override
    public ApiResult<CrossSellLinkPackage> getFullUpCrossSellData(StoreApiChannels channel) throws AxChannelException {
        CrossSellLinkPackage pkg = new CrossSellLinkPackage();
        pkg.setChannel(channel);

        final AxStarServiceResult<KioskGetUpCrossSellResponse> axApiResult = this.axStarKioskService.apiKioskGetUpCrossSell(channel, new KioskGetUpCrossSellRequest());
        if (!axApiResult.isSuccess()) {
            return new ApiResult<>();
        }

        final List<UpCrossSellTableEntity> axData = axApiResult.getData().getUpCrossSellTableEntityList();

        List<AxRetailUpCrossSellProduct> dataList = new ArrayList<>();
        if (axData.isEmpty()) {
            pkg.setSuccess(true);
            pkg.setMessage("Success");
            return new ApiResult<>(true, "", "", pkg);
        }

        Map<String, CrossSellLinkMainProduct> mainProductMap = new HashMap<>();

        for (UpCrossSellTableEntity crossSelData : axData) {

            AxRetailUpCrossSellProduct data = new AxRetailUpCrossSellProduct();
            dataList.add(data);
            data.setActive(crossSelData.getaCTIVE().equals("1") ? Boolean.TRUE : Boolean.FALSE);
            data.setChannel(new Long(crossSelData.getcHANNEL()));
            data.setItemId(crossSelData.getiTEMID());
            data.setMinimumQty(new BigDecimal(crossSelData.getmINIMUMQTY()).intValue());
            data.setMultipleQty(new BigDecimal(crossSelData.getmULTIPLEQTY()).intValue());
            data.setRecId(new Long(crossSelData.getrECID()));
            data.setTableAll(new Integer(crossSelData.gettABLEALL()));
            data.setUnitId(crossSelData.getuNITIDId());

            data.setLines(new ArrayList<>());
            for (UpCrossSellLineEntity line : crossSelData.getUpCrossSellLineList()) {
                AxRetailUpCrossSellLine dataLine = new AxRetailUpCrossSellLine();
                dataLine.setInventDimId(line.getiNVENTDIMID());
                dataLine.setOriginalItemId(line.getoRIGITEMID());
                dataLine.setItemId(line.getuPCROSSSELLITEM());
                dataLine.setQty(new BigDecimal(line.getqTY()).intValue());
                dataLine.setTableRecId(new Long(line.getuPCROSSSELLTABLERECID()));
                dataLine.setUnitId(line.getuNITID());
                dataLine.setUpsellType(new Integer(line.getuPSELLTYPE()));
                dataLine.setVariantId(line.getvARIANTID());
                data.getLines().add(dataLine);
            }

            CrossSellLinkMainProduct mainProduct = new CrossSellLinkMainProduct();
            mainProduct.setItemId(crossSelData.getiTEMID());
            mainProduct.setUnitId(crossSelData.getuNITIDId());
            mainProduct.setMultipleQty(new BigDecimal(crossSelData.getmULTIPLEQTY()).intValue());
            mainProduct.setMinimumQty(new BigDecimal(crossSelData.getmINIMUMQTY()).intValue());

            List<UpCrossSellLineEntity> relCrossSells = crossSelData.getUpCrossSellLineList();
            List<CrossSellLinkRelatedProduct> relatedProductList = new ArrayList<>();
            for (UpCrossSellLineEntity relCS : relCrossSells) {
                CrossSellLinkRelatedProduct relatedProduct = new CrossSellLinkRelatedProduct();
                relatedProduct.setRecordId(relCS.getuPCROSSSELLTABLERECID());
                relatedProduct.setItemId(relCS.getuPCROSSSELLITEM());
                relatedProduct.setQty(new BigDecimal(relCS.getqTY()).intValue());
                relatedProduct.setUnitId(relCS.getuNITID());
                relatedProductList.add(relatedProduct);
            }
            mainProduct.setRelatedProducts(relatedProductList);
            mainProductMap.put(mainProduct.getItemId(), mainProduct);
        }
        pkg.setMainProductMap(mainProductMap);
        pkg.setSuccess(true);
        pkg.setMessage("Success");
        pkg.setAxRetailUpCrossSellProductResultJson(JsonUtil.jsonify(dataList, new TypeToken<List<AxRetailUpCrossSellProduct>>() {
        }.getType()));

        return new ApiResult<>(true, "", "", pkg); // TODO
    }

    // {
    // "OfferId": "SLM-000001",
    // "CurrencyCode": "SGD",
    // "PriceDiscountGroup": 5637147576,
    // "Name": "10% disc",
    // "PeriodicDiscountType": 2,
    // "ConcurrencyMode": 2,
    // "IsDiscountCodeRequired": false,
    // "ValidationPeriodId": "",
    // "DateValidationType": 1,
    // "ValidFromDate": "1900-01-01T00:00:00+00:00",
    // "ValidToDate": "2154-12-31T00:00:00+08:00",
    // "DiscountType": 2,
    // "MixAndMatchDealPrice": 0,
    // "MixAndMatchDiscountPercent": 10,
    // "MixAndMatchDiscountAmount": 0,
    // "MixAndMatchNumberOfLeastExpensiveLines": 0,
    // "MixAndMatchNumberOfTimeApplicable": 0,
    // "ShouldCountNonDiscountItems": 0,
    // "DiscountLines": [
    // {
    // "OfferId": "SLM-000001",
    // "DiscountLineNumber": 1,
    // "DiscountLinePercentOrValue": 0,
    // "MixAndMatchLineGroup": "",
    // "MixAndMatchLineSpecificDiscountType": 0,
    // "MixAndMatchLineNumberOfItemsNeeded": 0,
    // "DiscountMethod": 0,
    // "DiscountAmount": 0,
    // "DiscountPercent": 10,
    // "OfferPrice": 0,
    // "OfferPriceIncludingTax": 0,
    // "UnitOfMeasureSymbol": "Ea",
    // "CategoryId": 5637150592,
    // "ProductId": 5637145331,
    // "DistinctProductVariantId": 0,
    // "ExtensionProperties": [
    // {
    // "Key": "DATAAREAID",
    // "Value": {
    // "BooleanValue": null,
    // "ByteValue": null,
    // "DecimalValue": null,
    // "DateTimeOffsetValue": null,
    // "IntegerValue": null,
    // "LongValue": null,
    // "StringValue": "slm"
    // }
    // }
    // ]
    // }
    // ],
    // "PriceGroups": [
    // {
    // "OfferId": "SLM-000001",
    // "RecordId": 5637152076,
    // "PriceGroupId": 5637147576,
    // "GroupId": "B2C",
    // "ExtensionProperties": [
    // {
    // "Key": "DATAAREAID",
    // "Value": {
    // "BooleanValue": null,
    // "ByteValue": null,
    // "DecimalValue": null,
    // "DateTimeOffsetValue": null,
    // "IntegerValue": null,
    // "LongValue": null,
    // "StringValue": "slm"
    // }
    // }
    // ]
    // },
    // {
    // "OfferId": "SLM-000001",
    // "RecordId": 5637153582,
    // "PriceGroupId": 5637151327,
    // "GroupId": "Kiosk",
    // "ExtensionProperties": [
    // {
    // "Key": "DATAAREAID",
    // "Value": {
    // "BooleanValue": null,
    // "ByteValue": null,
    // "DecimalValue": null,
    // "DateTimeOffsetValue": null,
    // "IntegerValue": null,
    // "LongValue": null,
    // "StringValue": "slm"
    // }
    // }
    // ]
    // }
    // ],
    // "DiscountCodes": [],
    // "MultibuyQuantityTiers": [],
    // "ThresholdDiscountTiers": [],
    // "ExtensionProperties": [
    // {
    // "Key": "DATAAREAID",
    // "Value": {
    // "BooleanValue": null,
    // "ByteValue": null,
    // "DecimalValue": null,
    // "DateTimeOffsetValue": null,
    // "IntegerValue": null,
    // "LongValue": null,
    // "StringValue": "slm"
    // }
    // }
    // ]
    // },
}
