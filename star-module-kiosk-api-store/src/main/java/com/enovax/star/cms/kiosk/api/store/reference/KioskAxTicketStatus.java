package com.enovax.star.cms.kiosk.api.store.reference;

/**
 * TICKETSTATUS { VALID = 1, EXPIRED = 2, NOACCESS = 3, UNKNOWNTICKETNUMBER = 4
 * , UNKNOWNFORMAT = 5, REENTRY = 6, BLACKLISTED = 7, RETURNED = 8, VOIDED = 9 ,
 * REPLACEDUPGRADE = 10, NOUSAGELEFT = 11, INACTIVE = 12, CANCELLED = 13 ,
 * BLOCKOUTPERIOD = 14, REDEEMED = 15, REPLACEDADDON = 16, REPLACEDREPRINT = 17
 * };
 * 
 * @author Justin
 *
 */
public enum KioskAxTicketStatus {

    INACTIVE("12");

    private final String code;

    private KioskAxTicketStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static KioskAxTicketStatus getAxTicketStatus(String code) {
        KioskAxTicketStatus status = null;
        if (INACTIVE.getCode().equals(code)) {
            status = INACTIVE;
        }
        return status;
    }

}
