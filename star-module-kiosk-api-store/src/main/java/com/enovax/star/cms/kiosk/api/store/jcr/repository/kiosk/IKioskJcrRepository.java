package com.enovax.star.cms.kiosk.api.store.jcr.repository.kiosk;

import java.text.ParseException;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.jcr.JcrKioskInfo;
import com.enovax.star.cms.kiosk.api.store.opsmodel.ScheduledMaintenance;

/**
 * Repositry for kiosk related data in JCR.
 * 
 * @author Justin
 * @since 7 SEP 16
 */
public interface IKioskJcrRepository {

    JcrKioskInfo get(String channel);

    List<JcrKioskInfo> getAll(String channel);

    ScheduledMaintenance getSchedule(String channel) throws ParseException;
}
