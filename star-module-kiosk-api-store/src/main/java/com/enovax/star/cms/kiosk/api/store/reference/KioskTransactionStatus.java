package com.enovax.star.cms.kiosk.api.store.reference;

public enum KioskTransactionStatus {

    TRANSACTION_CREATED("1", "TRANSACTION CREATED"), //
    PAYMENT_STARTED("2", "PAYMENT STARTED"), //
    PAYMENT_SUCCEED("3", "PAYMENT SUCCEED"), //
    TICKET_ISSUED("4", "TICKET_ISSUED"), //
    RECOVERY_FLOW_STARTED("5", "RECOVERY FLOW STARTED"), //
    REDEMPTION_TICKET_ISSUED("6", "REDEMPTION TICKET ISSUED"), //
    TICKET_NOT_ISSUED("7", "TICKET NOT ISSUED"), //
    TICKET_NOT_ISSUED_RECOVERY_FLOW("8", "TICKET NOT ISSUED RECOVERY FLOW"), //
    PAYMENT_VOID("9", "PAYMENT VOID"), //
    AX_CHECKOUT_SUCCESSFUL("10", "AX CHECKOUT SUCCESSFUL"), //
    AX_CHECKOUT_FAILED("11", "AX CHECKOUT FAILED"), //
    AX_REDEMPTION_SUCCESSFUL("12", "AX REDEMPTION SUCCESSFUL"), //
    AX_REDEMPTION_FAILED("13", "AX REDEMPTION FAILED"), //
	RECOVERY_FLOW_ISSUED("14", "RECOVERY FLOW STARTED"); //
    
	private final String code;
    public final String message;

    public static String getTransactionStatus(String code) {
        String transactionStatus = null;
        if (TRANSACTION_CREATED.getCode().equals(code)) {
            transactionStatus = TRANSACTION_CREATED.getCode();
        } else if (PAYMENT_STARTED.getCode().equals(code)) {
            transactionStatus = PAYMENT_STARTED.getCode();
        } else if (PAYMENT_SUCCEED.getCode().equals(code)) {
            transactionStatus = PAYMENT_SUCCEED.getCode();
        } else if (TICKET_ISSUED.getCode().equals(code)) {
            transactionStatus = TICKET_ISSUED.getCode();
        } else if (RECOVERY_FLOW_STARTED.getCode().equals(code)) {
            transactionStatus = RECOVERY_FLOW_STARTED.getCode();
        } else if (REDEMPTION_TICKET_ISSUED.getCode().equals(code)) {
            transactionStatus = REDEMPTION_TICKET_ISSUED.getCode();
        } else if (TICKET_NOT_ISSUED.getCode().equals(code)) {
            transactionStatus = TICKET_NOT_ISSUED.getCode();
        } else if (TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode().equals(code)) {
            transactionStatus = TICKET_NOT_ISSUED_RECOVERY_FLOW.getCode();
        } else if (PAYMENT_VOID.getCode().equals(code)) {
            transactionStatus = PAYMENT_VOID.getCode();
        } else if (AX_CHECKOUT_SUCCESSFUL.getCode().equals(code)) {
            transactionStatus = AX_CHECKOUT_SUCCESSFUL.getCode();
        } else if (AX_CHECKOUT_FAILED.getCode().equals(code)) {
            transactionStatus = AX_CHECKOUT_FAILED.getCode();
        } else if (AX_REDEMPTION_SUCCESSFUL.getCode().equals(code)) {
            transactionStatus = AX_REDEMPTION_SUCCESSFUL.getCode();
        } else if (AX_REDEMPTION_FAILED.getCode().equals(code)) {
            transactionStatus = AX_REDEMPTION_FAILED.getCode();
        }else if (RECOVERY_FLOW_ISSUED.getCode().equals(code)) {
            transactionStatus = RECOVERY_FLOW_ISSUED.getCode();
        }
        return transactionStatus;
    }

    private KioskTransactionStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return message;
    }
}
