


CREATE TABLE [dbo].[kiosk_tran]
(
  [oid] [INT] IDENTITY(1,1) NOT NULL,
  [receipt_no] [NVARCHAR](50),
  [ax_receipt_no] [NVARCHAR](50),
  [ax_receipt_id] [INT],
  [ax_cart_id] [NVARCHAR](50),
  [total_amount] [FLOAT],
  [gst_amount] [FLOAT],
  [currency] [NVARCHAR](3),
  [bank_approval_code] [NVARCHAR](10),
  [tickets_printed] [INT],
  [status] [NVARCHAR](5),
  [checkout_datetime] [DATETIME],
  [begin_datetime] [DATETIME],
  [end_datetime] [DATETIME],
  [rdp_pin_code] [NVARCHAR](50),
  [rdp_allow_partial] INT,
  [kiosk_oid] [INT]
);

ALTER TABLE [dbo].[kiosk_tran] ADD CONSTRAINT pk_kiosk_tran PRIMARY KEY  ([oid]);

CREATE TABLE [dbo].[kiosk_tran_item]
(
  [oid] [INT] IDENTITY(1,1) NOT NULL,
  [amount] [FLOAT],
  [quantity] [INT],
  [rdp_original_qty] [INT],
  [rdp_redeemed_qty] [INT],
  [rdp_remaining_qty] [INT],
  [product_id] [NVARCHAR](50),
  [item_id] [NVARCHAR](50),
  [parent_product_id] [NVARCHAR](50),
  [cms_prod_name] [NVARCHAR](200),
  [ax_prod_name] [NVARCHAR](200),
  [selected_event_date] [NVARCHAR](50),
  [event_group_id] [NVARCHAR](200),
  [event_line_id] [NVARCHAR](200),
  [sub_total] [FLOAT],
  [fk_tran_oid] [INT]
);
ALTER TABLE [dbo].[kiosk_ticket] ADD CONSTRAINT pk_kiosk_tran_item PRIMARY KEY  ([oid]);

CREATE TABLE [dbo].[kiosk_ticket]
(
  [oid] [INT] IDENTITY(1,1) NOT NULL,
  [event_date] [NVARCHAR](50),
  [event_group_id] [NVARCHAR](50),
  [event_line_id] [NVARCHAR](50),
  [item_id] [NVARCHAR](50),
  [qr_ticket_code] [NVARCHAR](50),
  [cms_prod_name] [NVARCHAR](200),
  [ax_prod_name] [NVARCHAR](200),
  [qty] [NVARCHAR](50),
  [start_date] [NVARCHAR](10),
  [end_date] [NVARCHAR](50),
  [code] [NVARCHAR](50),
  [table_id] [NVARCHAR](50),
  [tran_date] [NVARCHAR](50),
  [updated_datetime] [NVARCHAR](50),
  [create_datetime] [NVARCHAR](50),
  [tran_id] [NVARCHAR](50),
  [shift_id] [NVARCHAR](50),
  [usage_val_id] [NVARCHAR](50),
  [open_val_id] [NVARCHAR](50),
  [receipt_id] [NVARCHAR](50),
  [third_party] [NVARCHAR](50),
  [code_data] [NVARCHAR](1000),
  [token_data] [TEXT],
  [barcode] VARCHAR(MAX),
  [status] [nvarchar](5),
  [ticket_no] [NVARCHAR](50);
  [template_name] [NVARCHAR](50),
  [fk_tran_oid] INT
);


ALTER TABLE [dbo].[kiosk_ticket] ADD CONSTRAINT pk_kiosk_ticket PRIMARY KEY  ([oid]);

