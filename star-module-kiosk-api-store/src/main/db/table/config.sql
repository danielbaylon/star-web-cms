
CREATE TABLE [dbo].[kiosk_info]
(
	[oid] INT NOT NULL IDENTITY(1,1),
	[channel] [NVARCHAR] (20),
	[channel_id] [NVARCHAR](20),
	[channel_name] [NVARCHAR](20),
	[device_no] [NVARCHAR](20),
	[device_status] INT,
	[device_token] [NVARCHAR](500),
	[store_no] [NVARCHAR](20),
	[terminal_no] [NVARCHAR](20),
	[hardware_profile_id] [NVARCHAR](20),
    [cash_drawer] [NVARCHAR](20),
	[user_id] [NVARCHAR](20),
	[user_password] [NVARCHAR](20),
	[jcr_node_name] [NVARCHAR](50),
	[jcr_node_uuid] [NVARCHAR](50),
	[tran_seq] INT,
	[shift_seq] INT,
	[receipt_seq] INT,
	[cart_id] [NVARCHAR](20),
	[location] [NVARCHAR](200),
	[placement] [NVARCHAR](200),
	[terminal_merchant_info] [NVARCHAR](200)
);

ALTER TABLE [dbo].[kiosk_info] ADD CONSTRAINT pk_kiosk_info PRIMARY KEY  ([oid]);