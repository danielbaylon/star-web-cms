package com.enovax.star.migration.service;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.repository.ppslm.PPSLMPartnerRepository;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.migration.model.*;
import com.enovax.star.migration.repository.MigrationDao;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.UserManager;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by jonathan on 16/1/17.
 */
@Service
public class MigrationService {

    private static DecimalFormat numberFormatter = new DecimalFormat("##00");

    private SecuritySupport securitySupport;
    private static UserManager userManager;

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MigrationDao migrationDao;
    @Autowired
    private PPSLMPartnerRepository partnerRepo;

    @PostConstruct
    public void init(){
        securitySupport = Components.getComponent(SecuritySupport.class);
        userManager = securitySupport.getUserManager();
    }

    public void doB2BMigration() throws Exception {
        // migrate admin account
        try {
            List<AdminAccount> adminAccounts = migrationDao.getAdminAccount(0, 9999, "id", true);
            for (AdminAccount adminAccount : adminAccounts) {
                try {
                    User user = userManager.createUser("/admin/partner-portal-slm", adminAccount.getUsername(), "password");
                    userManager.setProperty(user, "email", adminAccount.getEmail());
                    double val = Double.valueOf(adminAccount.getId());
                    migrationDao.insertMigrationMapping("AdminAccount", (int) val, user.getName(), user.getIdentifier());
                } catch (Exception e) {
                    System.out.println("Error encountered during Admin Account migration record id " + adminAccount.getId() + ": " + e.getMessage());
                    log.error("Error encountered during Admin Account migration record id " + adminAccount.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during Admin Account migration: " + e.getMessage());
            log.error("Error encountered during Admin Account migration: " + e.getMessage(), e);
        }

        // update partner
        try {
            List<PPSLMPartner> partners = partnerRepo.findAll();
            for (PPSLMPartner partner : partners) {
                try {
                    String adminId = partner.getAccountManagerId();
                    if (!StringUtils.isEmpty(adminId) && NumberUtils.isNumber(adminId)) {
                        double val = Double.valueOf(adminId);
                        String nodeName = migrationDao.getMigrationMappingNodeName("AdminAccount", (int) val);
                        partner.setAccountManagerId(nodeName);
                        partnerRepo.save(partner);
                    }
                } catch (Exception e) {
                    System.out.println("Error encountered during Partner update record id " + partner.getId() + ": " + e.getMessage());
                    log.error("Error encountered during Partner update record id " + partner.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during Partner update: " + e.getMessage());
            log.error("Error encountered during Partner update: " + e.getMessage(), e);
        }

        // migrate tnc
        try {
            List<Tnc> tncs = migrationDao.getB2BTnc(0, 9999, "id", true);
            for (Tnc tnc : tncs) {
                try {
                    double val = Double.valueOf(tnc.getId());
                    Node node = JcrRepository.createNode(JcrWorkspace.TNC.getWorkspaceName(), "/partner-portal-slm", "X" + (int) val,
                        JcrWorkspace.TNC.getNodeType());
                    node.setProperty("content", tnc.getContent());
                    node.setProperty("title", tnc.getTitle());
                    node.setProperty("type", tnc.getType());
                    node.setProperty("status", "Active");
                    JcrRepository.updateNode(JcrWorkspace.TNC.getWorkspaceName(), node);
                    migrationDao.insertMigrationMapping("B2BTNC", (int) val, "X" + (int) val, node.getIdentifier());
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B TNC migration record id " + tnc.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B TNC migration record id " + tnc.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B TNC migration: " + e.getMessage());
            log.error("Error encountered during B2B TNC migration: " + e.getMessage(), e);
        }

        // migrate category
        try {
            List<Category> categories = migrationDao.getB2BCategory(0, 9999, "id", true);
            for (Category category : categories) {
                try {
                    double val = Double.valueOf(category.getId());
                    Node node = JcrRepository.createNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/partner-portal-slm", "X" + (int) val,
                        JcrWorkspace.ProductCategories.getNodeType());
                    node.setProperty("generatedURLPath", category.getName().toLowerCase());
                    node.setProperty("inactiveMessage", category.getInactiveMessage());
                    node.setProperty("name", category.getName());
                    JcrRepository.updateNode(JcrWorkspace.ProductCategories.getWorkspaceName(), node);
                    migrationDao.insertMigrationMapping("B2BCategory", (int) val, "X" + (int) val, node.getIdentifier());
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Category migration record id " + category.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Category migration record id " + category.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Category migration: " + e.getMessage());
            log.error("Error encountered during B2B Category migration: " + e.getMessage(), e);
        }

        // migrate product
        try {
            List<Product> products = migrationDao.getB2BProduct(0, 9999, "id", true);
            for (Product product : products) {
                try {
                    double val = Double.valueOf(product.getId());
                    Node node = JcrRepository.createNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/partner-portal-slm", "X" + (int) val,
                        JcrWorkspace.CMSProducts.getNodeType());
                    node.setProperty("description", product.getDescription());
                    node.setProperty("generatedURLPath", product.getName().replaceAll("\\s", "-"));
                    node.setProperty("minQty", Double.valueOf(product.getMinQty()).intValue());
                    node.setProperty("name", product.getName());
                    node.setProperty("productLevel", product.getProductLevel());
                    node.setProperty("shortDescription", product.getName());
                    node.setProperty("standaloneItem", product.getStandalone());
                    JcrRepository.updateNode(JcrWorkspace.CMSProducts.getWorkspaceName(), node);
                    migrationDao.insertMigrationMapping("B2BProduct", (int) val, "X" + (int) val, node.getIdentifier());
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Product migration record id " + product.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Product migration record id " + product.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Product migration: " + e.getMessage());
            log.error("Error encountered during B2B Product migration: " + e.getMessage(), e);
        }

        // migrate tnc map
        try {
            List<TncMap> tncMaps = migrationDao.getB2BProductTnc(0, 9999, "id", true);
            for (TncMap tncMap : tncMaps) {
                try {
                    double val = Double.valueOf(tncMap.getProductId());
                    double tncVal = Double.valueOf(tncMap.getTncId());
                    Node node = JcrRepository.getParentNode(JcrWorkspace.CMSProducts.getWorkspaceName(), "/cms-products/partner-portal-slm/X" + (int) val);
                    String tncUuid = migrationDao.getMigrationMappingUuid("B2BTNC", (int) tncVal);
                    node.setProperty("tnc", tncUuid);
                    JcrRepository.updateNode(JcrWorkspace.CMSProducts.getWorkspaceName(), node);
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Product TNC migration record id " + tncMap.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Product TNC migration record id " + tncMap.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Product TNC migration: " + e.getMessage());
            log.error("Error encountered during B2B Product TNC migration: " + e.getMessage(), e);
        }

        // migrate product order
        try {
            List<ProductOrder> orders = migrationDao.getB2BProductOrder(0, 9999, "displayOrder", true);
            for (ProductOrder order : orders) {
                try {
                    double val = Double.valueOf(order.getCategoryId());
                    double productVal = Double.valueOf(order.getProductId());
                    Node node = JcrRepository.getParentNode(JcrWorkspace.ProductCategories.getWorkspaceName(), "/partner-portal-slm/X" + (int) val);
                    Node subNode = null;
                    if (node.hasNode("default")) {
                        subNode = node.getNode("default");
                    } else {
                        subNode = node.addNode("default", JcrWorkspace.ProductDefaultSubCategories.getNodeType());
                    }
                    String productUuid = migrationDao.getMigrationMappingUuid("B2BProduct", (int) productVal);
                    NodeIterator iterator = subNode.getNodes();
                    int index = 0;
                    while (iterator.hasNext()) {
                        Node child = (Node) iterator.next();
                        int test = Integer.valueOf(child.getName());
                        if (test > index) {
                            index = test;
                        }
                    }
                    index++;
                    Node newNode = subNode.addNode(numberFormatter.format(index), JcrWorkspace.RelatedCMSProductForCategories.getNodeType());
                    newNode.setProperty("relatedCMSProductUUID", productUuid);
                    JcrRepository.updateNode(JcrWorkspace.RelatedCMSProductForCategories.getWorkspaceName(), newNode);
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Product Category migration record id " + order.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Product Category migration record id " + order.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Product Category migration: " + e.getMessage());
            log.error("Error encountered during B2B Product Category migration: " + e.getMessage(), e);
        }

        // migrate tier map TODO check tier id
        try {
            List<TierMap> tierMaps = migrationDao.getB2BTierMap(0, 9999, "id", true);
            for (TierMap tierMap : tierMaps) {
                try {
                    double val = Double.valueOf(tierMap.getTierId());
                    double productVal = Double.valueOf(tierMap.getProductId());
                    Node node = JcrRepository.getParentNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), "/partner-portal-slm");
                    NodeIterator iterator = node.getNodes();
                    int index = 0;
                    while (iterator.hasNext()) {
                        Node child = (Node) iterator.next();
                        int test = Integer.valueOf(child.getName());
                        if (test > index) {
                            index = test;
                        }
                    }
                    index++;
                    String productName = migrationDao.getMigrationMappingNodeName("B2BProduct", (int) productVal);
                    Node subNode = node.addNode(String.valueOf(index), JcrWorkspace.TierProductMappings.getNodeType());
                    subNode.setProperty("productId", productName);
                    subNode.setProperty("tierId", "Tier" + (int) val);
                    JcrRepository.updateNode(JcrWorkspace.TierProductMappings.getWorkspaceName(), subNode);
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Tier Map migration record id " + tierMap.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Tier Map migration record id " + tierMap.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Tier Map migration: " + e.getMessage());
            log.error("Error encountered during B2B Tier Map migration: " + e.getMessage(), e);
        }

        // migrate product map
        try {
            List<ExclusiveMap> exMaps = migrationDao.getB2BProductExclusive(0, 9999, "id", true);
            for (ExclusiveMap exMap : exMaps) {
                try {
                    if ("-1".equals(exMap.getActive())) {
                        double val = Double.valueOf(exMap.getPartnerId());
                        double productVal = Double.valueOf(exMap.getProductId());
                        String nodeName = String.valueOf((int) val);
                        Node subNode = null;
                        Node node = JcrRepository.getParentNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), "/partner-portal-slm");
                        if (node.hasNode(nodeName)) {
                            subNode = node.getNode(nodeName);
                        } else {
                            subNode = node.addNode(nodeName, JcrWorkspace.PartnerExclusiveProductMapping.getNodeType());
                        }
                        String productName = migrationDao.getMigrationMappingNodeName("B2BProduct", (int) productVal);
                        if (subNode.hasProperty("exclProdIds")) {
                            subNode.setProperty("exclProdIds", PropertyUtil.getString(subNode, "exclProdIds") + "," + productName);
                        } else {
                            subNode.setProperty("exclProdIds", productName);
                        }
                        JcrRepository.updateNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), subNode);
                    }
                } catch (Exception e) {
                    System.out.println("Error encountered during B2B Product Exclusive migration record id " + exMap.getId() + ": " + e.getMessage());
                    log.error("Error encountered during B2B Product Exclusive migration record id " + exMap.getId() + ": " + e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error encountered during B2B Product Exclusive migration: " + e.getMessage());
            log.error("Error encountered during B2B Product Exclusive migration: " + e.getMessage(), e);
        }
    }

    public void doB2BMigrationTxn() throws Exception {
        // migrate inventory
        // migrate inventory item
        // migrate offline payment
        // migrate revalidation
        // migrate mix and match
        // migrate mix and match item
    }

    public void doB2BMigration2() throws Exception {
        // migrate product group
        // update txn item
        // update pin item
    }

    public void doB2CMigration() throws Exception {
        // migrate tnc
        // migrate category
        // migrate product
        // migrate tnc map
        // migrate product order
    }

    public void doB2CMigration2() throws Exception {
        // migrate product group
    }

    public void doMFLGMigration() throws Exception {
        // migrate tnc
        // migrate category
        // migrate product
        // migrate tnc map
        // migrate product order
    }

    public void doMFLGMigration2() throws Exception {
        // migrate product group
    }

}
