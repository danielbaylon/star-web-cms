package com.enovax.star.migration.repository;

import com.enovax.star.migration.model.*;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 16/1/17.
 */
@Service
public class MigrationDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private MigrationMappingJcrRepository migrationMappingJcrRepo;

    private SQLQuery prepareSqlQuery(String query, List<Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        int i = 0;
        for (Object para : parameters) {
            queryObject.setParameter(i, para);
            i++;
        }
        return queryObject;
    }

    @Transactional
    public void insertMigrationMapping(String entity, Integer oldId, String nodeName, String uuid) {
        MigrationMappingJcr mapping = new MigrationMappingJcr();
        mapping.setEntity(entity);
        mapping.setOldId(oldId);
        mapping.setNodeName(nodeName);
        mapping.setUuid(uuid);
        migrationMappingJcrRepo.save(mapping);
    }

    @Transactional(readOnly=true)
    public String getMigrationMappingNodeName(String entity, Integer oldId) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, entity as entity, oldId as oldId, nodeName as nodeName, uuid as uuid "
            + "FROM MigrationMappingJcr "
            + "WHERE entity = ? AND oldId = ? ");
        final List<Object> paras = new ArrayList<>();
        paras.add(entity);
        paras.add(oldId);

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.INTEGER)
            .addScalar("entity", StandardBasicTypes.STRING)
            .addScalar("oldId", StandardBasicTypes.INTEGER)
            .addScalar("nodeName", StandardBasicTypes.STRING)
            .addScalar("uuid", StandardBasicTypes.STRING);

        List<Object[]> list = qry.list();
        if (list == null) {
            return null;
        }

        for (final Object[] o : list) {
            return (String) o[3];
        }
        return null;
    }

    @Transactional(readOnly=true)
    public String getMigrationMappingUuid(String entity, Integer oldId) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, entity as entity, oldId as oldId, nodeName as nodeName, uuid as uuid "
            + "FROM MigrationMappingJcr "
            + "WHERE entity = ? AND oldId = ? ");
        final List<Object> paras = new ArrayList<>();
        paras.add(entity);
        paras.add(oldId);

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.INTEGER)
            .addScalar("entity", StandardBasicTypes.STRING)
            .addScalar("oldId", StandardBasicTypes.INTEGER)
            .addScalar("nodeName", StandardBasicTypes.STRING)
            .addScalar("uuid", StandardBasicTypes.STRING);

        List<Object[]> list = qry.list();
        if (list == null) {
            return null;
        }

        for (final Object[] o : list) {
            return (String) o[4];
        }
        return null;
    }

    @Transactional(readOnly=true)
    public List<AdminAccount> getAdminAccount(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, username as username, email as email, password as password, status as status "
            + "FROM MigrationAdminAccount "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("username", StandardBasicTypes.STRING)
            .addScalar("email", StandardBasicTypes.STRING)
            .addScalar("password", StandardBasicTypes.STRING)
            .addScalar("status", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<AdminAccount> records = new ArrayList<>();
        for (final Object[] o : list) {
            final AdminAccount r = new AdminAccount();
            r.setId((String) o[0]);
            r.setUsername((String) o[1]);
            r.setEmail((String) o[2]);
            r.setPassword((String) o[3]);
            r.setStatus((String) o[4]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<Tnc> getB2BTnc(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, title as title, content as content, type as type "
            + "FROM MigrationPPSLMTNC "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("title", StandardBasicTypes.STRING)
            .addScalar("content", StandardBasicTypes.STRING)
            .addScalar("type", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<Tnc> records = new ArrayList<>();
        for (final Object[] o : list) {
            final Tnc r = new Tnc();
            r.setId((String) o[0]);
            r.setTitle((String) o[1]);
            r.setContent((String) o[2]);
            r.setType((String) o[3]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<Category> getB2BCategory(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, name as name, title as title, inactiveMessage as inactiveMessage, active as active "
            + "FROM MigrationPPSLMCategory "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("name", StandardBasicTypes.STRING)
            .addScalar("title", StandardBasicTypes.STRING)
            .addScalar("inactiveMessage", StandardBasicTypes.STRING)
            .addScalar("active", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<Category> records = new ArrayList<>();
        for (final Object[] o : list) {
            final Category r = new Category();
            r.setId((String) o[0]);
            r.setName((String) o[1]);
            r.setTitle((String) o[2]);
            r.setInactiveMessage((String) o[3]);
            r.setActive((String) o[4]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<Product> getB2BProduct(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, generalId as generalId, eventId as eventId, name as name, description as description, "
            + "productLevel as productLevel, standaline as standaline, minQty as minQty "
            + "FROM MigrationPPSLMCMSProduct "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("generalId", StandardBasicTypes.STRING)
            .addScalar("eventId", StandardBasicTypes.STRING)
            .addScalar("name", StandardBasicTypes.STRING)
            .addScalar("description", StandardBasicTypes.STRING)
            .addScalar("productLevel", StandardBasicTypes.STRING)
            .addScalar("standaline", StandardBasicTypes.STRING)
            .addScalar("minQty", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<Product> records = new ArrayList<>();
        for (final Object[] o : list) {
            final Product r = new Product();
            r.setId((String) o[0]);
            r.setGeneralId((String) o[1]);
            r.setEventId((String) o[2]);
            r.setName((String) o[3]);
            r.setDescription((String) o[4]);
            r.setProductLevel((String) o[5]);
            r.setStandalone((String) o[6]);
            r.setMinQty((String) o[7]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<TncMap> getB2BProductTnc(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, tncId as tncId, productId as productId "
            + "FROM MigrationPPSLMCMSProductTNC "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("tncId", StandardBasicTypes.STRING)
            .addScalar("productId", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<TncMap> records = new ArrayList<>();
        for (final Object[] o : list) {
            final TncMap r = new TncMap();
            r.setId((String) o[0]);
            r.setTncId((String) o[1]);
            r.setProductId((String) o[2]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<ProductOrder> getB2BProductOrder(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, categoryId as categoryId, productId as productId "
            + "FROM MigrationPPSLMCMSProductOrder "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("categoryId", StandardBasicTypes.STRING)
            .addScalar("productId", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<ProductOrder> records = new ArrayList<>();
        for (final Object[] o : list) {
            final ProductOrder r = new ProductOrder();
            r.setId((String) o[0]);
            r.setCategoryId((String) o[1]);
            r.setProductId((String) o[2]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<ExclusiveMap> getB2BProductExclusive(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, partnerId as partnerId, productId as productId, active as active "
            + "FROM MigrationPartnerProductMap "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("partnerId", StandardBasicTypes.STRING)
            .addScalar("productId", StandardBasicTypes.STRING)
            .addScalar("active", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<ExclusiveMap> records = new ArrayList<>();
        for (final Object[] o : list) {
            final ExclusiveMap r = new ExclusiveMap();
            r.setId((String) o[0]);
            r.setPartnerId((String) o[1]);
            r.setProductId((String) o[2]);
            r.setActive((String) o[3]);
            records.add(r);
        }
        return records;
    }

    @Transactional(readOnly=true)
    public List<TierMap> getB2BTierMap(int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT id as id, tierId as tierId, productId as productId "
            + "FROM MigrationTierProductMap "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("id", StandardBasicTypes.STRING)
            .addScalar("tierId", StandardBasicTypes.STRING)
            .addScalar("productId", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<TierMap> records = new ArrayList<>();
        for (final Object[] o : list) {
            final TierMap r = new TierMap();
            r.setId((String) o[0]);
            r.setTierId((String) o[1]);
            r.setProductId((String) o[2]);
            records.add(r);
        }
        return records;
    }

}
