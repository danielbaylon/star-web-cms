package com.enovax.star.migration.repository;

import javax.persistence.*;

/**
 * Created by jonathan on 16/1/17.
 */
@Entity
@Table(name = MigrationMappingJcr.TABLE_NAME)
public class MigrationMappingJcr {

    public static final String TABLE_NAME = "MigrationMappingJcr";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    @Column(name="entity")
    private String entity;
    @Column(name="oldId")
    private Integer oldId;
    @Column(name="nodeName")
    private String nodeName;
    @Column(name="uuid")
    private String uuid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getOldId() {
        return oldId;
    }

    public void setOldId(Integer oldId) {
        this.oldId = oldId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}


