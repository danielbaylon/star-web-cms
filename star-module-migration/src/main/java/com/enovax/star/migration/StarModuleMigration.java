package com.enovax.star.migration;

import com.enovax.star.migration.config.StarModuleMigrationAppConfig;
import com.enovax.star.migration.config.StarModuleMigrationWebConfig;
import com.enovax.star.migration.filter.SimpleCorsFilter;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * This class is optional and represents the configuration for the star-module-migration module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/star-module-migration</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class StarModuleMigration implements ModuleLifecycle {

    private String urlMappings = "/.migration/*";

    private DispatcherServlet dispatcherServlet;

    public String getUrlMappings() {
        return urlMappings;
    }

    public void setUrlMappings(String urlMappings) {
        this.urlMappings = urlMappings;
    }

    protected ServletContext getServletContext() {
        return Components.getComponent(ServletContext.class);
    }

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
            ServletContext servletContext = getServletContext();

            AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
            webCtx.register(StarModuleMigrationAppConfig.class, StarModuleMigrationWebConfig.class);

            dispatcherServlet = new DispatcherServlet(webCtx);

            final ServletRegistration.Dynamic dispatcherRegistration = servletContext.addServlet("migration-dispatcher", dispatcherServlet);
            dispatcherRegistration.setLoadOnStartup(1);
            dispatcherRegistration.setInitParameter("dispatchOptionsRequest", "true");
            dispatcherRegistration.setAsyncSupported(true);

            final String[] urlMappingArray = urlMappings.split(",");
            dispatcherRegistration.addMapping(urlMappingArray);

            final FilterRegistration.Dynamic corsRegistration = servletContext.addFilter("migrationCorsFilter", SimpleCorsFilter.class);
            corsRegistration.addMappingForServletNames(null, false, "migration-dispatcher");
            corsRegistration.setInitParameter("dispatchOptionsRequest", "true");
            corsRegistration.setAsyncSupported(true);
        }
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
            if (dispatcherServlet != null) {
                dispatcherServlet.destroy();
            }
        }
    }

}
