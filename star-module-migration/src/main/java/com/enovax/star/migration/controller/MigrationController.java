package com.enovax.star.migration.controller;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.enovax.star.migration.service.MigrationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by jonathan on 16/1/17.
 */
@Controller
@RequestMapping( "/migration/")
public class MigrationController {

    @Autowired
    private MigrationService migrationService;

    public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.UnexpectedSystemException,
            StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<K> handleException(String methodName, Throwable t, Class<K> resultClass) {
        if(t != null){
            log.error("!!! System exception encountered ["+methodName+"] !!!", t);
        }else{
            log.error("!!! System exception encountered ["+methodName+"] !!!");
        }
        if(t instanceof BizValidationException){
            BizValidationException be = (BizValidationException) t;
            if(be.getErrorMsg() != null && be.getErrorMsg().trim().length() > 0){
                return new ApiResult<>(false, "", be.getErrorMsg(), null);
            }
        }
        return handleUncaughtException(t, resultClass, null);
    }

    @RequestMapping(value = "b2b", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoB2BMigration(@RequestParam(name = "key", required = true) String key,
                                                             @RequestParam(name = "password", required = true) String password) {
        try {
            if (StringUtils.isEmpty(key) || StringUtils.isEmpty(password)) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            if (!"b2b".equals(key) || !"p@ssw0rd-=".equals(password) ) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            this.migrationService.doB2BMigration();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoB2BMigration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "b2c", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoB2CMigration(@RequestParam(name = "key", required = true) String key,
                                                             @RequestParam(name = "password", required = true) String password) {
        try {
            if (StringUtils.isEmpty(key) || StringUtils.isEmpty(password)) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            if (!"b2c".equals(key) || !"p@ssw0rd-=".equals(password) ) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            this.migrationService.doB2CMigration();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoB2CMigration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "mflg", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoMFLGMigration(@RequestParam(name = "key", required = true) String key,
                                                              @RequestParam(name = "password", required = true) String password) {
        try {
            if (StringUtils.isEmpty(key) || StringUtils.isEmpty(password)) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            if (!"mflg".equals(key) || !"p@ssw0rd-=".equals(password) ) {
                return new ResponseEntity<>(new ApiResult<>(false, "", "Invalid Access", null), HttpStatus.OK);
            }
            this.migrationService.doMFLGMigration();
            return new ResponseEntity<>(new ApiResult<>(true, "", "", null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoMFLGMigration", e, List.class), HttpStatus.OK);
        }
    }

}
