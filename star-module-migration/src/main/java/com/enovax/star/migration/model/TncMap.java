package com.enovax.star.migration.model;

/**
 * Created by jonathan on 16/1/17.
 */
public class TncMap {

    private String id;
    private String tncId;
    private String productId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTncId() {
        return tncId;
    }

    public void setTncId(String tncId) {
        this.tncId = tncId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
