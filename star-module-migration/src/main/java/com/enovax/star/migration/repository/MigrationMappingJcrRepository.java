package com.enovax.star.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jonathan on 16/1/17.
 */
@Repository
public interface MigrationMappingJcrRepository extends JpaRepository<MigrationMappingJcr, Integer> {
}
