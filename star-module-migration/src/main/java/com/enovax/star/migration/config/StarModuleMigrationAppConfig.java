package com.enovax.star.migration.config;

import com.enovax.star.cms.templatingkit.mgnl.functions.StarTemplatingFunctions;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@ComponentScan(
    basePackages = { "com.enovax.star.cms.commons", "com.enovax.star.migration" },
    excludeFilters = {
        @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
        @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION)
    }
)

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.enovax.star"})
@EnableScheduling
public class StarModuleMigrationAppConfig {

    protected Logger log = LoggerFactory.getLogger(StarModuleMigrationAppConfig.class);

    public static final String[] PROP_FILE_NAMES = new String[] {
        Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.db.data.source.location")
    };

    @Bean
    public static PropertySourcesPlaceholderConfigurer propsConfigurer() {
        PropertySourcesPlaceholderConfigurer bean = new PropertySourcesPlaceholderConfigurer();

        final int propCount = PROP_FILE_NAMES.length;
        final Resource[] resources = new FileSystemResource[propCount];
        for (int i = 0; i < propCount; i++) {
            resources[i] = new FileSystemResource(PROP_FILE_NAMES[i]);
        }

        bean.setLocations(resources);
        bean.setIgnoreUnresolvablePlaceholders(true);

        return bean;
    }

    private @Value("${ds.driverClass}") String driverClass;
    private @Value("${ds.jdbcUrl}") String jdbcUrl;
    private @Value("${ds.jdbcUser}") String jdbcUser;
    private @Value("${ds.jdbcPassword}") String jdbcPassword;

    private @Value("${c3p0.acquireIncrement}") Integer c3p0acquireIncrement;
    private @Value("${c3p0.idleConnectionTestPeriod}") Integer c3p0idleConnectionTestPeriod;
    private @Value("${c3p0.maxIdleTime}") Integer c3p0maxIdleTime;
    private @Value("${c3p0.maxPoolSize}") Integer c3p0maxPoolSize;
    private @Value("${c3p0.minPoolSize}") Integer c3p0minPoolSize;
    private @Value("${c3p0.maxStatementsPerConnection}") Integer c3p0maxStatementsPerConnection;
    private @Value("${c3p0.numHelperThreads}") Integer c3p0numHelperThreads;

    private @Value("${hibernate.bytecode.use_reflection_optimizer}") String hByteCodeUseReflectionOptimizer;
    private @Value("${hibernate.dialect}") String hDialect;
    private @Value("${hibernate.search.autoregister_listeners}") String hSearchAutoRegisterListeners;
    private @Value("${hibernate.show_sql}") String hShowSql;
    private @Value("${hibernate.format_sql}") String hFormatSql;

    public static final String[] PACKAGES_TO_SCAN_ENT_MGR = new String[] {
        "com.enovax.star"
    };

    @Bean
    public DataSource dataSource() {
        try {
            ComboPooledDataSource ds = new ComboPooledDataSource();

            ds.setDriverClass(driverClass);
            ds.setJdbcUrl(jdbcUrl);
            ds.setUser(jdbcUser);
            ds.setPassword(jdbcPassword);

            ds.setAcquireIncrement(c3p0acquireIncrement);
            ds.setIdleConnectionTestPeriod(c3p0idleConnectionTestPeriod);
            ds.setMaxIdleTime(c3p0maxIdleTime);
            ds.setMaxPoolSize(c3p0maxPoolSize);
            ds.setMinPoolSize(c3p0minPoolSize);
            ds.setMaxStatementsPerConnection(c3p0maxStatementsPerConnection);
            ds.setNumHelperThreads(c3p0numHelperThreads);

            return ds;
        } catch (PropertyVetoException pve) {
            log.error("Error with data source initialization.", pve);
            throw new RuntimeException(pve);
        }
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();

        bean.setDataSource(dataSource());
        bean.setPackagesToScan(PACKAGES_TO_SCAN_ENT_MGR);
        bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        final Properties jpaProps = new Properties();
        jpaProps.put("hibernate.bytecode.use_reflection_optimizer", hByteCodeUseReflectionOptimizer);
        jpaProps.put("hibernate.dialect", hDialect);
        jpaProps.put("hibernate.search.autoregister_listeners", hSearchAutoRegisterListeners);
        jpaProps.put("hibernate.show_sql", "false");
        jpaProps.put("hibernate.format_sql", hFormatSql);
        bean.setJpaProperties(jpaProps);

        return bean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager mgr = new JpaTransactionManager();
        mgr.setEntityManagerFactory(entityManagerFactory);
        return mgr;
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        HibernateExceptionTranslator bean = new HibernateExceptionTranslator();
        return bean;
    }

    @Bean
    public StarTemplatingFunctions starfn() { return Components.getComponent(StarTemplatingFunctions.class); }

    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory() throws Exception {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource());

        sessionBuilder.setProperty("hibernate.show_sql", "false");
        sessionBuilder.setProperty("hibernate.format_sql", hFormatSql);

        return sessionBuilder.buildSessionFactory();
    }

    private ThreadPoolTaskExecutor createThreadPoolTaskExecutor(int core, int max){
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(core);
        taskExecutor.setMaxPoolSize(max);
        return taskExecutor;
    }

    @Bean(name = "queuedTaskExecutor")
    public ThreadPoolTaskExecutor queuedTaskExecutor(){
        return createThreadPoolTaskExecutor(5, 20);
    }

    @Bean(name = "realTimeTaskExecutor")
    public ThreadPoolTaskExecutor realTimeTaskExecutor(){
        return createThreadPoolTaskExecutor(10, 30);
    }

}
