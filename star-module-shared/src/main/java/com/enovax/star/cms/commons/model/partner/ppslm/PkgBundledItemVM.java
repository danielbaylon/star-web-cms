package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.partner.ppslm.PkgUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class PkgBundledItemVM implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String itemCode;
    private String listingId;
    private String displayTitle;
    private List<PkgTransItemVM> transItems;
    private List<PkgTopupItemVM> topupItems;

    public PkgBundledItemVM() {

    }
    public PkgBundledItemVM(PPSLMInventoryTransactionItem item) {
        this.itemCode = item.getItemProductCode();
        this.listingId = item.getItemListingId();
        this.displayTitle = item.getDisplayName();
        transItems = new ArrayList<PkgTransItemVM>();
        topupItems = new ArrayList<PkgTopupItemVM>();
    }

    public void addTransItem(List<PPSLMInventoryTransactionItem> itransItems,
                             Integer ipkgQtyPerProd, String ipkgTktType) {
        List<PPSLMInventoryTransactionItem> oitems = PkgUtil.loadItemsToBundledItem(
                itransItems, ipkgQtyPerProd, ipkgTktType, this.itemCode, false);
        for (PPSLMInventoryTransactionItem item : oitems) {
            PkgTransItemVM transItem = new PkgTransItemVM(item);
            if (!transItems.contains(transItem)) {
                transItems.add(transItem);
            }
            for (PPSLMInventoryTransactionItem topup : item.getTopupItems()) {
                PkgTopupItemVM topupItem = new PkgTopupItemVM(topup);
                if (!topupItems.contains(topupItem)) {
                    topupItems.add(topupItem);
                }
            }
        }
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public List<PkgTransItemVM> getTransItems() {
        return transItems;
    }

    public void setTransItems(List<PkgTransItemVM> transItems) {
        this.transItems = transItems;
    }

    public List<PkgTopupItemVM> getTopupItems() {
        return topupItems;
    }

    public void setTopupItems(List<PkgTopupItemVM> topupItems) {
        this.topupItems = topupItems;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof PkgBundledItemVM))
            return false;
        PkgBundledItemVM otherMyClass = (PkgBundledItemVM) other;
        if (this.itemCode.equals(otherMyClass.getItemCode()) && this.listingId.equals(otherMyClass.getListingId())) {
            return true;
        }
        return false;
    }



//    @Override
//    public int hashCode() {
//        int result = 0;
//        result = (int) serialVersionUID + itemCode;
//        return result;
//    }

}
