package com.enovax.star.cms.commons.datamodel.ppslm;

import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Entity
@Table(name="PPSLMRevalidationTransaction")
public class PPSLMRevalidationTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "receiptNum", nullable = false)
    private String receiptNum;

    @Column(name = "transactionId", nullable = false)
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPSLMInventoryTransaction transaction;

    @Column(name = "mainAccountId", nullable = false)
    private Integer mainAccountId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainAccountId", referencedColumnName = "adminId", updatable = false, insertable = false)
    private PPSLMPartner partner;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "isSubAccountTrans", nullable = false)
    private Boolean subAccountTrans;

    @Column(name = "totalMainQty", nullable = false)
    private Integer totalMainQty;

    @Column(name = "totalTopupQty", nullable = false)
    private Integer totalTopupQty;

    @Column(name = "revalItemCode", nullable = false)
    private String revalItemCode;

    @Column(name = "revalFeeInCents", nullable = false)
    private Integer revalFeeInCents;

    @Column(name = "revalTopupItemCode")
    private Integer revalTopupItemCode;

    @Column(name = "revalTopupFeeInCents")
    private Integer revalTopupFeeInCents;

    @Column(name = "total", nullable = false)
    private BigDecimal total;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "tmMerchantId", nullable = false)
    private String tmMerchantId;

    @Column(name = "oldValidityEndDate", nullable = false)
    private Date oldValidityEndDate;

    @Column(name = "newValidityEndDate", nullable = false)
    private Date newValidityEndDate;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "tmStatus")
    private String tmStatus;

    @Column(name = "tmStatusDate")
    private Date tmStatusDate;

    @Column(name = "tmApprovalCode")
    private String tmApprovalCode;

    @Column(name = "tmErrorMessage")
    private String tmErrorMessage;

    @Column(name = "tmErrorCode")
    private String tmErrorCode;

    @Column(name = "paymentType")
    private String paymentType;

    @Column(name = "sactResponse")
    private String sactResponse;

    @Column(name = "reprintCount")
    private Integer reprintCount;


    @Column(name = "gstRate", nullable = false)
    private BigDecimal gstRate;


    @Column(name = "revalDetail")
    private String revalDetail;

    @Transient
    private List<PPSLMInventoryTransactionItem> revalItems = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public PPSLMInventoryTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(PPSLMInventoryTransaction transaction) {
        this.transaction = transaction;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public PPSLMPartner getPartner() {
        return partner;
    }

    public void setPartner(PPSLMPartner partner) {
        this.partner = partner;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public Integer getTotalMainQty() {
        return totalMainQty;
    }

    public void setTotalMainQty(Integer totalMainQty) {
        this.totalMainQty = totalMainQty;
    }

    public Integer getTotalTopupQty() {
        return totalTopupQty;
    }

    public void setTotalTopupQty(Integer totalTopupQty) {
        this.totalTopupQty = totalTopupQty;
    }

    public String getRevalItemCode() {
        return revalItemCode;
    }

    public void setRevalItemCode(String revalItemCode) {
        this.revalItemCode = revalItemCode;
    }

    public Integer getRevalFeeInCents() {
        return revalFeeInCents;
    }

    public void setRevalFeeInCents(Integer revalFeeInCents) {
        this.revalFeeInCents = revalFeeInCents;
    }

    public Integer getRevalTopupItemCode() {
        return revalTopupItemCode;
    }

    public void setRevalTopupItemCode(Integer revalTopupItemCode) {
        this.revalTopupItemCode = revalTopupItemCode;
    }

    public Integer getRevalTopupFeeInCents() {
        return revalTopupFeeInCents;
    }

    public void setRevalTopupFeeInCents(Integer revalTopupFeeInCents) {
        this.revalTopupFeeInCents = revalTopupFeeInCents;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Date getOldValidityEndDate() {
        return oldValidityEndDate;
    }

    public void setOldValidityEndDate(Date oldValidityEndDate) {
        this.oldValidityEndDate = oldValidityEndDate;
    }

    public Date getNewValidityEndDate() {
        return newValidityEndDate;
    }

    public void setNewValidityEndDate(Date newValidityEndDate) {
        this.newValidityEndDate = newValidityEndDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public Date getTmStatusDate() {
        return tmStatusDate;
    }

    public void setTmStatusDate(Date tmStatusDate) {
        this.tmStatusDate = tmStatusDate;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getTmErrorCode() {
        return tmErrorCode;
    }

    public void setTmErrorCode(String tmErrorCode) {
        this.tmErrorCode = tmErrorCode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSactResponse() {
        return sactResponse;
    }

    public void setSactResponse(String sactResponse) {
        this.sactResponse = sactResponse;
    }

    public Integer getReprintCount() {
        return reprintCount;
    }

    public void setReprintCount(Integer reprintCount) {
        this.reprintCount = reprintCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public String getRevalDetail() {
        return revalDetail;
    }

    public void setRevalDetail(String revalDetail) {
        this.revalDetail = revalDetail;
    }

    public List<PPSLMInventoryTransactionItem> getRevalItems() {
        if(revalItems == null){
            revalItems = TransactionUtil.getRevalItemsFromXml(revalDetail);
        }
        return revalItems;
    }

    public void setRevalItems(List<PPSLMInventoryTransactionItem> revalItems) {
        this.revalItems = revalItems;
    }

}
