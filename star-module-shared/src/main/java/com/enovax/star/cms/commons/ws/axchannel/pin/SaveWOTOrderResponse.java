
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SaveWOTOrderResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_SaveWOTOrderResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "saveWOTOrderResult"
})
@XmlRootElement(name = "SaveWOTOrderResponse", namespace = "http://tempuri.org/")
public class SaveWOTOrderResponse {

    @XmlElementRef(name = "SaveWOTOrderResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCSaveWOTOrderResponse> saveWOTOrderResult;

    /**
     * Gets the value of the saveWOTOrderResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCSaveWOTOrderResponse }{@code >}
     *     
     */
    public JAXBElement<SDCSaveWOTOrderResponse> getSaveWOTOrderResult() {
        return saveWOTOrderResult;
    }

    /**
     * Sets the value of the saveWOTOrderResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCSaveWOTOrderResponse }{@code >}
     *     
     */
    public void setSaveWOTOrderResult(JAXBElement<SDCSaveWOTOrderResponse> value) {
        this.saveWOTOrderResult = value;
    }

}
