package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Entity
@Table(name = "PPMFLGMixMatchPackageItem")
public class PPMFLGMixMatchPackageItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "packageId", nullable = false)
    private Integer packageId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packageId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGMixMatchPackage mmPkg;

    @Column(name = "receiptNum", nullable = false)
    private String receiptNum;

    @Column(name = "transactionItemId", nullable = false)
    private Integer transactionItemId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionItemId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGInventoryTransactionItem transItem;

    @Column(name = "displayName", nullable = false)
    private String displayName;

    @Column(name = "displayDetails", nullable = false)
    private String displayDetails;

    @Column(name = "dateOfVisit")
    private Date dateOfVisit;

    @Column(name = "eventGroupId")
    private String eventGroupId;

    @Column(name = "eventLineId")
    private String eventLineId;

    @Column(name = "eventName")
    private String eventName;

    @Column(name = "itemListingId", nullable = false)
    private String itemListingId;

    @Column(name = "itemProductCode", nullable = false)
    private String itemProductCode;

    @Column(name = "qty", nullable = false)
    private Integer qty;

    @Column(name = "qtyRedeemed")
    private Integer qtyRedeemed;

    @Column(name = "transItemType", nullable = false)
    private String transItemType;

    @Column(name = "ticketType", nullable = false)
    private String ticketType;

    @Column(name = "itemType", nullable = false)
    private String itemType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getTransactionItemId() {
        return transactionItemId;
    }

    public void setTransactionItemId(Integer transactionItemId) {
        this.transactionItemId = transactionItemId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getItemListingId() {
        return itemListingId;
    }

    public void setItemListingId(String itemListingId) {
        this.itemListingId = itemListingId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getTransItemType() {
        return transItemType;
    }

    public void setTransItemType(String transItemType) {
        this.transItemType = transItemType;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public PPMFLGMixMatchPackage getMmPkg() {
        return mmPkg;
    }

    public void setMmPkg(PPMFLGMixMatchPackage mmPkg) {
        this.mmPkg = mmPkg;
    }

    public PPMFLGInventoryTransactionItem getTransItem() {
        return transItem;
    }

    public void setTransItem(PPMFLGInventoryTransactionItem transItem) {
        this.transItem = transItem;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
