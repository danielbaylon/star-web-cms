package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.io.Serializable;

import com.enovax.star.cms.commons.model.kiosk.odata.AxConnectionRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 6 SEP 16
 */
public class KioskLogonRequest implements Serializable {

    @JsonProperty("connectionRequest")
    private AxConnectionRequest connectionRequest = new AxConnectionRequest();

    @JsonIgnore
    private String storeNumber;

    public AxConnectionRequest getConnectionRequest() {
        return connectionRequest;
    }

    public void setConnectionRequest(AxConnectionRequest connectionRequest) {
        this.connectionRequest = connectionRequest;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

}
