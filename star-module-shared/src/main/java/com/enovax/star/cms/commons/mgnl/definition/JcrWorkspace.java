package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 3/17/16.
 */
public enum JcrWorkspace {
    RemoteChannelsFolder("remote-channels", "mgnl:remote-channel-folder"),
    RemoteChannels("remote-channels", "mgnl:remote-channel"),
    CMSProducts("products", "mgnl:cms-product"),
    AXProducts("products", "mgnl:ax-product"),
    ProductCategories("product-categories", "mgnl:product-category"),
    ProductSubCategories("product-categories", "mgnl:product-sub-category"),
    ProductDefaultSubCategories("product-categories", "mgnl:product-default-sub-category"),
    Dam("dam", "mgnl:folder"),
    Resources("resources", "mgnl:folder"),
    RelatedCMSProductForCategories("product-categories", "mgnl:cms-product-for-category"),
    CMSConfig("cms-config", "mgnl:cms-config"),
    TNC("tnc", "mgnl:tnc"),
    Merchant("merchant", "mgnl:merchant"),
    MerchantDefault("merchant", "mgnl:merchant-default"),
    Announcement("announcement", "mgnl:announcement"),
    MaintenanceSchedule("schedule", "mgnl:schedule"),


    Affiliations("affiliations", "mgnl:affiliation"),
    Promotions("promotions", "mgnl:promotion"),
    DiscountCode("promotions", "mgnl:discount-code"),

    AXSyncLogs("product-sync-logs", "mgnl:ax-sync-logs"),
    AXProductSyncConfig("product-sync-config", "mgnl:content"),
	KioskInfo("kiosk-info", "mgnl:kiosk-info"),
	KioskGroup("kiosk-group", "mgnl:kiosk-group"),
	KioskGroupProductCategory("kiosk-group", "mgnl:kiosk-group-product-category"),
	KioskGroupKioskInfo("kiosk-group", "mgnl:kiosk-group-kiosk-info"),
	KioskReceiptTemplate("kiosk-receipt-template", "mgnl:kiosk-receipt-template"),
	
    //Temporary data stores
    ApiStoreDump("api-store-dump", "mgnl:folder"),
    StoreTransactions("store-transactions", "mgnl:folder"),
    StorePartners("store-partners", "mgnl:folder"),
    ProductTiers("product-tiers", "mgnl:product-tier"),
    Partners("partners", "mgnl:partner"),
    PartnerAccounts("partner-account", "mgnl:content"),
    TierProductMappings("tier-product-mappings", "mgnl:tier-product-mapping"),
    TierPartnerMappings("tier-partner-mappings", "mgnl:content"),
    InventoryTransactions("inventory-transactions", "mgnl:content"),
    InventoryTransactionItems("inventory-transaction-items", "mgnl:content"),
    MixMatchPackages("mix-match-packages", "mgnl:content"),
    MixMatchPackageItems("mix-match-package-items", "mgnl:content"),
    WOTReservations("wot-reservations", "mgnl:content"),
    PartnerDeposits("deposits", "mgnl:content"),
    AppConfigs("app-config","mgnl:app-config"),
    MailTemplates("mail-template","mgnl:mail-template"),
    Users("users","mgnl:user"),
    UserGroups("usergroups","mgnl:group"),
    Roles("roles","mgnl:role"),
    PartnerExclusiveProductMapping("partner-excl-prod-mapping","mgnl:partner-excl-prod-mapping")
    ;

    private final String workspaceName;
    private final String nodeType;

    JcrWorkspace(String name, String nodeType) {
        this.workspaceName = name;
        this.nodeType = nodeType;
    }

    public String getNodeType() {
        return nodeType;
    }

    public String getWorkspaceName() {
        return workspaceName;
    }

    public static String getNodeTypeFromWorkspace(JcrWorkspace workspace) {
        return workspace.nodeType;
    }
}
