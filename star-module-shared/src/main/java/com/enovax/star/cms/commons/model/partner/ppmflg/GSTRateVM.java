package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGGSTRate;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by lavanya on 24/10/16.
 */
public class GSTRateVM {
    private Integer id;
    private Integer rateInInt;
    private Date startDate;
    private Date endDate;
    private String startDateStr;
    private String endDateStr;
    private String status;
    private String statusLabel;

    public GSTRateVM() {
    }

    public GSTRateVM(PPMFLGGSTRate gst) {
        this.id = gst.getId();
        this.rateInInt = gst.getRate().multiply(new BigDecimal(100)).intValue();
        this.startDateStr = gst.getStartDate() == null ? "" : NvxDateUtils
                .formatDate(gst.getStartDate(),
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.endDateStr = gst.getEndDate() == null ? "" : NvxDateUtils.formatDate(
                gst.getEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.status = gst.getStatus();
        this.statusLabel = GeneralStatus.getLabel(gst.getStatus());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRateInInt() {
        return rateInInt;
    }

    public void setRateInInt(Integer rateInInt) {
        this.rateInInt = rateInInt;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getEndDate() {
        try {
            endDate = NvxDateUtils.parseDate(endDateStr,
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        try {
            startDate = NvxDateUtils.parseDate(startDateStr,
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }
}
