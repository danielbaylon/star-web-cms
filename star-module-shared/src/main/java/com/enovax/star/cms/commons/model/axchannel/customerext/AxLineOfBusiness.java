package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by houtao on 22/9/16.
 */
public class AxLineOfBusiness {

    private String description;
    private String lineOfBusinessId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLineOfBusinessId() {
        return lineOfBusinessId;
    }

    public void setLineOfBusinessId(String lineOfBusinessId) {
        this.lineOfBusinessId = lineOfBusinessId;
    }
}
