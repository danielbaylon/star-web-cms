package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppmflg.TransactionUtil;

import java.util.Date;

public class TransQueryVM {
    private Integer id;
    private String receiptNum;
    private Date createdDate;
    private String orgName;
    private String username;
    private String status;
    private String transType;
    private String linkedTrans;
    private String createdDateStr;
    private String paymentType;
    private String offlinePaymentStatus;
    private Integer offlinePaymentId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return TransactionUtil.getAdminStatus(this.status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getLinkedTrans() {
        return linkedTrans;
    }

    public void setLinkedTrans(String linkedTrans) {
        this.linkedTrans = linkedTrans;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateStr() {
        createdDateStr = NvxDateUtils.formatDate(this.createdDate,
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }


    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOfflinePaymentStatus() {
        return offlinePaymentStatus;
    }

    public void setOfflinePaymentStatus(String offlinePaymentStatus) {
        this.offlinePaymentStatus = offlinePaymentStatus;
    }

    public Integer getOfflinePaymentId() {
        return offlinePaymentId;
    }

    public void setOfflinePaymentId(Integer offlinePaymentId) {
        this.offlinePaymentId = offlinePaymentId;
    }
}
