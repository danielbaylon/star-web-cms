
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="custDepositDetails" type="{http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels}DepositDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "custDepositDetails"
})
@XmlRootElement(name = "TopUpCustomerDeposit")
public class TopUpCustomerDeposit {

    @XmlElementRef(name = "custDepositDetails", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<DepositDetails> custDepositDetails;

    /**
     * Gets the value of the custDepositDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DepositDetails }{@code >}
     *     
     */
    public JAXBElement<DepositDetails> getCustDepositDetails() {
        return custDepositDetails;
    }

    /**
     * Sets the value of the custDepositDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DepositDetails }{@code >}
     *     
     */
    public void setCustDepositDetails(JAXBElement<DepositDetails> value) {
        this.custDepositDetails = value;
    }

}
