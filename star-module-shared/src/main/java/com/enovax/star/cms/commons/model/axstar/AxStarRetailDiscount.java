package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AxStarRetailDiscount {

    //Unused variables

//    private List<String> thresholdDiscountTiers;

    private String offerId;
    private String currencyCode;
    private Long priceDiscountGroup;
    private String name;
    private Integer periodicDiscountType;
    private Integer concurrencyMode;
    private boolean isDiscountCodeRequired;
    private String validationPeriodId = "";
    private Integer dateValidationType;
    private String validFromDate;
    private String validToDate;
    private Integer discountType;
    private BigDecimal mixAndMatchDealPrice;
    private BigDecimal mixAndMatchDiscountPercent;
    private BigDecimal mixAndMatchDiscountAmount;
    private Integer mixAndMatchNumberOfLeastExpensiveLines;
    private Integer mixAndMatchNumberOfTimeApplicable;
    private Integer shouldCountNonDiscountItems;
    private List<AxStarRetailDiscountLine> discountLines = new ArrayList<>();
    private List<AxStarRetailDiscountPriceGroup> priceGroups;
    private List<AxStarRetailDiscountMultibuyQuantityTier> multibuyQuantityTiers;
    private List<AxStarRetailDiscountDiscountCode> discountCodes;

    private List<AxStarExtensionProperty> extensionProperties;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Long getPriceDiscountGroup() {
        return priceDiscountGroup;
    }

    public void setPriceDiscountGroup(Long priceDiscountGroup) {
        this.priceDiscountGroup = priceDiscountGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPeriodicDiscountType() {
        return periodicDiscountType;
    }

    public void setPeriodicDiscountType(Integer periodicDiscountType) {
        this.periodicDiscountType = periodicDiscountType;
    }

    public Integer getConcurrencyMode() {
        return concurrencyMode;
    }

    public void setConcurrencyMode(Integer concurrencyMode) {
        this.concurrencyMode = concurrencyMode;
    }

    public boolean isDiscountCodeRequired() {
        return isDiscountCodeRequired;
    }

    public void setDiscountCodeRequired(boolean discountCodeRequired) {
        isDiscountCodeRequired = discountCodeRequired;
    }

    public String getValidationPeriodId() {
        return validationPeriodId;
    }

    public void setValidationPeriodId(String validationPeriodId) {
        this.validationPeriodId = validationPeriodId;
    }

    public Integer getDateValidationType() {
        return dateValidationType;
    }

    public void setDateValidationType(Integer dateValidationType) {
        this.dateValidationType = dateValidationType;
    }

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public Integer getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    public BigDecimal getMixAndMatchDealPrice() {
        return mixAndMatchDealPrice;
    }

    public void setMixAndMatchDealPrice(BigDecimal mixAndMatchDealPrice) {
        this.mixAndMatchDealPrice = mixAndMatchDealPrice;
    }

    public BigDecimal getMixAndMatchDiscountPercent() {
        return mixAndMatchDiscountPercent;
    }

    public void setMixAndMatchDiscountPercent(BigDecimal mixAndMatchDiscountPercent) {
        this.mixAndMatchDiscountPercent = mixAndMatchDiscountPercent;
    }

    public BigDecimal getMixAndMatchDiscountAmount() {
        return mixAndMatchDiscountAmount;
    }

    public void setMixAndMatchDiscountAmount(BigDecimal mixAndMatchDiscountAmount) {
        this.mixAndMatchDiscountAmount = mixAndMatchDiscountAmount;
    }

    public Integer getMixAndMatchNumberOfLeastExpensiveLines() {
        return mixAndMatchNumberOfLeastExpensiveLines;
    }

    public void setMixAndMatchNumberOfLeastExpensiveLines(Integer mixAndMatchNumberOfLeastExpensiveLines) {
        this.mixAndMatchNumberOfLeastExpensiveLines = mixAndMatchNumberOfLeastExpensiveLines;
    }

    public Integer getMixAndMatchNumberOfTimeApplicable() {
        return mixAndMatchNumberOfTimeApplicable;
    }

    public void setMixAndMatchNumberOfTimeApplicable(Integer mixAndMatchNumberOfTimeApplicable) {
        this.mixAndMatchNumberOfTimeApplicable = mixAndMatchNumberOfTimeApplicable;
    }

    public Integer getShouldCountNonDiscountItems() {
        return shouldCountNonDiscountItems;
    }

    public void setShouldCountNonDiscountItems(Integer shouldCountNonDiscountItems) {
        this.shouldCountNonDiscountItems = shouldCountNonDiscountItems;
    }

    public List<AxStarRetailDiscountLine> getDiscountLines() {
        return discountLines;
    }

    public void setDiscountLines(List<AxStarRetailDiscountLine> discountLines) {
        this.discountLines = discountLines;
    }

    public List<AxStarRetailDiscountPriceGroup> getPriceGroups() {
        return priceGroups;
    }

    public void setPriceGroups(List<AxStarRetailDiscountPriceGroup> priceGroups) {
        this.priceGroups = priceGroups;
    }

    public List<AxStarRetailDiscountMultibuyQuantityTier> getMultibuyQuantityTiers() {
        return multibuyQuantityTiers;
    }

    public void setMultibuyQuantityTiers(List<AxStarRetailDiscountMultibuyQuantityTier> multibuyQuantityTiers) {
        this.multibuyQuantityTiers = multibuyQuantityTiers;
    }

    public List<AxStarRetailDiscountDiscountCode> getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(List<AxStarRetailDiscountDiscountCode> discountCodes) {
        this.discountCodes = discountCodes;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    /*
    [Sample JSON]
    {
    "OfferId": "SLM-000001",
    "CurrencyCode": "SGD",
    "PriceDiscountGroup": 5637147576,
    "Name": "10% disc",
    "PeriodicDiscountType": 2,
    "ConcurrencyMode": 0,
    "IsDiscountCodeRequired": false,
    "ValidationPeriodId": "",
    "DateValidationType": 1,
    "ValidFromDate": "1900-01-01T00:00:00+00:00",
    "ValidToDate": "2154-12-31T00:00:00+08:00",
    "DiscountType": 2,
    "MixAndMatchDealPrice": 0,
    "MixAndMatchDiscountPercent": 10,
    "MixAndMatchDiscountAmount": 0,
    "MixAndMatchNumberOfLeastExpensiveLines": 0,
    "MixAndMatchNumberOfTimeApplicable": 0,
    "ShouldCountNonDiscountItems": 0,
    "DiscountLines": [
      {
        "OfferId": "SLM-000001",
        "DiscountLineNumber": 1,
        "DiscountLinePercentOrValue": 0,
        "MixAndMatchLineGroup": "",
        "MixAndMatchLineSpecificDiscountType": 0,
        "MixAndMatchLineNumberOfItemsNeeded": 0,
        "DiscountMethod": 0,
        "DiscountAmount": 0,
        "DiscountPercent": 10,
        "OfferPrice": 0,
        "OfferPriceIncludingTax": 0,
        "UnitOfMeasureSymbol": "Ea",
        "CategoryId": 5637150592,
        "ProductId": 5637145331,
        "DistinctProductVariantId": 0,
        "ExtensionProperties": [
          {
            "Key": "DATAAREAID",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": "slm"
            }
          }
        ]
      }
    ],
    "PriceGroups": [
      {
        "OfferId": "SLM-000001",
        "RecordId": 5637152076,
        "PriceGroupId": 5637147576,
        "GroupId": "B2C",
        "ExtensionProperties": [
          {
            "Key": "DATAAREAID",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": "slm"
            }
          }
        ]
      }
    ],
    "DiscountCodes": [],
    "MultibuyQuantityTiers": [],
    "ThresholdDiscountTiers": [],
    "ExtensionProperties": [
      {
        "Key": "DATAAREAID",
        "Value": {
          "BooleanValue": null,
          "ByteValue": null,
          "DecimalValue": null,
          "DateTimeOffsetValue": null,
          "IntegerValue": null,
          "LongValue": null,
          "StringValue": "slm"
        }
      }
    ]
  }
     */
}
