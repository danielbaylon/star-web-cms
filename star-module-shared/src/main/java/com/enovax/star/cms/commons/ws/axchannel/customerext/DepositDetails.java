
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DepositDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DepositDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="AccountNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostingDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TenderTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Txt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepositDetails", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", propOrder = {
    "accountNum",
    "amount",
    "channelId",
    "currencyCode",
    "documentDate",
    "documentNum",
    "dueDate",
    "postingDate",
    "tenderTypeId",
    "txt"
})
public class DepositDetails
    extends CommerceEntity
{

    @XmlElementRef(name = "AccountNum", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountNum;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "ChannelId")
    protected Long channelId;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "DocumentDate", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentDate;
    @XmlElementRef(name = "DocumentNum", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentNum;
    @XmlElementRef(name = "DueDate", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dueDate;
    @XmlElementRef(name = "PostingDate", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> postingDate;
    @XmlElementRef(name = "TenderTypeId", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tenderTypeId;
    @XmlElementRef(name = "Txt", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", type = JAXBElement.class, required = false)
    protected JAXBElement<String> txt;

    /**
     * Gets the value of the accountNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountNum() {
        return accountNum;
    }

    /**
     * Sets the value of the accountNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountNum(JAXBElement<String> value) {
        this.accountNum = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the channelId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getChannelId() {
        return channelId;
    }

    /**
     * Sets the value of the channelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setChannelId(Long value) {
        this.channelId = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the documentDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentDate() {
        return documentDate;
    }

    /**
     * Sets the value of the documentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentDate(JAXBElement<String> value) {
        this.documentDate = value;
    }

    /**
     * Gets the value of the documentNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentNum() {
        return documentNum;
    }

    /**
     * Sets the value of the documentNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentNum(JAXBElement<String> value) {
        this.documentNum = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDueDate(JAXBElement<String> value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPostingDate(JAXBElement<String> value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the tenderTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTenderTypeId() {
        return tenderTypeId;
    }

    /**
     * Sets the value of the tenderTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTenderTypeId(JAXBElement<String> value) {
        this.tenderTypeId = value;
    }

    /**
     * Gets the value of the txt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTxt() {
        return txt;
    }

    /**
     * Sets the value of the txt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTxt(JAXBElement<String> value) {
        this.txt = value;
    }

}
