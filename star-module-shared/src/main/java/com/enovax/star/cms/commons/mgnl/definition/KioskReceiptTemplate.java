package com.enovax.star.cms.commons.mgnl.definition;

public enum KioskReceiptTemplate {

	Logo("logo"),
	CompanyName("companyName"),
	Address("address"),
	PostalCode("postalCode"),
	ContactDetails("contactDetails"),
    GSTDetails("gstNumber");
	
	private String propertyName;

	KioskReceiptTemplate(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}

}