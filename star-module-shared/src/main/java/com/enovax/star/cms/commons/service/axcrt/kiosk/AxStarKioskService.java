package com.enovax.star.cms.commons.service.axcrt.kiosk;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.axstar.AxStarResponseError;
import com.enovax.star.cms.commons.model.axstar.AxStarServiceResult;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCart;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCommerceProperty;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCommercePropertyValue;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDeviceActivationResult;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDeviceConfiguration;
import com.enovax.star.cms.commons.model.kiosk.odata.AxDiscountLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEmployee;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventAllocationEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventDate;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventDateList;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroup;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroupLineEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroupList;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroupTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventLineList;
import com.enovax.star.cms.commons.model.kiosk.odata.AxFacilityClassEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxHardwareProfile;
import com.enovax.star.cms.commons.model.kiosk.odata.AxHardwareProfileCashDrawer;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableVariantExtEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxJournalTransaction;
import com.enovax.star.cms.commons.model.kiosk.odata.AxNumberSequenceSeedData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartTransactionData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPackageLineEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPrintTokenEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProduct;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProductProperty;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProductPropertyTranslation;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrder;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrderSearch;
import com.enovax.star.cms.commons.model.kiosk.odata.AxShift;
import com.enovax.star.cms.commons.model.kiosk.odata.AxTemplateXMLEntityResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.AxTenderLine;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdatedTicketStatusEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxUseDiscountCounter;
import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellLineEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KiioskGetHardwareProfileByIdRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskActivationConnectionRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskAddCartLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskAddTenderLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskBlacklistTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutCancelRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutCompleteRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutPinStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartCheckoutStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartDeactivateTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartPinValidateAndReserveRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketReprintStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketSearchCriteriaRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartTicketUpdateReprintSuccessRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartValidateAndReserveRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCartValidateQuantityRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskCloseShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskDeviceActivationRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetCartsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetReceiptsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetRedemptionPinStartRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetShiftsByStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskGetUpCrossSellRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskLogoffRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskLogonRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskOpenShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskPINRedemptionUnlockRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskProceedPrintedRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSalesOrdersSearchRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchJournalTransactionsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchProductExtRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSearchXmlTemplateRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskSendRedeemTicketRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdateCartLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUpdatePrintStatusRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskUseShiftRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidCartLinesRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidCartsRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.request.KioskVoidTenderLineRequest;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutCompleteResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutStartPINResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartCheckoutStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartDeactivateTicketResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketReprintStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketSearchResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdatePrintStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartTicketUpdateReprintSuccessResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateAndReserveResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskCartValidateQuantityResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetLatestNumberSequenceResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetPINRedemptionStartResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetProductsResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetReceiptsResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetShiftsByStatusResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskGetUpCrossSellResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskPINRedemptionUnlockResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSalesOrdersSearchResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchJournalTransactionsResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchProductExtResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSearchXmlTemplateResponse;
import com.enovax.star.cms.commons.model.kiosk.odata.response.KioskSendRedeemTicketToAXResponse;
import com.enovax.star.cms.commons.service.axcrt.AxStarRestRequestHandler;
import com.enovax.star.cms.commons.service.axcrt.AxStarRestResponseErrorHandler;
import com.enovax.star.cms.commons.service.axcrt.AxStarRestResponseHandler;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;

@SuppressWarnings({ "UnnecessaryLocalVariable", "Duplicates" })
@Service
public class AxStarKioskService {

    private Logger log = LoggerFactory.getLogger(AxStarKioskService.class);

    public static final String URL_FRAG_PRODUCT_GET = "Product"; // Get all Ax
                                                                 // Products
                                                                 // with valid
                                                                 // validity
    public static final String URL_FRAG_RETAIL_DISCOUNTS_GET = "RetailDiscounts";
    public static final String URL_FRAG_AFFILIATIONS_GET = "Affiliation";
    public static final String URL_FRAG_CART_GET = "ShoppingCart";
    public static final String URL_FRAG_CART_ADD = "ShoppingCart/Add";
    public static final String URL_FRAG_CART_UPDATE = "ShoppingCart/Update";
    public static final String URL_FRAG_CART_REMOVE = "ShoppingCart/Remove";
    public static final String URL_FRAG_CART_DELETE = "ShoppingCart/Delete";
    public static final String URL_FRAG_CHECKOUT = "Checkout";
    public static final String URL_FRAG_CART_ADD_PROMOTION_CODE = "ShoppingCart/PromotionCode/Add";
    public static final String URL_FRAG_CART_ADD_AFFILIATION = "ShoppingCart/Affiliation/Add";
    public static final String URL_FRAG_CART_REMOVE_AFFILIATION = "ShoppingCart/Affiliation/Remove";
    public static final String URL_FRAG_CHECKOUT_PAYMENT_CARD_TYPES = "Checkout/PaymentCardTypes";
    public static final String URL_FRAG_SALES_ORDER_CREATE = "SalesOrder/Create";
    public static final String URL_FRAG_CUSTOMER_GET = "Customer";
    public static final String URL_FRAG_CUSTOMER_CREATE = "Customer/Create";
    public static final String URL_FRAG_CUSTOMER_UPDATE = "Customer/Update";
    public static final String URL_FRAG_GET_SINGLE_ITEMS_PRICE = "GetItemPrice";
    public static final String URL_FRAG_GET_ITEMS_PRICE = "GetItemsPrice";
    public static final String URL_FRAG_GET_CUSTOMER_GROUP = "Customer/Groups";
    public static final String URL_FRAG_GET_COUNTRY_REGIONS = "CountryRegions";
    public static final String URL_FRAG_GET_PRODUCT_CATALOG = "ProductCatalog";

    public static final String URL_FRAG_KIOSK_LOGON = "LogOn";
    public static final String URL_FRAG_KIOSK_LOGOFF = "LogOff";
    public static final String URL_FRAG_KIOSK_ACTIVATE_DEVICE = "ActivateDevice";
    public static final String URL_FRAG_KIOSK_GET_DEVICE_CONFIG = "GetDeviceConfiguration";
    public static final String URL_FRAG_KIOOSK_GET_HARDWARE_PROFILE = "GetHardwareProfileById";
    public static final String URL_FRAG_KIOSK_SEARCH_XML_TEMPLATE = "SearchXmlTemplate";
    public static final String URL_FRAG_KIOSK_GET_LATEST_NUMBER_SEQUENCE = "GetLatestNumberSequence";
    public static final String URL_FRAG_KIOSK_PRODUCT_SEARCH_EXT = "InventTableExtCollection/SearchProductExt";
    public static final String URL_FRAG_KIOSK_CART_VALIDATE_QUANTITY = "CartRetailTicketCollection/CartValidateQuantity";
    public static final String URL_FRAG_KIOSK_USE_DISCOUNT_COUNTER = "UseDiscountCounter";
    public static final String URL_FRAG_KIOSK_CARTS = "Carts";
    public static final String URL_FRAG_KIOSK_ADD_CART_LINES = "AddCartLines";
    public static final String URL_FRAG_KIOSK_UPDATE_CART_LINES = "UpdateCartLines";
    public static final String URL_FRAG_KIOSK_VOID_CART = "Void";
    public static final String URL_FRAG_KIOSK_VOID_CART_LINES = "VoidCartLines";
    public static final String URL_FRAG_KIOSK_CART_VALIDATE_AND_RESERVE = "CartRetailTicketCollection/CartValidateAndReserve";
    public static final String URL_FRAG_KIOSK_CARTS_ADD_TENDER_LINE = "AddTenderLine";
    public static final String URL_FRAG_KIOSK_CARTS_VOID_TENDER_LINE = "VoidTenderLine";
    public static final String URL_FRAG_KIOSK_CART_CHECKOUT_START = "CartRetailTicketCollection/CartCheckoutStart";
    public static final String URL_FRAG_KIOSK_CARTS_CHECKOUT = "Checkout";
    public static final String URL_FRAG_KIOSK_SALE_ORDER_GET_RECEIPTS = "GetReceipts";
    public static final String URL_FRAG_KIOSK_CART_CHECKOUT_COMPLETE = "CartRetailTicketCollection/CartCheckoutComplete";
    public static final String URL_FRAG_KIOSK_CART_CHECKOUT_CANCEL = "CartRetailTicketCollection/CartCheckoutCancel";
    public static final String URL_FRAG_KIOSK_SHIFTS_GET_BY_STATUS = "Shifts/GetByStatus";
    public static final String URL_FRAG_KIOSK_SHIFTS_USE = "Use";
    public static final String URL_FRAG_KIOSK_SHIFTS_OPEN = "Shifts/Open";
    public static final String URL_FRAG_KIOSK_SHIFTS_CLOSE = "Close";
    public static final String URL_FRAG_KIOSK_GET_PRODUCTS = "Products";
    public static final String URL_FRAG_KIOSK_GET_UPCROSSELL = "GetUpCrossSellTable";
    public static final String URL_FRAG_KIOSK_GET_PIN_REDEMPTION_START = "GetPINRedemptionStart";
    public static final String URL_FRAG_KIOSK_CART_CHECKOUT_START_PIN = "CartRetailTicketCollection/CartCheckoutStartPin";
    public static final String URL_FRAG_KIOSK_CART_TICKET_UPDATE_PRINT_STATUS = "CartRetailTicketCollection/CartTicketUpdatePrintStatus";
    public static final String URL_FRAG_KIOSK_PROCEED_WITH_PRINTED = "CartRetailTicketCollection/ProceedWithPrinted";
    public static final String URL_FRAG_KIOSK_SEND_REDEEM_TICKET_TO_AX = "CartRetailTicketCollection/SendRedeemTicketToAX";
    public static final String URL_FRAG_KIOSK_PIN_REDEMPTION_UNLOCK = "PINRedemptionUnlock";
    public static final String URL_FRAG_KIOSK_CART_TICKET_SEARCH = "CartRetailTicketCollection/CartTicketSearch";
    public static final String URL_FRAG_KIOSK_SEARCH_JOURNAL_TRANSACTION = "SearchJournalTransactions";
    public static final String URL_FRAG_KIOSK_SALES_ORDERS_SEARCH = "SalesOrders/Search";
    public static final String URL_FRAG_KIOSK_CART_TICKET_REPRINT_START = "CartRetailTicketCollection/CartTicketReprintStart";
    public static final String URL_FRAG_KIOSK_CART_TICKET_REPRINT_SUCCESS = "CartRetailTicketCollection/UpdateReprintSuccess";
    public static final String URL_FRAG_KIOSK_CART_DEACTIVATE_TICKET = "CartRetailTicketCollection/DeactivateTicket";

    public static final String URL_FRAG_KIOSK_BLACKLIST_TICKET = "CartRetailTicketCollection/BlacklistTicket";

    public static final String RESP_ERROR_DESERIALISATION_ERROR_MESSAGE = "Unable to deserialise object.";
    public static final String RESP_ERROR_TYPE_DESERIALISE_ERROR = "AxStar.AxStarResponseError.UnableToDeserialise";
    public static final String RESP_ERROR_TYPE_DESERIALISE_NORMAL = "AxStar.NormalResult.UnableToDeserialise";
    public static final String RESP_ERROR_TYPE_REST_GENERAL_ERROR = "AxStar.RestError.General";

    private final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

    private String apiUrlKIOSKSLM = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.kioskslm");
    private String apiUrlKIOSKMFLG = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.kioskmflg");
    private String slmAxChannelServiceURL = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.kiosk.slm");
    private String mflgAxChannelServiceURL = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.kiosk.mflg");

    private KioskLogonRequest kioskLogonRequest;

    @Autowired
    private AxStarRestResponseHandler responseHandler;

    @Autowired
    private AxStarRestRequestHandler requestHandler;

    private Map<StoreApiChannels, String> channelUrls = new HashMap<>();

    private Map<StoreApiChannels, String> kioskChannelUrls = new HashMap<>();

    private Map<String, String> kioskChannelCookie = new HashMap<>();

    @PostConstruct
    private void initService() {
        channelUrls.put(StoreApiChannels.KIOSK_SLM, apiUrlKIOSKSLM);
        channelUrls.put(StoreApiChannels.KIOSK_MFLG, apiUrlKIOSKMFLG);

        kioskChannelUrls.put(StoreApiChannels.KIOSK_SLM, slmAxChannelServiceURL);
        kioskChannelUrls.put(StoreApiChannels.KIOSK_MFLG, mflgAxChannelServiceURL);
    }

    private boolean isNotFoundError(String toAnalyze) {
        return StringUtils.isNotEmpty(toAnalyze) && toAnalyze.contains("was not found.");
    }

    private HttpHeaders generateHeaders(StoreApiChannels channel) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (StoreApiChannels.KIOSK_SLM == channel || StoreApiChannels.KIOSK_MFLG == channel) {
            this.getCookie(headers, channel.code);
        }
        return headers;
    }

    public AxStarServiceResult<String> doRestExchangeString(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, String requestBodyJsonString) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyJsonString, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, String.class);
    }

    public <T> AxStarServiceResult<T> doRestExchangeString(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, String requestBodyJsonString,
            Class<T> expectedResponseClazz) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyJsonString, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, expectedResponseClazz);
    }

    private Boolean retryLogOn(StoreApiChannels channel, Integer retryTimes) {

        Boolean loggedOn = Boolean.FALSE;

        retryTimes = retryTimes--;
        log.debug("retry logon " + retryTimes);
        this.kioskChannelCookie.remove(channel.code);
        String url = channelUrls.get(channel) + URL_FRAG_KIOSK_LOGON;
        Integer sequence = 0;
        if (retryTimes > 0 && kioskLogonRequest != null) {
            kioskLogonRequest.getConnectionRequest().setTransactionId("-1");
            HttpEntity<KioskLogonRequest> logonRequestEntity = new HttpEntity<>(kioskLogonRequest, generateHeaders(channel));
            AxStarServiceResult<AxEmployee> logonResult = doRestExchange(url, HttpMethod.POST, logonRequestEntity, AxEmployee.class);
            if (logonResult.isSuccess()) {
                AxStarServiceResult<KioskGetLatestNumberSequenceResponse> getLatestNumberSequenceResult = this.apiKioskGetLatestNumberSequence(channel);
                if (getLatestNumberSequenceResult.isSuccess() && getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList() != null) {
                    List<AxNumberSequenceSeedData> axNoSeqSeedDataList = getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList();
                    AxNumberSequenceSeedData transactionSequence = axNoSeqSeedDataList.stream().filter(s -> "8".equals(s.getDataTypeValue())).findAny().orElse(null);
                    sequence = new Integer(transactionSequence.getDataValue()) + 1;
                    url = channelUrls.get(channel) + URL_FRAG_KIOSK_LOGOFF;
                    KioskLogoffRequest logOffRequest = new KioskLogoffRequest();
                    HttpEntity<KioskLogoffRequest> logOffRequestEntity = new HttpEntity<>(logOffRequest, generateHeaders(channel));
                    AxStarServiceResult<String> logOffResult = doRestExchange(url, HttpMethod.POST, logOffRequestEntity, String.class);
                    if (logOffResult.isSuccess()) {
                        this.kioskChannelCookie.clear();
                        url = channelUrls.get(channel) + URL_FRAG_KIOSK_LOGON;
                        String transactionId = kioskLogonRequest.getStoreNumber() + "-" + kioskLogonRequest.getConnectionRequest().getDeviceNumber() + "-" + sequence;
                        kioskLogonRequest.getConnectionRequest().setTransactionId(transactionId);
                        logonRequestEntity = new HttpEntity<>(kioskLogonRequest, generateHeaders(channel));
                        logonResult = doRestExchange(url, HttpMethod.POST, logonRequestEntity, AxEmployee.class);
                        if (logonResult.isSuccess()) {
                            loggedOn = Boolean.TRUE;
                        } else {
                            loggedOn = this.retryLogOn(channel, retryTimes);
                        }
                    }
                }
            }
        }

        return loggedOn;
    }

    private Boolean retryLogonWhenStorageException(StoreApiChannels channel, AxStarServiceResult<?> result) {
        Boolean logon = Boolean.FALSE;
        if (kioskLogonRequest != null && result.getRawData() != null && result.getRawData().contains("StorageException")) {
            logon = this.retryLogOn(channel, 4);
        }
        return logon;
    }

    private Boolean logonWhenAccessDenied(StoreApiChannels channel, AxStarServiceResult<?> result) {
        Boolean logon = Boolean.FALSE;
        if (kioskLogonRequest != null && result.getRawData() != null && result.getRawData().contains("Access is denied due to invalid credentials")) {
            this.kioskChannelCookie.remove(channel.code);
            kioskLogonRequest.getConnectionRequest().setTransactionId("-1");
            this.apiKioskLogon(channel, this.kioskLogonRequest);
            AxStarServiceResult<KioskGetLatestNumberSequenceResponse> getLatestNumberSequenceResult = this.apiKioskGetLatestNumberSequence(channel);
            this.apiKioskLogoff(channel, new KioskLogoffRequest());

            if (getLatestNumberSequenceResult.isSuccess() && getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList() != null) {
                List<AxNumberSequenceSeedData> axNoSeqSeedDataList = getLatestNumberSequenceResult.getData().getAxNumberSequenceSeedDataList();

                AxNumberSequenceSeedData transactionSequence = axNoSeqSeedDataList.stream().filter(s -> "8".equals(s.getDataTypeValue())).findAny().orElse(null);

                if (transactionSequence != null) {
                    String sequence = transactionSequence.getDataValue();

                    String transactionId = kioskLogonRequest.getStoreNumber() + "-" + kioskLogonRequest.getConnectionRequest().getDeviceNumber() + "-" + sequence;
                    kioskLogonRequest.getConnectionRequest().setTransactionId(transactionId);
                    if (this.apiKioskLogon(channel, kioskLogonRequest).isSuccess()) {
                        logon = Boolean.TRUE;
                    }
                }
            }
        }
        return logon;
    }

    public <T, U> AxStarServiceResult<T> doRestExchange(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, U requestObject,
            Class<T> expectedResponseClazz) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<U> requestEntity = new HttpEntity<>(requestObject, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, expectedResponseClazz);
    }

    public <T, U> AxStarServiceResult<T> doKioskRestExchange(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, U requestObject,
            Class<T> expectedResponseClazz) {
        final String url = kioskChannelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<U> requestEntity = new HttpEntity<>(requestObject, generateHeaders(channel));

        AxStarServiceResult<T> result = doRestExchange(url, requestHttpMethod, requestEntity, expectedResponseClazz);

        if (!apiUrlFrag.contains(URL_FRAG_KIOSK_LOGON) && !apiUrlFrag.contains(URL_FRAG_KIOSK_LOGOFF) && !apiUrlFrag.contains(URL_FRAG_KIOSK_GET_LATEST_NUMBER_SEQUENCE)
                && logonWhenAccessDenied(channel, result)) {
            result = doRestExchange(channel, apiUrlFrag, requestHttpMethod, requestObject, expectedResponseClazz);
        } else if (apiUrlFrag.contains(URL_FRAG_KIOSK_LOGON) && retryLogonWhenStorageException(channel, result)) {
            result = doRestExchange(channel, apiUrlFrag, requestHttpMethod, requestObject, expectedResponseClazz);
        }
        return result;
    }

    public <T> AxStarServiceResult<T> doRestExchange(String url, HttpMethod requestHttpMethod, HttpEntity<?> requestEntity, Class<T> expectedResponseClazz) {
        final RestTemplate client = new RestTemplate();
        client.setErrorHandler(new AxStarRestResponseErrorHandler());
        SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) client.getRequestFactory();
        rf.setOutputStreaming(Boolean.FALSE);
        rf.setReadTimeout(300 * 1000);// todo:
        rf.setConnectTimeout(300 * 1000);// todo:

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(new LoggingRequestInterceptor());
        client.setInterceptors(interceptors);

        // Looks like this isn't the problem. (Setting property naming strategy
        // to upper camel case)
        // List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        // MappingJackson2HttpMessageConverter jsonMessageConverter = new
        // MappingJackson2HttpMessageConverter();
        // jsonMessageConverter.setObjectMapper(new
        // ObjectMapper().setPropertyNamingStrategy(new
        // PropertyNamingStrategy.UpperCamelCaseStrategy()));
        // messageConverters.add(jsonMessageConverter);
        // client.setMessageConverters(messageConverters);

        final AxStarServiceResult<T> result = new AxStarServiceResult<>();
        try {

            log.info("Calling REST API: " + url);
            final ResponseEntity<String> response = client.exchange(url, requestHttpMethod, requestEntity, String.class);
            final HttpStatus statusCode = response.getStatusCode();
            final String responseBody = response.getBody();
            this.saveCookie(url, response);
            log.info("Response Status: " + statusCode);
            log.info("Response Body [RAW]: " + responseBody);

            result.setRawData(responseBody);

            final boolean isError = statusCode.is4xxClientError() || statusCode.is5xxServerError();
            if (isError) {
                AxStarResponseError axStarResponseError;
                try {
                    axStarResponseError = responseHandler.handleResponse(responseBody, AxStarResponseError.class);
                } catch (Exception e) {
                    log.error("Error deserialising JSON.", e);
                    axStarResponseError = new AxStarResponseError();
                    axStarResponseError.setExceptionType(RESP_ERROR_TYPE_DESERIALISE_ERROR);
                    axStarResponseError.setExceptionMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setStackTrace("Contents of body: " + responseBody);
                }

                result.setSuccess(false);
                result.setError(axStarResponseError);
            } else {
                try {

                    if (expectedResponseClazz == String.class) {
                        result.setData((T) responseBody);
                    } else {
                        final T data = responseHandler.handleResponse(responseBody, expectedResponseClazz);
                        result.setData(data);
                    }

                    result.setSuccess(true);
                } catch (Exception e) {
                    log.error("Error deserialising JSON of normal API.", e);
                    final AxStarResponseError axStarResponseError = new AxStarResponseError();
                    axStarResponseError.setExceptionType(RESP_ERROR_TYPE_DESERIALISE_NORMAL);
                    axStarResponseError.setExceptionMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setStackTrace("Contents of body: " + responseBody);

                    result.setSuccess(false);
                    result.setError(axStarResponseError);
                }
            }
        } catch (Exception e) {
            final String exMsg = "Exception encountered when trying to process a REST API exchange. MSG: " + e.getMessage();
            log.error(exMsg, e);
            final AxStarResponseError axStarResponseError = new AxStarResponseError();
            axStarResponseError.setExceptionType(RESP_ERROR_TYPE_REST_GENERAL_ERROR);
            axStarResponseError.setExceptionMessage(exMsg);
            axStarResponseError.setMessage(exMsg);
            axStarResponseError.setStackTrace("See logs.");

            result.setSuccess(false);
            result.setError(axStarResponseError);
        }

        return result;
    }

    private void saveCookie(String url, final ResponseEntity<String> response) {
        if (url.contains(slmAxChannelServiceURL) || url.contains(mflgAxChannelServiceURL)) {
            HttpHeaders responseHeaders = response.getHeaders();
            List<String> cookies = responseHeaders.get("Set-Cookie");

            if (cookies != null && cookies.size() > 0) {
                String[] cookie = cookies.get(0).split(";");
                String key = "";

                if (cookie.length > 0 && cookie[0].split("=").length > 1 && StringUtils.isNotBlank(cookie[0].split("=")[1])) {
                    if (!cookies.get(0).toUpperCase().contains("EXPIRES")) {
                        cookie[0] += ";expires=" + DateTime.now().plusDays(100).toString("EEE, dd-MMM-yyyy HH:mm:ss zzz");
                    }
                    key = cookie[0];
                }
                if (url.contains(slmAxChannelServiceURL) && url.contains(mflgAxChannelServiceURL)) {
                    this.kioskChannelCookie.put(StoreApiChannels.KIOSK_SLM.code, key);
                    this.kioskChannelCookie.put(StoreApiChannels.KIOSK_MFLG.code, key);
                } else if (url.contains(slmAxChannelServiceURL)) {
                    this.kioskChannelCookie.put(StoreApiChannels.KIOSK_SLM.code, key);
                } else if (url.contains(mflgAxChannelServiceURL)) {
                    this.kioskChannelCookie.put(StoreApiChannels.KIOSK_MFLG.code, key);
                }
            }
        }
    }

    private void getCookie(final HttpHeaders httpHeaders, String channel) {
        if (StringUtils.isNotBlank(this.kioskChannelCookie.get(channel))) {
            httpHeaders.set("Cookie", this.kioskChannelCookie.get(channel));
        }
    }

    private void getErrorMessageFromResponse(AxStarServiceResult<?> result) {
        result.setError(new AxStarResponseError());
        JsonParser parser = new JsonParser();
        JsonObject rawData = (JsonObject) parser.parse(result.getRawData());
        JsonObject oDataError = rawData.get("odata.error").getAsJsonObject();
        String code = oDataError.get("code").getAsString();
        log.debug("error code : " + code);
        result.getError().setExceptionType(code);
        String message = oDataError.get("message").getAsJsonObject().get("value").getAsString();

        String[] messages = message.split(",");
        if (messages.length > 2 && messages[1].length() > 12) {
            result.getError().setExceptionMessage(messages[1].substring(11).replace("\\n", "  "));
            log.debug("error message : " + result.getError().getMessage());
        }
    }

    public AxStarServiceResult<AxDeviceActivationResult> apiKioskActivation(StoreApiChannels channel, KioskDeviceActivationRequest axActivationRequest) {

        final AxStarServiceResult<AxDeviceActivationResult> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_ACTIVATE_DEVICE, HttpMethod.POST, axActivationRequest,
                AxDeviceActivationResult.class);
        log.info("apiKioskActivation => " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<AxDeviceConfiguration> apiKioskGetDeviceConfiguration(StoreApiChannels channel) {

        final AxStarServiceResult<AxDeviceConfiguration> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_GET_DEVICE_CONFIG, HttpMethod.POST, "", AxDeviceConfiguration.class);
        log.info("apiKioskGetDeviceConfiguration => " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<AxHardwareProfile> apiKiioskGetHardwareProfileById(StoreApiChannels channel, KiioskGetHardwareProfileByIdRequest request) {

        final AxStarServiceResult<AxHardwareProfile> result = doKioskRestExchange(channel, URL_FRAG_KIOOSK_GET_HARDWARE_PROFILE, HttpMethod.POST, request, AxHardwareProfile.class);
        log.info("apiKiioskGetHardwareProfileById => " + JsonUtil.jsonify(result));
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "CashDrawers");
            Type type = new TypeToken<List<AxHardwareProfileCashDrawer>>() {
            }.getType();
            List<AxHardwareProfileCashDrawer> data = this.gson.fromJson(json, type);
            result.getData().setAxHardwareProfileCashDrawerList(data);
        }
        return result;
    }

    public AxStarServiceResult<AxEmployee> apiKioskActivationLogon(StoreApiChannels channel, KioskActivationConnectionRequest axActivationConnectionRequest) {
        this.kioskChannelCookie.clear();
        final AxStarServiceResult<AxEmployee> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_LOGON, HttpMethod.POST, axActivationConnectionRequest, AxEmployee.class);
        log.info("apiKioskActivationLogon => " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<AxEmployee> apiKioskLogon(StoreApiChannels channel, KioskLogonRequest axConnectionRequest) {
        this.kioskChannelCookie.clear();
        final AxStarServiceResult<AxEmployee> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_LOGON, HttpMethod.POST, axConnectionRequest, AxEmployee.class);
        log.info("apiKioskLogon => " + JsonUtil.jsonify(result));
        if (result.isSuccess()) {
            kioskLogonRequest = axConnectionRequest;
        }
        return result;
    }

    public AxStarServiceResult<String> apiKioskLogoff(StoreApiChannels channel, KioskLogoffRequest axLogoffRequest) {

        final AxStarServiceResult<String> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_LOGOFF, HttpMethod.POST, axLogoffRequest, String.class);
        if (result.isSuccess()) {
            this.kioskChannelCookie.clear();
        }
        log.info("apiKioskLogoff isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskSearchXmlTemplateResponse> apiKioskSearchXmlTemplate(StoreApiChannels channel, KioskSearchXmlTemplateRequest aXSearchXmlTemplateRequest) {

        final AxStarServiceResult<KioskSearchXmlTemplateResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SEARCH_XML_TEMPLATE, HttpMethod.POST,
                aXSearchXmlTemplateRequest, KioskSearchXmlTemplateResponse.class);
        log.info("apiKioskSearchXmlTemplate isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<AxTemplateXMLEntityResponse>>() {
            }.getType();
            List<AxTemplateXMLEntityResponse> data = this.gson.fromJson(json, type);
            KioskSearchXmlTemplateResponse response = new KioskSearchXmlTemplateResponse();
            response.setEntityResponse(data);
            result.setData(response);
        }
        return result;
    }

    private String getJsonArrayInString(String raw, String name) {
        String json = "{}";
        JsonParser parser = new JsonParser();
        Object object = parser.parse(raw);
        if (object instanceof JsonObject) {
            json = ((JsonObject) object).get(name).getAsJsonArray().toString();
        } else {
            if (((JsonArray) object).size() > 0) {
                JsonObject objectArray = (JsonObject) ((JsonArray) object).get(0);
                json = objectArray.get(name).getAsJsonArray().toString();
            }
        }
        return json;
    }

    public AxStarServiceResult<KioskGetLatestNumberSequenceResponse> apiKioskGetLatestNumberSequence(StoreApiChannels channel) {

        final AxStarServiceResult<KioskGetLatestNumberSequenceResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_GET_LATEST_NUMBER_SEQUENCE, HttpMethod.POST, "",
                KioskGetLatestNumberSequenceResponse.class);
        log.info("apkKioskGetLatestNumberSequence isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<AxNumberSequenceSeedData>>() {
            }.getType();
            List<AxNumberSequenceSeedData> data = this.gson.fromJson(json, type);
            KioskGetLatestNumberSequenceResponse response = new KioskGetLatestNumberSequenceResponse();
            response.setAxNumberSequenceSeedDataList(data);
            result.setData(response);
        }
        return result;
    }

    public AxStarServiceResult<KioskGetProductsResponse> apiKioskGetProducts(StoreApiChannels channel) {

        final AxStarServiceResult<KioskGetProductsResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_GET_PRODUCTS, HttpMethod.GET, "", KioskGetProductsResponse.class);

        log.info("apiKioskGetProducts isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {

            List<AxProduct> productList = new ArrayList<>();

            JsonParser parser = new JsonParser();
            Object object = parser.parse(result.getRawData());

            JsonArray productArray = ((JsonObject) object).get("value").getAsJsonArray();

            for (JsonElement product : productArray) {
                AxProduct axProduct = this.gson.fromJson(product.toString(), AxProduct.class);
                productList.add(axProduct);
                JsonArray productPropertiesArray = product.getAsJsonObject().get("ProductProperties").getAsJsonArray();
                for (JsonElement productProperties : productPropertiesArray) {

                    AxProductPropertyTranslation translation = this.gson.fromJson(productProperties.toString(), AxProductPropertyTranslation.class);
                    axProduct.getProductProperties().add(translation);
                    JsonArray translatedPropertiesArray = productProperties.getAsJsonObject().get("TranslatedProperties").getAsJsonArray();

                    Type type = new TypeToken<List<AxProductProperty>>() {
                    }.getType();
                    List<AxProductProperty> data = this.gson.fromJson(translatedPropertiesArray.toString(), type);
                    translation.setAxProductPropertyList(data);
                }
            }

            result.getData().setProductList(productList);
        }
        return result;
    }

    public AxStarServiceResult<KioskGetPINRedemptionStartResponse> apiKioskGetPINRedemptionStart(StoreApiChannels channel, KioskGetRedemptionPinStartRequest request) {

        final AxStarServiceResult<KioskGetPINRedemptionStartResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_GET_PIN_REDEMPTION_START, HttpMethod.POST, request,
                KioskGetPINRedemptionStartResponse.class);

        log.info("apiKioskGetPINRedemptionStart isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            result.setRawData(json);
            Type type = new TypeToken<List<AxPINRedemptionStartData>>() {
            }.getType();
            List<AxPINRedemptionStartData> axPinRedemptionStartDataList = this.gson.fromJson(json, type);

            KioskGetPINRedemptionStartResponse response = new KioskGetPINRedemptionStartResponse();
            response.setAxPINRedemptionStartDataList(axPinRedemptionStartDataList);

            if (axPinRedemptionStartDataList.size() > 0) {
                if (json.contains("PINRedemptionStartTransEntityCollection")) {
                    String transactionEntities = getJsonArrayInString(json, "PINRedemptionStartTransEntityCollection");
                    type = new TypeToken<List<AxPINRedemptionStartTransactionData>>() {
                    }.getType();
                    List<AxPINRedemptionStartTransactionData> transactionDataEntities = this.gson.fromJson(transactionEntities, type);

                    if (transactionEntities.contains("EventGroupList")) {

                        JsonParser parser = new JsonParser();
                        JsonArray transactionEntityArray = (JsonArray) parser.parse(transactionEntities);

                        int x = 0;
                        for (JsonElement transactionEntity : transactionEntityArray) {
                            JsonObject eventGroupList = transactionEntity.getAsJsonObject().getAsJsonObject("EventGroupList");

                            type = new TypeToken<AxEventGroupList>() {
                            }.getType();

                            AxEventGroupList eventGroupLines = this.gson.fromJson(eventGroupList, type);

                            String eventGrooups = getJsonArrayInString(eventGroupList.toString(), "EventGroups");
                            type = new TypeToken<List<AxEventGroup>>() {
                            }.getType();
                            List<AxEventGroup> eventDataGroups = this.gson.fromJson(eventGrooups, type);

                            eventGroupLines.setAxEventGroups(eventDataGroups);
                            if (eventGroupLines.getAxEventGroups() == null || eventGroupLines.getAxEventGroups() == null || eventGroupLines.getAxEventGroups().size() == 0) {
                                transactionDataEntities.get(x).setAxEventGroupList(null);
                            } else {
                                transactionDataEntities.get(x).setAxEventGroupList(eventGroupLines);
                            }

                            int y = 0;

                            for (JsonElement eventGroup : eventGroupList.get("EventGroups").getAsJsonArray()) {

                                JsonElement eventLineList = eventGroup.getAsJsonObject().get("EventLineList");
                                JsonArray eventLinesArray = eventLineList.getAsJsonObject().get("EventLines").getAsJsonArray();

                                type = new TypeToken<List<AxEventLine>>() {
                                }.getType();
                                List<AxEventLine> axEventLinesList = this.gson.fromJson(eventLinesArray.toString(), type);
                                AxEventLineList axEventLineList = new AxEventLineList();
                                axEventLineList.setAxEventLines(axEventLinesList);
                                eventDataGroups.get(y).setAxEventLineList(axEventLineList);

                                int z = 0;

                                for (JsonElement eventLine : eventLinesArray) {
                                    JsonObject eventDateListObject = eventLine.getAsJsonObject().getAsJsonObject("EventDateList");

                                    type = new TypeToken<AxEventDateList>() {
                                    }.getType();

                                    AxEventDateList eventDateList = this.gson.fromJson(eventDateListObject, type);
                                    String eventDates = getJsonArrayInString(eventDateListObject.toString(), "EventDates");
                                    type = new TypeToken<List<AxEventDate>>() {
                                    }.getType();
                                    List<AxEventDate> eventDatesList = this.gson.fromJson(eventDates, type);

                                    eventDateList.setAxEventDates(eventDatesList);
                                    if (eventDateList == null || eventDateList.getAxEventDates() == null || eventDateList.getAxEventDates().size() == 0) {
                                        axEventLineList.getAxEventLines().get(z).setEventDateList(null);
                                    } else {

                                        axEventLineList.getAxEventLines().get(z).setEventDateList(eventDateList);
                                    }
                                    z++;
                                }
                                y++;
                            }

                            x++;
                        }
                        response.getAxPINRedemptionStartDataList().get(0).setAxPINRedemptionStartTransactionDataList(transactionDataEntities);
                    }
                }
                result.setData(response);
            }
        } else {
            this.getErrorMessageFromResponse(result);
        }
        return result;
    }

    public AxStarServiceResult<KioskCartCheckoutStartPINResponse> apiKioskCartCheckoutStartPIN(StoreApiChannels channel, KioskCartCheckoutPinStartRequest request) {

        final AxStarServiceResult<KioskCartCheckoutStartPINResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_CHECKOUT_START_PIN, HttpMethod.POST, request,
                KioskCartCheckoutStartPINResponse.class);
        log.info("apiKioskCartCheckoutStartPIN isSuccess => " + result.isSuccess());

        if (result.isSuccess()) {

            KioskCartCheckoutStartPINResponse startPinCheckoutResponse = result.getData();

            JsonParser parser = new JsonParser();
            Object object = parser.parse(result.getRawData());

            JsonArray ticketCollectoinArray = ((JsonObject) object).get("CartRetailTicketTableEntityCollection").getAsJsonArray();
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();

            List<AxCartRetailTicketTableEntity> ticketList = this.gson.fromJson(ticketCollectoinArray.toString(), type);
            int i = 0;
            for (JsonElement ticketCollectionElement : ticketCollectoinArray) {
                JsonArray facilitiesArray = ticketCollectionElement.getAsJsonObject().get("Facilities").getAsJsonArray();
                type = new TypeToken<List<AxFacilityClassEntity>>() {
                }.getType();
                List<AxFacilityClassEntity> facilitiesList = new Gson().fromJson(facilitiesArray.toString(), type);

                ticketList.get(i).setFacilities(facilitiesList);
                
                JsonArray tokenListArray = ticketCollectionElement.getAsJsonObject().get("TokenList").getAsJsonArray();
                type = new TypeToken<List<AxPrintTokenEntity>>() {
                }.getType();
                List<AxPrintTokenEntity> tokenList = this.gson.fromJson(tokenListArray.toString(), type);

                ticketList.get(i).setTokenList(tokenList);
                
                
                i++;
            }
            startPinCheckoutResponse.setAxCartRetailTicketTableEntityList(ticketList);

        }

        return result;
    }

    public AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> apiKioskCartTicketUpdatePrintStatus(StoreApiChannels channel, KioskUpdatePrintStatusRequest request) {

        final AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_TICKET_UPDATE_PRINT_STATUS, HttpMethod.POST,
                request, KioskCartTicketUpdatePrintStatusResponse.class);
        log.info("apiKioskCartTicketUpdatePrintStatus isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> apiKioskProceedWithPrinted(StoreApiChannels channel, KioskProceedPrintedRequest request) {

        final AxStarServiceResult<KioskCartTicketUpdatePrintStatusResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_PROCEED_WITH_PRINTED, HttpMethod.POST, request,
                KioskCartTicketUpdatePrintStatusResponse.class);
        log.info("apiKioskProceedWithPrinted isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskSendRedeemTicketToAXResponse> apiKioskSendRedeemTicketToAX(StoreApiChannels channel, KioskSendRedeemTicketRequest request) {

        final AxStarServiceResult<KioskSendRedeemTicketToAXResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SEND_REDEEM_TICKET_TO_AX, HttpMethod.POST, request,
                KioskSendRedeemTicketToAXResponse.class);
        log.info("apiKioskProceedWithPrinted isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskPINRedemptionUnlockResponse> apiKioskPINRedemptionUnlock(StoreApiChannels channel, KioskPINRedemptionUnlockRequest request) {

        final AxStarServiceResult<KioskPINRedemptionUnlockResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_PIN_REDEMPTION_UNLOCK, HttpMethod.POST, request,
                KioskPINRedemptionUnlockResponse.class);
        log.info("apiKioskPINRedemptionUnlock isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskSearchProductExtResponse> apiKioskSearchProductExt(StoreApiChannels channel, KioskSearchProductExtRequest request) {

        final AxStarServiceResult<KioskSearchProductExtResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_PRODUCT_SEARCH_EXT, HttpMethod.POST, request,
                KioskSearchProductExtResponse.class);
        log.info("apiKioskSearchProductExt isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");

            Type type = new TypeToken<List<AxInventTableExtEntity>>() {
            }.getType();
            List<AxInventTableExtEntity> data = this.gson.fromJson(json, type);
            KioskSearchProductExtResponse response = new KioskSearchProductExtResponse();
            response.setAxInventTableExtEntityList(data);
            if (data.size() > 0) {
                if (json.contains("EventGroupTable")) {

                    String eventGroupTable = getJsonArrayInString(json, "EventGroupTable");
                    type = new TypeToken<List<AxEventGroupTableEntity>>() {
                    }.getType();
                    List<AxEventGroupTableEntity> events = this.gson.fromJson(eventGroupTable, type);

                    if (eventGroupTable.contains("EventGroupLines")) {

                        JsonParser parser = new JsonParser();
                        JsonArray eventGTArray = (JsonArray) parser.parse(eventGroupTable);
                        int j = 0;
                        for (JsonElement eventGT : eventGTArray) {
                            JsonArray eventGLArray = eventGT.getAsJsonObject().getAsJsonArray("EventGroupLines");
                            type = new TypeToken<List<AxEventGroupLineEntity>>() {
                            }.getType();
                            List<AxEventGroupLineEntity> eventGroupLines = this.gson.fromJson(eventGLArray.toString(), type);

                            int i = 0;
                            for (JsonElement allocLine : eventGLArray) {
                                JsonArray allocLineArray = allocLine.getAsJsonObject().getAsJsonArray("EventAllocationLines");
                                type = new TypeToken<List<AxEventAllocationEntity>>() {
                                }.getType();
                                List<AxEventAllocationEntity> allocationEntityList = this.gson.fromJson(allocLineArray.toString(), type);
                                eventGroupLines.get(i).setEventAllocations(allocationEntityList);
                                i++;
                            }
                            events.get(j).setEventGroupLines(eventGroupLines);
                        }
                        response.getAxInventTableExtEntityList().get(0).setEventGroups(events);
                    }
                }

                String productVariant = this.getJsonArrayInString(json, "ProductVariant");
                String packageLines = this.getJsonArrayInString(json, "packageLines");

                type = new TypeToken<List<AxInventTableVariantExtEntity>>() {
                }.getType();
                List<AxInventTableVariantExtEntity> variants = this.gson.fromJson(productVariant, type);

                type = new TypeToken<List<AxPackageLineEntity>>() {
                }.getType();
                List<AxPackageLineEntity> lines = this.gson.fromJson(packageLines, type);

                response.getAxInventTableExtEntityList().get(0).setProductVariants(variants);
                response.getAxInventTableExtEntityList().get(0).setPackageLines(lines);
            }
            result.setData(response);
        }
        return result;
    }

    public AxStarServiceResult<KioskCartValidateQuantityResponse> apiKioskCartValidateQuantity(StoreApiChannels channel, KioskCartValidateQuantityRequest request) {

        final AxStarServiceResult<KioskCartValidateQuantityResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_VALIDATE_QUANTITY, HttpMethod.POST, request,
                KioskCartValidateQuantityResponse.class);
        log.info("apiKioskCartValidateQuantity isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "CartRetailTicketTableEntityCollection");
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();
            List<AxCartRetailTicketTableEntity> data = this.gson.fromJson(json, type);
            result.getData().setAxCartRetailTicketTableEntityList(data);
        }
        return result;
    }

    public AxStarServiceResult<AxCart> apiKioskGetCarts(StoreApiChannels channel, KioskGetCartsRequest request) {

        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CARTS, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskGetCarts isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<AxSalesOrder> apiKioskVoidCarts(StoreApiChannels channel, KioskVoidCartsRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_VOID_CART;
        final AxStarServiceResult<AxSalesOrder> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxSalesOrder.class);
        log.info("apiKioskVoidCarts isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<AxCart> apiKioskAddCartLine(StoreApiChannels channel, KioskAddCartLineRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_ADD_CART_LINES;
        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskAddCartLine isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<AxCart> apiKioskUpdateCartLine(StoreApiChannels channel, KioskUpdateCartLineRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_UPDATE_CART_LINES;
        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskUpdateCartLine isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<AxUseDiscountCounter> apiKioskUseDiscountCounter(StoreApiChannels channel, AxUseDiscountCounter useDiscountCounter) {

        final AxStarServiceResult<AxUseDiscountCounter> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_USE_DISCOUNT_COUNTER, HttpMethod.POST, useDiscountCounter,
                AxUseDiscountCounter.class);
        log.info("apiKioskUseDiscountCounter isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskCartValidateAndReserveResponse> apiKioskCartValidateAndReserve(StoreApiChannels channel, KioskCartValidateAndReserveRequest request) {

        final AxStarServiceResult<KioskCartValidateAndReserveResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_VALIDATE_AND_RESERVE, HttpMethod.POST, request,
                KioskCartValidateAndReserveResponse.class);
        log.info("apiKioskCartValidateAndReserve isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskCartValidateAndReserveResponse> apiKioskCartPinValidateAndReserve(StoreApiChannels channel, KioskCartPinValidateAndReserveRequest request) {

        final AxStarServiceResult<KioskCartValidateAndReserveResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_VALIDATE_AND_RESERVE, HttpMethod.POST, request,
                KioskCartValidateAndReserveResponse.class);
        log.info("apiKioskCartPinValidateAndReserve isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskCartTicketSearchResponse> apiKioskCartTicketSearch(StoreApiChannels channel, KioskCartTicketSearchCriteriaRequest request) {

        final AxStarServiceResult<KioskCartTicketSearchResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_TICKET_SEARCH, HttpMethod.POST, request,
                KioskCartTicketSearchResponse.class);
        log.info("apiKioskCartTicketSearch isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            KioskCartTicketSearchResponse searchTicketResponse = result.getData();

            JsonParser parser = new JsonParser();
            Object object = parser.parse(result.getRawData());

            JsonArray ticketCollectoinArray = ((JsonObject) object).get("CartRetailTicketTableEntityCollection").getAsJsonArray();
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();

            List<AxCartRetailTicketTableEntity> ticketList = this.gson.fromJson(ticketCollectoinArray.toString(), type);
            int i = 0;
            for (JsonElement ticketCollectionElement : ticketCollectoinArray) {
                JsonArray facilitiesArray = ticketCollectionElement.getAsJsonObject().get("Facilities").getAsJsonArray();
                type = new TypeToken<List<AxFacilityClassEntity>>() {
                }.getType();
                List<AxFacilityClassEntity> facilitiesList = new Gson().fromJson(facilitiesArray.toString(), type);

                ticketList.get(i).setFacilities(facilitiesList);
                
                JsonArray tokenListArray = ticketCollectionElement.getAsJsonObject().get("TokenList").getAsJsonArray();
                type = new TypeToken<List<AxPrintTokenEntity>>() {
                }.getType();
                List<AxPrintTokenEntity> tokenList = this.gson.fromJson(tokenListArray.toString(), type);

                ticketList.get(i).setTokenList(tokenList);
                
                JsonArray extProperties = ticketCollectionElement.getAsJsonObject().get("ExtensionProperties").getAsJsonArray();
                type = new TypeToken<List<AxCommerceProperty>>() {
                }.getType();
                List<AxCommerceProperty> props = this.gson.fromJson(extProperties.toString(), type);

                ticketList.get(i).setExtensionProperties(props);
                
                i++;
            }
            searchTicketResponse.setAxCartRetailTicketTableEntityList(ticketList);
        }

        return result;
    }

    public AxStarServiceResult<KioskSearchJournalTransactionsResponse> apiKioskSearchJournalTransactions(StoreApiChannels channel, KioskSearchJournalTransactionsRequest request) {

        final AxStarServiceResult<KioskSearchJournalTransactionsResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SEARCH_JOURNAL_TRANSACTION, HttpMethod.POST, request,
                KioskSearchJournalTransactionsResponse.class);
        log.info("apiKioskSearchJournalTransactions isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<AxJournalTransaction>>() {
            }.getType();
            List<AxJournalTransaction> data = this.gson.fromJson(json, type);
            result.getData().setAxJournalTransactionList(data);
        }

        return result;
    }

    public AxStarServiceResult<KioskSalesOrdersSearchResponse> apiKioskSalesOrdersSearch(StoreApiChannels channel, KioskSalesOrdersSearchRequest request) {

        final AxStarServiceResult<KioskSalesOrdersSearchResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SALES_ORDERS_SEARCH, HttpMethod.POST, request,
                KioskSalesOrdersSearchResponse.class);
        log.info("apiKioskSalesOrdersSearch isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<AxSalesOrderSearch>>() {
            }.getType();
            List<AxSalesOrderSearch> axSalesOrderSearch = this.gson.fromJson(json, type);

            KioskSalesOrdersSearchResponse response = new KioskSalesOrdersSearchResponse();
            response.setAxSalesOrderSearch(axSalesOrderSearch);

            if (axSalesOrderSearch.size() > 0) {
                if (json.contains("SalesLines")) {
                    String salesLines = getJsonArrayInString(json, "SalesLines");
                    type = new TypeToken<List<AxSalesLine>>() {
                    }.getType();
                    List<AxSalesLine> salesLinesEntity = this.gson.fromJson(salesLines, type);

                    int i = 0;
                    JsonParser parser = new JsonParser();
                    JsonArray salesLinesArray = (JsonArray) parser.parse(salesLines);
                    for (JsonElement sale : salesLinesArray) {

                        JsonArray discountLineArray = sale.getAsJsonObject().getAsJsonArray("DiscountLines");
                        type = new TypeToken<List<AxDiscountLine>>() {
                        }.getType();
                        List<AxDiscountLine> discountLineList = this.gson.fromJson(discountLineArray.toString(), type);
                        salesLinesEntity.get(i).setDiscountLines(discountLineList);
                        i++;
                    }
                    response.getAxSalesOrderSearch().get(0).setSalesLines(salesLinesEntity);
                }
            }

            result.setData(response);
        }

        return result;
    }

    // Reprint
    public AxStarServiceResult<KioskCartTicketReprintStartResponse> apiKioskCartTicketReprintStart(StoreApiChannels channel, KioskCartTicketReprintStartRequest request) {
        final AxStarServiceResult<KioskCartTicketReprintStartResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_TICKET_REPRINT_START, HttpMethod.POST, request,
                KioskCartTicketReprintStartResponse.class);
        log.info("apiKioskCartTicketReprintStart isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "CartRetailTicketTableEntityCollection");
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();
            List<AxCartRetailTicketTableEntity> data = this.gson.fromJson(json, type);
            result.getData().setAxCartRetailTicketTableEntityList(data);
        }
        
        return result;
    }

    // ReprintSuccess
    public AxStarServiceResult<KioskCartTicketUpdateReprintSuccessResponse> apiKioskUpdateReprintSuccess(StoreApiChannels channel,
            KioskCartTicketUpdateReprintSuccessRequest request) {
        final AxStarServiceResult<KioskCartTicketUpdateReprintSuccessResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_TICKET_REPRINT_SUCCESS, HttpMethod.POST,
                request, KioskCartTicketUpdateReprintSuccessResponse.class);
        log.info("apiKioskUpdateReprintSuccess isSuccess => " + result.isSuccess());
        return result;
    }
    
    // Deactivate Ticket
    public AxStarServiceResult<KioskCartDeactivateTicketResponse> apiKioskDeactivateTicket(StoreApiChannels channel, KioskCartDeactivateTicketRequest request) {
        final AxStarServiceResult<KioskCartDeactivateTicketResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_DEACTIVATE_TICKET, HttpMethod.POST, request, KioskCartDeactivateTicketResponse.class);
        log.info("apiKioskDeactivateTicket isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<AxCart> apiKioskAddTenderLine(StoreApiChannels channel, KioskAddTenderLineRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_CARTS_ADD_TENDER_LINE;
        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskAddTenderLine isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<AxCart> apiKioskVoidTenderLine(StoreApiChannels channel, KioskVoidTenderLineRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_CARTS_VOID_TENDER_LINE;
        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskVoidTenderLine isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<KioskCartCheckoutStartResponse> apiKioskCartCheckoutStart(StoreApiChannels channel, KioskCartCheckoutStartRequest request) {
        final AxStarServiceResult<KioskCartCheckoutStartResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_CHECKOUT_START, HttpMethod.POST, request,
                KioskCartCheckoutStartResponse.class);
        log.info("apiKioskCartCheckoutStart isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {

            KioskCartCheckoutStartResponse checkoutResponse = result.getData();

            JsonParser parser = new JsonParser();
            Object object = parser.parse(result.getRawData());

            JsonArray ticketCollectoinArray = ((JsonObject) object).get("CartRetailTicketTableEntityCollection").getAsJsonArray();
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();

            List<AxCartRetailTicketTableEntity> ticketList = this.gson.fromJson(ticketCollectoinArray.toString(), type);
            int i = 0;
            for (JsonElement ticketCollectionElement : ticketCollectoinArray) {
                JsonArray facilitiesArray = ticketCollectionElement.getAsJsonObject().get("Facilities").getAsJsonArray();
                type = new TypeToken<List<AxFacilityClassEntity>>() {
                }.getType();
                List<AxFacilityClassEntity> facilitiesList = new Gson().fromJson(facilitiesArray.toString(), type);

                ticketList.get(i).setFacilities(facilitiesList);
                JsonArray tokenListArray = ticketCollectionElement.getAsJsonObject().get("TokenList").getAsJsonArray();
                type = new TypeToken<List<AxPrintTokenEntity>>() {
                }.getType();
                List<AxPrintTokenEntity> tokenList = this.gson.fromJson(tokenListArray.toString(), type);

                ticketList.get(i).setTokenList(tokenList);
                
                i++;
            }
            checkoutResponse.setAxCartRetailTicketTableEntityList(ticketList);

        }
        return result;
    }

    public AxStarServiceResult<AxSalesOrder> apiKioskCartCheckout(StoreApiChannels channel, KioskCartCheckoutRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_CARTS_CHECKOUT;
        final AxStarServiceResult<AxSalesOrder> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxSalesOrder.class);
        log.info("apiKioskCartCheckout isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskGetReceiptsResponse> apiKioskGetReceipts(StoreApiChannels channel, KioskGetReceiptsRequest request) {
        String urlFrag = "SalesOrders('" + request.getTransactionId() + "')/" + URL_FRAG_KIOSK_SALE_ORDER_GET_RECEIPTS;
        final AxStarServiceResult<KioskGetReceiptsResponse> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, KioskGetReceiptsResponse.class);
        log.info("apiKioskGetReceipts isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskCartCheckoutCompleteResponse> apiKioskCartCheckoutComplete(StoreApiChannels channel, KioskCartCheckoutCompleteRequest request) {
        final AxStarServiceResult<KioskCartCheckoutCompleteResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_CHECKOUT_COMPLETE, HttpMethod.POST, request,
                KioskCartCheckoutCompleteResponse.class);
        log.info("apiKioskCartCheckoutComplete isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "CartRetailTicketTableEntityCollection");
            Type type = new TypeToken<List<AxCartRetailTicketTableEntity>>() {
            }.getType();
            List<AxCartRetailTicketTableEntity> data = this.gson.fromJson(json, type);
            result.getData().setAxCartRetailTicketTableEntityList(data);
        }
        return result;
    }

    public AxStarServiceResult<KioskCartCheckoutCompleteResponse> apiKioskCartCheckoutCancel(StoreApiChannels channel, KioskCartCheckoutCancelRequest request) {
        final AxStarServiceResult<KioskCartCheckoutCompleteResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_CART_CHECKOUT_CANCEL, HttpMethod.POST, request,
                KioskCartCheckoutCompleteResponse.class);
        log.info("apiKioskCartCheckoutCancel isSuccess => " + result.isSuccess());
        return result;
    }

    private AxCart getAxCart(AxStarServiceResult<AxCart> result) {
        AxCart axCart = null;
        if (result.isSuccess()) {
            axCart = result.getData();

            JsonParser parser = new JsonParser();
            Object object = parser.parse(result.getRawData());

            JsonArray cartLinesArray = ((JsonObject) object).get("CartLines").getAsJsonArray();
            Type type = new TypeToken<List<AxCartLine>>() {
            }.getType();

            List<AxCartLine> cartLines = this.gson.fromJson(cartLinesArray.toString(), type);
            int i = 0;
            for (JsonElement cartLinesEelement : cartLinesArray) {
                JsonArray discountLinesArray = cartLinesEelement.getAsJsonObject().get("DiscountLines").getAsJsonArray();
                type = new TypeToken<List<AxDiscountLine>>() {
                }.getType();
                List<AxDiscountLine> axDiscountLines = this.gson.fromJson(discountLinesArray.toString(), type);

                cartLines.get(i).setDiscountLineList(axDiscountLines);
                i++;
            }
            axCart.setAxCartLineList(cartLines);

            List<AxCartLine> validCartLines = cartLines.stream().filter(l -> Boolean.FALSE.toString().equalsIgnoreCase(l.getIsVoided())).collect(Collectors.toList());
            result.getData().setAxCartLineList(validCartLines);

            String json = this.getJsonArrayInString(result.getRawData(), "TenderLines");
            type = new TypeToken<List<AxTenderLine>>() {
            }.getType();
            List<AxTenderLine> allTenderLines = this.gson.fromJson(json, type);
            List<AxTenderLine> tenderLines = allTenderLines.stream().filter(t -> Boolean.TRUE.toString().toUpperCase().equalsIgnoreCase(t.getIsVoidable().toUpperCase()))
                    .collect(Collectors.toList());
            result.getData().setAxTenderLineList(tenderLines);
        }

        return axCart;
    }

    public AxStarServiceResult<AxCart> apiKioskVoidCartLines(StoreApiChannels channel, KioskVoidCartLinesRequest request) {
        String urlFrag = "Carts('" + request.getCartId() + "')/" + URL_FRAG_KIOSK_VOID_CART_LINES;
        final AxStarServiceResult<AxCart> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxCart.class);
        log.info("apiKioskVoidCartLines isSuccess => " + result.isSuccess());
        this.getAxCart(result);
        return result;
    }

    public AxStarServiceResult<KioskGetShiftsByStatusResponse> apiKiioskGetShiftsByStatus(StoreApiChannels channel, KioskGetShiftsByStatusRequest request) {
        final AxStarServiceResult<KioskGetShiftsByStatusResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SHIFTS_GET_BY_STATUS, HttpMethod.POST, request,
                KioskGetShiftsByStatusResponse.class);
        log.info("apiKiioskGetShiftsByStatus isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<AxShift>>() {
            }.getType();
            List<AxShift> data = this.gson.fromJson(json, type);
            KioskGetShiftsByStatusResponse response = new KioskGetShiftsByStatusResponse();
            response.setAxShiftList(data);
            result.setData(response);
        }
        return result;
    }

    public AxStarServiceResult<AxShift> apiKioskUseShift(StoreApiChannels channel, KioskUseShiftRequest request) {
        String urlFrag = "Shifts(ShiftId='" + request.getShiftId() + "',TerminalId='" + request.getTerminalId() + "')/" + URL_FRAG_KIOSK_SHIFTS_USE;
        final AxStarServiceResult<AxShift> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, "", AxShift.class);
        log.info("apiKioskUseShift isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<AxShift> apiKioskOpenShift(StoreApiChannels channel, KioskOpenShiftRequest request) {
        final AxStarServiceResult<AxShift> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_SHIFTS_OPEN, HttpMethod.POST, request, AxShift.class);
        log.info("apiKioskOpenShift isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<AxShift> apiKioskCloseShift(StoreApiChannels channel, KioskCloseShiftRequest request) {
        String urlFrag = "Shifts(ShiftId='" + request.getShiftId() + "',TerminalId='" + request.getTerminalId() + "')/" + URL_FRAG_KIOSK_SHIFTS_CLOSE;
        final AxStarServiceResult<AxShift> result = doKioskRestExchange(channel, urlFrag, HttpMethod.POST, request, AxShift.class);

        log.info("apiKioskCloseShift isSuccess => " + result.isSuccess());
        return result;
    }

    public AxStarServiceResult<KioskGetUpCrossSellResponse> apiKioskGetUpCrossSell(StoreApiChannels channel, KioskGetUpCrossSellRequest request) {
        final AxStarServiceResult<KioskGetUpCrossSellResponse> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_GET_UPCROSSELL, HttpMethod.POST, request,
                KioskGetUpCrossSellResponse.class);
        log.info("apiKioskGetUpCrossSell isSuccess => " + result.isSuccess());
        if (result.isSuccess()) {
            String json = this.getJsonArrayInString(result.getRawData(), "value");
            Type type = new TypeToken<List<UpCrossSellTableEntity>>() {
            }.getType();
            List<UpCrossSellTableEntity> data = this.gson.fromJson(json, type);
            if (json.contains("UpCrossSellLines")) {
                JsonParser parser = new JsonParser();
                JsonArray ucsTableArray = (JsonArray) parser.parse(json);
                int j = 0;
                for (JsonElement eventGT : ucsTableArray) {
                    JsonArray ucsLineArray = eventGT.getAsJsonObject().getAsJsonArray("UpCrossSellLines");
                    type = new TypeToken<List<UpCrossSellLineEntity>>() {
                    }.getType();
                    List<UpCrossSellLineEntity> upCrossSellLineList = this.gson.fromJson(ucsLineArray.toString(), type);
                    data.get(j).setUpCrossSellLineList(upCrossSellLineList);
                    j++;
                }
                result.getData().setUpCrossSellTableEntityList(data);
            }
        }
        return result;
    }

    public AxStarServiceResult<AxUpdatedTicketStatusEntity> apiKioskBlacklistTickets(StoreApiChannels channel, KioskBlacklistTicketRequest request) {

        final AxStarServiceResult<AxUpdatedTicketStatusEntity> result = doKioskRestExchange(channel, URL_FRAG_KIOSK_BLACKLIST_TICKET, HttpMethod.POST, request,
                AxUpdatedTicketStatusEntity.class);

        log.info("apiKioskBlacklistTickets isSuccess => " + result.isSuccess());
        return result;
    }

    public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor {

        @Override
        public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
            traceRequest(request, body);
            ClientHttpResponse response = execution.execute(request, body);
            return response;
        }

        private void traceRequest(HttpRequest request, byte[] body) throws IOException {
            /**
             * System.out.println("===========================request
             * begin================================================");
             * System.out.println("URI : " + request.getURI());
             * System.out.println("Method : " + request.getMethod());
             * System.out.println("Headers : " + request.getHeaders());
             * System.out.println("Request body: " + new String(body, "UTF-8"));
             * System.out.println("===========================request
             * end================================================");
             **/
            log.info("===========================request begin================================================");
            log.info("URI         : " + request.getURI());
            log.info("Method      : " + request.getMethod());
            log.info("Headers     : " + request.getHeaders());
            log.info("Request body: " + new String(body, "UTF-8"));
            log.info("==========================request end================================================");
        }

    }
}
