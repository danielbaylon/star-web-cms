package com.enovax.star.cms.commons.model.kiosk.odata.request;

/**
 * 
 * @author Justin
 * @since 15 SEP 16
 */
public class KiioskGetHardwareProfileByIdRequest {

    private String hardwareProfileId;

    public String getHardwareProfileId() {
        return hardwareProfileId;
    }

    public void setHardwareProfileId(String hardwareProfileId) {
        this.hardwareProfileId = hardwareProfileId;
    }

}
