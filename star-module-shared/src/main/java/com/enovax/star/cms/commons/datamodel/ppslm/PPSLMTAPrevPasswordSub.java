package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPSLMTAPrevPasswordSub")
public class PPSLMTAPrevPasswordSub extends PPSLMPrevPassword implements Serializable{
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subAccountId",nullable = false)
    private PPSLMTASubAccount user;
    
    
    
    public PPSLMTASubAccount getUser() {
        return user;
    }
    public void setUser(PPSLMTASubAccount user) {
        this.user = user;
    }
    
    
        
    
}
