
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetailAffiliationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RetailAffiliationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="General"/>
 *     &lt;enumeration value="Loyalty"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RetailAffiliationType")
@XmlEnum
public enum RetailAffiliationType {

    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("General")
    GENERAL("General"),
    @XmlEnumValue("Loyalty")
    LOYALTY("Loyalty");
    private final String value;

    RetailAffiliationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RetailAffiliationType fromValue(String v) {
        for (RetailAffiliationType c: RetailAffiliationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
