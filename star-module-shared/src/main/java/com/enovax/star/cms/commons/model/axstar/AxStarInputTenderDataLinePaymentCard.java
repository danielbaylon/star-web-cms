package com.enovax.star.cms.commons.model.axstar;

public class AxStarInputTenderDataLinePaymentCard {

    private String cardType;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
