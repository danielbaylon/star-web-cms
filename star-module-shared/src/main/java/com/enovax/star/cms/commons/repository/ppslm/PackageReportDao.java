package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.InventoryReportModel;
import com.enovax.star.cms.commons.model.partner.ppslm.PackageReportModel;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;

/**
 * Created by lavanya on 8/11/16.
 */
public interface PackageReportDao {
    public ReportResult<PackageReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                           String orderBy, boolean asc, boolean isCount) throws Exception;
}
