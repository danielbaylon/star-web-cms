package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name="PPSLMTAMainRightsMapping")
public class PPSLMTAMainRightsMapping implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPSLMTAAccessRightsGroup accessRightsGroup;

	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainAccountId",nullable = false)
    private PPSLMTAMainAccount user;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public PPSLMTAMainAccount getUser() {
		return user;
	}


	public void setUser(PPSLMTAMainAccount user) {
		this.user = user;
	}


	public PPSLMTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}


	public void setAccessRightsGroup(PPSLMTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}
	
	
			
	
	
}
