package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * 
 * @author Justin
 * @since 15 SEP 16
 */
public class AxHardwareProfile {

    private String lineDisplayDelayForLinkedItems;
    private String lineDisplayBalanceText;
    private String lineDisplayBinaryConversion;
    private String lineDisplayCharacterSet;
    private String lineDisplayClosedLine1;
    private String lineDisplayClosedLine2;
    private String lineDisplayDeviceDescription;
    private String lineDisplayDeviceTypeValue;
    private String lineDisplayDeviceName;
    private String lineDisplayDisplayLinkedItem;
    private String lineDisplayDisplayTerminalClosed;
    private String lineDisplayTotalText;
    private String dualDisplayActive;
    private String dualDisplayWebBrowserUrl;
    private String dualDisplayImageRotatorInterval;
    private String dualDisplayImageRotatorPath;
    private String dualDisplayReceiptWidthPercentage;
    private String dualDisplayDisplayType;
    private String eftTypeId;
    private String eftCompanyId;
    private String eftConfiguration;
    private String eftPaymentConnectorName;
    private String eftMerchantPropertyXML;
    private String eftData;
    private String eftDescription;
    private String eftMerchantId;
    private String eftPassword;
    private String eftServerName;
    private String eftServerPort;
    private String eftUserId;
    private String eftTestMode;
    private String msrEndTrack1;
    private String msrEndTrack2;
    private String fiscalPrinterManagementReportPAFIdentification;
    private String fiscalPrinterManagementReportConfigParameter;
    private String fiscalPrinterManagementReportTenderType;
    private String fiscalPrinterManagementReportGiftCard;
    private String keyboardMappingId;
    private String keyLockDeviceTypeValue;
    private String keyLockDeviceDescription;
    private String keyLockDeviceName;
    private String msrDeviceTypeValue;
    private String msrDeviceDescription;
    private String msrDeviceName;
    private String msrMake;
    private String msrModel;
    private String msrSeparator;
    private String msrStartTrack;
    private String pinPadDeviceTypeValue;
    private String pinPadDeviceName;
    private String pinPadMake;
    private String pinPadModel;
    private String pinPadDeviceDescription;
    private String profileDescription;
    private String profileId;
    private String rfidDeviceTypeValue;
    private String rfidDeviceName;
    private String rfidDeviceDescription;
    private String scaleDeviceTypeValue;
    private String scaleDeviceName;
    private String scaleDeviceDescription;
    private String scaleManualInputAllowed;
    private String scaleTimeoutInSeconds;
    private String signatureCaptureDeviceTypeValue;
    private String signatureCaptureDeviceName;
    private String signatureCaptureMake;
    private String signatureCaptureModel;
    private String signatureCaptureDeviceDescription;
    private String signatureCaptureFormName;
    @JsonProperty("CashDrawers")
    private List<AxHardwareProfileCashDrawer> axHardwareProfileCashDrawerList;

    public String getLineDisplayDelayForLinkedItems() {
        return lineDisplayDelayForLinkedItems;
    }

    public void setLineDisplayDelayForLinkedItems(String lineDisplayDelayForLinkedItems) {
        this.lineDisplayDelayForLinkedItems = lineDisplayDelayForLinkedItems;
    }

    public String getLineDisplayBalanceText() {
        return lineDisplayBalanceText;
    }

    public void setLineDisplayBalanceText(String lineDisplayBalanceText) {
        this.lineDisplayBalanceText = lineDisplayBalanceText;
    }

    public String getLineDisplayBinaryConversion() {
        return lineDisplayBinaryConversion;
    }

    public void setLineDisplayBinaryConversion(String lineDisplayBinaryConversion) {
        this.lineDisplayBinaryConversion = lineDisplayBinaryConversion;
    }

    public String getLineDisplayCharacterSet() {
        return lineDisplayCharacterSet;
    }

    public void setLineDisplayCharacterSet(String lineDisplayCharacterSet) {
        this.lineDisplayCharacterSet = lineDisplayCharacterSet;
    }

    public String getLineDisplayClosedLine1() {
        return lineDisplayClosedLine1;
    }

    public void setLineDisplayClosedLine1(String lineDisplayClosedLine1) {
        this.lineDisplayClosedLine1 = lineDisplayClosedLine1;
    }

    public String getLineDisplayClosedLine2() {
        return lineDisplayClosedLine2;
    }

    public void setLineDisplayClosedLine2(String lineDisplayClosedLine2) {
        this.lineDisplayClosedLine2 = lineDisplayClosedLine2;
    }

    public String getLineDisplayDeviceDescription() {
        return lineDisplayDeviceDescription;
    }

    public void setLineDisplayDeviceDescription(String lineDisplayDeviceDescription) {
        this.lineDisplayDeviceDescription = lineDisplayDeviceDescription;
    }

    public String getLineDisplayDeviceTypeValue() {
        return lineDisplayDeviceTypeValue;
    }

    public void setLineDisplayDeviceTypeValue(String lineDisplayDeviceTypeValue) {
        this.lineDisplayDeviceTypeValue = lineDisplayDeviceTypeValue;
    }

    public String getLineDisplayDeviceName() {
        return lineDisplayDeviceName;
    }

    public void setLineDisplayDeviceName(String lineDisplayDeviceName) {
        this.lineDisplayDeviceName = lineDisplayDeviceName;
    }

    public String getLineDisplayDisplayLinkedItem() {
        return lineDisplayDisplayLinkedItem;
    }

    public void setLineDisplayDisplayLinkedItem(String lineDisplayDisplayLinkedItem) {
        this.lineDisplayDisplayLinkedItem = lineDisplayDisplayLinkedItem;
    }

    public String getLineDisplayDisplayTerminalClosed() {
        return lineDisplayDisplayTerminalClosed;
    }

    public void setLineDisplayDisplayTerminalClosed(String lineDisplayDisplayTerminalClosed) {
        this.lineDisplayDisplayTerminalClosed = lineDisplayDisplayTerminalClosed;
    }

    public String getLineDisplayTotalText() {
        return lineDisplayTotalText;
    }

    public void setLineDisplayTotalText(String lineDisplayTotalText) {
        this.lineDisplayTotalText = lineDisplayTotalText;
    }

    public String getDualDisplayActive() {
        return dualDisplayActive;
    }

    public void setDualDisplayActive(String dualDisplayActive) {
        this.dualDisplayActive = dualDisplayActive;
    }

    public String getDualDisplayWebBrowserUrl() {
        return dualDisplayWebBrowserUrl;
    }

    public void setDualDisplayWebBrowserUrl(String dualDisplayWebBrowserUrl) {
        this.dualDisplayWebBrowserUrl = dualDisplayWebBrowserUrl;
    }

    public String getDualDisplayImageRotatorInterval() {
        return dualDisplayImageRotatorInterval;
    }

    public void setDualDisplayImageRotatorInterval(String dualDisplayImageRotatorInterval) {
        this.dualDisplayImageRotatorInterval = dualDisplayImageRotatorInterval;
    }

    public String getDualDisplayImageRotatorPath() {
        return dualDisplayImageRotatorPath;
    }

    public void setDualDisplayImageRotatorPath(String dualDisplayImageRotatorPath) {
        this.dualDisplayImageRotatorPath = dualDisplayImageRotatorPath;
    }

    public String getDualDisplayReceiptWidthPercentage() {
        return dualDisplayReceiptWidthPercentage;
    }

    public void setDualDisplayReceiptWidthPercentage(String dualDisplayReceiptWidthPercentage) {
        this.dualDisplayReceiptWidthPercentage = dualDisplayReceiptWidthPercentage;
    }

    public String getDualDisplayDisplayType() {
        return dualDisplayDisplayType;
    }

    public void setDualDisplayDisplayType(String dualDisplayDisplayType) {
        this.dualDisplayDisplayType = dualDisplayDisplayType;
    }

    public String getEftTypeId() {
        return eftTypeId;
    }

    public void setEftTypeId(String eftTypeId) {
        this.eftTypeId = eftTypeId;
    }

    public String getEftCompanyId() {
        return eftCompanyId;
    }

    public void setEftCompanyId(String eftCompanyId) {
        this.eftCompanyId = eftCompanyId;
    }

    public String getEftConfiguration() {
        return eftConfiguration;
    }

    public void setEftConfiguration(String eftConfiguration) {
        this.eftConfiguration = eftConfiguration;
    }

    public String getEftPaymentConnectorName() {
        return eftPaymentConnectorName;
    }

    public void setEftPaymentConnectorName(String eftPaymentConnectorName) {
        this.eftPaymentConnectorName = eftPaymentConnectorName;
    }

    public String getEftMerchantPropertyXML() {
        return eftMerchantPropertyXML;
    }

    public void setEftMerchantPropertyXML(String eftMerchantPropertyXML) {
        this.eftMerchantPropertyXML = eftMerchantPropertyXML;
    }

    public String getEftData() {
        return eftData;
    }

    public void setEftData(String eftData) {
        this.eftData = eftData;
    }

    public String getEftDescription() {
        return eftDescription;
    }

    public void setEftDescription(String eftDescription) {
        this.eftDescription = eftDescription;
    }

    public String getEftMerchantId() {
        return eftMerchantId;
    }

    public void setEftMerchantId(String eftMerchantId) {
        this.eftMerchantId = eftMerchantId;
    }

    public String getEftPassword() {
        return eftPassword;
    }

    public void setEftPassword(String eftPassword) {
        this.eftPassword = eftPassword;
    }

    public String getEftServerName() {
        return eftServerName;
    }

    public void setEftServerName(String eftServerName) {
        this.eftServerName = eftServerName;
    }

    public String getEftServerPort() {
        return eftServerPort;
    }

    public void setEftServerPort(String eftServerPort) {
        this.eftServerPort = eftServerPort;
    }

    public String getEftUserId() {
        return eftUserId;
    }

    public void setEftUserId(String eftUserId) {
        this.eftUserId = eftUserId;
    }

    public String getEftTestMode() {
        return eftTestMode;
    }

    public void setEftTestMode(String eftTestMode) {
        this.eftTestMode = eftTestMode;
    }

    public String getMsrEndTrack1() {
        return msrEndTrack1;
    }

    public void setMsrEndTrack1(String msrEndTrack1) {
        this.msrEndTrack1 = msrEndTrack1;
    }

    public String getMsrEndTrack2() {
        return msrEndTrack2;
    }

    public void setMsrEndTrack2(String msrEndTrack2) {
        this.msrEndTrack2 = msrEndTrack2;
    }

    public String getFiscalPrinterManagementReportPAFIdentification() {
        return fiscalPrinterManagementReportPAFIdentification;
    }

    public void setFiscalPrinterManagementReportPAFIdentification(String fiscalPrinterManagementReportPAFIdentification) {
        this.fiscalPrinterManagementReportPAFIdentification = fiscalPrinterManagementReportPAFIdentification;
    }

    public String getFiscalPrinterManagementReportConfigParameter() {
        return fiscalPrinterManagementReportConfigParameter;
    }

    public void setFiscalPrinterManagementReportConfigParameter(String fiscalPrinterManagementReportConfigParameter) {
        this.fiscalPrinterManagementReportConfigParameter = fiscalPrinterManagementReportConfigParameter;
    }

    public String getFiscalPrinterManagementReportTenderType() {
        return fiscalPrinterManagementReportTenderType;
    }

    public void setFiscalPrinterManagementReportTenderType(String fiscalPrinterManagementReportTenderType) {
        this.fiscalPrinterManagementReportTenderType = fiscalPrinterManagementReportTenderType;
    }

    public String getFiscalPrinterManagementReportGiftCard() {
        return fiscalPrinterManagementReportGiftCard;
    }

    public void setFiscalPrinterManagementReportGiftCard(String fiscalPrinterManagementReportGiftCard) {
        this.fiscalPrinterManagementReportGiftCard = fiscalPrinterManagementReportGiftCard;
    }

    public String getKeyboardMappingId() {
        return keyboardMappingId;
    }

    public void setKeyboardMappingId(String keyboardMappingId) {
        this.keyboardMappingId = keyboardMappingId;
    }

    public String getKeyLockDeviceTypeValue() {
        return keyLockDeviceTypeValue;
    }

    public void setKeyLockDeviceTypeValue(String keyLockDeviceTypeValue) {
        this.keyLockDeviceTypeValue = keyLockDeviceTypeValue;
    }

    public String getKeyLockDeviceDescription() {
        return keyLockDeviceDescription;
    }

    public void setKeyLockDeviceDescription(String keyLockDeviceDescription) {
        this.keyLockDeviceDescription = keyLockDeviceDescription;
    }

    public String getKeyLockDeviceName() {
        return keyLockDeviceName;
    }

    public void setKeyLockDeviceName(String keyLockDeviceName) {
        this.keyLockDeviceName = keyLockDeviceName;
    }

    public String getMsrDeviceTypeValue() {
        return msrDeviceTypeValue;
    }

    public void setMsrDeviceTypeValue(String msrDeviceTypeValue) {
        this.msrDeviceTypeValue = msrDeviceTypeValue;
    }

    public String getMsrDeviceDescription() {
        return msrDeviceDescription;
    }

    public void setMsrDeviceDescription(String msrDeviceDescription) {
        this.msrDeviceDescription = msrDeviceDescription;
    }

    public String getMsrDeviceName() {
        return msrDeviceName;
    }

    public void setMsrDeviceName(String msrDeviceName) {
        this.msrDeviceName = msrDeviceName;
    }

    public String getMsrMake() {
        return msrMake;
    }

    public void setMsrMake(String msrMake) {
        this.msrMake = msrMake;
    }

    public String getMsrModel() {
        return msrModel;
    }

    public void setMsrModel(String msrModel) {
        this.msrModel = msrModel;
    }

    public String getMsrSeparator() {
        return msrSeparator;
    }

    public void setMsrSeparator(String msrSeparator) {
        this.msrSeparator = msrSeparator;
    }

    public String getMsrStartTrack() {
        return msrStartTrack;
    }

    public void setMsrStartTrack(String msrStartTrack) {
        this.msrStartTrack = msrStartTrack;
    }

    public String getPinPadDeviceTypeValue() {
        return pinPadDeviceTypeValue;
    }

    public void setPinPadDeviceTypeValue(String pinPadDeviceTypeValue) {
        this.pinPadDeviceTypeValue = pinPadDeviceTypeValue;
    }

    public String getPinPadDeviceName() {
        return pinPadDeviceName;
    }

    public void setPinPadDeviceName(String pinPadDeviceName) {
        this.pinPadDeviceName = pinPadDeviceName;
    }

    public String getPinPadMake() {
        return pinPadMake;
    }

    public void setPinPadMake(String pinPadMake) {
        this.pinPadMake = pinPadMake;
    }

    public String getPinPadModel() {
        return pinPadModel;
    }

    public void setPinPadModel(String pinPadModel) {
        this.pinPadModel = pinPadModel;
    }

    public String getPinPadDeviceDescription() {
        return pinPadDeviceDescription;
    }

    public void setPinPadDeviceDescription(String pinPadDeviceDescription) {
        this.pinPadDeviceDescription = pinPadDeviceDescription;
    }

    public String getProfileDescription() {
        return profileDescription;
    }

    public void setProfileDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getRfidDeviceTypeValue() {
        return rfidDeviceTypeValue;
    }

    public void setRfidDeviceTypeValue(String rfidDeviceTypeValue) {
        this.rfidDeviceTypeValue = rfidDeviceTypeValue;
    }

    public String getRfidDeviceName() {
        return rfidDeviceName;
    }

    public void setRfidDeviceName(String rfidDeviceName) {
        this.rfidDeviceName = rfidDeviceName;
    }

    public String getRfidDeviceDescription() {
        return rfidDeviceDescription;
    }

    public void setRfidDeviceDescription(String rfidDeviceDescription) {
        this.rfidDeviceDescription = rfidDeviceDescription;
    }

    public String getScaleDeviceTypeValue() {
        return scaleDeviceTypeValue;
    }

    public void setScaleDeviceTypeValue(String scaleDeviceTypeValue) {
        this.scaleDeviceTypeValue = scaleDeviceTypeValue;
    }

    public String getScaleDeviceName() {
        return scaleDeviceName;
    }

    public void setScaleDeviceName(String scaleDeviceName) {
        this.scaleDeviceName = scaleDeviceName;
    }

    public String getScaleDeviceDescription() {
        return scaleDeviceDescription;
    }

    public void setScaleDeviceDescription(String scaleDeviceDescription) {
        this.scaleDeviceDescription = scaleDeviceDescription;
    }

    public String getScaleManualInputAllowed() {
        return scaleManualInputAllowed;
    }

    public void setScaleManualInputAllowed(String scaleManualInputAllowed) {
        this.scaleManualInputAllowed = scaleManualInputAllowed;
    }

    public String getScaleTimeoutInSeconds() {
        return scaleTimeoutInSeconds;
    }

    public void setScaleTimeoutInSeconds(String scaleTimeoutInSeconds) {
        this.scaleTimeoutInSeconds = scaleTimeoutInSeconds;
    }

    public String getSignatureCaptureDeviceTypeValue() {
        return signatureCaptureDeviceTypeValue;
    }

    public void setSignatureCaptureDeviceTypeValue(String signatureCaptureDeviceTypeValue) {
        this.signatureCaptureDeviceTypeValue = signatureCaptureDeviceTypeValue;
    }

    public String getSignatureCaptureDeviceName() {
        return signatureCaptureDeviceName;
    }

    public void setSignatureCaptureDeviceName(String signatureCaptureDeviceName) {
        this.signatureCaptureDeviceName = signatureCaptureDeviceName;
    }

    public String getSignatureCaptureMake() {
        return signatureCaptureMake;
    }

    public void setSignatureCaptureMake(String signatureCaptureMake) {
        this.signatureCaptureMake = signatureCaptureMake;
    }

    public String getSignatureCaptureModel() {
        return signatureCaptureModel;
    }

    public void setSignatureCaptureModel(String signatureCaptureModel) {
        this.signatureCaptureModel = signatureCaptureModel;
    }

    public String getSignatureCaptureDeviceDescription() {
        return signatureCaptureDeviceDescription;
    }

    public void setSignatureCaptureDeviceDescription(String signatureCaptureDeviceDescription) {
        this.signatureCaptureDeviceDescription = signatureCaptureDeviceDescription;
    }

    public String getSignatureCaptureFormName() {
        return signatureCaptureFormName;
    }

    public void setSignatureCaptureFormName(String signatureCaptureFormName) {
        this.signatureCaptureFormName = signatureCaptureFormName;
    }

    public List<AxHardwareProfileCashDrawer> getAxHardwareProfileCashDrawerList() {
        return axHardwareProfileCashDrawerList;
    }

    public void setAxHardwareProfileCashDrawerList(List<AxHardwareProfileCashDrawer> axHardwareProfileCashDrawerList) {
        this.axHardwareProfileCashDrawerList = axHardwareProfileCashDrawerList;
    }

}
