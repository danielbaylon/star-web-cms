package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;

public class PartnerExtVM {

    private Integer id;
    private Integer partnerId;
    private Boolean approvalRequired;
    private String attrName;
    private String attrValue;
    private String attrValueLabel;
    private String attrValueType;
    private String attrValueFormat;
    private Boolean mandatoryInd;
    private String description;
    private String status;

    public PartnerExtVM() {
    }

    public PartnerExtVM(PPSLMPartnerExt partnerExt) {
        this.id = partnerExt.getId();
        this.partnerId = partnerExt.getPartnerId();
        this.approvalRequired = partnerExt.getApprovalRequired();
        this.attrName = partnerExt.getAttrName();
        this.attrValue = partnerExt.getAttrValue();
        this.attrValueType = partnerExt.getAttrValueType();
        this.attrValueFormat = partnerExt.getAttrValueFormat();
        this.mandatoryInd = partnerExt.getMandatoryInd();
        this.description = partnerExt.getDescription();
        this.status = partnerExt.getStatus();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Boolean getApprovalRequired() {
        return approvalRequired;
    }

    public void setApprovalRequired(Boolean approvalRequired) {
        this.approvalRequired = approvalRequired;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getAttrValueType() {
        return attrValueType;
    }

    public void setAttrValueType(String attrValueType) {
        this.attrValueType = attrValueType;
    }

    public String getAttrValueFormat() {
        return attrValueFormat;
    }

    public void setAttrValueFormat(String attrValueFormat) {
        this.attrValueFormat = attrValueFormat;
    }

    public Boolean getMandatoryInd() {
        return mandatoryInd;
    }

    public void setMandatoryInd(Boolean mandatoryInd) {
        this.mandatoryInd = mandatoryInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttrValueLabel() {
        return attrValueLabel;
    }

    public void setAttrValueLabel(String attrValueLabel) {
        this.attrValueLabel = attrValueLabel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((attrName == null) ? 0 : attrName.hashCode());
        result = prime * result
                + ((attrValue == null) ? 0 : attrValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PartnerExtVM other = (PartnerExtVM) obj;
        if (attrName == null) {
            if (other.attrName != null)
                return false;
        } else if (!attrName.equals(other.attrName))
            return false;
        if (attrValue == null) {
            if (other.attrValue != null)
                return false;
        } else if (!attrValue.equals(other.attrValue))
            return false;

        return true;
    }

}
