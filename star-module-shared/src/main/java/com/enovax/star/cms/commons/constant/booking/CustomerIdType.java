package com.enovax.star.cms.commons.constant.booking;

/**
 * Created by jennylynsze on 11/15/16.
 */
public enum CustomerIdType {
    NricFin,
    Passport
}
