package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by houtao on 26/8/16.
 */
@Entity
@Table(name = PPSLMWingsOfTimeReservationLog.TABLE_NAME)
public class PPSLMWingsOfTimeReservationLog {

    public static final String TABLE_NAME = "PPSLMWingsOfTimeReservationLog";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "wotReserveId")
    private Integer wotReserveId;

    @Column(name = "isAdminRequest")
    private boolean isAdminRequest;

    @Column(name = "isSubAccountTrans")
    private boolean isSubAccountTrans;

    @Column(name = "channel")
    private String channel;

    @Column(name = "serviceName")
    private String serviceName;

    @Column(name = "success")
    private boolean success;

    @Column(name = "errorCode")
    private String errorCode;

    @Column(name = "errorMsg")
    private String errorMsg;

    @Column(name = "requestText")
    private String requestText;

    @Column(name = "responseText")
    private String responseText;

    @Column(name = "createdDateTime")
    private Date createdDateTime;

    @Column(name = "createdBy")
    private String createdBy;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWotReserveId() {
        return wotReserveId;
    }

    public void setWotReserveId(Integer wotReserveId) {
        this.wotReserveId = wotReserveId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSubAccountTrans() {
        return isSubAccountTrans;
    }

    public void setSubAccountTrans(boolean subAccountTrans) {
        isSubAccountTrans = subAccountTrans;
    }

    public boolean isAdminRequest() {
        return isAdminRequest;
    }

    public void setAdminRequest(boolean adminRequest) {
        isAdminRequest = adminRequest;
    }
}
