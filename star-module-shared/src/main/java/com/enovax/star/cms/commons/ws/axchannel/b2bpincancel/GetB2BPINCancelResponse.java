
package com.enovax.star.cms.commons.ws.axchannel.b2bpincancel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetB2BPINCancelResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINCancelResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getB2BPINCancelResult"
})
@XmlRootElement(name = "GetB2BPINCancelResponse", namespace = "http://tempuri.org/")
public class GetB2BPINCancelResponse {

    @XmlElementRef(name = "GetB2BPINCancelResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINCancelResponse> getB2BPINCancelResult;

    /**
     * Gets the value of the getB2BPINCancelResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINCancelResponse> getGetB2BPINCancelResult() {
        return getB2BPINCancelResult;
    }

    /**
     * Sets the value of the getB2BPINCancelResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}
     *     
     */
    public void setGetB2BPINCancelResult(JAXBElement<SDCB2BPINCancelResponse> value) {
        this.getB2BPINCancelResult = value;
    }

}
