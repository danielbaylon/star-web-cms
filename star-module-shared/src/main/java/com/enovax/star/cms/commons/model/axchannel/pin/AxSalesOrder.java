package com.enovax.star.cms.commons.model.axchannel.pin;

import java.util.List;

/**
 * Created by houtao on 23/8/16.
 */
public class AxSalesOrder extends AxTransaction {

    private String id;
    private Long channelId;
    private String confirmationId;
    private String status;
    private String salesId;
    private String requestedDeliveryDate;
    private Integer paymentStatus;
    private String orderPlacedDate;

    private List<AxTransactionItem> txnItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(String confirmationId) {
        this.confirmationId = confirmationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getOrderPlacedDate() {
        return orderPlacedDate;
    }

    public void setOrderPlacedDate(String orderPlacedDate) {
        this.orderPlacedDate = orderPlacedDate;
    }

    public List<AxTransactionItem> getTxnItems() {
        return txnItems;
    }

    public void setTxnItems(List<AxTransactionItem> txnItems) {
        this.txnItems = txnItems;
    }
}
