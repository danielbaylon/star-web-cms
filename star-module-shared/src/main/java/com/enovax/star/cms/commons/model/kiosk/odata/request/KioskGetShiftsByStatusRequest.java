package com.enovax.star.cms.commons.model.kiosk.odata.request;

public class KioskGetShiftsByStatusRequest {

    private String statusValue;

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

}
