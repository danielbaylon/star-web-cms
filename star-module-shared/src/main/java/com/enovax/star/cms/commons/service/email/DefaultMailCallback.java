package com.enovax.star.cms.commons.service.email;

import org.slf4j.Logger;

import java.lang.ref.WeakReference;

public class DefaultMailCallback implements MailCallback {
    
    private WeakReference<Logger> log;
    
    public DefaultMailCallback(Logger log) {
        this.log = new WeakReference<Logger>(log);
    }
    
    @Override
    public void complete(String trackingId, boolean success) {
        final Logger l = this.log.get();
        if (l != null) {
            l.info("Email: {}, Status: {}", trackingId, success ? "Success" : "Failure");
        }
    }
}
