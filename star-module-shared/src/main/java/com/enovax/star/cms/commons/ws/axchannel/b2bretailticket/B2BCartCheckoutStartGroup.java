
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CartTicketListCriteria" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_CartTicketListCriteria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cartTicketListCriteria"
})
@XmlRootElement(name = "B2BCartCheckoutStartGroup", namespace = "http://tempuri.org/")
public class B2BCartCheckoutStartGroup {

    @XmlElementRef(name = "CartTicketListCriteria", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCartTicketListCriteria> cartTicketListCriteria;

    /**
     * Gets the value of the cartTicketListCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> getCartTicketListCriteria() {
        return cartTicketListCriteria;
    }

    /**
     * Sets the value of the cartTicketListCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}
     *     
     */
    public void setCartTicketListCriteria(JAXBElement<ArrayOfSDCCartTicketListCriteria> value) {
        this.cartTicketListCriteria = value;
    }

}
