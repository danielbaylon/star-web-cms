package com.enovax.star.cms.commons.mgnl.contentapp;

import info.magnolia.cms.security.User;
import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.app.AppDescriptor;
import info.magnolia.ui.api.app.AppView;
import info.magnolia.ui.api.app.SubAppDescriptor;
import info.magnolia.ui.api.location.DefaultLocation;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.contentapp.ContentApp;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by jennylynsze on 8/4/16.
 */
public class StarContentApp extends ContentApp {

    //private SecuritySupport securitySupport;

    @Inject
    public StarContentApp(AppContext appContext, AppView view, ComponentProvider componentProvider) {
        super(appContext, view, componentProvider);
    }

    @Override
    public void start(Location location)
    {
        User user = MgnlContext.getUser();
        AppDescriptor appDescriptor = getAppContext().getAppDescriptor();
        Map<String, SubAppDescriptor> map = appDescriptor.getSubApps();

        String firstSubAppLocation = null;
        String firstSubAppNameWithRights = null;

        for (String subAppName : map.keySet()) {
            if(!map.get(subAppName).isClosable()) {
                boolean openSubApp = false;

                SubAppDescriptor subAppDescriptor = map.get(subAppName);

                StarBrowserSubAppDescriptor starBrowserSubAppDescriptor;

                if(subAppDescriptor instanceof StarBrowserSubAppDescriptor) {
                    starBrowserSubAppDescriptor = (StarBrowserSubAppDescriptor) subAppDescriptor;
                    AccessDefinition accessDefinition = starBrowserSubAppDescriptor.getPermissions();


                    if(accessDefinition != null) {
                        if(accessDefinition.hasAccess(user)) {
                            openSubApp = true;
                        }
                    }else {
                        //no rights define means no restrictions
                        openSubApp = true;
                    }

                    if(openSubApp) {
                        if(starBrowserSubAppDescriptor.isOpenFirst()) {
                            firstSubAppLocation = subAppName;
                        }

                        if(firstSubAppNameWithRights == null && StringUtils.isEmpty(starBrowserSubAppDescriptor.getMainSubAppDisplay())) {
                            firstSubAppNameWithRights = subAppName;
                        }
                    }

                }else {
                    //assume that it is using a default browser then maybe it have no rights checking or something
                    openSubApp = true;
                }

                if(openSubApp) {
                    getAppContext().openSubApp(new DefaultLocation("app", getAppContext().getName(), subAppName));
                }
            }
        }

        if(firstSubAppLocation != null) {
            locationChanged(new DefaultLocation("app", getAppContext().getName(), firstSubAppLocation));
        }else if(firstSubAppNameWithRights != null) {
            locationChanged(new DefaultLocation("app", getAppContext().getName(), firstSubAppNameWithRights));
        }

    }


    //    private void getPermission(String role) {
//        SecurityUtil.
//        Iterator iterPermission = Security.getRoleManager().getACLs(role).values().iterator();
//        this.permissionList.add("<ul>");
//
//        while(true) {
//            ACL acl;
//            do {
//                if(!iterPermission.hasNext()) {
//                    this.permissionList.add("</ul>");
//                    return;
//                }
//
//                acl = (ACL)iterPermission.next();
//            } while(acl.getList().isEmpty());
//
//            Iterator i$ = acl.getList().iterator();
//
//            while(i$.hasNext()) {
//                Permission permission = (Permission)i$.next();
//                String repoName = acl.getName();
//                String message = this.getMessages().get("permissionlist.permission", new String[]{this.getPermissionAsName(repoName, permission), repoName, permission.getPattern().getPatternString()});
//                this.permissionList.add("<li>" + message + "</li>");
//            }
//        }
//    }

}
