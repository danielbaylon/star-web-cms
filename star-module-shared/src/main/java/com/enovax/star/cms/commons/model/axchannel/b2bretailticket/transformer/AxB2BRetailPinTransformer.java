package com.enovax.star.cms.commons.model.axchannel.b2bretailticket.transformer;

import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinLine;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ArrayOfSDCPinLine;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCPinLine;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCPinTable;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AxB2BRetailPinTransformer {

    public static List<AxRetailPinTable> fromWsGeneratePin(List<SDCPinTable> pinTables) {
        final List<AxRetailPinTable> pinResults = new ArrayList<>();

        for (SDCPinTable pinTable : pinTables) {
            final AxRetailPinTable pin = new AxRetailPinTable();

            pin.setChannelId(pinTable.getCHANNELID());
            pin.setCustomerAccount(pinTable.getCUSTACCOUNT().getValue());
            pin.setDataAreaId(pinTable.getDATAAREAID().getValue());
            pin.setDescription(pinTable.getDESCRIPTION().getValue());

            final XMLGregorianCalendar wsTicketEndDate = pinTable.getENDDATETIME();
            pin.setEndDateTime(wsTicketEndDate == null ? new Date() : wsTicketEndDate.toGregorianCalendar().getTime());

            pin.setGroupTicket(pinTable.getISGROUPTICKET() == 1);
            pin.setMediaType(pinTable.getMEDIATYPE().getValue());
            pin.setPackageName(pinTable.getPACKAGENAME().getValue());
            pin.setPinCode(pinTable.getPINCODE().getValue());
            pin.setQtyPerProduct(pinTable.getQTYPERPRODUCT().intValue());
            pin.setReferenceId(pinTable.getREFERENCEID().getValue());
            pin.setReplicationCounterFromOrigin(pinTable.getREPLICATIONCOUNTERFROMORIGIN());
            pin.setSourceType(pinTable.getSOURCETYPE());

            final XMLGregorianCalendar wsTicketStartDate = pinTable.getSTARTDATETIME();
            pin.setStartDateTime(wsTicketStartDate == null ? new Date() : wsTicketStartDate.toGregorianCalendar().getTime());

            pin.setTransactionId(pinTable.getTRANSACTIONID().getValue());

            final ArrayOfSDCPinLine arrayOfSDCPinLine = pinTable.getSDCPinLineCollection().getValue();
            final List<SDCPinLine> sdcPinLines = arrayOfSDCPinLine.getSDCPinLine();

            pin.setLines(transformFromSdcPinLines(sdcPinLines));

            pinResults.add(pin);
        }

        return pinResults;
    }

    public static List<AxRetailPinLine> transformFromSdcPinLines(List<SDCPinLine> sdcPinLines) {
        final List<AxRetailPinLine> pinLines = new ArrayList<>();

        for (SDCPinLine pl : sdcPinLines) {
            final AxRetailPinLine line = new AxRetailPinLine();

            line.setChannelId(pl.getCHANNELID());
            line.setDataAreaId(pl.getDATAAREAID().getValue());

            final XMLGregorianCalendar wsEndDate = pl.getENDDATETIME();
            line.setEndDateTime(wsEndDate == null ? new Date() : wsEndDate.toGregorianCalendar().getTime());

            final JAXBElement<XMLGregorianCalendar> wsEventDateElement = pl.getEVENTDATE();
            final XMLGregorianCalendar wsEventDate = wsEventDateElement.getValue();
            line.setEventDate(wsEventDate == null ? null: wsEventDate.toGregorianCalendar().getTime());

            line.setEventGroupId(pl.getEVENTGROUPID().getValue());
            line.setEventLineId(pl.getEVENTLINEID().getValue());
            line.setInventTransId(pl.getINVENTTRANSID().getValue());
            line.setInvoiceId(pl.getINVOICEID().getValue());
            line.setCombinedTicket(pl.getISCOMBINETICKET() == 1);
            line.setRedeemed(pl.getISREDEEMED() == 1);
            line.setItemId(pl.getITEMID().getValue());
            line.setLineNum(pl.getLINENUM().intValue());
            line.setMediaType(pl.getMEDIATYPE().getValue());
            line.setOpenValidityRule(pl.getOPENVALIDITYRULE().getValue());
            line.setQty(pl.getQTY().intValue());
            line.setQtyRedeemed(pl.getQTYREDEEMED().intValue());
            line.setQtyReturned(pl.getQTYRETURNED().intValue());
            line.setReferenceId(pl.getREFERENCEID().getValue());
            line.setReplicationCounterFromOrigin(pl.getREPLICATIONCOUNTERFROMORIGIN());
            line.setRetailVariantId(pl.getRETAILVARIANTID().getValue());
            line.setSalesId(pl.getSALESID().getValue());

            final XMLGregorianCalendar wsStartDate = pl.getSTARTDATETIME();
            line.setStartDateTime(wsStartDate == null ? new Date() : wsStartDate.toGregorianCalendar().getTime());

            line.setTransactionId(pl.getTRANSACTIONID().getValue());

            pinLines.add(line);
        }

        return pinLines;
    }


}
