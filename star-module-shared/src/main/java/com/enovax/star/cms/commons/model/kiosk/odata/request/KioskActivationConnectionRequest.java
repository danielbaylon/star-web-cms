package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxActivationConnectionRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskActivationConnectionRequest {

	@JsonProperty("connectionRequest")
	private AxActivationConnectionRequest activationConnectionRequest = new AxActivationConnectionRequest();

	public AxActivationConnectionRequest getActivationConnectionRequest() {
		return activationConnectionRequest;
	}

	public void setActivationConnectionRequest(AxActivationConnectionRequest activationConnectionRequest) {
		this.activationConnectionRequest = activationConnectionRequest;
	}

}
