package com.enovax.star.cms.commons.model.search;

import java.util.Map;

/**
 * Created by jonathan on 28/12/16.
 */
public class QsProd implements Comparable<QsProd> {

    private String id;
    private String displayTitle;
    private Map<String, String> displayTitleI18N;
    private String rawTitle;
    private String displayDescription;
    private Map<String, String> displayDescriptionI18N;
    private String urlTitle;
    private String shortDescription;
    private String category;
    private boolean isDefault;
    private boolean foundInTitle;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDisplayTitle() {
        return displayTitle;
    }
    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }
    public String getDisplayDescription() {
        return displayDescription;
    }
    public void setDisplayDescription(String displayDescription) {
        this.displayDescription = displayDescription;
    }
    public String getUrlTitle() {
        return urlTitle;
    }
    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }
    public String getShortDescription() {
        return shortDescription;
    }
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    public boolean getFoundInTitle() {
        return foundInTitle;
    }
    public void setFoundInTitle(boolean foundInTitle) {
        this.foundInTitle = foundInTitle;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public boolean getIsDefault() {
        return isDefault;
    }
    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }
    public String getRawTitle() {
        return rawTitle;
    }
    public void setRawTitle(String rawTitle) {
        this.rawTitle = rawTitle;
    }
    public Map<String, String> getDisplayTitleI18N() {
        return displayTitleI18N;
    }
    public void setDisplayTitleI18N(Map<String, String> displayTitleI18N) {
        this.displayTitleI18N = displayTitleI18N;
    }
    public Map<String, String> getDisplayDescriptionI18N() {
        return displayDescriptionI18N;
    }
    public void setDisplayDescriptionI18N(Map<String, String> displayDescriptionI18N) {
        this.displayDescriptionI18N = displayDescriptionI18N;
    }
    @Override
    public int compareTo(QsProd o) {
        if (this.foundInTitle && !o.foundInTitle) {
            return -1;
        }
        if (!this.foundInTitle && o.foundInTitle) {
            return 1;
        }

        return this.rawTitle.compareTo(o.rawTitle);
    }

}
