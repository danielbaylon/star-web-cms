package com.enovax.star.cms.commons.model.jcrworkspace;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * From mgnl:ax-product jcr workspace
 */
public class AxProduct implements Serializable {
    private long productListingId;
    private long displayProductNumber;
    private int productType;
    private String productSearchName;
    private String productName;
    private String productDesc;
    private BigDecimal productPrice;
    private int productPriceQuantity;
    private String productPriceUnitId;

    public AxProduct() {}

    public long getProductListingId() {
        return productListingId;
    }

    public void setProductListingId(long productListingId) {
        this.productListingId = productListingId;
    }

    public long getDisplayProductNumber() {
        return displayProductNumber;
    }

    public void setDisplayProductNumber(long displayProductNumber) {
        this.displayProductNumber = displayProductNumber;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getProductSearchName() {
        return productSearchName;
    }

    public void setProductSearchName(String productSearchName) {
        this.productSearchName = productSearchName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductPriceQuantity() {
        return productPriceQuantity;
    }

    public void setProductPriceQuantity(int productPriceQuantity) {
        this.productPriceQuantity = productPriceQuantity;
    }

    public String getProductPriceUnitId() {
        return productPriceUnitId;
    }

    public void setProductPriceUnitId(String productPriceUnitId) {
        this.productPriceUnitId = productPriceUnitId;
    }
}
