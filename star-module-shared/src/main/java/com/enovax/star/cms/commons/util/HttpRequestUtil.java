package com.enovax.star.cms.commons.util;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.LoggerFactory;

public class HttpRequestUtil {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(HttpRequestUtil.class);

    public static final int HTTP_RESPONSE_STATUS_OK = 200;
    public static final int HTTP_RESPONSE_STATUS_NO_CONTENT = 204;

    public static final String HTTP_REQUEST_METHOD_POST = "POST";

    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final String ACCEPT_TYPE_JSON = "application/json";

    public static final String CONTENT_TYPE_TEXT = "application/x-www-form-urlencoded";

    public static final String ACCEPT_TYPE_TEXT = "text/plain";

    private String acceptType = null;
    private String contentType = null;

    public HttpRequestUtil(){
    }
    public String request(String url, String request) throws Exception {
        URL requestURL = null;
        try {
            requestURL = new URL(url);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }

        HttpURLConnection httpURLConnection = null;

        try {
            httpURLConnection = (HttpURLConnection) requestURL.openConnection();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }

        httpURLConnection.setDoOutput(true);

        try {
            httpURLConnection.setRequestMethod(HTTP_REQUEST_METHOD_POST);
        } catch (ProtocolException e) {
            logger.error(e.getMessage());
            throw e;
        }

        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setDefaultUseCaches(false);

        httpURLConnection.setRequestProperty("Content-Type", contentType.trim());

        httpURLConnection.setRequestProperty("Accept", acceptType.trim());

        OutputStreamWriter outputStream = null;
        try {
            outputStream = new OutputStreamWriter(httpURLConnection.getOutputStream());
            if(request != null && request.trim().length() > 0){
                outputStream.write(request.toString());
                outputStream.flush();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally{
            try{
                outputStream.close();
            }catch(Exception ex2){}
        }

        try{
            return handleHttpURLConnectionTextResponse(httpURLConnection);
        }catch(Exception e){
            logger.error(e.getMessage());
            throw e;
        }
    }

    public JsonObject request(String url, JsonObject request) throws Exception {

        URL requestURL = null;
        try {
            requestURL = new URL(url);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }

        HttpURLConnection httpURLConnection = null;

        try {
            httpURLConnection = (HttpURLConnection) requestURL.openConnection();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }

        httpURLConnection.setDoOutput(true);

        try {
            httpURLConnection.setRequestMethod(HTTP_REQUEST_METHOD_POST);
        } catch (ProtocolException e) {
            logger.error(e.getMessage());
            throw e;
        }

        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setDefaultUseCaches(false);

        httpURLConnection.setRequestProperty("Content-Type", contentType.trim());

        httpURLConnection.setRequestProperty("Accept", acceptType.trim());

        OutputStreamWriter outputStream = null;
        try {
            outputStream = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStream.write(request.toString());
            outputStream.flush();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally{
            try{
                outputStream.close();
            }catch(Exception ex2){}
        }

        try{
            return handleHttpURLConnectionResponse(httpURLConnection);
        }catch(Exception e){
            logger.error(e.getMessage());
            throw e;
        }
    }

    public JsonObject handleHttpURLConnectionResponse(HttpURLConnection conn) throws Exception {
        InputStream is = null;
        try {
            is = conn.getInputStream();
            int ch;
            StringBuffer sb = new StringBuffer();
            while ((ch = is.read()) != -1) {
                sb.append((char) ch);
            }
            if(sb.length() > 0){
                JsonParser parser = new JsonParser();
                JsonElement ele = parser.parse(sb.toString().trim());
                return ele.getAsJsonObject();
            }
            return new JsonObject();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            if (is != null) {
                try{
                    is.close();
                }catch(Exception ex1){}
            }
        }
    }

    public String handleHttpURLConnectionTextResponse(HttpURLConnection conn) throws Exception {
        InputStream is = null;
        try {
            is = conn.getInputStream();
            int ch;
            StringBuffer sb = new StringBuffer();
            while ((ch = is.read()) != -1) {
                sb.append((char) ch);
            }
            return sb.toString().trim();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            if (is != null) {
                try{
                    is.close();
                }catch(Exception ex1){}
            }
        }
    }

    public String getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(String acceptType) {
        this.acceptType = acceptType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}