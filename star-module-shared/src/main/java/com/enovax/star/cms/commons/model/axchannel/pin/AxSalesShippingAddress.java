package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 23/8/16.
 */
public class AxSalesShippingAddress {

    protected String addressFriendlyName;
    protected AxSalesShippingAddressType addressType;
    protected String attentionTo;
    protected String city;
    protected String country;
    protected String county;
    protected Boolean deactivate;
    protected String districtName;
    protected String email;
    protected String emailContent;
    protected Boolean isPrimary;
    protected String name;
    protected String phone;
    protected Long recordId;
    protected String state;
    protected String street;
    protected String streetNumber;
    protected String zipCode;

    public String getAddressFriendlyName() {
        return addressFriendlyName;
    }

    public void setAddressFriendlyName(String addressFriendlyName) {
        this.addressFriendlyName = addressFriendlyName;
    }

    public AxSalesShippingAddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AxSalesShippingAddressType addressType) {
        this.addressType = addressType;
    }

    public String getAttentionTo() {
        return attentionTo;
    }

    public void setAttentionTo(String attentionTo) {
        this.attentionTo = attentionTo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Boolean getDeactivate() {
        return deactivate;
    }

    public void setDeactivate(Boolean deactivate) {
        this.deactivate = deactivate;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
