package com.enovax.star.cms.commons.constant.ppmflg;

import com.enovax.star.cms.commons.util.JsonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum OfflinePaymentType {
	CHEQUE("Cheque","Cheque"),
	BANK_TRANSFER("BankTransfer","Bank Transfer / Telegraphic Transfer");
    public String code;
    public String name;

    private OfflinePaymentType(String code, String name){
        this.code = code;
        this.name = name;       
    }

	public static final String toJson() {
		List<HashMap> res = new ArrayList<HashMap>();
		for (final OfflinePaymentType tmpObj : OfflinePaymentType.values()) {
			HashMap map = new HashMap<String, String>();
			map.put("code", tmpObj.code);
			map.put("name", tmpObj.name);
			res.add(map);
		}
		return JsonUtil.jsonify(res);
	}
}
