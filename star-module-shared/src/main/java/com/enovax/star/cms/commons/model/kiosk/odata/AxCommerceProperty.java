package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty
 * 
 * @author Justin
 *
 */
public class AxCommerceProperty {

    @JsonProperty("Key")
    private String key;

    @JsonProperty("Value")
    private AxCommercePropertyValue value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public AxCommercePropertyValue getValue() {
        return value;
    }

    public void setValue(AxCommercePropertyValue value) {
        this.value = value;
    }

}
