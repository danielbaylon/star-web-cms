package com.enovax.star.cms.commons.util;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class NvxDateUtils {

    public static final String DEFAULT_DATE_FORMAT_DISPLAY = "dd/MM/yyyy";
    public static final String DEFAULT_DATE_FORMAT_DISPLAY_TIME = "dd/MM/yyyy HH:mm:ss";
    public static final String DEFAULT_DATE_FORMAT_DISPLAY_TIME_MS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DEFAULT_DATE_FORMAT_DISPLAY_HHMM = "dd/MM/yyyy HH:mm";
    public static final String DEFAULT_TIME_FORMAT_DISPLAY_MMSS = "mm:ss";
    public static final String DEFAULT_TIME_FORMAT_DISPLAY_HHMM = "HH:mm";
    public static final String DEFAULT_TIME_FORMAT_DISPLAY_READABLE = "hh:mm a";
    public static final String DEFAULT_DATE_TIME_FORMAT_DISPLAY_READABLE = "dd/MM/yyyy hh:mm a";
    public static final String BOOKING_DATE_FORMAT_DISPLAY = "dd MMMMM, yyyy";
    public static final String PRODUCT_DETAILS_DATE_FORMAT_DISPLAY = "dd MMM yyyy";

    public static final String AX_TICKET_DATA_VALIDATOR_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String AX_CUSTOMER_EXT_SERVICE_DATE_FORMAT = "yyyy/MM/dd";
    public static final String AX_WOT_PROD_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String AX_WOT_PROD_DATE_FORMAT = "yyyy-MM-dd";
    public static final String AX_ONLINE_CAT_SERVICE_WOT_RESERVE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String AX_ONLINE_CAT_SERVICE_WOT_RESERVE_TIME_FORMAT = "hh:mm a";
    public static final String WOT_TICKET_DATE_PRINT_FORMAT = "EEEE, MMMM dd, yyyy";
    public static final String WOT_TICKET_DATE_PRINT_FORMAT_DASH = "yyyy-MM-dd";
    public static final String WOT_TICKET_MONTH_PRINT_FORMAT_DASH = "yyyy-MM";
    public static final String WOT_RECEIPT_DATE_PRINT_FORMAT = "MMMM dd, yyyy";
    public static final String WOT_RECEIPT_MONTH_PRINT_FORMAT = "MMMM, yyyy";
    public static final String WOT_TICKET_MONTH_PRINT_FORMAT = "MMMM, yyyy";
    public static final String WOT_TICKET_DATE_EXPORT_FORMAT_DATE = "dd MMMM yyyy";
    public static final String WOT_TICKET_DATE_EXPORT_FORMAT_WEEK = "EEEE";
    public static final String WOT_TICKET_DATE_TIME_PRINT_FORMAT = "EEEE, MMMM dd, yyyy h:mm a";
    public static final String WOT_TICKET_TIME_PRINT_FORMAT = "h:mm a";
    public static final String AX_DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String AX_DEFAULT_DATE_TIME_MSEC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String AX_DEFAULT_DATE_TIME_ZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String KIOSK_TICKET_TIME_PRINT_FORMAT = "HH:mm";
    public static final String KIOSK_TICKET_DATE_PRINT_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT_SCHEDULE = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter utcFmt = DateTimeFormat.forPattern(DATE_TIME_FORMAT).withZoneUTC();

    public static Calendar timeToUtcCal(String text) {
        final Calendar c = utcFmt.parseDateTime(text).toCalendar(null);
        return c;
    }

    public static String formatDateForDisplay(Date date, boolean includeTime) {
        return formatDate(date, includeTime ? DEFAULT_DATE_FORMAT_DISPLAY_TIME : DEFAULT_DATE_FORMAT_DISPLAY);
    }

    public static String formatDate(Date date, String pattern) {
        return (new SimpleDateFormat(pattern)).format(date);
    }

    public static Date parseDateFromDisplay(String date, boolean includeTime) {
        try {
            return parseDate(date, includeTime ? DEFAULT_DATE_FORMAT_DISPLAY_TIME : DEFAULT_DATE_FORMAT_DISPLAY);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date parseDate(String date, String pattern) throws ParseException {
        return (new SimpleDateFormat(pattern)).parse(date);
    }

    public static Date parseAxDate(String date, String pattern) throws ParseException {
        if (date == null || date.trim().length() == 0) {
            return null;
        }
        return (new SimpleDateFormat(pattern)).parse(date);
    }

    public static boolean isWeekday(Date date, List<Date> publicHolidays) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);

        if (day == Calendar.SUNDAY || day == Calendar.SATURDAY) {
            return false;
        }

        if (publicHolidays != null) {
            final Date dateToCompare = DateUtils.truncate(date, Calendar.DATE);
            for (Date holiday : publicHolidays) {
                if (DateUtils.truncate(holiday, Calendar.DATE).equals(dateToCompare)) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean datesEqualDayField(Date date1, Date date2) {
        return DateUtils.truncate(date1, Calendar.DATE).equals(DateUtils.truncate(date2, Calendar.DATE));
    }

    public static Date clearTime(Date date) {
        Date newDate;
        try {
            newDate = NvxDateUtils.parseDate(NvxDateUtils.formatDate(date, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            return newDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static Date timeOnly(Date date) {
        Date newDate;
        try {
            newDate = NvxDateUtils.parseDate(NvxDateUtils.formatDate(date, NvxDateUtils.DEFAULT_TIME_FORMAT_DISPLAY_HHMM), NvxDateUtils.DEFAULT_TIME_FORMAT_DISPLAY_HHMM);
            return newDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static int daysBetween(Date dateFrom, Date dateTo) {
        return (int) ((dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static int compareToCurrentDateDayField(Date dateToCompare, int daysOffset) {
        Date trunc1 = DateUtils.truncate(dateToCompare, Calendar.DATE);
        Date offsetCurrentDate = DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DATE), daysOffset);

        return trunc1.compareTo(offsetCurrentDate);
    }

    public static Date populateQueryFromDate(Date date, boolean includeTime) {
        if (!includeTime) {
            return date;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT_DISPLAY);
        String dateStr = sdf.format(date);
        dateStr = dateStr + " 00:00:00";
        sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        try {
            return sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date populateQueryToDate(Date date, boolean includeTime) {
        if (!includeTime) {
            return date;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT_DISPLAY);
        String dateStr = sdf.format(date);
        dateStr = dateStr + " 23:59:59";
        sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        try {
            return sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static final String MOD_DT_PATTERN = "dd/MM/yyyy HH:mm:ss";

    public static String formatModDt(Date dt) {
        return new SimpleDateFormat(MOD_DT_PATTERN).format(dt);
    }

    public static Date parseModDt(String dt) {
        try {
            return new SimpleDateFormat(MOD_DT_PATTERN).parse(dt);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean compareModDates(String oldModDt, Date actualModDt) {
        final Date toCompare = parseModDt(oldModDt);
        final Seconds secs = Seconds.secondsBetween(new DateTime(toCompare), new DateTime(actualModDt));
        int woah = secs.getSeconds();
        return woah == 0;
    }

    public static int hoursAgo(Date datetime) {
        Calendar date = Calendar.getInstance();
        date.setTime(datetime);
        Calendar now = Calendar.getInstance(); // Get time now
        long differenceInMillis = now.getTimeInMillis() - date.getTimeInMillis();
        long differenceInHours = (differenceInMillis) / 1000L / 60L / 60L; // Divide
                                                                           // by
                                                                           // millis/sec,
                                                                           // secs/min,
                                                                           // mins/hr
        return (int) differenceInHours;
    }

    public static String formatTime(Time time, String pattern) {
        return (new SimpleDateFormat(pattern)).format(time);
    }

    public static Time parseTime(String time, String pattern) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        final long ms = sdf.parse(time).getTime();
        return new Time(ms);
    }

    public static boolean inDateRange(Date startDate, Date endDate, Date checkDate, Integer cutOffDays, boolean inclusive, boolean truncate) {
        Date dt = null;
        Date startDt = null;
        Date endDt = null;

        if (checkDate != null) {
            dt = truncate ? DateUtils.truncate(checkDate, Calendar.DATE) : checkDate;
        }

        if (startDate != null) {
            startDt = truncate ? DateUtils.truncate(startDate, Calendar.DATE) : startDate;
        }

        if (endDate != null) {
            endDt = truncate ? DateUtils.truncate(endDate, Calendar.DATE) : endDate;
            if (cutOffDays != null) {
                endDt = DateUtils.addDays(endDate, cutOffDays * -1);
            }
        }

        boolean afterStartDate = true;
        boolean beforeEndDate = true;

        if (startDt != null) {
            afterStartDate = startDt.before(dt);
            if (inclusive) {
                afterStartDate = afterStartDate || startDt.equals(dt);
            }
        }

        if (endDt != null) {
            beforeEndDate = endDt.after(dt);
            if (inclusive) {
                beforeEndDate = beforeEndDate || endDt.equals(dt);
            }
        }

        return afterStartDate && beforeEndDate;
    }

    /**
     * Checks if a date is between the start date and the end date. If there is
     * no start/end date, it will not check against that date.
     *
     * @param startDate
     *            Start date can be null.
     * @param endDate
     *            End date can be null.
     * @param checkDate
     *            Date to check.
     * @param inclusive
     *            Whether or not the start/end date is included.
     * @param truncate
     *            Ignore time.
     * @return
     */
    public static boolean inDateRangeFlexible(Date startDate, Date endDate, Date checkDate, boolean inclusive, boolean truncate) {
        boolean result = true;
        if (startDate == null) {
            if (endDate != null) {
                final Date dt = truncate ? DateUtils.truncate(checkDate, Calendar.DATE) : checkDate;
                final Date endDt = truncate ? DateUtils.truncate(endDate, Calendar.DATE) : endDate;
                if (inclusive) {
                    result = dt.getTime() <= endDt.getTime();
                } else {
                    result = dt.getTime() < endDt.getTime();
                }
            }
        } else if (endDate == null) {
            final Date dt = truncate ? DateUtils.truncate(checkDate, Calendar.DATE) : checkDate;
            final Date startDt = truncate ? DateUtils.truncate(startDate, Calendar.DATE) : startDate;
            if (inclusive) {
                result = dt.getTime() >= startDt.getTime();
            } else {
                result = dt.getTime() > startDt.getTime();
            }
        } else {
            final Date dt = truncate ? DateUtils.truncate(checkDate, Calendar.DATE) : checkDate;
            final Date startDt = truncate ? DateUtils.truncate(startDate, Calendar.DATE) : startDate;
            final Date endDt = truncate ? DateUtils.truncate(endDate, Calendar.DATE) : endDate;
            if (inclusive) {
                result = dt.getTime() >= startDt.getTime() && dt.getTime() <= endDt.getTime();
            } else {
                result = dt.getTime() > startDt.getTime() && dt.getTime() < endDt.getTime();
            }
        }
        return result;
    }
}
