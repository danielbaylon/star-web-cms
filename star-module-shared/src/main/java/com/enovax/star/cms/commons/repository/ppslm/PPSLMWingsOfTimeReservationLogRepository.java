package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 26/8/16.
 */
@Repository
public interface PPSLMWingsOfTimeReservationLogRepository extends JpaRepository<PPSLMWingsOfTimeReservationLog, Integer> {
}
