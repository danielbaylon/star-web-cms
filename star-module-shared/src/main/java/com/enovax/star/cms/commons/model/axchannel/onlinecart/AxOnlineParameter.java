package com.enovax.star.cms.commons.model.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementRef;

/**
 * Created by houtao on 18/8/16.
 */
public class AxOnlineParameter {

    private String onLineSalesDirect;
    private String onLineSalesReservation;

    public String getOnLineSalesDirect() {
        return onLineSalesDirect;
    }

    public void setOnLineSalesDirect(String onLineSalesDirect) {
        this.onLineSalesDirect = onLineSalesDirect;
    }

    public String getOnLineSalesReservation() {
        return onLineSalesReservation;
    }

    public void setOnLineSalesReservation(String onLineSalesReservation) {
        this.onLineSalesReservation = onLineSalesReservation;
    }
}
