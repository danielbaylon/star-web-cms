package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by lavanya on 25/10/16.
 */
public class PaProfileRptFilterVM {
    private String orgName;
    private String paStatus;
    private String accMgrId;
    private String accMgrName;
    private Integer[] paIds;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getAccMgrId() {
        return accMgrId;
    }

    public void setAccMgrId(String accMgrId) {
        this.accMgrId = accMgrId;
    }

    public String getAccMgrName() {
        return accMgrName;
    }

    public void setAccMgrName(String accMgrName) {
        this.accMgrName = accMgrName;
    }

    public Integer[] getPaIds() {
        return paIds;
    }

    public void setPaIds(Integer[] paIds) {
        this.paIds = paIds;
    }
}
