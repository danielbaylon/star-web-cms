package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/14/16.
 */
public enum CMSProductCategoriesProperties {
    IconImage("iconImage"),
    Name("name"),
    GeneratedUrlPath("generatedURLPath"),
    TransportLayout("transportLayout");

	private String propertyName;

	CMSProductCategoriesProperties(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
