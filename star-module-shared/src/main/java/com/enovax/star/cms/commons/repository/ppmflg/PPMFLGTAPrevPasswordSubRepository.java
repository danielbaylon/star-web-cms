package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPrevPassword;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAPrevPasswordSub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGTAPrevPasswordSubRepository extends JpaRepository<PPMFLGTAPrevPasswordSub, Integer> {

    @Query("from PPMFLGTAPrevPasswordSub where subAccountId = ?1 order by createdDate desc")
    List<PPMFLGPrevPassword> findHistByAccountId(Integer id);
}
