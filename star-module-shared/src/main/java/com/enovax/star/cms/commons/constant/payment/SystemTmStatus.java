package com.enovax.star.cms.commons.constant.payment;

/**
 * Created by jennylynsze on 8/13/16.
 */
//please use the EnovaxTmSystemStatus
@Deprecated
public enum SystemTmStatus {
    //Reserved
    RedirectedToTm,
    TmQuerySent,

    //Available + Refunded + Expired
    Success,

    //Failed
    Failed,
    VoidSuccess,
    VoidFailed,

    //Incomplete
    NA;
}
