package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 23/8/16.
 */
public class AxWOTSalesOrder {

    private String generatedPIN;

    private AxSalesOrder salesOrder;

    public String getGeneratedPIN() {
        return generatedPIN;
    }

    public void setGeneratedPIN(String generatedPIN) {
        this.generatedPIN = generatedPIN;
    }

    public AxSalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(AxSalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }
}
