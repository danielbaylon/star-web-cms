package com.enovax.star.cms.commons.model.axchannel.onlinecart.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.onlinecart.AxOnlineParameter;
import com.enovax.star.cms.commons.ws.axchannel.pin.*;

import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 18/8/16.
 */
public class AxOnlineCartTransformer {
    public static ApiResult<List<AxOnlineParameter>> fromWsOnlineParameters(SDCOnLineParametersResponse response) {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }

        ApiResult<List<AxOnlineParameter>> result = new ApiResult<List<AxOnlineParameter>>();

        JAXBElement<ArrayOfSDCOnLineParameter> xmlArrayOfOnlineParam = response.getOnLineParameters();
        ArrayOfSDCOnLineParameter arrayOfSDCOnLineParam = null;
        if(xmlArrayOfOnlineParam != null){
            arrayOfSDCOnLineParam = xmlArrayOfOnlineParam.getValue();
        }
        List<SDCOnLineParameter> paramList = null;
        if(arrayOfSDCOnLineParam != null){
            paramList = arrayOfSDCOnLineParam.getSDCOnLineParameter();
        }

        List<AxOnlineParameter> retList = new ArrayList<AxOnlineParameter>();
        if(paramList != null && paramList.size() > 0){
            for(SDCOnLineParameter param : paramList){
                AxOnlineParameter p = new AxOnlineParameter();
                p.setOnLineSalesDirect(param.getOnLineSalesDirect().getValue());
                p.setOnLineSalesReservation(param.getOnLineSalesReservation().getValue());
                retList.add(p);
            }
            if(retList.size() > 0){
                result.setData(retList);
            }
        }
        result = getCommonResponseMessage(result, response);
        return result;
    }

    public static ApiResult getCommonResponseMessage(ApiResult axResponse, ServiceResponse response) {
        JAXBElement<ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if (errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (ResponseError rr : responseErrorList) {
                        if(rr == null){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }
}
