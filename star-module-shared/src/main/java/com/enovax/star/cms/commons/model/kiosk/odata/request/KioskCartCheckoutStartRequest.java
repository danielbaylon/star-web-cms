package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketListCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartCheckoutStartRequest {

    @JsonProperty("CartTicketListCriteria")
    private List<AxCartTicketListCriteria> axCartTicketListCriteria = new ArrayList<>();

    public List<AxCartTicketListCriteria> getAxCartTicketListCriteria() {
        return axCartTicketListCriteria;
    }

    public void setAxCartTicketListCriteria(List<AxCartTicketListCriteria> axCartTicketListCriteria) {
        this.axCartTicketListCriteria = axCartTicketListCriteria;
    }
}
