package com.enovax.star.cms.commons.service.ticketgen;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.constant.ticket.OnlineAxTicketToken;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.booking.StoreTransactionItem;
import com.enovax.star.cms.commons.model.ticketgen.*;
import com.enovax.star.cms.commons.service.template.ITicketTemplateService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.lowagie.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.*;

@Service
public class SimpleTicketGenerationService implements ITicketGenerationService {

    final Logger log = LoggerFactory.getLogger(SimpleTicketGenerationService.class);

    private String ticketXMLTemplateName = "kiosk-receipt-template";

    @Autowired
    ITicketTemplateService templateService;

    @PostConstruct
    public void init() {
        String oldValue = System.getProperty("java.protocol.handler.pkgs");
        if (oldValue == null) {
            System.setProperty("java.protocol.handler.pkgs", "org.xhtmlrenderer.protocols");
        } else if (!oldValue.contains("org.xhtmlrenderer.protocols")) {
            System.setProperty("java.protocol.handler.pkgs", oldValue + "|org.xhtmlrenderer.protocols");
        }
    }

    @Override
    public byte[] generateTickets(StoreApiChannels channel, ETicketDataCompiled compiledTicketData) throws IOException {
        final String htmlStart = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
                           "<html>" +
                                "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head>" +
                                "<body>";
        final String htmlEnd = "</body></html>";

        final String defaultQrCodeTemplate = " <div class= \"ticket-main-template\" style=\"width: 95%; margin: auto; padding: 10px; page-break-after: always;\">" + IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/eticket-template.html")) + "</div>";
        final String defaultBarcodeTemplate = " <div class= \"ticket-main-template\" style=\"width: 95%; margin: auto; padding: 10px; page-break-after: always;\">" + IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/eticket-barcode-template.html")) + "</div>";
        //TODO if last page do not have page-break-after please

        StringBuilder allTicket = new StringBuilder("");
        allTicket.append(htmlStart);

        List<ETicketData> ticketDatas = compiledTicketData.getTickets();

        for(int j = 0; j < ticketDatas.size(); j++) {
            ETicketData data = ticketDatas.get(j);
            AxRetailTemplateXML templateXML =  templateService.getTicketTemplate(channel, data.getTicketTemplateName());
            String updTemplate = null;
            if(templateXML != null && StringUtils.isNotBlank(templateXML.getXmlDocument())) {
                updTemplate = "<div class=\"ticket-main-template\" style=\"width: 95%; margin: auto; padding: 10px; page-break-after: always;\">" + templateXML.getXmlDocument() + "</div>";
            }

            ETicketTokenList tokenList =  JsonUtil.fromJson(data.getTokenData(), ETicketTokenList.class);

            Document doc;

            if(data.isThirdParty()) {

                if(updTemplate == null) {
                    updTemplate = new String(defaultBarcodeTemplate);
                }

                doc = Jsoup.parse(updTemplate);

                //barcode
                Elements elements = doc.getElementsByAttributeValue("data-template", "BarCode");
                for(int i = 0; i < elements.size(); i++) {
                    Element barCode = elements.get(i);
                    barCode.attr("src", "data:image/png;base64," + data.getBarcodeBase64()); //<img src="data:image/png;base64,{{barcodeBase64}}" style="max-width: 95%;" />
                    barCode.removeAttr("width");
                    barCode.removeAttr("height");
                }
            }else{

                if(updTemplate == null) {
                    updTemplate = new String(defaultQrCodeTemplate);
                }

                doc = Jsoup.parse(updTemplate);

                //qrcode
                Elements elements = doc.getElementsByAttributeValue("data-template", "QRCode");
                for(int i = 0; i < elements.size(); i++) {
                    Element qrCode = elements.get(i);
                    qrCode.attr("src", "data:image/png;base64," + data.getBarcodeBase64()); //<img src="data:image/png;base64,{{barcodeBase64}}" style="max-width: 95%;" />
                    qrCode.removeAttr("width");
                    qrCode.removeAttr("height");
                }
            }

            //the last one
            if(j == ticketDatas.size() - 1) {
                Elements mainTemplateElements = doc.getElementsByClass("ticket-main-template");
                Element mainTemplate = mainTemplateElements.get(0); //only one lang tlga
                mainTemplate.attr("style", "width: 95%; margin: auto; padding: 10px;"); //remove the page break thingy

            }

            doc.outputSettings().syntax( Document.OutputSettings.Syntax.xml);

            updTemplate = doc.html(); //back to string :)

            updTemplate = replace(updTemplate, OnlineAxTicketToken.BarcodeEndDate, tokenList.getBarcodeEndDate());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.BarcodePLU, tokenList.getBarcodePLU());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.BarcodeStartDate, tokenList.getBarcodeStartDate());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.CustomerName, tokenList.getCustomerName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.Description, tokenList.getDescription());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.DiscountApplied, tokenList.getDiscountApplied());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.DisplayName, tokenList.getDisplayName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.EventDate, tokenList.getEventDate());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.EventName, tokenList.getEventName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.Facilities, tokenList.getFacilities());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.FacilityAction, tokenList.getFacilityAction());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ItemNumber, tokenList.getItemNumber());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.LegalEntity, tokenList.getLegalEntity());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.MixMatchDescription, tokenList.getMixMatchDescription());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.MixMatchPackageName, tokenList.getMixMatchPackageName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.OperationIds, tokenList.getOperationIds());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PackageItem, tokenList.getPackageItem());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PackageItemName, tokenList.getPackageItemName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PaxPerTicket, tokenList.getPaxPerTicket());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PrintLabel, tokenList.getPrintLabel());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductImage, tokenList.getProductImage());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductOwner, tokenList.getProductOwner());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductSearchName, tokenList.getProductSearchName());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductVarColor, tokenList.getProductVarColor());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductVarConfig, tokenList.getProductVarConfig());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductVarSize, tokenList.getProductVarSize());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ProductVarStyle, tokenList.getProductVarStyle());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PublishPrice, tokenList.getPublishPrice());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TicketCode, tokenList.getTicketCode());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TicketNumber, tokenList.getTicketNumber());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TicketType, tokenList.getTicketType());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TicketValidity, tokenList.getTicketValidity());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.UsageValidity, tokenList.getUsageValidity());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ValidityEndDate, tokenList.getValidityEndDate());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.ValidityStartDate, tokenList.getValidityStartDate());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TermAndCondition, tokenList.getTermAndCondition());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.Disclaimer, tokenList.getDisclaimer());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.PackagePrintLabel, tokenList.getPackagePrintLabel());
            updTemplate = replace(updTemplate, OnlineAxTicketToken.TicketLineDesc, tokenList.getTicketLineDesc());
            allTicket.append(updTemplate);
        }

        //append the final
        allTicket.append(htmlEnd);

        //TODO loop
        //TODO replace token

//        final Handlebars hb = new Handlebars();
//        final Template hbTemplate = hb.compileInline(template);
//        final Context hbContext = Context.newContext(compiledTicketData);
//        final String generatedContent = hbTemplate.apply(hbContext);

        ByteArrayOutputStream output = null;
        try {

            output = new ByteArrayOutputStream();

            final ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(allTicket.toString());
            renderer.layout();
            renderer.createPDF(output, true);

            return output.toByteArray();
        } catch (DocumentException e) {
            log.error("Error encountered generating eTicket PDF", e);
            throw new IOException("Error encountered generating eTicket PDF", e);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private String replace(String xml, OnlineAxTicketToken token, String value) {
        return xml.replace(token.getReplacement(), StringUtils.isBlank(value) ? "" : value);
    }

    @Override
    public String generateReceiptXmlWithQRCode(StoreTransaction txn, String inputReceiptTemplateXml, String qrCode) throws IOException {

        if (StringUtils.isEmpty(qrCode)) {
            return generateReceiptXml(txn, inputReceiptTemplateXml);
        }

        ticketXMLTemplateName = "kiosk-receipt-with-qrcode-template";

        String receiptXMLString = generateReceiptXml(txn, inputReceiptTemplateXml);

        return receiptXMLString.replace("<Data></Data>", "<Data>" + qrCode + "</Data>");
    }

    @Override
    public String generateReceiptXml(StoreTransaction txn, final String inputReceiptTemplateXml) throws IOException {
        String receiptTemplateXml;
        if (StringUtils.isEmpty(inputReceiptTemplateXml)) {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/" + ticketXMLTemplateName);
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + ticketXMLTemplateName + ".xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", ticketXMLTemplateName, "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                receiptTemplateXml = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    receiptTemplateXml = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    receiptTemplateXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/" + ticketXMLTemplateName + ".xml"));
                }
            }
        } else {
            receiptTemplateXml = inputReceiptTemplateXml;
        }

        final Date now = new Date();
        final ReceiptMain receipt = new ReceiptMain();
        receipt.setPrintedDateText(NvxDateUtils.formatDate(now, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        receipt.setReceiptNumber(txn.getReceiptNumber());
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(txn.getTotalAmount(), "$ "));

        final List<ReceiptItem> items = new ArrayList<>();
        for (StoreTransactionItem sti : txn.getItems()) {
            final ReceiptItem item = new ReceiptItem();
            item.setProductName(sti.getName() + (StringUtils.isEmpty(sti.getEventGroupId()) ? "" : " - " + sti.getSelectedEventDate()));
            item.setQty(sti.getQty().toString());
            item.setSubtotalText(NvxNumberUtils.formatToCurrency(sti.getSubtotal(), "$ "));
            items.add(item);
        }

        receipt.setItems(items);

        final Handlebars hb = new Handlebars();
        final Template hbTemplate = hb.compileInline(receiptTemplateXml);
        final Context hbContext = Context.newContext(receipt);
        final String generatedContent = hbTemplate.apply(hbContext);

        return generatedContent.replace("\n", "");
    }

    @Override
    public Map<String, String> generatePaperTicketXml(
            ETicketDataCompiled compiledTicketData,
            final String inputTemplateXmlQr, final String inputTemplateXmlBarcode) throws IOException {
        final Map<String, String> xmlMap = new HashMap<>();

        String templateXmlQr;
        if (StringUtils.isEmpty(inputTemplateXmlQr)) {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/paper-ticket-template-qr");
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/paper-ticket-template-qr.xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", "paper-ticket-template-qr", "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                templateXmlQr = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    templateXmlQr = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    templateXmlQr = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/paper-ticket-template-qr.xml"));
                }
            }
        } else {
            templateXmlQr = inputTemplateXmlQr;
        }

        String templateXmlBarcode;
        if (StringUtils.isEmpty(inputTemplateXmlBarcode)) {
            Node templateNode = null;
            try {
                templateNode = JcrRepository.getParentNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/paper-ticket-template-barcode");
            } catch (RepositoryException e) {
                log.error("Error retrieving template from JCR.", e);
            }

            if (templateNode == null) {
                log.warn("No node found for Paper Ticket Template. Creating from default.");
                String defaultXml = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/paper-ticket-template-barcode.xml"));
                try {
                    Node newNode = JcrRepository.createNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), "/", "paper-ticket-template-barcode", "mgnl:content");
                    if (newNode != null) {
                        newNode.setProperty("templateXml", defaultXml);
                        JcrRepository.updateNode(JcrWorkspace.ApiStoreDump.getWorkspaceName(), newNode);
                    }
                } catch (RepositoryException e1) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                }
                templateXmlBarcode = defaultXml;
            } else {
                try {
                    Property xmlNode = templateNode.getProperty("templateXml");
                    templateXmlBarcode = xmlNode.getString();
                } catch (RepositoryException e) {
                    log.error("Still unable to create node for template. Defaulting to filesystem.");
                    templateXmlBarcode = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("store-templates/paper-ticket-template-barcode.xml"));
                }
            }
        } else {
            templateXmlBarcode = inputTemplateXmlBarcode;
        }

        for (ETicketData td : compiledTicketData.getTickets()) {
            final boolean isThirdParty = td.isThirdParty();

            String xml = isThirdParty ? templateXmlBarcode : templateXmlQr;
            xml = xml.replace("TicketNumber", td.getTicketNumber())
                    .replace("DisplayName", StringUtils.isEmpty(td.getShortDisplayName()) ? "" : td.getShortDisplayName())
                    .replace("TicketPersonType", StringUtils.isEmpty(td.getTicketPersonType()) ? "" : td.getTicketPersonType())
                    .replace("EventSession", StringUtils.isEmpty(td.getEventSession()) ? "" : td.getTicketPersonType())
                    .replace("EventDate", StringUtils.isEmpty(td.getEventDate()) ? "" : td.getTicketPersonType())
                    .replace("TotalPrice", td.getTotalPrice())
                    .replace("ValidityStartDate", td.getValidityStartDate())
                    .replace("ValidityEndDate", td.getValidityEndDate());
            xml = xml.replace(isThirdParty ? "QrCodeData" : "BarCodeData", td.getCodeData());

            xmlMap.put(td.getTicketNumber(), xml.replace("\n", ""));
        }

        return xmlMap;
    }
}
