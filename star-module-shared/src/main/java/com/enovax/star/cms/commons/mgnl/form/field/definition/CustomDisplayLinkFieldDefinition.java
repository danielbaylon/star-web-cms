package com.enovax.star.cms.commons.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.LinkFieldDefinition;

public class CustomDisplayLinkFieldDefinition extends LinkFieldDefinition {

    private boolean enabled = true; // Specify if the property is enabled or not
    public static final String NODE_NAME = "jcrName"; //pass in the "jcrName" to be able to display the node name instead

    private String targetPropertyToDisplay;

    public String getTargetPropertyToDisplay() {
        return targetPropertyToDisplay;
    }

    public void setTargetPropertyToDisplay(String targetPropertyToDisplay) {
        this.targetPropertyToDisplay = targetPropertyToDisplay;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
