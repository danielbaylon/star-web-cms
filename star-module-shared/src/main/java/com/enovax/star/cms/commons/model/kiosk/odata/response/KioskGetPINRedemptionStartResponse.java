package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Collection(SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTableEntity)
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskGetPINRedemptionStartResponse {

	@JsonProperty("value")
	private List<AxPINRedemptionStartData> axPINRedemptionStartDataList = new ArrayList<AxPINRedemptionStartData>();

	public void setAxPINRedemptionStartDataList(List<AxPINRedemptionStartData> axPINRedemptionStartDataList) {
		this.axPINRedemptionStartDataList = axPINRedemptionStartDataList;
	}

	public List<AxPINRedemptionStartData> getAxPINRedemptionStartDataList() {
		return axPINRedemptionStartDataList;
	}

}
