package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by lavanya on 8/9/16.
 */
public class PartnerDetailsVM {
    private PartnerVM partnerVM;
    private ApprovalLogVM approvalLogVM;
    private boolean canApprove;
    private boolean showRemarks;

    public PartnerDetailsVM() {
    }

    public PartnerDetailsVM(PartnerVM partnerVM, ApprovalLogVM approvalLogVM, boolean canApprove, boolean showRemarks) {
        this.partnerVM = partnerVM;
        this.approvalLogVM = approvalLogVM;
        this.canApprove = canApprove;
        this.showRemarks = showRemarks;
    }

    public PartnerVM getPartnerVM() {
        return partnerVM;
    }

    public void setPartnerVM(PartnerVM partnerVM) {
        this.partnerVM = partnerVM;
    }

    public boolean isCanApprove() {
        return canApprove;
    }

    public void setCanApprove(boolean canApprove) {
        this.canApprove = canApprove;
    }

    public boolean isShowRemarks() {
        return showRemarks;
    }

    public ApprovalLogVM getApprovalLogVM() {
        return approvalLogVM;
    }

    public void setApprovalLogVM(ApprovalLogVM approvalLogVM) {
        this.approvalLogVM = approvalLogVM;
    }

    public void setShowRemarks(boolean showRemarks) {
        this.showRemarks = showRemarks;
    }
}
