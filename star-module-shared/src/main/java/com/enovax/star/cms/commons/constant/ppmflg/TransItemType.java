package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 7/15/16.
 */
public enum TransItemType {
    Standard,
    Topup
}
