
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_PinLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_PinLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CHANNELID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DATAAREAID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENDDATETIME" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EVENTDATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EVENTGROUPID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EVENTLINEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INVENTTRANSID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INVOICEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISCOMBINETICKET" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ISREDEEMED" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ITEMID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINENUM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MEDIATYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OPENVALIDITYRULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QTY" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="QTYREDEEMED" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="QTYRETURNED" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="REFERENCEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REPLICATIONCOUNTERFROMORIGIN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RETAILVARIANTID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SALESID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STARTDATETIME" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TRANSACTIONID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_PinLine", propOrder = {
    "channelid",
    "dataareaid",
    "enddatetime",
    "eventdate",
    "eventgroupid",
    "eventlineid",
    "inventtransid",
    "invoiceid",
    "iscombineticket",
    "isredeemed",
    "itemid",
    "linenum",
    "mediatype",
    "openvalidityrule",
    "qty",
    "qtyredeemed",
    "qtyreturned",
    "referenceid",
    "replicationcounterfromorigin",
    "retailvariantid",
    "salesid",
    "startdatetime",
    "transactionid"
})
public class SDCPinLine {

    @XmlElement(name = "CHANNELID")
    protected Long channelid;
    @XmlElementRef(name = "DATAAREAID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataareaid;
    @XmlElement(name = "ENDDATETIME")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enddatetime;
    @XmlElementRef(name = "EVENTDATE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> eventdate;
    @XmlElementRef(name = "EVENTGROUPID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventgroupid;
    @XmlElementRef(name = "EVENTLINEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventlineid;
    @XmlElementRef(name = "INVENTTRANSID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> inventtransid;
    @XmlElementRef(name = "INVOICEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceid;
    @XmlElement(name = "ISCOMBINETICKET")
    protected Integer iscombineticket;
    @XmlElement(name = "ISREDEEMED")
    protected Integer isredeemed;
    @XmlElementRef(name = "ITEMID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemid;
    @XmlElement(name = "LINENUM")
    protected BigDecimal linenum;
    @XmlElementRef(name = "MEDIATYPE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mediatype;
    @XmlElementRef(name = "OPENVALIDITYRULE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> openvalidityrule;
    @XmlElement(name = "QTY")
    protected BigDecimal qty;
    @XmlElement(name = "QTYREDEEMED")
    protected BigDecimal qtyredeemed;
    @XmlElement(name = "QTYRETURNED")
    protected BigDecimal qtyreturned;
    @XmlElementRef(name = "REFERENCEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> referenceid;
    @XmlElement(name = "REPLICATIONCOUNTERFROMORIGIN")
    protected Integer replicationcounterfromorigin;
    @XmlElementRef(name = "RETAILVARIANTID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retailvariantid;
    @XmlElementRef(name = "SALESID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesid;
    @XmlElement(name = "STARTDATETIME")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startdatetime;
    @XmlElementRef(name = "TRANSACTIONID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionid;

    /**
     * Gets the value of the channelid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCHANNELID() {
        return channelid;
    }

    /**
     * Sets the value of the channelid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCHANNELID(Long value) {
        this.channelid = value;
    }

    /**
     * Gets the value of the dataareaid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDATAAREAID() {
        return dataareaid;
    }

    /**
     * Sets the value of the dataareaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDATAAREAID(JAXBElement<String> value) {
        this.dataareaid = value;
    }

    /**
     * Gets the value of the enddatetime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENDDATETIME() {
        return enddatetime;
    }

    /**
     * Sets the value of the enddatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENDDATETIME(XMLGregorianCalendar value) {
        this.enddatetime = value;
    }

    /**
     * Gets the value of the eventdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getEVENTDATE() {
        return eventdate;
    }

    /**
     * Sets the value of the eventdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setEVENTDATE(JAXBElement<XMLGregorianCalendar> value) {
        this.eventdate = value;
    }

    /**
     * Gets the value of the eventgroupid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEVENTGROUPID() {
        return eventgroupid;
    }

    /**
     * Sets the value of the eventgroupid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEVENTGROUPID(JAXBElement<String> value) {
        this.eventgroupid = value;
    }

    /**
     * Gets the value of the eventlineid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEVENTLINEID() {
        return eventlineid;
    }

    /**
     * Sets the value of the eventlineid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEVENTLINEID(JAXBElement<String> value) {
        this.eventlineid = value;
    }

    /**
     * Gets the value of the inventtransid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getINVENTTRANSID() {
        return inventtransid;
    }

    /**
     * Sets the value of the inventtransid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setINVENTTRANSID(JAXBElement<String> value) {
        this.inventtransid = value;
    }

    /**
     * Gets the value of the invoiceid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getINVOICEID() {
        return invoiceid;
    }

    /**
     * Sets the value of the invoiceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setINVOICEID(JAXBElement<String> value) {
        this.invoiceid = value;
    }

    /**
     * Gets the value of the iscombineticket property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getISCOMBINETICKET() {
        return iscombineticket;
    }

    /**
     * Sets the value of the iscombineticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setISCOMBINETICKET(Integer value) {
        this.iscombineticket = value;
    }

    /**
     * Gets the value of the isredeemed property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getISREDEEMED() {
        return isredeemed;
    }

    /**
     * Sets the value of the isredeemed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setISREDEEMED(Integer value) {
        this.isredeemed = value;
    }

    /**
     * Gets the value of the itemid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getITEMID() {
        return itemid;
    }

    /**
     * Sets the value of the itemid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setITEMID(JAXBElement<String> value) {
        this.itemid = value;
    }

    /**
     * Gets the value of the linenum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLINENUM() {
        return linenum;
    }

    /**
     * Sets the value of the linenum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLINENUM(BigDecimal value) {
        this.linenum = value;
    }

    /**
     * Gets the value of the mediatype property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMEDIATYPE() {
        return mediatype;
    }

    /**
     * Sets the value of the mediatype property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMEDIATYPE(JAXBElement<String> value) {
        this.mediatype = value;
    }

    /**
     * Gets the value of the openvalidityrule property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOPENVALIDITYRULE() {
        return openvalidityrule;
    }

    /**
     * Sets the value of the openvalidityrule property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOPENVALIDITYRULE(JAXBElement<String> value) {
        this.openvalidityrule = value;
    }

    /**
     * Gets the value of the qty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTY() {
        return qty;
    }

    /**
     * Sets the value of the qty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTY(BigDecimal value) {
        this.qty = value;
    }

    /**
     * Gets the value of the qtyredeemed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTYREDEEMED() {
        return qtyredeemed;
    }

    /**
     * Sets the value of the qtyredeemed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTYREDEEMED(BigDecimal value) {
        this.qtyredeemed = value;
    }

    /**
     * Gets the value of the qtyreturned property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTYRETURNED() {
        return qtyreturned;
    }

    /**
     * Sets the value of the qtyreturned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTYRETURNED(BigDecimal value) {
        this.qtyreturned = value;
    }

    /**
     * Gets the value of the referenceid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREFERENCEID() {
        return referenceid;
    }

    /**
     * Sets the value of the referenceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREFERENCEID(JAXBElement<String> value) {
        this.referenceid = value;
    }

    /**
     * Gets the value of the replicationcounterfromorigin property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getREPLICATIONCOUNTERFROMORIGIN() {
        return replicationcounterfromorigin;
    }

    /**
     * Sets the value of the replicationcounterfromorigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setREPLICATIONCOUNTERFROMORIGIN(Integer value) {
        this.replicationcounterfromorigin = value;
    }

    /**
     * Gets the value of the retailvariantid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRETAILVARIANTID() {
        return retailvariantid;
    }

    /**
     * Sets the value of the retailvariantid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRETAILVARIANTID(JAXBElement<String> value) {
        this.retailvariantid = value;
    }

    /**
     * Gets the value of the salesid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSALESID() {
        return salesid;
    }

    /**
     * Sets the value of the salesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSALESID(JAXBElement<String> value) {
        this.salesid = value;
    }

    /**
     * Gets the value of the startdatetime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTARTDATETIME() {
        return startdatetime;
    }

    /**
     * Sets the value of the startdatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTARTDATETIME(XMLGregorianCalendar value) {
        this.startdatetime = value;
    }

    /**
     * Gets the value of the transactionid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTRANSACTIONID() {
        return transactionid;
    }

    /**
     * Sets the value of the transactionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTRANSACTIONID(JAXBElement<String> value) {
        this.transactionid = value;
    }

}
