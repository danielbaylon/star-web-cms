package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.List;

/**
 * Created by lavanya on 25/10/16.
 */
public class PartnerProfileReportInitVM {
    private List<AdminAccountVM> adminAccountVMs;
    private List<PartnerVM> allPartnerVMs;

    public PartnerProfileReportInitVM() {
    }

    public PartnerProfileReportInitVM(List<AdminAccountVM> adminAccountVMs, List<PartnerVM> allPartnerVMs) {
        this.adminAccountVMs = adminAccountVMs;
        this.allPartnerVMs = allPartnerVMs;
    }

    public List<AdminAccountVM> getAdminAccountVMs() {
        return adminAccountVMs;
    }

    public void setAdminAccountVMs(List<AdminAccountVM> adminAccountVMs) {
        this.adminAccountVMs = adminAccountVMs;
    }

    public List<PartnerVM> getAllPartnerVMs() {
        return allPartnerVMs;
    }

    public void setAllPartnerVMs(List<PartnerVM> allPartnerVMs) {
        this.allPartnerVMs = allPartnerVMs;
    }
}
