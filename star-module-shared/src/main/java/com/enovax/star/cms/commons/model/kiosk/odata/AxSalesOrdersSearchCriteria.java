package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 *
 * @author dbaylon
 *
 */

public class AxSalesOrdersSearchCriteria {

	@JsonProperty("TransactionIds")
	private List<String> transactionIds = new ArrayList<>();

	@JsonProperty("SalesId")
	private String salesId;

	@JsonProperty("ReceiptId")
	private String receiptId;

	@JsonProperty("ChannelReferenceId")
	private String channelReferenceId;

	@JsonProperty("CustomerAccountNumber")
	private String customerAccountNumber;

	@JsonProperty("CustomerFirstName")
	private String customerFirstName;

	@JsonProperty("CustomerLastName")
	private String customerLastName;

	@JsonProperty("StoreId")
	private String storeId;

	@JsonProperty("TerminalId")
	private String terminalId;

	@JsonProperty("ItemId")
	private String itemId;

	@JsonProperty("Barcode")
	private String barcode;

	@JsonProperty("SerialNumber")
	private String serialNumber;

	@JsonProperty("StaffId")
	private String staffId;

	@JsonProperty("StartDateTime")
	private String startDateTime;

	@JsonProperty("EndDateTime")
	private String endDateTime;

	@JsonProperty("ReceiptEmailAddress")
	private String receiptEmailAddress;

	@JsonProperty("SearchIdentifiers")
	private String searchIdentifiers;

	@JsonProperty("SearchLocationTypeValue")
	private int searchLocationTypeValue = 3;

	@JsonProperty("SearchTypeValue")
	private int searchTypeValue;

	@JsonProperty("IncludeDetails")
	private boolean includeDetails;

	@JsonProperty("SalesTransactionTypeValues")
	private List<Integer> salesTransactionTypeValues = new ArrayList<>();

	@JsonProperty("TransactionStatusTypeValues")
	private List<Integer> transactionStatusTypeValues = new ArrayList<>();

	public void setTransactionIds(List<String> transactionIds) {
		this.transactionIds = transactionIds;
	}
	
	public List<String> getTransactionIds() {
		return transactionIds;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getChannelReferenceId() {
		return channelReferenceId;
	}

	public void setChannelReferenceId(String channelReferenceId) {
		this.channelReferenceId = channelReferenceId;
	}

	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}

	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getReceiptEmailAddress() {
		return receiptEmailAddress;
	}

	public void setReceiptEmailAddress(String receiptEmailAddress) {
		this.receiptEmailAddress = receiptEmailAddress;
	}

	public String getSearchIdentifiers() {
		return searchIdentifiers;
	}

	public void setSearchIdentifiers(String searchIdentifiers) {
		this.searchIdentifiers = searchIdentifiers;
	}

	public int getSearchLocationTypeValue() {
		return searchLocationTypeValue;
	}

	public void setSearchLocationTypeValue(int searchLocationTypeValue) {
		this.searchLocationTypeValue = searchLocationTypeValue;
	}

	public int getSearchTypeValue() {
		return searchTypeValue;
	}

	public void setSearchTypeValue(int searchTypeValue) {
		this.searchTypeValue = searchTypeValue;
	}

	public boolean isIncludeDetails() {
		return includeDetails;
	}

	public void setIncludeDetails(boolean includeDetails) {
		this.includeDetails = includeDetails;
	}

	public List<Integer> getSalesTransactionTypeValues() {
		return salesTransactionTypeValues;
	}

	public void setSalesTransactionTypeValues(List<Integer> salesTransactionTypeValues) {
		this.salesTransactionTypeValues = salesTransactionTypeValues;
	}

	public List<Integer> getTransactionStatusTypeValues() {
		return transactionStatusTypeValues;
	}

	public void setTransactionStatusTypeValues(List<Integer> transactionStatusTypeValues) {
		this.transactionStatusTypeValues = transactionStatusTypeValues;
	}

}
