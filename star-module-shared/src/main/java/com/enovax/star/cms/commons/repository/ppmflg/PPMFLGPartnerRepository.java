package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGPartnerRepository extends JpaRepository<PPMFLGPartner, Integer>, JpaSpecificationExecutor<PPMFLGPartner> {

    PPMFLGPartner findByAccountCode(String accountCode);

    PPMFLGPartner findById(int partnerId);

    PPMFLGPartner findFirstByAdminId(int adminId);

    PPMFLGPartner findByAxAccountNumber(String custAccNum);

    PPMFLGPartner findFirstByAdminIdAndAccountCode(Integer adminId, String accountCode);

    @Query("select u from PPMFLGPartner u where u.id in :ids ")
    List<PPMFLGPartner> findByPartnerIds(@Param("ids") List<Integer> paIdList);

    List<PPMFLGPartner> findByOrgNameLike(String orgName);

    List<PPMFLGPartner> findByStatus(String status);

    @Query(value="select p from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status)" +
            "and (p.id in (:ids)))" )
    Page getPartnerProfileWithId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status, @Param("ids")List<Integer> paIdList, Pageable pageable);

    @Query(value="select count(p) from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status)" +
            "and (p.id in (:ids)))")
    int getPartnerProfileSizeWithId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status, @Param("ids")List<Integer> paIdList);

    @Query(value="select p from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status))")
    Page getPartnerProfileWithoutId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status, Pageable pageable);

    @Query(value="select count(p) from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status))")
    int getPartnerProfileSizeWithoutId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status);

    @Query(value="select p from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status))")
    List<PPMFLGPartner> getPartnerProfileWithoutId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status);

    @Query(value="select p from PPMFLGPartner p " +
            "where ((:acctMgrId is null or p.accountManagerId=:acctMgrId)" +
            "and (:orgName is null or p.orgName like :orgName)" +
            "and (:status is null or p.status=:status)" +
            "and (p.id in (:ids)))" )
    List<PPMFLGPartner> getPartnerProfileWithId(@Param("acctMgrId")String acctMgrId, @Param("orgName")String orgName, @Param("status")String status, @Param("ids")List<Integer> paIdList);

    int countByIdAndStatus(Integer paId, String code);
}
