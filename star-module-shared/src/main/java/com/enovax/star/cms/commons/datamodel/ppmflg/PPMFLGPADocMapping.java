package com.enovax.star.cms.commons.datamodel.ppmflg;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PPMFLGPADocMapping")
public class PPMFLGPADocMapping implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="partnerId",insertable = false, updatable = false)
    private PPMFLGPartner partner;
    
    
    @Column(name="partnerId", nullable=false)
    private Integer partnerId;
    
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="docId",insertable = false, updatable = false)
    private PPMFLGPADocument paDoc;
    
    
    @Column(name="docId", nullable=false)
    private Integer docId;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public PPMFLGPartner getPartner() {
        return partner;
    }


    public void setPartner(PPMFLGPartner partner) {
        this.partner = partner;
    }


    public Integer getPartnerId() {
        return partnerId;
    }


    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }


    public PPMFLGPADocument getPaDoc() {
        return paDoc;
    }


    public void setPaDoc(PPMFLGPADocument paDoc) {
        this.paDoc = paDoc;
    }


    public Integer getDocId() {
        return docId;
    }


    public void setDocId(Integer docId) {
        this.docId = docId;
    }
    
}
