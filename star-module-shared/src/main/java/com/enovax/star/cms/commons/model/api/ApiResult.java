package com.enovax.star.cms.commons.model.api;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.util.ProjectUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

public class ApiResult<T> {
    private boolean success = false;

    @JsonIgnore
    private ApiErrorCodes apiErrorCode = null;

    private String errorCode = "";
    private String message = "";
    private T data = null;
    private int total;

    public ApiResult() {

    }

    public ApiResult(boolean success, String errorCode, String message, T data) {
        this.success = success;
        this.errorCode = errorCode;
        this.message = message;
        this.data = data;
    }

    public ApiResult(boolean success, String errorCode, String message, int total, T data) {
        this.success = success;
        this.errorCode = errorCode;
        this.message = message;
        this.data = data;
        this.total = total;
    }

    public ApiResult(ApiErrorCodes err) { this(err, "", ""); }
    public ApiResult(ApiErrorCodes err, String additionalMessage) {
        this(err, additionalMessage, "");
    }
    public ApiResult(ApiErrorCodes err, String additionalMessage, String logId) {
        this.success = false;
        this.errorCode = err.code;
        this.apiErrorCode = err;

        final String msg = err.message + (StringUtils.isEmpty(additionalMessage) ? "" : " " + additionalMessage);
        this.message = StringUtils.isEmpty(logId) ? msg : ProjectUtils.appendLogId(msg, logId);
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @JsonIgnore
    public ApiErrorCodes getApiErrorCode() {
        return apiErrorCode;
    }

    public void setApiErrorCode(ApiErrorCodes apiErrorCode) {
        this.apiErrorCode = apiErrorCode;
    }
}
