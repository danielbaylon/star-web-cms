package com.enovax.star.cms.commons.mgnl.contentapp;

import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.ui.contentapp.browser.ConfiguredBrowserSubAppDescriptor;

/**
 * Created by jennylynsze on 8/3/16.
 */
public class ConfiguredStarBrowserSubAppDescriptor extends ConfiguredBrowserSubAppDescriptor implements StarBrowserSubAppDescriptor {
    private AccessDefinition permissions;
    private String mainSubAppDisplay;
    private boolean openFirst;

    public void setPermissions(AccessDefinition permissions) {
        this.permissions = permissions;
    }

    public void setMainSubAppDisplay(String mainSubAppDisplay) {
        this.mainSubAppDisplay = mainSubAppDisplay;
    }

    public void setOpenFirst(boolean openFirst) {
        this.openFirst = openFirst;
    }

    @Override
    public AccessDefinition getPermissions() {
        return this.permissions;
    }

    @Override
    public String getMainSubAppDisplay() {
        return this.mainSubAppDisplay;
    }

    @Override
    public boolean isOpenFirst() {
        return openFirst;
    }
}
