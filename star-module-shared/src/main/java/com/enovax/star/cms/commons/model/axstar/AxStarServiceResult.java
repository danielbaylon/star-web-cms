package com.enovax.star.cms.commons.model.axstar;

public class AxStarServiceResult<T> {

    private boolean success = false;
    private AxStarResponseError error;
    private String rawData;
    private T data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public AxStarResponseError getError() {
        return error;
    }

    public void setError(AxStarResponseError error) {
        this.error = error;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
