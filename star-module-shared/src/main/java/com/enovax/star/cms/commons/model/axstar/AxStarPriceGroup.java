package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

/**
 * Created by jensen on 27/6/16.
 */
public class AxStarPriceGroup {

    private Long recordId;
    private Long priceGroupId;
    private String groupId;

    private List<AxStarExtensionProperty> extensionProperties;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(Long priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
