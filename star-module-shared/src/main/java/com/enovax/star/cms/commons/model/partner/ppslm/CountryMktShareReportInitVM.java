package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.List;

/**
 * Created by lavanya on 28/10/16.
 */
public class CountryMktShareReportInitVM {
    private List<PartnerVM> allPartnerVMs;
    private List<CountryVM> countryVMs;

    public CountryMktShareReportInitVM() {
    }

    public CountryMktShareReportInitVM(List<PartnerVM> allPartnerVMs, List<CountryVM> countryVMs) {
        this.allPartnerVMs = allPartnerVMs;
        this.countryVMs = countryVMs;
    }

    public List<CountryVM> getCountryVMs() {
        return countryVMs;
    }

    public void setCountryVMs(List<CountryVM> countryVMs) {
        this.countryVMs = countryVMs;
    }

    public List<PartnerVM> getAllPartnerVMs() {
        return allPartnerVMs;
    }

    public void setAllPartnerVMs(List<PartnerVM> allPartnerVMs) {
        this.allPartnerVMs = allPartnerVMs;
    }
}
