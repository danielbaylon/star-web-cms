
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateCreatedCustomerResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_UpdateCreatedCustomerResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCreatedCustomerResult"
})
@XmlRootElement(name = "UpdateCreatedCustomerResponse")
public class UpdateCreatedCustomerResponse {

    @XmlElementRef(name = "UpdateCreatedCustomerResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCUpdateCreatedCustomerResponse> updateCreatedCustomerResult;

    /**
     * Gets the value of the updateCreatedCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCUpdateCreatedCustomerResponse }{@code >}
     *     
     */
    public JAXBElement<SDCUpdateCreatedCustomerResponse> getUpdateCreatedCustomerResult() {
        return updateCreatedCustomerResult;
    }

    /**
     * Sets the value of the updateCreatedCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCUpdateCreatedCustomerResponse }{@code >}
     *     
     */
    public void setUpdateCreatedCustomerResult(JAXBElement<SDCUpdateCreatedCustomerResponse> value) {
        this.updateCreatedCustomerResult = value;
    }

}
