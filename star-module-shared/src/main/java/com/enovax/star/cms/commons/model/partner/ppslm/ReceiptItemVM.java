package com.enovax.star.cms.commons.model.partner.ppslm;
import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.math.BigDecimal;

/**
 * Created by lavanya on 13/9/16.
 */
public class ReceiptItemVM implements Comparable<ReceiptItemVM> {
    private Boolean itemTopup;
    private String displayName;
    private Integer qty;
    private String subTotalStr;
    private String gstStr;

    private String revalFeeStr;
    private String singleRevalFeeStr;
    private String revalFeeGSTStr;

    private String prodId = "";
    private String scheduleId = "";
    private String mainItemId = "";
    private Integer id = -1;

    private String unitPriceStr;
    private Integer unpackagedQty;

    //Full means with GST
    private String revalFeeFullStr;
    private String singleRevalFeeFullStr;
    private String revalFeeGSTFullStr;

    private String receiptNum;

    public ReceiptItemVM() {
    }


    public ReceiptItemVM(PPSLMInventoryTransactionItem item, BigDecimal gst) {
        if (TransItemType.Topup.toString().equals(item.getTransItemType())) {
            this.itemTopup = true;
        } else {
            this.itemTopup = false;
        }
        this.displayName = item.getDisplayName();
        this.qty = item.getQty();
        this.subTotalStr = TransactionUtil.priceWithDecimal(item.getSubTotal(),
                "");
        this.gstStr = TransactionUtil.gstWithDecimal(item.getSubTotal(), gst, "");

        this.id = item.getId();
        this.mainItemId = item.getMainItemProductId()+item.getMainItemListingId();
        this.scheduleId = item.getEventGroupId() == "" ? "" : item.getEventGroupId()+item.getEventLineId()+item.getDateOfVisit();

        this.prodId = item.getProductId();
        if(item.getUnitPrice()!=null){
            this.unitPriceStr = TransactionUtil.priceWithDecimal(item.getUnitPrice(),"");
        }
        this.unpackagedQty = item.getUnpackagedQty();
    }

//    public ReceiptItemVM(InventoryTransactionItem item,
//            BigDecimal normalRevalidatFee, BigDecimal topupRevalidatFee) {
//        this(item);
//        BigDecimal revalidateFee = TransactionUtil.getTransItemRevalidateFee(
//                item, normalRevalidatFee, topupRevalidatFee);
//
//        if (this.itemTopup) {
//            if (topupRevalidatFee.equals(new BigDecimal(0))) {
//                this.revalFeeStr = "";
//                this.singleRevalFeeStr = "";
//                this.revalFeeGSTStr = "";
//            }
//        } else {
//            this.revalFeeStr = TransactionUtil.priceWithDecimal(revalidateFee,
//                    null);
//            this.singleRevalFeeStr = TransactionUtil.priceWithDecimal(
//                    normalRevalidatFee, null);
//            this.revalFeeGSTStr = TransactionUtil.gstWithDecimal(revalidateFee,
//                    item.getInventoryTrans().getGstRate(), null);
//        }
//    }

    public ReceiptItemVM(PPSLMInventoryTransactionItem item, BigDecimal gstRate,
                         BigDecimal normalRevalidatFee, BigDecimal topupRevalidatFee) {
        this(item, null);
        BigDecimal revalidateFee = TransactionUtil.getTransItemRevalidateFee(
                item, normalRevalidatFee, topupRevalidatFee);

        if (this.itemTopup) {
            if (topupRevalidatFee.equals(BigDecimal.ZERO)) {
                this.revalFeeStr = "";
                this.singleRevalFeeStr = "";
                this.revalFeeGSTStr = "";
                this.revalFeeFullStr = "";
                this.singleRevalFeeFullStr = "";
                this.revalFeeGSTFullStr = "";
            }
        } else {
            this.revalFeeStr = TransactionUtil.priceWithDecimal(revalidateFee,
                    "");
            this.singleRevalFeeStr = TransactionUtil.priceWithDecimal(
                    normalRevalidatFee, "");
            this.revalFeeGSTStr = TransactionUtil.gstWithDecimal(revalidateFee,
                    gstRate, "");
            this.revalFeeFullStr = TransactionUtil.priceWithDecimal(revalidateFee,
                    null);
            this.singleRevalFeeFullStr = TransactionUtil.priceWithDecimal(
                    normalRevalidatFee, null);
            this.revalFeeGSTFullStr = TransactionUtil.gstWithDecimal(revalidateFee,
                    gstRate, null);
        }
    }

    public Boolean getItemTopup() {
        return itemTopup;
    }

    public void setItemTopup(Boolean itemTopup) {
        this.itemTopup = itemTopup;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getSubTotalStr() {
        return subTotalStr;
    }

    public void setSubTotalStr(String subTotalStr) {
        this.subTotalStr = subTotalStr;
    }

    public String getGstStr() {
        return gstStr;
    }

    public void setGstStr(String gstStr) {
        this.gstStr = gstStr;
    }

    public String getRevalFeeStr() {
        return revalFeeStr;
    }

    public void setRevalFeeStr(String revalFeeStr) {
        this.revalFeeStr = revalFeeStr;
    }

    public String getSingleRevalFeeStr() {
        return singleRevalFeeStr;
    }

    public void setSingleRevalFeeStr(String singleRevalFeeStr) {
        this.singleRevalFeeStr = singleRevalFeeStr;
    }

    public String getRevalFeeGSTStr() {
        return revalFeeGSTStr;
    }

    public void setRevalFeeGSTStr(String revalFeeGSTStr) {
        this.revalFeeGSTStr = revalFeeGSTStr;
    }

    public String getMainItemId() {
        return mainItemId;
    }

    public void setMainItemId(String mainItemId) {
        this.mainItemId = mainItemId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getUnitPriceStr() {
        return unitPriceStr;
    }

    public void setUnitPriceStr(String unitPriceStr) {
        this.unitPriceStr = unitPriceStr;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public String getRevalFeeFullStr() {
        return revalFeeFullStr;
    }

    public void setRevalFeeFullStr(String revalFeeFullStr) {
        this.revalFeeFullStr = revalFeeFullStr;
    }

    public String getSingleRevalFeeFullStr() {
        return singleRevalFeeFullStr;
    }

    public void setSingleRevalFeeFullStr(String singleRevalFeeFullStr) {
        this.singleRevalFeeFullStr = singleRevalFeeFullStr;
    }

    public String getRevalFeeGSTFullStr() {
        return revalFeeGSTFullStr;
    }

    public void setRevalFeeGSTFullStr(String revalFeeGSTFullStr) {
        this.revalFeeGSTFullStr = revalFeeGSTFullStr;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    @Override
    public int compareTo(ReceiptItemVM p2) {
        ReceiptItemVM p1 = this;
        if (!p1.getProdId().equals(p2.getProdId()) ) {
            return p1.getProdId().compareTo(p2.getProdId());
        }
        if (!p1.getScheduleId().equals(p2.getScheduleId())) {
            return p1.getScheduleId().compareTo(p2.getScheduleId());
        }
        if (!p1.itemTopup && !p2.itemTopup) {
            return p1.getId() - p2.getId();
        }
        if (p1.itemTopup && p2.itemTopup) {
            int ires = p1.getMainItemId().compareTo(p2.getMainItemId());
            if (ires != 0) {
                return ires;
            }
            int qres = p1.getQty() - p2.getQty();
            if (qres != 0) {
                return qres;
            }
            return p1.getDisplayName().compareTo(p2.getDisplayName());
        }

        if (!p1.itemTopup && p2.itemTopup) {
            if(p1.getId().equals(p2.getMainItemId()) ){
                return -1;
            }else{
                return String.valueOf(p1.getId()).compareTo(p2.getMainItemId());
            }
        }
        if (p1.itemTopup && !p2.itemTopup) {
            if(p2.getId().equals(p1.getMainItemId() )){
                return 1;
            }else{
                return String.valueOf(p1.getMainItemId()).compareTo(String.valueOf(p2.getId()));
            }
        }
        return p1.getId() - p2.getId();
    }
}

