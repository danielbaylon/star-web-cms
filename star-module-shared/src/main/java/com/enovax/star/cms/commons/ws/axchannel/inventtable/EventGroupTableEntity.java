
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EventGroupTableEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventGroupTableEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CapacityCalendarId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAreaID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EventGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventGroupLinesCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfEventGroupLineEntity" minOccurs="0"/>
 *         &lt;element name="IsSingleEvent" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RecID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="RecVersion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventGroupTableEntity", propOrder = {
    "capacityCalendarId",
    "dataAreaID",
    "eventDate",
    "eventGroupId",
    "eventGroupLinesCollection",
    "isSingleEvent",
    "recID",
    "recVersion"
})
public class EventGroupTableEntity {

    @XmlElementRef(name = "CapacityCalendarId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> capacityCalendarId;
    @XmlElementRef(name = "DataAreaID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataAreaID;
    @XmlElement(name = "EventDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventDate;
    @XmlElementRef(name = "EventGroupId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventGroupId;
    @XmlElementRef(name = "EventGroupLinesCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEventGroupLineEntity> eventGroupLinesCollection;
    @XmlElement(name = "IsSingleEvent")
    protected Integer isSingleEvent;
    @XmlElement(name = "RecID")
    protected Long recID;
    @XmlElement(name = "RecVersion")
    protected Integer recVersion;

    /**
     * Gets the value of the capacityCalendarId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCapacityCalendarId() {
        return capacityCalendarId;
    }

    /**
     * Sets the value of the capacityCalendarId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCapacityCalendarId(JAXBElement<String> value) {
        this.capacityCalendarId = value;
    }

    /**
     * Gets the value of the dataAreaID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataAreaID() {
        return dataAreaID;
    }

    /**
     * Sets the value of the dataAreaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataAreaID(JAXBElement<String> value) {
        this.dataAreaID = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventDate(XMLGregorianCalendar value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the eventGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventGroupId() {
        return eventGroupId;
    }

    /**
     * Sets the value of the eventGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventGroupId(JAXBElement<String> value) {
        this.eventGroupId = value;
    }

    /**
     * Gets the value of the eventGroupLinesCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventGroupLineEntity }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEventGroupLineEntity> getEventGroupLinesCollection() {
        return eventGroupLinesCollection;
    }

    /**
     * Sets the value of the eventGroupLinesCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventGroupLineEntity }{@code >}
     *     
     */
    public void setEventGroupLinesCollection(JAXBElement<ArrayOfEventGroupLineEntity> value) {
        this.eventGroupLinesCollection = value;
    }

    /**
     * Gets the value of the isSingleEvent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsSingleEvent() {
        return isSingleEvent;
    }

    /**
     * Sets the value of the isSingleEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsSingleEvent(Integer value) {
        this.isSingleEvent = value;
    }

    /**
     * Gets the value of the recID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecID() {
        return recID;
    }

    /**
     * Sets the value of the recID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecID(Long value) {
        this.recID = value;
    }

    /**
     * Gets the value of the recVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecVersion() {
        return recVersion;
    }

    /**
     * Sets the value of the recVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecVersion(Integer value) {
        this.recVersion = value;
    }

}
