package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/21/16.
 */
public enum AXProductPromotionProperties {
    OfferId("offerId"),
    PromotionAdditionalInfo("promotionAdditionalInfo"),
    PromotionDisplay("promotionDisplay"),
    CachedDiscountPrice("cachedDiscountPrice"),
    PromotionIcon("promotionIcon"),
    PromotionText("promotionText"),
    SystemName("systemName"),
    SystemStatus("systemStatus"),
    ValidFrom("validFrom"),
    ValidTo("validTo"),
    CurrencyCode("currencyCode"),
    UsedAsUnlocker("usedAsUnlocker"),


    Affiliation("affiliation"),
    //In the Affiliaton under Promotions
    RelatedAffUUID("relatedAffUUID"),
    DiscountCode("discountCode"),
    RelatedDiscountCodeUUID("relatedDiscountCodeUUID"),//uuid of the affiliation
    RelatedProduct("relatedProduct"),
    RelatedProductUUID("relatedAXProductUUID");

    private String propertyName;

    AXProductPromotionProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
