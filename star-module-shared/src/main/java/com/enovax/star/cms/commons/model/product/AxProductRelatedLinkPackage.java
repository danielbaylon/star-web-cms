package com.enovax.star.cms.commons.model.product;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkPackage;

/**
 * Created by jennylynsze on 7/20/16.
 */
public class AxProductRelatedLinkPackage {
    private boolean success;
    private String message;
    private StoreApiChannels channel;
    private AxProductLinkPackage axProductLinkPackage;
    private CrossSellLinkPackage crossSellLinkPackage;
    private PromotionLinkPackage promotionLinkPackage;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public AxProductLinkPackage getAxProductLinkPackage() {
        return axProductLinkPackage;
    }

    public void setAxProductLinkPackage(AxProductLinkPackage axProductLinkPackage) {
        this.axProductLinkPackage = axProductLinkPackage;
    }

    public CrossSellLinkPackage getCrossSellLinkPackage() {
        return crossSellLinkPackage;
    }

    public void setCrossSellLinkPackage(CrossSellLinkPackage crossSellLinkPackage) {
        this.crossSellLinkPackage = crossSellLinkPackage;
    }

    public PromotionLinkPackage getPromotionLinkPackage() {
        return promotionLinkPackage;
    }

    public void setPromotionLinkPackage(PromotionLinkPackage promotionLinkPackage) {
        this.promotionLinkPackage = promotionLinkPackage;
    }
}
