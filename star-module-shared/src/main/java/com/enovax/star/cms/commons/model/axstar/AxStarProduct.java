package com.enovax.star.cms.commons.model.axstar;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 7/19/16.
 */
public class AxStarProduct {
    private Long recordId;
    private String itemId;
    private String locale;
    private String productNumber;
    private String offlineImage;
    private boolean hasLinkedProducts;
    private boolean isMasterProduct;
    private boolean isKit;
    private boolean isRemote;
    private String productName;
    private String description;
    private BigDecimal basePrice;
    private BigDecimal price;
    private BigDecimal adjustedPrice;
    private String searchName;
    private List<AxStarProductPropertyTranslation> productProperties;
    private Map<Long, List<AxStarProductCatalog>> productCatalogMap;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getOfflineImage() {
        return offlineImage;
    }

    public void setOfflineImage(String offlineImage) {
        this.offlineImage = offlineImage;
    }

    public boolean isHasLinkedProducts() {
        return hasLinkedProducts;
    }

    public void setHasLinkedProducts(boolean hasLinkedProducts) {
        this.hasLinkedProducts = hasLinkedProducts;
    }

    public boolean isMasterProduct() {
        return isMasterProduct;
    }

    public void setIsMasterProduct(boolean isMasterProduct) {
        this.isMasterProduct = isMasterProduct;
    }

    public boolean isKit() {
        return isKit;
    }

    public void setIsKit(boolean isKit) {
        this.isKit = isKit;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setIsRemote(boolean isRemote) {
        this.isRemote = isRemote;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(BigDecimal adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public List<AxStarProductPropertyTranslation> getProductProperties() {
        return productProperties;
    }

    public void setProductProperties(List<AxStarProductPropertyTranslation> productProperties) {
        this.productProperties = productProperties;
    }

    public Map<Long, List<AxStarProductCatalog>> getProductCatalogMap() {
        return productCatalogMap;
    }

    public void setProductCatalogMap(Map<Long, List<AxStarProductCatalog>> productCatalogMap) {
        this.productCatalogMap = productCatalogMap;
    }

    //    {
//        "ProductCatalogMap": {
//        "5637157335": [
//        {
//            "RecordId": 0,
//                "ChannelId": 0,
//                "Name": "",
//                "Description": "",
//                "Language": "",
//                "IsSnapshotEnabled": false,
//                "ValidFrom": "1900-01-01T00:00:00+00:00",
//                "ValidTo": "2154-12-31T00:00:00+08:00",
//                "CreatedOn": "1901-01-01T00:00:00+00:00",
//                "ModifiedOn": "1901-01-01T00:00:00+00:00",
//                "PublishedOn": "1901-01-01T00:00:00+00:00",
//                "Image": null,
//                "ExtensionProperties": []
//        }
//        ]
//    },
//        "RecordId": 5637157335,
//            "ItemId": "101200000013",
//            "Locale": "en-us",
//            "ProductNumber": "101200000013",
//            "OfflineImage": "",
//            "Rules": {
//        "ProductId": 5637157335,
//                "HasLinkedProducts": false,
//                "IsBlocked": false,
//                "DateOfBlocking": "1900-01-01T00:00:00+00:00",
//                "DateToActivate": "1900-01-01T00:00:00+00:00",
//                "DateToBlock": "9999-12-31T23:59:59.9999999+00:00",
//                "PriceKeyingRequirementValue": 0,
//                "QuantityKeyingRequirementValue": 1,
//                "MustKeyInComment": false,
//                "CanQuantityBecomeNegative": false,
//                "MustScaleItem": false,
//                "CanPriceBeZero": false,
//                "IsSerialized": false,
//                "IsActiveInSalesProcess": false,
//                "DefaultUnitOfMeasure": "",
//                "ExtensionProperties": []
//    },
//        "HasLinkedProducts": false,
//            "IsMasterProduct": false,
//            "IsKit": false,
//            "IsRemote": false,
//            "ChangeTrackingInformation": {
//        "ModifiedDateTime": "1901-01-01T00:00:00+00:00",
//                "ChangeActionValue": 1,
//                "RequestedActionValue": 1
//    },
//        "ProductName": "Segway @Fun Ride (2 Rides)",
//            "Description": "",
//            "Image": {
//        "Items": [
//        {
//            "Url": "Images/Products/en-us/101200000013.jpg",
//                "AltText": ""
//        }
//        ]
//    },
//        "UnitsOfMeasureSymbol": [],
//        "LinkedProducts": [],
//        "BasePrice": 0,
//            "Price": 0,
//            "AdjustedPrice": 0,
//            "Context": {
//        "ChannelId": 5637149827,
//                "CatalogId": null
//    },
//        "PrimaryCategoryId": 0,
//            "CategoryIds": [
//        5637150593
//        ],
//        "RelatedProducts": [],
//        "ProductsRelatedToThis": [],
//        "ProductSchema": [
//        "ProductName",
//                "Description",
//                "ItemNumber",
//                "Image2",
//                "Video",
//                "Image"
//        ],
//        "ProductProperties": [
//        {
//            "TranslationLanguage": "en-us",
//                "TranslatedProperties": [
//            {
//                "PropertyTypeValue": 5,
//                    "KeyName": "ProductName",
//                    "FriendlyName": "Product name",
//                    "RecordId": 5637144590,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "Segway @Fun Ride (2 Rides)",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "Segway @Fun Ride (2 Rides)",
//                    "ExtensionProperties": [],
//                "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//            },
//            {
//                "PropertyTypeValue": 5,
//                    "KeyName": "Description",
//                    "FriendlyName": "Description",
//                    "RecordId": 5637144591,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//                "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//            },
//            {
//                "PropertyTypeValue": 5,
//                    "KeyName": "ItemNumber",
//                    "FriendlyName": "Item number",
//                    "RecordId": 5637144592,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "101200000013",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//                "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//            },
//            {
//                "PropertyTypeValue": 41,
//                    "KeyName": "Image2",
//                    "FriendlyName": "Image",
//                    "RecordId": 5637144577,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144580,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//                "IsReference": false,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//            },
//            {
//                "PropertyTypeValue": 40,
//                    "KeyName": "Video",
//                    "FriendlyName": "Video",
//                    "RecordId": 5637144578,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144581,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//                "IsReference": false,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//            },
//            {
//                "PropertyTypeValue": 41,
//                    "KeyName": "Image",
//                    "FriendlyName": "Image",
//                    "RecordId": 5637144594,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144580,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//                "IsReference": true,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//            }
//            ]
//        }
//        ],
//        "CompositionInformation": null,
//            "DefaultProductProperties": {
//        "Image2": {
//            "PropertyTypeValue": 41,
//                    "KeyName": "Image2",
//                    "FriendlyName": "Image",
//                    "RecordId": 5637144577,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144580,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//            "IsReference": false,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//        },
//        "Video": {
//            "PropertyTypeValue": 40,
//                    "KeyName": "Video",
//                    "FriendlyName": "Video",
//                    "RecordId": 5637144578,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144581,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//            "IsReference": false,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//        },
//        "Image": {
//            "PropertyTypeValue": 41,
//                    "KeyName": "Image",
//                    "FriendlyName": "Image",
//                    "RecordId": 5637144594,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 5637144580,
//                    "ValueString": "Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//            "IsReference": true,
//                    "ProductId": 0,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 2,
//                    "DateTimeOffsetValue": null
//        },
//        "ProductName": {
//            "PropertyTypeValue": 5,
//                    "KeyName": "ProductName",
//                    "FriendlyName": "Product name",
//                    "RecordId": 5637144590,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "Segway @Fun Ride (2 Rides)",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "Segway @Fun Ride (2 Rides)",
//                    "ExtensionProperties": [],
//            "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//        },
//        "Description": {
//            "PropertyTypeValue": 5,
//                    "KeyName": "Description",
//                    "FriendlyName": "Description",
//                    "RecordId": 5637144591,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//            "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//        },
//        "ItemNumber": {
//            "PropertyTypeValue": 5,
//                    "KeyName": "ItemNumber",
//                    "FriendlyName": "Item number",
//                    "RecordId": 5637144592,
//                    "IsDimensionProperty": false,
//                    "AttributeValueId": 0,
//                    "ValueString": "101200000013",
//                    "UnitText": null,
//                    "GroupId": 0,
//                    "GroupTypeValue": 0,
//                    "GroupName": "",
//                    "Language": "en-us",
//                    "Translation": "",
//                    "ExtensionProperties": [],
//            "IsReference": true,
//                    "ProductId": 5637157335,
//                    "CategoryId": 0,
//                    "CatalogId": 0,
//                    "Distance": 0,
//                    "Source": 5,
//                    "DateTimeOffsetValue": "1900-01-01T00:00:00+00:00"
//        }
//    },
//        "SearchName": "Segway@FunRide(2 Rid",
//            "DisplayNumber": null,
//            "ExtensionProperties": []
//    }
}
