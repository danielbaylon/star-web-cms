package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackageItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Repository
public interface PPSLMMixMatchPackageItemRepository extends JpaRepository<PPSLMMixMatchPackageItem, Integer> {
    List<PPSLMMixMatchPackageItem> findByTransactionItemId(Integer transItemId);
    List<PPSLMMixMatchPackageItem> findByTransactionItemIdIn(List<Integer> transItemIds);
    List<PPSLMMixMatchPackageItem> findByPackageId(Integer pkgId);

    @Query("SELECT i.transactionItemId " +
            " from PPSLMMixMatchPackageItem i " +
            " where i.packageId = :packageId " +
            " and i.transItemType = 'Standard' ")
    List<Integer> getPkgTransactionItemsIdByPkgId(@Param("packageId") Integer packageId);

    @Query(value = "select count(1) from PPSLMMixMatchPackageItem item, PPSLMMixMatchPackage package " +
            "where item.packageId = package.id " +
            "and package.status = 'Reserved' " +
            "and item.receiptNum = :receiptNum ", nativeQuery = true)
    Long noOfItemWithSameTransactionReserved(@Param("receiptNum") String receiptNum);

    @Query("select i from PPSLMMixMatchPackageItem i where i.packageId = :packageId " +
            "and i.itemProductCode= :itemCode")
    List<PPSLMMixMatchPackageItem> getPkgItemsByItemCode(@Param("packageId") Integer packageId, @Param("itemCode") String itemCode);

}
