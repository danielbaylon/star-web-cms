package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class MixMatchPkgTransItemVM implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer itemId;
    private String displayName;
    private String displayDetails;
    private Integer pkgQty;
    private Integer qtyRedeemed;
    private String receiptNum;
    private List<MixMatchPkgTopupItemVM> topupItems;
    private String ticketType;

    public MixMatchPkgTransItemVM() {}

    public MixMatchPkgTransItemVM(PPSLMInventoryTransactionItem item) {
        this.itemId = item.getId();
        this.displayName = item.getDisplayName();
        this.displayDetails = item.getDisplayDetails();
        this.pkgQty = item.getPkgQty();
        this.receiptNum = item.getInventoryTrans().getReceiptNum();
        this.ticketType = item.getTicketType();
        topupItems = new ArrayList<MixMatchPkgTopupItemVM>();

        if(item.getTopupItems() != null) {
            for (PPSLMInventoryTransactionItem topup : item.getTopupItems()) {
                topup.setPkgQty(item.getPkgQty()); //should have same no. of quantity
                MixMatchPkgTopupItemVM tempObj = new MixMatchPkgTopupItemVM(topup);
                if(!topupItems.contains(tempObj)){
                    topupItems.add(tempObj);
                }

            }
        }
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public List<MixMatchPkgTopupItemVM> getTopupItems() {
        return topupItems;
    }

    public void setTopupItems(List<MixMatchPkgTopupItemVM> topupItems) {
        this.topupItems = topupItems;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getPkgQty() {
        return pkgQty;
    }

    public void setPkgQty(Integer pkgQty) {
        this.pkgQty = pkgQty;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof MixMatchPkgTransItemVM))
            return false;
        MixMatchPkgTransItemVM otherMyClass = (MixMatchPkgTransItemVM) other;
        if (this.itemId.equals(otherMyClass.getItemId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) serialVersionUID + itemId;
        return result;
    }
}
