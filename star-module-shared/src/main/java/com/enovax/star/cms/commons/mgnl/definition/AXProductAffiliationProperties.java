package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/21/16.
 */
public enum AXProductAffiliationProperties {

    //Independent Affiliation
    RecordId("recordId"),
    SystemName("systemName"),
    SystemDescription("systemDescription"),
    DiscountType("affiliationDiscountType"),
    DiscountTypeCreditCard("affiliationDiscountTypeCreditCard"),
    DiscountTypePaymentType("affiliationDiscountTypePaymentType");

    private String propertyName;

    AXProductAffiliationProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
