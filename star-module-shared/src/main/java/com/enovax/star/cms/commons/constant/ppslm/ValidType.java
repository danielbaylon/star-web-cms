package com.enovax.star.cms.commons.constant.ppslm;

public enum ValidType {
    NA("NA","NA"),
    PERIOD("PERIOD","Start&End Date"),
    DAY("DAY","Number of Days");

    public final String code;
    public final String desc;

    private ValidType(String code,String desc) {
        this.code = code;
        this.desc = desc;
    }
}
