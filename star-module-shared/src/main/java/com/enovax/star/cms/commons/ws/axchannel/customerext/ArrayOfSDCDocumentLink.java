
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_DocumentLink complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_DocumentLink">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_DocumentLink" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_DocumentLink" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_DocumentLink", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcDocumentLink"
})
public class ArrayOfSDCDocumentLink {

    @XmlElement(name = "SDC_DocumentLink", nillable = true)
    protected List<SDCDocumentLink> sdcDocumentLink;

    /**
     * Gets the value of the sdcDocumentLink property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcDocumentLink property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCDocumentLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCDocumentLink }
     * 
     * 
     */
    public List<SDCDocumentLink> getSDCDocumentLink() {
        if (sdcDocumentLink == null) {
            sdcDocumentLink = new ArrayList<SDCDocumentLink>();
        }
        return this.sdcDocumentLink;
    }

}
