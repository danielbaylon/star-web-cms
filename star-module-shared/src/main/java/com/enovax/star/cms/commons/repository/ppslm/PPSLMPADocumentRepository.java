package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PPSLMPADocumentRepository extends JpaRepository<PPSLMPADocument, Integer> {
    PPSLMPADocument findById(Integer id);
}
