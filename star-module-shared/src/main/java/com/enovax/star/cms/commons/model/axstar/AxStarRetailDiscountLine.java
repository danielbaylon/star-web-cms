package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.List;

public class AxStarRetailDiscountLine {

    //Unused properties
    
//    private List<String> extensionProperties;
    
    private String offerId;
    private BigDecimal discountLineNumber; //1,
    private Integer discountLinePercentOrValue; //0,
    private String mixAndMatchLineGroup; //private String private String ,
    private Integer mixAndMatchLineSpecificDiscountType; //0,
    private Integer mixAndMatchLineNumberOfItemsNeeded; //0,
    private Integer discountMethod; //0,
    private BigDecimal discountAmount; //0,
    private BigDecimal discountPercent; //10,
    private BigDecimal offerPrice; //0,
    private BigDecimal offerPriceIncludingTax; //0,
    private String unitOfMeasureSymbol; //private String Eaprivate String ,
    private Long categoryId; //5637150597,
    private Long productId; //5637149079,
    private Long distinctProductVariantId; //0,

    private List<AxStarExtensionProperty> extensionProperties;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public BigDecimal getDiscountLineNumber() {
        return discountLineNumber;
    }

    public void setDiscountLineNumber(BigDecimal discountLineNumber) {
        this.discountLineNumber = discountLineNumber;
    }

    public Integer getDiscountLinePercentOrValue() {
        return discountLinePercentOrValue;
    }

    public void setDiscountLinePercentOrValue(Integer discountLinePercentOrValue) {
        this.discountLinePercentOrValue = discountLinePercentOrValue;
    }

    public String getMixAndMatchLineGroup() {
        return mixAndMatchLineGroup;
    }

    public void setMixAndMatchLineGroup(String mixAndMatchLineGroup) {
        this.mixAndMatchLineGroup = mixAndMatchLineGroup;
    }

    public Integer getMixAndMatchLineSpecificDiscountType() {
        return mixAndMatchLineSpecificDiscountType;
    }

    public void setMixAndMatchLineSpecificDiscountType(Integer mixAndMatchLineSpecificDiscountType) {
        this.mixAndMatchLineSpecificDiscountType = mixAndMatchLineSpecificDiscountType;
    }

    public Integer getMixAndMatchLineNumberOfItemsNeeded() {
        return mixAndMatchLineNumberOfItemsNeeded;
    }

    public void setMixAndMatchLineNumberOfItemsNeeded(Integer mixAndMatchLineNumberOfItemsNeeded) {
        this.mixAndMatchLineNumberOfItemsNeeded = mixAndMatchLineNumberOfItemsNeeded;
    }

    public Integer getDiscountMethod() {
        return discountMethod;
    }

    public void setDiscountMethod(Integer discountMethod) {
        this.discountMethod = discountMethod;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(BigDecimal offerPrice) {
        this.offerPrice = offerPrice;
    }

    public BigDecimal getOfferPriceIncludingTax() {
        return offerPriceIncludingTax;
    }

    public void setOfferPriceIncludingTax(BigDecimal offerPriceIncludingTax) {
        this.offerPriceIncludingTax = offerPriceIncludingTax;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getDistinctProductVariantId() {
        return distinctProductVariantId;
    }

    public void setDistinctProductVariantId(Long distinctProductVariantId) {
        this.distinctProductVariantId = distinctProductVariantId;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    /*
    [SAMPLE JSON]
    {
        "OfferId": "MFLG-000001",
        "DiscountLineNumber": 1,
        "DiscountLinePercentOrValue": 0,
        "MixAndMatchLineGroup": "",
        "MixAndMatchLineSpecificDiscountType": 0,
        "MixAndMatchLineNumberOfItemsNeeded": 0,
        "DiscountMethod": 0,
        "DiscountAmount": 0,
        "DiscountPercent": 10,
        "OfferPrice": 0,
        "OfferPriceIncludingTax": 0,
        "UnitOfMeasureSymbol": "Ea",
        "CategoryId": 5637150597,
        "ProductId": 5637149079,
        "DistinctProductVariantId": 0,
        "ExtensionProperties": [
          {
            "Key": "DATAAREAID",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": "mflg"
            }
          }
        ]
      }
     */
}
