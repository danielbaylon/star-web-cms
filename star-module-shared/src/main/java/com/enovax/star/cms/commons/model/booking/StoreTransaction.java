package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

public class StoreTransaction implements Serializable {

    private String receiptNumber;
    private String channel;
    private String custName;
    private String custEmail;
    private String custIdType;
    private String custIdNo;
//    private String custJewelCard;
    private String custSalutation;
    private String custMobile;
    private String custCompanyName;
    private boolean custSubscribe = false;
    private String custNationality;
    private String custReferSource;
    private String custDob;
    private String paymentType;
    private BigDecimal totalAmount;
    private String currency;
//    private String bookFeeType;
//    private int bookFeeCents;
//    private int bookFeeItemCode;
//    private boolean bookFeeWaived;
    private String tmMerchantId;
    private String tmSelectedMerchant;
//    private Date statusDate;
    private String errorCode;
    private String stage;
//    private String sactResId;
//    private String sactRecurResId;
//    private String sactPin;
    private String tmStatus;
//    private String tmErrorMessage;
//    private String tmApprovalCode;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private String trafficSource = "";
    private String ip = "";
    private String maskedCc = "";
    private String lanCode = "";
    private String mgnlLocale = "";

    private boolean recoveryFlowTicketsPrinted;

    private Map<String, String> appliedPromoCodes = new HashMap<>();
    private FunCartAffiliation bankAffiliation;
    private FunCartAffiliation paymentModeAffiliation;

    private List<StoreTransactionItem> items = new ArrayList<>();
    private List<AxRetailTicketRecord> axTickets = new ArrayList<>();
    private List<ETicketData> eTickets = new ArrayList<>();
    private String pinCode;
    private boolean pinToGenerate = false;

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public boolean isCustSubscribe() {
        return custSubscribe;
    }

    public void setCustSubscribe(boolean custSubscribe) {
        this.custSubscribe = custSubscribe;
    }

    public String getCustNationality() {
        return custNationality;
    }

    public void setCustNationality(String custNationality) {
        this.custNationality = custNationality;
    }

    public String getCustReferSource() {
        return custReferSource;
    }

    public void setCustReferSource(String custReferSource) {
        this.custReferSource = custReferSource;
    }

    public String getCustDob() {
        return custDob;
    }

    public void setCustDob(String custDob) {
        this.custDob = custDob;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public List<StoreTransactionItem> getItems() {
        return items;
    }

    public void setItems(List<StoreTransactionItem> items) {
        this.items = items;
    }

    public List<ETicketData> geteTickets() {
        return eTickets;
    }

    public void seteTickets(List<ETicketData> eTickets) {
        this.eTickets = eTickets;
    }

    public List<AxRetailTicketRecord> getAxTickets() {
        return axTickets;
    }

    public void setAxTickets(List<AxRetailTicketRecord> axTickets) {
        this.axTickets = axTickets;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCustIdType() {
        return custIdType;
    }

    public void setCustIdType(String custIdType) {
        this.custIdType = custIdType;
    }

    public String getCustIdNo() {
        return custIdNo;
    }

    public void setCustIdNo(String custIdNo) {
        this.custIdNo = custIdNo;
    }

    public String getCustSalutation() {
        return custSalutation;
    }

    public void setCustSalutation(String custSalutation) {
        this.custSalutation = custSalutation;
    }

    public String getCustCompanyName() {
        return custCompanyName;
    }

    public void setCustCompanyName(String custCompanyName) {
        this.custCompanyName = custCompanyName;
    }

    public boolean isRecoveryFlowTicketsPrinted() {
        return recoveryFlowTicketsPrinted;
    }

    public void setRecoveryFlowTicketsPrinted(boolean recoveryFlowTicketsPrinted) {
        this.recoveryFlowTicketsPrinted = recoveryFlowTicketsPrinted;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getTmSelectedMerchant() {
        return tmSelectedMerchant;
    }

    public void setTmSelectedMerchant(String tmSelectedMerchant) {
        this.tmSelectedMerchant = tmSelectedMerchant;
    }

    public boolean isPinToGenerate() {
        return pinToGenerate;
    }

    public void setPinToGenerate(boolean pinToGenerate) {
        this.pinToGenerate = pinToGenerate;
    }

    public Map<String, String> getAppliedPromoCodes() {
        return appliedPromoCodes;
    }

    public void setAppliedPromoCodes(Map<String, String> appliedPromoCodes) {
        this.appliedPromoCodes = appliedPromoCodes;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMaskedCc() {
        return maskedCc;
    }

    public void setMaskedCc(String maskedCc) {
        this.maskedCc = maskedCc;
    }

    public String getLanCode() {
        return lanCode;
    }

    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }

    public FunCartAffiliation getBankAffiliation() {
        return bankAffiliation;
    }

    public void setBankAffiliation(FunCartAffiliation bankAffiliation) {
        this.bankAffiliation = bankAffiliation;
    }

    public FunCartAffiliation getPaymentModeAffiliation() {
        return paymentModeAffiliation;
    }

    public void setPaymentModeAffiliation(FunCartAffiliation paymentModeAffiliation) {
        this.paymentModeAffiliation = paymentModeAffiliation;
    }

    public String getMgnlLocale() {
        return mgnlLocale;
    }

    public void setMgnlLocale(String mgnlLocale) {
        this.mgnlLocale = mgnlLocale;
    }
}