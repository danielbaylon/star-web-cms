package com.enovax.star.cms.commons.service.ticketgen;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.booking.StoreTransaction;
import com.enovax.star.cms.commons.model.ticketgen.ETicketDataCompiled;

import java.io.IOException;
import java.util.Map;

public interface ITicketGenerationService {

    byte[] generateTickets(StoreApiChannels channel, ETicketDataCompiled compiledTicketData) throws IOException;

    String generateReceiptXmlWithQRCode(StoreTransaction txn, String inputReceiptTemplateXml, String qrCode) throws IOException;
    String generateReceiptXml(StoreTransaction txn, String inputReceiptTemplateXml) throws IOException;

    Map<String, String> generatePaperTicketXml(
            ETicketDataCompiled compiledTicketData,
            String inputTemplateXmlQr, String inputTemplateXmlBarcode) throws IOException;
}
