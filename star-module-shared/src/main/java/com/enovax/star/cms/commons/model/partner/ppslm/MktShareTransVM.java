package com.enovax.star.cms.commons.model.partner.ppslm;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lavanya on 1/11/16.
 */
public class MktShareTransVM {
    private Integer paId;
    private String tranDateStr;
    private BigDecimal amount;
    private String amountStr;
    private List<MktShareRegionVM> regionMkts;

    public Integer getPaId() {
        return paId;
    }

    public void setPaId(Integer paId) {
        this.paId = paId;
    }

    public String getTranDateStr() {
        return tranDateStr;
    }

    public void setTranDateStr(String tranDateStr) {
        this.tranDateStr = tranDateStr;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<MktShareRegionVM> getRegionMkts() {
        return regionMkts;
    }

    public void setRegionMkts(List<MktShareRegionVM> regionMkts) {
        this.regionMkts = regionMkts;
    }

    public String getAmountStr() {
        return amountStr;
    }

    public void setAmountStr(String amountStr) {
        this.amountStr = amountStr;
    }
}
