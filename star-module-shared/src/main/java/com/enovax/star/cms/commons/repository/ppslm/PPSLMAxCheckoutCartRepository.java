package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAxCheckoutCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jennylynsze on 8/18/16.
 */
@Repository
public interface PPSLMAxCheckoutCartRepository extends JpaRepository<PPSLMAxCheckoutCart, Integer> {

    PPSLMAxCheckoutCart findByReceiptNum(String receiptNum);
}
