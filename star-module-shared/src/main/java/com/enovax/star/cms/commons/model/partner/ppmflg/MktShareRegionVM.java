package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.math.BigDecimal;

/**
 * Created by lavanya on 1/11/16.
 */
public class MktShareRegionVM implements Cloneable{
    private Integer paId;
    private Integer regionId;
    private String regionName;
    private String ctyId;
    private String ctyName;
    private Integer percentage;
    private String transDateStr;
    private String ammountStr;
    private BigDecimal ammount;


    public Integer getPaId() {
        return paId;
    }

    public void setPaId(Integer paId) {
        this.paId = paId;
    }

    public String getCtyId() {
        return ctyId;
    }

    public void setCtyId(String ctyId) {
        this.ctyId = ctyId;
    }

    public String getCtyName() {
        return ctyName;
    }

    public void setCtyName(String ctyName) {
        this.ctyName = ctyName;
    }

    public String getAmmountStr() {
        return ammountStr;
    }

    public void setAmmountStr(String ammountStr) {
        this.ammountStr = ammountStr;
    }

    public String getTransDateStr() {
        return transDateStr;
    }

    public void setTransDateStr(String transDateStr) {
        this.transDateStr = transDateStr;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Override
    public MktShareRegionVM clone() throws CloneNotSupportedException {
        return (MktShareRegionVM) super.clone();
    }
}
