package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCommerceProperty;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Collection(SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTableEntity)
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskSendRedeemTicketToAXResponse {

	@JsonProperty("IsSucceed")
	private String isSucceed;

	@JsonProperty("ErrorMessage")
	private String errorMessage;

	/**
	 * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
	 * 
	 */
	@JsonProperty("ExtensionProperties")
	private List<AxCommerceProperty> axExtensionPropertiesDataList;

	public String getIsSucceed() {
		return isSucceed;
	}

	public void setIsSucceed(String isSucceed) {
		this.isSucceed = isSucceed;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<AxCommerceProperty> getAxExtensionPropertiesDataList() {
		return axExtensionPropertiesDataList;
	}

	public void setAxExtensionPropertiesDataList(List<AxCommerceProperty> axExtensionPropertiesDataList) {
		this.axExtensionPropertiesDataList = axExtensionPropertiesDataList;
	}

}
