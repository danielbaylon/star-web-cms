package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lavanya on 1/11/16.
 */
public class MktSharePartnerVM {
    private Integer id;
    private String uen;
    private String status;
    private String orgName;
    private BigDecimal total;
    private String tranDateStr;
    private String totalStr;
    private List<MktShareTransVM> dailyTrans;
    private List<MktShareRegionVM> mktRegionDetails;

    public MktSharePartnerVM(Integer id, String orgName, String uen, String status, BigDecimal total) {
        this.id = id;
        this.orgName = orgName;
        this.uen = uen;
        this.status = status;
        this.total = total;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public List<MktShareTransVM> getDailyTrans() {
        return dailyTrans;
    }

    public void setDailyTrans(List<MktShareTransVM> dailyTrans) {
        this.dailyTrans = dailyTrans;
    }

    public String getTotalStr() {
        return totalStr;
    }

    public void setTotalStr(String totalStr) {
        this.totalStr = totalStr;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTranDateStr() {
        return tranDateStr;
    }

    public void setTranDateStr(String tranDateStr) {
        this.tranDateStr = tranDateStr;
    }

    public List<MktShareRegionVM> getMktRegionDetails() {
        return mktRegionDetails;
    }

    public void setMktRegionDetails(List<MktShareRegionVM> mktRegionDetails) {
        this.mktRegionDetails = mktRegionDetails;
    }
}
