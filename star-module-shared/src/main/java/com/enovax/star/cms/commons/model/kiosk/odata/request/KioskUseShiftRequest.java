package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskUseShiftRequest {

    @JsonProperty("ShiftId")
    private String shiftId;

    @JsonProperty("TerminalId")
    private String terminalId;

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

}
