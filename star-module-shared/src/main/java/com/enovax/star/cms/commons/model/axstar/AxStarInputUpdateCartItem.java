package com.enovax.star.cms.commons.model.axstar;

public class AxStarInputUpdateCartItem {

    private String lineId;
    private int quantity;

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
