package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventGroupList
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventGroupList {

    /**
     * Collection(SDC_NonBindableCRTExtension.Entity.EventGroups)
     * 
     */
    @JsonProperty("EventGroups")
    private List<AxEventGroup> axEventGroups = new ArrayList<>();

    public List<AxEventGroup> getAxEventGroups() {
        return axEventGroups;
    }

    public void setAxEventGroups(List<AxEventGroup> axEventGroups) {
        this.axEventGroups = axEventGroups;
    }
    
    

    /**
     * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
     * 
     */
//    @JsonProperty("ExtensionProperties")
//    private AxCommercePropertyList axCommercePropertyList = new AxCommercePropertyList();
//
//    public AxEventGroups getAxEventGroups() {
//        return axEventGroups;
//    }
//
//    public void setAxEventGroups(AxEventGroups axEventGroups) {
//        this.axEventGroups = axEventGroups;
//    }
//
//    public AxCommercePropertyList getAxCommercePropertyList() {
//        return axCommercePropertyList;
//    }
//
//    public void setAxCommercePropertyList(AxCommercePropertyList axCommercePropertyList) {
//        this.axCommercePropertyList = axCommercePropertyList;
//    }

}
