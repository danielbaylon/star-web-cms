package com.enovax.star.cms.commons.mgnl.form.field.validator.factory;

import com.enovax.star.cms.commons.mgnl.form.field.validator.DateRangeValidator;
import com.enovax.star.cms.commons.mgnl.form.field.validator.definition.DateRangeValidatorDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 10/14/16.
 */
public class DateRangeValidatorFactory  extends AbstractFieldValidatorFactory<DateRangeValidatorDefinition> {

    private Item item;
    private DateRangeValidatorDefinition definition;

    public DateRangeValidatorFactory(DateRangeValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition = definition;
    }

    @Override
    public Validator createValidator() {
        return new DateRangeValidator(this.item, this.definition.getStartDateField(), this.definition.getEndDateField(), this.definition.getErrorMessage());
    }
}
