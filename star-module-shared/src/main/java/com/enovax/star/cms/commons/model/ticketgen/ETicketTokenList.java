package com.enovax.star.cms.commons.model.ticketgen;

import com.enovax.star.cms.commons.constant.ticket.OnlineAxTicketToken;

import java.util.Map;

/**
 * Created by jennylynsze on 12/19/16.
 */
public class ETicketTokenList {
    private String printLabel;

    private String publishPrice;

    private String ticketType;

    private String productOwner;

    private String paxPerTicket;

    private String usageValidity;

    private String ticketValidity;

    private String validityStartDate;

    private String validityEndDate;

    private String legalEntity;

    private String ticketNumber;

    private String ticketCode;

    private String productImage;

    private String itemNumber;

    private String customerName;

    private String displayName;

    private String description;

    private String productSearchName;

    private String productVarConfig;

    private String productVarSize;

    private String productVarColor;

    private String productVarStyle;

    private String packageItem;

    private String packageItemName;

    private String mixMatchPackageName;

    private String mixMatchDescription;

    private String eventDate;

    private String eventName;

    private String facilities;

    private String facilityAction;

    private String operationIds;

    private String discountApplied;

    private String barcodePLU;

    private String barcodeStartDate;

    private String barcodeEndDate;

    private String termAndCondition;

    private String disclaimer;

    private String ticketLineDesc;

    private String packagePrintLabel;

    public ETicketTokenList() {}

    public ETicketTokenList(Map<String, String> axTokenList) {
        for (String key: axTokenList.keySet()) {
            String value = axTokenList.get(key);

            OnlineAxTicketToken ticketToken = OnlineAxTicketToken.toEnum(key);
            if (ticketToken != null) {
                switch (ticketToken) {
                    case BarcodeEndDate:
                        this.setBarcodeEndDate(value);
                        break;
                    case BarcodePLU:
                        this.setBarcodePLU(value);
                        break;
                    case BarcodeStartDate:
                        this.setBarcodeStartDate(value);
                        break;
                    case CustomerName:
                        this.setCustomerName(value);
                        break;
                    case Description:
                        this.setDescription(value);
                        break;
                    case DiscountApplied:
                        this.setDiscountApplied(value);
                        break;
                    case DisplayName:
                        this.setDisplayName(value);
                        break;
                    case EventDate:
                        this.setEventDate(value);
                        break;
                    case EventName:
                        this.setEventName(value);
                        break;
                    case Facilities:
                        this.setFacilities(value);
                        break;
                    case FacilityAction:
                        this.setFacilityAction(value);
                        break;
                    case ItemNumber:
                        this.setItemNumber(value);
                        break;
                    case LegalEntity:
                        this.setLegalEntity(value);
                        break;
                    case MixMatchDescription:
                        this.setMixMatchDescription(value);
                        break;
                    case MixMatchPackageName:
                        this.setMixMatchPackageName(value);
                        break;
                    case OperationIds:
                        this.setOperationIds(value);
                        break;
                    case PackageItem:
                        this.setPackageItem(value);
                        break;
                    case PackageItemName:
                        this.setPackageItemName(value);
                        break;
                    case PaxPerTicket:
                        this.setPaxPerTicket(value);
                        break;
                    case PrintLabel:
                        this.setPrintLabel(value);
                        break;
                    case ProductImage:
                        this.setProductImage(value);
                        break;
                    case ProductOwner:
                        this.setProductOwner(value);
                        break;
                    case ProductSearchName:
                        this.setProductSearchName(value);
                        break;
                    case ProductVarColor:
                        this.setProductVarColor(value);
                        break;
                    case ProductVarConfig:
                        this.setProductVarConfig(value);
                        break;
                    case ProductVarSize:
                        this.setProductVarSize(value);
                        break;
                    case ProductVarStyle:
                        this.setProductVarStyle(value);
                        break;
                    case PublishPrice:
                        this.setPublishPrice(value);
                        break;
                    case TicketCode:
                        this.setTicketCode(value);
                        break;
                    case TicketNumber:
                        this.setTicketNumber(value);
                        break;
                    case TicketType:
                        this.setTicketType(value);
                        break;
                    case TicketValidity:
                        this.setTicketValidity(value);
                        break;
                    case UsageValidity:
                        this.setUsageValidity(value);
                        break;
                    case ValidityEndDate:
                        this.setValidityEndDate(value);
                        break;
                    case ValidityStartDate:
                        this.setValidityStartDate(value);
                        break;
                    case TermAndCondition:
                        this.setTermAndCondition(value);
                        break;
                    case Disclaimer:
                        this.setDisclaimer(value);
                        break;
                    case TicketLineDesc:
                        this.setTicketLineDesc(value);
                    case PackagePrintLabel:
                        this.setPackagePrintLabel(value);
                    default:
                        break;
                }
            }
        }
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getPrintLabel() {
        return printLabel;
    }

    public void setPrintLabel(String printLabel) {
        this.printLabel = printLabel;
    }

    public String getPublishPrice() {
        return publishPrice;
    }

    public void setPublishPrice(String publishPrice) {
        this.publishPrice = publishPrice;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(String productOwner) {
        this.productOwner = productOwner;
    }

    public String getPaxPerTicket() {
        return paxPerTicket;
    }

    public void setPaxPerTicket(String paxPerTicket) {
        this.paxPerTicket = paxPerTicket;
    }

    public String getUsageValidity() {
        return usageValidity;
    }

    public void setUsageValidity(String usageValidity) {
        this.usageValidity = usageValidity;
    }

    public String getTicketValidity() {
        return ticketValidity;
    }

    public void setTicketValidity(String ticketValidity) {
        this.ticketValidity = ticketValidity;
    }

    public String getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(String validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public String getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(String validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductSearchName() {
        return productSearchName;
    }

    public void setProductSearchName(String productSearchName) {
        this.productSearchName = productSearchName;
    }

    public String getProductVarConfig() {
        return productVarConfig;
    }

    public void setProductVarConfig(String productVarConfig) {
        this.productVarConfig = productVarConfig;
    }

    public String getProductVarSize() {
        return productVarSize;
    }

    public void setProductVarSize(String productVarSize) {
        this.productVarSize = productVarSize;
    }

    public String getProductVarColor() {
        return productVarColor;
    }

    public void setProductVarColor(String productVarColor) {
        this.productVarColor = productVarColor;
    }

    public String getProductVarStyle() {
        return productVarStyle;
    }

    public void setProductVarStyle(String productVarStyle) {
        this.productVarStyle = productVarStyle;
    }

    public String getPackageItem() {
        return packageItem;
    }

    public void setPackageItem(String packageItem) {
        this.packageItem = packageItem;
    }

    public String getPackageItemName() {
        return packageItemName;
    }

    public void setPackageItemName(String packageItemName) {
        this.packageItemName = packageItemName;
    }

    public String getMixMatchPackageName() {
        return mixMatchPackageName;
    }

    public void setMixMatchPackageName(String mixMatchPackageName) {
        this.mixMatchPackageName = mixMatchPackageName;
    }

    public String getMixMatchDescription() {
        return mixMatchDescription;
    }

    public void setMixMatchDescription(String mixMatchDescription) {
        this.mixMatchDescription = mixMatchDescription;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getFacilityAction() {
        return facilityAction;
    }

    public void setFacilityAction(String facilityAction) {
        this.facilityAction = facilityAction;
    }

    public String getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(String operationIds) {
        this.operationIds = operationIds;
    }

    public String getDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(String discountApplied) {
        this.discountApplied = discountApplied;
    }

    public String getBarcodePLU() {
        return barcodePLU;
    }

    public void setBarcodePLU(String barcodePLU) {
        this.barcodePLU = barcodePLU;
    }

    public String getBarcodeStartDate() {
        return barcodeStartDate;
    }

    public void setBarcodeStartDate(String barcodeStartDate) {
        this.barcodeStartDate = barcodeStartDate;
    }

    public String getBarcodeEndDate() {
        return barcodeEndDate;
    }

    public void setBarcodeEndDate(String barcodeEndDate) {
        this.barcodeEndDate = barcodeEndDate;
    }

    public String getTermAndCondition() {
        return termAndCondition;
    }

    public void setTermAndCondition(String termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getTicketLineDesc() {
        return ticketLineDesc;
    }

    public void setTicketLineDesc(String ticketLineDesc) {
        this.ticketLineDesc = ticketLineDesc;
    }

    public String getPackagePrintLabel() {
        return packagePrintLabel;
    }

    public void setPackagePrintLabel(String packagePrintLabel) {
        this.packagePrintLabel = packagePrintLabel;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
