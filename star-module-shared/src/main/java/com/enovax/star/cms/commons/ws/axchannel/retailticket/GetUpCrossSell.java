
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="criteria" type="{http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel}UpCrossSellTableCriteria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "criteria"
})
@XmlRootElement(name = "GetUpCrossSell")
public class GetUpCrossSell {

    @XmlElementRef(name = "criteria", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<UpCrossSellTableCriteria> criteria;

    /**
     * Gets the value of the criteria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}
     *     
     */
    public JAXBElement<UpCrossSellTableCriteria> getCriteria() {
        return criteria;
    }

    /**
     * Sets the value of the criteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}
     *     
     */
    public void setCriteria(JAXBElement<UpCrossSellTableCriteria> value) {
        this.criteria = value;
    }

}
