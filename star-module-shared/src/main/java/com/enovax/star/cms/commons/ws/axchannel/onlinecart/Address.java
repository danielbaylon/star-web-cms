
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="AddressTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AttentionTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingCompliment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="County" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Deactivate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DirectoryPartyLocationRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DirectoryPartyLocationRoleRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DirectoryPartyTableRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DistrictName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailContent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailLogisticsLocationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailLogisticsLocationRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="EmailRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ExpireRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="FullAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsPrivate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LogisticsLocationExtRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="LogisticsLocationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogisticsLocationRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="LogisticsLocationRoleRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneLogisticsLocationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneLogisticsLocationRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="PhoneRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Postbox" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StreetNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThreeLetterISORegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TwoLetterISORegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlLogisticsLocationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlLogisticsLocationRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="UrlRecordId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
    "addressTypeValue",
    "attentionTo",
    "buildingCompliment",
    "city",
    "county",
    "countyName",
    "deactivate",
    "directoryPartyLocationRecordId",
    "directoryPartyLocationRoleRecordId",
    "directoryPartyTableRecordId",
    "districtName",
    "email",
    "emailContent",
    "emailLogisticsLocationId",
    "emailLogisticsLocationRecordId",
    "emailRecordId",
    "expireRecordId",
    "fullAddress",
    "isPrimary",
    "isPrivate",
    "logisticsLocationExtRecordId",
    "logisticsLocationId",
    "logisticsLocationRecordId",
    "logisticsLocationRoleRecordId",
    "name",
    "partyNumber",
    "phone",
    "phoneExt",
    "phoneLogisticsLocationId",
    "phoneLogisticsLocationRecordId",
    "phoneRecordId",
    "postbox",
    "recordId",
    "sortOrder",
    "state",
    "stateName",
    "street",
    "streetNumber",
    "taxGroup",
    "threeLetterISORegionName",
    "twoLetterISORegionName",
    "url",
    "urlLogisticsLocationId",
    "urlLogisticsLocationRecordId",
    "urlRecordId",
    "zipCode"
})
public class Address
    extends CommerceEntity
{

    @XmlElement(name = "AddressTypeValue")
    protected Integer addressTypeValue;
    @XmlElementRef(name = "AttentionTo", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attentionTo;
    @XmlElementRef(name = "BuildingCompliment", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> buildingCompliment;
    @XmlElementRef(name = "City", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> city;
    @XmlElementRef(name = "County", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> county;
    @XmlElementRef(name = "CountyName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> countyName;
    @XmlElement(name = "Deactivate")
    protected Boolean deactivate;
    @XmlElement(name = "DirectoryPartyLocationRecordId")
    protected Long directoryPartyLocationRecordId;
    @XmlElement(name = "DirectoryPartyLocationRoleRecordId")
    protected Long directoryPartyLocationRoleRecordId;
    @XmlElement(name = "DirectoryPartyTableRecordId")
    protected Long directoryPartyTableRecordId;
    @XmlElementRef(name = "DistrictName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> districtName;
    @XmlElementRef(name = "Email", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> email;
    @XmlElementRef(name = "EmailContent", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailContent;
    @XmlElementRef(name = "EmailLogisticsLocationId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailLogisticsLocationId;
    @XmlElement(name = "EmailLogisticsLocationRecordId")
    protected Long emailLogisticsLocationRecordId;
    @XmlElement(name = "EmailRecordId")
    protected Long emailRecordId;
    @XmlElement(name = "ExpireRecordId")
    protected Long expireRecordId;
    @XmlElementRef(name = "FullAddress", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullAddress;
    @XmlElement(name = "IsPrimary")
    protected Boolean isPrimary;
    @XmlElement(name = "IsPrivate")
    protected Boolean isPrivate;
    @XmlElement(name = "LogisticsLocationExtRecordId")
    protected Long logisticsLocationExtRecordId;
    @XmlElementRef(name = "LogisticsLocationId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logisticsLocationId;
    @XmlElement(name = "LogisticsLocationRecordId")
    protected Long logisticsLocationRecordId;
    @XmlElement(name = "LogisticsLocationRoleRecordId")
    protected Long logisticsLocationRoleRecordId;
    @XmlElementRef(name = "Name", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "PartyNumber", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partyNumber;
    @XmlElementRef(name = "Phone", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phone;
    @XmlElementRef(name = "PhoneExt", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneExt;
    @XmlElementRef(name = "PhoneLogisticsLocationId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneLogisticsLocationId;
    @XmlElement(name = "PhoneLogisticsLocationRecordId")
    protected Long phoneLogisticsLocationRecordId;
    @XmlElement(name = "PhoneRecordId")
    protected Long phoneRecordId;
    @XmlElementRef(name = "Postbox", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> postbox;
    @XmlElement(name = "RecordId")
    protected Long recordId;
    @XmlElement(name = "SortOrder")
    protected Integer sortOrder;
    @XmlElementRef(name = "State", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> state;
    @XmlElementRef(name = "StateName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> stateName;
    @XmlElementRef(name = "Street", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> street;
    @XmlElementRef(name = "StreetNumber", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> streetNumber;
    @XmlElementRef(name = "TaxGroup", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxGroup;
    @XmlElementRef(name = "ThreeLetterISORegionName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> threeLetterISORegionName;
    @XmlElementRef(name = "TwoLetterISORegionName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> twoLetterISORegionName;
    @XmlElementRef(name = "Url", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> url;
    @XmlElementRef(name = "UrlLogisticsLocationId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> urlLogisticsLocationId;
    @XmlElement(name = "UrlLogisticsLocationRecordId")
    protected Long urlLogisticsLocationRecordId;
    @XmlElement(name = "UrlRecordId")
    protected Long urlRecordId;
    @XmlElementRef(name = "ZipCode", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> zipCode;

    /**
     * Gets the value of the addressTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAddressTypeValue() {
        return addressTypeValue;
    }

    /**
     * Sets the value of the addressTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAddressTypeValue(Integer value) {
        this.addressTypeValue = value;
    }

    /**
     * Gets the value of the attentionTo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttentionTo() {
        return attentionTo;
    }

    /**
     * Sets the value of the attentionTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttentionTo(JAXBElement<String> value) {
        this.attentionTo = value;
    }

    /**
     * Gets the value of the buildingCompliment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBuildingCompliment() {
        return buildingCompliment;
    }

    /**
     * Sets the value of the buildingCompliment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBuildingCompliment(JAXBElement<String> value) {
        this.buildingCompliment = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCity(JAXBElement<String> value) {
        this.city = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounty(JAXBElement<String> value) {
        this.county = value;
    }

    /**
     * Gets the value of the countyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountyName() {
        return countyName;
    }

    /**
     * Sets the value of the countyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountyName(JAXBElement<String> value) {
        this.countyName = value;
    }

    /**
     * Gets the value of the deactivate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeactivate() {
        return deactivate;
    }

    /**
     * Sets the value of the deactivate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeactivate(Boolean value) {
        this.deactivate = value;
    }

    /**
     * Gets the value of the directoryPartyLocationRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDirectoryPartyLocationRecordId() {
        return directoryPartyLocationRecordId;
    }

    /**
     * Sets the value of the directoryPartyLocationRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDirectoryPartyLocationRecordId(Long value) {
        this.directoryPartyLocationRecordId = value;
    }

    /**
     * Gets the value of the directoryPartyLocationRoleRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDirectoryPartyLocationRoleRecordId() {
        return directoryPartyLocationRoleRecordId;
    }

    /**
     * Sets the value of the directoryPartyLocationRoleRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDirectoryPartyLocationRoleRecordId(Long value) {
        this.directoryPartyLocationRoleRecordId = value;
    }

    /**
     * Gets the value of the directoryPartyTableRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDirectoryPartyTableRecordId() {
        return directoryPartyTableRecordId;
    }

    /**
     * Sets the value of the directoryPartyTableRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDirectoryPartyTableRecordId(Long value) {
        this.directoryPartyTableRecordId = value;
    }

    /**
     * Gets the value of the districtName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDistrictName() {
        return districtName;
    }

    /**
     * Sets the value of the districtName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDistrictName(JAXBElement<String> value) {
        this.districtName = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmail(JAXBElement<String> value) {
        this.email = value;
    }

    /**
     * Gets the value of the emailContent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailContent() {
        return emailContent;
    }

    /**
     * Sets the value of the emailContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailContent(JAXBElement<String> value) {
        this.emailContent = value;
    }

    /**
     * Gets the value of the emailLogisticsLocationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailLogisticsLocationId() {
        return emailLogisticsLocationId;
    }

    /**
     * Sets the value of the emailLogisticsLocationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailLogisticsLocationId(JAXBElement<String> value) {
        this.emailLogisticsLocationId = value;
    }

    /**
     * Gets the value of the emailLogisticsLocationRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEmailLogisticsLocationRecordId() {
        return emailLogisticsLocationRecordId;
    }

    /**
     * Sets the value of the emailLogisticsLocationRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEmailLogisticsLocationRecordId(Long value) {
        this.emailLogisticsLocationRecordId = value;
    }

    /**
     * Gets the value of the emailRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEmailRecordId() {
        return emailRecordId;
    }

    /**
     * Sets the value of the emailRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEmailRecordId(Long value) {
        this.emailRecordId = value;
    }

    /**
     * Gets the value of the expireRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExpireRecordId() {
        return expireRecordId;
    }

    /**
     * Sets the value of the expireRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExpireRecordId(Long value) {
        this.expireRecordId = value;
    }

    /**
     * Gets the value of the fullAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullAddress() {
        return fullAddress;
    }

    /**
     * Sets the value of the fullAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullAddress(JAXBElement<String> value) {
        this.fullAddress = value;
    }

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPrimary(Boolean value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the isPrivate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPrivate() {
        return isPrivate;
    }

    /**
     * Sets the value of the isPrivate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPrivate(Boolean value) {
        this.isPrivate = value;
    }

    /**
     * Gets the value of the logisticsLocationExtRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLogisticsLocationExtRecordId() {
        return logisticsLocationExtRecordId;
    }

    /**
     * Sets the value of the logisticsLocationExtRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLogisticsLocationExtRecordId(Long value) {
        this.logisticsLocationExtRecordId = value;
    }

    /**
     * Gets the value of the logisticsLocationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogisticsLocationId() {
        return logisticsLocationId;
    }

    /**
     * Sets the value of the logisticsLocationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogisticsLocationId(JAXBElement<String> value) {
        this.logisticsLocationId = value;
    }

    /**
     * Gets the value of the logisticsLocationRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLogisticsLocationRecordId() {
        return logisticsLocationRecordId;
    }

    /**
     * Sets the value of the logisticsLocationRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLogisticsLocationRecordId(Long value) {
        this.logisticsLocationRecordId = value;
    }

    /**
     * Gets the value of the logisticsLocationRoleRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLogisticsLocationRoleRecordId() {
        return logisticsLocationRoleRecordId;
    }

    /**
     * Sets the value of the logisticsLocationRoleRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLogisticsLocationRoleRecordId(Long value) {
        this.logisticsLocationRoleRecordId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the partyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartyNumber() {
        return partyNumber;
    }

    /**
     * Sets the value of the partyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartyNumber(JAXBElement<String> value) {
        this.partyNumber = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhone(JAXBElement<String> value) {
        this.phone = value;
    }

    /**
     * Gets the value of the phoneExt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneExt() {
        return phoneExt;
    }

    /**
     * Sets the value of the phoneExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneExt(JAXBElement<String> value) {
        this.phoneExt = value;
    }

    /**
     * Gets the value of the phoneLogisticsLocationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneLogisticsLocationId() {
        return phoneLogisticsLocationId;
    }

    /**
     * Sets the value of the phoneLogisticsLocationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneLogisticsLocationId(JAXBElement<String> value) {
        this.phoneLogisticsLocationId = value;
    }

    /**
     * Gets the value of the phoneLogisticsLocationRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPhoneLogisticsLocationRecordId() {
        return phoneLogisticsLocationRecordId;
    }

    /**
     * Sets the value of the phoneLogisticsLocationRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPhoneLogisticsLocationRecordId(Long value) {
        this.phoneLogisticsLocationRecordId = value;
    }

    /**
     * Gets the value of the phoneRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPhoneRecordId() {
        return phoneRecordId;
    }

    /**
     * Sets the value of the phoneRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPhoneRecordId(Long value) {
        this.phoneRecordId = value;
    }

    /**
     * Gets the value of the postbox property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPostbox() {
        return postbox;
    }

    /**
     * Sets the value of the postbox property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPostbox(JAXBElement<String> value) {
        this.postbox = value;
    }

    /**
     * Gets the value of the recordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecordId() {
        return recordId;
    }

    /**
     * Sets the value of the recordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecordId(Long value) {
        this.recordId = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSortOrder(Integer value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setState(JAXBElement<String> value) {
        this.state = value;
    }

    /**
     * Gets the value of the stateName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStateName() {
        return stateName;
    }

    /**
     * Sets the value of the stateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStateName(JAXBElement<String> value) {
        this.stateName = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStreet(JAXBElement<String> value) {
        this.street = value;
    }

    /**
     * Gets the value of the streetNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStreetNumber() {
        return streetNumber;
    }

    /**
     * Sets the value of the streetNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStreetNumber(JAXBElement<String> value) {
        this.streetNumber = value;
    }

    /**
     * Gets the value of the taxGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxGroup() {
        return taxGroup;
    }

    /**
     * Sets the value of the taxGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxGroup(JAXBElement<String> value) {
        this.taxGroup = value;
    }

    /**
     * Gets the value of the threeLetterISORegionName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThreeLetterISORegionName() {
        return threeLetterISORegionName;
    }

    /**
     * Sets the value of the threeLetterISORegionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThreeLetterISORegionName(JAXBElement<String> value) {
        this.threeLetterISORegionName = value;
    }

    /**
     * Gets the value of the twoLetterISORegionName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTwoLetterISORegionName() {
        return twoLetterISORegionName;
    }

    /**
     * Sets the value of the twoLetterISORegionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTwoLetterISORegionName(JAXBElement<String> value) {
        this.twoLetterISORegionName = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUrl(JAXBElement<String> value) {
        this.url = value;
    }

    /**
     * Gets the value of the urlLogisticsLocationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUrlLogisticsLocationId() {
        return urlLogisticsLocationId;
    }

    /**
     * Sets the value of the urlLogisticsLocationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUrlLogisticsLocationId(JAXBElement<String> value) {
        this.urlLogisticsLocationId = value;
    }

    /**
     * Gets the value of the urlLogisticsLocationRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUrlLogisticsLocationRecordId() {
        return urlLogisticsLocationRecordId;
    }

    /**
     * Sets the value of the urlLogisticsLocationRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUrlLogisticsLocationRecordId(Long value) {
        this.urlLogisticsLocationRecordId = value;
    }

    /**
     * Gets the value of the urlRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUrlRecordId() {
        return urlRecordId;
    }

    /**
     * Sets the value of the urlRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUrlRecordId(Long value) {
        this.urlRecordId = value;
    }

    /**
     * Gets the value of the zipCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getZipCode() {
        return zipCode;
    }

    /**
     * Sets the value of the zipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setZipCode(JAXBElement<String> value) {
        this.zipCode = value;
    }

}
