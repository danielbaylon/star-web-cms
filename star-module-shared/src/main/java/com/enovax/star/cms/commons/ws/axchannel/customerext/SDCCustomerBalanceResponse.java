
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_CustomerBalanceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CustomerBalanceResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="customerBalanceEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_CustomerBalance" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CustomerBalanceResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "customerBalanceEntity"
})
public class SDCCustomerBalanceResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "customerBalanceEntity", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCustomerBalance> customerBalanceEntity;

    /**
     * Gets the value of the customerBalanceEntity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerBalance }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCustomerBalance> getCustomerBalanceEntity() {
        return customerBalanceEntity;
    }

    /**
     * Sets the value of the customerBalanceEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerBalance }{@code >}
     *     
     */
    public void setCustomerBalanceEntity(JAXBElement<ArrayOfSDCCustomerBalance> value) {
        this.customerBalanceEntity = value;
    }

}
