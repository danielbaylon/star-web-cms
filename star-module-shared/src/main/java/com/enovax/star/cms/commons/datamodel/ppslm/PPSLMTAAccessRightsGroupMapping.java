package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPSLMTAAccessRightsGroupMapping")
public class PPSLMTAAccessRightsGroupMapping implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	
	@ManyToOne
	@JoinColumn(name = "accessRightId",nullable = false)
	private PPSLMTAAccessRight accessRight;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPSLMTAAccessRightsGroup accessRightsGroup;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PPSLMTAAccessRight getAccessRight() {
		return accessRight;
	}

	public void setAccessRight(PPSLMTAAccessRight accessRight) {
		this.accessRight = accessRight;
	}

	public PPSLMTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}

	public void setAccessRightsGroup(PPSLMTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}
	
	

}
