package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class AxDiscountLine {
	
	@JsonProperty("SaleLineNumber")
	private String saleLineNumber;
	@JsonProperty("OfferId")
	private String offerId;
	@JsonProperty("OfferName")
	private String offerName;
	@JsonProperty("Amount")
	private String amount;
	@JsonProperty("EffectiveAmount")
	private String effectiveAmount;
	@JsonProperty("Percentage")
	private String percentage;
	@JsonProperty("DealPrice")
	private String dealPrice;
	@JsonProperty("DiscountLineTypeValue")
	private Integer discountLineTypeValue;
	@JsonProperty("ManualDiscountTypeValue")
	private Integer manualDiscountTypeValue;
	@JsonProperty("CustomerDiscountTypeValue")
	private Integer customerDiscountTypeValue;
	@JsonProperty("PeriodicDiscountTypeValue")
	private Integer periodicDiscountTypeValue;
	@JsonProperty("DiscountApplicationGroup")
	private Object discountApplicationGroup;
	@JsonProperty("ConcurrencyModeValue")
	private Integer concurrencyModeValue;
	@JsonProperty("IsCompoundable")
	private Boolean isCompoundable;
	@JsonProperty("DiscountCode")
	private String discountCode;
	@JsonProperty("PricingPriorityNumber")
	private Integer pricingPriorityNumber;
	@JsonProperty("IsDiscountCodeRequired")
	private Boolean isDiscountCodeRequired;
	@JsonProperty("ThresholdAmountRequired")
	private String thresholdAmountRequired;
	@JsonProperty("ExtensionProperties")
	private List<Object> extensionProperties = new ArrayList<Object>();

	/**
	 * 
	 * @return The saleLineNumber
	 */
	@JsonProperty("SaleLineNumber")
	public String getSaleLineNumber() {
		return saleLineNumber;
	}

	/**
	 * 
	 * @param saleLineNumber
	 *            The SaleLineNumber
	 */
	@JsonProperty("SaleLineNumber")
	public void setSaleLineNumber(String saleLineNumber) {
		this.saleLineNumber = saleLineNumber;
	}

	/**
	 * 
	 * @return The offerId
	 */
	@JsonProperty("OfferId")
	public String getOfferId() {
		return offerId;
	}

	/**
	 * 
	 * @param offerId
	 *            The OfferId
	 */
	@JsonProperty("OfferId")
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	/**
	 * 
	 * @return The offerName
	 */
	@JsonProperty("OfferName")
	public String getOfferName() {
		return offerName;
	}

	/**
	 * 
	 * @param offerName
	 *            The OfferName
	 */
	@JsonProperty("OfferName")
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	/**
	 * 
	 * @return The amount
	 */
	@JsonProperty("Amount")
	public String getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 *            The Amount
	 */
	@JsonProperty("Amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return The effectiveAmount
	 */
	@JsonProperty("EffectiveAmount")
	public String getEffectiveAmount() {
		return effectiveAmount;
	}

	/**
	 * 
	 * @param effectiveAmount
	 *            The EffectiveAmount
	 */
	@JsonProperty("EffectiveAmount")
	public void setEffectiveAmount(String effectiveAmount) {
		this.effectiveAmount = effectiveAmount;
	}

	/**
	 * 
	 * @return The percentage
	 */
	@JsonProperty("Percentage")
	public String getPercentage() {
		return percentage;
	}

	/**
	 * 
	 * @param percentage
	 *            The Percentage
	 */
	@JsonProperty("Percentage")
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	/**
	 * 
	 * @return The dealPrice
	 */
	@JsonProperty("DealPrice")
	public String getDealPrice() {
		return dealPrice;
	}

	/**
	 * 
	 * @param dealPrice
	 *            The DealPrice
	 */
	@JsonProperty("DealPrice")
	public void setDealPrice(String dealPrice) {
		this.dealPrice = dealPrice;
	}

	/**
	 * 
	 * @return The discountLineTypeValue
	 */
	@JsonProperty("DiscountLineTypeValue")
	public Integer getDiscountLineTypeValue() {
		return discountLineTypeValue;
	}

	/**
	 * 
	 * @param discountLineTypeValue
	 *            The DiscountLineTypeValue
	 */
	@JsonProperty("DiscountLineTypeValue")
	public void setDiscountLineTypeValue(Integer discountLineTypeValue) {
		this.discountLineTypeValue = discountLineTypeValue;
	}

	/**
	 * 
	 * @return The manualDiscountTypeValue
	 */
	@JsonProperty("ManualDiscountTypeValue")
	public Integer getManualDiscountTypeValue() {
		return manualDiscountTypeValue;
	}

	/**
	 * 
	 * @param manualDiscountTypeValue
	 *            The ManualDiscountTypeValue
	 */
	@JsonProperty("ManualDiscountTypeValue")
	public void setManualDiscountTypeValue(Integer manualDiscountTypeValue) {
		this.manualDiscountTypeValue = manualDiscountTypeValue;
	}

	/**
	 * 
	 * @return The customerDiscountTypeValue
	 */
	@JsonProperty("CustomerDiscountTypeValue")
	public Integer getCustomerDiscountTypeValue() {
		return customerDiscountTypeValue;
	}

	/**
	 * 
	 * @param customerDiscountTypeValue
	 *            The CustomerDiscountTypeValue
	 */
	@JsonProperty("CustomerDiscountTypeValue")
	public void setCustomerDiscountTypeValue(Integer customerDiscountTypeValue) {
		this.customerDiscountTypeValue = customerDiscountTypeValue;
	}

	/**
	 * 
	 * @return The periodicDiscountTypeValue
	 */
	@JsonProperty("PeriodicDiscountTypeValue")
	public Integer getPeriodicDiscountTypeValue() {
		return periodicDiscountTypeValue;
	}

	/**
	 * 
	 * @param periodicDiscountTypeValue
	 *            The PeriodicDiscountTypeValue
	 */
	@JsonProperty("PeriodicDiscountTypeValue")
	public void setPeriodicDiscountTypeValue(Integer periodicDiscountTypeValue) {
		this.periodicDiscountTypeValue = periodicDiscountTypeValue;
	}

	/**
	 * 
	 * @return The discountApplicationGroup
	 */
	@JsonProperty("DiscountApplicationGroup")
	public Object getDiscountApplicationGroup() {
		return discountApplicationGroup;
	}

	/**
	 * 
	 * @param discountApplicationGroup
	 *            The DiscountApplicationGroup
	 */
	@JsonProperty("DiscountApplicationGroup")
	public void setDiscountApplicationGroup(Object discountApplicationGroup) {
		this.discountApplicationGroup = discountApplicationGroup;
	}

	/**
	 * 
	 * @return The concurrencyModeValue
	 */
	@JsonProperty("ConcurrencyModeValue")
	public Integer getConcurrencyModeValue() {
		return concurrencyModeValue;
	}

	/**
	 * 
	 * @param concurrencyModeValue
	 *            The ConcurrencyModeValue
	 */
	@JsonProperty("ConcurrencyModeValue")
	public void setConcurrencyModeValue(Integer concurrencyModeValue) {
		this.concurrencyModeValue = concurrencyModeValue;
	}

	/**
	 * 
	 * @return The isCompoundable
	 */
	@JsonProperty("IsCompoundable")
	public Boolean getIsCompoundable() {
		return isCompoundable;
	}

	/**
	 * 
	 * @param isCompoundable
	 *            The IsCompoundable
	 */
	@JsonProperty("IsCompoundable")
	public void setIsCompoundable(Boolean isCompoundable) {
		this.isCompoundable = isCompoundable;
	}

	/**
	 * 
	 * @return The discountCode
	 */
	@JsonProperty("DiscountCode")
	public String getDiscountCode() {
		return discountCode;
	}

	/**
	 * 
	 * @param discountCode
	 *            The DiscountCode
	 */
	@JsonProperty("DiscountCode")
	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	/**
	 * 
	 * @return The pricingPriorityNumber
	 */
	@JsonProperty("PricingPriorityNumber")
	public Integer getPricingPriorityNumber() {
		return pricingPriorityNumber;
	}

	/**
	 * 
	 * @param pricingPriorityNumber
	 *            The PricingPriorityNumber
	 */
	@JsonProperty("PricingPriorityNumber")
	public void setPricingPriorityNumber(Integer pricingPriorityNumber) {
		this.pricingPriorityNumber = pricingPriorityNumber;
	}

	/**
	 * 
	 * @return The isDiscountCodeRequired
	 */
	@JsonProperty("IsDiscountCodeRequired")
	public Boolean getIsDiscountCodeRequired() {
		return isDiscountCodeRequired;
	}

	/**
	 * 
	 * @param isDiscountCodeRequired
	 *            The IsDiscountCodeRequired
	 */
	@JsonProperty("IsDiscountCodeRequired")
	public void setIsDiscountCodeRequired(Boolean isDiscountCodeRequired) {
		this.isDiscountCodeRequired = isDiscountCodeRequired;
	}

	/**
	 * 
	 * @return The thresholdAmountRequired
	 */
	@JsonProperty("ThresholdAmountRequired")
	public String getThresholdAmountRequired() {
		return thresholdAmountRequired;
	}

	/**
	 * 
	 * @param thresholdAmountRequired
	 *            The ThresholdAmountRequired
	 */
	@JsonProperty("ThresholdAmountRequired")
	public void setThresholdAmountRequired(String thresholdAmountRequired) {
		this.thresholdAmountRequired = thresholdAmountRequired;
	}

	/**
	 * 
	 * @return The extensionProperties
	 */
	@JsonProperty("ExtensionProperties")
	public List<Object> getExtensionProperties() {
		return extensionProperties;
	}

	/**
	 * 
	 * @param extensionProperties
	 *            The ExtensionProperties
	 */
	@JsonProperty("ExtensionProperties")
	public void setExtensionProperties(List<Object> extensionProperties) {
		this.extensionProperties = extensionProperties;
	}

}
