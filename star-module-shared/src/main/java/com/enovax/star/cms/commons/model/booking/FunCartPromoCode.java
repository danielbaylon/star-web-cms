package com.enovax.star.cms.commons.model.booking;

/**
 * Created by jensen on 26/9/16.
 */
public class FunCartPromoCode {

    private String promoCode;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}
