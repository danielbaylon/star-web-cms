package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxTransactionSearchCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskSearchJournalTransactionsRequest {

	@JsonProperty("searchCriteria")
	private AxTransactionSearchCriteria axTransactionSearchCriteria;

	public void setAxTransactionSearchCriteria(AxTransactionSearchCriteria axTransactionSearchCriteria) {
		this.axTransactionSearchCriteria = axTransactionSearchCriteria;
	}
	
	public AxTransactionSearchCriteria getAxTransactionSearchCriteria() {
		return axTransactionSearchCriteria;
	}
}
