package com.enovax.star.cms.commons.constant;

/**
 * Created by jennylynsze on 11/23/16.
 */
public class AppConfigConstants {

    //For load balancing test~
    public static final String GENERAL_APP_KEY = "general";
    public static final String ENABLE_TM_SERVICE_KEY = "enableTMService";
    public static final String ENABLE_MAIL_SERVICE_KEY = "enableMailService";

}
