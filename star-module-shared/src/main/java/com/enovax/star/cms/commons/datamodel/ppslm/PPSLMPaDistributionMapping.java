package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;

@Entity
@Table(name = "PPSLMPaDistributionMapping")
public class PPSLMPaDistributionMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name="percentage", nullable=false)
    private Integer percentage;
    
    @Column(name="adminId", nullable=false)
    private String adminId;

    @Column(name="partnerId", nullable=false)
    private Integer partnerId;
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="partnerId",insertable = false, updatable = false)
    private PPSLMPartner partner;
    
    @Column(name="countryId", nullable=false)
    private String countryId;

    @Transient
    private boolean found;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public PPSLMPartner getPartner() {
        return partner;
    }

    public void setPartner(PPSLMPartner partner) {
        this.partner = partner;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }
}
