package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccount;

/**
 * Created by lavanya on 2/9/16.
 */
public class UserResult {
    public static enum Result {
        Success,
        Failed,
        Last5Passwords,
        ExistingUser,
        ExceedSubAcc
    }

    public final Result result;
    public final PPSLMTAAccount user;

    public UserResult(Result result, PPSLMTAAccount user) {
        this.result = result;
        this.user = user;
    }
}
