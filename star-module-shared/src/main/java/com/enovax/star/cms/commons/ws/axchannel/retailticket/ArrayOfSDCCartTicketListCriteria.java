
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_CartTicketListCriteria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_CartTicketListCriteria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_CartTicketListCriteria" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_CartTicketListCriteria" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_CartTicketListCriteria", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcCartTicketListCriteria"
})
public class ArrayOfSDCCartTicketListCriteria {

    @XmlElement(name = "SDC_CartTicketListCriteria", nillable = true)
    protected List<SDCCartTicketListCriteria> sdcCartTicketListCriteria;

    /**
     * Gets the value of the sdcCartTicketListCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcCartTicketListCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCCartTicketListCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCCartTicketListCriteria }
     * 
     * 
     */
    public List<SDCCartTicketListCriteria> getSDCCartTicketListCriteria() {
        if (sdcCartTicketListCriteria == null) {
            sdcCartTicketListCriteria = new ArrayList<SDCCartTicketListCriteria>();
        }
        return this.sdcCartTicketListCriteria;
    }

}
