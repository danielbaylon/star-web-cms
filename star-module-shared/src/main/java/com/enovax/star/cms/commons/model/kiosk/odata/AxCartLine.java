package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.CartLine
 * 
 * @author Justin
 *
 */
public class AxCartLine {

    @JsonProperty("Barcode")
    private String barcode;

    @JsonProperty("Comment")
    private String comment;

    @JsonProperty("DeliveryMode")
    private String deliveryMode;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("DiscountAmount")
    private String discountAmount;
    
    //TODO DiscountLines : {result: [{}]} as request for void

    @JsonIgnore
    private List<AxDiscountLine> discountLineList = new ArrayList<>();

    @JsonProperty("ExtendedPrice")
    private String extendedPrice;

    @JsonProperty("InventoryDimensionId")
    private String inventoryDimensionId;

    @JsonProperty("InvoiceAmount")
    private String invoiceAmount;

    @JsonProperty("InvoiceId")
    private String invoiceId;

    @JsonProperty("IsCustomerAccountDeposit")
    private String isCustomerAccountDeposit = Boolean.FALSE.toString();

    @JsonProperty("IsGiftCardLine")
    private String isGiftCardLine = Boolean.FALSE.toString();

    @JsonProperty("IsInvoiceLine")
    private String isInvoiceLine = Boolean.FALSE.toString();

    @JsonProperty("IsPriceOverridden")
    private String isPriceOverridden = Boolean.FALSE.toString();

    @JsonProperty("IsVoided")
    private String isVoided = Boolean.FALSE.toString();;

    @JsonProperty("ItemId")
    private String itemId;

    @JsonProperty("ItemTaxGroupId")
    private String itemTaxGroupId;

    @JsonProperty("LineDiscount")
    private String lineDiscount;

    @JsonProperty("LineId")
    private String lineId;

    @JsonProperty("LineManualDiscountAmount")
    private String lineManualDiscountAmount;

    @JsonProperty("LineManualDiscountPercentage")
    private String lineManualDiscountPercentage;

    @JsonProperty("LinePercentageDiscount")
    private String linePercentageDiscount;

    @JsonProperty("NetAmountWithoutTax")
    private String netAmountWithoutTax;

    @JsonProperty("Price")
    private String price;

    @JsonProperty("ProductId")
    private String productId;

    @JsonProperty("Quantity")
    private String quantity;

    @JsonProperty("ReturnInventTransId")
    private String returnInventTransId;

    @JsonProperty("ReturnLineNumber")
    private String returnLineNumber;

    @JsonProperty("ReturnTransactionId")
    private String returnTransactionId;

    @JsonProperty("SalesStatusValue")
    private String salesStatusValue;

    @JsonProperty("SerialNumber")
    private String serialNumber;

    @JsonProperty("ShippingAddress")
    private String shippingAddress = "";

    @JsonProperty("StoreNumber")
    private String storeNumber;

    @JsonProperty("TaxAmount")
    private String taxAmount;

    @JsonProperty("TaxOverrideCode")
    private String taxOverrideCode = "";

    @JsonProperty("TaxRatePercent")
    private String taxRatePercent;

    @JsonProperty("TotalAmount")
    private String totalAmount;

    @JsonProperty("UnitOfMeasureSymbol")
    private String unitOfMeasureSymbol;

    @JsonProperty("WarehouseId")
    private String warehouseId;

    // private String ShippingAddress"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.Address"/>

    @JsonIgnore
    private String deliveryModeChargeAmount;

    @JsonIgnore
    private String requestedDeliveryDate;

    @JsonIgnore
    private String quantityOrdered;
    @JsonIgnore
    private String quantityInvoiced;

    @JsonIgnore
    private String originalPrice;

    // private String PromotionLines" Type="Collection(Edm.String)"/>
    // private String DiscountLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.DiscountLine)"
    // Nullable="false"/>
    // private String ReasonCodeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ReasonCodeLine)"
    // Nullable="false"/>
    // private String ChargeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ChargeLine)"

    // private String ExtensionProperties"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
    // Nullable="false"/>
    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(String extendedPrice) {
        this.extendedPrice = extendedPrice;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getItemTaxGroupId() {
        return itemTaxGroupId;
    }

    public void setItemTaxGroupId(String itemTaxGroupId) {
        this.itemTaxGroupId = itemTaxGroupId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNetAmountWithoutTax() {
        return netAmountWithoutTax;
    }

    public void setNetAmountWithoutTax(String netAmountWithoutTax) {
        this.netAmountWithoutTax = netAmountWithoutTax;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(String lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public String getLinePercentageDiscount() {
        return linePercentageDiscount;
    }

    public void setLinePercentageDiscount(String linePercentageDiscount) {
        this.linePercentageDiscount = linePercentageDiscount;
    }

    public String getLineManualDiscountPercentage() {
        return lineManualDiscountPercentage;
    }

    public void setLineManualDiscountPercentage(String lineManualDiscountPercentage) {
        this.lineManualDiscountPercentage = lineManualDiscountPercentage;
    }

    public String getLineManualDiscountAmount() {
        return lineManualDiscountAmount;
    }

    public void setLineManualDiscountAmount(String lineManualDiscountAmount) {
        this.lineManualDiscountAmount = lineManualDiscountAmount;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(String deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getReturnTransactionId() {
        return returnTransactionId;
    }

    public void setReturnTransactionId(String returnTransactionId) {
        this.returnTransactionId = returnTransactionId;
    }

    public String getReturnLineNumber() {
        return returnLineNumber;
    }

    public void setReturnLineNumber(String returnLineNumber) {
        this.returnLineNumber = returnLineNumber;
    }

    public String getReturnInventTransId() {
        return returnInventTransId;
    }

    public void setReturnInventTransId(String returnInventTransId) {
        this.returnInventTransId = returnInventTransId;
    }

    public String getIsVoided() {
        return isVoided;
    }

    public void setIsVoided(String isVoided) {
        this.isVoided = isVoided;
    }

    public String getIsGiftCardLine() {
        return isGiftCardLine;
    }

    public void setIsGiftCardLine(String isGiftCardLine) {
        this.isGiftCardLine = isGiftCardLine;
    }

    public String getSalesStatusValue() {
        return salesStatusValue;
    }

    public void setSalesStatusValue(String salesStatusValue) {
        this.salesStatusValue = salesStatusValue;
    }

    public String getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(String quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public String getQuantityInvoiced() {
        return quantityInvoiced;
    }

    public void setQuantityInvoiced(String quantityInvoiced) {
        this.quantityInvoiced = quantityInvoiced;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getIsPriceOverridden() {
        return isPriceOverridden;
    }

    public void setIsPriceOverridden(String isPriceOverridden) {
        this.isPriceOverridden = isPriceOverridden;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getIsInvoiceLine() {
        return isInvoiceLine;
    }

    public void setIsInvoiceLine(String isInvoiceLine) {
        this.isInvoiceLine = isInvoiceLine;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getTaxRatePercent() {
        return taxRatePercent;
    }

    public void setTaxRatePercent(String taxRatePercent) {
        this.taxRatePercent = taxRatePercent;
    }

    public String getIsCustomerAccountDeposit() {
        return isCustomerAccountDeposit;
    }

    public void setIsCustomerAccountDeposit(String isCustomerAccountDeposit) {
        this.isCustomerAccountDeposit = isCustomerAccountDeposit;
    }

    public List<AxDiscountLine> getDiscountLineList() {
        return discountLineList;
    }

    public void setDiscountLineList(List<AxDiscountLine> discountLineList) {
        this.discountLineList = discountLineList;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
