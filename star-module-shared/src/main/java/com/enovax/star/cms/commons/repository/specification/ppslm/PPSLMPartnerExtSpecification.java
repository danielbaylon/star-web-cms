package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 8/9/16.
 */
public class PPSLMPartnerExtSpecification extends BaseSpecification<PPSLMPartnerExt> {
}
