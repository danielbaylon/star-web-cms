package com.enovax.star.cms.commons.model.kiosk.odata;

public class AxShift {
    
    private String shiftId;
    private String terminalId;
    private String startingAmountTotal;
    private String floatingEntryAmountTotal;
    private String addToTenderAmountTotal;
    private String safeDropTotal;
    private String bankDropTotal;
    private String removeTenderAmountTotal;
    private String declareTenderAmountTotal;
    private String overShortTotal;
    private String tenderedTotal;
    private String changeTotal;
    private String incomeAccountTotal;
    private String expenseAccountTotal;
    private String cashDrawer;
    private String storeRecordId;
    private String storeId;
    private String staffId;
    private String currentStaffId;
    private String statusValue;
    private String startDateTime;
    private String closeDateTime;
    private String closedAtTerminalId;
    private String currentTerminalId;
    private String salesTotal;
    private String statusDateTime;
    private String returnsTotal;
    private String paidToAccountTotal;
    private String taxTotal;
    private String discountTotal;
    private String roundedAmountTotal;
    private String customerCount;
    private String saleTransactionCount;
    private String noSaleTransactionCount;
    private String voidTransactionCount;
    private String logOnTransactionCount;
    private String suspendedTransactionCount;
    private String transactionCount;
    // private List<AxShiftTenderLine> tenderLines;

    // private List<AxShiftAccountLine> accountLines;

    private String isShared;
    // private List<CommerceProperty> extensionProperties;

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getStartingAmountTotal() {
        return startingAmountTotal;
    }

    public void setStartingAmountTotal(String startingAmountTotal) {
        this.startingAmountTotal = startingAmountTotal;
    }

    public String getFloatingEntryAmountTotal() {
        return floatingEntryAmountTotal;
    }

    public void setFloatingEntryAmountTotal(String floatingEntryAmountTotal) {
        this.floatingEntryAmountTotal = floatingEntryAmountTotal;
    }

    public String getAddToTenderAmountTotal() {
        return addToTenderAmountTotal;
    }

    public void setAddToTenderAmountTotal(String addToTenderAmountTotal) {
        this.addToTenderAmountTotal = addToTenderAmountTotal;
    }

    public String getSafeDropTotal() {
        return safeDropTotal;
    }

    public void setSafeDropTotal(String safeDropTotal) {
        this.safeDropTotal = safeDropTotal;
    }

    public String getBankDropTotal() {
        return bankDropTotal;
    }

    public void setBankDropTotal(String bankDropTotal) {
        this.bankDropTotal = bankDropTotal;
    }

    public String getRemoveTenderAmountTotal() {
        return removeTenderAmountTotal;
    }

    public void setRemoveTenderAmountTotal(String removeTenderAmountTotal) {
        this.removeTenderAmountTotal = removeTenderAmountTotal;
    }

    public String getDeclareTenderAmountTotal() {
        return declareTenderAmountTotal;
    }

    public void setDeclareTenderAmountTotal(String declareTenderAmountTotal) {
        this.declareTenderAmountTotal = declareTenderAmountTotal;
    }

    public String getOverShortTotal() {
        return overShortTotal;
    }

    public void setOverShortTotal(String overShortTotal) {
        this.overShortTotal = overShortTotal;
    }

    public String getTenderedTotal() {
        return tenderedTotal;
    }

    public void setTenderedTotal(String tenderedTotal) {
        this.tenderedTotal = tenderedTotal;
    }

    public String getChangeTotal() {
        return changeTotal;
    }

    public void setChangeTotal(String changeTotal) {
        this.changeTotal = changeTotal;
    }

    public String getIncomeAccountTotal() {
        return incomeAccountTotal;
    }

    public void setIncomeAccountTotal(String incomeAccountTotal) {
        this.incomeAccountTotal = incomeAccountTotal;
    }

    public String getExpenseAccountTotal() {
        return expenseAccountTotal;
    }

    public void setExpenseAccountTotal(String expenseAccountTotal) {
        this.expenseAccountTotal = expenseAccountTotal;
    }

    public String getCashDrawer() {
        return cashDrawer;
    }

    public void setCashDrawer(String cashDrawer) {
        this.cashDrawer = cashDrawer;
    }

    public String getStoreRecordId() {
        return storeRecordId;
    }

    public void setStoreRecordId(String storeRecordId) {
        this.storeRecordId = storeRecordId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getCurrentStaffId() {
        return currentStaffId;
    }

    public void setCurrentStaffId(String currentStaffId) {
        this.currentStaffId = currentStaffId;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getCloseDateTime() {
        return closeDateTime;
    }

    public void setCloseDateTime(String closeDateTime) {
        this.closeDateTime = closeDateTime;
    }

    public String getClosedAtTerminalId() {
        return closedAtTerminalId;
    }

    public void setClosedAtTerminalId(String closedAtTerminalId) {
        this.closedAtTerminalId = closedAtTerminalId;
    }

    public String getCurrentTerminalId() {
        return currentTerminalId;
    }

    public void setCurrentTerminalId(String currentTerminalId) {
        this.currentTerminalId = currentTerminalId;
    }

    public String getSalesTotal() {
        return salesTotal;
    }

    public void setSalesTotal(String salesTotal) {
        this.salesTotal = salesTotal;
    }

    public String getStatusDateTime() {
        return statusDateTime;
    }

    public void setStatusDateTime(String statusDateTime) {
        this.statusDateTime = statusDateTime;
    }

    public String getReturnsTotal() {
        return returnsTotal;
    }

    public void setReturnsTotal(String returnsTotal) {
        this.returnsTotal = returnsTotal;
    }

    public String getPaidToAccountTotal() {
        return paidToAccountTotal;
    }

    public void setPaidToAccountTotal(String paidToAccountTotal) {
        this.paidToAccountTotal = paidToAccountTotal;
    }

    public String getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(String taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(String discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getRoundedAmountTotal() {
        return roundedAmountTotal;
    }

    public void setRoundedAmountTotal(String roundedAmountTotal) {
        this.roundedAmountTotal = roundedAmountTotal;
    }

    public String getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(String customerCount) {
        this.customerCount = customerCount;
    }

    public String getSaleTransactionCount() {
        return saleTransactionCount;
    }

    public void setSaleTransactionCount(String saleTransactionCount) {
        this.saleTransactionCount = saleTransactionCount;
    }

    public String getNoSaleTransactionCount() {
        return noSaleTransactionCount;
    }

    public void setNoSaleTransactionCount(String noSaleTransactionCount) {
        this.noSaleTransactionCount = noSaleTransactionCount;
    }

    public String getVoidTransactionCount() {
        return voidTransactionCount;
    }

    public void setVoidTransactionCount(String voidTransactionCount) {
        this.voidTransactionCount = voidTransactionCount;
    }

    public String getLogOnTransactionCount() {
        return logOnTransactionCount;
    }

    public void setLogOnTransactionCount(String logOnTransactionCount) {
        this.logOnTransactionCount = logOnTransactionCount;
    }

    public String getSuspendedTransactionCount() {
        return suspendedTransactionCount;
    }

    public void setSuspendedTransactionCount(String suspendedTransactionCount) {
        this.suspendedTransactionCount = suspendedTransactionCount;
    }

    public String getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(String transactionCount) {
        this.transactionCount = transactionCount;
    }

    public String getIsShared() {
        return isShared;
    }

    public void setIsShared(String isShared) {
        this.isShared = isShared;
    }

}
