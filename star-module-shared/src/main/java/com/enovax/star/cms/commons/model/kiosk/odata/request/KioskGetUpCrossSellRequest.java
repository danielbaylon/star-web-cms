package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellTableCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 6 OCT 16
 */
public class KioskGetUpCrossSellRequest {

    @JsonProperty("upCrossSellTableCriteria")
    private UpCrossSellTableCriteria upCrossSellTableCriteria = new UpCrossSellTableCriteria();

    public UpCrossSellTableCriteria getUpCrossSellTableCriteria() {
        return upCrossSellTableCriteria;
    }

    public void setUpCrossSellTableCriteria(UpCrossSellTableCriteria upCrossSellTableCriteria) {
        this.upCrossSellTableCriteria = upCrossSellTableCriteria;
    }

}
