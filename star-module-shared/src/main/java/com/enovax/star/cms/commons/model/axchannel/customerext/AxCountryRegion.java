package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by jennylynsze on 12/27/16.
 */
public class AxCountryRegion {
    private String countryId;
    private String countryName;
    private String regionId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}
