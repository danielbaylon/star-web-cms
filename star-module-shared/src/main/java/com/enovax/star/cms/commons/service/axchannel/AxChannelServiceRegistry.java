package com.enovax.star.cms.commons.service.axchannel;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class AxChannelServiceRegistry {

    //TODO Grab URLs from Magnolia
    //TODO Kiosk to be separated... if using ODATA

    private Logger log = LoggerFactory.getLogger(AxChannelServiceRegistry.class);

    //DEV2
//    private String urlSLMB2C = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/";
//    private String urlSLMB2B = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/";
    private String urlSLMKIOSKWEB = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50007/";

    //DEV3
    private final String urlSLMB2C = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.b2cslm");
    private final String urlMFLGB2C = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.b2cmflg");
    private final String urlSLMB2B = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.ppslm");
    private final String urlMFLGB2B = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axchannelservice.api.url.ppmflg");

//      private String urlSLMB2C = "http://10.181.9.196:50000/";
//      private String urlSLMB2B = "http://10.181.9.196:50001/";
    //private String urlSLMB2B = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/";

    private boolean initStubsOnStart = false;

    private Map<StoreApiChannels, AxChannelServiceMainStub> channelMainStubs = new HashMap<>();

    @PostConstruct
    public void initRegistry() {
        log.info("Initialising AX Channel Custom API Registry...");

        //Allow logging of HTTP requests, including the SOAP services that'll be initialised in this class.
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

        final AxChannelServiceMainStub mainStubB2CSLM = new AxChannelServiceMainStub(StoreApiChannels.B2C_SLM, urlSLMB2C, false);
       // mainStubB2CSLM.setUsesDiscountCounter(true);
        mainStubB2CSLM.setUsesInventTable(true);
        mainStubB2CSLM.setUsesRetailTicketTable(true);
        //mainStubB2CSLM.setUsesUpCrossSell(true);
        //mainStubB2CSLM.setUsesUseDiscountCounter(true);
        mainStubB2CSLM.setUsesB2BRetailTicket(true);
        //mainStubB2CSLM.setUsesOnlineCart(true);
        //mainStubB2CSLM.setUsesB2BPINView(true);
        //mainStubB2CSLM.setUsesB2BPINViewLine(true);
        mainStubB2CSLM.setUsesPIN(true);
        channelMainStubs.put(StoreApiChannels.B2C_SLM, mainStubB2CSLM);

        final AxChannelServiceMainStub mainStubB2CMFLG = new AxChannelServiceMainStub(StoreApiChannels.B2C_MFLG, urlMFLGB2C, false);
        //mainStubB2CMFLG.setUsesDiscountCounter(true);
        mainStubB2CMFLG.setUsesInventTable(true);
        //mainStubB2CMFLG.setUsesOnlineCart(true);
        mainStubB2CMFLG.setUsesRetailTicketTable(true);
        //mainStubB2CMFLG.setUsesUpCrossSell(true);
        //mainStubB2CMFLG.setUsesUseDiscountCounter(true);
        mainStubB2CMFLG.setUsesB2BRetailTicket(true);
        mainStubB2CMFLG.setUsesPIN(true);
        channelMainStubs.put(StoreApiChannels.B2C_MFLG, mainStubB2CMFLG);

        final AxChannelServiceMainStub mainStubB2BSLM = new AxChannelServiceMainStub(StoreApiChannels.PARTNER_PORTAL_SLM, urlSLMB2B, false);
        //mainStubB2BSLM.setUsesDiscountCounter(true);
        mainStubB2BSLM.setUsesInventTable(true);
        mainStubB2BSLM.setUsesRetailTicketTable(true);
        //mainStubB2BSLM.setUsesUpCrossSell(true);
        //mainStubB2BSLM.setUsesUseDiscountCounter(true);
        //mainStubB2BSLM.setUsesB2BPINView(true);
        //mainStubB2BSLM.setUsesB2BPINViewLine(true);
        mainStubB2BSLM.setUsesB2BRetailTicket(true);
        mainStubB2BSLM.setUsesCustomerExt(true);
        //mainStubB2BSLM.setUsesB2BPINUpdate(true);
        //mainStubB2BSLM.setUsesB2BPINCancel(true);
//        mainStubB2BSLM.setUsesOnlineCart(true);
        //mainStubB2BSLM.setUsesPenaltyCharge(true);
       // mainStubB2BSLM.setUsesB2BUpdatePINStatus(true);
        mainStubB2BSLM.setUsesPIN(true);
        channelMainStubs.put(StoreApiChannels.PARTNER_PORTAL_SLM, mainStubB2BSLM);

        final AxChannelServiceMainStub mainStubB2BMFLG = new AxChannelServiceMainStub(StoreApiChannels.PARTNER_PORTAL_MFLG, urlMFLGB2B, false);

        //mainStubB2BMFLG.setUsesDiscountCounter(true);
        mainStubB2BMFLG.setUsesInventTable(true);
        mainStubB2BMFLG.setUsesRetailTicketTable(true);
        //mainStubB2BMFLG.setUsesUpCrossSell(true);
        //mainStubB2BMFLG.setUsesUseDiscountCounter(true);
       // mainStubB2BMFLG.setUsesB2BPINView(true);
        //mainStubB2BMFLG.setUsesB2BPINViewLine(true);
        mainStubB2BMFLG.setUsesB2BRetailTicket(true);
        mainStubB2BMFLG.setUsesCustomerExt(true);
//        mainStubB2BMFLG.setUsesB2BPINUpdate(true);
//        mainStubB2BMFLG.setUsesB2BPINCancel(true);
//        mainStubB2BMFLG.setUsesOnlineCart(true);
//        mainStubB2BMFLG.setUsesPenaltyCharge(true);
//        mainStubB2BMFLG.setUsesB2BUpdatePINStatus(true);
        mainStubB2BMFLG.setUsesPIN(true);
        channelMainStubs.put(StoreApiChannels.PARTNER_PORTAL_MFLG, mainStubB2BMFLG);

        final AxChannelServiceMainStub mainStubKIOSKWEB = new AxChannelServiceMainStub(StoreApiChannels.KIOSK_SLM, urlSLMKIOSKWEB, false);
        channelMainStubs.put(StoreApiChannels.KIOSK_SLM, mainStubKIOSKWEB);

        //Controls if system should initialise services on system startup.
        if (!initStubsOnStart) {
            log.info("AX Channel Custom API stubs will not be initialised on startup.");
            return;
        }

        log.info("Initialising AX Channel Custom API stubs...");
        //TODO Initialisation code here
    }

    public AxChannelServiceMainStub getChannelStub(StoreApiChannels channel) throws AxChannelException {
        final AxChannelServiceMainStub mainStub = channelMainStubs.get(channel);
        if (mainStub == null) {
            throw new AxChannelException("Channel not found or not supported: " +
                    (channel == null ? "Null" : channel.toString()));
        }

        if (!mainStub.isInit()) {
            final boolean initSuccess = mainStub.initialise();
            if (!initSuccess) {
                throw new AxChannelException("Unable to initialise channel API stub for " + channel.toString() + ".");
            }
        }

        return mainStub;
    }
}
