package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCapacityGroup;
import com.enovax.star.cms.commons.model.axstar.AxCustomerGroup;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PartnerVM {

    private static final String[] simpleModelFieldList = new String[]{"correspondenceAddress","correspondenceCity","correspondencePostalCode","useOneAddress"};

    private Integer id;
    private String orgName;
    private String accountCode;
    private String countryCode;
    private String countryName;
    private Integer adminId;
    private String revalFeeItemId;
    private String revalFeeStr;
    private Integer revalPeriodMonths;
    private String remarks;
    private String tierId;
    private String tierLabel;
    private String accountManagerId;
    private String accountManager;
    private String orgTypeCode;
    private String orgTypeName;
    private String branchName;
    private String uen;
    private String licenseNum;
    private String licenseExpDate;
    private String registrationYear;
    private String contactPerson;
    private String contactDesignation;
    private String address;
    private String postalCode;
    private String city;
    private String telNum;
    private String mobileNum;
    private String faxNum;
    private String email;
    private String website;
    private String languagePreference;
    private String mainDestinations;
    private String createdBy;
    private Date createdDate;
    private String createdDateStr;
    private String modifiedBy;
    private String file1Type;
    private String file2Type;
    private String file3Type;
    private String status;
    private String statusLabel;
    private List<PartnerDocumentVM> paDocs;
    private String dailyTransCapStr;
    private List<String> excluProdIds;
    private List<PartnerProductVM> excluProds;
    private Boolean subAccountEnabled;

    private Integer subAccCount;

    private String preDefinedBody;
    private String preDefinedFooter;
    private String baseUrl;
    private String username;
    private String password;
    private List<ReasonVM> reasons;

    private List<PaDistributionMappingVM> distributionMapping;
    private Boolean offlinePaymentEnabled;

    private String axAccountNumber;

    private boolean isResubmitRequired = false;
    private String resubmitReason;
    private String resubmitRemarks;

    private boolean hasUserRemarks = false;
    private String userRemarks;

    private List<PartnerExtVM> extension = new ArrayList<PartnerExtVM>();

    public PartnerVM() {}

    public PartnerVM(PPSLMPartner pa, boolean sampleModel) {
        this.id = pa.getId();
        this.username = pa.getMainAccount().getUsername();
        this.accountCode = pa.getAccountCode();
        this.adminId = pa.getAdminId();
        this.orgName = pa.getOrgName();
        this.orgTypeCode = pa.getOrgTypeCode();
        this.countryCode = pa.getCountryCode();
        this.branchName = pa.getBranchName();
        this.uen = pa.getUen();
        this.licenseNum = pa.getLicenseNum();
        this.registrationYear = pa.getRegistrationYear();
        this.licenseExpDate = pa.getLicenseExpDate();
        this.contactPerson = pa.getContactPerson();
        this.contactDesignation = pa.getContactDesignation();
        this.address = pa.getAddress();
        this.postalCode = pa.getPostalCode();
        this.city = pa.getCity();
        this.telNum = pa.getTelNum();
        this.mobileNum = pa.getMobileNum();
        this.faxNum = pa.getFaxNum();
        this.email = pa.getEmail();
        this.website = pa.getWebsite();
        this.languagePreference = pa.getLanguagePreference();
        this.mainDestinations = pa.getMainDestinations();
        this.status = pa.getStatus();
        this.statusLabel = PartnerStatus.getLabel(pa.getStatus());
        if(!sampleModel){
            this.accountManagerId = pa.getAccountManagerId();
            this.dailyTransCapStr = TransactionUtil.priceWithDecimal(pa.getDailyTransCap(), "");
            this.revalPeriodMonths = pa.getRevalPeriodMonths();
            this.revalFeeItemId = pa.getRevalFeeItemId();
            this.axAccountNumber = pa.getAxAccountNumber();
            this.tierId = pa.getTierId();
        }
        this.populateExtensionProperties(pa, sampleModel);
    }

    public PartnerVM(PPSLMPartner pa) {
        this(pa, false);
    }

    public PartnerVM(PartnerHist paHist, String serverUrl) {
        this.id = paHist.getId();
        this.adminId = paHist.getAdminId();
        this.accountCode = paHist.getAccountCode();
        this.orgName = paHist.getOrgName();
        this.orgTypeCode = paHist.getOrgTypeCode();
        this.countryCode = paHist.getCountryCode();
        this.branchName = paHist.getBranchName();
        this.uen = paHist.getUen();
        this.licenseNum = paHist.getLicenseNum();
        this.registrationYear = paHist.getRegistrationYear();
        this.licenseExpDate = paHist.getLicenseExpDate();
        this.contactPerson = paHist.getContactPerson();
        this.contactDesignation = paHist.getContactDesignation();
        this.address = paHist.getAddress();
        this.postalCode = paHist.getPostalCode();
        this.city = paHist.getCity();
        this.telNum = paHist.getTelNum();
        this.mobileNum = paHist.getMobileNum();
        this.faxNum = paHist.getFaxNum();
        this.email = paHist.getEmail();
        this.website = paHist.getWebsite();
        this.languagePreference = paHist.getLanguagePreference();
        this.mainDestinations = paHist.getMainDestinations();
        this.status = paHist.getStatus();
        this.statusLabel = PartnerStatus.getLabel(paHist.getStatus());
        this.accountManagerId = paHist.getAccountManagerId();
        this.dailyTransCapStr = TransactionUtil.priceWithDecimal(paHist.getDailyTransCap(), "");
        this.remarks = paHist.getRemarks();
        this.modifiedBy = paHist.getModifiedBy();
        this.subAccountEnabled = paHist.getSubAccountEnabled();
        this.revalPeriodMonths = paHist.getRevalPeriodMonths();
        List<PaDistributionMappingHist> pdMaps = paHist.getDistributionMapping();
        if(pdMaps!=null){
            distributionMapping = new ArrayList<PaDistributionMappingVM>();
            for(PaDistributionMappingHist tmpObj : pdMaps){
                distributionMapping.add(new PaDistributionMappingVM(tmpObj));
            }
        }
        this.excluProdIds = paHist.getExcluProdIds();
        this.revalFeeItemId = paHist.getRevalFeeItemId();
        this.tierId = paHist.getTierId();
        this.createdDateStr = paHist.getCreatedDateStr();
        this.baseUrl = serverUrl;
        this.axAccountNumber = paHist.getAxAccountNumber();

        List<PartnerExtVM> extVMs = paHist.getExtension();
        if(extVMs != null && extVMs.size() > 0){
            for(PartnerExtVM pvm : extVMs){
                this.extension.add(pvm);
            }
        }
    }


    private void populateExtensionProperties(PPSLMPartner pa) {
        populateExtensionProperties(pa, false);
    }

    private void populateExtensionProperties(PPSLMPartner pa, boolean sampleModel) {
        if(pa == null){
            return;
        }
        List<PPSLMPartnerExt> exts = pa.getPartnerExtList();
        if(exts == null || exts.size() == 0){
            return;
        }
        for(PPSLMPartnerExt i : exts){
            if(sampleModel){
                boolean found = false;
                for(String str : simpleModelFieldList){
                    if(i.getAttrName() != null && str.equalsIgnoreCase(i.getAttrName())){
                        found = true;
                    }
                }
                if(!found){
                    continue;
                }
            }
            PartnerExtVM vm = new PartnerExtVM();
            vm.setId(i.getId());
            vm.setPartnerId(i.getPartnerId());
            vm.setApprovalRequired(i.getApprovalRequired());
            vm.setAttrName(i.getAttrName());
            vm.setAttrValue(i.getAttrValue());
            vm.setAttrValueType(i.getAttrValueType());
            vm.setAttrValueFormat(i.getAttrValueFormat());
            vm.setMandatoryInd(i.getMandatoryInd());
            vm.setDescription(i.getDescription());
            vm.setStatus(i.getStatus());
            extension.add(vm);
        }
    }

    public void populateCustomerGroupName(List<AxCustomerGroup> customerGroupList){
        if(this.extension != null && this.extension.size() > 0 && customerGroupList != null && customerGroupList.size() > 0){
            for(PartnerExtVM i : extension){
                if("customerGroup".equals(i.getAttrName()) && i.getAttrValue() != null && i.getAttrValue().trim().length() > 0){
                    for(AxCustomerGroup k : customerGroupList){
                        if(i.getAttrValue().equals(k.getCustomerGroupNumber())){
                            i.setAttrValueLabel(k.getCustomerGroupName());
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    public void populateCapacityGroupName(List<AxCapacityGroup> customerGroupList){
        if(this.extension != null && this.extension.size() > 0 && customerGroupList != null && customerGroupList.size() > 0){
            for(PartnerExtVM i : extension){
                if("capacityGroupId".equals(i.getAttrName()) && i.getAttrValue() != null && i.getAttrValue().trim().length() > 0){
                    for(AxCapacityGroup k : customerGroupList){
                        if( k.getEventAllocationGroupID() != null && k.getEventAllocationGroupID().trim().length() > 0 && i.getAttrValue().equals(k.getEventAllocationGroupID())) {
                            i.setAttrValueLabel(k.getName());
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getRevalFeeItemId() {
        return revalFeeItemId;
    }

    public void setRevalFeeItemId(String revalFeeItemId) {
        this.revalFeeItemId = revalFeeItemId;
    }

    public String getRevalFeeStr() {
        return revalFeeStr;
    }

    public void setRevalFeeStr(String revalFeeStr) {
        this.revalFeeStr = revalFeeStr;
    }

    public Integer getRevalPeriodMonths() {
        return revalPeriodMonths;
    }

    public void setRevalPeriodMonths(Integer revalPeriodMonths) {
        this.revalPeriodMonths = revalPeriodMonths;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTierId() {
        return tierId;
    }

    public void setTierId(String tierId) {
        this.tierId = tierId;
    }

    public String getTierLabel() {
        return tierLabel;
    }

    public void setTierLabel(String tierLabel) {
        this.tierLabel = tierLabel;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getOrgTypeCode() {
        return orgTypeCode;
    }

    public void setOrgTypeCode(String orgTypeCode) {
        this.orgTypeCode = orgTypeCode;
    }

    public String getOrgTypeName() {
        return orgTypeName;
    }

    public void setOrgTypeName(String orgTypeName) {
        this.orgTypeName = orgTypeName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactDesignation() {
        return contactDesignation;
    }

    public void setContactDesignation(String contactDesignation) {
        this.contactDesignation = contactDesignation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }

    public void setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
    }

    public String getMainDestinations() {
        return mainDestinations;
    }

    public void setMainDestinations(String mainDestinations) {
        this.mainDestinations = mainDestinations;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getFile1Type() {
        return file1Type;
    }

    public void setFile1Type(String file1Type) {
        this.file1Type = file1Type;
    }

    public String getFile2Type() {
        return file2Type;
    }

    public void setFile2Type(String file2Type) {
        this.file2Type = file2Type;
    }

    public String getFile3Type() {
        return file3Type;
    }

    public void setFile3Type(String file3Type) {
        this.file3Type = file3Type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public List<PartnerDocumentVM> getPaDocs() {
        return paDocs;
    }

    public void setPaDocs(List<PartnerDocumentVM> paDocs) {
        this.paDocs = paDocs;
    }

    public String getDailyTransCapStr() {
        return dailyTransCapStr;
    }

    public void setDailyTransCapStr(String dailyTransCapStr) {
        this.dailyTransCapStr = dailyTransCapStr;
    }

    public List<String> getExcluProdIds() {
        return excluProdIds;
    }

    public void setExcluProdIds(List<String> excluProdIds) {
        this.excluProdIds = excluProdIds;
    }

    public List<PartnerProductVM> getExcluProds() {
        return excluProds;
    }

    public void setExcluProds(List<PartnerProductVM> excluProds) {
        this.excluProds = excluProds;
    }

    public Boolean getSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(Boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }

    public Integer getSubAccCount() {
        return subAccCount;
    }

    public void setSubAccCount(Integer subAccCount) {
        this.subAccCount = subAccCount;
    }

    public String getPreDefinedBody() {
        return preDefinedBody;
    }

    public void setPreDefinedBody(String preDefinedBody) {
        this.preDefinedBody = preDefinedBody;
    }

    public String getPreDefinedFooter() {
        return preDefinedFooter;
    }

    public void setPreDefinedFooter(String preDefinedFooter) {
        this.preDefinedFooter = preDefinedFooter;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ReasonVM> getReasons() {
        return reasons;
    }

    public void setReasons(List<ReasonVM> reasons) {
        this.reasons = reasons;
    }

    public List<PaDistributionMappingVM> getDistributionMapping() {
        return distributionMapping;
    }

    public void setDistributionMapping(List<PaDistributionMappingVM> distributionMapping) {
        this.distributionMapping = distributionMapping;
    }

    public Boolean getOfflinePaymentEnabled() {
        return offlinePaymentEnabled;
    }

    public void setOfflinePaymentEnabled(Boolean offlinePaymentEnabled) {
        this.offlinePaymentEnabled = offlinePaymentEnabled;
    }

    public String getAxAccountNumber() {
        return axAccountNumber;
    }

    public void setAxAccountNumber(String axAccountNumber) {
        this.axAccountNumber = axAccountNumber;
    }

    public List<PartnerExtVM> getExtension() {
        return extension;
    }

    public void setExtension(List<PartnerExtVM> extension) {
        this.extension = extension;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }

    public void populateProeprtyName(List<CountryVM> ctyVMs, List<PartnerTypeVM> paTypes) {
        if(paTypes != null && paTypes.size() > 0 && this.orgTypeCode != null && this.orgTypeCode.trim().length() > 0 ){
            for(PartnerTypeVM paTypeVM : paTypes){
                if(paTypeVM != null && paTypeVM.getId() != null && paTypeVM.getId().trim().equals(orgTypeCode)){
                    this.setOrgTypeName(paTypeVM.getLabel());
                    break;
                }
            }
        }
        if(ctyVMs != null && ctyVMs.size() > 0 && this.countryCode != null && countryCode.trim().length() > 0){
            for(CountryVM ctyVM : ctyVMs){
                if(ctyVM != null && ctyVM.getCtyCode() != null && ctyVM.getCtyCode().equals(countryCode)){
                    this.setCountryName(ctyVM.getCtyName());
                    break;
                }
            }
        }
    }

    public String getExtensionPropertyValue(String name) {
        if(name != null && name.trim().length() > 0){
            List<PartnerExtVM> exts = this.getExtension();
            if(exts != null && exts.size() > 0){
                for(PartnerExtVM i : exts){
                    if( i.getAttrName() != null && i.getAttrName().trim().length() > 0 && i.getAttrValue() != null && i.getAttrValue().trim().length() > 0 && name.equalsIgnoreCase(i.getAttrName().trim())){
                        return i.getAttrValue().trim();
                    }
                }
            }
        }
        return null;
    }

    public String getResubmitRemarks() {
        return resubmitRemarks;
    }

    public void setResubmitRemarks(String resubmitRemarks) {
        this.resubmitRemarks = resubmitRemarks;
    }

    public boolean isHasUserRemarks() {
        return hasUserRemarks;
    }

    public void setHasUserRemarks(boolean hasUserRemarks) {
        this.hasUserRemarks = hasUserRemarks;
    }

    public String getUserRemarks() {
        return userRemarks;
    }

    public void setUserRemarks(String userRemarks) {
        this.userRemarks = userRemarks;
    }

    public String getResubmitReason() {
        return resubmitReason;
    }

    public void setResubmitReason(String resubmitReason) {
        this.resubmitReason = resubmitReason;
    }

    public boolean isResubmitRequired() {
        return isResubmitRequired;
    }

    public void setResubmitRequired(boolean resubmitRequired) {
        isResubmitRequired = resubmitRequired;
    }
}

