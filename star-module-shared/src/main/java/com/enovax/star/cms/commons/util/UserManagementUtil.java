package com.enovax.star.cms.commons.util;

import com.enovax.star.cms.commons.mgnl.definition.Roles;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import info.magnolia.cms.security.User;
import info.magnolia.context.MgnlContext;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;

@Deprecated
public class UserManagementUtil {

    public static boolean isSuperUser() {
        return MgnlContext.getUser().hasRole(Roles.SUPERUSER.getRole());
    }

    public static boolean hasAccessToB2CSLMCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CSLMPRODUCTCATEGORY.getRole()) || roles.contains(Roles.B2CSLMPUBLISHCATEGORY.getRole());
    }

    public static boolean hasAccessToB2CMFLGCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CMFLGPRODUCTCATEGORY.getRole()) || roles.contains(Roles.B2CMFLGPUBLISHCATEGORY.getRole());
    }

    public static boolean hasAccessToKioskSLMCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKSLMPRODUCTCATEGORY.getRole()) || roles.contains(Roles.KIOSKSLMPUBLISHCATEGORY.getRole());
    }

    public static boolean hasAccessToKioskMFLGCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKMFLGPRODUCTCATEGORY.getRole()) || roles.contains(Roles.KIOSKMFLGPUBLISHCATEGORY.getRole());
    }

    public static boolean hasAccessToPPSLMCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.PPSLMPRODUCTCATEGORY.getRole()) || roles.contains(Roles.PPSLMPUBLISHCATEGORY.getRole());
    }

    public static boolean hasAccessToPPMFLGCategory(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.PPMFLGPRODUCTCATEGORY.getRole()) || roles.contains(Roles.PPMFLGPUBLISHCATEGORY.getRole());
    }


    public static boolean hasAccessToB2CSLMProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CSLMCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToB2CMFLGProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CMFLGCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToKioskSLMProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKSLMCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToKioskMFLGProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKMFLGCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToPPSLMProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.PPSLMCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToPPMFLGProduct(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.PPMFLGCMSPRODUCT.getRole());
    }

    public static boolean hasAccessToB2CSLMAXSync(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CSLMAXSYNC.getRole());
    }

    public static boolean hasAccessToB2CMFLGAXSync(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.B2CMFLGAXSYNC.getRole());
    }

    public static boolean hasAccessToKioskSLMAXSync(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKSLMAXSYNC.getRole());
    }

    public static boolean hasAccessToKioskMFLGAXSync(User user) {
        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }
        return roles.contains(Roles.KIOSKMFLGAXSYNC.getRole());
    }

    public static boolean hasAccessToPPSLMAXSync(User user) {

        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }

        return roles.contains(Roles.PPSLMAXSYNC.getRole());
    }

    public static boolean hasAccessToPPMFLGAXSync(User user) {

        Collection<String> roles = user.getAllRoles();
        if(roles == null) {
            return false;
        }

        return roles.contains(Roles.PPMFLGAXSYNC.getRole());
    }

    /*
    Get the users with group that has role of this roleName or have role of this roleName
     */
    public static Collection<String> getUsersWithRole(final String roleName) {

        //get the node first for the role
        Iterable<Node> roleNodesIterable = null;
        try {
            roleNodesIterable = JcrRepository.query("userroles", "mgnl:role", "//element(*, mgnl:role)[fn:name() = '" + roleName + "']");
        } catch (RepositoryException e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        }

        if(roleNodesIterable == null) {
            return Collections.EMPTY_LIST;
        }

        Iterator<Node> roleNodesIterator = roleNodesIterable.iterator();
        Node roleNode = null;
        while(roleNodesIterator.hasNext()) {
            roleNode = roleNodesIterator.next();
        }

        if(roleNode == null) {
            return Collections.EMPTY_LIST;
        } else {
            Set<String> userList = new HashSet<>();

            //get all users with roles
            try {
                Iterable<Node> userNodesByRoleIterable = JcrRepository.query("users", "mgnl:contentNode", "//element(*, mgnl:contentNode)[jcr:contains(., '" + roleNode.getIdentifier() + "')]");

                if(userNodesByRoleIterable != null) {
                    Iterator<Node> userNodesByRoleIterator = userNodesByRoleIterable.iterator();
                    while(userNodesByRoleIterator.hasNext()) {
                        userList.add(userNodesByRoleIterator.next().getParent().getName());
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }

            //get all users with group that has roles of this
            try {
                Iterable<Node> groupNodesIterable = JcrRepository.query("usergroups", "mgnl:contentNode", "//element(*, mgnl:contentNode)[jcr:contains(., '" + roleNode.getIdentifier() + "')]");
                Iterator<Node> groupNodesIterator = groupNodesIterable.iterator();
                while(groupNodesIterator.hasNext()) {
                    Node groupNode = groupNodesIterator.next().getParent();
                    Iterable<Node> userNodesByGroupIterable = JcrRepository.query("users", "mgnl:contentNode", "//element(*, mgnl:contentNode)[jcr:contains(., '" + groupNode.getIdentifier() + "')]");
                    Iterator<Node> userNodesByGroupIterator = userNodesByGroupIterable.iterator();

                    while(userNodesByGroupIterator.hasNext()) {
                        userList.add(userNodesByGroupIterator.next().getParent().getName());
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }

            return userList;
        }
    }

}
