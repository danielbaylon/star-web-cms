package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTARightsMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lavanya on 5/9/16.
 */
@Repository
public interface PPSLMTARightsMappingRepository extends JpaRepository<PPSLMTARightsMapping, Integer> {

    PPSLMTARightsMapping findById(int id);

    List<PPSLMTARightsMapping> deleteById(int id);

    List<PPSLMTARightsMapping> findBySubUser(PPSLMTASubAccount subuserId);

}
