package com.enovax.star.cms.commons.constant.ppslm;

/**
 * Created by jennylynsze on 5/18/16.
 */
public enum TicketStatus {
    Reserved,
    Available,
    Revalidated,
    Refunded,
    Expired,
    Expiring,
    Incomplete,
    Failed,
    Forfeited,
    Pending_Approval,
    Pending_Submit;
    //Expiring are only for display
}
