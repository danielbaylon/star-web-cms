
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CartValidateAndReserveQuantityOnlineResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_CartRetailTicketTableResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cartValidateAndReserveQuantityOnlineResult"
})
@XmlRootElement(name = "CartValidateAndReserveQuantityOnlineResponse")
public class CartValidateAndReserveQuantityOnlineResponse {

    @XmlElementRef(name = "CartValidateAndReserveQuantityOnlineResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCCartRetailTicketTableResponse> cartValidateAndReserveQuantityOnlineResult;

    /**
     * Gets the value of the cartValidateAndReserveQuantityOnlineResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}
     *     
     */
    public JAXBElement<SDCCartRetailTicketTableResponse> getCartValidateAndReserveQuantityOnlineResult() {
        return cartValidateAndReserveQuantityOnlineResult;
    }

    /**
     * Sets the value of the cartValidateAndReserveQuantityOnlineResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}
     *     
     */
    public void setCartValidateAndReserveQuantityOnlineResult(JAXBElement<SDCCartRetailTicketTableResponse> value) {
        this.cartValidateAndReserveQuantityOnlineResult = value;
    }

}
