package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by jennylynsze on 8/12/16.
 */
public class PPSLMInventoryTransactionItemSpecification extends BaseSpecification<PPSLMInventoryTransactionItem> {
}
