package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxSendRedeemTiketToAXCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskSendRedeemTicketRequest {

    @JsonProperty("SendRedeemTicketCriteria")
    private AxSendRedeemTiketToAXCriteria criteria = new AxSendRedeemTiketToAXCriteria();

    public AxSendRedeemTiketToAXCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(AxSendRedeemTiketToAXCriteria criteria) {
        this.criteria = criteria;
    }

}
