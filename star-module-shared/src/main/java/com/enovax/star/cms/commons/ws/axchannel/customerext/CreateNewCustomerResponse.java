
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateNewCustomerResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_CreateCustomerResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createNewCustomerResult"
})
@XmlRootElement(name = "CreateNewCustomerResponse")
public class CreateNewCustomerResponse {

    @XmlElementRef(name = "CreateNewCustomerResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCCreateCustomerResponse> createNewCustomerResult;

    /**
     * Gets the value of the createNewCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCCreateCustomerResponse }{@code >}
     *     
     */
    public JAXBElement<SDCCreateCustomerResponse> getCreateNewCustomerResult() {
        return createNewCustomerResult;
    }

    /**
     * Sets the value of the createNewCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCCreateCustomerResponse }{@code >}
     *     
     */
    public void setCreateNewCustomerResult(JAXBElement<SDCCreateCustomerResponse> value) {
        this.createNewCustomerResult = value;
    }

}
