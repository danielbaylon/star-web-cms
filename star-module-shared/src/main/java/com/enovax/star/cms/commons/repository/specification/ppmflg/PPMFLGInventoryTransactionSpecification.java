package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 17/8/16.
 */
public class PPMFLGInventoryTransactionSpecification extends BaseSpecification<PPMFLGInventoryTransaction> {
}
