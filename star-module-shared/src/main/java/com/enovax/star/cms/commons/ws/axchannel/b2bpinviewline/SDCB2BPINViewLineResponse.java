
package com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_B2BPINViewLineResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_B2BPINViewLineResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="b2bPinViewLineTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_B2BPINViewLineTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_B2BPINViewLineResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "b2BPinViewLineTable"
})
public class SDCB2BPINViewLineResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "b2bPinViewLineTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCB2BPINViewLineTable> b2BPinViewLineTable;

    /**
     * Gets the value of the b2BPinViewLineTable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> getB2BPinViewLineTable() {
        return b2BPinViewLineTable;
    }

    /**
     * Sets the value of the b2BPinViewLineTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}
     *     
     */
    public void setB2BPinViewLineTable(JAXBElement<ArrayOfSDCB2BPINViewLineTable> value) {
        this.b2BPinViewLineTable = value;
    }

}
