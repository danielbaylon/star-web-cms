package com.enovax.star.cms.commons.model.product.crosssell;

/**
 * Created by jensen on 25/6/16.
 */
public class CrossSellLinkRelatedProduct {

    private String itemId; //productCode
    private String recordId; //productId or listingId
    private String unitId;
    private Integer qty; //Not sure what this is used for

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
