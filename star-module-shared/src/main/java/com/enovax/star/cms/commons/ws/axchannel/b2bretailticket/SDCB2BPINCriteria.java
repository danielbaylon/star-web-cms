
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_B2BPINCriteria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_B2BPINCriteria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerateB2BPinCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfB2BPIN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_B2BPINCriteria", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "generateB2BPinCollection"
})
public class SDCB2BPINCriteria {

    @XmlElementRef(name = "GenerateB2BPinCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfB2BPIN> generateB2BPinCollection;

    /**
     * Gets the value of the generateB2BPinCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfB2BPIN }{@code >}
     *     
     */
    public JAXBElement<ArrayOfB2BPIN> getGenerateB2BPinCollection() {
        return generateB2BPinCollection;
    }

    /**
     * Sets the value of the generateB2BPinCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfB2BPIN }{@code >}
     *     
     */
    public void setGenerateB2BPinCollection(JAXBElement<ArrayOfB2BPIN> value) {
        this.generateB2BPinCollection = value;
    }

}
