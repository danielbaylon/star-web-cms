package com.enovax.star.cms.commons.exception;

/**
 * Created by houtao on 13/9/16.
 */
public class BizValidationException extends Exception {

    private String errorMsg = null;

    public BizValidationException(Exception str){
        super(str);
        if(str != null){
            errorMsg = str.getMessage();
        }
    }
    public BizValidationException(String str){
        super(str);
        if(str != null){
            this.errorMsg = str;
        }
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
