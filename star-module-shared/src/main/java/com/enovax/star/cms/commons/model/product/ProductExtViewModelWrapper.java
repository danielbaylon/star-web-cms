package com.enovax.star.cms.commons.model.product;

import java.util.List;

public class ProductExtViewModelWrapper {

    public ProductExtViewModelWrapper() {

    }

    public ProductExtViewModelWrapper(List<ProductExtViewModel> products) {
        this.products = products;
    }

    private List<ProductExtViewModel> products;

    public List<ProductExtViewModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductExtViewModel> products) {
        this.products = products;
    }
}
