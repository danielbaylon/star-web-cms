package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 1/15/17.
 */
public enum TAAccessRight {
    attractions,
    reports,
    accMgmt,
    purchaseTickets,
    revalidateTickets,
    redemptionStatus,
    viewInventory,
    generateBundle,
    packages,
    events,
    subAccMgmt,
    reserveWOT,
    viewWOTReceipts,
    manageDeposits,
    topupDeposits,
    offlinePayment
}
