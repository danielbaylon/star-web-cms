package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = PPSLMPartner.TABLE_NAME)
public class PPSLMPartner implements Serializable{

    public static final String TABLE_NAME = "PPSLMPartner";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "adminId", nullable = false)
    private Integer adminId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adminId", updatable = false, insertable = false)
    private PPSLMTAMainAccount mainAccount;

    @Column(name = "axAccountNumber", nullable = true)
    private String axAccountNumber;

    @Column(name = "accountCode", nullable = false)
    private String accountCode;

    @Column(name = "revalFeeItemId", nullable = true)
    private String revalFeeItemId;

    @Column(name = "revalPeriodMonths", nullable = true)
    private Integer revalPeriodMonths;

    @Column(name = "tierId", nullable = true)
    private String tierId;

    @Column(name = "remarks", nullable = true)
    private String remarks;

    @Column(name = "accountManagerId", nullable = true)
    private String accountManagerId;

    @Column(name = "orgType", nullable = false)
    private String orgTypeCode;

    @Column(name = "country", nullable = false)
    private String countryCode;

    @Column(name = "orgName", nullable = false)
    private String orgName;

    @Column(name = "branchName", nullable = true)
    private String branchName;

    @Column(name = "uen", nullable = true)
    private String uen;

    @Column(name = "licenseNum", nullable = false)
    private String licenseNum;

    @Column(name = "licenseExpDate")
    private String licenseExpDate;

    @Column(name = "registrationYear", nullable = true)
    private String registrationYear;

    @Column(name = "contactPerson", nullable = false)
    private String contactPerson;

    @Column(name = "contactDesignation", nullable = false)
    private String contactDesignation;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "postalCode", nullable = false)
    private String postalCode;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "telNum", nullable = false)
    private String telNum;

    @Column(name = "mobileNum")
    private String mobileNum;

    @Column(name = "faxNum", nullable = true)
    private String faxNum;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "website", nullable = true)
    private String website;

    @Column(name = "languagePreference", nullable = true)
    private String languagePreference;

    @Column(name = "mainDestinations", nullable = true)
    private String mainDestinations;

    @Column(name = "createdBy", nullable = false)
    private String createdBy;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "modifiedBy", nullable = false)
    private String modifiedBy;

    @Column(name = "modifiedDate", nullable = false)
    private Date modifiedDate;

    @Column(name = "dailyTransCap")
    private BigDecimal dailyTransCap;

    @Column(name = "status")
    private String status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="partner", targetEntity = PPSLMPartnerExt.class)
    private List<PPSLMPartnerExt> partnerExtList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public PPSLMTAMainAccount getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(PPSLMTAMainAccount mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    /***
    @OneToMany(fetch = FetchType.LAZY,mappedBy= "partner")
    @NotFound(action= NotFoundAction.IGNORE)
    private List<PartnerExclusiveMapping> exclusiveMaps;
    **/

    public Integer getRevalPeriodMonths() {
        return revalPeriodMonths;
    }

    public void setRevalPeriodMonths(Integer revalPeriodMonths) {
        this.revalPeriodMonths = revalPeriodMonths;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactDesignation() {
        return contactDesignation;
    }

    public void setContactDesignation(String contactDesignation) {
        this.contactDesignation = contactDesignation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }

    public void setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
    }

    public String getMainDestinations() {
        return mainDestinations;
    }

    public void setMainDestinations(String mainDestinations) {
        this.mainDestinations = mainDestinations;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BigDecimal getDailyTransCap() {
        return dailyTransCap;
    }

    public void setDailyTransCap(BigDecimal dailyTransCap) {
        this.dailyTransCap = dailyTransCap;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }

    public String getAxAccountNumber() {
        return axAccountNumber;
    }

    public void setAxAccountNumber(String axAccountNumber) {
        this.axAccountNumber = axAccountNumber;
    }

    public List<PPSLMPartnerExt> getPartnerExtList() {
        return partnerExtList;
    }

    public void setPartnerExtList(List<PPSLMPartnerExt> partnerExtList) {
        this.partnerExtList = partnerExtList;
    }

    public String getRevalFeeItemId() {
        return revalFeeItemId;
    }

    public void setRevalFeeItemId(String revalFeeItemId) {
        this.revalFeeItemId = revalFeeItemId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getOrgTypeCode() {
        return orgTypeCode;
    }

    public void setOrgTypeCode(String orgTypeCode) {
        this.orgTypeCode = orgTypeCode;
    }

    public String getTierId() {
        return tierId;
    }

    public void setTierId(String tierId) {
        this.tierId = tierId;
    }
}
