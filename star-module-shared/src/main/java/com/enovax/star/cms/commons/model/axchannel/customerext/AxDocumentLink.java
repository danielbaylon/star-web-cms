package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by houtao on 26/9/16.
 */
public class AxDocumentLink {

    private String documentName;
    private String link;

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
