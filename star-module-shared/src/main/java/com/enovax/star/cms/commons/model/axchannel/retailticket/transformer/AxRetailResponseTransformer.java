package com.enovax.star.cms.commons.model.axchannel.retailticket.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.*;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class AxRetailResponseTransformer {

    public static ApiResult<List<AxRetailTicketRecord>> fromWsCart(SDCCartRetailTicketTableResponse wsResponse) {
        if (wsResponse == null) {
            return new ApiResult<>();
        }

        final ArrayOfResponseError arrayOfResponseError = wsResponse.getErrors().getValue();
        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), null);
        }

        final SDCCartRetailTicketTable arrayOfSDCRetailTicketTable = wsResponse.getCartRetailTicketCollection().getValue();
        if (arrayOfSDCRetailTicketTable == null) {
            return new ApiResult<>(true, "", "Successful but no records returned.", new ArrayList<>());
        }
        final boolean isError = 1 == arrayOfSDCRetailTicketTable.getErrorStatus();
        if (isError) {
            return new ApiResult<>(false, "ERROR", arrayOfSDCRetailTicketTable.getErrorMessage().getValue(), null);
        }

        final ArrayOfSDCCartRetailTicketItem arrayOfSDCCartRetailTicketItem = arrayOfSDCRetailTicketTable.getCartRetailTicketTableEntityCollection().getValue();
        if (arrayOfSDCCartRetailTicketItem == null) {
            return new ApiResult<>(true, "", "Successful but no records returned.", new ArrayList<>());
        }

        return new ApiResult<>(true, "", "", AxRetailTicketRecordTransformer.fromWsCart(arrayOfSDCCartRetailTicketItem.getSDCCartRetailTicketItem()));
    }
}
