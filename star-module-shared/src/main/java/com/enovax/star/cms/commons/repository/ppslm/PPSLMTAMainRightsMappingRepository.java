package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainRightsMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PPSLMTAMainRightsMappingRepository extends JpaRepository<PPSLMTAMainRightsMapping, Integer> {
}
