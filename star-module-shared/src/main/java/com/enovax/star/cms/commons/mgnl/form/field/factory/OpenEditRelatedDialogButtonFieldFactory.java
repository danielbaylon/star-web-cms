package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.EditButton;
import com.enovax.star.cms.commons.mgnl.form.field.definition.OpenEditRelatedDialogButtonFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.dialog.callback.DefaultEditorCallback;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenter;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenterFactory;
import info.magnolia.ui.form.field.factory.AbstractFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class OpenEditRelatedDialogButtonFieldFactory<D extends OpenEditRelatedDialogButtonFieldDefinition> extends AbstractFieldFactory<D, Object> {

    private final FormDialogPresenterFactory formDialogPresenterFactory;
    private final UiContext uiContext;
    private EditButton button;
    private final OpenEditRelatedDialogButtonFieldDefinition definition;
    private final Item relatedFieldItem;
    private Item itemToEdit;

    @Inject
    public OpenEditRelatedDialogButtonFieldFactory(D definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18NAuthoringSupport, FormDialogPresenterFactory formDialogPresenterFactory) {
        super(definition, relatedFieldItem, uiContext, i18NAuthoringSupport);
        this.formDialogPresenterFactory = formDialogPresenterFactory;
        this.uiContext = uiContext;
        this.definition = definition;
        this.relatedFieldItem = relatedFieldItem;
    }


    private Button.ClickListener createButtonClickListener() {
        return new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                final FormDialogPresenter formDialogPresenter = formDialogPresenterFactory.createFormDialogPresenter(definition.getDialogName());
                if(formDialogPresenter == null) {
                    //this.uiContext.openNotification(MessageStyleTypeEnum.ERROR, false, this.i18n.translate("ui-framework.actions.dialog.not.registered", new Object[]{dialogName}));
                } else {

                    //if the workspace is define, then it means it gets the detail from other workspace, so need to change the item
                    if(!StringUtils.isEmpty(definition.getWorkspace())) {
                        Node relatedFieldItemNode = ((JcrNodeAdapter) relatedFieldItem).getJcrItem();

                        String uuid = PropertyUtil.getString(relatedFieldItemNode, definition.getUuidPropertyName());
                        Node itemToEditNode = null;
                        try {
                            itemToEditNode = NodeUtil.getNodeByIdentifier(definition.getWorkspace(), uuid);

                        } catch (RepositoryException e) {
                            e.printStackTrace();
                        }

                        if(itemToEditNode != null) {
                            JcrNodeAdapter itemToEditAdapter = new JcrNodeAdapter(itemToEditNode);
                            OpenEditRelatedDialogButtonFieldFactory.this.itemToEdit = itemToEditAdapter;
                        }
                    }else {
                        //otherwise, need to retain the same one
                        OpenEditRelatedDialogButtonFieldFactory.this.itemToEdit = OpenEditRelatedDialogButtonFieldFactory.this.relatedFieldItem;
                    }


                    formDialogPresenter.start(OpenEditRelatedDialogButtonFieldFactory.this.itemToEdit, definition.getDialogName(), uiContext, new DefaultEditorCallback(formDialogPresenter)  {
                        public void onSuccess(String actionName) {
                            uiContext.openNotification(MessageStyleTypeEnum.INFO, true, "Saved Successfully.");
                            formDialogPresenter.closeDialog();
                        }

                        public void onCancel() {
                            formDialogPresenter.closeDialog();
                        }
                    });


                }
            }
        };
    }


    @Override
    protected Field<Object> createFieldComponent()
    {
        this.button = new EditButton();
        this.button.getButton().setCaption(this.definition.getButtonCaption());
        this.button.getButton().addClickListener(createButtonClickListener());

        return this.button;
    }
}
