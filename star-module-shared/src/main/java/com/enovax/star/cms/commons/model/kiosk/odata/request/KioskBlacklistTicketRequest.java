package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxUpdateRetailTicketStatusCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskBlacklistTicketRequest {

    @JsonProperty("BlacklistTicketCriteria")
    private AxUpdateRetailTicketStatusCriteria blacklistTicketCriteria = new AxUpdateRetailTicketStatusCriteria();

    public AxUpdateRetailTicketStatusCriteria getBlacklistTicketCriteria() {
        return blacklistTicketCriteria;
    }

    public void setBlacklistTicketCriteria(AxUpdateRetailTicketStatusCriteria blacklistTicketCriteria) {
        this.blacklistTicketCriteria = blacklistTicketCriteria;
    }

}
