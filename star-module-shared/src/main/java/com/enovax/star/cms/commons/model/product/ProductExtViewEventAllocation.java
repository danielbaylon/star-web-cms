package com.enovax.star.cms.commons.model.product;

import java.util.Date;

public class ProductExtViewEventAllocation {

    /**
     * dd/MM/yyyy
     */
    private String date;
    private Date rawDate;
    private String eventGroupId;
    private String eventLineId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getRawDate() {
        return rawDate;
    }

    public void setRawDate(Date rawDate) {
        this.rawDate = rawDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }
}
