package com.enovax.star.cms.commons.constant.api;

/**
 * Created by jensen on 10/11/16.
 */
public enum TransactionStage {
    Reserved,
    Canceled,
    Success,
    Incomplete,
    Failed
}
