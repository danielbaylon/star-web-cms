
package com.enovax.star.cms.commons.ws.axretail;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CartUpdateQuantityResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_RetailTicketTableResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cartUpdateQuantityResult"
})
@XmlRootElement(name = "CartUpdateQuantityResponse")
public class CartUpdateQuantityResponse {

    @XmlElementRef(name = "CartUpdateQuantityResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCRetailTicketTableResponse> cartUpdateQuantityResult;

    /**
     * Gets the value of the cartUpdateQuantityResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}
     *     
     */
    public JAXBElement<SDCRetailTicketTableResponse> getCartUpdateQuantityResult() {
        return cartUpdateQuantityResult;
    }

    /**
     * Sets the value of the cartUpdateQuantityResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}
     *     
     */
    public void setCartUpdateQuantityResult(JAXBElement<SDCRetailTicketTableResponse> value) {
        this.cartUpdateQuantityResult = value;
    }

}
