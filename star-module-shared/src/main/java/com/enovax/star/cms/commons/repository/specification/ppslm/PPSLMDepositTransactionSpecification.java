package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 18/8/16.
 */
public class PPSLMDepositTransactionSpecification extends BaseSpecification<PPSLMDepositTransaction> {
}
