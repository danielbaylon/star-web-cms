package com.enovax.star.cms.commons.model.kiosk.odata.request;

/**
 * Created by Justin on 7/9/16.
 */
public class KioskLogoffRequest {

    private String transactionId = "-1";

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
