package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskVoidTenderLineRequest {

    @JsonProperty("isPreprocessed")
    private String isPreprocessed = "false";

    @JsonProperty("reasonCodeLines")
    private List reasonCodeLines = new ArrayList();

    @JsonProperty("tenderLineId")
    private String tenderLineId;

    @JsonIgnore
    private String cartId;

    public String getIsPreprocessed() {
        return isPreprocessed;
    }

    public void setIsPreprocessed(String isPreprocessed) {
        this.isPreprocessed = isPreprocessed;
    }

    public List getReasonCodeLines() {
        return reasonCodeLines;
    }

    public void setReasonCodeLines(List reasonCodeLines) {
        this.reasonCodeLines = reasonCodeLines;
    }

    public String getTenderLineId() {
        return tenderLineId;
    }

    public void setTenderLineId(String tenderLineId) {
        this.tenderLineId = tenderLineId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

}
