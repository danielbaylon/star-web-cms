package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketPINTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCommerceProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskCartTicketUpdatePrintStatusResponse {

	@JsonProperty("TransactionId")
	private String transactionId;

	@JsonProperty("IsRealTime")
	private String isRealTime;

	@JsonProperty("ErrorMessage")
	private String errorMessage;

	@JsonProperty("ErrorStatus")
	private String errorStatus;

	@JsonProperty("CartRetailTicketPINTableEntityCollection")
	private List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList;

	@JsonProperty("CartRetailTicketTableEntityCollection")
	private List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList;

	/**
	 * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
	 * 
	 */
	@JsonProperty("ExtensionProperties")
	private List<AxCommerceProperty> axExtensionPropertiesDataList;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getIsRealTime() {
		return isRealTime;
	}

	public void setIsRealTime(String isRealTime) {
		this.isRealTime = isRealTime;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(String errorStatus) {
		this.errorStatus = errorStatus;
	}

	public List<AxCartRetailTicketPINTableEntity> getAxCartRetailTicketPINTableEntityList() {
		return axCartRetailTicketPINTableEntityList;
	}

	public void setAxCartRetailTicketPINTableEntityList(
			List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList) {
		this.axCartRetailTicketPINTableEntityList = axCartRetailTicketPINTableEntityList;
	}

	public List<AxCartRetailTicketTableEntity> getAxCartRetailTicketTableEntityList() {
		return axCartRetailTicketTableEntityList;
	}

	public void setAxCartRetailTicketTableEntityList(
			List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList) {
		this.axCartRetailTicketTableEntityList = axCartRetailTicketTableEntityList;
	}

	public List<AxCommerceProperty> getAxExtensionPropertiesDataList() {
		return axExtensionPropertiesDataList;
	}

	public void setAxExtensionPropertiesDataList(List<AxCommerceProperty> axExtensionPropertiesDataList) {
		this.axExtensionPropertiesDataList = axExtensionPropertiesDataList;
	}

}
