package com.enovax.star.cms.commons.model.axchannel.inventtable;

import java.math.BigDecimal;
import java.util.Date;

public class AxRetailProductExtPackageLine {

    private String absoluteCapacityId;
    private boolean allowRevisit;
    private BigDecimal cost;
    private Date createdDate;
    private String dataAreaId;
    private String description;
    private BigDecimal entryWeight;
    private String itemId;
    private String lineGroup;
    private BigDecimal lineNumber;
    private Date modifiedDate;
    private String packageId;
    private int printingBehavior;
    private String recId;
    private String retailVariantId;
    private BigDecimal revenue;

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public boolean isAllowRevisit() {
        return allowRevisit;
    }

    public void setAllowRevisit(boolean allowRevisit) {
        this.allowRevisit = allowRevisit;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public BigDecimal getEntryWeight() {
        return entryWeight;
    }

    public void setEntryWeight(BigDecimal entryWeight) {
        this.entryWeight = entryWeight;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getLineGroup() {
        return lineGroup;
    }

    public void setLineGroup(String lineGroup) {
        this.lineGroup = lineGroup;
    }

    public BigDecimal getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(BigDecimal lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public int getPrintingBehavior() {
        return printingBehavior;
    }

    public void setPrintingBehavior(int printingBehavior) {
        this.printingBehavior = printingBehavior;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
