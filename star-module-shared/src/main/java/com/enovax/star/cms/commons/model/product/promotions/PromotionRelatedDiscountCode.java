package com.enovax.star.cms.commons.model.product.promotions;

import java.util.Date;

/**
 * Created by jensen on 25/6/16.
 */
public class PromotionRelatedDiscountCode {
    private String recordId;
    private String barcode;
    private String discountCode;
    private String systemName;
    private String relatedOfferId;
    private boolean systemActive;
    private Date validFrom;
    private Date validTo;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getRelatedOfferId() {
        return relatedOfferId;
    }

    public void setRelatedOfferId(String relatedOfferId) {
        this.relatedOfferId = relatedOfferId;
    }

    public boolean isSystemActive() {
        return systemActive;
    }

    public void setSystemActive(boolean systemActive) {
        this.systemActive = systemActive;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
