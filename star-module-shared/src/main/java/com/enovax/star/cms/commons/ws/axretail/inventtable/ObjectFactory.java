
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axretail.inventtable package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfEventGroupTableEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfEventGroupTableEntity");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _EventAllocation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventAllocation");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _SDCInventTableExt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_InventTableExt");
    private final static QName _ArrayOfEventGroupLineEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfEventGroupLineEntity");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _EventGroupTableEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupTableEntity");
    private final static QName _PackageLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLine");
    private final static QName _SDCTemplateXMLEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_TemplateXMLEntity");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCInventTableExtCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_InventTableExtCriteria");
    private final static QName _ArrayOfSDCTemplateXMLEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_TemplateXMLEntity");
    private final static QName _ArrayOfPackageLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfPackageLine");
    private final static QName _SDCTemplateXMLCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_TemplateXMLCriteria");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _SDCTemplateXMLSearchResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_TemplateXMLSearchResponse");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _InventTableVariantExt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InventTableVariantExt");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _ArrayOfEventAllocation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfEventAllocation");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _ArrayOfSDCInventTableExt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_InventTableExt");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _EventGroupLineEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupLineEntity");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _SDCInventTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_InventTableResponse");
    private final static QName _ArrayOfInventTableVariantExt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfInventTableVariantExt");
    private final static QName _SearchProductExtSDCInventTableExtCriteria_QNAME = new QName("http://tempuri.org/", "SDC_InventTableExtCriteria");
    private final static QName _SDCTemplateXMLCriteriaTemplateName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TemplateName");
    private final static QName _SDCInventTableResponseInventTableExtCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "InventTableExtCollection");
    private final static QName _SDCInventTableExtCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCInventTableExtCriteriaDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DataAreaId");
    private final static QName _SDCInventTableExtProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductName");
    private final static QName _SDCInventTableExtDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DataAreaId");
    private final static QName _SDCInventTableExtOperationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OperationId");
    private final static QName _SDCInventTableExtUsageValidityRuleId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "UsageValidityRuleId");
    private final static QName _SDCInventTableExtEventGroupTableCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupTableCollection");
    private final static QName _SDCInventTableExtEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupId");
    private final static QName _SDCInventTableExtTemplateDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TemplateDescription");
    private final static QName _SDCInventTableExtItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ItemId");
    private final static QName _SDCInventTableExtDefaultEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DefaultEventLineId");
    private final static QName _SDCInventTableExtOpenValidityRuleId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityRuleId");
    private final static QName _SDCInventTableExtPackageLinesCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLinesCollection");
    private final static QName _SDCInventTableExtFacilityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "FacilityId");
    private final static QName _SDCInventTableExtAbsoluteCapacityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AbsoluteCapacityId");
    private final static QName _SDCInventTableExtMediaTypeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "MediaTypeDescription");
    private final static QName _SDCInventTableExtMediaTypeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "MediaTypeId");
    private final static QName _SDCInventTableExtAccessId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AccessId");
    private final static QName _SDCInventTableExtInventTableVariantExtCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InventTableVariantExtCollection");
    private final static QName _SDCInventTableExtNecMembershipTypeCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Nec_MembershipTypeCode");
    private final static QName _SDCInventTableExtTemplateName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TemplateName");
    private final static QName _SDCInventTableExtPackageId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageId");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCTemplateXMLSearchResponseTemplateXMLCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "TemplateXMLCollection");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SearchXmlTemplateResponseSearchXmlTemplateResult_QNAME = new QName("http://tempuri.org/", "SearchXmlTemplateResult");
    private final static QName _EventGroupTableEntityEventGroupLinesCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupLinesCollection");
    private final static QName _EventGroupTableEntityDataAreaID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DataAreaID");
    private final static QName _EventGroupTableEntityCapacityCalendarId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CapacityCalendarId");
    private final static QName _SearchProductExtResponseSearchProductExtResult_QNAME = new QName("http://tempuri.org/", "SearchProductExtResult");
    private final static QName _PackageLineRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RetailVariantId");
    private final static QName _PackageLineDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Description");
    private final static QName _PackageLineLineGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LineGroup");
    private final static QName _SDCTemplateXMLEntityBlobData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "BlobData");
    private final static QName _SDCTemplateXMLEntityXmlDocument_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "XmlDocument");
    private final static QName _SearchXmlTemplateTemplateXMLCriteria_QNAME = new QName("http://tempuri.org/", "TemplateXMLCriteria");
    private final static QName _EventAllocationEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventLineId");
    private final static QName _InventTableVariantExtInventDimId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InventDimId");
    private final static QName _InventTableVariantExtName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Name");
    private final static QName _EventGroupLineEntityEventAllocationCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventAllocationCollection");
    private final static QName _EventGroupLineEntityEventCapacityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventCapacityId");
    private final static QName _EventGroupLineEntityEventName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axretail.inventtable
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SDCInventTableExtCriteria }
     * 
     */
    public SDCInventTableExtCriteria createSDCInventTableExtCriteria() {
        return new SDCInventTableExtCriteria();
    }

    /**
     * Create an instance of {@link SDCTemplateXMLCriteria }
     * 
     */
    public SDCTemplateXMLCriteria createSDCTemplateXMLCriteria() {
        return new SDCTemplateXMLCriteria();
    }

    /**
     * Create an instance of {@link SearchXmlTemplateResponse }
     * 
     */
    public SearchXmlTemplateResponse createSearchXmlTemplateResponse() {
        return new SearchXmlTemplateResponse();
    }

    /**
     * Create an instance of {@link SDCTemplateXMLSearchResponse }
     * 
     */
    public SDCTemplateXMLSearchResponse createSDCTemplateXMLSearchResponse() {
        return new SDCTemplateXMLSearchResponse();
    }

    /**
     * Create an instance of {@link SearchXmlTemplate }
     * 
     */
    public SearchXmlTemplate createSearchXmlTemplate() {
        return new SearchXmlTemplate();
    }

    /**
     * Create an instance of {@link SearchProductExt }
     * 
     */
    public SearchProductExt createSearchProductExt() {
        return new SearchProductExt();
    }

    /**
     * Create an instance of {@link SearchProductExtResponse }
     * 
     */
    public SearchProductExtResponse createSearchProductExtResponse() {
        return new SearchProductExtResponse();
    }

    /**
     * Create an instance of {@link SDCInventTableResponse }
     * 
     */
    public SDCInventTableResponse createSDCInventTableResponse() {
        return new SDCInventTableResponse();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link ArrayOfEventAllocation }
     * 
     */
    public ArrayOfEventAllocation createArrayOfEventAllocation() {
        return new ArrayOfEventAllocation();
    }

    /**
     * Create an instance of {@link EventAllocation }
     * 
     */
    public EventAllocation createEventAllocation() {
        return new EventAllocation();
    }

    /**
     * Create an instance of {@link ArrayOfEventGroupTableEntity }
     * 
     */
    public ArrayOfEventGroupTableEntity createArrayOfEventGroupTableEntity() {
        return new ArrayOfEventGroupTableEntity();
    }

    /**
     * Create an instance of {@link SDCInventTableExt }
     * 
     */
    public SDCInventTableExt createSDCInventTableExt() {
        return new SDCInventTableExt();
    }

    /**
     * Create an instance of {@link ArrayOfEventGroupLineEntity }
     * 
     */
    public ArrayOfEventGroupLineEntity createArrayOfEventGroupLineEntity() {
        return new ArrayOfEventGroupLineEntity();
    }

    /**
     * Create an instance of {@link ArrayOfSDCInventTableExt }
     * 
     */
    public ArrayOfSDCInventTableExt createArrayOfSDCInventTableExt() {
        return new ArrayOfSDCInventTableExt();
    }

    /**
     * Create an instance of {@link EventGroupTableEntity }
     * 
     */
    public EventGroupTableEntity createEventGroupTableEntity() {
        return new EventGroupTableEntity();
    }

    /**
     * Create an instance of {@link PackageLine }
     * 
     */
    public PackageLine createPackageLine() {
        return new PackageLine();
    }

    /**
     * Create an instance of {@link SDCTemplateXMLEntity }
     * 
     */
    public SDCTemplateXMLEntity createSDCTemplateXMLEntity() {
        return new SDCTemplateXMLEntity();
    }

    /**
     * Create an instance of {@link EventGroupLineEntity }
     * 
     */
    public EventGroupLineEntity createEventGroupLineEntity() {
        return new EventGroupLineEntity();
    }

    /**
     * Create an instance of {@link ArrayOfSDCTemplateXMLEntity }
     * 
     */
    public ArrayOfSDCTemplateXMLEntity createArrayOfSDCTemplateXMLEntity() {
        return new ArrayOfSDCTemplateXMLEntity();
    }

    /**
     * Create an instance of {@link ArrayOfPackageLine }
     * 
     */
    public ArrayOfPackageLine createArrayOfPackageLine() {
        return new ArrayOfPackageLine();
    }

    /**
     * Create an instance of {@link InventTableVariantExt }
     * 
     */
    public InventTableVariantExt createInventTableVariantExt() {
        return new InventTableVariantExt();
    }

    /**
     * Create an instance of {@link ArrayOfInventTableVariantExt }
     * 
     */
    public ArrayOfInventTableVariantExt createArrayOfInventTableVariantExt() {
        return new ArrayOfInventTableVariantExt();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventGroupTableEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfEventGroupTableEntity")
    public JAXBElement<ArrayOfEventGroupTableEntity> createArrayOfEventGroupTableEntity(ArrayOfEventGroupTableEntity value) {
        return new JAXBElement<ArrayOfEventGroupTableEntity>(_ArrayOfEventGroupTableEntity_QNAME, ArrayOfEventGroupTableEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventAllocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventAllocation")
    public JAXBElement<EventAllocation> createEventAllocation(EventAllocation value) {
        return new JAXBElement<EventAllocation>(_EventAllocation_QNAME, EventAllocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInventTableExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_InventTableExt")
    public JAXBElement<SDCInventTableExt> createSDCInventTableExt(SDCInventTableExt value) {
        return new JAXBElement<SDCInventTableExt>(_SDCInventTableExt_QNAME, SDCInventTableExt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventGroupLineEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfEventGroupLineEntity")
    public JAXBElement<ArrayOfEventGroupLineEntity> createArrayOfEventGroupLineEntity(ArrayOfEventGroupLineEntity value) {
        return new JAXBElement<ArrayOfEventGroupLineEntity>(_ArrayOfEventGroupLineEntity_QNAME, ArrayOfEventGroupLineEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventGroupTableEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupTableEntity")
    public JAXBElement<EventGroupTableEntity> createEventGroupTableEntity(EventGroupTableEntity value) {
        return new JAXBElement<EventGroupTableEntity>(_EventGroupTableEntity_QNAME, EventGroupTableEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLine")
    public JAXBElement<PackageLine> createPackageLine(PackageLine value) {
        return new JAXBElement<PackageLine>(_PackageLine_QNAME, PackageLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTemplateXMLEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_TemplateXMLEntity")
    public JAXBElement<SDCTemplateXMLEntity> createSDCTemplateXMLEntity(SDCTemplateXMLEntity value) {
        return new JAXBElement<SDCTemplateXMLEntity>(_SDCTemplateXMLEntity_QNAME, SDCTemplateXMLEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInventTableExtCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_InventTableExtCriteria")
    public JAXBElement<SDCInventTableExtCriteria> createSDCInventTableExtCriteria(SDCInventTableExtCriteria value) {
        return new JAXBElement<SDCInventTableExtCriteria>(_SDCInventTableExtCriteria_QNAME, SDCInventTableExtCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCTemplateXMLEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_TemplateXMLEntity")
    public JAXBElement<ArrayOfSDCTemplateXMLEntity> createArrayOfSDCTemplateXMLEntity(ArrayOfSDCTemplateXMLEntity value) {
        return new JAXBElement<ArrayOfSDCTemplateXMLEntity>(_ArrayOfSDCTemplateXMLEntity_QNAME, ArrayOfSDCTemplateXMLEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPackageLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfPackageLine")
    public JAXBElement<ArrayOfPackageLine> createArrayOfPackageLine(ArrayOfPackageLine value) {
        return new JAXBElement<ArrayOfPackageLine>(_ArrayOfPackageLine_QNAME, ArrayOfPackageLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTemplateXMLCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_TemplateXMLCriteria")
    public JAXBElement<SDCTemplateXMLCriteria> createSDCTemplateXMLCriteria(SDCTemplateXMLCriteria value) {
        return new JAXBElement<SDCTemplateXMLCriteria>(_SDCTemplateXMLCriteria_QNAME, SDCTemplateXMLCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTemplateXMLSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_TemplateXMLSearchResponse")
    public JAXBElement<SDCTemplateXMLSearchResponse> createSDCTemplateXMLSearchResponse(SDCTemplateXMLSearchResponse value) {
        return new JAXBElement<SDCTemplateXMLSearchResponse>(_SDCTemplateXMLSearchResponse_QNAME, SDCTemplateXMLSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InventTableVariantExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InventTableVariantExt")
    public JAXBElement<InventTableVariantExt> createInventTableVariantExt(InventTableVariantExt value) {
        return new JAXBElement<InventTableVariantExt>(_InventTableVariantExt_QNAME, InventTableVariantExt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventAllocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfEventAllocation")
    public JAXBElement<ArrayOfEventAllocation> createArrayOfEventAllocation(ArrayOfEventAllocation value) {
        return new JAXBElement<ArrayOfEventAllocation>(_ArrayOfEventAllocation_QNAME, ArrayOfEventAllocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCInventTableExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_InventTableExt")
    public JAXBElement<ArrayOfSDCInventTableExt> createArrayOfSDCInventTableExt(ArrayOfSDCInventTableExt value) {
        return new JAXBElement<ArrayOfSDCInventTableExt>(_ArrayOfSDCInventTableExt_QNAME, ArrayOfSDCInventTableExt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventGroupLineEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupLineEntity")
    public JAXBElement<EventGroupLineEntity> createEventGroupLineEntity(EventGroupLineEntity value) {
        return new JAXBElement<EventGroupLineEntity>(_EventGroupLineEntity_QNAME, EventGroupLineEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInventTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_InventTableResponse")
    public JAXBElement<SDCInventTableResponse> createSDCInventTableResponse(SDCInventTableResponse value) {
        return new JAXBElement<SDCInventTableResponse>(_SDCInventTableResponse_QNAME, SDCInventTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInventTableVariantExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfInventTableVariantExt")
    public JAXBElement<ArrayOfInventTableVariantExt> createArrayOfInventTableVariantExt(ArrayOfInventTableVariantExt value) {
        return new JAXBElement<ArrayOfInventTableVariantExt>(_ArrayOfInventTableVariantExt_QNAME, ArrayOfInventTableVariantExt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInventTableExtCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SDC_InventTableExtCriteria", scope = SearchProductExt.class)
    public JAXBElement<SDCInventTableExtCriteria> createSearchProductExtSDCInventTableExtCriteria(SDCInventTableExtCriteria value) {
        return new JAXBElement<SDCInventTableExtCriteria>(_SearchProductExtSDCInventTableExtCriteria_QNAME, SDCInventTableExtCriteria.class, SearchProductExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TemplateName", scope = SDCTemplateXMLCriteria.class)
    public JAXBElement<String> createSDCTemplateXMLCriteriaTemplateName(String value) {
        return new JAXBElement<String>(_SDCTemplateXMLCriteriaTemplateName_QNAME, String.class, SDCTemplateXMLCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCInventTableExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "InventTableExtCollection", scope = SDCInventTableResponse.class)
    public JAXBElement<ArrayOfSDCInventTableExt> createSDCInventTableResponseInventTableExtCollection(ArrayOfSDCInventTableExt value) {
        return new JAXBElement<ArrayOfSDCInventTableExt>(_SDCInventTableResponseInventTableExtCollection_QNAME, ArrayOfSDCInventTableExt.class, SDCInventTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCInventTableExtCriteria.class)
    public JAXBElement<String> createSDCInventTableExtCriteriaItemId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtCriteriaItemId_QNAME, String.class, SDCInventTableExtCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DataAreaId", scope = SDCInventTableExtCriteria.class)
    public JAXBElement<String> createSDCInventTableExtCriteriaDataAreaId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtCriteriaDataAreaId_QNAME, String.class, SDCInventTableExtCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtProductName(String value) {
        return new JAXBElement<String>(_SDCInventTableExtProductName_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtDataAreaId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtDataAreaId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OperationId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtOperationId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtOperationId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityRuleId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtUsageValidityRuleId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtUsageValidityRuleId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventGroupTableEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupTableCollection", scope = SDCInventTableExt.class)
    public JAXBElement<ArrayOfEventGroupTableEntity> createSDCInventTableExtEventGroupTableCollection(ArrayOfEventGroupTableEntity value) {
        return new JAXBElement<ArrayOfEventGroupTableEntity>(_SDCInventTableExtEventGroupTableCollection_QNAME, ArrayOfEventGroupTableEntity.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtEventGroupId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtEventGroupId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateDescription", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtTemplateDescription(String value) {
        return new JAXBElement<String>(_SDCInventTableExtTemplateDescription_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtItemId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtItemId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DefaultEventLineId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtDefaultEventLineId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtDefaultEventLineId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityRuleId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtOpenValidityRuleId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtOpenValidityRuleId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPackageLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLinesCollection", scope = SDCInventTableExt.class)
    public JAXBElement<ArrayOfPackageLine> createSDCInventTableExtPackageLinesCollection(ArrayOfPackageLine value) {
        return new JAXBElement<ArrayOfPackageLine>(_SDCInventTableExtPackageLinesCollection_QNAME, ArrayOfPackageLine.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "FacilityId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtFacilityId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtFacilityId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AbsoluteCapacityId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtAbsoluteCapacityId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtAbsoluteCapacityId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MediaTypeDescription", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtMediaTypeDescription(String value) {
        return new JAXBElement<String>(_SDCInventTableExtMediaTypeDescription_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MediaTypeId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtMediaTypeId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtMediaTypeId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AccessId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtAccessId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtAccessId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInventTableVariantExt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InventTableVariantExtCollection", scope = SDCInventTableExt.class)
    public JAXBElement<ArrayOfInventTableVariantExt> createSDCInventTableExtInventTableVariantExtCollection(ArrayOfInventTableVariantExt value) {
        return new JAXBElement<ArrayOfInventTableVariantExt>(_SDCInventTableExtInventTableVariantExtCollection_QNAME, ArrayOfInventTableVariantExt.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Nec_MembershipTypeCode", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtNecMembershipTypeCode(String value) {
        return new JAXBElement<String>(_SDCInventTableExtNecMembershipTypeCode_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateName", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtTemplateName(String value) {
        return new JAXBElement<String>(_SDCInventTableExtTemplateName_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = SDCInventTableExt.class)
    public JAXBElement<String> createSDCInventTableExtPackageId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtPackageId_QNAME, String.class, SDCInventTableExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCTemplateXMLEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "TemplateXMLCollection", scope = SDCTemplateXMLSearchResponse.class)
    public JAXBElement<ArrayOfSDCTemplateXMLEntity> createSDCTemplateXMLSearchResponseTemplateXMLCollection(ArrayOfSDCTemplateXMLEntity value) {
        return new JAXBElement<ArrayOfSDCTemplateXMLEntity>(_SDCTemplateXMLSearchResponseTemplateXMLCollection_QNAME, ArrayOfSDCTemplateXMLEntity.class, SDCTemplateXMLSearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTemplateXMLSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SearchXmlTemplateResult", scope = SearchXmlTemplateResponse.class)
    public JAXBElement<SDCTemplateXMLSearchResponse> createSearchXmlTemplateResponseSearchXmlTemplateResult(SDCTemplateXMLSearchResponse value) {
        return new JAXBElement<SDCTemplateXMLSearchResponse>(_SearchXmlTemplateResponseSearchXmlTemplateResult_QNAME, SDCTemplateXMLSearchResponse.class, SearchXmlTemplateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventGroupLineEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupLinesCollection", scope = EventGroupTableEntity.class)
    public JAXBElement<ArrayOfEventGroupLineEntity> createEventGroupTableEntityEventGroupLinesCollection(ArrayOfEventGroupLineEntity value) {
        return new JAXBElement<ArrayOfEventGroupLineEntity>(_EventGroupTableEntityEventGroupLinesCollection_QNAME, ArrayOfEventGroupLineEntity.class, EventGroupTableEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaID", scope = EventGroupTableEntity.class)
    public JAXBElement<String> createEventGroupTableEntityDataAreaID(String value) {
        return new JAXBElement<String>(_EventGroupTableEntityDataAreaID_QNAME, String.class, EventGroupTableEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CapacityCalendarId", scope = EventGroupTableEntity.class)
    public JAXBElement<String> createEventGroupTableEntityCapacityCalendarId(String value) {
        return new JAXBElement<String>(_EventGroupTableEntityCapacityCalendarId_QNAME, String.class, EventGroupTableEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = EventGroupTableEntity.class)
    public JAXBElement<String> createEventGroupTableEntityEventGroupId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtEventGroupId_QNAME, String.class, EventGroupTableEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInventTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SearchProductExtResult", scope = SearchProductExtResponse.class)
    public JAXBElement<SDCInventTableResponse> createSearchProductExtResponseSearchProductExtResult(SDCInventTableResponse value) {
        return new JAXBElement<SDCInventTableResponse>(_SearchProductExtResponseSearchProductExtResult_QNAME, SDCInventTableResponse.class, SearchProductExtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AbsoluteCapacityId", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineAbsoluteCapacityId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtAbsoluteCapacityId_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineRetailVariantId(String value) {
        return new JAXBElement<String>(_PackageLineRetailVariantId_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Description", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineDescription(String value) {
        return new JAXBElement<String>(_PackageLineDescription_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineDataAreaId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtDataAreaId_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LineGroup", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineLineGroup(String value) {
        return new JAXBElement<String>(_PackageLineLineGroup_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = PackageLine.class)
    public JAXBElement<String> createPackageLinePackageId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtPackageId_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = PackageLine.class)
    public JAXBElement<String> createPackageLineItemId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtItemId_QNAME, String.class, PackageLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateName", scope = SDCTemplateXMLEntity.class)
    public JAXBElement<String> createSDCTemplateXMLEntityTemplateName(String value) {
        return new JAXBElement<String>(_SDCInventTableExtTemplateName_QNAME, String.class, SDCTemplateXMLEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateDescription", scope = SDCTemplateXMLEntity.class)
    public JAXBElement<String> createSDCTemplateXMLEntityTemplateDescription(String value) {
        return new JAXBElement<String>(_SDCInventTableExtTemplateDescription_QNAME, String.class, SDCTemplateXMLEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "BlobData", scope = SDCTemplateXMLEntity.class)
    public JAXBElement<byte[]> createSDCTemplateXMLEntityBlobData(byte[] value) {
        return new JAXBElement<byte[]>(_SDCTemplateXMLEntityBlobData_QNAME, byte[].class, SDCTemplateXMLEntity.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "XmlDocument", scope = SDCTemplateXMLEntity.class)
    public JAXBElement<String> createSDCTemplateXMLEntityXmlDocument(String value) {
        return new JAXBElement<String>(_SDCTemplateXMLEntityXmlDocument_QNAME, String.class, SDCTemplateXMLEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTemplateXMLCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "TemplateXMLCriteria", scope = SearchXmlTemplate.class)
    public JAXBElement<SDCTemplateXMLCriteria> createSearchXmlTemplateTemplateXMLCriteria(SDCTemplateXMLCriteria value) {
        return new JAXBElement<SDCTemplateXMLCriteria>(_SearchXmlTemplateTemplateXMLCriteria_QNAME, SDCTemplateXMLCriteria.class, SearchXmlTemplate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = EventAllocation.class)
    public JAXBElement<String> createEventAllocationEventLineId(String value) {
        return new JAXBElement<String>(_EventAllocationEventLineId_QNAME, String.class, EventAllocation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = EventAllocation.class)
    public JAXBElement<String> createEventAllocationEventGroupId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtEventGroupId_QNAME, String.class, EventAllocation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AbsoluteCapacityId", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtAbsoluteCapacityId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtAbsoluteCapacityId_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtRetailVariantId(String value) {
        return new JAXBElement<String>(_PackageLineRetailVariantId_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Description", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtDescription(String value) {
        return new JAXBElement<String>(_PackageLineDescription_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtPackageId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtPackageId_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InventDimId", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtInventDimId(String value) {
        return new JAXBElement<String>(_InventTableVariantExtInventDimId_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Name", scope = InventTableVariantExt.class)
    public JAXBElement<String> createInventTableVariantExtName(String value) {
        return new JAXBElement<String>(_InventTableVariantExtName_QNAME, String.class, InventTableVariantExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEventAllocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventAllocationCollection", scope = EventGroupLineEntity.class)
    public JAXBElement<ArrayOfEventAllocation> createEventGroupLineEntityEventAllocationCollection(ArrayOfEventAllocation value) {
        return new JAXBElement<ArrayOfEventAllocation>(_EventGroupLineEntityEventAllocationCollection_QNAME, ArrayOfEventAllocation.class, EventGroupLineEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventCapacityId", scope = EventGroupLineEntity.class)
    public JAXBElement<String> createEventGroupLineEntityEventCapacityId(String value) {
        return new JAXBElement<String>(_EventGroupLineEntityEventCapacityId_QNAME, String.class, EventGroupLineEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = EventGroupLineEntity.class)
    public JAXBElement<String> createEventGroupLineEntityEventLineId(String value) {
        return new JAXBElement<String>(_EventAllocationEventLineId_QNAME, String.class, EventGroupLineEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaID", scope = EventGroupLineEntity.class)
    public JAXBElement<String> createEventGroupLineEntityDataAreaID(String value) {
        return new JAXBElement<String>(_EventGroupTableEntityDataAreaID_QNAME, String.class, EventGroupLineEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = EventGroupLineEntity.class)
    public JAXBElement<String> createEventGroupLineEntityEventGroupId(String value) {
        return new JAXBElement<String>(_SDCInventTableExtEventGroupId_QNAME, String.class, EventGroupLineEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventName", scope = EventGroupLineEntity.class)
    public JAXBElement<String> createEventGroupLineEntityEventName(String value) {
        return new JAXBElement<String>(_EventGroupLineEntityEventName_QNAME, String.class, EventGroupLineEntity.class, value);
    }

}
