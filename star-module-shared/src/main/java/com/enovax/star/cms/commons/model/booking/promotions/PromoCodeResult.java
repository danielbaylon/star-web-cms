package com.enovax.star.cms.commons.model.booking.promotions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 7/9/16.
 */
public class PromoCodeResult implements Serializable {

  private String promoCode;
  private Boolean redirect;
  private String redirectUrl;

  private List<TicketUpdateModel> updatedTickets = new ArrayList<>();
  private List<UnlockedTicketModel> unlockedTickets = new ArrayList<>();

  public PromoCodeResult() {

  }

  public String getPromoCode() {
    return promoCode;
  }

  public void setPromoCode(String promoCode) {
    this.promoCode = promoCode;
  }

  public List<TicketUpdateModel> getUpdatedTickets() {
    return updatedTickets;
  }

  public void setUpdatedTickets(List<TicketUpdateModel> updatedTickets) {
    this.updatedTickets = updatedTickets;
  }

  public List<UnlockedTicketModel> getUnlockedTickets() {
    return unlockedTickets;
  }

  public void setUnlockedTickets(List<UnlockedTicketModel> unlockedTickets) {
    this.unlockedTickets = unlockedTickets;
  }

  public Boolean getRedirect() {
    return redirect;
  }

  public void setRedirect(Boolean redirect) {
    this.redirect = redirect;
  }

  public String getRedirectUrl() {
    return redirectUrl;
  }

  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }
}
