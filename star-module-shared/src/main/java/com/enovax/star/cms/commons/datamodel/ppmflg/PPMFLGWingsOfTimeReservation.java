package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 7/13/16.
 */
@Entity
@Table(name = PPMFLGWingsOfTimeReservation.TABLE_NAME)
public class PPMFLGWingsOfTimeReservation {
    public static final String TABLE_NAME = "PPMFLGWingsOfTimeReservation";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "mainAccountId")
    private Integer mainAccountId;

    @Column(name = "reservationType")
    private String reservationType;

    @Column(name = "isAdminRequest")
    private boolean isAdminRequest;

    @Column(name = "username")
    private String username;

    @Column(name = "isSubAccountTrans")
    private boolean isSubAccountTrans;

    @Column(name = "splitSrcId")
    private Integer splitSrcId;

    @Column(name = "splitSrcQty")
    private Integer splitSrcQty;

    @Column(name = "receiptNum")
    private String receiptNum;

    @Column(name = "pinCode")
    private String pinCode;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "status")
    private String status;

    @Column(name = "productId")
    private String productId;

    @Column(name = "eventLineId")
    private String eventLineId;

    @Column(name = "eventName")
    private String eventName;

    @Column(name = "eventDate")
    private Date eventDate;

    @Column(name = "eventStartTime")
    private Long eventStartTime;

    @Column(name = "eventTime")
    private Long eventTime;

    @Column(name = "eventEndTime")
    private Long eventEndTime;

    @Column(name = "eventCapacityId")
    private String eventCapacityId;

    @Column(name = "qtySold")
    private Integer qtySold;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "qtyRedeemed")
    private Integer qtyRedeemed;

    @Column(name = "qtyReturned")
    private Integer qtyReturned;

    @Column(name = "qtyUnredeemed")
    private Integer qtyUnredeemed;

    @Column(name = "ticketPrice")
    private BigDecimal ticketPrice;

    @Column(name ="createdDate")
    private Date createdDate;

    @Column(name ="createdBy")
    private String createdBy;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    @Column(name ="modifiedBy")
    private String modifiedBy;

    @Column(name = "itemId")
    private String itemId;

    @Column(name = "mediaTypeId")
    private String mediaTypeId;

    @Column(name = "eventGroupId")
    private String eventGroupId;

    @Column(name = "openValidityEndDate")
    private Date openValidityEndDate;

    @Column(name = "openValidityStartDate")
    private Date openValidityStartDate;

    @Column(name = "receiptEmail")
    private String receiptEmail;

    @Column(name = "axCartId")
    private String axCartId;

    @Column(name = "axCheckoutCartId")
    private String axCheckoutCartId;

    @Column(name = "axCheckoutLineId")
    private String axCheckoutLineId;

    //@Column(name = "axLineId")
    //private String axLineId;

    //@Column(name = "axPrice")
    //private BigDecimal axPrice;

    //@Column(name = "axExternalPrice")
    //private BigDecimal axExternalPrice;

    //@Column(name = "axTaxAmt")
    //private BigDecimal axTaxAmt;

    //@Column(name = "axItemTaxGroupId")
    //private String axItemTaxGroupId;

    //@Column(name = "axTotalAmt")
    //private BigDecimal axTotalAmt;

    //@Column(name = "axSubtotalAmt")
    //private BigDecimal axSubtotalAmt;

    //@Column(name = "axNetAmtWoTax")
    //private BigDecimal axNetAmtWoTax;

    //@Column(name = "axDiscountAmt")
    //private BigDecimal axDiscountAmt;

    //@Column(name = "axrtId")
    //private String axrtId;
    //@Column(name = "axrtValChannelId")
    //private Long axrtValChannelId;
    @Column(name = "axrtConfirmationId")
    private String axrtConfirmationId;
    @Column(name = "axrtSalesOrderStatus")
    private String axrtSalesOrderStatus;
    @Column(name = "axrtSalesId")
    private String axrtSalesId;
    //@Column(name = "axrtRequestedDeliveryDate")
    //private String axrtRequestedDeliveryDate;
    //@Column(name = "axrtPaymentStatus")
    //private Integer axrtPaymentStatus;
    //@Column(name = "axrtOrderPlacedDate")
    //private String axrtOrderPlacedDate;
    //@Column(name = "axrtChargeAmountWithCurrency")
    //private String axrtChargeAmountWithCurrency;
    //@Column(name = "axrtDeliveryModeDescription")
    //private String axrtDeliveryModeDescription;
    //@Column(name = "axrtDeliveryModeId")
    //private String axrtDeliveryModeId;
    //@Column(name = "axrtDiscountAmount")
    //private BigDecimal axrtDiscountAmount;
    //@Column(name = "axrtSubtotalWithCurrency")
    //private String axrtSubtotalWithCurrency;
    //@Column(name = "axrtTaxAmountWithCurrency")
    //private String axrtTaxAmountWithCurrency;
    @Column(name = "axrtTotalAmount")
    private BigDecimal axrtTotalAmount;
    //@Column(name = "axrtTotalAmountWithCurrency")
    //private String axrtTotalAmountWithCurrency;
    @Column(name = "axrtItemId")
    private String axrtItemId;
    @Column(name = "axrtItemType")
    private String axrtItemType;
    @Column(name = "axrtLineId")
    private String axrtLineId;
    //@Column(name = "axrtNetAmountWithCurrency")
    //private String axrtNetAmountWithCurrency;
    //@Column(name = "axrtpPriceWithCurrency")
    //private String axrtpPriceWithCurrency;
    @Column(name = "axrtProductId")
    private Long axrtProductId;
    @Column(name = "axrtQuantity")
    private Integer axrtQuantity;

    @Column(name = "axReferenceId")
    private String axReferenceId;
    @Column(name = "axInventTransId")
    private String axInventTransId;

    @Column(name = "reserveNeedsActivation")
    private Boolean reserveNeedsActivation;
    @Column(name = "reserveOriginalTicketCode")
    private String reserveOriginalTicketCode;
    @Column(name = "reserveQtyGroup")
    private BigDecimal reserveQtyGroup;
    @Column(name = "reserveRecId")
    private Long reserveRecId;
    @Column(name = "reserveStartDate")
    private Date reserveStartDate;
    @Column(name = "reserveEndDate")
    private Date reserveEndDate;
    @Column(name = "reserveTicketCode")
    private String reserveTicketCode;
    @Column(name = "reserveTicketStatus")
    private Integer reserveTicketStatus;
    @Column(name = "reserveTransactionId")
    private String reserveTransactionId;

    @Column(name = "purchaseSrcId")
    private Integer purchaseSrcId;

    @Column(name = "purchaseSrcQty")
    private Integer purchaseSrcQty;

    @Column(name = "releaseNotifiedDate")
    private Date releaseNotifiedDate;

    @Column(name = "releasedDate")
    private Date releasedDate;

    @Column(name = "cancelNotifiedDate")
    private Date cancelNotifiedDate;

    @Column(name = "axChargeInvoice")
    private String axChargeInvoice;
    @Column(name = "axChargeVoucher")
    private String axChargeVoucher;
    @Column(name = "axChargeDocumentNum")
    private String axChargeDocumentNum;
    @Column(name = "axChargeAmount")
    private BigDecimal axChargeAmount;
    @Column(name = "axChargeQty")
    private Integer axChargeQty;

    @Column(name = "axRefundInvoice")
    private String axRefundInvoice;
    @Column(name = "axRefundVoucher")
    private String axRefundVoucher;
    @Column(name = "axRefundDocumentNum")
    private String axRefundDocumentNum;
    @Column(name = "axRefundAmount")
    private BigDecimal axRefundAmount;
    @Column(name = "axRefundQty")
    private Integer axRefundQty;

    @Column(name = "washeddown")
    private Integer washeddown;

    @Column(name = "qtyWashedDown")
    private Integer qtyWashedDown;

    @Column(name = "qtyAutoGenerated")
    private Integer qtyAutoGenerated;


    @Transient
    private Date eventCutOffTime;

    public Long getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(Long eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public Long getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(Long eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventCapacityId() {
        return eventCapacityId;
    }

    public void setEventCapacityId(String eventCapacityId) {
        this.eventCapacityId = eventCapacityId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isSubAccountTrans() {
        return isSubAccountTrans;
    }

    public void setIsSubAccountTrans(boolean isSubAccountTrans) {
        this.isSubAccountTrans = isSubAccountTrans;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public Integer getQtyUnredeemed() {
        return qtyUnredeemed;
    }

    public void setQtyUnredeemed(Integer qtyUnredeemed) {
        this.qtyUnredeemed = qtyUnredeemed;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public Date getOpenValidityEndDate() {
        return openValidityEndDate;
    }

    public void setOpenValidityEndDate(Date openValidityEndDate) {
        this.openValidityEndDate = openValidityEndDate;
    }

    public Date getOpenValidityStartDate() {
        return openValidityStartDate;
    }

    public void setOpenValidityStartDate(Date openValidityStartDate) {
        this.openValidityStartDate = openValidityStartDate;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public void setSubAccountTrans(boolean subAccountTrans) {
        isSubAccountTrans = subAccountTrans;
    }



    public String getAxCartId() {
        return axCartId;
    }

    public void setAxCartId(String axCartId) {
        this.axCartId = axCartId;
    }
//
//    public String getAxLineId() {
//        return axLineId;
//    }
//
//    public void setAxLineId(String axLineId) {
//        this.axLineId = axLineId;
//    }
//
//    public BigDecimal getAxPrice() {
//        return axPrice;
//    }
//
//    public void setAxPrice(BigDecimal axPrice) {
//        this.axPrice = axPrice;
//    }
//
//    public BigDecimal getAxExternalPrice() {
//        return axExternalPrice;
//    }
//
//    public void setAxExternalPrice(BigDecimal axExternalPrice) {
//        this.axExternalPrice = axExternalPrice;
//    }
//
//    public BigDecimal getAxTaxAmt() {
//        return axTaxAmt;
//    }
//
//    public void setAxTaxAmt(BigDecimal axTaxAmt) {
//        this.axTaxAmt = axTaxAmt;
//    }
//
//    public String getAxItemTaxGroupId() {
//        return axItemTaxGroupId;
//    }
//
//    public void setAxItemTaxGroupId(String axItemTaxGroupId) {
//        this.axItemTaxGroupId = axItemTaxGroupId;
//    }
//
//    public BigDecimal getAxTotalAmt() {
//        return axTotalAmt;
//    }
//
//    public void setAxTotalAmt(BigDecimal axTotalAmt) {
//        this.axTotalAmt = axTotalAmt;
//    }
//
//    public BigDecimal getAxSubtotalAmt() {
//        return axSubtotalAmt;
//    }
//
//    public void setAxSubtotalAmt(BigDecimal axSubtotalAmt) {
//        this.axSubtotalAmt = axSubtotalAmt;
//    }
//
//    public BigDecimal getAxNetAmtWoTax() {
//        return axNetAmtWoTax;
//    }
//
//    public void setAxNetAmtWoTax(BigDecimal axNetAmtWoTax) {
//        this.axNetAmtWoTax = axNetAmtWoTax;
//    }
//
    public String getAxCheckoutCartId() {
        return axCheckoutCartId;
    }

    public void setAxCheckoutCartId(String axCheckoutCartId) {
        this.axCheckoutCartId = axCheckoutCartId;
    }
//
//    public BigDecimal getAxDiscountAmt() {
//        return axDiscountAmt;
//    }
//
//    public void setAxDiscountAmt(BigDecimal axDiscountAmt) {
//        this.axDiscountAmt = axDiscountAmt;
//    }
//
//    public String getAxrtId() {
//        return axrtId;
//    }
//
//    public void setAxrtId(String axrtId) {
//        this.axrtId = axrtId;
//    }
//
//    public Long getAxrtValChannelId() {
//        return axrtValChannelId;
//    }
//
//    public void setAxrtValChannelId(Long axrtValChannelId) {
//        this.axrtValChannelId = axrtValChannelId;
//    }

    public String getAxrtConfirmationId() {
        return axrtConfirmationId;
    }

    public void setAxrtConfirmationId(String axrtConfirmationId) {
        this.axrtConfirmationId = axrtConfirmationId;
    }

    public String getAxrtSalesOrderStatus() {
        return axrtSalesOrderStatus;
    }

    public void setAxrtSalesOrderStatus(String axrtSalesOrderStatus) {
        this.axrtSalesOrderStatus = axrtSalesOrderStatus;
    }

    public String getAxrtSalesId() {
        return axrtSalesId;
    }

    public void setAxrtSalesId(String axrtSalesId) {
        this.axrtSalesId = axrtSalesId;
    }

//    public String getAxrtRequestedDeliveryDate() {
//        return axrtRequestedDeliveryDate;
//    }
//
//    public void setAxrtRequestedDeliveryDate(String axrtRequestedDeliveryDate) {
//        this.axrtRequestedDeliveryDate = axrtRequestedDeliveryDate;
//    }
//
//    public Integer getAxrtPaymentStatus() {
//        return axrtPaymentStatus;
//    }
//
//    public void setAxrtPaymentStatus(Integer axrtPaymentStatus) {
//        this.axrtPaymentStatus = axrtPaymentStatus;
//    }
//
//    public String getAxrtOrderPlacedDate() {
//        return axrtOrderPlacedDate;
//    }
//
//    public void setAxrtOrderPlacedDate(String axrtOrderPlacedDate) {
//        this.axrtOrderPlacedDate = axrtOrderPlacedDate;
//    }
//
//    public String getAxrtChargeAmountWithCurrency() {
//        return axrtChargeAmountWithCurrency;
//    }
//
//    public void setAxrtChargeAmountWithCurrency(String axrtChargeAmountWithCurrency) {
//        this.axrtChargeAmountWithCurrency = axrtChargeAmountWithCurrency;
//    }
//
//    public String getAxrtDeliveryModeDescription() {
//        return axrtDeliveryModeDescription;
//    }
//
//    public void setAxrtDeliveryModeDescription(String axrtDeliveryModeDescription) {
//        this.axrtDeliveryModeDescription = axrtDeliveryModeDescription;
//    }
//
//    public String getAxrtDeliveryModeId() {
//        return axrtDeliveryModeId;
//    }
//
//    public void setAxrtDeliveryModeId(String axrtDeliveryModeId) {
//        this.axrtDeliveryModeId = axrtDeliveryModeId;
//    }
//
//    public BigDecimal getAxrtDiscountAmount() {
//        return axrtDiscountAmount;
//    }
//
//    public void setAxrtDiscountAmount(BigDecimal axrtDiscountAmount) {
//        this.axrtDiscountAmount = axrtDiscountAmount;
//    }
//
//    public String getAxrtSubtotalWithCurrency() {
//        return axrtSubtotalWithCurrency;
//    }
//
//    public void setAxrtSubtotalWithCurrency(String axrtSubtotalWithCurrency) {
//        this.axrtSubtotalWithCurrency = axrtSubtotalWithCurrency;
//    }
//
//    public String getAxrtTaxAmountWithCurrency() {
//        return axrtTaxAmountWithCurrency;
//    }
//
//    public void setAxrtTaxAmountWithCurrency(String axrtTaxAmountWithCurrency) {
//        this.axrtTaxAmountWithCurrency = axrtTaxAmountWithCurrency;
//    }

    public BigDecimal getAxrtTotalAmount() {
        return axrtTotalAmount;
    }

    public void setAxrtTotalAmount(BigDecimal axrtTotalAmount) {
        this.axrtTotalAmount = axrtTotalAmount;
    }

//    public String getAxrtTotalAmountWithCurrency() {
//        return axrtTotalAmountWithCurrency;
//    }
//
//    public void setAxrtTotalAmountWithCurrency(String axrtTotalAmountWithCurrency) {
//        this.axrtTotalAmountWithCurrency = axrtTotalAmountWithCurrency;
//    }

    public String getAxrtItemId() {
        return axrtItemId;
    }

    public void setAxrtItemId(String axrtItemId) {
        this.axrtItemId = axrtItemId;
    }

    public String getAxrtItemType() {
        return axrtItemType;
    }

    public void setAxrtItemType(String axrtItemType) {
        this.axrtItemType = axrtItemType;
    }

    public String getAxrtLineId() {
        return axrtLineId;
    }

    public void setAxrtLineId(String axrtLineId) {
        this.axrtLineId = axrtLineId;
    }

//    public String getAxrtNetAmountWithCurrency() {
//        return axrtNetAmountWithCurrency;
//    }
//
//    public void setAxrtNetAmountWithCurrency(String axrtNetAmountWithCurrency) {
//        this.axrtNetAmountWithCurrency = axrtNetAmountWithCurrency;
//    }
//
//    public String getAxrtpPriceWithCurrency() {
//        return axrtpPriceWithCurrency;
//    }
//
//    public void setAxrtpPriceWithCurrency(String axrtpPriceWithCurrency) {
//        this.axrtpPriceWithCurrency = axrtpPriceWithCurrency;
//    }

    public Long getAxrtProductId() {
        return axrtProductId;
    }

    public void setAxrtProductId(Long axrtProductId) {
        this.axrtProductId = axrtProductId;
    }

    public Integer getAxrtQuantity() {
        return axrtQuantity;
    }

    public void setAxrtQuantity(Integer axrtQuantity) {
        this.axrtQuantity = axrtQuantity;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getAxCheckoutLineId() {
        return axCheckoutLineId;
    }

    public void setAxCheckoutLineId(String axCheckoutLineId) {
        this.axCheckoutLineId = axCheckoutLineId;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public String getAxReferenceId() {
        return axReferenceId;
    }

    public void setAxReferenceId(String axReferenceId) {
        this.axReferenceId = axReferenceId;
    }

    public String getAxInventTransId() {
        return axInventTransId;
    }

    public void setAxInventTransId(String axInventTransId) {
        this.axInventTransId = axInventTransId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Integer getQtyReturned() {
        return qtyReturned;
    }

    public void setQtyReturned(Integer qtyReturned) {
        this.qtyReturned = qtyReturned;
    }


    public Date getEventCutOffTime() {
        return eventCutOffTime;
    }

    public void setEventCutOffTime(Date eventCutOffTime) {
        this.eventCutOffTime = eventCutOffTime;
    }

    public String getReservationType() {
        return reservationType;
    }

    public void setReservationType(String reservationType) {
        this.reservationType = reservationType;
    }

    public boolean isAdminRequest() {
        return isAdminRequest;
    }

    public void setAdminRequest(Boolean adminRequest) {
        isAdminRequest = adminRequest;
    }

    public boolean isReserveNeedsActivation() {
        return reserveNeedsActivation;
    }

    public void setReserveNeedsActivation(Boolean reserveNeedsActivation) {
        this.reserveNeedsActivation = reserveNeedsActivation;
    }

    public String getReserveOriginalTicketCode() {
        return reserveOriginalTicketCode;
    }

    public void setReserveOriginalTicketCode(String reserveOriginalTicketCode) {
        this.reserveOriginalTicketCode = reserveOriginalTicketCode;
    }

    public BigDecimal getReserveQtyGroup() {
        return reserveQtyGroup;
    }

    public void setReserveQtyGroup(BigDecimal reserveQtyGroup) {
        this.reserveQtyGroup = reserveQtyGroup;
    }

    public Long getReserveRecId() {
        return reserveRecId;
    }

    public void setReserveRecId(Long reserveRecId) {
        this.reserveRecId = reserveRecId;
    }

    public Date getReserveStartDate() {
        return reserveStartDate;
    }

    public void setReserveStartDate(Date reserveStartDate) {
        this.reserveStartDate = reserveStartDate;
    }

    public Date getReserveEndDate() {
        return reserveEndDate;
    }

    public void setReserveEndDate(Date reserveEndDate) {
        this.reserveEndDate = reserveEndDate;
    }

    public String getReserveTicketCode() {
        return reserveTicketCode;
    }

    public void setReserveTicketCode(String reserveTicketCode) {
        this.reserveTicketCode = reserveTicketCode;
    }

    public Integer getReserveTicketStatus() {
        return reserveTicketStatus;
    }

    public void setReserveTicketStatus(Integer reserveTicketStatus) {
        this.reserveTicketStatus = reserveTicketStatus;
    }

    public String getReserveTransactionId() {
        return reserveTransactionId;
    }

    public void setReserveTransactionId(String reserveTransactionId) {
        this.reserveTransactionId = reserveTransactionId;
    }

    public void setAdminRequest(boolean adminRequest) {
        isAdminRequest = adminRequest;
    }

    public Boolean getReserveNeedsActivation() {
        return reserveNeedsActivation;
    }

    public Integer getSplitSrcId() {
        return splitSrcId;
    }

    public void setSplitSrcId(Integer splitSrcId) {
        this.splitSrcId = splitSrcId;
    }

    public Integer getSplitSrcQty() {
        return splitSrcQty;
    }

    public void setSplitSrcQty(Integer splitSrcQty) {
        this.splitSrcQty = splitSrcQty;
    }

    public Integer getPurchaseSrcId() {
        return purchaseSrcId;
    }

    public void setPurchaseSrcId(Integer purchaseSrcId) {
        this.purchaseSrcId = purchaseSrcId;
    }

    public Integer getPurchaseSrcQty() {
        return purchaseSrcQty;
    }

    public void setPurchaseSrcQty(Integer purchaseSrcQty) {
        this.purchaseSrcQty = purchaseSrcQty;
    }

    public Integer getQtySold() {
        return qtySold;
    }

    public void setQtySold(Integer qtySold) {
        this.qtySold = qtySold;
    }

    public Date getReleaseNotifiedDate() {
        return releaseNotifiedDate;
    }

    public void setReleaseNotifiedDate(Date releaseNotifiedDate) {
        this.releaseNotifiedDate = releaseNotifiedDate;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public Date getCancelNotifiedDate() {
        return cancelNotifiedDate;
    }

    public void setCancelNotifiedDate(Date cancelNotifiedDate) {
        this.cancelNotifiedDate = cancelNotifiedDate;
    }

    public Long getEventTime() {
        return eventTime;
    }

    public void setEventTime(Long eventTime) {
        this.eventTime = eventTime;
    }


    public String getAxChargeInvoice() {
        return axChargeInvoice;
    }

    public void setAxChargeInvoice(String axChargeInvoice) {
        this.axChargeInvoice = axChargeInvoice;
    }

    public String getAxChargeVoucher() {
        return axChargeVoucher;
    }

    public void setAxChargeVoucher(String axChargeVoucher) {
        this.axChargeVoucher = axChargeVoucher;
    }

    public String getAxChargeDocumentNum() {
        return axChargeDocumentNum;
    }

    public void setAxChargeDocumentNum(String axChargeDocumentNum) {
        this.axChargeDocumentNum = axChargeDocumentNum;
    }

    public BigDecimal getAxChargeAmount() {
        return axChargeAmount;
    }

    public void setAxChargeAmount(BigDecimal axChargeAmount) {
        this.axChargeAmount = axChargeAmount;
    }

    public String getAxRefundInvoice() {
        return axRefundInvoice;
    }

    public void setAxRefundInvoice(String axRefundInvoice) {
        this.axRefundInvoice = axRefundInvoice;
    }

    public String getAxRefundVoucher() {
        return axRefundVoucher;
    }

    public void setAxRefundVoucher(String axRefundVoucher) {
        this.axRefundVoucher = axRefundVoucher;
    }

    public String getAxRefundDocumentNum() {
        return axRefundDocumentNum;
    }

    public void setAxRefundDocumentNum(String axRefundDocumentNum) {
        this.axRefundDocumentNum = axRefundDocumentNum;
    }

    public BigDecimal getAxRefundAmount() {
        return axRefundAmount;
    }

    public void setAxRefundAmount(BigDecimal axRefundAmount) {
        this.axRefundAmount = axRefundAmount;
    }

    public Integer getAxChargeQty() {
        return axChargeQty;
    }

    public void setAxChargeQty(Integer axChargeQty) {
        this.axChargeQty = axChargeQty;
    }

    public Integer getAxRefundQty() {
        return axRefundQty;
    }

    public void setAxRefundQty(Integer axRefundQty) {
        this.axRefundQty = axRefundQty;
    }

    public PPMFLGWingsOfTimeReservation getDataCopy() {
        PPMFLGWingsOfTimeReservation c = new PPMFLGWingsOfTimeReservation();

        c.setId(this.getId());
        c.setMainAccountId(this.getMainAccountId());
        c.setReservationType(this.getReservationType());
        c.setAdminRequest(this.isAdminRequest());
        c.setUsername(this.getUsername());
        c.setIsSubAccountTrans(this.isSubAccountTrans);
        c.setSplitSrcId(this.getSplitSrcId());
        c.setSplitSrcQty(this.getSplitSrcQty());
        c.setReceiptNum(this.getReceiptNum());
        c.setPinCode(this.getPinCode());
        c.setRemarks(this.getRemarks());
        c.setStatus(this.getStatus());
        c.setProductId(this.getProductId());
        c.setEventLineId(this.getEventLineId());
        c.setEventName(this.getEventName());
        c.setEventDate(this.getEventDate());
        c.setEventStartTime(this.getEventStartTime());
        c.setEventTime(this.getEventTime());
        c.setEventEndTime(this.getEventEndTime());
        c.setEventCapacityId(this.getEventCapacityId());
        c.setQtySold(this.getQtySold());
        c.setQty(this.getQty());
        c.setQtyRedeemed(this.getQtyRedeemed());
        c.setQtyReturned(this.getQtyReturned());
        c.setQtyUnredeemed(this.getQtyUnredeemed());
        c.setTicketPrice(this.getTicketPrice());
        c.setCreatedDate(this.getCreatedDate());
        c.setCreatedBy(this.getCreatedBy());
        c.setModifiedDate(this.getModifiedDate());
        c.setModifiedBy(this.getModifiedBy());
        c.setItemId(this.getItemId());
        c.setMediaTypeId(this.getMediaTypeId());
        c.setEventGroupId(this.getEventGroupId());
        c.setOpenValidityEndDate(this.getOpenValidityEndDate());
        c.setOpenValidityStartDate(this.getOpenValidityStartDate());
        c.setReceiptEmail(this.getReceiptEmail());
        //c.setAxCartId(this.getAxCartId());
        //c.setAxCheckoutCartId(this.getAxCheckoutCartId());
        //c.setAxCheckoutLineId(this.getAxCheckoutLineId());
        //c.setAxLineId(this.getAxLineId());
        //c.setAxPrice(this.getAxPrice());
        //c.setAxExternalPrice(this.getAxExternalPrice());
        //c.setAxTaxAmt(this.getAxTaxAmt());
        //c.setAxItemTaxGroupId(this.getAxItemTaxGroupId());
        //c.setAxTotalAmt(this.getAxTotalAmt());
        //c.setAxSubtotalAmt(this.getAxSubtotalAmt());
        //c.setAxNetAmtWoTax(this.getAxNetAmtWoTax());
        //c.setAxDiscountAmt(this.getAxDiscountAmt());
        //c.setAxrtId(this.getAxrtId());
        //c.setAxrtValChannelId(this.getAxrtValChannelId());
        c.setAxrtConfirmationId(this.getAxrtConfirmationId());
        c.setAxrtSalesOrderStatus(this.getAxrtSalesOrderStatus());
        c.setAxrtSalesId(this.getAxrtSalesId());
        //c.setAxrtRequestedDeliveryDate(this.getAxrtRequestedDeliveryDate());
        //c.setAxrtPaymentStatus(this.getAxrtPaymentStatus());
        //c.setAxrtOrderPlacedDate(this.getAxrtOrderPlacedDate());
        //c.setAxrtChargeAmountWithCurrency(this.getAxrtChargeAmountWithCurrency());
        //c.setAxrtDeliveryModeDescription(this.getAxrtDeliveryModeDescription());
        //c.setAxrtDeliveryModeId(this.getAxrtDeliveryModeId());
        //c.setAxrtDiscountAmount(this.getAxrtDiscountAmount());
        //c.setAxrtSubtotalWithCurrency(this.getAxrtSubtotalWithCurrency());
        //c.setAxrtTaxAmountWithCurrency(this.getAxrtTaxAmountWithCurrency());
        c.setAxrtTotalAmount(this.getAxrtTotalAmount());
        //c.setAxrtTotalAmountWithCurrency(this.getAxrtTotalAmountWithCurrency());
        c.setAxrtItemId(this.getAxrtItemId());
        c.setAxrtItemType(this.getAxrtItemType());
        c.setAxrtLineId(this.getAxrtLineId());
        //c.setAxrtNetAmountWithCurrency(this.getAxrtNetAmountWithCurrency());
        //c.setAxrtpPriceWithCurrency(this.getAxrtpPriceWithCurrency());
        c.setAxrtProductId(this.getAxrtProductId());
        c.setAxrtQuantity(this.getAxrtQuantity());
        c.setAxReferenceId(this.getAxReferenceId());
        c.setAxInventTransId(this.getAxInventTransId());
        //c.setReserveNeedsActivation(this.getReserveNeedsActivation());
        c.setReserveOriginalTicketCode(this.getReserveOriginalTicketCode());
        c.setReserveQtyGroup(this.getReserveQtyGroup());
        c.setReserveRecId(this.getReserveRecId());
        c.setReserveStartDate(this.getReserveStartDate());
        c.setReserveEndDate(this.getReserveEndDate());
        c.setReserveTicketCode(this.getReserveTicketCode());
        c.setReserveTicketStatus(this.getReserveTicketStatus());
        c.setReserveTransactionId(this.getReserveTransactionId());
        c.setPurchaseSrcId(this.getPurchaseSrcId());
        c.setPurchaseSrcQty(this.getPurchaseSrcQty());
        c.setReleaseNotifiedDate(this.getReleaseNotifiedDate());
        c.setReleasedDate(this.getReleasedDate());
        c.setCancelNotifiedDate(this.getCancelNotifiedDate());
        c.setWasheddown(this.getWasheddown());
        c.setQtyWashedDown(this.getQtyWashedDown());
        c.setQtyAutoGenerated(this.getQtyAutoGenerated());
        return c;
    }

    public Integer getWasheddown() {
        return washeddown;
    }

    public void setWasheddown(Integer washeddown) {
        this.washeddown = washeddown;
    }

    public Integer getQtyWashedDown() {
        return qtyWashedDown;
    }

    public void setQtyWashedDown(Integer qtyWashedDown) {
        this.qtyWashedDown = qtyWashedDown;
    }

    public Integer getQtyAutoGenerated() {
        return qtyAutoGenerated;
    }

    public void setQtyAutoGenerated(Integer qtyAutoGenerated) {
        this.qtyAutoGenerated = qtyAutoGenerated;
    }
}
