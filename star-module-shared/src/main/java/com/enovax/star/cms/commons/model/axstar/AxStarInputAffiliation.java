package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarInputAffiliation {

    private String cartId;
    private String customerId;
    private List<Long> affiliatioIds;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<Long> getAffiliatioIds() {
        return affiliatioIds;
    }

    public void setAffiliatioIds(List<Long> affiliatioIds) {
        this.affiliatioIds = affiliatioIds;
    }
}
