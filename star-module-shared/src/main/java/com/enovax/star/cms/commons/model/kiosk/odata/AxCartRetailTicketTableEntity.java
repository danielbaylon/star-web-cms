package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_TicketingExtension.Entity.CartRetailTicketTableEntity
 * 
 * @author Justin
 *
 */
public class AxCartRetailTicketTableEntity {

    private String endDate;

    private String eventDate;

    private String eventGroupId;

    private String eventLineId;

    private String itemId;

    private String origTicketCode;

    private String pinCode;

    private String qtyGroup;

    private String startDate;

    private String ticketCode;

    private String ticketStatus;

    private String ticketTableId;

    private String transDate;

    private String modifiedDateTime;

    private String createdDateTime;

    private String dataAreaId;

    private String recId;

    private String transactionId;

    private String usageValidityId;

    private String openValidityId;

    private String productName;

    private String needActivation;

    private String channelId;

    private String lineId;

    private String nonReturnable;

    private String ticketCodePackage;

    private String isFailedSendTicket;

    private String receiptId;

    private String paxId;

    private String ticketPrintStatus;

    private String staffId;

    private String shiftId;

    private String reasonCode;

    private String templateName;
    
    private String description;
    
    private String packageName;
    
    private String legalEntityDataareaId;
    
    private String refTransactionId;
    
    @JsonProperty("TICKETLINEXML")
    private String ticketLineXML;
    
    private List<AxRetailTicketLineEntity> lines;
    
    @JsonProperty("Facilities")
    private List<AxFacilityClassEntity> facilities;

    @JsonProperty("TokenList")
    private List<AxPrintTokenEntity> tokenList;
    
    @JsonProperty("ExtensionProperties")
    private List<AxCommerceProperty> extensionProperties;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getOrigTicketCode() {
        return origTicketCode;
    }

    public void setOrigTicketCode(String origTicketCode) {
        this.origTicketCode = origTicketCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getQtyGroup() {
        return qtyGroup;
    }

    public void setQtyGroup(String qtyGroup) {
        this.qtyGroup = qtyGroup;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicketTableId() {
        return ticketTableId;
    }

    public void setTicketTableId(String ticketTableId) {
        this.ticketTableId = ticketTableId;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUsageValidityId() {
        return usageValidityId;
    }

    public void setUsageValidityId(String usageValidityId) {
        this.usageValidityId = usageValidityId;
    }

    public String getOpenValidityId() {
        return openValidityId;
    }

    public void setOpenValidityId(String openValidityId) {
        this.openValidityId = openValidityId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNeedActivation() {
        return needActivation;
    }

    public void setNeedActivation(String needActivation) {
        this.needActivation = needActivation;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getNonReturnable() {
        return nonReturnable;
    }

    public void setNonReturnable(String nonReturnable) {
        this.nonReturnable = nonReturnable;
    }

    public String getTicketCodePackage() {
        return ticketCodePackage;
    }

    public void setTicketCodePackage(String ticketCodePackage) {
        this.ticketCodePackage = ticketCodePackage;
    }

    public String getIsFailedSendTicket() {
        return isFailedSendTicket;
    }

    public void setIsFailedSendTicket(String isFailedSendTicket) {
        this.isFailedSendTicket = isFailedSendTicket;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getPaxId() {
        return paxId;
    }

    public void setPaxId(String paxId) {
        this.paxId = paxId;
    }

    public String getTicketPrintStatus() {
        return ticketPrintStatus;
    }

    public void setTicketPrintStatus(String ticketPrintStatus) {
        this.ticketPrintStatus = ticketPrintStatus;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Boolean isNeedsActivation() {
        return "1".endsWith(this.needActivation) ? Boolean.TRUE : Boolean.FALSE;
    }

    public List<AxFacilityClassEntity> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<AxFacilityClassEntity> facilities) {
        this.facilities = facilities;
    }

    public List<AxPrintTokenEntity> getTokenList() {
        return tokenList;
    }

    public void setTokenList(List<AxPrintTokenEntity> tokenList) {
        this.tokenList = tokenList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getLegalEntityDataareaId() {
        return legalEntityDataareaId;
    }

    public void setLegalEntityDataareaId(String legalEntityDataareaId) {
        this.legalEntityDataareaId = legalEntityDataareaId;
    }

    public String getRefTransactionId() {
        return refTransactionId;
    }

    public void setRefTransactionId(String refTransactionId) {
        this.refTransactionId = refTransactionId;
    }

    public String getTicketLineXML() {
        return ticketLineXML;
    }

    public void setTicketLineXML(String ticketLineXML) {
        this.ticketLineXML = ticketLineXML;
    }

    public List<AxRetailTicketLineEntity> getLines() {
        return lines;
    }

    public void setLines(List<AxRetailTicketLineEntity> lines) {
        this.lines = lines;
    }

    public List<AxCommerceProperty> getExtensionProperties() {
        return extensionProperties;
    }
    
    public void setExtensionProperties(List<AxCommerceProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}
