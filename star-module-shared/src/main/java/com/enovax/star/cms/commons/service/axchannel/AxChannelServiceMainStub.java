package com.enovax.star.cms.commons.service.axchannel;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ISDCB2BRetailTicketService;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCB2BRetailTicketService;
import com.enovax.star.cms.commons.ws.axchannel.customerext.ISDCCustomerService;
import com.enovax.star.cms.commons.ws.axchannel.customerext.SDCCustomerExtService;
import com.enovax.star.cms.commons.ws.axchannel.inventtable.ISDCInventTableService;
import com.enovax.star.cms.commons.ws.axchannel.inventtable.SDCInventTableService;
import com.enovax.star.cms.commons.ws.axchannel.pin.ISDCPINService;
import com.enovax.star.cms.commons.ws.axchannel.pin.SDCPinService;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.ISDCRetailTicketTableService;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCRetailTicketTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class AxChannelServiceMainStub {

    private Logger log = LoggerFactory.getLogger(getClass());

    private StoreApiChannels channel;
    private String apiBaseUrl;
    private boolean init = false;

//    private boolean usesB2BPINView = false;
//    private String urlFragB2BPINView = "Services/SDC_B2BPINViewService.svc";
//    private ISDCB2BPINViewService stubB2BPINView;
//    private SDCB2BPINViewService wsB2BPINView;

//    private boolean usesB2BPINCancel = false;
//    private String urlFragB2BPINCancel = "Services/SDC_B2BPINCancelService.svc";
//    private ISDCB2BPINCancelService stubB2BPINCancel;
//    private SDCB2BPINCancelService wsB2BPINCancel;

//    private boolean usesB2BPINUpdate = false;
//    private String urlFragB2BPINUpdate = "Services/SDC_B2BPINUpdateService.svc";
//    private ISDCB2BPINUpdateService stubB2BPINUpdate;
//    private SDCB2BPINUpdateService wsB2BPINUpdate;

//    private boolean usesB2BPINViewLine = false;
//    private String urlFragB2BPINViewLine = "Services/SDC_B2BPINViewLineService.svc";
//    private ISDCB2BPINViewLineService stubB2BPINViewLine;
//    private SDCB2BPINViewLineService wsB2BPINViewLine;

    private boolean usesB2BRetailTicket = false;
    private String urlFragB2BRetailTicket = "Services/SDC_B2BRetailTicketService.svc";
    private ISDCB2BRetailTicketService stubB2BRetailTicket;
    private SDCB2BRetailTicketService wsB2BRetailTicket;

//    private boolean usesBlockPINRedemption = false;
//    private String urlFragBlockPINRedemption = "Services/SDC_BlockPINRedemptionService.svc";
//    private ISDCBlockPINRedemptionService stubBlockPINRedemption;
//    private SDCBlockPINRedemptionService wsBlockPINRedemption;

    private boolean usesCustomerExt = false;
    private String urlFragCustomerExt = "Services/SDC_CustomerExtService.svc";
    private ISDCCustomerService stubCustomerExt;
    private SDCCustomerExtService wsCustomerExt;

//    private boolean usesDiscountCounter = false;
//    private String urlFragDiscountCounter = "Services/SDC_DiscountCounterService.svc";
//    private ISDCDiscountCounterService stubDiscountCounter;
//    private SDCDiscountCounterService wsDiscountCounter;

    private boolean usesInventTable = false;
    private String urlFragInventTable = "Services/SDC_InventTableService.svc";
    private ISDCInventTableService stubInventTable;
    private SDCInventTableService wsInventTable;

//    private boolean usesOnlineCart = false;
//    private String urlFragOnlineCart = "Services/SDC_OnlineCartService.svc";
//    private ISDCOnlineCartService stubOnlineCart;
//    private SDCOnlineCartService wsOnlineCart;

    private boolean usesRetailTicketTable = false;
    private String urlFragRetailTicketTable = "Services/SDC_RetailTicketTableService.svc";
    private ISDCRetailTicketTableService stubRetailTicketTable;
    private SDCRetailTicketTableService wsRetailTicketTable;

//    private boolean usesUpCrossSell = false;
//    private String urlFragUpCrossSell = "Services/SDC_UpCrossSellService.svc";
//    private ISDCUpCrossSellService stubUpCrossSell;
//    private SDCUpCrossSellService wsUpCrossSell;
//
//    private boolean usesUseDiscountCounter = false;
//    private String urlFragUseDiscountCounter = "Services/SDC_UseDiscountCounterService.svc";
//    private ISDCUseDiscountCounterService stubUseDiscountCounter;
//    private SDCUseDiscountCounterService wsUseDiscountCounter;
//
//    private boolean usesPenaltyCharge = false;
//    private String urlFragUsePenaltyCharge = "Services/SDC_CustPenaltyChargeService.svc";
//    private ISDCCustPenaltyChargeService stubPenaltyCharge;
//    private SDCCustPenaltyChargeService wsPenaltyCharge;
//
//    private boolean usesB2BUpdatePINStatus = false;
//    private String urlFragB2BUpdatePINStatus = "Services/SDC_B2BUpdatePINStatusService.svc";
//    private ISDCB2BUpdatePINStatusService stubB2BUpdatePINStatus;
//    private SDCB2BUpdatePINStatusService wsB2BUpdatePINStatus;


    private boolean usesPIN = false;
    private String urlFragPIN = "Services/SDC_PinService.svc";
    private ISDCPINService stubPIN;
    private SDCPinService wsPIN;


    public AxChannelServiceMainStub(StoreApiChannels channel, String apiBaseUrl, boolean initialiseAll) {
        this.channel = channel;
        this.apiBaseUrl = apiBaseUrl;

        if (initialiseAll) {
            initialiseAll();
        }
    }

    public boolean initialiseAll() {
        return initialise(true, true, true, true, true);
    }

    public boolean initialise(boolean usesB2BRetailTicket,
                              boolean usesCustomerExt,
                              boolean usesInventTable,
                              boolean usesRetailTicketTable,
                              boolean usesPIN) {
//        this.usesB2BPINView = usesB2BPINView;
//        this.usesB2BPINViewLine = usesB2BPINViewLine;
        this.usesB2BRetailTicket = usesB2BRetailTicket;
//        this.usesBlockPINRedemption = usesBlockPINRedemption;
        this.usesCustomerExt = usesCustomerExt;
//        this.usesDiscountCounter = usesDiscountCounter;
        this.usesInventTable = usesInventTable;
//        this.usesOnlineCart = usesOnlineCart;
        this.usesRetailTicketTable = usesRetailTicketTable;
//        this.usesUpCrossSell = usesUpCrossSell;
//        this.usesUseDiscountCounter = usesUseDiscountCounter;
//        this.usesB2BPINUpdate = usesB2BPINUpdate;
//        this.usesB2BPINCancel = usesB2BPINCancel;
//        this.usesPenaltyCharge = usesPenaltyCharge;
//        this.usesB2BUpdatePINStatus = usesB2BUpdatePINStatus;
        this.usesPIN = usesPIN;

        return initialise();
    }

    public boolean initialise() {
        try {

//            if (usesB2BPINView) {
//                wsB2BPINView = new SDCB2BPINViewService(new URL(apiBaseUrl + urlFragB2BPINView));
//                stubB2BPINView = wsB2BPINView.getBasicHttpBindingISDCB2BPINViewService();
//            }
//
//            if (usesB2BPINViewLine) {
//                wsB2BPINViewLine = new SDCB2BPINViewLineService(new URL(apiBaseUrl + urlFragB2BPINViewLine));
//                stubB2BPINViewLine = wsB2BPINViewLine.getBasicHttpBindingISDCB2BPINViewLineService();
//            }

            if (usesB2BRetailTicket) {
                wsB2BRetailTicket = new SDCB2BRetailTicketService(new URL(apiBaseUrl + urlFragB2BRetailTicket));
                stubB2BRetailTicket = wsB2BRetailTicket.getBasicHttpBindingISDCB2BRetailTicketService();
            }

//            if (usesBlockPINRedemption) {
//                wsBlockPINRedemption = new SDCBlockPINRedemptionService(new URL(apiBaseUrl + urlFragBlockPINRedemption));
//                stubBlockPINRedemption = wsBlockPINRedemption.getBasicHttpBindingISDCBlockPINRedemptionService();
//            }

            if (usesCustomerExt) {
                wsCustomerExt = new SDCCustomerExtService(new URL(apiBaseUrl + urlFragCustomerExt));
                stubCustomerExt = wsCustomerExt.getBasicHttpBindingISDCCustomerService();
            }

//            if (usesDiscountCounter) {
//                wsDiscountCounter = new SDCDiscountCounterService(new URL(apiBaseUrl + urlFragDiscountCounter));
//                stubDiscountCounter = wsDiscountCounter.getBasicHttpBindingISDCDiscountCounterService();
//            }

            if (usesInventTable) {
                wsInventTable = new SDCInventTableService(new URL(apiBaseUrl + urlFragInventTable));
                stubInventTable = wsInventTable.getBasicHttpBindingISDCInventTableService();
            }

//            if (usesOnlineCart) {
//                wsOnlineCart = new SDCOnlineCartService(new URL(apiBaseUrl + urlFragOnlineCart));
//                stubOnlineCart = wsOnlineCart.getBasicHttpBindingISDCOnlineCartService();
//            }

            if (usesRetailTicketTable) {
                wsRetailTicketTable = new SDCRetailTicketTableService(new URL(apiBaseUrl + urlFragRetailTicketTable));
                stubRetailTicketTable = wsRetailTicketTable.getBasicHttpBindingISDCRetailTicketTableService();
            }

//            if (usesUpCrossSell) {
//                wsUpCrossSell = new SDCUpCrossSellService(new URL(apiBaseUrl + urlFragUpCrossSell));
//                stubUpCrossSell = wsUpCrossSell.getBasicHttpBindingISDCUpCrossSellService();
//            }
//
//            if (usesUseDiscountCounter) {
//                wsUseDiscountCounter = new SDCUseDiscountCounterService(new URL(apiBaseUrl + urlFragUseDiscountCounter));
//                stubUseDiscountCounter = wsUseDiscountCounter.getBasicHttpBindingISDCUseDiscountCounterService();
//            }
//
//            if (usesB2BPINUpdate) {
//                wsB2BPINUpdate = new SDCB2BPINUpdateService(new URL(apiBaseUrl + urlFragB2BPINUpdate));
//                stubB2BPINUpdate = wsB2BPINUpdate.getBasicHttpBindingISDCB2BPINUpdateService();
//            }
//
//            if(usesB2BPINCancel) {
//                wsB2BPINCancel =  new SDCB2BPINCancelService(new URL(apiBaseUrl + urlFragB2BPINCancel));
//                stubB2BPINCancel = wsB2BPINCancel.getBasicHttpBindingISDCB2BPINCancelService();
//            }
//
//            if(usesPenaltyCharge){
//                wsPenaltyCharge = new SDCCustPenaltyChargeService(new URL(apiBaseUrl + urlFragUsePenaltyCharge));
//                stubPenaltyCharge = wsPenaltyCharge.getBasicHttpBindingISDCCustPenaltyChargeService();
//            }
//
//
//            if(usesB2BUpdatePINStatus){
//                wsB2BUpdatePINStatus = new SDCB2BUpdatePINStatusService(new URL(apiBaseUrl + urlFragB2BUpdatePINStatus));
//                stubB2BUpdatePINStatus = wsB2BUpdatePINStatus.getBasicHttpBindingISDCB2BUpdatePINStatusService();
//            }

            if(usesPIN) {
                wsPIN = new SDCPinService(new URL(apiBaseUrl + urlFragPIN));
                stubPIN = wsPIN.getBasicHttpBindingISDCPINService();
            }

            log.info("Initialised AxChannelServiceMainStub for " + this.channel.name());
            this.init = true;
        } catch (Exception e) {
            log.error("Error initialising AxChannelServiceMainStub for " + this.channel.name(), e);
            this.init = false;
        }

        return this.init;
    }

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

//    public boolean isUsesB2BPINView() {
//        return usesB2BPINView;
//    }
//
//    public void setUsesB2BPINView(boolean usesB2BPINView) {
//        this.usesB2BPINView = usesB2BPINView;
//    }
//
//    public String getUrlFragB2BPINView() {
//        return urlFragB2BPINView;
//    }
//
//    public void setUrlFragB2BPINView(String urlFragB2BPINView) {
//        this.urlFragB2BPINView = urlFragB2BPINView;
//    }
//
//    public ISDCB2BPINViewService getStubB2BPINView() {
//        return stubB2BPINView;
//    }
//
//    public void setStubB2BPINView(ISDCB2BPINViewService stubB2BPINView) {
//        this.stubB2BPINView = stubB2BPINView;
//    }
//
//    public SDCB2BPINViewService getWsB2BPINView() {
//        return wsB2BPINView;
//    }
//
//    public void setWsB2BPINView(SDCB2BPINViewService wsB2BPINView) {
//        this.wsB2BPINView = wsB2BPINView;
//    }
//
//    public boolean isUsesB2BPINViewLine() {
//        return usesB2BPINViewLine;
//    }
//
//    public void setUsesB2BPINViewLine(boolean usesB2BPINViewLine) {
//        this.usesB2BPINViewLine = usesB2BPINViewLine;
//    }
//
//    public String getUrlFragB2BPINViewLine() {
//        return urlFragB2BPINViewLine;
//    }

//    public void setUrlFragB2BPINViewLine(String urlFragB2BPINViewLine) {
//        this.urlFragB2BPINViewLine = urlFragB2BPINViewLine;
//    }
//
//    public ISDCB2BPINViewLineService getStubB2BPINViewLine() {
//        return stubB2BPINViewLine;
//    }
//
//    public void setStubB2BPINViewLine(ISDCB2BPINViewLineService stubB2BPINViewLine) {
//        this.stubB2BPINViewLine = stubB2BPINViewLine;
//    }
//
//    public SDCB2BPINViewLineService getWsB2BPINViewLine() {
//        return wsB2BPINViewLine;
//    }
//
//    public void setWsB2BPINViewLine(SDCB2BPINViewLineService wsB2BPINViewLine) {
//        this.wsB2BPINViewLine = wsB2BPINViewLine;
//    }

    public boolean isUsesB2BRetailTicket() {
        return usesB2BRetailTicket;
    }

    public void setUsesB2BRetailTicket(boolean usesB2BRetailTicket) {
        this.usesB2BRetailTicket = usesB2BRetailTicket;
    }

    public String getUrlFragB2BRetailTicket() {
        return urlFragB2BRetailTicket;
    }

    public void setUrlFragB2BRetailTicket(String urlFragB2BRetailTicket) {
        this.urlFragB2BRetailTicket = urlFragB2BRetailTicket;
    }

    public ISDCB2BRetailTicketService getStubB2BRetailTicket() {
        return stubB2BRetailTicket;
    }

    public void setStubB2BRetailTicket(ISDCB2BRetailTicketService stubB2BRetailTicket) {
        this.stubB2BRetailTicket = stubB2BRetailTicket;
    }

    public SDCB2BRetailTicketService getWsB2BRetailTicket() {
        return wsB2BRetailTicket;
    }

    public void setWsB2BRetailTicket(SDCB2BRetailTicketService wsB2BRetailTicket) {
        this.wsB2BRetailTicket = wsB2BRetailTicket;
    }

//    public boolean isUsesBlockPINRedemption() {
//        return usesBlockPINRedemption;
//    }
//
//    public void setUsesBlockPINRedemption(boolean usesBlockPINRedemption) {
//        this.usesBlockPINRedemption = usesBlockPINRedemption;
//    }
//
//    public String getUrlFragBlockPINRedemption() {
//        return urlFragBlockPINRedemption;
//    }
//
//    public void setUrlFragBlockPINRedemption(String urlFragBlockPINRedemption) {
//        this.urlFragBlockPINRedemption = urlFragBlockPINRedemption;
//    }
//
//    public ISDCBlockPINRedemptionService getStubBlockPINRedemption() {
//        return stubBlockPINRedemption;
//    }
//
//    public void setStubBlockPINRedemption(ISDCBlockPINRedemptionService stubBlockPINRedemption) {
//        this.stubBlockPINRedemption = stubBlockPINRedemption;
//    }
//
//    public SDCBlockPINRedemptionService getWsBlockPINRedemption() {
//        return wsBlockPINRedemption;
//    }
//
//    public void setWsBlockPINRedemption(SDCBlockPINRedemptionService wsBlockPINRedemption) {
//        this.wsBlockPINRedemption = wsBlockPINRedemption;
//    }

    public boolean isUsesCustomerExt() {
        return usesCustomerExt;
    }

    public void setUsesCustomerExt(boolean usesCustomerExt) {
        this.usesCustomerExt = usesCustomerExt;
    }

    public String getUrlFragCustomerExt() {
        return urlFragCustomerExt;
    }

    public void setUrlFragCustomerExt(String urlFragCustomerExt) {
        this.urlFragCustomerExt = urlFragCustomerExt;
    }

    public ISDCCustomerService getStubCustomerExt() {
        return stubCustomerExt;
    }

    public void setStubCustomerExt(ISDCCustomerService stubCustomerExt) {
        this.stubCustomerExt = stubCustomerExt;
    }

    public SDCCustomerExtService getWsCustomerExt() {
        return wsCustomerExt;
    }

    public void setWsCustomerExt(SDCCustomerExtService wsCustomerExt) {
        this.wsCustomerExt = wsCustomerExt;
    }

//    public boolean isUsesDiscountCounter() {
//        return usesDiscountCounter;
//    }
//
//    public void setUsesDiscountCounter(boolean usesDiscountCounter) {
//        this.usesDiscountCounter = usesDiscountCounter;
//    }
//
//    public String getUrlFragDiscountCounter() {
//        return urlFragDiscountCounter;
//    }
//
//    public void setUrlFragDiscountCounter(String urlFragDiscountCounter) {
//        this.urlFragDiscountCounter = urlFragDiscountCounter;
//    }
//
//    public ISDCDiscountCounterService getStubDiscountCounter() {
//        return stubDiscountCounter;
//    }
//
//    public void setStubDiscountCounter(ISDCDiscountCounterService stubDiscountCounter) {
//        this.stubDiscountCounter = stubDiscountCounter;
//    }
//
//    public SDCDiscountCounterService getWsDiscountCounter() {
//        return wsDiscountCounter;
//    }
//
//    public void setWsDiscountCounter(SDCDiscountCounterService wsDiscountCounter) {
//        this.wsDiscountCounter = wsDiscountCounter;
//    }

    public boolean isUsesInventTable() {
        return usesInventTable;
    }

    public void setUsesInventTable(boolean usesInventTable) {
        this.usesInventTable = usesInventTable;
    }

    public String getUrlFragInventTable() {
        return urlFragInventTable;
    }

    public void setUrlFragInventTable(String urlFragInventTable) {
        this.urlFragInventTable = urlFragInventTable;
    }

    public ISDCInventTableService getStubInventTable() {
        return stubInventTable;
    }

    public void setStubInventTable(ISDCInventTableService stubInventTable) {
        this.stubInventTable = stubInventTable;
    }

    public SDCInventTableService getWsInventTable() {
        return wsInventTable;
    }

    public void setWsInventTable(SDCInventTableService wsInventTable) {
        this.wsInventTable = wsInventTable;
    }

//    public boolean isUsesOnlineCart() {
//        return usesOnlineCart;
//    }
//
//    public void setUsesOnlineCart(boolean usesOnlineCart) {
//        this.usesOnlineCart = usesOnlineCart;
//    }
//
//    public String getUrlFragOnlineCart() {
//        return urlFragOnlineCart;
//    }
//
//    public void setUrlFragOnlineCart(String urlFragOnlineCart) {
//        this.urlFragOnlineCart = urlFragOnlineCart;
//    }
//
//    public ISDCOnlineCartService getStubOnlineCart() {
//        return stubOnlineCart;
//    }
//
//    public void setStubOnlineCart(ISDCOnlineCartService stubOnlineCart) {
//        this.stubOnlineCart = stubOnlineCart;
//    }
//
//    public SDCOnlineCartService getWsOnlineCart() {
//        return wsOnlineCart;
//    }
//
//    public void setWsOnlineCart(SDCOnlineCartService wsOnlineCart) {
//        this.wsOnlineCart = wsOnlineCart;
//    }

    public boolean isUsesRetailTicketTable() {
        return usesRetailTicketTable;
    }

    public void setUsesRetailTicketTable(boolean usesRetailTicketTable) {
        this.usesRetailTicketTable = usesRetailTicketTable;
    }

    public String getUrlFragRetailTicketTable() {
        return urlFragRetailTicketTable;
    }

    public void setUrlFragRetailTicketTable(String urlFragRetailTicketTable) {
        this.urlFragRetailTicketTable = urlFragRetailTicketTable;
    }

    public ISDCRetailTicketTableService getStubRetailTicketTable() {
        return stubRetailTicketTable;
    }

    public void setStubRetailTicketTable(ISDCRetailTicketTableService stubRetailTicketTable) {
        this.stubRetailTicketTable = stubRetailTicketTable;
    }

    public SDCRetailTicketTableService getWsRetailTicketTable() {
        return wsRetailTicketTable;
    }

    public void setWsRetailTicketTable(SDCRetailTicketTableService wsRetailTicketTable) {
        this.wsRetailTicketTable = wsRetailTicketTable;
    }

//    public boolean isUsesUpCrossSell() {
//        return usesUpCrossSell;
//    }
//
//    public void setUsesUpCrossSell(boolean usesUpCrossSell) {
//        this.usesUpCrossSell = usesUpCrossSell;
//    }
//
//    public String getUrlFragUpCrossSell() {
//        return urlFragUpCrossSell;
//    }
//
//    public void setUrlFragUpCrossSell(String urlFragUpCrossSell) {
//        this.urlFragUpCrossSell = urlFragUpCrossSell;
//    }
//
//    public ISDCUpCrossSellService getStubUpCrossSell() {
//        return stubUpCrossSell;
//    }
//
//    public void setStubUpCrossSell(ISDCUpCrossSellService stubUpCrossSell) {
//        this.stubUpCrossSell = stubUpCrossSell;
//    }
//
//    public SDCUpCrossSellService getWsUpCrossSell() {
//        return wsUpCrossSell;
//    }
//
//    public void setWsUpCrossSell(SDCUpCrossSellService wsUpCrossSell) {
//        this.wsUpCrossSell = wsUpCrossSell;
//    }
//
//    public boolean isUsesUseDiscountCounter() {
//        return usesUseDiscountCounter;
//    }
//
//    public void setUsesUseDiscountCounter(boolean usesUseDiscountCounter) {
//        this.usesUseDiscountCounter = usesUseDiscountCounter;
//    }
//
//    public String getUrlFragUseDiscountCounter() {
//        return urlFragUseDiscountCounter;
//    }
//
//    public void setUrlFragUseDiscountCounter(String urlFragUseDiscountCounter) {
//        this.urlFragUseDiscountCounter = urlFragUseDiscountCounter;
//    }
//
//    public ISDCUseDiscountCounterService getStubUseDiscountCounter() {
//        return stubUseDiscountCounter;
//    }
//
//    public void setStubUseDiscountCounter(ISDCUseDiscountCounterService stubUseDiscountCounter) {
//        this.stubUseDiscountCounter = stubUseDiscountCounter;
//    }
//
//    public SDCUseDiscountCounterService getWsUseDiscountCounter() {
//        return wsUseDiscountCounter;
//    }
//
//    public void setWsUseDiscountCounter(SDCUseDiscountCounterService wsUseDiscountCounter) {
//        this.wsUseDiscountCounter = wsUseDiscountCounter;
//    }
//
//
//    public boolean isUsesB2BPINCancel() {
//        return usesB2BPINCancel;
//    }
//
//    public void setUsesB2BPINCancel(boolean usesB2BPINCancel) {
//        this.usesB2BPINCancel = usesB2BPINCancel;
//    }
//
//    public String getUrlFragB2BPINCancel() {
//        return urlFragB2BPINCancel;
//    }
//
//    public void setUrlFragB2BPINCancel(String urlFragB2BPINCancel) {
//        this.urlFragB2BPINCancel = urlFragB2BPINCancel;
//    }
//
//    public ISDCB2BPINCancelService getStubB2BPINCancel() {
//        return stubB2BPINCancel;
//    }
//
//    public void setStubB2BPINCancel(ISDCB2BPINCancelService stubB2BPINCancel) {
//        this.stubB2BPINCancel = stubB2BPINCancel;
//    }
//
//    public SDCB2BPINCancelService getWsB2BPINCancel() {
//        return wsB2BPINCancel;
//    }
//
//    public void setWsB2BPINCancel(SDCB2BPINCancelService wsB2BPINCancel) {
//        this.wsB2BPINCancel = wsB2BPINCancel;
//    }
//
//    public boolean isUsesB2BPINUpdate() {
//        return usesB2BPINUpdate;
//    }
//
//    public void setUsesB2BPINUpdate(boolean usesB2BPINUpdate) {
//        this.usesB2BPINUpdate = usesB2BPINUpdate;
//    }
//
//    public String getUrlFragB2BPINUpdate() {
//        return urlFragB2BPINUpdate;
//    }
//
//    public void setUrlFragB2BPINUpdate(String urlFragB2BPINUpdate) {
//        this.urlFragB2BPINUpdate = urlFragB2BPINUpdate;
//    }
//
//    public ISDCB2BPINUpdateService getStubB2BPINUpdate() {
//        return stubB2BPINUpdate;
//    }
//
//    public void setStubB2BPINUpdate(ISDCB2BPINUpdateService stubB2BPINUpdate) {
//        this.stubB2BPINUpdate = stubB2BPINUpdate;
//    }
//
//    public SDCB2BPINUpdateService getWsB2BPINUpdate() {
//        return wsB2BPINUpdate;
//    }
//
//    public void setWsB2BPINUpdate(SDCB2BPINUpdateService wsB2BPINUpdate) {
//        this.wsB2BPINUpdate = wsB2BPINUpdate;
//    }
//
//    public boolean isUsesPenaltyCharge() {
//        return usesPenaltyCharge;
//    }
//
//    public void setUsesPenaltyCharge(boolean usesPenaltyCharge) {
//        this.usesPenaltyCharge = usesPenaltyCharge;
//    }
//
//    public String getUrlFragUsePenaltyCharge() {
//        return urlFragUsePenaltyCharge;
//    }
//
//    public void setUrlFragUsePenaltyCharge(String urlFragUsePenaltyCharge) {
//        this.urlFragUsePenaltyCharge = urlFragUsePenaltyCharge;
//    }
//
//    public ISDCCustPenaltyChargeService getStubPenaltyCharge() {
//        return stubPenaltyCharge;
//    }
//
//    public void setStubPenaltyCharge(ISDCCustPenaltyChargeService stubPenaltyCharge) {
//        this.stubPenaltyCharge = stubPenaltyCharge;
//    }
//
//    public SDCCustPenaltyChargeService getWsPenaltyCharge() {
//        return wsPenaltyCharge;
//    }
//
//    public void setWsPenaltyCharge(SDCCustPenaltyChargeService wsPenaltyCharge) {
//        this.wsPenaltyCharge = wsPenaltyCharge;
//    }
//
//    public boolean isUsesB2BUpdatePINStatus() {
//        return usesB2BUpdatePINStatus;
//    }
//
//    public void setUsesB2BUpdatePINStatus(boolean usesB2BUpdatePINStatus) {
//        this.usesB2BUpdatePINStatus = usesB2BUpdatePINStatus;
//    }
//
//    public String getUrlFragB2BUpdatePINStatus() {
//        return urlFragB2BUpdatePINStatus;
//    }
//
//    public void setUrlFragB2BUpdatePINStatus(String urlFragB2BUpdatePINStatus) {
//        this.urlFragB2BUpdatePINStatus = urlFragB2BUpdatePINStatus;
//    }
//
//    public ISDCB2BUpdatePINStatusService getStubB2BUpdatePINStatus() {
//        return stubB2BUpdatePINStatus;
//    }
//
//    public void setStubB2BUpdatePINStatus(ISDCB2BUpdatePINStatusService stubB2BUpdatePINStatus) {
//        this.stubB2BUpdatePINStatus = stubB2BUpdatePINStatus;
//    }
//
//    public SDCB2BUpdatePINStatusService getWsB2BUpdatePINStatus() {
//        return wsB2BUpdatePINStatus;
//    }
//
//    public void setWsB2BUpdatePINStatus(SDCB2BUpdatePINStatusService wsB2BUpdatePINStatus) {
//        this.wsB2BUpdatePINStatus = wsB2BUpdatePINStatus;
//    }

    public boolean isUsesPIN() {
        return usesPIN;
    }

    public void setUsesPIN(boolean usesPIN) {
        this.usesPIN = usesPIN;
    }

    public ISDCPINService getStubPIN() {
        return stubPIN;
    }

    public void setStubPIN(ISDCPINService stubPIN) {
        this.stubPIN = stubPIN;
    }
}
