package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMPartnerExtRepository extends JpaRepository<PPSLMPartnerExt, Integer>, JpaSpecificationExecutor<PPSLMPartnerExt> {

    PPSLMPartnerExt findFirstByPartnerIdAndAttrName(Integer id, String name);

    PPSLMPartnerExt findFirstByPartnerIdAndAttrNameAndStatus(Integer id, String name, String code);

    List<PPSLMPartnerExt> findByPartnerIdAndAttrNameInAndStatus(Integer id, List<String> name, String status);

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'updateExtensionFailed' and u.attrValue = 'Y' and u.status = 'A' ")
    List<PPSLMPartnerExt> getAllUpdateFailedCustomer();

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'updateExtensionFailedTimes' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPSLMPartnerExt> findByPartnerExtensionUpdateFailedTimes(Integer partnerId);

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'updateExtensionFailed' and u.attrValue = 'Y' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPSLMPartnerExt> findReUpdateFailedExtensionByPartnerId(Integer id);

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'updateExtensionFailedTimes' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPSLMPartnerExt> findReUpdateFailedTimesExtensionByPartnerId(Integer id);

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'offlinePaymentEnabled' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPSLMPartnerExt> findOfflinePaymentConfigByPartnerId(Integer id);

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'wotReservationCap' ")
    List<PPSLMPartnerExt> finAllWingsOfTimeReservationCapList();

    @Query("select u from PPSLMPartnerExt u where u.attrName = 'wotReservationValidUntil' ")
    List<PPSLMPartnerExt> finAllWingsOfTimeReservationValidUntilList();

    @Query("select u from PPSLMPartnerExt u where u.partnerId = ?1 ")
    List<PPSLMPartnerExt> findAllPartnerExtByPartnerId(Integer id);

    @Query("select u from PPSLMPartnerExt u where u.partnerId = ?1 and u.attrName = 'capacityGroupId' ")
    List<PPSLMPartnerExt> findCapacityGroupIdByPartnerId(Integer id);


}
