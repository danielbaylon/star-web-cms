package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="PPSLMApprover")
public class PPSLMApprover {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    
    @Column(name="adminId")
    private String adminId;
    
    @Column(name="category")
    private String category;
   
    @Column(name="status")
    private String status;

    @Column(name="startDate")
    private Date startDate;
    
    @Column(name="endDate")
    private Date endDate;

    @Column(name="createdBy", nullable=false, length=100)
    private String createdBy;
    
    @Column(name="createdDate", nullable=false, length=23)
    private Date createdDate;
    
    @Column(name="modifiedBy", nullable=false, length=100)
    private String modifiedBy;

    @Column(name="modifiedDate", nullable=false, length=23)
    private Date modifiedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
