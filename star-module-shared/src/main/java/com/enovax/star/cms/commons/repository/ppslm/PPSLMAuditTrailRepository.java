package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAuditTrail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lavanya on 5/9/16.
 */
@Repository
public interface PPSLMAuditTrailRepository extends JpaRepository<PPSLMAuditTrail, Integer> {
}
