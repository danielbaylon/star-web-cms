package com.enovax.star.cms.commons.model.product.promotions;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jensen on 25/6/16.
 */
public class PromotionLinkPackage {

    private boolean success;
    private String message;
    private StoreApiChannels channel;
    private List<PromotionLinkMainProduct> mainProductList = new ArrayList<>();
    private List<PromotionLinkMainPromotion> mainPromotionList = new ArrayList<>();
    private String retailDiscountResultJson;
    private String affiliationResultJson;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PromotionLinkMainProduct> getMainProductList() {
        return mainProductList;
    }

    public void setMainProductList(List<PromotionLinkMainProduct> mainProductList) {
        this.mainProductList = mainProductList;
    }

    public List<PromotionLinkMainPromotion> getMainPromotionList() {
        return mainPromotionList;
    }

    public void setMainPromotionList(List<PromotionLinkMainPromotion> mainPromotionList) {
        this.mainPromotionList = mainPromotionList;
    }

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public String getRetailDiscountResultJson() {
        return retailDiscountResultJson;
    }

    public void setRetailDiscountResultJson(String retailDiscountResultJson) {
        this.retailDiscountResultJson = retailDiscountResultJson;
    }

    public String getAffiliationResultJson() {
        return affiliationResultJson;
    }

    public void setAffiliationResultJson(String affiliationResultJson) {
        this.affiliationResultJson = affiliationResultJson;
    }
}
