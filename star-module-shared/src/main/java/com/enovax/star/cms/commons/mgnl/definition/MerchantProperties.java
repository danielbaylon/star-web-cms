package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jace on 15/9/16.
 */
public enum MerchantProperties {

  Name("name"),
  MerchantId("merchantId"),
  AcceptsAllCards("acceptsAllCards"),
  Active("isActive");

  private String propertyName;

  MerchantProperties(String propertyName) {
    this.propertyName = propertyName;
  }

  public String getPropertyName() {
    return propertyName;
  }
}
