package com.enovax.star.cms.commons.datamodel.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.RevalItem;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 7/15/16.
 */
@Entity
@Table(name = PPSLMInventoryTransactionItem.TABLE_NAME)
public class PPSLMInventoryTransactionItem {
    public static final String TABLE_NAME = "PPSLMInventoryTransactionItem";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "transactionId", nullable = false)
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPSLMInventoryTransaction inventoryTrans;

    @Column(name = "productId", nullable = false)
    private String productId;

    @Column(name = "productName")
    private String productName;


    @Transient
    private List<PPSLMInventoryTransactionItem> topupItems;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "itemListingId", nullable = false)
    private String itemListingId;

    @Column(name = "itemProductCode", nullable = false)
    private String itemProductCode;

    @Column(name = "transItemType", nullable = false)
    private String transItemType;

    @Column(name = "displayName", nullable = false)
    private String displayName;

    @Column(name = "displayDetails", nullable = false)
    private String displayDetails;

    @Column(name = "dateOfVisit")
    private Date dateOfVisit;

    @Column(name = "eventGroupId")
    private String eventGroupId;

    @Column(name = "eventLineId")
    private String eventLineId;

    @Column(name = "eventName")
    private String eventName;

    @Column(name = "ticketType", nullable = false)
    private String ticketType;

    @Column(name = "itemType", nullable = false)
    private String itemType;

    @Column(name = "unitPrice", nullable = false)
    private BigDecimal unitPrice;

    @Column(name = "baseQty", nullable = false)
    private Integer baseQty;

    @Column(name = "qty", nullable = false)
    private Integer qty;

    @Column(name = "subTotal", nullable = false)
    private BigDecimal subTotal;

    @Column(name = "otherDetails")
    private String otherDetails;

    @Column(name = "unpackagedQty", nullable = false)
    private Integer unpackagedQty;

    @Column(name = "mainItemProductId")
    private String mainItemProductId;

    @Column(name = "mainItemListingId")
    private String mainItemListingId;

    @Column(name = "validityStartDate")
    private Date validityStartDate;

    @Column(name = "validityEndDate")
    private Date validityEndDate;

    @Column(name = "validityDateType")
    private String validityDateType;


    @Transient
    private Integer pkgQty;


    //TODO evaluate if will need the comparison??????
//    @Override
//    public int compareTo(PPSLMInventoryTransactionItem p2) {
//        PPSLMInventoryTransactionItem p1 = this;
//
//
//        //TODO compare the event dates
////        if (p1.getScheduleId() != p2.getScheduleId()) {
////            return p1.getScheduleId() - p2.getScheduleId();
////        }
//
//            //TODO evaluate everything
////        final Integer mainItemP1 = p1.getMainItemId() == null ? -1 : p1.getMainItemId();
////        final Integer mainItemP2 = p2.getMainItemId() == null ? -1 : p2.getMainItemId();
////
////        final boolean isTopup1 = TransItemType.Topup.toString().equals(p1.getTransItemType());
////        final boolean isTopup2 = TransItemType.Topup.toString().equals(p2.getTransItemType());
////
////        if (!isTopup1 && !isTopup2) {
////            return p1.getId() - p2.getId();
////        }
////
////        if (isTopup1 && isTopup2) {
////            int ires = mainItemP1 - mainItemP2;
////            if (ires != 0) {
////                return ires;
////            }
////
////            int qres = p1.getQty() - p2.getQty();
////            if (qres != 0) {
////                return qres;
////            }
////
////            return p1.getDisplayName().compareTo(p2.getDisplayName());
////        }
////
////        if (isTopup1 && !isTopup2) {
////            int mi1 = mainItemP1;
////            int mi2 = p2.getId();
////
////            if(mi1 == mi2){
////                return 1;
////            }else{
////                return mi1 - mi2;
////            }
////        }
////
////        if (!isTopup1 && isTopup2) {
////            int mi1 = p1.getId();
////            int mi2 = mainItemP2;
////
////            if(mi1 == mi2){
////                return -1;
////            }else{
////                return mi1 - mi2;
////            }
////        }
//
//        return p1.getId() - p2.getId();
//    }

    public PPSLMInventoryTransactionItem() {

    }

    //TODO when needed
//    public PPSLMInventoryTransactionItem(Integer id, Integer transactionId, PPSLMInventoryTransaction inventoryTrans,
//                                    Integer productId,
//                                    List<PPSLMInventoryTransactionItem> topupItems, String status, Integer itemId, Integer itemCode,
//                                    String itemType, String transItemType, String displayName, String displayDetails,
//                                    Date dateOfVisit, Integer ticketType, Integer unitPriceCents, Float displayUnitPrice,
//                                    Integer baseQty, Integer qty, BigDecimal subTotal, String otherDetails, Integer unpackagedQty,
//                                    Integer pkgQty) {
//        super();
//        this.id = id;
//        this.transactionId = transactionId;
//        this.inventoryTrans = inventoryTrans;
//        this.productId = productId;
//        this.product = product;
//        this.scheduleId = scheduleId;
//        this.topupItems = topupItems;
//        this.status = status;
//        this.itemId = itemId;
//        this.itemCode = itemCode;
//        this.itemType = itemType;
//        this.transItemType = transItemType;
//        this.displayName = displayName;
//        this.displayDetails = displayDetails;
//        this.dateOfVisit = dateOfVisit;
//        this.ticketType = ticketType;
//        this.unitPriceCents = unitPriceCents;
//        this.displayUnitPrice = displayUnitPrice;
//        this.baseQty = baseQty;
//        this.qty = qty;
//        this.subTotal = subTotal;
//        this.otherDetails = otherDetails;
//        this.unpackagedQty = unpackagedQty;
//        this.pkgQty = pkgQty;
//    }


    public PPSLMInventoryTransactionItem(RevalItem item) {
        //TODO when needed
        this.id = item.getId();
        this.transactionId = item.getTransactionId();
        this.productId = item.getProductId();
        this.eventGroupId = item.getEventGroupId();
        this.eventLineId = item.getEventLineId();
        this.dateOfVisit = item.getDateOfVisit();
        this.itemListingId = item.getItemListingId();
        this.itemProductCode = item.getItemProductCode();
        this.mainItemListingId = item.getMainItemListingId();
        this.mainItemProductId = item.getMainItemProductId();
        this.itemType = item.getItemType();
        this.transItemType = item.getTransItemType();
        this.displayName = item.getDisplayName();
        this.displayDetails = item.getDisplayDetails();
        this.ticketType = item.getTicketType();
        this.qty = item.getQty();
        this.unpackagedQty = item.getUnpackagedQty();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public PPSLMInventoryTransaction getInventoryTrans() {
        return inventoryTrans;
    }

    public void setInventoryTrans(PPSLMInventoryTransaction inventoryTrans) {
        this.inventoryTrans = inventoryTrans;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getPkgQty() {
        return pkgQty;
    }

    public void setPkgQty(Integer pkgQty) {
        this.pkgQty = pkgQty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemListingId() {
        return itemListingId;
    }

    public void setItemListingId(String itemListingId) {
        this.itemListingId = itemListingId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public String getTransItemType() {
        return transItemType;
    }

    public void setTransItemType(String transItemType) {
        this.transItemType = transItemType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getTicketType() {
        return ticketType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getBaseQty() {
        return baseQty;
    }

    public void setBaseQty(Integer baseQty) {
        this.baseQty = baseQty;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public String getMainItemProductId() {
        return mainItemProductId;
    }

    public void setMainItemProductId(String mainItemProductId) {
        this.mainItemProductId = mainItemProductId;
    }

    public String getMainItemListingId() {
        return mainItemListingId;
    }

    public void setMainItemListingId(String mainItemListingId) {
        this.mainItemListingId = mainItemListingId;
    }

    public Date getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(Date validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getValidityDateType() {
        return validityDateType;
    }

    public void setValidityDateType(String validityDateType) {
        this.validityDateType = validityDateType;
    }

    public List<PPSLMInventoryTransactionItem> getTopupItems() {
        return topupItems;
    }

    public void setTopupItems(List<PPSLMInventoryTransactionItem> topupItems) {
        this.topupItems = topupItems;
    }

}
