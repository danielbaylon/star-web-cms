package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.io.Serializable;
import java.util.List;

public class PartnerAccountVMWrapper implements Serializable{

    private List<PartnerAccountVM> accounts;

    public List<PartnerAccountVM> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<PartnerAccountVM> accounts) {
        this.accounts = accounts;
    }
}