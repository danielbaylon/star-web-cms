
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetLineOfBusinessResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_LineOfBusinessResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLineOfBusinessResult"
})
@XmlRootElement(name = "GetLineOfBusinessResponse")
public class GetLineOfBusinessResponse {

    @XmlElementRef(name = "GetLineOfBusinessResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCLineOfBusinessResponse> getLineOfBusinessResult;

    /**
     * Gets the value of the getLineOfBusinessResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCLineOfBusinessResponse }{@code >}
     *     
     */
    public JAXBElement<SDCLineOfBusinessResponse> getGetLineOfBusinessResult() {
        return getLineOfBusinessResult;
    }

    /**
     * Sets the value of the getLineOfBusinessResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCLineOfBusinessResponse }{@code >}
     *     
     */
    public void setGetLineOfBusinessResult(JAXBElement<SDCLineOfBusinessResponse> value) {
        this.getLineOfBusinessResult = value;
    }

}
