package com.enovax.star.cms.commons.model.axchannel.retailticket;

import java.math.BigDecimal;
import java.util.*;

public class AxRetailTicketRecord {

    private Date createdDate;
    private String dataAreaId;
    private Date endDate;
    private Date eventDate;
    private String eventGroupId;
    private String eventLineId;
    private String itemId;
    private Date modifiedDate;
    private boolean needsActivation;
    private String openValidityId;
    private String originalTicketCode;
    private String pinCode;
    private String productName;
    private BigDecimal qtyGroup;
    private Long recId;
    private Date startDate;
    private String ticketCode;
    private Integer ticketStatus;
    private String ticketTableId;
    private Date transactionDate;
    private String transactionId;
    private String usageValidityId;
    private String templateName;

    private Map<String,String> printTokenEntity = new HashMap<>();
    private List<AxFacility> facilityClassEntity = new ArrayList<>();

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isNeedsActivation() {
        return needsActivation;
    }

    public void setNeedsActivation(boolean needsActivation) {
        this.needsActivation = needsActivation;
    }

    public String getOpenValidityId() {
        return openValidityId;
    }

    public void setOpenValidityId(String openValidityId) {
        this.openValidityId = openValidityId;
    }

    public String getOriginalTicketCode() {
        return originalTicketCode;
    }

    public void setOriginalTicketCode(String originalTicketCode) {
        this.originalTicketCode = originalTicketCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getQtyGroup() {
        return qtyGroup;
    }

    public void setQtyGroup(BigDecimal qtyGroup) {
        this.qtyGroup = qtyGroup;
    }

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public Integer getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(Integer ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicketTableId() {
        return ticketTableId;
    }

    public void setTicketTableId(String ticketTableId) {
        this.ticketTableId = ticketTableId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUsageValidityId() {
        return usageValidityId;
    }

    public void setUsageValidityId(String usageValidityId) {
        this.usageValidityId = usageValidityId;
    }

    public Map<String, String> getPrintTokenEntity() {
        return printTokenEntity;
    }

    public void setPrintTokenEntity(Map<String, String> printTokenEntity) {
        this.printTokenEntity = printTokenEntity;
    }

    public List<AxFacility> getFacilityClassEntity() {
        return facilityClassEntity;
    }

    public void setFacilityClassEntity(List<AxFacility> facilityClassEntity) {
        this.facilityClassEntity = facilityClassEntity;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}
