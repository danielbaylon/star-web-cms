package com.enovax.star.cms.commons.service.email;

import com.enovax.star.cms.commons.constant.AppConfigConstants;
import com.enovax.star.cms.commons.constant.ppslm.SysParamConst;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import com.enovax.star.cms.commons.service.config.ISysCfgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.Map;

@Service
public class DefaultMailService implements IMailService {
    
    protected Logger log = LoggerFactory.getLogger(DefaultMailService.class);

    @Autowired
    private ISysCfgService sysCfgSrv;

    @Autowired
    private ISystemParamRepository sysParamRepo;

    @SuppressWarnings("static-access")
    @Override
    @Transactional(readOnly=true)
    public String getDefaultSender() {
        final String sender = getMailer().getDefaultSender();
        return sender;
    }

    //TODO This will become a bottleneck, have to enhance this (e.g. multiple mailers on different threads processing different emails)
    
    @Override
    public void setDefaultSender(String sender) {
        Mailer.setDefaultSender(sender);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Mailer getMailer() {
        if (!Mailer.isInit()) {
            Map<String, String> params = sysCfgSrv.getEmailConfigs();
            Mailer.setMailerProps(params.get(SysParamConst.MailHost.toString()), params.get(SysParamConst.MailPort.toString()),
                    "Y".equals(params.get(SysParamConst.MailAuthReq.toString())),
                    "Y".equals(params.get(SysParamConst.MailSslReq.toString())),
                    params.get(SysParamConst.MailUsername.toString()), params.get(SysParamConst.MailPassword.toString()),
                    params.get(SysParamConst.MailSender.toString()));
            Mailer.rebuild();
        }
        
        return Mailer.getInstance();
    }

    @Override
    @Transactional
    public void sendEmail(MailProperties mailProps, MailCallback mailCallback) throws MessagingException {

        String enableMailService =  sysParamRepo.getSystemParamValueByKey(AppConfigConstants.GENERAL_APP_KEY, AppConfigConstants.ENABLE_MAIL_SERVICE_KEY);
        if(enableMailService != null && "false".equals(enableMailService)) {
            //dont send
        }else {
            getMailer().sendEmail(mailProps, mailCallback);
        }
    }

    /****
    public MailProperties getAdminMailProps(String trackingId, String subject, String body,
                                            boolean isBodyHtml) {
        try {

            final Map<String, String> params = getAdminEmailCfgs();

            String bccGroup = params.get(SysParamConst.AdminMailBcc.toString());
            String[] bccUsers = StringUtils.split(bccGroup, ",");
            String ccGroup = params.get(SysParamConst.AdminMailCc.toString());
            String[] ccUsers = StringUtils.split(ccGroup, ",");
            String toGroup = params.get(SysParamConst.AdminMailTo.toString());
            String[] toUsers = StringUtils.split(toGroup, ",");

            return new MailProperties(trackingId, toUsers, ccUsers, bccUsers, subject, body, isBodyHtml, getDefaultSender());
        } catch (Exception e) {
            throw new RuntimeException("A general error occurred while creating the email properties.", e);
        }
    }
    *****/

    public MailProperties prepareMailProps(String trackingId, String subject, String body, boolean isBodyHtml, String[] toUsers){
    	try {
            return new MailProperties(trackingId, toUsers, null, null, subject, body, isBodyHtml, getDefaultSender());
        } catch (Exception e) {
            throw new RuntimeException("A general error occurred while creating the email properties.", e);
        }
    }

    
}
