package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.NumberSequenceSeedData
 * @author Justin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxNumberSequenceSeedData {
    
    
    @JsonProperty("DataTypeValue")
    private String dataTypeValue;
    
    @JsonProperty("DataValue")
    private String dataValue;

    public String getDataTypeValue() {
        return dataTypeValue;
    }

    public void setDataTypeValue(String dataTypeValue) {
        this.dataTypeValue = dataTypeValue;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }
    


    
}
