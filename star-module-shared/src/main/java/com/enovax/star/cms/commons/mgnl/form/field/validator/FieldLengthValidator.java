package com.enovax.star.cms.commons.mgnl.form.field.validator;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.AbstractValidator;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class FieldLengthValidator extends AbstractValidator<String> {

    private Item item;
    private Integer maxLength;

    /**
     * Constructs a validator with the given error message.
     *
     * @param errorMessage the message to be included in an {@link Validator.InvalidValueException}
     *                     (with "{0}" replaced by the value that failed validation).
     */
    public FieldLengthValidator(Item item, Integer maxlength, String errorMessage) {
        super(errorMessage);
        this.item = item;
        this.maxLength = maxlength;
    }

    @Override
    protected boolean isValidValue(String value) {

        if(value != null) {
            return value.length() <= this.maxLength;
        }

        return true;
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }
}
