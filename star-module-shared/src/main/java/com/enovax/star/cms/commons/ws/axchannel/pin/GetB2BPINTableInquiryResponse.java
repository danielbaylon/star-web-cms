
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetB2BPINTableInquiryResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINTableInquiryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getB2BPINTableInquiryResult"
})
@XmlRootElement(name = "GetB2BPINTableInquiryResponse", namespace = "http://tempuri.org/")
public class GetB2BPINTableInquiryResponse {

    @XmlElementRef(name = "GetB2BPINTableInquiryResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINTableInquiryResponse> getB2BPINTableInquiryResult;

    /**
     * Gets the value of the getB2BPINTableInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINTableInquiryResponse> getGetB2BPINTableInquiryResult() {
        return getB2BPINTableInquiryResult;
    }

    /**
     * Sets the value of the getB2BPINTableInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}
     *     
     */
    public void setGetB2BPINTableInquiryResult(JAXBElement<SDCB2BPINTableInquiryResponse> value) {
        this.getB2BPINTableInquiryResult = value;
    }

}
