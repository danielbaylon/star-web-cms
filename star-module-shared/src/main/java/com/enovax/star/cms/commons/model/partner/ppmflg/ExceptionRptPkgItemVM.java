package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackageItem;
import com.enovax.star.cms.commons.util.NvxDateUtils;

/**
 * Created by lavanya on 10/11/16.
 */
public class ExceptionRptPkgItemVM {
    private String tmStatusDateStr;
    private String receiptNum;
    private Integer pkgQty;
    private Integer transItemQty;
    private String displayName;

    public ExceptionRptPkgItemVM() {
    }

    public ExceptionRptPkgItemVM(PPMFLGMixMatchPackageItem item) {
        this.receiptNum = item.getReceiptNum();
        this.pkgQty = item.getQty();
        this.transItemQty = item.getTransItem().getQty();
        this.tmStatusDateStr = NvxDateUtils.formatDate(item.getTransItem()
                        .getInventoryTrans().getTmStatusDate(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.displayName = item.getDisplayName();

    }

    public String getTmStatusDateStr() {
        return tmStatusDateStr;
    }

    public void setTmStatusDateStr(String tmStatusDateStr) {
        this.tmStatusDateStr = tmStatusDateStr;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getPkgQty() {
        return pkgQty;
    }

    public void setPkgQty(Integer pkgQty) {
        this.pkgQty = pkgQty;
    }

    public Integer getTransItemQty() {
        return transItemQty;
    }

    public void setTransItemQty(Integer transItemQty) {
        this.transItemQty = transItemQty;
    }

    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
