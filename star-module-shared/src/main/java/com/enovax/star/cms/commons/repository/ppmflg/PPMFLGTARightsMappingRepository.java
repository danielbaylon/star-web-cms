package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTARightsMapping;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lavanya on 5/9/16.
 */
@Repository
public interface PPMFLGTARightsMappingRepository extends JpaRepository<PPMFLGTARightsMapping, Integer> {

    PPMFLGTARightsMapping findById(int id);

    List<PPMFLGTARightsMapping> deleteById(int id);

    List<PPMFLGTARightsMapping> findBySubUser(PPMFLGTASubAccount subuserId);

}
