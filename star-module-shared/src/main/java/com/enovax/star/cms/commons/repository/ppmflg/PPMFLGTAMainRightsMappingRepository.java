package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainRightsMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PPMFLGTAMainRightsMappingRepository extends JpaRepository<PPMFLGTAMainRightsMapping, Integer> {
}
