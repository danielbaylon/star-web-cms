package com.enovax.star.cms.commons.model.ticketgen;

public class ReceiptItem {
    private String subtotalText;
    private String qty;
    private String productName;
    private String amount;
    private String axProductName;


    public String getSubtotalText() {
        return subtotalText;
    }

    public void setSubtotalText(String subtotalText) {
        this.subtotalText = subtotalText;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAxProductName() {
        return axProductName;
    }

    public void setAxProductName(String axProductName) {
        this.axProductName = axProductName;
    }
}
