package com.enovax.star.cms.commons.mgnl.contentapp;

import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.api.view.View;
import info.magnolia.ui.framework.app.BaseSubApp;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 8/3/16.
 */
public class StarMainSubApp extends BaseSubApp {

    private final StarBrowserSubAppDescriptor subAppDescriptor;
    @Inject
    protected StarMainSubApp(SubAppContext subAppContext, StarMainSubAppView view) {
        super(subAppContext, view);
        this.subAppDescriptor =  (StarBrowserSubAppDescriptor) subAppContext.getSubAppDescriptor();
    }

    @Override
    public View start(Location location) {
        StarMainSubAppView view = (StarMainSubAppView) super.start(location);
        view.setDisplay(this.subAppDescriptor.getMainSubAppDisplay());
        return view;
    }
}
