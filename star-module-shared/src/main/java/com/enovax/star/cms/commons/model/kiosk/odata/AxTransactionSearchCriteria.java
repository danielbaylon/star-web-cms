package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 *
 * @author dbaylon
 *
 */

public class AxTransactionSearchCriteria {

	@JsonProperty("TransactionIds")
	private List<String> results = new ArrayList<>();

	@JsonProperty("SalesId")
	private String salesId;

	@JsonProperty("ReceiptId")
	private String receiptId;

	@JsonProperty("ChannelReferenceId")
	private String channelReferenceId;

	@JsonProperty("CustomerAccountNumber")
	private String customerAccountNumber;

	@JsonProperty("CustomerFirstName")
	private String customerFirstName;

	@JsonProperty("CustomerLastName")
	private String customerLastName;

	@JsonProperty("StoreId")
	private String storeId;

	@JsonProperty("TerminalId")
	private String terminalId;

	@JsonProperty("ItemId")
	private String itemId;

	@JsonProperty("Barcode")
	private String barcode;

	@JsonProperty("SerialNumber")
	private String serialNumber;

	@JsonProperty("StaffId")
	private String staffId;

	@JsonProperty("StartDateTime")
	private String startDateTime;

	@JsonProperty("EndDateTime")
	private String endDateTime;

	@JsonProperty("ReceiptEmailAddress")
	private String receiptEmailAddress;

	@JsonProperty("SearchIdentifiers")
	private String searchIdentifiers;

	@JsonProperty("SearchLocationTypeValue")
	private int searchLocationTypeValue = 3;

	public List<String> getResults() {
		return results;
	}
	
	public void setResults(List<String> results) {
		this.results = results;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getChannelReferenceId() {
		return channelReferenceId;
	}

	public void setChannelReferenceId(String channelReferenceId) {
		this.channelReferenceId = channelReferenceId;
	}

	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}

	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getReceiptEmailAddress() {
		return receiptEmailAddress;
	}

	public void setReceiptEmailAddress(String receiptEmailAddress) {
		this.receiptEmailAddress = receiptEmailAddress;
	}

	public String getSearchIdentifiers() {
		return searchIdentifiers;
	}

	public void setSearchIdentifiers(String searchIdentifiers) {
		this.searchIdentifiers = searchIdentifiers;
	}

	public int getSearchLocationTypeValue() {
		return searchLocationTypeValue;
	}

	public void setSearchLocationTypeValue(int searchLocationTypeValue) {
		this.searchLocationTypeValue = searchLocationTypeValue;
	}

}
