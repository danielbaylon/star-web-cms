package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/6/16.
 */
public enum CMSProductProperties {
    Name("name"),
    RelatedAXProducts("relatedAXProductUUID"),
    ProductImages("relatedProductImage"),
    TNC("tnc"),
    RecommendedProducts("recommendedProducts"),
	AdmissionIncluded("admissionIncluded"),
    ProductLevel("productLevel"),
    MinQty("minQty"),
    ClosingTime("closingTime"),
    StandaloneItem("standaloneItem");
	
    private String propertyName;

    CMSProductProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
