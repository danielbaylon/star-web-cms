package com.enovax.star.cms.commons.service.ftp;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;
import java.util.Map;

public class FTPServiceImpl implements FTPService {

    private ISystemParamRepository paramRepo;

    private boolean isConnected;
    private String channel;
    private String ftpCfg;

    public void init(ISystemParamRepository paramRepo, String channel, String ftpCfg){
        this.paramRepo = paramRepo;
        this.channel = channel;
        this.ftpCfg = ftpCfg;
    }

    @Override
    public FTPClient connect() throws BizValidationException {
        Map<String, String> ftpCfgs = paramRepo.getSystemParamAsMapByKey(channel.toLowerCase(), ftpCfg);
        if(ftpCfgs == null || ftpCfgs.size() == 0){
            throw new BizValidationException("AX FTP Configs not found.");
        }
        String url  = ftpCfgs.get("ax.ftp.host");
        if(url == null || url.trim().length() == 0){
            throw new BizValidationException("AX FTP Host not found.");
        }
        String port = ftpCfgs.get("ax.ftp.port");
        if(port == null || port.trim().length() == 0){
            throw new BizValidationException("AX FTP Port not found.");
        }
        String user = ftpCfgs.get("ax.ftp.user");
        if(user == null || user.trim().length() == 0){
            throw new BizValidationException("AX FTP User not found.");
        }
        String pass = ftpCfgs.get("ax.ftp.pass");
        if(pass == null || pass.trim().length() == 0){
            throw new BizValidationException("AX FTP Pass not found.");
        }
        int ftpPort = -1;
        try{
            ftpPort = Integer.parseInt(port.trim());
        }catch (Exception ex){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        if(ftpPort == -1){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        FTPClient ftpClient = new FTPClient();
        try{
            ftpClient.connect(url, ftpPort);
            int reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpClient.disconnect();
                throw new BizValidationException("Connecting to AX FTP server is failed");
            }
        }catch (Exception ex){
            throw new BizValidationException("Connecting to AX FTP server is failed");
        }catch (Throwable ex){
            throw new BizValidationException("Connecting to AX FTP server is failed");
        }
        this.isConnected = true;
        return ftpClient;
    }

    @Override
    public void login(FTPClient client) throws BizValidationException {
        Map<String, String> ftpCfgs = paramRepo.getSystemParamAsMapByKey(channel.toLowerCase(), ftpCfg);
        if(ftpCfgs == null || ftpCfgs.size() == 0){
            throw new BizValidationException("AX FTP Configs not found.");
        }
        String url  = ftpCfgs.get("ax.ftp.host");
        if(url == null || url.trim().length() == 0){
            throw new BizValidationException("AX FTP Host not found.");
        }
        String port = ftpCfgs.get("ax.ftp.port");
        if(port == null || port.trim().length() == 0){
            throw new BizValidationException("AX FTP Port not found.");
        }
        String user = ftpCfgs.get("ax.ftp.user");
        if(user == null || user.trim().length() == 0){
            throw new BizValidationException("AX FTP User not found.");
        }
        String pass = ftpCfgs.get("ax.ftp.pass");
        if(pass == null || pass.trim().length() == 0){
            throw new BizValidationException("AX FTP Pass not found.");
        }
        int ftpPort = -1;
        try{
            ftpPort = Integer.parseInt(port.trim());
        }catch (Exception ex){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        if(ftpPort == -1){
            throw new BizValidationException("Invalid AX FTP Port Config found");
        }
        try{
            client.login(user, pass);
            client.setFileType(FTP.BINARY_FILE_TYPE);
        }catch (Exception ex){
            throw new BizValidationException("Connecting to AX FTP server is failed");
        }catch (Throwable ex){
            throw new BizValidationException("Connecting to AX FTP server is failed");
        }
    }

    @Override
    public void logout(FTPClient client) throws IOException {
        client.logout();
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
