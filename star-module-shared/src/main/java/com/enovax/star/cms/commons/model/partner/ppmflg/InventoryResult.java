package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class InventoryResult {
    private long total;
    private List<InventoryTransactionItemVM> transItemVms;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<InventoryTransactionItemVM> getTransItemVms() {
        return transItemVms;
    }

    public void setTransItemVms(List<InventoryTransactionItemVM> transItemVms) {
        this.transItemVms = transItemVms;
    }
}
