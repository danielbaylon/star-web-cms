
package com.enovax.star.cms.commons.ws.axchannel.discountcounter;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDiscountCounterResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_GetDiscountCounterResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDiscountCounterResult"
})
@XmlRootElement(name = "GetDiscountCounterResponse", namespace = "http://tempuri.org/")
public class GetDiscountCounterResponse {

    @XmlElementRef(name = "GetDiscountCounterResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCGetDiscountCounterResponse> getDiscountCounterResult;

    /**
     * Gets the value of the getDiscountCounterResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCGetDiscountCounterResponse }{@code >}
     *     
     */
    public JAXBElement<SDCGetDiscountCounterResponse> getGetDiscountCounterResult() {
        return getDiscountCounterResult;
    }

    /**
     * Sets the value of the getDiscountCounterResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCGetDiscountCounterResponse }{@code >}
     *     
     */
    public void setGetDiscountCounterResult(JAXBElement<SDCGetDiscountCounterResponse> value) {
        this.getDiscountCounterResult = value;
    }

}
