package com.enovax.star.cms.commons.model.product.promotions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 6/25/16.
 */
public class PromotionLinkMainProduct {
    private String itemId;
    private List<ProductRelatedPromotion> relatedPromotions = new ArrayList<>();

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public List<ProductRelatedPromotion> getRelatedPromotions() {
        return relatedPromotions;
    }

    public void setRelatedPromotions(List<ProductRelatedPromotion> relatedPromotions) {
        this.relatedPromotions = relatedPromotions;
    }
}
