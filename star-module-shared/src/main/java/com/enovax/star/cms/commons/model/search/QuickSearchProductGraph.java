package com.enovax.star.cms.commons.model.search;

import java.util.List;

/**
 * Created by jonathan on 28/12/16.
 */
public class QuickSearchProductGraph {

    private final List<QsProd> prods;
    private final QsResult result;

    public static enum QsResult {
        Found,
        NotFound,
        Error
    }

    public QuickSearchProductGraph(List<QsProd> prods) {
        this.result = QsResult.Error;
        this.prods = prods;
    }

    public QuickSearchProductGraph(QsResult qsRes, List<QsProd> prods) {
        this.result = qsRes;
        this.prods = prods;
    }

    public List<QsProd> getProds() {
        return prods;
    }

    public QsResult getResult() {
        return result;
    }

}
