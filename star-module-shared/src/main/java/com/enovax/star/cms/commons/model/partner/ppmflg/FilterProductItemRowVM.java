package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by lavanya on 7/11/16.
 */
public class FilterProductItemRowVM {
    private String productId;
    private String prodName;
    private String itemName;

    public FilterProductItemRowVM(String productId, String prodName, String itemName) {
        this.productId = productId;
        this.prodName = prodName;
        this.itemName = itemName;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }
    public String getProdName() {
        return prodName;
    }
    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
    public String getItemName() {
        return itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
