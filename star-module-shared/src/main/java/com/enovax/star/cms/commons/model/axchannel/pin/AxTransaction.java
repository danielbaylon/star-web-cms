package com.enovax.star.cms.commons.model.axchannel.pin;

import java.math.BigDecimal;

/**
 * Created by houtao on 26/8/16.
 */
public class AxTransaction {

    private String chargeAmountWithCurrency;

    private String deliveryModeDescription;

    private String deliveryModeId;

    private BigDecimal discountAmount;

    private String discountAmountWithCurrency;

    private String loyaltyCardId;

    private String subtotalWithCurrency;

    private String taxAmountWithCurrency;

    private BigDecimal totalAmount;

    private String totalAmountWithCurrency;


    public String getChargeAmountWithCurrency() {
        return chargeAmountWithCurrency;
    }

    public void setChargeAmountWithCurrency(String chargeAmountWithCurrency) {
        this.chargeAmountWithCurrency = chargeAmountWithCurrency;
    }

    public String getDeliveryModeDescription() {
        return deliveryModeDescription;
    }

    public void setDeliveryModeDescription(String deliveryModeDescription) {
        this.deliveryModeDescription = deliveryModeDescription;
    }

    public String getDeliveryModeId() {
        return deliveryModeId;
    }

    public void setDeliveryModeId(String deliveryModeId) {
        this.deliveryModeId = deliveryModeId;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmountWithCurrency() {
        return discountAmountWithCurrency;
    }

    public void setDiscountAmountWithCurrency(String discountAmountWithCurrency) {
        this.discountAmountWithCurrency = discountAmountWithCurrency;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public String getSubtotalWithCurrency() {
        return subtotalWithCurrency;
    }

    public void setSubtotalWithCurrency(String subtotalWithCurrency) {
        this.subtotalWithCurrency = subtotalWithCurrency;
    }

    public String getTaxAmountWithCurrency() {
        return taxAmountWithCurrency;
    }

    public void setTaxAmountWithCurrency(String taxAmountWithCurrency) {
        this.taxAmountWithCurrency = taxAmountWithCurrency;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmountWithCurrency() {
        return totalAmountWithCurrency;
    }

    public void setTotalAmountWithCurrency(String totalAmountWithCurrency) {
        this.totalAmountWithCurrency = totalAmountWithCurrency;
    }
}
