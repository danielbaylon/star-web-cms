package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="PPMFLGTAAccessRight")
public class PPMFLGTAAccessRight implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "topNavName", nullable = false)
	private String topNavName;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = false)
	private String description;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="accessRight")
    private List<PPMFLGTAAccessRightsGroupMapping> accessRightsGroupMapping = new ArrayList<PPMFLGTAAccessRightsGroupMapping>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTopNavName() {
		return topNavName;
	}

	public void setTopNavName(String topNavName) {
		this.topNavName = topNavName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<PPMFLGTAAccessRightsGroupMapping> getAccessRightsGroupMapping() {
		return accessRightsGroupMapping;
	}

	public void setAccessRightsGroupMapping(
			List<PPMFLGTAAccessRightsGroupMapping> accessRightsGroupMapping) {
		this.accessRightsGroupMapping = accessRightsGroupMapping;
	}
    
    
	
}
