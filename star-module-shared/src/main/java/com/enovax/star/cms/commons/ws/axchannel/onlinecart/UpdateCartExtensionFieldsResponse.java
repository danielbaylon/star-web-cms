
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateCartExtensionFieldsResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_UpdateCartExtensionFieldsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCartExtensionFieldsResult"
})
@XmlRootElement(name = "UpdateCartExtensionFieldsResponse", namespace = "http://tempuri.org/")
public class UpdateCartExtensionFieldsResponse {

    @XmlElementRef(name = "UpdateCartExtensionFieldsResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCUpdateCartExtensionFieldsResponse> updateCartExtensionFieldsResult;

    /**
     * Gets the value of the updateCartExtensionFieldsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCUpdateCartExtensionFieldsResponse }{@code >}
     *     
     */
    public JAXBElement<SDCUpdateCartExtensionFieldsResponse> getUpdateCartExtensionFieldsResult() {
        return updateCartExtensionFieldsResult;
    }

    /**
     * Sets the value of the updateCartExtensionFieldsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCUpdateCartExtensionFieldsResponse }{@code >}
     *     
     */
    public void setUpdateCartExtensionFieldsResult(JAXBElement<SDCUpdateCartExtensionFieldsResponse> value) {
        this.updateCartExtensionFieldsResult = value;
    }

}
