
package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTableEntity
 * 
 * @author dbaylon
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxPINRedemptionStartData {

    @JsonProperty("AllowPartialRedemption")
    private String allowPartialRedemption;

    @JsonProperty("CustAccount")
    private String custAccount;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("ExpiryDate")
    private String expiryDate;

    @JsonProperty("IsGroupTicket")
    private String groupTicket;

    @JsonProperty("MediaType")
    private String mediaType;

    @JsonProperty("MediaTypeId")
    private String mediaTypeId;

    @JsonProperty("PackageName")
    private String packageName;

    @JsonProperty("PINCode")
    private String pINCode;

    @JsonProperty("QtyPerProduct")
    private String qtyPerProduct;

    @JsonProperty("ReferenceId")
    private String referenceId;

    @JsonProperty("SessionNo")
    private String sessionNo;

    @JsonProperty("SourceRecId")
    private String sourceRecId;

    @JsonProperty("UserId")
    private String userId;

    @JsonProperty("PINStatus")
    private String pINStatus;

    @JsonProperty("ReasonCode")
    private String reasonCode;

    @JsonProperty("ReasonDesc")
    private String reasonDescription;

    @JsonProperty("DataAreaId")
    private String dataAreaId;

    @SerializedName("sourceTable")
    private String sourceTable;

    @JsonProperty("TicketCodePackage")
    private String ticketCodePackage;

    @JsonProperty("GuestName")
    private String guestName;

    @JsonProperty("CreditCardDigits")
    private String creditCardDigits;

    @JsonProperty("PINType")
    private String pINType;

    @JsonProperty("LastRedemptionLoc")
    private String lastRedemptionLoc;

    @JsonProperty("LastRedemptionDate")
    private String lastRedemptionDate;

    /**
     * Collection(SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTransEntity)
     */
    @JsonProperty("PINRedemptionStartTransEntityCollection")
    private List<AxPINRedemptionStartTransactionData> axPINRedemptionStartTransactionDataList = new ArrayList<>();

    /**
     * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
   
    @JsonProperty("ExtensionProperties")
    private AxCommercePropertyList axCommercePropertyList = new AxCommercePropertyList();
    */
    public String getAllowPartialRedemption() {

        return allowPartialRedemption;
    }

    public void setAllowPartialRedemption(String allowPartialRedemption) {

        this.allowPartialRedemption = allowPartialRedemption;
    }

    public String getCustAccount() {

        return custAccount;
    }

    public void setCustAccount(String custAccount) {

        this.custAccount = custAccount;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getExpiryDate() {

        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {

        this.expiryDate = expiryDate;
    }

    public String getGroupTicket() {

        return groupTicket;
    }

    public void setGroupTicket(String groupTicket) {

        this.groupTicket = groupTicket;
    }

    public String getMediaType() {

        return mediaType;
    }

    public void setMediaType(String mediaType) {

        this.mediaType = mediaType;
    }

    public String getMediaTypeId() {

        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {

        this.mediaTypeId = mediaTypeId;
    }

    public String getPackageName() {

        return packageName;
    }

    public void setPackageName(String packageName) {

        this.packageName = packageName;
    }

    public String getpINCode() {
        return pINCode;
    }

    public void setpINCode(String pINCode) {
        this.pINCode = pINCode;
    }

    public String getQtyPerProduct() {

        return qtyPerProduct;
    }

    public void setQtyPerProduct(String qtyPerProduct) {

        this.qtyPerProduct = qtyPerProduct;
    }

    public String getReferenceId() {

        return referenceId;
    }

    public void setReferenceId(String referenceId) {

        this.referenceId = referenceId;
    }

    public String getSessionNo() {

        return sessionNo;
    }

    public void setSessionNo(String sessionNo) {

        this.sessionNo = sessionNo;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public String getpINStatus() {
        return pINStatus;
    }

    public void setpINStatus(String pINStatus) {
        this.pINStatus = pINStatus;
    }

    public String getReasonCode() {

        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {

        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {

        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {

        this.reasonDescription = reasonDescription;
    }

    public String getDataAreaId() {

        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {

        this.dataAreaId = dataAreaId;
    }

    public String getSourceTable() {

        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {

        this.sourceTable = sourceTable;
    }

    public String getTicketCodePackage() {

        return ticketCodePackage;
    }

    public void setTicketCodePackage(String ticketCodePackage) {

        this.ticketCodePackage = ticketCodePackage;
    }

    public String getGuestName() {

        return guestName;
    }

    public void setGuestName(String guestName) {

        this.guestName = guestName;
    }

    public String getCreditCardDigits() {

        return creditCardDigits;
    }

    public void setCreditCardDigits(String creditCardDigits) {

        this.creditCardDigits = creditCardDigits;
    }

    public void setpINType(String pINType) {
        this.pINType = pINType;
    }

    public String getpINType() {
        return pINType;
    }

    public String getLastRedemptionLoc() {

        return lastRedemptionLoc;
    }

    public void setLastRedemptionLoc(String lastRedemptionLoc) {

        this.lastRedemptionLoc = lastRedemptionLoc;
    }

    public String getLastRedemptionDate() {

        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(String lastRedemptionDate) {

        this.lastRedemptionDate = lastRedemptionDate;
    }

    public List<AxPINRedemptionStartTransactionData> getAxPINRedemptionStartTransactionDataList() {

        return axPINRedemptionStartTransactionDataList;
    }

    public void setAxPINRedemptionStartTransactionDataList(List<AxPINRedemptionStartTransactionData> axPINRedemptionStartTransactionDataList) {

        this.axPINRedemptionStartTransactionDataList = axPINRedemptionStartTransactionDataList;
    }

    public String getSourceRecId() {
        return sourceRecId;
    }

    public void setSourceRecId(String sourceRecId) {
        this.sourceRecId = sourceRecId;
    }

//    public AxCommercePropertyList getAxCommercePropertyList() {
//        return axCommercePropertyList;
//    }
//
//    public void setAxCommercePropertyList(AxCommercePropertyList axCommercePropertyList) {
//        this.axCommercePropertyList = axCommercePropertyList;
//    }

}
