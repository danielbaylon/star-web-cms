package com.enovax.star.cms.commons.jcrrepository.system;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

@Service
public class DefaultJcrEmailTemplateRepository implements IEmailTemplateRepository {

    private static final Logger log = LoggerFactory.getLogger(DefaultJcrEmailTemplateRepository.class);

    @Override
    public boolean hasSystemParamByKey(String appKey, String paramKey) {
        if(paramKey == null || paramKey.trim().length() == 0){
            return false;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.MailTemplates.getWorkspaceName(), "/" + appKey +"/" + paramKey.trim());
            if(node == null){
                return false;
            }
            try{
                getPropertyValue(node, "subject");
                getPropertyValue(node, "value");
                return true;
            }catch (Exception ex){
                log.error("Mail Template [/"+appKey+"/"+paramKey+"] not found : "+ ex.getMessage());
            }
        } catch (RepositoryException e) {
            log.error("Mail Template [/"+appKey+"/"+paramKey+"] not found : "+ e.getMessage());
        }
        return false;
    }

    public SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey) {
        if(paramKey == null || paramKey.trim().length() == 0){
            return null;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.MailTemplates.getWorkspaceName(), "/" + appKey +"/" + paramKey.trim());
            if(node == null){
                return null;
            }
            return populateSystemParamVM(node, true, false);
        } catch (RepositoryException e) {
            log.error("Mail Template [/"+appKey+"/"+paramKey+"] not found : "+ e.getMessage());
        }

        return null;
    }

    private SysEmailTemplateVM populateSystemParamVM(Node node, boolean isRootElement, boolean requiredAttrName) throws RepositoryException {
        if(node == null){
            return null;
        }
        SysEmailTemplateVM vm = new SysEmailTemplateVM();
        vm.setKey(node.getName());
        vm.setSubject(getPropertyValue(node, "subject"));
        vm.setBody(getPropertyValue(node, "value"));
        return vm;
    }

    private String getPropertyValue(Node node, String key) throws RepositoryException {
        if(key == null || key.trim().length() == 0){
            return null;
        }
        if(!node.hasProperty(key.trim())){
            return null;
        }
        Property prop = node.getProperty(key.trim());
        if(prop == null){
            return null;
        }
        String str = prop.getString();
        prop = null;
        if(str.trim().length() > 0){
            return str.trim();
        }
        return null;
    }
}
