package com.enovax.star.cms.commons.model.ticket;

import java.util.List;

public class Facility {

    private String facilityId;
    private List<String> operationIds;
    private Integer facilityAction;

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public List<String> getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(List<String> operationIds) {
        this.operationIds = operationIds;
    }

    public Integer getFacilityAction() {
        return facilityAction;
    }

    public void setFacilityAction(Integer facilityAction) {
        this.facilityAction = facilityAction;
    }

}
