
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfInventTableVariantExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfInventTableVariantExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InventTableVariantExt" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}InventTableVariantExt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfInventTableVariantExt", propOrder = {
    "inventTableVariantExt"
})
public class ArrayOfInventTableVariantExt {

    @XmlElement(name = "InventTableVariantExt", nillable = true)
    protected List<InventTableVariantExt> inventTableVariantExt;

    /**
     * Gets the value of the inventTableVariantExt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inventTableVariantExt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInventTableVariantExt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InventTableVariantExt }
     * 
     * 
     */
    public List<InventTableVariantExt> getInventTableVariantExt() {
        if (inventTableVariantExt == null) {
            inventTableVariantExt = new ArrayList<InventTableVariantExt>();
        }
        return this.inventTableVariantExt;
    }

}
