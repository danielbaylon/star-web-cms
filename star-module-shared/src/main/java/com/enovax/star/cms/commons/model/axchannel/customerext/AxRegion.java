package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by jennylynsze on 12/27/16.
 */
public class AxRegion {
    private String regionId;
    private String description;

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
