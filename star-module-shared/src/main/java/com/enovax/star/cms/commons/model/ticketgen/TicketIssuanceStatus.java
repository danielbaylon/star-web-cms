package com.enovax.star.cms.commons.model.ticketgen;

public class TicketIssuanceStatus {

	private String ticketIssuranceStatus;
	private String ticketIssuranceMsg1;
	private String ticketIssuranceMsg2;
	private String ticketIssuranceMsg3;
	private String ticketIssuranceMsg4;

	public String getTicketIssuranceStatus() {
		return ticketIssuranceStatus;
	}

	public void setTicketIssuranceStatus(String ticketIssuranceStatus) {
		this.ticketIssuranceStatus = ticketIssuranceStatus;
	}

	public String getTicketIssuranceMsg1() {
		return ticketIssuranceMsg1;
	}

	public void setTicketIssuranceMsg1(String ticketIssuranceMsg1) {
		this.ticketIssuranceMsg1 = ticketIssuranceMsg1;
	}

	public String getTicketIssuranceMsg2() {
		return ticketIssuranceMsg2;
	}

	public void setTicketIssuranceMsg2(String ticketIssuranceMsg2) {
		this.ticketIssuranceMsg2 = ticketIssuranceMsg2;
	}

	public String getTicketIssuranceMsg3() {
		return ticketIssuranceMsg3;
	}

	public void setTicketIssuranceMsg3(String ticketIssuranceMsg3) {
		this.ticketIssuranceMsg3 = ticketIssuranceMsg3;
	}

	public String getTicketIssuranceMsg4() {
		return ticketIssuranceMsg4;
	}

	public void setTicketIssuranceMsg4(String ticketIssuranceMsg4) {
		this.ticketIssuranceMsg4 = ticketIssuranceMsg4;
	}

}
