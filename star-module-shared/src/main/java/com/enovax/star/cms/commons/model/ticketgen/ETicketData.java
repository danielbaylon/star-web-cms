package com.enovax.star.cms.commons.model.ticketgen;

import com.enovax.star.cms.commons.model.ticket.TicketData;

public class ETicketData implements Comparable<ETicketData> {

    private boolean isThirdParty = false;
    private String codeData; // TODO This is actually for paper ticket leh
    private String tokenData;
    private String barcodeBase64;
    private String displayName;
    private String ticketNumber;
    private String ticketPersonType;
    private String validityStartDate;
    private String validityEndDate;
    private String totalPrice;
    private String itemId;

    private String shortDisplayName;
    private String eventDate;
    private String eventSession;

    private String ticketTemplateName;

    @Override
    public int compareTo(ETicketData o) {
        return this.itemId.compareTo(o.itemId);
    }

    public String getShortDisplayName() {
        return shortDisplayName;
    }

    public void setShortDisplayName(String shortDisplayName) {
        this.shortDisplayName = shortDisplayName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventSession() {
        return eventSession;
    }

    public void setEventSession(String eventSession) {
        this.eventSession = eventSession;
    }

    private TicketData rawValidationData = new TicketData();

    public String getBarcodeBase64() {
        return barcodeBase64;
    }

    public void setBarcodeBase64(String barcodeBase64) {
        this.barcodeBase64 = barcodeBase64;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketPersonType() {
        return ticketPersonType;
    }

    public void setTicketPersonType(String ticketPersonType) {
        this.ticketPersonType = ticketPersonType;
    }

    public String getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(String validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public String getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(String validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public TicketData getRawValidationData() {
        return rawValidationData;
    }

    public void setRawValidationData(TicketData rawValidationData) {
        this.rawValidationData = rawValidationData;
    }

    public boolean isThirdParty() {
        return isThirdParty;
    }

    public void setThirdParty(boolean thirdParty) {
        isThirdParty = thirdParty;
    }

    public String getCodeData() {
        return codeData;
    }

    public void setCodeData(String codeData) {
        this.codeData = codeData;
    }

    public String getTicketTemplateName() {
        return ticketTemplateName;
    }

    public void setTicketTemplateName(String ticketTemplateName) {
        this.ticketTemplateName = ticketTemplateName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTokenData() {
        return tokenData;
    }

    public void setTokenData(String tokenData) {
        this.tokenData = tokenData;
    }

}
