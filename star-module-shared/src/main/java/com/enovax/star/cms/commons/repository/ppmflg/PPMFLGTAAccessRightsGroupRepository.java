package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccessRightsGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGTAAccessRightsGroupRepository extends JpaRepository<PPMFLGTAAccessRightsGroup, Integer> {

    PPMFLGTAAccessRightsGroup findById(int id);

    List<PPMFLGTAAccessRightsGroup> findByType(String type);

}
