package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;

import java.io.Serializable;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class MixMatchPkgTopupItemVM implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer itemId;
    private String displayName;
    private String displayDetails;
    private Integer pkgQty;
    private Integer qtyRedeemed;

    public MixMatchPkgTopupItemVM(PPSLMInventoryTransactionItem topup) {
        this.itemId = topup.getId();
        this.displayName = topup.getDisplayName();
        this.displayDetails = topup.getDisplayDetails();
        this.pkgQty = topup.getPkgQty();
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public Integer getPkgQty() {
        return pkgQty;
    }

    public void setPkgQty(Integer pkgQty) {
        this.pkgQty = pkgQty;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof MixMatchPkgTopupItemVM))
            return false;
        MixMatchPkgTopupItemVM otherMyClass = (MixMatchPkgTopupItemVM) other;
        if (this.itemId.equals(otherMyClass.getItemId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) serialVersionUID + itemId;
        return result;
    }
}
