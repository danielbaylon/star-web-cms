package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGETicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/15/16.
 */
@Repository
public interface PPMFLGETicketRepository  extends JpaRepository<PPMFLGETicket, Integer> {

    List<PPMFLGETicket> findByPackageId(Integer packageId);
}
