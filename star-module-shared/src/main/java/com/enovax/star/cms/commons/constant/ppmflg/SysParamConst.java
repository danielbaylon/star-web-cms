package com.enovax.star.cms.commons.constant.ppmflg;

public enum SysParamConst {

    SiteMode,
    
	//General
	FailedLoginAttempts,
	DaysPasswordExpires,
	NumPasswordHistory,
	Recaptcha,
	NumSubAccount,

    RecordsPerPage,
    
	//Mail
	MailHost,
	MailPort,
	MailAuthReq,
	MailSslReq,
	MailUsername,
	MailPassword,
	MailSender,

	//AdminMail
	FnbMailTo,
	AdminMailTo,
	AdminMailCc,
	AdminMailBcc,
    
    MailSubjectVoidSuccess,
    MailSubjectVoidFailed,
    MailContentVoidSuccess,
    MailContentVoidFailed,
    
    //GeneralCatalog
    GenCataRetryCount,
    GenCataRetryMillis,
    GenCataRetrieveMode,
    GenCataFtpUrl,
    GenCataFtpPort,
    GenCataFtpUser,
    GenCataFtpPass,
    GenCataFile,
    GenCataDirPath,
    
    //EventCatalog
    EventCataRetryCount,
    EventCataRetryMillis,
    EventCataRetrieveMode,
    EventCataFtpUrl,
    EventCataFtpPort,
    EventCataFtpUser,
    EventCataFtpPass,
    EventCataFile,
    EventCataDirPath,
    
    //Purchase
    PurchaseMinQtyPerTxn,
    PurchaseMaxDailyTxnLimit,
    PurchaseMaxAmountPerTxn,
    Purchase,
 
    
    //Transactions Setting
    //1 unit will be 1 week, default 2 week
    TicketExpiringAlertPeriod,
    //1 unit will be 1 month, default 6 month
    TicketExpiringPeriod,   
    //1 unit will be 1 month, default 3 month
    TicketRevalidatePeriod,  
    //1 unit will be 1 day
    TicketAllowRevalidatePeriod,  
    
    //Revalidate Setting
    NormalRevalidateItemCode,
    NormalRevalidateItemPrice,
    TopupRevalidateItemCode,
    TopupRevalidateItemPrice,
    
    //Receipt Prefix
    RevalidateReceiptPrefix,
    NormalReceiptPrefix,
    
    //For Pkg
    PkgAllowedItemsCodes,
    PkgAllowedMultipleSites,
    PkgAllowedSingleSites,
    PkgAllowedMinQty,
    PkgAllowedMaxQty,
    PkgVildDateDiffToSACT,//SACT use UTC time zone with a Singapore Time, which is not correct, it create a time diff with our system
    
    //Pin Batch
    PinRemoteDir,
    PinRemoteArchiveDir,// e.g. /void-recon/Archive2014/
    PinProcessDir,
    PinArchiveDir,
    PinAdhocProcessDir,
    PinAdhocArchiveDir,
    PinFtpUrl,
    PinFtpPort,
    PinFtpUser,
    PinFtpPass,
    PinRetryCount,
    PinRetryMillis,
    Pin,
    PinRetrieveMode,
    
    //Reval Free
    RevalFreePaymentID,
    RevalFreePaymentType,
    
    //VoidRecon
    VoidFtpUrl,
    VoidFtpPort,
    VoidFtpUser,
    VoidFtpPass,
    VoidFtpRetryCount,
    VoidFtpRetryMillis,
    VoidFtpDir,
    VoidFtpTempDir,
    VoidFtpMinsPast,
    Void,
    
    //Maintain
    MaintHead,
    MaintMsg,
    MaintImg,

    //Wings of Time
    PartnerWoTAdvancedDaysReleaseBackendReservedTickets,
    WotAdminReservationPeriodLimit,
    PartnerTicketonHoldTime,
    PartnerWoTReservationCap,
    PartnerOnlinePaymentTime,
    WoTAdminReservationCap,
    WoTeTicketEnabled,
    WoTAdvancedHoursUnredeemedReservationCancelReminder,
    WotReservationCutoffTime
    ;

    

    public static enum YesNoVals { Y, N }
    public static enum SiteModeVals { L, UM }
    public static enum BookFeeToggleVals { ON, OFF }
    public static enum BookFeeModeVals { None, Trans, Ticket }
    public static enum CatalogRetrieveMode { FTP, DIR , SFTP}

}
