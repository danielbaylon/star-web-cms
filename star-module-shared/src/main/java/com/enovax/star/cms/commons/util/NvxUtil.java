package com.enovax.star.cms.commons.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class NvxUtil {

    public static final String DEFAULT_ENCODING = "UTF-8";

    public static final Pattern regexLowercase = Pattern.compile(".*?[a-z]+?.*");
    public static final Pattern regexUppercase = Pattern.compile(".*?[A-Z]+?.*");
    public static final Pattern regexNumber = Pattern.compile(".*?[0-9]+.*");
    public static final Pattern regexNonAlphanumeric = Pattern.compile(".*?[^a-zA-Z\\d\\s]+.*");

    public static boolean hasLowercase(String text) {
        return regexLowercase.matcher(text).matches();
    }

    public static boolean hasUppercase(String text) {
        return regexUppercase.matcher(text).matches();
    }

    public static boolean hasNumber(String text) {
        return regexNumber.matcher(text).matches();
    }

    public static boolean hasNonAlphanumeric(String text) {
        return regexNonAlphanumeric.matcher(text).matches();
    }

    public static boolean validatePassword(String pw, int minLength,
                                           int maxLength) {
        int len = pw.length();
        return len >= minLength && len <= maxLength && hasLowercase(pw)
                && hasUppercase(pw) && hasNumber(pw) && hasNonAlphanumeric(pw);
    }

    public static String stringToJson(List<String> list) {
        String json = JsonUtil.getGson(false).toJson(list);
        if (StringUtils.isEmpty(json)) {
            return "[]";
        }

        return json;
    }

    public static String arrayCombine(Object[] inputs, String delim) {
        return StringUtils.join(inputs, delim);
    }

    public static String[] arrayDecombine(String input, String delim) {
        return StringUtils.split(input, delim);
    }

    public static String generateRandomAlphaNumeric(int length,
                                                    boolean uppercase) {
        String x = RandomStringUtils.randomAlphanumeric(length);
        return uppercase ? x.toUpperCase() : x;
    }

    public static String encodeUrl(String input) {
        try {
            return URLEncoder.encode(input, DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            return input;
        }
    }

    public static String escapeHtml(String input) {
        return StringEscapeUtils.escapeHtml4(input);
    }

    public static final int DEFAULT_REDUCE_LENGTH = 50;

    public static String reduceText(String text) {
        return reduceText(text, DEFAULT_REDUCE_LENGTH);
    }

    public static String reduceText(String text, int maxSize) {
        if (text.length() <= maxSize) {
            return text;
        }
        return text.substring(0, DEFAULT_REDUCE_LENGTH - 3) + "...";
    }

    public static String linebreaksToBr(String text) {
        return text.replace("\r\n", "<br/>").replace("\n", "<br/>");
    }

    public static String replaceLineBreaks(String stringToProcess,
                                           String replacementText) {
        return stringToProcess.replaceAll("\\r|\\n", replacementText);
    }

    /**
     * @param parameters
     * @param key
     * @param tclass
     * @return Can support int and String type
     */
    public static <T> T getRequestParam(Map parameters, String key,
                                        Class<T> tclass) {
        String[] paraArray = (String[]) parameters.get(key);
        if (paraArray != null && paraArray.length > 0) {
            String para = paraArray[0];
            if (tclass == Integer.class) {
                return (T) new Integer(Integer.parseInt(para));
            }
            return (T) para;
        }
        return null;
    }

    public static <T> T getObjFromStr(String str, Class<T> tclass) {
        if (tclass == Integer.class) {
            return (T) new Integer(Integer.parseInt(str));
        } else if (tclass == BigDecimal.class) {
            return (T) new BigDecimal(str);
        } else if (tclass == String.class) {
            return (T) str;
        }
        return null;
    }

    /**
     * @param obj
     * @return the object need have a XmlRootElement in annotation
     */
    public static String getXmlfromObj(Object obj) {
        StringWriter sw = new StringWriter();
        String xmlString = null;
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(obj, sw);
            xmlString = sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        } finally {
            try {
                sw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return xmlString;
    }

    public static <T> T getObjFromXml(String xml, Class<T> iclass) {
        JAXBContext context;
        StringReader reader = null;
        try {
            context = JAXBContext.newInstance(iclass);
            Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            reader = new StringReader(xml);
            T obj = (T) jaxbUnmarshaller.unmarshal(reader);
            return obj;
        } catch (JAXBException e) {
            e.printStackTrace();
        } finally {
            reader.close();
        }
        return null;
    }

    public static String getCommaStr(String... strings) {
        String finalStr = null;
        for (String tempStr : strings) {
            if (finalStr == null) {
                finalStr = tempStr;
            } else {
                finalStr = finalStr + "," + tempStr;
            }
        }
        return finalStr;
    }

    public static <T> boolean compareList(List<T> l1, List<T> l2) {
        if (l1 == null) {
            l1 = new ArrayList<>();
        }

        if (l2 == null) {
            l2 = new ArrayList<>();
        }

        if (l1.size() != l2.size()) {
            return false;
        }

        ArrayList<T> nl1 = new ArrayList<>(l1);
        ArrayList<T> nl2 = new ArrayList<>(l2);

        nl1.removeAll(nl2);

        return nl1.size() == 0;
    }

}
