package com.enovax.star.cms.commons.model.product.promotions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jensen on 25/6/16.
 */
public class ProductRelatedPromotion {

    private String offerId;
    private String systemName;
    private boolean systemActive;
    private Date validFrom;
    private Date validTo;
    private BigDecimal estimatedDiscountedUnitPrice;
    private String currencyCode;


    private List<PromotionRelatedAffiliation> affiliations = new ArrayList<>();
    private List<PromotionRelatedDiscountCode> discountCodes = new ArrayList<>();

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public boolean isSystemActive() {
        return systemActive;
    }

    public void setSystemActive(boolean systemActive) {
        this.systemActive = systemActive;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public BigDecimal getEstimatedDiscountedUnitPrice() {
        return estimatedDiscountedUnitPrice;
    }

    public void setEstimatedDiscountedUnitPrice(BigDecimal estimatedDiscountedUnitPrice) {
        this.estimatedDiscountedUnitPrice = estimatedDiscountedUnitPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public List<PromotionRelatedDiscountCode> getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(List<PromotionRelatedDiscountCode> discountCodes) {
        this.discountCodes = discountCodes;
    }

    public List<PromotionRelatedAffiliation> getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(List<PromotionRelatedAffiliation> affiliations) {
        this.affiliations = affiliations;
    }
}
