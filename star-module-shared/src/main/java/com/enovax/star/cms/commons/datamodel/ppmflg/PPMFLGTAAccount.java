package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by jennylynsze on 7/14/16.
 */
@MappedSuperclass
public abstract class PPMFLGTAAccount implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "salt", nullable = false)
    private String salt;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "pwdLastModDt", nullable = false)
    private Date pwdLastModDt;

    @Column(name = "pwdAttempts", nullable = false)
    private Integer pwdAttempts;

    @Column(name = "pwdForceChange", nullable = false)
    private boolean pwdForceChange;

    @Column(name = "createdBy", nullable = false)
    private String createdBy;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "modifiedBy", nullable = false)
    private String modifiedBy;

    @Column(name = "modifiedDate", nullable = false)
    private Date modifiedDate;

    @Transient
    private Set<String> accessRights ;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }



    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }



    public Date getPwdLastModDt() {
        return pwdLastModDt;
    }

    public void setPwdLastModDt(Date pwdLastModDt) {
        this.pwdLastModDt = pwdLastModDt;
    }



    public Integer getPwdAttempts() {
        return pwdAttempts;
    }


    public void setPwdAttempts(Integer pwdAttempts) {
        this.pwdAttempts = pwdAttempts;
    }



    public boolean getPwdForceChange() {
        return pwdForceChange;
    }

    public void setPwdForceChange(boolean pwdForceChange) {
        this.pwdForceChange = pwdForceChange;
    }



    public String getCreatedBy() {
        return createdBy;
    }


    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    public Date getCreatedDate() {
        return createdDate;
    }


    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public Date getModifiedDate() {
        return modifiedDate;
    }
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    protected void copy(final PPMFLGTAAccount source)
    {

        this.id = source.id;
        this.username=source.username;
        this.email=source.email;
        this.password = source.password;
        this.salt=source.salt;
        this.status=source.status;
        this.pwdLastModDt=source.pwdLastModDt;
        this.pwdAttempts=source.pwdAttempts;
        this.pwdForceChange=source.pwdForceChange;
        this.createdBy=source.createdBy;
        this.createdDate=source.createdDate;
        this.modifiedBy=source.modifiedBy;
        this.modifiedDate=source.modifiedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getAccessRights() {
        return accessRights;
    }

    public void setAccessRights(Set<String> accessRights) {
        this.accessRights = accessRights;
    }
}
