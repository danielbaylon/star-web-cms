package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * <EntityType Name="UpdatedTicketStatusEntity">
 * <Key><PropertyRef Name="ErrorMessage"/> </Key>
 * <Property Name="ErrorMessage" Type="Edm.String" Nullable="false"/>
 * <Property Name="RetailTicketStatusHistoryEntityCollection" Type=
 * "Collection(SDC_TicketingExtension.Entity.RetailTicketStatusHistoryEntity)"
 * Nullable="false"/>
 * <Property Name="IsError" Type="Edm.Int32" Nullable="false"/>
 * <Property Name="ExtensionProperties" Type=
 * "Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
 * Nullable="false"/> </EntityType>
 * 
 * @author Justin
 *
 */
public class AxUpdatedTicketStatusEntity {

    private String errorMessage;

    private String isError;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

}
