package com.enovax.star.cms.commons.mgnl.definition;

public enum KioskGroupProperties {

	RelatedProductCategoryUUID("relatedProductCategoryUUID"),
	RelatedKioskInfoUUID("relatedKioskInfoUUID"),
	ProductFilter("productFilter"),
	ExcludeProduct("excludeProduct"),
	CategoryData("categoriesData"),
    KioskInfo("kioskInfo");
	
	private String propertyName;

	KioskGroupProperties(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}

}