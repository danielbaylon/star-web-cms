
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_CartRetailTicketPINItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_CartRetailTicketPINItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_CartRetailTicketPINItem" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_CartRetailTicketPINItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_CartRetailTicketPINItem", propOrder = {
    "sdcCartRetailTicketPINItem"
})
public class ArrayOfSDCCartRetailTicketPINItem {

    @XmlElement(name = "SDC_CartRetailTicketPINItem", nillable = true)
    protected List<SDCCartRetailTicketPINItem> sdcCartRetailTicketPINItem;

    /**
     * Gets the value of the sdcCartRetailTicketPINItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcCartRetailTicketPINItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCCartRetailTicketPINItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCCartRetailTicketPINItem }
     * 
     * 
     */
    public List<SDCCartRetailTicketPINItem> getSDCCartRetailTicketPINItem() {
        if (sdcCartRetailTicketPINItem == null) {
            sdcCartRetailTicketPINItem = new ArrayList<SDCCartRetailTicketPINItem>();
        }
        return this.sdcCartRetailTicketPINItem;
    }

}
