package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPSLMTARightsMapping")
public class PPSLMTARightsMapping implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPSLMTAAccessRightsGroup accessRightsGroup;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subAccountId",nullable = false)
    private PPSLMTASubAccount subUser;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public PPSLMTASubAccount getSubUser() {
		return subUser;
	}


	public void setSubUser(PPSLMTASubAccount subUser) {
		this.subUser = subUser;
	}


	public PPSLMTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}


	public void setAccessRightsGroup(PPSLMTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}

	

	
	
}
