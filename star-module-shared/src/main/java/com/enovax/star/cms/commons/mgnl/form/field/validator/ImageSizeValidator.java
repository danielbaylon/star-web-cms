package com.enovax.star.cms.commons.mgnl.form.field.validator;

import com.enovax.star.cms.commons.mgnl.form.field.validator.definition.ImageSizeValidatorDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.AbstractStringValidator;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.metadata.MagnoliaAssetMetadata;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class ImageSizeValidator extends AbstractStringValidator {
    private static final Logger log = LoggerFactory.getLogger(ImageSizeValidator.class);


    private DamTemplatingFunctions damObj;
    private ImageSizeValidatorDefinition definition;

    private final Item item;

    public ImageSizeValidator(Item item, String errorMessage, ImageSizeValidatorDefinition definition) {
        super(errorMessage);
        this.item = item;
        this.definition = definition;
    }

    @Override
    protected boolean isValidValue(String value) {
        if(StringUtils.isEmpty(value)) {
            return true;
        }
        this.damObj = Components.getComponent(DamTemplatingFunctions.class);
        Asset ast = damObj.getAsset(value);
        return ast== null ? false : checkImageDimensions(ast);
    }

    @Override
    public void validate(Object value) throws Validator.InvalidValueException {
        super.validate(value);
    }

    public boolean checkImageDimensions(Asset ast){
        int originalImgWidth          = definition.getWidth();
        int originalImgHeight         = definition.getHeight();

        if (ast.supports(MagnoliaAssetMetadata.class)) {
            MagnoliaAssetMetadata metadata = ast.getMetadata(MagnoliaAssetMetadata.class);
            long width = metadata.getWidth();
            long height = metadata.getHeight();

            if (width <= originalImgWidth & height <= originalImgHeight) {
                return true;
            }
        }
        return false;
    }
}
