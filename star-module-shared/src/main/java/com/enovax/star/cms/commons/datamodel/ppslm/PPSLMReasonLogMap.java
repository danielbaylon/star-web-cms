package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = PPSLMReasonLogMap.TABLE_NAME)
public class PPSLMReasonLogMap implements Serializable {

    public static final String TABLE_NAME = "PPSLMReasonLogMap";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "reasonId")
    private Integer reasonId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reasonId", updatable = false, insertable = false)
    private PPSLMReason reason;

    @Column(name = "logId")
    private Integer logId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "logId", updatable = false, insertable = false)
    private PPSLMApprovalLog log;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public PPSLMReason getReason() {
        return reason;
    }

    public void setReason(PPSLMReason reason) {
        this.reason = reason;
    }

    public PPSLMApprovalLog getLog() {
        return log;
    }

    public void setLog(PPSLMApprovalLog log) {
        this.log = log;
    }
}
