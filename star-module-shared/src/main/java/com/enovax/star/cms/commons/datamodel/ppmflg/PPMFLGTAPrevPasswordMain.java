package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

@Entity
@Table(name="PPMFLGTAPrevPasswordMain")
public class PPMFLGTAPrevPasswordMain extends PPMFLGPrevPassword {
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "mainAccountId",nullable = false)
    private PPMFLGTAMainAccount user;
    

    
    
    public PPMFLGTAMainAccount getUser() {
        return user;
    }
    public void setUser(PPMFLGTAMainAccount user) {
        this.user = user;
    }
    
    
    
}
