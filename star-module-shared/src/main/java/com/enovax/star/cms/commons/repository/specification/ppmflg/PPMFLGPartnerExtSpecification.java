package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 8/9/16.
 */
public class PPMFLGPartnerExtSpecification extends BaseSpecification<PPMFLGPartnerExt> {
}
