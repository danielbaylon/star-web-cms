package com.enovax.star.cms.commons.service.axchannel;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;
import com.enovax.star.cms.commons.model.axchannel.inventtable.transformer.AxInventTableResponseTransformer;
import com.enovax.star.cms.commons.model.axchannel.onlinecart.AxOnlineParameter;
import com.enovax.star.cms.commons.model.axchannel.onlinecart.transformer.AxOnlineCartTransformer;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellProduct;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.transformer.AxUpCrossSellResponseTransformer;
import com.enovax.star.cms.commons.ws.axchannel.inventtable.*;
import com.enovax.star.cms.commons.ws.axchannel.pin.ISDCPINService;
import com.enovax.star.cms.commons.ws.axchannel.pin.SDCOnLineParametersResponse;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.ISDCRetailTicketTableService;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCGetUpCrossSellResponse;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.UpCrossSellTableCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings("Duplicates")
@Service
public class AxChannelProductsService {

    //TODO More logging
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelServiceRegistry axChannelServiceRegistry;

    public void setAxChannelServiceRegistry(AxChannelServiceRegistry axChannelServiceRegistry) { this.axChannelServiceRegistry = axChannelServiceRegistry; }

    /**
     * Return product extension data from the channel DB. Can return specific items or all items.<br><br>
     *
     * WS: /services/SDC_InventTableService.svc <br>
     * API: SearchProductExt
     */
    public ApiResult<List<AxRetailProductExtData>> apiSearchProductExt(StoreApiChannels channel,
                                                                       boolean isAll, String itemId, Long listingId) throws AxChannelException {
        log.info("[AX Channel Product Service] Calling apiSearchProductExt.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCInventTableService stub = channelStub.getStubInventTable();

            final com.enovax.star.cms.commons.ws.axchannel.inventtable.ObjectFactory factory =
                    new com.enovax.star.cms.commons.ws.axchannel.inventtable.ObjectFactory();

            final SDCInventTableExtCriteria criteria = factory.createSDCInventTableExtCriteria();
            if (isAll) {
                criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(""));
                criteria.setProductId(0L);
            } else {
                criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(itemId));
                criteria.setProductId(listingId);
            }

            final SDCInventTableResponse axResponse = stub.searchProductExt(criteria);

            final ApiResult<List<AxRetailProductExtData>> result = AxInventTableResponseTransformer.fromWsProductExt(axResponse);

            return result;
        } catch (Exception e) {
            final String errMsg = "[AX Channel Product Service] Error: apiSearchProductExt";
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Return product extension data from the channel DB. Can return specific items or all items.<br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc<br>
     * API: GetUpCrossSell
     */
    public ApiResult<List<AxRetailUpCrossSellProduct>> apiGetUpCrossSellData(StoreApiChannels channel) throws AxChannelException {
        log.info("[AX Channel Product Service] Calling apiGetUpCrossSellData");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }


            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();

            com.enovax.star.cms.commons.ws.axchannel.retailticket.ObjectFactory factory = new com.enovax.star.cms.commons.ws.axchannel.retailticket.ObjectFactory();

            final UpCrossSellTableCriteria criteria = factory.createUpCrossSellTableCriteria();
            criteria.setItemId(factory.createUpCrossSellTableCriteriaItemId(""));
            criteria.setChannel(0L);
            criteria.setDataLevelValue(0);

            final SDCGetUpCrossSellResponse axResponse = stub.getUpCrossSell(criteria);

            ApiResult<List<AxRetailUpCrossSellProduct>> result = AxUpCrossSellResponseTransformer.fromWs(axResponse);

            return result;
        } catch (Exception e) {
            final String errMsg = "[AX Channel Product Service] Error: apiGetUpCrossSellData";
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }

    }

    /**
     * Search ticket template for product, or get all templates. <br><br>
     *
     * WS: /services/SDC_OnlineCartService.svc <br>
     * API: SearchXmlTemplate
     */
    public ApiResult<List<AxRetailTemplateXML>> apiSearchProductTicketTemplate(StoreApiChannels channel, String templateName) throws AxChannelException {
        log.info("[AX Channel Product Service] Calling apiSearchProductTicketTemplate...");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCInventTableService stub = channelStub.getStubInventTable();

            final com.enovax.star.cms.commons.ws.axchannel.inventtable.ObjectFactory factory =
                    new com.enovax.star.cms.commons.ws.axchannel.inventtable.ObjectFactory();

            final SDCTemplateXMLCriteria criteria = factory.createSDCTemplateXMLCriteria();
            criteria.setTemplateName(factory.createSDCTemplateXMLCriteriaTemplateName(templateName));

            final SDCTemplateXMLSearchResponse axResponse = stub.searchXmlTemplate(criteria);

            final ApiResult<List<AxRetailTemplateXML>> result = AxInventTableResponseTransformer.fromWsSearchXmlTemplate(axResponse);

            return result;
        } catch (Exception e) {
            final String errMsg = "[AX Channel Product Service] Error: apiSearchProductExt";
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Get system parameters from AX for online channels. <br><br>
     *
     * WS: /services/SDC_OnlineCartService.svc <br>
     * API: GetOnLineParameters
     */
    public ApiResult<List<AxOnlineParameter>> apiGetOnlineSystemParameters(StoreApiChannels channel) throws AxChannelException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinSrv = stub.getStubPIN();
        SDCOnLineParametersResponse response = pinSrv.getOnLineParameters();
        return AxOnlineCartTransformer.fromWsOnlineParameters(response);
    }
/****
 *

 private boolean firstStart = true;

 private Map<StoreApiChannels, AxRetailProductExtServiceStubDetails> channelStubs = new HashMap<>();

 @Value("${api.store.axretail.api.url.inventtable.b2cmflg}")
 private String apiUrlB2CMFLG;
 @Value("${api.store.axretail.api.url.inventtable.b2cslm}")
 private String apiUrlB2CSLM;
 @Value("${api.store.axretail.api.url.inventtable.kiosk}")
 private String apiUrlKIOSK;
 @Value("${api.store.axretail.api.url.inventtable.partnerportal}")
 private String apiUrlPARTNERPORTAL;

 //TODO Synchronization issues on initialisation

 @PostConstruct
 private void initService() {
 try {

 if (StringUtils.isEmpty(apiUrlB2CSLM) || "${api.store.axretail.api.url.inventtable.b2cslm}".equals(apiUrlB2CSLM)) {
 //                apiUrlB2CSLM = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_InventTableService.svc";
 apiUrlB2CSLM = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50000/Services/SDC_InventTableService.svc";
 }
 if (StringUtils.isEmpty(apiUrlB2CMFLG) || "${api.store.axretail.api.url.inventtable.b2cmflg}".equals(apiUrlB2CMFLG)) {
 apiUrlB2CMFLG = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50005/Services/SDC_InventTableService.svc";
 }
 if (StringUtils.isEmpty(apiUrlPARTNERPORTAL) || "${api.store.axretail.api.url.inventtable.partnerportal}".equals(apiUrlPARTNERPORTAL)) {
 //                apiUrlPARTNERPORTAL = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50006/Services/SDC_InventTableService.svc";
 apiUrlPARTNERPORTAL = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:50001/Services/SDC_InventTableService.svc";
 }
 if (StringUtils.isEmpty(apiUrlKIOSK) || "${api.store.axretail.api.url.inventtable.kiosk}".equals(apiUrlKIOSK)) {
 apiUrlKIOSK = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50007/Services/SDC_InventTableService.svc";
 }

 channelStubs.put(StoreApiChannels.B2C_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlB2CMFLG));
 channelStubs.put(StoreApiChannels.B2C_SLM, new AxRetailProductExtServiceStubDetails(apiUrlB2CSLM));
 channelStubs.put(StoreApiChannels.KIOSK_SLM, new AxRetailProductExtServiceStubDetails(apiUrlKIOSK));
 channelStubs.put(StoreApiChannels.KIOSK_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlKIOSK));
 channelStubs.put(StoreApiChannels.PARTNER_PORTAL_SLM, new AxRetailProductExtServiceStubDetails(apiUrlPARTNERPORTAL));
 channelStubs.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new AxRetailProductExtServiceStubDetails(apiUrlPARTNERPORTAL));

 if (firstStart) { firstStart = false; return; }

 boolean success;

 log.info("Initialising AX Retail Product Ext Service for B2C MFLG.");
 success = channelStubs.get(StoreApiChannels.B2C_MFLG).initialise();
 log.info("AX Retail Product Ext Service for B2C MFLG : INIT " + (success ? "SUCCESS" : "FAILED"));

 log.info("Initialising AX Retail Product Ext Service for B2C SLM.");
 success = channelStubs.get(StoreApiChannels.B2C_SLM).initialise();
 log.info("AX Retail Product Ext Service for B2C SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

 log.info("Initialising AX Retail Product Ext Service for KIOSK SLM.");
 success = channelStubs.get(StoreApiChannels.KIOSK_SLM).initialise();
 log.info("AX Retail Product Ext Service for KIOSK SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

 log.info("Initialising AX Retail Product Ext Service for KIOSK MFLG.");
 success = channelStubs.get(StoreApiChannels.KIOSK_MFLG).initialise();
 log.info("AX Retail Product Ext Service for KIOSK MFLG: INIT " + (success ? "SUCCESS" : "FAILED"));

 log.info("Initialising AX Product Ext Retail Service for PARTNER PORTAL SLM.");
 success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_SLM).initialise();
 log.info("AX Retail Service Product Ext for PARTNER PORTAL SLM : INIT " + (success ? "SUCCESS" : "FAILED"));

 log.info("Initialising AX Product Ext Retail Service for PARTNER PORTAL MFLG.");
 success = channelStubs.get(StoreApiChannels.PARTNER_PORTAL_MFLG).initialise();
 log.info("AX Retail Service Product Ext for PARTNER PORTAL MFLG : INIT " + (success ? "SUCCESS" : "FAILED"));

 } catch (Exception e) {
 log.error("Error initialising AX Retail Product Ext Service.", e);
 }
 }

 public ApiResult<List<AxRetailProductExtData>> apiSearchProductExt(StoreApiChannels channel,
 boolean isAll, String itemId, Long listingId) throws Exception {
 log.info("[AX Retail Product Ext Service Client] Calling apiSearchProductExt");
 try {


 final AxRetailProductExtServiceStubDetails channelStub = channelStubs.get(channel);
 if (!channelStub.isInit()) {
 channelStub.initialise();
 }
 final ISDCInventTableService stub = channelStub.getStub();

 ObjectFactory factory = new ObjectFactory();

 final SDCInventTableExtCriteria criteria = factory.createSDCInventTableExtCriteria();
 if (isAll) {
 criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(""));
 criteria.setProductId(0L);
 } else {
 criteria.setItemId(factory.createSDCInventTableExtCriteriaItemId(itemId));
 criteria.setProductId(listingId);
 }

 final SDCInventTableResponse axResponse = stub.searchProductExt(criteria);
 final ApiResult<List<AxRetailProductExtData>> result = TempAxRetailProductExtResponseTransformer.fromWs(axResponse);

 return result;
 } catch (Exception e) {
 final String errMsg = "[AX Retail Product Ext Service Client] Error: apiSearchProductExt";
 log.error(errMsg, e);
 throw new Exception(errMsg, e);
 }
 }

 */
}
