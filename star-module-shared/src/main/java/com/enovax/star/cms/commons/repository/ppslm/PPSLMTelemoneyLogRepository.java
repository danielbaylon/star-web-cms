package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTelemoneyLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 14/11/16.
 */
@Repository
public interface PPSLMTelemoneyLogRepository extends JpaRepository<PPSLMTelemoneyLog, Integer> {

}
