package com.enovax.star.cms.commons.model.axchannel.customerext;

import java.math.BigDecimal;

/**
 * Created by houtao on 17/8/16.
 */
public class AxCustomerBalances {

    private BigDecimal balance;
    private BigDecimal balanceAmount;
    private BigDecimal creditLimitAmount;
    private BigDecimal openOrderAmount;
    private String accountNum;
    private Integer mandatoryCreditLimit;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public BigDecimal getCreditLimitAmount() {
        return creditLimitAmount;
    }

    public void setCreditLimitAmount(BigDecimal creditLimitAmount) {
        this.creditLimitAmount = creditLimitAmount;
    }

    public BigDecimal getOpenOrderAmount() {
        return openOrderAmount;
    }

    public void setOpenOrderAmount(BigDecimal openOrderAmount) {
        this.openOrderAmount = openOrderAmount;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Integer getMandatoryCreditLimit() {
        return mandatoryCreditLimit;
    }

    public void setMandatoryCreditLimit(Integer mandatoryCreditLimit) {
        this.mandatoryCreditLimit = mandatoryCreditLimit;
    }
}
