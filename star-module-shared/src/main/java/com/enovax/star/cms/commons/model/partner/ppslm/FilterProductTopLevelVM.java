package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.*;

/**
 * Created by lavanya on 7/11/16.
 */
public class FilterProductTopLevelVM implements Comparable<FilterProductTopLevelVM>{
    private String productId;
    private String prodName;
    private List<String> itemNames = new ArrayList<>();

    public static List<FilterProductTopLevelVM> convert(List<FilterProductItemRowVM> flatRows) {
        final List<FilterProductTopLevelVM> rows = new ArrayList<>();

        final Map<String, FilterProductTopLevelVM> prodMap = new HashMap<String, FilterProductTopLevelVM>();
        for (FilterProductItemRowVM flatRow : flatRows) {
            FilterProductTopLevelVM prod = prodMap.get(flatRow.getProductId());
            if (prod == null) {
                prod = new FilterProductTopLevelVM();
                prod.setProdName(flatRow.getProdName());
                prod.setProductId(flatRow.getProductId());
                prodMap.put(flatRow.getProductId(), prod);
            }
            prod.getItemNames().add(flatRow.getItemName());
        }

        for (FilterProductTopLevelVM mapped : prodMap.values()) {
            Collections.sort(mapped.getItemNames());
            rows.add(mapped);
        }
        Collections.sort(rows);

        return rows;
    }

    @Override
    public int compareTo(FilterProductTopLevelVM other) {
        return this.productId.compareTo(other.productId);
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }
    public String getProdName() {
        return prodName;
    }
    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
    public List<String> getItemNames() {
        return itemNames;
    }
    public void setItemNames(List<String> itemNames) {
        this.itemNames = itemNames;
    }
}
