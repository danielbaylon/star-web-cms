package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;

public class TelemoneyProcessingDataB2C extends TelemoneyProcessingData {

    private boolean slm;
    private B2CSLMStoreTransaction slmTxn;
    private B2CMFLGStoreTransaction mflgTxn;
    private StoreTransactionExtensionData extData;
    private StoreTransaction txn;
    private FunCart cart;

    public StoreTransaction getTxn() {
        return txn;
    }

    public void setTxn(StoreTransaction txn) {
        this.txn = txn;
    }

    public FunCart getCart() {
        return cart;
    }

    public void setCart(FunCart cart) {
        this.cart = cart;
    }

    public B2CMFLGStoreTransaction getMflgTxn() {
        return mflgTxn;
    }

    public void setMflgTxn(B2CMFLGStoreTransaction mflgTxn) {
        this.mflgTxn = mflgTxn;
    }

    public B2CSLMStoreTransaction getSlmTxn() {
        return slmTxn;
    }

    public void setSlmTxn(B2CSLMStoreTransaction slmTxn) {
        this.slmTxn = slmTxn;
    }

    public boolean isSlm() {
        return slm;
    }

    public void setSlm(boolean slm) {
        this.slm = slm;
    }

    public StoreTransactionExtensionData getExtData() {
        return extData;
    }

    public void setExtData(StoreTransactionExtensionData extData) {
        this.extData = extData;
    }
}
