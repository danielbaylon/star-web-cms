package com.enovax.star.cms.commons.model.axchannel.b2bupdatepinstatus;

/**
 * Created by jennylynsze on 10/7/16.
 */
public class AxB2BUpdatePinStatus {
    private String pinCode;
    private Integer pinStatus;
    private String reasonCode;
    private String processedBy;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Integer getPinStatus() {
        return pinStatus;
    }

    public void setPinStatus(Integer pinStatus) {
        this.pinStatus = pinStatus;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }
}
