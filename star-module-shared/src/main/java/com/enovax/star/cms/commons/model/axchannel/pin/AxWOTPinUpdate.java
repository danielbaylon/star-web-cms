package com.enovax.star.cms.commons.model.axchannel.pin;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houtao on 18/8/16.
 */
public class AxWOTPinUpdate {

    private String pinCode;
    private Integer qty;
    private String eventLineId;
    private Date eventDate;
    private String markupCode;
    private BigDecimal markupAmt;
    private String itemId;
    private String inventTransId;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getMarkupCode() {
        return markupCode;
    }

    public void setMarkupCode(String markupCode) {
        this.markupCode = markupCode;
    }

    public BigDecimal getMarkupAmt() {
        return markupAmt;
    }

    public void setMarkupAmt(BigDecimal markupAmt) {
        this.markupAmt = markupAmt;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getInventTransId() {
        return inventTransId;
    }

    public void setInventTransId(String inventTransId) {
        this.inventTransId = inventTransId;
    }
}
