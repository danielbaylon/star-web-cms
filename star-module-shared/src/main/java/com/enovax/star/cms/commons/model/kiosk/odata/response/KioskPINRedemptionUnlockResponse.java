
package com.enovax.star.cms.commons.model.kiosk.odata.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Collection(SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTableEntity)
 * 
 * @author dbaylon
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskPINRedemptionUnlockResponse {

	@JsonProperty("value")
	private Object object;

	public void setObject(Object object) {
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

}
