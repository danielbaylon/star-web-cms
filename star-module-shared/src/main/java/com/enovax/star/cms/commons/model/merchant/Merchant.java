package com.enovax.star.cms.commons.model.merchant;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by jace on 15/9/16.
 */
public class Merchant {

  private String id;
  private String name;
  private String merchantId;
  private boolean acceptsAllCards;
  private boolean active;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public boolean isAcceptsAllCards() {
    return acceptsAllCards;
  }

  public void setAcceptsAllCards(boolean acceptsAllCards) {
    this.acceptsAllCards = acceptsAllCards;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Merchant && StringUtils.equals(getId(), ((Merchant) obj).getId())) {
      return true;
    }
    return super.equals(obj);
  }
}
