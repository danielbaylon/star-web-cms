package com.enovax.star.cms.commons.repository.specification.query;

import java.util.Date;
import java.util.List;

public class QCriteria {

    public enum Operation {
        EQ("="), LT("<"), LE("<="), GT(">"), GE(">="), LIKE("%"), NOTNULL("NOTNULL"), IN("IN");
        private String code;
        Operation(String code){
            this.code = code;
        }
        public String code(){
           return this.code;
        };
    };

    public enum ValueType {
        String(0), Integer(1), Date(3);
        private int code;
        ValueType(int code){this.code = code; }
        public int code(){ return this.code; }
    }

    private String key;
    private Operation operation;
    private String value;
    private Integer intValue;
    private Date dateValue;
    private ValueType valueType;
    private List<?> objects;

    public QCriteria(String key, Operation operation, String value){
        this.key = key;
        this.operation = operation;
        this.value = value;
        if(value != null && value.trim().length() > 0){
            this.valueType = ValueType.String;
        }
    }

    public QCriteria(String key, Operation operation, Integer value){
        this.key = key;
        this.operation = operation;
        this.intValue = value;
        if(value != null) {
            this.valueType = ValueType.Integer;
        }
    }

    public QCriteria(String key, Operation operation, Date value){
        this.key = key;
        this.operation = operation;
        this.dateValue = value;
        if(value != null) {
            this.valueType = ValueType.Date;
        }
    }

    public QCriteria(String key, Operation operation, List<?> objects, ValueType type){
        this.key = key;
        this.operation = operation;
        this.objects = objects;
        this.valueType = type;
    }

    public String getKey() {
        return key;
    }

    public Operation getOperation() {
        return operation;
    }

    public String getValue() {
        return value;
    }

    public Integer getIntValue() {
        return intValue;
    }

    public void setIntValue(Integer intValue) {
        this.intValue = intValue;
    }


    public ValueType getValueType() {
        return valueType;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public List<?> getObjects() {
        return objects;
    }
}
