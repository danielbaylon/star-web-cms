package com.enovax.star.cms.commons.mgnl.form.field;

import com.enovax.star.cms.commons.mgnl.form.field.definition.CustomDisplayLinkFieldDefinition;
import com.vaadin.data.Property;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.*;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.app.AppController;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.form.field.component.ContentPreviewComponent;
import info.magnolia.ui.form.field.converter.IdentifierToPathConverter;
import org.apache.commons.lang3.StringUtils;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

public class CustomDisplayLinkField extends CustomField<String> {
    // Define layout and component
    private final VerticalLayout rootLayout = new VerticalLayout();
    private final HorizontalLayout linkLayout = new HorizontalLayout();
    private final CustomStaticField displayField = new CustomStaticField("");
    private final TextField textField = new TextField(); //TODO note this field is not being used, but i will retain it anyway
    private final TextField valueField = new TextField();

    private final Button selectButton = new NativeButton();

    private final IdentifierToPathConverter converter;
    private final CustomDisplayLinkFieldDefinition definition;
    private String buttonCaptionNew;
    private String buttonCaptionOther;

    private final ComponentProvider componentProvider;

    public CustomDisplayLinkField(CustomDisplayLinkFieldDefinition linkFieldDefinition, ComponentProvider componentProvider) {
        this.definition = linkFieldDefinition;
        this.converter = definition.getIdentifierToPathConverter();
        if (this.converter != null) {
            this.converter.setWorkspaceName(definition.getTargetWorkspace());
        }
        this.componentProvider = componentProvider;
        setImmediate(true);
    }

    @Deprecated
    public CustomDisplayLinkField(CustomDisplayLinkFieldDefinition linkFieldDefinition, AppController appController, UiContext uiContext, ComponentProvider componentProvider) {
        this(linkFieldDefinition, componentProvider);
    }

    @Override
    protected Component initContent() {
        addStyleName("linkfield");
        // Initialize root
        rootLayout.setSizeFull();
        rootLayout.setSpacing(true);
        // Handle Text Field
        textField.setImmediate(true);
        textField.setWidth(100, Unit.PERCENTAGE);
        textField.setNullRepresentation("");
        textField.setNullSettingAllowed(true);
        textField.setEnabled(this.definition.isEnabled());

        // Handle Link Layout (Text Field & Select Button)
        linkLayout.setSizeFull();
        linkLayout.addComponent(displayField);
        linkLayout.setExpandRatio(displayField, 1);
        linkLayout.setComponentAlignment(displayField, Alignment.MIDDLE_LEFT);

        // Only Handle Select button if the Text field is not Read Only.
        if (!textField.isReadOnly()) {
            selectButton.addStyleName("magnoliabutton");
            linkLayout.addComponent(selectButton);
            linkLayout.setExpandRatio(selectButton, 0);
            linkLayout.setComponentAlignment(selectButton, Alignment.MIDDLE_RIGHT);
        }

        //initiliaze the value for the text field
        if(!StringUtils.isEmpty(valueField.getValue())) {
            textField.setValue(generateTextFieldDisplay(valueField.getValue()));
            displayField.setLabelValue(generateTextFieldDisplay(valueField.getValue()));
        }

        setButtonCaption(getDisplay()); //based the caption on the textfield display.

        if(!textField.isEnabled() && StringUtils.isNotBlank(getDisplay())) {
            textField.setEnabled(true);
            textField.setReadOnly(true);
        }

        rootLayout.addComponent(linkLayout);

        // Register the content preview if it's define.
        if (definition.getContentPreviewDefinition() != null && definition.getContentPreviewDefinition().getContentPreviewClass() != null) {
            registerContentPreview();
        }
        return rootLayout;
    }

    public TextField getTextField() {
        return this.valueField;
    }

    public Button getSelectButton() {
        return this.selectButton;
    }

    @Override
    public String getValue() {
        return valueField.getValue();
    }

    @Override
    public void setValue(String newValue) throws ReadOnlyException, Converter.ConversionException {
        valueField.setValue(newValue);
    }

    public String getDisplay() {
        return textField.getValue();
    }

    public void setDisplay(String newValue) {
        boolean isReadOnly = textField.isReadOnly();
        if(isReadOnly) {
            textField.setReadOnly(false); //change first to false
        }
        textField.setValue(newValue);
        displayField.setLabelValue(newValue);
        if(isReadOnly) {
            textField.setReadOnly(true); //change to true again
        }

        if(!textField.isEnabled() && StringUtils.isNotBlank(getDisplay())) {
            textField.setEnabled(true);
            textField.setReadOnly(true);
        }
    }

    /**
     * Update the Link component. <br>
     * - Set text Field as read only if desired. In this case remove the add button.
     * - If it is not read only. update the button label.
     */
    protected void updateComponents(String currentValue) {
        if (!definition.isFieldEditable() && StringUtils.isNotBlank(currentValue)) {
            setDisplay(generateTextFieldDisplay(currentValue));
            textField.setReadOnly(true);
            if (linkLayout.getComponentIndex(selectButton) != -1) {
                linkLayout.removeComponent(selectButton);
            }
        } else {
            setDisplay(generateTextFieldDisplay(currentValue));
            setButtonCaption(getDisplay());  //change to showing display
        }
    }


    protected String generateTextFieldDisplay(String currentValue) {
        Session session = null;
        try {
            session = MgnlContext.getJCRSession(definition.getTargetWorkspace());

            if(CustomDisplayLinkFieldDefinition.NODE_NAME.equals(definition.getTargetPropertyToDisplay())) {
                return session.getNodeByIdentifier(currentValue).getName();
            }
            return session.getNodeByIdentifier(currentValue).getProperty(definition.getTargetPropertyToDisplay()).getString();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Set propertyDatasource.
     * If the translator is not null, set it as datasource.
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void setPropertyDataSource(Property newDataSource) {
        if (converter != null) {
            textField.setConverter(converter);
        }
        //TODO made changes here
        valueField.setPropertyDataSource(newDataSource);
        valueField.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                String value = event.getProperty().getValue() != null ? event.getProperty().getValue().toString() : StringUtils.EMPTY;
                updateComponents(value);
            }
        });

        super.setPropertyDataSource(newDataSource);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Property getPropertyDataSource() {
        return valueField.getPropertyDataSource();
    }   //TODO change this one

    @Override
    public Class<String> getType() {
        return String.class;
    }

    private void setButtonCaption(String value) {
        if (StringUtils.isNotBlank(value)) {
            selectButton.setCaption(buttonCaptionOther);
            selectButton.setDescription(buttonCaptionOther);
        } else {
            selectButton.setCaption(buttonCaptionNew);
            selectButton.setDescription(buttonCaptionNew);
        }
    }

    private void registerContentPreview() {
        final ContentPreviewComponent<?> contentPreviewComponent = Components.newInstance(definition.getContentPreviewDefinition().getContentPreviewClass(), definition.getTargetWorkspace(), componentProvider);
        textField.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                String itemReference = event.getProperty().getValue().toString();
                contentPreviewComponent.onValueChange(itemReference);
                contentPreviewComponent.setVisible(StringUtils.isNotBlank(itemReference));
            }
        });
        contentPreviewComponent.onValueChange(textField.getValue());
        contentPreviewComponent.setVisible(StringUtils.isNotBlank(textField.getValue()));
        rootLayout.addComponentAsFirst(contentPreviewComponent);
    }

    /**
     * Caption section.
     */
    public void setButtonCaptionNew(String buttonCaptionNew) {
        this.buttonCaptionNew = buttonCaptionNew;
    }

    public void setButtonCaptionOther(String buttonCaptionOther) {
        this.buttonCaptionOther = buttonCaptionOther;
    }

}
