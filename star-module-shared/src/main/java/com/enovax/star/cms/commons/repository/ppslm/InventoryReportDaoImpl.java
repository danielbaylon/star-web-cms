package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.InventoryReportModel;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by lavanya on 8/11/16.
 */
@Repository
public class InventoryReportDaoImpl implements InventoryReportDao {
    @Autowired(required = false)
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    private static final String SQL_INV_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, it.receiptNum as receiptNum, it.createdDate as txnDate, " +
                    "it.validityEndDate as expiryDate, item.displayName as itemDesc, item.qty as qtyPurchased,  " +
                    "item.qty - item.unpackagedQty as qtyPackaged, item.unpackagedQty as balance,  " +
                    "isnull(rt.receiptNum, '') as revalidateReference, it.status as transStatus, it.tmStatus as tmStatus, " +
                    "case  " +
                    "when it.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when it.status in ('Available', 'Revalidated', 'Refunded', 'Expired', 'Expiring') then 'Successful' " +
                    "when it.status in ('Failed') then 'Failed' " +
                    "end as transStatusText " +
                    "from PPSLMInventoryTransaction it " +
                    "inner join PPSLMTAMainAccount acct on it.mainAccountId = acct.id " +
                    "inner join PPSLMPartner partner on acct.id = partner.adminId " +
                    "inner join PPSLMInventoryTransactionItem item on it.id = item.transactionId " +
                    "left join PPSLMRevalidationTransaction rt on it.id = rt.transactionId " +
                    "where 1 = 1 " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_IR_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_IR_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";

    private static final String SQL_IR_CLAUSE_TXN_DT = " and it.createdDate >= :fromDate and it.createdDate - 1 <= :toDate ";
    private static final String SQL_IR_CLAUSE_RECEIPT = " and it.receiptNum = :receiptNum ";
    private static final String SQL_IR_CLAUSE_STATUS = "and it.status in (:statuses) ";
    private static final String SQL_IR_CLAUSE_ITEM_NAME = " and it.id in (select distinct item2.transactionId from PPSLMInventoryTransactionItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    @SuppressWarnings("unchecked")
    public ReportResult<InventoryReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                             String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<InventoryReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_IR_CLAUSE_TXN_DT);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_IR_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_IR_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (StringUtils.isNotEmpty(filter.getReceiptNum())) {
            invSb.append(SQL_IR_CLAUSE_RECEIPT);

            params.put("receiptNum",  "%" + filter.getOrgName() + "%");
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_PA_CLAUSE);
            params.put("paIds", filter.getPaIds() );
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_IR_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

        }

        invSb.append(SQL_IR_CLAUSE_STATUS);
        final List<String> statuses;
        if (StringUtils.isNotEmpty(filter.getStatus())) {
            statuses = Arrays.asList(filter.getStatus());
        } else {
            statuses = Arrays.asList("Available", "Revalidated", "Refunded", "Expired", "Expiring", "Forfeited");
        }
        params.put("statuses", statuses);

        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "receiptNum":
                    sqlOrderBy = " order by tbl.receiptNum "; break;
                case "txnDate":
                case "txnDateText":
                    sqlOrderBy = " order by tbl.txnDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "itemDesc":
                    sqlOrderBy = " order by tbl.itemDesc "; break;
                case "revalidateReference":
                    sqlOrderBy = " order by tbl.revalidateReference "; break;
                case "transStatus":
                    sqlOrderBy = " order by tbl.transStatus "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_INV_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("receiptNum", StandardBasicTypes.STRING)
                .addScalar("txnDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("itemDesc", StandardBasicTypes.STRING)
                .addScalar("qtyPurchased", StandardBasicTypes.INTEGER)
                .addScalar("qtyPackaged", StandardBasicTypes.INTEGER)
                .addScalar("balance", StandardBasicTypes.INTEGER)
                .addScalar("revalidateReference", StandardBasicTypes.STRING)
                .addScalar("transStatus", StandardBasicTypes.STRING)
                .addScalar("tmStatus", StandardBasicTypes.STRING)
                .addScalar("transStatusText", StandardBasicTypes.STRING)
                .setResultTransformer(Transformers.aliasToBean(InventoryReportModel.class));

        final List<InventoryReportModel> theList = qry.list();
        for (InventoryReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }

    public SQLQuery prepareSqlQuery(String query, Map<String, Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        for (String key : parameters.keySet()) {
            Object para = parameters.get(key);
            if (para instanceof List<?>) {
                queryObject.setParameterList(key, (List<?>)para);
            } else {
                queryObject.setParameter(key, para);
            }
        }
        return queryObject;
    }
}
