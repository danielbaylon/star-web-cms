package com.enovax.star.cms.commons.tracking;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jensen on 19/5/15.
 */
@Service
public class Trackr {

    public Map<String, String> getHeaderMap(HttpServletRequest req) {
        final Map<String, String> headers = new HashMap<>();
        final Enumeration<String> names = req.getHeaderNames();
        while (names.hasMoreElements()) {
            final String name = names.nextElement();
            headers.put(name, req.getHeader(name));
        }
        return headers;
    }

    public Trackd buildTrackd(String sid, HttpServletRequest req) {
        final Trackd trackd = new Trackd();

        trackd.setSid(sid);
        trackd.setAccessDate(new Date());

        final StringBuffer reqUrl = req.getRequestURL();
        final String qryStr = req.getQueryString();
        final String fullUrl = reqUrl.append(StringUtils.isEmpty(qryStr) ? "" : "?" + qryStr).toString();
        trackd.setFullUrl(fullUrl);
        trackd.setParams(req.getParameterMap());
        trackd.setCookies(req.getCookies());
        trackd.setReq(req);
        trackd.setContextPath(req.getContextPath());
        String theIp = req.getHeader("X-FORWARDED-FOR");
        trackd.setIp(StringUtils.isEmpty(theIp) ? req.getRemoteAddr() : theIp);

        final Map<String, String> headers = getHeaderMap(req);
        trackd.setHeaders(headers);

        trackd.setAcceptLang(headers.get(HeaderNames.AcceptLanguage.code));
        trackd.setHost(headers.get(HeaderNames.Host.code));
        trackd.setReferrer(headers.get(HeaderNames.Referrer.code));
        trackd.setUserAgent(headers.get(HeaderNames.UserAgent.code));

        return trackd;
    }
}
