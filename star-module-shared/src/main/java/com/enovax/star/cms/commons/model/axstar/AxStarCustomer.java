package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 4/8/16.
 */
public class AxStarCustomer {

    private String  accountNumber = null;
    private Long recordId = null;
    private Date    createdDateTime = null;
    private String  chargeGroup = null;
    private String  priceGroup = null;
    private Boolean isCustomerTaxInclusive = null;
    private String  phone = null;
    private Long phoneRecordId = null;
    private String  phoneExt = null;
    private String  cellphone = null;
    private String  email = null;
    private Long emailRecordId = null;
    private String  url = null;
    private Long urlRecordId = null;
    private String  name = null;
    private Long personNameId = null;
    private String  firstName = null;
    private String  middleName = null;
    private String  lastName = null;
    private Long directoryPartyRecordId = null;
    private String  partyNumber = null;
    private Long customerTypeValue = null;
    private String  language = null;
    private String  customerGroup = null;
    private String  currencyCode = null;
    private String  cNPJCPFNumber = null;
    private String  identificationNumber = null;
    private String  invoiceAccount = null;
    private Boolean mandatoryCreditLimit = null;
    private String  creditRating = null;
    private BigDecimal creditLimit = null;
    private BigDecimal balance = null;
    private Boolean blocked = null;
    private Boolean useOrderNumberReference = null;
    private String  organizationId = null;
    private Boolean usePurchaseRequest = null;
    private String  multilineDiscountGroup = null;
    private String  totalDiscountGroup = null;
    private String  lineDiscountGroup = null;
    private String  taxGroup = null;
    private String  salesTaxGroup = null;
    private String  taxExemptNumber = null;
    private String  vatNumber = null;
    private String  taxOffice = null;
    private Boolean nonChargeableAccount = null;
    private String  tag = null;
    private Long receiptSettings = null;
    private String  receiptEmail = null;
    private Long retailCustomerTableRecordId = null;
    private String  offlineImage = null;
    private String  newCustomerPartyNumber = null;
    private List<AxStarAffiliation> customerAffiliations = null;
    private List<AxStarAddress>     addresses = null;
    private AxStarRichMediaLocations image = null;
    private List<AxStarExtensionProperty> extensionProperties = null;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getChargeGroup() {
        return chargeGroup;
    }

    public void setChargeGroup(String chargeGroup) {
        this.chargeGroup = chargeGroup;
    }

    public String getPriceGroup() {
        return priceGroup;
    }

    public void setPriceGroup(String priceGroup) {
        this.priceGroup = priceGroup;
    }

    public Boolean getCustomerTaxInclusive() {
        return isCustomerTaxInclusive;
    }

    public void setCustomerTaxInclusive(Boolean customerTaxInclusive) {
        isCustomerTaxInclusive = customerTaxInclusive;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getPhoneRecordId() {
        return phoneRecordId;
    }

    public void setPhoneRecordId(Long phoneRecordId) {
        this.phoneRecordId = phoneRecordId;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getEmailRecordId() {
        return emailRecordId;
    }

    public void setEmailRecordId(Long emailRecordId) {
        this.emailRecordId = emailRecordId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getUrlRecordId() {
        return urlRecordId;
    }

    public void setUrlRecordId(Long urlRecordId) {
        this.urlRecordId = urlRecordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPersonNameId() {
        return personNameId;
    }

    public void setPersonNameId(Long personNameId) {
        this.personNameId = personNameId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getDirectoryPartyRecordId() {
        return directoryPartyRecordId;
    }

    public void setDirectoryPartyRecordId(Long directoryPartyRecordId) {
        this.directoryPartyRecordId = directoryPartyRecordId;
    }

    public String getPartyNumber() {
        return partyNumber;
    }

    public void setPartyNumber(String partyNumber) {
        this.partyNumber = partyNumber;
    }

    public Long getCustomerTypeValue() {
        return customerTypeValue;
    }

    public void setCustomerTypeValue(Long customerTypeValue) {
        this.customerTypeValue = customerTypeValue;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getcNPJCPFNumber() {
        return cNPJCPFNumber;
    }

    public void setcNPJCPFNumber(String cNPJCPFNumber) {
        this.cNPJCPFNumber = cNPJCPFNumber;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getInvoiceAccount() {
        return invoiceAccount;
    }

    public void setInvoiceAccount(String invoiceAccount) {
        this.invoiceAccount = invoiceAccount;
    }

    public Boolean getMandatoryCreditLimit() {
        return mandatoryCreditLimit;
    }

    public void setMandatoryCreditLimit(Boolean mandatoryCreditLimit) {
        this.mandatoryCreditLimit = mandatoryCreditLimit;
    }

    public String getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Boolean getUseOrderNumberReference() {
        return useOrderNumberReference;
    }

    public void setUseOrderNumberReference(Boolean useOrderNumberReference) {
        this.useOrderNumberReference = useOrderNumberReference;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Boolean getUsePurchaseRequest() {
        return usePurchaseRequest;
    }

    public void setUsePurchaseRequest(Boolean usePurchaseRequest) {
        this.usePurchaseRequest = usePurchaseRequest;
    }

    public String getMultilineDiscountGroup() {
        return multilineDiscountGroup;
    }

    public void setMultilineDiscountGroup(String multilineDiscountGroup) {
        this.multilineDiscountGroup = multilineDiscountGroup;
    }

    public String getTotalDiscountGroup() {
        return totalDiscountGroup;
    }

    public void setTotalDiscountGroup(String totalDiscountGroup) {
        this.totalDiscountGroup = totalDiscountGroup;
    }

    public String getLineDiscountGroup() {
        return lineDiscountGroup;
    }

    public void setLineDiscountGroup(String lineDiscountGroup) {
        this.lineDiscountGroup = lineDiscountGroup;
    }

    public String getTaxGroup() {
        return taxGroup;
    }

    public void setTaxGroup(String taxGroup) {
        this.taxGroup = taxGroup;
    }

    public String getSalesTaxGroup() {
        return salesTaxGroup;
    }

    public void setSalesTaxGroup(String salesTaxGroup) {
        this.salesTaxGroup = salesTaxGroup;
    }

    public String getTaxExemptNumber() {
        return taxExemptNumber;
    }

    public void setTaxExemptNumber(String taxExemptNumber) {
        this.taxExemptNumber = taxExemptNumber;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public Boolean getNonChargeableAccount() {
        return nonChargeableAccount;
    }

    public void setNonChargeableAccount(Boolean nonChargeableAccount) {
        this.nonChargeableAccount = nonChargeableAccount;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getReceiptSettings() {
        return receiptSettings;
    }

    public void setReceiptSettings(Long receiptSettings) {
        this.receiptSettings = receiptSettings;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public Long getRetailCustomerTableRecordId() {
        return retailCustomerTableRecordId;
    }

    public void setRetailCustomerTableRecordId(Long retailCustomerTableRecordId) {
        this.retailCustomerTableRecordId = retailCustomerTableRecordId;
    }

    public String getOfflineImage() {
        return offlineImage;
    }

    public void setOfflineImage(String offlineImage) {
        this.offlineImage = offlineImage;
    }

    public String getNewCustomerPartyNumber() {
        return newCustomerPartyNumber;
    }

    public void setNewCustomerPartyNumber(String newCustomerPartyNumber) {
        this.newCustomerPartyNumber = newCustomerPartyNumber;
    }

    public List<AxStarAffiliation> getCustomerAffiliations() {
        return customerAffiliations;
    }

    public void setCustomerAffiliations(List<AxStarAffiliation> customerAffiliations) {
        this.customerAffiliations = customerAffiliations;
    }

    public List<AxStarAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AxStarAddress> addresses) {
        this.addresses = addresses;
    }

    public AxStarRichMediaLocations getImage() {
        return image;
    }

    public void setImage(AxStarRichMediaLocations image) {
        this.image = image;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
