package com.enovax.star.cms.commons.model.kiosk.odata;

import java.io.Serializable;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.DeviceActivationResult
 * 
 * @author Justin
 * @since 6 SEP 16
 */

public class AxDeviceActivationResult implements Serializable {

	private AxDevice device;

	public AxDevice getDevice() {
		return device;
	}

	public void setDevice(AxDevice device) {
		this.device = device;
	}

}
