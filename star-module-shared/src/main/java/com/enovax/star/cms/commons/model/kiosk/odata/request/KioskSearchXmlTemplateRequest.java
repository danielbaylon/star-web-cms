package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AXTemplateXMLEntityRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskSearchXmlTemplateRequest {

	@JsonProperty("TemplateXMLCriteria")
	private AXTemplateXMLEntityRequest templateXMLCriteria = new AXTemplateXMLEntityRequest();

	public AXTemplateXMLEntityRequest getTemplateXMLCriteria() {
		return templateXMLCriteria;
	}

	public void setTemplateXMLCriteria(AXTemplateXMLEntityRequest templateXMLCriteria) {
		this.templateXMLCriteria = templateXMLCriteria;
	}

}
