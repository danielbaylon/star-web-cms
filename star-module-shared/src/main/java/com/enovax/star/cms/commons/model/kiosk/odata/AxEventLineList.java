package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventLineList
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventLineList {

    /**
     * Collection(SDC_NonBindableCRTExtension.Entity.EventLines)
     * 
     */
    @JsonProperty("EventLines")
    private List<AxEventLine> axEventLines = new ArrayList<>();

    public List<AxEventLine> getAxEventLines() {
        return axEventLines;
    }

    public void setAxEventLines(List<AxEventLine> axEventLines) {
        this.axEventLines = axEventLines;
    }

}
