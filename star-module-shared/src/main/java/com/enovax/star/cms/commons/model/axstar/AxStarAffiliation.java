package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

/**
 * Created by jensen on 27/6/16.
 */
public class AxStarAffiliation {

    private Long recordId;
    private Integer affiliationTypeValue;
    private String description;
    private String name;

    private List<AxStarExtensionProperty> extensionProperties;
    private List<AxStarPriceGroup> priceGroups;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Integer getAffiliationTypeValue() {
        return affiliationTypeValue;
    }

    public void setAffiliationTypeValue(Integer affiliationTypeValue) {
        this.affiliationTypeValue = affiliationTypeValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AxStarPriceGroup> getPriceGroups() {
        return priceGroups;
    }

    public void setPriceGroups(List<AxStarPriceGroup> priceGroups) {
        this.priceGroups = priceGroups;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
