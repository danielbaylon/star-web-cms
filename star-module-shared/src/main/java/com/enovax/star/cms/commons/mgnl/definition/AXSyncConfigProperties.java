package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 10/31/16.
 */
public enum AXSyncConfigProperties {

    SyncOption("syncOption");

    private String propertyName;

    AXSyncConfigProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
