
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_CustPenaltyChargeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CustPenaltyChargeResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="custPenaltyChargetTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_CustPenaltyChargeTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CustPenaltyChargeResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "custPenaltyChargetTable"
})
public class SDCCustPenaltyChargeResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "custPenaltyChargetTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCustPenaltyChargeTable> custPenaltyChargetTable;

    /**
     * Gets the value of the custPenaltyChargetTable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustPenaltyChargeTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCustPenaltyChargeTable> getCustPenaltyChargetTable() {
        return custPenaltyChargetTable;
    }

    /**
     * Sets the value of the custPenaltyChargetTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustPenaltyChargeTable }{@code >}
     *     
     */
    public void setCustPenaltyChargetTable(JAXBElement<ArrayOfSDCCustPenaltyChargeTable> value) {
        this.custPenaltyChargetTable = value;
    }

}
