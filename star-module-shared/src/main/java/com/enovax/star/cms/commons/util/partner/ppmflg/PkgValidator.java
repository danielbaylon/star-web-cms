package com.enovax.star.cms.commons.util.partner.ppmflg;

import com.enovax.star.cms.commons.constant.api.ApiErrorCodes;
import com.enovax.star.cms.commons.constant.ppmflg.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackageItem;
import com.enovax.star.cms.commons.mgnl.definition.CMSProductProperties;
import com.enovax.star.cms.commons.model.partner.ppmflg.ResultVM;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;

/**
 * Created by jennylynsze on 5/18/16.
 */
public class PkgValidator {

    private static Logger log = LoggerFactory.getLogger(PkgValidator.class);

    public final static String Break = "\n";
    public static final Integer MIN_QTY_ALLOWED = 1;
    public static final Integer MAX_QTY_ALLOWED = 100;

    public static ResultVM verifyPackageItems(
            List<PPMFLGInventoryTransactionItem> transItems, Map<String, Node> productMap) {

        ResultVM finalVm = new ResultVM();
        finalVm.setStatus(true);

        log.info("Check is standalone mix with others......");
        ResultVM resultVm1 = checkStandaloneMixwithOtherItem(transItems, productMap);
        if (!resultVm1.isStatus()) {
            finalVm.setStatus(false);
            finalVm.setMessage(resultVm1.getMessage() + Break);
        }

        log.info("Check is event mix with others......");
        ResultVM resultVm2 = checkEventMixedWithOther(transItems, productMap);
        if (!resultVm2.isStatus()) {
            finalVm.setStatus(false);
            finalVm.setMessage((finalVm.getMessage() == null ? "" : finalVm
                    .getMessage()) + resultVm2.getMessage() + Break);
        }

        log.info("Check is event same event");
        if (checkIfAllEventItem(transItems)) {
            boolean result3 = checkIfAllEventSame(transItems);
            if (!result3) {
                finalVm.setStatus(false);
                finalVm.setMessage((finalVm.getMessage() == null ? "" : finalVm
                        .getMessage()) + ApiErrorCodes.PkgDifferentEventSchedule.message
                        + Break);
            }
        }

        return finalVm;
    }

    public static ResultVM verifyNewAddedItem(
            PPMFLGInventoryTransactionItem itemToAdd,
            List<PPMFLGInventoryTransactionItem> transItems, String pkgTktType,
            List<PPMFLGMixMatchPackageItem> pkgItemWithSameItem, String pkgTktMediaType,
            List<ProductExtViewModel> productExtViewModelList,
            Map<String, Node> productMap) {
        ResultVM resultVm = new ResultVM();
        resultVm.setStatus(true);
        // Check if ticket type as user defined type.
        if (itemToAdd != null && pkgTktType != null) {
            Boolean res = PkgUtil.verifyItemWithPackageType(itemToAdd,
                    pkgTktType);
            if (!res) {
                resultVm.setMessage(resultVm.getMessage() == null ? ""
                        : ApiErrorCodes.PkgCreateErrTypeMismatch.message
                        + Break);
                resultVm.setStatus(false);
            }
        }
        // check if item reserved.
        if (!checkItemNotReserved(pkgItemWithSameItem)) {
            resultVm.setMessage((resultVm.getMessage() == null ? "" : resultVm
                    .getMessage())
                    //TODO see message.properties
                    //pkg.create.err.item.reserved
                    //+ textProvider.getText(PkgValidator.MSG_ItemReserved)
                    + ApiErrorCodes.PkgCreateErrItemReserved.message
                    + Break);
            resultVm.setStatus(false);
        }
        // check if same item have different topup
        if (itemToAdd != null) {
            boolean result = checkItemHasDiffTopup(itemToAdd, transItems);
            if (!result) {
                resultVm.setMessage((resultVm.getMessage() == null ? ""
                        : resultVm.getMessage()+ Break)
                        + "pkg.create.err.different.topup");
                         //TODO see message.properties
                       // + textProvider.getText(PkgValidator.MSG_SameItemWithDiffTopup));
                resultVm.setStatus(false);

            }
        }

        //if the user selected a ticket media and the ticket media is not a pincode, need to check if possible to generate for eticket
//        if(StringUtils.isNotEmpty(pkgTktMediaType) && !TicketMediaType.Pincode.name().equals(pkgTktMediaType)) {
//            String tktMediaTypeAllowed = PkgUtil.getTicketMediaTypeAllowed(productExtViewModelList);
//
//            if(StringUtils.isNotEmpty(tktMediaTypeAllowed) && !tktMediaTypeAllowed.equals(pkgTktMediaType)) {
//                resultVm.setMessage((resultVm.getMessage() == null ? ""
//                        : resultVm.getMessage()+ Break)
//                        + "The ticket Media should be Pincode, please change the ticket Media to Pincode");
//                //TODO see message.properties
//                // + textProvider.getText(PkgValidator.MSG_SameItemWithDiffTopup));
//                resultVm.setStatus(false);
//            }
//        }

        // overall check
        ResultVM overallRes = verifyPackageItems(transItems, productMap);
        if (!overallRes.isStatus()) {
            resultVm.setMessage((resultVm.getMessage() == null ? "" : resultVm
                    .getMessage()) + overallRes.getMessage());
            resultVm.setStatus(false);
        }
        return resultVm;
    }



    public static ResultVM checkIfItemNeedRemoveDueToQty(
            List<PPMFLGInventoryTransactionItem> transItems, Integer ipkgQtyPerProd, String ipkgTktType) {
        ResultVM resultVm = new ResultVM();
        if (ipkgQtyPerProd == null) {
            return resultVm;
        }
        List<PPMFLGInventoryTransactionItem> finalList = PkgUtil
                .getAvailableListUnderPkg(transItems, ipkgQtyPerProd,
                        ipkgTktType);
        List<PPMFLGInventoryTransactionItem> templList = PkgUtil
                .getAvailableListUnderPkg(transItems, ipkgQtyPerProd,
                        ipkgTktType, false);

        if (finalList.size() < templList.size()) {
            resultVm.setStatus(false);
            resultVm.setMessage(ApiErrorCodes.PkgValidateInsufficientQty.message);
        }
        return resultVm;
    }

//    public static ResultVM checkItemsMaxAllowed(
//            List<InventoryTransactionItem> transItems, Integer maxItemCodeNum,
//            Integer maxMSitesNum, Integer maxSSitesNum) {
//        ResultVM resultVm = new ResultVM();
//        int singleSiteNum = 0;
//        int muiltipleSiteNum = 0;
//        Set<Integer> itemCodes = new TreeSet<Integer>();
//        for (InventoryTransactionItem item : transItems) {
//            if (ItemType.S.toString().equals(item.getItemType())) {
//                singleSiteNum++;
//            }
//            if (ItemType.M.toString().equals(item.getItemType())) {
//                muiltipleSiteNum++;
//            }
//            itemCodes.add(item.getItemCode());
//            for (InventoryTransactionItem topup : item.getTopupItems()) {
//                //For top up will be reused, item code will be same
//                itemCodes.add(Integer.parseInt(topup.getItemCode()+""+topup.getItemId()));
//            }
//        }
//        if (maxItemCodeNum != null) {
//            if (itemCodes.size() > maxItemCodeNum) {
//                resultVm.setStatus(false);
//                resultVm.setMessage(textProvider.getText(PkgValidator.MSG_ExceedMaxSits,new String[]{String.valueOf(maxItemCodeNum)}));
//                return resultVm;
//            }
//        }
//        if (maxMSitesNum != null && muiltipleSiteNum > maxMSitesNum) {
//            resultVm.setStatus(false);
//            resultVm.setMessage(textProvider.getText(PkgValidator.MSG_ExceedMaxMuiltpleSits));
//            return resultVm;
//        }
//        if (maxSSitesNum != null && singleSiteNum > maxSSitesNum) {
//            resultVm.setStatus(false);
//            resultVm.setMessage(textProvider.getText(PkgValidator.MSG_ExceedMaxSingleSits));
//            return resultVm;
//        }
//        log.debug("singleSiteNum: "+singleSiteNum+"--------maxSSitesNum "+maxSSitesNum);
//        log.debug("muiltipleSiteNum: "+muiltipleSiteNum+"--------maxMSitesNum "+maxMSitesNum);
//        return resultVm;
//    }


    private static boolean checkItemHasDiffTopup(
            PPMFLGInventoryTransactionItem itemToAdd,
            List<PPMFLGInventoryTransactionItem> transItems) {
        List<PPMFLGInventoryTransactionItem> topup = itemToAdd.getTopupItems();
        for (PPMFLGInventoryTransactionItem temp : transItems) {
            if (!checkIsTopupSame(topup, temp.getTopupItems())
                    && itemToAdd.getItemProductCode().equals(temp.getItemProductCode())) { //todo check
                return false;
            }
        }
        return true;
    }

    private static boolean checkIsTopupSame(
            List<PPMFLGInventoryTransactionItem> topup1,
            List<PPMFLGInventoryTransactionItem> topup2) {
        if (topup1 == null && topup2 == null) {
            return true;
        }
        if (topup1 == null)
            topup1 = new ArrayList<>();
        if (topup2 == null)
            topup2 = new ArrayList<>();
        if (topup1.size() != topup2.size()) {
            return false;
        }
        Set<String> topup1ItemCodes = PkgUtil.getAllItemCode(topup1);
        Set<String> topup2ItemCodes = PkgUtil.getAllItemCode(topup2);
        for (String tempCode : topup1ItemCodes) {
            if (!topup2ItemCodes.contains(tempCode)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkItemNotReserved(
            List<PPMFLGMixMatchPackageItem> pkgItemWithSameItem) {
        for (PPMFLGMixMatchPackageItem item : pkgItemWithSameItem) {
            String tempStatus = item.getMmPkg().getStatus();
            if (TicketStatus.Reserved.toString().equals(tempStatus)) {
                return false;
            }
        }
        return true;
    }


    //TODO cannot mix and match with other product
    private static ResultVM checkStandaloneMixwithOtherItem(
            List<PPMFLGInventoryTransactionItem> transItems, Map<String, Node> productMap) {
        ResultVM result = new ResultVM();

        boolean pass = true;
        boolean havingStandaloneItem = false;
        boolean havingNonStandaloneItem = false;
        String preStandaloneProductId = null;
        String preStandaloneItemCode = null;

        for (PPMFLGInventoryTransactionItem item : transItems) {
            Node cmsProductNode = productMap.get(item.getProductId());
            boolean isStandalone = PropertyUtil.getBoolean(cmsProductNode, CMSProductProperties.StandaloneItem.getPropertyName(), false);

            if(isStandalone){
                havingStandaloneItem = true;
                if(havingNonStandaloneItem){
                    pass = false;
                    break;
                }
                if(preStandaloneProductId == null){
                    try {
                        preStandaloneProductId = cmsProductNode.getName();
                        preStandaloneItemCode = item.getItemProductCode();
                    } catch (RepositoryException e) {
                        log.error(e.getMessage(), e);
                    }
                }else{
                    try {
                        String productId = cmsProductNode.getName();

                        if(!preStandaloneProductId.equals(productId)){
                            pass = false;
                            break;
                        }else if(!preStandaloneItemCode.equals(item.getItemProductCode())) {
                            pass = false;
                            break;
                        }

                    } catch (RepositoryException e) {
                        log.error(e.getMessage(), e);
                    }

                }
            }else{
                havingNonStandaloneItem = true;
                if(havingStandaloneItem){
                    pass = false;
                    break;
                }
            }
        }

        result.setStatus(pass);

        if (!pass) {
            result.setMessage(ApiErrorCodes.PkgCreateErrMixStandalone.message);
        }
        return result;
    }

    private static ResultVM checkEventMixedWithOther(
            List<PPMFLGInventoryTransactionItem> transItems, Map<String, Node> productMap) {
        int genTkt = 0;
        int eventTkt = 0;
        int standaloneTkt = 0;
        ResultVM result = new ResultVM();
        for (PPMFLGInventoryTransactionItem item : transItems) {
            Node cmsProductNode = productMap.get(item.getProductId());
            boolean isStandalone = PropertyUtil.getBoolean(cmsProductNode, CMSProductProperties.StandaloneItem.getPropertyName(), false);

            if(StringUtils.isNotEmpty(item.getEventGroupId())) {
                eventTkt++;
            }else {
                if(StringUtils.isEmpty(item.getEventGroupId())) {
                    genTkt++;
                }

                if(isStandalone) {
                    standaloneTkt++;
                }
            }
        }

        if (eventTkt > 0 && (genTkt > 0 || standaloneTkt > 0)) {
            result.setStatus(false);
            result.setMessage(ApiErrorCodes.PkgValidateEventItemCantMix.message);
        } else {
            result.setStatus(true);
        }
        return result;
    }

    private static boolean checkIfAllEventItem(
            List<PPMFLGInventoryTransactionItem> transItems) {
        int eventTkt = 0;
        for (PPMFLGInventoryTransactionItem item : transItems) {
            if (StringUtils.isNotEmpty(item.getEventGroupId())) {
                eventTkt++;
            }
        }
        if (eventTkt == transItems.size()) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean checkIfAllEventSame(
            List<PPMFLGInventoryTransactionItem> transItems) {

        String eventGroupId = null;
        String eventLineId = null;
        Date dateOfVisit = null;
        boolean res = true;
        for (PPMFLGInventoryTransactionItem item : transItems) {

            if(eventGroupId == null) {
                eventGroupId = item.getEventGroupId();
            }
            if(eventLineId == null) {
                eventLineId = item.getEventLineId();
            }

            if(dateOfVisit == null) {
                dateOfVisit = item.getDateOfVisit();
            }

            if(eventGroupId != null && !eventGroupId.equals(item.getEventGroupId())) {
                return false;
            }

            if(eventLineId != null && !eventLineId.equals(item.getEventLineId())) {
                return false;
            }

            if(dateOfVisit != null && item.getDateOfVisit() != null && dateOfVisit.getTime() != item.getDateOfVisit().getTime()) {
                return false;
            }
        }
        return res;
    }

    public static ResultVM checkIfAnyItemReserved(List<PPMFLGMixMatchPackageItem> pkgitems, String pkgName){
        ResultVM result = new ResultVM();
        for(PPMFLGMixMatchPackageItem item:pkgitems){
            String tempStatus = item.getMmPkg().getStatus();
            if (TicketStatus.Reserved.toString().equals(tempStatus)&&
                    !pkgName.equals(item.getMmPkg().getName())) {
                result.setStatus(false);
                result.setMessage(ApiErrorCodes.PkgValidateCreateErrItemsReserved.message);
            }
        }
        return result;
    }

    public static ResultVM checkMinQtyAllowed(Integer inQty, Integer systemQty) {
        ResultVM result = new ResultVM();
        if (systemQty != null && inQty != null && systemQty > inQty) {
            result.setStatus(false);
            result.setMessage(ApiErrorCodes.PkgValidateQtyProductMin.message.replace("{0}", systemQty + ""));
        }
        return result;
    }

    public static ResultVM checkMaxQtyAllowed(Integer inQty, Integer systemQty) {
        ResultVM result = new ResultVM();
        if (systemQty != null && inQty != null && systemQty < inQty) {
            result.setStatus(false);
            result.setMessage(ApiErrorCodes.PkgValidateQtyProductMax.message.replace("{0}", systemQty + ""));
        }
        return result;
    }
}
