package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGAuditTrail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lavanya on 5/9/16.
 */
@Repository
public interface PPMFLGAuditTrailRepository extends JpaRepository<PPMFLGAuditTrail, Integer> {
}
