package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

/**
 * 
 * SDC_NonBindableCRTExtension.Entity.EventGroupTableEntity
 * 
 * @author Justin
 *
 */
public class AxEventGroupTableEntity {

    private String capacityCalendarId;
    private String eventDate;
    private String eventGroupId;
    private String isSingleEvent;
    private String dataAreaID;
    private String recVersion;
    private String recID;

    private List<AxEventGroupLineEntity> eventGroupLines;
    private List<AxCommerceProperty> extensionProperties;

    public String getCapacityCalendarId() {
        return capacityCalendarId;
    }

    public void setCapacityCalendarId(String capacityCalendarId) {
        this.capacityCalendarId = capacityCalendarId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getIsSingleEvent() {
        return isSingleEvent;
    }

    public void setIsSingleEvent(String isSingleEvent) {
        this.isSingleEvent = isSingleEvent;
    }

    public Boolean isSingleEvent() {
        return "1".equals(this.isSingleEvent) ? Boolean.TRUE : Boolean.FALSE;
    }

    public String getDataAreaID() {
        return dataAreaID;
    }

    public void setDataAreaID(String dataAreaID) {
        this.dataAreaID = dataAreaID;
    }

    public String getRecVersion() {
        return recVersion;
    }

    public void setRecVersion(String recVersion) {
        this.recVersion = recVersion;
    }

    public String getRecID() {
        return recID;
    }

    public void setRecID(String recID) {
        this.recID = recID;
    }

    public List<AxEventGroupLineEntity> getEventGroupLines() {
        return eventGroupLines;
    }

    public void setEventGroupLines(List<AxEventGroupLineEntity> eventGroupLines) {
        this.eventGroupLines = eventGroupLines;
    }

    public List<AxCommerceProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxCommerceProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
