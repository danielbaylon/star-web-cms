package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by houtao on 3/8/16.
 */
public interface MessageConst {

    String renew_pwd_InvalidOrignialPassword = "InvalidOrignialPassword";
    String renew_pwd_AllFieldsRequired = "AllFieldsRequired";
    String renew_pwd_Last5Passwords = "Last5Passwords";
    String renew_pwd_RenewPassNotMatch = "RenewPassNotMatch";
    String renew_pwd_PasswordPolicyNotMet = "PasswordPolicyNotMet";
    String renew_pwd_UsernameInvalid = "UsernameInvalid";
    String renew_pwd_PasswordLeadingTrailing = "PasswordLeadingTrailing";
    String login_err_LoginFailed = "login.err.LoginFailed";
    String login_err_LoginSuspended = "login.err.LoginSuspended";
    String login_err_AccountInactive = "login.err.AccountInactive";
    String login_err_AccountLocked = "login.err.AccountLocked";
    String login_err_ExceedPasswordAttempts = "login.err.ExceedPasswordAttempts";
    String login_err_ExceedConcurrentLogin = "login.err.ExceedConcurrentLogin";
}
