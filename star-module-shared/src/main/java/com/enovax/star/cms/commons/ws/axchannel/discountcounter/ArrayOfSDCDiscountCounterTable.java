
package com.enovax.star.cms.commons.ws.axchannel.discountcounter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_DiscountCounterTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_DiscountCounterTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_DiscountCounterTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_DiscountCounterTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_DiscountCounterTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcDiscountCounterTable"
})
public class ArrayOfSDCDiscountCounterTable {

    @XmlElement(name = "SDC_DiscountCounterTable", nillable = true)
    protected List<SDCDiscountCounterTable> sdcDiscountCounterTable;

    /**
     * Gets the value of the sdcDiscountCounterTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcDiscountCounterTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCDiscountCounterTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCDiscountCounterTable }
     * 
     * 
     */
    public List<SDCDiscountCounterTable> getSDCDiscountCounterTable() {
        if (sdcDiscountCounterTable == null) {
            sdcDiscountCounterTable = new ArrayList<SDCDiscountCounterTable>();
        }
        return this.sdcDiscountCounterTable;
    }

}
