
package com.enovax.star.cms.commons.ws.axchannel.penaltycharge;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustPenaltyChargeResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_CustPenaltyChargeResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustPenaltyChargeResult"
})
@XmlRootElement(name = "GetCustPenaltyChargeResponse", namespace = "http://tempuri.org/")
public class GetCustPenaltyChargeResponse {

    @XmlElementRef(name = "GetCustPenaltyChargeResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCCustPenaltyChargeResponse> getCustPenaltyChargeResult;

    /**
     * Gets the value of the getCustPenaltyChargeResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCCustPenaltyChargeResponse }{@code >}
     *     
     */
    public JAXBElement<SDCCustPenaltyChargeResponse> getGetCustPenaltyChargeResult() {
        return getCustPenaltyChargeResult;
    }

    /**
     * Sets the value of the getCustPenaltyChargeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCCustPenaltyChargeResponse }{@code >}
     *     
     */
    public void setGetCustPenaltyChargeResult(JAXBElement<SDCCustPenaltyChargeResponse> value) {
        this.getCustPenaltyChargeResult = value;
    }

}
