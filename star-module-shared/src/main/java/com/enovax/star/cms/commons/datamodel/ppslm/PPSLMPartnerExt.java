package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = PPSLMPartnerExt.TABLE_NAME)
public class PPSLMPartnerExt implements Serializable {

    public static final String TABLE_NAME = "PPSLMPartnerExt";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "partnerId", nullable = false)
    private Integer partnerId;

    @Column(name = "approvalRequired")
    private Boolean approvalRequired;

    @Column(name = "attrName")
    private String attrName;

    @Column(name = "attrValue")
    private String attrValue;

    @Column(name = "attrValueType")
    private String attrValueType;

    @Column(name = "attrValueFormat")
    private String attrValueFormat;

    @Column(name = "mandatoryInd")
    private Boolean mandatoryInd;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "partnerId",nullable = false, insertable = false, updatable = false)
    private PPSLMPartner partner;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getApprovalRequired() {
        return approvalRequired;
    }

    public void setApprovalRequired(Boolean approvalRequired) {
        this.approvalRequired = approvalRequired;
    }

    public String getAttrValueType() {
        return attrValueType;
    }

    public void setAttrValueType(String attrValueType) {
        this.attrValueType = attrValueType;
    }

    public String getAttrValueFormat() {
        return attrValueFormat;
    }

    public void setAttrValueFormat(String attrValueFormat) {
        this.attrValueFormat = attrValueFormat;
    }

    public Boolean getMandatoryInd() {
        return mandatoryInd;
    }

    public void setMandatoryInd(Boolean mandatoryInd) {
        this.mandatoryInd = mandatoryInd;
    }

    public PPSLMPartner getPartner() {
        return partner;
    }

    public void setPartner(PPSLMPartner partner) {
        this.partner = partner;
    }
}
