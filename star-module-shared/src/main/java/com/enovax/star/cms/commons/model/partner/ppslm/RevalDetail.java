package com.enovax.star.cms.commons.model.partner.ppslm;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@XmlRootElement(name = "revalDetail")
public class RevalDetail {
    List<RevalItem> revalItems;

    public List<RevalItem> getRevalItems() {
        return revalItems;
    }

    public void setRevalItems(List<RevalItem> revalItems) {
        this.revalItems = revalItems;
    }
}

