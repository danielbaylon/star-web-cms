
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfEventGroupTableEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEventGroupTableEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventGroupTableEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}EventGroupTableEntity" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEventGroupTableEntity", propOrder = {
    "eventGroupTableEntity"
})
public class ArrayOfEventGroupTableEntity {

    @XmlElement(name = "EventGroupTableEntity", nillable = true)
    protected List<EventGroupTableEntity> eventGroupTableEntity;

    /**
     * Gets the value of the eventGroupTableEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventGroupTableEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventGroupTableEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventGroupTableEntity }
     * 
     * 
     */
    public List<EventGroupTableEntity> getEventGroupTableEntity() {
        if (eventGroupTableEntity == null) {
            eventGroupTableEntity = new ArrayList<EventGroupTableEntity>();
        }
        return this.eventGroupTableEntity;
    }

}
