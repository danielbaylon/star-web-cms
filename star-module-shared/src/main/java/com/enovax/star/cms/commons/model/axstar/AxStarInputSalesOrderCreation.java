package com.enovax.star.cms.commons.model.axstar;

import java.util.List;
import java.util.Map;

public class AxStarInputSalesOrderCreation {

    private String salesOrderNumber;
    private String email;
    private String cartId;
    private String customerId;
    private Map<String, String> stringExtensionProperties;
    private String deliveryOption;

    private List<AxStarInputTenderDataLine> tenderDataLines;

    public String getSalesOrderNumber() {
        return salesOrderNumber;
    }

    public void setSalesOrderNumber(String salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<AxStarInputTenderDataLine> getTenderDataLines() {
        return tenderDataLines;
    }

    public void setTenderDataLines(List<AxStarInputTenderDataLine> tenderDataLines) {
        this.tenderDataLines = tenderDataLines;
    }

    public String getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(String deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public Map<String, String> getStringExtensionProperties() {
        return stringExtensionProperties;
    }

    public void setStringExtensionProperties(Map<String, String> stringExtensionProperties) {
        this.stringExtensionProperties = stringExtensionProperties;
    }
}
