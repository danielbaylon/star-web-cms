package com.enovax.star.cms.commons.model.ticketgen;

import java.util.List;

public class ETicketDataCompiled {

    private List<ETicketData> tickets;

    public List<ETicketData> getTickets() {
        return tickets;
    }

    public void setTickets(List<ETicketData> tickets) {
        this.tickets = tickets;
    }
}
