
package com.enovax.star.cms.commons.ws.axchannel.b2bpinupdate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetB2BPINUpdateResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINUpdateResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getB2BPINUpdateResult"
})
@XmlRootElement(name = "GetB2BPINUpdateResponse", namespace = "http://tempuri.org/")
public class GetB2BPINUpdateResponse {

    @XmlElementRef(name = "GetB2BPINUpdateResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINUpdateResponse> getB2BPINUpdateResult;

    /**
     * Gets the value of the getB2BPINUpdateResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINUpdateResponse> getGetB2BPINUpdateResult() {
        return getB2BPINUpdateResult;
    }

    /**
     * Sets the value of the getB2BPINUpdateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}
     *     
     */
    public void setGetB2BPINUpdateResult(JAXBElement<SDCB2BPINUpdateResponse> value) {
        this.getB2BPINUpdateResult = value;
    }

}
