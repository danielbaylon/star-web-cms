package com.enovax.star.cms.commons.constant.ppmflg;

public enum PPMFLGPartnerExtValueType {

    TRUE("true"),
    FALSE("false");

    public String code;

    private PPMFLGPartnerExtValueType(String code) {
        this.code = code;
    }
}
