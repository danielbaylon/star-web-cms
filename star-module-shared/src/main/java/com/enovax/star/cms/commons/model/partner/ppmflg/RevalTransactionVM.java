package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 10/11/16.
 */
public class RevalTransactionVM implements Serializable {

    private Integer id;
    private String receiptNum;
    private Date tmStatusDate;
    private String tmStatus;
    private List<RevalItem> revalItems;

    public RevalTransactionVM() {}

    public RevalTransactionVM(PPMFLGRevalidationTransaction trans) {
        this.id = trans.getId();
        this.receiptNum = trans.getReceiptNum();
        this.tmStatus = trans.getTmStatus();
        this.tmStatusDate = trans.getTmStatusDate();
        //this.revalItems = trans.getRevalItems();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Date getTmStatusDate() {
        return tmStatusDate;
    }

    public void setTmStatusDate(Date tmStatusDate) {
        this.tmStatusDate = tmStatusDate;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public List<RevalItem> getRevalItems() {
        return revalItems;
    }

    public void setRevalItems(List<RevalItem> revalItems) {
        this.revalItems = revalItems;
    }
}
