package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 17/8/16.
 */
@Repository
public interface PPMFLGDepositTransactionRepository extends JpaRepository<PPMFLGDepositTransaction, Integer>, JpaSpecificationExecutor<PPMFLGDepositTransaction> {

    List<PPMFLGDepositTransaction> findById(Integer id);

    PPMFLGDepositTransaction findByReceiptNum(String receiptNum);

    @Query("select t from PPMFLGDepositTransaction t where t.receiptNum = :receiptNum and t.sessionId = :sessionId and t.transType = :transType ")
    List<PPMFLGDepositTransaction> findTxnByReceiptAndSessionIdAndTransType(@Param("receiptNum") String receiptNum, @Param("sessionId") String sessionId, @Param("transType") String transType);

    @Query("select t from PPMFLGDepositTransaction t where t.tmRedirectDate is not null and t.status = 'Reserved' and t.tmStatus = 'RedirectedToTm' and t.tmApprovalCode is null and t.tmResponseDate is null and t.tmQuerySentDate is null and t.tmQuerySentStatus is null and t.tmRedirectDate >= ?1 and t.tmRedirectDate <= ?2 ")
    List<PPMFLGDepositTransaction> getPendingPaymentQueryList(Date lowerBoundDate, Date upperBoundDate);
}
