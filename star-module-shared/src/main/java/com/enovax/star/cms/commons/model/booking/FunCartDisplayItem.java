package com.enovax.star.cms.commons.model.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FunCartDisplayItem {

    private String cartItemId;
    private String listingId;
    private String productCode;
    private String type;

    private String eventGroupId;
    private String eventLineId;
    private String selectedEventDate = ""; // dd/MM/yyyy
    private String eventSessionName;

    private String name;
    private String productDescription;

    private String description;
    private Integer qty;

    private BigDecimal price;
    private String priceText;

    private BigDecimal total;
    private String totalText;

    private BigDecimal discountTotal;
    private String discountTotalText;
    private String discountLabel;

    private String parentListingId;
    private String parentCmsProductId;

    private boolean isTopup = false;

    private boolean openDate = Boolean.FALSE;

    private boolean isCapacity = Boolean.FALSE;

    // only items have been added to ax woul have a line id.
    private String lineId = "";

    private List<String> promoLabels = new ArrayList<>();
    private List<String> offerIds = new ArrayList<>();

    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getSelectedEventDate() {
        return selectedEventDate;
    }

    public void setSelectedEventDate(String selectedEventDate) {
        this.selectedEventDate = selectedEventDate;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTotalText() {
        return totalText;
    }

    public void setTotalText(String totalText) {
        this.totalText = totalText;
    }

    public boolean isTopup() {
        return isTopup;
    }

    public void setIsTopup(boolean isTopup) {
        this.isTopup = isTopup;
    }

    public BigDecimal getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(BigDecimal discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getDiscountTotalText() {
        return discountTotalText;
    }

    public void setDiscountTotalText(String discountTotalText) {
        this.discountTotalText = discountTotalText;
    }

    public String getDiscountLabel() {
        return discountLabel;
    }

    public void setDiscountLabel(String discountLabel) {
        this.discountLabel = discountLabel;
    }

    public String getEventSessionName() {
        return eventSessionName;
    }

    public void setEventSessionName(String eventSessionName) {
        this.eventSessionName = eventSessionName;
    }

    public String getParentListingId() {
        return parentListingId;
    }

    public void setParentListingId(String parentListingId) {
        this.parentListingId = parentListingId;
    }

    public List<String> getPromoLabels() {
        return promoLabels;
    }

    public void setPromoLabels(List<String> promoLabels) {
        this.promoLabels = promoLabels;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public boolean isOpenDate() {
        return openDate;
    }

    public void setOpenDate(boolean openDate) {
        this.openDate = openDate;
    }

    public boolean isCapacity() {
        return isCapacity;
    }

    public void setCapacity(boolean isCapacity) {
        this.isCapacity = isCapacity;
    }

    public String getParentCmsProductId() {
        return parentCmsProductId;
    }

    public void setParentCmsProductId(String parentCmsProductId) {
        this.parentCmsProductId = parentCmsProductId;
    }

    public void setTopup(boolean topup) {
        isTopup = topup;
    }

    public List<String> getOfferIds() {
        return offerIds;
    }

    public void setOfferIds(List<String> offerIds) {
        this.offerIds = offerIds;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
