package com.enovax.star.cms.commons.model.product.crosssell;

import java.util.List;
import java.util.Map;

/**
 * Created by jensen on 25/6/16.
 */
public class CrossSellLinkMainProduct {

    private String itemId;
    private Integer minimumQty;
    private Integer multipleQty;
    private String unitId;

    private List<CrossSellLinkRelatedProduct> relatedProducts;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getMinimumQty() {
        return minimumQty;
    }

    public void setMinimumQty(Integer minimumQty) {
        this.minimumQty = minimumQty;
    }

    public Integer getMultipleQty() {
        return multipleQty;
    }

    public void setMultipleQty(Integer multipleQty) {
        this.multipleQty = multipleQty;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public List<CrossSellLinkRelatedProduct> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(List<CrossSellLinkRelatedProduct> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }
}
