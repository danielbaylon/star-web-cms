package com.enovax.star.cms.commons.mgnl.form.field;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

/**
 * Created by jennylynsze on 5/5/16.
 */
public class EditButton extends CustomField<Object> {
    private Button button;

    public EditButton() {
        this.button = new Button();
    }

    public Button getButton() {
        return this.button;
    }

    @Override
    protected Component initContent() {
        return this.button;
    }

    @Override
    public Class<?> getType() {
        return this.getPropertyDataSource().getType();
    }
}
