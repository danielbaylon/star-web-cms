package com.enovax.star.cms.commons.model.axchannel.pin;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by houtao on 23/8/16.
 */
public class AxSalesItem {

    protected String color;
    protected String comment;
    protected String configuration;
    protected String deliveryModeDescription;
    protected String deliveryModeId;
    protected String description;
    protected BigDecimal discountAmount;
    protected String discountAmountWithCurrency;
    protected String electronicDeliveryEmail;
    protected AxImageInfo image;
    protected String imageData;
    protected String itemId;
    protected AxTransactionItemType itemType;
    protected List<AxSalesItem> kitComponents;
    protected String lineId;
    protected String netAmountWithCurrency;
    protected String offerNames;
    protected String priceWithCurrency;
    protected String productDetails;
    protected Long productId;
    protected String productName;
    protected String productNumber;
    protected String productUrl;
    protected List<String> promotionLines;
    protected Integer quantity;
    protected AxSalesShippingAddress shippingAddress;
    protected String size;
    protected String style;
    protected String taxAmountWithCurrency;
    protected String variantInventoryDimensionId;


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getDeliveryModeDescription() {
        return deliveryModeDescription;
    }

    public void setDeliveryModeDescription(String deliveryModeDescription) {
        this.deliveryModeDescription = deliveryModeDescription;
    }

    public String getDeliveryModeId() {
        return deliveryModeId;
    }

    public void setDeliveryModeId(String deliveryModeId) {
        this.deliveryModeId = deliveryModeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmountWithCurrency() {
        return discountAmountWithCurrency;
    }

    public void setDiscountAmountWithCurrency(String discountAmountWithCurrency) {
        this.discountAmountWithCurrency = discountAmountWithCurrency;
    }

    public String getElectronicDeliveryEmail() {
        return electronicDeliveryEmail;
    }

    public void setElectronicDeliveryEmail(String electronicDeliveryEmail) {
        this.electronicDeliveryEmail = electronicDeliveryEmail;
    }

    public AxImageInfo getImage() {
        return image;
    }

    public void setImage(AxImageInfo image) {
        this.image = image;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public AxTransactionItemType getItemType() {
        return itemType;
    }

    public void setItemType(AxTransactionItemType itemType) {
        this.itemType = itemType;
    }

    public List<AxSalesItem> getKitComponents() {
        return kitComponents;
    }

    public void setKitComponents(List<AxSalesItem> kitComponents) {
        this.kitComponents = kitComponents;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getNetAmountWithCurrency() {
        return netAmountWithCurrency;
    }

    public void setNetAmountWithCurrency(String netAmountWithCurrency) {
        this.netAmountWithCurrency = netAmountWithCurrency;
    }

    public String getOfferNames() {
        return offerNames;
    }

    public void setOfferNames(String offerNames) {
        this.offerNames = offerNames;
    }

    public String getPriceWithCurrency() {
        return priceWithCurrency;
    }

    public void setPriceWithCurrency(String priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public List<String> getPromotionLines() {
        return promotionLines;
    }

    public void setPromotionLines(List<String> promotionLines) {
        this.promotionLines = promotionLines;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public AxSalesShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AxSalesShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getTaxAmountWithCurrency() {
        return taxAmountWithCurrency;
    }

    public void setTaxAmountWithCurrency(String taxAmountWithCurrency) {
        this.taxAmountWithCurrency = taxAmountWithCurrency;
    }

    public String getVariantInventoryDimensionId() {
        return variantInventoryDimensionId;
    }

    public void setVariantInventoryDimensionId(String variantInventoryDimensionId) {
        this.variantInventoryDimensionId = variantInventoryDimensionId;
    }
}
