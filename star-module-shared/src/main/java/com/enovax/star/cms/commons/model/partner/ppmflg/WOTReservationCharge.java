package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.math.BigDecimal;

/**
 * Created by jennylynsze on 6/24/16.
 */
public class WOTReservationCharge {

    private String markupCode;
    private BigDecimal ticketPrice;
    private Integer totalQty;
    private BigDecimal penaltyCharge;
    private BigDecimal refundedAmount;
    private BigDecimal ticketPurchased;
    private BigDecimal depositToBeCharged;
    private BigDecimal depositToBeRefunded;
    private BigDecimal rebookPenaltyCharge;

    private Integer originalTotalQty;
    private BigDecimal originalTicketPrice;
    private BigDecimal originalRefundAmount;

    private BigDecimal penaltyChargeCfg;
    private String showDateDisplay;
    private String showTimeDisplay;

    private boolean isCanncelRequired = false;
    private boolean noChangesFound    = false;

    /** private DepositVM deposit; **/

    public BigDecimal getPenaltyCharge() {
        return penaltyCharge;
    }

    public void setPenaltyCharge(BigDecimal penaltyCharge) {
        this.penaltyCharge = penaltyCharge;
    }

    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public BigDecimal getTicketPurchased() {
        return ticketPurchased;
    }

    public void setTicketPurchased(BigDecimal ticketPurchased) {
        this.ticketPurchased = ticketPurchased;
    }

    public BigDecimal getDepositToBeCharged() {
        return depositToBeCharged;
    }

    public void setDepositToBeCharged(BigDecimal depositToBeCharged) {
        this.depositToBeCharged = depositToBeCharged;
    }

    public BigDecimal getDepositToBeRefunded() {
        return depositToBeRefunded;
    }

    public void setDepositToBeRefunded(BigDecimal depositToBeRefunded) {
        this.depositToBeRefunded = depositToBeRefunded;
    }

    public String getMarkupCode() {
        return markupCode;
    }

    public void setMarkupCode(String markupCode) {
        this.markupCode = markupCode;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    /***
    public DepositVM getDeposit() {
        return deposit;
    }

    public void setDeposit(DepositVM deposit) {
        this.deposit = deposit;
    }

    ***/


    public BigDecimal getRebookPenaltyCharge() {
        return rebookPenaltyCharge;
    }

    public void setRebookPenaltyCharge(BigDecimal rebookPenaltyCharge) {
        this.rebookPenaltyCharge = rebookPenaltyCharge;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public boolean isCanncelRequired() {
        return isCanncelRequired;
    }

    public void setCanncelRequired(boolean canncelRequired) {
        isCanncelRequired = canncelRequired;
    }

    public Integer getOriginalTotalQty() {
        return originalTotalQty;
    }

    public void setOriginalTotalQty(Integer originalTotalQty) {
        this.originalTotalQty = originalTotalQty;
    }

    public BigDecimal getOriginalTicketPrice() {
        return originalTicketPrice;
    }

    public void setOriginalTicketPrice(BigDecimal originalTicketPrice) {
        this.originalTicketPrice = originalTicketPrice;
    }

    public boolean isNoChangesFound() {
        return noChangesFound;
    }

    public void setNoChangesFound(boolean noChangesFound) {
        this.noChangesFound = noChangesFound;
    }

    public String getShowDateDisplay() {
        return showDateDisplay;
    }

    public void setShowDateDisplay(String showDateDisplay) {
        this.showDateDisplay = showDateDisplay;
    }

    public String getShowTimeDisplay() {
        return showTimeDisplay;
    }

    public void setShowTimeDisplay(String showTimeDisplay) {
        this.showTimeDisplay = showTimeDisplay;
    }


    public BigDecimal getPenaltyChargeCfg() {
        return penaltyChargeCfg;
    }

    public void setPenaltyChargeCfg(BigDecimal penaltyChargeCfg) {
        this.penaltyChargeCfg = penaltyChargeCfg;
    }

    public BigDecimal getOriginalRefundAmount() {
        return originalRefundAmount;
    }

    public void setOriginalRefundAmount(BigDecimal originalRefundAmount) {
        this.originalRefundAmount = originalRefundAmount;
    }
}
