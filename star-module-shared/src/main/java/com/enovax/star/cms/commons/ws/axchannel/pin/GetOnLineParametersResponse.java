
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOnLineParametersResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_OnLineParametersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOnLineParametersResult"
})
@XmlRootElement(name = "GetOnLineParametersResponse", namespace = "http://tempuri.org/")
public class GetOnLineParametersResponse {

    @XmlElementRef(name = "GetOnLineParametersResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCOnLineParametersResponse> getOnLineParametersResult;

    /**
     * Gets the value of the getOnLineParametersResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCOnLineParametersResponse }{@code >}
     *     
     */
    public JAXBElement<SDCOnLineParametersResponse> getGetOnLineParametersResult() {
        return getOnLineParametersResult;
    }

    /**
     * Sets the value of the getOnLineParametersResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCOnLineParametersResponse }{@code >}
     *     
     */
    public void setGetOnLineParametersResult(JAXBElement<SDCOnLineParametersResponse> value) {
        this.getOnLineParametersResult = value;
    }

}
