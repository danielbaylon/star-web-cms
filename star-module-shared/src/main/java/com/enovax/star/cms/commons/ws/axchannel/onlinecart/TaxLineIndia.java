
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxLineIndia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxLineIndia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}TaxLine">
 *       &lt;sequence>
 *         &lt;element name="IsTaxOnTax" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TaxCodesInFormula" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="TaxComponent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxFormula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxItemGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxLineIndia", propOrder = {
    "isTaxOnTax",
    "taxCodesInFormula",
    "taxComponent",
    "taxFormula",
    "taxItemGroup"
})
public class TaxLineIndia
    extends TaxLine
{

    @XmlElement(name = "IsTaxOnTax")
    protected Boolean isTaxOnTax;
    @XmlElementRef(name = "TaxCodesInFormula", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> taxCodesInFormula;
    @XmlElementRef(name = "TaxComponent", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxComponent;
    @XmlElementRef(name = "TaxFormula", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxFormula;
    @XmlElementRef(name = "TaxItemGroup", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxItemGroup;

    /**
     * Gets the value of the isTaxOnTax property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTaxOnTax() {
        return isTaxOnTax;
    }

    /**
     * Sets the value of the isTaxOnTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTaxOnTax(Boolean value) {
        this.isTaxOnTax = value;
    }

    /**
     * Gets the value of the taxCodesInFormula property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getTaxCodesInFormula() {
        return taxCodesInFormula;
    }

    /**
     * Sets the value of the taxCodesInFormula property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setTaxCodesInFormula(JAXBElement<ArrayOfstring> value) {
        this.taxCodesInFormula = value;
    }

    /**
     * Gets the value of the taxComponent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxComponent() {
        return taxComponent;
    }

    /**
     * Sets the value of the taxComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxComponent(JAXBElement<String> value) {
        this.taxComponent = value;
    }

    /**
     * Gets the value of the taxFormula property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxFormula() {
        return taxFormula;
    }

    /**
     * Sets the value of the taxFormula property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxFormula(JAXBElement<String> value) {
        this.taxFormula = value;
    }

    /**
     * Gets the value of the taxItemGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxItemGroup() {
        return taxItemGroup;
    }

    /**
     * Sets the value of the taxItemGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxItemGroup(JAXBElement<String> value) {
        this.taxItemGroup = value;
    }

}
