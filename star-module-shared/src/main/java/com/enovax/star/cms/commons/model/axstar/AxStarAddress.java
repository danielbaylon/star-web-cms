package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

/**
 * Created by houtao on 4/8/16.
 */
public class AxStarAddress {


    private String  name = null;
    private String  fullAddress = null;
    private Long recordId = null;
    private String  street = null;
    private String  streetNumber = null;
    private String  county = null;
    private String  countyName = null;
    private String  city = null;
    private String  districtName = null;
    private String  state = null;
    private String  stateName = null;
    private String  zipCode = null;
    private String  threeLetterISORegionName = null;
    private String  phone = null;
    private Long phoneRecordId = null;
    private String  phoneExt = null;
    private String  email = null;
    private String  emailContent = null;
    private Long emailRecordId = null;
    private String  url = null;
    private Long urlRecordId = null;
    private String  twoLetterISORegionName = null;
    private Boolean deactivate = null;
    private String  attentionTo = null;
    private String  buildingCompliment = null;
    private String  postbox = null;
    private String  taxGroup = null;
    private Integer addressTypeValue = null;
    private Boolean isPrimary = null;
    private Boolean isPrivate = null;
    private String  partyNumber = null;
    private Long directoryPartyTableRecordId = null;
    private Long directoryPartyLocationRecordId = null;
    private Long directoryPartyLocationRoleRecordId = null;
    private String  logisticsLocationId = null;
    private Long logisticsLocationRecordId = null;
    private Long logisticsLocationExtRecordId = null;
    private Long logisticsLocationRoleRecordId = null;
    private Long phoneLogisticsLocationRecordId = null;
    private String  phoneLogisticsLocationId = null;
    private Long emailLogisticsLocationRecordId = null;
    private String  emailLogisticsLocationId = null;
    private Long urlLogisticsLocationRecordId = null;
    private String  urlLogisticsLocationId = null;
    private Long expireRecordId = null;
    private Integer sortOrder = null;
    private List<AxStarExtensionProperty> extensionProperties = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getThreeLetterISORegionName() {
        return threeLetterISORegionName;
    }

    public void setThreeLetterISORegionName(String threeLetterISORegionName) {
        this.threeLetterISORegionName = threeLetterISORegionName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getPhoneRecordId() {
        return phoneRecordId;
    }

    public void setPhoneRecordId(Long phoneRecordId) {
        this.phoneRecordId = phoneRecordId;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public Long getEmailRecordId() {
        return emailRecordId;
    }

    public void setEmailRecordId(Long emailRecordId) {
        this.emailRecordId = emailRecordId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getUrlRecordId() {
        return urlRecordId;
    }

    public void setUrlRecordId(Long urlRecordId) {
        this.urlRecordId = urlRecordId;
    }

    public String getTwoLetterISORegionName() {
        return twoLetterISORegionName;
    }

    public void setTwoLetterISORegionName(String twoLetterISORegionName) {
        this.twoLetterISORegionName = twoLetterISORegionName;
    }

    public Boolean getDeactivate() {
        return deactivate;
    }

    public void setDeactivate(Boolean deactivate) {
        this.deactivate = deactivate;
    }

    public String getAttentionTo() {
        return attentionTo;
    }

    public void setAttentionTo(String attentionTo) {
        this.attentionTo = attentionTo;
    }

    public String getBuildingCompliment() {
        return buildingCompliment;
    }

    public void setBuildingCompliment(String buildingCompliment) {
        this.buildingCompliment = buildingCompliment;
    }

    public String getPostbox() {
        return postbox;
    }

    public void setPostbox(String postbox) {
        this.postbox = postbox;
    }

    public String getTaxGroup() {
        return taxGroup;
    }

    public void setTaxGroup(String taxGroup) {
        this.taxGroup = taxGroup;
    }

    public Integer getAddressTypeValue() {
        return addressTypeValue;
    }

    public void setAddressTypeValue(Integer addressTypeValue) {
        this.addressTypeValue = addressTypeValue;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getPartyNumber() {
        return partyNumber;
    }

    public void setPartyNumber(String partyNumber) {
        this.partyNumber = partyNumber;
    }

    public Long getDirectoryPartyTableRecordId() {
        return directoryPartyTableRecordId;
    }

    public void setDirectoryPartyTableRecordId(Long directoryPartyTableRecordId) {
        this.directoryPartyTableRecordId = directoryPartyTableRecordId;
    }

    public Long getDirectoryPartyLocationRecordId() {
        return directoryPartyLocationRecordId;
    }

    public void setDirectoryPartyLocationRecordId(Long directoryPartyLocationRecordId) {
        this.directoryPartyLocationRecordId = directoryPartyLocationRecordId;
    }

    public Long getDirectoryPartyLocationRoleRecordId() {
        return directoryPartyLocationRoleRecordId;
    }

    public void setDirectoryPartyLocationRoleRecordId(Long directoryPartyLocationRoleRecordId) {
        this.directoryPartyLocationRoleRecordId = directoryPartyLocationRoleRecordId;
    }

    public String getLogisticsLocationId() {
        return logisticsLocationId;
    }

    public void setLogisticsLocationId(String logisticsLocationId) {
        this.logisticsLocationId = logisticsLocationId;
    }

    public Long getLogisticsLocationRecordId() {
        return logisticsLocationRecordId;
    }

    public void setLogisticsLocationRecordId(Long logisticsLocationRecordId) {
        this.logisticsLocationRecordId = logisticsLocationRecordId;
    }

    public Long getLogisticsLocationExtRecordId() {
        return logisticsLocationExtRecordId;
    }

    public void setLogisticsLocationExtRecordId(Long logisticsLocationExtRecordId) {
        this.logisticsLocationExtRecordId = logisticsLocationExtRecordId;
    }

    public Long getLogisticsLocationRoleRecordId() {
        return logisticsLocationRoleRecordId;
    }

    public void setLogisticsLocationRoleRecordId(Long logisticsLocationRoleRecordId) {
        this.logisticsLocationRoleRecordId = logisticsLocationRoleRecordId;
    }

    public Long getPhoneLogisticsLocationRecordId() {
        return phoneLogisticsLocationRecordId;
    }

    public void setPhoneLogisticsLocationRecordId(Long phoneLogisticsLocationRecordId) {
        this.phoneLogisticsLocationRecordId = phoneLogisticsLocationRecordId;
    }

    public String getPhoneLogisticsLocationId() {
        return phoneLogisticsLocationId;
    }

    public void setPhoneLogisticsLocationId(String phoneLogisticsLocationId) {
        this.phoneLogisticsLocationId = phoneLogisticsLocationId;
    }

    public Long getEmailLogisticsLocationRecordId() {
        return emailLogisticsLocationRecordId;
    }

    public void setEmailLogisticsLocationRecordId(Long emailLogisticsLocationRecordId) {
        this.emailLogisticsLocationRecordId = emailLogisticsLocationRecordId;
    }

    public String getEmailLogisticsLocationId() {
        return emailLogisticsLocationId;
    }

    public void setEmailLogisticsLocationId(String emailLogisticsLocationId) {
        this.emailLogisticsLocationId = emailLogisticsLocationId;
    }

    public Long getUrlLogisticsLocationRecordId() {
        return urlLogisticsLocationRecordId;
    }

    public void setUrlLogisticsLocationRecordId(Long urlLogisticsLocationRecordId) {
        this.urlLogisticsLocationRecordId = urlLogisticsLocationRecordId;
    }

    public String getUrlLogisticsLocationId() {
        return urlLogisticsLocationId;
    }

    public void setUrlLogisticsLocationId(String urlLogisticsLocationId) {
        this.urlLogisticsLocationId = urlLogisticsLocationId;
    }

    public Long getExpireRecordId() {
        return expireRecordId;
    }

    public void setExpireRecordId(Long expireRecordId) {
        this.expireRecordId = expireRecordId;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
