package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPaDistributionMapping;


public class PaDistributionMappingHist {

    private Integer percentage;
    private String countryId;
    private String adminId;

    public PaDistributionMappingHist() {
    }
    
    public PaDistributionMappingHist(PaDistributionMappingVM pdMap) {
        this.percentage = pdMap.getPercentage();
        this.countryId = pdMap.getCountryId();
        this.adminId = pdMap.getAdminId();
    }
    
    public PaDistributionMappingHist(PPSLMPaDistributionMapping pdMap) {
        this.percentage = pdMap.getPercentage();
        this.countryId = pdMap.getCountryId();
        this.adminId = pdMap.getAdminId();
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adminId == null) ? 0 : adminId.hashCode());
        result = prime * result
                + ((countryId == null) ? 0 : countryId.hashCode());
        result = prime * result
                + ((percentage == null) ? 0 : percentage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaDistributionMappingHist other = (PaDistributionMappingHist) obj;
        if (adminId == null) {
            if (other.adminId != null)
                return false;
        } else if (!adminId.equals(other.adminId))
            return false;
        if (countryId == null) {
            if (other.countryId != null)
                return false;
        } else if (!countryId.equals(other.countryId))
            return false;
        if (percentage == null) {
            if (other.percentage != null)
                return false;
        } else if (!percentage.equals(other.percentage))
            return false;
        return true;
    }


}
