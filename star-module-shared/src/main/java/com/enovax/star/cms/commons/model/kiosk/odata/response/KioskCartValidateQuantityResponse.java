package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;

/**
 * 
 * @author Justin
 *
 */
public class KioskCartValidateQuantityResponse {

    private String errorMessage;

    private String errorStatus;

    private String transactionId;

    private List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<AxCartRetailTicketTableEntity> getAxCartRetailTicketTableEntityList() {
        return axCartRetailTicketTableEntityList;
    }

    public void setAxCartRetailTicketTableEntityList(List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList) {
        this.axCartRetailTicketTableEntityList = axCartRetailTicketTableEntityList;
    }

}
