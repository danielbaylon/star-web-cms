package com.enovax.star.cms.commons.model.axchannel.retailticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 11/28/16.
 */
public class AxFacility {
    private String facilityId;
    private List<String> operationIds = new ArrayList<>();
    private Integer facilityAction;
    private List<Integer> daysOfWeekUsage = new ArrayList<>();

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public Integer getFacilityAction() {
        return facilityAction;
    }

    public void setFacilityAction(Integer facilityAction) {
        this.facilityAction = facilityAction;
    }

    public List<String> getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(List<String> operationIds) {
        this.operationIds = operationIds;
    }

    public List<Integer> getDaysOfWeekUsage() {
        return daysOfWeekUsage;
    }

    public void setDaysOfWeekUsage(List<Integer> daysOfWeekUsage) {
        this.daysOfWeekUsage = daysOfWeekUsage;
    }
}
