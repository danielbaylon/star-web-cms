
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for AffiliationLoyaltyTier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AffiliationLoyaltyTier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="AffiliationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="AffiliationType" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}RetailAffiliationType" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoyaltyTierId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ReasonCodeLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfReasonCodeLine" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AffiliationLoyaltyTier", propOrder = {
    "affiliationId",
    "affiliationType",
    "customerId",
    "loyaltyTierId",
    "reasonCodeLines"
})
public class AffiliationLoyaltyTier
    extends CommerceEntity
{

    @XmlElement(name = "AffiliationId")
    protected Long affiliationId;
    @XmlElement(name = "AffiliationType")
    @XmlSchemaType(name = "string")
    protected RetailAffiliationType affiliationType;
    @XmlElementRef(name = "CustomerId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerId;
    @XmlElement(name = "LoyaltyTierId")
    protected Long loyaltyTierId;
    @XmlElementRef(name = "ReasonCodeLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfReasonCodeLine> reasonCodeLines;

    /**
     * Gets the value of the affiliationId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAffiliationId() {
        return affiliationId;
    }

    /**
     * Sets the value of the affiliationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAffiliationId(Long value) {
        this.affiliationId = value;
    }

    /**
     * Gets the value of the affiliationType property.
     * 
     * @return
     *     possible object is
     *     {@link RetailAffiliationType }
     *     
     */
    public RetailAffiliationType getAffiliationType() {
        return affiliationType;
    }

    /**
     * Sets the value of the affiliationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetailAffiliationType }
     *     
     */
    public void setAffiliationType(RetailAffiliationType value) {
        this.affiliationType = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerId(JAXBElement<String> value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the loyaltyTierId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLoyaltyTierId() {
        return loyaltyTierId;
    }

    /**
     * Sets the value of the loyaltyTierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLoyaltyTierId(Long value) {
        this.loyaltyTierId = value;
    }

    /**
     * Gets the value of the reasonCodeLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfReasonCodeLine> getReasonCodeLines() {
        return reasonCodeLines;
    }

    /**
     * Sets the value of the reasonCodeLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public void setReasonCodeLines(JAXBElement<ArrayOfReasonCodeLine> value) {
        this.reasonCodeLines = value;
    }

}
