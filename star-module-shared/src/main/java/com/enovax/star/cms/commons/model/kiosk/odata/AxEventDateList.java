package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventDateList
 * 
 * @author justin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventDateList {

    /**
     * Collection(SDC_NonBindableCRTExtension.Entity.EventDates)
     * 
     */
    @JsonProperty("EventDates")
    private List<AxEventDate> axEventDates = new ArrayList<>();

    public List<AxEventDate> getAxEventDates() {
        return axEventDates;
    }

    public void setAxEventDates(List<AxEventDate> axEventDates) {
        this.axEventDates = axEventDates;
    }

}
