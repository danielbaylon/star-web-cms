package com.enovax.star.cms.commons.service.config;

import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DefaultSysCfgService implements ISysCfgService {

    @Autowired
    private ISystemParamRepository sysParamRepo;

    @Override
    public String getAxDateTimeFormat() {
        return sysParamRepo.getSystemParamValueByKey("partner-portal-slm","ax.crt.api.partner.registration.date.time.default.format");
    }

    @Override
    public Map<String, String> getEmailConfigs() {
        return sysParamRepo.getSystemParamAsMapByKey("partner-portal-slm","sys.param.mail.server.config");
    }

    @Override
    public int getDefaultMaximumThreadPoolQuanity() {
        return 10;
    }
}
