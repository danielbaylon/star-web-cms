package com.enovax.star.cms.commons.model.system;

import java.util.List;

public class SysParamVM {

    private String name;
    private String attrName;
    private String path;
    private String value;
    private boolean requireApprove;
    private List<SysParamVM> cfgs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<SysParamVM> getCfgs() {
        return cfgs;
    }

    public void setCfgs(List<SysParamVM> cfgs) {
        this.cfgs = cfgs;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public boolean isRequireApprove() {
        return requireApprove;
    }

    public void setRequireApprove(boolean requireApprove) {
        this.requireApprove = requireApprove;
    }

}
