
package com.enovax.star.cms.commons.ws.axchannel.b2bupdatepinstatus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.b2bupdatepinstatus package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _SDCB2BUpdatePINStatusResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BUpdatePINStatusResponse");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _SDCB2BUpdatePINStatusTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BUpdatePINStatusTable");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCB2BUpdatePINStatusTableReturnMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "returnMessage");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SDCB2BUpdatePINStatusResponseTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "table");
    private final static QName _GetB2BUpdatePINStatusResponseGetB2BUpdatePINStatusResult_QNAME = new QName("http://tempuri.org/", "GetB2BUpdatePINStatusResult");
    private final static QName _GetB2BUpdatePINStatusProcessedBy_QNAME = new QName("http://tempuri.org/", "_processedBy");
    private final static QName _GetB2BUpdatePINStatusPinCode_QNAME = new QName("http://tempuri.org/", "_pinCode");
    private final static QName _GetB2BUpdatePINStatusReasonCode_QNAME = new QName("http://tempuri.org/", "_reasonCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.b2bupdatepinstatus
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SDCB2BUpdatePINStatusResponse }
     * 
     */
    public SDCB2BUpdatePINStatusResponse createSDCB2BUpdatePINStatusResponse() {
        return new SDCB2BUpdatePINStatusResponse();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link SDCB2BUpdatePINStatusTable }
     * 
     */
    public SDCB2BUpdatePINStatusTable createSDCB2BUpdatePINStatusTable() {
        return new SDCB2BUpdatePINStatusTable();
    }

    /**
     * Create an instance of {@link GetB2BUpdatePINStatusResponse }
     * 
     */
    public GetB2BUpdatePINStatusResponse createGetB2BUpdatePINStatusResponse() {
        return new GetB2BUpdatePINStatusResponse();
    }

    /**
     * Create an instance of {@link GetB2BUpdatePINStatus }
     * 
     */
    public GetB2BUpdatePINStatus createGetB2BUpdatePINStatus() {
        return new GetB2BUpdatePINStatus();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BUpdatePINStatusResponse")
    public JAXBElement<SDCB2BUpdatePINStatusResponse> createSDCB2BUpdatePINStatusResponse(SDCB2BUpdatePINStatusResponse value) {
        return new JAXBElement<SDCB2BUpdatePINStatusResponse>(_SDCB2BUpdatePINStatusResponse_QNAME, SDCB2BUpdatePINStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BUpdatePINStatusTable")
    public JAXBElement<SDCB2BUpdatePINStatusTable> createSDCB2BUpdatePINStatusTable(SDCB2BUpdatePINStatusTable value) {
        return new JAXBElement<SDCB2BUpdatePINStatusTable>(_SDCB2BUpdatePINStatusTable_QNAME, SDCB2BUpdatePINStatusTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "returnMessage", scope = SDCB2BUpdatePINStatusTable.class)
    public JAXBElement<String> createSDCB2BUpdatePINStatusTableReturnMessage(String value) {
        return new JAXBElement<String>(_SDCB2BUpdatePINStatusTableReturnMessage_QNAME, String.class, SDCB2BUpdatePINStatusTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "table", scope = SDCB2BUpdatePINStatusResponse.class)
    public JAXBElement<SDCB2BUpdatePINStatusTable> createSDCB2BUpdatePINStatusResponseTable(SDCB2BUpdatePINStatusTable value) {
        return new JAXBElement<SDCB2BUpdatePINStatusTable>(_SDCB2BUpdatePINStatusResponseTable_QNAME, SDCB2BUpdatePINStatusTable.class, SDCB2BUpdatePINStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BUpdatePINStatusResult", scope = GetB2BUpdatePINStatusResponse.class)
    public JAXBElement<SDCB2BUpdatePINStatusResponse> createGetB2BUpdatePINStatusResponseGetB2BUpdatePINStatusResult(SDCB2BUpdatePINStatusResponse value) {
        return new JAXBElement<SDCB2BUpdatePINStatusResponse>(_GetB2BUpdatePINStatusResponseGetB2BUpdatePINStatusResult_QNAME, SDCB2BUpdatePINStatusResponse.class, GetB2BUpdatePINStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_processedBy", scope = GetB2BUpdatePINStatus.class)
    public JAXBElement<String> createGetB2BUpdatePINStatusProcessedBy(String value) {
        return new JAXBElement<String>(_GetB2BUpdatePINStatusProcessedBy_QNAME, String.class, GetB2BUpdatePINStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_pinCode", scope = GetB2BUpdatePINStatus.class)
    public JAXBElement<String> createGetB2BUpdatePINStatusPinCode(String value) {
        return new JAXBElement<String>(_GetB2BUpdatePINStatusPinCode_QNAME, String.class, GetB2BUpdatePINStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_reasonCode", scope = GetB2BUpdatePINStatus.class)
    public JAXBElement<String> createGetB2BUpdatePINStatusReasonCode(String value) {
        return new JAXBElement<String>(_GetB2BUpdatePINStatusReasonCode_QNAME, String.class, GetB2BUpdatePINStatus.class, value);
    }

}
