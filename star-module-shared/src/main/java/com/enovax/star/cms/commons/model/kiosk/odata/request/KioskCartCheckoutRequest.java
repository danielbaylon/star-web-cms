package com.enovax.star.cms.commons.model.kiosk.odata.request;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class KioskCartCheckoutRequest {

    @JsonIgnore
    private String cartId;

    private String cardTypeId;

    private String paymentCard;

    private String receiptEmail;

    private String receiptNumberSequence;

    private String tokenizedPaymentCard;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(String cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public String getPaymentCard() {
        return paymentCard;
    }

    public void setPaymentCard(String paymentCard) {
        this.paymentCard = paymentCard;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public String getReceiptNumberSequence() {
        return receiptNumberSequence;
    }

    public void setReceiptNumberSequence(String receiptNumberSequence) {
        this.receiptNumberSequence = receiptNumberSequence;
    }

    public String getTokenizedPaymentCard() {
        return tokenizedPaymentCard;
    }

    public void setTokenizedPaymentCard(String tokenizedPaymentCard) {
        this.tokenizedPaymentCard = tokenizedPaymentCard;
    }

}
