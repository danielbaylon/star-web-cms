package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class AxUseDiscountCounter {

    @JsonProperty("OfferId")
    private String offerId;

    @JsonProperty("DiscountCode")
    private String discountCode = "";

    @JsonProperty("TransactionId")
    private String transactoinId;

    @JsonProperty("InventTransId")
    private String inventTransId;

    @JsonProperty("Status")
    private String status = "1";

    @JsonProperty("IsUpdate")
    private String isUpdate = "0";

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getTransactoinId() {
        return transactoinId;
    }

    public void setTransactoinId(String transactoinId) {
        this.transactoinId = transactoinId;
    }

    public String getInventTransId() {
        return inventTransId;
    }

    public void setInventTransId(String inventTransId) {
        this.inventTransId = inventTransId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }

}
