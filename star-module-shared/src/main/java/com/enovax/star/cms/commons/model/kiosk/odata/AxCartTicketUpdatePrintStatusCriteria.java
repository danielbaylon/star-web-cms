package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

/**
 *
 * SDC_TicketingExtension.DataModel.CartTicketUpdatePrintStatusCriteria
 *
 *
 * @author dbaylon
 *
 */
@JsonIgnoreProperties
@JsonSerialize(include = Inclusion.NON_NULL)
public class AxCartTicketUpdatePrintStatusCriteria {

    @JsonProperty("PrintHistory")
    private List<AxPrintHistory> axPrintHistory = new ArrayList<>();

    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("DataAreaId")
    private String dataAreaId;

    @JsonProperty("ShiftId")
    private String shiftId;

    @JsonProperty("StaffId")
    private String staffId;

    @JsonProperty("TerminalId")
    private String terminalId;

    @JsonProperty("StoreId")
    private String storeId;

    @JsonProperty("PinCode")
    private String pinCode;

    public List<AxPrintHistory> getAxPrintHistory() {
        return axPrintHistory;
    }

    public void setAxPrintHistory(List<AxPrintHistory> axPrintHistory) {
        this.axPrintHistory = axPrintHistory;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

}
