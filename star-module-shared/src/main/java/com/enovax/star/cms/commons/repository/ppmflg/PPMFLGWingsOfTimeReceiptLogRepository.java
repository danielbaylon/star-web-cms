package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReceiptLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 1/11/16.
 */
@Repository
public interface PPMFLGWingsOfTimeReceiptLogRepository extends JpaRepository<PPMFLGWingsOfTimeReceiptLog, Integer> {

    @Query("from PPMFLGWingsOfTimeReceiptLog where type = ?1 and date = ?2 and mainAccountId = ?3 order by id desc ")
    List<PPMFLGWingsOfTimeReceiptLog> findWoTReceiptGenerationLogByTypeAndDate(String s, Date date, Integer mainAccId);
}
