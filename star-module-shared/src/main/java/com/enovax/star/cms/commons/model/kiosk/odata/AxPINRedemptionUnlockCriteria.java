
package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_TicketingExtension.DataModel.CartTicketUpdatePrintStatusCriteria
 *
 * @author dbaylon
 */
@JsonIgnoreProperties
public class AxPINRedemptionUnlockCriteria {

	@JsonProperty("PINCode")
	private String pinCode;

	@JsonProperty("SessionId")
	private String sessionId;

	@JsonProperty("SourceTable")
	private String sourceTable;

	@JsonProperty("UserId")
	private String userId;

	public String getPinCode() {

		return pinCode;
	}

	public void setPinCode(String pinCode) {

		this.pinCode = pinCode;
	}

	public String getSessionId() {

		return sessionId;
	}

	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

	public String getSourceTable() {

		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {

		this.sourceTable = sourceTable;
	}

	public String getUserId() {

		return userId;
	}

	public void setUserId(String userId) {

		this.userId = userId;
	}

}
