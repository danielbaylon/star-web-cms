package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMDepositTransaction;

public class TelemoneyProcessingDataDepositTopup extends TelemoneyProcessingData {

    private String logKey;
    private PPSLMDepositTransaction slmTxn;
    private PPMFLGDepositTransaction mflgTx;

    public PPSLMDepositTransaction getSlmTxn() {
        return slmTxn;
    }

    public void setSlmTxn(PPSLMDepositTransaction slmTxn) {
        this.slmTxn = slmTxn;
    }

    public String getLogKey() {
        return logKey;
    }

    public void setLogKey(String logKey) {
        this.logKey = logKey;
    }

    public PPMFLGDepositTransaction getMflgTx() {
        return mflgTx;
    }

    public void setMflgTx(PPMFLGDepositTransaction mflgTx) {
        this.mflgTx = mflgTx;
    }
}
