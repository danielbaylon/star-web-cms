
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_PINRedemptionTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_PINRedemptionTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_PINRedemptionTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_PINRedemptionTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_PINRedemptionTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcpinRedemptionTable"
})
public class ArrayOfSDCPINRedemptionTable {

    @XmlElement(name = "SDC_PINRedemptionTable", nillable = true)
    protected List<SDCPINRedemptionTable> sdcpinRedemptionTable;

    /**
     * Gets the value of the sdcpinRedemptionTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcpinRedemptionTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCPINRedemptionTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCPINRedemptionTable }
     * 
     * 
     */
    public List<SDCPINRedemptionTable> getSDCPINRedemptionTable() {
        if (sdcpinRedemptionTable == null) {
            sdcpinRedemptionTable = new ArrayList<SDCPINRedemptionTable>();
        }
        return this.sdcpinRedemptionTable;
    }

}
