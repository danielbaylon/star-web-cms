package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AxCartTicketReprintCriteria {
    
	@JsonProperty("IsRealTime")
	private int isRealTime;
	
	@JsonProperty("ItemId")
	private String itemId;
	
	@JsonProperty("ReasonCode")
	private String reasonCode;
	
	@JsonProperty("ShiftId")
	private String shiftId;
	
	@JsonProperty("StaffId")
	private String staffId;
	
	@JsonProperty("StoreId")
	private String storeId;
	
	@JsonProperty("TerminalId")
	private String terminalId;
	
	@JsonProperty("TicketCode")
	private String ticketCode;
	
	@JsonProperty("TicketTableId")
	private String ticketTableId;
	
	@JsonProperty("TransactionId")
	private String transactionId;

	public void setIsRealTime(int isRealTime) {
		this.isRealTime = isRealTime;
	}
	
	public int getIsRealTime() {
		return isRealTime;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getShiftId() {
		return shiftId;
	}

	public void setShiftId(String shiftId) {
		this.shiftId = shiftId;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public String getTicketTableId() {
		return ticketTableId;
	}

	public void setTicketTableId(String ticketTableId) {
		this.ticketTableId = ticketTableId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
}
