package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.model.tnc.TncVM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FunCartDisplay {

    private String cartId;
    private BigDecimal total = BigDecimal.ZERO;
    private String totalText;
    private int totalQty = 0;
    private int totalMainQty = 0;
    private String payMethod = "";
    private List<String> payMethodOptions = new ArrayList<>();
    private Boolean hasTnc = false;
    private List<TncVM> tncs = new ArrayList<>();
    private BigDecimal gstRate = new BigDecimal("0.07"); //default one

    /**
     * To indicate if products in cart have admission included.
     */
    private Boolean hasAdmission = Boolean.FALSE;

    private List<FunCartDisplayCmsProduct> cmsProducts = new ArrayList<>();

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTotalText() {
        return totalText;
    }

    public void setTotalText(String totalText) {
        this.totalText = totalText;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getTotalMainQty() {
        return totalMainQty;
    }

    public void setTotalMainQty(int totalMainQty) {
        this.totalMainQty = totalMainQty;
    }

    public List<FunCartDisplayCmsProduct> getCmsProducts() {
        return cmsProducts;
    }

    public void setCmsProducts(List<FunCartDisplayCmsProduct> cmsProducts) {
        this.cmsProducts = cmsProducts;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public Boolean getHasAdmission() {
        return hasAdmission;
    }

    public void setHasAdmission(Boolean hasAdmission) {
        this.hasAdmission = hasAdmission;
    }

    public List<String> getPayMethodOptions() {
        return payMethodOptions;
    }

    public void setPayMethodOptions(List<String> payMethodOptions) {
        this.payMethodOptions = payMethodOptions;
    }

    public List<TncVM> getTncs() {
        return tncs;
    }

    public void setTncs(List<TncVM> tncs) {
        this.tncs = tncs;
    }

    public Boolean getHasTnc() {
        return hasTnc;
    }

    public void setHasTnc(Boolean hasTnc) {
        this.hasTnc = hasTnc;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }
}
