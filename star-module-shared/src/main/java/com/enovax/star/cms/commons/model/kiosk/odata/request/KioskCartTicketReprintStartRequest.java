package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketReprintCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartTicketReprintStartRequest {

	@JsonProperty("CartTicketReprintCriteria")
	private List<AxCartTicketReprintCriteria> axCartTicketReprintCriteria;

	public void setAxCartTicketReprintCriteria(List<AxCartTicketReprintCriteria> axCartTicketReprintCriteria) {
		this.axCartTicketReprintCriteria = axCartTicketReprintCriteria;
	}

	public List<AxCartTicketReprintCriteria> getAxCartTicketReprintCriteria() {
		return axCartTicketReprintCriteria;
	}

}
