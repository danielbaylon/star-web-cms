package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketPINTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartTicketReprintStartResponse {
    
    private String isRealTime;

    private String transactionId;

    private String errorMessage;

    private String errorStatus;

    @JsonProperty("CartRetailTicketPINTableEntityCollection")
    private List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList;

    @JsonProperty("CartRetailTicketTableEntityCollection")
    private List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList;

    public String getIsRealTime() {
        return isRealTime;
    }

    public void setIsRealTime(String isRealTime) {
        this.isRealTime = isRealTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public List<AxCartRetailTicketPINTableEntity> getAxCartRetailTicketPINTableEntityList() {
        return axCartRetailTicketPINTableEntityList;
    }

    public void setAxCartRetailTicketPINTableEntityList(List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList) {
        this.axCartRetailTicketPINTableEntityList = axCartRetailTicketPINTableEntityList;
    }

    public List<AxCartRetailTicketTableEntity> getAxCartRetailTicketTableEntityList() {
        return axCartRetailTicketTableEntityList;
    }

    public void setAxCartRetailTicketTableEntityList(List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList) {
        this.axCartRetailTicketTableEntityList = axCartRetailTicketTableEntityList;
    }
}
