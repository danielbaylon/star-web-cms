package com.enovax.star.cms.commons.repository.specification.builder;

import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.List;

public class SpecificationBuilder {

    public Specifications build(List<QCriteria> criterias, Class<? extends BaseSpecification> clazz) throws Exception {
        Specifications specs = null;
        if(criterias != null && clazz != null){
            for(QCriteria criteria : criterias){
                BaseSpecification spec = clazz.newInstance();
                spec.setCriteria(criteria);
                if(specs == null){
                    specs = Specifications.where(spec);
                }else{
                    specs = specs.and(spec);
                }
            }
        }
        return specs;
    }
}
