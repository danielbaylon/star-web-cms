package com.enovax.star.cms.commons.model.booking;

import java.math.BigDecimal;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

public class FunCartItem {

    private String cmsProductName;
    private String cmsProductId;

    private String listingId;
    private String productCode;
    private String name;
    private String type;
    private Integer qty;
    private BigDecimal price;

    private String cartItemId;
    private String axLineCommentId = UUID.randomUUID().toString();

    private String eventGroupId;
    private String eventLineId;
    private String eventSessionName;
    private String selectedEventDate = ""; // dd/MM/yyyy

    private boolean isTopup = false;
    private String parentCmsProductId;
    private String parentListingId;

    private String parentAxLineCommentId;

    private String discountId;
    private String discountCode;

    private boolean openDate = false;

    private boolean isCapacity = false;

    private int printing; // to check for barcode

    /**
     * Added by Justin. lineId would have value for items already added to AX
     * regardless of line is voided or not.
     *
     */
    private String lineId;

    private boolean mediaTypePin = false;

    public String generateCartItemId() {
        final boolean isEvent = StringUtils.isNotEmpty(eventGroupId);
        return (isTopup ? parentCmsProductId + parentListingId : "") + cmsProductId + listingId + (isEvent ? eventGroupId + eventLineId + selectedEventDate.replace("/", "") : "");
    }

    public String generateEventSched() {
        final boolean isEvent = StringUtils.isNotEmpty(eventGroupId);
        return (isEvent ? eventGroupId + eventLineId + selectedEventDate.replace("/", "") : "");
    }

    public String getCmsProductName() {
        return cmsProductName;
    }

    public void setCmsProductName(String cmsProductName) {
        this.cmsProductName = cmsProductName;
    }

    public String getCmsProductId() {
        return cmsProductId;
    }

    public void setCmsProductId(String cmsProductId) {
        this.cmsProductId = cmsProductId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getSelectedEventDate() {
        return selectedEventDate;
    }

    public void setSelectedEventDate(String selectedEventDate) {
        this.selectedEventDate = selectedEventDate;
    }

    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getEventSessionName() {
        return eventSessionName;
    }

    public void setEventSessionName(String eventSessionName) {
        this.eventSessionName = eventSessionName;
    }

    public String getParentListingId() {
        return parentListingId;
    }

    public void setParentListingId(String parentListingId) {
        this.parentListingId = parentListingId;
    }

    public String getParentCmsProductId() {
        return parentCmsProductId;
    }

    public void setParentCmsProductId(String parentCmsProductId) {
        this.parentCmsProductId = parentCmsProductId;
    }

    public boolean isTopup() {
        return isTopup;
    }

    public void setIsTopup(boolean isTopup) {
        this.isTopup = isTopup;
    }

    public String getAxLineCommentId() {
        return axLineCommentId;
    }

    public void setAxLineCommentId(String axLineCommentId) {
        this.axLineCommentId = axLineCommentId;
    }

    public String getParentAxLineCommentId() {
        return parentAxLineCommentId;
    }

    public void setParentAxLineCommentId(String parentAxLineCommentId) {
        this.parentAxLineCommentId = parentAxLineCommentId;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public int getPrinting() {
        return printing;
    }
    public void setPrinting(int printing) {
        this.printing = printing;
    }

    public boolean isMediaTypePin() {
        return mediaTypePin;
    }

    public void setMediaTypePin(boolean mediaTypePin) {
        this.mediaTypePin = mediaTypePin;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public boolean isOpenDate() {
        return openDate;
    }

    public void setOpenDate(boolean openDate) {
        this.openDate = openDate;
    }

    public boolean isCapacity() {
        return isCapacity;
    }

    public void setCapacity(boolean isCapacity) {
        this.isCapacity = isCapacity;
    }

    
}
