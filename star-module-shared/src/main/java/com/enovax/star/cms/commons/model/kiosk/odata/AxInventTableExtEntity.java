package com.enovax.star.cms.commons.model.kiosk.odata;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtEventGroup;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtPackageLine;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtProductVariant;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * 
 * SDC_TicketingExtension.Entity.InventTableExtEntity
 * 
 * 
 * @author Justin
 *
 */
public class AxInventTableExtEntity {

    private static final String ON = "1";
    private String absoluteCapacityId;
    private String accessId;
    private String dataAreaId;
    private String defaultEventLineId;
    private String defineOpenDate;
    private String eventGroupId;

    @JsonProperty("EventGroupTable")
    private List<AxEventGroupTableEntity> eventGroups = new ArrayList<>();

    private String facilityId;

    @JsonProperty("ProductVariant")
    private List<AxInventTableVariantExtEntity> productVariants = new ArrayList<>();

    private String isCapacity;
    private String isPackage;
    private String isTransport;
    private String isTicketing;
    private String itemId;
    private String mediaTypeDescription;
    private String mediaTypeId;
    // private String Nec_IsMember;
    // private String Nec_MembershipTypeCode;
    private String nonReturnable;
    private String openEndDateTime;
    private String openStartDateTime;
    private String openValidityRuleId;
    private String operationId;
    private String ownAttraction;
    private String packageId;

    @JsonProperty("packageLines")
    private List<AxPackageLineEntity> packageLines = new ArrayList<>();

    private int printerType;
    private int printing;
    private String productId;
    private String productName;
    private String recId;
    private String templateDescription;
    private String templateName;
    private String usageValidityRuleId;

    private String NOOFPAX;

    private String product;

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDefaultEventLineId() {
        return defaultEventLineId;
    }

    public void setDefaultEventLineId(String defaultEventLineId) {
        this.defaultEventLineId = defaultEventLineId;
    }

    public String getDefineOpenDate() {
        return defineOpenDate;
    }

    public void setDefineOpenDate(String defineOpenDate) {
        this.defineOpenDate = defineOpenDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public List<AxEventGroupTableEntity> getEventGroups() {
        return eventGroups;
    }

    public void setEventGroups(List<AxEventGroupTableEntity> eventGroups) {
        this.eventGroups = eventGroups;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public List<AxInventTableVariantExtEntity> getProductVariants() {
        return productVariants;
    }

    public void setProductVariants(List<AxInventTableVariantExtEntity> productVariants) {
        this.productVariants = productVariants;
    }

    public String getIsCapacity() {
        return isCapacity;
    }

    public void setIsCapacity(String isCapacity) {
        this.isCapacity = isCapacity;
    }

    public Boolean isCapacity() {
        Boolean isCapacity = Boolean.FALSE;

        if (ON.equals(isCapacity)) {
            isCapacity = Boolean.TRUE;
        }
        return isCapacity;
    }

    public String getIsPackage() {
        return isPackage;
    }

    public void setIsPackage(String isPackage) {
        this.isPackage = isPackage;
    }

    public Boolean isPackage() {
        Boolean isPackage = Boolean.FALSE;

        if (ON.equals(isPackage)) {
            isPackage = Boolean.TRUE;
        }
        return isPackage;
    }

    public String getIsTransport() {
        return isTransport;
    }

    public void setIsTransport(String isTransport) {
        this.isTransport = isTransport;
    }

    public Boolean isTransport() {
        Boolean isTransport = Boolean.FALSE;

        if (ON.equals(isTransport)) {
            isTransport = Boolean.TRUE;
        }
        return isTransport;
    }

    public String getIsTicketing() {
        return isTicketing;
    }

    public void setIsTicketing(String isTicketing) {
        this.isTicketing = isTicketing;
    }

    public Boolean isTicketing() {
        Boolean isTicketing = Boolean.FALSE;

        if (ON.equals(isTicketing)) {
            isTicketing = Boolean.TRUE;
        }
        return isTicketing;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMediaTypeDescription() {
        return mediaTypeDescription;
    }

    public void setMediaTypeDescription(String mediaTypeDescription) {
        this.mediaTypeDescription = mediaTypeDescription;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public String getNonReturnable() {
        return nonReturnable;
    }

    public void setNonReturnable(String nonReturnable) {
        this.nonReturnable = nonReturnable;
    }

    public String getOpenEndDateTime() {
        return openEndDateTime;
    }

    public Date getOpenEndDateTime(String format) {
        Date date = null;
        try {
            date = NvxDateUtils.parseDate(this.openEndDateTime, format);
        } catch (ParseException e) {
        }
        return date;
    }

    public void setOpenEndDateTime(String openEndDateTime) {
        this.openEndDateTime = openEndDateTime;
    }

    public String getOpenStartDateTime() {
        return openStartDateTime;
    }

    public Date getOpenStartDateTime(String format) {
        Date date = null;
        try {
            date = NvxDateUtils.parseDate(this.openStartDateTime, format);
        } catch (ParseException e) {
        }
        return date;
    }

    public void setOpenStartDateTime(String openStartDateTime) {
        this.openStartDateTime = openStartDateTime;
    }

    public String getOpenValidityRuleId() {
        return openValidityRuleId;
    }

    public void setOpenValidityRuleId(String openValidityRuleId) {
        this.openValidityRuleId = openValidityRuleId;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getOwnAttraction() {
        return ownAttraction;
    }

    public void setOwnAttraction(String ownAttraction) {
        this.ownAttraction = ownAttraction;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public List<AxPackageLineEntity> getPackageLines() {
        return packageLines;
    }

    public void setPackageLines(List<AxPackageLineEntity> packageLines) {
        this.packageLines = packageLines;
    }

    public int getPrinterType() {
        return printerType;
    }

    public void setPrinterType(int printerType) {
        this.printerType = printerType;
    }

    public int getPrinting() {
        return printing;
    }

    public void setPrinting(int printing) {
        this.printing = printing;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getUsageValidityRuleId() {
        return usageValidityRuleId;
    }

    public void setUsageValidityRuleId(String usageValidityRuleId) {
        this.usageValidityRuleId = usageValidityRuleId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getNOOFPAX() {
        return NOOFPAX;
    }

    public void setNOOFPAX(String nOOFPAX) {
        NOOFPAX = nOOFPAX;
    }

}
