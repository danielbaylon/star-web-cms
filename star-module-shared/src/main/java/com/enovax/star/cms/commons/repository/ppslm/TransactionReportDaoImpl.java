package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;
import com.enovax.star.cms.commons.model.partner.ppslm.TransMegaStatus;
import com.enovax.star.cms.commons.model.partner.ppslm.TransReportModel;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 8/11/16.
 */
@Repository
public class TransactionReportDaoImpl implements TransactionReportDao {

    @Autowired(required = false)
    @Qualifier("sessionFactory")
    SessionFactory sessionFactory;

    public SQLQuery prepareSqlQuery(String query, Map<String, Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        for (String key : parameters.keySet()) {
            Object para = parameters.get(key);
            if (para instanceof List<?>) {
                queryObject.setParameterList(key, (List<?>)para);
            } else {
                queryObject.setParameter(key, para);
            }
        }
        return queryObject;
    }

    public SQLQuery prepareSqlQuery(String query, List<Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        int i=0;
        for (Object para : parameters) {
            queryObject.setParameter(i, para);
            i++;
        }
        return queryObject;
    }

    private Object executeSqlQueryScalar(String query, List<Object> params) {
        SQLQuery qry = prepareSqlQuery(query, params);
        return qry.uniqueResult();
    }

    private static final String SQL_TRANS_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, rt.receiptNum as receiptNum, rt.createdDate as txnDate, " +
                    "null as expiryDate, null as revalExpiryDate, 'Revalidation Admin Charge' as itemDesc, " +
                    "rt.totalMainQty as qty, rt.revalFeeInCents as priceInCents,  " +
                    "rt.totalMainQty * rt.revalFeeInCents as totalAmountCents, rt.username as purchasedBy, " +
                    "it.receiptNum as revalidateReference, rt.status as transStatus, rt.tmStatus as tmStatus, " +
                    "case  " +
                    "when rt.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when rt.status in ('Available', 'Revalidated', 'Refunded', 'Expired', 'Expiring') then 'Successful' " +
                    "when rt.status in ('Failed') then 'Failed' " +
                    "end as transStatusText, 'Revalidation Item Fee Code: ' + cast(rt.revalItemCode as varchar(10)) as remarks," +
                    "1 as reval, " +
                    "rt.paymentType, opr.status as offlinePaymentStatus, opr.approveDate as offlinePayApproveDate, " +
                    "opr.id as offlinePaymentId " +
                    "from PPSLMRevalidationTransaction rt " +
                    "inner join PPSLMInventoryTransaction it on it.id = rt.transactionId " +
                    "inner join PPSLMTAMainAccount acct on rt.mainAccountId = acct.id " +
                    "inner join PPSLMPartner partner on acct.id = partner.adminId " +
                    "left  join PPSLMOfflinePaymentRequest opr on opr.transactionId = rt.id and opr.transactionId = -1 " +
                    "where 1 = 1 " +
                    "{{revalFilterClause}} " +
                    "union all " +
                    "select partner.orgName as orgName, it.receiptNum as receiptNum, it.createdDate as txnDate,  " +
                    "isnull(rt.oldValidityEndDate, it.validityEndDate) as expiryDate, rt.newValidityEndDate as revalExpiryDate, " +
                    "item.displayName as itemDesc, item.qty as qty, item.unitPrice * 100 as priceInCents,  " +
                    "item.qty * item.unitPrice * 100 as totalAmountCents, it.username as purchasedBy,  " +
                    "isnull(rt.receiptNum, '') as revalidateReference, it.status as transStatus, it.tmStatus as tmStatus, " +
                    "case  " +
                    "when it.status in ('Reserved', 'Incomplete') then 'Incomplete' " +
                    "when it.status in ('Available', 'Revalidated', 'Expired', 'Expiring') then 'Successful' " +
                    "when it.status in ('Refunded') then 'Refunded' " +
                    "when it.status in ('Failed') then 'Failed' " +
                    "end as transStatusText, '' as remarks," +
                    "0 as reval, " +
                    "case it.paymentType when 'OfflinePayment' then opr.paymentType else it.paymentType end paymentType, " +
                    "opr.status as offlinePaymentStatus, opr.approveDate as offlinePayApproveDate, "+
                    "opr.id offlinePaymentId "+
                    "from PPSLMInventoryTransaction it " +
                    "inner join PPSLMTAMainAccount acct on it.mainAccountId = acct.id " +
                    "inner join PPSLMPartner partner on acct.id = partner.adminId " +
                    "inner join PPSLMInventoryTransactionItem item on it.id = item.transactionId " +
                    "left join PPSLMRevalidationTransaction rt on it.id = rt.transactionId " +
                    "left join PPSLMOfflinePaymentRequest opr on opr.transactionId = it.id " +
                    "where 1 = 1 " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_INVALIDATE_CLAUSE = " and 1 = 0 ";
    private static final String SQL_TR_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_TR_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";

    private static final String SQL_TR_CLAUSE_TXN_DT_REVAL = " and rt.createdDate >= :fromDate and rt.createdDate - 1 <= :toDate ";
    private static final String SQL_TR_CLAUSE_RECEIPT_REVAL = " and rt.receiptNum = :receiptNum ";
    private static final String SQL_TR_CLAUSE_STATUS_REVAL = " and rt.status in (:transStatuses) ";

    private static final String SQL_TR_CLAUSE_TXN_DT = " and it.createdDate >= :fromDate and it.createdDate - 1 <= :toDate ";
    private static final String SQL_TR_CLAUSE_RECEIPT = " and it.receiptNum = :receiptNum ";
    private static final String SQL_TR_CLAUSE_STATUS = " and it.status in (:transStatuses) ";
    private static final String SQL_TR_CLAUSE_ITEM_NAME = " and it.id in (select distinct item2.transactionId from PPSLMInventoryTransactionItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    public ReportResult<TransReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<TransReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_TR_CLAUSE_TXN_DT);
        final StringBuilder revalSb = new StringBuilder(SQL_TR_CLAUSE_TXN_DT_REVAL);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_TR_CLAUSE_ACCOUNT_ID);
            revalSb.append(SQL_TR_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_TR_CLAUSE_ORG_NAME);
            revalSb.append(SQL_TR_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            params.put("paIds", filter.getPaIds() );
        }

        if (StringUtils.isNotEmpty(filter.getReceiptNum())) {
            invSb.append(SQL_TR_CLAUSE_RECEIPT);
            revalSb.append(SQL_TR_CLAUSE_RECEIPT_REVAL);

            params.put("receiptNum",  filter.getReceiptNum());
        }

        if(filter.getPaymentType() != null && filter.getPaymentType().trim().length() > 0){
            if("Online".equalsIgnoreCase(filter.getPaymentType())){
                invSb.append("   and opr.id is null and ( it.paymentType is null or it.paymentType <> 'OfflinePayment' ) ");
                revalSb.append(" and opr.id is null and ( rt.paymentType is null or rt.paymentType <> 'OfflinePayment' ) ");
                if (filter.getTransMegaStatus() != null) {
                    final List<String> statuses = TransMegaStatus.getChildStatuses(filter.getTransMegaStatus());
                    if(statuses != null && statuses.size() > 0){
                        invSb.append(SQL_TR_CLAUSE_STATUS);
                        revalSb.append(SQL_TR_CLAUSE_STATUS_REVAL);
                        params.put("transStatuses", statuses);
                    }
                }
            }else if("Offline".equalsIgnoreCase(filter.getPaymentType())){
                invSb.append("   and opr.id is not null and ( it.paymentType is not null or it.paymentType = 'OfflinePayment' ) ");
                revalSb.append(" and opr.id is not null and ( rt.paymentType is not null or rt.paymentType = 'OfflinePayment' ) ");
                if(filter.getStatus() != null && filter.getStatus().trim().length() > 0){
                    invSb.append("   and opr.status is not null and opr.status = :oprStatus ");
                    revalSb.append(" and opr.status is not null and opr.status = :oprStatus ");
                    params.put("oprStatus", filter.getStatus().trim());
                }
            }
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_TR_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

            if (StringUtils.isEmpty(filter.getRevalItem())) {
                revalSb.setLength(0);
                revalSb.append(SQL_INVALIDATE_CLAUSE);
            }
        } else {
            if (!StringUtils.isEmpty(filter.getRevalItem())) {
                invSb.setLength(0);
                invSb.append(SQL_INVALIDATE_CLAUSE);
            }
        }

        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_PA_CLAUSE);
            revalSb.append(SQL_PA_CLAUSE);
        }

        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "receiptNum":
                    sqlOrderBy = " order by tbl.receiptNum "; break;
                case "txnDate":
                case "txnDateText":
                    sqlOrderBy = " order by tbl.txnDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "revalExpiryDate":
                case "revalExpiryDateText":
                    sqlOrderBy = " order by tbl.revalExpiryDate "; break;
                case "itemDesc":
                    sqlOrderBy = " order by tbl.itemDesc "; break;
                case "purchasedBy":
                    sqlOrderBy = " order by tbl.purchasedBy "; break;
                case "revalidateReference":
                    sqlOrderBy = " order by tbl.revalidateReference "; break;
                case "transStatus":
                case "transStatusText":
                    sqlOrderBy = " order by tbl.transStatusText "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_TRANS_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{revalFilterClause}}", revalSb.toString())
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("receiptNum", StandardBasicTypes.STRING)
                .addScalar("txnDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("revalExpiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("itemDesc", StandardBasicTypes.STRING)
                .addScalar("qty", StandardBasicTypes.INTEGER)
                .addScalar("priceInCents", StandardBasicTypes.INTEGER)
                .addScalar("totalAmountCents", StandardBasicTypes.INTEGER)
                .addScalar("purchasedBy", StandardBasicTypes.STRING)
                .addScalar("revalidateReference", StandardBasicTypes.STRING)
                .addScalar("transStatus", StandardBasicTypes.STRING)
                .addScalar("tmStatus", StandardBasicTypes.STRING)
                .addScalar("transStatusText", StandardBasicTypes.STRING)
                .addScalar("remarks", StandardBasicTypes.STRING)
                .addScalar("reval", StandardBasicTypes.INTEGER)
                .addScalar("paymentType", StandardBasicTypes.STRING)
                .addScalar("offlinePaymentStatus", StandardBasicTypes.STRING)
                .addScalar("offlinePaymentId", StandardBasicTypes.INTEGER)
                .addScalar("offlinePayApproveDate", StandardBasicTypes.DATE)
                .setResultTransformer(Transformers.aliasToBean(TransReportModel.class));

        final List<TransReportModel> theList = qry.list();
        for (TransReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }
}
