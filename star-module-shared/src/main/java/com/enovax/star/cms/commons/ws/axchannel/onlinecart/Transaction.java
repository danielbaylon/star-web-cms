
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Transaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChargeAmountWithCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryModeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryModeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountAmountWithCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscountCodes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="LoyaltyCardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShippingAddress" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}Address" minOccurs="0"/>
 *         &lt;element name="SubtotalWithCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxAmountWithCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalAmountWithCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transaction", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", propOrder = {
    "chargeAmountWithCurrency",
    "deliveryModeDescription",
    "deliveryModeId",
    "discountAmount",
    "discountAmountWithCurrency",
    "discountCodes",
    "loyaltyCardId",
    "shippingAddress",
    "subtotalWithCurrency",
    "taxAmountWithCurrency",
    "totalAmount",
    "totalAmountWithCurrency"
})
@XmlSeeAlso({
    SalesOrder.class
})
public class Transaction {

    @XmlElementRef(name = "ChargeAmountWithCurrency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> chargeAmountWithCurrency;
    @XmlElementRef(name = "DeliveryModeDescription", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryModeDescription;
    @XmlElementRef(name = "DeliveryModeId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryModeId;
    @XmlElement(name = "DiscountAmount")
    protected BigDecimal discountAmount;
    @XmlElementRef(name = "DiscountAmountWithCurrency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discountAmountWithCurrency;
    @XmlElementRef(name = "DiscountCodes", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> discountCodes;
    @XmlElementRef(name = "LoyaltyCardId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loyaltyCardId;
    @XmlElementRef(name = "ShippingAddress", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ShippingAddress> shippingAddress;
    @XmlElementRef(name = "SubtotalWithCurrency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subtotalWithCurrency;
    @XmlElementRef(name = "TaxAmountWithCurrency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxAmountWithCurrency;
    @XmlElement(name = "TotalAmount")
    protected BigDecimal totalAmount;
    @XmlElementRef(name = "TotalAmountWithCurrency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> totalAmountWithCurrency;

    /**
     * Gets the value of the chargeAmountWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChargeAmountWithCurrency() {
        return chargeAmountWithCurrency;
    }

    /**
     * Sets the value of the chargeAmountWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChargeAmountWithCurrency(JAXBElement<String> value) {
        this.chargeAmountWithCurrency = value;
    }

    /**
     * Gets the value of the deliveryModeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryModeDescription() {
        return deliveryModeDescription;
    }

    /**
     * Sets the value of the deliveryModeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryModeDescription(JAXBElement<String> value) {
        this.deliveryModeDescription = value;
    }

    /**
     * Gets the value of the deliveryModeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryModeId() {
        return deliveryModeId;
    }

    /**
     * Sets the value of the deliveryModeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryModeId(JAXBElement<String> value) {
        this.deliveryModeId = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the discountAmountWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscountAmountWithCurrency() {
        return discountAmountWithCurrency;
    }

    /**
     * Sets the value of the discountAmountWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscountAmountWithCurrency(JAXBElement<String> value) {
        this.discountAmountWithCurrency = value;
    }

    /**
     * Gets the value of the discountCodes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getDiscountCodes() {
        return discountCodes;
    }

    /**
     * Sets the value of the discountCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setDiscountCodes(JAXBElement<ArrayOfstring> value) {
        this.discountCodes = value;
    }

    /**
     * Gets the value of the loyaltyCardId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoyaltyCardId() {
        return loyaltyCardId;
    }

    /**
     * Sets the value of the loyaltyCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoyaltyCardId(JAXBElement<String> value) {
        this.loyaltyCardId = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ShippingAddress }{@code >}
     *     
     */
    public JAXBElement<ShippingAddress> getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ShippingAddress }{@code >}
     *     
     */
    public void setShippingAddress(JAXBElement<ShippingAddress> value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the subtotalWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubtotalWithCurrency() {
        return subtotalWithCurrency;
    }

    /**
     * Sets the value of the subtotalWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubtotalWithCurrency(JAXBElement<String> value) {
        this.subtotalWithCurrency = value;
    }

    /**
     * Gets the value of the taxAmountWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxAmountWithCurrency() {
        return taxAmountWithCurrency;
    }

    /**
     * Sets the value of the taxAmountWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxAmountWithCurrency(JAXBElement<String> value) {
        this.taxAmountWithCurrency = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the totalAmountWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTotalAmountWithCurrency() {
        return totalAmountWithCurrency;
    }

    /**
     * Sets the value of the totalAmountWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTotalAmountWithCurrency(JAXBElement<String> value) {
        this.totalAmountWithCurrency = value;
    }

}
