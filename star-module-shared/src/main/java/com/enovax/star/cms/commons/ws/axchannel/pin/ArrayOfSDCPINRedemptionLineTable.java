
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_PINRedemptionLineTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_PINRedemptionLineTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_PINRedemptionLineTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_PINRedemptionLineTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_PINRedemptionLineTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcpinRedemptionLineTable"
})
public class ArrayOfSDCPINRedemptionLineTable {

    @XmlElement(name = "SDC_PINRedemptionLineTable", nillable = true)
    protected List<SDCPINRedemptionLineTable> sdcpinRedemptionLineTable;

    /**
     * Gets the value of the sdcpinRedemptionLineTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcpinRedemptionLineTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCPINRedemptionLineTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCPINRedemptionLineTable }
     * 
     * 
     */
    public List<SDCPINRedemptionLineTable> getSDCPINRedemptionLineTable() {
        if (sdcpinRedemptionLineTable == null) {
            sdcpinRedemptionLineTable = new ArrayList<SDCPINRedemptionLineTable>();
        }
        return this.sdcpinRedemptionLineTable;
    }

}
