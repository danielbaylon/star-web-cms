package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by houtao on 22/9/16.
 */
public class AxPriceGroup {

    private boolean isB2B;
    private String name;
    private String priceGroupId;

    public boolean isB2B() {
        return isB2B;
    }

    public void setB2B(boolean b2B) {
        isB2B = b2B;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(String priceGroupId) {
        this.priceGroupId = priceGroupId;
    }
}
