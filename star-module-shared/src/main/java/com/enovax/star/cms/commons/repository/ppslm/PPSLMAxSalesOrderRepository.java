package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAxSalesOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/17/16.
 */
@Repository
public interface PPSLMAxSalesOrderRepository extends JpaRepository<PPSLMAxSalesOrder, Integer>{
    List<PPSLMAxSalesOrder> findByTransactionId(Integer transactionId);
}
