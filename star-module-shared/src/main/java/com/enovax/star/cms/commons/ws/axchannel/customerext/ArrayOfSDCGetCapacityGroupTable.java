
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_GetCapacityGroupTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_GetCapacityGroupTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_GetCapacityGroupTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_GetCapacityGroupTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_GetCapacityGroupTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcGetCapacityGroupTable"
})
public class ArrayOfSDCGetCapacityGroupTable {

    @XmlElement(name = "SDC_GetCapacityGroupTable", nillable = true)
    protected List<SDCGetCapacityGroupTable> sdcGetCapacityGroupTable;

    /**
     * Gets the value of the sdcGetCapacityGroupTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcGetCapacityGroupTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCGetCapacityGroupTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCGetCapacityGroupTable }
     * 
     * 
     */
    public List<SDCGetCapacityGroupTable> getSDCGetCapacityGroupTable() {
        if (sdcGetCapacityGroupTable == null) {
            sdcGetCapacityGroupTable = new ArrayList<SDCGetCapacityGroupTable>();
        }
        return this.sdcGetCapacityGroupTable;
    }

}
