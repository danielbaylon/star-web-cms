package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by lavanya on 8/11/16.
 */
public class InventoryReportModel {
    private String orgName;
    private String receiptNum;
    private Date txnDate;
    private Date expiryDate;
    private String itemDesc;
    private Integer qtyPurchased;
    private Integer qtyPackaged;
    private Integer balance;
    private String revalidateReference;
    private String transStatus;
    private String tmStatus;
    private String transStatusText;
    private String remarks;

    @Transient
    private String txnDateText;
    @Transient
    private String expiryDateText;

    public void initReportValues() {
        this.txnDateText = txnDate == null ? "" : NvxDateUtils.formatDateForDisplay(txnDate, true);
        this.expiryDateText = expiryDate == null ? "" : NvxDateUtils.formatDateForDisplay(expiryDate, false);
        if (!EnovaxTmSystemStatus.Success.toString().equals(tmStatus)) {
            this.remarks = StringUtils.isEmpty(tmStatus) ? "" : "TM Status: "
                    + tmStatus;
        }
    }

    /*
     * GET + SET
     */

    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public Date getTxnDate() {
        return txnDate;
    }
    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }
    public Date getExpiryDate() {
        return expiryDate;
    }
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    public String getItemDesc() {
        return itemDesc;
    }
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
    public Integer getQtyPurchased() {
        return qtyPurchased;
    }
    public void setQtyPurchased(Integer qtyPurchased) {
        this.qtyPurchased = qtyPurchased;
    }
    public Integer getQtyPackaged() {
        return qtyPackaged;
    }
    public void setQtyPackaged(Integer qtyPackaged) {
        this.qtyPackaged = qtyPackaged;
    }
    public Integer getBalance() {
        return balance;
    }
    public void setBalance(Integer balance) {
        this.balance = balance;
    }
    public String getRevalidateReference() {
        return revalidateReference;
    }
    public void setRevalidateReference(String revalidateReference) {
        this.revalidateReference = revalidateReference;
    }
    public String getTransStatus() {
        return transStatus;
    }
    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }
    public String getTmStatus() {
        return tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getTransStatusText() {
        return transStatusText;
    }
    public void setTransStatusText(String transStatusText) {
        this.transStatusText = transStatusText;
    }
    public String getRemarks() {
        return remarks;
    }
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    public String getTxnDateText() {
        return txnDateText;
    }
    public void setTxnDateText(String txnDateText) {
        this.txnDateText = txnDateText;
    }
    public String getExpiryDateText() {
        return expiryDateText;
    }
    public void setExpiryDateText(String expiryDateText) {
        this.expiryDateText = expiryDateText;
    }
}
