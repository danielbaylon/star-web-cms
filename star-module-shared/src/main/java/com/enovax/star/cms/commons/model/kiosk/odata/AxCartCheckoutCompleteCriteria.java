package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 10 SEP 16
 */
public class AxCartCheckoutCompleteCriteria {

    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("DataLevelValue")
    private String dataLevelValue;

    @JsonProperty("IsPINRedemption")
    private String isPINRedemption;

    @JsonProperty("AccountNum")
    private String accountNum;

    @JsonProperty("SourceTable")
    private String sourceTable;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDataLevelValue() {
        return dataLevelValue;
    }

    public void setDataLevelValue(String dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public String getIsPINRedemption() {
        return isPINRedemption;
    }

    public void setIsPINRedemption(String isPINRedemption) {
        this.isPINRedemption = isPINRedemption;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

}
