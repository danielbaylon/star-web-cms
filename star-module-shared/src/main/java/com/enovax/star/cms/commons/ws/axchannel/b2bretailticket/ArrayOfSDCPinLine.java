
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_PinLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_PinLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_PinLine" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_PinLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_PinLine", propOrder = {
    "sdcPinLine"
})
public class ArrayOfSDCPinLine {

    @XmlElement(name = "SDC_PinLine", nillable = true)
    protected List<SDCPinLine> sdcPinLine;

    /**
     * Gets the value of the sdcPinLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcPinLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCPinLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCPinLine }
     * 
     * 
     */
    public List<SDCPinLine> getSDCPinLine() {
        if (sdcPinLine == null) {
            sdcPinLine = new ArrayList<SDCPinLine>();
        }
        return this.sdcPinLine;
    }

}
