package com.enovax.star.cms.commons.constant.ppslm;


import com.enovax.star.cms.commons.model.partner.ppslm.DropDownVM;

import java.util.ArrayList;
import java.util.List;

public enum PartnerDocType {

    CompanyProfile("CP","Company Profile/ACRA"),
    ACRADocument("AD","ACRA Document"),
    PartnerLicense("PL","Travel Agent License");
    public String code;
    public String label;

    private PartnerDocType(String code,String label){
        this.code = code;
        this.label = label;
    }

    public static String getLabel(String code){
        for(PartnerDocType tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }

    public static List<DropDownVM> getInUseDocTypes(){
        List<DropDownVM> ddvms = new ArrayList<>();
        ddvms.add(new DropDownVM(CompanyProfile.code,CompanyProfile.label));
        ddvms.add(new DropDownVM(PartnerLicense.code,PartnerLicense.label));
        return ddvms;
    }
}
