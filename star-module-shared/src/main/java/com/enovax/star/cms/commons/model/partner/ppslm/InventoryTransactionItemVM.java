package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class InventoryTransactionItemVM implements Serializable, Comparable<InventoryTransactionItemVM> {

    private Integer id;

    private Integer transId;

    private String cmsProductName;
    private String cmsProductId;

    private String listingId;
    private String productCode;
    private String name;

    private String type;
    private Integer qty;
    private Integer unpackagedQty;

    private String receiptNumber;

    private String itemType;
    private String displayDetails;
    private Integer baseQty = 1;
    private BigDecimal unitPrice;
    private BigDecimal subtotal;

    private BigDecimal discountTotal;
    private String discountLabel;

    private Date validityStartDate;
    private String validityStartDateStr;
    private Date validityEndDate;
    private String validityEndDateStr;
    private String status;
    private String username;


    private String transactionItemId;

    private String eventGroupId;
    private String eventLineId;
    private String eventSessionName;
    private String selectedEventDate = ""; //dd/MM/yyyy

    private boolean ableToPkg;

    private Integer pkgQty;

    private Date dateOfVisit;

    private boolean isTopup;
    private String parentCmsProductId = "";
    private String parentListingId = "";

    private List<TopupItemVM> topupItems = new ArrayList<>();

    public InventoryTransactionItemVM() {

    }

    public InventoryTransactionItemVM(PPSLMInventoryTransactionItem item) {
        this.id = item.getId();
        this.cmsProductName = item.getProductName();
        this.cmsProductId = item.getProductId();
        this.receiptNumber = item.getInventoryTrans().getReceiptNum();
        this.type = item.getTicketType();
        this.validityStartDate = item.getInventoryTrans().getValidityStartDate();
        this.validityEndDate = item.getInventoryTrans().getValidityEndDate();
        this.baseQty= item.getBaseQty();
        this.qty = item.getQty();
        this.unpackagedQty = item.getUnpackagedQty();
        this.name = item.getDisplayName();
        this.displayDetails = item.getDisplayDetails();

        if(validityStartDate!= null)
            this.validityStartDateStr = NvxDateUtils.formatDate(item.getValidityStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        if(validityEndDate!= null)
            this.validityEndDateStr = NvxDateUtils.formatDate(item.getValidityEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.subtotal = item.getSubTotal();
        this.username = item.getInventoryTrans().getUsername();
        this.ableToPkg =  TransactionUtil.isItemAvailable(item);
        this.transId = item.getTransactionId();
        this.unitPrice = item.getUnitPrice();
        this.status = item.getInventoryTrans().getStatus();
    }

    public static List<InventoryTransactionItemVM> convertInventoryItemVM(
            List<PPSLMInventoryTransactionItem> list, Integer tktepweeks) {
        List<InventoryTransactionItemVM> itemVms = new ArrayList<InventoryTransactionItemVM>();
        for (PPSLMInventoryTransactionItem oitem : list) {
            InventoryTransactionItemVM iitemVm = new InventoryTransactionItemVM(
                    oitem);
            // set status as Expiring in x weeks
            if (TicketStatus.Available.toString().equals(
                    oitem.getInventoryTrans().getStatus())
                    || TicketStatus.Revalidated.toString().equals(
                    oitem.getInventoryTrans().getStatus())) {
                long weeks = TransactionUtil.getExpiringInWeeks(oitem
                        .getInventoryTrans());
                if (weeks <= tktepweeks && weeks > 0) {
                    iitemVm.setStatus(TransactionUtil.getExpiringStatus(oitem
                            .getInventoryTrans()));
                }
            }
            List<TopupItemVM> topupVms = new ArrayList<TopupItemVM>();
            for (PPSLMInventoryTransactionItem otopup : oitem.getTopupItems()) {
                topupVms.add(new TopupItemVM(otopup));
            }
            iitemVm.setTopupItems(topupVms);
            itemVms.add(iitemVm);
        }
        return itemVms;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCmsProductName() {
        return cmsProductName;
    }

    public void setCmsProductName(String cmsProductName) {
        this.cmsProductName = cmsProductName;
    }

    public String getCmsProductId() {
        return cmsProductId;
    }

    public void setCmsProductId(String cmsProductId) {
        this.cmsProductId = cmsProductId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Integer getBaseQty() {
        return baseQty;
    }

    public void setBaseQty(Integer baseQty) {
        this.baseQty = baseQty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(BigDecimal discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getDiscountLabel() {
        return discountLabel;
    }

    public void setDiscountLabel(String discountLabel) {
        this.discountLabel = discountLabel;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getValidityStartDateStr() {
        return validityStartDateStr;
    }

    public void setValidityStartDateStr(String validityStartDateStr) {
        this.validityStartDateStr = validityStartDateStr;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAbleToPkg() {
        return ableToPkg;
    }

    public void setAbleToPkg(boolean ableToPkg) {
        this.ableToPkg = ableToPkg;
    }

    public Date getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(Date validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public boolean checkItemAvailable() {
        return this.getUnpackagedQty() > 0;
    }

    public String getExpiringStatus(InventoryTransactionVM trans){
        //TODO evaluate
        return "";
//        long weeks = TransactionUtil.getExpiringInWeeks(trans);
//        String displayStatus;
//        if (TicketStatus.Available.toString().equals(trans.getStatus())
//                && weeks <= TransactionUtil.EXPIRING_ALERT_PERIOD && weeks > 0) {
//            displayStatus = TransactionUtil.getExpiringStatus(trans);
//        } else  if(weeks <= 0) {
//            displayStatus = TicketStatus.Expired.toString();
//        } else {
//            displayStatus = trans.getStatus();
//        }
//        return displayStatus;
    }

    public List<TopupItemVM> getTopupItems() {
        return topupItems;
    }

    public void setTopupItems(List<TopupItemVM> topupItems) {
        this.topupItems = topupItems;
    }

    public Integer getPkgQty() {
        return pkgQty;
    }

    public void setPkgQty(Integer pkgQty) {
        this.pkgQty = pkgQty;
    }

    @Override
    public int compareTo(InventoryTransactionItemVM o) {
        Date odate1 = this.getValidityEndDate();
        Date odate2 =  o.getValidityEndDate();
        if(odate1.before(odate2)){
            return 0;
        }
        return 1;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getTransactionItemId() {
        return transactionItemId;
    }

    public void setTransactionItemId(String transactionItemId) {
        this.transactionItemId = transactionItemId;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventSessionName() {
        return eventSessionName;
    }

    public void setEventSessionName(String eventSessionName) {
        this.eventSessionName = eventSessionName;
    }

    public String getSelectedEventDate() {
        return selectedEventDate;
    }

    public void setSelectedEventDate(String selectedEventDate) {
        this.selectedEventDate = selectedEventDate;
    }

    public boolean isTopup() {
        return isTopup;
    }

    public void setTopup(boolean topup) {
        isTopup = topup;
    }

    public String getParentListingId() {
        return parentListingId;
    }

    public void setParentListingId(String parentListingId) {
        this.parentListingId = parentListingId;
    }

    public String getParentCmsProductId() {
        return parentCmsProductId;
    }

    public void setParentCmsProductId(String parentCmsProductId) {
        this.parentCmsProductId = parentCmsProductId;
    }
}
