package com.enovax.star.cms.commons.datamodel.ppslm.tier;

/**
 * Created by jennylynsze on 5/13/16.
 */
public class ProductTier {
//    Node productTier;
//    ContentMap productTierMap;
//    Integer id;

    private Integer id;
    private String priceGroupId;
    private String name;
    private int isB2B;

    public ProductTier() {

    }

    public ProductTier(int id, String priceGroupId, String name, int isB2B) {
        this.id = id;
        this.priceGroupId = priceGroupId;
        this.name = name;
        this.isB2B = isB2B;
    }

    public String getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(String priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsB2B() {
        return isB2B;
    }

    public void setIsB2B(int isB2B) {
        this.isB2B = isB2B;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    //    public ProductTier(Node productTier) {
//        this.productTier = productTier;
//        this.productTierMap = new ContentMap(productTier);
//    }
//
//    public Node getProductTier() {
//        return productTier;
//    }
//
//    public void setProductTier(Node productTier) {
//        this.productTier = productTier;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public ContentMap getProductTierMap() {
//        return productTierMap;
//    }
//
//    public void setProductTierMap(ContentMap productTierMap) {
//        this.productTierMap = productTierMap;
//    }
}
