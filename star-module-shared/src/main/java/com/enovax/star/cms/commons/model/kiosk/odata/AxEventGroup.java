package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventGroups
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventGroup {

    @JsonProperty("EventGroupIdOption")
    private String eventGroupIdOption;

    @JsonProperty("EventTypeIdOption")
    private String eventTypeIdOption;

    @JsonProperty("EventLevelOption")
    private String eventLevelOption;

    /**
     * SDC_NonBindableCRTExtension.Entity.EventLineList
     * 
     */
    @JsonProperty("EventLineList")
    private AxEventLineList axEventLineList = new AxEventLineList();

    public String getEventGroupIdOption() {
        return eventGroupIdOption;
    }

    public void setEventGroupIdOption(String eventGroupIdOption) {
        this.eventGroupIdOption = eventGroupIdOption;
    }

    public String getEventTypeIdOption() {
        return eventTypeIdOption;
    }

    public void setEventTypeIdOption(String eventTypeIdOption) {
        this.eventTypeIdOption = eventTypeIdOption;
    }

    public String getEventLevelOption() {
        return eventLevelOption;
    }

    public void setEventLevelOption(String eventLevelOption) {
        this.eventLevelOption = eventLevelOption;
    }

    public AxEventLineList getAxEventLineList() {
        return axEventLineList;
    }

    public void setAxEventLineList(AxEventLineList axEventLineList) {
        this.axEventLineList = axEventLineList;
    }

}
