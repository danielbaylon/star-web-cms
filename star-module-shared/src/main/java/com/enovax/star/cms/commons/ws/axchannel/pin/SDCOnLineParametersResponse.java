
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_OnLineParametersResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_OnLineParametersResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="OnLineParameters" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_OnLineParameter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_OnLineParametersResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "onLineParameters"
})
public class SDCOnLineParametersResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "OnLineParameters", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCOnLineParameter> onLineParameters;

    /**
     * Gets the value of the onLineParameters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCOnLineParameter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCOnLineParameter> getOnLineParameters() {
        return onLineParameters;
    }

    /**
     * Sets the value of the onLineParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCOnLineParameter }{@code >}
     *     
     */
    public void setOnLineParameters(JAXBElement<ArrayOfSDCOnLineParameter> value) {
        this.onLineParameters = value;
    }

}
