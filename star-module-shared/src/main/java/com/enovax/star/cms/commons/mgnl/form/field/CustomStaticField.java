package com.enovax.star.cms.commons.mgnl.form.field;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;

/**
 * Created by jennylynsze on 11/24/16.
 */
public class CustomStaticField  extends CustomField<Object> {
    private Label label;

    public CustomStaticField(String stringLabel) {
        this.label = new Label(stringLabel);
        this.label.setContentMode(ContentMode.HTML);
    }

    protected Component initContent() {
        return this.label;
    }

    public Object getValue() {
        return this.label.getValue();
    }

    public Class<?> getType() {
        return this.getPropertyDataSource().getType();
    }

    public void setLabelValue(String value) {
        this.label.setValue(value);
    }
}
