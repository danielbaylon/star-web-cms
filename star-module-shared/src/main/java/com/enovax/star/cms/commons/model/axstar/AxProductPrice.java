package com.enovax.star.cms.commons.model.axstar;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by houtao on 5/9/16.
 */
public class AxProductPrice implements Serializable {

    Long productId;
    Long listingId;
    BigDecimal basePrice;
    BigDecimal tradeAgreementPrice;
    BigDecimal adjustedPrice;
    BigDecimal customerContextualPrice;
    BigDecimal discountAmount;
    String currencyCode;
    String itemId;
    String inventoryDimensionId;
    String unitOfMeasure;
    String validFrom;
    Long productLookupId;
    Long channelId;
    Long catalogId;
    List<AxStarExtensionProperty> extensionProperties;


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getListingId() {
        return listingId;
    }

    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getTradeAgreementPrice() {
        return tradeAgreementPrice;
    }

    public void setTradeAgreementPrice(BigDecimal tradeAgreementPrice) {
        this.tradeAgreementPrice = tradeAgreementPrice;
    }

    public BigDecimal getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(BigDecimal adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public BigDecimal getCustomerContextualPrice() {
        return customerContextualPrice;
    }

    public void setCustomerContextualPrice(BigDecimal customerContextualPrice) {
        this.customerContextualPrice = customerContextualPrice;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public Long getProductLookupId() {
        return productLookupId;
    }

    public void setProductLookupId(Long productLookupId) {
        this.productLookupId = productLookupId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
