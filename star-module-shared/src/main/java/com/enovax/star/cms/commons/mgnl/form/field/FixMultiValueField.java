package com.enovax.star.cms.commons.mgnl.form.field;

import com.enovax.star.cms.commons.mgnl.form.field.definition.FixMultiValueFieldDefinition;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterators;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.AbstractCustomMultiField;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;
import info.magnolia.ui.form.field.transformer.TransformedProperty;
import info.magnolia.ui.form.field.transformer.Transformer;
import info.magnolia.ui.form.field.transformer.multi.MultiTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Iterator;

/**
 * Created by jennylynsze on 6/14/16.
 */
public class FixMultiValueField extends AbstractCustomMultiField<FixMultiValueFieldDefinition, PropertysetItem> {

    private static final Logger log = LoggerFactory.getLogger(FixMultiValueField.class);

    private final ConfiguredFieldDefinition fieldDefinition;

    private final Button addButton = new NativeButton();
    private String buttonCaptionAdd;
    private String buttonCaptionRemove;
    private String buttonCaptionMoveUp = "Move Up";
    private String buttonCaptionMoveDown = "Move Down";

    public FixMultiValueField(FixMultiValueFieldDefinition definition, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider, Item relatedFieldItem, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, fieldFactoryFactory, componentProvider, relatedFieldItem, i18nAuthoringSupport);
        this.fieldDefinition = definition.getField();
        // Only propagate read only if the parent definition is read only
        if (definition.isReadOnly()) {
            fieldDefinition.setReadOnly(true);
        }
    }

    @Override
    protected Component initContent() {
        // Init root layout
        addStyleName("linkfield");
        root = new VerticalLayout();
        root.setSpacing(true);
        root.setWidth(100, Unit.PERCENTAGE);
        root.setHeight(-1, Unit.PIXELS);

        // Initialize Existing field
        initFields();

        return root;
    }

    /**
     * Initialize the MultiField. <br>
     * Create as many configured Field as we have related values already stored.
     */
    @Override
    protected void initFields(PropertysetItem newValue) {
        root.removeAllComponents();
        Iterator<?> it = newValue.getItemPropertyIds().iterator();
        boolean hasFieldContent = false;
        while (it.hasNext()) {
            Object propertyId = it.next();
            Property<?> property = newValue.getItemProperty(propertyId);
            root.addComponent(createEntryComponent(propertyId, property));
            hasFieldContent = true;
        }

        if(!hasFieldContent) {
            root.addComponent(createNoComponentDisplay());
        }
    }

    private Component createNoComponentDisplay() {
        final HorizontalLayout layout = new HorizontalLayout();
        Label display = new Label("<br><br><b>" + definition.getNoDataDisplay() + "</b>");
        display.setContentMode(ContentMode.HTML);
        layout.addComponent(display);
        return layout;
    }
    /**
     * Create a single element.<br>
     * This single element is composed of:<br>
     * - a configured field <br>
     * - a remove Button<br>
     */
    private Component createEntryComponent(Object propertyId, Property<?> property) {

        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidth(100, Unit.PERCENTAGE);
        layout.setHeight(-1, Unit.PIXELS);

        final Field<?> field = createLocalField(fieldDefinition, property, true); // creates property datasource if given property is null
        layout.addComponent(field);

        // bind the field's property to the item
        if (property == null) {
            property = field.getPropertyDataSource();
            ((PropertysetItem) getPropertyDataSource().getValue()).addItemProperty(propertyId, property);
        }
        final Property<?> propertyReference = property;
        // set layout to full width
        layout.setWidth(100, Unit.PERCENTAGE);

        // distribute space in favour of field over delete button
        layout.setExpandRatio(field, 1);
        if (definition.isReadOnly()) {
            return layout;
        }

        // move up Button
        Button moveUpButton = new Button();
        moveUpButton.setHtmlContentAllowed(true);
        moveUpButton.setCaption("<span class=\"" + "icon-arrow2_n" + "\"></span>");
        moveUpButton.addStyleName("inline");
        moveUpButton.setDescription(buttonCaptionMoveUp);
        moveUpButton.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                onMove(layout, propertyReference, true);
            }
        });

        // move down Button
        Button moveDownButton = new Button();
        moveDownButton.setHtmlContentAllowed(true);
        moveDownButton.setCaption("<span class=\"" + "icon-arrow2_s" + "\"></span>");
        moveDownButton.addStyleName("inline");
        moveDownButton.setDescription(buttonCaptionMoveDown);
        moveDownButton.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                onMove(layout, propertyReference, false);
            }
        });

        layout.addComponents(moveUpButton, moveDownButton);

        // make sure button stays aligned with the field and not with the optional field label when used
        layout.setComponentAlignment(moveUpButton, Alignment.BOTTOM_RIGHT);
        layout.setComponentAlignment(moveDownButton, Alignment.BOTTOM_RIGHT);

        return layout;
    }

    @Override
    public Class<? extends PropertysetItem> getType() {
        return PropertysetItem.class;
    }

    /**
     * Caption section.
     */
    public void setButtonCaptionAdd(String buttonCaptionAdd) {
        this.buttonCaptionAdd = buttonCaptionAdd;
    }

    public void setButtonCaptionRemove(String buttonCaptionRemove) {
        this.buttonCaptionRemove = buttonCaptionRemove;
    }

    /**
     * Ensure that id of the {@link PropertysetItem} stay coherent.<br>
     * Assume that we have 3 values 0:a, 1:b, 2:c, and 1 is removed <br>
     * If we just remove 1, the {@link PropertysetItem} will contain 0:a, 2:c, .<br>
     * But we should have : 0:a, 1:c, .
     */
    private void removeValueProperty(int fromIndex) {
        getValue().removeItemProperty(fromIndex);
        int toIndex = fromIndex;
        int valuesSize = getValue().getItemPropertyIds().size();
        if (fromIndex == valuesSize) {
            return;
        }
        while (fromIndex < valuesSize) {
            toIndex = fromIndex;
            fromIndex +=1;
            getValue().addItemProperty(toIndex, getValue().getItemProperty(fromIndex));
            getValue().removeItemProperty(fromIndex);
        }
    }

    /**
     * Switches two properties. We have to clone the original {@link PropertysetItem} to re-arrange the ordering.
     */
    private void switchItemProperties(Object firstPropertyId, Object secondPropertyId) {
        Property propertyFirst = getValue().getItemProperty(firstPropertyId);
        Property propertySecond = getValue().getItemProperty(secondPropertyId);

        try {
            PropertysetItem storedValues = (PropertysetItem) getValue().clone();
            if (storedValues != null) {
                for (Object propertyId : storedValues.getItemPropertyIds()) {
                    getValue().removeItemProperty(propertyId);
                    if (propertyId == firstPropertyId) {
                        getValue().addItemProperty(firstPropertyId, propertySecond);
                    } else if (propertyId == secondPropertyId) {
                        getValue().addItemProperty(secondPropertyId, propertyFirst);
                    } else {
                        getValue().addItemProperty(propertyId, storedValues.getItemProperty(propertyId));
                    }
                }
                getPropertyDataSource().setValue(getValue());
            }
        } catch (CloneNotSupportedException e) {
            log.error("Unable to switch properties on MultiField. Unable to clone PropertysetItem.", e);
        }

    }

    private void onDelete(Component layout, Property<?> propertyReference) {
        root.removeComponent(layout);
        Transformer<?> transformer = ((TransformedProperty<?>) getPropertyDataSource()).getTransformer();

        // get propertyId to delete, this might have changed since initialization above (see #removeValueProperty)
        Object propertyId = findPropertyId(getValue(), propertyReference);

        if (transformer instanceof MultiTransformer) {
            ((MultiTransformer) transformer).removeProperty(propertyId);
        } else {
            if (propertyId != null && propertyId.getClass().isAssignableFrom(Integer.class)) {
                removeValueProperty((Integer) propertyId);
            } else {
                log.error("Property id {} is not an integer and as such property can't be removed", propertyId);
            }
            getPropertyDataSource().setValue(getValue());
        }
    }

    /**
     * Takes care of moving a field up or down. Tries hard not to assume much about the layout, so we're iterating over parents
     * and component types to make sure we're dealing with Fields.
     */
    private void onMove(Component layout, Property<?> propertyReference, boolean moveUp) {
        int currentPosition = root.getComponentIndex(layout);
        int switchPosition = currentPosition + (moveUp ? -1 : 1);

        Field[] fields = Iterators.toArray(Iterators.filter(Iterators.transform(root.iterator(), new Function<Component, Field>() {
            @Nullable
            @Override
            public Field apply(Component input) {
                if (input instanceof HasComponents) {
                    Optional<Component> field = Iterators.tryFind(((HasComponents) input).iterator(), Predicates.instanceOf(Field.class));
                    if (field.isPresent()) {
                        return (Field) field.get();
                    }
                }
                return null;
            }
        }), Predicates.notNull()), Field.class);

        if (moveUp && currentPosition != 0 || (!moveUp && currentPosition != fields.length - 1)) {

            Field switchField = fields[switchPosition];
            Object currentPropertyId =  FixMultiValueField.this.findPropertyId(getValue(), propertyReference);
            Object switchPropertyId = FixMultiValueField.this.findPropertyId(getValue(), switchField.getPropertyDataSource());
            if ((int) switchPropertyId == -1) {
                Property switchProperty = ((PropertysetItem) getPropertyDataSource().getValue()).getItemProperty(switchPosition);
                if (switchProperty != null)
                    switchPropertyId = FixMultiValueField.this.findPropertyId(getValue(), switchProperty);
            }
            root.replaceComponent(root.getComponent(currentPosition), root.getComponent(switchPosition));
            switchItemProperties(currentPropertyId, switchPropertyId);
            //switchItemProperties(currentPosition, switchPosition);
        }
    }
}