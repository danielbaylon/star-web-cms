package com.enovax.star.cms.commons.repository.specification;

import com.enovax.star.cms.commons.repository.specification.query.QCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseSpecification<T> implements Specification<T> {

    private static Map<String,String> referencesMap = new HashMap<String,String>();

    static{
        referencesMap.put("'","\\'");
        referencesMap.put("%","\\%");
        referencesMap.put("\"","\\\"");
        referencesMap.put("\\","\\\\");
        referencesMap.put("\n","\\\n");
        referencesMap.put("\0","\\\0");
        referencesMap.put("\b","\\\b");
        referencesMap.put("\r","\\\r");
        referencesMap.put("\t","\\\t");
        referencesMap.put("\f","\\\f");
    }

    private QCriteria criteria;

    public void setCriteria(QCriteria criteria){
        this.criteria = criteria;
    }

    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if(!isValidCriteria()){
            return null;
        }
        if(QCriteria.Operation.EQ.name().equals(criteria.getOperation().name())){
            return eq(root, query, cb);
        }else if(QCriteria.Operation.LT.name().equals(criteria.getOperation().name())){
            return lt(root, query, cb);
        }else if(QCriteria.Operation.LE.name().equals(criteria.getOperation().name())){
            return le(root, query, cb);
        }else if(QCriteria.Operation.GT.name().equals(criteria.getOperation().name())){
            return gt(root, query, cb);
        }else if(QCriteria.Operation.GE.name().equals(criteria.getOperation().name())){
            return ge(root, query, cb);
        }else if(QCriteria.Operation.LIKE.name().equals(criteria.getOperation().name())){
            return like(root, query, cb);
        }else if(QCriteria.Operation.NOTNULL.name().equals(criteria.getOperation().name())){
            return notNull(root, query, cb);
        }else if(QCriteria.Operation.IN.name().equals(criteria.getOperation().name())){
            return in(root, query, cb);
        }
        return null;
    }

    public boolean isValidCriteria() {
        if(criteria == null || criteria.getKey() == null || criteria.getKey().trim().length() == 0 || criteria.getOperation() == null || criteria.getValueType() == null){
            return false;
        }
        return true;
    }

    private Predicate in(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if(criteria.getObjects() == null || criteria.getObjects().size() == 0){
            return null;
        }
        return cb.and( root.get(criteria.getKey()).in(criteria.getObjects()));
    }

    private Predicate eq(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getValueType()){
            case Integer:
                return cb.equal( root.get(criteria.getKey()), criteria.getIntValue() );
            case Date:
                return cb.equal( root.get(criteria.getKey()), criteria.getDateValue() );
        }
        return cb.equal( root.get(criteria.getKey()), criteria.getValue() );
    }

    private Predicate like(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if(root.get(criteria.getKey()).getJavaType() == String.class){
            return cb.like( cb.upper( root.<String>get(criteria.getKey())) , "%"+escapeSpecialCharacters(criteria.getValue())+"%"  );
        }else{
            return eq(root, query, cb);
        }
    }
    private Predicate lt(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getValueType()){
            case Integer:
                return cb.lessThan( root.get(criteria.getKey()), criteria.getIntValue() );
            case Date:
                return cb.lessThan( root.get(criteria.getKey()), criteria.getDateValue() );
        }
        return cb.lessThan( root.get(criteria.getKey()), criteria.getValue() );
    }

    private Predicate le(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getValueType()){
            case Integer:
                return cb.lessThanOrEqualTo( root.get(criteria.getKey()), criteria.getIntValue() );
            case Date:
                return cb.lessThanOrEqualTo( root.get(criteria.getKey()), criteria.getDateValue() );
        }
        return cb.lessThanOrEqualTo( root.get(criteria.getKey()), criteria.getValue() );
    }

    private Predicate gt(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getValueType()){
            case Integer:
                return cb.greaterThan( root.get(criteria.getKey()), criteria.getIntValue() );
            case Date:
                return cb.greaterThan( root.get(criteria.getKey()), criteria.getDateValue() );
        }
        return cb.greaterThan( root.get(criteria.getKey()), criteria.getValue() );
    }

    private Predicate ge(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getValueType()){
            case Integer:
                return cb.greaterThanOrEqualTo( root.get(criteria.getKey()), criteria.getIntValue() );
            case Date:
                return cb.greaterThanOrEqualTo( root.get(criteria.getKey()), criteria.getDateValue() );
        }
        return cb.greaterThanOrEqualTo( root.get(criteria.getKey()), criteria.getValue() );
    }
    private Predicate notNull(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.isNotNull(root.get(criteria.getKey()));
    }

    private static String escapeSpecialCharacters(String val) {
        if(val != null && val.trim().length() > 0){
            StringBuffer sbuffer = new StringBuffer(val.length());
            for (int i = 0; i < val.length(); i++) {
                String c = val.substring(i,i+1);
                if (referencesMap.get(c) != null) {
                    sbuffer.append(referencesMap.get(c));
                } else {
                    sbuffer.append(c);
                }
            }
            val = sbuffer.toString();
        }
        return val;
    }

}
