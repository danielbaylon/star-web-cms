package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

public class PPSLMPartnerSpecification extends BaseSpecification<PPSLMPartner> {

}
