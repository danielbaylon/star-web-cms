
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_InsertOnlineReferenceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_InsertOnlineReferenceResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="insertOnlineReferenceTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_InsertOnlineReferenceTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_InsertOnlineReferenceResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "insertOnlineReferenceTable"
})
public class SDCInsertOnlineReferenceResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "insertOnlineReferenceTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> insertOnlineReferenceTable;

    /**
     * Gets the value of the insertOnlineReferenceTable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCInsertOnlineReferenceTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> getInsertOnlineReferenceTable() {
        return insertOnlineReferenceTable;
    }

    /**
     * Sets the value of the insertOnlineReferenceTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCInsertOnlineReferenceTable }{@code >}
     *     
     */
    public void setInsertOnlineReferenceTable(JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> value) {
        this.insertOnlineReferenceTable = value;
    }

}
