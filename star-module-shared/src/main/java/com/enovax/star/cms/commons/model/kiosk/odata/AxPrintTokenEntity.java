package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * <ComplexType Name="PrintTokenEntity">
 * <Property Name="TokenKey" Type="Edm.String"/>
 * <Property Name="Value" Type="Edm.String"/>
 * <Property Name="ExtensionProperties" Type=
 * "Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
 * Nullable="false"/> </ComplexType>
 * 
 * @author Justin
 *
 */
public class AxPrintTokenEntity {

    @JsonProperty("TokenKey")
    private String tokenKey;

    @JsonProperty("Value")
    private String value;

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
