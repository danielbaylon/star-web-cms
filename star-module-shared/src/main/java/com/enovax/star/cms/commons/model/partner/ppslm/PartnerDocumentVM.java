package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.PartnerDocType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocument;

public class PartnerDocumentVM {

    private Integer id;
    private String filePath;
    private String fileName;
    private String fileType;
    private String fileTypeLabel;
    
    public PartnerDocumentVM(){
    }
    public PartnerDocumentVM(PartnerDocumentVM pa){
        this.id = pa.getId();
        this.filePath = pa.getFilePath();
        this.fileName = pa.getFileName();
        this.fileTypeLabel = PartnerDocType.getLabel(pa.getFileType());
    }

    public PartnerDocumentVM(PPSLMPADocument paDoc) {
        this.id = paDoc.getId();
        this.filePath = paDoc.getFilePath();
        this.fileName = paDoc.getFileName();
        this.fileType = paDoc.getFileType();
        this.fileTypeLabel = PartnerDocType.getLabel(paDoc.getFileType());
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileType() {
        return fileType;
    }
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
    public String getFileTypeLabel() {
        return fileTypeLabel;
    }
    public void setFileTypeLabel(String fileTypeLabel) {
        this.fileTypeLabel = fileTypeLabel;
    }
}
