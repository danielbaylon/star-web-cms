package com.enovax.star.cms.commons.model.product;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 7/20/16.
 */
public class AxProductLinkPackage {
    private boolean success;
    private String message;
    private StoreApiChannels channel;
    private List<AxMainProduct> mainProductList;
    private Map<Long, BigDecimal> mainProductPriceMap;
    private String axProductsResultJson;
    private String axRetailProductExtDataResultJson;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public List<AxMainProduct> getMainProductList() {
        return mainProductList;
    }

    public void setMainProductList(List<AxMainProduct> mainProductList) {
        this.mainProductList = mainProductList;
    }

    public String getAxProductsResultJson() {
        return axProductsResultJson;
    }

    public void setAxProductsResultJson(String axProductsResultJson) {
        this.axProductsResultJson = axProductsResultJson;
    }

    public Map<Long, BigDecimal> getMainProductPriceMap() {
        return mainProductPriceMap;
    }

    public void setMainProductPriceMap(Map<Long, BigDecimal> mainProductPriceMap) {
        this.mainProductPriceMap = mainProductPriceMap;
    }

    public String getAxRetailProductExtDataResultJson() {
        return axRetailProductExtDataResultJson;
    }

    public void setAxRetailProductExtDataResultJson(String axRetailProductExtDataResultJson) {
        this.axRetailProductExtDataResultJson = axRetailProductExtDataResultJson;
    }
}
