
package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionUnlockCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author dbaylon
 */
public class KioskPINRedemptionUnlockRequest {

	@JsonProperty("PINRedemptionUnlockCriteria")
	private AxPINRedemptionUnlockCriteria axPINRedmeptionUnlockCriteria = new AxPINRedemptionUnlockCriteria();

	public void setAxPINRedmeptionUnlockCriteria(
		AxPINRedemptionUnlockCriteria axPINRedmeptionUnlockCriteria) {

		this.axPINRedmeptionUnlockCriteria = axPINRedmeptionUnlockCriteria;
	}

	public AxPINRedemptionUnlockCriteria getAxPINRedmeptionUnlockCriteria() {

		return axPINRedmeptionUnlockCriteria;
	}

}
