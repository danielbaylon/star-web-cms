package com.enovax.star.cms.commons.model.ticketgen;

import java.util.List;

public class ReceiptMain extends ReceiptCompanyDetails {

    private String location;
    private String kioskId;
    private String gst;
    private String transId;
    private String receiptSeqNumber;
    private String receiptNumber;
    private String printedDateText;
    private String totalText;
    private List<ReceiptItem> items;
    private List<KioskPaymentItem> kioskPaymentItems;
    
    private TicketIssuanceStatus ticketIssuanceStatus;

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getPrintedDateText() {
        return printedDateText;
    }

    public void setPrintedDateText(String printedDateText) {
        this.printedDateText = printedDateText;
    }

    public List<ReceiptItem> getItems() {
        return items;
    }

    public void setItems(List<ReceiptItem> items) {
        this.items = items;
    }

    public String getTotalText() {
        return totalText;
    }

    public void setTotalText(String totalText) {
        this.totalText = totalText;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getKioskId() {
        return kioskId;
    }

    public void setKioskId(String kioskId) {
        this.kioskId = kioskId;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public List<KioskPaymentItem> getKioskPaymentItems() {
        return kioskPaymentItems;
    }

    public void setKioskPaymentItems(List<KioskPaymentItem> kioskPaymentItems) {
        this.kioskPaymentItems = kioskPaymentItems;
    }

    public String getReceiptSeqNumber() {
        return receiptSeqNumber;
    }

    public void setReceiptSeqNumber(String receiptSeqNumber) {
        this.receiptSeqNumber = receiptSeqNumber;
    }
    
    public void setTicketIssuanceStatus(TicketIssuanceStatus ticketIssuanceStatus) {
		this.ticketIssuanceStatus = ticketIssuanceStatus;
	}
    
    public TicketIssuanceStatus getTicketIssuanceStatus() {
		return ticketIssuanceStatus;
	}
}
