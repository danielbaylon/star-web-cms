
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_GetDiscountCounterResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_GetDiscountCounterResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="discountCounterEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_DiscountCounterTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_GetDiscountCounterResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "discountCounterEntity"
})
public class SDCGetDiscountCounterResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "discountCounterEntity", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCDiscountCounterTable> discountCounterEntity;

    /**
     * Gets the value of the discountCounterEntity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCDiscountCounterTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCDiscountCounterTable> getDiscountCounterEntity() {
        return discountCounterEntity;
    }

    /**
     * Sets the value of the discountCounterEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCDiscountCounterTable }{@code >}
     *     
     */
    public void setDiscountCounterEntity(JAXBElement<ArrayOfSDCDiscountCounterTable> value) {
        this.discountCounterEntity = value;
    }

}
