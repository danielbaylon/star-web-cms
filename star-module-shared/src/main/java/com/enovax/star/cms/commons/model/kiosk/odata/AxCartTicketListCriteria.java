package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * SDC_TicketingExtension.DataModel.CartTicketListCriteria
 *
 *
 * @author Justin
 * @since 9 SEP 16
 *
 */

public class AxCartTicketListCriteria {

    @JsonProperty("DataLevelValue")
    private String dataLevelValue = "4";

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("EventGroupId")
    private String eventGroupId = "";

    @JsonProperty("EventLineId")
    private String eventLineId = "";

    @JsonProperty("IsReturn")
    private String isReturn = "0";

    @JsonProperty("isUpdateCapacity")
    private String isUpdateCapacity = "1";

    @JsonProperty("ItemId")
    private String itemId;

    @JsonProperty("LineId")
    private String lineId;

    @JsonProperty("ProductId")
    private String productId;

    @JsonProperty("Qty")
    private String qty;

    @JsonProperty("ReasonCode")
    private String reasonCode = "";

    @JsonProperty("RetailVariantId")
    private String retailVariantId = "";

    @JsonProperty("ReturnInventTransId")
    private String returnInventTransId = "";

    @JsonProperty("ReturnLineNumber")
    private String returnLineNumber = "0";

    @JsonProperty("ReturnTicketCodes")
    private String returnTicketCodes = "";

    @JsonProperty("ReturnTransactionId")
    private String returnTransactionId = "";

    @JsonProperty("ShiftId")
    private String shiftId;

    @JsonProperty("StaffId")
    private String staffId;

    @JsonProperty("TableGroupAll")
    private String tableGroupAll = "0";

    @JsonProperty("TransactionId")
    private String transactionId;

    public String getDataLevelValue() {
        return dataLevelValue;
    }

    public void setDataLevelValue(String dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(String isReturn) {
        this.isReturn = isReturn;
    }

    public String getIsUpdateCapacity() {
        return isUpdateCapacity;
    }

    public void setIsUpdateCapacity(String isUpdateCapacity) {
        this.isUpdateCapacity = isUpdateCapacity;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public String getReturnInventTransId() {
        return returnInventTransId;
    }

    public void setReturnInventTransId(String returnInventTransId) {
        this.returnInventTransId = returnInventTransId;
    }

    public String getReturnLineNumber() {
        return returnLineNumber;
    }

    public void setReturnLineNumber(String returnLineNumber) {
        this.returnLineNumber = returnLineNumber;
    }

    public String getReturnTicketCodes() {
        return returnTicketCodes;
    }

    public void setReturnTicketCodes(String returnTicketCodes) {
        this.returnTicketCodes = returnTicketCodes;
    }

    public String getReturnTransactionId() {
        return returnTransactionId;
    }

    public void setReturnTransactionId(String returnTransactionId) {
        this.returnTransactionId = returnTransactionId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getTableGroupAll() {
        return tableGroupAll;
    }

    public void setTableGroupAll(String tableGroupAll) {
        this.tableGroupAll = tableGroupAll;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}
