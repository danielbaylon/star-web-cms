package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.tier.TierProductMapping;
import com.enovax.star.cms.commons.model.product.CmsMainProduct;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class PartnerProductVM  extends CmsMainProduct implements Serializable {

    private String tiers;

    public PartnerProductVM() {
        super();
    }

    public PartnerProductVM(Node productNode) {
        try {
            setId(productNode.getName());
            setName(productNode.getProperty("name").getString());
            setDescription(productNode.getProperty("description").getString());
            setProdType("General");
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public String groupTierStr(List<TierProductMapping> tierMap){
        String itiers = "";
        if(tierMap!= null && tierMap.size()!=0 ) {
            for (TierProductMapping tier : tierMap) {
                ProductTierVM productTierVM = new ProductTierVM(tier.getProductTier());
                if ("".equals(itiers)) {
                    itiers = productTierVM.getName();
                } else {
                    itiers = itiers + ", " + productTierVM.getName();
                }
            }
        }
        return itiers;
    }

    public String getTiers() {
        return tiers;
    }

    public void setTiers(String tiers) {
        this.tiers = tiers;
    }

}

