package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <ComplexType Name="FacilityClassEntity">
 * <Property Name="facilityId" Type="Edm.String"/>
 * <Property Name="operationIds" Type="Collection(Edm.String)"/>
 * <Property Name="facilityAction" Type="Edm.Int32"/>
 * <Property Name="daysOfWeekUsage" Type="Collection(Edm.Int32)" Nullable=
 * "false"/> <Property Name="facilityName" Type="Edm.String"/>
 * <Property Name="operationName" Type="Collection(Edm.String)"/>
 * <Property Name="ExtensionProperties" Type=
 * "Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
 * Nullable="false"/> </ComplexType>
 * 
 * @author Justin
 *
 */
public class AxFacilityClassEntity {

    @JsonProperty("FacilityId")
    private String facilityId;

    @JsonProperty("FacilityAction")
    private String facilityAction;

    @JsonProperty("OperationIds")
    private List<String> operationIds = new ArrayList<>();

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityAction() {
        return facilityAction;
    }

    public void setFacilityAction(String facilityAction) {
        this.facilityAction = facilityAction;
    }

    public List<String> getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(List<String> operationIds) {
        this.operationIds = operationIds;
    }

}
