
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for CommerceEntitySearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommerceEntitySearch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataLevelValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommerceEntitySearch", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", propOrder = {
    "dataLevelValue"
})
@XmlSeeAlso({
    UpCrossSellTableCriteria.class
})
public class CommerceEntitySearch {

    @XmlElement(name = "DataLevelValue")
    protected Integer dataLevelValue;

    /**
     * Gets the value of the dataLevelValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataLevelValue() {
        return dataLevelValue;
    }

    /**
     * Sets the value of the dataLevelValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataLevelValue(Integer value) {
        this.dataLevelValue = value;
    }

}
