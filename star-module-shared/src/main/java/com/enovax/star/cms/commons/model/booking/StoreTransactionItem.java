package com.enovax.star.cms.commons.model.booking;

import java.math.BigDecimal;

public class StoreTransactionItem {

    private String cmsProductName;
    private String cmsProductId;

    private String listingId;
    private String productCode;
    private String name;
    private String type;
    private Integer qty;

    private String receiptNumber;

    private String itemType;
    private String displayDetails;
    private Integer baseQty = 1;
    private BigDecimal unitPrice;
    private BigDecimal subtotal;

    private BigDecimal discountTotal;
    private String discountLabel;
    private String promoCode;
    private String promoCodeDiscountId;

    private String transactionItemId;

    private String eventGroupId;
    private String eventLineId;
    private String eventSessionName;
    private String selectedEventDate = ""; //dd/MM/yyyy

    private boolean isTopup = false;
    private String parentCmsProductId = "";
    private String parentListingId = "";
    private String otherDetails;

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventSessionName() {
        return eventSessionName;
    }

    public void setEventSessionName(String eventSessionName) {
        this.eventSessionName = eventSessionName;
    }

    public String getSelectedEventDate() {
        return selectedEventDate;
    }

    public void setSelectedEventDate(String selectedEventDate) {
        this.selectedEventDate = selectedEventDate;
    }

    public String getCmsProductName() {
        return cmsProductName;
    }

    public void setCmsProductName(String cmsProductName) {
        this.cmsProductName = cmsProductName;
    }

    public String getCmsProductId() {
        return cmsProductId;
    }

    public void setCmsProductId(String cmsProductId) {
        this.cmsProductId = cmsProductId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Integer getBaseQty() {
        return baseQty;
    }

    public void setBaseQty(Integer baseQty) {
        this.baseQty = baseQty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(BigDecimal discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getDiscountLabel() {
        return discountLabel;
    }

    public void setDiscountLabel(String discountLabel) {
        this.discountLabel = discountLabel;
    }

    public String getTransactionItemId() {
        return transactionItemId;
    }

    public void setTransactionItemId(String transactionItemId) {
        this.transactionItemId = transactionItemId;
    }

    public boolean isTopup() {
        return isTopup;
    }

    public void setTopup(boolean topup) {
        isTopup = topup;
    }

    public String getParentCmsProductId() {
        return parentCmsProductId;
    }

    public void setParentCmsProductId(String parentCmsProductId) {
        this.parentCmsProductId = parentCmsProductId;
    }

    public String getParentListingId() {
        return parentListingId;
    }

    public void setParentListingId(String parentListingId) {
        this.parentListingId = parentListingId;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeDiscountId() {
        return promoCodeDiscountId;
    }

    public void setPromoCodeDiscountId(String promoCodeDiscountId) {
        this.promoCodeDiscountId = promoCodeDiscountId;
    }
}
