package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxShift;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskGetShiftsByStatusResponse {

    @JsonProperty("value")
    private List<AxShift> axShiftList;

    public List<AxShift> getAxShiftList() {
        return axShiftList;
    }

    public void setAxShiftList(List<AxShift> axShiftList) {
        this.axShiftList = axShiftList;
    }

}
