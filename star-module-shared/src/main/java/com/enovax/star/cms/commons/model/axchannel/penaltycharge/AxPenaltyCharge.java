package com.enovax.star.cms.commons.model.axchannel.penaltycharge;

import java.math.BigDecimal;

/**
 * Created by houtao on 5/9/16.
 */
public class AxPenaltyCharge {

    protected String accountNum;
    protected String eventGroupId;
    protected String eventLineId;
    protected String markupCode;
    protected BigDecimal markupAmt;

    protected Integer accountCode;
    protected String dataAreaId;
    protected String description;
    protected String penaltyId;
    protected Long recId;

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getMarkupCode() {
        return markupCode;
    }

    public void setMarkupCode(String markupCode) {
        this.markupCode = markupCode;
    }

    public BigDecimal getMarkupAmt() {
        return markupAmt;
    }

    public void setMarkupAmt(BigDecimal markupAmt) {
        this.markupAmt = markupAmt;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPenaltyId() {
        return penaltyId;
    }

    public void setPenaltyId(String penaltyId) {
        this.penaltyId = penaltyId;
    }

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }
}
