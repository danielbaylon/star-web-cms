package com.enovax.star.cms.commons.model.axretail.inventtable;

import com.enovax.star.cms.commons.ws.axretail.inventtable.ISDCInventTableService;
import com.enovax.star.cms.commons.ws.axretail.inventtable.SDCInventTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class AxRetailProductExtServiceStubDetails {

    private Logger log = LoggerFactory.getLogger(getClass());

    private SDCInventTableService ws;
    private ISDCInventTableService stub;
    private String apiUrl;
    private boolean init = false;

    public AxRetailProductExtServiceStubDetails(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean initialise() {
        try {

            ws = new SDCInventTableService(new URL(apiUrl));
            stub = ws.getBasicHttpBindingISDCInventTableService();

            log.info("Initialised AX Retail Product Ext Service Client for " + this.apiUrl);
            this.init = true;
        } catch (Exception e) {
            log.error("Error initialising AX Retail Product Ext Service Client for " + this.apiUrl, e);
            this.init = false;
        }

        return this.init;
    }

    public SDCInventTableService getWs() {
        return ws;
    }

    public ISDCInventTableService getStub() {
        return stub;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean isInit() {
        return init;
    }
}
