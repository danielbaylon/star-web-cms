package com.enovax.star.cms.commons.datamodel.ppslm;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PPSLMPADocMapping")
public class PPSLMPADocMapping implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="partnerId",insertable = false, updatable = false)
    private PPSLMPartner partner;
    
    
    @Column(name="partnerId", nullable=false)
    private Integer partnerId;
    
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="docId",insertable = false, updatable = false)
    private PPSLMPADocument paDoc;
    
    
    @Column(name="docId", nullable=false)
    private Integer docId;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public PPSLMPartner getPartner() {
        return partner;
    }


    public void setPartner(PPSLMPartner partner) {
        this.partner = partner;
    }


    public Integer getPartnerId() {
        return partnerId;
    }


    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }


    public PPSLMPADocument getPaDoc() {
        return paDoc;
    }


    public void setPaDoc(PPSLMPADocument paDoc) {
        this.paDoc = paDoc;
    }


    public Integer getDocId() {
        return docId;
    }


    public void setDocId(Integer docId) {
        this.docId = docId;
    }
    
}
