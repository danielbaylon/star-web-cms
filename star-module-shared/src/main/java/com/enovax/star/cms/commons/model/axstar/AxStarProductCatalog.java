package com.enovax.star.cms.commons.model.axstar;

/**
 * Created by jennylynsze on 7/19/16.
 */
public class AxStarProductCatalog {
    private Long recordId;
    private Long channelId;
    private String name;
    private String description;
    private String language;
    private String isSnapshotEnabled;
    private String validFrom;
    private String validTo;
    private String createdOn;
    private String modifiedOn;
    private String publishedOn;
    private String image;

    //TODO

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIsSnapshotEnabled() {
        return isSnapshotEnabled;
    }

    public void setIsSnapshotEnabled(String isSnapshotEnabled) {
        this.isSnapshotEnabled = isSnapshotEnabled;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(String publishedOn) {
        this.publishedOn = publishedOn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    //
//    "RecordId": 0,
//            "ChannelId": 0,
//            "Name": "",
//            "Description": "",
//            "Language": "",
//            "IsSnapshotEnabled": false,
//            "ValidFrom": "1900-01-01T00:00:00+00:00",
//            "ValidTo": "2154-12-31T00:00:00+08:00",
//            "CreatedOn": "1901-01-01T00:00:00+00:00",
//            "ModifiedOn": "1901-01-01T00:00:00+00:00",
//            "PublishedOn": "1901-01-01T00:00:00+00:00",
//            "Image": null,
//            "ExtensionProperties": []
}
