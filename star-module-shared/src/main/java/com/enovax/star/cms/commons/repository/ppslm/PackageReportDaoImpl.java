package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.PackageReportModel;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 8/11/16.
 */
@Repository
public class PackageReportDaoImpl implements PackageReportDao {

    @Autowired(required = false)
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    private static final String SQL_PIN_REPORT_MAIN =
            "select {{finalSelect}} " +
                    "from " +
                    "( " +
                    "select partner.orgName as orgName, pkg.pinCode as pinCode, pkg.ticketGeneratedDate as ticketGeneratedDate, " +
                    "pkg.ticketMedia as ticketMedia, pkg.expiryDate as expiryDate, pkg.name as packageName, pkg.description as packageDesc, pkg.qty as pinCodeQty,  " +
                    "pkg.qtyRedeemed as qtyRedeemed, pkg.lastRedemptionDate as lastRedemptionDate, pkg.id as pkgId, " +
                    "stuff((select ', ' + item.displayName  " +
                    "from PPSLMMixMatchPackageItem item  " +
                    "where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as indivItems, " +
                    "stuff((select ', ' + item.receiptNum  " +
                    "from PPSLMMixMatchPackageItem item  " +
                    "where item.packageId = pkg.id " +
                    "for xml path('')), 1, 1, '') as indivReceipts " +
                    "from PPSLMMixMatchPackage pkg " +
                    "inner join PPSLMTAMainAccount acct on pkg.mainAccountId = acct.id " +
                    "inner join PPSLMPartner partner on acct.id = partner.adminId " +
                    "where 1 = 1" +
                    "and pkg.status in ('Available', 'Expired', 'Redeemed') " +
                    "{{filterClause}} " +
                    ") tbl {{orderByClause}} ";

    private static final String SQL_PIN_CLAUSE_ORG_NAME = " and partner.orgName like :orgName ";
    private static final String SQL_PIN_CLAUSE_ACCOUNT_ID = " and acct.id = :taAccountId ";
    private static final String SQL_PIN_CLAUSE_TICKET_MEDIA = " and pkg.ticketMedia = :ticketMedia ";
    private static final String SQL_PIN_CLAUSE_TXN_DT = " and pkg.ticketGeneratedDate >= :fromDate and pkg.ticketGeneratedDate - 1 <= :toDate ";
    private static final String SQL_PIN_CLAUSE_ITEM_NAME = " and pkg.id in (select distinct item2.packageId from PPSLMMixMatchPackageItem item2 where item2.displayName in (:itemNames)) ";
    private static final String SQL_PA_CLAUSE = " and partner.id in (:paIds) ";

    @Override
    @SuppressWarnings("unchecked")
    public ReportResult<PackageReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                           String orderBy, boolean asc, boolean isCount) throws Exception {
        final ReportResult<PackageReportModel> result = new ReportResult<>();
        result.setIsEmpty(true);
        result.setTotalCount(0);

        final Map<String, Object> params = new HashMap<>();
        final StringBuilder invSb = new StringBuilder(SQL_PIN_CLAUSE_TXN_DT);

        //Date range is mandatory
        final Date fromDate = NvxDateUtils.parseDate(filter.getFromDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        final Date toDate = NvxDateUtils.parseDate(filter.getToDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        if (!filter.getIsSysad()) {
            invSb.append(SQL_PIN_CLAUSE_ACCOUNT_ID);

            params.put("taAccountId", filter.getPartnerId());
        }

        if (StringUtils.isNotEmpty(filter.getOrgName())) {
            invSb.append(SQL_PIN_CLAUSE_ORG_NAME);

            params.put("orgName", "%" + filter.getOrgName() + "%");
        }

        if (StringUtils.isNotEmpty(filter.getTicketMedia())) {
            invSb.append(SQL_PIN_CLAUSE_TICKET_MEDIA);

            params.put("ticketMedia", filter.getTicketMedia());
        }

        if (!filter.getItemNames().isEmpty()) {
            invSb.append(SQL_PIN_CLAUSE_ITEM_NAME);
            params.put("itemNames", filter.getItemNames());

        }
        if (filter.getPaIds() != null && filter.getPaIds().length>0) {
            invSb.append(SQL_PA_CLAUSE);
            params.put("paIds", filter.getPaIds() );
        }
        String sqlOrderBy = "";
        if (!isCount && StringUtils.isNotEmpty(orderBy)) {
            switch (orderBy) {
                case "orgName":
                    sqlOrderBy = " order by tbl.orgName "; break;
                case "ticketGeneratedDate":
                    sqlOrderBy = " order by tbl.ticketGeneratedDate "; break;
                case "expiryDate":
                case "expiryDateText":
                    sqlOrderBy = " order by tbl.expiryDate "; break;
                case "lastRedemptionDate":
                case "lastRedemptionDateText":
                    sqlOrderBy = " order by tbl.lastRedemptionDate "; break;
            }

            if (StringUtils.isNotEmpty(sqlOrderBy)) {
                sqlOrderBy += (asc ? " asc " : " desc ");
            }
        }

        final String sql = SQL_PIN_REPORT_MAIN
                .replace("{{finalSelect}}", (isCount ? " count(1) " : " * "))
                .replace("{{filterClause}}", invSb.toString())
                .replace("{{orderByClause}}", sqlOrderBy);

        final SQLQuery qry = prepareSqlQuery(sql, params);

        if (skip != -1) {
            qry.setFirstResult(skip);
            qry.setMaxResults(take);
            qry.setFetchSize(take);
        }

        if (isCount) {
            final Integer count = (Integer)qry.uniqueResult();
            result.setIsEmpty(count == 0);
            result.setTotalCount(count);
            return result;
        }

        qry.addScalar("orgName", StandardBasicTypes.STRING)
                .addScalar("pinCode", StandardBasicTypes.STRING)
                .addScalar("ticketGeneratedDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("expiryDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("packageName", StandardBasicTypes.STRING)
                .addScalar("packageDesc", StandardBasicTypes.STRING)
                .addScalar("pinCodeQty", StandardBasicTypes.INTEGER)
                .addScalar("qtyRedeemed", StandardBasicTypes.INTEGER)
                .addScalar("lastRedemptionDate", StandardBasicTypes.TIMESTAMP)
                .addScalar("pkgId", StandardBasicTypes.INTEGER)
                .addScalar("indivItems", StandardBasicTypes.STRING)
                .addScalar("indivReceipts", StandardBasicTypes.STRING)
                .addScalar("ticketMedia",StandardBasicTypes.STRING)
                .setResultTransformer(Transformers.aliasToBean(PackageReportModel.class));

        final List<PackageReportModel> theList = qry.list();
        for (PackageReportModel row : theList) {
            row.initReportValues();
        }

        result.setIsEmpty(theList.isEmpty());
        result.setRows(theList);

        return result;
    }

    public SQLQuery prepareSqlQuery(String query, Map<String, Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        for (String key : parameters.keySet()) {
            Object para = parameters.get(key);
            if (para instanceof List<?>) {
                queryObject.setParameterList(key, (List<?>)para);
            } else {
                queryObject.setParameter(key, para);
            }
        }
        return queryObject;
    }
}
