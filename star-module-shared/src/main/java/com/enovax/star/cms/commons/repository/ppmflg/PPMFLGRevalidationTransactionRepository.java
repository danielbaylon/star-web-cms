package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lavanya on 16/9/16.
 */
public interface PPMFLGRevalidationTransactionRepository extends JpaRepository<PPMFLGRevalidationTransaction, Integer> {
    PPMFLGRevalidationTransaction findById(Integer id);
    PPMFLGRevalidationTransaction findByReceiptNum(String receiptNum);
}
