package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartDeactivateTicketCriteria;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartDeactivateTicketRequest {

    @JsonProperty("DeactivateTicketCriteria")
    private AxCartDeactivateTicketCriteria axCartDeactivateTicketCriteria;

    public void setAxCartDeactivateTicketCriteria(AxCartDeactivateTicketCriteria axCartDeactivateTicketCriteria) {
        this.axCartDeactivateTicketCriteria = axCartDeactivateTicketCriteria;
    }

    public AxCartDeactivateTicketCriteria getAxCartDeactivateTicketCriteria() {
        return axCartDeactivateTicketCriteria;
    }
}
