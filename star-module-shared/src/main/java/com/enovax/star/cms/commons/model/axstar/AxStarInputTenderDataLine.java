package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;

public class AxStarInputTenderDataLine {

    private BigDecimal amount;
    private String tenderType;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }
}
