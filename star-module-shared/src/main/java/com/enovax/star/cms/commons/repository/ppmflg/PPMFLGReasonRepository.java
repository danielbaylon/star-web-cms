package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGReason;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGReasonRepository extends JpaRepository<PPMFLGReason, Integer> {

    @Query("from PPMFLGReason where status = ?1 ")
    List<PPMFLGReason> findAllReasonByStatus(String code);

    @Query("from PPMFLGReason where id = ?1 ")
    PPMFLGReason findById(Integer id);

    @Query(value="select r from PPMFLGReason r where status = 'A' or status='I'")
    Page getReasonByPage(Pageable pageable);

    @Query(value = "select count(r) from PPMFLGReason r where status = 'A' or status='I'")
    int getReasonSize();
}
