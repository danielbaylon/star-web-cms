package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.List;

/**
 * Created by houtao on 26/7/16.
 */
public class RequestApprovalVM {

    private List<ApprovalLogVM> requests;

    private List<ReasonVM> reasons;

    public List<ApprovalLogVM> getRequests() {
        return requests;
    }

    public void setRequests(List<ApprovalLogVM> requests) {
        this.requests = requests;
    }

    public List<ReasonVM> getReasons() {
        return reasons;
    }

    public void setReasons(List<ReasonVM> reasons) {
        this.reasons = reasons;
    }
}
