package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;

import java.io.Serializable;
import java.util.List;

public class PartnerAccount implements Serializable{

    private Integer id;
    private String username;
    private String password;
    private String salt;

    private String email;
    private String name;

    private String accountCode;

    private String relatedCustomerId;

    private PartnerVM partner;

    private Integer mainAccountId;
    private PPMFLGTAMainAccount mainAccount;

    private boolean isSubAccount;
    private boolean subAccountEnabled;

    private boolean pwdForceChange;

    private List<String> userRights;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelatedCustomerId() {
        return relatedCustomerId;
    }

    public void setRelatedCustomerId(String relatedCustomerId) {
        this.relatedCustomerId = relatedCustomerId;
    }

    public PartnerVM getPartner() {
        return partner;
    }

    public void setPartner(PartnerVM partner) {
        this.partner = partner;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public boolean isSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }

    public boolean isPwdForceChange() {
        return pwdForceChange;
    }

    public void setPwdForceChange(boolean pwdForceChange) {
        this.pwdForceChange = pwdForceChange;
    }

    public boolean isSubAccount() {
        return isSubAccount;
    }

    public void setSubAccount(boolean subAccount) {
        isSubAccount = subAccount;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public PPMFLGTAMainAccount getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(PPMFLGTAMainAccount mainAccount) {
        this.mainAccount = mainAccount;
    }

    public List<String> getUserRights() {
        return userRights;
    }

    public void setUserRights(List<String> userRights) {
        this.userRights = userRights;
    }
}