
package com.enovax.star.cms.commons.ws.axchannel.blockpinredemption;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_GetBlockPINRedemptionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_GetBlockPINRedemptionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="pinRedemptionTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_PINRedemptionTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_GetBlockPINRedemptionResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "pinRedemptionTable"
})
public class SDCGetBlockPINRedemptionResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "pinRedemptionTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCPINRedemptionTable> pinRedemptionTable;

    /**
     * Gets the value of the pinRedemptionTable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCPINRedemptionTable> getPinRedemptionTable() {
        return pinRedemptionTable;
    }

    /**
     * Sets the value of the pinRedemptionTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionTable }{@code >}
     *     
     */
    public void setPinRedemptionTable(JAXBElement<ArrayOfSDCPINRedemptionTable> value) {
        this.pinRedemptionTable = value;
    }

}
