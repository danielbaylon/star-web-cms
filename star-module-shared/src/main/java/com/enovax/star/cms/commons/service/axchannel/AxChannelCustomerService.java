package com.enovax.star.cms.commons.service.axchannel;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.*;
import com.enovax.star.cms.commons.ws.axchannel.customerext.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AxChannelCustomerService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelServiceRegistry axChannelServiceRegistry;

    /**
     * Update customer extension properties for an existing customer in AX.
     * Cannot be used to update standrd AX fields. <br><br>
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: UpdateCreatedCustomer
     */
    public ApiResult<Boolean> apiUpdateCreatedCustomer(StoreApiChannels channel, AxCustomerTable cust, boolean enablePartnerPortal) throws Exception {
        if(cust == null){
            throw new Exception("Customer Details is empty");
        }
        final ObjectFactory factory = new ObjectFactory();
        SDCCustomerUpdateTable c = factory.createSDCCustomerUpdateTable();
        if(cust.getAccountMgrCMS() != null) {
            c.setAccountMgrCMS(factory.createSDCCustomerTableAccountMgrCMS(cust.getAccountMgrCMS()));
        }
        c.setAccountNum(factory.createSDCCustomerTableAccountNum(cust.getAccountNum()));
        c.setAllowOnAccount(cust.getAllowOnAccount());
        if(cust.getContactPersonStr() != null){
            c.setContactPersonStr(factory.createSDCCustomerTableContactPersonStr(cust.getContactPersonStr()));
        }
        if(cust.getDesignation() != null ){
            c.setDesignation(factory.createSDCCustomerTableDesignation(cust.getDesignation()));
        }
        if(cust.getDocumentLinkDelimited() != null){
            c.setDocumentLinkDelimited(factory.createSDCCustomerTableDocumentLinkDelimited(cust.getDocumentLinkDelimited()));
        }
        ArrayOfSDCDocumentLink arrayOfSDCDocumentLink = factory.createArrayOfSDCDocumentLink();
        List<SDCDocumentLink> sdcDocumentLink = arrayOfSDCDocumentLink.getSDCDocumentLink();
        if(cust.getDocumentLinks() != null && cust.getDocumentLinks().size() > 0){
            List<AxDocumentLink> docLinks = cust.getDocumentLinks();
            for(AxDocumentLink i : docLinks){
                SDCDocumentLink docLink = factory.createSDCDocumentLink();
                docLink.setDocumentName(factory.createSDCDocumentLinkDocumentName(i.getDocumentName()));
                docLink.setLink(factory.createSDCDocumentLinkLink(i.getLink()));
                sdcDocumentLink.add(docLink);
            }
        }
        c.setDocumentLinkLines(factory.createArrayOfSDCDocumentLink(arrayOfSDCDocumentLink));
        if(cust.getFax() != null){
            c.setFax(factory.createSDCCustomerTableFax(cust.getFax()));
        }
        if(cust.getFaxExtension() != null ){
            c.setFaxExtension(factory.createSDCCustomerTableFaxExtension(cust.getFaxExtension()));
        }else{
            c.setFaxExtension(factory.createSDCCustomerTableFaxExtension(""));
        }
        if(cust.getMarketDistributionDelimited() != null ){
            c.setMarketDistributionDelimited(factory.createSDCCustomerTableMarketDistributionDelimited(cust.getMarketDistributionDelimited()));
        }
        ArrayOfSDCMarketDistribution arrayOfSDCMarketDistribution = factory.createArrayOfSDCMarketDistribution();
        List<SDCMarketDistribution> sdcMarketDistribution = new ArrayList<SDCMarketDistribution>();
        if(cust.getMarketDistributions() != null){
            List<AxMarketDistribution> list = cust.getMarketDistributions();
            for(AxMarketDistribution i : list){
                SDCMarketDistribution d = factory.createSDCMarketDistribution();
                d.setCountryRegion(factory.createSDCMarketDistributionCountryRegion(i.getCountryRegion()));
                d.setPercent(factory.createSDCMarketDistributionPercent(i.getPercent()));
                d.setSalesManager(factory.createSDCMarketDistributionSalesManager(i.getSalesManager()));
                sdcMarketDistribution.add(d);
            }
        }
        c.setMarketDistributionLines(factory.createArrayOfSDCMarketDistribution(arrayOfSDCMarketDistribution));
        if(cust.getRegisteredDate() != null){
            c.setRegisteredDate(convertToXMLGregorianCalendar(cust.getRegisteredDate()));
        }
        c.setReservationAmendment(cust.getReservationAmendment());
        if(cust.getUenNumber() != null){
            c.setUENNumber(factory.createSDCCustomerTableUENNumber(cust.getUenNumber()));
        }
        if(cust.getTaExpiryDate() != null){
            c.setTAExpiryDate(convertToXMLGregorianCalendar(cust.getTaExpiryDate()));
        }
        if(cust.getTaLicenseNumber() != null){
            c.setTALicenseNumber(factory.createSDCCustomerTableTALicenseNumber(cust.getTaLicenseNumber()));
        }else{
            c.setTALicenseNumber(factory.createSDCCustomerTableTALicenseNumber(cust.getTaLicenseNumber()));
        }
        if(cust.getBranchName() != null){
            c.setBranchName(factory.createSDCCustomerTableBranchName(cust.getBranchName()));
        }
        if(cust.getCapacityGroupId() != null){
            c.setCapacityGroupId(factory.createSDCCustomerTableCapacityGroupId(cust.getCapacityGroupId()));
        }
        if(cust.getLineOfBusiness() != null && cust.getLineOfBusiness().trim().length() > 0){
            c.setLineOfBusiness(factory.createSDCCustomerUpdateTableLineOfBusiness(cust.getLineOfBusiness()));
        }
        if(cust.getCountriesInterestedIn() != null){
            c.setCountriesInterestedIn(factory.createSDCCustomerTableCountriesInterestedIn(cust.getCountriesInterestedIn()));
        }
        if(cust.getDailyTransCapValue() != null){
            c.setDailyTransCapValue(cust.getDailyTransCapValue());
        }
        c.setEnablePartnerPortal(enablePartnerPortal ?  1 : 0);
        if(cust.getLanguageId() != null){
            c.setLanguageId(factory.createSDCCustomerTableLanguageId(cust.getLanguageId()));
        }
        if(cust.getMainAccLoginName() != null){
            c.setMainAccLoginName(factory.createSDCCustomerTableMainAccLoginName(cust.getMainAccLoginName()));
        }
        if(cust.getOfficeNo() != null){
            c.setOfficeNo(factory.createSDCCustomerTableOfficeNo(cust.getOfficeNo()));
        }
        if(cust.getRevalidationFee() != null){
            c.setRevalidationFee(factory.createSDCCustomerTableRevalidationFee(cust.getRevalidationFee().toString()));
        }
        if(cust.getRevalidationPeriod() != null){
            c.setRevalidationPeriod(cust.getRevalidationPeriod());
        }
        if(cust.getCountryCode() != null){
            c.setCountryRegion(factory.createSDCCustomerUpdateTableCountryRegion(cust.getCountryCode()));
        }

        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCCustomerService custSrv = stub.getStubCustomerExt();

        SDCUpdateCreatedCustomerResponse response = custSrv.updateCreatedCustomer(c);
        boolean success = false;
        if(response != null && response.getUpdateCustomer() != null && response.getUpdateCustomer().getValue() != null && (
                response.getUpdateCustomer().getValue().contains(cust.getAccountNum())
                &&
                response.getUpdateCustomer().getValue().contains("successfully")
        )){
            success = true;
        }
        ApiResult<Boolean> api = new ApiResult<>();
        api.setSuccess(success);
        if(!success){
            if(response != null && response.getErrors() != null){
                api = processErrors(response, api);
            }
        }
        return api;
    }
    private XMLGregorianCalendar convertToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED
        );
    }

    /**
     * Get customer extension properties for an existing customer in AX.
     * Cannot be used to get standard AX fields. <br><br>
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetCreatedCustomer
     */
    public ApiResult<AxCustomerTable> apiGetCreatedCustomer(StoreApiChannels channel, String axAccountNumber) throws AxChannelException {
        ApiResult<AxCustomerTable> apiResult = new ApiResult<AxCustomerTable>();
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCCustomerService custSrv = stub.getStubCustomerExt();
        SDCGetCreatedCustomerResponse response = custSrv.getCreatedCustomer(axAccountNumber);
        if(response == null){
            return null;
        }
        JAXBElement<ArrayOfSDCCustomerTable> custArrXml = response.getCustomerEntity();
        if(custArrXml == null){
            apiResult = this.processErrors(response, apiResult);
            apiResult.setSuccess(false);
            return apiResult;
        }
        ArrayOfSDCCustomerTable custArr = custArrXml.getValue();
        if(custArr == null){
            apiResult = this.processErrors(response, apiResult);
            apiResult.setSuccess(false);
            return apiResult;
        }
        List<SDCCustomerTable> custList = custArr.getSDCCustomerTable();
        if(custList == null){
            apiResult = this.processErrors(response, apiResult);
            apiResult.setSuccess(false);
            return apiResult;
        }
        SDCCustomerTable cust = custList.get(0);
        if(cust == null){
            apiResult = this.processErrors(response, apiResult);
            apiResult.setSuccess(false);
            return apiResult;
        }
        AxCustomerTable c = new AxCustomerTable();
        c.setAccountMgrCMS(cust.getAccountMgrCMS() != null ? cust.getAccountMgrCMS().getValue() : null);
        c.setAllowOnAccount(cust.getAllowOnAccount());
        c.setContactPersonStr(cust.getContactPersonStr() != null ? cust.getContactPersonStr().getValue() : null);
        c.setDesignation(cust.getDesignation() != null ? cust.getDesignation().getValue() : null);
        c.setFax(cust.getFax() != null ? cust.getFax().getValue() : null);
        c.setRegisteredDate(cust.getRegisteredDate() != null ? cust.getRegisteredDate().toGregorianCalendar().getTime() : null);
        c.setUenNumber(cust.getUENNumber() != null ? cust.getUENNumber().getValue() : null);
        c.setTaExpiryDate(cust.getTAExpiryDate() != null ? cust.getTAExpiryDate().toGregorianCalendar().getTime() : null);
        c.setTaLicenseNumber(cust.getTALicenseNumber() != null ? cust.getTALicenseNumber().getValue() : null);
        c.setBranchName(cust.getBranchName() != null ? cust.getBranchName().getValue() : null);
        c.setCapacityGroupId(cust.getCapacityGroupId() != null ?cust.getCapacityGroupId().getValue() : null);
        c.setCountriesInterestedIn(cust.getCountriesInterestedIn() != null ? cust.getCountriesInterestedIn().getValue() : null);
        c.setDailyTransCapValue(cust.getDailyTransCapValue());
        c.setEnablePartnerPortal(cust.getEnablePartnerPortal());
        c.setMainAccLoginName(cust.getMainAccLoginName() != null ? cust.getMainAccLoginName().getValue() : null);
        c.setOfficeNo(cust.getOfficeNo() != null ? cust.getOfficeNo().getValue() : null);
        c.setRevalidationFee(cust.getRevalidationFee() != null && cust.getRevalidationFee().getValue() != null ? cust.getRevalidationFee().getValue().trim() : null);
        c.setRevalidationPeriod(cust.getRevalidationPeriod() != null ? cust.getRevalidationPeriod() : null);
        c.setLineOfBusiness(cust.getLineOfBusiness() != null && cust.getLineOfBusiness().getValue() != null ? cust.getLineOfBusiness().getValue().trim() : null);
        c.setCountryCode(cust.getCountryRegion() != null && cust.getCountryRegion().getValue() != null ? cust.getCountryRegion().getValue().trim() : null);
        List<AxMarketDistribution> axMDList = new ArrayList<AxMarketDistribution>();
        JAXBElement<ArrayOfSDCMarketDistribution> mdxml = cust.getMarketDistributionLines();
        if(mdxml != null){
            ArrayOfSDCMarketDistribution amd = mdxml.getValue();
            if(amd != null){
                List<SDCMarketDistribution> mds = amd.getSDCMarketDistribution();
                if(mds != null && mds.size() > 0){
                    for(SDCMarketDistribution md : mds){
                        AxMarketDistribution t = new AxMarketDistribution();
                        t.setCountryRegion(md.getCountryRegion() != null ? md.getCountryRegion().getValue() : null);
                        t.setPercent(md.getPercent() != null ? md.getPercent().getValue() : null);
                        t.setSalesManager(md.getSalesManager() != null ? md.getSalesManager().getValue() : null);
                        axMDList.add(t);
                    }
                }
            }
        }
        c.setMarketDistributions(axMDList);
        apiResult.setData(c);
        apiResult.setSuccess(true);
        return apiResult;
    }

    public void apiViewCustomerTransactions() {
        //TODO Implement
    }


    /**
     * GetList of Line of business
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetLineOfBusiness
     */
    public ApiResult<List<AxLineOfBusiness>> getAxLineOfBusinessList(StoreApiChannels channel){
        List<AxLineOfBusiness> list = new ArrayList<AxLineOfBusiness>();
        ApiResult<List<AxLineOfBusiness>> api = new ApiResult<List<AxLineOfBusiness>>();
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCCustomerService custSrv = stub.getStubCustomerExt();
            SDCLineOfBusinessResponse response = custSrv.getLineOfBusiness();
            if(response != null){
                JAXBElement<ArrayOfSDCLineOfBusiness> xmlArray = response.getLineOfBusinessEntity();
                if(xmlArray != null){
                    ArrayOfSDCLineOfBusiness array = xmlArray.getValue();
                    if(array != null){
                        List<SDCLineOfBusiness> lblist = array.getSDCLineOfBusiness();
                        if(lblist != null){
                            for(SDCLineOfBusiness i : lblist){
                                AxLineOfBusiness alb = new AxLineOfBusiness();
                                alb.setDescription(i.getDescription() != null ? i.getDescription().getValue() : null);
                                alb.setLineOfBusinessId(i.getLineOfBusinessId() != null ? i.getLineOfBusinessId().getValue() : null);
                                list.add(alb);
                            }
                        }
                    }
                }
            }
            api = processResponse(response, api, list);
        }catch (Exception ex){
            api.setSuccess(false);
            ex.getMessage();
        }
        api.setData(list);
        return api;
    }

    /**
     * Get List of Price Group
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetCapacityGroup
     */
    public ApiResult<List<AxCapacityGroup>> getAxCapacityGroupList(StoreApiChannels channel){
        List<AxCapacityGroup> list = new ArrayList<AxCapacityGroup>();
        ApiResult<List<AxCapacityGroup>> api = new ApiResult<List<AxCapacityGroup>>();
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCCustomerService custSrv = stub.getStubCustomerExt();
            SDCGetCapacityGrpResponse response = custSrv.getCapacityGroup();
            if(response != null){
                JAXBElement<ArrayOfSDCGetCapacityGroupTable> xmlArray = response.getGetCapacityGroup();
                if(xmlArray != null){
                    ArrayOfSDCGetCapacityGroupTable array = xmlArray.getValue();
                    if(array != null){
                        List<SDCGetCapacityGroupTable> lblist = array.getSDCGetCapacityGroupTable();
                        if(lblist != null){
                            for(SDCGetCapacityGroupTable i : lblist){
                                AxCapacityGroup pg = new AxCapacityGroup();
                                //TODO ok not really a todo but basically NEC change their variable name and I dont want to change coz a lot of things will need to change and I'm too lazy do do it
                                pg.setEventAllocationGroupID(i.getCapacityGroupId() == null ? null : i.getCapacityGroupId().getValue());
                                pg.setName(i.getDescription() == null ? null : i.getDescription().getValue());
                                list.add(pg);
                            }
                        }
                    }
                }
            }
            api = processResponse(response, api, list);
        }catch (Exception ex){
            api.setSuccess(false);
            ex.getMessage();
        }
        api.setData(list);
        return api;
    }

    /**
     * Get List of Price Group
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetPriceGroup
     */
    public ApiResult<List<AxPriceGroup>> getAxPriceGroupList(StoreApiChannels channel){
        List<AxPriceGroup> list = new ArrayList<AxPriceGroup>();
        ApiResult<List<AxPriceGroup>> api = new ApiResult<List<AxPriceGroup>>();
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCCustomerService custSrv = stub.getStubCustomerExt();
            SDCGetPriceGroupResponse response = custSrv.getPriceGroup();
            if(response != null){
                JAXBElement<ArrayOfSDCPriceGroup> xmlArray = response.getPriceGroupEntity();
                if(xmlArray != null){
                    ArrayOfSDCPriceGroup array = xmlArray.getValue();
                    if(array != null){
                        List<SDCPriceGroup> lblist = array.getSDCPriceGroup();
                        if(lblist != null){
                            for(SDCPriceGroup i : lblist){
                                AxPriceGroup pg = new AxPriceGroup();
                                pg.setB2B(i.getIsB2B() != null ? i.getIsB2B().intValue() == 1 : false);
                                pg.setName(i.getName() != null ? i.getName().getValue() : null);
                                pg.setPriceGroupId(i.getPriceGroupId() != null ? i.getPriceGroupId().getValue() : null);
                                list.add(pg);
                            }
                        }
                    }
                }
            }
            api = processResponse(response, api, list);
        }catch (Exception ex){
            api.setSuccess(false);
            ex.getMessage();
        }
        api.setData(list);
        return api;
    }

    private ApiResult processResponse(ServiceResponse response, ApiResult api, List list) {
        if(list.size() == 0){
            if(response == null){
                api.setSuccess(false);
            }else{
                api = processErrors(response, api);
            }
        }else{
            api.setSuccess(true);
        }
        return api;
    }

    private ApiResult processErrors(ServiceResponse response, ApiResult api){
        JAXBElement<ArrayOfResponseError> eleError = response.getErrors();
        if(eleError != null){
            ArrayOfResponseError errorArray = eleError.getValue();
            if(errorArray != null){
                List<ResponseError> errors = errorArray.getResponseError();
                if(errors != null){
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for(ResponseError re : errors){
                        if(re != null){
                            if(re.getErrorCode() != null && re.getErrorCode().getValue() != null){
                                if(sbErrCodes.length() == 0){
                                    sbErrCodes.append(" | ");
                                }
                                sbErrCodes.append(re.getErrorCode().getValue());
                            }
                            if(re.getErrorMessage() != null && re.getErrorMessage().getValue() != null){
                                if(sbErrMessages.length() == 0){
                                    sbErrMessages.append(" | ");
                                }
                                sbErrMessages.append(re.getErrorMessage().getValue());
                            }
                        }
                    }
                    if(sbErrCodes.length() != 0 || sbErrMessages.length() != 0){
                        api.setErrorCode(sbErrCodes.toString());
                        api.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return api;
    }

    /**
     * GetList of Line of business
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetRegion
     */
    public ApiResult<List<AxRegion>> getRegion(StoreApiChannels channel){
        List<AxRegion> list = new ArrayList<AxRegion>();
        ApiResult<List<AxRegion>> api = new ApiResult<List<AxRegion>>();
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCCustomerService custSrv = stub.getStubCustomerExt();
            SDCRegionResponse response = custSrv.getRegion();
            if(response != null){
                JAXBElement<ArrayOfSDCRegionTable> xmlArray = response.getGetRegion();
                if(xmlArray != null){
                    ArrayOfSDCRegionTable array = xmlArray.getValue();
                    if(array != null){
                        List<SDCRegionTable> lblist = array.getSDCRegionTable();
                        if(lblist != null){
                            for(SDCRegionTable i : lblist){
                                AxRegion region = new AxRegion();
                                region.setDescription(i.getDescription() != null ? i.getDescription().getValue() : null);
                                region.setRegionId(i.getRegionId() != null ? i.getRegionId().getValue() : null);
                                list.add(region);
                            }
                        }
                    }
                }
            }
            api = processResponse(response, api, list);
        }catch (Exception ex){
            api.setSuccess(false);
            ex.getMessage();
        }
        api.setData(list);
        return api;
    }


    /**
     * GetList of Line of business
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetCountryRegion
     */
    public ApiResult<List<AxCountryRegion>> getCountryRegion(StoreApiChannels channel){
        List<AxCountryRegion> list = new ArrayList<AxCountryRegion>();
        ApiResult<List<AxCountryRegion>> api = new ApiResult<List<AxCountryRegion>>();
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCCustomerService custSrv = stub.getStubCustomerExt();
            SDCCountryRegionResponse response = custSrv.getCountryRegion();
            if(response != null){
                JAXBElement<ArrayOfSDCCountryRegionTable> xmlArray = response.getGetCountryRegion();
                if(xmlArray != null){
                    ArrayOfSDCCountryRegionTable array = xmlArray.getValue();
                    if(array != null){
                        List<SDCCountryRegionTable> lblist = array.getSDCCountryRegionTable();
                        if(lblist != null){
                            for(SDCCountryRegionTable i : lblist){
                                AxCountryRegion country = new AxCountryRegion();
                                country.setCountryId(i.getCountryId() != null ? i.getCountryId().getValue() : null);
                                country.setCountryName(i.getCountryName() !=null ? i.getCountryName().getValue() : null);
                                country.setRegionId(i.getRegionId() != null ? i.getRegionId().getValue() : null);
                                list.add(country);
                            }
                        }
                    }
                }
            }
            api = processResponse(response, api, list);
        }catch (Exception ex){
            api.setSuccess(false);
            ex.getMessage();
        }
        api.setData(list);
        return api;
    }
}
