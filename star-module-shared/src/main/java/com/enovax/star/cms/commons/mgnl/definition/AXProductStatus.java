package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/6/16.
 */
public enum AXProductStatus {
    Active,
    Inactive
}
