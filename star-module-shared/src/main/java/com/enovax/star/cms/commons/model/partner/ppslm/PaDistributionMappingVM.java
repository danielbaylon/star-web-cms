package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPaDistributionMapping;

public class PaDistributionMappingVM {
    private Integer id;
    private Integer percentage;
    private String countryId;
    private String adminId;
    private Integer partnerId;

    private String adminNm;
    private String countryNm;

    public PaDistributionMappingVM(){
    }
    
    public PaDistributionMappingVM(PPSLMPaDistributionMapping pdMap){
        this.id = pdMap.getId();
        this.percentage = pdMap.getPercentage();
        this.countryId = pdMap.getCountryId();
        this.adminId = pdMap.getAdminId();
        this.partnerId = pdMap.getPartnerId();
        //TODO this.countryNm = "";
        //TODO this.adminNm = pdMap.getAdmin().getUsername();
    }

    public PaDistributionMappingVM(PaDistributionMappingHist tmpObj) {
        this.percentage = tmpObj.getPercentage();
        this.countryId = tmpObj.getCountryId();
        this.adminId = tmpObj.getAdminId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public String getCountryNm() {
        return countryNm;
    }

    public void setCountryNm(String countryNm) {
        this.countryNm = countryNm;
    }

    public String getAdminNm() {
        return adminNm;
    }

    public void setAdminNm(String adminNm) {
        this.adminNm = adminNm;
    }
}
