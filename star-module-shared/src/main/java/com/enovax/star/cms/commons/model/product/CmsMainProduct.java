package com.enovax.star.cms.commons.model.product;

/*
    This one is used for Partner Portal so far
 */
public class CmsMainProduct {
    private String id;
    private String name;
    private String description;
    private boolean isExclusiveDeal;
    private String prodType;
    private String prodLevel;
    private Long minQty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isExclusiveDeal() {
        return isExclusiveDeal;
    }

    public void setIsExclusiveDeal(boolean isExclusiveDeal) {
        this.isExclusiveDeal = isExclusiveDeal;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public String getProdLevel() {
        return prodLevel;
    }

    public void setProdLevel(String prodLevel) {
        this.prodLevel = prodLevel;
    }

    public Long getMinQty() {
        return minQty;
    }

    public void setMinQty(Long minQty) {
        this.minQty = minQty;
    }
}
