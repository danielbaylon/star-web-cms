package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by houtao on 19/8/16.
 */
public class PagedData<T> {

    private int size;
    private T data;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
