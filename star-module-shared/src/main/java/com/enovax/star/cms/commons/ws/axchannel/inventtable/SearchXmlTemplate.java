
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TemplateXMLCriteria" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_TemplateXMLCriteria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "templateXMLCriteria"
})
@XmlRootElement(name = "SearchXmlTemplate", namespace = "http://tempuri.org/")
public class SearchXmlTemplate {

    @XmlElementRef(name = "TemplateXMLCriteria", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCTemplateXMLCriteria> templateXMLCriteria;

    /**
     * Gets the value of the templateXMLCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCTemplateXMLCriteria }{@code >}
     *     
     */
    public JAXBElement<SDCTemplateXMLCriteria> getTemplateXMLCriteria() {
        return templateXMLCriteria;
    }

    /**
     * Sets the value of the templateXMLCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCTemplateXMLCriteria }{@code >}
     *     
     */
    public void setTemplateXMLCriteria(JAXBElement<SDCTemplateXMLCriteria> value) {
        this.templateXMLCriteria = value;
    }

}
