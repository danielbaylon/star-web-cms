package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PPSLMTASubAccount")
public class PPSLMTASubAccount extends PPSLMTAAccount implements Serializable {

    @ManyToOne
    @JoinColumn(name = "mainAccountId", nullable = false)
    private PPSLMTAMainAccount mainUser;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", targetEntity = PPSLMTAPrevPasswordSub.class)
    private List<PPSLMPrevPassword> prevPws = new ArrayList<PPSLMPrevPassword>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "subUser")
    private List<PPSLMTARightsMapping> rightsMapping = new ArrayList<PPSLMTARightsMapping>();

    @Transient
    private List<String> rightsMappingId;

    @Column(name = "title", nullable = false)
    private String title;

    public PPSLMTAMainAccount getTAMainAccount() {
        return mainUser;
    }

    public void setTAMainAccount(PPSLMTAMainAccount user) {
        this.mainUser = user;
    }

    public List<PPSLMPrevPassword> getPrevPws() {
        return prevPws;
    }

    public void setPrevPws(List<PPSLMPrevPassword> prevPws) {
        this.prevPws = prevPws;
    }

    public PPSLMTAMainAccount getMainUser() {
        return mainUser;
    }

    public void setMainUser(PPSLMTAMainAccount mainUser) {
        this.mainUser = mainUser;
    }

    public List<PPSLMTARightsMapping> getRightsMapping() {
        return rightsMapping;
    }

    public void setRightsMapping(List<PPSLMTARightsMapping> rightsMapping) {
        this.rightsMapping = rightsMapping;
    }

    public List<String> getRightsMappingId() {
        return rightsMappingId;
    }

    public void setRightsMappingId(List<String> rightsMappingId) {
        this.rightsMappingId = rightsMappingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public boolean hasPermission(Integer rightsId) {
        for (PPSLMTARightsMapping rightmap : getRightsMapping()) {
            if (rightsId != null
                    && rightsId.equals(rightmap.getAccessRightsGroup().getId())) {
                return true;
            }
        }
        return false;
    }

}
