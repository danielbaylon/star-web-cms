package com.enovax.star.cms.commons.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.SelectFieldDefinition;

/**
 * Created by jennylynsze on 3/23/16.
 */
public class MultiLanguageSelectFieldDefinition extends SelectFieldDefinition {
    private String valuePropertyDefault;
    private String labelPropertyDefault;

    public String getValuePropertyDefault() {
        return valuePropertyDefault;
    }

    public void setValuePropertyDefault(String valuePropertyDefault) {
        this.valuePropertyDefault = valuePropertyDefault;
    }

    public String getLabelPropertyDefault() {
        return labelPropertyDefault;
    }

    public void setLabelPropertyDefault(String labelPropertyDefault) {
        this.labelPropertyDefault = labelPropertyDefault;
    }
}
