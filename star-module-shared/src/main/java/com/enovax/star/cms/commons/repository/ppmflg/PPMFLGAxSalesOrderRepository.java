package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGAxSalesOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/17/16.
 */
@Repository
public interface PPMFLGAxSalesOrderRepository extends JpaRepository<PPMFLGAxSalesOrder, Integer>{
    List<PPMFLGAxSalesOrder> findByTransactionId(Integer transactionId);
}
