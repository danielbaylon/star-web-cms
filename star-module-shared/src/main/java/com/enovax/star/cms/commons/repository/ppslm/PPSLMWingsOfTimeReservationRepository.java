package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PPSLMWingsOfTimeReservationRepository extends JpaRepository<PPSLMWingsOfTimeReservation, Integer>, JpaSpecificationExecutor<PPSLMWingsOfTimeReservation> {

    PPSLMWingsOfTimeReservation findById(int id);

    @Query("select id from PPSLMWingsOfTimeReservation where ( (qtyUnredeemed > 0) or ((qty - qtyRedeemed) > 0) ) and receiptNum is not null and pinCode is not null and (status = 'Confirmed' or status = 'PartiallyRedeemed') and mainAccountId = ?1 ")
    List<Integer> findAllActiveUnRedeemdedTicketsByMainAccountId(Integer mainAccId);

    @Query("select u.id from PPSLMWingsOfTimeReservation u where u.qtyUnredeemed > 0 and u.receiptNum is not null and u.pinCode is not null and (u.status = 'Confirmed' or u.status = 'PartiallyRedeemed')")
    List<Integer> findAllActiveUnRedeemdedTickets();

    @Query("select u.id from PPSLMWingsOfTimeReservation u where u.qtyRedeemed = 0 and u.qty > 0 and u.eventDate is not null and u.eventDate <= ?1 and u.isAdminRequest = false and u.eventStartTime is not null and u.receiptNum is not null and u.pinCode is not null and u.status = 'Confirmed' and u.cancelNotifiedDate is null ")
    List<Integer> findAllActiveUnRedeemdedReservations(Date eventStartDate);

    @Query("select count(u) from PPSLMWingsOfTimeReservation u where u.purchaseSrcId is not null and u.purchaseSrcId = ?1 and (u.status = 'Confirmed' or u.status = 'PartiallyRedeemed' or u.status = 'FullyRedeemed') and u.purchaseSrcQty is not null ")
    Long countReservationPurchasedTimes(Integer id);

    @Query("select sum(u.qty) from PPSLMWingsOfTimeReservation u where u.isAdminRequest = false and u.purchaseSrcId is null and u.purchaseSrcQty is null and (u.status = 'Confirmed' or u.status = 'PartiallyRedeemed' or u.status = 'FullyRedeemed') and u.mainAccountId =?1 and u.productId = ?2 and u.itemId = ?3 and u.eventGroupId = ?4 and u.eventLineId = ?5 and u.eventDate = ?6 ")
    Long sumPartnerWoTTicketPurchased(Integer mainAccId, String productId, String itemId, String eventGroupId, String eventLineId, Date date);

    @Query("select u.id from PPSLMWingsOfTimeReservation u where u.isAdminRequest = true and (u.status = 'Reserved' or u.status = 'PartiallyPurchased') and u.qty > u.qtySold and u.releaseNotifiedDate is null and u.releasedDate is null and u.eventDate is not null ")
    List<Integer> findAllUnconfirmedBackendReservationWithNonNotifyDate();

    @Query("select u.id from PPSLMWingsOfTimeReservation u where u.isAdminRequest = true and (u.status = 'Reserved' or u.status = 'PartiallyPurchased') and u.qty > u.qtySold and u.releaseNotifiedDate is not null and u.releasedDate is null and u.eventDate is not null ")
    List<Integer> findAllUnconfirmedBackendReservationWithNotifyDate();

    @Query("select u.id from PPSLMWingsOfTimeReservation u where u.isAdminRequest = false and ( u.axChargeVoucher is null ) and (u.status = 'Confirmed' or u.status = 'Cancelled' or u.status = 'FullyRedeemed' or u.status = 'PartiallyRedeemed') and u.eventDate is not null and u.mainAccountId = ?1 and u.eventDate >= ?2 and u.eventDate <= ?3 ")
    List<Integer> findAllPendingInvoicedWithDateRange(Integer mainAccountId, Date startDateTime, Date endDateTime);

    @Query("select count(u) from PPSLMWingsOfTimeReservation u where u.isAdminRequest = false and ( u.axChargeVoucher is not null ) and (u.status = 'Confirmed' or u.status = 'Cancelled' or u.status = 'FullyRedeemed' or u.status = 'PartiallyRedeemed') and u.eventDate is not null and u.mainAccountId = ?1 and u.eventDate >= ?2 and u.eventDate <= ?3 ")
    Long findTotalInvoicedTransactionWithDateRange(Integer mainAccountId, Date startDateTime, Date endDateTime);

    @Query("select u from PPSLMWingsOfTimeReservation u where u.isAdminRequest = false and ( u.axChargeVoucher is not null ) and (u.status = 'Confirmed' or u.status = 'Cancelled' or u.status = 'FullyRedeemed' or u.status = 'PartiallyRedeemed') and u.eventDate is not null and u.mainAccountId = ?1 and u.eventDate >= ?2 and u.eventDate <= ?3 ")
    List<PPSLMWingsOfTimeReservation> findAllInvoicedTransactionWithDateRange(Integer mainAccountId, Date startDateTime, Date endDateTime);


}