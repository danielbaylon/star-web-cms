package com.enovax.star.cms.commons.model.kiosk.odata;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.enovax.star.cms.commons.util.NvxDateUtils;

/**
 * SDC_NonBindableCRTExtension.Entity.EventAllocationEntity
 * 
 * @author Justin
 *
 */
public class AxEventAllocationEntity {

    private String eventGroupId;
    private String eventLineId;
    private String eventDate;
    private List<AxCommerceProperty> extensionProperties;

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventDate() {
        return eventDate;
    }
    
    public Date getEventDate(String format) {
        Date date = null;
        try {
            date = NvxDateUtils.parseDate(this.eventDate, format);
        } catch (ParseException e) {
        }
        return date;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public List<AxCommerceProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxCommerceProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}
