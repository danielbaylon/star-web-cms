package com.enovax.star.cms.commons.datamodel.ppslm;

import java.util.List;

/**
 * Created by lavanya on 22/9/16.
 */
public class PartnerExclProdMapping {
    Integer partnerId;
    List<String> exclProdIds;

    public PartnerExclProdMapping() {
    }

    public PartnerExclProdMapping(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public List<String> getExclProdIds() {
        return exclProdIds;
    }

    public void setExclProdIds(List<String> exclProdIds) {
        this.exclProdIds = exclProdIds;
    }

}
