package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 7/11/16.
 */
public class ReportFilterVM {
    private boolean isSysad;
    private Integer partnerId;
    private String orgName;
    private String fromDate;
    private String toDate;
    private String ticketMedia;
    private String receiptNum;
    private String status;
    private TransMegaStatus transMegaStatus;
    private List<String> itemNames = new ArrayList<>();
    private String revalItem = "";

    private String paymentType;

    private Integer[] paIds;

    public boolean getIsSysad() {
        return isSysad;
    }
    public void setIsSysad(boolean isSysad) {
        this.isSysad = isSysad;
    }
    public Integer getPartnerId() {
        return partnerId;
    }
    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }
    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
    public String getFromDate() {
        return fromDate;
    }
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
    public String getToDate() {
        return toDate;
    }
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public TransMegaStatus getTransMegaStatus() {
        return transMegaStatus;
    }
    public void setTransMegaStatus(TransMegaStatus transMegaStatus) {
        this.transMegaStatus = transMegaStatus;
    }
    public List<String> getItemNames() {
        return itemNames;
    }
    public void setItemNames(List<String> itemNames) {
        this.itemNames = itemNames;
    }
    public String getRevalItem() {
        return revalItem;
    }
    public void setRevalItem(String revalItem) {
        this.revalItem = revalItem;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Integer[] getPaIds() {
        return paIds;
    }
    public void setPaIds(Integer[] paIds) {
        this.paIds = paIds;
    }

    public String getTicketMedia() {
        return ticketMedia;
    }

    public void setTicketMedia(String ticketMedia) {
        this.ticketMedia = ticketMedia;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
