package com.enovax.star.cms.commons.model.booking.skydining;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCartMainCourse {
  private String menuId;
  private String menuName;
  private int qty;

  public String getMenuId() {
    return menuId;
  }

  public void setMenuId(String menuId) {
    this.menuId = menuId;
  }

  public String getMenuName() {
    return menuName;
  }

  public void setMenuName(String menuName) {
    this.menuName = menuName;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }
}
