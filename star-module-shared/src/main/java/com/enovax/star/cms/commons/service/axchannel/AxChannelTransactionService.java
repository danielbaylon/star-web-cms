package com.enovax.star.cms.commons.service.axchannel;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailGeneratePinInput;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.AxRetailPinTable;
import com.enovax.star.cms.commons.model.axchannel.b2bretailticket.transformer.AxB2BRetailResponseTransformer;
import com.enovax.star.cms.commons.model.axchannel.b2bupdatepinstatus.AxB2BUpdatePinStatus;
import com.enovax.star.cms.commons.model.axchannel.b2bupdatepinstatus.transformer.AxB2BUpdatePinStatusResponseTransformer;
import com.enovax.star.cms.commons.model.axchannel.discountcounter.AxDiscountCounter;
import com.enovax.star.cms.commons.model.axchannel.discountcounter.transformer.AxDiscountCounterTransformer;
import com.enovax.star.cms.commons.model.axchannel.penaltycharge.AxPenaltyCharge;
import com.enovax.star.cms.commons.model.axchannel.penaltycharge.transformer.AxPenaltyChargeTransformer;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTOrder;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTPinUpdate;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTPinViewTable;
import com.enovax.star.cms.commons.model.axchannel.pin.AxWOTSalesOrder;
import com.enovax.star.cms.commons.model.axchannel.pin.transformer.AxPinTransformer;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxInsertOnlineReference;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.model.axchannel.retailticket.transformer.AxInsertOnlineReferenceResponseTransformer;
import com.enovax.star.cms.commons.model.axchannel.retailticket.transformer.AxRetailResponseTransformer;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.*;
import com.enovax.star.cms.commons.ws.axchannel.pin.*;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.ArrayOfSDCCartTicketListCriteria;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.*;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.ObjectFactory;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartTicketListCriteria;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@SuppressWarnings({"UnnecessaryLocalVariable", "Duplicates"})
@Service
public class AxChannelTransactionService {

    //TODO More logging
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelServiceRegistry axChannelServiceRegistry;

    public void setAxChannelServiceRegistry(AxChannelServiceRegistry axChannelServiceRegistry) {
        this.axChannelServiceRegistry = axChannelServiceRegistry;
    }

    /**
     * For a list of items, validate barcode stock or event capacity. <br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: CartValidateQuantity
     */
    public ApiResult<List<AxRetailTicketRecord>> apiCartValidateQuantity(
            StoreApiChannels channel, String transactionId, List<AxRetailCartTicket> cartTickets) throws AxChannelException {
        log.info("[AX Channel Service] apiCartValidateQuantity");
//        log.info("[Request] channel=" + channel + ", transactionId=" + transactionId + ", cartTicketsList=" + JsonUtil.jsonify(cartTickets));

        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();

            final ObjectFactory factory = new ObjectFactory();

            final ArrayOfSDCCartTicketListCriteria arrayOfSDCCartTicketListCriteria = factory.createArrayOfSDCCartTicketListCriteria();

            final List<SDCCartTicketListCriteria> criteriaList = arrayOfSDCCartTicketListCriteria.getSDCCartTicketListCriteria();

            for (AxRetailCartTicket ct : cartTickets) {
                SDCCartTicketListCriteria criteria = factory.createSDCCartTicketListCriteria();
                criteria.setQty(BigDecimal.valueOf(ct.getQty()));
                criteria.setProductId(ct.getListingId());
                criteria.setItemId(factory.createSDCCartTicketListCriteriaItemId(ct.getItemId()));
                final String txnId = StringUtils.isEmpty(ct.getTransactionId()) ? transactionId : ct.getTransactionId();
                if(txnId != null){
                    criteria.setTransactionId(factory.createSDCCartTicketListCriteriaTransactionId(txnId));
                }

                criteria.setTableGroupAll(0);
                criteria.setLineId(factory.createSDCCartTicketListCriteriaLineId(ct.getLineId()));


                if (StringUtils.isNotEmpty(ct.getEventGroupId())) {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(ct.getEventGroupId()));
                } else {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(""));
                }

                if (StringUtils.isNotEmpty(ct.getEventLineId())) {
                    criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(ct.getEventLineId()));

                    //if have event date, definitily it will have an event line id first
                    if(ct.getEventDate() != null) {
                        GregorianCalendar gc = new GregorianCalendar();
                        gc.setTime(ct.getEventDate());
                        XMLGregorianCalendar eventDateXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
                        criteria.setEventDate(eventDateXml);
                    }

                    criteria.setIsUpdateCapacity(ct.getUpdateCapacity());

                } else {
                    criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(""));
                }

                if(ct.getAccountNum() != null && ct.getAccountNum().trim().length() > 0){
                    criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(ct.getAccountNum()));
                }else {
                    criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(""));
                }

                criteriaList.add(criteria);
            }

            final SDCCartRetailTicketTableResponse axResponse = stub.cartValidateQuantity(arrayOfSDCCartTicketListCriteria);
            final ApiResult<List<AxRetailTicketRecord>> result = AxRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiCartValidateQuantity: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiCartValidateQuantity: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * For a list of items, validate and reserve barcode stock or event capacity.
     * Used either in conjunction with PIN generation API or for B2B inventory purchases. <br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: CartValidateAndReserveQuantityOnline (will use the online one.....)
     */
    public ApiResult<List<AxRetailTicketRecord>> apiCartValidateAndReserveQuantity(
            StoreApiChannels channel, String transactionId, List<AxRetailCartTicket> cartTickets, String accountNum) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiCartValidateAndReserveQuantity.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();

            final ObjectFactory factory = new ObjectFactory();

            final ArrayOfSDCCartTicketListCriteria arrayOfSDCCartTicketListCriteria = factory.createArrayOfSDCCartTicketListCriteria();
            final List<SDCCartTicketListCriteria> criteriaList = arrayOfSDCCartTicketListCriteria.getSDCCartTicketListCriteria();

            for (AxRetailCartTicket ct : cartTickets) {
                SDCCartTicketListCriteria criteria = factory.createSDCCartTicketListCriteria();
                criteria.setQty(BigDecimal.valueOf(ct.getQty()));
                criteria.setProductId(ct.getListingId());
                criteria.setItemId(factory.createSDCCartTicketListCriteriaItemId(ct.getItemId()));
                final String txnId = StringUtils.isEmpty(ct.getTransactionId()) ? transactionId : ct.getTransactionId();
                criteria.setTransactionId(factory.createSDCCartTicketListCriteriaTransactionId(txnId));
                criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(accountNum));
                criteria.setTableGroupAll(0);
                criteria.setLineId(factory.createSDCCartTicketListCriteriaLineId(ct.getLineId()));

                if (StringUtils.isNotEmpty(ct.getEventGroupId())) {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(ct.getEventGroupId()));

                    if (StringUtils.isNotEmpty(ct.getEventLineId())) {
                        criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(ct.getEventLineId()));

                        //if have event date, definitily it will have an event line id first
                        if(ct.getEventDate() != null) {
                            GregorianCalendar gc = new GregorianCalendar();
                            gc.setTime(ct.getEventDate());
                            XMLGregorianCalendar eventDateXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
                            criteria.setEventDate(eventDateXml);
                        }

                    } else {
                        criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(""));
                    }
                } else {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(""));
                    criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(""));
                }


                criteria.setIsUpdateCapacity(ct.getUpdateCapacity()); //take out from event coz this is also used for others
                criteriaList.add(criteria);
            }

            final SDCCartRetailTicketTableResponse axResponse = stub.cartValidateAndReserveQuantityOnline(arrayOfSDCCartTicketListCriteria);
            final ApiResult<List<AxRetailTicketRecord>> result = AxRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiCartValidateAndReserveQuantity: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiCartValidateAndReserveQuantity: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Start eTicket generation for B2C. <br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: CartCheckoutStartOnline
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2CCartCheckoutStart(
            StoreApiChannels channel, String transactionId, List<AxRetailCartTicket> cartTickets) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiB2CCartCheckoutStart.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();

            final ObjectFactory factory = new ObjectFactory();

            final ArrayOfSDCCartTicketListCriteria arrayOfSDCCartTicketListCriteria = factory.createArrayOfSDCCartTicketListCriteria();

            final List<SDCCartTicketListCriteria> criteriaList = arrayOfSDCCartTicketListCriteria.getSDCCartTicketListCriteria();

            for (AxRetailCartTicket ct : cartTickets) {
                SDCCartTicketListCriteria criteria = factory.createSDCCartTicketListCriteria();
                criteria.setQty(BigDecimal.valueOf(ct.getQty()));
                criteria.setProductId(ct.getListingId());
                criteria.setItemId(factory.createSDCCartTicketListCriteriaItemId(ct.getItemId()));
                final String txnId = StringUtils.isEmpty(ct.getTransactionId()) ? transactionId : ct.getTransactionId();
                criteria.setTransactionId(factory.createSDCCartTicketListCriteriaTransactionId(txnId));
                criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(""));
                criteria.setTableGroupAll(0);
                criteria.setLineId(factory.createSDCCartTicketListCriteriaLineId(ct.getLineId()));

                if(StringUtils.isNotEmpty(ct.getEventGroupId())) {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(ct.getEventGroupId()));
                    criteria.setIsUpdateCapacity(ct.getUpdateCapacity());

                    if (StringUtils.isNotEmpty(ct.getEventLineId())) {
                        criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(ct.getEventLineId()));

                        if(ct.getEventDate() != null) {
                            GregorianCalendar c = new GregorianCalendar();
                            c.setTime(ct.getEventDate());
                            XMLGregorianCalendar eventDateXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                            criteria.setEventDate(eventDateXml);
                        }
                    }
                }




                criteriaList.add(criteria);
            }

            final SDCCartRetailTicketTableResponse axResponse = stub.cartCheckoutStartOnline(arrayOfSDCCartTicketListCriteria);
            final ApiResult<List<AxRetailTicketRecord>> result = AxRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutStart: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutStart: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Complete (confirm) eTicket generation for B2C. <br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: CartCheckoutComplete
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2CCartCheckoutComplete(
            StoreApiChannels channel, String transactionId) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiB2CCartCheckoutComplete.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();
            final SDCCartRetailTicketTableResponse axResponse = stub.cartCheckoutComplete(transactionId);
            final ApiResult<List<AxRetailTicketRecord>> result = AxRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutComplete: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutComplete: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Cancel eTicket generation for B2C. <br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: CartCheckoutCancel
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2CCartCheckoutCancel(StoreApiChannels channel, String transactionId) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiB2CCartCheckoutCancel.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stub = channelStub.getStubRetailTicketTable();
            final SDCCartRetailTicketTableResponse axResponse = stub.cartCheckoutCancel(transactionId);
            final ApiResult<List<AxRetailTicketRecord>> result = AxRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutCancel: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2CCartCheckoutCancel: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * For a discount with a discount code, return the max use and quantity remaining of the discount code. <br><br>
     *
     * WS: /services/SDC_DiscountCounterService.svc <br>
     * API: GetDiscountCounter
     */
    public ApiResult<AxDiscountCounter> apiGetDiscountCounter(StoreApiChannels channel, String offerId, String discountCode) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiGetDiscountCounter.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stubRetailTicketTable = channelStub.getStubRetailTicketTable();
            final SDCGetDiscountCounterResponse dcResponse = stubRetailTicketTable.getDiscountCounter(offerId, discountCode);
            final ApiResult<AxDiscountCounter> result = AxDiscountCounterTransformer.fromWsGetDiscountCounter(dcResponse);
            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiGetDiscountCounter: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiGetDiscountCounter: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Register the discount code usage for the discount. Can be used to update the status,
     * e.g. to cancel discount usage. <br><br>
     *
     * WS: /services/SDC_DiscountCounterService.svc <br>
     * API: UseDiscountCounter
     */
    public ApiResult<String> apiUseDiscountCounter(StoreApiChannels channel, String offerId, String discountCode, String transactionId,
                                                   String inventTransId, int status, int isUpdate) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiUseDiscountCounter.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCRetailTicketTableService stubRetailTicketTable = channelStub.getStubRetailTicketTable();
            final SDCUseDiscountCounterResponse useResponse = stubRetailTicketTable.useDiscountCounter(
                    offerId, discountCode, transactionId, inventTransId, status, isUpdate);
            final ApiResult<String> result = AxDiscountCounterTransformer.fromWsUseDiscountCounter(useResponse);
            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiUseDiscountCounter: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiUseDiscountCounter: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * After a transaction, further update AX on custom fields. <br><br>
     *
     * WS: /services/SDC_OnlineCartService.svc <br>
     * API: UpdateCartExtensions
     */
    public void apiUpdateCartExtensions() {
        //TODO Implement
    }

    /**
     * Start eTicket generation for B2B. Single product in 1 ticket. <br><br>
     *
     * WS: /services/SDC_B2BRetailTicketService.svc <br>
     * API: B2BCartCheckoutStartSingle
     */
    public  ApiResult<List<AxRetailTicketRecord>> apiB2BCartCheckoutStartSingle(StoreApiChannels channel, List<AxRetailCartTicket> cartTickets, String customerId) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiB2BCartCheckoutStartSingle.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCB2BRetailTicketService stub = channelStub.getStubB2BRetailTicket();

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory factory = new com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory();

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ArrayOfSDCCartTicketListCriteria arrayOfSDCCartTicketListCriteria = factory.createArrayOfSDCCartTicketListCriteria();


            final List<com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartTicketListCriteria> criteriaList = arrayOfSDCCartTicketListCriteria.getSDCCartTicketListCriteria();

            for (AxRetailCartTicket ct : cartTickets) {
                com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartTicketListCriteria criteria = factory.createSDCCartTicketListCriteria();
                criteria.setQty(BigDecimal.valueOf(ct.getQty()));
                criteria.setProductId(ct.getListingId());
                criteria.setItemId(factory.createSDCCartTicketListCriteriaItemId(ct.getItemId()));

                final String txnId = ct.getTransactionId();
                criteria.setTransactionId(factory.createSDCCartTicketListCriteriaTransactionId(txnId));
                criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(customerId));
                criteria.setTableGroupAll(0);
                criteria.setLineId(factory.createSDCCartTicketListCriteriaLineId(ct.getLineId()));
                criteria.setLineNumber(factory.createSDCCartTicketListCriteriaLineNumber(ct.getLineNumber()));

                if(StringUtils.isNotEmpty(ct.getEventGroupId())) {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(ct.getEventGroupId()));
                    criteria.setIsUpdateCapacity(ct.getUpdateCapacity());

                    if (StringUtils.isNotEmpty(ct.getEventLineId())) {
                        criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(ct.getEventLineId()));

                        if(ct.getEventDate() != null) {
                            GregorianCalendar c = new GregorianCalendar();
                            c.setTime(ct.getEventDate());
                            XMLGregorianCalendar eventDateXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                            criteria.setEventDate(eventDateXml);
                        }
                    }
                }

                criteriaList.add(criteria);
            }

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartRetailTicketTableResponse axResponse = stub.b2BCartCheckoutStartSingle(arrayOfSDCCartTicketListCriteria);
            final ApiResult<List<AxRetailTicketRecord>> result = AxB2BRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutStartSingle: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutStartSingle: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }

    }

    /**
     * Start eTicket generation for B2B. Multiple products in 1 ticket. <br><br>
     *
     * WS: /services/SDC_B2BRetailTicketService.svc <br>
     * API: B2BCartCheckoutStartCombined
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2BCartCheckoutStartCombined(StoreApiChannels channel, List<AxRetailCartTicket> cartTickets, String customerId) throws AxChannelException{
        log.info("[AX Channel Service] Calling apiB2BCartCheckoutStartCombined.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCB2BRetailTicketService stub = channelStub.getStubB2BRetailTicket();

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory factory = new com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory();

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ArrayOfSDCCartTicketListCriteria arrayOfSDCCartTicketListCriteria = factory.createArrayOfSDCCartTicketListCriteria();


            final List<com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartTicketListCriteria> criteriaList = arrayOfSDCCartTicketListCriteria.getSDCCartTicketListCriteria();


            for (AxRetailCartTicket ct : cartTickets) {
                com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartTicketListCriteria criteria = factory.createSDCCartTicketListCriteria();
                criteria.setQty(BigDecimal.valueOf(ct.getQty()));
                criteria.setProductId(ct.getListingId());
                criteria.setItemId(factory.createSDCCartTicketListCriteriaItemId(ct.getItemId()));

                final String txnId = ct.getTransactionId();
                criteria.setTransactionId(factory.createSDCCartTicketListCriteriaTransactionId(txnId));
                criteria.setAccountNum(factory.createSDCCartTicketListCriteriaAccountNum(customerId));
                criteria.setTableGroupAll(0);
                criteria.setLineId(factory.createSDCCartTicketListCriteriaLineId(ct.getLineId()));
                criteria.setLineNumber(factory.createSDCCartTicketListCriteriaLineNumber(ct.getLineNumber()));

                if(StringUtils.isNotEmpty(ct.getEventGroupId())) {
                    criteria.setEventGroupId(factory.createSDCCartTicketListCriteriaEventGroupId(ct.getEventGroupId()));
                    criteria.setIsUpdateCapacity(ct.getUpdateCapacity());

                    if (StringUtils.isNotEmpty(ct.getEventLineId())) {
                        criteria.setEventLineId(factory.createSDCCartTicketListCriteriaEventLineId(ct.getEventLineId()));

                        if(ct.getEventDate() != null) {
                            GregorianCalendar c = new GregorianCalendar();
                            c.setTime(ct.getEventDate());
                            XMLGregorianCalendar eventDateXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                            criteria.setEventDate(eventDateXml);
                        }
                    }
                }

                criteriaList.add(criteria);
            }


            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartRetailTicketTableResponse axResponse = stub.b2BCartCheckoutStartCombined(arrayOfSDCCartTicketListCriteria);
            final ApiResult<List<AxRetailTicketRecord>> result = AxB2BRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutStartCombined: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutStartCombined: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Complete (confirm) eTicket generation for B2B. <br><br>
     *
     * WS: /services/SDC_B2BRetailTicketService.svc <br>
     * API: B2BCartCheckoutComplete
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2BCartCheckoutComplete(StoreApiChannels channel, String transactionId) throws AxChannelException{
        log.info("[AX Channel Service] Calling apiB2BCartCheckoutComplete.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCB2BRetailTicketService stub = channelStub.getStubB2BRetailTicket();
            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartRetailTicketTableResponse axResponse = stub.b2BCartCheckoutComplete(transactionId);
            final ApiResult<List<AxRetailTicketRecord>> result = AxB2BRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutComplete: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutComplete: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Cancel eTicket generation for B2B. <br><br>
     *
     * WS: /services/SDC_B2BRetailTicketService.svc <br>
     * API: B2BCartCheckoutCancel
     */
    public ApiResult<List<AxRetailTicketRecord>> apiB2BCartCheckoutCancel(StoreApiChannels channel, String transactionId) throws AxChannelException{
        log.info("[AX Channel Service] Calling apiB2BCartCheckoutComplete.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCB2BRetailTicketService stub = channelStub.getStubB2BRetailTicket();
            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.SDCCartRetailTicketTableResponse axResponse = stub.b2BCartCheckoutCancel(transactionId);
            final ApiResult<List<AxRetailTicketRecord>> result = AxB2BRetailResponseTransformer.fromWsCart(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutCancel: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiB2BCartCheckoutCancel: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Generate PIN for B2B and B2C <br><br>
     *
     * WS: /services/SDC_B2BRetailTicketService.svc <br>
     * API: GenerateB2BPin
     */
    public ApiResult<List<AxRetailPinTable>> apiGeneratePin(StoreApiChannels channel, List<AxRetailGeneratePinInput> pinLines) throws AxChannelException {
        log.info("[AX Channel Service] Calling apiGeneratePin.");
        try {

            final AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
            if (!channelStub.isInit()) {
                channelStub.initialise();
            }

            final ISDCB2BRetailTicketService stub = channelStub.getStubB2BRetailTicket();

            final com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory factory =
                    new com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.ObjectFactory();

            final ArrayOfB2BPIN arrayOfPin = factory.createArrayOfB2BPIN();
            final List<B2BPIN> pinInputList = arrayOfPin.getB2BPIN();

            GregorianCalendar gc;
            final DatatypeFactory dtFactory = DatatypeFactory.newInstance();
            for (AxRetailGeneratePinInput pinLine : pinLines) {
                final B2BPIN pinInput = factory.createB2BPIN();
                pinInput.setAllowPartialRedemption(pinLine.isAllowPartialRedemption() ? 1 : 0);
                pinInput.setCustAccount(factory.createB2BPINCustAccount(pinLine.getCustomerAccount()));
                pinInput.setDescription(factory.createB2BPINDescription(pinLine.getDescription()));

                gc = new GregorianCalendar();
                gc.setTime(pinLine.getEndDateTime());
                pinInput.setEndDateTime(dtFactory.newXMLGregorianCalendar(gc));

                if (StringUtils.isNotEmpty(pinLine.getEventGroupId())) {
                    pinInput.setEventGroupId(factory.createB2BPINEventGroupId(pinLine.getEventGroupId()));


                    if (StringUtils.isNotEmpty(pinLine.getEventLineId())) {
                        pinInput.setEventLineId(factory.createB2BPINEventLineId(pinLine.getEventLineId()));

                        if(pinLine.getEventDate() != null) {
                            gc = new GregorianCalendar();
                            gc.setTime(pinLine.getEventDate());
                            XMLGregorianCalendar eventDateXml = dtFactory.newXMLGregorianCalendar(gc);

                            pinInput.setEventDate(factory.createB2BPINEventDate(eventDateXml));
                        }

                    } else {
                        pinInput.setEventLineId(factory.createB2BPINEventLineId(""));
                    }

                } else {
                    pinInput.setEventGroupId(factory.createB2BPINEventGroupId(""));
                    pinInput.setEventLineId(factory.createB2BPINEventLineId(""));
                }


                pinInput.setGuestName(factory.createB2BPINGuestName(pinLine.getGuestName()));

                pinInput.setCreditCardDigits( factory.createB2BPINCreditCardDigits(pinLine.getCcLast4Digits()));
                pinInput.setPinType(pinLine.getPinType().code);

                pinInput.setInventTransId(factory.createB2BPINInventTransId(pinLine.getInventTransId()));
                pinInput.setInvoiceId(factory.createB2BPINInvoiceId(pinLine.getInvoiceId()));
                pinInput.setIsCombineTicket(pinLine.isCombineTicket() ? 1 : 0);
                pinInput.setIsGroupTicket(pinLine.isGroupTicket() ? 1 : 0);
                pinInput.setItemId(factory.createB2BPINItemId(pinLine.getItemId()));
                pinInput.setLineNum(pinLine.getLineNumber());
                pinInput.setMediaType(factory.createB2BPINMediaType(pinLine.getMediaType()));
                pinInput.setPackageName(factory.createB2BPINPackageName(pinLine.getPackageName()));
                pinInput.setQty(pinLine.getQty());
                pinInput.setQtyPerProduct(pinLine.getQtyPerProduct());
                pinInput.setReferenceId(factory.createB2BPINReferenceId(pinLine.getReferenceId()));
                if (StringUtils.isNotEmpty(pinLine.getRetailVariantId())) {
                    pinInput.setRetailVariantId(factory.createB2BPINRetailVariantId(pinLine.getRetailVariantId()));
                } else {
                    pinInput.setRetailVariantId(factory.createB2BPINRetailVariantId(""));
                }
                pinInput.setSalesId(factory.createB2BPINSalesId(pinLine.getSalesId()));

                gc = new GregorianCalendar();
                gc.setTime(pinLine.getStartDateTime());
                pinInput.setStartDateTime(dtFactory.newXMLGregorianCalendar(gc));

                pinInput.setTransactionId(factory.createB2BPINTransactionId(pinLine.getTransactionId()));

                pinInputList.add(pinInput);
            }

            final SDCB2BPINCriteria pinCriteria = factory.createSDCB2BPINCriteria();
            pinCriteria.setGenerateB2BPinCollection(factory.createSDCB2BPINCriteriaGenerateB2BPinCollection(arrayOfPin));

            final SDCB2BPINPopulateResponse axResponse = stub.generateB2BPin(pinCriteria);
            final ApiResult<List<AxRetailPinTable>> result = AxB2BRetailResponseTransformer.fromWsGeneratePin(axResponse);

            return result;
        } catch (AxChannelException ace) {
            final String errMsg = "Error encountered calling apiGeneratePin: " + ace.getMessage();
            log.error(errMsg, ace);
            throw ace;
        } catch (Exception e) {
            final String errMsg = "Error encountered calling apiGeneratePin: " + e.getMessage();
            log.error(errMsg, e);
            throw new AxChannelException(errMsg, e);
        }
    }

    /**
     * Block redemption of a certain PIN. <br><br>
     *
     * WS: /services/SDC_BlockPINRedemptionService.svc <br>
     * API: GetBlockPINRedemption
     */
    public void apiBlockPinRedemption() {
        //TODO Implement
    }

    /**
     * View B2B PIN Information: PIN Header with PIN Lines <br><br>
     *
     * WS: /services/SDC_B2BPINViewService.svc <br>
     * API: GetB2BPINTableInquiry
     */
    public ApiResult<AxWOTPinViewTable> apiGetB2BPINTableInquiry(StoreApiChannels channel, String pincode) throws AxChannelException, ParseException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinView = stub.getStubPIN();
        SDCB2BPINTableInquiryResponse response = pinView.getB2BPINTableInquiry(pincode);
        return AxPinTransformer.fromWsB2BPINTableInquiry(response);
    }


    /**
     * (Wings of Time) View B2B PIN <br><br>
     *
     * WS: /services/SDC_B2BPINViewService.svc <br>
     * API: GetB2BPINView
     */
    @Deprecated
    public ApiResult<List<AxWOTPinViewTable>> apiWotViewPin_oos(StoreApiChannels channel, String customerAccount, Integer reservationType) throws AxChannelException, ParseException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinView = stub.getStubPIN();
        SDCB2BPINViewResponse response = pinView.getB2BPINView(customerAccount, reservationType);
        return AxPinTransformer.fromWsB2BPINView(response);
    }

//    /**
//     * (Wings of Time) View B2B PIN Line <br><br>
//     *
//     * WS: /services/SDC_B2BPINViewLineService.svc <br>
//     * API: GetB2BPINViewLine
//     */
//    @Deprecated
//    public ApiResult<List<AxWOTPinViewLine>> apiWotViewPinLine_oos(StoreApiChannels channel, String pinRefId) throws AxChannelException, ParseException {
//        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
//        ISDCPINService pinService = stub.getStubPIN();
//        SDCB2BPINViewLineResponse response = pinService.getB2BPINViewLine(pinRefId);
//        return AxPinTransformer.fromWsB2BPINViewLine(response);
//    }

    /**
     * (Wings of Time) Get Customer Penalty Charge
     * WS: /services/SDC_PinService.svc
     * API: GetCustPenaltyCharge
     */
    public ApiResult<List<AxPenaltyCharge>> apiGetWoTCustPenaltyCharge(StoreApiChannels channel, String custAccount){
        ApiResult<List<AxPenaltyCharge>> api = new ApiResult<List<AxPenaltyCharge>>();
        SDCCustPenaltyChargeResponse response = null;
        try{
            AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
            ISDCPINService pinSrv = stub.getStubPIN();
            response = pinSrv.getCustPenaltyCharge(custAccount);
        }catch (Exception ex){
            if(response == null){
                return AxPenaltyChargeTransformer.fromWsPenaltyChargeException(ex);
            }
        }
        return AxPenaltyChargeTransformer.fromWsPenaltyChargeResponse(response, custAccount);
    }

    /**
     * (Wings of Time) Update B2B PIN <br><br>
     *
     * WS: /services/SDC_PinService.svc <br>
     * API: B2BPINUpdate
     */
    public ApiResult<String> apiWotUpdatePin(StoreApiChannels channel, AxWOTPinUpdate req) throws AxChannelException, DatatypeConfigurationException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinUpdateSrv = stub.getStubPIN();
        SDCB2BPINUpdateResponse response =
                pinUpdateSrv.b2BPINUpdate(req.getPinCode(), req.getQty(), req.getEventLineId(),
                        convertToXMLGregorianCalendarDate(req.getEventDate()), req.getMarkupCode(),
                        req.getMarkupAmt(), req.getItemId(), req.getInventTransId());
        return AxPinTransformer.fromWsB2BPinUpdate(response);
    }

    /**
     * (Wings of Time) Cancel B2B PIN <br><br>
     *
     * WS: /services/SDC_PinService.svc <br>
     * API: B2BPINCancel
     */
    public ApiResult<String> apiWotCancelPin(StoreApiChannels channel, String pinCode, String markupCode, BigDecimal markupAmt) throws AxChannelException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinCancelSrv = stub.getStubPIN();
        SDCB2BPINCancelResponse response = pinCancelSrv.b2BPINCancel(pinCode, markupCode, markupAmt);
        return AxPinTransformer.fromWsB2BPinCancel(response);
    }


    /**
     * (Wings of Time) Save Order <br><br>
     *
     * WS: /services/SDC_PinService.svc <br>
     * API: SaveWOTOrder
     */
    public ApiResult<AxWOTSalesOrder> apiWotSaveOrder(StoreApiChannels channel, AxWOTOrder order) throws AxChannelException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService cartSrv = stub.getStubPIN();
        SDCSaveWOTOrderResponse response = cartSrv.saveWOTOrder(order.getCartId(), order.getCustomerAccount(), order.getReceiptEmail(), order.getOnlineSalesPool());
        return AxPinTransformer.fromWotSaveOrder(response);
    }

    private XMLGregorianCalendar convertToXMLGregorianCalendarDate(Date date) throws DatatypeConfigurationException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1, //need to add the month
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED
        );
    }


    /**
     * Update B2B PIN Status <br><br>
     *
     * WS: /services/SDC_PinService.svc <br>
     * API: B2BUpdatePINStatus
     */
    public ApiResult<String> apiB2BUpdatePinStatus(StoreApiChannels channel, AxB2BUpdatePinStatus req) throws AxChannelException, DatatypeConfigurationException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCPINService pinSrv = stub.getStubPIN();
        SDCB2BUpdatePINStatusResponse response =
                pinSrv.b2BUpdatePINStatus(req.getPinCode(), req.getPinStatus(), req.getReasonCode(), req.getProcessedBy());
        return AxB2BUpdatePinStatusResponseTransformer.fromWsB2BUpdatePINStatus(response);
    }


    /**
     * Used for Confirm Sales Order<br><br>
     *
     * WS: /services/SDC_RetailTicketTableService.svc <br>
     * API: insertOnlineReference
     */
    public ApiResult<String> apiInsertOnlineReference(StoreApiChannels channel, AxInsertOnlineReference req) throws AxChannelException, DatatypeConfigurationException {
        AxChannelServiceMainStub stub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCRetailTicketTableService retailSrv = stub.getStubRetailTicketTable();
        SDCInsertOnlineReferenceResponse response =
               retailSrv.insertOnlineReference(req.getTransactionId(), convertToXMLGregorianCalendarDate(req.getTransDate()));
        return AxInsertOnlineReferenceResponseTransformer.fromWsInsertOnlineResponse(response);
    }

}
