package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccessRightsGroup;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccessRightsGroupMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTARightsMapping;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PartnerAccountVM implements Serializable{

    private Integer id;
    private String username;
    private String password;
    private String email;
    private String name;
    private String accountCode;
    private String relatedCustomerId;
    private PartnerVM partner;
    private boolean subAccountEnabled;
    private String title;
    private ArrayList<Integer> rightsIds;
    private String status;

    //email template data
    private String predefinedBody;
    //private  String subAccountHolderName;
    private String subAccountLoginName;
    private String faqLink;
    private String mainAccountLoginName;

    private boolean isNotSelf;

    public String getMainAccountLoginName() {
        return mainAccountLoginName;
    }

    public void setMainAccountLoginName(String mainAccountLoginName) {
        this.mainAccountLoginName = mainAccountLoginName;
    }

    public String getFaqLink() {
        return faqLink;
    }

    public void setFaqLink(String faqLink) {
        this.faqLink = faqLink;
    }

    public PartnerAccountVM() {

    }

    public PartnerAccountVM(PPSLMTASubAccount acc){
        this.id = acc.getId();
        this.username = acc.getUsername();
        this.name = acc.getName();
        this.email = acc.getEmail();
        this.title = acc.getTitle();
        this.email = acc.getEmail();
        this.status = acc.getStatus();
        rightsIds = new ArrayList<Integer>();
        List<PPSLMTARightsMapping> rightsMappingList = acc.getRightsMapping();
        if(rightsMappingList!=null){
            for(PPSLMTARightsMapping temp: rightsMappingList){
                PPSLMTAAccessRightsGroup group = temp.getAccessRightsGroup();
                rightsIds.add(group.getId());
            }
        }
    }

    public String getPredefinedBody() {
        return predefinedBody;
    }

    public void setPredefinedBody(String predefinedBody) {
        this.predefinedBody = predefinedBody;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubAccountHolderName() {
        return name;
    }

    public void setSubAccountHolderName(String subAccountHolderName) {
        this.name = subAccountHolderName;
    }

    public String getSubAccountLoginName() {
        return username;
    }

    public void setSubAccountLoginName(String subAccountLoginName) {
        this.username = subAccountLoginName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelatedCustomerId() {
        return relatedCustomerId;
    }

    public void setRelatedCustomerId(String relatedCustomerId) {
        this.relatedCustomerId = relatedCustomerId;
    }

    public PartnerVM getPartner() {
        return partner;
    }

    public void setPartner(PartnerVM partner) {
        this.partner = partner;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public boolean isSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Integer> getRightsIds() {
        return rightsIds;
    }

    public void setRightsIds(ArrayList<Integer> rightsIds) {
        this.rightsIds = rightsIds;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNotSelf() {
        return isNotSelf;
    }

    public void setNotSelf(boolean notSelf) {
        isNotSelf = notSelf;
    }
}