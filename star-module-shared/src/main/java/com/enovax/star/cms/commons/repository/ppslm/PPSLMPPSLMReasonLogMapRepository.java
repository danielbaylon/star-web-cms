package com.enovax.star.cms.commons.repository.ppslm;


import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMReasonLogMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMPPSLMReasonLogMapRepository extends JpaRepository<PPSLMReasonLogMap, Integer> {

    @Query("from PPSLMReasonLogMap where logId = ?1")
    List<PPSLMReasonLogMap> findByLogId(Integer id);
}
