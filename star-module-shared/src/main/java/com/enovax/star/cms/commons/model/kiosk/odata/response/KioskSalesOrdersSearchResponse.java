package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrderSearch;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskSalesOrdersSearchResponse {

	@JsonProperty("value")
	private List<AxSalesOrderSearch> axSalesOrderSearch;
	
	public void setAxSalesOrderSearch(List<AxSalesOrderSearch> axSalesOrderSearch) {
		this.axSalesOrderSearch = axSalesOrderSearch;
	}
	
	public List<AxSalesOrderSearch> getAxSalesOrderSearch() {
		return axSalesOrderSearch;
	}
}
