package com.enovax.star.cms.commons.model.axchannel.pin;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 18/8/16.
 */
public class AxWOTPinViewTable {

    private Integer allowPartialRedemption;
    private String custAccount;
    private String description;
    private Date endDateTime;
    private Date startDateTime;
    private Integer isGroupTicket;
    private Integer isReservation;
    private Integer isWashedDown;
    private String mediaType;
    private String pinBlockedBy;
    private Date pinBlockedDateTime;
    private String pinCode;
    private String pinStatus;
    private String packageName;
    private Integer qtyPerProduct;
    private String reasonCode;
    private String referenceId;
    private Integer sourceType;
    private Date lastRedeemDateTime;

    private List<AxWOTPinViewLine> pinViewLineCollection;

    public Integer getAllowPartialRedemption() {
        return allowPartialRedemption;
    }

    public void setAllowPartialRedemption(Integer allowPartialRedemption) {
        this.allowPartialRedemption = allowPartialRedemption;
    }

    public String getCustAccount() {
        return custAccount;
    }

    public void setCustAccount(String custAccount) {
        this.custAccount = custAccount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Integer getIsGroupTicket() {
        return isGroupTicket;
    }

    public void setIsGroupTicket(Integer isGroupTicket) {
        this.isGroupTicket = isGroupTicket;
    }

    public Integer getIsReservation() {
        return isReservation;
    }

    public void setIsReservation(Integer isReservation) {
        this.isReservation = isReservation;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getPinBlockedBy() {
        return pinBlockedBy;
    }

    public void setPinBlockedBy(String pinBlockedBy) {
        this.pinBlockedBy = pinBlockedBy;
    }

    public Date getPinBlockedDateTime() {
        return pinBlockedDateTime;
    }

    public void setPinBlockedDateTime(Date pinBlockedDateTime) {
        this.pinBlockedDateTime = pinBlockedDateTime;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPinStatus() {
        return pinStatus;
    }

    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getQtyPerProduct() {
        return qtyPerProduct;
    }

    public void setQtyPerProduct(Integer qtyPerProduct) {
        this.qtyPerProduct = qtyPerProduct;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public Date getLastRedeemDateTime() {
        return lastRedeemDateTime;
    }

    public void setLastRedeemDateTime(Date lastRedeemDateTime) {
        this.lastRedeemDateTime = lastRedeemDateTime;
    }

    public List<AxWOTPinViewLine> getPinViewLineCollection() {
        return pinViewLineCollection;
    }

    public void setPinViewLineCollection(List<AxWOTPinViewLine> pinViewLineCollection) {
        this.pinViewLineCollection = pinViewLineCollection;
    }

    public Integer getIsWashedDown() {
        return isWashedDown;
    }

    public void setIsWashedDown(Integer isWashedDown) {
        this.isWashedDown = isWashedDown;
    }
}
