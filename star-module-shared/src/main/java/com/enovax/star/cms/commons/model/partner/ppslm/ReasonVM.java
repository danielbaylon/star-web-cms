package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMReason;

import java.util.List;

public class ReasonVM {

    private Integer id;
    private String title;
    private String content;
    private String status;
    private String statusLabel;
    private String remarks;
    private List<Integer> reasonIds;
    
    
    public ReasonVM(){
    }
    
    public ReasonVM(PPSLMReason reason){
        this.id = reason.getId();
        this.title = reason.getTitle();
        this.content = reason.getContent();
        this.status = reason.getStatus();
        this.statusLabel = GeneralStatus.getLabel(reason.getStatus());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<Integer> getReasonIds() {
        return reasonIds;
    }

    public void setReasonIds(List<Integer> reasonIds) {
        this.reasonIds = reasonIds;
    }
}
