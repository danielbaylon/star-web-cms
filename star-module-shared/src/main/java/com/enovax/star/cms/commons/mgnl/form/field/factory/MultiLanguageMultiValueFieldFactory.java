package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.MultiLanguageMultiValueField;
import com.enovax.star.cms.commons.mgnl.form.field.definition.MultiLanguageMultiValueFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.ui.Field;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.MultiValueFieldDefinition;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;
import info.magnolia.ui.form.field.factory.MultiValueFieldFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/30/16.
 */
public class MultiLanguageMultiValueFieldFactory <T extends MultiLanguageMultiValueFieldDefinition> extends MultiValueFieldFactory<T> {

    private FieldFactoryFactory fieldFactoryFactory;
    private ComponentProvider componentProvider;
    private I18NAuthoringSupport i18nAuthoringSupport;

    @Inject
    public MultiLanguageMultiValueFieldFactory(MultiValueFieldDefinition definition, Item relatedFieldItem,  UiContext uiContext,  I18NAuthoringSupport i18nAuthoringSupport, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, fieldFactoryFactory, componentProvider);
        this.fieldFactoryFactory = fieldFactoryFactory;
        this.componentProvider = componentProvider;
        this.i18nAuthoringSupport = i18nAuthoringSupport;
    }


    @Override
    protected Field<PropertysetItem> createFieldComponent() {
        // FIXME change i18n setting : MGNLUI-1548
        definition.setI18nBasename(getMessages().getBasename());

        MultiLanguageMultiValueField field = new MultiLanguageMultiValueField(definition, fieldFactoryFactory, componentProvider, item, i18nAuthoringSupport);
        // Set Caption
        field.setButtonCaptionAdd(getMessage(definition.getButtonSelectAddLabel()));
        field.setButtonCaptionRemove(getMessage(definition.getButtonSelectRemoveLabel()));

        return field;
    }
}

