package com.enovax.star.cms.commons.datamodel.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 7/14/16.
 */
@Entity
@Table(name = PPSLMInventoryTransaction.TABLE_NAME)
public class PPSLMInventoryTransaction {
    public static final String TABLE_NAME = "PPSLMInventoryTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "receiptNum", nullable = false)
    private String receiptNum;

    @Column(name = "mainAccountId", nullable = false)
    private Integer mainAccountId;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "mainAccountId", updatable = false, insertable = false)
    private PPSLMTAMainAccount mainAccount;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "mainAccountId", referencedColumnName = "adminId", updatable = false, insertable = false)
//    private PPSLMPartner partner;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "isSubAccountTrans", nullable = false)
    private Boolean subAccountTrans;

    @Column(name = "totalAmount", nullable = false)
    private BigDecimal totalAmount;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "tmMerchantId", nullable = false)
    private String tmMerchantId;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "tmStatus")
    private String tmStatus;

    @Column(name = "tmStatusDate")
    private Date tmStatusDate;

    @Column(name = "tmApprovalCode")
    private String tmApprovalCode;

    @Column(name = "tmErrorCode")
    private String tmErrorCode;

    @Column(name = "tmErrorMessage")
    private String tmErrorMessage;

    @Column(name = "paymentType")
    private String paymentType;

    @Column(name = "ip")
    private String ip;

    @Column(name = "maskedCc")
    private String maskedCc;

    @Column(name = "reprintCount")
    private Integer reprintCount;

    @Column(name = "validityStartDate")
    private Date validityStartDate;

    @Column(name = "validityEndDate", nullable = false)
    private Date validityEndDate;

    @Column(name = "revalidated", nullable = false)
    private boolean revalidated;

    @Column(name = "ticketGenerated", nullable = false)
    private boolean ticketGenerated;

    @Column(name = "gstRate", nullable = false)
    private BigDecimal gstRate;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "modifiedDate", nullable = false)
    private Date modifiedDate;

    @Transient
    private String gstRateStr;

    @Transient
    private Integer transQty = 0;

    @Transient
    private Integer revalidatePeriod = 3;

    @Transient
    private Integer allowRevalPeriod = 2;

    @Transient
    private Integer expiringAlertPeriod = 2;

    @Transient
    private String displayStatus;

    @Transient
    private String offlinePaymentStatus;

    @Transient
    private Integer rptId;

    @Transient
    private Integer offlinePaymentId;

    @Transient
    private List<PPSLMMixMatchPackage> mmpkgs;

    @Transient
    private String gstStr;

    @Transient
    private String exclGstStr;

    @Transient
    private Boolean canRevalidateByAcc = null;

    @OneToOne(fetch = FetchType.LAZY, mappedBy="inventoryTransaction")
    private PPSLMOfflinePaymentRequest offlinePayReq;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "inventoryTrans")
//    @OrderBy("productId ASC, itemId ASC, transItemType ASC")
    List<PPSLMInventoryTransactionItem> inventoryItems = new ArrayList<PPSLMInventoryTransactionItem>(
            0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction")
    private List<PPSLMRevalidationTransaction> revalTransList;

    @Transient
    private PPSLMRevalidationTransaction revalidateTrans;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

//    public PPSLMPartner getPartner() {
//        return partner;
//    }
//
//    public void setPartner(PPSLMPartner partner) {
//        this.partner = partner;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean isSubAccountTrans() {
        return subAccountTrans;
    }

    public Boolean getSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public Date getTmStatusDate() {
        return tmStatusDate;
    }

    public void setTmStatusDate(Date tmStatusDate) {
        this.tmStatusDate = tmStatusDate;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMaskedCc() {
        return maskedCc;
    }

    public void setMaskedCc(String maskedCc) {
        this.maskedCc = maskedCc;
    }

    public Integer getReprintCount() {
        return reprintCount;
    }

    public void setReprintCount(Integer reprintCount) {
        this.reprintCount = reprintCount;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public boolean isRevalidated() {
        return revalidated;
    }

    public boolean getRevalidated() {
        return revalidated;
    }

    public void setRevalidated(boolean revalidated) {
        this.revalidated = revalidated;
    }

    public boolean isTicketGenerated() {
        return ticketGenerated;
    }

    public void setTicketGenerated(boolean ticketGenerated) {
        this.ticketGenerated = ticketGenerated;
    }

    public List<PPSLMInventoryTransactionItem> getInventoryItems() {
        return inventoryItems;
    }

    public void setInventoryItems(List<PPSLMInventoryTransactionItem> inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    public Integer getTransQty() {
        return transQty;
    }

    public void setTransQty(Integer transQty) {
        this.transQty = transQty;
    }

    public void sumQty() {
        int tempQty = 0;
        for (PPSLMInventoryTransactionItem tranItem : getInventoryItems()) {
            tempQty = tempQty + tranItem.getUnpackagedQty();
        }
        transQty = tempQty;
    }

    public Integer getRevalidatePeriod() {
        return revalidatePeriod;
    }

    public void setRevalidatePeriod(Integer revalidatePeriod) {
        this.revalidatePeriod = revalidatePeriod;
    }

    public PPSLMRevalidationTransaction getRevalidateTrans() {
        if (this.revalTransList != null && this.revalTransList.size() > 0) {
            for (PPSLMRevalidationTransaction tempObj : this.revalTransList) {
                if (EnovaxTmSystemStatus.Success.toString().equals(
                        tempObj.getTmStatus())) {
                    return tempObj;
                }
            }
        }
        return null;
    }

    public void setRevalidateTrans(PPSLMRevalidationTransaction revalidateTrans) {
        this.revalidateTrans = revalidateTrans;
    }

    public List<PPSLMRevalidationTransaction> getRevalTransList() {
        return revalTransList;
    }

    public void setRevalTransList(List<PPSLMRevalidationTransaction> revalTransList) {
        this.revalTransList = revalTransList;
    }


    //original: revalidatale
    public boolean revalidateAvailable() {
        if (TransactionUtil.hasExpiredRevalidateDate(this.validityEndDate,
                this.revalidatePeriod)) {
            return false;
        }
        if (this.revalidated
                || TicketStatus.Revalidated.toString().equals(this.status)) {
            return false;
        }
        String[] allowedStatus = new String[] {
                TicketStatus.Available.toString(),
                TicketStatus.Expired.toString(),
                TicketStatus.Expiring.toString() };
        if (!Arrays.asList(allowedStatus).contains(this.status)) {
            return false;
        }
        if (!TransactionUtil.withInRevalPeriod(this)) {
            return false;
        }

        // check it has event, if have event also not allow
        boolean isSpecialValidityDateType = false;
        boolean hasAvailableItems = false;
        for (PPSLMInventoryTransactionItem item : this.getInventoryItems()) {
            if(StringUtils.isNotEmpty(item.getEventGroupId())) {
                return false;
            }

            // if it is not event item, then, check for remaining quantities
            if(item.getUnpackagedQty() > 0){
                hasAvailableItems = true;
            }
//            if(ValidType.PERIOD.code.toString().equalsIgnoreCase(item.getValidityDateType()) || ValidType.DAY.code.toString().equalsIgnoreCase(item.getValidityDateType())){
//                isSpecialValidityDateType = true;
//            }
        }

        if(!hasAvailableItems){
            return false;
        }

        if(isSpecialValidityDateType){
            return false;
        }

        //TODO check if the login user have rights to revalidate the transaction......
        if (this.canRevalidateByAcc != null && !this.canRevalidateByAcc) {
            return false;
        }
        return true;
    }

    public Date getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(Date validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public List<PPSLMMixMatchPackage> getMmpkgs() {
        return mmpkgs;
    }

    public void setMmpkgs(List<PPSLMMixMatchPackage> mmpkgs) {
        this.mmpkgs = mmpkgs;
    }

    public Integer getAllowRevalPeriod() {
        return allowRevalPeriod;
    }

    public void setAllowRevalPeriod(Integer allowRevalPeriod) {
        this.allowRevalPeriod = allowRevalPeriod;
    }

    public String getDisplayStatus() {
        // set status as Expiring in x weeks
        long weeks = TransactionUtil.getExpiringInWeeks(this);
        if (TicketStatus.Available.toString().equals(this.getStatus())
                && weeks <= this.expiringAlertPeriod && weeks > 0) {
            displayStatus = TransactionUtil.getExpiringStatus(this);
        } else {
            displayStatus = this.status;
        }
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public Integer getExpiringAlertPeriod() {
        return expiringAlertPeriod;
    }

    public void setExpiringAlertPeriod(Integer expiringAlertPeriod) {
        this.expiringAlertPeriod = expiringAlertPeriod;
    }

    public String getGstStr() {
        this.gstStr = TransactionUtil.gstWithDecimal(this.getTotalAmount(), this.gstRate,
                null);
        return gstStr;
    }

    public void setGstStr(String gstStr) {
        this.gstStr = gstStr;
    }

    public String getExclGstStr() {
        this.exclGstStr = TransactionUtil.getExclGSTCostWithDecimal(
                this.getTotalAmount(),this.gstRate, null);
        return exclGstStr;
    }

    public void setExclGstStr(String exclGstStr) {
        this.exclGstStr = exclGstStr;
    }

    public String getTmErrorCode() {
        return tmErrorCode;
    }

    public void setTmErrorCode(String tmErrorCode) {
        this.tmErrorCode = tmErrorCode;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public String getGstRateStr() {
        return TransactionUtil.getDisplayGSTRate(gstRate);
    }

    public void setGstRateStr(String gstRateStr) {
        this.gstRateStr = gstRateStr;
    }

    public Boolean isCanRevalidateByAcc() {
        return canRevalidateByAcc;
    }

    public void setCanRevalidateByAcc(Boolean canRevalidateByAcc) {
        this.canRevalidateByAcc = canRevalidateByAcc;
    }

    public PPSLMOfflinePaymentRequest getOfflinePayReq() {
        return offlinePayReq;
    }

    public void setOfflinePayReq(PPSLMOfflinePaymentRequest offlinePayReq) {
        this.offlinePayReq = offlinePayReq;
    }

    public PPSLMTAMainAccount getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(PPSLMTAMainAccount mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getOfflinePaymentStatus() {
        return offlinePaymentStatus;
    }

    public void setOfflinePaymentStatus(String offlinePaymentStatus) {
        this.offlinePaymentStatus = offlinePaymentStatus;
    }

    public Integer getOfflinePaymentId() {
        return offlinePaymentId;
    }

    public void setOfflinePaymentId(Integer offlinePaymentId) {
        this.offlinePaymentId = offlinePaymentId;
    }

    public Boolean getCanRevalidateByAcc() {
        return canRevalidateByAcc;
    }

    public Integer getRptId() {
        return rptId;
    }

    public void setRptId(Integer rptId) {
        this.rptId = rptId;
    }
}
