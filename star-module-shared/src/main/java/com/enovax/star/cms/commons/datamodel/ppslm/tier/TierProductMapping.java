package com.enovax.star.cms.commons.datamodel.ppslm.tier;

import com.enovax.star.cms.commons.model.partner.ppslm.PartnerProductVM;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 5/13/16.
 */
public class TierProductMapping {

    private int id;
    private String tierId;
    private ProductTier productTier;
    private String productId;
    private PartnerProductVM productVM;
    private Node mappingNode;

    public TierProductMapping() {
    }

    public TierProductMapping(Node mappingNode) {
        try {
            this.id = Integer.parseInt(mappingNode.getName());
            this.tierId = mappingNode.getProperty("tierId").getString();
            this.productId = mappingNode.getProperty("productId").getString();
            this.mappingNode = mappingNode;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTierId() {
        return tierId;
    }

    public void setTierId(String tierId) {
        this.tierId = tierId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public ProductTier getProductTier() {
        return productTier;
    }

    public void setProductTier(ProductTier productTier) {
        this.productTier = productTier;
    }

    public PartnerProductVM getProductVM() {
        return productVM;
    }

    public void setProductVM(PartnerProductVM productVM) {
        this.productVM = productVM;
    }

    public Node getMappingNode() {
        return mappingNode;
    }

    public void setMappingNode(Node mappingNode) {
        this.mappingNode = mappingNode;
    }
}
