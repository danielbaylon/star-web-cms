package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

/**
 * Created by houtao on 20/8/16.
 */

@Entity
@Table(name = PPMFLGRefreshHist.TABLE_NAME)
public class PPMFLGRefreshHist {

    public static final String TABLE_NAME = "PPMFLGRefreshHist";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;

    @Column(name = "key1")
    private String key1;

    @Column(name = "key2")
    private String key2;

    @Column(name = "key3")
    private String key3;

    @Column(name = "value")
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getKey3() {
        return key3;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
