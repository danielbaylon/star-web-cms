
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerateB2BPinResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_B2BPINPopulateResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generateB2BPinResult"
})
@XmlRootElement(name = "GenerateB2BPinResponse", namespace = "http://tempuri.org/")
public class GenerateB2BPinResponse {

    @XmlElementRef(name = "GenerateB2BPinResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINPopulateResponse> generateB2BPinResult;

    /**
     * Gets the value of the generateB2BPinResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINPopulateResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINPopulateResponse> getGenerateB2BPinResult() {
        return generateB2BPinResult;
    }

    /**
     * Sets the value of the generateB2BPinResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINPopulateResponse }{@code >}
     *     
     */
    public void setGenerateB2BPinResult(JAXBElement<SDCB2BPINPopulateResponse> value) {
        this.generateB2BPinResult = value;
    }

}
