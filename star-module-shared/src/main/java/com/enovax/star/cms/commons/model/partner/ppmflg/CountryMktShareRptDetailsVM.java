package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.List;

/**
 * Created by lavanya on 4/11/16.
 */
public class CountryMktShareRptDetailsVM {
    List<CountryVM> allCountryVMs;
    List<MktSharePartnerVM> mktSharePartnerVMs;

    public CountryMktShareRptDetailsVM() {
    }

    public CountryMktShareRptDetailsVM(List<CountryVM> allCountryVMs, List<MktSharePartnerVM> mktSharePartnerVMs) {
        this.allCountryVMs = allCountryVMs;
        this.mktSharePartnerVMs = mktSharePartnerVMs;
    }

    public List<CountryVM> getAllCountryVMs() {
        return allCountryVMs;
    }

    public void setAllCountryVMs(List<CountryVM> allCountryVMs) {
        this.allCountryVMs = allCountryVMs;
    }

    public List<MktSharePartnerVM> getMktSharePartnerVMs() {
        return mktSharePartnerVMs;
    }

    public void setMktSharePartnerVMs(List<MktSharePartnerVM> mktSharePartnerVMs) {
        this.mktSharePartnerVMs = mktSharePartnerVMs;
    }
}
