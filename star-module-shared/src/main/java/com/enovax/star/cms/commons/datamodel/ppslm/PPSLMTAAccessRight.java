package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="PPSLMTAAccessRight")
public class PPSLMTAAccessRight implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "topNavName", nullable = false)
	private String topNavName;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = false)
	private String description;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="accessRight")
    private List<PPSLMTAAccessRightsGroupMapping> accessRightsGroupMapping = new ArrayList<PPSLMTAAccessRightsGroupMapping>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTopNavName() {
		return topNavName;
	}

	public void setTopNavName(String topNavName) {
		this.topNavName = topNavName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<PPSLMTAAccessRightsGroupMapping> getAccessRightsGroupMapping() {
		return accessRightsGroupMapping;
	}

	public void setAccessRightsGroupMapping(
			List<PPSLMTAAccessRightsGroupMapping> accessRightsGroupMapping) {
		this.accessRightsGroupMapping = accessRightsGroupMapping;
	}
    
    
	
}
