package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprovalLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGApprovalLogRepository extends JpaRepository<PPMFLGApprovalLog, Integer>, JpaSpecificationExecutor<PPMFLGApprovalLog>{

    @Query("from PPMFLGApprovalLog where approverType = ?1 and status = ?2 ")
    List<PPMFLGApprovalLog> findAllByApproverTypeAndStatus(String type, String status);

    PPMFLGApprovalLog findById(Integer id);

    List<PPMFLGApprovalLog> findByCategoryAndRelatedKeyOrderByCreatedDateDesc(String category, String relatedKey);

    @Query("from PPMFLGApprovalLog where relatedKey = ?1 and category = ?2 and status='Pending' order by createdDate")
    List<PPMFLGApprovalLog> findPendingRequest(String relatedKey, String category);

    @Query("from PPMFLGApprovalLog where relatedKey = ?1 and category = 'Partner' and actionType = 'Resubmit' and status is null order by createdDate desc")
    List<PPMFLGApprovalLog> findPartnerLatestApproverLog(String id);

    @Query(value = "select count(*) from PPMFLGApprovalLog where status = 'Pending' and " +
            " ((currentValue  not like '%<excluProdIds>' + :productId + '</excluProdIds>%' and " +
            "   previousValue like '%<excluProdIds>' + :productId + '</excluProdIds>%') or " +
            "  (previousValue not like '%<excluProdIds>' + :productId + '</excluProdIds>%' and " +
            "   currentValue like '%<excluProdIds>' + :productId + '</excluProdIds>%'))", nativeQuery = true)
    int getPendingRequestForProdCnt(@Param("productId") String productId);
}
