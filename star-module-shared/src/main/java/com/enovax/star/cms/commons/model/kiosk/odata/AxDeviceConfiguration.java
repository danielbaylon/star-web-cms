package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.DeviceConfiguration
 * 
 * @author Justin
 * @since 8 SEP 16
 */

public class AxDeviceConfiguration {

    private String storeNumber;

    private String hardwareProfile;

    private String placement;

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getHardwareProfile() {
        return hardwareProfile;
    }

    public void setHardwareProfile(String hardwareProfile) {
        this.hardwareProfile = hardwareProfile;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

}
