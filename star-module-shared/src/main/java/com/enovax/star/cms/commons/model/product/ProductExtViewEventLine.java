package com.enovax.star.cms.commons.model.product;

import java.util.List;

public class ProductExtViewEventLine {

    private String eventLineId;
    private String eventName;
    private Integer eventTime;
    private Integer eventStartTime;
    private Integer eventEndTime;
    private String eventCapacityId;
    private List<ProductExtViewEventAllocation> eventDates;

    public Integer getEventTime() {
        return eventTime;
    }

    public void setEventTime(Integer eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public List<ProductExtViewEventAllocation> getEventDates() {
        return eventDates;
    }

    public void setEventDates(List<ProductExtViewEventAllocation> eventDates) {
        this.eventDates = eventDates;
    }

    public Integer getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(Integer eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public Integer getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(Integer eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventCapacityId() {
        return eventCapacityId;
    }

    public void setEventCapacityId(String eventCapacityId) {
        this.eventCapacityId = eventCapacityId;
    }
}
