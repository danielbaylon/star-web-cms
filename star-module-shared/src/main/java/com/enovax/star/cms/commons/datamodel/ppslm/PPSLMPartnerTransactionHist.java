package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houtao on 20/8/16.
 */

@Entity
@Table(name = PPSLMPartnerTransactionHist.TABLE_NAME)
public class PPSLMPartnerTransactionHist implements Serializable{

    public static final String TABLE_NAME = "PPSLMPartnerTransactionHist";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "amountCur")
    private BigDecimal amountCur;

    @Column(name = "currencyCode")
    private String currencyCode;

    @Column(name = "documentNum")
    private String documentNum;

    @Column(name = "invoice")
    private String invoice;

    @Column(name = "orderAccount")
    private String orderAccount;

    @Column(name = "paymentReference")
    private String paymentReference;

    @Column(name = "transDate")
    private Date transDate;

    @Column(name = "transType")
    private String transType;

    @Column(name = "voucher")
    private String voucher;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmountCur() {
        return amountCur;
    }

    public void setAmountCur(BigDecimal amountCur) {
        this.amountCur = amountCur;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDocumentNum() {
        return documentNum;
    }

    public void setDocumentNum(String documentNum) {
        this.documentNum = documentNum;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getOrderAccount() {
        return orderAccount;
    }

    public void setOrderAccount(String orderAccount) {
        this.orderAccount = orderAccount;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }
}
