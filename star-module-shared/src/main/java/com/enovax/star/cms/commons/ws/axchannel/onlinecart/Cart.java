
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cart complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cart">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="AffiliationLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfAffiliationLoyaltyTier" minOccurs="0"/>
 *         &lt;element name="AmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AmountPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeValues" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfAttributeValueBase" minOccurs="0"/>
 *         &lt;element name="AvailableDepositAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BeginDateTime" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="BusinessDate" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="CancellationChargeAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CartLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfCartLine" minOccurs="0"/>
 *         &lt;element name="CartStatusValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CartType" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CartType" minOccurs="0"/>
 *         &lt;element name="CartTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ChargeLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfChargeLine" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerAccountDepositAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerOrderModeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DeliveryMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryModeChargeAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountCodes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="EstimatedShippingAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncomeExpenseLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfIncomeExpenseLine" minOccurs="0"/>
 *         &lt;element name="IncomeExpenseTotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCreatedOffline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsDiscountFullyCalculated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsFavorite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsRecurring" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsRequiredAmountPaid" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsReturnByReceipt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LoyaltyCardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModifiedDateTime" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OverriddenDepositAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PrepaymentAmountPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PrepaymentAppliedOnPickup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PromotionLines" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="QuotationExpiryDate" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="ReasonCodeLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfReasonCodeLine" minOccurs="0"/>
 *         &lt;element name="ReceiptEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptTransactionTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RequestedDeliveryDate" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="RequiredDepositAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ReturnTransactionHasLoyaltyPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SalesId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShippingAddress" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}Address" minOccurs="0"/>
 *         &lt;element name="StaffId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubtotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SubtotalAmountWithoutTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxAmountExclusive" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxAmountInclusive" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxOnCancellationCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxOverrideCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxViewLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfTaxViewLine" minOccurs="0"/>
 *         &lt;element name="TenderLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfTenderLine" minOccurs="0"/>
 *         &lt;element name="TerminalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalManualDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalManualDiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="WarehouseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cart", propOrder = {
    "affiliationLines",
    "amountDue",
    "amountPaid",
    "attributeValues",
    "availableDepositAmount",
    "beginDateTime",
    "businessDate",
    "cancellationChargeAmount",
    "cartLines",
    "cartStatusValue",
    "cartType",
    "cartTypeValue",
    "channelId",
    "chargeAmount",
    "chargeLines",
    "comment",
    "customerAccountDepositAmount",
    "customerId",
    "customerOrderModeValue",
    "deliveryMode",
    "deliveryModeChargeAmount",
    "discountAmount",
    "discountCodes",
    "estimatedShippingAmount",
    "id",
    "incomeExpenseLines",
    "incomeExpenseTotalAmount",
    "invoiceComment",
    "isCreatedOffline",
    "isDiscountFullyCalculated",
    "isFavorite",
    "isRecurring",
    "isRequiredAmountPaid",
    "isReturnByReceipt",
    "isSuspended",
    "loyaltyCardId",
    "modifiedDateTime",
    "name",
    "orderNumber",
    "overriddenDepositAmount",
    "prepaymentAmountPaid",
    "prepaymentAppliedOnPickup",
    "promotionLines",
    "quotationExpiryDate",
    "reasonCodeLines",
    "receiptEmail",
    "receiptTransactionTypeValue",
    "requestedDeliveryDate",
    "requiredDepositAmount",
    "returnTransactionHasLoyaltyPayment",
    "salesId",
    "shippingAddress",
    "staffId",
    "subtotalAmount",
    "subtotalAmountWithoutTax",
    "taxAmount",
    "taxAmountExclusive",
    "taxAmountInclusive",
    "taxOnCancellationCharge",
    "taxOverrideCode",
    "taxViewLines",
    "tenderLines",
    "terminalId",
    "totalAmount",
    "totalManualDiscountAmount",
    "totalManualDiscountPercentage",
    "transactionTypeValue",
    "warehouseId"
})
public class Cart
    extends CommerceEntity
{

    @XmlElementRef(name = "AffiliationLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAffiliationLoyaltyTier> affiliationLines;
    @XmlElement(name = "AmountDue")
    protected BigDecimal amountDue;
    @XmlElement(name = "AmountPaid")
    protected BigDecimal amountPaid;
    @XmlElementRef(name = "AttributeValues", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAttributeValueBase> attributeValues;
    @XmlElement(name = "AvailableDepositAmount")
    protected BigDecimal availableDepositAmount;
    @XmlElementRef(name = "BeginDateTime", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> beginDateTime;
    @XmlElementRef(name = "BusinessDate", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> businessDate;
    @XmlElementRef(name = "CancellationChargeAmount", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> cancellationChargeAmount;
    @XmlElementRef(name = "CartLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCartLine> cartLines;
    @XmlElement(name = "CartStatusValue")
    protected Integer cartStatusValue;
    @XmlElement(name = "CartType")
    @XmlSchemaType(name = "string")
    protected CartType cartType;
    @XmlElement(name = "CartTypeValue")
    protected Integer cartTypeValue;
    @XmlElement(name = "ChannelId")
    protected Long channelId;
    @XmlElement(name = "ChargeAmount")
    protected BigDecimal chargeAmount;
    @XmlElementRef(name = "ChargeLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfChargeLine> chargeLines;
    @XmlElementRef(name = "Comment", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> comment;
    @XmlElement(name = "CustomerAccountDepositAmount")
    protected BigDecimal customerAccountDepositAmount;
    @XmlElementRef(name = "CustomerId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerId;
    @XmlElement(name = "CustomerOrderModeValue")
    protected Integer customerOrderModeValue;
    @XmlElementRef(name = "DeliveryMode", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryMode;
    @XmlElementRef(name = "DeliveryModeChargeAmount", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> deliveryModeChargeAmount;
    @XmlElement(name = "DiscountAmount")
    protected BigDecimal discountAmount;
    @XmlElementRef(name = "DiscountCodes", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> discountCodes;
    @XmlElementRef(name = "EstimatedShippingAmount", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> estimatedShippingAmount;
    @XmlElementRef(name = "Id", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> id;
    @XmlElementRef(name = "IncomeExpenseLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfIncomeExpenseLine> incomeExpenseLines;
    @XmlElement(name = "IncomeExpenseTotalAmount")
    protected BigDecimal incomeExpenseTotalAmount;
    @XmlElementRef(name = "InvoiceComment", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceComment;
    @XmlElement(name = "IsCreatedOffline")
    protected Boolean isCreatedOffline;
    @XmlElement(name = "IsDiscountFullyCalculated")
    protected Boolean isDiscountFullyCalculated;
    @XmlElement(name = "IsFavorite")
    protected Boolean isFavorite;
    @XmlElement(name = "IsRecurring")
    protected Boolean isRecurring;
    @XmlElement(name = "IsRequiredAmountPaid")
    protected Boolean isRequiredAmountPaid;
    @XmlElement(name = "IsReturnByReceipt")
    protected Boolean isReturnByReceipt;
    @XmlElement(name = "IsSuspended")
    protected Boolean isSuspended;
    @XmlElementRef(name = "LoyaltyCardId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loyaltyCardId;
    @XmlElementRef(name = "ModifiedDateTime", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> modifiedDateTime;
    @XmlElementRef(name = "Name", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "OrderNumber", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumber;
    @XmlElementRef(name = "OverriddenDepositAmount", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> overriddenDepositAmount;
    @XmlElement(name = "PrepaymentAmountPaid")
    protected BigDecimal prepaymentAmountPaid;
    @XmlElement(name = "PrepaymentAppliedOnPickup")
    protected BigDecimal prepaymentAppliedOnPickup;
    @XmlElementRef(name = "PromotionLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> promotionLines;
    @XmlElementRef(name = "QuotationExpiryDate", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> quotationExpiryDate;
    @XmlElementRef(name = "ReasonCodeLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfReasonCodeLine> reasonCodeLines;
    @XmlElementRef(name = "ReceiptEmail", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> receiptEmail;
    @XmlElement(name = "ReceiptTransactionTypeValue")
    protected Integer receiptTransactionTypeValue;
    @XmlElementRef(name = "RequestedDeliveryDate", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> requestedDeliveryDate;
    @XmlElement(name = "RequiredDepositAmount")
    protected BigDecimal requiredDepositAmount;
    @XmlElement(name = "ReturnTransactionHasLoyaltyPayment")
    protected Boolean returnTransactionHasLoyaltyPayment;
    @XmlElementRef(name = "SalesId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesId;
    @XmlElementRef(name = "ShippingAddress", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> shippingAddress;
    @XmlElementRef(name = "StaffId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> staffId;
    @XmlElement(name = "SubtotalAmount")
    protected BigDecimal subtotalAmount;
    @XmlElement(name = "SubtotalAmountWithoutTax")
    protected BigDecimal subtotalAmountWithoutTax;
    @XmlElement(name = "TaxAmount")
    protected BigDecimal taxAmount;
    @XmlElement(name = "TaxAmountExclusive")
    protected BigDecimal taxAmountExclusive;
    @XmlElement(name = "TaxAmountInclusive")
    protected BigDecimal taxAmountInclusive;
    @XmlElement(name = "TaxOnCancellationCharge")
    protected BigDecimal taxOnCancellationCharge;
    @XmlElementRef(name = "TaxOverrideCode", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxOverrideCode;
    @XmlElementRef(name = "TaxViewLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfTaxViewLine> taxViewLines;
    @XmlElementRef(name = "TenderLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfTenderLine> tenderLines;
    @XmlElementRef(name = "TerminalId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> terminalId;
    @XmlElement(name = "TotalAmount")
    protected BigDecimal totalAmount;
    @XmlElement(name = "TotalManualDiscountAmount")
    protected BigDecimal totalManualDiscountAmount;
    @XmlElement(name = "TotalManualDiscountPercentage")
    protected BigDecimal totalManualDiscountPercentage;
    @XmlElement(name = "TransactionTypeValue")
    protected Integer transactionTypeValue;
    @XmlElementRef(name = "WarehouseId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseId;

    /**
     * Gets the value of the affiliationLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAffiliationLoyaltyTier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAffiliationLoyaltyTier> getAffiliationLines() {
        return affiliationLines;
    }

    /**
     * Sets the value of the affiliationLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAffiliationLoyaltyTier }{@code >}
     *     
     */
    public void setAffiliationLines(JAXBElement<ArrayOfAffiliationLoyaltyTier> value) {
        this.affiliationLines = value;
    }

    /**
     * Gets the value of the amountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountDue() {
        return amountDue;
    }

    /**
     * Sets the value of the amountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountDue(BigDecimal value) {
        this.amountDue = value;
    }

    /**
     * Gets the value of the amountPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    /**
     * Sets the value of the amountPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountPaid(BigDecimal value) {
        this.amountPaid = value;
    }

    /**
     * Gets the value of the attributeValues property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAttributeValueBase }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAttributeValueBase> getAttributeValues() {
        return attributeValues;
    }

    /**
     * Sets the value of the attributeValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAttributeValueBase }{@code >}
     *     
     */
    public void setAttributeValues(JAXBElement<ArrayOfAttributeValueBase> value) {
        this.attributeValues = value;
    }

    /**
     * Gets the value of the availableDepositAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailableDepositAmount() {
        return availableDepositAmount;
    }

    /**
     * Sets the value of the availableDepositAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailableDepositAmount(BigDecimal value) {
        this.availableDepositAmount = value;
    }

    /**
     * Gets the value of the beginDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getBeginDateTime() {
        return beginDateTime;
    }

    /**
     * Sets the value of the beginDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setBeginDateTime(JAXBElement<DateTimeOffset> value) {
        this.beginDateTime = value;
    }

    /**
     * Gets the value of the businessDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getBusinessDate() {
        return businessDate;
    }

    /**
     * Sets the value of the businessDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setBusinessDate(JAXBElement<DateTimeOffset> value) {
        this.businessDate = value;
    }

    /**
     * Gets the value of the cancellationChargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getCancellationChargeAmount() {
        return cancellationChargeAmount;
    }

    /**
     * Sets the value of the cancellationChargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setCancellationChargeAmount(JAXBElement<BigDecimal> value) {
        this.cancellationChargeAmount = value;
    }

    /**
     * Gets the value of the cartLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCartLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCartLine> getCartLines() {
        return cartLines;
    }

    /**
     * Sets the value of the cartLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCartLine }{@code >}
     *     
     */
    public void setCartLines(JAXBElement<ArrayOfCartLine> value) {
        this.cartLines = value;
    }

    /**
     * Gets the value of the cartStatusValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCartStatusValue() {
        return cartStatusValue;
    }

    /**
     * Sets the value of the cartStatusValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCartStatusValue(Integer value) {
        this.cartStatusValue = value;
    }

    /**
     * Gets the value of the cartType property.
     * 
     * @return
     *     possible object is
     *     {@link CartType }
     *     
     */
    public CartType getCartType() {
        return cartType;
    }

    /**
     * Sets the value of the cartType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CartType }
     *     
     */
    public void setCartType(CartType value) {
        this.cartType = value;
    }

    /**
     * Gets the value of the cartTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCartTypeValue() {
        return cartTypeValue;
    }

    /**
     * Sets the value of the cartTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCartTypeValue(Integer value) {
        this.cartTypeValue = value;
    }

    /**
     * Gets the value of the channelId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getChannelId() {
        return channelId;
    }

    /**
     * Sets the value of the channelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setChannelId(Long value) {
        this.channelId = value;
    }

    /**
     * Gets the value of the chargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    /**
     * Sets the value of the chargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargeAmount(BigDecimal value) {
        this.chargeAmount = value;
    }

    /**
     * Gets the value of the chargeLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfChargeLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfChargeLine> getChargeLines() {
        return chargeLines;
    }

    /**
     * Sets the value of the chargeLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfChargeLine }{@code >}
     *     
     */
    public void setChargeLines(JAXBElement<ArrayOfChargeLine> value) {
        this.chargeLines = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComment(JAXBElement<String> value) {
        this.comment = value;
    }

    /**
     * Gets the value of the customerAccountDepositAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCustomerAccountDepositAmount() {
        return customerAccountDepositAmount;
    }

    /**
     * Sets the value of the customerAccountDepositAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCustomerAccountDepositAmount(BigDecimal value) {
        this.customerAccountDepositAmount = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerId(JAXBElement<String> value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the customerOrderModeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustomerOrderModeValue() {
        return customerOrderModeValue;
    }

    /**
     * Sets the value of the customerOrderModeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustomerOrderModeValue(Integer value) {
        this.customerOrderModeValue = value;
    }

    /**
     * Gets the value of the deliveryMode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * Sets the value of the deliveryMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryMode(JAXBElement<String> value) {
        this.deliveryMode = value;
    }

    /**
     * Gets the value of the deliveryModeChargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    /**
     * Sets the value of the deliveryModeChargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setDeliveryModeChargeAmount(JAXBElement<BigDecimal> value) {
        this.deliveryModeChargeAmount = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the discountCodes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getDiscountCodes() {
        return discountCodes;
    }

    /**
     * Sets the value of the discountCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setDiscountCodes(JAXBElement<ArrayOfstring> value) {
        this.discountCodes = value;
    }

    /**
     * Gets the value of the estimatedShippingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    /**
     * Sets the value of the estimatedShippingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setEstimatedShippingAmount(JAXBElement<BigDecimal> value) {
        this.estimatedShippingAmount = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = value;
    }

    /**
     * Gets the value of the incomeExpenseLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIncomeExpenseLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfIncomeExpenseLine> getIncomeExpenseLines() {
        return incomeExpenseLines;
    }

    /**
     * Sets the value of the incomeExpenseLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIncomeExpenseLine }{@code >}
     *     
     */
    public void setIncomeExpenseLines(JAXBElement<ArrayOfIncomeExpenseLine> value) {
        this.incomeExpenseLines = value;
    }

    /**
     * Gets the value of the incomeExpenseTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIncomeExpenseTotalAmount() {
        return incomeExpenseTotalAmount;
    }

    /**
     * Sets the value of the incomeExpenseTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIncomeExpenseTotalAmount(BigDecimal value) {
        this.incomeExpenseTotalAmount = value;
    }

    /**
     * Gets the value of the invoiceComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvoiceComment() {
        return invoiceComment;
    }

    /**
     * Sets the value of the invoiceComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvoiceComment(JAXBElement<String> value) {
        this.invoiceComment = value;
    }

    /**
     * Gets the value of the isCreatedOffline property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCreatedOffline() {
        return isCreatedOffline;
    }

    /**
     * Sets the value of the isCreatedOffline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCreatedOffline(Boolean value) {
        this.isCreatedOffline = value;
    }

    /**
     * Gets the value of the isDiscountFullyCalculated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    /**
     * Sets the value of the isDiscountFullyCalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDiscountFullyCalculated(Boolean value) {
        this.isDiscountFullyCalculated = value;
    }

    /**
     * Gets the value of the isFavorite property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFavorite() {
        return isFavorite;
    }

    /**
     * Sets the value of the isFavorite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFavorite(Boolean value) {
        this.isFavorite = value;
    }

    /**
     * Gets the value of the isRecurring property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRecurring() {
        return isRecurring;
    }

    /**
     * Sets the value of the isRecurring property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRecurring(Boolean value) {
        this.isRecurring = value;
    }

    /**
     * Gets the value of the isRequiredAmountPaid property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    /**
     * Sets the value of the isRequiredAmountPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRequiredAmountPaid(Boolean value) {
        this.isRequiredAmountPaid = value;
    }

    /**
     * Gets the value of the isReturnByReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReturnByReceipt() {
        return isReturnByReceipt;
    }

    /**
     * Sets the value of the isReturnByReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReturnByReceipt(Boolean value) {
        this.isReturnByReceipt = value;
    }

    /**
     * Gets the value of the isSuspended property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSuspended() {
        return isSuspended;
    }

    /**
     * Sets the value of the isSuspended property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSuspended(Boolean value) {
        this.isSuspended = value;
    }

    /**
     * Gets the value of the loyaltyCardId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoyaltyCardId() {
        return loyaltyCardId;
    }

    /**
     * Sets the value of the loyaltyCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoyaltyCardId(JAXBElement<String> value) {
        this.loyaltyCardId = value;
    }

    /**
     * Gets the value of the modifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * Sets the value of the modifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setModifiedDateTime(JAXBElement<DateTimeOffset> value) {
        this.modifiedDateTime = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumber(JAXBElement<String> value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the overriddenDepositAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    /**
     * Sets the value of the overriddenDepositAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setOverriddenDepositAmount(JAXBElement<BigDecimal> value) {
        this.overriddenDepositAmount = value;
    }

    /**
     * Gets the value of the prepaymentAmountPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    /**
     * Sets the value of the prepaymentAmountPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrepaymentAmountPaid(BigDecimal value) {
        this.prepaymentAmountPaid = value;
    }

    /**
     * Gets the value of the prepaymentAppliedOnPickup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrepaymentAppliedOnPickup() {
        return prepaymentAppliedOnPickup;
    }

    /**
     * Sets the value of the prepaymentAppliedOnPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrepaymentAppliedOnPickup(BigDecimal value) {
        this.prepaymentAppliedOnPickup = value;
    }

    /**
     * Gets the value of the promotionLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getPromotionLines() {
        return promotionLines;
    }

    /**
     * Sets the value of the promotionLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setPromotionLines(JAXBElement<ArrayOfstring> value) {
        this.promotionLines = value;
    }

    /**
     * Gets the value of the quotationExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getQuotationExpiryDate() {
        return quotationExpiryDate;
    }

    /**
     * Sets the value of the quotationExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setQuotationExpiryDate(JAXBElement<DateTimeOffset> value) {
        this.quotationExpiryDate = value;
    }

    /**
     * Gets the value of the reasonCodeLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfReasonCodeLine> getReasonCodeLines() {
        return reasonCodeLines;
    }

    /**
     * Sets the value of the reasonCodeLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public void setReasonCodeLines(JAXBElement<ArrayOfReasonCodeLine> value) {
        this.reasonCodeLines = value;
    }

    /**
     * Gets the value of the receiptEmail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReceiptEmail() {
        return receiptEmail;
    }

    /**
     * Sets the value of the receiptEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReceiptEmail(JAXBElement<String> value) {
        this.receiptEmail = value;
    }

    /**
     * Gets the value of the receiptTransactionTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptTransactionTypeValue() {
        return receiptTransactionTypeValue;
    }

    /**
     * Sets the value of the receiptTransactionTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptTransactionTypeValue(Integer value) {
        this.receiptTransactionTypeValue = value;
    }

    /**
     * Gets the value of the requestedDeliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    /**
     * Sets the value of the requestedDeliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setRequestedDeliveryDate(JAXBElement<DateTimeOffset> value) {
        this.requestedDeliveryDate = value;
    }

    /**
     * Gets the value of the requiredDepositAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRequiredDepositAmount() {
        return requiredDepositAmount;
    }

    /**
     * Sets the value of the requiredDepositAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRequiredDepositAmount(BigDecimal value) {
        this.requiredDepositAmount = value;
    }

    /**
     * Gets the value of the returnTransactionHasLoyaltyPayment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnTransactionHasLoyaltyPayment() {
        return returnTransactionHasLoyaltyPayment;
    }

    /**
     * Sets the value of the returnTransactionHasLoyaltyPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnTransactionHasLoyaltyPayment(Boolean value) {
        this.returnTransactionHasLoyaltyPayment = value;
    }

    /**
     * Gets the value of the salesId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesId() {
        return salesId;
    }

    /**
     * Sets the value of the salesId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesId(JAXBElement<String> value) {
        this.salesId = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setShippingAddress(JAXBElement<Address> value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the staffId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStaffId() {
        return staffId;
    }

    /**
     * Sets the value of the staffId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStaffId(JAXBElement<String> value) {
        this.staffId = value;
    }

    /**
     * Gets the value of the subtotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubtotalAmount() {
        return subtotalAmount;
    }

    /**
     * Sets the value of the subtotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubtotalAmount(BigDecimal value) {
        this.subtotalAmount = value;
    }

    /**
     * Gets the value of the subtotalAmountWithoutTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    /**
     * Sets the value of the subtotalAmountWithoutTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubtotalAmountWithoutTax(BigDecimal value) {
        this.subtotalAmountWithoutTax = value;
    }

    /**
     * Gets the value of the taxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets the value of the taxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmount(BigDecimal value) {
        this.taxAmount = value;
    }

    /**
     * Gets the value of the taxAmountExclusive property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmountExclusive() {
        return taxAmountExclusive;
    }

    /**
     * Sets the value of the taxAmountExclusive property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmountExclusive(BigDecimal value) {
        this.taxAmountExclusive = value;
    }

    /**
     * Gets the value of the taxAmountInclusive property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmountInclusive() {
        return taxAmountInclusive;
    }

    /**
     * Sets the value of the taxAmountInclusive property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmountInclusive(BigDecimal value) {
        this.taxAmountInclusive = value;
    }

    /**
     * Gets the value of the taxOnCancellationCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    /**
     * Sets the value of the taxOnCancellationCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxOnCancellationCharge(BigDecimal value) {
        this.taxOnCancellationCharge = value;
    }

    /**
     * Gets the value of the taxOverrideCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxOverrideCode() {
        return taxOverrideCode;
    }

    /**
     * Sets the value of the taxOverrideCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxOverrideCode(JAXBElement<String> value) {
        this.taxOverrideCode = value;
    }

    /**
     * Gets the value of the taxViewLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTaxViewLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfTaxViewLine> getTaxViewLines() {
        return taxViewLines;
    }

    /**
     * Sets the value of the taxViewLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTaxViewLine }{@code >}
     *     
     */
    public void setTaxViewLines(JAXBElement<ArrayOfTaxViewLine> value) {
        this.taxViewLines = value;
    }

    /**
     * Gets the value of the tenderLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTenderLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfTenderLine> getTenderLines() {
        return tenderLines;
    }

    /**
     * Sets the value of the tenderLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTenderLine }{@code >}
     *     
     */
    public void setTenderLines(JAXBElement<ArrayOfTenderLine> value) {
        this.tenderLines = value;
    }

    /**
     * Gets the value of the terminalId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTerminalId() {
        return terminalId;
    }

    /**
     * Sets the value of the terminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTerminalId(JAXBElement<String> value) {
        this.terminalId = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the totalManualDiscountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    /**
     * Sets the value of the totalManualDiscountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalManualDiscountAmount(BigDecimal value) {
        this.totalManualDiscountAmount = value;
    }

    /**
     * Gets the value of the totalManualDiscountPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    /**
     * Sets the value of the totalManualDiscountPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalManualDiscountPercentage(BigDecimal value) {
        this.totalManualDiscountPercentage = value;
    }

    /**
     * Gets the value of the transactionTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionTypeValue() {
        return transactionTypeValue;
    }

    /**
     * Sets the value of the transactionTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionTypeValue(Integer value) {
        this.transactionTypeValue = value;
    }

    /**
     * Gets the value of the warehouseId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseId() {
        return warehouseId;
    }

    /**
     * Sets the value of the warehouseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseId(JAXBElement<String> value) {
        this.warehouseId = value;
    }

}
