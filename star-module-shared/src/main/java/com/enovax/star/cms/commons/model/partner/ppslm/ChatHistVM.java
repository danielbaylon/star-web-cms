package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.text.ParseException;
import java.util.Date;

public class ChatHistVM  implements Comparable<ChatHistVM> {
    private String username;
    private String usertype;
    private String content;
    private String createDtStr;
    
    public ChatHistVM(){}
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateDtStr() {
		return createDtStr;
	}
	public void setCreateDtStr(String createDtStr) {
		this.createDtStr = createDtStr;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	@Override
	public int compareTo(ChatHistVM o) {
		try {
			Date iDt = NvxDateUtils.parseDate(this.createDtStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
			Date oDt = NvxDateUtils.parseDate(o.createDtStr, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
			return iDt.compareTo(oDt);
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
}
