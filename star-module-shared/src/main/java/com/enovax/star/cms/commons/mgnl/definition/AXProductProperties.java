package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/6/16.
 */
public enum AXProductProperties {
    
    ProductListingId("productListingId"),
    DisplayProductNumber("displayProductNumber"),
    ProductSearchName("productSearchName"),
    ProductName("productName"),
    Channel("channel"),
    Status("status"),
    ProductPrice("productPrice"),
    ItemName("itemName"),
    Description("description"),
    Promotion("promotion"),
    CrossSell("cross-sell"),
    PromotionIcon("promotionIcon"),
    CrossSellImage("crossSellItemImage"),
    MediaTypeDescription("mediaTypeDescription"),
    MediaTypeId("mediaTypeId"),
    IsCapacity("isCapacity"),
    OwnAttraction("ownAttraction"),
    TemplateName("templateName"),
    RelatedPromotionUUID("relatedPromotionUUID"),
    SystemStatus("systemStatus"),
	TicketType("ticketType"),
    NoOfPax("noOfPax"),
    Printing("printing"),
    IsPackage("isPackage"),
    PublishedPrice("publishedPrice"),
    ValidFrom("validFrom"),
    ValidTo("validTo");

    private String propertyName;

    AXProductProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }


//    promotion
//      relatedPromotionUUID
//      systemStatus


}
