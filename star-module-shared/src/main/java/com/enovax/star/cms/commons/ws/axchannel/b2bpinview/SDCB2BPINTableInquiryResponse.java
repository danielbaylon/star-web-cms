
package com.enovax.star.cms.commons.ws.axchannel.b2bpinview;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_B2BPINTableInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_B2BPINTableInquiryResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="b2bPinViewTableInquiry" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_B2BPINViewTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_B2BPINTableInquiryResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "b2BPinViewTableInquiry"
})
public class SDCB2BPINTableInquiryResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "b2bPinViewTableInquiry", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINViewTable> b2BPinViewTableInquiry;

    /**
     * Gets the value of the b2BPinViewTableInquiry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINViewTable> getB2BPinViewTableInquiry() {
        return b2BPinViewTableInquiry;
    }

    /**
     * Sets the value of the b2BPinViewTableInquiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}
     *     
     */
    public void setB2BPinViewTableInquiry(JAXBElement<SDCB2BPINViewTable> value) {
        this.b2BPinViewTableInquiry = value;
    }

}
