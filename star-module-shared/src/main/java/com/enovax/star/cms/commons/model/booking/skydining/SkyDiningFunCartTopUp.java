package com.enovax.star.cms.commons.model.booking.skydining;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCartTopUp {
  private String topUpId;
  private String topUpName;
  private int qty;

  public String getTopUpId() {
    return topUpId;
  }

  public void setTopUpId(String topUpId) {
    this.topUpId = topUpId;
  }

  public String getTopUpName() {
    return topUpName;
  }

  public void setTopUpName(String topUpName) {
    this.topUpName = topUpName;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }
}
