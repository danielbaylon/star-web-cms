package com.enovax.star.cms.commons.datamodel.b2cmflg;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = B2CMFLGStoreTransaction.TABLE_NAME)
public class B2CMFLGStoreTransaction {

    public static final String TABLE_NAME = "B2CMFLGStoreTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction")
    private List<B2CMFLGStoreTransactionItem> items = new ArrayList<>();

    @Column(name = "receiptNum", nullable = false)
    private String receiptNum;

    @Column(name = "custName")
    private String custName;

    @Column(name = "custEmail")
    private String custEmail;

    @Column(name = "custIdType")
    private String custIdType;

    @Column(name = "custIdNo")
    private String custIdNo;

    @Column(name = "custMobile")
    private String custMobile;

    @Column(name = "custSubscribe")
    private boolean custSubscribe;

    @Column(name = "custNationality")
    private String custNationality;

    @Column(name = "custReferSource")
    private String custReferSource;

    @Column(name = "custDob")
    private String custDob;

    @Column(name = "custSalutation")
    private String custSalutation;

    @Column(name = "custCompanyName")
    private String custCompanyName;

    @Column(name = "custJewelCard")
    private String custJewelCard;

    @Column(name = "totalAmount", nullable = false)
    private BigDecimal totalAmount;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "tmMerchantId", nullable = false)
    private String tmMerchantId;

    @Column(name = "stage", nullable = false)
    private String stage;

    @Column(name = "tmStatus", nullable = false)
    private String tmStatus;

    @Column(name="tmError")
    private String tmError;

    @Column(name = "tmErrorMessage", nullable = false)
    private String tmErrorMessage;

    @Column(name = "tmApprovalCode", nullable = false)
    private String tmApprovalCode;

    @Column(name = "statusDate", nullable = false)
    private Date statusDate;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "modifiedDate", nullable = false)
    private Date modifiedDate;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "trafficSource")
    private String trafficSource;

    @Column(name = "ip")
    private String ip;

    @Column(name = "maskedCc")
    private String maskedCc;

    @Column(name = "paymentType", nullable = false)
    private String paymentType;

    @Column(name = "langCode")
    private String langCode;

    @Column(name = "pinCode")
    private String pinCode;

    @Column(name = "ticketMedia")
    private String ticketMedia;

    @Column(name = "promoCodes")
    private String promoCodes;

    @Column(name = "affiliations")
    private String affiliations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustIdType() {
        return custIdType;
    }

    public void setCustIdType(String custIdType) {
        this.custIdType = custIdType;
    }

    public String getCustIdNo() {
        return custIdNo;
    }

    public void setCustIdNo(String custIdNo) {
        this.custIdNo = custIdNo;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public boolean isCustSubscribe() {
        return custSubscribe;
    }

    public void setCustSubscribe(boolean custSubscribe) {
        this.custSubscribe = custSubscribe;
    }

    public String getCustNationality() {
        return custNationality;
    }

    public void setCustNationality(String custNationality) {
        this.custNationality = custNationality;
    }

    public String getCustReferSource() {
        return custReferSource;
    }

    public void setCustReferSource(String custReferSource) {
        this.custReferSource = custReferSource;
    }

    public String getCustDob() {
        return custDob;
    }

    public void setCustDob(String custDob) {
        this.custDob = custDob;
    }

    public String getCustSalutation() {
        return custSalutation;
    }

    public void setCustSalutation(String custSalutation) {
        this.custSalutation = custSalutation;
    }

    public String getCustCompanyName() {
        return custCompanyName;
    }

    public void setCustCompanyName(String custCompanyName) {
        this.custCompanyName = custCompanyName;
    }

    public String getCustJewelCard() {
        return custJewelCard;
    }

    public void setCustJewelCard(String custJewelCard) {
        this.custJewelCard = custJewelCard;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMaskedCc() {
        return maskedCc;
    }

    public void setMaskedCc(String maskedCc) {
        this.maskedCc = maskedCc;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTicketMedia() {
        return ticketMedia;
    }

    public void setTicketMedia(String ticketMedia) {
        this.ticketMedia = ticketMedia;
    }

    public String getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(String promoCodes) {
        this.promoCodes = promoCodes;
    }

    public String getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(String affiliations) {
        this.affiliations = affiliations;
    }

    public String getTmError() {
        return tmError;
    }

    public void setTmError(String tmError) {
        this.tmError = tmError;
    }

    public List<B2CMFLGStoreTransactionItem> getItems() {
        return items;
    }

    public void setItems(List<B2CMFLGStoreTransactionItem> items) {
        this.items = items;
    }
}
