package com.enovax.star.cms.commons.model.axstar;

import java.util.Date;

/**
 * Created by jennylynsze on 8/18/16.
 */
public class AxStarCartLineComment {
    private String cmsProductId;

    private Date eventDate;
    private String eventLineId;
    private String eventGroupId;

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getCmsProductId() {
        return cmsProductId;
    }

    public void setCmsProductId(String cmsProductId) {
        this.cmsProductId = cmsProductId;
    }
}
