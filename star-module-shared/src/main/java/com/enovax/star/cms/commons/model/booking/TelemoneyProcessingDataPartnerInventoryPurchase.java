package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;

/**
 * Created by jennylynsze on 11/15/16.
 */
public class TelemoneyProcessingDataPartnerInventoryPurchase extends TelemoneyProcessingData {
    private boolean slm;
    private PPSLMInventoryTransaction slmTxn;
    private PPMFLGInventoryTransaction mflgTxn;
//    private B2CMFLGStoreTransaction mflgTxn;
    private InventoryTransactionVM txn;
    private PPSLMTAMainAccount mainAccount;
    private PPSLMTASubAccount subAccount;
    private com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount mflgMainAccount;
    private com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount mflgSubAccount;
    private boolean isSubAccount;
    private String email;

    public boolean isSlm() {
        return slm;
    }

    public void setSlm(boolean slm) {
        this.slm = slm;
    }

    public PPSLMInventoryTransaction getSlmTxn() {
        return slmTxn;
    }

    public void setSlmTxn(PPSLMInventoryTransaction slmTxn) {
        this.slmTxn = slmTxn;
    }

    public InventoryTransactionVM getTxn() {
        return txn;
    }

    public void setTxn(InventoryTransactionVM txn) {
        this.txn = txn;
    }

    public PPSLMTAMainAccount getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(PPSLMTAMainAccount mainAccount) {
        this.mainAccount = mainAccount;
    }

    public PPSLMTASubAccount getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(PPSLMTASubAccount subAccount) {
        this.subAccount = subAccount;
    }

    public boolean isSubAccount() {
        return isSubAccount;
    }

    public void setIsSubAccount(boolean isSubAccount) {
        this.isSubAccount = isSubAccount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PPMFLGInventoryTransaction getMflgTxn() {
        return mflgTxn;
    }

    public void setMflgTxn(PPMFLGInventoryTransaction mflgTxn) {
        this.mflgTxn = mflgTxn;
    }

    public PPMFLGTAMainAccount getMflgMainAccount() {
        return mflgMainAccount;
    }

    public void setMflgMainAccount(PPMFLGTAMainAccount mflgMainAccount) {
        this.mflgMainAccount = mflgMainAccount;
    }

    public PPMFLGTASubAccount getMflgSubAccount() {
        return mflgSubAccount;
    }

    public void setMflgSubAccount(PPMFLGTASubAccount mflgSubAccount) {
        this.mflgSubAccount = mflgSubAccount;
    }

    public void setSubAccount(boolean subAccount) {
        isSubAccount = subAccount;
    }
}
