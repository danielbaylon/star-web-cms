package com.enovax.star.cms.commons.util;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonUtil {

    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

    protected static Gson gson = new Gson();
    protected static Gson gsonHtml = new GsonBuilder().disableHtmlEscaping().create();
    protected static Gson gsonUpperCamelCase = new  GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

    public static void setGson(Gson newGson, boolean html) {
        if (html) gsonHtml = newGson;
        else gson = newGson;
    }

    public static Gson getGson(boolean html) {
        return html ? gsonHtml : gson;
    }

    public Gson getGson(boolean html, String dateTimeFormat, FieldNamingPolicy policy) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        if(!html){
            gsonBuilder.disableHtmlEscaping();
        }
        gsonBuilder = gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                String dateString = json.getAsString();
                if(dateString != null && dateString.trim().length() > 0){
                    try{
                        if(dateString.lastIndexOf('.') > 0 && dateString.lastIndexOf('+') > 0 && dateString.lastIndexOf('+') > dateString.lastIndexOf('.')){
                            String noOfMillionSeconds = dateString.substring(dateString.lastIndexOf("."), dateString.lastIndexOf("+")+1);
                            if(noOfMillionSeconds.startsWith(".") && noOfMillionSeconds.endsWith("+")){
                                int msl = noOfMillionSeconds.length() - 2;
                                StringBuilder ms = new StringBuilder("");
                                for(int i = 0 ; i < msl; i ++){
                                    ms.append("S");
                                }
                                String newFmt = dateTimeFormat.replace("{MS}", ms.toString());
                                ms = null;
                                SimpleDateFormat sdf = new SimpleDateFormat(newFmt);
                                Date date = sdf.parse(dateString);
                                sdf = null;
                                return date;
                            }
                        }
                    }catch (Exception ex){
                        logger.error("Parsing date ["+dateString+"] failed.", ex);
                        throw new JsonParseException(ex);
                    }
                }
                return null;
            }
        });
        if(policy != null){
            gsonBuilder.setFieldNamingPolicy(policy);
        }
        Gson gson = gsonBuilder.serializeNulls().create();

        gsonBuilder = null;
        return gson;
    }

    public static String jsonify(Object bagOfPrimitives) {
        return jsonify(bagOfPrimitives, true);
    }

    public static String jsonify(Object bagOfPrimitives, boolean escapeHtml) {
        return escapeHtml ? gson.toJson(bagOfPrimitives) : gsonHtml.toJson(bagOfPrimitives);
    }

    public static String jsonify(Object bagOfPrimitives, Type typeOfSrc) {
        return gson.toJson(bagOfPrimitives, typeOfSrc);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return fromJson(json, true, clazz);
    }

    public static <T> T fromJson(String json, boolean escapeHtml, Class<T> clazz) {
        return escapeHtml ? gson.fromJson(json, clazz) : gsonHtml.fromJson(json, clazz);
    }

    public static <T> T fromJsonUpperCamelCase(String json, Class<T> clazz) {
        return gsonUpperCamelCase.fromJson(json, clazz);
    }
}
