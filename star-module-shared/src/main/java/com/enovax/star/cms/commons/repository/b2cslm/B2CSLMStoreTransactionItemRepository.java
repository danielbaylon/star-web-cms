package com.enovax.star.cms.commons.repository.b2cslm;

import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jensen on 13/11/16.
 */
public interface B2CSLMStoreTransactionItemRepository extends JpaRepository<B2CSLMStoreTransactionItem, Integer> {
}
