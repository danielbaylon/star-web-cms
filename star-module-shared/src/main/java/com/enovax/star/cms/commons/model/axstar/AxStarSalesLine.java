package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 8/17/16.
 */
public class AxStarSalesLine {

    //TODO Need to add more fields

    private String lineId;
    private String description;
    private String originLineId;
    private String taxOverrideCode = ""; //Nullable
    private Long productId;
    private String barcode;
    private Long masterProductId;
    private Long listingId;
    private boolean isPriceOverridden;
    private BigDecimal originalPrice;
    private BigDecimal totalAmount;
    private BigDecimal netAmountWithoutTax;
    private BigDecimal discountAmount;
    private BigDecimal totalDiscount;
    private BigDecimal totalPercentageDiscount;
    private BigDecimal lineDiscount;
    private BigDecimal periodicDiscount;
    private BigDecimal lineManualDiscountAmount;
    private BigDecimal lineManualDiscountPercentage;
//    private String shippingAddress;
    private String deliveryMode;
    private String comment;
    private String requestedDeliveryDate;
    private String inventoryLocationId;
    private String inventoryDimensionId;
    private Integer itemType;
    private boolean scaleItem;
    private String reservationId;
    private Integer lineNumber;
    private Integer returnQuantity;
    private Integer statusValue;
    private Integer salesStatusValue;
    private Integer productSourceValue;
    /*
      "IsGiftCardLine": false,
      "GiftCardId": "",
      "GiftCardCurrencyCode": "",
      "GiftCardOperationValue": 0,
     */
    private boolean isInvoiceLine;
    private String invoiceId;
    private BigDecimal invoiceAmount;
    private boolean isInvoiceSettled;
    private boolean isPriceLocked;
    private boolean isVoided;
//    private String chargeLines;
    /*

      "BasePrice": 0,
      "AgreementPrice": 0,
      "AdjustedPrice": 0,
      "ReturnTransactionId": "",
      "ReturnLineNumber": 0,
      "ReturnInventTransId": "",
      "ReturnStore": "",
      "ReturnTerminalId": "",
      "ReturnChannelId": 5637146827,
      "Store": "",
      "TerminalId": "",
      "SalesDate": null,
      "QuantityInvoiced": null,
      "QuantityOrdered": null,
      "RecordId": 0,
      "SerialNumber": "",
      "BatchId": "",
      "DeliveryModeChargeAmount": null,
      "UnitOfMeasureSymbol": null,
      "CatalogId": 0,
      "ElectronicDeliveryEmailAddress": "",
      "ElectronicDeliveryEmailContent": "",
      "LoyaltyDiscountAmount": 0,
      "LoyaltyPercentageDiscount": 0,
      "IsCustomerAccountDeposit": false,
      "Blocked": false,
      "Found": false,
      "DateToActivateItem": "0001-01-01T00:00:00+00:00",
      "LinePercentageDiscount": 0,
      "PeriodicPercentageDiscount": 0,
      "QuantityDiscounted": 0,
      "UnitQuantity": 0,
      "UnitOfMeasureConversion": {
        "ItemId": null,
        "FromUnitOfMeasureId": null,
        "ToUnitOfMeasureId": null,
        "FromUnitOfMeasureSymbol": null,
        "ToUnitOfMeasureSymbol": null,
        "IsBackward": false,
        "RecordId": 0,
        "ProductRecordId": 0,
        "Factor": 1,
        "Numerator": 1,
        "Denominator": 1,
        "InnerOffset": 0,
        "OuterOffset": 0,
        "ExtensionProperties": []
      },
      "DiscountLines": [],
      "PeriodicDiscountPossibilities": [],
      "ReasonCodeLines": [],
      "ReturnLabelProperties": null,
      "LineMultilineDiscOnItem": 0,
      "LineIdsLinkedProductMap": {},
      "LinkedParentLineId": null,
      "LineMultilineDiscOnItemValue": 0,
      "WasChanged": false,
      "OriginalSalesOrderUnitOfMeasure": null,
      "InventOrderUnitOfMeasure": null,
      "IsLoyaltyDiscountApplied": false,
      */

    private String itemId;
    private Integer quantity;
    private BigDecimal price;
    private String itemTaxGroupId;
    private String salesTaxGroupId;
    private BigDecimal taxAmount;
//    "SalesOrderUnitOfMeasure": null,
    private BigDecimal netAmount;
    private BigDecimal netAmountPerUnit;
    private BigDecimal grossAmount;

    /*
      "TaxLines": [],
      "TaxAmountExemptInclusive": 0,
      "TaxAmountInclusive": 0,
      "TaxAmountExclusive": 0,
      "NetAmountWithAllInclusiveTax": 10,
      "BeginDateTime": "0001-01-01T00:00:00+00:00",
      "EndDateTime": "0001-01-01T00:00:00+00:00",
      "TaxRatePercent": 0,
      "IsReturnByReceipt": false,
      "ExtensionProperties": [
        {
          "Key": "TRANSACTIONID",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": "18aug001"
          }
        },
        {
          "Key": "UNIT",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": "ea"
          }
        },
        {
          "Key": "INVENTORYSITEID",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": ""
          }
        },
        {
          "Key": "LOGISTICSPOSTALADDRESS",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": 0,
            "StringValue": null
          }
        }
      ]
     */

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginLineId() {
        return originLineId;
    }

    public void setOriginLineId(String originLineId) {
        this.originLineId = originLineId;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Long getMasterProductId() {
        return masterProductId;
    }

    public void setMasterProductId(Long masterProductId) {
        this.masterProductId = masterProductId;
    }

    public Long getListingId() {
        return listingId;
    }

    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }

    public boolean isPriceOverridden() {
        return isPriceOverridden;
    }

    public void setPriceOverridden(boolean priceOverridden) {
        isPriceOverridden = priceOverridden;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getNetAmountWithoutTax() {
        return netAmountWithoutTax;
    }

    public void setNetAmountWithoutTax(BigDecimal netAmountWithoutTax) {
        this.netAmountWithoutTax = netAmountWithoutTax;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getTotalPercentageDiscount() {
        return totalPercentageDiscount;
    }

    public void setTotalPercentageDiscount(BigDecimal totalPercentageDiscount) {
        this.totalPercentageDiscount = totalPercentageDiscount;
    }

    public BigDecimal getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(BigDecimal lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public BigDecimal getPeriodicDiscount() {
        return periodicDiscount;
    }

    public void setPeriodicDiscount(BigDecimal periodicDiscount) {
        this.periodicDiscount = periodicDiscount;
    }

    public BigDecimal getLineManualDiscountAmount() {
        return lineManualDiscountAmount;
    }

    public void setLineManualDiscountAmount(BigDecimal lineManualDiscountAmount) {
        this.lineManualDiscountAmount = lineManualDiscountAmount;
    }

    public BigDecimal getLineManualDiscountPercentage() {
        return lineManualDiscountPercentage;
    }

    public void setLineManualDiscountPercentage(BigDecimal lineManualDiscountPercentage) {
        this.lineManualDiscountPercentage = lineManualDiscountPercentage;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getInventoryLocationId() {
        return inventoryLocationId;
    }

    public void setInventoryLocationId(String inventoryLocationId) {
        this.inventoryLocationId = inventoryLocationId;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public boolean isScaleItem() {
        return scaleItem;
    }

    public void setScaleItem(boolean scaleItem) {
        this.scaleItem = scaleItem;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public Integer getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(Integer statusValue) {
        this.statusValue = statusValue;
    }

    public Integer getSalesStatusValue() {
        return salesStatusValue;
    }

    public void setSalesStatusValue(Integer salesStatusValue) {
        this.salesStatusValue = salesStatusValue;
    }

    public Integer getProductSourceValue() {
        return productSourceValue;
    }

    public void setProductSourceValue(Integer productSourceValue) {
        this.productSourceValue = productSourceValue;
    }

    public boolean isInvoiceLine() {
        return isInvoiceLine;
    }

    public void setInvoiceLine(boolean invoiceLine) {
        isInvoiceLine = invoiceLine;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public boolean isInvoiceSettled() {
        return isInvoiceSettled;
    }

    public void setInvoiceSettled(boolean invoiceSettled) {
        isInvoiceSettled = invoiceSettled;
    }

    public boolean isPriceLocked() {
        return isPriceLocked;
    }

    public void setPriceLocked(boolean priceLocked) {
        isPriceLocked = priceLocked;
    }

    public boolean isVoided() {
        return isVoided;
    }

    public void setVoided(boolean voided) {
        isVoided = voided;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemTaxGroupId() {
        return itemTaxGroupId;
    }

    public void setItemTaxGroupId(String itemTaxGroupId) {
        this.itemTaxGroupId = itemTaxGroupId;
    }

    public String getSalesTaxGroupId() {
        return salesTaxGroupId;
    }

    public void setSalesTaxGroupId(String salesTaxGroupId) {
        this.salesTaxGroupId = salesTaxGroupId;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public BigDecimal getNetAmountPerUnit() {
        return netAmountPerUnit;
    }

    public void setNetAmountPerUnit(BigDecimal netAmountPerUnit) {
        this.netAmountPerUnit = netAmountPerUnit;
    }

    public BigDecimal getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(BigDecimal grossAmount) {
        this.grossAmount = grossAmount;
    }

    /*
    "SalesLines": [
    {
      "LineId": null,
      "Description": null,
      "OriginLineId": null,
      "TaxOverrideCode": null,
      "ProductId": 5637145352,
      "Barcode": "",
      "MasterProductId": 0,
      "ListingId": 5637145352,
      "IsPriceOverridden": false,
      "OriginalPrice": 0,
      "TotalAmount": 10,
      "NetAmountWithoutTax": 0,
      "DiscountAmount": 0,
      "TotalDiscount": 0,
      "TotalPercentageDiscount": 0,
      "LineDiscount": 0,
      "PeriodicDiscount": 0,
      "LineManualDiscountPercentage": 0,
      "LineManualDiscountAmount": 0,
      "ShippingAddress": {
        "Name": "SDCB2C",
        "FullAddress": "",
        "RecordId": 0,
        "Street": "",
        "StreetNumber": "",
        "County": "",
        "CountyName": "",
        "City": "Singapore",
        "DistrictName": "",
        "State": "",
        "StateName": "",
        "ZipCode": "",
        "ThreeLetterISORegionName": "SGP",
        "Phone": "",
        "PhoneRecordId": 0,
        "PhoneExt": "",
        "Email": "",
        "EmailContent": "",
        "EmailRecordId": 0,
        "Url": "",
        "UrlRecordId": 0,
        "TwoLetterISORegionName": "",
        "Deactivate": false,
        "AttentionTo": "",
        "BuildingCompliment": "",
        "Postbox": "",
        "TaxGroup": "",
        "AddressTypeValue": 0,
        "IsPrimary": false,
        "IsPrivate": false,
        "PartyNumber": null,
        "DirectoryPartyTableRecordId": 0,
        "DirectoryPartyLocationRecordId": 0,
        "DirectoryPartyLocationRoleRecordId": 0,
        "LogisticsLocationId": "",
        "LogisticsLocationRecordId": 0,
        "LogisticsLocationExtRecordId": 0,
        "LogisticsLocationRoleRecordId": 0,
        "PhoneLogisticsLocationRecordId": 0,
        "PhoneLogisticsLocationId": "",
        "EmailLogisticsLocationRecordId": 0,
        "EmailLogisticsLocationId": "",
        "UrlLogisticsLocationRecordId": 0,
        "UrlLogisticsLocationId": "",
        "ExpireRecordId": 0,
        "SortOrder": 0,
        "ExtensionProperties": [
          {
            "Key": "SALESNAME",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": ""
            }
          },
          {
            "Key": "TRANSACTIONID",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": "18aug001"
            }
          },
          {
            "Key": "SALELINENUM",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": 1,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": null
            }
          }
        ]
      },
      "DeliveryMode": "CPick",
      "Comment": "",
      "RequestedDeliveryDate": "2016-08-18T00:00:00+08:00",
      "InventoryLocationId": "SDC B2C",
      "InventoryDimensionId": null,
      "ItemType": 0,
      "ScaleItem": false,
      "ReservationId": "00000000-0000-0000-0000-000000000000",
      "LineNumber": 1,
      "ReturnQuantity": 0,
      "StatusValue": 0,
      "SalesStatusValue": 0,
      "ProductSourceValue": 0,
      "IsGiftCardLine": false,
      "GiftCardId": "",
      "GiftCardCurrencyCode": "",
      "GiftCardOperationValue": 0,
      "IsInvoiceLine": false,
      "InvoiceId": "",
      "InvoiceAmount": 0,
      "IsInvoiceSettled": false,
      "IsVoided": false,
      "IsPriceLocked": false,
      "ChargeLines": [
        {
          "ChargeCode": "SHP",
          "CurrencyCode": null,
          "ModuleTypeValue": 1,
          "ChargeTypeValue": 0,
          "ChargeMethodValue": 0,
          "CalculatedAmount": 0,
          "Value": 0,
          "Description": null,
          "TransactionId": "18aug001",
          "SaleLineNumber": 1,
          "FromAmount": 0,
          "ToAmount": 0,
          "Keep": 0,
          "ItemId": null,
          "Quantity": 0,
          "Price": 0,
          "ItemTaxGroupId": "GST",
          "SalesTaxGroupId": null,
          "TaxAmount": 0,
          "SalesOrderUnitOfMeasure": null,
          "NetAmount": 0,
          "NetAmountPerUnit": 0,
          "GrossAmount": 0,
          "TaxLines": [],
          "TaxAmountExemptInclusive": 0,
          "TaxAmountInclusive": 0,
          "TaxAmountExclusive": 0,
          "NetAmountWithAllInclusiveTax": 0,
          "BeginDateTime": "0001-01-01T00:00:00+00:00",
          "EndDateTime": "0001-01-01T00:00:00+00:00",
          "TaxRatePercent": 0,
          "IsReturnByReceipt": false,
          "ExtensionProperties": [
            {
              "Key": "CURRENCYCODE",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": "SGD"
              }
            },
            {
              "Key": "MARKUPLINENUM",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": 0,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": null
              }
            },
            {
              "Key": "STORE",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": ""
              }
            },
            {
              "Key": "TAXGROUP",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": ""
              }
            },
            {
              "Key": "TERMINALID",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": ""
              }
            },
            {
              "Key": "METHOD",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": 0,
                "LongValue": null,
                "StringValue": null
              }
            },
            {
              "Key": "DATAAREAID",
              "Value": {
                "BooleanValue": null,
                "ByteValue": null,
                "DecimalValue": null,
                "DateTimeOffsetValue": null,
                "IntegerValue": null,
                "LongValue": null,
                "StringValue": "sdc"
              }
            }
          ]
        }
      ],
      "BasePrice": 0,
      "AgreementPrice": 0,
      "AdjustedPrice": 0,
      "ReturnTransactionId": "",
      "ReturnLineNumber": 0,
      "ReturnInventTransId": "",
      "ReturnStore": "",
      "ReturnTerminalId": "",
      "ReturnChannelId": 5637146827,
      "Store": "",
      "TerminalId": "",
      "SalesDate": null,
      "QuantityInvoiced": null,
      "QuantityOrdered": null,
      "RecordId": 0,
      "SerialNumber": "",
      "BatchId": "",
      "DeliveryModeChargeAmount": null,
      "UnitOfMeasureSymbol": null,
      "CatalogId": 0,
      "ElectronicDeliveryEmailAddress": "",
      "ElectronicDeliveryEmailContent": "",
      "LoyaltyDiscountAmount": 0,
      "LoyaltyPercentageDiscount": 0,
      "IsCustomerAccountDeposit": false,
      "Blocked": false,
      "Found": false,
      "DateToActivateItem": "0001-01-01T00:00:00+00:00",
      "LinePercentageDiscount": 0,
      "PeriodicPercentageDiscount": 0,
      "QuantityDiscounted": 0,
      "UnitQuantity": 0,
      "UnitOfMeasureConversion": {
        "ItemId": null,
        "FromUnitOfMeasureId": null,
        "ToUnitOfMeasureId": null,
        "FromUnitOfMeasureSymbol": null,
        "ToUnitOfMeasureSymbol": null,
        "IsBackward": false,
        "RecordId": 0,
        "ProductRecordId": 0,
        "Factor": 1,
        "Numerator": 1,
        "Denominator": 1,
        "InnerOffset": 0,
        "OuterOffset": 0,
        "ExtensionProperties": []
      },
      "DiscountLines": [],
      "PeriodicDiscountPossibilities": [],
      "ReasonCodeLines": [],
      "ReturnLabelProperties": null,
      "LineMultilineDiscOnItem": 0,
      "LineIdsLinkedProductMap": {},
      "LinkedParentLineId": null,
      "LineMultilineDiscOnItemValue": 0,
      "WasChanged": false,
      "OriginalSalesOrderUnitOfMeasure": null,
      "InventOrderUnitOfMeasure": null,
      "IsLoyaltyDiscountApplied": false,
      "ItemId": "1102101001",
      "Quantity": 1,
      "Price": 10,
      "ItemTaxGroupId": "GST",
      "SalesTaxGroupId": "",
      "TaxAmount": 0,
      "SalesOrderUnitOfMeasure": null,
      "NetAmount": 10,
      "NetAmountPerUnit": 0,
      "GrossAmount": 10,
      "TaxLines": [],
      "TaxAmountExemptInclusive": 0,
      "TaxAmountInclusive": 0,
      "TaxAmountExclusive": 0,
      "NetAmountWithAllInclusiveTax": 10,
      "BeginDateTime": "0001-01-01T00:00:00+00:00",
      "EndDateTime": "0001-01-01T00:00:00+00:00",
      "TaxRatePercent": 0,
      "IsReturnByReceipt": false,
      "ExtensionProperties": [
        {
          "Key": "TRANSACTIONID",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": "18aug001"
          }
        },
        {
          "Key": "UNIT",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": "ea"
          }
        },
        {
          "Key": "INVENTORYSITEID",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": null,
            "StringValue": ""
          }
        },
        {
          "Key": "LOGISTICSPOSTALADDRESS",
          "Value": {
            "BooleanValue": null,
            "ByteValue": null,
            "DecimalValue": null,
            "DateTimeOffsetValue": null,
            "IntegerValue": null,
            "LongValue": 0,
            "StringValue": null
          }
        }
      ]
    }
  ],
     */
}
