package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.model.axchannel.customerext.AxLineOfBusiness;

public class PartnerTypeVM {

    private String id;

    private String label;

    private String description;

    private String status;
    public PartnerTypeVM(){

    }
    public PartnerTypeVM(AxLineOfBusiness pt) {
        if(pt != null){
            this.setId(pt.getLineOfBusinessId());
            this.setLabel(pt.getDescription());
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
