
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_CountryRegionTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_CountryRegionTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_CountryRegionTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_CountryRegionTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_CountryRegionTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcCountryRegionTable"
})
public class ArrayOfSDCCountryRegionTable {

    @XmlElement(name = "SDC_CountryRegionTable", nillable = true)
    protected List<SDCCountryRegionTable> sdcCountryRegionTable;

    /**
     * Gets the value of the sdcCountryRegionTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcCountryRegionTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCCountryRegionTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCCountryRegionTable }
     * 
     * 
     */
    public List<SDCCountryRegionTable> getSDCCountryRegionTable() {
        if (sdcCountryRegionTable == null) {
            sdcCountryRegionTable = new ArrayList<SDCCountryRegionTable>();
        }
        return this.sdcCountryRegionTable;
    }

}
