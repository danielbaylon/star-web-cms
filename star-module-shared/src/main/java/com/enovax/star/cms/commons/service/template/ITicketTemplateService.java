package com.enovax.star.cms.commons.service.template;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;

import java.util.List;

/**
 * Created by jennylynsze on 12/15/16.
 */
public interface ITicketTemplateService {
    void updateTicketTemplateCache(StoreApiChannels channel) throws AxChannelException;
    List<AxRetailTemplateXML> getTicketTemplates(StoreApiChannels channel) throws AxChannelException;
    AxRetailTemplateXML getTicketTemplate(StoreApiChannels channel, String templateName);
}
