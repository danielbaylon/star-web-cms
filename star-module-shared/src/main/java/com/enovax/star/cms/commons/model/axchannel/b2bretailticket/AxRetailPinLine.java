package com.enovax.star.cms.commons.model.axchannel.b2bretailticket;

import java.util.Date;

/**
 * Created by jensen on 17/8/16.
 */
public class AxRetailPinLine {

    private Long channelId;
    private String dataAreaId;
    private Date endDateTime;
    private Date eventDate;
    private String eventGroupId;
    private String eventLineId;
    private String inventTransId;
    private String invoiceId;
    private boolean combinedTicket;
    private boolean redeemed;
    private String itemId;
    private Integer lineId;
    private Integer lineNum;
    private String mediaType;
    private String openValidityRule;
    private Integer qty;
    private Integer qtyRedeemed;
    private Integer qtyReturned;
    private String referenceId;
    private Integer replicationCounterFromOrigin;
    private String retailVariantId;
    private String salesId;
    private Date startDateTime;
    private String transactionId;

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public boolean isCombinedTicket() {
        return combinedTicket;
    }

    public void setCombinedTicket(boolean combinedTicket) {
        this.combinedTicket = combinedTicket;
    }

    public boolean isRedeemed() {
        return redeemed;
    }

    public void setRedeemed(boolean redeemed) {
        this.redeemed = redeemed;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getOpenValidityRule() {
        return openValidityRule;
    }

    public void setOpenValidityRule(String openValidityRule) {
        this.openValidityRule = openValidityRule;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public Integer getQtyReturned() {
        return qtyReturned;
    }

    public void setQtyReturned(Integer qtyReturned) {
        this.qtyReturned = qtyReturned;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public Integer getReplicationCounterFromOrigin() {
        return replicationCounterFromOrigin;
    }

    public void setReplicationCounterFromOrigin(Integer replicationCounterFromOrigin) {
        this.replicationCounterFromOrigin = replicationCounterFromOrigin;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getInventTransId() {
        return inventTransId;
    }

    public void setInventTransId(String inventTransId) {
        this.inventTransId = inventTransId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getLineNum() {
        return lineNum;
    }

    public void setLineNum(Integer lineNum) {
        this.lineNum = lineNum;
    }
}
