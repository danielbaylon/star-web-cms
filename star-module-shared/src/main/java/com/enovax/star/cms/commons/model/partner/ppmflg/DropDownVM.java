package com.enovax.star.cms.commons.model.partner.ppmflg;

public class DropDownVM {
    private String code;
    private String label;

    public DropDownVM() {
        super();
    }

    public DropDownVM(String code, String label) {
        super();
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
