package com.enovax.star.cms.commons.jcrrepository.system.ppslm;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.partner.ppslm.AdminAccountVM;
import info.magnolia.cms.security.*;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.jcr.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lavanya on 20/9/16.
 */
@Repository
public class DefaultJcrUserRepository implements IUserRepository {

    private SecuritySupport securitySupport;
    private static UserManager userManager;
    private static RoleManager roleManager;
    private static GroupManager groupManager;


    @PostConstruct
    public void init(){
        securitySupport = Components.getComponent(SecuritySupport.class);
        userManager = securitySupport.getUserManager();
        roleManager = securitySupport.getRoleManager();
        groupManager = securitySupport.getGroupManager();
    }
    
    @Override
    public List<AdminAccountVM> getAllPPSLMUsers() {
        try {

            String baseQuery = "/jcr:root/admin/partner-portal-slm//element(*, mgnl:user)";
            Iterable<Node> nodes = JcrRepository.query(JcrWorkspace.Users.getWorkspaceName(),JcrWorkspace.Users.getNodeType(),baseQuery);
            Iterator<Node> iter = nodes.iterator();
            List<AdminAccountVM> adminAccountVMs = new ArrayList<>();
            while(iter.hasNext()) {
                Node node = iter.next();
                AdminAccountVM accountVM = new AdminAccountVM();
                accountVM.setId(node.getName());
                accountVM.setUsername(node.getName());
                accountVM.setEmail(node.getProperty("email").getString());
                accountVM.setDesignation(node.getProperty("title").getString());
//                accountVM.setCountryIds(getCountryList(node));
//                accountVM.setGroups(getGroupsList(node));
//                accountVM.setRoleIds(userManager.getUser(node.getName()).getRoles());
                accountVM.setStatus("Active");
                adminAccountVMs.add(accountVM);
            }
            return adminAccountVMs;
        }
        catch(RepositoryException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getAdminUserAssignedRights(String adminId) {
        User user = userManager.getUser(adminId);
        if(user != null && user.getAllRoles() != null){
            return new ArrayList<>(user.getAllRoles());
        }
        return new ArrayList<>();
    }

    @Override
    public User getUserById(String adminId) {
        return userManager.getUser(adminId);
    }

//    private List<String> getCountryList(Node node) throws RepositoryException{
//        Iterator<Node> countriesIter = node.getProperties("countries");
//        List<String> countriesList = new ArrayList<>();
//        while(countriesIter.hasNext()) {
//            Node countryNode = countriesIter.next();
//            countriesList.add(countryNode.getName());
//        }
//        return countriesList;
//    }
//
//    private List<String> getGroupsList(Node node) throws RepositoryException{
//        Iterator<Node> groupsIter = node.getProperties("groups");
//        List<String> groupsList = new ArrayList<>();
//        while(groupsIter.hasNext()) {
//            Node groupNode = groupsIter.next();
//            groupsList.add(groupNode.getName());
//        }
//        return groupsList;
//    }
//
//    private List<String> getRolesList(Node node) throws RepositoryException{
//        Iterator<Node> rolesIter = node.getProperties("roles");
//        List<String> roleList = new ArrayList<>();
//        while(rolesIter.hasNext()) {
//            Node roleNode = rolesIter.next();
//            roleList.add(roleNode.getName());
//        }
//        return roleList;
//    }

    protected Session getSession() throws RepositoryException {
        return MgnlContext.getJCRSession(JcrWorkspace.Users.getWorkspaceName());
    }

    public List<String> getCountryList(String adminId) {
        try {
            User user = userManager.getUser(adminId);
            String uuid = user.getIdentifier();
            Node userNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Users.getWorkspaceName(), uuid);
            NodeIterator iter = userNode.getNodes("countries");
            Node countryNode;
            List<String> countries= new ArrayList<>();
            while(iter.hasNext()) {
                countryNode = iter.nextNode();
                PropertyIterator iter1 = countryNode.getProperties();
                while(iter1.hasNext()) {
                    Property prop = iter1.nextProperty();
                    if(StringUtils.isNumeric(prop.getName())) {
                        String str1 = prop.getValue().getString();
                        countries.add(str1);
                    }
                }
            }
            return countries;
        }catch (RepositoryException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AdminAccountVM> getAdminAccountVMByCountry(String countryId) {
        List<AdminAccountVM> adminAccountVms = getAllPPSLMUsers();
        Iterator<AdminAccountVM> iter = adminAccountVms.iterator();
        while(iter.hasNext()) {
           AdminAccountVM adminAccountVm = iter.next();
            List countryList = getCountryList(adminAccountVm.getUsername());
            if(!countryList.contains(countryId))
                iter.remove();
        }
        return adminAccountVms;
    }

    @Override
    public String getUserEmailById(String accMgrId) {
        if(accMgrId == null || accMgrId.trim().length() == 0){
            return null;
        }
        User user = getUserById(accMgrId);
        if(user == null){
            return null;
        }
        try{
            String email = user.getProperty("email");
            if(email != null && email.trim().length() > 0){
                return email.trim();
            }
            return email;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> getUserbyRole(String role) {
        List<String> userList = new ArrayList<>();
        userList = new ArrayList<String>(userManager.getUsersWithRole(role));
        return userList;
    }

    public List<String> getUserbyGroup(String group) {
        List<String> userList = new ArrayList<>();
        userList = new ArrayList<String>(userManager.getUsersWithGroup(group));
        return userList;
    }

    public List<String> getGroupbyRole(String role) {
        List<String> groupList = new ArrayList<>();
        groupList = new ArrayList<String>(groupManager.getGroupsWithRole(role));
        return groupList;
    }

    public List<AdminAccountVM> getAdminAccountVMbyRole(String role) {
        List<String> userList = new ArrayList<>();
        List<String> userList2 = new ArrayList<>();
        List<AdminAccountVM> adminAccountVms=new ArrayList<>();
        userList = getUserbyRole(role);
        List<String> groupList = getGroupbyRole(role);
        for(String groupId : groupList) {
            userList2.addAll(getUserbyGroup(groupId));
        }
        userList.removeAll(userList2);
        userList.addAll(userList2);
        for(String userId : userList) {
            User user=getUserById(userId);
            String email = user.getProperty("email") != null ? user.getProperty("email").toString() : "";
            AdminAccountVM adminAccountVM = new AdminAccountVM(userId,user.getName(),user.getProperty("title"), email,new ArrayList<String>(user.getRoles()),new ArrayList<String>(user.getGroups()),
                    getCountryList(userId));
            adminAccountVms.add(adminAccountVM);
        }
        return adminAccountVms;
    }
}
