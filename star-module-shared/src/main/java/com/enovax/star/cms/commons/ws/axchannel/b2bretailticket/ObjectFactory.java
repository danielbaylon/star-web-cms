
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.b2bretailticket package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketItem");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCCartRetailTicketPINLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINLineItem");
    private final static QName _ArrayOfSDCFacility_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_Facility");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _SDCCartRetailTicketPINItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINItem");
    private final static QName _SDCCartRetailTicketTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_CartRetailTicketTableResponse");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _SDCFacility_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_Facility");
    private final static QName _ArrayOfSDCCartRetailTicketPINItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketPINItem");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _SDCPrintTokenTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PrintTokenTable");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _ArrayOfB2BPIN_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfB2BPIN");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _SDCB2BPINPopulateResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_B2BPINPopulateResponse");
    private final static QName _ArrayOfSDCPrintTokenTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_PrintTokenTable");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _ArrayOfint_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _SDCPinTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PinTable");
    private final static QName _SDCCartRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketTable");
    private final static QName _SDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CartTicketListCriteria");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfSDCPinLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_PinLine");
    private final static QName _ArrayOfSDCCartRetailTicketLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketLineItem");
    private final static QName _ArrayOfSDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CartTicketListCriteria");
    private final static QName _ArrayOfSDCCartRetailTicketPINLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketPINLineItem");
    private final static QName _SDCB2BPINCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINCriteria");
    private final static QName _SDCCartRetailTicketLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketLineItem");
    private final static QName _SDCPinLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PinLine");
    private final static QName _ArrayOfSDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketItem");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ArrayOfSDCPinTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_PinTable");
    private final static QName _B2BPIN_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "B2BPIN");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _B2BCartCheckoutStartCombinedResponseB2BCartCheckoutStartCombinedResult_QNAME = new QName("http://tempuri.org/", "B2BCartCheckoutStartCombinedResult");
    private final static QName _SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "CartRetailTicketCollection");
    private final static QName _SDCCartTicketListCriteriaEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCCartTicketListCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCCartTicketListCriteriaTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCCartTicketListCriteriaLineNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineNumber");
    private final static QName _SDCCartTicketListCriteriaRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCCartTicketListCriteriaEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _SDCCartTicketListCriteriaLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineId");
    private final static QName _SDCCartTicketListCriteriaDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DataAreaId");
    private final static QName _SDCCartTicketListCriteriaAccountNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "AccountNum");
    private final static QName _SDCFacilityFacilityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "facilityId");
    private final static QName _SDCFacilityDaysOfWeekUsage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "daysOfWeekUsage");
    private final static QName _SDCFacilityOperationIds_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "operationIds");
    private final static QName _SDCPinTableGUESTNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "GUESTNAME");
    private final static QName _SDCPinTableCREDITCARDDIGITS_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CREDITCARDDIGITS");
    private final static QName _SDCPinTableCUSTACCOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CUSTACCOUNT");
    private final static QName _SDCPinTableDESCRIPTION_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DESCRIPTION");
    private final static QName _SDCPinTableMEDIATYPE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "MEDIATYPE");
    private final static QName _SDCPinTableSDCPinLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PinLineCollection");
    private final static QName _SDCPinTableDATAAREAID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DATAAREAID");
    private final static QName _SDCPinTableLEGALENTITYDATAAREAID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LEGALENTITYDATAAREAID");
    private final static QName _SDCPinTableREFERENCEID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "REFERENCEID");
    private final static QName _SDCPinTablePACKAGENAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PACKAGENAME");
    private final static QName _SDCPinTablePINCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PINCODE");
    private final static QName _SDCPinTableTRANSACTIONID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TRANSACTIONID");
    private final static QName _B2BCartCheckoutCompleteTransactionId_QNAME = new QName("http://tempuri.org/", "transactionId");
    private final static QName _B2BCartCheckoutStartGroupCartTicketListCriteria_QNAME = new QName("http://tempuri.org/", "CartTicketListCriteria");
    private final static QName _B2BPINSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SalesId");
    private final static QName _B2BPINInventTransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InventTransId");
    private final static QName _B2BPINInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InvoiceId");
    private final static QName _B2BPINCreditCardDigits_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CreditCardDigits");
    private final static QName _B2BPINDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Description");
    private final static QName _B2BPINEventDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventDate");
    private final static QName _B2BPINPackageName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PackageName");
    private final static QName _B2BPINCustAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CustAccount");
    private final static QName _B2BPINMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MediaType");
    private final static QName _B2BPINGuestName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "GuestName");
    private final static QName _B2BPINReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReferenceId");
    private final static QName _B2BCartCheckoutCompleteResponseB2BCartCheckoutCompleteResult_QNAME = new QName("http://tempuri.org/", "B2BCartCheckoutCompleteResult");
    private final static QName _SDCCartRetailTicketPINLineItemInventtransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InventtransId");
    private final static QName _SDCCartRetailTicketPINLineItemDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DataAreaId");
    private final static QName _SDCCartRetailTicketPINLineItemEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupId");
    private final static QName _SDCCartRetailTicketPINLineItemSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SalesId");
    private final static QName _SDCCartRetailTicketPINLineItemItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ItemId");
    private final static QName _SDCCartRetailTicketPINLineItemInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InvoiceId");
    private final static QName _SDCCartRetailTicketPINLineItemOpenValidityRule_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityRule");
    private final static QName _SDCCartRetailTicketPINLineItemRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RetailVariantId");
    private final static QName _SDCCartRetailTicketPINLineItemPackageLineItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLineItemId");
    private final static QName _SDCCartRetailTicketPINLineItemTicketTableId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketTableId");
    private final static QName _SDCCartRetailTicketPINLineItemEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventLineId");
    private final static QName _SDCCartRetailTicketPINLineItemReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ReferenceId");
    private final static QName _SDCCartRetailTicketPINLineItemMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "MediaType");
    private final static QName _SDCCartRetailTicketPINLineItemPackageId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageId");
    private final static QName _SDCCartRetailTicketPINLineItemPackageLineGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLineGroup");
    private final static QName _SDCB2BPINPopulateResponsePinTableCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "PinTableCollection");
    private final static QName _SDCB2BPINPopulateResponseRequestReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RequestReferenceId");
    private final static QName _SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CartRetailTicketTableEntityCollection");
    private final static QName _SDCCartRetailTicketTablePaxId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PaxId");
    private final static QName _SDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CartRetailTicketPINTableEntityCollection");
    private final static QName _SDCCartRetailTicketTableErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ErrorMessage");
    private final static QName _SDCCartRetailTicketTableTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TransactionId");
    private final static QName _GenerateB2BPinB2BPINCriteria_QNAME = new QName("http://tempuri.org/", "B2BPINCriteria");
    private final static QName _SDCCartRetailTicketLineItemDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Description");
    private final static QName _SDCCartRetailTicketLineItemLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LineId");
    private final static QName _SDCCartRetailTicketLineItemUsageValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "UsageValidityId");
    private final static QName _SDCCartRetailTicketLineItemRefTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RefTransactionId");
    private final static QName _SDCCartRetailTicketLineItemOpenValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityId");
    private final static QName _SDCCartRetailTicketItemPAXID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PAXID");
    private final static QName _SDCCartRetailTicketItemProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductName");
    private final static QName _SDCCartRetailTicketItemSDCFacilityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_FacilityCollection");
    private final static QName _SDCCartRetailTicketItemSDCPrintTokenCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PrintTokenCollection");
    private final static QName _SDCCartRetailTicketItemPinCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PinCode");
    private final static QName _SDCCartRetailTicketItemTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketCode");
    private final static QName _SDCCartRetailTicketItemSDCCartRetailTicketLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketLineCollection");
    private final static QName _SDCCartRetailTicketItemTemplateName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TemplateName");
    private final static QName _SDCCartRetailTicketItemOrigTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OrigTicketCode");
    private final static QName _SDCCartRetailTicketItemTicketCodePackage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketCodePackage");
    private final static QName _B2BCartCheckoutStartSingleResponseB2BCartCheckoutStartSingleResult_QNAME = new QName("http://tempuri.org/", "B2BCartCheckoutStartSingleResult");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCB2BPINCriteriaGenerateB2BPinCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "GenerateB2BPinCollection");
    private final static QName _SDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINLineCollection");
    private final static QName _SDCCartRetailTicketPINItemProductPINCodePackage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductPINCodePackage");
    private final static QName _SDCCartRetailTicketPINItemPRODUCTPINCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PRODUCTPINCODE");
    private final static QName _SDCCartRetailTicketPINItemCustAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CustAccount");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _B2BCartCheckoutCancelResponseB2BCartCheckoutCancelResult_QNAME = new QName("http://tempuri.org/", "B2BCartCheckoutCancelResult");
    private final static QName _SDCPrintTokenTableTokenKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TokenKey");
    private final static QName _SDCPrintTokenTableValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Value");
    private final static QName _B2BCartCheckoutStartGroupResponseB2BCartCheckoutStartGroupResult_QNAME = new QName("http://tempuri.org/", "B2BCartCheckoutStartGroupResult");
    private final static QName _GenerateB2BPinResponseGenerateB2BPinResult_QNAME = new QName("http://tempuri.org/", "GenerateB2BPinResult");
    private final static QName _SDCPinLineINVENTTRANSID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "INVENTTRANSID");
    private final static QName _SDCPinLineINVOICEID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "INVOICEID");
    private final static QName _SDCPinLineEVENTGROUPID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EVENTGROUPID");
    private final static QName _SDCPinLineEVENTLINEID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EVENTLINEID");
    private final static QName _SDCPinLineOPENVALIDITYRULE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OPENVALIDITYRULE");
    private final static QName _SDCPinLineEVENTDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EVENTDATE");
    private final static QName _SDCPinLineSALESID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SALESID");
    private final static QName _SDCPinLineRETAILVARIANTID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RETAILVARIANTID");
    private final static QName _SDCPinLineITEMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ITEMID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.b2bretailticket
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link ArrayOfint }
     * 
     */
    public ArrayOfint createArrayOfint() {
        return new ArrayOfint();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link SDCB2BPINPopulateResponse }
     * 
     */
    public SDCB2BPINPopulateResponse createSDCB2BPINPopulateResponse() {
        return new SDCB2BPINPopulateResponse();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTableResponse }
     * 
     */
    public SDCCartRetailTicketTableResponse createSDCCartRetailTicketTableResponse() {
        return new SDCCartRetailTicketTableResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketPINItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketPINItem createArrayOfSDCCartRetailTicketPINItem() {
        return new ArrayOfSDCCartRetailTicketPINItem();
    }

    /**
     * Create an instance of {@link SDCPinTable }
     * 
     */
    public SDCPinTable createSDCPinTable() {
        return new SDCPinTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTable }
     * 
     */
    public SDCCartRetailTicketTable createSDCCartRetailTicketTable() {
        return new SDCCartRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCPrintTokenTable }
     * 
     */
    public SDCPrintTokenTable createSDCPrintTokenTable() {
        return new SDCPrintTokenTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketItem }
     * 
     */
    public SDCCartRetailTicketItem createSDCCartRetailTicketItem() {
        return new SDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPinLine }
     * 
     */
    public ArrayOfSDCPinLine createArrayOfSDCPinLine() {
        return new ArrayOfSDCPinLine();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketLineItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketLineItem createArrayOfSDCCartRetailTicketLineItem() {
        return new ArrayOfSDCCartRetailTicketLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketPINLineItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketPINLineItem createArrayOfSDCCartRetailTicketPINLineItem() {
        return new ArrayOfSDCCartRetailTicketPINLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPrintTokenTable }
     * 
     */
    public ArrayOfSDCPrintTokenTable createArrayOfSDCPrintTokenTable() {
        return new ArrayOfSDCPrintTokenTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketPINLineItem }
     * 
     */
    public SDCCartRetailTicketPINLineItem createSDCCartRetailTicketPINLineItem() {
        return new SDCCartRetailTicketPINLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCFacility }
     * 
     */
    public ArrayOfSDCFacility createArrayOfSDCFacility() {
        return new ArrayOfSDCFacility();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketLineItem }
     * 
     */
    public SDCCartRetailTicketLineItem createSDCCartRetailTicketLineItem() {
        return new SDCCartRetailTicketLineItem();
    }

    /**
     * Create an instance of {@link SDCPinLine }
     * 
     */
    public SDCPinLine createSDCPinLine() {
        return new SDCPinLine();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketItem createArrayOfSDCCartRetailTicketItem() {
        return new ArrayOfSDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPinTable }
     * 
     */
    public ArrayOfSDCPinTable createArrayOfSDCPinTable() {
        return new ArrayOfSDCPinTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketPINItem }
     * 
     */
    public SDCCartRetailTicketPINItem createSDCCartRetailTicketPINItem() {
        return new SDCCartRetailTicketPINItem();
    }

    /**
     * Create an instance of {@link SDCFacility }
     * 
     */
    public SDCFacility createSDCFacility() {
        return new SDCFacility();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartTicketListCriteria }
     * 
     */
    public ArrayOfSDCCartTicketListCriteria createArrayOfSDCCartTicketListCriteria() {
        return new ArrayOfSDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link SDCB2BPINCriteria }
     * 
     */
    public SDCB2BPINCriteria createSDCB2BPINCriteria() {
        return new SDCB2BPINCriteria();
    }

    /**
     * Create an instance of {@link SDCCartTicketListCriteria }
     * 
     */
    public SDCCartTicketListCriteria createSDCCartTicketListCriteria() {
        return new SDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link B2BPIN }
     * 
     */
    public B2BPIN createB2BPIN() {
        return new B2BPIN();
    }

    /**
     * Create an instance of {@link ArrayOfB2BPIN }
     * 
     */
    public ArrayOfB2BPIN createArrayOfB2BPIN() {
        return new ArrayOfB2BPIN();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartSingle }
     * 
     */
    public B2BCartCheckoutStartSingle createB2BCartCheckoutStartSingle() {
        return new B2BCartCheckoutStartSingle();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartCombined }
     * 
     */
    public B2BCartCheckoutStartCombined createB2BCartCheckoutStartCombined() {
        return new B2BCartCheckoutStartCombined();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutCancelResponse }
     * 
     */
    public B2BCartCheckoutCancelResponse createB2BCartCheckoutCancelResponse() {
        return new B2BCartCheckoutCancelResponse();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartGroupResponse }
     * 
     */
    public B2BCartCheckoutStartGroupResponse createB2BCartCheckoutStartGroupResponse() {
        return new B2BCartCheckoutStartGroupResponse();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutCompleteResponse }
     * 
     */
    public B2BCartCheckoutCompleteResponse createB2BCartCheckoutCompleteResponse() {
        return new B2BCartCheckoutCompleteResponse();
    }

    /**
     * Create an instance of {@link GenerateB2BPinResponse }
     * 
     */
    public GenerateB2BPinResponse createGenerateB2BPinResponse() {
        return new GenerateB2BPinResponse();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutComplete }
     * 
     */
    public B2BCartCheckoutComplete createB2BCartCheckoutComplete() {
        return new B2BCartCheckoutComplete();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartGroup }
     * 
     */
    public B2BCartCheckoutStartGroup createB2BCartCheckoutStartGroup() {
        return new B2BCartCheckoutStartGroup();
    }

    /**
     * Create an instance of {@link GenerateB2BPin }
     * 
     */
    public GenerateB2BPin createGenerateB2BPin() {
        return new GenerateB2BPin();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartSingleResponse }
     * 
     */
    public B2BCartCheckoutStartSingleResponse createB2BCartCheckoutStartSingleResponse() {
        return new B2BCartCheckoutStartSingleResponse();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutStartCombinedResponse }
     * 
     */
    public B2BCartCheckoutStartCombinedResponse createB2BCartCheckoutStartCombinedResponse() {
        return new B2BCartCheckoutStartCombinedResponse();
    }

    /**
     * Create an instance of {@link B2BCartCheckoutCancel }
     * 
     */
    public B2BCartCheckoutCancel createB2BCartCheckoutCancel() {
        return new B2BCartCheckoutCancel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketItem")
    public JAXBElement<SDCCartRetailTicketItem> createSDCCartRetailTicketItem(SDCCartRetailTicketItem value) {
        return new JAXBElement<SDCCartRetailTicketItem>(_SDCCartRetailTicketItem_QNAME, SDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINLineItem")
    public JAXBElement<SDCCartRetailTicketPINLineItem> createSDCCartRetailTicketPINLineItem(SDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<SDCCartRetailTicketPINLineItem>(_SDCCartRetailTicketPINLineItem_QNAME, SDCCartRetailTicketPINLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_Facility")
    public JAXBElement<ArrayOfSDCFacility> createArrayOfSDCFacility(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_ArrayOfSDCFacility_QNAME, ArrayOfSDCFacility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINItem")
    public JAXBElement<SDCCartRetailTicketPINItem> createSDCCartRetailTicketPINItem(SDCCartRetailTicketPINItem value) {
        return new JAXBElement<SDCCartRetailTicketPINItem>(_SDCCartRetailTicketPINItem_QNAME, SDCCartRetailTicketPINItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_CartRetailTicketTableResponse")
    public JAXBElement<SDCCartRetailTicketTableResponse> createSDCCartRetailTicketTableResponse(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_SDCCartRetailTicketTableResponse_QNAME, SDCCartRetailTicketTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_Facility")
    public JAXBElement<SDCFacility> createSDCFacility(SDCFacility value) {
        return new JAXBElement<SDCFacility>(_SDCFacility_QNAME, SDCFacility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketPINItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketPINItem> createArrayOfSDCCartRetailTicketPINItem(ArrayOfSDCCartRetailTicketPINItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINItem>(_ArrayOfSDCCartRetailTicketPINItem_QNAME, ArrayOfSDCCartRetailTicketPINItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PrintTokenTable")
    public JAXBElement<SDCPrintTokenTable> createSDCPrintTokenTable(SDCPrintTokenTable value) {
        return new JAXBElement<SDCPrintTokenTable>(_SDCPrintTokenTable_QNAME, SDCPrintTokenTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfB2BPIN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfB2BPIN")
    public JAXBElement<ArrayOfB2BPIN> createArrayOfB2BPIN(ArrayOfB2BPIN value) {
        return new JAXBElement<ArrayOfB2BPIN>(_ArrayOfB2BPIN_QNAME, ArrayOfB2BPIN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINPopulateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_B2BPINPopulateResponse")
    public JAXBElement<SDCB2BPINPopulateResponse> createSDCB2BPINPopulateResponse(SDCB2BPINPopulateResponse value) {
        return new JAXBElement<SDCB2BPINPopulateResponse>(_SDCB2BPINPopulateResponse_QNAME, SDCB2BPINPopulateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_PrintTokenTable")
    public JAXBElement<ArrayOfSDCPrintTokenTable> createArrayOfSDCPrintTokenTable(ArrayOfSDCPrintTokenTable value) {
        return new JAXBElement<ArrayOfSDCPrintTokenTable>(_ArrayOfSDCPrintTokenTable_QNAME, ArrayOfSDCPrintTokenTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfint")
    public JAXBElement<ArrayOfint> createArrayOfint(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_ArrayOfint_QNAME, ArrayOfint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPinTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PinTable")
    public JAXBElement<SDCPinTable> createSDCPinTable(SDCPinTable value) {
        return new JAXBElement<SDCPinTable>(_SDCPinTable_QNAME, SDCPinTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketTable")
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTable(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTable_QNAME, SDCCartRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CartTicketListCriteria")
    public JAXBElement<SDCCartTicketListCriteria> createSDCCartTicketListCriteria(SDCCartTicketListCriteria value) {
        return new JAXBElement<SDCCartTicketListCriteria>(_SDCCartTicketListCriteria_QNAME, SDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPinLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_PinLine")
    public JAXBElement<ArrayOfSDCPinLine> createArrayOfSDCPinLine(ArrayOfSDCPinLine value) {
        return new JAXBElement<ArrayOfSDCPinLine>(_ArrayOfSDCPinLine_QNAME, ArrayOfSDCPinLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketLineItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketLineItem> createArrayOfSDCCartRetailTicketLineItem(ArrayOfSDCCartRetailTicketLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketLineItem>(_ArrayOfSDCCartRetailTicketLineItem_QNAME, ArrayOfSDCCartRetailTicketLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CartTicketListCriteria")
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createArrayOfSDCCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_ArrayOfSDCCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketPINLineItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> createArrayOfSDCCartRetailTicketPINLineItem(ArrayOfSDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem>(_ArrayOfSDCCartRetailTicketPINLineItem_QNAME, ArrayOfSDCCartRetailTicketPINLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINCriteria")
    public JAXBElement<SDCB2BPINCriteria> createSDCB2BPINCriteria(SDCB2BPINCriteria value) {
        return new JAXBElement<SDCB2BPINCriteria>(_SDCB2BPINCriteria_QNAME, SDCB2BPINCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketLineItem")
    public JAXBElement<SDCCartRetailTicketLineItem> createSDCCartRetailTicketLineItem(SDCCartRetailTicketLineItem value) {
        return new JAXBElement<SDCCartRetailTicketLineItem>(_SDCCartRetailTicketLineItem_QNAME, SDCCartRetailTicketLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPinLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PinLine")
    public JAXBElement<SDCPinLine> createSDCPinLine(SDCPinLine value) {
        return new JAXBElement<SDCPinLine>(_SDCPinLine_QNAME, SDCPinLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createArrayOfSDCCartRetailTicketItem(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_ArrayOfSDCCartRetailTicketItem_QNAME, ArrayOfSDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPinTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_PinTable")
    public JAXBElement<ArrayOfSDCPinTable> createArrayOfSDCPinTable(ArrayOfSDCPinTable value) {
        return new JAXBElement<ArrayOfSDCPinTable>(_ArrayOfSDCPinTable_QNAME, ArrayOfSDCPinTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link B2BPIN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "B2BPIN")
    public JAXBElement<B2BPIN> createB2BPIN(B2BPIN value) {
        return new JAXBElement<B2BPIN>(_B2BPIN_QNAME, B2BPIN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BCartCheckoutStartCombinedResult", scope = B2BCartCheckoutStartCombinedResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createB2BCartCheckoutStartCombinedResponseB2BCartCheckoutStartCombinedResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_B2BCartCheckoutStartCombinedResponseB2BCartCheckoutStartCombinedResult_QNAME, SDCCartRetailTicketTableResponse.class, B2BCartCheckoutStartCombinedResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "CartRetailTicketCollection", scope = SDCCartRetailTicketTableResponse.class)
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTableResponseCartRetailTicketCollection(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME, SDCCartRetailTicketTable.class, SDCCartRetailTicketTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventGroupId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaItemId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaItemId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaTransactionId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineNumber", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaLineNumber(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaLineNumber_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaRetailVariantId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DataAreaId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaDataAreaId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaAccountNum(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaAccountNum_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "facilityId", scope = SDCFacility.class)
    public JAXBElement<String> createSDCFacilityFacilityId(String value) {
        return new JAXBElement<String>(_SDCFacilityFacilityId_QNAME, String.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "daysOfWeekUsage", scope = SDCFacility.class)
    public JAXBElement<ArrayOfint> createSDCFacilityDaysOfWeekUsage(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_SDCFacilityDaysOfWeekUsage_QNAME, ArrayOfint.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "operationIds", scope = SDCFacility.class)
    public JAXBElement<ArrayOfstring> createSDCFacilityOperationIds(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_SDCFacilityOperationIds_QNAME, ArrayOfstring.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "GUESTNAME", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableGUESTNAME(String value) {
        return new JAXBElement<String>(_SDCPinTableGUESTNAME_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CREDITCARDDIGITS", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableCREDITCARDDIGITS(String value) {
        return new JAXBElement<String>(_SDCPinTableCREDITCARDDIGITS_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CUSTACCOUNT", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableCUSTACCOUNT(String value) {
        return new JAXBElement<String>(_SDCPinTableCUSTACCOUNT_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DESCRIPTION", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableDESCRIPTION(String value) {
        return new JAXBElement<String>(_SDCPinTableDESCRIPTION_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MEDIATYPE", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableMEDIATYPE(String value) {
        return new JAXBElement<String>(_SDCPinTableMEDIATYPE_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPinLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PinLineCollection", scope = SDCPinTable.class)
    public JAXBElement<ArrayOfSDCPinLine> createSDCPinTableSDCPinLineCollection(ArrayOfSDCPinLine value) {
        return new JAXBElement<ArrayOfSDCPinLine>(_SDCPinTableSDCPinLineCollection_QNAME, ArrayOfSDCPinLine.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DATAAREAID", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableDATAAREAID(String value) {
        return new JAXBElement<String>(_SDCPinTableDATAAREAID_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LEGALENTITYDATAAREAID", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableLEGALENTITYDATAAREAID(String value) {
        return new JAXBElement<String>(_SDCPinTableLEGALENTITYDATAAREAID_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "REFERENCEID", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableREFERENCEID(String value) {
        return new JAXBElement<String>(_SDCPinTableREFERENCEID_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PACKAGENAME", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTablePACKAGENAME(String value) {
        return new JAXBElement<String>(_SDCPinTablePACKAGENAME_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PINCODE", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTablePINCODE(String value) {
        return new JAXBElement<String>(_SDCPinTablePINCODE_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TRANSACTIONID", scope = SDCPinTable.class)
    public JAXBElement<String> createSDCPinTableTRANSACTIONID(String value) {
        return new JAXBElement<String>(_SDCPinTableTRANSACTIONID_QNAME, String.class, SDCPinTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = B2BCartCheckoutComplete.class)
    public JAXBElement<String> createB2BCartCheckoutCompleteTransactionId(String value) {
        return new JAXBElement<String>(_B2BCartCheckoutCompleteTransactionId_QNAME, String.class, B2BCartCheckoutComplete.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = B2BCartCheckoutStartGroup.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createB2BCartCheckoutStartGroupCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_B2BCartCheckoutStartGroupCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, B2BCartCheckoutStartGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventGroupId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINSalesId(String value) {
        return new JAXBElement<String>(_B2BPINSalesId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InventTransId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINInventTransId(String value) {
        return new JAXBElement<String>(_B2BPINInventTransId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINItemId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaItemId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InvoiceId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINInvoiceId(String value) {
        return new JAXBElement<String>(_B2BPINInvoiceId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaTransactionId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CreditCardDigits", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINCreditCardDigits(String value) {
        return new JAXBElement<String>(_B2BPINCreditCardDigits_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINDescription(String value) {
        return new JAXBElement<String>(_B2BPINDescription_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventDate", scope = B2BPIN.class)
    public JAXBElement<XMLGregorianCalendar> createB2BPINEventDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_B2BPINEventDate_QNAME, XMLGregorianCalendar.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PackageName", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINPackageName(String value) {
        return new JAXBElement<String>(_B2BPINPackageName_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CustAccount", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINCustAccount(String value) {
        return new JAXBElement<String>(_B2BPINCustAccount_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINMediaType(String value) {
        return new JAXBElement<String>(_B2BPINMediaType_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "GuestName", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINGuestName(String value) {
        return new JAXBElement<String>(_B2BPINGuestName_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaRetailVariantId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventLineId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = B2BPIN.class)
    public JAXBElement<String> createB2BPINReferenceId(String value) {
        return new JAXBElement<String>(_B2BPINReferenceId_QNAME, String.class, B2BPIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BCartCheckoutCompleteResult", scope = B2BCartCheckoutCompleteResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createB2BCartCheckoutCompleteResponseB2BCartCheckoutCompleteResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_B2BCartCheckoutCompleteResponseB2BCartCheckoutCompleteResult_QNAME, SDCCartRetailTicketTableResponse.class, B2BCartCheckoutCompleteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InventtransId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemInventtransId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemInventtransId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SalesId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemSalesId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemSalesId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemItemId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InvoiceId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemInvoiceId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemInvoiceId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityRule", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemOpenValidityRule(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemOpenValidityRule_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemRetailVariantId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineItemId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageLineItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageLineItemId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ReferenceId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemReferenceId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemReferenceId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MediaType", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemMediaType(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemMediaType_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineGroup", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageLineGroup(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageLineGroup_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPinTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "PinTableCollection", scope = SDCB2BPINPopulateResponse.class)
    public JAXBElement<ArrayOfSDCPinTable> createSDCB2BPINPopulateResponsePinTableCollection(ArrayOfSDCPinTable value) {
        return new JAXBElement<ArrayOfSDCPinTable>(_SDCB2BPINPopulateResponsePinTableCollection_QNAME, ArrayOfSDCPinTable.class, SDCB2BPINPopulateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RequestReferenceId", scope = SDCB2BPINPopulateResponse.class)
    public JAXBElement<String> createSDCB2BPINPopulateResponseRequestReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINPopulateResponseRequestReferenceId_QNAME, String.class, SDCB2BPINPopulateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CartRetailTicketTableEntityCollection", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createSDCCartRetailTicketTableCartRetailTicketTableEntityCollection(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME, ArrayOfSDCCartRetailTicketItem.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PaxId", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTablePaxId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTablePaxId_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CartRetailTicketPINTableEntityCollection", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketPINItem> createSDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection(ArrayOfSDCCartRetailTicketPINItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINItem>(_SDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection_QNAME, ArrayOfSDCCartRetailTicketPINItem.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ErrorMessage", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableErrorMessage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableErrorMessage_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BPINCriteria", scope = GenerateB2BPin.class)
    public JAXBElement<SDCB2BPINCriteria> createGenerateB2BPinB2BPINCriteria(SDCB2BPINCriteria value) {
        return new JAXBElement<SDCB2BPINCriteria>(_GenerateB2BPinB2BPINCriteria_QNAME, SDCB2BPINCriteria.class, GenerateB2BPin.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Description", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemDescription(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDescription_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LineId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemLineId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemItemId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemUsageValidityId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemRetailVariantId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineItemId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageLineItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageLineItemId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RefTransactionId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemRefTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemRefTransactionId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineGroup", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageLineGroup(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemPackageLineGroup_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemOpenValidityId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PAXID", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemPAXID(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPAXID_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemProductName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemProductName_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_FacilityCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCFacility> createSDCCartRetailTicketItemSDCFacilityCollection(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_SDCCartRetailTicketItemSDCFacilityCollection_QNAME, ArrayOfSDCFacility.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PrintTokenCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCPrintTokenTable> createSDCCartRetailTicketItemSDCPrintTokenCollection(ArrayOfSDCPrintTokenTable value) {
        return new JAXBElement<ArrayOfSDCPrintTokenTable>(_SDCCartRetailTicketItemSDCPrintTokenCollection_QNAME, ArrayOfSDCPrintTokenTable.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemItemId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PinCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemPinCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPinCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemUsageValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketLineCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketLineItem> createSDCCartRetailTicketItemSDCCartRetailTicketLineCollection(ArrayOfSDCCartRetailTicketLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketLineItem>(_SDCCartRetailTicketItemSDCCartRetailTicketLineCollection_QNAME, ArrayOfSDCCartRetailTicketLineItem.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateName", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTemplateName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTemplateName_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrigTicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOrigTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemOrigTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCodePackage", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketCodePackage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCodePackage_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemOpenValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = B2BCartCheckoutStartSingle.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createB2BCartCheckoutStartSingleCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_B2BCartCheckoutStartGroupCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, B2BCartCheckoutStartSingle.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BCartCheckoutStartSingleResult", scope = B2BCartCheckoutStartSingleResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createB2BCartCheckoutStartSingleResponseB2BCartCheckoutStartSingleResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_B2BCartCheckoutStartSingleResponseB2BCartCheckoutStartSingleResult_QNAME, SDCCartRetailTicketTableResponse.class, B2BCartCheckoutStartSingleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = B2BCartCheckoutCancel.class)
    public JAXBElement<String> createB2BCartCheckoutCancelTransactionId(String value) {
        return new JAXBElement<String>(_B2BCartCheckoutCompleteTransactionId_QNAME, String.class, B2BCartCheckoutCancel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = B2BCartCheckoutStartCombined.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createB2BCartCheckoutStartCombinedCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_B2BCartCheckoutStartGroupCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, B2BCartCheckoutStartCombined.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfB2BPIN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "GenerateB2BPinCollection", scope = SDCB2BPINCriteria.class)
    public JAXBElement<ArrayOfB2BPIN> createSDCB2BPINCriteriaGenerateB2BPinCollection(ArrayOfB2BPIN value) {
        return new JAXBElement<ArrayOfB2BPIN>(_SDCB2BPINCriteriaGenerateB2BPinCollection_QNAME, ArrayOfB2BPIN.class, SDCB2BPINCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINLineCollection", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> createSDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection(ArrayOfSDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem>(_SDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection_QNAME, ArrayOfSDCCartRetailTicketPINLineItem.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductPINCodePackage", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemProductPINCodePackage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemProductPINCodePackage_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PRODUCTPINCODE", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemPRODUCTPINCODE(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemPRODUCTPINCODE_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemProductName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemProductName_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CustAccount", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemCustAccount(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemCustAccount_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PaxId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemPaxId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTablePaxId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_FacilityCollection", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<ArrayOfSDCFacility> createSDCCartRetailTicketPINItemSDCFacilityCollection(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_SDCCartRetailTicketItemSDCFacilityCollection_QNAME, ArrayOfSDCFacility.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "REFERENCEID", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemREFERENCEID(String value) {
        return new JAXBElement<String>(_SDCPinTableREFERENCEID_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemItemId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BCartCheckoutCancelResult", scope = B2BCartCheckoutCancelResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createB2BCartCheckoutCancelResponseB2BCartCheckoutCancelResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_B2BCartCheckoutCancelResponseB2BCartCheckoutCancelResult_QNAME, SDCCartRetailTicketTableResponse.class, B2BCartCheckoutCancelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TokenKey", scope = SDCPrintTokenTable.class)
    public JAXBElement<String> createSDCPrintTokenTableTokenKey(String value) {
        return new JAXBElement<String>(_SDCPrintTokenTableTokenKey_QNAME, String.class, SDCPrintTokenTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Value", scope = SDCPrintTokenTable.class)
    public JAXBElement<String> createSDCPrintTokenTableValue(String value) {
        return new JAXBElement<String>(_SDCPrintTokenTableValue_QNAME, String.class, SDCPrintTokenTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BCartCheckoutStartGroupResult", scope = B2BCartCheckoutStartGroupResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createB2BCartCheckoutStartGroupResponseB2BCartCheckoutStartGroupResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_B2BCartCheckoutStartGroupResponseB2BCartCheckoutStartGroupResult_QNAME, SDCCartRetailTicketTableResponse.class, B2BCartCheckoutStartGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINPopulateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GenerateB2BPinResult", scope = GenerateB2BPinResponse.class)
    public JAXBElement<SDCB2BPINPopulateResponse> createGenerateB2BPinResponseGenerateB2BPinResult(SDCB2BPINPopulateResponse value) {
        return new JAXBElement<SDCB2BPINPopulateResponse>(_GenerateB2BPinResponseGenerateB2BPinResult_QNAME, SDCB2BPINPopulateResponse.class, GenerateB2BPinResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "INVENTTRANSID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineINVENTTRANSID(String value) {
        return new JAXBElement<String>(_SDCPinLineINVENTTRANSID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "INVOICEID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineINVOICEID(String value) {
        return new JAXBElement<String>(_SDCPinLineINVOICEID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MEDIATYPE", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineMEDIATYPE(String value) {
        return new JAXBElement<String>(_SDCPinTableMEDIATYPE_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EVENTGROUPID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineEVENTGROUPID(String value) {
        return new JAXBElement<String>(_SDCPinLineEVENTGROUPID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EVENTLINEID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineEVENTLINEID(String value) {
        return new JAXBElement<String>(_SDCPinLineEVENTLINEID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OPENVALIDITYRULE", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineOPENVALIDITYRULE(String value) {
        return new JAXBElement<String>(_SDCPinLineOPENVALIDITYRULE_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TRANSACTIONID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineTRANSACTIONID(String value) {
        return new JAXBElement<String>(_SDCPinTableTRANSACTIONID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EVENTDATE", scope = SDCPinLine.class)
    public JAXBElement<XMLGregorianCalendar> createSDCPinLineEVENTDATE(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SDCPinLineEVENTDATE_QNAME, XMLGregorianCalendar.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SALESID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineSALESID(String value) {
        return new JAXBElement<String>(_SDCPinLineSALESID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DATAAREAID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineDATAAREAID(String value) {
        return new JAXBElement<String>(_SDCPinTableDATAAREAID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "REFERENCEID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineREFERENCEID(String value) {
        return new JAXBElement<String>(_SDCPinTableREFERENCEID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RETAILVARIANTID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineRETAILVARIANTID(String value) {
        return new JAXBElement<String>(_SDCPinLineRETAILVARIANTID_QNAME, String.class, SDCPinLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ITEMID", scope = SDCPinLine.class)
    public JAXBElement<String> createSDCPinLineITEMID(String value) {
        return new JAXBElement<String>(_SDCPinLineITEMID_QNAME, String.class, SDCPinLine.class, value);
    }

}
