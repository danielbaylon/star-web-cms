package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 6/24/16.
 */
public enum DepositTransactionType {
    DepositTopUp("Deposit Top-up"),
    TicketPurchase("Ticket Purchase"),
    TicketCancelled("Ticket Cancelled"),
    AmendReservation("Amend Show Date/Time"),
    QuantityIncrease("Quantity Increase"),
    QuantityReversal("Quantity Reversal");

    public String description;
    DepositTransactionType(String description) {
        this.description = description;
    }

}
