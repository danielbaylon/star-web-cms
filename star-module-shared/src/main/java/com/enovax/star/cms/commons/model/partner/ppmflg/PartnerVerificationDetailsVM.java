package com.enovax.star.cms.commons.model.partner.ppmflg;


import com.enovax.star.cms.commons.model.axchannel.customerext.AxCapacityGroup;
import com.enovax.star.cms.commons.model.axstar.AxCustomerGroup;

import java.util.List;

public class PartnerVerificationDetailsVM {

    private PartnerVM partnerVM;
    private List<AdminAccountVM> adminAccountVms;
    private List<RevalFeeItemVM> revalFeeItemVMs;
    private List<ProductTierVM> prodTierVms;
    private List<ReasonVM> reasonVms;
    private List<CountryVM> ctyVMs;
    private Integer ticketRevalidatePeriod;
    private List<ProductTierVM> tierVMs;
    private PartnerAccountVM partnerAccountVM;
    private List<AxCustomerGroup> customerGroupList;
    private List<AxCapacityGroup> capacityGroupList;

    public PartnerVerificationDetailsVM(){}

    public PartnerVerificationDetailsVM(PartnerVM partnerVM, List<AdminAccountVM> adminAccountVms, List<RevalFeeItemVM> revalFeeItemVMs, List<ProductTierVM> prodTierVms, List<ReasonVM> reasonVms, List<CountryVM> ctyVMs, List<ProductTierVM> tierVMs, Integer ticketRevalidatePeriod, List<AxCustomerGroup> custGrp, List<AxCapacityGroup> capacityGroupList) {
        this.partnerVM = partnerVM;
        this.adminAccountVms = adminAccountVms;
        this.revalFeeItemVMs = revalFeeItemVMs;
        this.prodTierVms = prodTierVms;
        this.reasonVms = reasonVms;
        this.ctyVMs = ctyVMs;
        this.tierVMs = tierVMs;
        this.ticketRevalidatePeriod = ticketRevalidatePeriod;
        this.customerGroupList = custGrp;
        this.capacityGroupList = capacityGroupList;
    }

    public PartnerVerificationDetailsVM(List<AdminAccountVM> adminAccountVms) {
        this.adminAccountVms = adminAccountVms;
    }

    public List<AdminAccountVM> getAdminAccountVms() {
        return adminAccountVms;
    }

    public void setAdminAccountVms(List<AdminAccountVM> adminAccountVms) {
        this.adminAccountVms = adminAccountVms;
    }

    public List<RevalFeeItemVM> getRevalFeeItemVMs() {
        return revalFeeItemVMs;
    }

    public void setRevalFeeItemVMs(List<RevalFeeItemVM> revalFeeItemVMs) {
        this.revalFeeItemVMs = revalFeeItemVMs;
    }

    public List<ProductTierVM> getProdTierVms() {
        return prodTierVms;
    }

    public void setProdTierVms(List<ProductTierVM> prodTierVms) {
        this.prodTierVms = prodTierVms;
    }

    public List<ReasonVM> getReasonVms() {
        return reasonVms;
    }

    public void setReasonVms(List<ReasonVM> reasonVms) {
        this.reasonVms = reasonVms;
    }

    public List<CountryVM> getCtyVMs() {
        return ctyVMs;
    }

    public void setCtyVMs(List<CountryVM> ctyVMs) {
        this.ctyVMs = ctyVMs;
    }

    public Integer getTicketRevalidatePeriod() {
        return ticketRevalidatePeriod;
    }

    public void setTicketRevalidatePeriod(Integer ticketRevalidatePeriod) {
        this.ticketRevalidatePeriod = ticketRevalidatePeriod;
    }

    public PartnerVM getPartnerVM() {
        return partnerVM;
    }

    public void setPartnerVM(PartnerVM partnerVM) {
        this.partnerVM = partnerVM;
    }


    public List<ProductTierVM> getTierVMs() {
        return tierVMs;
    }

    public void setTierVMs(List<ProductTierVM> tierVMs) {
        this.tierVMs = tierVMs;
    }

    public PartnerAccountVM getPartnerAccountVM() {
        return partnerAccountVM;
    }

    public void setPartnerAccountVM(PartnerAccountVM partnerAccountVM) {
        this.partnerAccountVM = partnerAccountVM;
    }

    public List<AxCustomerGroup> getCustomerGroupList() {
        return customerGroupList;
    }

    public void setCustomerGroupList(List<AxCustomerGroup> customerGroupList) {
        this.customerGroupList = customerGroupList;
    }

    public List<AxCapacityGroup> getCapacityGroupList() {
        return capacityGroupList;
    }

    public void setCapacityGroupList(List<AxCapacityGroup> capacityGroupList) {
        this.capacityGroupList = capacityGroupList;
    }
}
