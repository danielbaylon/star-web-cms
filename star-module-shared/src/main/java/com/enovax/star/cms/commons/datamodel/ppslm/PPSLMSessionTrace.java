package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="PPSLMSessionTrace")
public class PPSLMSessionTrace implements Serializable {

    private Integer id;
    private PPSLMSessionInfo sessionInfo;
    private Date traceDate;
    private String traceLabel;
    private String traceDesc;
    private String request;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="sessionInfoId")
    public PPSLMSessionInfo getPPSLMSessionInfo() {
        return sessionInfo;
    }
    public void setPPSLMSessionInfo(PPSLMSessionInfo PPSLMSessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    @Column(name="traceDate")
    public Date getTraceDate() {
        return traceDate;
    }
    public void setTraceDate(Date traceDate) {
        this.traceDate = traceDate;
    }

    @Column(name="traceLabel")
    public String getTraceLabel() {
        return traceLabel;
    }
    public void setTraceLabel(String traceLabel) {
        this.traceLabel = traceLabel;
    }

    @Column(name="traceDesc")
    public String getTraceDesc() {
        return traceDesc;
    }
    public void setTraceDesc(String traceDesc) {
        this.traceDesc = traceDesc;
    }
    
    @Column(name="request")
    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }
}
