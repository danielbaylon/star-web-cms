package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

/**
 * Created by jennylynsze on 10/13/16.
 */
public class AxStarProductPropertyTranslation {

    public static final String EN = "en-us";

    private String translationLanguage;
    private List<AxStarProductProperty> translatedProperties;

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

    public List<AxStarProductProperty> getTranslatedProperties() {
        return translatedProperties;
    }

    public void setTranslatedProperties(List<AxStarProductProperty> translatedProperties) {
        this.translatedProperties = translatedProperties;
    }
}
