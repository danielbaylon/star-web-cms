package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

/**
 * Created by jennylynsze on 8/15/16.
 */
@Entity
@Table(name="PPMFLGETicket")
public class PPMFLGETicket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "packageId", nullable = false)
    private Integer packageId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packageId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGMixMatchPackage mmPkg;

    @Column(name = "ticketNumber", nullable = false)
    private String ticketNumber;

    @Column(name = "displayName")
    private String displayName;

    @Column(name = "ticketPersonType")
    private String ticketPersonType;

    @Column(name = "validityStartDate")
    private String validityStartDate;

    @Column(name = "validityEndDate")
    private String validityEndDate;

    @Column(name = "totalPrice")
    private String totalPrice;

    @Column(name = "shortDisplayName")
    private String shortDisplayName;

    @Column(name = "eventDate")
    private String eventDate;

    @Column(name = "eventSession")
    private String eventSession;

    @Column(name = "eTicketData")
    private String eTicketData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public PPMFLGMixMatchPackage getMmPkg() {
        return mmPkg;
    }

    public void setMmPkg(PPMFLGMixMatchPackage mmPkg) {
        this.mmPkg = mmPkg;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTicketPersonType() {
        return ticketPersonType;
    }

    public void setTicketPersonType(String ticketPersonType) {
        this.ticketPersonType = ticketPersonType;
    }

    public String getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(String validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public String getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(String validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getShortDisplayName() {
        return shortDisplayName;
    }

    public void setShortDisplayName(String shortDisplayName) {
        this.shortDisplayName = shortDisplayName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventSession() {
        return eventSession;
    }

    public void setEventSession(String eventSession) {
        this.eventSession = eventSession;
    }

    public String geteTicketData() {
        return eTicketData;
    }

    public void seteTicketData(String eTicketData) {
        this.eTicketData = eTicketData;
    }
}
