package com.enovax.star.cms.commons.model.partner.ppslm;


import com.enovax.star.cms.commons.model.axchannel.customerext.AxCountryRegion;

public class CountryVM {
    private String ctyCode;
    private String ctyName;

    private String regionId;
    private String regionName;

    public CountryVM() {
    }

    public CountryVM(AxCountryRegion c) {
        this.ctyCode = c.getCountryId();
        this.ctyName = c.getCountryName();
        this.regionId = c.getRegionId();
    }

    public String getCtyCode() {
        return ctyCode;
    }

    public void setCtyCode(String ctyCode) {
        this.ctyCode = ctyCode;
    }

    public String getCtyName() {
        return ctyName;
    }

    public void setCtyName(String ctyName) {
        this.ctyName = ctyName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
