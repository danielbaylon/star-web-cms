package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservation;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 27/8/16.
 */
public class PPMFLGWingsOfTimeReservationSpecification extends BaseSpecification<PPMFLGWingsOfTimeReservation> {
}
