package com.enovax.star.cms.commons.model.axchannel.b2bretailticket;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jensen on 17/8/16.
 */
public class AxRetailPinTable {

    private Long channelId;
    private String customerAccount;
    private String dataAreaId;
    private String description;
    private Date endDateTime;
    private boolean groupTicket;
    private String mediaType;
    private String packageName;
    private String pinCode;
    private Integer qtyPerProduct;
    private String referenceId;
    private Integer replicationCounterFromOrigin;
    private Integer sourceType;
    private Date startDateTime;
    private String transactionId;

    private List<AxRetailPinLine> lines = new ArrayList<>();

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public boolean isGroupTicket() {
        return groupTicket;
    }

    public void setGroupTicket(boolean groupTicket) {
        this.groupTicket = groupTicket;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Integer getQtyPerProduct() {
        return qtyPerProduct;
    }

    public void setQtyPerProduct(Integer qtyPerProduct) {
        this.qtyPerProduct = qtyPerProduct;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public Integer getReplicationCounterFromOrigin() {
        return replicationCounterFromOrigin;
    }

    public void setReplicationCounterFromOrigin(Integer replicationCounterFromOrigin) {
        this.replicationCounterFromOrigin = replicationCounterFromOrigin;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<AxRetailPinLine> getLines() {
        return lines;
    }

    public void setLines(List<AxRetailPinLine> lines) {
        this.lines = lines;
    }
}
