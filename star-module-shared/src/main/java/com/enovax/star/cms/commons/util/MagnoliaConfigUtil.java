package com.enovax.star.cms.commons.util;

import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;

/**
 * Created by jennylynsze on 10/4/16.
 */
public class MagnoliaConfigUtil {

    final static MagnoliaConfigurationProperties configurationProperties = Components.getComponent(MagnoliaConfigurationProperties.class);

    public static boolean isAuthorInstance() {
        return "true".equals(configurationProperties.getProperty("magnolia.bootstrap.authorInstance"));
    }

    public static boolean isDevMode() {
        return "true".equals(configurationProperties.getProperty("magnolia.develop"));
    }
}
