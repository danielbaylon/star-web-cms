package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AxCart {

    private String id;
    // private String AffiliationLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.AffiliationLoyaltyTier)"
    // Nullable="false"/>
    private String isRequiredAmountPaid;
    private String customerAccountDepositAmount;
    private String isDiscountFullyCalculated;
    private String amountDue;
    private String amountPaid;
    // private String AttributeValues"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.AttributeValueBase)"
    // Nullable="false"/>
    private String beginDateTime;
    // private String businessDate;
    // private String cancellationChargeAmount;
    // private String estimatedShippingAmount;

    @JsonProperty("CartLines")
    private List<AxCartLine> axCartLineList = new ArrayList<>();
    private String cartType;
    // private String cartTypeValue;
    private String channelId;
    private String chargeAmount;
    // private String ChargeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ChargeLine)"
    // Nullable="false"/>
    // private String TaxViewLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.TaxViewLine)"
    // Nullable="false"/>
    private String comment;
    private String invoiceComment;
    // private String customerId;
    // private String customerOrderModeValue;
    // private String deliveryMode;
    // private String deliveryModeChargeAmount;
    private String discountAmount;
    // private String discountCodes;

    private String transactionTypeValue;
    // private String IncomeExpenseLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.IncomeExpenseLine)"
    // Nullable="false"/>
    private String incomeExpenseTotalAmount;
    // private String isReturnByReceipt;
    // private String returnTransactionHasLoyaltyPayment;
    // private String isFavorite;
    // private String isRecurring;
    // private String isSuspended;
    // private String loyaltyCardId;
    private String modifiedDateTime;
    // private String name;
    // private String orderNumber;
    // private String availableDepositAmount;
    private String overriddenDepositAmount;
    private String prepaymentAmountPaid;
    private String prepaymentAppliedOnPickup;
    // private List<AxPromotionLine> promotionLines;
    // private String quotationExpiryDate;
    // private String ReasonCodeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ReasonCodeLine)"
    // Nullable="false"/>
    // private String receiptEmail;
    // private String requestedDeliveryDate;
    // private String requiredDepositAmount;
    // private String salesId;
    // private String ShippingAddress"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.Address"/>
    private String staffId;
    private String subtotalAmount;
    private String subtotalAmountWithoutTax;
    private String taxAmount;
    private String taxOnCancellationCharge;
    // private String taxOverrideCode;

    @JsonProperty("TenderLines")
    private List<AxTenderLine> axTenderLineList;

    private String terminalId;
    private String totalAmount;
    private String totalManualDiscountAmount;
    private String totalManualDiscountPercentage;
    private String warehouseId;
    // private String isCreatedOffline;
    private String cartStatusValue;
    // private String receiptTransactionTypeValue;

    // private String ExtensionProperties"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
    // Nullable="false"/>package com.enovax.star.cms.commons.model.kiosk.odata;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    public void setIsRequiredAmountPaid(String isRequiredAmountPaid) {
        this.isRequiredAmountPaid = isRequiredAmountPaid;
    }

    public String getCustomerAccountDepositAmount() {
        return customerAccountDepositAmount;
    }

    public void setCustomerAccountDepositAmount(String customerAccountDepositAmount) {
        this.customerAccountDepositAmount = customerAccountDepositAmount;
    }

    public String getIsDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    public void setIsDiscountFullyCalculated(String isDiscountFullyCalculated) {
        this.isDiscountFullyCalculated = isDiscountFullyCalculated;
    }

    public String getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(String amountDue) {
        this.amountDue = amountDue;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public List<AxCartLine> getAxCartLineList() {
        return axCartLineList;
    }

    public void setAxCartLineList(List<AxCartLine> axCartLineList) {
        this.axCartLineList = axCartLineList;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInvoiceComment() {
        return invoiceComment;
    }

    public void setInvoiceComment(String invoiceComment) {
        this.invoiceComment = invoiceComment;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getTransactionTypeValue() {
        return transactionTypeValue;
    }

    public void setTransactionTypeValue(String transactionTypeValue) {
        this.transactionTypeValue = transactionTypeValue;
    }

    public String getIncomeExpenseTotalAmount() {
        return incomeExpenseTotalAmount;
    }

    public void setIncomeExpenseTotalAmount(String incomeExpenseTotalAmount) {
        this.incomeExpenseTotalAmount = incomeExpenseTotalAmount;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    public void setOverriddenDepositAmount(String overriddenDepositAmount) {
        this.overriddenDepositAmount = overriddenDepositAmount;
    }

    public String getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    public void setPrepaymentAmountPaid(String prepaymentAmountPaid) {
        this.prepaymentAmountPaid = prepaymentAmountPaid;
    }

    public String getPrepaymentAppliedOnPickup() {
        return prepaymentAppliedOnPickup;
    }

    public void setPrepaymentAppliedOnPickup(String prepaymentAppliedOnPickup) {
        this.prepaymentAppliedOnPickup = prepaymentAppliedOnPickup;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(String subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public String getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    public void setSubtotalAmountWithoutTax(String subtotalAmountWithoutTax) {
        this.subtotalAmountWithoutTax = subtotalAmountWithoutTax;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    public void setTaxOnCancellationCharge(String taxOnCancellationCharge) {
        this.taxOnCancellationCharge = taxOnCancellationCharge;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    public void setTotalManualDiscountAmount(String totalManualDiscountAmount) {
        this.totalManualDiscountAmount = totalManualDiscountAmount;
    }

    public String getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    public void setTotalManualDiscountPercentage(String totalManualDiscountPercentage) {
        this.totalManualDiscountPercentage = totalManualDiscountPercentage;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCartStatusValue() {
        return cartStatusValue;
    }

    public void setCartStatusValue(String cartStatusValue) {
        this.cartStatusValue = cartStatusValue;
    }

    public List<AxTenderLine> getAxTenderLineList() {
        return axTenderLineList;
    }

    public void setAxTenderLineList(List<AxTenderLine> axTenderLineList) {
        this.axTenderLineList = axTenderLineList;
    }

}
