package com.enovax.star.cms.commons.mgnl.form.field.validator.factory;

import com.enovax.star.cms.commons.mgnl.form.field.validator.FieldLengthValidator;
import com.enovax.star.cms.commons.mgnl.form.field.validator.definition.FieldLengthValidatorDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class FieldLengthValidatorFactory extends AbstractFieldValidatorFactory<FieldLengthValidatorDefinition> {

    private Item item;
    private FieldLengthValidatorDefinition definition;

    public FieldLengthValidatorFactory(FieldLengthValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition = definition;
    }

    @Override
    public Validator createValidator() {
        return new FieldLengthValidator(this.item, this.definition.getMaxlength(), this.definition.getErrorMessage());
    }
}
