package com.enovax.star.cms.commons.service.config;

import java.util.Map;

/**
 * Created by houtao on 5/8/16.
 */
public interface ISysCfgService {

    public String getAxDateTimeFormat();

    Map<String,String> getEmailConfigs();

    int getDefaultMaximumThreadPoolQuanity();
}
