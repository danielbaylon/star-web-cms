
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_GetUpCrossSellResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_GetUpCrossSellResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="upCrossSellEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_UpCrossSellTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_GetUpCrossSellResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "upCrossSellEntity"
})
public class SDCGetUpCrossSellResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "upCrossSellEntity", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCUpCrossSellTable> upCrossSellEntity;

    /**
     * Gets the value of the upCrossSellEntity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCUpCrossSellTable> getUpCrossSellEntity() {
        return upCrossSellEntity;
    }

    /**
     * Sets the value of the upCrossSellEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}
     *     
     */
    public void setUpCrossSellEntity(JAXBElement<ArrayOfSDCUpCrossSellTable> value) {
        this.upCrossSellEntity = value;
    }

}
