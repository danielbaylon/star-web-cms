
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetRegionResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_RegionResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRegionResult"
})
@XmlRootElement(name = "GetRegionResponse")
public class GetRegionResponse {

    @XmlElementRef(name = "GetRegionResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCRegionResponse> getRegionResult;

    /**
     * Gets the value of the getRegionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCRegionResponse }{@code >}
     *     
     */
    public JAXBElement<SDCRegionResponse> getGetRegionResult() {
        return getRegionResult;
    }

    /**
     * Sets the value of the getRegionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCRegionResponse }{@code >}
     *     
     */
    public void setGetRegionResult(JAXBElement<SDCRegionResponse> value) {
        this.getRegionResult = value;
    }

}
