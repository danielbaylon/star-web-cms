package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerTransactionHist;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 21/8/16.
 */
public class PPSLMPartnerTransactionHistSpecification extends BaseSpecification<PPSLMPartnerTransactionHist> {
}
