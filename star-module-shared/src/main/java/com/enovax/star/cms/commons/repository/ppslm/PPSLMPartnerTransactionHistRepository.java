package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerTransactionHist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 21/8/16.
 */
@Repository
public interface PPSLMPartnerTransactionHistRepository extends JpaRepository<PPSLMPartnerTransactionHist, Integer>, JpaSpecificationExecutor<PPSLMPartnerTransactionHist>{

    PPSLMPartnerTransactionHist findFirstByOrderAccountAndVoucher(String orderAccount, String voucher);
}
