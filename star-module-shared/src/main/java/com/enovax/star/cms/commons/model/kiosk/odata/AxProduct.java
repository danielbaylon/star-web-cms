package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.Product
 * 
 * @author Justin
 * @since 29 SEP 16
 *
 */
public class AxProduct {

    private String recordId;
    private String productName;
    private String productNumber;
    private String description;
    private String isMasterProduct;
    private String isKit;
    private String itemId;
    private String hasLinkedProducts;
    // rules" Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductRules"
    // Nullable="false"/>
    private String locale;
    private String offlineImage;
    private String isRemote;
    // changeTrackingInformation"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductChangeTrackingInformation"/>
    // image"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.RichMediaLocations"/>
    // <Property Name="UnitsOfMeasureSymbol" Type="Collection(Edm.String)"/>
    // linkedProducts"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.LinkedProduct)"
    // Nullable="false"/>
    private String basePrice;
    private String price;
    private String adjustedPrice;
    // context"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.ProjectionDomain"/>
    private String primaryCategoryId;
    // <Property Name="CategoryIds" Type="Collection(Edm.Int64)"
    // Nullable="false"/>
    // relatedProducts"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.RelatedProduct)"
    // Nullable="false"/>
    // productsRelatedToThis"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.RelatedProduct)"
    // Nullable="false"/>

    private List<AxProductPropertyTranslation> productProperties = new ArrayList<>();
    // Nullable="false"/>
    // compositionInformation"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductCompositionInformation"/>
    private String searchName;

    // extensionProperties"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
    // Nullable="false"/>
    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsMasterProduct() {
        return isMasterProduct;
    }

    public void setIsMasterProduct(String isMasterProduct) {
        this.isMasterProduct = isMasterProduct;
    }

    public String getIsKit() {
        return isKit;
    }

    public void setIsKit(String isKit) {
        this.isKit = isKit;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getHasLinkedProducts() {
        return hasLinkedProducts;
    }

    public void setHasLinkedProducts(String hasLinkedProducts) {
        this.hasLinkedProducts = hasLinkedProducts;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getOfflineImage() {
        return offlineImage;
    }

    public void setOfflineImage(String offlineImage) {
        this.offlineImage = offlineImage;
    }

    public String getIsRemote() {
        return isRemote;
    }

    public void setIsRemote(String isRemote) {
        this.isRemote = isRemote;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(String adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public String getPrimaryCategoryId() {
        return primaryCategoryId;
    }

    public void setPrimaryCategoryId(String primaryCategoryId) {
        this.primaryCategoryId = primaryCategoryId;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public List<AxProductPropertyTranslation> getProductProperties() {
        return productProperties;
    }

    public void setProductProperties(List<AxProductPropertyTranslation> productProperties) {
        this.productProperties = productProperties;
    }

}
