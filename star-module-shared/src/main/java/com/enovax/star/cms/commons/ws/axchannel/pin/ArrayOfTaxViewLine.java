
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfTaxViewLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTaxViewLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxViewLine" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}TaxViewLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTaxViewLine", propOrder = {
    "taxViewLine"
})
public class ArrayOfTaxViewLine {

    @XmlElement(name = "TaxViewLine", nillable = true)
    protected List<TaxViewLine> taxViewLine;

    /**
     * Gets the value of the taxViewLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxViewLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxViewLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxViewLine }
     * 
     * 
     */
    public List<TaxViewLine> getTaxViewLine() {
        if (taxViewLine == null) {
            taxViewLine = new ArrayList<TaxViewLine>();
        }
        return this.taxViewLine;
    }

}
