package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskSearchProductExtResponse {

    @JsonProperty("value")
    private List<AxInventTableExtEntity> axInventTableExtEntityList;

    public List<AxInventTableExtEntity> getAxInventTableExtEntityList() {
        return axInventTableExtEntityList;
    }

    public void setAxInventTableExtEntityList(List<AxInventTableExtEntity> axInventTableExtEntityList) {
        this.axInventTableExtEntityList = axInventTableExtEntityList;
    }

}
