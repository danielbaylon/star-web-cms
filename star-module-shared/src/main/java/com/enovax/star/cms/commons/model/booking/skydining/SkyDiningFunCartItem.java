package com.enovax.star.cms.commons.model.booking.skydining;

import com.enovax.star.cms.commons.model.booking.FunCartItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCartItem extends FunCartItem {
  private String remarks;
  private String themeId;
  private String themeName;
  private String discountId;
  private String jewelCard;
  private String jewelCardExpiry;
  private String promoCode;
  private String merchantId;
  private Integer priceType;
  private Integer discountPrice;
  private Integer originalPrice;

  private List<SkyDiningFunCartMainCourse> mainCourses = new ArrayList<>();
  private List<SkyDiningFunCartTopUp> topUps = new ArrayList<>();

  public List<SkyDiningFunCartMainCourse> getMainCourses() {
    return mainCourses;
  }

  public void setMainCourses(List<SkyDiningFunCartMainCourse> mainCourses) {
    this.mainCourses = mainCourses;
  }

  public List<SkyDiningFunCartTopUp> getTopUps() {
    return topUps;
  }

  public void setTopUps(List<SkyDiningFunCartTopUp> topUps) {
    this.topUps = topUps;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public String getThemeId() {
    return themeId;
  }

  public void setThemeId(String themeId) {
    this.themeId = themeId;
  }

  public String getThemeName() {
    return themeName;
  }

  public void setThemeName(String themeName) {
    this.themeName = themeName;
  }

  public String getDiscountId() {
    return discountId;
  }

  public void setDiscountId(String discountId) {
    this.discountId = discountId;
  }

  public String getJewelCard() {
    return jewelCard;
  }

  public void setJewelCard(String jewelCard) {
    this.jewelCard = jewelCard;
  }

  public String getJewelCardExpiry() {
    return jewelCardExpiry;
  }

  public void setJewelCardExpiry(String jewelCardExpiry) {
    this.jewelCardExpiry = jewelCardExpiry;
  }

  public String getPromoCode() {
    return promoCode;
  }

  public void setPromoCode(String promoCode) {
    this.promoCode = promoCode;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public Integer getPriceType() {
    return priceType;
  }

  public void setPriceType(Integer priceType) {
    this.priceType = priceType;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Integer getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(Integer originalPrice) {
    this.originalPrice = originalPrice;
  }
}
