
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_TemplateXMLSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_TemplateXMLSearchResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="TemplateXMLCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_TemplateXMLEntity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_TemplateXMLSearchResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "templateXMLCollection"
})
public class SDCTemplateXMLSearchResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "TemplateXMLCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCTemplateXMLEntity> templateXMLCollection;

    /**
     * Gets the value of the templateXMLCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCTemplateXMLEntity }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCTemplateXMLEntity> getTemplateXMLCollection() {
        return templateXMLCollection;
    }

    /**
     * Sets the value of the templateXMLCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCTemplateXMLEntity }{@code >}
     *     
     */
    public void setTemplateXMLCollection(JAXBElement<ArrayOfSDCTemplateXMLEntity> value) {
        this.templateXMLCollection = value;
    }

}
