package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventLines
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventLine {

    @JsonProperty("EventLineIdOption")
    private String eventLineIdOption;

    @JsonProperty("EventDateList")
    private AxEventDateList eventDateList;

    public String getEventLineIdOption() {
        return eventLineIdOption;
    }

    public void setEventLineIdOption(String eventLineIdOption) {
        this.eventLineIdOption = eventLineIdOption;
    }

    public AxEventDateList getEventDateList() {
        return eventDateList;
    }

    public void setEventDateList(AxEventDateList eventDateList) {
        this.eventDateList = eventDateList;
    }

}
