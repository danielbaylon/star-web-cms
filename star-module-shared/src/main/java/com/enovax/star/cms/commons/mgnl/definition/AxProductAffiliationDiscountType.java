package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 9/27/16.
 */
public enum AxProductAffiliationDiscountType {
    None,
    CreditCard,
    PaymentType,
    FaberLicence
}
