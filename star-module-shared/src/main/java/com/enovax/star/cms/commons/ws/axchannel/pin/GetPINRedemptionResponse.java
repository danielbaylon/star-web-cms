
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPINRedemptionResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_GetBlockPINRedemptionResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPINRedemptionResult"
})
@XmlRootElement(name = "GetPINRedemptionResponse", namespace = "http://tempuri.org/")
public class GetPINRedemptionResponse {

    @XmlElementRef(name = "GetPINRedemptionResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCGetBlockPINRedemptionResponse> getPINRedemptionResult;

    /**
     * Gets the value of the getPINRedemptionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCGetBlockPINRedemptionResponse }{@code >}
     *     
     */
    public JAXBElement<SDCGetBlockPINRedemptionResponse> getGetPINRedemptionResult() {
        return getPINRedemptionResult;
    }

    /**
     * Sets the value of the getPINRedemptionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCGetBlockPINRedemptionResponse }{@code >}
     *     
     */
    public void setGetPINRedemptionResult(JAXBElement<SDCGetBlockPINRedemptionResponse> value) {
        this.getPINRedemptionResult = value;
    }

}
