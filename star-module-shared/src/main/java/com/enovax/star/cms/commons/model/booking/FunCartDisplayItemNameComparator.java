package com.enovax.star.cms.commons.model.booking;

import java.util.Comparator;

/**
 * Created by jensen on 28/6/16.
 */
public class FunCartDisplayItemNameComparator implements Comparator<FunCartDisplayItem> {

    @Override
    public int compare(FunCartDisplayItem o1, FunCartDisplayItem o2) {
        final int nameCompare = o1.getName().compareTo(o2.getName());
        if (nameCompare != 0) {
            return nameCompare;
        }

        return o1.getListingId().compareTo(o2.getListingId());
    }
}
