package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReceiptLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 1/11/16.
 */
@Repository
public interface PPSLMWingsOfTimeReceiptLogRepository extends JpaRepository<PPSLMWingsOfTimeReceiptLog, Integer> {

    @Query("from PPSLMWingsOfTimeReceiptLog where type = ?1 and date = ?2 and mainAccountId = ?3 order by id desc ")
    List<PPSLMWingsOfTimeReceiptLog> findWoTReceiptGenerationLogByTypeAndDate(String s, Date date, Integer mainAccId);
}
