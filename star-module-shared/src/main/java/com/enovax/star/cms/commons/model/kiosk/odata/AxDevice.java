package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.Device
 * 
 * @author Justin
 * @since 7 SEP 16
 */
public class AxDevice {

	private String channelId;

	private String channelName;

	private String deviceNumber;

	private String deviceType;

	private String terminalId;

	private String token;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
