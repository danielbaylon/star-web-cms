package com.enovax.star.cms.commons.util;

import com.google.common.io.Files;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class FileUtil {

    public static final String IMG_EXT_PNG = ".png";
    public static final String IMG_EXT_JPG = ".jpg";
    public static final String IMG_EXT_GIF = ".gif";
    public static final String FILE_EXT_PDF = ".pdf";
    public static final String FILE_EXT_CSV = ".csv";
    public static final String IMG_EXT_JPEG = ".jpeg";
    public static final String FILE_EXT_XLS = ".xls";

    public static final Long SIZE_4M =  (long) 4194304;
    public static final String FILE_TOO_LARGE_MSG = "file.err.toolarge";

    private static int hashIterations = 1;

    public static boolean isImgExt(String file) {
        if (StringUtils.isEmpty(file) || file.length() < 5) {
            return false;
        }

        if (file.toLowerCase().endsWith(IMG_EXT_JPG) || file.toLowerCase().endsWith(IMG_EXT_PNG)
                || file.toLowerCase().endsWith(IMG_EXT_GIF)|| file.toLowerCase().endsWith(IMG_EXT_JPEG)) {
            return true;
        }

        return false;
    }

    public static boolean checkImgDimensions(File imgFile, int height, int width)
            throws IOException {
        final BufferedImage img = ImageIO.read(imgFile);
        if (img.getHeight() > height || img.getWidth() > width) {
            return false;
        }
        return true;
    }

    public static String hashFile(File file) throws IOException {
        return hashFile(Files.toByteArray(file));
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public static String hashFile(byte[] file) throws IOException {
        final String theHash = DigestUtils.md5Hex(file);
        return theHash;
    }

    public static void copyFile(File src, String basePath, String filename)
            throws IOException {
        final String bp = basePath.endsWith(File.separator) ? basePath
                : basePath + File.separator;

        final File dir = new File(bp);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new IOException(
                        "Unable to create directories needed for storing the file.");
            }
        }
        Files.copy(src, new File(bp + filename));
    }

    
    
    public static BufferedReader getBufferReader(String fileName) throws FileNotFoundException {
        BufferedReader br = null;
        br = new BufferedReader(new FileReader(fileName));
        return br;
    }

}
