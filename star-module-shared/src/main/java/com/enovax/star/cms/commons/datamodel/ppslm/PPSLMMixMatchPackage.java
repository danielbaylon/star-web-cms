package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Entity
@Table(name= "PPSLMMixMatchPackage")
public class PPSLMMixMatchPackage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "mainAccountId", nullable = false)
    private Integer mainAccountId;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "mainAccountId", referencedColumnName = "adminId", updatable = false, insertable = false)
//    private PPSLMPartner partner;

    @Column(name = "username", nullable = false)
    private String username;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "mmPkg")
    List<PPSLMMixMatchPackageItem> pkgItems;

    @Column(name = "isSubAccountTrans", nullable = false)
    private Boolean subAccountTrans;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "qty", nullable = false)
    private Integer qty;

    @Column(name = "ticketMedia", nullable = false)
    private String ticketMedia;

//   TODO evaluate this
    @Column(name = "pinCode")
    private String pinCode;

    @Column(name = "ticketGeneratedDate")
    private Date ticketGeneratedDate;

    @Column(name = "pinRequestId")
    private String pinRequestId;

    @Column(name = "expiryDate", nullable = false)
    private Date expiryDate;

    @Column(name = "status", nullable = false)
    private String status;
    // default will be set to 0
    @Column(name = "qtyRedeemed", nullable = false)
    private Integer qtyRedeemed; //TODO, i think this one might be obsolete???? (or can retain for previous one)

    @Column(name = "totalQty")
    private Integer totalQty;

    @Column(name = "totalQtyRedeemed")
    private Integer totalQtyRedeemed;

    @Column(name = "lastRedemptionDate")
    private Date lastRedemptionDate;

    @Column(name = "dateOfVisit")
    private Date dateOfVisit;

    @Column(name = "lastCheckDate")
    private Date lastCheckDate;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public Date getLastRedemptionDate() {
        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(Date lastRedemptionDate) {
        this.lastRedemptionDate = lastRedemptionDate;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public List<PPSLMMixMatchPackageItem> getPkgItems() {
        return pkgItems;
    }

    public void setPkgItems(List<PPSLMMixMatchPackageItem> pkgItems) {
        this.pkgItems = pkgItems;
    }

//    public PPSLMPartner getPartner() {
//        return partner;
//    }
//
//    public void setPartner(PPSLMPartner partner) {
//        this.partner = partner;
//    }

    public Date getLastCheckDate() {
        return lastCheckDate;
    }

    public void setLastCheckDate(Date lastCheckDate) {
        this.lastCheckDate = lastCheckDate;
    }

    public Boolean isSubAccountTrans() {
        return subAccountTrans;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getTicketMedia() {
        return ticketMedia;
    }

    public void setTicketMedia(String ticketMedia) {
        this.ticketMedia = ticketMedia;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getTicketGeneratedDate() {
        return ticketGeneratedDate;
    }

    public void setTicketGeneratedDate(Date ticketGeneratedDate) {
        this.ticketGeneratedDate = ticketGeneratedDate;
    }

    public String getPinRequestId() {
        return pinRequestId;
    }

    public void setPinRequestId(String pinRequestId) {
        this.pinRequestId = pinRequestId;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public Integer getTotalQtyRedeemed() {
        return totalQtyRedeemed;
    }

    public void setTotalQtyRedeemed(Integer totalQtyRedeemed) {
        this.totalQtyRedeemed = totalQtyRedeemed;
    }
}
