
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionItemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionItemType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Kit"/>
 *     &lt;enumeration value="KitComponent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionItemType", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models")
@XmlEnum
public enum TransactionItemType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Kit")
    KIT("Kit"),
    @XmlEnumValue("KitComponent")
    KIT_COMPONENT("KitComponent");
    private final String value;

    TransactionItemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionItemType fromValue(String v) {
        for (TransactionItemType c: TransactionItemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
