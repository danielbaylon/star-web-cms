package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 *
 * @author dbaylon
 *
 */

public class AxCartTicketSearchCriteria {

    @JsonProperty("DataLevelValue")
    private String dataLevelValue = "4";

	@JsonProperty("ReceiptNumber")
	private String receiptNumber;

	@JsonProperty("TransactionId")
	private String transactionId = "";

	public String getDataLevelValue() {
		return dataLevelValue;
	}

	public void setDataLevelValue(String dataLevelValue) {
		this.dataLevelValue = dataLevelValue;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
