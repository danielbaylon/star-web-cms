
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.customerext package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SDCCustomerUpdateTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CustomerUpdateTable");
    private final static QName _SDCLineOfBusiness_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_LineOfBusiness");
    private final static QName _SDCCountryRegionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CountryRegionTable");
    private final static QName _ArrayOfSDCCountryRegionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CountryRegionTable");
    private final static QName _SDCMarketDistribution_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_MarketDistribution");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _CommercePropertyBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceProperty");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _SDCCustomerBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CustomerBalance");
    private final static QName _ArrayOfSDCGetCapacityGroupTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_GetCapacityGroupTable");
    private final static QName _CustomerTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "CustomerTransactionDetails");
    private final static QName _ArrayOfSDCPriceGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_PriceGroup");
    private final static QName _SDCGetCapacityGroupTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_GetCapacityGroupTable");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _CommercePropertyValueBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommercePropertyValue");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _DateTimeOffset_QNAME = new QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset");
    private final static QName _SDCRegionResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_RegionResponse");
    private final static QName _CustomerBalancesBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CustomerBalances");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _SDCCountryRegionResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_CountryRegionResponse");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfCustomerTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "ArrayOfCustomerTransactionDetails");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _ArrayOfSDCCustomerBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CustomerBalance");
    private final static QName _SDCLineOfBusinessResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_LineOfBusinessResponse");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _ArrayOfSDCRegionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_RegionTable");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _SDCTopUpCustomerDepositResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_TopUpCustomerDepositResponse");
    private final static QName _ArrayOfSDCMarketDistribution_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_MarketDistribution");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _SDCGetCapacityGrpResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetCapacityGrpResponse");
    private final static QName _ArrayOfSDCCustomerTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CustomerTable");
    private final static QName _DepositDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "DepositDetails");
    private final static QName _SDCCustomerBalanceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_CustomerBalanceResponse");
    private final static QName _ArrayOfCommerceProperty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfCommerceProperty");
    private final static QName _SDCPriceGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_PriceGroup");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _SDCViewCustomerTransactionsResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_ViewCustomerTransactionsResponse");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _SDCCustomerTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CustomerTable");
    private final static QName _SDCCreateCustomerResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_CreateCustomerResponse");
    private final static QName _ArrayOfSDCLineOfBusiness_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_LineOfBusiness");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _SDCViewCustomerBalanceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_ViewCustomerBalanceResponse");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _SDCUpdateCreatedCustomerResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_UpdateCreatedCustomerResponse");
    private final static QName _SDCGetPriceGroupResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetPriceGroupResponse");
    private final static QName _CommerceEntityBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceEntity");
    private final static QName _SDCGetCreatedCustomerResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetCreatedCustomerResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _ArrayOfSDCDocumentLink_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_DocumentLink");
    private final static QName _SDCDocumentLink_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_DocumentLink");
    private final static QName _SDCRegionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_RegionTable");
    private final static QName _SDCCustomerTableCountriesInterestedIn_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_countriesInterestedIn");
    private final static QName _SDCCustomerTableLanguageId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_languageId");
    private final static QName _SDCCustomerTableUENNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UENNumber");
    private final static QName _SDCCustomerTableContactPersonStr_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ContactPersonStr");
    private final static QName _SDCCustomerTableRevalidationUnit_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_revalidationUnit");
    private final static QName _SDCCustomerTableBranchName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_branchName");
    private final static QName _SDCCustomerTableDesignation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Designation");
    private final static QName _SDCCustomerTableTALicenseNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_TALicenseNumber");
    private final static QName _SDCCustomerTableDocumentLinkDelimited_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DocumentLinkDelimited");
    private final static QName _SDCCustomerTableCountryRegion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CountryRegion");
    private final static QName _SDCCustomerTableLineOfBusiness_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineOfBusiness");
    private final static QName _SDCCustomerTableMarketDistributionDelimited_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MarketDistributionDelimited");
    private final static QName _SDCCustomerTableRevalidationFee_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_revalidationFee");
    private final static QName _SDCCustomerTableDocumentLinkLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DocumentLinkLines");
    private final static QName _SDCCustomerTableOfficeNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_officeNo");
    private final static QName _SDCCustomerTableFax_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Fax");
    private final static QName _SDCCustomerTableAccountMgrCMS_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "AccountMgrCMS");
    private final static QName _SDCCustomerTableCapacityGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_capacityGroupId");
    private final static QName _SDCCustomerTableMainAccLoginName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "_mainAccLoginName");
    private final static QName _SDCCustomerTableFaxExtension_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "FaxExtension");
    private final static QName _SDCCustomerTableMarketDistributionLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MarketDistributionLines");
    private final static QName _SDCCustomerTableAccountNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "AccountNum");
    private final static QName _SDCGetPriceGroupResponsePriceGroupEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "priceGroupEntity");
    private final static QName _GetCreatedCustomerResponseGetCreatedCustomerResult_QNAME = new QName("http://tempuri.org/", "GetCreatedCustomerResult");
    private final static QName _ViewCustomerTransactionsResponseViewCustomerTransactionsResult_QNAME = new QName("http://tempuri.org/", "ViewCustomerTransactionsResult");
    private final static QName _SDCLineOfBusinessResponseLineOfBusinessEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "lineOfBusinessEntity");
    private final static QName _SDCLineOfBusinessDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Description");
    private final static QName _SDCLineOfBusinessLineOfBusinessId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineOfBusinessId");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCCustomerBalanceResponseCustomerBalanceEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "customerBalanceEntity");
    private final static QName _GetCreatedCustomerCustAccountNum_QNAME = new QName("http://tempuri.org/", "custAccountNum");
    private final static QName _SDCCreateCustomerResponseCreateCustomer_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "createCustomer");
    private final static QName _GetCountryRegionResponseGetCountryRegionResult_QNAME = new QName("http://tempuri.org/", "GetCountryRegionResult");
    private final static QName _UpdateCreatedCustomerCustTable_QNAME = new QName("http://tempuri.org/", "custTable");
    private final static QName _SDCDocumentLinkDocumentName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DocumentName");
    private final static QName _SDCDocumentLinkLink_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Link");
    private final static QName _SDCGetCapacityGrpResponseGetCapacityGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "getCapacityGroup");
    private final static QName _UpdateCreatedCustomerResponseUpdateCreatedCustomerResult_QNAME = new QName("http://tempuri.org/", "UpdateCreatedCustomerResult");
    private final static QName _GetPriceGroupResponseGetPriceGroupResult_QNAME = new QName("http://tempuri.org/", "GetPriceGroupResult");
    private final static QName _ViewCustomerBalanceCustAccount_QNAME = new QName("http://tempuri.org/", "custAccount");
    private final static QName _ViewCustomerBalanceCurrencyCode_QNAME = new QName("http://tempuri.org/", "currencyCode");
    private final static QName _GetLineOfBusinessResponseGetLineOfBusinessResult_QNAME = new QName("http://tempuri.org/", "GetLineOfBusinessResult");
    private final static QName _ViewCustomerBalanceResponseViewCustomerBalanceResult_QNAME = new QName("http://tempuri.org/", "ViewCustomerBalanceResult");
    private final static QName _SDCUpdateCreatedCustomerResponseUpdateCustomer_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "updateCustomer");
    private final static QName _GetCustomerBalanceAccountNum_QNAME = new QName("http://tempuri.org/", "accountNum");
    private final static QName _GetCustomerBalanceResponseGetCustomerBalanceResult_QNAME = new QName("http://tempuri.org/", "GetCustomerBalanceResult");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _CommercePropertyValueLongValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "LongValue");
    private final static QName _CommercePropertyValueStringValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StringValue");
    private final static QName _CommercePropertyValueByteValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ByteValue");
    private final static QName _CommercePropertyValueBooleanValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "BooleanValue");
    private final static QName _CommercePropertyValueIntegerValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "IntegerValue");
    private final static QName _CommercePropertyValueDateTimeOffsetValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DateTimeOffsetValue");
    private final static QName _CommercePropertyValueDecimalValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DecimalValue");
    private final static QName _SDCCountryRegionResponseGetCountryRegion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "getCountryRegion");
    private final static QName _TopUpCustomerDepositCustDepositDetails_QNAME = new QName("http://tempuri.org/", "custDepositDetails");
    private final static QName _SDCCountryRegionTableRegionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RegionId");
    private final static QName _SDCCountryRegionTableCountryId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CountryId");
    private final static QName _SDCCountryRegionTableCountryName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CountryName");
    private final static QName _SDCPriceGroupName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Name");
    private final static QName _SDCPriceGroupPriceGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PriceGroupId");
    private final static QName _DepositDetailsTenderTypeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "TenderTypeId");
    private final static QName _DepositDetailsDueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "DueDate");
    private final static QName _DepositDetailsDocumentDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "DocumentDate");
    private final static QName _DepositDetailsCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "CurrencyCode");
    private final static QName _DepositDetailsTxt_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "Txt");
    private final static QName _DepositDetailsAccountNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "AccountNum");
    private final static QName _DepositDetailsDocumentNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "DocumentNum");
    private final static QName _DepositDetailsPostingDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "PostingDate");
    private final static QName _GetCapacityGroupResponseGetCapacityGroupResult_QNAME = new QName("http://tempuri.org/", "GetCapacityGroupResult");
    private final static QName _CustomerTransactionDetailsTransType_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "TransType");
    private final static QName _CustomerTransactionDetailsVoucher_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "Voucher");
    private final static QName _CustomerTransactionDetailsSalesPoolId_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "SalesPoolId");
    private final static QName _CustomerTransactionDetailsType_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "Type");
    private final static QName _CustomerTransactionDetailsInvoice_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "Invoice");
    private final static QName _CustomerTransactionDetailsPaymentReference_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "PaymentReference");
    private final static QName _CustomerTransactionDetailsQty_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "Qty");
    private final static QName _CustomerTransactionDetailsOrderAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", "OrderAccount");
    private final static QName _SDCGetCreatedCustomerResponseCustomerEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "customerEntity");
    private final static QName _GetRegionResponseGetRegionResult_QNAME = new QName("http://tempuri.org/", "GetRegionResult");
    private final static QName _CommercePropertyValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Value");
    private final static QName _CommercePropertyKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Key");
    private final static QName _SDCViewCustomerTransactionsResponseCustomerTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "CustomerTransactionDetails");
    private final static QName _SDCTopUpCustomerDepositResponseErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "errorMessage");
    private final static QName _CreateNewCustomerResponseCreateNewCustomerResult_QNAME = new QName("http://tempuri.org/", "CreateNewCustomerResult");
    private final static QName _TopUpCustomerDepositResponseTopUpCustomerDepositResult_QNAME = new QName("http://tempuri.org/", "TopUpCustomerDepositResult");
    private final static QName _SDCRegionResponseGetRegion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "getRegion");
    private final static QName _CommerceEntityExtensionProperties_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ExtensionProperties");
    private final static QName _SDCViewCustomerBalanceResponseCustomerBalanceDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "CustomerBalanceDetails");
    private final static QName _SDCMarketDistributionPercent_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Percent");
    private final static QName _SDCMarketDistributionSalesManager_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SalesManager");
    private final static QName _SDCCustomerBalanceCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CurrencyCode");
    private final static QName _SDCGetCapacityGroupTableCapacityGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CapacityGroupId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.customerext
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetLineOfBusinessResponse }
     * 
     */
    public GetLineOfBusinessResponse createGetLineOfBusinessResponse() {
        return new GetLineOfBusinessResponse();
    }

    /**
     * Create an instance of {@link SDCLineOfBusinessResponse }
     * 
     */
    public SDCLineOfBusinessResponse createSDCLineOfBusinessResponse() {
        return new SDCLineOfBusinessResponse();
    }

    /**
     * Create an instance of {@link GetRegionResponse }
     * 
     */
    public GetRegionResponse createGetRegionResponse() {
        return new GetRegionResponse();
    }

    /**
     * Create an instance of {@link SDCRegionResponse }
     * 
     */
    public SDCRegionResponse createSDCRegionResponse() {
        return new SDCRegionResponse();
    }

    /**
     * Create an instance of {@link GetCountryRegionResponse }
     * 
     */
    public GetCountryRegionResponse createGetCountryRegionResponse() {
        return new GetCountryRegionResponse();
    }

    /**
     * Create an instance of {@link SDCCountryRegionResponse }
     * 
     */
    public SDCCountryRegionResponse createSDCCountryRegionResponse() {
        return new SDCCountryRegionResponse();
    }

    /**
     * Create an instance of {@link GetCustomerBalance }
     * 
     */
    public GetCustomerBalance createGetCustomerBalance() {
        return new GetCustomerBalance();
    }

    /**
     * Create an instance of {@link GetCountryRegion }
     * 
     */
    public GetCountryRegion createGetCountryRegion() {
        return new GetCountryRegion();
    }

    /**
     * Create an instance of {@link GetCreatedCustomer }
     * 
     */
    public GetCreatedCustomer createGetCreatedCustomer() {
        return new GetCreatedCustomer();
    }

    /**
     * Create an instance of {@link ViewCustomerBalance }
     * 
     */
    public ViewCustomerBalance createViewCustomerBalance() {
        return new ViewCustomerBalance();
    }

    /**
     * Create an instance of {@link GetLineOfBusiness }
     * 
     */
    public GetLineOfBusiness createGetLineOfBusiness() {
        return new GetLineOfBusiness();
    }

    /**
     * Create an instance of {@link ViewCustomerTransactions }
     * 
     */
    public ViewCustomerTransactions createViewCustomerTransactions() {
        return new ViewCustomerTransactions();
    }

    /**
     * Create an instance of {@link TopUpCustomerDepositResponse }
     * 
     */
    public TopUpCustomerDepositResponse createTopUpCustomerDepositResponse() {
        return new TopUpCustomerDepositResponse();
    }

    /**
     * Create an instance of {@link SDCTopUpCustomerDepositResponse }
     * 
     */
    public SDCTopUpCustomerDepositResponse createSDCTopUpCustomerDepositResponse() {
        return new SDCTopUpCustomerDepositResponse();
    }

    /**
     * Create an instance of {@link GetCapacityGroupResponse }
     * 
     */
    public GetCapacityGroupResponse createGetCapacityGroupResponse() {
        return new GetCapacityGroupResponse();
    }

    /**
     * Create an instance of {@link SDCGetCapacityGrpResponse }
     * 
     */
    public SDCGetCapacityGrpResponse createSDCGetCapacityGrpResponse() {
        return new SDCGetCapacityGrpResponse();
    }

    /**
     * Create an instance of {@link CreateNewCustomerResponse }
     * 
     */
    public CreateNewCustomerResponse createCreateNewCustomerResponse() {
        return new CreateNewCustomerResponse();
    }

    /**
     * Create an instance of {@link SDCCreateCustomerResponse }
     * 
     */
    public SDCCreateCustomerResponse createSDCCreateCustomerResponse() {
        return new SDCCreateCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCreatedCustomer }
     * 
     */
    public UpdateCreatedCustomer createUpdateCreatedCustomer() {
        return new UpdateCreatedCustomer();
    }

    /**
     * Create an instance of {@link SDCCustomerUpdateTable }
     * 
     */
    public SDCCustomerUpdateTable createSDCCustomerUpdateTable() {
        return new SDCCustomerUpdateTable();
    }

    /**
     * Create an instance of {@link ViewCustomerBalanceResponse }
     * 
     */
    public ViewCustomerBalanceResponse createViewCustomerBalanceResponse() {
        return new ViewCustomerBalanceResponse();
    }

    /**
     * Create an instance of {@link SDCViewCustomerBalanceResponse }
     * 
     */
    public SDCViewCustomerBalanceResponse createSDCViewCustomerBalanceResponse() {
        return new SDCViewCustomerBalanceResponse();
    }

    /**
     * Create an instance of {@link GetCreatedCustomerResponse }
     * 
     */
    public GetCreatedCustomerResponse createGetCreatedCustomerResponse() {
        return new GetCreatedCustomerResponse();
    }

    /**
     * Create an instance of {@link SDCGetCreatedCustomerResponse }
     * 
     */
    public SDCGetCreatedCustomerResponse createSDCGetCreatedCustomerResponse() {
        return new SDCGetCreatedCustomerResponse();
    }

    /**
     * Create an instance of {@link ViewCustomerTransactionsResponse }
     * 
     */
    public ViewCustomerTransactionsResponse createViewCustomerTransactionsResponse() {
        return new ViewCustomerTransactionsResponse();
    }

    /**
     * Create an instance of {@link SDCViewCustomerTransactionsResponse }
     * 
     */
    public SDCViewCustomerTransactionsResponse createSDCViewCustomerTransactionsResponse() {
        return new SDCViewCustomerTransactionsResponse();
    }

    /**
     * Create an instance of {@link CreateNewCustomer }
     * 
     */
    public CreateNewCustomer createCreateNewCustomer() {
        return new CreateNewCustomer();
    }

    /**
     * Create an instance of {@link TopUpCustomerDeposit }
     * 
     */
    public TopUpCustomerDeposit createTopUpCustomerDeposit() {
        return new TopUpCustomerDeposit();
    }

    /**
     * Create an instance of {@link DepositDetails }
     * 
     */
    public DepositDetails createDepositDetails() {
        return new DepositDetails();
    }

    /**
     * Create an instance of {@link UpdateCreatedCustomerResponse }
     * 
     */
    public UpdateCreatedCustomerResponse createUpdateCreatedCustomerResponse() {
        return new UpdateCreatedCustomerResponse();
    }

    /**
     * Create an instance of {@link SDCUpdateCreatedCustomerResponse }
     * 
     */
    public SDCUpdateCreatedCustomerResponse createSDCUpdateCreatedCustomerResponse() {
        return new SDCUpdateCreatedCustomerResponse();
    }

    /**
     * Create an instance of {@link GetCapacityGroup }
     * 
     */
    public GetCapacityGroup createGetCapacityGroup() {
        return new GetCapacityGroup();
    }

    /**
     * Create an instance of {@link GetPriceGroup }
     * 
     */
    public GetPriceGroup createGetPriceGroup() {
        return new GetPriceGroup();
    }

    /**
     * Create an instance of {@link GetCustomerBalanceResponse }
     * 
     */
    public GetCustomerBalanceResponse createGetCustomerBalanceResponse() {
        return new GetCustomerBalanceResponse();
    }

    /**
     * Create an instance of {@link SDCCustomerBalanceResponse }
     * 
     */
    public SDCCustomerBalanceResponse createSDCCustomerBalanceResponse() {
        return new SDCCustomerBalanceResponse();
    }

    /**
     * Create an instance of {@link GetRegion }
     * 
     */
    public GetRegion createGetRegion() {
        return new GetRegion();
    }

    /**
     * Create an instance of {@link GetPriceGroupResponse }
     * 
     */
    public GetPriceGroupResponse createGetPriceGroupResponse() {
        return new GetPriceGroupResponse();
    }

    /**
     * Create an instance of {@link SDCGetPriceGroupResponse }
     * 
     */
    public SDCGetPriceGroupResponse createSDCGetPriceGroupResponse() {
        return new SDCGetPriceGroupResponse();
    }

    /**
     * Create an instance of {@link SDCMarketDistribution }
     * 
     */
    public SDCMarketDistribution createSDCMarketDistribution() {
        return new SDCMarketDistribution();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCustomerTable }
     * 
     */
    public ArrayOfSDCCustomerTable createArrayOfSDCCustomerTable() {
        return new ArrayOfSDCCustomerTable();
    }

    /**
     * Create an instance of {@link SDCCustomerTable }
     * 
     */
    public SDCCustomerTable createSDCCustomerTable() {
        return new SDCCustomerTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCountryRegionTable }
     * 
     */
    public ArrayOfSDCCountryRegionTable createArrayOfSDCCountryRegionTable() {
        return new ArrayOfSDCCountryRegionTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCLineOfBusiness }
     * 
     */
    public ArrayOfSDCLineOfBusiness createArrayOfSDCLineOfBusiness() {
        return new ArrayOfSDCLineOfBusiness();
    }

    /**
     * Create an instance of {@link ArrayOfSDCMarketDistribution }
     * 
     */
    public ArrayOfSDCMarketDistribution createArrayOfSDCMarketDistribution() {
        return new ArrayOfSDCMarketDistribution();
    }

    /**
     * Create an instance of {@link SDCLineOfBusiness }
     * 
     */
    public SDCLineOfBusiness createSDCLineOfBusiness() {
        return new SDCLineOfBusiness();
    }

    /**
     * Create an instance of {@link SDCCountryRegionTable }
     * 
     */
    public SDCCountryRegionTable createSDCCountryRegionTable() {
        return new SDCCountryRegionTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPriceGroup }
     * 
     */
    public ArrayOfSDCPriceGroup createArrayOfSDCPriceGroup() {
        return new ArrayOfSDCPriceGroup();
    }

    /**
     * Create an instance of {@link SDCGetCapacityGroupTable }
     * 
     */
    public SDCGetCapacityGroupTable createSDCGetCapacityGroupTable() {
        return new SDCGetCapacityGroupTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCDocumentLink }
     * 
     */
    public ArrayOfSDCDocumentLink createArrayOfSDCDocumentLink() {
        return new ArrayOfSDCDocumentLink();
    }

    /**
     * Create an instance of {@link SDCDocumentLink }
     * 
     */
    public SDCDocumentLink createSDCDocumentLink() {
        return new SDCDocumentLink();
    }

    /**
     * Create an instance of {@link ArrayOfSDCRegionTable }
     * 
     */
    public ArrayOfSDCRegionTable createArrayOfSDCRegionTable() {
        return new ArrayOfSDCRegionTable();
    }

    /**
     * Create an instance of {@link SDCRegionTable }
     * 
     */
    public SDCRegionTable createSDCRegionTable() {
        return new SDCRegionTable();
    }

    /**
     * Create an instance of {@link SDCPriceGroup }
     * 
     */
    public SDCPriceGroup createSDCPriceGroup() {
        return new SDCPriceGroup();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCustomerBalance }
     * 
     */
    public ArrayOfSDCCustomerBalance createArrayOfSDCCustomerBalance() {
        return new ArrayOfSDCCustomerBalance();
    }

    /**
     * Create an instance of {@link ArrayOfSDCGetCapacityGroupTable }
     * 
     */
    public ArrayOfSDCGetCapacityGroupTable createArrayOfSDCGetCapacityGroupTable() {
        return new ArrayOfSDCGetCapacityGroupTable();
    }

    /**
     * Create an instance of {@link SDCCustomerBalance }
     * 
     */
    public SDCCustomerBalance createSDCCustomerBalance() {
        return new SDCCustomerBalance();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link CommerceEntity }
     * 
     */
    public CommerceEntity createCommerceEntity() {
        return new CommerceEntity();
    }

    /**
     * Create an instance of {@link ArrayOfCommerceProperty }
     * 
     */
    public ArrayOfCommerceProperty createArrayOfCommerceProperty() {
        return new ArrayOfCommerceProperty();
    }

    /**
     * Create an instance of {@link CommerceProperty }
     * 
     */
    public CommerceProperty createCommerceProperty() {
        return new CommerceProperty();
    }

    /**
     * Create an instance of {@link CommercePropertyValue }
     * 
     */
    public CommercePropertyValue createCommercePropertyValue() {
        return new CommercePropertyValue();
    }

    /**
     * Create an instance of {@link CustomerBalances }
     * 
     */
    public CustomerBalances createCustomerBalances() {
        return new CustomerBalances();
    }

    /**
     * Create an instance of {@link DateTimeOffset }
     * 
     */
    public DateTimeOffset createDateTimeOffset() {
        return new DateTimeOffset();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerTransactionDetails }
     * 
     */
    public ArrayOfCustomerTransactionDetails createArrayOfCustomerTransactionDetails() {
        return new ArrayOfCustomerTransactionDetails();
    }

    /**
     * Create an instance of {@link CustomerTransactionDetails }
     * 
     */
    public CustomerTransactionDetails createCustomerTransactionDetails() {
        return new CustomerTransactionDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerUpdateTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CustomerUpdateTable")
    public JAXBElement<SDCCustomerUpdateTable> createSDCCustomerUpdateTable(SDCCustomerUpdateTable value) {
        return new JAXBElement<SDCCustomerUpdateTable>(_SDCCustomerUpdateTable_QNAME, SDCCustomerUpdateTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCLineOfBusiness }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_LineOfBusiness")
    public JAXBElement<SDCLineOfBusiness> createSDCLineOfBusiness(SDCLineOfBusiness value) {
        return new JAXBElement<SDCLineOfBusiness>(_SDCLineOfBusiness_QNAME, SDCLineOfBusiness.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCountryRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CountryRegionTable")
    public JAXBElement<SDCCountryRegionTable> createSDCCountryRegionTable(SDCCountryRegionTable value) {
        return new JAXBElement<SDCCountryRegionTable>(_SDCCountryRegionTable_QNAME, SDCCountryRegionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCountryRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CountryRegionTable")
    public JAXBElement<ArrayOfSDCCountryRegionTable> createArrayOfSDCCountryRegionTable(ArrayOfSDCCountryRegionTable value) {
        return new JAXBElement<ArrayOfSDCCountryRegionTable>(_ArrayOfSDCCountryRegionTable_QNAME, ArrayOfSDCCountryRegionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCMarketDistribution }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_MarketDistribution")
    public JAXBElement<SDCMarketDistribution> createSDCMarketDistribution(SDCMarketDistribution value) {
        return new JAXBElement<SDCMarketDistribution>(_SDCMarketDistribution_QNAME, SDCMarketDistribution.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceProperty")
    public JAXBElement<CommerceProperty> createCommercePropertyBase(CommerceProperty value) {
        return new JAXBElement<CommerceProperty>(_CommercePropertyBase_QNAME, CommerceProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CustomerBalance")
    public JAXBElement<SDCCustomerBalance> createSDCCustomerBalance(SDCCustomerBalance value) {
        return new JAXBElement<SDCCustomerBalance>(_SDCCustomerBalance_QNAME, SDCCustomerBalance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCGetCapacityGroupTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_GetCapacityGroupTable")
    public JAXBElement<ArrayOfSDCGetCapacityGroupTable> createArrayOfSDCGetCapacityGroupTable(ArrayOfSDCGetCapacityGroupTable value) {
        return new JAXBElement<ArrayOfSDCGetCapacityGroupTable>(_ArrayOfSDCGetCapacityGroupTable_QNAME, ArrayOfSDCGetCapacityGroupTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "CustomerTransactionDetails")
    public JAXBElement<CustomerTransactionDetails> createCustomerTransactionDetails(CustomerTransactionDetails value) {
        return new JAXBElement<CustomerTransactionDetails>(_CustomerTransactionDetails_QNAME, CustomerTransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPriceGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_PriceGroup")
    public JAXBElement<ArrayOfSDCPriceGroup> createArrayOfSDCPriceGroup(ArrayOfSDCPriceGroup value) {
        return new JAXBElement<ArrayOfSDCPriceGroup>(_ArrayOfSDCPriceGroup_QNAME, ArrayOfSDCPriceGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetCapacityGroupTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_GetCapacityGroupTable")
    public JAXBElement<SDCGetCapacityGroupTable> createSDCGetCapacityGroupTable(SDCGetCapacityGroupTable value) {
        return new JAXBElement<SDCGetCapacityGroupTable>(_SDCGetCapacityGroupTable_QNAME, SDCGetCapacityGroupTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommercePropertyValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommercePropertyValue")
    public JAXBElement<CommercePropertyValue> createCommercePropertyValueBase(CommercePropertyValue value) {
        return new JAXBElement<CommercePropertyValue>(_CommercePropertyValueBase_QNAME, CommercePropertyValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/System", name = "DateTimeOffset")
    public JAXBElement<DateTimeOffset> createDateTimeOffset(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_DateTimeOffset_QNAME, DateTimeOffset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRegionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_RegionResponse")
    public JAXBElement<SDCRegionResponse> createSDCRegionResponse(SDCRegionResponse value) {
        return new JAXBElement<SDCRegionResponse>(_SDCRegionResponse_QNAME, SDCRegionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBalances }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CustomerBalances")
    public JAXBElement<CustomerBalances> createCustomerBalancesBase(CustomerBalances value) {
        return new JAXBElement<CustomerBalances>(_CustomerBalancesBase_QNAME, CustomerBalances.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCountryRegionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_CountryRegionResponse")
    public JAXBElement<SDCCountryRegionResponse> createSDCCountryRegionResponse(SDCCountryRegionResponse value) {
        return new JAXBElement<SDCCountryRegionResponse>(_SDCCountryRegionResponse_QNAME, SDCCountryRegionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "ArrayOfCustomerTransactionDetails")
    public JAXBElement<ArrayOfCustomerTransactionDetails> createArrayOfCustomerTransactionDetails(ArrayOfCustomerTransactionDetails value) {
        return new JAXBElement<ArrayOfCustomerTransactionDetails>(_ArrayOfCustomerTransactionDetails_QNAME, ArrayOfCustomerTransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CustomerBalance")
    public JAXBElement<ArrayOfSDCCustomerBalance> createArrayOfSDCCustomerBalance(ArrayOfSDCCustomerBalance value) {
        return new JAXBElement<ArrayOfSDCCustomerBalance>(_ArrayOfSDCCustomerBalance_QNAME, ArrayOfSDCCustomerBalance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCLineOfBusinessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_LineOfBusinessResponse")
    public JAXBElement<SDCLineOfBusinessResponse> createSDCLineOfBusinessResponse(SDCLineOfBusinessResponse value) {
        return new JAXBElement<SDCLineOfBusinessResponse>(_SDCLineOfBusinessResponse_QNAME, SDCLineOfBusinessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_RegionTable")
    public JAXBElement<ArrayOfSDCRegionTable> createArrayOfSDCRegionTable(ArrayOfSDCRegionTable value) {
        return new JAXBElement<ArrayOfSDCRegionTable>(_ArrayOfSDCRegionTable_QNAME, ArrayOfSDCRegionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTopUpCustomerDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_TopUpCustomerDepositResponse")
    public JAXBElement<SDCTopUpCustomerDepositResponse> createSDCTopUpCustomerDepositResponse(SDCTopUpCustomerDepositResponse value) {
        return new JAXBElement<SDCTopUpCustomerDepositResponse>(_SDCTopUpCustomerDepositResponse_QNAME, SDCTopUpCustomerDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCMarketDistribution }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_MarketDistribution")
    public JAXBElement<ArrayOfSDCMarketDistribution> createArrayOfSDCMarketDistribution(ArrayOfSDCMarketDistribution value) {
        return new JAXBElement<ArrayOfSDCMarketDistribution>(_ArrayOfSDCMarketDistribution_QNAME, ArrayOfSDCMarketDistribution.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetCapacityGrpResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetCapacityGrpResponse")
    public JAXBElement<SDCGetCapacityGrpResponse> createSDCGetCapacityGrpResponse(SDCGetCapacityGrpResponse value) {
        return new JAXBElement<SDCGetCapacityGrpResponse>(_SDCGetCapacityGrpResponse_QNAME, SDCGetCapacityGrpResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CustomerTable")
    public JAXBElement<ArrayOfSDCCustomerTable> createArrayOfSDCCustomerTable(ArrayOfSDCCustomerTable value) {
        return new JAXBElement<ArrayOfSDCCustomerTable>(_ArrayOfSDCCustomerTable_QNAME, ArrayOfSDCCustomerTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "DepositDetails")
    public JAXBElement<DepositDetails> createDepositDetails(DepositDetails value) {
        return new JAXBElement<DepositDetails>(_DepositDetails_QNAME, DepositDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_CustomerBalanceResponse")
    public JAXBElement<SDCCustomerBalanceResponse> createSDCCustomerBalanceResponse(SDCCustomerBalanceResponse value) {
        return new JAXBElement<SDCCustomerBalanceResponse>(_SDCCustomerBalanceResponse_QNAME, SDCCustomerBalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfCommerceProperty")
    public JAXBElement<ArrayOfCommerceProperty> createArrayOfCommerceProperty(ArrayOfCommerceProperty value) {
        return new JAXBElement<ArrayOfCommerceProperty>(_ArrayOfCommerceProperty_QNAME, ArrayOfCommerceProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPriceGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_PriceGroup")
    public JAXBElement<SDCPriceGroup> createSDCPriceGroup(SDCPriceGroup value) {
        return new JAXBElement<SDCPriceGroup>(_SDCPriceGroup_QNAME, SDCPriceGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCViewCustomerTransactionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_ViewCustomerTransactionsResponse")
    public JAXBElement<SDCViewCustomerTransactionsResponse> createSDCViewCustomerTransactionsResponse(SDCViewCustomerTransactionsResponse value) {
        return new JAXBElement<SDCViewCustomerTransactionsResponse>(_SDCViewCustomerTransactionsResponse_QNAME, SDCViewCustomerTransactionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CustomerTable")
    public JAXBElement<SDCCustomerTable> createSDCCustomerTable(SDCCustomerTable value) {
        return new JAXBElement<SDCCustomerTable>(_SDCCustomerTable_QNAME, SDCCustomerTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCreateCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_CreateCustomerResponse")
    public JAXBElement<SDCCreateCustomerResponse> createSDCCreateCustomerResponse(SDCCreateCustomerResponse value) {
        return new JAXBElement<SDCCreateCustomerResponse>(_SDCCreateCustomerResponse_QNAME, SDCCreateCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCLineOfBusiness }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_LineOfBusiness")
    public JAXBElement<ArrayOfSDCLineOfBusiness> createArrayOfSDCLineOfBusiness(ArrayOfSDCLineOfBusiness value) {
        return new JAXBElement<ArrayOfSDCLineOfBusiness>(_ArrayOfSDCLineOfBusiness_QNAME, ArrayOfSDCLineOfBusiness.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCViewCustomerBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_ViewCustomerBalanceResponse")
    public JAXBElement<SDCViewCustomerBalanceResponse> createSDCViewCustomerBalanceResponse(SDCViewCustomerBalanceResponse value) {
        return new JAXBElement<SDCViewCustomerBalanceResponse>(_SDCViewCustomerBalanceResponse_QNAME, SDCViewCustomerBalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpdateCreatedCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_UpdateCreatedCustomerResponse")
    public JAXBElement<SDCUpdateCreatedCustomerResponse> createSDCUpdateCreatedCustomerResponse(SDCUpdateCreatedCustomerResponse value) {
        return new JAXBElement<SDCUpdateCreatedCustomerResponse>(_SDCUpdateCreatedCustomerResponse_QNAME, SDCUpdateCreatedCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetPriceGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetPriceGroupResponse")
    public JAXBElement<SDCGetPriceGroupResponse> createSDCGetPriceGroupResponse(SDCGetPriceGroupResponse value) {
        return new JAXBElement<SDCGetPriceGroupResponse>(_SDCGetPriceGroupResponse_QNAME, SDCGetPriceGroupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceEntity")
    public JAXBElement<CommerceEntity> createCommerceEntityBase(CommerceEntity value) {
        return new JAXBElement<CommerceEntity>(_CommerceEntityBase_QNAME, CommerceEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetCreatedCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetCreatedCustomerResponse")
    public JAXBElement<SDCGetCreatedCustomerResponse> createSDCGetCreatedCustomerResponse(SDCGetCreatedCustomerResponse value) {
        return new JAXBElement<SDCGetCreatedCustomerResponse>(_SDCGetCreatedCustomerResponse_QNAME, SDCGetCreatedCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCDocumentLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_DocumentLink")
    public JAXBElement<ArrayOfSDCDocumentLink> createArrayOfSDCDocumentLink(ArrayOfSDCDocumentLink value) {
        return new JAXBElement<ArrayOfSDCDocumentLink>(_ArrayOfSDCDocumentLink_QNAME, ArrayOfSDCDocumentLink.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCDocumentLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_DocumentLink")
    public JAXBElement<SDCDocumentLink> createSDCDocumentLink(SDCDocumentLink value) {
        return new JAXBElement<SDCDocumentLink>(_SDCDocumentLink_QNAME, SDCDocumentLink.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_RegionTable")
    public JAXBElement<SDCRegionTable> createSDCRegionTable(SDCRegionTable value) {
        return new JAXBElement<SDCRegionTable>(_SDCRegionTable_QNAME, SDCRegionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_countriesInterestedIn", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableCountriesInterestedIn(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCountriesInterestedIn_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_languageId", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableLanguageId(String value) {
        return new JAXBElement<String>(_SDCCustomerTableLanguageId_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UENNumber", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableUENNumber(String value) {
        return new JAXBElement<String>(_SDCCustomerTableUENNumber_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ContactPersonStr", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableContactPersonStr(String value) {
        return new JAXBElement<String>(_SDCCustomerTableContactPersonStr_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_revalidationUnit", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableRevalidationUnit(String value) {
        return new JAXBElement<String>(_SDCCustomerTableRevalidationUnit_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_branchName", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableBranchName(String value) {
        return new JAXBElement<String>(_SDCCustomerTableBranchName_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Designation", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableDesignation(String value) {
        return new JAXBElement<String>(_SDCCustomerTableDesignation_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_TALicenseNumber", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableTALicenseNumber(String value) {
        return new JAXBElement<String>(_SDCCustomerTableTALicenseNumber_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DocumentLinkDelimited", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableDocumentLinkDelimited(String value) {
        return new JAXBElement<String>(_SDCCustomerTableDocumentLinkDelimited_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CountryRegion", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableCountryRegion(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCountryRegion_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineOfBusiness", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableLineOfBusiness(String value) {
        return new JAXBElement<String>(_SDCCustomerTableLineOfBusiness_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MarketDistributionDelimited", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableMarketDistributionDelimited(String value) {
        return new JAXBElement<String>(_SDCCustomerTableMarketDistributionDelimited_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_revalidationFee", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableRevalidationFee(String value) {
        return new JAXBElement<String>(_SDCCustomerTableRevalidationFee_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCDocumentLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DocumentLinkLines", scope = SDCCustomerTable.class)
    public JAXBElement<ArrayOfSDCDocumentLink> createSDCCustomerTableDocumentLinkLines(ArrayOfSDCDocumentLink value) {
        return new JAXBElement<ArrayOfSDCDocumentLink>(_SDCCustomerTableDocumentLinkLines_QNAME, ArrayOfSDCDocumentLink.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_officeNo", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableOfficeNo(String value) {
        return new JAXBElement<String>(_SDCCustomerTableOfficeNo_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Fax", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableFax(String value) {
        return new JAXBElement<String>(_SDCCustomerTableFax_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountMgrCMS", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableAccountMgrCMS(String value) {
        return new JAXBElement<String>(_SDCCustomerTableAccountMgrCMS_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_capacityGroupId", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableCapacityGroupId(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCapacityGroupId_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_mainAccLoginName", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableMainAccLoginName(String value) {
        return new JAXBElement<String>(_SDCCustomerTableMainAccLoginName_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "FaxExtension", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableFaxExtension(String value) {
        return new JAXBElement<String>(_SDCCustomerTableFaxExtension_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCMarketDistribution }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MarketDistributionLines", scope = SDCCustomerTable.class)
    public JAXBElement<ArrayOfSDCMarketDistribution> createSDCCustomerTableMarketDistributionLines(ArrayOfSDCMarketDistribution value) {
        return new JAXBElement<ArrayOfSDCMarketDistribution>(_SDCCustomerTableMarketDistributionLines_QNAME, ArrayOfSDCMarketDistribution.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCustomerTable.class)
    public JAXBElement<String> createSDCCustomerTableAccountNum(String value) {
        return new JAXBElement<String>(_SDCCustomerTableAccountNum_QNAME, String.class, SDCCustomerTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPriceGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "priceGroupEntity", scope = SDCGetPriceGroupResponse.class)
    public JAXBElement<ArrayOfSDCPriceGroup> createSDCGetPriceGroupResponsePriceGroupEntity(ArrayOfSDCPriceGroup value) {
        return new JAXBElement<ArrayOfSDCPriceGroup>(_SDCGetPriceGroupResponsePriceGroupEntity_QNAME, ArrayOfSDCPriceGroup.class, SDCGetPriceGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetCreatedCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCreatedCustomerResult", scope = GetCreatedCustomerResponse.class)
    public JAXBElement<SDCGetCreatedCustomerResponse> createGetCreatedCustomerResponseGetCreatedCustomerResult(SDCGetCreatedCustomerResponse value) {
        return new JAXBElement<SDCGetCreatedCustomerResponse>(_GetCreatedCustomerResponseGetCreatedCustomerResult_QNAME, SDCGetCreatedCustomerResponse.class, GetCreatedCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCViewCustomerTransactionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ViewCustomerTransactionsResult", scope = ViewCustomerTransactionsResponse.class)
    public JAXBElement<SDCViewCustomerTransactionsResponse> createViewCustomerTransactionsResponseViewCustomerTransactionsResult(SDCViewCustomerTransactionsResponse value) {
        return new JAXBElement<SDCViewCustomerTransactionsResponse>(_ViewCustomerTransactionsResponseViewCustomerTransactionsResult_QNAME, SDCViewCustomerTransactionsResponse.class, ViewCustomerTransactionsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCLineOfBusiness }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "lineOfBusinessEntity", scope = SDCLineOfBusinessResponse.class)
    public JAXBElement<ArrayOfSDCLineOfBusiness> createSDCLineOfBusinessResponseLineOfBusinessEntity(ArrayOfSDCLineOfBusiness value) {
        return new JAXBElement<ArrayOfSDCLineOfBusiness>(_SDCLineOfBusinessResponseLineOfBusinessEntity_QNAME, ArrayOfSDCLineOfBusiness.class, SDCLineOfBusinessResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCLineOfBusiness.class)
    public JAXBElement<String> createSDCLineOfBusinessDescription(String value) {
        return new JAXBElement<String>(_SDCLineOfBusinessDescription_QNAME, String.class, SDCLineOfBusiness.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineOfBusinessId", scope = SDCLineOfBusiness.class)
    public JAXBElement<String> createSDCLineOfBusinessLineOfBusinessId(String value) {
        return new JAXBElement<String>(_SDCLineOfBusinessLineOfBusinessId_QNAME, String.class, SDCLineOfBusiness.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "customerBalanceEntity", scope = SDCCustomerBalanceResponse.class)
    public JAXBElement<ArrayOfSDCCustomerBalance> createSDCCustomerBalanceResponseCustomerBalanceEntity(ArrayOfSDCCustomerBalance value) {
        return new JAXBElement<ArrayOfSDCCustomerBalance>(_SDCCustomerBalanceResponseCustomerBalanceEntity_QNAME, ArrayOfSDCCustomerBalance.class, SDCCustomerBalanceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custAccountNum", scope = GetCreatedCustomer.class)
    public JAXBElement<String> createGetCreatedCustomerCustAccountNum(String value) {
        return new JAXBElement<String>(_GetCreatedCustomerCustAccountNum_QNAME, String.class, GetCreatedCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "createCustomer", scope = SDCCreateCustomerResponse.class)
    public JAXBElement<String> createSDCCreateCustomerResponseCreateCustomer(String value) {
        return new JAXBElement<String>(_SDCCreateCustomerResponseCreateCustomer_QNAME, String.class, SDCCreateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCountryRegionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCountryRegionResult", scope = GetCountryRegionResponse.class)
    public JAXBElement<SDCCountryRegionResponse> createGetCountryRegionResponseGetCountryRegionResult(SDCCountryRegionResponse value) {
        return new JAXBElement<SDCCountryRegionResponse>(_GetCountryRegionResponseGetCountryRegionResult_QNAME, SDCCountryRegionResponse.class, GetCountryRegionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerUpdateTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custTable", scope = UpdateCreatedCustomer.class)
    public JAXBElement<SDCCustomerUpdateTable> createUpdateCreatedCustomerCustTable(SDCCustomerUpdateTable value) {
        return new JAXBElement<SDCCustomerUpdateTable>(_UpdateCreatedCustomerCustTable_QNAME, SDCCustomerUpdateTable.class, UpdateCreatedCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DocumentName", scope = SDCDocumentLink.class)
    public JAXBElement<String> createSDCDocumentLinkDocumentName(String value) {
        return new JAXBElement<String>(_SDCDocumentLinkDocumentName_QNAME, String.class, SDCDocumentLink.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Link", scope = SDCDocumentLink.class)
    public JAXBElement<String> createSDCDocumentLinkLink(String value) {
        return new JAXBElement<String>(_SDCDocumentLinkLink_QNAME, String.class, SDCDocumentLink.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCGetCapacityGroupTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "getCapacityGroup", scope = SDCGetCapacityGrpResponse.class)
    public JAXBElement<ArrayOfSDCGetCapacityGroupTable> createSDCGetCapacityGrpResponseGetCapacityGroup(ArrayOfSDCGetCapacityGroupTable value) {
        return new JAXBElement<ArrayOfSDCGetCapacityGroupTable>(_SDCGetCapacityGrpResponseGetCapacityGroup_QNAME, ArrayOfSDCGetCapacityGroupTable.class, SDCGetCapacityGrpResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpdateCreatedCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "UpdateCreatedCustomerResult", scope = UpdateCreatedCustomerResponse.class)
    public JAXBElement<SDCUpdateCreatedCustomerResponse> createUpdateCreatedCustomerResponseUpdateCreatedCustomerResult(SDCUpdateCreatedCustomerResponse value) {
        return new JAXBElement<SDCUpdateCreatedCustomerResponse>(_UpdateCreatedCustomerResponseUpdateCreatedCustomerResult_QNAME, SDCUpdateCreatedCustomerResponse.class, UpdateCreatedCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetPriceGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetPriceGroupResult", scope = GetPriceGroupResponse.class)
    public JAXBElement<SDCGetPriceGroupResponse> createGetPriceGroupResponseGetPriceGroupResult(SDCGetPriceGroupResponse value) {
        return new JAXBElement<SDCGetPriceGroupResponse>(_GetPriceGroupResponseGetPriceGroupResult_QNAME, SDCGetPriceGroupResponse.class, GetPriceGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custAccount", scope = ViewCustomerBalance.class)
    public JAXBElement<String> createViewCustomerBalanceCustAccount(String value) {
        return new JAXBElement<String>(_ViewCustomerBalanceCustAccount_QNAME, String.class, ViewCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "currencyCode", scope = ViewCustomerBalance.class)
    public JAXBElement<String> createViewCustomerBalanceCurrencyCode(String value) {
        return new JAXBElement<String>(_ViewCustomerBalanceCurrencyCode_QNAME, String.class, ViewCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCLineOfBusinessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetLineOfBusinessResult", scope = GetLineOfBusinessResponse.class)
    public JAXBElement<SDCLineOfBusinessResponse> createGetLineOfBusinessResponseGetLineOfBusinessResult(SDCLineOfBusinessResponse value) {
        return new JAXBElement<SDCLineOfBusinessResponse>(_GetLineOfBusinessResponseGetLineOfBusinessResult_QNAME, SDCLineOfBusinessResponse.class, GetLineOfBusinessResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerUpdateTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custTable", scope = CreateNewCustomer.class)
    public JAXBElement<SDCCustomerUpdateTable> createCreateNewCustomerCustTable(SDCCustomerUpdateTable value) {
        return new JAXBElement<SDCCustomerUpdateTable>(_UpdateCreatedCustomerCustTable_QNAME, SDCCustomerUpdateTable.class, CreateNewCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custAccount", scope = ViewCustomerTransactions.class)
    public JAXBElement<String> createViewCustomerTransactionsCustAccount(String value) {
        return new JAXBElement<String>(_ViewCustomerBalanceCustAccount_QNAME, String.class, ViewCustomerTransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "currencyCode", scope = ViewCustomerTransactions.class)
    public JAXBElement<String> createViewCustomerTransactionsCurrencyCode(String value) {
        return new JAXBElement<String>(_ViewCustomerBalanceCurrencyCode_QNAME, String.class, ViewCustomerTransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCViewCustomerBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ViewCustomerBalanceResult", scope = ViewCustomerBalanceResponse.class)
    public JAXBElement<SDCViewCustomerBalanceResponse> createViewCustomerBalanceResponseViewCustomerBalanceResult(SDCViewCustomerBalanceResponse value) {
        return new JAXBElement<SDCViewCustomerBalanceResponse>(_ViewCustomerBalanceResponseViewCustomerBalanceResult_QNAME, SDCViewCustomerBalanceResponse.class, ViewCustomerBalanceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "updateCustomer", scope = SDCUpdateCreatedCustomerResponse.class)
    public JAXBElement<String> createSDCUpdateCreatedCustomerResponseUpdateCustomer(String value) {
        return new JAXBElement<String>(_SDCUpdateCreatedCustomerResponseUpdateCustomer_QNAME, String.class, SDCUpdateCreatedCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "accountNum", scope = GetCustomerBalance.class)
    public JAXBElement<String> createGetCustomerBalanceAccountNum(String value) {
        return new JAXBElement<String>(_GetCustomerBalanceAccountNum_QNAME, String.class, GetCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "currencyCode", scope = GetCustomerBalance.class)
    public JAXBElement<String> createGetCustomerBalanceCurrencyCode(String value) {
        return new JAXBElement<String>(_ViewCustomerBalanceCurrencyCode_QNAME, String.class, GetCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustomerBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCustomerBalanceResult", scope = GetCustomerBalanceResponse.class)
    public JAXBElement<SDCCustomerBalanceResponse> createGetCustomerBalanceResponseGetCustomerBalanceResult(SDCCustomerBalanceResponse value) {
        return new JAXBElement<SDCCustomerBalanceResponse>(_GetCustomerBalanceResponseGetCustomerBalanceResult_QNAME, SDCCustomerBalanceResponse.class, GetCustomerBalanceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LongValue", scope = CommercePropertyValue.class)
    public JAXBElement<Long> createCommercePropertyValueLongValue(Long value) {
        return new JAXBElement<Long>(_CommercePropertyValueLongValue_QNAME, Long.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StringValue", scope = CommercePropertyValue.class)
    public JAXBElement<String> createCommercePropertyValueStringValue(String value) {
        return new JAXBElement<String>(_CommercePropertyValueStringValue_QNAME, String.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ByteValue", scope = CommercePropertyValue.class)
    public JAXBElement<Short> createCommercePropertyValueByteValue(Short value) {
        return new JAXBElement<Short>(_CommercePropertyValueByteValue_QNAME, Short.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "BooleanValue", scope = CommercePropertyValue.class)
    public JAXBElement<Boolean> createCommercePropertyValueBooleanValue(Boolean value) {
        return new JAXBElement<Boolean>(_CommercePropertyValueBooleanValue_QNAME, Boolean.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "IntegerValue", scope = CommercePropertyValue.class)
    public JAXBElement<Integer> createCommercePropertyValueIntegerValue(Integer value) {
        return new JAXBElement<Integer>(_CommercePropertyValueIntegerValue_QNAME, Integer.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DateTimeOffsetValue", scope = CommercePropertyValue.class)
    public JAXBElement<DateTimeOffset> createCommercePropertyValueDateTimeOffsetValue(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CommercePropertyValueDateTimeOffsetValue_QNAME, DateTimeOffset.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DecimalValue", scope = CommercePropertyValue.class)
    public JAXBElement<BigDecimal> createCommercePropertyValueDecimalValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CommercePropertyValueDecimalValue_QNAME, BigDecimal.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCountryRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "getCountryRegion", scope = SDCCountryRegionResponse.class)
    public JAXBElement<ArrayOfSDCCountryRegionTable> createSDCCountryRegionResponseGetCountryRegion(ArrayOfSDCCountryRegionTable value) {
        return new JAXBElement<ArrayOfSDCCountryRegionTable>(_SDCCountryRegionResponseGetCountryRegion_QNAME, ArrayOfSDCCountryRegionTable.class, SDCCountryRegionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custDepositDetails", scope = TopUpCustomerDeposit.class)
    public JAXBElement<DepositDetails> createTopUpCustomerDepositCustDepositDetails(DepositDetails value) {
        return new JAXBElement<DepositDetails>(_TopUpCustomerDepositCustDepositDetails_QNAME, DepositDetails.class, TopUpCustomerDeposit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RegionId", scope = SDCCountryRegionTable.class)
    public JAXBElement<String> createSDCCountryRegionTableRegionId(String value) {
        return new JAXBElement<String>(_SDCCountryRegionTableRegionId_QNAME, String.class, SDCCountryRegionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CountryId", scope = SDCCountryRegionTable.class)
    public JAXBElement<String> createSDCCountryRegionTableCountryId(String value) {
        return new JAXBElement<String>(_SDCCountryRegionTableCountryId_QNAME, String.class, SDCCountryRegionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CountryName", scope = SDCCountryRegionTable.class)
    public JAXBElement<String> createSDCCountryRegionTableCountryName(String value) {
        return new JAXBElement<String>(_SDCCountryRegionTableCountryName_QNAME, String.class, SDCCountryRegionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Name", scope = SDCPriceGroup.class)
    public JAXBElement<String> createSDCPriceGroupName(String value) {
        return new JAXBElement<String>(_SDCPriceGroupName_QNAME, String.class, SDCPriceGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PriceGroupId", scope = SDCPriceGroup.class)
    public JAXBElement<String> createSDCPriceGroupPriceGroupId(String value) {
        return new JAXBElement<String>(_SDCPriceGroupPriceGroupId_QNAME, String.class, SDCPriceGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "TenderTypeId", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsTenderTypeId(String value) {
        return new JAXBElement<String>(_DepositDetailsTenderTypeId_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "DueDate", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsDueDate(String value) {
        return new JAXBElement<String>(_DepositDetailsDueDate_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "DocumentDate", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsDocumentDate(String value) {
        return new JAXBElement<String>(_DepositDetailsDocumentDate_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "CurrencyCode", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsCurrencyCode(String value) {
        return new JAXBElement<String>(_DepositDetailsCurrencyCode_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "Txt", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsTxt(String value) {
        return new JAXBElement<String>(_DepositDetailsTxt_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "AccountNum", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsAccountNum(String value) {
        return new JAXBElement<String>(_DepositDetailsAccountNum_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "DocumentNum", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsDocumentNum(String value) {
        return new JAXBElement<String>(_DepositDetailsDocumentNum_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "PostingDate", scope = DepositDetails.class)
    public JAXBElement<String> createDepositDetailsPostingDate(String value) {
        return new JAXBElement<String>(_DepositDetailsPostingDate_QNAME, String.class, DepositDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetCapacityGrpResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCapacityGroupResult", scope = GetCapacityGroupResponse.class)
    public JAXBElement<SDCGetCapacityGrpResponse> createGetCapacityGroupResponseGetCapacityGroupResult(SDCGetCapacityGrpResponse value) {
        return new JAXBElement<SDCGetCapacityGrpResponse>(_GetCapacityGroupResponseGetCapacityGroupResult_QNAME, SDCGetCapacityGrpResponse.class, GetCapacityGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "TransType", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsTransType(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsTransType_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "Voucher", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsVoucher(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsVoucher_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "SalesPoolId", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsSalesPoolId(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsSalesPoolId_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "Type", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsType(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsType_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "Invoice", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsInvoice(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsInvoice_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "PaymentReference", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsPaymentReference(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsPaymentReference_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "CurrencyCode", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsCurrencyCode(String value) {
        return new JAXBElement<String>(_DepositDetailsCurrencyCode_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "DocumentNum", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsDocumentNum(String value) {
        return new JAXBElement<String>(_DepositDetailsDocumentNum_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "Qty", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsQty(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsQty_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", name = "OrderAccount", scope = CustomerTransactionDetails.class)
    public JAXBElement<String> createCustomerTransactionDetailsOrderAccount(String value) {
        return new JAXBElement<String>(_CustomerTransactionDetailsOrderAccount_QNAME, String.class, CustomerTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "customerEntity", scope = SDCGetCreatedCustomerResponse.class)
    public JAXBElement<ArrayOfSDCCustomerTable> createSDCGetCreatedCustomerResponseCustomerEntity(ArrayOfSDCCustomerTable value) {
        return new JAXBElement<ArrayOfSDCCustomerTable>(_SDCGetCreatedCustomerResponseCustomerEntity_QNAME, ArrayOfSDCCustomerTable.class, SDCGetCreatedCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRegionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetRegionResult", scope = GetRegionResponse.class)
    public JAXBElement<SDCRegionResponse> createGetRegionResponseGetRegionResult(SDCRegionResponse value) {
        return new JAXBElement<SDCRegionResponse>(_GetRegionResponseGetRegionResult_QNAME, SDCRegionResponse.class, GetRegionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommercePropertyValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Value", scope = CommerceProperty.class)
    public JAXBElement<CommercePropertyValue> createCommercePropertyValue(CommercePropertyValue value) {
        return new JAXBElement<CommercePropertyValue>(_CommercePropertyValue_QNAME, CommercePropertyValue.class, CommerceProperty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Key", scope = CommerceProperty.class)
    public JAXBElement<String> createCommercePropertyKey(String value) {
        return new JAXBElement<String>(_CommercePropertyKey_QNAME, String.class, CommerceProperty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "CustomerTransactionDetails", scope = SDCViewCustomerTransactionsResponse.class)
    public JAXBElement<ArrayOfCustomerTransactionDetails> createSDCViewCustomerTransactionsResponseCustomerTransactionDetails(ArrayOfCustomerTransactionDetails value) {
        return new JAXBElement<ArrayOfCustomerTransactionDetails>(_SDCViewCustomerTransactionsResponseCustomerTransactionDetails_QNAME, ArrayOfCustomerTransactionDetails.class, SDCViewCustomerTransactionsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "errorMessage", scope = SDCTopUpCustomerDepositResponse.class)
    public JAXBElement<String> createSDCTopUpCustomerDepositResponseErrorMessage(String value) {
        return new JAXBElement<String>(_SDCTopUpCustomerDepositResponseErrorMessage_QNAME, String.class, SDCTopUpCustomerDepositResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RegionId", scope = SDCRegionTable.class)
    public JAXBElement<String> createSDCRegionTableRegionId(String value) {
        return new JAXBElement<String>(_SDCCountryRegionTableRegionId_QNAME, String.class, SDCRegionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCRegionTable.class)
    public JAXBElement<String> createSDCRegionTableDescription(String value) {
        return new JAXBElement<String>(_SDCLineOfBusinessDescription_QNAME, String.class, SDCRegionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCreateCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CreateNewCustomerResult", scope = CreateNewCustomerResponse.class)
    public JAXBElement<SDCCreateCustomerResponse> createCreateNewCustomerResponseCreateNewCustomerResult(SDCCreateCustomerResponse value) {
        return new JAXBElement<SDCCreateCustomerResponse>(_CreateNewCustomerResponseCreateNewCustomerResult_QNAME, SDCCreateCustomerResponse.class, CreateNewCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCTopUpCustomerDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "TopUpCustomerDepositResult", scope = TopUpCustomerDepositResponse.class)
    public JAXBElement<SDCTopUpCustomerDepositResponse> createTopUpCustomerDepositResponseTopUpCustomerDepositResult(SDCTopUpCustomerDepositResponse value) {
        return new JAXBElement<SDCTopUpCustomerDepositResponse>(_TopUpCustomerDepositResponseTopUpCustomerDepositResult_QNAME, SDCTopUpCustomerDepositResponse.class, TopUpCustomerDepositResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRegionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "getRegion", scope = SDCRegionResponse.class)
    public JAXBElement<ArrayOfSDCRegionTable> createSDCRegionResponseGetRegion(ArrayOfSDCRegionTable value) {
        return new JAXBElement<ArrayOfSDCRegionTable>(_SDCRegionResponseGetRegion_QNAME, ArrayOfSDCRegionTable.class, SDCRegionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ExtensionProperties", scope = CommerceEntity.class)
    public JAXBElement<ArrayOfCommerceProperty> createCommerceEntityExtensionProperties(ArrayOfCommerceProperty value) {
        return new JAXBElement<ArrayOfCommerceProperty>(_CommerceEntityExtensionProperties_QNAME, ArrayOfCommerceProperty.class, CommerceEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBalances }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "CustomerBalanceDetails", scope = SDCViewCustomerBalanceResponse.class)
    public JAXBElement<CustomerBalances> createSDCViewCustomerBalanceResponseCustomerBalanceDetails(CustomerBalances value) {
        return new JAXBElement<CustomerBalances>(_SDCViewCustomerBalanceResponseCustomerBalanceDetails_QNAME, CustomerBalances.class, SDCViewCustomerBalanceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Percent", scope = SDCMarketDistribution.class)
    public JAXBElement<String> createSDCMarketDistributionPercent(String value) {
        return new JAXBElement<String>(_SDCMarketDistributionPercent_QNAME, String.class, SDCMarketDistribution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesManager", scope = SDCMarketDistribution.class)
    public JAXBElement<String> createSDCMarketDistributionSalesManager(String value) {
        return new JAXBElement<String>(_SDCMarketDistributionSalesManager_QNAME, String.class, SDCMarketDistribution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CountryRegion", scope = SDCMarketDistribution.class)
    public JAXBElement<String> createSDCMarketDistributionCountryRegion(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCountryRegion_QNAME, String.class, SDCMarketDistribution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CurrencyCode", scope = SDCCustomerBalance.class)
    public JAXBElement<String> createSDCCustomerBalanceCurrencyCode(String value) {
        return new JAXBElement<String>(_SDCCustomerBalanceCurrencyCode_QNAME, String.class, SDCCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCustomerBalance.class)
    public JAXBElement<String> createSDCCustomerBalanceAccountNum(String value) {
        return new JAXBElement<String>(_SDCCustomerTableAccountNum_QNAME, String.class, SDCCustomerBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_countriesInterestedIn", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableCountriesInterestedIn(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCountriesInterestedIn_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_languageId", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableLanguageId(String value) {
        return new JAXBElement<String>(_SDCCustomerTableLanguageId_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UENNumber", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableUENNumber(String value) {
        return new JAXBElement<String>(_SDCCustomerTableUENNumber_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ContactPersonStr", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableContactPersonStr(String value) {
        return new JAXBElement<String>(_SDCCustomerTableContactPersonStr_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_branchName", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableBranchName(String value) {
        return new JAXBElement<String>(_SDCCustomerTableBranchName_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Designation", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableDesignation(String value) {
        return new JAXBElement<String>(_SDCCustomerTableDesignation_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_TALicenseNumber", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableTALicenseNumber(String value) {
        return new JAXBElement<String>(_SDCCustomerTableTALicenseNumber_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DocumentLinkDelimited", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableDocumentLinkDelimited(String value) {
        return new JAXBElement<String>(_SDCCustomerTableDocumentLinkDelimited_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CountryRegion", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableCountryRegion(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCountryRegion_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineOfBusiness", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableLineOfBusiness(String value) {
        return new JAXBElement<String>(_SDCCustomerTableLineOfBusiness_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MarketDistributionDelimited", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableMarketDistributionDelimited(String value) {
        return new JAXBElement<String>(_SDCCustomerTableMarketDistributionDelimited_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_revalidationFee", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableRevalidationFee(String value) {
        return new JAXBElement<String>(_SDCCustomerTableRevalidationFee_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCDocumentLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DocumentLinkLines", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<ArrayOfSDCDocumentLink> createSDCCustomerUpdateTableDocumentLinkLines(ArrayOfSDCDocumentLink value) {
        return new JAXBElement<ArrayOfSDCDocumentLink>(_SDCCustomerTableDocumentLinkLines_QNAME, ArrayOfSDCDocumentLink.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_officeNo", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableOfficeNo(String value) {
        return new JAXBElement<String>(_SDCCustomerTableOfficeNo_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Fax", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableFax(String value) {
        return new JAXBElement<String>(_SDCCustomerTableFax_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountMgrCMS", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableAccountMgrCMS(String value) {
        return new JAXBElement<String>(_SDCCustomerTableAccountMgrCMS_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_capacityGroupId", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableCapacityGroupId(String value) {
        return new JAXBElement<String>(_SDCCustomerTableCapacityGroupId_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "_mainAccLoginName", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableMainAccLoginName(String value) {
        return new JAXBElement<String>(_SDCCustomerTableMainAccLoginName_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "FaxExtension", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableFaxExtension(String value) {
        return new JAXBElement<String>(_SDCCustomerTableFaxExtension_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCMarketDistribution }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MarketDistributionLines", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<ArrayOfSDCMarketDistribution> createSDCCustomerUpdateTableMarketDistributionLines(ArrayOfSDCMarketDistribution value) {
        return new JAXBElement<ArrayOfSDCMarketDistribution>(_SDCCustomerTableMarketDistributionLines_QNAME, ArrayOfSDCMarketDistribution.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCustomerUpdateTable.class)
    public JAXBElement<String> createSDCCustomerUpdateTableAccountNum(String value) {
        return new JAXBElement<String>(_SDCCustomerTableAccountNum_QNAME, String.class, SDCCustomerUpdateTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CapacityGroupId", scope = SDCGetCapacityGroupTable.class)
    public JAXBElement<String> createSDCGetCapacityGroupTableCapacityGroupId(String value) {
        return new JAXBElement<String>(_SDCGetCapacityGroupTableCapacityGroupId_QNAME, String.class, SDCGetCapacityGroupTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCGetCapacityGroupTable.class)
    public JAXBElement<String> createSDCGetCapacityGroupTableDescription(String value) {
        return new JAXBElement<String>(_SDCLineOfBusinessDescription_QNAME, String.class, SDCGetCapacityGroupTable.class, value);
    }

}
