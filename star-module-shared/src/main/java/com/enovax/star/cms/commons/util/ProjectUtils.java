package com.enovax.star.cms.commons.util;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import org.apache.commons.lang3.RandomStringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class ProjectUtils {

    public static final String RECEIPT_PREFIX = "STAR";
    public static final String RECEIPT_DATE_FORMAT = "yyMMdd";
    public static final String RECEIPT_STRING_GEN_RANGE = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890";
    public static final int RECEIPT_STRING_GEN_LENGTH = 6;

    public static String generateReceiptNumber(Date date) {
        final SimpleDateFormat sdf = new SimpleDateFormat(RECEIPT_DATE_FORMAT);
        final String df = sdf.format(date);

        return RECEIPT_PREFIX + df + RandomStringUtils.random(RECEIPT_STRING_GEN_LENGTH, RECEIPT_STRING_GEN_RANGE);
    }

    public static String generateReceiptNumber(StoreApiChannels channel, Date date) {
        final SimpleDateFormat sdf = new SimpleDateFormat(RECEIPT_DATE_FORMAT);
        final String df = sdf.format(date);

        return channel.receiptPrefix + df + RandomStringUtils.random(RECEIPT_STRING_GEN_LENGTH, RECEIPT_STRING_GEN_RANGE);
    }

    public static String generateReservationRedeemCode() {
        return RandomStringUtils.random(RECEIPT_STRING_GEN_LENGTH, RECEIPT_STRING_GEN_RANGE);
    }

    public static String generateUniqueLogId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String appendLogId(String message, String logId) {
        return message + " [error-ref : " + logId + "]";
    }
}
