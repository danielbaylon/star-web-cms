package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = PPMFLGTAMainAccount.TABLE_NAME)
public class PPMFLGTAMainAccount extends PPMFLGTAAccount implements Serializable{
    public static final String TABLE_NAME = "PPMFLGTAMainAccount";


    @Column(name = "accountCode", nullable = false)
    private String accountCode;

    @Column(name = "subAccountEnabled", nullable = false)
    private Boolean subAccountEnabled;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="user", targetEntity=PPMFLGTAPrevPasswordMain.class)
    private List<PPMFLGPrevPassword> prevPws = new ArrayList<PPMFLGPrevPassword>();


    @OneToMany(mappedBy="user")
    private List<PPMFLGTAMainRightsMapping> rightsMapping = new ArrayList<PPMFLGTAMainRightsMapping>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="mainAccountId")
    private List<PPMFLGTASubAccount> subAccs = new ArrayList<PPMFLGTASubAccount>();

    @OneToOne(fetch=FetchType.EAGER, mappedBy="mainAccount")
    private PPMFLGPartner profile;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public List<PPMFLGPrevPassword> getPrevPws() {
        return prevPws;
    }
    public void setPrevPws(List<PPMFLGPrevPassword> prevPws) {
        this.prevPws = prevPws;
    }

    public List<PPMFLGTASubAccount> getSubAccs() {
        return subAccs;
    }
    public void setSubAccs(List<PPMFLGTASubAccount> subAccs) {
        this.subAccs = subAccs;
    }

    public List<PPMFLGTAMainRightsMapping> getRightsMapping() {
        return rightsMapping;
    }

    public void setRightsMapping(List<PPMFLGTAMainRightsMapping> rightsMapping) {
        this.rightsMapping = rightsMapping;
    }

    public PPMFLGPartner getProfile() {
        return profile;
    }

    public void setProfile(PPMFLGPartner profile) {
        this.profile = profile;
    }

    public Boolean getSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(Boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }


}
