package com.enovax.star.cms.commons.model.kiosk.odata;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.ConnectionReques
 * 
 * @author Justin
 * @since 6 SEP 16
 */
public class AxConnectionRequest implements Serializable{

	@JsonProperty("DeviceNumber")
	private String deviceNumber;

	@JsonProperty("DeviceToken")
	private String deviceToken;

	@JsonProperty("UserId")
	private String userId;

	@JsonProperty("Password")
	private String password;

	@JsonProperty("AuthenticationProvider")
	private String authenticationProvider;

	@JsonProperty("LogOnKey")
	private String logOnKey;

	@JsonProperty("LogOnType")
	private String logOnType;

	@JsonProperty("TransactionId")
	private String transactionId;

	//@JsonProperty("ExtraData")
	//private String extraData;

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthenticationProvider() {
		return authenticationProvider;
	}

	public void setAuthenticationProvider(String authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}

	public String getLogOnKey() {
		return logOnKey;
	}

	public void setLogOnKey(String logOnKey) {
		this.logOnKey = logOnKey;
	}

	public String getLogOnType() {
		return logOnType;
	}

	public void setLogOnType(String logOnType) {
		this.logOnType = logOnType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
/**
	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}
**/
}
