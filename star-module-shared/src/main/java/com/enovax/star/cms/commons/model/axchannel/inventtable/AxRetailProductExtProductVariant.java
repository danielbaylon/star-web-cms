package com.enovax.star.cms.commons.model.axchannel.inventtable;

public class AxRetailProductExtProductVariant {

    private String absoluteCapacityId;
    private String description;
    private Long distinctProductVariant;
    private Long ecoResDistinctProductVariant;
    private String inventDimId;
    private String name;
    private String packageId;
    private String productId;
    private String retailVariantId;

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDistinctProductVariant() {
        return distinctProductVariant;
    }

    public void setDistinctProductVariant(Long distinctProductVariant) {
        this.distinctProductVariant = distinctProductVariant;
    }

    public Long getEcoResDistinctProductVariant() {
        return ecoResDistinctProductVariant;
    }

    public void setEcoResDistinctProductVariant(Long ecoResDistinctProductVariant) {
        this.ecoResDistinctProductVariant = ecoResDistinctProductVariant;
    }

    public String getInventDimId() {
        return inventDimId;
    }

    public void setInventDimId(String inventDimId) {
        this.inventDimId = inventDimId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }
}
