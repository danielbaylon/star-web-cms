package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = PPSLMTAMainAccount.TABLE_NAME)
public class PPSLMTAMainAccount extends PPSLMTAAccount implements Serializable{
    public static final String TABLE_NAME = "PPSLMTAMainAccount";


    @Column(name = "accountCode", nullable = false)
    private String accountCode;

    @Column(name = "subAccountEnabled", nullable = false)
    private Boolean subAccountEnabled;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="user", targetEntity=PPSLMTAPrevPasswordMain.class)
    private List<PPSLMPrevPassword> prevPws = new ArrayList<PPSLMPrevPassword>();


    @OneToMany(mappedBy="user")
    private List<PPSLMTAMainRightsMapping> rightsMapping = new ArrayList<PPSLMTAMainRightsMapping>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="mainAccountId")
    private List<PPSLMTASubAccount> subAccs = new ArrayList<PPSLMTASubAccount>();

    @OneToOne(fetch=FetchType.EAGER, mappedBy="mainAccount")
    private PPSLMPartner profile;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public List<PPSLMPrevPassword> getPrevPws() {
        return prevPws;
    }
    public void setPrevPws(List<PPSLMPrevPassword> prevPws) {
        this.prevPws = prevPws;
    }

    public List<PPSLMTASubAccount> getSubAccs() {
        return subAccs;
    }
    public void setSubAccs(List<PPSLMTASubAccount> subAccs) {
        this.subAccs = subAccs;
    }

    public List<PPSLMTAMainRightsMapping> getRightsMapping() {
        return rightsMapping;
    }

    public void setRightsMapping(List<PPSLMTAMainRightsMapping> rightsMapping) {
        this.rightsMapping = rightsMapping;
    }

    public PPSLMPartner getProfile() {
        return profile;
    }

    public void setProfile(PPSLMPartner profile) {
        this.profile = profile;
    }

    public Boolean getSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(Boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }


}
