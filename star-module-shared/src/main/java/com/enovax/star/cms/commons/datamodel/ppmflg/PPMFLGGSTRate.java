package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by lavanya on 26/10/16.
 */
@Entity
@Table(name="PPMFLGGSTRate")
public class PPMFLGGSTRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;

    @Column(name="rate", nullable = false)
    private BigDecimal rate;

    @Column(name="startDate", nullable = false)
    private Date startDate;

    @Column(name="endDate")
    private Date endDate;

    @Column(name = "status")
    private String status;

    @Column(name="createdBy", nullable=false, length=100)
    private String createdBy;

    @Column(name="createdDate", nullable=false, length=23)
    private Date createdDate;

    @Column(name="modifiedBy", nullable=false, length=100)
    private String modifiedBy;

    @Column(name="modifiedDate", nullable=false, length=23)
    private Date modifiedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
