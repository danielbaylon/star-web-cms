package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.CartTenderLine
 * 
 * @author Justin
 *
 */
public class AxCartTenderLine {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

    @JsonProperty("TenderLineId")
    private String tenderLineId;

    @JsonProperty("TenderTypeId")
    private String tenderTypeId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTenderLineId() {
        return tenderLineId;
    }

    public void setTenderLineId(String tenderLineId) {
        this.tenderLineId = tenderLineId;
    }

    public String getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(String tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }

}
