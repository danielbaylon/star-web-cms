package com.enovax.star.cms.commons.model.booking.promotions;

import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.FunCartAffiliation;

/**
 * Note: Only 1 action at a time, e.g. add bank affiliation, remove bank affiliation, etc.
 */
public class ApplyAffiliationRequest {

    private boolean isRemove;
    private boolean isBankAffiliation;
    private FunCartAffiliation affiliationToAdd;
    private CustomerDetails deets;

    public boolean isRemove() {
        return isRemove;
    }

    public void setIsRemove(boolean remove) {
        isRemove = remove;
    }

    public boolean isBankAffiliation() {
        return isBankAffiliation;
    }

    public void setIsBankAffiliation(boolean bankAffiliation) {
        isBankAffiliation = bankAffiliation;
    }

    public FunCartAffiliation getAffiliationToAdd() {
        return affiliationToAdd;
    }

    public void setAffiliationToAdd(FunCartAffiliation affiliationToAdd) {
        this.affiliationToAdd = affiliationToAdd;
    }

    public CustomerDetails getDeets() {
        return deets;
    }

    public void setDeets(CustomerDetails deets) {
        this.deets = deets;
    }
}
