package com.enovax.star.cms.commons.model.partner.ppmflg;
import java.util.Date;

public class TransQueryFilterVM {
    private String fromDtStr;
    private String toDtStr;
    private String receiptNum;
    private String orgName;
    private String accountCode;
    private Date fromDate;
    private Date toDate;
    private Integer[] paIds;
    private String paymentType;
    private String mixedStatus;
    
    public String getFromDtStr() {
        return fromDtStr;
    }

    public void setFromDtStr(String fromDtStr) {
        this.fromDtStr = fromDtStr;
    }

    public String getToDtStr() {
        return toDtStr;
    }

    public void setToDtStr(String toDtStr) {
        this.toDtStr = toDtStr;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public Integer[] getPaIds() {
        return paIds;
    }

    public void setPaIds(Integer[] paIds) {
        this.paIds = paIds;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getMixedStatus() {
        return mixedStatus;
    }

    public void setMixedStatus(String mixedStatus) {
        this.mixedStatus = mixedStatus;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
