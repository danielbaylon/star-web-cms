
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchProductExtResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_InventTableResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchProductExtResult"
})
@XmlRootElement(name = "SearchProductExtResponse", namespace = "http://tempuri.org/")
public class SearchProductExtResponse {

    @XmlElementRef(name = "SearchProductExtResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCInventTableResponse> searchProductExtResult;

    /**
     * Gets the value of the searchProductExtResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCInventTableResponse }{@code >}
     *     
     */
    public JAXBElement<SDCInventTableResponse> getSearchProductExtResult() {
        return searchProductExtResult;
    }

    /**
     * Sets the value of the searchProductExtResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCInventTableResponse }{@code >}
     *     
     */
    public void setSearchProductExtResult(JAXBElement<SDCInventTableResponse> value) {
        this.searchProductExtResult = value;
    }

}
