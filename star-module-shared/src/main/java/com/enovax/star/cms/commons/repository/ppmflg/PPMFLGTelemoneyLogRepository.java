package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTelemoneyLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 14/11/16.
 */
@Repository
public interface PPMFLGTelemoneyLogRepository extends JpaRepository<PPMFLGTelemoneyLog, Integer> {

}
