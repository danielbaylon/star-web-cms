package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <ComplexType Name="UpdateRetailTicketStatusCriteria">
 * <Property Name="TicketCode" Type="Edm.String"/>
 * <Property Name="StaffId" Type="Edm.String"/>
 * <Property Name="ShiftId" Type="Edm.String"/>
 * <Property Name="StoreId" Type="Edm.String"/>
 * <Property Name="TerminalId" Type="Edm.String"/>
 * <Property Name="ReasonCode" Type="Edm.String"/>
 * <Property Name="TicketStatus" Type="Edm.Int32" Nullable="false"/>
 * <Property Name="TransactionId" Type="Edm.String"/>
 * <Property Name="DataLevelValue" Type="Edm.Int32" Nullable="false"/>
 * </ComplexType>
 * 
 * @author Justin
 *
 */
public class AxUpdateRetailTicketStatusCriteria {

    @JsonProperty("TicketCode")
    private String ticketCode;

    @JsonProperty("StaffId")
    private String staffId;

    @JsonProperty("ShiftId")
    private String shiftId;

    @JsonProperty("StoreId")
    private String storeId;

    @JsonProperty("TerminalId")
    private String terminalId;

    @JsonProperty("ReasonCode")
    private String reasonCode;

    @JsonProperty("TicketStatus")
    private String ticketStatus;

    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("DataLevelValue")
    private String dataLevelValue;

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDataLevelValue() {
        return dataLevelValue;
    }

    public void setDataLevelValue(String dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

}
