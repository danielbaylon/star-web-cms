package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.SalesLine
 * 
 * @author Justin
 *
 */
public class AxSalesLine {

    private String lineId;
    private String description;
    private String originLineId;
    private String taxOverrideCode;
    private String productId;
    private String barcode;
    private String masterProductId;
    private String listingId;
    private String isPriceOverridden;
    private String originalPrice;
    private String totalAmount;
    private String netAmountWithoutTax;
    private String discountAmount;
    private String totalDiscount;
    private String totalPercentageDiscount;
    private String lineDiscount;
    private String periodicDiscount;
    private String lineManualDiscountPercentage;
    private String lineManualDiscountAmount;
    // ShippingAddress"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.Address"/>
    private String deliveryMode;
    private String comment;
    private String requestedDeliveryDate;
    private String inventoryLocationId;
    private String inventoryDimensionId;
    private String itemType;
    private String scaleItem;
    private String reservationId;
    private String lineNumber;
    private String returnQuantity;
    private String statusValue;
    private String salesStatusValue;
    private String productSourceValue;
    private String isGiftCardLine;
    private String giftCardId;
    private String giftCardCurrencyCode;
    private String giftCardOperationValue;
    private String isInvoiceLine;
    private String invoiceId;
    private String invoiceAmount;
    private String isInvoiceSettled;
    private String isVoided;
    private String isPriceLocked;
    // ChargeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ChargeLine)"
    // Nullable="false"/>
    private String basePrice;
    private String agreementPrice;
    private String adjustedPrice;
    private String returnTransactionId;
    private String returnLineNumber;
    private String returnInventTransId;
    private String returnStore;
    private String returnTerminalId;
    private String returnChannelId;
    private String store;
    private String terminalId;
    private String salesDate;
    private String quantityInvoiced;
    private String quantityOrdered;
    private String recordId;
    private String serialNumber;
    private String batchId;
    private String deliveryModeChargeAmount;
    private String unitOfMeasureSymbol;
    private String catalogId;
    private String electronicDeliveryEmailAddress;
    private String electronicDeliveryEmailContent;
    private String loyaltyDiscountAmount;
    private String loyaltyPercentageDiscount;
    private String isCustomerAccountDeposit;
    private String blocked;
    private String found;
    private String dateToActivateItem;
    private String linePercentageDiscount;
    private String periodicPercentageDiscount;
    private String quantityDiscounted;
    private String unitQuantity;
    // UnitOfMeasureConversion"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.UnitOfMeasureConversion"/>
    // DiscountLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.DiscountLine)"
    // Nullable="false"/>
    // PeriodicDiscountPossibilities"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.DiscountLine)"
    // Nullable="false"/>
    // ReasonCodeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ReasonCodeLine)"
    // Nullable="false"/>
    // ReturnLabelProperties"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.ReturnLabelContent"/>
    private String lineMultilineDiscOnItem;
    private String linkedParentLineId;
    private String lineMultilineDiscOnItemValue;
    private String wasChanged;
    private String originalSalesOrderUnitOfMeasure;
    private String inventOrderUnitOfMeasure;
    private String isLoyaltyDiscountApplied;
    private String itemId;
    private String quantity;
    private String price;
    private String itemTaxGroupId;
    private String salesTaxGroupId;
    private String taxAmount;
    private String salesOrderUnitOfMeasure;
    private String netAmount;
    private String netAmountPerUnit;
    private String grossAmount;
    // TaxLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.TaxLine)"
    // Nullable="false"/>
    private String taxAmountExemptInclusive;
    private String taxAmountInclusive;
    private String taxAmountExclusive;
    private String netAmountWithAllInclusiveTax;
    private String beginDateTime;
    private String endDateTime;
    private String taxRatePercent;
    private String isReturnByReceipt;

    @JsonProperty("DiscountLines")
    private List<AxDiscountLine> discountLines;
    // ExtensionProperties"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)"
    // Nullable="false"/>
    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginLineId() {
        return originLineId;
    }

    public void setOriginLineId(String originLineId) {
        this.originLineId = originLineId;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getMasterProductId() {
        return masterProductId;
    }

    public void setMasterProductId(String masterProductId) {
        this.masterProductId = masterProductId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getIsPriceOverridden() {
        return isPriceOverridden;
    }

    public void setIsPriceOverridden(String isPriceOverridden) {
        this.isPriceOverridden = isPriceOverridden;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNetAmountWithoutTax() {
        return netAmountWithoutTax;
    }

    public void setNetAmountWithoutTax(String netAmountWithoutTax) {
        this.netAmountWithoutTax = netAmountWithoutTax;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPercentageDiscount() {
        return totalPercentageDiscount;
    }

    public void setTotalPercentageDiscount(String totalPercentageDiscount) {
        this.totalPercentageDiscount = totalPercentageDiscount;
    }

    public String getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(String lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public String getPeriodicDiscount() {
        return periodicDiscount;
    }

    public void setPeriodicDiscount(String periodicDiscount) {
        this.periodicDiscount = periodicDiscount;
    }

    public String getLineManualDiscountPercentage() {
        return lineManualDiscountPercentage;
    }

    public void setLineManualDiscountPercentage(String lineManualDiscountPercentage) {
        this.lineManualDiscountPercentage = lineManualDiscountPercentage;
    }

    public String getLineManualDiscountAmount() {
        return lineManualDiscountAmount;
    }

    public void setLineManualDiscountAmount(String lineManualDiscountAmount) {
        this.lineManualDiscountAmount = lineManualDiscountAmount;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getInventoryLocationId() {
        return inventoryLocationId;
    }

    public void setInventoryLocationId(String inventoryLocationId) {
        this.inventoryLocationId = inventoryLocationId;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getScaleItem() {
        return scaleItem;
    }

    public void setScaleItem(String scaleItem) {
        this.scaleItem = scaleItem;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getSalesStatusValue() {
        return salesStatusValue;
    }

    public void setSalesStatusValue(String salesStatusValue) {
        this.salesStatusValue = salesStatusValue;
    }

    public String getProductSourceValue() {
        return productSourceValue;
    }

    public void setProductSourceValue(String productSourceValue) {
        this.productSourceValue = productSourceValue;
    }

    public String getIsGiftCardLine() {
        return isGiftCardLine;
    }

    public void setIsGiftCardLine(String isGiftCardLine) {
        this.isGiftCardLine = isGiftCardLine;
    }

    public String getGiftCardId() {
        return giftCardId;
    }

    public void setGiftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
    }

    public String getGiftCardCurrencyCode() {
        return giftCardCurrencyCode;
    }

    public void setGiftCardCurrencyCode(String giftCardCurrencyCode) {
        this.giftCardCurrencyCode = giftCardCurrencyCode;
    }

    public String getGiftCardOperationValue() {
        return giftCardOperationValue;
    }

    public void setGiftCardOperationValue(String giftCardOperationValue) {
        this.giftCardOperationValue = giftCardOperationValue;
    }

    public String getIsInvoiceLine() {
        return isInvoiceLine;
    }

    public void setIsInvoiceLine(String isInvoiceLine) {
        this.isInvoiceLine = isInvoiceLine;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getIsInvoiceSettled() {
        return isInvoiceSettled;
    }

    public void setIsInvoiceSettled(String isInvoiceSettled) {
        this.isInvoiceSettled = isInvoiceSettled;
    }

    public String getIsVoided() {
        return isVoided;
    }

    public void setIsVoided(String isVoided) {
        this.isVoided = isVoided;
    }

    public String getIsPriceLocked() {
        return isPriceLocked;
    }

    public void setIsPriceLocked(String isPriceLocked) {
        this.isPriceLocked = isPriceLocked;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getAgreementPrice() {
        return agreementPrice;
    }

    public void setAgreementPrice(String agreementPrice) {
        this.agreementPrice = agreementPrice;
    }

    public String getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(String adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public String getReturnTransactionId() {
        return returnTransactionId;
    }

    public void setReturnTransactionId(String returnTransactionId) {
        this.returnTransactionId = returnTransactionId;
    }

    public String getReturnLineNumber() {
        return returnLineNumber;
    }

    public void setReturnLineNumber(String returnLineNumber) {
        this.returnLineNumber = returnLineNumber;
    }

    public String getReturnInventTransId() {
        return returnInventTransId;
    }

    public void setReturnInventTransId(String returnInventTransId) {
        this.returnInventTransId = returnInventTransId;
    }

    public String getReturnStore() {
        return returnStore;
    }

    public void setReturnStore(String returnStore) {
        this.returnStore = returnStore;
    }

    public String getReturnTerminalId() {
        return returnTerminalId;
    }

    public void setReturnTerminalId(String returnTerminalId) {
        this.returnTerminalId = returnTerminalId;
    }

    public String getReturnChannelId() {
        return returnChannelId;
    }

    public void setReturnChannelId(String returnChannelId) {
        this.returnChannelId = returnChannelId;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public String getQuantityInvoiced() {
        return quantityInvoiced;
    }

    public void setQuantityInvoiced(String quantityInvoiced) {
        this.quantityInvoiced = quantityInvoiced;
    }

    public String getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(String quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(String deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getElectronicDeliveryEmailAddress() {
        return electronicDeliveryEmailAddress;
    }

    public void setElectronicDeliveryEmailAddress(String electronicDeliveryEmailAddress) {
        this.electronicDeliveryEmailAddress = electronicDeliveryEmailAddress;
    }

    public String getElectronicDeliveryEmailContent() {
        return electronicDeliveryEmailContent;
    }

    public void setElectronicDeliveryEmailContent(String electronicDeliveryEmailContent) {
        this.electronicDeliveryEmailContent = electronicDeliveryEmailContent;
    }

    public String getLoyaltyDiscountAmount() {
        return loyaltyDiscountAmount;
    }

    public void setLoyaltyDiscountAmount(String loyaltyDiscountAmount) {
        this.loyaltyDiscountAmount = loyaltyDiscountAmount;
    }

    public String getLoyaltyPercentageDiscount() {
        return loyaltyPercentageDiscount;
    }

    public void setLoyaltyPercentageDiscount(String loyaltyPercentageDiscount) {
        this.loyaltyPercentageDiscount = loyaltyPercentageDiscount;
    }

    public String getIsCustomerAccountDeposit() {
        return isCustomerAccountDeposit;
    }

    public void setIsCustomerAccountDeposit(String isCustomerAccountDeposit) {
        this.isCustomerAccountDeposit = isCustomerAccountDeposit;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getFound() {
        return found;
    }

    public void setFound(String found) {
        this.found = found;
    }

    public String getDateToActivateItem() {
        return dateToActivateItem;
    }

    public void setDateToActivateItem(String dateToActivateItem) {
        this.dateToActivateItem = dateToActivateItem;
    }

    public String getLinePercentageDiscount() {
        return linePercentageDiscount;
    }

    public void setLinePercentageDiscount(String linePercentageDiscount) {
        this.linePercentageDiscount = linePercentageDiscount;
    }

    public String getPeriodicPercentageDiscount() {
        return periodicPercentageDiscount;
    }

    public void setPeriodicPercentageDiscount(String periodicPercentageDiscount) {
        this.periodicPercentageDiscount = periodicPercentageDiscount;
    }

    public String getQuantityDiscounted() {
        return quantityDiscounted;
    }

    public void setQuantityDiscounted(String quantityDiscounted) {
        this.quantityDiscounted = quantityDiscounted;
    }

    public String getUnitQuantity() {
        return unitQuantity;
    }

    public void setUnitQuantity(String unitQuantity) {
        this.unitQuantity = unitQuantity;
    }

    public String getLineMultilineDiscOnItem() {
        return lineMultilineDiscOnItem;
    }

    public void setLineMultilineDiscOnItem(String lineMultilineDiscOnItem) {
        this.lineMultilineDiscOnItem = lineMultilineDiscOnItem;
    }

    public String getLinkedParentLineId() {
        return linkedParentLineId;
    }

    public void setLinkedParentLineId(String linkedParentLineId) {
        this.linkedParentLineId = linkedParentLineId;
    }

    public String getLineMultilineDiscOnItemValue() {
        return lineMultilineDiscOnItemValue;
    }

    public void setLineMultilineDiscOnItemValue(String lineMultilineDiscOnItemValue) {
        this.lineMultilineDiscOnItemValue = lineMultilineDiscOnItemValue;
    }

    public String getWasChanged() {
        return wasChanged;
    }

    public void setWasChanged(String wasChanged) {
        this.wasChanged = wasChanged;
    }

    public String getOriginalSalesOrderUnitOfMeasure() {
        return originalSalesOrderUnitOfMeasure;
    }

    public void setOriginalSalesOrderUnitOfMeasure(String originalSalesOrderUnitOfMeasure) {
        this.originalSalesOrderUnitOfMeasure = originalSalesOrderUnitOfMeasure;
    }

    public String getInventOrderUnitOfMeasure() {
        return inventOrderUnitOfMeasure;
    }

    public void setInventOrderUnitOfMeasure(String inventOrderUnitOfMeasure) {
        this.inventOrderUnitOfMeasure = inventOrderUnitOfMeasure;
    }

    public String getIsLoyaltyDiscountApplied() {
        return isLoyaltyDiscountApplied;
    }

    public void setIsLoyaltyDiscountApplied(String isLoyaltyDiscountApplied) {
        this.isLoyaltyDiscountApplied = isLoyaltyDiscountApplied;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItemTaxGroupId() {
        return itemTaxGroupId;
    }

    public void setItemTaxGroupId(String itemTaxGroupId) {
        this.itemTaxGroupId = itemTaxGroupId;
    }

    public String getSalesTaxGroupId() {
        return salesTaxGroupId;
    }

    public void setSalesTaxGroupId(String salesTaxGroupId) {
        this.salesTaxGroupId = salesTaxGroupId;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getSalesOrderUnitOfMeasure() {
        return salesOrderUnitOfMeasure;
    }

    public void setSalesOrderUnitOfMeasure(String salesOrderUnitOfMeasure) {
        this.salesOrderUnitOfMeasure = salesOrderUnitOfMeasure;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getNetAmountPerUnit() {
        return netAmountPerUnit;
    }

    public void setNetAmountPerUnit(String netAmountPerUnit) {
        this.netAmountPerUnit = netAmountPerUnit;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getTaxAmountExemptInclusive() {
        return taxAmountExemptInclusive;
    }

    public void setTaxAmountExemptInclusive(String taxAmountExemptInclusive) {
        this.taxAmountExemptInclusive = taxAmountExemptInclusive;
    }

    public String getTaxAmountInclusive() {
        return taxAmountInclusive;
    }

    public void setTaxAmountInclusive(String taxAmountInclusive) {
        this.taxAmountInclusive = taxAmountInclusive;
    }

    public String getTaxAmountExclusive() {
        return taxAmountExclusive;
    }

    public void setTaxAmountExclusive(String taxAmountExclusive) {
        this.taxAmountExclusive = taxAmountExclusive;
    }

    public String getNetAmountWithAllInclusiveTax() {
        return netAmountWithAllInclusiveTax;
    }

    public void setNetAmountWithAllInclusiveTax(String netAmountWithAllInclusiveTax) {
        this.netAmountWithAllInclusiveTax = netAmountWithAllInclusiveTax;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getTaxRatePercent() {
        return taxRatePercent;
    }

    public void setTaxRatePercent(String taxRatePercent) {
        this.taxRatePercent = taxRatePercent;
    }

    public String getIsReturnByReceipt() {
        return isReturnByReceipt;
    }

    public void setIsReturnByReceipt(String isReturnByReceipt) {
        this.isReturnByReceipt = isReturnByReceipt;
    }
    
    public void setDiscountLines(List<AxDiscountLine> discountLines) {
		this.discountLines = discountLines;
	}
    
    public List<AxDiscountLine> getDiscountLines() {
		return discountLines;
	}

}
