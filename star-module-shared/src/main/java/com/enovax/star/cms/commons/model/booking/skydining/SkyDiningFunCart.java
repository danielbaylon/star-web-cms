package com.enovax.star.cms.commons.model.booking.skydining;

import com.enovax.star.cms.commons.model.booking.FunCart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCart extends FunCart {
  private List<SkyDiningFunCartItem> skyDiningItems = new ArrayList<>();

  public List<SkyDiningFunCartItem> getSkyDiningItems() {
    return skyDiningItems;
  }

  public void setSkyDiningItems(List<SkyDiningFunCartItem> skyDiningItems) {
    this.skyDiningItems = skyDiningItems;
  }
}
