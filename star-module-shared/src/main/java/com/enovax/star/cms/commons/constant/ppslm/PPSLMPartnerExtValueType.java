package com.enovax.star.cms.commons.constant.ppslm;

public enum PPSLMPartnerExtValueType {

    TRUE("true"),
    FALSE("false");

    public String code;

    private PPSLMPartnerExtValueType(String code) {
        this.code = code;
    }
}
