package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 5/17/16.
 */
public enum TicketType {
    Adult("Adult", 0),
    Child("Child", 1),
    Standard("Standard", 2),
    Special("Special", 4),
    AdultSet("AdultSet", 10000),
    ChildSet("ChildSet", 10001),
    StandardSet("StandardSet", 10002);

    public static final String NAME_STD = "Standard";

    public final String display;
    public final int type;
    private TicketType(String display, int type) {
        this.display = display;
        this.type = type;
    }

    public static final TicketType fromType(int type) {
        for (TicketType tt : values()) {
            if (tt.type == type) {
                return tt;
            }
        }
        return Standard; //If not found, consider as standard
    }

    public static final boolean isStandard(String type) {
        return !Adult.toString().equals(type) && !Child.toString().equals(type);
    }

    public static final boolean isStandard(TicketType type) {
        return !Adult.equals(type) && !Child.equals(type);
    }

    public static final TicketType getSetEquiv(int type) {
        return type == Adult.type ? AdultSet : type == Child.type ? ChildSet : StandardSet;
    }

    @Override
    public String toString() {
        return display;
    }
}
