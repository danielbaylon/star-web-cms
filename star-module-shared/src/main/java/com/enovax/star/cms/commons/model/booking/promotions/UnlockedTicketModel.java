package com.enovax.star.cms.commons.model.booking.promotions;

import com.enovax.star.cms.commons.model.jcrworkspace.AxProduct;

import java.io.Serializable;

/**
 * Created by jace on 9/9/16.
 */
public class UnlockedTicketModel extends AxProduct implements Serializable {

    private String promotionDisplayName;
    private String promotionIconImageURL;
    private String promotionText;
    private String additionalInfo;
    private String cachedDiscountPrice;
    private String ticketTypeText;
    private String productPriceText;

    public UnlockedTicketModel() {

    }

    public String getPromotionDisplayName() {
        return promotionDisplayName;
    }

    public void setPromotionDisplayName(String promotionDisplayName) {
        this.promotionDisplayName = promotionDisplayName;
    }

    public String getPromotionIconImageURL() {
        return promotionIconImageURL;
    }

    public void setPromotionIconImageURL(String promotionIconImageURL) {
        this.promotionIconImageURL = promotionIconImageURL;
    }

    public String getPromotionText() {
        return promotionText;
    }

    public void setPromotionText(String promotionText) {
        this.promotionText = promotionText;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getCachedDiscountPrice() {
        return cachedDiscountPrice;
    }

    public void setCachedDiscountPrice(String cachedDiscountPrice) {
        this.cachedDiscountPrice = cachedDiscountPrice;
    }

    public String getTicketTypeText() {
        return ticketTypeText;
    }

    public void setTicketTypeText(String ticketTypeText) {
        this.ticketTypeText = ticketTypeText;
    }

    public String getProductPriceText() {
        return productPriceText;
    }

    public void setProductPriceText(String productPriceText) {
        this.productPriceText = productPriceText;
    }
}
