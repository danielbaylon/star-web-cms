package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by houtao on 18/10/16.
 */
public class AxCapacityGroup {

    //Plese note that the ID is actually pertaining to the capacity group id, the name is actually the description of that group
    private String eventAllocationGroupID;
    private String name;

    public String getEventAllocationGroupID() {
        return eventAllocationGroupID;
    }

    public void setEventAllocationGroupID(String eventAllocationGroupID) {
        this.eventAllocationGroupID = eventAllocationGroupID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
