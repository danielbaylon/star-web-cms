package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class KioskCloseShiftRequest {

    private String forceClose = Boolean.FALSE.toString();

    private String transactionId;

    @JsonIgnore
    private String shiftId;

    @JsonIgnore
    private String terminalId;

    public String getForceClose() {
        return forceClose;
    }

    public void setForceClose(String forceClose) {
        this.forceClose = forceClose;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

}
