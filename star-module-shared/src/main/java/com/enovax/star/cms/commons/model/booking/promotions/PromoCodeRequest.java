package com.enovax.star.cms.commons.model.booking.promotions;

import java.io.Serializable;

/**
 * Created by jace on 9/9/16.
 */
public class PromoCodeRequest implements Serializable {

  private Boolean fromHeader;
  private String language;
  private String promoCode;
  private String productId;

  public PromoCodeRequest() {

  }

  public String getPromoCode() {
    return promoCode;
  }

  public void setPromoCode(String promoCode) {
    this.promoCode = promoCode;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Boolean getFromHeader() {
    return fromHeader;
  }

  public void setFromHeader(Boolean fromHeader) {
    this.fromHeader = fromHeader;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }
}
