package com.enovax.star.cms.commons.model.partner.ppslm;
import java.util.*;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class PackageViewResultVM {
    private Integer id;
    private String name;
    private String pinCode;
    private String pkgTktMedia;
    private Date ticketGeneratedDate;
    private String orgName;
    private Date lastRedemptionDate;
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPkgTktMedia() {
        return pkgTktMedia;
    }

    public void setPkgTktMedia(String pkgTktMedia) {
        this.pkgTktMedia = pkgTktMedia;
    }

    public Date getTicketGeneratedDate() {
        return ticketGeneratedDate;
    }

    public void setTicketGeneratedDate(Date ticketGeneratedDate) {
        this.ticketGeneratedDate = ticketGeneratedDate;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Date getLastRedemptionDate() {
        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(Date lastRedemptionDate) {
        this.lastRedemptionDate = lastRedemptionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}
