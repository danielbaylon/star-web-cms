package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.model.booking.FunCartDisplay;

import java.math.BigDecimal;

/**
 * Created by jennylynsze on 5/17/16.
 */
public class PartnerReceiptDisplay extends FunCartDisplay {

    private String dateOfPurchase;
    private String receiptNumber;
    private String paymentType;
    private String paymentTypeLabel;

    private String transStatus;
    private boolean isError;
    private String errorCode;
    private String errorMessage;
    private String message;

    private BigDecimal totalTotal;
    private String totalTotalText;

    private PartnerVM partner;

    private String validityStartDate;
    private String validityEndDate;

    private String username;

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeLabel() {
        return paymentTypeLabel;
    }

    public void setPaymentTypeLabel(String paymentTypeLabel) {
        this.paymentTypeLabel = paymentTypeLabel;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getTotalTotal() {
        return totalTotal;
    }

    public void setTotalTotal(BigDecimal totalTotal) {
        this.totalTotal = totalTotal;
    }

    public String getTotalTotalText() {
        return totalTotalText;
    }

    public void setTotalTotalText(String totalTotalText) {
        this.totalTotalText = totalTotalText;
    }

    public PartnerVM getPartner() {
        return partner;
    }

    public void setPartner(PartnerVM partner) {
        this.partner = partner;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public String getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(String validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public String getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(String validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
