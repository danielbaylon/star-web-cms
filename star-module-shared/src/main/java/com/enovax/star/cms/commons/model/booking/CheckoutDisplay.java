package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.model.merchant.Merchant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CheckoutDisplay extends FunCartDisplay {

  private boolean hasBookingFee;
  private String bookFeeMode;
  private int bookPriceInCents = 0;
  private String bookPriceText = "";
  private String bookFeeWaiverCode = "";
  private int bookFeeQty = 0;
  private BigDecimal bookFeeSubTotal = BigDecimal.ZERO;
  private String bookFeeSubTotalText = "";

  private String waivedTotalText;

  private CustomerDetails customer;
  private String customerJson;

  private String dateOfPurchase;
  private String receiptNumber;
  private String pin;
  private String barcodeUrl;
  private String baseUrl;
  private String paymentType;
  private String paymentTypeLabel;
  private String customerName;

  private boolean hasTnc;

  private boolean hasAd;
  private boolean hasAdLink;
  private String adLink;
  private String adUrl;

  private String transStatus;
  private boolean isError;
  private String errorCode;
  private String errorMessage;
  private String message;

  private String custServEmail;

  private String ccDigits;

  private String emailMsgStyle;

  private BigDecimal totalTotal;
  private String totalTotalText;

  private Boolean hasPromoMerchant;
  private List<Merchant> promoMerchants = new ArrayList<>();
  private String selectedMerchant;
  private List<FunCartAffiliation> affiliations = new ArrayList<>();
  private Boolean hasAffiliations = false;

  private BigDecimal gstRate = new BigDecimal(0.07);  //Default one

  public boolean isHasBookingFee() {
    return hasBookingFee;
  }

  public void setHasBookingFee(boolean hasBookingFee) {
    this.hasBookingFee = hasBookingFee;
  }

  public String getBookFeeMode() {
    return bookFeeMode;
  }

  public void setBookFeeMode(String bookFeeMode) {
    this.bookFeeMode = bookFeeMode;
  }

  public int getBookPriceInCents() {
    return bookPriceInCents;
  }

  public void setBookPriceInCents(int bookPriceInCents) {
    this.bookPriceInCents = bookPriceInCents;
  }

  public String getBookPriceText() {
    return bookPriceText;
  }

  public void setBookPriceText(String bookPriceText) {
    this.bookPriceText = bookPriceText;
  }

  public String getBookFeeWaiverCode() {
    return bookFeeWaiverCode;
  }

  public void setBookFeeWaiverCode(String bookFeeWaiverCode) {
    this.bookFeeWaiverCode = bookFeeWaiverCode;
  }

  public int getBookFeeQty() {
    return bookFeeQty;
  }

  public void setBookFeeQty(int bookFeeQty) {
    this.bookFeeQty = bookFeeQty;
  }

  public BigDecimal getBookFeeSubTotal() {
    return bookFeeSubTotal;
  }

  public void setBookFeeSubTotal(BigDecimal bookFeeSubTotal) {
    this.bookFeeSubTotal = bookFeeSubTotal;
  }

  public String getBookFeeSubTotalText() {
    return bookFeeSubTotalText;
  }

  public void setBookFeeSubTotalText(String bookFeeSubTotalText) {
    this.bookFeeSubTotalText = bookFeeSubTotalText;
  }

  public String getWaivedTotalText() {
    return waivedTotalText;
  }

  public void setWaivedTotalText(String waivedTotalText) {
    this.waivedTotalText = waivedTotalText;
  }

  public CustomerDetails getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerDetails customer) {
    this.customer = customer;
  }

  public String getCustomerJson() {
    return customerJson;
  }

  public void setCustomerJson(String customerJson) {
    this.customerJson = customerJson;
  }

  public String getDateOfPurchase() {
    return dateOfPurchase;
  }

  public void setDateOfPurchase(String dateOfPurchase) {
    this.dateOfPurchase = dateOfPurchase;
  }

  public String getReceiptNumber() {
    return receiptNumber;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }

  public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }

  public String getBarcodeUrl() {
    return barcodeUrl;
  }

  public void setBarcodeUrl(String barcodeUrl) {
    this.barcodeUrl = barcodeUrl;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public String getPaymentTypeLabel() {
    return paymentTypeLabel;
  }

  public void setPaymentTypeLabel(String paymentTypeLabel) {
    this.paymentTypeLabel = paymentTypeLabel;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public boolean isHasTnc() {
    return hasTnc;
  }

  public void setHasTnc(boolean hasTnc) {
    this.hasTnc = hasTnc;
  }

  public boolean isHasAd() {
    return hasAd;
  }

  public void setHasAd(boolean hasAd) {
    this.hasAd = hasAd;
  }

  public boolean isHasAdLink() {
    return hasAdLink;
  }

  public void setHasAdLink(boolean hasAdLink) {
    this.hasAdLink = hasAdLink;
  }

  public String getAdLink() {
    return adLink;
  }

  public void setAdLink(String adLink) {
    this.adLink = adLink;
  }

  public String getAdUrl() {
    return adUrl;
  }

  public void setAdUrl(String adUrl) {
    this.adUrl = adUrl;
  }

  public String getTransStatus() {
    return transStatus;
  }

  public void setTransStatus(String transStatus) {
    this.transStatus = transStatus;
  }

  public boolean isError() {
    return isError;
  }

  public void setError(boolean error) {
    isError = error;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCustServEmail() {
    return custServEmail;
  }

  public void setCustServEmail(String custServEmail) {
    this.custServEmail = custServEmail;
  }

  public String getCcDigits() {
    return ccDigits;
  }

  public void setCcDigits(String ccDigits) {
    this.ccDigits = ccDigits;
  }

  public String getEmailMsgStyle() {
    return emailMsgStyle;
  }

  public void setEmailMsgStyle(String emailMsgStyle) {
    this.emailMsgStyle = emailMsgStyle;
  }

  public BigDecimal getTotalTotal() {
    return totalTotal;
  }

  public void setTotalTotal(BigDecimal totalTotal) {
    this.totalTotal = totalTotal;
  }

  public String getTotalTotalText() {
    return totalTotalText;
  }

  public void setTotalTotalText(String totalTotalText) {
    this.totalTotalText = totalTotalText;
  }

  public Boolean getHasPromoMerchant() {
    return hasPromoMerchant;
  }

  public void setHasPromoMerchant(Boolean hasPromoMerchant) {
    this.hasPromoMerchant = hasPromoMerchant;
  }

  public List<Merchant> getPromoMerchants() {
    return promoMerchants;
  }

  public void setPromoMerchants(List<Merchant> promoMerchants) {
    this.promoMerchants = promoMerchants;
  }

  public String getSelectedMerchant() {
    return selectedMerchant;
  }

  public void setSelectedMerchant(String selectedMerchant) {
    this.selectedMerchant = selectedMerchant;
  }

  public void setIsError(boolean isError) {
    this.isError = isError;
  }

  public List<FunCartAffiliation> getAffiliations() {
    return affiliations;
  }

  public void setAffiliations(List<FunCartAffiliation> affiliations) {
    this.affiliations = affiliations;
  }

  public Boolean getHasAffiliations() {
    return hasAffiliations;
  }

  public void setHasAffiliations(Boolean hasAffiliations) {
    this.hasAffiliations = hasAffiliations;
  }

  @Override
  public BigDecimal getGstRate() {
    return gstRate;
  }

  @Override
  public void setGstRate(BigDecimal gstRate) {
    this.gstRate = gstRate;
  }
}
