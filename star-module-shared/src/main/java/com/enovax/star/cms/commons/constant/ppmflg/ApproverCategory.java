package com.enovax.star.cms.commons.constant.ppmflg;

public enum ApproverCategory {
    Partner("Partner","Partner"),
    SystemParam("SystemParam","System Param");

    public final String code;
    public final String label;

    private ApproverCategory(String code, String label) {
        this.code = code;
        this.label = label;
    }
    public static String getLabel(String code){
        for(ApproverCategory tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
    public static ApproverCategory fromCode(String code) {
        for (ApproverCategory f : values()) {
            if (f.code.equalsIgnoreCase(code)) {
                return f;
            }
        }
        return null;
    }
}
