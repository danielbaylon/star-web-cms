package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Justin
 *
 */
public class AxProductPropertyTranslation {

    public static final String EN = "en-us";

    private String translationLanguage;

    private List<AxProductProperty> axProductPropertyList = new ArrayList<>();

    public List<AxProductProperty> getAxProductPropertyList() {
        return axProductPropertyList;
    }

    public void setAxProductPropertyList(List<AxProductProperty> axProductPropertyList) {
        this.axProductPropertyList = axProductPropertyList;
    }

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

}
