package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lavanya on 13/9/16.
 */
public class ReceiptProdVM implements Serializable{
    private static final long serialVersionUID = 1L;
    private String prodId;
    private String displayTitle;
    private List<ReceiptItemVM> items;
    private String subTotalStr;
    private String subTotalRevalStr;


    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }


    public ReceiptProdVM(String prodId, String name) {
        this.prodId = prodId;
        this.displayTitle = name;
    }

    public void addProdItem(PPSLMInventoryTransaction trans) {
        items = new ArrayList<ReceiptItemVM>();
        BigDecimal subTotal = new BigDecimal(0);
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            if (this.prodId.equals(item.getProductId())) {
                ReceiptItemVM receiptItem = new ReceiptItemVM(item,trans.getGstRate());
                items.add(receiptItem);
                subTotal = subTotal.add(item.getSubTotal());
            }
        }
        this.subTotalStr = TransactionUtil.priceWithDecimal(subTotal , null);
        Collections.sort(items);
    }

//    public void addProdItem(InventoryTransaction trans,
//            BigDecimal normalRevalidatFee, BigDecimal topupRevalidatFee) {
//        items = new ArrayList<ReceiptItemVM>();
//        BigDecimal subTotal = new BigDecimal(0);
//        for (InventoryTransactionItem item : trans.getInventoryItems()) {
//            if (this.prodId.equals(item.getProductId())) {
//                ReceiptItemVM receiptItem = new ReceiptItemVM(item, trans
//                        .getRevalidateTrans().getGstRate(), normalRevalidatFee,
//                        topupRevalidatFee);
//                items.add(receiptItem);
//                if (TransItemType.Topup.toString().equals(
//                        item.getTransItemType())) {
//                    subTotal = subTotal.add(new BigDecimal(item.getQty())
//                            .multiply(topupRevalidatFee));
//                } else {
//                    subTotal = subTotal.add(new BigDecimal(item.getQty())
//                            .multiply(normalRevalidatFee));
//                }
//            }
//
//        }
//        this.subTotalRevalStr = TransactionUtil.priceWithDecimal(subTotal , null);
//        Collections.sort(items);
//    }

    public void addProdItem(PPSLMRevalidationTransaction revalTrans) {
        BigDecimal normailRevalFee = new BigDecimal(
                revalTrans.getRevalFeeInCents() / 100);
        BigDecimal topupRevalFee = new BigDecimal(
                revalTrans.getRevalTopupFeeInCents() / 100);
        items = new ArrayList<ReceiptItemVM>();
        BigDecimal subTotal = new BigDecimal(0);
        for (PPSLMInventoryTransactionItem item : revalTrans.getRevalItems()) {
            if (this.prodId.equals(item.getProductId())) {
                ReceiptItemVM receiptItem = new ReceiptItemVM(item,
                        revalTrans.getGstRate(), normailRevalFee,
                        topupRevalFee);
                items.add(receiptItem);
                if (TransItemType.Topup.toString().equals(
                        item.getTransItemType())) {
                    subTotal = subTotal.add(new BigDecimal(item.getQty())
                            .multiply(topupRevalFee));
                } else {
                    subTotal = subTotal.add(new BigDecimal(item.getQty())
                            .multiply(normailRevalFee));
                }
            }

        }
        this.subTotalRevalStr = TransactionUtil
                .priceWithDecimal(subTotal, null);
        Collections.sort(items);
    }

    public List<ReceiptItemVM> getItems() {
        return items;
    }

    public void setItems(List<ReceiptItemVM> items) {
        this.items = items;
    }

    public String getSubTotalStr() {
        return subTotalStr;
    }

    public void setSubTotalStr(String subTotalStr) {
        this.subTotalStr = subTotalStr;
    }

    public String getSubTotalRevalStr() {
        return subTotalRevalStr;
    }

    public void setSubTotalRevalStr(String subTotalRevalStr) {
        this.subTotalRevalStr = subTotalRevalStr;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof ReceiptProdVM))
            return false;
        ReceiptProdVM otherMyClass = (ReceiptProdVM) other;
        if (this.prodId.equals(otherMyClass.getProdId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        //TODO
        int result = 0;
        result = (int) serialVersionUID;
        return result;
    }
}
