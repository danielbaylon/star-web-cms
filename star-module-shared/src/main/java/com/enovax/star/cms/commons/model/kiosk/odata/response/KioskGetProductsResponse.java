package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxProduct;

/**
 * { "odata.metadata": "odata.count": "value": []}
 * 
 * @author Justin
 *
 */
public class KioskGetProductsResponse {

    private List<AxProduct> productList = new ArrayList<>();

    public List<AxProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<AxProduct> productList) {
        this.productList = productList;
    }

}
