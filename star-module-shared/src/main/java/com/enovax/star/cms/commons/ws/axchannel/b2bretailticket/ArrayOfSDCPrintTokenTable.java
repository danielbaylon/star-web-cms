
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_PrintTokenTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_PrintTokenTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_PrintTokenTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_PrintTokenTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_PrintTokenTable", propOrder = {
    "sdcPrintTokenTable"
})
public class ArrayOfSDCPrintTokenTable {

    @XmlElement(name = "SDC_PrintTokenTable", nillable = true)
    protected List<SDCPrintTokenTable> sdcPrintTokenTable;

    /**
     * Gets the value of the sdcPrintTokenTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcPrintTokenTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCPrintTokenTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCPrintTokenTable }
     * 
     * 
     */
    public List<SDCPrintTokenTable> getSDCPrintTokenTable() {
        if (sdcPrintTokenTable == null) {
            sdcPrintTokenTable = new ArrayList<SDCPrintTokenTable>();
        }
        return this.sdcPrintTokenTable;
    }

}
