package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 8/16/16.
 */
public class AxStarSalesOrder {
    private String currencyCode;
    private Integer documentStatusValue;
    private Integer recordId;
    private Integer statusValue;
    private String orderPlacedDate;
    private BigDecimal customerAccountDepositAmount;
    //TODO AffiliationLoyaltyTierLines
    private boolean isRequiredAmountPaid;
    private boolean isDiscountFullyCalculated;
    private BigDecimal amountDue;
    private BigDecimal estimatedShippingAmount;
    private BigDecimal amountPaid;

    //TODO attributeValues

    private BigDecimal availableDepositAmount;
    private String beginDateTime;
    private String createdDateTime;
    private String businessDate;
    private BigDecimal calculatedDepositAmount;
    private BigDecimal cancellationCharge;
    private Long channelId;
    private String channelReferenceId;
    private BigDecimal chargeAmount;
    //TODO chargeLines
    private String comment;
    private String invoiceComment;

    //TODO ContactInformationCollection
    private String customerId;
    private Integer customerOrderModeValue;
    private Integer customerOrderTypeValue;
    private String deliveryMode;
    private BigDecimal deliveryModeChargeAmount;
    private BigDecimal discountAmount;
    private List<String> discountCodes;
    private Integer entryStatusValue;
    private BigDecimal grossAmount;
    private boolean hasLoyaltyPayment;
    private String id;
    //TODO IncomeExpenseLines
    private BigDecimal incomeExpenseTotalAmount;
    private String inventoryLocationId;
    private boolean isCreatedOffline;
    private boolean isReturnByReceipt;
    private boolean isSuspended;
    private boolean isTaxIncludedInPrice;
    private BigDecimal lineDiscount;
    private Integer lineDiscountCalculationTypeValue;
    private String loyaltyCardId;
    private BigDecimal loyaltyDiscountAmount;
    private BigDecimal loyaltyManualDiscountAmount;
    //TODO loyaltyRewardPointLInes
    private Date modifiedDateTime;
    private String name;
    private String storeName;
    private String storeAddress;
    private BigDecimal netAmount;
    private BigDecimal netAmountWithNoTax;
    private BigDecimal netAmountWithTax;
    private BigDecimal numberOfItems;
    private BigDecimal overriddenDepositAmount;
    private BigDecimal periodicDiscountAmount;
    private BigDecimal prepaymentAmountAppliedOnPickup;
    private BigDecimal prepaymentAmountInvoiced;
    private BigDecimal prepaymentAmountPaid;
    private String quotationExpiryDate;
    //TODO reasonCodeLines
    private String receiptEmail;
    private String receiptId;
    private String requestedDeliveryDate;
    private BigDecimal requiredDepositAmount;
    private boolean returnTransactionHasLoyaltyPayment;
    private String salesId;
    private BigDecimal salesPaymentDifference;

    private List<AxStarSalesLine> salesLines;

    private Integer shiftId;
    private String shiftTerminalId;
    private AxStarAddress shippingAddress;
    private String staffId;
    private String statementCode;
    private String storeId;
    private BigDecimal subtotalAmount;
    private BigDecimal subtotalAmountWithoutTax;
    private BigDecimal taxAmount;
    private BigDecimal taxAmountExclusive;
    private BigDecimal taxAmountInclusive;
    //TODO taxLines;
    private BigDecimal taxOnCancellationCharge;
    private String taxOverrideCode;
    //TODO tenderlines;
    private String terminalId;
    private BigDecimal totalAmount;
    private BigDecimal totalDiscount;
    private BigDecimal totalManualDiscountAmount;
    private BigDecimal totalManualDiscountPercentage;
    private Integer transactionTypeValue;
    private List<AxStarExtensionProperty> extensionProperties;


    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getDocumentStatusValue() {
        return documentStatusValue;
    }

    public void setDocumentStatusValue(Integer documentStatusValue) {
        this.documentStatusValue = documentStatusValue;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(Integer statusValue) {
        this.statusValue = statusValue;
    }

    public String getOrderPlacedDate() {
        return orderPlacedDate;
    }

    public void setOrderPlacedDate(String orderPlacedDate) {
        this.orderPlacedDate = orderPlacedDate;
    }

    public BigDecimal getCustomerAccountDepositAmount() {
        return customerAccountDepositAmount;
    }

    public void setCustomerAccountDepositAmount(BigDecimal customerAccountDepositAmount) {
        this.customerAccountDepositAmount = customerAccountDepositAmount;
    }

    public boolean isRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    public void setIsRequiredAmountPaid(boolean isRequiredAmountPaid) {
        this.isRequiredAmountPaid = isRequiredAmountPaid;
    }

    public boolean isDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    public void setIsDiscountFullyCalculated(boolean isDiscountFullyCalculated) {
        this.isDiscountFullyCalculated = isDiscountFullyCalculated;
    }

    public BigDecimal getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(BigDecimal amountDue) {
        this.amountDue = amountDue;
    }

    public BigDecimal getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getAvailableDepositAmount() {
        return availableDepositAmount;
    }

    public void setAvailableDepositAmount(BigDecimal availableDepositAmount) {
        this.availableDepositAmount = availableDepositAmount;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public BigDecimal getCalculatedDepositAmount() {
        return calculatedDepositAmount;
    }

    public void setCalculatedDepositAmount(BigDecimal calculatedDepositAmount) {
        this.calculatedDepositAmount = calculatedDepositAmount;
    }

    public BigDecimal getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(BigDecimal cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getChannelReferenceId() {
        return channelReferenceId;
    }

    public void setChannelReferenceId(String channelReferenceId) {
        this.channelReferenceId = channelReferenceId;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInvoiceComment() {
        return invoiceComment;
    }

    public void setInvoiceComment(String invoiceComment) {
        this.invoiceComment = invoiceComment;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getCustomerOrderModeValue() {
        return customerOrderModeValue;
    }

    public void setCustomerOrderModeValue(Integer customerOrderModeValue) {
        this.customerOrderModeValue = customerOrderModeValue;
    }

    public Integer getCustomerOrderTypeValue() {
        return customerOrderTypeValue;
    }

    public void setCustomerOrderTypeValue(Integer customerOrderTypeValue) {
        this.customerOrderTypeValue = customerOrderTypeValue;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public BigDecimal getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(BigDecimal deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public List<String> getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(List<String> discountCodes) {
        this.discountCodes = discountCodes;
    }

    public Integer getEntryStatusValue() {
        return entryStatusValue;
    }

    public void setEntryStatusValue(Integer entryStatusValue) {
        this.entryStatusValue = entryStatusValue;
    }

    public BigDecimal getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(BigDecimal grossAmount) {
        this.grossAmount = grossAmount;
    }

    public boolean isHasLoyaltyPayment() {
        return hasLoyaltyPayment;
    }

    public void setHasLoyaltyPayment(boolean hasLoyaltyPayment) {
        this.hasLoyaltyPayment = hasLoyaltyPayment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getIncomeExpenseTotalAmount() {
        return incomeExpenseTotalAmount;
    }

    public void setIncomeExpenseTotalAmount(BigDecimal incomeExpenseTotalAmount) {
        this.incomeExpenseTotalAmount = incomeExpenseTotalAmount;
    }

    public String getInventoryLocationId() {
        return inventoryLocationId;
    }

    public void setInventoryLocationId(String inventoryLocationId) {
        this.inventoryLocationId = inventoryLocationId;
    }

    public boolean isCreatedOffline() {
        return isCreatedOffline;
    }

    public void setIsCreatedOffline(boolean isCreatedOffline) {
        this.isCreatedOffline = isCreatedOffline;
    }

    public boolean isReturnByReceipt() {
        return isReturnByReceipt;
    }

    public void setIsReturnByReceipt(boolean isReturnByReceipt) {
        this.isReturnByReceipt = isReturnByReceipt;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

    public void setIsSuspended(boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public boolean isTaxIncludedInPrice() {
        return isTaxIncludedInPrice;
    }

    public void setIsTaxIncludedInPrice(boolean isTaxIncludedInPrice) {
        this.isTaxIncludedInPrice = isTaxIncludedInPrice;
    }

    public BigDecimal getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(BigDecimal lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public Integer getLineDiscountCalculationTypeValue() {
        return lineDiscountCalculationTypeValue;
    }

    public void setLineDiscountCalculationTypeValue(Integer lineDiscountCalculationTypeValue) {
        this.lineDiscountCalculationTypeValue = lineDiscountCalculationTypeValue;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public BigDecimal getLoyaltyDiscountAmount() {
        return loyaltyDiscountAmount;
    }

    public void setLoyaltyDiscountAmount(BigDecimal loyaltyDiscountAmount) {
        this.loyaltyDiscountAmount = loyaltyDiscountAmount;
    }

    public BigDecimal getLoyaltyManualDiscountAmount() {
        return loyaltyManualDiscountAmount;
    }

    public void setLoyaltyManualDiscountAmount(BigDecimal loyaltyManualDiscountAmount) {
        this.loyaltyManualDiscountAmount = loyaltyManualDiscountAmount;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public BigDecimal getNetAmountWithNoTax() {
        return netAmountWithNoTax;
    }

    public void setNetAmountWithNoTax(BigDecimal netAmountWithNoTax) {
        this.netAmountWithNoTax = netAmountWithNoTax;
    }

    public BigDecimal getNetAmountWithTax() {
        return netAmountWithTax;
    }

    public void setNetAmountWithTax(BigDecimal netAmountWithTax) {
        this.netAmountWithTax = netAmountWithTax;
    }

    public BigDecimal getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(BigDecimal numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public BigDecimal getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    public void setOverriddenDepositAmount(BigDecimal overriddenDepositAmount) {
        this.overriddenDepositAmount = overriddenDepositAmount;
    }

    public BigDecimal getPeriodicDiscountAmount() {
        return periodicDiscountAmount;
    }

    public void setPeriodicDiscountAmount(BigDecimal periodicDiscountAmount) {
        this.periodicDiscountAmount = periodicDiscountAmount;
    }

    public BigDecimal getPrepaymentAmountAppliedOnPickup() {
        return prepaymentAmountAppliedOnPickup;
    }

    public void setPrepaymentAmountAppliedOnPickup(BigDecimal prepaymentAmountAppliedOnPickup) {
        this.prepaymentAmountAppliedOnPickup = prepaymentAmountAppliedOnPickup;
    }

    public BigDecimal getPrepaymentAmountInvoiced() {
        return prepaymentAmountInvoiced;
    }

    public void setPrepaymentAmountInvoiced(BigDecimal prepaymentAmountInvoiced) {
        this.prepaymentAmountInvoiced = prepaymentAmountInvoiced;
    }

    public BigDecimal getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    public void setPrepaymentAmountPaid(BigDecimal prepaymentAmountPaid) {
        this.prepaymentAmountPaid = prepaymentAmountPaid;
    }

    public String getQuotationExpiryDate() {
        return quotationExpiryDate;
    }

    public void setQuotationExpiryDate(String quotationExpiryDate) {
        this.quotationExpiryDate = quotationExpiryDate;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public BigDecimal getRequiredDepositAmount() {
        return requiredDepositAmount;
    }

    public void setRequiredDepositAmount(BigDecimal requiredDepositAmount) {
        this.requiredDepositAmount = requiredDepositAmount;
    }

    public boolean isReturnTransactionHasLoyaltyPayment() {
        return returnTransactionHasLoyaltyPayment;
    }

    public void setReturnTransactionHasLoyaltyPayment(boolean returnTransactionHasLoyaltyPayment) {
        this.returnTransactionHasLoyaltyPayment = returnTransactionHasLoyaltyPayment;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public BigDecimal getSalesPaymentDifference() {
        return salesPaymentDifference;
    }

    public void setSalesPaymentDifference(BigDecimal salesPaymentDifference) {
        this.salesPaymentDifference = salesPaymentDifference;
    }

    public Integer getShiftId() {
        return shiftId;
    }

    public void setShiftId(Integer shiftId) {
        this.shiftId = shiftId;
    }

    public String getShiftTerminalId() {
        return shiftTerminalId;
    }

    public void setShiftTerminalId(String shiftTerminalId) {
        this.shiftTerminalId = shiftTerminalId;
    }

    public AxStarAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AxStarAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStatementCode() {
        return statementCode;
    }

    public void setStatementCode(String statementCode) {
        this.statementCode = statementCode;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public BigDecimal getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(BigDecimal subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public BigDecimal getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    public void setSubtotalAmountWithoutTax(BigDecimal subtotalAmountWithoutTax) {
        this.subtotalAmountWithoutTax = subtotalAmountWithoutTax;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTaxAmountExclusive() {
        return taxAmountExclusive;
    }

    public void setTaxAmountExclusive(BigDecimal taxAmountExclusive) {
        this.taxAmountExclusive = taxAmountExclusive;
    }

    public BigDecimal getTaxAmountInclusive() {
        return taxAmountInclusive;
    }

    public void setTaxAmountInclusive(BigDecimal taxAmountInclusive) {
        this.taxAmountInclusive = taxAmountInclusive;
    }

    public BigDecimal getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    public void setTaxOnCancellationCharge(BigDecimal taxOnCancellationCharge) {
        this.taxOnCancellationCharge = taxOnCancellationCharge;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    public void setTotalManualDiscountAmount(BigDecimal totalManualDiscountAmount) {
        this.totalManualDiscountAmount = totalManualDiscountAmount;
    }

    public BigDecimal getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    public void setTotalManualDiscountPercentage(BigDecimal totalManualDiscountPercentage) {
        this.totalManualDiscountPercentage = totalManualDiscountPercentage;
    }

    public Integer getTransactionTypeValue() {
        return transactionTypeValue;
    }

    public void setTransactionTypeValue(Integer transactionTypeValue) {
        this.transactionTypeValue = transactionTypeValue;
    }

    public List<AxStarSalesLine> getSalesLines() {
        return salesLines;
    }

    public void setSalesLines(List<AxStarSalesLine> salesLines) {
        this.salesLines = salesLines;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    //    {
//        "CurrencyCode": "",
//            "DocumentStatusValue": 0,
//            "RecordId": 0,
//            "StatusValue": 0,
//            "OrderPlacedDate": "2016-08-17T01:28:17.227+00:00",
//            "CustomerAccountDepositAmount": 0,
//            "AffiliationLoyaltyTierLines": [],
//        "IsRequiredAmountPaid": false,
//            "IsDiscountFullyCalculated": false,
//            "AmountDue": 0,
//            "EstimatedShippingAmount": null,
//            "AmountPaid": 0,
//            "AttributeValues": [],
//        "AvailableDepositAmount": 0,
//            "BeginDateTime": "2016-08-17T09:28:17+08:00",
//            "CreatedDateTime": "1753-01-01T00:00:00+00:00",
//            "BusinessDate": "2016-08-17T00:00:00+08:00",
//            "CalculatedDepositAmount": 0,
//            "CancellationCharge": null,
//            "ChannelId": 5637149827,
//            "ChannelReferenceId": "ONE06C7BA4BDA243C4A20996151FEDEE3D",
//            "ChargeAmount": 0,
//            "ChargeLines": [],
//        "Comment": "",
//            "InvoiceComment": "",
//            "ContactInformationCollection": [
//        {
//            "Value": "SLM_B2C@mail.com",
//                "ContactInformationTypeValue": 2,
//                "ExtensionProperties": []
//        },
//        {
//            "Value": "",
//                "ContactInformationTypeValue": 1,
//                "ExtensionProperties": []
//        }
//        ],
//        "CustomerId": "SLMB2CCust",
//            "CustomerOrderModeValue": 0,
//            "CustomerOrderTypeValue": 0,
//            "DeliveryMode": "CPickUp",
//            "DeliveryModeChargeAmount": null,
//            "DiscountAmount": 0,
//            "DiscountCodes": [],
//        "EntryStatusValue": 0,
//            "GrossAmount": 224.5,
//            "HasLoyaltyPayment": false,
//            "Id": "CO2E060586190542B7837F17CFD3E2A862",
//            "IncomeExpenseLines": [],
//        "IncomeExpenseTotalAmount": 0,
//            "InventoryLocationId": "SLMB2C",
//            "IsCreatedOffline": false,
//            "IsReturnByReceipt": false,
//            "IsSuspended": false,
//            "IsTaxIncludedInPrice": false,
//            "LineDiscount": 0,
//            "LineDiscountCalculationTypeValue": 0,
//            "LoyaltyCardId": "",
//            "LoyaltyDiscountAmount": 0,
//            "LoyaltyManualDiscountAmount": null,
//            "LoyaltyRewardPointLines": [],
//        "ModifiedDateTime": "1753-01-01T00:00:00+00:00",
//            "Name": "SLM_B2C",
//            "StoreName": null,
//            "StoreAddress": null,
//            "NetAmount": 0,
//            "NetAmountWithNoTax": 224.5,
//            "NetAmountWithTax": 0,
//            "NumberOfItems": 0,
//            "OverriddenDepositAmount": null,
//            "PeriodicDiscountAmount": 0,
//            "PrepaymentAmountAppliedOnPickup": 0,
//            "PrepaymentAmountInvoiced": 0,
//            "PrepaymentAmountPaid": 0,
//            "QuotationExpiryDate": null,
//            "ReasonCodeLines": [],
//        "ReceiptEmail": "SLM_B2C@mail.com",
//            "ReceiptId": "",
//            "RequestedDeliveryDate": "2016-08-17T00:00:00+08:00",
//            "RequiredDepositAmount": 0,
//            "ReturnTransactionHasLoyaltyPayment": false,
//            "SalesId": "",
//            "SalesPaymentDifference": 224.5,
//            "SalesLines": [
//        {
//            "LineId": null,
//                "Description": null,
//                "OriginLineId": null,
//                "TaxOverrideCode": null,
//                "ProductId": 5637157341,
//                "Barcode": "",
//                "MasterProductId": 0,
//                "ListingId": 5637157341,
//                "IsPriceOverridden": false,
//                "OriginalPrice": 0,
//                "TotalAmount": 224.5,
//                "NetAmountWithoutTax": 0,
//                "DiscountAmount": 0,
//                "TotalDiscount": 0,
//                "TotalPercentageDiscount": 0,
//                "LineDiscount": 0,
//                "PeriodicDiscount": 0,
//                "LineManualDiscountPercentage": 0,
//                "LineManualDiscountAmount": 0,
//                "ShippingAddress": {
//            "Name": "SLM_B2C",
//                    "FullAddress": "",
//                    "RecordId": 0,
//                    "Street": "",
//                    "StreetNumber": "",
//                    "County": "",
//                    "CountyName": "",
//                    "City": "Singapore",
//                    "DistrictName": "",
//                    "State": "",
//                    "StateName": "",
//                    "ZipCode": "",
//                    "ThreeLetterISORegionName": "SGP",
//                    "Phone": "",
//                    "PhoneRecordId": 0,
//                    "PhoneExt": "",
//                    "Email": "",
//                    "EmailContent": "",
//                    "EmailRecordId": 0,
//                    "Url": "",
//                    "UrlRecordId": 0,
//                    "TwoLetterISORegionName": "",
//                    "Deactivate": false,
//                    "AttentionTo": "",
//                    "BuildingCompliment": "",
//                    "Postbox": "",
//                    "TaxGroup": "",
//                    "AddressTypeValue": 0,
//                    "IsPrimary": false,
//                    "IsPrivate": false,
//                    "PartyNumber": null,
//                    "DirectoryPartyTableRecordId": 0,
//                    "DirectoryPartyLocationRecordId": 0,
//                    "DirectoryPartyLocationRoleRecordId": 0,
//                    "LogisticsLocationId": "",
//                    "LogisticsLocationRecordId": 0,
//                    "LogisticsLocationExtRecordId": 0,
//                    "LogisticsLocationRoleRecordId": 0,
//                    "PhoneLogisticsLocationRecordId": 0,
//                    "PhoneLogisticsLocationId": "",
//                    "EmailLogisticsLocationRecordId": 0,
//                    "EmailLogisticsLocationId": "",
//                    "UrlLogisticsLocationRecordId": 0,
//                    "UrlLogisticsLocationId": "",
//                    "ExpireRecordId": 0,
//                    "SortOrder": 0,
//                    "ExtensionProperties": [
//            {
//                "Key": "SALESNAME",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "TRANSACTIONID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "CO2E060586190542B7837F17CFD3E2A862"
//            }
//            },
//            {
//                "Key": "SALELINENUM",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": 1,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            }
//            ]
//        },
//            "DeliveryMode": "CPickUp",
//                "Comment": "",
//                "RequestedDeliveryDate": "2016-08-17T00:00:00+08:00",
//                "InventoryLocationId": "SLMB2C",
//                "InventoryDimensionId": null,
//                "ItemType": 0,
//                "ReservationId": "00000000-0000-0000-0000-000000000000",
//                "LineNumber": 1,
//                "ReturnQuantity": 0,
//                "StatusValue": 0,
//                "SalesStatusValue": 0,
//                "ProductSourceValue": 0,
//                "IsGiftCardLine": false,
//                "GiftCardId": "",
//                "GiftCardCurrencyCode": "",
//                "GiftCardOperationValue": 0,
//                "IsInvoiceLine": false,
//                "InvoiceId": "",
//                "InvoiceAmount": 0,
//                "IsInvoiceSettled": false,
//                "IsVoided": false,
//                "IsPriceLocked": false,
//                "ChargeLines": [],
//            "BasePrice": 0,
//                "AgreementPrice": 0,
//                "AdjustedPrice": 0,
//                "ReturnTransactionId": "",
//                "ReturnLineNumber": 0,
//                "ReturnInventTransId": "",
//                "ReturnStore": "",
//                "ReturnTerminalId": "",
//                "ReturnChannelId": 5637149827,
//                "Store": "",
//                "TerminalId": "",
//                "SalesDate": null,
//                "QuantityInvoiced": null,
//                "QuantityOrdered": null,
//                "RecordId": 0,
//                "SerialNumber": "",
//                "BatchId": "",
//                "DeliveryModeChargeAmount": null,
//                "UnitOfMeasureSymbol": null,
//                "CatalogId": 0,
//                "ElectronicDeliveryEmailAddress": "",
//                "ElectronicDeliveryEmailContent": "",
//                "LoyaltyDiscountAmount": 0,
//                "LoyaltyPercentageDiscount": 0,
//                "IsCustomerAccountDeposit": false,
//                "Blocked": false,
//                "Found": false,
//                "DateToActivateItem": "0001-01-01T00:00:00+00:00",
//                "LinePercentageDiscount": 0,
//                "PeriodicPercentageDiscount": 0,
//                "QuantityDiscounted": 0,
//                "UnitQuantity": 0,
//                "UnitOfMeasureConversion": {
//            "ItemId": null,
//                    "FromUnitOfMeasureId": null,
//                    "ToUnitOfMeasureId": null,
//                    "FromUnitOfMeasureSymbol": null,
//                    "ToUnitOfMeasureSymbol": null,
//                    "IsBackward": false,
//                    "RecordId": 0,
//                    "ProductRecordId": 0,
//                    "Factor": 1,
//                    "Numerator": 1,
//                    "Denominator": 1,
//                    "InnerOffset": 0,
//                    "OuterOffset": 0,
//                    "ExtensionProperties": []
//        },
//            "DiscountLines": [],
//            "PeriodicDiscountPossibilities": [],
//            "ReasonCodeLines": [],
//            "ReturnLabelProperties": null,
//                "LineMultilineDiscOnItem": 0,
//                "LineIdsLinkedProductMap": {},
//            "LinkedParentLineId": null,
//                "LineMultilineDiscOnItemValue": 0,
//                "WasChanged": false,
//                "OriginalSalesOrderUnitOfMeasure": null,
//                "InventOrderUnitOfMeasure": null,
//                "IsLoyaltyDiscountApplied": false,
//                "ItemId": "108100000001",
//                "Quantity": 5,
//                "Price": 44.9,
//                "ItemTaxGroupId": "GST",
//                "SalesTaxGroupId": "",
//                "TaxAmount": 0,
//                "SalesOrderUnitOfMeasure": null,
//                "NetAmount": 224.5,
//                "NetAmountPerUnit": 0,
//                "GrossAmount": 224.5,
//                "TaxLines": [],
//            "TaxAmountExemptInclusive": 0,
//                "TaxAmountInclusive": 0,
//                "TaxAmountExclusive": 0,
//                "NetAmountWithAllInclusiveTax": 224.5,
//                "BeginDateTime": "0001-01-01T00:00:00+00:00",
//                "EndDateTime": "0001-01-01T00:00:00+00:00",
//                "TaxRatePercent": 0,
//                "IsReturnByReceipt": false,
//                "ExtensionProperties": [
//            {
//                "Key": "TRANSACTIONID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "CO2E060586190542B7837F17CFD3E2A862"
//            }
//            },
//            {
//                "Key": "UNIT",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "Ea"
//            }
//            },
//            {
//                "Key": "INVENTORYSITEID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "LOGISTICSPOSTALADDRESS",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": 0,
//                        "StringValue": null
//            }
//            }
//            ]
//        }
//        ],
//        "ShiftId": 0,
//            "ShiftTerminalId": "",
//            "ShippingAddress": {
//        "Name": "SLM_B2C",
//                "FullAddress": "",
//                "RecordId": 0,
//                "Street": "",
//                "StreetNumber": "",
//                "County": "",
//                "CountyName": "",
//                "City": "Singapore",
//                "DistrictName": "",
//                "State": "",
//                "StateName": "",
//                "ZipCode": "",
//                "ThreeLetterISORegionName": "SGP",
//                "Phone": "",
//                "PhoneRecordId": 0,
//                "PhoneExt": "",
//                "Email": "",
//                "EmailContent": "",
//                "EmailRecordId": 0,
//                "Url": "",
//                "UrlRecordId": 0,
//                "TwoLetterISORegionName": "",
//                "Deactivate": false,
//                "AttentionTo": "",
//                "BuildingCompliment": "",
//                "Postbox": "",
//                "TaxGroup": "",
//                "AddressTypeValue": 0,
//                "IsPrimary": false,
//                "IsPrivate": false,
//                "PartyNumber": null,
//                "DirectoryPartyTableRecordId": 0,
//                "DirectoryPartyLocationRecordId": 0,
//                "DirectoryPartyLocationRoleRecordId": 0,
//                "LogisticsLocationId": "",
//                "LogisticsLocationRecordId": 0,
//                "LogisticsLocationExtRecordId": 0,
//                "LogisticsLocationRoleRecordId": 0,
//                "PhoneLogisticsLocationRecordId": 0,
//                "PhoneLogisticsLocationId": "",
//                "EmailLogisticsLocationRecordId": 0,
//                "EmailLogisticsLocationId": "",
//                "UrlLogisticsLocationRecordId": 0,
//                "UrlLogisticsLocationId": "",
//                "ExpireRecordId": 0,
//                "SortOrder": 0,
//                "ExtensionProperties": [
//        {
//            "Key": "SALESNAME",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": ""
//        }
//        },
//        {
//            "Key": "TRANSACTIONID",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": "CO2E060586190542B7837F17CFD3E2A862"
//        }
//        },
//        {
//            "Key": "SALELINENUM",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": 0,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": null
//        }
//        }
//        ]
//    },
//        "StaffId": "",
//            "StatementCode": "",
//            "StoreId": "",
//            "SubtotalAmount": 224.5,
//            "SubtotalAmountWithoutTax": 0,
//            "TaxAmount": 0,
//            "TaxAmountExclusive": 0,
//            "TaxAmountInclusive": 0,
//            "TaxLines": [],
//        "TaxOnCancellationCharge": 0,
//            "TaxOverrideCode": null,
//            "TenderLines": [
//        {
//            "Authorization": "",
//                "TransactionStatusValue": 0,
//                "IncomeExpenseAccountTypeValue": -1,
//                "MaskedCardNumber": "SLMB2CCust",
//                "TenderDate": "2016-08-17T09:28:17+08:00",
//                "IsPreProcessed": false,
//                "TenderLineId": null,
//                "Amount": 224.5,
//                "CashBackAmount": 0,
//                "AmountInTenderedCurrency": 224.5,
//                "AmountInCompanyCurrency": 224.5,
//                "Currency": "SGD",
//                "ExchangeRate": 1,
//                "CompanyCurrencyExchangeRate": 1,
//                "TenderTypeId": "6",
//                "SignatureData": null,
//                "ReasonCodeLines": [],
//            "LineNumber": 1,
//                "GiftCardId": "",
//                "CreditMemoId": "",
//                "CustomerId": "SLMB2CCust",
//                "LoyaltyCardId": "",
//                "CardTypeId": "",
//                "IsChangeLine": false,
//                "IsHistorical": false,
//                "IsVoidable": false,
//                "StatusValue": 4,
//                "ExtensionProperties": [
//            {
//                "Key": "AUTHENTICATIONCODE",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "CHANNELID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": 5637149827,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "COUNTER",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": 0,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "ISPREPAYMENT",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": 0,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "MANAGERKEYLIVE",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": 0,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "MESSAGENUM",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": 0,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "QTY",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": 1,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": null
//            }
//            },
//            {
//                "Key": "RECEIPTID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "STAFF",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "STATEMENTCODE",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "STORE",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "TERMINAL",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": ""
//            }
//            },
//            {
//                "Key": "TRANSACTIONID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "CO2E060586190542B7837F17CFD3E2A862"
//            }
//            },
//            {
//                "Key": "DATAAREAID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "slm"
//            }
//            }
//            ]
//        }
//        ],
//        "TerminalId": "",
//            "TotalAmount": 224.5,
//            "TotalDiscount": 0,
//            "TotalManualDiscountAmount": 0,
//            "TotalManualDiscountPercentage": 0,
//            "TransactionTypeValue": 27,
//            "ExtensionProperties": [
//        {
//            "Key": "INVENTSITEID",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": ""
//        }
//        },
//        {
//            "Key": "LOGISTICSPOSTALADDRESS",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": 0,
//                    "StringValue": null
//        }
//        },
//        {
//            "Key": "EMAIL",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": "SLM_B2C@mail.com"
//        }
//        },
//        {
//            "Key": "PHONE",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": ""
//        }
//        },
//        {
//            "Key": "CURRENCY",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": "SGD"
//        }
//        }
//        ]
//    }




}
