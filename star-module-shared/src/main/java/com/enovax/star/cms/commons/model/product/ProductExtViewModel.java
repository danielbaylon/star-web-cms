package com.enovax.star.cms.commons.model.product;

import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtEventAllocation;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtEventGroup;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtEventGroupLine;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductExtViewModel {

    private String absoluteCapacityId;
    private boolean isPackage;
    private boolean isCapacity;
    private boolean isTicketing;
    private boolean isTransport;
    private boolean defineOpenDate;
    private boolean isOpenDate;
    private String itemId;
    private String productId;
    private String mediaTypeId;
    private Date openValidityStartDate;
    private Date openValidityEndDate;
    private int printing;
    private int printerType;
    private int noOfPax;
    private String templateName;

    private boolean isEvent = false;
    private boolean isSingleEvent = false;
    private String eventGroupId = "";
    private String defaultEventLineId = "";
    private List<ProductExtViewEventLine> eventLines;

    public static ProductExtViewModel fromExtData(AxRetailProductExtData data) {
        final ProductExtViewModel model = new ProductExtViewModel();
        model.setAbsoluteCapacityId(data.getAbsoluteCapacityId());
        model.setPackage(data.isPackage());
        model.setCapacity(data.isCapacity());
        model.setTicketing(data.isTicketing());
        model.setTransport(data.isTransport());
        model.setItemId(data.getItemId());
        model.setProductId(data.getProductId());
        model.setMediaTypeId(data.getMediaTypeId());
        model.setOpenValidityEndDate(data.getOpenEndDateTime());
        model.setOpenValidityStartDate(data.getOpenStartDateTime());
        model.setPrinting(data.getPrinting());
        model.setPrinterType(data.getPrinterType());
        model.setNoOfPax(data.getNoOfPax());
        model.setTemplateName(data.getTemplateName());
        model.setDefineOpenDate(data.isDefineOpenDate());
        model.setOpenDate(!data.isDefineOpenDate()); //Have to do this because API field is misnamed.

        final String eventGroupId = data.getEventGroupId();
        
        if(data.isPackage()) {
            model.setEvent(StringUtils.isNotEmpty(eventGroupId));
        }else {
            model.setEvent(data.isCapacity()); //the isCapacity is actualy is Event Capacity in AX
        }

        if (model.isEvent) {
            List<AxRetailProductExtEventGroup> eventGroups = data.getEventGroups();
            if (eventGroups != null && !eventGroups.isEmpty()) {
                final AxRetailProductExtEventGroup eventGroup = eventGroups.get(0);
                model.setEventGroupId(eventGroupId);
                model.setSingleEvent(eventGroup.isSingleEvent());
                model.setDefaultEventLineId(data.getDefaultEventLineId());

                final List<ProductExtViewEventLine> eventLines = new ArrayList<>();
                for (AxRetailProductExtEventGroupLine groupLine : eventGroup.getEventGroupLines()) {
                    final ProductExtViewEventLine line = new ProductExtViewEventLine();
                    line.setEventLineId(groupLine.getEventLineId());
                    line.setEventName(groupLine.getEventName());
                    line.setEventTime(groupLine.getEventTime());
                    line.setEventStartTime(groupLine.getEventStartTime());
                    line.setEventEndTime(groupLine.getEventEndTime());
                    line.setEventCapacityId(groupLine.getEventCapacityId());

                    final List<ProductExtViewEventAllocation> eventDates = new ArrayList<>();
                    for (AxRetailProductExtEventAllocation alloc : groupLine.getEventAllocations()) {
                        final ProductExtViewEventAllocation eventDate = new ProductExtViewEventAllocation();
                        eventDate.setEventLineId(alloc.getEventLineId());
                        eventDate.setEventGroupId(alloc.getEventGroupId());
                        eventDate.setRawDate(alloc.getEventDate());
                        eventDate.setDate(NvxDateUtils.formatDate(eventDate.getRawDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));

                        eventDates.add(eventDate);
                    }
                    line.setEventDates(eventDates);

                    eventLines.add(line);
                }
                model.setEventLines(eventLines);
            }
        }

        return model;
    }

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public void setPackage(boolean aPackage) {
        isPackage = aPackage;
    }

    public boolean isCapacity() {
        return isCapacity;
    }

    public void setCapacity(boolean capacity) {
        isCapacity = capacity;
    }

    public boolean isTicketing() {
        return isTicketing;
    }

    public void setTicketing(boolean ticketing) {
        isTicketing = ticketing;
    }

    public boolean isTransport() {
        return isTransport;
    }

    public void setTransport(boolean transport) {
        isTransport = transport;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public Date getOpenValidityStartDate() {
        return openValidityStartDate;
    }

    public void setOpenValidityStartDate(Date openValidityStartDate) {
        this.openValidityStartDate = openValidityStartDate;
    }

    public Date getOpenValidityEndDate() {
        return openValidityEndDate;
    }

    public void setOpenValidityEndDate(Date openValidityEndDate) {
        this.openValidityEndDate = openValidityEndDate;
    }

    public int getPrinting() {
        return printing;
    }

    public void setPrinting(int printing) {
        this.printing = printing;
    }

    public int getPrinterType() {
        return printerType;
    }

    public void setPrinterType(int printerType) {
        this.printerType = printerType;
    }

    public int getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(int noOfPax) {
        this.noOfPax = noOfPax;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public boolean isEvent() {
        return isEvent;
    }

    public void setEvent(boolean event) {
        isEvent = event;
    }

    public boolean isSingleEvent() {
        return isSingleEvent;
    }

    public void setSingleEvent(boolean singleEvent) {
        isSingleEvent = singleEvent;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getDefaultEventLineId() {
        return defaultEventLineId;
    }

    public void setDefaultEventLineId(String defaultEventLineId) {
        this.defaultEventLineId = defaultEventLineId;
    }

    public List<ProductExtViewEventLine> getEventLines() {
        return eventLines;
    }

    public void setEventLines(List<ProductExtViewEventLine> eventLines) {
        this.eventLines = eventLines;
    }

    public boolean isDefineOpenDate() {
        return defineOpenDate;
    }

    public void setDefineOpenDate(boolean defineOpenDate) {
        this.defineOpenDate = defineOpenDate;
    }

    public boolean isOpenDate() {
        return isOpenDate;
    }

    public void setOpenDate(boolean openDate) {
        isOpenDate = openDate;
    }
}
