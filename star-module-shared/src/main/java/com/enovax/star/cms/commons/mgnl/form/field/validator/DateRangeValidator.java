package com.enovax.star.cms.commons.mgnl.form.field.validator;

import com.vaadin.data.Item;
import com.vaadin.data.validator.AbstractValidator;

import java.util.Date;

/**
 * Created by jennylynsze on 10/14/16.
 */
public class DateRangeValidator extends AbstractValidator<Date> {
    private Item item;
    private String startDateField;
    private String endDateField;

    /**
     * Constructs a validator with the given error message.
     *
     * @param errorMessage the message to be included in an {@link InvalidValueException}
     *                     (with "{0}" replaced by the value that failed validation).
     */
    public DateRangeValidator(Item item, String startDateField, String endDateField, String errorMessage) {
        super(errorMessage);
        this.item = item;
        this.startDateField = startDateField;
        this.endDateField = endDateField;
    }

    @Override
    protected boolean isValidValue(Date value) {

        Date startDate = (Date) this.item.getItemProperty(startDateField).getValue();
        Date endDate = (Date) this.item.getItemProperty(endDateField).getValue();

        if(startDate.compareTo(endDate) > 0) {
            return false;
        }

        return true;
    }

    @Override
    public Class<Date> getType() {
        return Date.class;
    }
}
