package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackageItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Repository
public interface PPMFLGMixMatchPackageItemRepository extends JpaRepository<PPMFLGMixMatchPackageItem, Integer> {
    List<PPMFLGMixMatchPackageItem> findByTransactionItemId(Integer transItemId);
    List<PPMFLGMixMatchPackageItem> findByTransactionItemIdIn(List<Integer> transItemIds);
    List<PPMFLGMixMatchPackageItem> findByPackageId(Integer pkgId);

    @Query("SELECT i.transactionItemId " +
            " from PPMFLGMixMatchPackageItem i " +
            " where i.packageId = :packageId " +
            " and i.transItemType = 'Standard' ")
    List<Integer> getPkgTransactionItemsIdByPkgId(@Param("packageId") Integer packageId);

    @Query(value = "select count(1) from PPMFLGMixMatchPackageItem item, PPMFLGMixMatchPackage package " +
            "where item.packageId = package.id " +
            "and package.status = 'Reserved' " +
            "and item.receiptNum = :receiptNum ", nativeQuery = true)
    Long noOfItemWithSameTransactionReserved(@Param("receiptNum") String receiptNum);

    @Query("select i from PPMFLGMixMatchPackageItem i where i.packageId = :packageId " +
            "and i.itemProductCode= :itemCode")
    List<PPMFLGMixMatchPackageItem> getPkgItemsByItemCode(@Param("packageId") Integer packageId, @Param("itemCode") String itemCode);

}
