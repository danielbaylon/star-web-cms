package com.enovax.star.cms.commons.mgnl.form.field.validator.definition;

import com.enovax.star.cms.commons.mgnl.form.field.validator.factory.DateRangeValidatorFactory;
import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 10/14/16.
 */
public class DateRangeValidatorDefinition extends ConfiguredFieldValidatorDefinition {

    private String startDateField;
    private String endDateField;
    private String errorMessage;

    public DateRangeValidatorDefinition() {
        this.setFactoryClass(DateRangeValidatorFactory.class);
    }

    public String getStartDateField() {
        return startDateField;
    }

    public void setStartDateField(String startDateField) {
        this.startDateField = startDateField;
    }

    public String getEndDateField() {
        return endDateField;
    }

    public void setEndDateField(String endDateField) {
        this.endDateField = endDateField;
    }
}