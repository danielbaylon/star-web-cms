package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketSearchCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskCartTicketSearchCriteriaRequest {

	@JsonProperty("CartTicketSearchCriteria")
	private AxCartTicketSearchCriteria axCartTicketSearchCriteria;

	public void setAxCartTicketSearchCriteria(AxCartTicketSearchCriteria axCartTicketSearchCriteria) {
		this.axCartTicketSearchCriteria = axCartTicketSearchCriteria;
	}

	public AxCartTicketSearchCriteria getAxCartTicketSearchCriteria() {
		return axCartTicketSearchCriteria;
	}

}
