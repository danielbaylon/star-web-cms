package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.util.Date;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class PkgTransItemVM implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private Integer itemId;
    private String prodId;
    private String receiptNum;
    private Integer unpackagedQty;
    private Integer packagedQty;
    private String validityEndDateStr;

    public PkgTransItemVM() {

    }

    public PkgTransItemVM(PPSLMInventoryTransactionItem item) {
        this.itemId = item.getId();
        this.prodId = item.getProductId();
        this.receiptNum = item.getInventoryTrans().getReceiptNum();
        this.unpackagedQty = item.getUnpackagedQty();
        this.packagedQty = item.getPkgQty();
        Date validateEndDate = item.getValidityEndDate();
        this.validityEndDateStr = NvxDateUtils.formatDateForDisplay(validateEndDate,
                false);
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getPackagedQty() {
        return packagedQty;
    }

    public void setPackagedQty(Integer packagedQty) {
        this.packagedQty = packagedQty;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof PkgTransItemVM))
            return false;
        PkgTransItemVM otherMyClass = (PkgTransItemVM) other;
        if (this.itemId.equals(otherMyClass.getItemId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) serialVersionUID + itemId;
        return result;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }
}

