package com.enovax.star.cms.commons.model.product.promotions;

import java.math.BigDecimal;

/**
 * Created by jennylynsze on 9/12/16.
 */
public class PromotionRelatedProduct {
    private String itemId;
    private BigDecimal estimatedDiscountedUnitPrice;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getEstimatedDiscountedUnitPrice() {
        return estimatedDiscountedUnitPrice;
    }

    public void setEstimatedDiscountedUnitPrice(BigDecimal estimatedDiscountedUnitPrice) {
        this.estimatedDiscountedUnitPrice = estimatedDiscountedUnitPrice;
    }
}
