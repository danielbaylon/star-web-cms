package com.enovax.star.cms.commons.model.axchannel.retailticket.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.*;

import javax.xml.bind.JAXBElement;
import java.util.List;

/**
 * Created by jennylynsze on 1/10/17.
 */
public class AxInsertOnlineReferenceResponseTransformer {

    public static ApiResult<String> fromWsInsertOnlineResponse(SDCInsertOnlineReferenceResponse response) {

        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }

        final ArrayOfResponseError arrayOfResponseError = response.getErrors().getValue();

        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), null);
        }

        ApiResult<String> result = new ApiResult<String>();
        result.setSuccess(false);
        JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> xmlInsertOnlineResp = response.getInsertOnlineReferenceTable();
        if( xmlInsertOnlineResp != null &&  xmlInsertOnlineResp.getValue() != null){
            ArrayOfSDCInsertOnlineReferenceTable insertOnlineReferenceTable =  xmlInsertOnlineResp.getValue();
            if(insertOnlineReferenceTable != null) {
                List<SDCInsertOnlineReferenceTable> responseList = insertOnlineReferenceTable.getSDCInsertOnlineReferenceTable();
                if(responseList.size() > 0) {
                    SDCInsertOnlineReferenceTable refTable =  responseList.get(0);
                    boolean success = refTable.getIsError() == 0; //0 is success
                    result.setSuccess(success);
                    result.setMessage(refTable.getMessage()==null?"":refTable.getMessage().getValue());
                    result.setData("");
                    return result;
                }
            }

        }
        return result;
    }


}
