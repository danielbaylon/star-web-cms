package com.enovax.star.cms.commons.repository.b2cslm;

import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 11/11/16.
 */
@Repository
public interface B2CSLMStoreTransactionRepository  extends JpaRepository<B2CSLMStoreTransaction, Integer> {

    B2CSLMStoreTransaction findByReceiptNum(String receiptNum);

    @Query(value = "select * from B2CSLMStoreTransaction " +
            "where stage = 'Reserved' " +
            "and (tmStatus is null or tmStatus = '') " +
            "and createdDate <= DATEADD(mi, -:intervalFrom, dbo.GetLocalDate(DEFAULT))", nativeQuery = true)
    List<B2CSLMStoreTransaction> retrieveReservedUnpaidTimedOutTransactions(@Param("intervalFrom") Integer intervalFrom);

    @Query(value = "select * from B2CSLMStoreTransaction " +
            "where stage = 'Reserved' " +
            "and tmStatus is not null " +
            "and modifiedDate <= :intervalFrom " +
            "and modifiedDate > :intervalTo", nativeQuery = true)
    List<B2CSLMStoreTransaction> retrieveTransactionsWithPendingPayment(@Param("intervalFrom") Date intervalFrom, @Param("intervalTo") Date intervalTo);

    @Query(value = "select * from B2CSLMStoreTransaction " +
            "where stage = 'Reserved' " +
            "and tmStatus is not null " +
            "and modifiedDate <= :intervalFrom", nativeQuery = true)
    List<B2CSLMStoreTransaction> retrieveTransactionsWithPendingPayment(@Param("intervalFrom") Date intervalFrom);
}
