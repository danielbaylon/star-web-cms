package com.enovax.star.cms.commons.model.booking;

/**
 * Created by jensen on 10/11/16.
 */
public class TelemoneyParamPackage {

    private String tmReturnUrl;
    private String tmReturnUrlSkyDining;
    private String tmStatusUrl;
    private Integer transEmptyLowerBoundMillis;
    private Integer transEmptyUpperBoundMillis;
    private Integer transTimeoutLowerBoundMillis;
    private Integer transTimeoutUpperBoundMillis;

    public TelemoneyParamPackage() {
    }

    public TelemoneyParamPackage(String tmReturnUrl, String tmStatusUrl, Integer transEmptyLowerBoundMillis, Integer transEmptyUpperBoundMillis, Integer transTimeoutLowerBoundMillis, Integer transTimeoutUpperBoundMillis) {
        this.tmReturnUrl = tmReturnUrl;
        this.tmStatusUrl = tmStatusUrl;
        this.transEmptyLowerBoundMillis = transEmptyLowerBoundMillis;
        this.transEmptyUpperBoundMillis = transEmptyUpperBoundMillis;
        this.transTimeoutLowerBoundMillis = transTimeoutLowerBoundMillis;
        this.transTimeoutUpperBoundMillis = transTimeoutUpperBoundMillis;
    }

    public String getTmReturnUrl() {
        return tmReturnUrl;
    }

    public void setTmReturnUrl(String tmReturnUrl) {
        this.tmReturnUrl = tmReturnUrl;
    }

    public String getTmReturnUrlSkyDining() {
        return tmReturnUrlSkyDining;
    }

    public void setTmReturnUrlSkyDining(String tmReturnUrlSkyDining) {
        this.tmReturnUrlSkyDining = tmReturnUrlSkyDining;
    }

    public String getTmStatusUrl() {
        return tmStatusUrl;
    }

    public void setTmStatusUrl(String tmStatusUrl) {
        this.tmStatusUrl = tmStatusUrl;
    }

    public Integer getTransEmptyLowerBoundMillis() {
        return transEmptyLowerBoundMillis;
    }

    public void setTransEmptyLowerBoundMillis(Integer transEmptyLowerBoundMillis) {
        this.transEmptyLowerBoundMillis = transEmptyLowerBoundMillis;
    }

    public Integer getTransEmptyUpperBoundMillis() {
        return transEmptyUpperBoundMillis;
    }

    public void setTransEmptyUpperBoundMillis(Integer transEmptyUpperBoundMillis) {
        this.transEmptyUpperBoundMillis = transEmptyUpperBoundMillis;
    }

    public Integer getTransTimeoutLowerBoundMillis() {
        return transTimeoutLowerBoundMillis;
    }

    public void setTransTimeoutLowerBoundMillis(Integer transTimeoutLowerBoundMillis) {
        this.transTimeoutLowerBoundMillis = transTimeoutLowerBoundMillis;
    }

    public Integer getTransTimeoutUpperBoundMillis() {
        return transTimeoutUpperBoundMillis;
    }

    public void setTransTimeoutUpperBoundMillis(Integer transTimeoutUpperBoundMillis) {
        this.transTimeoutUpperBoundMillis = transTimeoutUpperBoundMillis;
    }
}
