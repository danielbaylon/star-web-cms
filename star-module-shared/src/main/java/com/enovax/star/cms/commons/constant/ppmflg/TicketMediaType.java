package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 10/3/16.
 */
public enum TicketMediaType {
    Pincode("PINCODE"),
    ExcelFile("Excel File"),
    ETicket("E-Ticket");

    public String label;

    TicketMediaType(String label) {
        this.label = label;
    }

}
