package com.enovax.star.cms.commons.jcrrepository.system;

import com.enovax.payment.tm.constant.TmRequestType;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;

public interface ITmParamRepository {
    TelemoneyParamPackage getTmParams(StoreApiChannels channel) throws JcrRepositoryException;
    TelemoneyParamPackage getTmParams(StoreApiChannels channel, TmRequestType transactionType) throws JcrRepositoryException;
}
