package com.enovax.star.cms.commons.model.axstar;

/**
 * Created by houtao on 22/9/16.
 */
public class AxCustomerGroup {

    private String customerGroupNumber;
    private String customerGroupName;

    public String getCustomerGroupNumber() {
        return customerGroupNumber;
    }

    public void setCustomerGroupNumber(String customerGroupNumber) {
        this.customerGroupNumber = customerGroupNumber;
    }

    public String getCustomerGroupName() {
        return customerGroupName;
    }

    public void setCustomerGroupName(String customerGroupName) {
        this.customerGroupName = customerGroupName;
    }
}
