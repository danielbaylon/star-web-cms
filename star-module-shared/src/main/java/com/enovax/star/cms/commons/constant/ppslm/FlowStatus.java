package com.enovax.star.cms.commons.constant.ppslm;

public enum FlowStatus {
    Pending("Pending","Pending"),
    Approved("Approved","Approved"),
    Rejected("Rejected","Rejected"),
    Completed("Completed","Completed");

    public final String code;
    public final String label;

    private FlowStatus(String code, String label) {
        this.code = code;
        this.label = label;
    }
    public static String getLabel(String code){
        for(FlowStatus tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
}
