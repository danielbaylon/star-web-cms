package com.enovax.star.cms.commons.mgnl.form.field.transformer;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.PropertysetItem;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.multi.DelegatingMultiValueFieldTransformer;
import info.magnolia.ui.vaadin.integration.jcr.DefaultProperty;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by jennylynsze on 3/29/16.
 */
public class MultiLanguageDelegatingMultiValueSubnodeTransformer extends DelegatingMultiValueFieldTransformer {

    private static final Logger log = LoggerFactory.getLogger(MultiLanguageDelegatingMultiValueSubnodeTransformer.class);

    private JcrNodeAdapter subNode;

    @Inject
    public MultiLanguageDelegatingMultiValueSubnodeTransformer(Item relatedFormItem, ConfiguredFieldDefinition definition, Class<PropertysetItem> type, I18NAuthoringSupport i18nAuthoringSupport) {
        super(relatedFormItem, definition, type, i18nAuthoringSupport);

        // In case the child item has been create already but hasn't persisted yet
//        if (subNode == null) {
//            if (((JcrNodeAdapter) relatedFormItem).getChildren().containsKey(definition.getName())) {
//                subNode = (JcrNodeAdapter) ((JcrNodeAdapter) relatedFormItem).getChildren().get(definition.getName());
//            }
//        }
    }

    /**
     * This transformer's write implementation is empty. We do not need to write to the item as this is delegated to the sub-fields.
     */
    @Override
    public void writeToItem(PropertysetItem newValue) {
        JcrNodeAdapter rootItem = getRootItem();
        if (rootItem == null) {
            // nothing to write yet, someone just clicked add and then clicked away to other field
            return;
        }
        rootItem.getChildren().clear();
        // Add childItems to the rootItem
        setNewChildItem(rootItem, newValue);
        // Remove all no more existing children
        //detachNoMoreExistingChildren(rootItem);
        // Attach or Detach rootItem from parent
        // handleRootitemAndParent(rootItem);
        //log.debug("CALL writeToItem");
    }

    /**
     * If values are already stored, remove the no more existing one.
     */
    private void detachNoMoreExistingChildren(JcrNodeAdapter rootItem) {
        try {
            List<Node> children = getStoredChildNodes(rootItem);
            for (Node child : children) {
                if (rootItem.getChild(child.getName()) == null) {
                    JcrNodeAdapter toRemove = new JcrNodeAdapter(child);
                    rootItem.removeChild(toRemove);
                } else {
                    detachNoMoreExistingChildren((JcrNodeAdapter) rootItem.getChild(child.getName()));
                }
            }
        } catch (RepositoryException e) {
            log.error("Could remove children", e);
        }
    }

    protected void setNewChildItem(JcrNodeAdapter rootItem, PropertysetItem newValue) {
        // Used to build the ChildItemName;
        Set<String> childNames = new HashSet<String>();
        Node rootNode = rootItem.getJcrItem();
        try {
            Iterator<?> it = newValue.getItemPropertyIds().iterator();
            while (it.hasNext()) {
                Object propertyName = it.next();
                Property<?> p = newValue.getItemProperty(propertyName);
                // Do not handle null values
                if (p == null || p.getValue() == null) {
                    continue;
                }
                Object value = p.getValue();
                // Create the child Item Name with i18n supports
                String childName = createChildItemName(childNames, value, rootItem);
                // Get or create the childItem
                JcrNodeAdapter childItem = initializeChildItem(rootItem, rootNode, childName);
                if (value instanceof JcrNodeAdapter) {
                    // Replace properties
                    JcrNodeAdapter subItem = (JcrNodeAdapter)value;
                    for (Object propId : subItem.getItemPropertyIds()) {
                        setChildItemValue(childItem, propId, subItem.getItemProperty(propId).getValue());
                    }
                } else {
                    // Set the Value to the ChildItem
                    setChildItemValue(childItem, propertyName, value);
                }
            }
        } catch (Exception e) {
            log.warn("Not able to create a Child Item for {} ", rootItem.getItemId(), e);
        }
    }

    /**
     * Basic Implementation that create child Nodes with increasing number as Name.
     */
    protected String createChildItemName(Set<String> childNames, Object value, JcrNodeAdapter rootItem) {
        int nb = 0;
        String name = "0";
        if (!Locale.ENGLISH.toString().equals(getLocale().toString()) && definition.isI18n()) {
            // Other languages
            name = String.format("%s_%s", name, getLocale().toString());
        }
        DecimalFormat df = new DecimalFormat("0");
        while (childNames.contains(name)) {
            nb += 1;
            name = df.format(nb);
            if (!Locale.ENGLISH.toString().equals(getLocale().toString()) && definition.isI18n()) {
                // Other languages
                name = String.format("%s_%s", name, getLocale().toString());
            }
        }
        childNames.add(name);
        return name;
    }

    /**
     * Create a Child Item.<br>
     * - if the related node already has a Child Node called 'childName', initialize the ChildItem based on this child Node.<br>
     * - else create a new JcrNodeAdapter.
     */
    protected JcrNodeAdapter initializeChildItem(JcrNodeAdapter rootItem, Node rootNode, String childName) throws PathNotFoundException, RepositoryException {
        JcrNodeAdapter childItem = null;
        if (!(rootItem instanceof JcrNewNodeAdapter) && rootNode.hasNode(childName)) {
            childItem = new JcrNodeAdapter(rootNode.getNode(childName));
        } else {
            childItem = new JcrNewNodeAdapter(rootNode, childNodeType, childName);
        }
        rootItem.addChild(childItem);
        return childItem;
    }

    /**
     * Set the value as property to the childItem.
     */
    protected void setChildItemValue(JcrNodeAdapter childItem, Object propertyName, Object value) {
        childItem.addItemProperty(propertyName, new DefaultProperty(value));
    }


    /**
     * Overridden to keep only the digit part in child-node names.
     */
    @Override
    protected String getSubItemBaseName() {
        return "";
    }

    /**
     * Overridden to get an intermediate sub-node where the multiple nodes are stored, rather than directly under the root node.
     * <p>
     * The sub-node is created if it doesn't exist, and is named after the multi-value field definition.
     */
    @Override
    protected JcrNodeAdapter getRootItem() {
        if (subNode == null) {
            JcrNodeAdapter rootItem = super.getRootItem();
            Node rootJcrItem = rootItem.getJcrItem();

//            if(rootItem instanceof  JcrNewNodeAdapter) {
//                try {
//                    rootJcrItem = rootJcrItem.getParent();
//                } catch (RepositoryException e) {
//                    e.printStackTrace();
//                }
//            }

            try {
                if (rootJcrItem.hasNode(definition.getName())) {
                    subNode = new JcrNodeAdapter(rootJcrItem.getNode(definition.getName()));
                } else if (rootItem.getChildren().containsKey(definition.getName())) {
                    // Initialize an intermediate sub-node when the child item has been create already but hasn't persisted yet.
                    Object childItem = rootItem.getChildren().get(definition.getName());
                    if (childItem instanceof JcrNodeAdapter) {
                        subNode = (JcrNodeAdapter) childItem;
                    }
                } else {
                    subNode = new JcrNewNodeAdapter(rootJcrItem, NodeTypes.ContentNode.NAME, definition.getName());
                }
                rootItem.addChild(subNode);
            } catch (RepositoryException e) {
                log.warn(String.format("Could not determine whether form item '%s' had a child node named '%s'", rootJcrItem, definition.getName()), e);
            }
        }
        return subNode;
    }

}
