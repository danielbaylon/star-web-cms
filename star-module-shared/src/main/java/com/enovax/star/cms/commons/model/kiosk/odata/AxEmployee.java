package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.Employee
 * 
 * @author Justin
 * @since 6 SEP 16
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEmployee implements Serializable {

	@JsonProperty("StaffId")
	private String staffId;

	@JsonProperty("NameOnReceipt")
	private String nameOnReceipt;

	@JsonProperty("Name")
	private String name;

	@JsonProperty("CultureName")
	private String cultureName;

	@JsonProperty("ChangePassword")
	private String changePassword;

	//@JsonProperty("Permissions")
	//private EmployeePermissions permissions;

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getNameOnReceipt() {
		return nameOnReceipt;
	}

	public void setNameOnReceipt(String nameOnReceipt) {
		this.nameOnReceipt = nameOnReceipt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCultureName() {
		return cultureName;
	}

	public void setCultureName(String cultureName) {
		this.cultureName = cultureName;
	}

	public String getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(String changePassword) {
		this.changePassword = changePassword;
	}
/**
	public EmployeePermissions getPermissions() {
		return permissions;
	}

	public void setPermissions(EmployeePermissions permissions) {
		this.permissions = permissions;
	}
**/
	

}
