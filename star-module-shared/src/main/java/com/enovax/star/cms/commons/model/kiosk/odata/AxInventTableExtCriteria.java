package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_TicketingExtension.DataModel.InventTableExtCriteria
 * 
 * @author Justin
 *
 */
public class AxInventTableExtCriteria {

    @JsonProperty("CategoryHierarchy")
    private String categoryHierarchy = "0";

    @JsonProperty("ChannelId")
    private String channelId = "0";

    @JsonProperty("DataAreaId")
    private String dataAreaId = "";

    @JsonProperty("DataLevelValue")
    private String dataLevelValue = "4";

    @JsonProperty("ItemId")
    private String itemId = "";

    @JsonIgnore
    private String productId;

    public String getCategoryHierarchy() {
        return categoryHierarchy;
    }

    public void setCategoryHierarchy(String categoryHierarchy) {
        this.categoryHierarchy = categoryHierarchy;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDataLevelValue() {
        return dataLevelValue;
    }

    public void setDataLevelValue(String dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
