package com.enovax.star.cms.commons.mgnl.contentapp;

import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.ui.contentapp.browser.BrowserSubAppDescriptor;

/**
 * Created by jennylynsze on 8/3/16.
 */
public interface StarBrowserSubAppDescriptor extends BrowserSubAppDescriptor {
    AccessDefinition getPermissions();
    String getMainSubAppDisplay();
    boolean isOpenFirst();
}
