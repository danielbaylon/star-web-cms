
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_UpCrossSellTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_UpCrossSellTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACTIVE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CHANNEL" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ITEMID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MINIMUMQTY" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MULTIPLEQTY" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RECID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TABLEALL" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="UNITIDId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpCrossSellLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_UpCrossSellLineTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_UpCrossSellTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "active",
    "channel",
    "itemid",
    "minimumqty",
    "multipleqty",
    "recid",
    "tableall",
    "unitidId",
    "upCrossSellLines"
})
public class SDCUpCrossSellTable {

    @XmlElement(name = "ACTIVE")
    protected Integer active;
    @XmlElement(name = "CHANNEL")
    protected Long channel;
    @XmlElementRef(name = "ITEMID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemid;
    @XmlElement(name = "MINIMUMQTY")
    protected BigDecimal minimumqty;
    @XmlElement(name = "MULTIPLEQTY")
    protected BigDecimal multipleqty;
    @XmlElement(name = "RECID")
    protected Long recid;
    @XmlElement(name = "TABLEALL")
    protected Integer tableall;
    @XmlElementRef(name = "UNITIDId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unitidId;
    @XmlElementRef(name = "UpCrossSellLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCUpCrossSellLineTable> upCrossSellLines;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getACTIVE() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setACTIVE(Integer value) {
        this.active = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCHANNEL() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCHANNEL(Long value) {
        this.channel = value;
    }

    /**
     * Gets the value of the itemid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getITEMID() {
        return itemid;
    }

    /**
     * Sets the value of the itemid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setITEMID(JAXBElement<String> value) {
        this.itemid = value;
    }

    /**
     * Gets the value of the minimumqty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMINIMUMQTY() {
        return minimumqty;
    }

    /**
     * Sets the value of the minimumqty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMINIMUMQTY(BigDecimal value) {
        this.minimumqty = value;
    }

    /**
     * Gets the value of the multipleqty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMULTIPLEQTY() {
        return multipleqty;
    }

    /**
     * Sets the value of the multipleqty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMULTIPLEQTY(BigDecimal value) {
        this.multipleqty = value;
    }

    /**
     * Gets the value of the recid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRECID() {
        return recid;
    }

    /**
     * Sets the value of the recid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRECID(Long value) {
        this.recid = value;
    }

    /**
     * Gets the value of the tableall property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTABLEALL() {
        return tableall;
    }

    /**
     * Sets the value of the tableall property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTABLEALL(Integer value) {
        this.tableall = value;
    }

    /**
     * Gets the value of the unitidId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUNITIDId() {
        return unitidId;
    }

    /**
     * Sets the value of the unitidId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUNITIDId(JAXBElement<String> value) {
        this.unitidId = value;
    }

    /**
     * Gets the value of the upCrossSellLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCUpCrossSellLineTable> getUpCrossSellLines() {
        return upCrossSellLines;
    }

    /**
     * Sets the value of the upCrossSellLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}
     *     
     */
    public void setUpCrossSellLines(JAXBElement<ArrayOfSDCUpCrossSellLineTable> value) {
        this.upCrossSellLines = value;
    }

}
