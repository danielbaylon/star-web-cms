package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.UpCrossSellTableEntity;

/**
 * 
 * @author Justin
 * @since 6 OCT 16
 */
public class KioskGetUpCrossSellResponse {
    
    
    private List<UpCrossSellTableEntity>  upCrossSellTableEntityList = new ArrayList<>();

    public List<UpCrossSellTableEntity> getUpCrossSellTableEntityList() {
        return upCrossSellTableEntityList;
    }

    public void setUpCrossSellTableEntityList(List<UpCrossSellTableEntity> upCrossSellTableEntityList) {
        this.upCrossSellTableEntityList = upCrossSellTableEntityList;
    }
    
    

}
