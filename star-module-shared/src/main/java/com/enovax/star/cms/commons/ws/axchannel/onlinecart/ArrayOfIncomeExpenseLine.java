
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIncomeExpenseLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIncomeExpenseLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IncomeExpenseLine" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}IncomeExpenseLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIncomeExpenseLine", propOrder = {
    "incomeExpenseLine"
})
public class ArrayOfIncomeExpenseLine {

    @XmlElement(name = "IncomeExpenseLine", nillable = true)
    protected List<IncomeExpenseLine> incomeExpenseLine;

    /**
     * Gets the value of the incomeExpenseLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the incomeExpenseLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncomeExpenseLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IncomeExpenseLine }
     * 
     * 
     */
    public List<IncomeExpenseLine> getIncomeExpenseLine() {
        if (incomeExpenseLine == null) {
            incomeExpenseLine = new ArrayList<IncomeExpenseLine>();
        }
        return this.incomeExpenseLine;
    }

}
