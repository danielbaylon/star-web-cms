package com.enovax.star.cms.commons.model.axchannel.pin.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.pin.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.ws.axchannel.pin.*;

import javax.xml.bind.JAXBElement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 12/27/16.
 */
public class AxPinTransformer {
    public static ApiResult<AxWOTPinViewTable> fromWsB2BPINTableInquiry(SDCB2BPINTableInquiryResponse response) throws ParseException {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        List<AxWOTPinViewTable> list = new ArrayList<AxWOTPinViewTable>();
        ApiResult<AxWOTPinViewTable> result = new ApiResult<>();

        JAXBElement<SDCB2BPINViewTable> xmlViewTable = response.getB2BPinViewTableInquiry();

        SDCB2BPINViewTable viewTable = null;
        if( xmlViewTable != null ){
            viewTable =  xmlViewTable.getValue();
        }

        if(viewTable != null){

            JAXBElement<ArrayOfSDCB2BPINViewLineTable> xmlPinLines = viewTable.getPINViewLineCollection();
            ArrayOfSDCB2BPINViewLineTable arrayOfPinViewLineTable = null;

            if(xmlPinLines != null){
                arrayOfPinViewLineTable = xmlPinLines.getValue();
            }

            AxWOTPinViewTable table = AxPinTransformer.fromWsB2BPINTableInquiry(viewTable, arrayOfPinViewLineTable.getSDCB2BPINViewLineTable());
            result.setSuccess(viewTable.isReturnStatus() != null ? viewTable.isReturnStatus().booleanValue() : false);
            result.setData(table);
        }else {
            result = getCommonResponseMessage(result, response);
        }

        return result;
    }

    public static ApiResult<List<AxWOTPinViewTable>> fromWsB2BPINView(SDCB2BPINViewResponse response) throws ParseException {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        List<AxWOTPinViewTable> list = new ArrayList<AxWOTPinViewTable>();
        ApiResult<List<AxWOTPinViewTable>> result = new ApiResult<List<AxWOTPinViewTable>>();

        JAXBElement<ArrayOfSDCB2BPINViewTable> xmlArrayOfPinViewTable = response.getB2BPinViewTable();
        ArrayOfSDCB2BPINViewTable arrayOfPinViewTable = null;
        if(xmlArrayOfPinViewTable != null ){
            arrayOfPinViewTable = xmlArrayOfPinViewTable.getValue();
        }
        if(arrayOfPinViewTable != null){
            List<SDCB2BPINViewTable> pinviews = arrayOfPinViewTable.getSDCB2BPINViewTable();
            if(pinviews != null && pinviews.size() > 0){
                list = AxPinTransformer.fromWsB2BPINView(pinviews);
            }
        }
        if(list != null && list.size() > 0){
            result.setSuccess(true);
            result.setData(list);
        }else{
            result = getCommonResponseMessage(result, response);
        }
        return result;
    }

    public static ApiResult getCommonResponseMessage(ApiResult axResponse, ServiceResponse response) {
        JAXBElement<ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if (errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (ResponseError rr : responseErrorList) {
                        if(rr == null){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }

    public static ApiResult<List<AxWOTPinViewLine>> fromWsB2BPINViewLine(SDCB2BPINViewLineResponse response) throws ParseException {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        List<AxWOTPinViewLine> list = new ArrayList<AxWOTPinViewLine>();
        ApiResult<List<AxWOTPinViewLine>> result = new ApiResult<List<AxWOTPinViewLine>>();

        JAXBElement<ArrayOfSDCB2BPINViewLineTable> xmlArrayOfPinViewLineTable = response.getB2BPinViewLineTable();
        ArrayOfSDCB2BPINViewLineTable arrayOfPinViewLineTable = null;
        if(xmlArrayOfPinViewLineTable != null){
            arrayOfPinViewLineTable = xmlArrayOfPinViewLineTable.getValue();
        }
        if(arrayOfPinViewLineTable != null){
            List<SDCB2BPINViewLineTable> pinViewLine = arrayOfPinViewLineTable.getSDCB2BPINViewLineTable();
            if(pinViewLine != null && pinViewLine.size() > 0){
                list = AxPinTransformer.fromB2BPINViewLine(pinViewLine);
            }
        }
        if(list != null && list.size() > 0){
            result.setSuccess(true);
            result.setData(list);
        }else{
            result = getCommonResponseMessage(result, response);
        }
        return result;
    }

    public static ApiResult getCommonResponseMessage(ApiResult axResponse, com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline.ServiceResponse response) {
        JAXBElement<com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline.ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if (errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline.ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline.ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline.ResponseError rr : responseErrorList) {
                        if(rr == null){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }

    public static ApiResult<String> fromWsB2BPinUpdate(SDCB2BPINUpdateResponse response) {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        ApiResult<String> result = new ApiResult<String>();
        result.setSuccess(false);
        JAXBElement<SDCB2BPINUpdateCancelTable> xmlUpdateTable = response.getTable();
        if(xmlUpdateTable != null && xmlUpdateTable.getValue() != null){
            SDCB2BPINUpdateCancelTable updateTable = xmlUpdateTable.getValue();
            result.setSuccess(updateTable.isReturnStatus() != null ? updateTable.isReturnStatus().booleanValue() : false);
            result.setData(updateTable.getReturnMessage() != null ? updateTable.getReturnMessage().getValue() : null);
        }
        return getCommonResponseMessage(result, response);
    }


    public static ApiResult<String> fromWsB2BPinCancel(SDCB2BPINCancelResponse response) {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        ApiResult<String> result = new ApiResult<String>();
        JAXBElement<SDCB2BPINUpdateCancelTable> xmlUpdateTable = response.getTable();
        if(xmlUpdateTable != null && xmlUpdateTable.getValue() != null){
            SDCB2BPINUpdateCancelTable updateTable = xmlUpdateTable.getValue();
            result.setSuccess(updateTable.isReturnStatus() != null ? updateTable.isReturnStatus().booleanValue() : false);
            result.setData(updateTable.getReturnMessage() != null ? updateTable.getReturnMessage().getValue() : null);
        }
        return getCommonResponseMessage(result, response);
    }


    public static ApiResult<AxWOTSalesOrder> fromWotSaveOrder(SDCSaveWOTOrderResponse response) {
        ApiResult<AxWOTSalesOrder> axResponse = new ApiResult<AxWOTSalesOrder>();
        if(response != null){
            axResponse.setSuccess(response.isSuccess());
        }else{
            axResponse.setSuccess(false);
        }
        if(response != null && response.isSuccess() != null && response.isSuccess().booleanValue() && response.getGeneratedPINId() != null && response.getGeneratedPINId().getValue() != null && response.getGeneratedPINId().getValue().trim().length() > 0){
            AxWOTSalesOrder order = new AxWOTSalesOrder();
            order.setGeneratedPIN(response.getGeneratedPINId().getValue());
            JAXBElement<SalesOrder> xmlSalesOrder = response.getOrder();
            if(xmlSalesOrder != null && xmlSalesOrder.getValue() != null){
                order.setSalesOrder(populateAxSalesOrder(xmlSalesOrder.getValue()));
            }
            axResponse.setData(order);
        }else{
            String errMsg = response.getErrMessage() != null ? response.getErrMessage().getValue() : null;
            if(errMsg != null && errMsg.trim().length() > 0){
                axResponse.setMessage(errMsg);
            }else{
                getCommonResponseMessage(axResponse, response);
            }
        }
        return axResponse;
    }

    private static AxSalesOrder populateAxSalesOrder(SalesOrder so) {
        if(so == null){
            return null;
        }
        AxSalesOrder ao = new AxSalesOrder();
        ao.setId(so.getId() != null ? so.getId().getValue() : null);
        ao.setChannelId(so.getChannelId() != null ? so.getChannelId().longValue() : null);
        ao.setConfirmationId(so.getConfirmationId() != null ? so.getConfirmationId().getValue() : null);
        ao.setStatus(so.getStatus() != null ? so.getStatus().getValue() : null);
        ao.setSalesId(so.getSalesId() != null ? so.getSalesId().getValue() : null);
        ao.setRequestedDeliveryDate(so.getRequestedDeliveryDate() != null ? so.getRequestedDeliveryDate().getValue() : null);
        ao.setPaymentStatus(so.getPaymentStatus());
        ao.setOrderPlacedDate(so.getOrderPlacedDate() != null ? so.getOrderPlacedDate().getValue() : null);
        ao.setChargeAmountWithCurrency(so.getChargeAmountWithCurrency() != null ? so.getChargeAmountWithCurrency().getValue() : null);
        ao.setDeliveryModeDescription(so.getDeliveryModeDescription() != null ? so.getDeliveryModeDescription().getValue() : null);
        ao.setDeliveryModeId(so.getDeliveryModeId() != null ? so.getDeliveryModeId().getValue() : null);
        ao.setDiscountAmount(so.getDiscountAmount());
        ao.setDiscountAmountWithCurrency(so.getDiscountAmountWithCurrency() != null ? so.getDiscountAmountWithCurrency().getValue() : null);
        ao.setLoyaltyCardId(so.getLoyaltyCardId() != null ? so.getLoyaltyCardId().getValue() : null);
        ao.setSubtotalWithCurrency(so.getSubtotalWithCurrency() != null ? so.getSubtotalWithCurrency().getValue() : null);
        ao.setTaxAmountWithCurrency(so.getTaxAmountWithCurrency() != null ? so.getTaxAmountWithCurrency().getValue() : null);
        ao.setTotalAmount(so.getTotalAmount());
        ao.setTotalAmountWithCurrency(so.getTotalAmountWithCurrency() != null ? so.getTotalAmountWithCurrency().getValue() : null);

        JAXBElement<ArrayOfTransactionItem> xmlItems = so.getItems();
        if(xmlItems != null && xmlItems.getValue() != null && xmlItems.getValue().getTransactionItem() != null && xmlItems.getValue().getTransactionItem().size() > 0){
            List<AxTransactionItem> txnItems = new ArrayList<AxTransactionItem>();
            List<TransactionItem> xmlTxnItems = xmlItems.getValue().getTransactionItem();
            for(TransactionItem xti : xmlTxnItems){
                AxTransactionItem ati = new AxTransactionItem();
                ati.setDiscountAmount(xti.getDiscountAmount());
                ati.setDiscountAmountWithCurrency(xti.getDiscountAmountWithCurrency() != null ? xti.getDiscountAmountWithCurrency().getValue() : null);
                ati.setItemId(xti.getItemId() != null ? xti.getItemId().getValue() : null);
                ati.setItemType(xti.getItemType() != null ? AxTransactionItemType.fromValue(xti.getItemType().value()) : AxTransactionItemType.NONE);
                ati.setLineId(xti.getLineId() != null ? xti.getLineId().getValue() : null);
                ati.setNetAmountWithCurrency(xti.getNetAmountWithCurrency() != null ? xti.getNetAmountWithCurrency().getValue() : null);
                ati.setPriceWithCurrency(xti.getPriceWithCurrency() != null ? xti.getPriceWithCurrency().getValue() : null);
                ati.setProductId(xti.getProductId());
                ati.setQuantity(xti.getQuantity());
                ati.setTaxAmountWithCurrency(xti.getTaxAmountWithCurrency() != null ? xti.getTaxAmountWithCurrency().getValue() : null);
                ati.setVariantInventoryDimensionId(xti.getVariantInventoryDimensionId() != null ? xti.getVariantInventoryDimensionId().getValue() : null);
                txnItems.add(ati);
            }
            ao.setTxnItems(txnItems);
        }
        return ao;
    }

    private static ApiResult<AxWOTSalesOrder> getCommonResponseMessage(ApiResult<AxWOTSalesOrder> axResponse, SDCSaveWOTOrderResponse response) {
        JAXBElement<com.enovax.star.cms.commons.ws.axchannel.pin.ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if (errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            com.enovax.star.cms.commons.ws.axchannel.pin.ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<com.enovax.star.cms.commons.ws.axchannel.pin.ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (com.enovax.star.cms.commons.ws.axchannel.pin.ResponseError rr : responseErrorList) {
                        if(rr == null){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }

    public static AxWOTPinViewTable fromWsB2BPINTableInquiry(SDCB2BPINViewTable pinview, List<SDCB2BPINViewLineTable> pinviewLines) throws ParseException {
        if(pinview == null){
            return null;
        }

        AxWOTPinViewTable table = new AxWOTPinViewTable();
        table.setAllowPartialRedemption(pinview.getAllowPartialRedemption());
        table.setCustAccount(pinview.getCustAccount().getValue());
        table.setDescription(pinview.getDescription().getValue());
        table.setIsWashedDown(pinview.getIsWashedDown());
        if(pinview.getEndDateTime() != null && pinview.getEndDateTime().getValue() != null && pinview.getEndDateTime().getValue().trim().length() > 0) {
            table.setEndDateTime(NvxDateUtils.parseDate(pinview.getEndDateTime().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_TIME_FORMAT));
        }
        table.setIsGroupTicket(pinview.getIsGroupTicket());
        table.setIsReservation(pinview.getIsReservation());
        table.setMediaType(pinview.getMediaType().getValue());
        table.setPinBlockedBy(pinview.getPINBlockedBy().getValue());
        if(pinview.getPINBlockedDateTime() != null && pinview.getPINBlockedDateTime().getValue() != null && pinview.getPINBlockedDateTime().getValue().trim().length() > 0) {
            table.setPinBlockedDateTime(NvxDateUtils.parseDate(pinview.getPINBlockedDateTime().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_TIME_FORMAT));
        }
        table.setPinCode(pinview.getPINCode().getValue());
        table.setPinStatus(pinview.getPINStatus().getValue());
        table.setPackageName(pinview.getPackageName().getValue());
        table.setQtyPerProduct(pinview.getQtyPerProduct());
        table.setReferenceId(pinview.getReferenceId().getValue());
        table.setSourceType(pinview.getSourceType());

        if(pinview.getLastRedeemDateTime() != null && pinview.getLastRedeemDateTime().getValue() != null && pinview.getLastRedeemDateTime().getValue().trim().length() > 0) {
            table.setLastRedeemDateTime(NvxDateUtils.parseDate(pinview.getLastRedeemDateTime().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_TIME_FORMAT));
        }

        if(pinviewLines == null || pinviewLines.size() == 0){
            return null;
        }

        List<AxWOTPinViewLine> list = new ArrayList<AxWOTPinViewLine>();
        for(SDCB2BPINViewLineTable t : pinviewLines){
            AxWOTPinViewLine line = new AxWOTPinViewLine();
            if(t.getEndDateTime() != null && t.getEndDateTime().getValue() != null && t.getEndDateTime().getValue().trim().length() > 0) {
                line.setEndDateTime(NvxDateUtils.parseDate(t.getEndDateTime().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_TIME_FORMAT));
            }
            if(t.getEventDate() != null && t.getEventDate().getValue() != null && t.getEventDate().getValue().trim().length() > 0) {
                line.setEventDate(NvxDateUtils.parseDate(t.getEventDate().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_FORMAT));
            }
            line.setEventGroupId(t.getEventGroupId().getValue());
            line.setEventLineId(t.getEventLineId().getValue());
            line.setInventTransId(t.getInventTransId().getValue());
            line.setInvoiceId(t.getInvoiceId().getValue());
            line.setIsCombineTicket(t.getIsCombineTicket());
            line.setIsRedeemed(t.getIsRedeemed());
            line.setItemId(t.getItemId().getValue());
            line.setMediaType(t.getMediaType().getValue());
            line.setNoOfPax(t.getNoOfPax());
            line.setOpenValidityRule(t.getOpenValidityRule().getValue());
            line.setQty(t.getQty());
            line.setQtyRedeemed(t.getQtyRedeemed());
            line.setQtyReturned(t.getQtyReturned());
            line.setQtyAutoGenerated(t.getQtyAutoGenerated());
            line.setQtyWashedDown(t.getQtyWashedDown());
            line.setReferenceId(t.getReferenceId().getValue());
            line.setRetailVariantId(t.getRetailVariantId().getValue());
            line.setSalesId(t.getSalesId().getValue());
            if(t.getStartDateTime() != null && t.getStartDateTime().getValue() != null && t.getStartDateTime().getValue().trim().length() > 0) {
                line.setStartDateTime(NvxDateUtils.parseDate(t.getStartDateTime().getValue(), NvxDateUtils.AX_WOT_PROD_DATE_TIME_FORMAT));
            }
            line.setTransactionId(t.getTransactionId().getValue());
            line.setVersionRefRecId(t.getVersionRefRecId() != null ? t.getVersionRefRecId().longValue() : 0 );
            line.setTransactionLineNum(t.getTransactionLineNum() !=null? t.getTransactionLineNum().getValue(): null);
            list.add(line);
        }

        table.setPinViewLineCollection(list);

        return table;
    }



    public static List<AxWOTPinViewTable> fromWsB2BPINView(List<SDCB2BPINViewTable> pinviews) throws ParseException {
        if(pinviews == null || pinviews.size() == 0){
            return null;
        }
        List<AxWOTPinViewTable> list = new ArrayList<AxWOTPinViewTable>();
        for(SDCB2BPINViewTable t : pinviews){
            AxWOTPinViewTable table = new AxWOTPinViewTable();
            table.setAllowPartialRedemption(t.getAllowPartialRedemption());
            table.setCustAccount(t.getCustAccount().getValue());
            table.setDescription(t.getDescription().getValue());
            if(t.getEndDateTime() != null && t.getEndDateTime().getValue() != null && t.getEndDateTime().getValue().trim().length() > 0) {
                table.setEndDateTime(NvxDateUtils.parseDate(t.getEndDateTime().getValue(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
            }
            table.setIsGroupTicket(t.getIsGroupTicket());
            table.setIsReservation(t.getIsReservation());
            table.setMediaType(t.getMediaType().getValue());
            table.setPinBlockedBy(t.getPINBlockedBy().getValue());
            if(t.getPINBlockedDateTime() != null && t.getPINBlockedDateTime().getValue() != null && t.getPINBlockedDateTime().getValue().trim().length() > 0) {
                table.setPinBlockedDateTime(NvxDateUtils.parseDate(t.getPINBlockedDateTime().getValue(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
            }
            table.setPinCode(t.getPINCode().getValue());
            table.setPinStatus(t.getPINStatus().getValue());
            table.setPackageName(t.getPackageName().getValue());
            table.setQtyPerProduct(t.getQtyPerProduct());
            table.setReferenceId(t.getReferenceId().getValue());
            table.setSourceType(t.getSourceType());
            list.add(table);
        }
        return list;
    }

    public static List<AxWOTPinViewLine> fromB2BPINViewLine(List<SDCB2BPINViewLineTable> pinViewLine) throws ParseException {
        if(pinViewLine == null || pinViewLine.size() == 0){
            return null;
        }
        List<AxWOTPinViewLine> list = new ArrayList<AxWOTPinViewLine>();
        for(SDCB2BPINViewLineTable t : pinViewLine){
            AxWOTPinViewLine line = new AxWOTPinViewLine();
            if(t.getEndDateTime() != null && t.getEndDateTime().getValue() != null && t.getEndDateTime().getValue().trim().length() > 0) {
                line.setEndDateTime(NvxDateUtils.parseDate(t.getEndDateTime().getValue(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
            }
            if(t.getEventDate() != null && t.getEventDate().getValue() != null && t.getEventDate().getValue().trim().length() > 0) {
                line.setEventDate(NvxDateUtils.parseDate(t.getEventDate().getValue(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_FORMAT));
            }
            line.setEventGroupId(t.getEventGroupId().getValue());
            line.setEventLineId(t.getEventLineId().getValue());
            line.setInventTransId(t.getInventTransId().getValue());
            line.setInvoiceId(t.getInvoiceId().getValue());
            line.setIsCombineTicket(t.getIsCombineTicket());
            line.setIsRedeemed(t.getIsRedeemed());
            line.setItemId(t.getItemId().getValue());
            line.setMediaType(t.getMediaType().getValue());
            line.setNoOfPax(t.getNoOfPax());
            line.setOpenValidityRule(t.getOpenValidityRule().getValue());
            line.setQty(t.getQty());
            line.setQtyRedeemed(t.getQtyRedeemed());
            line.setQtyReturned(t.getQtyReturned());
            line.setReferenceId(t.getReferenceId().getValue());
            line.setRetailVariantId(t.getRetailVariantId().getValue());
            line.setSalesId(t.getSalesId().getValue());
            if(t.getStartDateTime() != null && t.getStartDateTime().getValue() != null && t.getStartDateTime().getValue().trim().length() > 0) {
                line.setStartDateTime(NvxDateUtils.parseDate(t.getStartDateTime().getValue(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT));
            }
            line.setTransactionId(t.getTransactionId().getValue());
            line.setVersionRefRecId(t.getVersionRefRecId());
            list.add(line);
        }
        return list;
    }

}
