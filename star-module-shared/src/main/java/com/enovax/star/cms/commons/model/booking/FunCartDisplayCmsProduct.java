package com.enovax.star.cms.commons.model.booking;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jensen on 28/3/16.
 */
public class FunCartDisplayCmsProduct {

    private String id;
    private String name;
    private BigDecimal subtotal;
    private String subtotalText;
    /**
     * Used in shopping cart to let system redirect page to edit admission or
     * edit ticket order page.
     */
    private Boolean isAdmission = Boolean.FALSE;

    /**
     * Used in shopping cart to let system decide edit product in product order
     * page or admission page (transport layout)
     */
    private Boolean isTransportLayout = Boolean.FALSE;

    private List<FunCartDisplayItem> items = new ArrayList<>();

    public void reorganiseItems() {
        final List<FunCartDisplayItem> mainItems = new ArrayList<>();
        final Map<String, List<FunCartDisplayItem>> topupMap = new HashMap<>();
        for (FunCartDisplayItem item : items) {
            if (item.isTopup()) {
                // TODO cannot handle cases where the same ax product is added
                // to the cms product...
                List<FunCartDisplayItem> topups = topupMap.get(item.getParentListingId());
                if (topups == null) {
                    topups = new ArrayList<>();
                    topupMap.put(item.getParentListingId(), topups);
                }
                topups.add(item);
            } else {
                mainItems.add(item);
            }
        }

        final List<FunCartDisplayItem> finalItems = new ArrayList<>();

        Collections.sort(mainItems, new FunCartDisplayItemNameComparator());

        for (FunCartDisplayItem mainItem : mainItems) {
            finalItems.add(mainItem);
            final List<FunCartDisplayItem> topups = topupMap.get(mainItem.getListingId());
            if (topups != null) {
                for (FunCartDisplayItem topupItem : topups) {
                    finalItems.add(topupItem);
                }
            }
        }

        items = finalItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotalText() {
        return subtotalText;
    }

    public void setSubtotalText(String subtotalText) {
        this.subtotalText = subtotalText;
    }

    public List<FunCartDisplayItem> getItems() {
        return items;
    }

    public void setItems(List<FunCartDisplayItem> items) {
        this.items = items;
    }

    public Boolean getIsAdmission() {
        return isAdmission;
    }

    public void setIsAdmission(Boolean isAdmission) {
        this.isAdmission = isAdmission;
    }

    public Boolean getIsTransportLayout() {
        return isTransportLayout;
    }

    public void setIsTransportLayout(Boolean isTransportLayout) {
        this.isTransportLayout = isTransportLayout;
    }

}
