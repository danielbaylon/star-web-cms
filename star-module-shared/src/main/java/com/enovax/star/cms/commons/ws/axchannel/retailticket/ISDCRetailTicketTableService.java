
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ISDC_RetailTicketTableService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ISDCRetailTicketTableService {


    /**
     * 
     * @param transactionId
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCRetailTicketTableResponse
     */
    @WebMethod(operationName = "RetailTicketSearch", action = "http://tempuri.org/ISDC_RetailTicketTableService/RetailTicketSearch")
    @WebResult(name = "RetailTicketSearchResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "RetailTicketSearch", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.RetailTicketSearch")
    @ResponseWrapper(localName = "RetailTicketSearchResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.RetailTicketSearchResponse")
    public SDCRetailTicketTableResponse retailTicketSearch(
        @WebParam(name = "transactionId", targetNamespace = "http://tempuri.org/")
        String transactionId);

    /**
     * 
     * @param cartTicketListCriteria
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartValidateQuantity", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartValidateQuantity")
    @WebResult(name = "CartValidateQuantityResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartValidateQuantity", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateQuantity")
    @ResponseWrapper(localName = "CartValidateQuantityResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateQuantityResponse")
    public SDCCartRetailTicketTableResponse cartValidateQuantity(
        @WebParam(name = "CartTicketListCriteria", targetNamespace = "http://tempuri.org/")
        ArrayOfSDCCartTicketListCriteria cartTicketListCriteria);

    /**
     * 
     * @param cartTicketListCriteria
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartCheckoutStartOnline", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartCheckoutStartOnline")
    @WebResult(name = "CartCheckoutStartOnlineResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartCheckoutStartOnline", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutStartOnline")
    @ResponseWrapper(localName = "CartCheckoutStartOnlineResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutStartOnlineResponse")
    public SDCCartRetailTicketTableResponse cartCheckoutStartOnline(
        @WebParam(name = "CartTicketListCriteria", targetNamespace = "http://tempuri.org/")
        ArrayOfSDCCartTicketListCriteria cartTicketListCriteria);

    /**
     * 
     * @param transactionId
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartCheckoutComplete", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartCheckoutComplete")
    @WebResult(name = "CartCheckoutCompleteResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartCheckoutComplete", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutComplete")
    @ResponseWrapper(localName = "CartCheckoutCompleteResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutCompleteResponse")
    public SDCCartRetailTicketTableResponse cartCheckoutComplete(
        @WebParam(name = "transactionId", targetNamespace = "http://tempuri.org/")
        String transactionId);

    /**
     * 
     * @param transactionId
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartCheckoutCancel", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartCheckoutCancel")
    @WebResult(name = "CartCheckoutCancelResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartCheckoutCancel", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutCancel")
    @ResponseWrapper(localName = "CartCheckoutCancelResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartCheckoutCancelResponse")
    public SDCCartRetailTicketTableResponse cartCheckoutCancel(
        @WebParam(name = "transactionId", targetNamespace = "http://tempuri.org/")
        String transactionId);

    /**
     * 
     * @param cartTicketListCriteria
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartValidateAndReserveQuantity", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartValidateAndReserveQuantity")
    @WebResult(name = "CartValidateAndReserveQuantityResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartValidateAndReserveQuantity", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateAndReserveQuantity")
    @ResponseWrapper(localName = "CartValidateAndReserveQuantityResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateAndReserveQuantityResponse")
    public SDCCartRetailTicketTableResponse cartValidateAndReserveQuantity(
        @WebParam(name = "CartTicketListCriteria", targetNamespace = "http://tempuri.org/")
        ArrayOfSDCCartTicketListCriteria cartTicketListCriteria);

    /**
     * 
     * @param cartTicketListCriteria
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCCartRetailTicketTableResponse
     */
    @WebMethod(operationName = "CartValidateAndReserveQuantityOnline", action = "http://tempuri.org/ISDC_RetailTicketTableService/CartValidateAndReserveQuantityOnline")
    @WebResult(name = "CartValidateAndReserveQuantityOnlineResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CartValidateAndReserveQuantityOnline", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateAndReserveQuantityOnline")
    @ResponseWrapper(localName = "CartValidateAndReserveQuantityOnlineResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.CartValidateAndReserveQuantityOnlineResponse")
    public SDCCartRetailTicketTableResponse cartValidateAndReserveQuantityOnline(
        @WebParam(name = "CartTicketListCriteria", targetNamespace = "http://tempuri.org/")
        ArrayOfSDCCartTicketListCriteria cartTicketListCriteria);

    /**
     * 
     * @param criteria
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCGetUpCrossSellResponse
     */
    @WebMethod(operationName = "GetUpCrossSell", action = "http://tempuri.org/ISDC_RetailTicketTableService/GetUpCrossSell")
    @WebResult(name = "GetUpCrossSellResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GetUpCrossSell", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.GetUpCrossSell")
    @ResponseWrapper(localName = "GetUpCrossSellResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.GetUpCrossSellResponse")
    public SDCGetUpCrossSellResponse getUpCrossSell(
        @WebParam(name = "criteria", targetNamespace = "http://tempuri.org/")
        UpCrossSellTableCriteria criteria);

    /**
     * 
     * @param discountCode
     * @param offerId
     * @param inventTransId
     * @param transactionId
     * @param isUpdate
     * @param status
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCUseDiscountCounterResponse
     */
    @WebMethod(operationName = "UseDiscountCounter", action = "http://tempuri.org/ISDC_RetailTicketTableService/UseDiscountCounter")
    @WebResult(name = "UseDiscountCounterResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "UseDiscountCounter", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.UseDiscountCounter")
    @ResponseWrapper(localName = "UseDiscountCounterResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.UseDiscountCounterResponse")
    public SDCUseDiscountCounterResponse useDiscountCounter(
        @WebParam(name = "offerId", targetNamespace = "http://tempuri.org/")
        String offerId,
        @WebParam(name = "discountCode", targetNamespace = "http://tempuri.org/")
        String discountCode,
        @WebParam(name = "transactionId", targetNamespace = "http://tempuri.org/")
        String transactionId,
        @WebParam(name = "inventTransId", targetNamespace = "http://tempuri.org/")
        String inventTransId,
        @WebParam(name = "status", targetNamespace = "http://tempuri.org/")
        Integer status,
        @WebParam(name = "isUpdate", targetNamespace = "http://tempuri.org/")
        Integer isUpdate);

    /**
     * 
     * @param discountCode
     * @param offerId
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCGetDiscountCounterResponse
     */
    @WebMethod(operationName = "GetDiscountCounter", action = "http://tempuri.org/ISDC_RetailTicketTableService/GetDiscountCounter")
    @WebResult(name = "GetDiscountCounterResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GetDiscountCounter", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.GetDiscountCounter")
    @ResponseWrapper(localName = "GetDiscountCounterResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.GetDiscountCounterResponse")
    public SDCGetDiscountCounterResponse getDiscountCounter(
        @WebParam(name = "offerId", targetNamespace = "http://tempuri.org/")
        String offerId,
        @WebParam(name = "discountCode", targetNamespace = "http://tempuri.org/")
        String discountCode);

    /**
     * 
     * @param transDate
     * @param transactionID
     * @return
     *     returns com.enovax.star.cms.commons.ws.axchannel.retailticket.SDCInsertOnlineReferenceResponse
     */
    @WebMethod(action = "http://tempuri.org/ISDC_RetailTicketTableService/insertOnlineReference")
    @WebResult(name = "insertOnlineReferenceResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "insertOnlineReference", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.InsertOnlineReference")
    @ResponseWrapper(localName = "insertOnlineReferenceResponse", targetNamespace = "http://tempuri.org/", className = "com.enovax.star.cms.commons.ws.axchannel.retailticket.InsertOnlineReferenceResponse")
    public SDCInsertOnlineReferenceResponse insertOnlineReference(
        @WebParam(name = "transactionID", targetNamespace = "http://tempuri.org/")
        String transactionID,
        @WebParam(name = "transDate", targetNamespace = "http://tempuri.org/")
        XMLGregorianCalendar transDate);

}
