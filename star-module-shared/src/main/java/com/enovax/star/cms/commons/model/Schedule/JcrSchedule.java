package com.enovax.star.cms.commons.model.Schedule;

import java.io.Serializable;

public class JcrSchedule implements Serializable {

    private String jcrName;

    private String name;

    private String startDate;

    private String endDate;

    private String kioskInfo;

    private String message;

    public String getJcrName() {
        return jcrName;
    }

    public void setJcrName(String jcrName) {
        this.jcrName = jcrName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getKioskInfo() {
        return kioskInfo;
    }

    public void setKioskInfo(String kioskInfo) {
        this.kioskInfo = kioskInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
