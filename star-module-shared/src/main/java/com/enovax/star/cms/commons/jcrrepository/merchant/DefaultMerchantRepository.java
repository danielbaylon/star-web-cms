package com.enovax.star.cms.commons.jcrrepository.merchant;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.mgnl.definition.MerchantProperties;
import com.enovax.star.cms.commons.model.merchant.Merchant;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 15/9/16.
 */
@Repository
public class DefaultMerchantRepository implements IMerchantRepository {

  public static final String WORKSPACE_NAME = "merchant";

  protected final Logger log = LoggerFactory.getLogger(DefaultMerchantRepository.class);

  @Override
  public List<Merchant> getAll(String channel, Boolean active, Boolean acceptsAllCards) {
    List<Merchant> merchants = new ArrayList<>();
    try {
      final Session session = getSession();
      final Node rootNode = session.getRootNode();
      if (rootNode.hasNode(channel)) {
        final Node channelNode = rootNode.getNode(channel);
        if (channelNode.hasNodes()) {
          final NodeIterator nodes = channelNode.getNodes();
          while (nodes.hasNext()) {
            final Node node = nodes.nextNode();
            final Merchant merchant = convertNodeToMerchant(node);
            boolean addToList = true;
            if (active != null) {
              if (active != merchant.isActive()) {
                addToList = false;
              }
            }
            if (addToList && acceptsAllCards != null) {
              if (acceptsAllCards != merchant.isAcceptsAllCards()) {
                addToList = false;
              }
            }
            if (addToList) {
              merchants.add(merchant);
            }
          }
        }
      }
    } catch (RepositoryException e) {
      log.error("Error retrieving merchant information.", e);
    }
    return merchants;
  }

  @Override
  public Merchant find(String channel, String id) {
    try {
      final Session session = getSession();
      final Node rootNode = session.getRootNode();
      final String relPath = channel + "/" + id;
      if (rootNode.hasNode(relPath)) {
        return convertNodeToMerchant(rootNode.getNode(relPath));
      }
    } catch (RepositoryException e) {
      log.error("Error retrieving merchant information.", e);
    }
    return null;
  }

  @Override
  public Merchant findByUuid(String uuid) {
    try {
      Node merchantNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.MerchantDefault.getWorkspaceName(), uuid);
      if (merchantNode != null) {
        return convertNodeToMerchant(merchantNode);
      }
    } catch (RepositoryException e) {
      log.error("Error retrieving merchant information.", e);
    }
    return null;
  }

  protected Merchant convertNodeToMerchant(Node node) throws RepositoryException {
    if (node != null) {
      Merchant merchant = new Merchant();
      merchant.setAcceptsAllCards(PropertyUtil.getBoolean(node, MerchantProperties.AcceptsAllCards.getPropertyName(), false));
      merchant.setActive(PropertyUtil.getBoolean(node, MerchantProperties.Active.getPropertyName(), false));
      merchant.setId(node.getIdentifier());
      merchant.setMerchantId(PropertyUtil.getString(node, MerchantProperties.MerchantId.getPropertyName(), ""));
      merchant.setName(PropertyUtil.getString(node, MerchantProperties.Name.getPropertyName(), ""));
      return merchant;
    }
    return null;
  }

  protected Session getSession() throws RepositoryException {
    return MgnlContext.getJCRSession(WORKSPACE_NAME);
  }
}
