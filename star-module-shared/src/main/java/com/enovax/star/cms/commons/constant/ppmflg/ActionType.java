package com.enovax.star.cms.commons.constant.ppmflg;

public enum ActionType {
    Create("Create","Create"),
    Update("Update","Update"),
    Resubmit("Resubmit","Resubmit"),
    Reject("Reject","Reject"),
    Approve("Approve","Approve");

    public final String code;
    public final String label;

    private ActionType(String code, String label) {
        this.code = code;
        this.label = label;
    }
    public static String getLabel(String code){
        for(ActionType tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
}
