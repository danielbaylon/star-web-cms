package com.enovax.star.cms.commons.service.ftp;

import com.enovax.star.cms.commons.exception.BizValidationException;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class FTPFileActionService implements FTPAction {

    private static final Logger log = LoggerFactory.getLogger(FTPFileActionService.class);

    @Override
    public void storeFile(FTPClient ftpClient, String localFilePath, String remoteFilePath, String trackingId) throws FileNotFoundException, BizValidationException {
        InputStream is = null;
        try{
            is = new FileInputStream(localFilePath);
            ftpClient.storeFile(remoteFilePath, is);
        }catch (Exception ex) {
            log.error(trackingId+" - uploading local file " + localFilePath + " to AX FTP directory " + remoteFilePath + " is failed : " + ex.getMessage(), ex);
            throw new BizValidationException("Uploading local file to AX is failed");
        }catch (Throwable ex2){
            log.error(trackingId+" - uploading local file " + localFilePath + " to AX FTP directory " + remoteFilePath + " is failed : " + ex2.getMessage(), ex2);
            throw new BizValidationException("Uploading local file to AX is failed");
        }finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception ex) {
                } catch (Throwable ex2) {
                }
            }
        }
    }

    @Override
    public void downloadFile(FTPClient ftpClient, String localFilePath, String remoteFilePath, String trackingId) throws FileNotFoundException, BizValidationException {
        if(localFilePath != null && localFilePath.trim().length() > 0 && remoteFilePath != null && remoteFilePath.trim().length() > 0 && ftpClient != null){
            OutputStream os = null;
            try{
                os = new FileOutputStream(localFilePath);
                ftpClient.retrieveFile(remoteFilePath, os);
            }catch (Exception ex) {
                log.error(trackingId+" - downloading file " + localFilePath + " from AX FTP  " + remoteFilePath + " is failed : " + ex.getMessage(), ex);
                throw new BizValidationException("Downloading file from AX is failed");
            }catch (Throwable ex2){
                log.error(trackingId+" - downloading local file " + localFilePath + " to AX FTP  " + remoteFilePath + " is failed : " + ex2.getMessage(), ex2);
                throw new BizValidationException("Downloading file from AX is failed");
            }finally {
                if (os != null) {
                    try {
                        os.flush();
                        os.close();
                    } catch (Exception ex) {
                    } catch (Throwable ex2) {
                    }
                }
            }
        }
    }
}
