package com.enovax.star.cms.commons.mgnl.contentapp;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by jennylynsze on 8/4/16.
 */
@StyleSheet({"vaadin://custom-app-style.css"})
public class StarMainSubAppViewImpl extends VerticalLayout implements StarMainSubAppView {

    private String display = "";
    private Label title;

    public StarMainSubAppViewImpl() {
        addStyleName("main-sub-app");
        setMargin(true);
        setSpacing(true);
        this.title = new Label(getDisplay());
        this.title.addStyleName("page-title");
        addComponent(this.title);
    }

    @Override
    public Component asVaadinComponent() {
        return this;
    }

    public String getDisplay() {
        return display;
    }

    @Override
    public void setDisplay(String display) {
        this.display = display;
        this.title.setValue(display);
    }
}
