package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;

@Entity
@Table(name="PPSLMTAPrevPasswordMain")
public class PPSLMTAPrevPasswordMain extends PPSLMPrevPassword {
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "mainAccountId",nullable = false)
    private PPSLMTAMainAccount user;
    

    
    
    public PPSLMTAMainAccount getUser() {
        return user;
    }
    public void setUser(PPSLMTAMainAccount user) {
        this.user = user;
    }
    
    
    
}
