package com.enovax.star.cms.commons.model.axchannel.inventtable;

import java.util.Date;

public class AxRetailProductExtEventAllocation {

    private Date eventDate;
    private String eventGroupId;
    private String eventLineId;

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }
}
