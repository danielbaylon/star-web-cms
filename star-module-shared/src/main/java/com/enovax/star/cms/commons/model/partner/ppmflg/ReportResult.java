package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 8/11/16.
 */
public class ReportResult<T> {
    private boolean isEmpty;
    private int totalCount;
    private List<T> rows = new ArrayList<T>();

    public boolean getIsEmpty() {
        return isEmpty;
    }
    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }
    public int getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
    public List<T> getRows() {
        return rows;
    }
    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
