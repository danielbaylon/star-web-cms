package com.enovax.star.cms.commons.tracking;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Created by jensen on 19/5/15.
 */
public class Trackd {

    protected String sid;
    protected Date accessDate;

    protected String host;
    protected String referrer;
    protected String userAgent;
    protected String acceptLang;
    protected String contextPath;
    protected String ip;
    protected String fullUrl;

    protected Map<String, String> headers;
    protected Map<String, String[]> params;
    protected Cookie[] cookies;
    protected HttpServletRequest req;
    protected String jsonRequest;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[START-REQUEST-LOG | SID: " + this.sid + "]\n")
                .append(">> URL : " + this.fullUrl + "\n")
                .append(">> IP : " + this.ip + "\n")
                .append(">> ACCESS_DATE : " + this.accessDate.toString() + "\n");

        if (cookies != null) {
            sb.append(">> COOKIES <<\n");
            for (Cookie cookie : cookies) {
                sb.append("[" + cookie.getName() + " : " + cookie.getValue() + " (Max Age:" + cookie.getMaxAge() + "]\n");
            }
        }

        if (!params.isEmpty()) {
            sb.append(">> PARAMETERS <<\n");
            for (String key : params.keySet()) {
                sb.append("[" + key + " : " + Arrays.toString(params.get(key)) + "]\n");
            }
        }

        sb.append(">> HEADERS <<\n");
        for (String key : headers.keySet()) {
            sb.append("[" + key + " : " + headers.get(key) + "]\n");
        }

        sb.append(">> JSON Params <<\n");
        sb.append(this.jsonRequest);
        sb.append("\n");

        sb.append("[END-REQUEST-LOG]");

        return sb.toString();
    }

    public String getSid() {
        return sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getReferrer() {
        return referrer;
    }
    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    public String getAcceptLang() {
        return acceptLang;
    }
    public void setAcceptLang(String acceptLang) {
        this.acceptLang = acceptLang;
    }
    public Map<String, String> getHeaders() {
        return headers;
    }
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
    public Map<String, String[]> getParams() {
        return params;
    }
    public void setParams(Map<String, String[]> params) {
        this.params = params;
    }
    public Cookie[] getCookies() {
        return cookies;
    }
    public void setCookies(Cookie[] cookies) {
        this.cookies = cookies;
    }
    public HttpServletRequest getReq() {
        return req;
    }
    public void setReq(HttpServletRequest req) {
        this.req = req;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getJsonRequest() {
        return jsonRequest;
    }

    public void setJsonRequest(String jsonRequest) {
        this.jsonRequest = jsonRequest;
    }
}
