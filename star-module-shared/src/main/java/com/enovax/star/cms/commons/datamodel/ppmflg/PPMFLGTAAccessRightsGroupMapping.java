package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPMFLGTAAccessRightsGroupMapping")
public class PPMFLGTAAccessRightsGroupMapping implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	
	@ManyToOne
	@JoinColumn(name = "accessRightId",nullable = false)
	private PPMFLGTAAccessRight accessRight;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPMFLGTAAccessRightsGroup accessRightsGroup;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PPMFLGTAAccessRight getAccessRight() {
		return accessRight;
	}

	public void setAccessRight(PPMFLGTAAccessRight accessRight) {
		this.accessRight = accessRight;
	}

	public PPMFLGTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}

	public void setAccessRightsGroup(PPMFLGTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}
	
	

}
