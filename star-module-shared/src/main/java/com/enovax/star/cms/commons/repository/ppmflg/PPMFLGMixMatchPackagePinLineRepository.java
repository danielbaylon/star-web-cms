package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackagePinLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 1/3/17.
 */
@Repository
public interface PPMFLGMixMatchPackagePinLineRepository extends JpaRepository<PPMFLGMixMatchPackagePinLine, Integer> {
    List<PPMFLGMixMatchPackagePinLine> findByPackageItemId(Integer packageItemId);
}
