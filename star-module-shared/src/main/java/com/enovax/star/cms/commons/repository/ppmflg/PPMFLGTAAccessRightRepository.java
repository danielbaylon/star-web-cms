package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccessRight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by jennylynsze on 1/12/17.
 */
public interface PPMFLGTAAccessRightRepository extends JpaRepository<PPMFLGTAAccessRight, Integer> {

    @Query(value = "select * from PPMFLGTAAccessRight e where e.id in( " +
            " select d.accessRightId from PPMFLGTAAccessRightsGroup c, PPMFLGTAAccessRightsGroupMapping d where c.id in( " +
            " select b.accessRightsGroupId from PPMFLGTAMainAccount a, PPMFLGTAMainRightsMapping b where b.mainAccountId=a.id and a.username= :username) " +
            "     and c.id = d.accessRightsGroupId) ", nativeQuery = true)
    List<PPMFLGTAAccessRight> getMainUserAccessRights(@Param("username") String username);


    @Query(value = "select * from PPMFLGTAAccessRight e where e.id in( " +
            " select d.accessRightId from PPMFLGTAAccessRightsGroup c, PPMFLGTAAccessRightsGroupMapping d where c.id in( " +
            " select b.accessRightsGroupId from PPMFLGTaSubAccount a, PPMFLGTARightsMapping b where b.subAccountId =a.id and a.username = :username) " +
            "     and c.id = d.accessRightsGroupId) ", nativeQuery = true)
    List<PPMFLGTAAccessRight> getSubUserAccessRights(@Param("username") String username);
}
