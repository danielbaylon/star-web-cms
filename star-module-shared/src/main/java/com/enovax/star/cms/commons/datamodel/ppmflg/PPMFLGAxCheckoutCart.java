package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

/**
 * Created by jennylynsze on 8/18/16.
 */
@Entity
@Table(name = "PPMFLGAxCheckoutCart")
public class PPMFLGAxCheckoutCart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "receiptNum")
    private String receiptNum;

    @Column(name = "checkoutCartJson")
    private String checkoutCartJson;

    @Column(name = "sessionCartJson")
    private String sessionCartJson;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCheckoutCartJson() {
        return checkoutCartJson;
    }

    public void setCheckoutCartJson(String checkoutCartJson) {
        this.checkoutCartJson = checkoutCartJson;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getSessionCartJson() {
        return sessionCartJson;
    }

    public void setSessionCartJson(String sessionCartJson) {
        this.sessionCartJson = sessionCartJson;
    }
}
