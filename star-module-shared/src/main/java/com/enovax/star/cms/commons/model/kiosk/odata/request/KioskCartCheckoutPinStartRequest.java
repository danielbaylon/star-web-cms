package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartPinTicketListCriteria;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartTransactionData;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskCartCheckoutPinStartRequest {

    /**
     * Collection(SDC_TicketingExtension.DataModel.CartTicketListCriteria)
     * 
     */
    @JsonProperty("CartTicketListCriteria")
    private List<AxCartPinTicketListCriteria> axCartTicketListCriterias = new ArrayList<>();

    /**
     * Collection(SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTransEntity)
     * 
     */
    @JsonProperty("PINRedemptionLines")
    private List<AxPINRedemptionStartTransactionData> axPINRedemptionStartTransactionList = new ArrayList<>();

    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("StoreId")
    private String storeId;

    @JsonProperty("TerminalId")
    private String terminalId;

    public List<AxCartPinTicketListCriteria> getAxCartTicketListCriterias() {
        return axCartTicketListCriterias;
    }

    public void setAxCartTicketListCriterias(List<AxCartPinTicketListCriteria> axCartTicketListCriterias) {
        this.axCartTicketListCriterias = axCartTicketListCriterias;
    }

    public List<AxPINRedemptionStartTransactionData> getAxPINRedemptionStartTransactionList() {
        return axPINRedemptionStartTransactionList;
    }

    public void setAxPINRedemptionStartTransactionList(List<AxPINRedemptionStartTransactionData> axPINRedemptionStartTransactionList) {
        this.axPINRedemptionStartTransactionList = axPINRedemptionStartTransactionList;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

}
