package com.enovax.star.cms.commons.mgnl.form.field.validator.definition;

import com.enovax.star.cms.commons.mgnl.form.field.validator.factory.ImageSizeValidatorFactory;
import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class ImageSizeValidatorDefinition extends ConfiguredFieldValidatorDefinition {
    private int width;
    private int height;
    private long size;

    public ImageSizeValidatorDefinition() {
        this.setFactoryClass(ImageSizeValidatorFactory.class);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
