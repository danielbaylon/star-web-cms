package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGPartnerExtRepository extends JpaRepository<PPMFLGPartnerExt, Integer>, JpaSpecificationExecutor<PPMFLGPartnerExt> {

    PPMFLGPartnerExt findFirstByPartnerIdAndAttrName(Integer id, String name);

    PPMFLGPartnerExt findFirstByPartnerIdAndAttrNameAndStatus(Integer id, String name, String code);

    List<PPMFLGPartnerExt> findByPartnerIdAndAttrNameInAndStatus(Integer id, List<String> name, String status);

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'updateExtensionFailed' and u.attrValue = 'Y' and u.status = 'A' ")
    List<PPMFLGPartnerExt> getAllUpdateFailedCustomer();

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'updateExtensionFailedTimes' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPMFLGPartnerExt> findByPartnerExtensionUpdateFailedTimes(Integer partnerId);

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'updateExtensionFailed' and u.attrValue = 'Y' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPMFLGPartnerExt> findReUpdateFailedExtensionByPartnerId(Integer id);

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'updateExtensionFailedTimes' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPMFLGPartnerExt> findReUpdateFailedTimesExtensionByPartnerId(Integer id);

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'offlinePaymentEnabled' and u.status = 'A' and u.partnerId = ?1 ")
    List<PPMFLGPartnerExt> findOfflinePaymentConfigByPartnerId(Integer id);

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'wotReservationCap' ")
    List<PPMFLGPartnerExt> finAllWingsOfTimeReservationCapList();

    @Query("select u from PPMFLGPartnerExt u where u.attrName = 'wotReservationValidUntil' ")
    List<PPMFLGPartnerExt> finAllWingsOfTimeReservationValidUntilList();

    @Query("select u from PPMFLGPartnerExt u where u.partnerId = ?1 ")
    List<PPMFLGPartnerExt> findAllPartnerExtByPartnerId(Integer id);

    @Query("select u from PPMFLGPartnerExt u where u.partnerId = ?1 and u.attrName = 'capacityGroupId' ")
    List<PPMFLGPartnerExt> findCapacityGroupIdByPartnerId(Integer id);


}
