package com.enovax.star.cms.commons.service.axcrt;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.*;

@SuppressWarnings({ "UnnecessaryLocalVariable", "Duplicates" })
@Service
public class AxStarService {

    private Logger log = LoggerFactory.getLogger(AxStarService.class);

    public static final String URL_FRAG_PRODUCT_GET = "Product"; // Get all Ax
                                                                 // Products
                                                                 // with valid
                                                                 // validity
    public static final String URL_FRAG_RETAIL_DISCOUNTS_GET = "RetailDiscounts";
    public static final String URL_FRAG_AFFILIATIONS_GET = "Affiliation";
    public static final String URL_FRAG_CART_GET = "ShoppingCart";
    public static final String URL_FRAG_CART_ADD = "ShoppingCart/Add";
    public static final String URL_FRAG_CART_UPDATE = "ShoppingCart/Update";
    public static final String URL_FRAG_CART_REMOVE = "ShoppingCart/Remove";
    public static final String URL_FRAG_CART_DELETE = "ShoppingCart/Delete";
    public static final String URL_FRAG_CHECKOUT = "Checkout";
    public static final String URL_FRAG_CART_ADD_PROMOTION_CODE = "ShoppingCart/PromotionCode/Add";
    public static final String URL_FRAG_CART_ADD_AFFILIATION = "ShoppingCart/Affiliation/Add";
    public static final String URL_FRAG_CART_REMOVE_AFFILIATION = "ShoppingCart/Affiliation/Remove";
    public static final String URL_FRAG_CHECKOUT_PAYMENT_CARD_TYPES = "Checkout/PaymentCardTypes";
    public static final String URL_FRAG_SALES_ORDER_CREATE = "SalesOrder/Create";
    public static final String URL_FRAG_CUSTOMER_GET = "Customer";
    public static final String URL_FRAG_CUSTOMER_CREATE = "Customer/Create";
    public static final String URL_FRAG_CUSTOMER_UPDATE = "Customer/Update";
    public static final String URL_FRAG_GET_SINGLE_ITEMS_PRICE = "GetItemPrice";
    public static final String URL_FRAG_GET_ITEMS_PRICE = "GetItemsPrice";
    public static final String URL_FRAG_GET_CUSTOMER_GROUP = "Customer/Groups";
    public static final String URL_FRAG_GET_COUNTRY_REGIONS = "CountryRegions";
    public static final String URL_FRAG_GET_PRODUCT_CATALOG = "ProductCatalog";

    public static final String RESP_ERROR_DESERIALISATION_ERROR_MESSAGE = "Unable to deserialise object.";
    public static final String RESP_ERROR_TYPE_DESERIALISE_ERROR = "AxStar.AxStarResponseError.UnableToDeserialise";
    public static final String RESP_ERROR_TYPE_DESERIALISE_NORMAL = "AxStar.NormalResult.UnableToDeserialise";
    public static final String RESP_ERROR_TYPE_REST_GENERAL_ERROR = "AxStar.RestError.General";

    private final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

    private final String apiUrlB2CMFLG = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.b2cmflg");
    private final String apiUrlB2CSLM = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.b2cslm");

    private final String apiUrlPARTNERPORTALSLM = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.ppslm");
    private final String apiUrlPARTNERPORTALMFLG = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.ppmflg");

    private String apiUrlKIOSKSLM = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.kioskslm");
    private String apiUrlKIOSKMFLG = Components.getComponent(MagnoliaConfigurationProperties.class).getProperty("star.axstar.api.url.kioskmflg");
    
    
    
    @Autowired
    private AxStarRestResponseHandler responseHandler;
    @Autowired
    private AxStarRestRequestHandler requestHandler;

    private Map<StoreApiChannels, String> channelUrls = new HashMap<>();

    @PostConstruct
    private void initService() {
        // final String defaultUrl =
        // "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50001/.Star/";
        final String defaultUrl = "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:55000/.Star/";
        // final String defaultUrl =
        // "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:35080/RetailServerWebApp";

        if (StringUtils.isEmpty(apiUrlB2CMFLG) || "${api.store.axstar.api.url.b2cmflg}".equals(apiUrlB2CMFLG)) {
            // apiUrlB2CMFLG = defaultUrl + "MFLG_B2C/";
        }
        if (StringUtils.isEmpty(apiUrlB2CSLM) || "${api.store.axstar.api.url.b2cslm}".equals(apiUrlB2CSLM)) {
            // apiUrlB2CSLM = defaultUrl + "SLM_B2C/";
            // apiUrlB2CSLM = defaultUrl + "StoreSdcB2C/";
        }
        // if (StringUtils.isEmpty(apiUrlKIOSK) ||
        // "${api.store.axstar.api.url.kiosk}".equals(apiUrlKIOSK)) {
        // apiUrlKIOSK = defaultUrl + "SLMKIOSK/";
        // apiUrlKIOSK = defaultUrl + "SDCOTC/v1/";
        // }
        // if (StringUtils.isEmpty(apiUrlPARTNERPORTAL) ||
        // "${api.store.axstar.api.url.partnerportal}".equals(apiUrlPARTNERPORTAL))
        // {
        // apiUrlPARTNERPORTAL = defaultUrl + "SLMB2B/";
        // apiUrlPARTNERPORTAL = defaultUrl + "StoreSdcB2B/";
        // }

        // final String defaultUrl =
        // "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50001/.Star/";
        // final String defaultUrl =
        // "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:55000/.Star/";
        // final String defaultUrl =
        // "http://ax2012r3-demo-sentosadev3-d2-a3aebdbed7751359.cloudapp.net:35080/RetailServerWebApp";

        // if (StringUtils.isEmpty(apiUrlB2CMFLG) ||
        // "${api.store.axstar.api.url.b2cmflg}".equals(apiUrlB2CMFLG)) {
        // apiUrlB2CMFLG = defaultUrl + "MFLG_B2C/";
        // }
        // if (StringUtils.isEmpty(apiUrlB2CSLM) ||
        // "${api.store.axstar.api.url.b2cslm}".equals(apiUrlB2CSLM)) {
        // apiUrlB2CSLM = defaultUrl + "SLM_B2C/";
        // apiUrlB2CSLM = defaultUrl + "StoreSdcB2C/";
        // }
        // if (StringUtils.isEmpty(apiUrlKIOSK) ||
        // "${api.store.axstar.api.url.kiosk}".equals(apiUrlKIOSK)) {
        // apiUrlKIOSK = defaultUrl + "SLMKIOSK/";
        // apiUrlKIOSK = defaultUrl + "SDCOTC/v1/";
        // }
        // if (StringUtils.isEmpty(apiUrlPARTNERPORTAL) ||
        // "${api.store.axstar.api.url.partnerportal}".equals(apiUrlPARTNERPORTAL))
        // {
        // apiUrlPARTNERPORTAL = defaultUrl + "SLMB2B/";
        // apiUrlPARTNERPORTAL = defaultUrl + "StoreSdcB2B/";
        // }

        channelUrls.put(StoreApiChannels.B2C_MFLG, apiUrlB2CMFLG);
        channelUrls.put(StoreApiChannels.B2C_SLM, apiUrlB2CSLM);

        channelUrls.put(StoreApiChannels.PARTNER_PORTAL_SLM, apiUrlPARTNERPORTALSLM);
        channelUrls.put(StoreApiChannels.PARTNER_PORTAL_MFLG, apiUrlPARTNERPORTALMFLG);
        
        channelUrls.put(StoreApiChannels.KIOSK_SLM, apiUrlKIOSKSLM);
        channelUrls.put(StoreApiChannels.KIOSK_MFLG, apiUrlKIOSKMFLG);
    }

    private boolean isNotFoundError(String toAnalyze) {
        return StringUtils.isNotEmpty(toAnalyze) && toAnalyze.contains("was not found.");
    }

    private HttpHeaders generateHeaders(StoreApiChannels channel) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public AxStarServiceResult<String> doRestExchangeString(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, String requestBodyJsonString) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyJsonString, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, String.class);
    }

    public <T> AxStarServiceResult<T> doRestExchangeString(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, String requestBodyJsonString,
            Class<T> expectedResponseClazz) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyJsonString, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, expectedResponseClazz);
    }

    public <T, U> AxStarServiceResult<T> doRestExchange(StoreApiChannels channel, String apiUrlFrag, HttpMethod requestHttpMethod, U requestObject,
            Class<T> expectedResponseClazz) {
        final String url = channelUrls.get(channel) + apiUrlFrag;
        final HttpEntity<U> requestEntity = new HttpEntity<>(requestObject, generateHeaders(channel));

        return doRestExchange(url, requestHttpMethod, requestEntity, expectedResponseClazz);
    }

    public <T> AxStarServiceResult<T> doRestExchange(String url, HttpMethod requestHttpMethod, HttpEntity<?> requestEntity, Class<T> expectedResponseClazz) {
        final RestTemplate client = new RestTemplate();
        client.setErrorHandler(new AxStarRestResponseErrorHandler());
        SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) client.getRequestFactory();
        rf.setOutputStreaming(Boolean.FALSE);
        rf.setReadTimeout(300 * 1000);// todo:
        rf.setConnectTimeout(300 * 1000);// todo:

        // Looks like this isn't the problem. (Setting property naming strategy
        // to upper camel case)
        // List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        // MappingJackson2HttpMessageConverter jsonMessageConverter = new
        // MappingJackson2HttpMessageConverter();
        // jsonMessageConverter.setObjectMapper(new
        // ObjectMapper().setPropertyNamingStrategy(new
        // PropertyNamingStrategy.UpperCamelCaseStrategy()));
        // messageConverters.add(jsonMessageConverter);
        // client.setMessageConverters(messageConverters);

        final AxStarServiceResult<T> result = new AxStarServiceResult<>();
        try {

            log.info("Calling REST API: " + url);
            final ResponseEntity<String> response = client.exchange(url, requestHttpMethod, requestEntity, String.class);
            final HttpStatus statusCode = response.getStatusCode();
            final String responseBody = response.getBody();

            log.info("Response Status: " + statusCode);
            log.info("Response Body [RAW]: " + responseBody);

            result.setRawData(responseBody);

            final boolean isError = statusCode.is4xxClientError() || statusCode.is5xxServerError();
            if (isError) {
                AxStarResponseError axStarResponseError;
                try {
                    axStarResponseError = responseHandler.handleResponse(responseBody, AxStarResponseError.class);
                } catch (Exception e) {
                    log.error("Error deserialising JSON.", e);
                    axStarResponseError = new AxStarResponseError();
                    axStarResponseError.setExceptionType(RESP_ERROR_TYPE_DESERIALISE_ERROR);
                    axStarResponseError.setExceptionMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setStackTrace("Contents of body: " + responseBody);
                }

                result.setSuccess(false);
                result.setError(axStarResponseError);
            } else {
                try {

                    if (expectedResponseClazz == String.class) {
                        result.setData((T) responseBody);
                    } else {
                        final T data = responseHandler.handleResponse(responseBody, expectedResponseClazz);
                        result.setData(data);
                    }

                    result.setSuccess(true);
                } catch (Exception e) {
                    log.error("Error deserialising JSON of normal API.", e);
                    final AxStarResponseError axStarResponseError = new AxStarResponseError();
                    axStarResponseError.setExceptionType(RESP_ERROR_TYPE_DESERIALISE_NORMAL);
                    axStarResponseError.setExceptionMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setMessage(RESP_ERROR_DESERIALISATION_ERROR_MESSAGE);
                    axStarResponseError.setStackTrace("Contents of body: " + responseBody);

                    result.setSuccess(false);
                    result.setError(axStarResponseError);
                }
            }
        } catch (Exception e) {
            final String exMsg = "Exception encountered when trying to process a REST API exchange. MSG: " + e.getMessage();
            log.error(exMsg, e);
            final AxStarResponseError axStarResponseError = new AxStarResponseError();
            axStarResponseError.setExceptionType(RESP_ERROR_TYPE_REST_GENERAL_ERROR);
            axStarResponseError.setExceptionMessage(exMsg);
            axStarResponseError.setMessage(exMsg);
            axStarResponseError.setStackTrace("See logs.");

            result.setSuccess(false);
            result.setError(axStarResponseError);
        }

        return result;
    }

    public AxStarServiceResult<List<AxStarAffiliation>> apiAffiliationsGet(StoreApiChannels channel) {
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_AFFILIATIONS_GET, HttpMethod.GET, null, String.class);
        log.info("apiAffiliationsGet => " + JsonUtil.jsonify(result));

        final List<AxStarAffiliation> theList;
        if (result.isSuccess()) {
            final Type affiliationListType = new TypeToken<ArrayList<AxStarAffiliation>>() {
            }.getType();
            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
            theList = gson.fromJson(result.getData(), affiliationListType);
        } else {
            theList = new ArrayList<>();
        }

        final AxStarServiceResult<List<AxStarAffiliation>> affiliationResult = new AxStarServiceResult<>();
        affiliationResult.setSuccess(result.isSuccess());
        affiliationResult.setData(theList);
        affiliationResult.setRawData(result.getRawData());
        affiliationResult.setError(result.getError());

        return affiliationResult;
    }

    public AxStarServiceResult<List<AxStarRetailDiscount>> apiRetailDiscountsGet(StoreApiChannels channel) {
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_RETAIL_DISCOUNTS_GET, HttpMethod.GET, null, String.class);
        log.info("apiRetailDiscountsGet => " + JsonUtil.jsonify(result));

        final List<AxStarRetailDiscount> theList;
        if (result.isSuccess()) {
            final Type retailDiscountsListType = new TypeToken<ArrayList<AxStarRetailDiscount>>() {
            }.getType();
            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
            theList = gson.fromJson(result.getData(), retailDiscountsListType);
        } else {
            theList = new ArrayList<>();
        }

        final AxStarServiceResult<List<AxStarRetailDiscount>> discountsResult = new AxStarServiceResult<>();
        discountsResult.setSuccess(result.isSuccess());
        discountsResult.setData(theList);
        discountsResult.setRawData(result.getRawData());
        discountsResult.setError(result.getError());

        return discountsResult;
    }

    public AxStarServiceResult<List<AxStarProduct>> apiProductsGet(StoreApiChannels channel) {
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_PRODUCT_GET, HttpMethod.GET, null, String.class);
        log.info("apiProductsGet => " + JsonUtil.jsonify(result));

        final List<AxStarProduct> theList;
        if (result.isSuccess()) {
            final Type productListType = new TypeToken<ArrayList<AxStarProduct>>() {
            }.getType();
            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
            theList = gson.fromJson(result.getData(), productListType);
        } else {
            theList = new ArrayList<>();
        }

        final AxStarServiceResult<List<AxStarProduct>> productsResult = new AxStarServiceResult<>();
        productsResult.setSuccess(result.isSuccess());
        productsResult.setData(theList);
        productsResult.setRawData(result.getRawData());
        productsResult.setError(result.getError());

        return productsResult;
    }

    public AxStarServiceResult<Map<String, AxStarProductCatalog>> apiProductCatalogGet(StoreApiChannels channel) {

        AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_GET_PRODUCT_CATALOG, HttpMethod.GET, null, String.class);

        log.info("apiProductCatalogGet response ==> " + JsonUtil.jsonify(result));

        Map<String, AxStarProductCatalog> productCatalogMap = new HashMap<String, AxStarProductCatalog>();
        if (result.isSuccess()) {

            JsonParser parser = new JsonParser();
            JsonArray array = (JsonArray) parser.parse(result.getRawData());

            for (JsonElement element : array) {
                for (Map.Entry<String, JsonElement> entry : element.getAsJsonObject().entrySet()) {
                    String productId = entry.getKey();

                    Type type = new TypeToken<List<AxStarProductCatalog>>() {
                    }.getType();
                    List<AxStarProductCatalog> data = this.gson.fromJson(entry.getValue().toString(), type);
                    if (data != null && data.size() > 0) {
                        productCatalogMap.put(productId, data.get(0));
                    }
                }
            }

        }

        final AxStarServiceResult<Map<String, AxStarProductCatalog>> retResult = new AxStarServiceResult<>();
        retResult.setSuccess(result.isSuccess());
        retResult.setData(productCatalogMap);
        retResult.setRawData(result.getRawData());
        retResult.setError(result.getError());

        return retResult;

    }

    public AxStarServiceResult<AxStarCartNested> apiCartGet(StoreApiChannels channel, String cartId, String customerId) {
        final AxStarInputAddCart cart = new AxStarInputAddCart();
        cart.setCartId(cartId);
        cart.setCustomerId(customerId);

        log.info("apiCartGet [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<AxStarCartNested> result = doRestExchange(channel, URL_FRAG_CART_GET, HttpMethod.POST, cart, AxStarCartNested.class);
        log.info("apiCartGet [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<AxStarCartNested> apiCartAddItem(StoreApiChannels channel, AxStarInputAddCart cart) {
        log.info("apiCartAddItem [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<AxStarCartNested> result = doRestExchange(channel, URL_FRAG_CART_ADD, HttpMethod.POST, cart, AxStarCartNested.class);
        log.info("apiCartAddItem [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<AxStarCartNested> apiCartUpdateItem(StoreApiChannels channel, AxStarInputUpdateCart cart) {
        log.info("apiCartUpdateItem [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<AxStarCartNested> result = doRestExchange(channel, URL_FRAG_CART_UPDATE, HttpMethod.POST, cart, AxStarCartNested.class);
        log.info("apiCartUpdateItem [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<AxStarCartNested> apiCartRemoveItem(StoreApiChannels channel, AxStarInputRemoveCart cart) {
        log.info("apiCartRemoveItem [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<AxStarCartNested> result = doRestExchange(channel, URL_FRAG_CART_REMOVE, HttpMethod.POST, cart, AxStarCartNested.class);
        log.info("apiCartRemoveItem [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<String> apiCartApplyPromoCode(StoreApiChannels channel, String cartId, String customerId, String promoCode) {
        final AxStarInputAddPromoCode cart = new AxStarInputAddPromoCode();
        cart.setCartId(cartId);
        cart.setCustomerId(customerId);
        cart.setPromotionCode(promoCode);

        log.info("apiCartApplyPromoCode [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_CART_ADD_PROMOTION_CODE, HttpMethod.POST, cart, String.class);
        log.info("apiCartApplyPromoCode [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<String> apiCartAddAffiliation(StoreApiChannels channel, String cartId, String customerId, List<Long> affiliationIds) {
        final AxStarInputAffiliation affInput = new AxStarInputAffiliation();
        affInput.setAffiliatioIds(affiliationIds);
        affInput.setCartId(cartId);
        affInput.setCustomerId(customerId);

        log.info("apiCartAddAffiliation [Request] => " + JsonUtil.jsonify(affInput));
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_CART_ADD_AFFILIATION, HttpMethod.POST, affInput, String.class);
        log.info("apiCartAddAffiliation [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<String> apiCartRemoveAffiliation(StoreApiChannels channel, String cartId, String customerId, List<Long> affiliationIds) {
        final AxStarInputAffiliation affInput = new AxStarInputAffiliation();
        affInput.setAffiliatioIds(affiliationIds);
        affInput.setCartId(cartId);
        affInput.setCustomerId(customerId);

        log.info("apiCartRemoveAffiliation [Request] => " + JsonUtil.jsonify(affInput));
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_CART_REMOVE_AFFILIATION, HttpMethod.POST, affInput, String.class);
        log.info("apiCartRemoveAffiliation [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<String> apiCartDeleteEntireCart(StoreApiChannels channel, String cartId, String customerId) {
        final AxStarInputAddCart cart = new AxStarInputAddCart();
        cart.setCartId(cartId);
        cart.setCustomerId(customerId);

        log.info("apiCartDeleteEntireCart [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_CART_DELETE, HttpMethod.POST, cart, String.class);
        log.info("apiCartDeleteEntireCart [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<AxStarCartNested> apiCheckout(StoreApiChannels channel, String cartId, String customerId, String salesOrderNumber) {
        final AxStarInputAddCart cart = new AxStarInputAddCart();
        cart.setCartId(cartId);
        cart.setCustomerId(customerId);
        cart.setSalesOrderNumber(salesOrderNumber);

        log.info("apiCheckout [Request] => " + JsonUtil.jsonify(cart));
        final AxStarServiceResult<AxStarCartNested> result = doRestExchange(channel, URL_FRAG_CHECKOUT, HttpMethod.POST, cart, AxStarCartNested.class);
        log.info("apiCheckout [Response] => " + JsonUtil.jsonify(result));

        return result;
    }


    /*
    NOTE: If there's a sales order create, you have to make sure you call the sales order confirm (insertOnlineReference) from NEC once everything is done!!!!!
     */
    public AxStarServiceResult<AxStarSalesOrder> apiSalesOrderCreate(StoreApiChannels channel, AxStarInputSalesOrderCreation soCreator) {
        log.info("apiSalesOrderCreate [Request] => " + JsonUtil.jsonify(soCreator));
        final AxStarServiceResult<AxStarSalesOrder> result = doRestExchange(channel, URL_FRAG_SALES_ORDER_CREATE, HttpMethod.POST, soCreator, AxStarSalesOrder.class);
        log.info("apiSalesOrderCreate [Response] => " + JsonUtil.jsonify(result));

        return result;
    }

    public AxStarServiceResult<AxStarCustomer> apiCustomerGet(StoreApiChannels channel, String customerId) {
        String url = channelUrls.get(channel) + URL_FRAG_CUSTOMER_GET + "/" + customerId;
        log.info("apiCustomerGet request ==> " + url);
        AxStarServiceResult<AxStarCustomer> result = doRestExchange(channel, URL_FRAG_CUSTOMER_GET + "/" + customerId, HttpMethod.GET, null, AxStarCustomer.class);
        log.info("apiCustomerGet response ==> " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<AxStarCustomer> apiCustomerCreate(StoreApiChannels channel, AxStarCustomer customer) {
        String url = channelUrls.get(channel) + URL_FRAG_CUSTOMER_CREATE;
        log.info("apiCustomerCreate request ==> " + url);
        AxStarServiceResult<AxStarCustomer> result = doRestExchangeString(channel, URL_FRAG_CUSTOMER_CREATE, HttpMethod.POST, requestHandler.parseObjectToString(customer),
                AxStarCustomer.class);
        log.info("apiCustomerCreate response ==> " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<AxStarCustomer> apiCustomerUpdate(StoreApiChannels channel, AxStarCustomer customer) {
        String url = channelUrls.get(channel) + URL_FRAG_CUSTOMER_UPDATE;
        log.info("apiCustomerUpdate request ==> " + url);
        AxStarServiceResult<AxStarCustomer> result = doRestExchange(channel, URL_FRAG_CUSTOMER_UPDATE, HttpMethod.POST, customer, AxStarCustomer.class);
        log.info("apiCustomerUpdate response ==> " + JsonUtil.jsonify(result));
        return result;
    }

//    public AxStarServiceResult<List<AxStarCountryRegionInfo>> apiCountryRegionsGet(StoreApiChannels channel) {
//        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_GET_COUNTRY_REGIONS, HttpMethod.GET, null, String.class);
//        log.info("apiCountryRegionsGet => " + JsonUtil.jsonify(result));
//
//        final List<AxStarCountryRegionInfo> theList;
//        if (result.isSuccess()) {
//            final Type countryRegionListType = new TypeToken<ArrayList<AxStarCountryRegionInfo>>() {
//            }.getType();
//            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
//            theList = gson.fromJson(result.getData(), countryRegionListType);
//        } else {
//            theList = new ArrayList<>();
//        }
//
//        final AxStarServiceResult<List<AxStarCountryRegionInfo>> retResult = new AxStarServiceResult<>();
//        retResult.setSuccess(result.isSuccess());
//        retResult.setData(theList);
//        retResult.setRawData(result.getRawData());
//        retResult.setError(result.getError());
//
//        return retResult;
//    }

    public AxStarServiceResult<List<AxCustomerGroup>> apiCustomerGroupGet(StoreApiChannels channel) {
        final AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_GET_CUSTOMER_GROUP, HttpMethod.GET, null, String.class);
        log.info("apiCustomerGroupGet => " + JsonUtil.jsonify(result));

        final List<AxCustomerGroup> theList;
        if (result.isSuccess()) {
            final Type customerGroupType = new TypeToken<ArrayList<AxCustomerGroup>>() {
            }.getType();
            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
            theList = gson.fromJson(result.getData(), customerGroupType);
        } else {
            theList = new ArrayList<>();
        }

        final AxStarServiceResult<List<AxCustomerGroup>> retResult = new AxStarServiceResult<>();
        retResult.setSuccess(result.isSuccess());
        retResult.setData(theList);
        retResult.setRawData(result.getRawData());
        retResult.setError(result.getError());

        return retResult;
    }

    /*
     * if no prdouctId's pass, it will return all the list
     */
    public AxStarServiceResult<AxProductPriceMap> apiGetItemsPriceByCustomerId(StoreApiChannels channel, String axAccountNumber, Long[] productIds) {
        if (axAccountNumber == null || axAccountNumber.trim().length() == 0) {
            return null;
        }

        if (productIds == null) {
            productIds = new Long[0];
        }

        String url = channelUrls.get(channel) + URL_FRAG_GET_ITEMS_PRICE + "/" + axAccountNumber;
        log.info("apiGetItemsPriceByCustomerId request ==> " + url);
        AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_GET_ITEMS_PRICE + "/" + axAccountNumber, HttpMethod.POST, productIds, String.class);
        log.info("apiGetItemsPriceByCustomerId response ==> " + JsonUtil.jsonify(result));

        final AxProductPriceMap productPriceMap = new AxProductPriceMap();
        if (result.isSuccess()) {
            final Type productPriceType = new TypeToken<HashMap<String, List<AxProductPrice>>>() {
            }.getType();
            final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
            productPriceMap.setProductPriceMap(gson.fromJson(result.getData(), productPriceType));
        }

        final AxStarServiceResult<AxProductPriceMap> retResult = new AxStarServiceResult<>();
        retResult.setSuccess(result.isSuccess());
        retResult.setData(productPriceMap);
        retResult.setRawData(result.getRawData());
        retResult.setError(result.getError());

        return retResult;
    }

    public AxStarServiceResult<String> apiGetSingleItemPriceByCustomerId(StoreApiChannels channel, String axAccountNumber, String prodId) {
        if (axAccountNumber == null || axAccountNumber.trim().length() == 0 || prodId == null || prodId.trim().length() == 0) {
            return null;
        }
        String url = channelUrls.get(channel) + URL_FRAG_GET_SINGLE_ITEMS_PRICE + "/" + axAccountNumber + "/" + prodId;
        log.info("apiGetSingleItemPriceByCustomerId request ==> " + url);
        AxStarServiceResult<String> result = doRestExchangeString(channel, URL_FRAG_GET_SINGLE_ITEMS_PRICE + "/" + axAccountNumber + "/" + prodId, HttpMethod.GET, null);
        log.info("apiGetSingleItemPriceByCustomerId response ==> " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<String> apiGetSingleItemPrice(StoreApiChannels channel, String prodId) {
        if (prodId == null || prodId.trim().length() == 0) {
            return null;
        }
        String url = channelUrls.get(channel) + URL_FRAG_GET_SINGLE_ITEMS_PRICE + "/" + prodId;
        log.info("apiGetSingleItemPrice request ==> " + url);
        AxStarServiceResult<String> result = doRestExchangeString(channel, URL_FRAG_GET_SINGLE_ITEMS_PRICE + "/" + prodId, HttpMethod.GET, null);
        log.info("apiGetSingleItemPrice response ==> " + JsonUtil.jsonify(result));
        return result;
    }

    public AxStarServiceResult<String> apiGetItemsPrice(StoreApiChannels channel, Long[] productIds) {
        if (productIds == null || productIds.length == 0) {
            return null;
        }
        String url = channelUrls.get(channel) + URL_FRAG_GET_ITEMS_PRICE;
        log.info("apiGetItemsPrice request ==> " + url);
        AxStarServiceResult<String> result = doRestExchange(channel, URL_FRAG_GET_ITEMS_PRICE, HttpMethod.POST, productIds, String.class);
        log.info("apiGetItemsPrice response ==> " + JsonUtil.jsonify(result));
        return result;
    }

}
