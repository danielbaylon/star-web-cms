package com.enovax.star.cms.commons.constant;

/**
 * Created by jennylynsze on 10/21/16.
 */
public class AxProductPropertiesKey {

    public static final String TICKET_TYPE_KEY_JCR_PATH = "/ax-product-properties-key/ticket-type";
    public static final String PUBLISH_PRICE_KEY_JCR_PATH = "/ax-product-properties-key/publish-price";

    public static final String TICKET_TYPE = "1TicketType";
    public static final String PUBLISHED_PRICE = "2PublishPrice";

    public static final String TICKET_TYPE_CHILD = "Child";
    public static final String TICKET_TYPE_ADULT = "Adult";
    public static final String TICKET_TYPE_STANDARD = "Standard";

}
