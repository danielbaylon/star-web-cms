package com.enovax.star.cms.commons.model.booking.promotions;

import java.io.Serializable;

/**
 * Created by jace on 9/9/16.
 */
public class TicketUpdateModel implements Serializable {

  private long productListingId;

  private String promotionDisplayName;
  private String promotionIconImageURL;
  private String promotionText;
  private String additionalInfo;
  private String cachedDiscountPrice;

  public TicketUpdateModel() {

  }

  public long getProductListingId() {
    return productListingId;
  }

  public void setProductListingId(long productListingId) {
    this.productListingId = productListingId;
  }

  public String getPromotionDisplayName() {
    return promotionDisplayName;
  }

  public void setPromotionDisplayName(String promotionDisplayName) {
    this.promotionDisplayName = promotionDisplayName;
  }

  public String getPromotionIconImageURL() {
    return promotionIconImageURL;
  }

  public void setPromotionIconImageURL(String promotionIconImageURL) {
    this.promotionIconImageURL = promotionIconImageURL;
  }

  public String getPromotionText() {
    return promotionText;
  }

  public void setPromotionText(String promotionText) {
    this.promotionText = promotionText;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getCachedDiscountPrice() {
    return cachedDiscountPrice;
  }

  public void setCachedDiscountPrice(String cachedDiscountPrice) {
    this.cachedDiscountPrice = cachedDiscountPrice;
  }
}
