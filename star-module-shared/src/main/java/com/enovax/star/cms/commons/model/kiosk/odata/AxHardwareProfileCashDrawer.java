package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * 
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.HardwareProfileCashDrawer
 * 
 * @author Justin
 * @since 15 SEP 16
 *
 */
public class AxHardwareProfileCashDrawer {

    private String profileId;

    private String deviceTypeValue;

    private String deviceName;

    private String deviceDescription;

    private String deviceMake;

    private String deviceModel;

    private String useCashDrawerPool;

    private String cashDrawerPoolId;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getDeviceTypeValue() {
        return deviceTypeValue;
    }

    public void setDeviceTypeValue(String deviceTypeValue) {
        this.deviceTypeValue = deviceTypeValue;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public String getDeviceMake() {
        return deviceMake;
    }

    public void setDeviceMake(String deviceMake) {
        this.deviceMake = deviceMake;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getUseCashDrawerPool() {
        return useCashDrawerPool;
    }

    public void setUseCashDrawerPool(String useCashDrawerPool) {
        this.useCashDrawerPool = useCashDrawerPool;
    }

    public String getCashDrawerPoolId() {
        return cashDrawerPoolId;
    }

    public void setCashDrawerPoolId(String cashDrawerPoolId) {
        this.cashDrawerPoolId = cashDrawerPoolId;
    }

}
