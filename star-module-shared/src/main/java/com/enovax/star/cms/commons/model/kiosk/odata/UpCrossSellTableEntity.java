package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.ArrayList;
import java.util.List;

public class UpCrossSellTableEntity {
    
    private String tABLEALL;
    
    private String cHANNEL;
    
    private String mINIMUMQTY;
    
    private String mULTIPLEQTY;
    
    private String aCTIVE;
    
    private String iTEMID;
    
    private String uNITIDId;
    
    private String rECID;
    
    private List<UpCrossSellLineEntity> upCrossSellLineList = new ArrayList<>();
    public String gettABLEALL() {
        return tABLEALL;
    }
    public void settABLEALL(String tABLEALL) {
        this.tABLEALL = tABLEALL;
    }
    public String getcHANNEL() {
        return cHANNEL;
    }
    public void setcHANNEL(String cHANNEL) {
        this.cHANNEL = cHANNEL;
    }
    public String getmINIMUMQTY() {
        return mINIMUMQTY;
    }
    public void setmINIMUMQTY(String mINIMUMQTY) {
        this.mINIMUMQTY = mINIMUMQTY;
    }
    public String getmULTIPLEQTY() {
        return mULTIPLEQTY;
    }
    public void setmULTIPLEQTY(String mULTIPLEQTY) {
        this.mULTIPLEQTY = mULTIPLEQTY;
    }
    public String getaCTIVE() {
        return aCTIVE;
    }
    public void setaCTIVE(String aCTIVE) {
        this.aCTIVE = aCTIVE;
    }
    public String getiTEMID() {
        return iTEMID;
    }
    public void setiTEMID(String iTEMID) {
        this.iTEMID = iTEMID;
    }
    public String getuNITIDId() {
        return uNITIDId;
    }
    public void setuNITIDId(String uNITIDId) {
        this.uNITIDId = uNITIDId;
    }
    public String getrECID() {
        return rECID;
    }
    public void setrECID(String rECID) {
        this.rECID = rECID;
    }
    public List<UpCrossSellLineEntity> getUpCrossSellLineList() {
        return upCrossSellLineList;
    }
    public void setUpCrossSellLineList(List<UpCrossSellLineEntity> upCrossSellLineList) {
        this.upCrossSellLineList = upCrossSellLineList;
    }
    
}
