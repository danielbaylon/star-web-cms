package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.PkgUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class PkgVM implements Serializable {
    private static final long serialVersionUID = 1L;

    private String pkgName;
    private List<PkgBundledItemVM> bundledItems;
    private String pkgExpiryDateStr;
    private Integer pkgQtyPerProd;
    private String pkgTktType;
    private String pkgDesc;
    private String pkgTktMedia;
    private String pkgTktMedialAllowed;

    private Integer itemIdToAdd;
    private String transItemIds; //added new field
//
    public PkgVM() {

    }

    public PkgVM(List<PPSLMInventoryTransactionItem> itransItems, String ipkgName,
                 Integer ipkgQtyPerProd, String ipkgTktType,String pkgDesc, String pkgTktMedia, String pkgTktMedialAllowed) {
        this.pkgName = ipkgName;
        this.bundledItems = new ArrayList<PkgBundledItemVM>();
        this.pkgTktType = ipkgTktType;
        this.pkgQtyPerProd = ipkgQtyPerProd;
        for (PPSLMInventoryTransactionItem item : itransItems) {
            PkgBundledItemVM bundledItem = new PkgBundledItemVM(item);
            if (!bundledItems.contains(bundledItem)) {
                bundledItem.addTransItem(itransItems, ipkgQtyPerProd,
                        ipkgTktType);
                bundledItems.add(bundledItem);
            }
        }
        List<PPSLMInventoryTransactionItem> allItems = PkgUtil
                .getAvailableListUnderPkg(itransItems, ipkgQtyPerProd,
                        ipkgTktType, false);
        Date latestExpireDt = PkgUtil.getLatestExpiringDate(allItems);
        pkgExpiryDateStr = (latestExpireDt==null)?"":NvxDateUtils.formatDate(latestExpireDt,
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);

        this.pkgQtyPerProd = ipkgQtyPerProd;
        this.pkgDesc = pkgDesc;
        this.pkgTktMedia = pkgTktMedia;
        this.pkgTktMedialAllowed = pkgTktMedialAllowed;
    }

    public List<PkgBundledItemVM> getBundledItems() {
        return bundledItems;
    }

    public void setBundledItems(List<PkgBundledItemVM> bundledItems) {
        this.bundledItems = bundledItems;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getPkgExpiryDateStr() {
        return pkgExpiryDateStr;
    }

    public void setPkgExpiryDateStr(String pkgExpiryDateStr) {
        this.pkgExpiryDateStr = pkgExpiryDateStr;
    }

    public Integer getPkgQtyPerProd() {
        return pkgQtyPerProd;
    }

    public void setPkgQtyPerProd(Integer pkgQtyPerProd) {
        this.pkgQtyPerProd = pkgQtyPerProd;
    }


    public String getPkgTktType() {
        return pkgTktType;
    }

    public void setPkgTktType(String pkgTktType) {
        this.pkgTktType = pkgTktType;
    }

    public String getPkgDesc() {
        return pkgDesc;
    }

    public void setPkgDesc(String pkgDesc) {
        this.pkgDesc = pkgDesc;
    }


    public Integer getItemIdToAdd() {
        return itemIdToAdd;
    }

    public void setItemIdToAdd(Integer itemIdToAdd) {
        this.itemIdToAdd = itemIdToAdd;
    }

    public String getTransItemIds() {
        return transItemIds;
    }

    public void setTransItemIds(String transItemIds) {
        this.transItemIds = transItemIds;
    }

    public String getPkgTktMedia() {
        return pkgTktMedia;
    }

    public void setPkgTktMedia(String pkgTktMedia) {
        this.pkgTktMedia = pkgTktMedia;
    }

    public String getPkgTktMedialAllowed() {
        return pkgTktMedialAllowed;
    }

    public void setPkgTktMedialAllowed(String pkgTktMedialAllowed) {
        this.pkgTktMedialAllowed = pkgTktMedialAllowed;
    }
}
