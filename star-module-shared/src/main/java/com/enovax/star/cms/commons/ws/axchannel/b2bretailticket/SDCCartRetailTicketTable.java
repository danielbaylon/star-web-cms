
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for SDC_CartRetailTicketTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CartRetailTicketTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CartRetailTicketPINTableEntityCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_CartRetailTicketPINItem" minOccurs="0"/>
 *         &lt;element name="CartRetailTicketTableEntityCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_CartRetailTicketItem" minOccurs="0"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorStatus" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PaxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CartRetailTicketTable", propOrder = {
    "cartRetailTicketPINTableEntityCollection",
    "cartRetailTicketTableEntityCollection",
    "errorMessage",
    "errorStatus",
    "paxId",
    "transactionId"
})
public class SDCCartRetailTicketTable {

    @XmlElementRef(name = "CartRetailTicketPINTableEntityCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCartRetailTicketPINItem> cartRetailTicketPINTableEntityCollection;
    @XmlElementRef(name = "CartRetailTicketTableEntityCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCartRetailTicketItem> cartRetailTicketTableEntityCollection;
    @XmlElementRef(name = "ErrorMessage", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> errorMessage;
    @XmlElement(name = "ErrorStatus")
    protected Integer errorStatus;
    @XmlElementRef(name = "PaxId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paxId;
    @XmlElementRef(name = "TransactionId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionId;

    /**
     * Gets the value of the cartRetailTicketPINTableEntityCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCartRetailTicketPINItem> getCartRetailTicketPINTableEntityCollection() {
        return cartRetailTicketPINTableEntityCollection;
    }

    /**
     * Sets the value of the cartRetailTicketPINTableEntityCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}
     *     
     */
    public void setCartRetailTicketPINTableEntityCollection(JAXBElement<ArrayOfSDCCartRetailTicketPINItem> value) {
        this.cartRetailTicketPINTableEntityCollection = value;
    }

    /**
     * Gets the value of the cartRetailTicketTableEntityCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> getCartRetailTicketTableEntityCollection() {
        return cartRetailTicketTableEntityCollection;
    }

    /**
     * Sets the value of the cartRetailTicketTableEntityCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}
     *     
     */
    public void setCartRetailTicketTableEntityCollection(JAXBElement<ArrayOfSDCCartRetailTicketItem> value) {
        this.cartRetailTicketTableEntityCollection = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErrorMessage(JAXBElement<String> value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the errorStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getErrorStatus() {
        return errorStatus;
    }

    /**
     * Sets the value of the errorStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setErrorStatus(Integer value) {
        this.errorStatus = value;
    }

    /**
     * Gets the value of the paxId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaxId() {
        return paxId;
    }

    /**
     * Sets the value of the paxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaxId(JAXBElement<String> value) {
        this.paxId = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransactionId(JAXBElement<String> value) {
        this.transactionId = value;
    }

}
