
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_CartRetailTicketPINItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CartRetailTicketPINItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChannelID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CustAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAreaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PRODUCTPINCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductPINCodePackage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QtyGroup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="REFERENCEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SDC_CartRetailTicketPINLineCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_CartRetailTicketPINLineItem" minOccurs="0"/>
 *         &lt;element name="SDC_FacilityCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_Facility" minOccurs="0"/>
 *         &lt;element name="StartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CartRetailTicketPINItem", propOrder = {
    "channelID",
    "custAccount",
    "dataAreaId",
    "endDateTime",
    "itemId",
    "productpincode",
    "paxId",
    "productName",
    "productPINCodePackage",
    "qtyGroup",
    "referenceid",
    "sdcCartRetailTicketPINLineCollection",
    "sdcFacilityCollection",
    "startDateTime"
})
public class SDCCartRetailTicketPINItem {

    @XmlElement(name = "ChannelID")
    protected Long channelID;
    @XmlElementRef(name = "CustAccount", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custAccount;
    @XmlElementRef(name = "DataAreaId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataAreaId;
    @XmlElement(name = "EndDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDateTime;
    @XmlElementRef(name = "ItemId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemId;
    @XmlElementRef(name = "PRODUCTPINCODE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productpincode;
    @XmlElementRef(name = "PaxId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paxId;
    @XmlElementRef(name = "ProductName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productName;
    @XmlElementRef(name = "ProductPINCodePackage", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productPINCodePackage;
    @XmlElement(name = "QtyGroup")
    protected BigDecimal qtyGroup;
    @XmlElementRef(name = "REFERENCEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> referenceid;
    @XmlElementRef(name = "SDC_CartRetailTicketPINLineCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> sdcCartRetailTicketPINLineCollection;
    @XmlElementRef(name = "SDC_FacilityCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCFacility> sdcFacilityCollection;
    @XmlElement(name = "StartDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateTime;

    /**
     * Gets the value of the channelID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getChannelID() {
        return channelID;
    }

    /**
     * Sets the value of the channelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setChannelID(Long value) {
        this.channelID = value;
    }

    /**
     * Gets the value of the custAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustAccount() {
        return custAccount;
    }

    /**
     * Sets the value of the custAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustAccount(JAXBElement<String> value) {
        this.custAccount = value;
    }

    /**
     * Gets the value of the dataAreaId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataAreaId() {
        return dataAreaId;
    }

    /**
     * Sets the value of the dataAreaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataAreaId(JAXBElement<String> value) {
        this.dataAreaId = value;
    }

    /**
     * Gets the value of the endDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets the value of the endDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDateTime(XMLGregorianCalendar value) {
        this.endDateTime = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setItemId(JAXBElement<String> value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the productpincode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPRODUCTPINCODE() {
        return productpincode;
    }

    /**
     * Sets the value of the productpincode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPRODUCTPINCODE(JAXBElement<String> value) {
        this.productpincode = value;
    }

    /**
     * Gets the value of the paxId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaxId() {
        return paxId;
    }

    /**
     * Sets the value of the paxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaxId(JAXBElement<String> value) {
        this.paxId = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductName(JAXBElement<String> value) {
        this.productName = value;
    }

    /**
     * Gets the value of the productPINCodePackage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductPINCodePackage() {
        return productPINCodePackage;
    }

    /**
     * Sets the value of the productPINCodePackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductPINCodePackage(JAXBElement<String> value) {
        this.productPINCodePackage = value;
    }

    /**
     * Gets the value of the qtyGroup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQtyGroup() {
        return qtyGroup;
    }

    /**
     * Sets the value of the qtyGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQtyGroup(BigDecimal value) {
        this.qtyGroup = value;
    }

    /**
     * Gets the value of the referenceid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREFERENCEID() {
        return referenceid;
    }

    /**
     * Sets the value of the referenceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREFERENCEID(JAXBElement<String> value) {
        this.referenceid = value;
    }

    /**
     * Gets the value of the sdcCartRetailTicketPINLineCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> getSDCCartRetailTicketPINLineCollection() {
        return sdcCartRetailTicketPINLineCollection;
    }

    /**
     * Sets the value of the sdcCartRetailTicketPINLineCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}
     *     
     */
    public void setSDCCartRetailTicketPINLineCollection(JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> value) {
        this.sdcCartRetailTicketPINLineCollection = value;
    }

    /**
     * Gets the value of the sdcFacilityCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCFacility> getSDCFacilityCollection() {
        return sdcFacilityCollection;
    }

    /**
     * Sets the value of the sdcFacilityCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}
     *     
     */
    public void setSDCFacilityCollection(JAXBElement<ArrayOfSDCFacility> value) {
        this.sdcFacilityCollection = value;
    }

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

}
