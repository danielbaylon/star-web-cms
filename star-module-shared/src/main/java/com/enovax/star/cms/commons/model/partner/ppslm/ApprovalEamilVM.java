package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.util.NvxDateUtils;

public class ApprovalEamilVM {

    private String createdBy;
    private String createdDateStr;
    private String description;
    private String remarks;
    private String baseUrl;
    private String preDefinedFooter;

    public ApprovalEamilVM() {
    }

    public ApprovalEamilVM(PPSLMApprovalLog applog, String webRoot) {
        this.preDefinedFooter = "";
        this.createdBy = applog.getCreatedBy();
        this.createdDateStr = applog.getCreatedDate() == null ? "" : NvxDateUtils
                .formatDate(applog.getCreatedDate(),
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.description = applog.getDescription();
        this.remarks = applog.getRemarks();
        this.baseUrl = webRoot;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPreDefinedFooter() {
        return preDefinedFooter;
    }

    public void setPreDefinedFooter(String preDefinedFooter) {
        this.preDefinedFooter = preDefinedFooter;
    }
}
