package com.enovax.star.cms.commons.model.kiosk.jcr;

import java.io.Serializable;

public class JcrKioskInfo implements Serializable {

    private String jcrName;

    private String deviceNumber;

    private String terminalNumber;

    private String userId;

    private String userPassword;

    private String jcrNodeUuid;

    private String location;

    private String shutdownHour;

    private String shutdownMin;

    public String getJcrName() {
        return jcrName;
    }

    public void setJcrName(String jcrName) {
        this.jcrName = jcrName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getTerminalNumber() {
        return terminalNumber;
    }

    public void setTerminalNumber(String terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getJcrNodeUuid() {
        return jcrNodeUuid;
    }

    public void setJcrNodeUuid(String jcrNodeUuid) {
        this.jcrNodeUuid = jcrNodeUuid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getShutdownHour() {
        return shutdownHour;
    }

    public void setShutdownHour(String shutdownHour) {
        this.shutdownHour = shutdownHour;
    }

    public String getShutdownMin() {
        return shutdownMin;
    }

    public void setShutdownMin(String shutdownMin) {
        this.shutdownMin = shutdownMin;
    }

}
