package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = PPSLMRegion.TABLE_NAME)
public class PPSLMRegion {

    public static final String TABLE_NAME = "PPSLMRegion";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;

    @Column(name="regionName")
    private String regionName;

    @Column(name="description")
    private String description;

    @Column(name="status")
    private String status;

    @Column(name="createdBy", nullable=false, length=100)
    private String createdBy;

    @Column(name="createdDate", nullable=false, length=23)
    private Date createdDate;

    @Column(name="modifiedBy", nullable=false, length=100)
    private String modifiedBy;

    @Column(name="modifiedDate", nullable=false, length=23)
    private Date modifiedDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
