package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

/**
 * SDC_NonBindableCRTExtension.Entity.EventGroupLineEntity
 * 
 * @author Justin
 *
 */
public class AxEventGroupLineEntity {

    private String eventCapacityId;
    private String eventEndTime;
    private String eventGroupId;
    private String eventLineId;
    private String eventName;
    private String eventStartTime;
    private String dataAreaID;
    private String recVersion;
    private String recID;
    private List<AxEventAllocationEntity> eventAllocations;
    private List<AxCommerceProperty> extensionProperties;

    public String getEventCapacityId() {
        return eventCapacityId;
    }

    public void setEventCapacityId(String eventCapacityId) {
        this.eventCapacityId = eventCapacityId;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getDataAreaID() {
        return dataAreaID;
    }

    public void setDataAreaID(String dataAreaID) {
        this.dataAreaID = dataAreaID;
    }

    public String getRecVersion() {
        return recVersion;
    }

    public void setRecVersion(String recVersion) {
        this.recVersion = recVersion;
    }

    public String getRecID() {
        return recID;
    }

    public void setRecID(String recID) {
        this.recID = recID;
    }

    public List<AxEventAllocationEntity> getEventAllocations() {
        return eventAllocations;
    }

    public void setEventAllocations(List<AxEventAllocationEntity> eventAllocations) {
        this.eventAllocations = eventAllocations;
    }

    public List<AxCommerceProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxCommerceProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}
