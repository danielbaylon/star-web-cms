package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.payment.SystemTmStatus;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.commons.util.NvxUtil;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by lavanya on 8/11/16.
 */
public class TransReportModel {
    private String orgName;
    private String receiptNum;
    private Date txnDate;
    private Date expiryDate;
    private Date revalExpiryDate;
    private String itemDesc;
    private Integer qty;
    private Integer priceInCents;
    private Integer totalAmountCents;
    private String purchasedBy;
    private String revalidateReference;
    private String transStatus;
    private String tmStatus;
    private String transStatusText;
    private String remarks;
    private Integer reval;

    private String paymentType;
    private String offlinePaymentStatus;
    private Integer offlinePaymentId;
    private Date offlinePayApproveDate;

    @Transient
    private String txnDateText;
    @Transient
    private String expiryDateText;
    @Transient
    private String revalExpiryDateText;
    @Transient
    private BigDecimal price;
    @Transient
    private String priceText;
    @Transient
    private BigDecimal totalAmount;
    @Transient
    private String totalAmountText;
    @Transient
    private String offlinePayApproveDateText;

    @Transient
    private String referenceNum;

    private Date paymentDate;

    @Transient
    private String paymentDateText;


    public void initReportValues() {
        this.offlinePayApproveDateText = "";
        if(offlinePaymentId != null && offlinePaymentId.intValue() > 0 && offlinePaymentStatus != null && offlinePaymentStatus.trim().length() > 0){
            this.offlinePayApproveDateText = offlinePayApproveDate == null ? "" : NvxDateUtils.formatDateForDisplay(offlinePayApproveDate, false);
        }
        this.paymentDateText = paymentDate == null ? "" : NvxDateUtils.formatDateForDisplay(paymentDate, false);
        this.txnDateText = txnDate == null ? "" : NvxDateUtils.formatDateForDisplay(txnDate, true);
        this.expiryDateText = expiryDate == null ? "" : NvxDateUtils.formatDateForDisplay(expiryDate, false);
        this.revalExpiryDateText = revalExpiryDate == null ? "" : NvxDateUtils.formatDateForDisplay(revalExpiryDate, false);
        this.price = NvxNumberUtils.centsToMoney(priceInCents);
        this.priceText = NvxNumberUtils.formatToCurrency(price);
        this.totalAmount = NvxNumberUtils.centsToMoney(totalAmountCents);
        this.totalAmountText = NvxNumberUtils.formatToCurrency(totalAmount);
        if (!EnovaxTmSystemStatus.Success.toString().equals(tmStatus)) {
            this.remarks = StringUtils.isEmpty(tmStatus) ? "" : "TM Status: "
                    + tmStatus;
        }
    }

    public TransReportModel() {

    }

    public TransReportModel(String orgName, String receiptNum, Date txnDateTime, Date expiryDate,
                            Date revalExpiryDate, String itemDesc, Integer qty, Integer priceInCents,
                            Integer totalAmountCents, String purchasedBy, String revalidateReference, String transStatus,
                            String tmStatus, String transStatusText, String remarks, Integer reval) {
        this.orgName = orgName;
        this.receiptNum = receiptNum;
        this.txnDate = txnDateTime;
        this.expiryDate = expiryDate;
        this.revalExpiryDate = revalExpiryDate;
        this.itemDesc = itemDesc;
        this.qty = qty;
        this.priceInCents = priceInCents;
        this.totalAmountCents = totalAmountCents;
        this.purchasedBy = purchasedBy;
        this.revalidateReference = revalidateReference;
        this.transStatus = transStatus;
        this.tmStatus = tmStatus;
        this.transStatusText = transStatusText;
        this.remarks = remarks;
        this.reval = reval;

        //TODO
    }

    /*
     * Get/Set
     */

    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public Date getExpiryDate() {
        return expiryDate;
    }
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    public Date getRevalExpiryDate() {
        return revalExpiryDate;
    }
    public void setRevalExpiryDate(Date revalExpiryDate) {
        this.revalExpiryDate = revalExpiryDate;
    }
    public String getItemDesc() {
        return itemDesc;
    }
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
    public Integer getQty() {
        return qty;
    }
    public void setQty(Integer qty) {
        this.qty = qty;
    }
    public Integer getPriceInCents() {
        return priceInCents;
    }
    public void setPriceInCents(Integer priceInCents) {
        this.priceInCents = priceInCents;
    }
    public Integer getTotalAmountCents() {
        return totalAmountCents;
    }
    public void setTotalAmountCents(Integer totalAmountCents) {
        this.totalAmountCents = totalAmountCents;
    }
    public String getPurchasedBy() {
        return purchasedBy;
    }
    public void setPurchasedBy(String purchasedBy) {
        this.purchasedBy = purchasedBy;
    }
    public String getRevalidateReference() {
        return revalidateReference;
    }
    public void setRevalidateReference(String revalidateReference) {
        this.revalidateReference = revalidateReference;
    }
    public String getTransStatus() {
        return transStatus;
    }
    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }
    public String getTmStatus() {
        return tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getTransStatusText() {
        return transStatusText;
    }
    public void setTransStatusText(String transStatusText) {
        this.transStatusText = transStatusText;
    }
    public String getRemarks() {
        return remarks;
    }
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String getPriceText() {
        return priceText;
    }
    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    public String getTotalAmountText() {
        return totalAmountText;
    }
    public void setTotalAmountText(String totalAmountText) {
        this.totalAmountText = totalAmountText;
    }
    public String getTxnDateText() {
        return txnDateText;
    }
    public void setTxnDateText(String txnDateText) {
        this.txnDateText = txnDateText;
    }
    public String getExpiryDateText() {
        return expiryDateText;
    }
    public void setExpiryDateText(String expiryDateText) {
        this.expiryDateText = expiryDateText;
    }
    public String getRevalExpiryDateText() {
        return revalExpiryDateText;
    }
    public void setRevalExpiryDateText(String revalExpiryDateText) {
        this.revalExpiryDateText = revalExpiryDateText;
    }

    public Integer getReval() {
        return reval;
    }

    public void setReval(Integer reval) {
        this.reval = reval;
    }

    public Date getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }


    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOfflinePaymentStatus() {
        return offlinePaymentStatus;
    }

    public void setOfflinePaymentStatus(String offlinePaymentStatus) {
        this.offlinePaymentStatus = offlinePaymentStatus;
    }

    public Integer getOfflinePaymentId() {
        return offlinePaymentId;
    }

    public void setOfflinePaymentId(Integer offlinePaymentId) {
        this.offlinePaymentId = offlinePaymentId;
    }

    public Date getOfflinePayApproveDate() {
        return offlinePayApproveDate;
    }

    public void setOfflinePayApproveDate(Date offlinePayApproveDate) {
        this.offlinePayApproveDate = offlinePayApproveDate;
    }

    public String getOfflinePayApproveDateText() {
        return offlinePayApproveDateText;
    }

    public void setOfflinePayApproveDateText(String offlinePayApproveDateText) {
        this.offlinePayApproveDateText = offlinePayApproveDateText;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentDateText() {
        return paymentDateText;
    }

    public void setPaymentDateText(String paymentDateText) {
        this.paymentDateText = paymentDateText;
    }

    public String getReferenceNum() {
        return referenceNum;
    }

    public void setReferenceNum(String referenceNum) {
        this.referenceNum = referenceNum;
    }
}
