package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 9/25/16.
 */
public class PartnerItemPriceVM {
    private String listingId; //ListingId, starts with 5
    private String productCode; //displayProductNumber, starts with 1

    private BigDecimal basePrice;
    private BigDecimal customerPrice;

    private Date validFrom;

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(BigDecimal customerPrice) {
        this.customerPrice = customerPrice;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }
}
