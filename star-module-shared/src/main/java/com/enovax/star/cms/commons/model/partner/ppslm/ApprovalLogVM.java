package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import com.enovax.star.cms.commons.util.NvxDateUtils;

public class ApprovalLogVM {

    private String createdDateStr;
    private String createdBy;
    private String description;
    private Integer id;
    private Boolean hasHist;
    private String status;
    private String remarks;

    public ApprovalLogVM(PPSLMApprovalLog applog) {
        this.id = applog.getId();
        this.createdDateStr = applog.getCreatedDate() == null ? "" : NvxDateUtils
                .formatDate(applog.getCreatedDate(),
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        
        this.createdBy  = applog.getCreatedBy();
        this.description  = applog.getDescription();
        this.status  = applog.getStatus();
        this.remarks  = applog.getRemarks();
        if(applog.getPreviousValue()!=null){
            this.hasHist = true; 
        }else{
            this.hasHist = false;
        }
    }
    
    public ApprovalLogVM() {
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getHasHist() {
        return hasHist;
    }

    public void setHasHist(Boolean hasHist) {
        this.hasHist = hasHist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
