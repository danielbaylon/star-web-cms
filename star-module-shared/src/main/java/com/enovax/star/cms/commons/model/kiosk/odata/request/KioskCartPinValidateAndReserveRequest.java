package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartPinTicketListCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskCartPinValidateAndReserveRequest {

    @JsonProperty("CartTicketListCriteria")
    private List<AxCartPinTicketListCriteria> axCartTicketListCriteria = new ArrayList<>();

    public List<AxCartPinTicketListCriteria> getAxCartTicketListCriteria() {
        return axCartTicketListCriteria;
    }

    public void setAxCartTicketListCriteria(List<AxCartPinTicketListCriteria> axCartTicketListCriteria) {
        this.axCartTicketListCriteria = axCartTicketListCriteria;
    }
}
