
package com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _SDCB2BPINViewLineResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINViewLineResponse");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINViewLineTable");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _ArrayOfSDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_B2BPINViewLineTable");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _SDCB2BPINViewLineTableEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCB2BPINViewLineTableSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SalesId");
    private final static QName _SDCB2BPINViewLineTableInventTransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InventTransId");
    private final static QName _SDCB2BPINViewLineTableItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCB2BPINViewLineTableInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InvoiceId");
    private final static QName _SDCB2BPINViewLineTableOpenValidityRule_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "OpenValidityRule");
    private final static QName _SDCB2BPINViewLineTableTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCB2BPINViewLineTableEventDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventDate");
    private final static QName _SDCB2BPINViewLineTableMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MediaType");
    private final static QName _SDCB2BPINViewLineTableRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCB2BPINViewLineTableStartDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "StartDateTime");
    private final static QName _SDCB2BPINViewLineTableEndDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EndDateTime");
    private final static QName _SDCB2BPINViewLineTableEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _SDCB2BPINViewLineTableReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReferenceId");
    private final static QName _GetB2BPINViewLinePinRefId_QNAME = new QName("http://tempuri.org/", "pinRefId");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _GetB2BPINViewLineResponseGetB2BPINViewLineResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINViewLineResult");
    private final static QName _SDCB2BPINViewLineResponseB2BPinViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewLineTable");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetB2BPINViewLine }
     * 
     */
    public GetB2BPINViewLine createGetB2BPINViewLine() {
        return new GetB2BPINViewLine();
    }

    /**
     * Create an instance of {@link GetB2BPINViewLineResponse }
     * 
     */
    public GetB2BPINViewLineResponse createGetB2BPINViewLineResponse() {
        return new GetB2BPINViewLineResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewLineResponse }
     * 
     */
    public SDCB2BPINViewLineResponse createSDCB2BPINViewLineResponse() {
        return new SDCB2BPINViewLineResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewLineTable }
     * 
     */
    public SDCB2BPINViewLineTable createSDCB2BPINViewLineTable() {
        return new SDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCB2BPINViewLineTable }
     * 
     */
    public ArrayOfSDCB2BPINViewLineTable createArrayOfSDCB2BPINViewLineTable() {
        return new ArrayOfSDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINViewLineResponse")
    public JAXBElement<SDCB2BPINViewLineResponse> createSDCB2BPINViewLineResponse(SDCB2BPINViewLineResponse value) {
        return new JAXBElement<SDCB2BPINViewLineResponse>(_SDCB2BPINViewLineResponse_QNAME, SDCB2BPINViewLineResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINViewLineTable")
    public JAXBElement<SDCB2BPINViewLineTable> createSDCB2BPINViewLineTable(SDCB2BPINViewLineTable value) {
        return new JAXBElement<SDCB2BPINViewLineTable>(_SDCB2BPINViewLineTable_QNAME, SDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_B2BPINViewLineTable")
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createArrayOfSDCB2BPINViewLineTable(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_ArrayOfSDCB2BPINViewLineTable_QNAME, ArrayOfSDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventGroupId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableSalesId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableSalesId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InventTransId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInventTransId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInventTransId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableItemId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableItemId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InvoiceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInvoiceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInvoiceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OpenValidityRule", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableOpenValidityRule(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableOpenValidityRule_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableTransactionId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventDate", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventDate(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventDate_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableMediaType(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableMediaType_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableRetailVariantId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "StartDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableStartDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableStartDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EndDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEndDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEndDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventLineId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableReferenceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pinRefId", scope = GetB2BPINViewLine.class)
    public JAXBElement<String> createGetB2BPINViewLinePinRefId(String value) {
        return new JAXBElement<String>(_GetB2BPINViewLinePinRefId_QNAME, String.class, GetB2BPINViewLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINViewLineResult", scope = GetB2BPINViewLineResponse.class)
    public JAXBElement<SDCB2BPINViewLineResponse> createGetB2BPINViewLineResponseGetB2BPINViewLineResult(SDCB2BPINViewLineResponse value) {
        return new JAXBElement<SDCB2BPINViewLineResponse>(_GetB2BPINViewLineResponseGetB2BPINViewLineResult_QNAME, SDCB2BPINViewLineResponse.class, GetB2BPINViewLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewLineTable", scope = SDCB2BPINViewLineResponse.class)
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createSDCB2BPINViewLineResponseB2BPinViewLineTable(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_SDCB2BPINViewLineResponseB2BPinViewLineTable_QNAME, ArrayOfSDCB2BPINViewLineTable.class, SDCB2BPINViewLineResponse.class, value);
    }

}
