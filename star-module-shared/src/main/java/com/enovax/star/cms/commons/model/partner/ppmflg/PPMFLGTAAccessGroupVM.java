package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccessRightsGroup;

/**
 * Created by lavanya on 26/8/16.
 */
public class PPMFLGTAAccessGroupVM {
    private Integer id;
    private String name;
    private String description;
    private String type;
    public PPMFLGTAAccessGroupVM(){
    }

    public PPMFLGTAAccessGroupVM(PPMFLGTAAccessRightsGroup accGroup){
        this.id = accGroup.getId();
        this.name = accGroup.getName();
        this.type = accGroup.getType();
        this.description = accGroup.getDescription();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
