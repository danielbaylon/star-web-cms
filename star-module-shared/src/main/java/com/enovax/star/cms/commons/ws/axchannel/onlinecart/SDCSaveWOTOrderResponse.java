
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for SDC_SaveWOTOrderResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_SaveWOTOrderResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="GeneratedPINId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Order" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SalesOrder" minOccurs="0"/>
 *         &lt;element name="ErrMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_SaveWOTOrderResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "generatedPINId",
    "order",
    "errMessage",
    "success"
})
public class SDCSaveWOTOrderResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "GeneratedPINId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<String> generatedPINId;
    @XmlElementRef(name = "Order", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrder> order;
    @XmlElementRef(name = "ErrMessage", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<String> errMessage;
    @XmlElement(name = "Success")
    protected Boolean success;

    /**
     * Gets the value of the generatedPINId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGeneratedPINId() {
        return generatedPINId;
    }

    /**
     * Sets the value of the generatedPINId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGeneratedPINId(JAXBElement<String> value) {
        this.generatedPINId = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrder }{@code >}
     *     
     */
    public JAXBElement<SalesOrder> getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrder }{@code >}
     *     
     */
    public void setOrder(JAXBElement<SalesOrder> value) {
        this.order = value;
    }

    /**
     * Gets the value of the success property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setSuccess(Boolean value) {
        this.success = value;
    }


    /**
     * Gets the value of the errorMessage property.
     *
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getErrMessage() {
        return errMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     *
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setErrMessage(JAXBElement<String> value) {
        this.errMessage = value;
    }

}
