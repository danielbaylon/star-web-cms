package com.enovax.star.cms.commons.model.axstar;

/**
 * Created by houtao on 4/8/16.
 */
public class AxStarRichMediaLocation {

    private String url = null;
    private String altText = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }
}
