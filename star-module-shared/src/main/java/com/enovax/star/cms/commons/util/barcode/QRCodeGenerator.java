package com.enovax.star.cms.commons.util.barcode;

import com.enovax.star.cms.commons.util.Base64;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.QRCode;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class QRCodeGenerator {

    private static final String charset = "UTF-8";
    private static final Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<>();
    private static final Map<EncodeHintType, ErrorCorrectionLevel> logoHintMap = new HashMap<>();

    public static String toBase64WithLogo(String qrCodeData, int width, int height, String logoResourcePath) throws WriterException, IOException {
        //Use a higher error-correction level
        logoHintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

        //Generate QR code
        BitMatrix matrix = createQRCode(qrCodeData, charset, logoHintMap, width, height);

        BufferedImage img = MatrixToImageWriter.toBufferedImage(matrix);

        //Retrieve resource
        BufferedImage overlay = ImageIO.read(QRCode.class.getResourceAsStream(logoResourcePath));

        //Compute deltas
        int deltaHeight = img.getHeight() - overlay.getHeight();
        int deltaWidth  = img.getWidth()  - overlay.getWidth();

        //Create combined image
        BufferedImage combined = new BufferedImage(height, width, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D)combined.getGraphics();
        g.drawImage(img, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(overlay, (int)Math.round(deltaWidth/2), (int)Math.round(deltaHeight/2), null);

        //Convert image to a base64 string
        ByteArrayOutputStream stream = null;
        try {
            stream = new ByteArrayOutputStream();
            ImageIO.write(combined, "PNG", stream);
            byte[] qrBytes = stream.toByteArray();

            final String b64 = Base64.encodeToString(qrBytes, false);
            return b64;
        } finally {
            if (stream != null) { stream.close(); }
        }
    }

    public static byte[] toByteArray(String qrCodeData, int width, int height) throws WriterException, IOException {
		/* ErrorCorrectionLevel
		 * H = ~30% correction
		 * L = ~7% correction
		 * M = ~15% correction
		 * Q = ~25% correction
		*/
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        BitMatrix matrix = createQRCode(qrCodeData, charset, hintMap, width, height);
        ByteArrayOutputStream stream = null;
        try {
            stream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(matrix, "PNG", stream);
            byte[] qrBytes = stream.toByteArray();
            return qrBytes;
        } finally {
            if (stream != null) { stream.close(); }
        }
    }

    public static String toBase64(String qrCodeData, int width, int height) throws WriterException, IOException {
        byte[] qrCodeBytes = toByteArray(qrCodeData, width, height);
        String qrCodeBase64 = Base64.encodeToString(qrCodeBytes, false);
        return qrCodeBase64;
    }

    public static void toFile(String qrCodeData, int width, int height, String filePath) throws WriterException, IOException {
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        BitMatrix matrix = createQRCode(qrCodeData, charset, hintMap, width, height);
        MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath.lastIndexOf('.') + 1), new File(filePath));
    }

    public static BitMatrix createQRCode(String qrCodeData, String charset,
                                         Map<EncodeHintType, ErrorCorrectionLevel> hintMap, int qrCodeheight, int qrCodewidth)
            throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes(charset), charset),
                BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
        return matrix;
    }

    public static String readQRCode(String filePath, Map hintMap) throws IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filePath)))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
        return qrCodeResult.getText();
    }

    public static String readQRCode(final byte[] qrCodeBytes, Map hintMap) throws IOException, NotFoundException {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(qrCodeBytes);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(byteStream))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
        return qrCodeResult.getText();
    }


    public static byte[] compressGZIP(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new GZIPOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    public static String decompressGZIP(byte[] bytes) throws Exception {
        InputStream in = new GZIPInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static String compressAndEncode(String text) {
        byte[] compressedBytes = compressGZIP(text);
        String compressedText = Base64.encodeToString(compressedBytes, false);
        return compressedText;
    }

    public static String decodeAndDecompress(String compressedText) throws Exception{
        String decompressedText = decompressGZIP(Base64.decode(compressedText));
        return decompressedText;
    }


}
