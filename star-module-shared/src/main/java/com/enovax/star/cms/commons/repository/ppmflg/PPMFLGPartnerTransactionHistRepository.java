package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerTransactionHist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 21/8/16.
 */
@Repository
public interface PPMFLGPartnerTransactionHistRepository extends JpaRepository<PPMFLGPartnerTransactionHist, Integer>, JpaSpecificationExecutor<PPMFLGPartnerTransactionHist>{

    PPMFLGPartnerTransactionHist findFirstByOrderAccountAndVoucher(String orderAccount, String voucher);
}
