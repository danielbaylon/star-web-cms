
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for UpCrossSellTableCriteria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpCrossSellTableCriteria">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntitySearch">
 *       &lt;sequence>
 *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpCrossSellTableCriteria", namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", propOrder = {
    "channel",
    "itemId"
})
public class UpCrossSellTableCriteria
    extends CommerceEntitySearch
{

    @XmlElement(name = "Channel")
    protected Long channel;
    @XmlElementRef(name = "ItemId", namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemId;

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setChannel(Long value) {
        this.channel = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setItemId(JAXBElement<String> value) {
        this.itemId = value;
    }

}
