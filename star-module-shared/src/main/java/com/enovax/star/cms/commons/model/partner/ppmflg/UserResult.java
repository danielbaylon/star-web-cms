package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAAccount;

/**
 * Created by lavanya on 2/9/16.
 */
public class UserResult {
    public static enum Result {
        Success,
        Failed,
        Last5Passwords,
        ExistingUser,
        ExceedSubAcc
    }

    public final Result result;
    public final PPMFLGTAAccount user;

    public UserResult(Result result, PPMFLGTAAccount user) {
        this.result = result;
        this.user = user;
    }
}
