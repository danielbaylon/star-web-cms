package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.definition.MultiLanguageSelectFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractSelect;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.SelectFieldFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 3/23/16.
 */
public class MultiLanguageSelectFieldFactory<D extends MultiLanguageSelectFieldDefinition> extends SelectFieldFactory<D> {


    @Inject
    public MultiLanguageSelectFieldFactory(D definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
    }


    @Override
    protected AbstractSelect createFieldComponent() {
        String currentLocale = getLocale().toString();
        if("en".equals(currentLocale)) {
            definition.setValueProperty(((MultiLanguageSelectFieldDefinition)definition).getValuePropertyDefault());
            definition.setLabelProperty(((MultiLanguageSelectFieldDefinition)definition).getLabelPropertyDefault());
        }else {
            definition.setValueProperty(((MultiLanguageSelectFieldDefinition)definition).getValuePropertyDefault() + "_" + currentLocale);
            definition.setLabelProperty(((MultiLanguageSelectFieldDefinition)definition).getLabelPropertyDefault() + "_" + currentLocale);
        }
        return super.createFieldComponent();
    }

}
