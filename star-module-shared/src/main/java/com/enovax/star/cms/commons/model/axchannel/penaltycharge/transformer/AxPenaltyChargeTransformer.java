package com.enovax.star.cms.commons.model.axchannel.penaltycharge.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.penaltycharge.AxPenaltyCharge;
import com.enovax.star.cms.commons.ws.axchannel.pin.*;

import javax.xml.bind.JAXBElement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houtao on 5/9/16.
 */
public class AxPenaltyChargeTransformer {
    public static ApiResult<List<AxPenaltyCharge>> fromWsPenaltyChargeException(Exception ex) {
        ApiResult<List<AxPenaltyCharge>> apiResult = new ApiResult<List<AxPenaltyCharge>>();
        apiResult.setSuccess(false);
        apiResult.setMessage(ex != null && ex.getMessage() != null ? ex.getMessage() : "Unexpected exception found");
        return apiResult;
    }

    public static ApiResult<List<AxPenaltyCharge>> fromWsPenaltyChargeResponse(SDCCustPenaltyChargeResponse response, String custAccountNum) {
        if(response == null){
            ApiResult<List<AxPenaltyCharge>> apiResult = new ApiResult<List<AxPenaltyCharge>>();
            apiResult.setSuccess(false);
            apiResult.setMessage("No response found");
            return apiResult;
        }
        ApiResult<List<AxPenaltyCharge>> apiResult = new ApiResult<List<AxPenaltyCharge>>();
        List<AxPenaltyCharge> pcs = new ArrayList<AxPenaltyCharge>();
        if(response != null && response.getCustPenaltyChargetTable() != null && response.getCustPenaltyChargetTable().getValue() != null){
            ArrayOfSDCCustPenaltyChargeTable arrayOfPcTable = response.getCustPenaltyChargetTable().getValue();
            if(arrayOfPcTable != null && arrayOfPcTable.getSDCCustPenaltyChargeTable() != null && arrayOfPcTable.getSDCCustPenaltyChargeTable().size() > 0){
                List<SDCCustPenaltyChargeTable> pcList = arrayOfPcTable.getSDCCustPenaltyChargeTable();
                if(pcList != null && pcList.size() > 0){
                    for(SDCCustPenaltyChargeTable pct : pcList){
                        boolean filtered = false;
                        String accountNum = getNonEmptyStringValue(pct.getACCOUNTNUM());
                        if(custAccountNum != null && custAccountNum.trim().length() > 0){
                            if(custAccountNum.equalsIgnoreCase(accountNum)){
                                filtered = true;
                            }
                        }else{
                            if("".equalsIgnoreCase(accountNum)){
                                filtered = true;
                            }
                        }
                        if(filtered){
                            AxPenaltyCharge pc = new AxPenaltyCharge();
                            pc.setAccountNum(accountNum);
                            pc.setEventGroupId(getNonEmptyStringValue(pct.getEVENTGROUPID()));
                            pc.setEventLineId(getNonEmptyStringValue(pct.getEVENTLINEID()));
                            pc.setMarkupCode(getNonEmptyStringValue(pct.getMARKUPCODE()));
                            pc.setMarkupAmt(pct.getMISCCHARGEAMOUNT() != null ? pct.getMISCCHARGEAMOUNT() : new BigDecimal(0.0));
                            pc.setAccountCode(pct.getACCOUNTCODE());
                            pc.setDataAreaId(getNonEmptyStringValue(pct.getDATAAREAID()));
                            pc.setDescription(getNonEmptyStringValue(pct.getDESCRIPTION()));
                            pc.setPenaltyId(getNonEmptyStringValue(pct.getPENALTYID()));
                            pc.setRecId(pct.getRECID());
                            pcs.add(pc);
                        }
                    }
                }
            }
        }
        apiResult.setData(pcs);
        apiResult.setSuccess(true);
        if(pcs.size() == 0){
            apiResult = checkDefaultResponse(apiResult, response);
        }
        return apiResult;
    }

    public static ApiResult checkDefaultResponse(ApiResult axResponse, ServiceResponse response) {
        boolean errorFound = false;
        JAXBElement<ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if(errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (ResponseError rr : responseErrorList) {
                        if(!(rr != null)){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                            errorFound = true;
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                            errorFound = true;
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        axResponse.setSuccess(!errorFound);
        return axResponse;
    }

    private static String getNonEmptyStringValue(JAXBElement<String> accountnum) {
        if(accountnum != null){
            if(accountnum.getValue() != null && accountnum.getValue().trim().length() > 0){
                return accountnum.getValue().trim();
            }
        }
        return "";
    }
}
