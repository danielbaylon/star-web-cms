package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/6/16.
 */
public enum CMSProductSubCategoriesProperties {
    RelatedCMSProducts("relatedCMSProductUUID");

    private String propertyName;

    CMSProductSubCategoriesProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}

