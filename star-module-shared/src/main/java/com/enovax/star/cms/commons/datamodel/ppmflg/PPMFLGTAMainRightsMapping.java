package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name="PPMFLGTAMainRightsMapping")
public class PPMFLGTAMainRightsMapping implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPMFLGTAAccessRightsGroup accessRightsGroup;

	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainAccountId",nullable = false)
    private PPMFLGTAMainAccount user;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public PPMFLGTAMainAccount getUser() {
		return user;
	}


	public void setUser(PPMFLGTAMainAccount user) {
		this.user = user;
	}


	public PPMFLGTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}


	public void setAccessRightsGroup(PPMFLGTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}
	
	
			
	
	
}
