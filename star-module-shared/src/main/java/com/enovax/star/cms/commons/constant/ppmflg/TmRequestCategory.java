package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by houtao on 14/11/16.
 */
public enum TmRequestCategory {

    DepositTopup,
    Purchase,
    Revalidate

}
