package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = PPMFLGReasonLogMap.TABLE_NAME)
public class PPMFLGReasonLogMap implements Serializable {

    public static final String TABLE_NAME = "PPMFLGReasonLogMap";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "reasonId")
    private Integer reasonId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reasonId", updatable = false, insertable = false)
    private PPMFLGReason reason;

    @Column(name = "logId")
    private Integer logId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "logId", updatable = false, insertable = false)
    private PPMFLGApprovalLog log;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public PPMFLGReason getReason() {
        return reason;
    }

    public void setReason(PPMFLGReason reason) {
        this.reason = reason;
    }

    public PPMFLGApprovalLog getLog() {
        return log;
    }

    public void setLog(PPMFLGApprovalLog log) {
        this.log = log;
    }
}
