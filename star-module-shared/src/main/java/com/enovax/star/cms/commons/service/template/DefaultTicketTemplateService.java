package com.enovax.star.cms.commons.service.template;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;
import com.enovax.star.cms.commons.model.ticket.TicketTemplateCacheObject;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelProductsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jennylynsze on 12/15/16.
 */
@Service
public class DefaultTicketTemplateService implements  ITicketTemplateService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AxChannelProductsService axChannelProductsService;

    private Map<StoreApiChannels, TicketTemplateCacheObject> templateDataCache = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        templateDataCache.put(StoreApiChannels.B2C_MFLG, new TicketTemplateCacheObject());
        templateDataCache.put(StoreApiChannels.B2C_SLM, new TicketTemplateCacheObject());
        templateDataCache.put(StoreApiChannels.PARTNER_PORTAL_SLM, new TicketTemplateCacheObject());
        templateDataCache.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new TicketTemplateCacheObject());
    }

    @Override
    public void updateTicketTemplateCache(StoreApiChannels channel) throws AxChannelException{
        TicketTemplateCacheObject obj = new TicketTemplateCacheObject();
        List<AxRetailTemplateXML> ticketTemplates = getTicketTemplates(channel);

        if(ticketTemplates != null) {
            Map<String, AxRetailTemplateXML> templateXMLMap = new HashMap<>();
            for(AxRetailTemplateXML ticket: ticketTemplates) {
                templateXMLMap.put(ticket.getTemplateName(), ticket);
            }
            obj.setTemplateXMLMap(templateXMLMap);
        }
        templateDataCache.put(channel, obj);
    }

    @Override
         public List<AxRetailTemplateXML> getTicketTemplates(StoreApiChannels channel) throws AxChannelException {
        ApiResult<List<AxRetailTemplateXML>> ticketTemplatesRes =  axChannelProductsService.apiSearchProductTicketTemplate(channel, ""); //get all

        if(!ticketTemplatesRes.isSuccess()) {
            log.error("Error in retrieving the ticket templates...");
            return null;
        }

        return ticketTemplatesRes.getData();
    }

    @Override
    public AxRetailTemplateXML getTicketTemplate(StoreApiChannels channel, String templateName) {
        TicketTemplateCacheObject cacheTemplate = templateDataCache.get(channel);
        Map<String,AxRetailTemplateXML> cacheTemplateMap = cacheTemplate.getTemplateXMLMap();

        if(!cacheTemplateMap.containsKey(templateName)) {
            ApiResult<List<AxRetailTemplateXML>> ticketTemplatesRes = null; //get all
            try {
                ticketTemplatesRes = axChannelProductsService.apiSearchProductTicketTemplate(channel, templateName);

                if(ticketTemplatesRes.isSuccess()) {
                    List<AxRetailTemplateXML> templateList = ticketTemplatesRes.getData();
                    if(templateList != null) {
                        cacheTemplateMap.put(templateName, templateList.get(0));
                    }
                }
            } catch (AxChannelException e) {
                log.error(e.getMessage(), e);
            }
        }

        return cacheTemplateMap.get(templateName);
    }
}
