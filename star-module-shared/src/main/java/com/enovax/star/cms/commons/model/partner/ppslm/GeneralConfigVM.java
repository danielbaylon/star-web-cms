package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lavanya on 28/9/16.
 */
public class GeneralConfigVM {
    private Map<String, Object> sysMap;
    private ApproverVM approverVm;
    private List<ApproverVM> candidatesList;

    public GeneralConfigVM() {
        sysMap = new HashMap<String, Object>();
    }

    public GeneralConfigVM(Map<String, Object> sysMap, ApproverVM approverVm, List<ApproverVM> candidatesList) {
        this.sysMap = sysMap;
        this.approverVm = approverVm;
        this.candidatesList = candidatesList;
    }

    public Map<String, Object> getSysMap() {
        return sysMap;
    }

    public void setSysMap(Map<String, Object> sysMap) {
        this.sysMap = sysMap;
    }

    public ApproverVM getApproverVm() {
        return approverVm;
    }

    public void setApproverVm(ApproverVM approverVm) {
        this.approverVm = approverVm;
    }

    public List<ApproverVM> getCandidatesList() {
        return candidatesList;
    }

    public void setCandidatesList(List<ApproverVM> candidatesList) {
        this.candidatesList = candidatesList;
    }
}
