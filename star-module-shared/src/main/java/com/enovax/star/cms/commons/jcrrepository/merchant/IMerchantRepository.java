package com.enovax.star.cms.commons.jcrrepository.merchant;

import com.enovax.star.cms.commons.model.merchant.Merchant;

import java.util.List;

/**
 * Created by jace on 15/9/16.
 */
public interface IMerchantRepository {
  Merchant find(String channel, String id);

  Merchant findByUuid(String uuid);

  List<Merchant> getAll(String channel, Boolean active, Boolean acceptsAllCards);
}
