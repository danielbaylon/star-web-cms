
package com.enovax.star.cms.commons.ws.axchannel.upcrosssell;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.upcrosssell package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfSDCUpCrossSellLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_UpCrossSellLineTable");
    private final static QName _SDCUpCrossSellTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_UpCrossSellTable");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _SDCGetUpCrossSellResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetUpCrossSellResponse");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _ArrayOfSDCUpCrossSellTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_UpCrossSellTable");
    private final static QName _CommerceEntitySearch_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceEntitySearch");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _UpCrossSellTableCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", "UpCrossSellTableCriteria");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _SDCUpCrossSellLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_UpCrossSellLineTable");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SDCUpCrossSellTableITEMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ITEMID");
    private final static QName _SDCUpCrossSellTableUpCrossSellLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UpCrossSellLines");
    private final static QName _SDCUpCrossSellTableUNITIDId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UNITIDId");
    private final static QName _SDCUpCrossSellLineTableORIGITEMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ORIGITEMID");
    private final static QName _SDCUpCrossSellLineTableINVENTDIMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "INVENTDIMID");
    private final static QName _SDCUpCrossSellLineTableVARIANTID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "VARIANTID");
    private final static QName _SDCUpCrossSellLineTableUPCROSSSELLITEM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UPCROSSSELLITEM");
    private final static QName _SDCUpCrossSellLineTableUNITID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UNITID");
    private final static QName _GetUpCrossSellCriteria_QNAME = new QName("http://tempuri.org/", "criteria");
    private final static QName _GetUpCrossSellResponseGetUpCrossSellResult_QNAME = new QName("http://tempuri.org/", "GetUpCrossSellResult");
    private final static QName _SDCGetUpCrossSellResponseUpCrossSellEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "upCrossSellEntity");
    private final static QName _UpCrossSellTableCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", "ItemId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.upcrosssell
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUpCrossSell }
     * 
     */
    public GetUpCrossSell createGetUpCrossSell() {
        return new GetUpCrossSell();
    }

    /**
     * Create an instance of {@link UpCrossSellTableCriteria }
     * 
     */
    public UpCrossSellTableCriteria createUpCrossSellTableCriteria() {
        return new UpCrossSellTableCriteria();
    }

    /**
     * Create an instance of {@link GetUpCrossSellResponse }
     * 
     */
    public GetUpCrossSellResponse createGetUpCrossSellResponse() {
        return new GetUpCrossSellResponse();
    }

    /**
     * Create an instance of {@link SDCGetUpCrossSellResponse }
     * 
     */
    public SDCGetUpCrossSellResponse createSDCGetUpCrossSellResponse() {
        return new SDCGetUpCrossSellResponse();
    }

    /**
     * Create an instance of {@link CommerceEntitySearch }
     * 
     */
    public CommerceEntitySearch createCommerceEntitySearch() {
        return new CommerceEntitySearch();
    }

    /**
     * Create an instance of {@link SDCUpCrossSellTable }
     * 
     */
    public SDCUpCrossSellTable createSDCUpCrossSellTable() {
        return new SDCUpCrossSellTable();
    }

    /**
     * Create an instance of {@link SDCUpCrossSellLineTable }
     * 
     */
    public SDCUpCrossSellLineTable createSDCUpCrossSellLineTable() {
        return new SDCUpCrossSellLineTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCUpCrossSellTable }
     * 
     */
    public ArrayOfSDCUpCrossSellTable createArrayOfSDCUpCrossSellTable() {
        return new ArrayOfSDCUpCrossSellTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCUpCrossSellLineTable }
     * 
     */
    public ArrayOfSDCUpCrossSellLineTable createArrayOfSDCUpCrossSellLineTable() {
        return new ArrayOfSDCUpCrossSellLineTable();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_UpCrossSellLineTable")
    public JAXBElement<ArrayOfSDCUpCrossSellLineTable> createArrayOfSDCUpCrossSellLineTable(ArrayOfSDCUpCrossSellLineTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellLineTable>(_ArrayOfSDCUpCrossSellLineTable_QNAME, ArrayOfSDCUpCrossSellLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_UpCrossSellTable")
    public JAXBElement<SDCUpCrossSellTable> createSDCUpCrossSellTable(SDCUpCrossSellTable value) {
        return new JAXBElement<SDCUpCrossSellTable>(_SDCUpCrossSellTable_QNAME, SDCUpCrossSellTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetUpCrossSellResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetUpCrossSellResponse")
    public JAXBElement<SDCGetUpCrossSellResponse> createSDCGetUpCrossSellResponse(SDCGetUpCrossSellResponse value) {
        return new JAXBElement<SDCGetUpCrossSellResponse>(_SDCGetUpCrossSellResponse_QNAME, SDCGetUpCrossSellResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_UpCrossSellTable")
    public JAXBElement<ArrayOfSDCUpCrossSellTable> createArrayOfSDCUpCrossSellTable(ArrayOfSDCUpCrossSellTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellTable>(_ArrayOfSDCUpCrossSellTable_QNAME, ArrayOfSDCUpCrossSellTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceEntitySearch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceEntitySearch")
    public JAXBElement<CommerceEntitySearch> createCommerceEntitySearch(CommerceEntitySearch value) {
        return new JAXBElement<CommerceEntitySearch>(_CommerceEntitySearch_QNAME, CommerceEntitySearch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", name = "UpCrossSellTableCriteria")
    public JAXBElement<UpCrossSellTableCriteria> createUpCrossSellTableCriteria(UpCrossSellTableCriteria value) {
        return new JAXBElement<UpCrossSellTableCriteria>(_UpCrossSellTableCriteria_QNAME, UpCrossSellTableCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_UpCrossSellLineTable")
    public JAXBElement<SDCUpCrossSellLineTable> createSDCUpCrossSellLineTable(SDCUpCrossSellLineTable value) {
        return new JAXBElement<SDCUpCrossSellLineTable>(_SDCUpCrossSellLineTable_QNAME, SDCUpCrossSellLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ITEMID", scope = SDCUpCrossSellTable.class)
    public JAXBElement<String> createSDCUpCrossSellTableITEMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellTableITEMID_QNAME, String.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UpCrossSellLines", scope = SDCUpCrossSellTable.class)
    public JAXBElement<ArrayOfSDCUpCrossSellLineTable> createSDCUpCrossSellTableUpCrossSellLines(ArrayOfSDCUpCrossSellLineTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellLineTable>(_SDCUpCrossSellTableUpCrossSellLines_QNAME, ArrayOfSDCUpCrossSellLineTable.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UNITIDId", scope = SDCUpCrossSellTable.class)
    public JAXBElement<String> createSDCUpCrossSellTableUNITIDId(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellTableUNITIDId_QNAME, String.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ORIGITEMID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableORIGITEMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableORIGITEMID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "INVENTDIMID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableINVENTDIMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableINVENTDIMID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "VARIANTID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableVARIANTID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableVARIANTID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UPCROSSSELLITEM", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableUPCROSSSELLITEM(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableUPCROSSSELLITEM_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UNITID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableUNITID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableUNITID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "criteria", scope = GetUpCrossSell.class)
    public JAXBElement<UpCrossSellTableCriteria> createGetUpCrossSellCriteria(UpCrossSellTableCriteria value) {
        return new JAXBElement<UpCrossSellTableCriteria>(_GetUpCrossSellCriteria_QNAME, UpCrossSellTableCriteria.class, GetUpCrossSell.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetUpCrossSellResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetUpCrossSellResult", scope = GetUpCrossSellResponse.class)
    public JAXBElement<SDCGetUpCrossSellResponse> createGetUpCrossSellResponseGetUpCrossSellResult(SDCGetUpCrossSellResponse value) {
        return new JAXBElement<SDCGetUpCrossSellResponse>(_GetUpCrossSellResponseGetUpCrossSellResult_QNAME, SDCGetUpCrossSellResponse.class, GetUpCrossSellResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "upCrossSellEntity", scope = SDCGetUpCrossSellResponse.class)
    public JAXBElement<ArrayOfSDCUpCrossSellTable> createSDCGetUpCrossSellResponseUpCrossSellEntity(ArrayOfSDCUpCrossSellTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellTable>(_SDCGetUpCrossSellResponseUpCrossSellEntity_QNAME, ArrayOfSDCUpCrossSellTable.class, SDCGetUpCrossSellResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", name = "ItemId", scope = UpCrossSellTableCriteria.class)
    public JAXBElement<String> createUpCrossSellTableCriteriaItemId(String value) {
        return new JAXBElement<String>(_UpCrossSellTableCriteriaItemId_QNAME, String.class, UpCrossSellTableCriteria.class, value);
    }

}
