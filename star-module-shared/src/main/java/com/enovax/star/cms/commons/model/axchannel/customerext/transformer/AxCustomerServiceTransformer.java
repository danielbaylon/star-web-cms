package com.enovax.star.cms.commons.model.axchannel.customerext.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTransactionDetails;
import com.enovax.star.cms.commons.ws.axchannel.customerext.*;

import javax.xml.bind.JAXBElement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by houtao on 17/8/16.
 */
public class AxCustomerServiceTransformer extends AxResponseTransformer {

    public static ApiResult<String> fromWsTopupDeposit(SDCTopUpCustomerDepositResponse response) {
        ApiResult<String> axResponse = new ApiResult<String>();
        axResponse.setSuccess(response.isSuccess());
        axResponse.setMessage(response.getErrorMessage().getValue());
        if(!response.isSuccess()){
            axResponse = checkDefaultResponse(axResponse, response);
        }
        return axResponse;
    }

    /***
     public static ApiResult<AxCustomerBalances> fromWsViewCustomerBalance(SDCViewCustomerBalanceResponse response) {
     ApiResult<AxCustomerBalances> axResponse = new ApiResult<AxCustomerBalances>();
     JAXBElement<CustomerBalances> details = response.getCustomerBalanceDetails();
     AxCustomerBalances axCustBalance = null;
     if(details != null){
     CustomerBalances customerBalances = details.getValue();
     if(customerBalances != null){
     axCustBalance = new AxCustomerBalances();
     axCustBalance.setBalance(customerBalances.getBalance());
     axCustBalance.setCreditLimit(customerBalances.getCreditLimit());
     axCustBalance.setInvoiceAccountBalance(customerBalances.getInvoiceAccountBalance());
     axCustBalance.setInvoiceAccountCreditLimit(customerBalances.getInvoiceAccountCreditLimit());
     axCustBalance.setInvoiceAccountPendingBalance(customerBalances.getPendingBalance());
     axCustBalance.setBalance(customerBalances.getBalance());
     axResponse.setData(axCustBalance);
     }
     }
     if(axCustBalance == null){
     axResponse = checkDefaultResponse(axResponse, response);
     }
     return axResponse;
     }
     ***/

    public static ApiResult<List<AxCustomerTransactionDetails>> fromWsViewDepositTransactions(SDCViewCustomerTransactionsResponse response) {
        ApiResult<List<AxCustomerTransactionDetails>> axResponse = new ApiResult<List<AxCustomerTransactionDetails>>();

        List<AxCustomerTransactionDetails> axList = new ArrayList<AxCustomerTransactionDetails>();

        JAXBElement<ArrayOfCustomerTransactionDetails> xmlArrayOfTxnList = null;
        ArrayOfCustomerTransactionDetails              arrayOfTxnList = null;
        List<CustomerTransactionDetails>               txnList = null;

        xmlArrayOfTxnList = response.getCustomerTransactionDetails();
        if(xmlArrayOfTxnList != null){
            arrayOfTxnList = xmlArrayOfTxnList.getValue();
        }
        if(arrayOfTxnList != null){
            txnList = arrayOfTxnList.getCustomerTransactionDetails();
        }
        if(txnList != null){
            Iterator<CustomerTransactionDetails> iter = txnList.iterator();
            while(iter.hasNext()){
                CustomerTransactionDetails c = iter.next();
                if(c == null){
                    continue;
                }
                AxCustomerTransactionDetails ac = new AxCustomerTransactionDetails();
                ac.setAmountCur(c.getAmountCur());
                ac.setCurrencyCode(c.getCurrencyCode().getValue());
                ac.setDocumentNum(c.getDocumentNum().getValue());
                ac.setInvoice(c.getInvoice().getValue());
                ac.setOrderAccount(c.getOrderAccount().getValue());
                ac.setPaymentReference(c.getPaymentReference().getValue());
                if(c.getTransDate() != null && c.getTransDate().toGregorianCalendar() != null) {
                    ac.setTransDate(c.getTransDate().toGregorianCalendar().getTime());
                }
                ac.setTransType(c.getTransType().getValue());
                ac.setVoucher(c.getVoucher().getValue());
                ac.setType(c.getType() != null ? c.getType().getValue() : null);
                ac.setSalesPoolId(c.getSalesPoolId() != null ? c.getSalesPoolId().getValue() : null);
                axList.add(ac);
            }
        }
        if(axList.size() == 0){
            axResponse = checkDefaultResponse(axResponse, response);
        }else{
            axResponse.setSuccess(true);
        }
        axResponse.setData(axList);
        return axResponse;
    }

    public static ApiResult<AxCustomerBalances> fromWsGetCustomerBalance(SDCCustomerBalanceResponse response) {
        ApiResult<AxCustomerBalances> axResponse = new ApiResult<AxCustomerBalances>();
        JAXBElement<ArrayOfSDCCustomerBalance> details = response.getCustomerBalanceEntity();
        AxCustomerBalances axCustBalance = null;
        if(details != null){
            ArrayOfSDCCustomerBalance customerBalances = details.getValue();
            if(customerBalances != null){
                List<SDCCustomerBalance> balanceList = customerBalances.getSDCCustomerBalance();
                if(balanceList != null && balanceList.size() > 0){
                    for(SDCCustomerBalance balance : balanceList){
                        if(balance == null){
                            continue;
                        }
                        if(axCustBalance == null){
                            axCustBalance = new AxCustomerBalances();
                        }
                        axCustBalance.setBalanceAmount(new BigDecimal(balance.getBalanceAmount()));
                        axCustBalance.setCreditLimitAmount(new BigDecimal(balance.getCreditLimitAmount()));
                        axCustBalance.setMandatoryCreditLimit(balance.getMandatoryCreditLimit());
                        axCustBalance.setOpenOrderAmount(new BigDecimal(balance.getOpenOrderAmount()));
                        axCustBalance.setAccountNum(balance.getAccountNum().getValue());

                        BigDecimal balanceAmt = new BigDecimal(0);
                        balanceAmt = balanceAmt.add(axCustBalance.getBalanceAmount().abs());
                        balanceAmt = balanceAmt.add(axCustBalance.getCreditLimitAmount().abs());
                        balanceAmt = balanceAmt.subtract(axCustBalance.getOpenOrderAmount().abs());

                        axCustBalance.setBalance(balanceAmt);
                    }
                    if(axCustBalance != null){
                        axResponse.setSuccess(true);
                        axResponse.setData(axCustBalance);
                    }

                }

            }
        }
        if(axCustBalance == null){
            axResponse = checkDefaultResponse(axResponse, response);
        }
        return axResponse;
    }
}
