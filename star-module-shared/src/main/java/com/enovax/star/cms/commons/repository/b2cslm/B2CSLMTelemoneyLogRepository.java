package com.enovax.star.cms.commons.repository.b2cslm;

import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMTelemoneyLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jonathan on 7/12/16.
 */
@Repository
public interface B2CSLMTelemoneyLogRepository extends JpaRepository<B2CSLMTelemoneyLog, Integer> {
}
