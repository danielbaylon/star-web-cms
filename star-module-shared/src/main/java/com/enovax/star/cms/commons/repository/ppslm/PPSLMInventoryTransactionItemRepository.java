package com.enovax.star.cms.commons.repository.ppslm;


import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.model.partner.ppslm.FilterProductItemRowVM;
import com.enovax.star.cms.commons.model.partner.ppslm.FilterProductTopLevelVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PPSLMInventoryTransactionItemRepository extends JpaRepository<PPSLMInventoryTransactionItem, Integer>, JpaSpecificationExecutor<PPSLMInventoryTransactionItem> {

    List<PPSLMInventoryTransactionItem> findByTransactionId(Integer transactionId);

    /*
          if(showAvailable){
            criteria.add(Restrictions.in("itrans.status", TransactionUtil.getAvailableStatus()));
            criteria.add(Restrictions.gt("unpackagedQty", 0));
        }else{
            criteria.add(Restrictions.in("itrans.status", TransactionUtil.getAllDisplayStatus()));
     */

    @Query(value = "select i from PPSLMInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))")
    Page getInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm,
            Pageable pageable);

    @Query(value = "select i from PPSLMInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))" +
            " order by i.id DESC")
   List<PPSLMInventoryTransactionItem> getInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm);

    @Query(value = "select count(i) from PPSLMInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))")
    Long countInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm
           );


    List<PPSLMInventoryTransactionItem> findByIdIn(List<Integer> idList);


    @Query(value = "select i from PPSLMInventoryTransactionItem i" +
            " where i.transactionId = :transactionId " +
            " and i.productId = :productId " +
            " and i.unpackagedQty = :unpackagedQty " +
            " and i.mainItemProductId = :mainItemProductId " +
            " and i.mainItemListingId = :mainItemListingId " +
            " and i.transItemType = 'Topup' " +
            " and (:eventGroupId is null or i.eventGroupId = :eventGroupId) " +
            " and (:eventLineId is null or i.eventLineId = :eventLineId)" +
            " and (:dateOfVisit is null or i.dateOfVisit = :dateOfVisit) ")
    List<PPSLMInventoryTransactionItem> getTaggedTopItem
    (@Param("transactionId") Integer transactionId,
     @Param("productId") String productId,
     @Param("unpackagedQty") Integer unpackedQty,
     @Param("mainItemProductId") String mainItemProductId,
     @Param("mainItemListingId") String mainItemListingId,
     @Param("eventGroupId") String eventGroupId,
     @Param("eventLineId") String eventLineId,
     @Param("dateOfVisit") Date dateOfVisit);


    @Query(value="select distinct new com.enovax.star.cms.commons.model.partner.ppslm.FilterProductItemRowVM(i.productId, i.productName, i.displayName) from PPSLMInventoryTransactionItem i")
    List<FilterProductItemRowVM> getItemsFilter();

    @Query(value="select distinct new com.enovax.star.cms.commons.model.partner.ppslm.FilterProductItemRowVM(i.productId, i.productName, i.displayName) " +
            "from PPSLMInventoryTransactionItem i " +
            "where i.inventoryTrans.mainAccountId = :mainAccountId")
    List<FilterProductItemRowVM> getItemsFilterByMainAccount(@Param("mainAccountId") Integer mainAccountId);
}
