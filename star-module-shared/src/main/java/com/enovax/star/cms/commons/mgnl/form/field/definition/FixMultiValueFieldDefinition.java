package com.enovax.star.cms.commons.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.MultiValueFieldDefinition;

/**
 * Created by jennylynsze on 6/14/16.
 */
public class FixMultiValueFieldDefinition extends MultiValueFieldDefinition {
    private String noDataDisplay = "No Data displayed.";

    public String getNoDataDisplay() {
        return noDataDisplay;
    }

    public void setNoDataDisplay(String noDataDisplay) {
        this.noDataDisplay = noDataDisplay;
    }
}
