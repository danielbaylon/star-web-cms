package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 8/13/16.
 */
@Repository
public interface PPSLMMixMatchPackageRepository extends JpaRepository<PPSLMMixMatchPackage, Integer> {
    PPSLMMixMatchPackage findByName(String name);
    List<PPSLMMixMatchPackage> findByIdInAndStatus(List<Integer> ids, String status);

    //STatus to show
//    return new String[] { PkgStatus.Available.toString(),
//            PkgStatus.Expired.toString(), PkgStatus.Redeemed.toString(),
//            PkgStatus.Deactivated.toString()};

    @Query("select p from PPSLMMixMatchPackage p " +
            " where (:fromDate is null or p.ticketGeneratedDate >= :fromDate) " +
            " and (:toDate is null or p.ticketGeneratedDate <= :toDate) " +
            " and (((:status is null or :status = '') and (p.status in ('Available', 'Expired', 'Redeemed', 'Deactivated'))) or p.status =:status) " +
            " and (:pkgNm is null or :pkgNm = '' or p.name like :pkgNm) " +
            " and (:ticketMedia is null or :ticketMedia = p.ticketMedia) " +
            " and p.mainAccountId = :adminId ")
    Page getPackages(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("status") String status,
            @Param("ticketMedia") String pkgTktMedia,
            @Param("pkgNm") String pkgNm,
            Pageable pageable);

    @Query("select p from PPSLMMixMatchPackage p " +
            " where (:fromDate is null or p.ticketGeneratedDate >= :fromDate) " +
            " and (:toDate is null or p.ticketGeneratedDate <= :toDate) " +
            " and (((:status is null or :status = '') and (p.status in ('Available', 'Expired', 'Redeemed', 'Deactivated'))) or p.status =:status) " +
            " and (:pkgNm is null or :pkgNm = '' or p.name like :pkgNm) " +
            " and (:ticketMedia is null or :ticketMedia = p.ticketMedia) " +
            " and p.mainAccountId = :adminId " +
            " order by p.id DESC")
    List<PPSLMMixMatchPackage> getPackages(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("status") String status,
            @Param("ticketMedia") String pkgTktMedia,
            @Param("pkgNm") String pkgNm);


    @Query("select count(p) from PPSLMMixMatchPackage p " +
            " where (:fromDate is null or p.ticketGeneratedDate >= :fromDate) " +
            " and (:toDate is null or p.ticketGeneratedDate <= :toDate) " +
            " and (((:status is null or :status = '') and (p.status in ('Available', 'Expired', 'Redeemed', 'Deactivated'))) or p.status =:status) " +
            " and (:ticketMedia is null or :ticketMedia = p.ticketMedia) " +
            " and (:pkgNm is null or :pkgNm = '' or p.name like :pkgNm) " +
            " and p.mainAccountId = :adminId ")
    Integer countPackages(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("status") String status,
            @Param("ticketMedia") String pkgTktMedia,
            @Param("pkgNm") String pkgNm);

    @Query("select p from PPSLMMixMatchPackage p " +
            " join p.pkgItems i where (i.receiptNum = :receiptNum)")
    List<PPSLMMixMatchPackage> getPackage(@Param("receiptNum") String receiptNum);


    List<PPSLMMixMatchPackage> findByStatusInAndExpiryDateLessThan(List<String> status, Date currDate);

    List<PPSLMMixMatchPackage> findByStatus(String status);

    List<PPSLMMixMatchPackage> findByStatusAndCreatedDateLessThan(String status, Date currDate);
}
