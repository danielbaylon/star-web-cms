package com.enovax.star.cms.commons.repository.b2cmflg;

import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionExtension;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jensen on 15/11/16.
 */
public interface B2CMFLGStoreTransactionExtensionRepository extends JpaRepository<B2CMFLGStoreTransactionExtension, Integer> {

    B2CMFLGStoreTransactionExtension findByReceiptNumber(String receiptNumber);

}
