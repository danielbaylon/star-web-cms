package com.enovax.star.cms.commons.model.partner.ppmflg;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 10/11/16.
 */
public class ExceptionRptDetailsVM {
    private Integer pkgId;
    private Integer pkgItemId;

    private String pinCode;
    private String ticketMedia;
    private List<ExceptionRptPkgItemVM> pkgItems = new ArrayList<>();
    private String orgName;
    private String accountCode;
    //private String displayName;

    private Integer purchasedQty;
    private Integer pinCodeQty;
    private Integer qtyRedeemed;

    private String itemProductCode;

    private Integer v1;
    private Integer v2;
    private Integer v3;

    public String getPinCode() {
        return pinCode;
    }
    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
//    public Date getTmStatusDate() {
//        return tmStatusDate;
//    }
//
//    public void setTmStatusDate(Date tmStatusDate) {
//        if (tmStatusDate != null) {
//            tmStatusDateStr = NvxUtil.formatDate(tmStatusDate,
//                    NvxUtil.DEFAULT_DATE_FORMAT_DISPLAY);
//        }
//        this.tmStatusDate = tmStatusDate;
//    }


    public String getTicketMedia() {
        return ticketMedia;
    }

    public void setTicketMedia(String ticketMedia) {
        this.ticketMedia = ticketMedia;
    }

    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
    public String getAccountCode() {
        return accountCode;
    }
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }
//    public String getDisplayName() {
//        return displayName;
//    }
//    public void setDisplayName(String displayName) {
//        this.displayName = displayName;
//    }
    public Integer getPurchasedQty() {
        return purchasedQty;
    }
    public void setPurchasedQty(Integer purchasedQty) {
        this.purchasedQty = purchasedQty;
    }
    public Integer getPinCodeQty() {
        return pinCodeQty;
    }
    public void setPinCodeQty(Integer pinCodeQty) {
        this.pinCodeQty = pinCodeQty;
    }
    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }
    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public List<ExceptionRptPkgItemVM> getPkgItems() {
        return pkgItems;
    }
    public void setPkgItems(List<ExceptionRptPkgItemVM> pkgItems) {
        this.pkgItems = pkgItems;
    }
    public Integer getV1() {
        return v1;
    }
    public void setV1(Integer v1) {
        this.v1 = v1;
    }
    public Integer getV2() {
        return v2;
    }
    public void setV2(Integer v2) {
        this.v2 = v2;
    }
    public Integer getV3() {
        return v3;
    }
    public void setV3(Integer v3) {
        this.v3 = v3;
    }
    public Integer getPkgId() {
        return pkgId;
    }
    public void setPkgId(Integer pkgId) {
        this.pkgId = pkgId;
    }
    public Integer getPkgItemId() {
        return pkgItemId;
    }
    public void setPkgItemId(Integer pkgItemId) {
        this.pkgItemId = pkgItemId;
    }
    public String getItemProductCode() {
        return itemProductCode;
    }
    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

}
