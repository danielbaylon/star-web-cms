package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PPMFLGPADocumentRepository extends JpaRepository<PPMFLGPADocument, Integer> {
    PPMFLGPADocument findById(Integer id);
}
