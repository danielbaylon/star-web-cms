package com.enovax.star.cms.commons.mgnl.form.field.definition;

import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.definition.FieldDefinition;

/**
 * Created by jennylynsze on 6/20/16.
 */
public class OpenEditRelatedDialogButtonFieldDefinition extends ConfiguredFieldDefinition implements FieldDefinition {
    private String dialogName;
    private String buttonCaption;
    private String workspace;
    private String uuidPropertyName;

    public String getDialogName() {
        return dialogName;
    }

    public void setDialogName(String dialogName) {
        this.dialogName = dialogName;
    }

    public String getButtonCaption() {
        return buttonCaption;
    }

    public void setButtonCaption(String buttonCaption) {
        this.buttonCaption = buttonCaption;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getUuidPropertyName() {
        return uuidPropertyName;
    }

    public void setUuidPropertyName(String uuidPropertyName) {
        this.uuidPropertyName = uuidPropertyName;
    }
}
