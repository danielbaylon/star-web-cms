package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartnerExt;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "partner")
public class PartnerHist implements Cloneable {
    private Integer id;
    private Integer adminId;
    private String axAccountNumber;
    private String accountCode;
    private String revalFeeItemId;
    private Integer revalPeriodMonths;
    private String remarks;
    private String tierId;
    private String accountManagerId;
    private String orgTypeCode;
    private String countryCode;
    private String orgName;
    private String branchName;
    private String uen;
    private String licenseNum;
    private String licenseExpDate;
    private String registrationYear;
    private String contactPerson;
    private String contactDesignation;
    private String address;
    private String postalCode;
    private String city;
    private String telNum;
    private String mobileNum;
    private String faxNum;
    private String email;
    private String website;
    private String languagePreference;
    private String mainDestinations;
    private String createdBy;
    private String createdDateStr;
    private String modifiedBy;
    private String status;
    private List<Integer> paDocIds;
    private List<String> excluProdIds;
    private BigDecimal dailyTransCap;
    private Boolean subAccountEnabled;

    private List<PaDistributionMappingHist> distributionMapping;

    private List<PartnerExtVM> extension = new ArrayList<PartnerExtVM>();


    public PartnerHist() {
    }

    public PartnerHist(PPSLMPartner pa) {
        this.id = pa.getId();
        this.revalFeeItemId = pa.getRevalFeeItemId();
        this.accountCode = pa.getAccountCode();
        this.orgName = pa.getOrgName();
        this.orgTypeCode = pa.getOrgTypeCode();
        this.countryCode = pa.getCountryCode();
        this.branchName = pa.getBranchName();
        this.uen = pa.getUen();
        this.licenseNum = pa.getLicenseNum();
        this.registrationYear = pa.getRegistrationYear();
        this.licenseExpDate = pa.getLicenseExpDate();
        this.contactPerson = pa.getContactPerson();
        this.contactDesignation = pa.getContactDesignation();
        this.address = pa.getAddress();
        this.postalCode = pa.getPostalCode();
        this.city = pa.getCity();
        this.telNum = pa.getTelNum();
        this.mobileNum = pa.getMobileNum();
        this.faxNum = pa.getFaxNum();
        this.email = pa.getEmail();
        this.website = pa.getWebsite();
        this.languagePreference = pa.getLanguagePreference();
        this.mainDestinations = pa.getMainDestinations();
        this.status = pa.getStatus();
        this.dailyTransCap = pa.getDailyTransCap();
        this.accountManagerId = pa.getAccountManagerId();
        this.revalPeriodMonths = pa.getRevalPeriodMonths();
        this.createdDateStr = pa.getCreatedDate() == null ? "" : NvxDateUtils.formatDate(pa.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.axAccountNumber = pa.getAxAccountNumber();

        populateExtensionProperties(pa);
    }

    private void populateExtensionProperties(PPSLMPartner pa) {
        if(pa == null){
            return;
        }
        List<PPSLMPartnerExt> exts = pa.getPartnerExtList();
        if(exts == null || exts.size() == 0){
            return;
        }
        for(PPSLMPartnerExt i : exts){
            PartnerExtVM vm = new PartnerExtVM();
            vm.setId(i.getId());
            vm.setPartnerId(i.getPartnerId());
            vm.setApprovalRequired(i.getApprovalRequired());
            vm.setAttrName(i.getAttrName());
            vm.setAttrValue(i.getAttrValue());
            vm.setAttrValueType(i.getAttrValueType());
            vm.setAttrValueFormat(i.getAttrValueFormat());
            vm.setMandatoryInd(i.getMandatoryInd());
            vm.setDescription(i.getDescription());
            vm.setStatus(i.getStatus());
            extension.add(vm);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getOrgTypeCode() {
        return orgTypeCode;
    }

    public void setOrgTypeCode(String orgTypeCode) {
        this.orgTypeCode = orgTypeCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getAxAccountNumber() {
        return axAccountNumber;
    }

    public void setAxAccountNumber(String axAccountNumber) {
        this.axAccountNumber = axAccountNumber;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getRevalFeeItemId() {
        return revalFeeItemId;
    }

    public void setRevalFeeItemId(String revalFeeItemId) {
        this.revalFeeItemId = revalFeeItemId;
    }

    public Integer getRevalPeriodMonths() {
        return revalPeriodMonths;
    }

    public void setRevalPeriodMonths(Integer revalPeriodMonths) {
        this.revalPeriodMonths = revalPeriodMonths;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTierId() {
        return tierId;
    }

    public void setTierId(String tierId) {
        this.tierId = tierId;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactDesignation() {
        return contactDesignation;
    }

    public void setContactDesignation(String contactDesignation) {
        this.contactDesignation = contactDesignation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }

    public void setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
    }

    public String getMainDestinations() {
        return mainDestinations;
    }

    public void setMainDestinations(String mainDestinations) {
        this.mainDestinations = mainDestinations;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Integer> getPaDocIds() {
        return paDocIds;
    }

    public void setPaDocIds(List<Integer> paDocIds) {
        this.paDocIds = paDocIds;
    }

    public List<String> getExcluProdIds() {
        return excluProdIds;
    }

    public void setExcluProdIds(List<String> excluProdIds) {
        this.excluProdIds = excluProdIds;
    }

    public BigDecimal getDailyTransCap() {
        return dailyTransCap;
    }

    public void setDailyTransCap(BigDecimal dailyTransCap) {
        this.dailyTransCap = dailyTransCap;
    }

    public Boolean getSubAccountEnabled() {
        return subAccountEnabled;
    }

    public void setSubAccountEnabled(Boolean subAccountEnabled) {
        this.subAccountEnabled = subAccountEnabled;
    }

    public List<PaDistributionMappingHist> getDistributionMapping() {
        return distributionMapping;
    }

    public void setDistributionMapping(List<PaDistributionMappingHist> distributionMapping) {
        this.distributionMapping = distributionMapping;
    }

    public List<PartnerExtVM> getExtension() {
        return extension;
    }

    public void setExtension(List<PartnerExtVM> extension) {
        this.extension = extension;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }
}
