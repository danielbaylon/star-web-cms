package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPMFLGTARightsMapping")
public class PPMFLGTARightsMapping implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "accessRightsGroupId",nullable = false)
	private PPMFLGTAAccessRightsGroup accessRightsGroup;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subAccountId",nullable = false)
    private PPMFLGTASubAccount subUser;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public PPMFLGTASubAccount getSubUser() {
		return subUser;
	}


	public void setSubUser(PPMFLGTASubAccount subUser) {
		this.subUser = subUser;
	}


	public PPMFLGTAAccessRightsGroup getAccessRightsGroup() {
		return accessRightsGroup;
	}


	public void setAccessRightsGroup(PPMFLGTAAccessRightsGroup accessRightsGroup) {
		this.accessRightsGroup = accessRightsGroup;
	}

	

	
	
}
