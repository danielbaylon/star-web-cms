package com.enovax.star.cms.commons.constant.ppmflg;

public enum PartnerStatus {
    PendingReSubmit("PendingReSubmit","Resubmission Required"),
    PendingVerify("PendingVerify","Pending Verify"),
    Pending("Pending","Pending"),
    Active("Active","Active"),
    Rejected("Rejected","Rejected"),
    Suspended("Suspended","Suspended"),
    Ceased("Ceased","Ceased"),
    ;

    public final String code;
    public final String label;

    private PartnerStatus(String code, String label) {
        this.code = code;
        this.label = label;
    }
    public static String getLabel(String code){
        for(PartnerStatus tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
}
