package com.enovax.star.cms.commons.model.product;

public class ProductItemCriteria {

    /**
     * Product Code
     */
    private String itemId; //productCode
    /**
     * Listing ID
     */
    private String productId; //listingId

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
