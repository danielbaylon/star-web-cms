
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetB2BPINViewResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINViewResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getB2BPINViewResult"
})
@XmlRootElement(name = "GetB2BPINViewResponse", namespace = "http://tempuri.org/")
public class GetB2BPINViewResponse {

    @XmlElementRef(name = "GetB2BPINViewResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINViewResponse> getB2BPINViewResult;

    /**
     * Gets the value of the getB2BPINViewResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINViewResponse> getGetB2BPINViewResult() {
        return getB2BPINViewResult;
    }

    /**
     * Sets the value of the getB2BPINViewResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}
     *     
     */
    public void setGetB2BPINViewResult(JAXBElement<SDCB2BPINViewResponse> value) {
        this.getB2BPINViewResult = value;
    }

}
