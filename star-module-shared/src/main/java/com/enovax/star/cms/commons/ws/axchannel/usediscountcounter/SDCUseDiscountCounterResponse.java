
package com.enovax.star.cms.commons.ws.axchannel.usediscountcounter;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_UseDiscountCounterResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_UseDiscountCounterResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="useDiscountCounterEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_UseDiscountCounterTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_UseDiscountCounterResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "useDiscountCounterEntity"
})
public class SDCUseDiscountCounterResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "useDiscountCounterEntity", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCUseDiscountCounterTable> useDiscountCounterEntity;

    /**
     * Gets the value of the useDiscountCounterEntity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUseDiscountCounterTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCUseDiscountCounterTable> getUseDiscountCounterEntity() {
        return useDiscountCounterEntity;
    }

    /**
     * Sets the value of the useDiscountCounterEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUseDiscountCounterTable }{@code >}
     *     
     */
    public void setUseDiscountCounterEntity(JAXBElement<ArrayOfSDCUseDiscountCounterTable> value) {
        this.useDiscountCounterEntity = value;
    }

}
