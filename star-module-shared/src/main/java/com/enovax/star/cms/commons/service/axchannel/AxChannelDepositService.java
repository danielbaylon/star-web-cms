package com.enovax.star.cms.commons.service.axchannel;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerBalances;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxCustomerTransactionDetails;
import com.enovax.star.cms.commons.model.axchannel.customerext.AxDepositRequest;
import com.enovax.star.cms.commons.model.axchannel.customerext.transformer.AxCustomerServiceTransformer;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.ws.axchannel.customerext.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AxChannelDepositService {

    private Logger log = LoggerFactory.getLogger(AxChannelDepositService.class);

    @Autowired
    private AxChannelServiceRegistry axChannelServiceRegistry;

    public void setAxChannelServiceRegistry(AxChannelServiceRegistry axChannelServiceRegistry) { this.axChannelServiceRegistry = axChannelServiceRegistry; }

    /**
     * Topup customer's deposit balance in AX. <br><br>
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: TopUpCustomerDeposit
     */
    public ApiResult<String> apiTopupDeposit(StoreApiChannels channel, AxDepositRequest request) throws AxChannelException {
        ObjectFactory objectFactory = new ObjectFactory();
        DepositDetails depositDetails = objectFactory.createDepositDetails();
        depositDetails.setAccountNum(objectFactory.createDepositDetailsAccountNum(request.getAccountNum()));
        depositDetails.setAmount(request.getAmount());
        depositDetails.setCurrencyCode(objectFactory.createDepositDetailsCurrencyCode(request.getCurrencyCode()));
        if(request.getDocumentDate() != null){
            depositDetails.setDocumentDate(objectFactory.createDepositDetailsDocumentDate(NvxDateUtils.formatDate(request.getDocumentDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT)));
        }
        if(request.getDocumentNum() != null){
            depositDetails.setDocumentNum(objectFactory.createDepositDetailsDocumentNum(request.getDocumentNum()));
        }
        if(request.getDueDate() != null){
            depositDetails.setDueDate(objectFactory.createDepositDetailsDueDate(NvxDateUtils.formatDate(request.getDueDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT)));
        }
        if(request.getPostingDate() != null){
            depositDetails.setPostingDate(objectFactory.createDepositDetailsPostingDate(NvxDateUtils.formatDate(request.getPostingDate(), NvxDateUtils.AX_CUSTOMER_EXT_SERVICE_DATE_TIME_FORMAT)));
        }
        if(request.getTxt() != null){
            depositDetails.setTxt(objectFactory.createDepositDetailsTxt(request.getTxt()));
        }
        if(request.getTenderTypeId() != null){
            depositDetails.setTenderTypeId(objectFactory.createDepositDetailsTenderTypeId(request.getTenderTypeId()));
        }
        String threadId = "AxChannelDepositService - apiTopupDeposit - "+Thread.currentThread().getId()+" - ";
        try{
            log.info(threadId + "Request : "+channel.name()+" - started -  "+ JsonUtil.jsonify(request));
        }catch (Exception ex){
            log.error(ex.getMessage(), ex);
        }catch (Throwable th){
            log.error(th.getMessage(), th);
        }
        depositDetails.setChannelId(0l);// set as zero value here, API will take care of the channel id
        AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCCustomerService custExt = channelStub.getStubCustomerExt();
        SDCTopUpCustomerDepositResponse response = custExt.topUpCustomerDeposit(depositDetails);
        log.info(threadId + "Response  : "+channel.name()+" - finished");
        ApiResult<String> result = AxCustomerServiceTransformer.fromWsTopupDeposit(response);
        return result;
    }

    /**
     * Gets the deposit balance and credit limit information of a customer. <br><br>
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: GetCustomerBalance
     */
    public ApiResult<AxCustomerBalances> apiGetCustomerBalance(StoreApiChannels channel, String axAccountNumber, String currencyCode) throws AxChannelException {
        AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCCustomerService custSrv = channelStub.getStubCustomerExt();

        String threadId = "AxChannelDepositService - apiGetCustomerBalance - "+Thread.currentThread().getId()+" - ";
        log.info(threadId + "Request : "+channel.name()+" - "+ axAccountNumber + " - "+currencyCode+" - started ");
        SDCCustomerBalanceResponse response = custSrv.getCustomerBalance(axAccountNumber, currencyCode);
        log.info(threadId + "Response : "+channel.name()+" - finished");
        return AxCustomerServiceTransformer.fromWsGetCustomerBalance(response);
    }

    /**
     * Gets the customer deposit transaction history. <br><br>
     *
     * WS: /services/SDC_CustomerExtService.svc <br>
     * API: ViewCustomerBalance
     */
    public ApiResult<List<AxCustomerTransactionDetails>> apiViewDepositTransactions(StoreApiChannels channel, String axAccountNumber, String currencyCode, Date fromDate, Date toDate) throws AxChannelException, DatatypeConfigurationException {
        AxChannelServiceMainStub channelStub = axChannelServiceRegistry.getChannelStub(channel);
        ISDCCustomerService custSrv = channelStub.getStubCustomerExt();

        XMLGregorianCalendar fromDateObj = null;
        XMLGregorianCalendar toDateObj   = null;
        if(fromDate != null){
            fromDateObj = convertToViewDepositTransactionDate(fromDate);
        }
        if(toDate != null){
            toDateObj = convertToViewDepositTransactionDate(toDate);
        }
        String threadId = "AxChannelDepositService - apiViewDepositTransactions - "+Thread.currentThread().getId()+" - ";
        log.info(threadId + "Request : "+channel.name()+" - "+ axAccountNumber + " - "+currencyCode + " - "+ (fromDate != null ? fromDate.toString() : "") + " - " + (toDate != null ? toDate.toString() : "") +" - started ");
        SDCViewCustomerTransactionsResponse response = custSrv.viewCustomerTransactions(axAccountNumber, currencyCode, fromDateObj, toDateObj);
        log.info(threadId + "Response : "+channel.name()+" - finished");
        return AxCustomerServiceTransformer.fromWsViewDepositTransactions(response);
    }

    private XMLGregorianCalendar convertToViewDepositTransactionDate(Date date) throws DatatypeConfigurationException {
        /***
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        ***/
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1, //need to add the month
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED);
    }

}
