package com.enovax.star.cms.commons.model.partner.ppmflg;
import com.enovax.star.cms.commons.model.booking.partner.PartnerFunCartEmailDisplay;

public class OfflinePaymentResultVM {

	private boolean canManage;
	private boolean canApprove;

	private PartnerFunCartEmailDisplay cart;
	private OfflinePaymentRequestVM paymentDetail;

	public PartnerFunCartEmailDisplay getCart() {
		return cart;
	}

	public void setCart(PartnerFunCartEmailDisplay cart) {
		this.cart = cart;
	}

	public OfflinePaymentRequestVM getPaymentDetail() {
		return paymentDetail;
	}

	public void setPaymentDetail(OfflinePaymentRequestVM paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public boolean isCanApprove() {
		return canApprove;
	}

	public void setCanApprove(boolean canApprove) {
		this.canApprove = canApprove;
	}

	public boolean isCanManage() {
		return canManage;
	}

	public void setCanManage(boolean canManage) {
		this.canManage = canManage;
	}
}
