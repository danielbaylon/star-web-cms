package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxNumberSequenceSeedData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.NumberSequenceSeedData)
 * 
 * @author Justin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskGetLatestNumberSequenceResponse {

    @JsonProperty("value")
    private List<AxNumberSequenceSeedData> axNumberSequenceSeedDataList = new ArrayList<AxNumberSequenceSeedData>();

    public List<AxNumberSequenceSeedData> getAxNumberSequenceSeedDataList() {
        return axNumberSequenceSeedDataList;
    }

    public void setAxNumberSequenceSeedDataList(List<AxNumberSequenceSeedData> axNumberSequenceSeedDataList) {
        this.axNumberSequenceSeedDataList = axNumberSequenceSeedDataList;
    }

}
