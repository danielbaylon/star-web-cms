package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPaDistributionMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGPaDistributionMappingRepository extends JpaRepository<PPMFLGPaDistributionMapping, Integer> {

    List<PPMFLGPaDistributionMapping> findByPartnerId(Integer id);

    PPMFLGPaDistributionMapping findFirstByAdminIdAndPartnerIdAndCountryId(String adminId, Integer partnerId, String countryId);

    @Query("from PPMFLGPaDistributionMapping where partnerId in ?1")
    List<PPMFLGPaDistributionMapping> findMappingbyPartnerIds(@Param("partnerIds") List<Integer> partnerIds);
}
