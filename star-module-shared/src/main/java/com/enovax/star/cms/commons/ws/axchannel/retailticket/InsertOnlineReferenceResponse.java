
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="insertOnlineReferenceResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_InsertOnlineReferenceResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertOnlineReferenceResult"
})
@XmlRootElement(name = "insertOnlineReferenceResponse")
public class InsertOnlineReferenceResponse {

    @XmlElementRef(name = "insertOnlineReferenceResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCInsertOnlineReferenceResponse> insertOnlineReferenceResult;

    /**
     * Gets the value of the insertOnlineReferenceResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCInsertOnlineReferenceResponse }{@code >}
     *     
     */
    public JAXBElement<SDCInsertOnlineReferenceResponse> getInsertOnlineReferenceResult() {
        return insertOnlineReferenceResult;
    }

    /**
     * Sets the value of the insertOnlineReferenceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCInsertOnlineReferenceResponse }{@code >}
     *     
     */
    public void setInsertOnlineReferenceResult(JAXBElement<SDCInsertOnlineReferenceResponse> value) {
        this.insertOnlineReferenceResult = value;
    }

}
