package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccessRight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 1/11/17.
 */
@Repository
public interface PPSLMTAAccessRightRepository extends JpaRepository<PPSLMTAAccessRight, Integer> {

    @Query(value = "select * from PPSLMTAAccessRight e where e.id in( " +
            " select d.accessRightId from PPSLMTAAccessRightsGroup c, PPSLMTAAccessRightsGroupMapping d where c.id in( " +
            " select b.accessRightsGroupId from PPSLMTAMainAccount a, PPSLMTAMainRightsMapping b where b.mainAccountId=a.id and a.username= :username) " +
            "     and c.id = d.accessRightsGroupId) ", nativeQuery = true)
    List<PPSLMTAAccessRight> getMainUserAccessRights(@Param("username") String username);


    @Query(value = "select * from PPSLMTAAccessRight e where e.id in( " +
            " select d.accessRightId from PPSLMTAAccessRightsGroup c, PPSLMTAAccessRightsGroupMapping d where c.id in( " +
            " select b.accessRightsGroupId from PPSLMTaSubAccount a, PPSLMTARightsMapping b where b.subAccountId =a.id and a.username = :username) " +
            "     and c.id = d.accessRightsGroupId) ", nativeQuery = true)
    List<PPSLMTAAccessRight> getSubUserAccessRights(@Param("username") String username);
}
