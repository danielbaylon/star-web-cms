package com.enovax.star.cms.commons.model.partner.ppslm;

/**
 * Created by houtao on 7/9/16.
 */
public class WoTEmailVM {

    private String baseUrl;
    private String baseAppUrl;
    private String baseAppLogoUrl;
    private String partnerName;
    private String ticketEventPinCode;
    private String ticketBarcodeImageString;
    private String ticketEventDateForPrint;
    private String ticketEventCutoffTimeForPrint;
    private String ticketEventStartTime;
    private String ticketEventTime;
    private String ticketEventEndTime;
    private String ticketEventName;
    private String ticketQty;
    private String ticketStatus;
    private String ticketRemarks;
    private String wotGeneralInfo;
    private String ticketEventStartDateAndTime;


    private String backendReservationReleaseDate;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getTicketEventPinCode() {
        return ticketEventPinCode;
    }

    public void setTicketEventPinCode(String ticketEventPinCode) {
        this.ticketEventPinCode = ticketEventPinCode;
    }

    public String getTicketBarcodeImageString() {
        return ticketBarcodeImageString;
    }

    public void setTicketBarcodeImageString(String ticketBarcodeImageString) {
        this.ticketBarcodeImageString = ticketBarcodeImageString;
    }

    public String getTicketEventDateForPrint() {
        return ticketEventDateForPrint;
    }

    public void setTicketEventDateForPrint(String ticketEventDateForPrint) {
        this.ticketEventDateForPrint = ticketEventDateForPrint;
    }

    public String getTicketEventStartTime() {
        return ticketEventStartTime;
    }

    public void setTicketEventStartTime(String ticketEventStartTime) {
        this.ticketEventStartTime = ticketEventStartTime;
    }

    public String getTicketEventEndTime() {
        return ticketEventEndTime;
    }

    public void setTicketEventEndTime(String ticketEventEndTime) {
        this.ticketEventEndTime = ticketEventEndTime;
    }

    public String getTicketEventName() {
        return ticketEventName;
    }

    public void setTicketEventName(String ticketEventName) {
        this.ticketEventName = ticketEventName;
    }

    public String getTicketQty() {
        return ticketQty;
    }

    public void setTicketQty(String ticketQty) {
        this.ticketQty = ticketQty;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicketRemarks() {
        return ticketRemarks;
    }

    public void setTicketRemarks(String ticketRemarks) {
        this.ticketRemarks = ticketRemarks;
    }

    public String getWotGeneralInfo() {
        return wotGeneralInfo;
    }

    public void setWotGeneralInfo(String wotGeneralInfo) {
        this.wotGeneralInfo = wotGeneralInfo;
    }

    public String getTicketEventCutoffTimeForPrint() {
        return ticketEventCutoffTimeForPrint;
    }

    public void setTicketEventCutoffTimeForPrint(String ticketEventCutoffTimeForPrint) {
        this.ticketEventCutoffTimeForPrint = ticketEventCutoffTimeForPrint;
    }

    public String getBackendReservationReleaseDate() {
        return backendReservationReleaseDate;
    }

    public void setBackendReservationReleaseDate(String backendReservationReleaseDate) {
        this.backendReservationReleaseDate = backendReservationReleaseDate;
    }

    public String getTicketEventStartDateAndTime() {
        return ticketEventStartDateAndTime;
    }

    public void setTicketEventStartDateAndTime(String ticketEventStartDateAndTime) {
        this.ticketEventStartDateAndTime = ticketEventStartDateAndTime;
    }

    public String getTicketEventTime() {
        return ticketEventTime;
    }

    public void setTicketEventTime(String ticketEventTime) {
        this.ticketEventTime = ticketEventTime;
    }

    public String getBaseAppUrl() {
        return baseAppUrl;
    }

    public void setBaseAppUrl(String baseAppUrl) {
        this.baseAppUrl = baseAppUrl;
    }

    public String getBaseAppLogoUrl() {
        return baseAppLogoUrl;
    }

    public void setBaseAppLogoUrl(String baseAppLogoUrl) {
        this.baseAppLogoUrl = baseAppLogoUrl;
    }
}
