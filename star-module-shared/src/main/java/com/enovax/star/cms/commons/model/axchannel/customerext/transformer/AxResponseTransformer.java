package com.enovax.star.cms.commons.model.axchannel.customerext.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.ws.axchannel.customerext.ArrayOfResponseError;
import com.enovax.star.cms.commons.ws.axchannel.customerext.ResponseError;
import com.enovax.star.cms.commons.ws.axchannel.customerext.ServiceResponse;

import javax.xml.bind.JAXBElement;
import java.util.List;

/**
 * Created by houtao on 18/8/16.
 */
public class AxResponseTransformer {

    public static ApiResult checkDefaultResponse(ApiResult axResponse, ServiceResponse response) {
        JAXBElement<ArrayOfResponseError> errorArrayResponse = response.getErrors();
        if(errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (ResponseError rr : responseErrorList) {
                        if(!(rr != null)){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }

}
