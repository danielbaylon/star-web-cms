package com.enovax.star.cms.commons.model.product.promotions;

/**
 * Created by jensen on 25/6/16.
 */
public class PromotionRelatedAffiliation {

    private String recordId;
    private String systemName;
    private String systemDescription;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemDescription() {
        return systemDescription;
    }

    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }
}
