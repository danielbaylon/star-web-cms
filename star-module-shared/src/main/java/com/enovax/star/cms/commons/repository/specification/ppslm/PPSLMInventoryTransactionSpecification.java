package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 17/8/16.
 */
public class PPSLMInventoryTransactionSpecification extends BaseSpecification<PPSLMInventoryTransaction> {
}
