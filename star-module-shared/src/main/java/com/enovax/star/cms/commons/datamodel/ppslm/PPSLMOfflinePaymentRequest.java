package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PPSLMOfflinePaymentRequest")
public class PPSLMOfflinePaymentRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "transactionId")
    private Integer transactionId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPSLMInventoryTransaction inventoryTransaction;

	@Column(name = "mainAccountId", nullable = false)
	private Integer mainAccountId;
    
    @Column(name = "paymentType")
    private String paymentType;
    
    @Column(name = "referenceNum")
    private String referenceNum;
    
    @Column(name = "bankName")
    private String bankName;
//
//	@Column(name = "bankCountryCode")
//	private String bankCountryCode;

	@Column(name = "bankCountry")
	private String bankCountry;

    @Column(name = "status")
    private String status;
    
    @Column(name = "detail")
    private String detail;
    
    @Column(name = "approverRemarks")
    private String approverRemarks;
    
    @Column(name = "remarks")
    private String remarks;
    
    @Column(name = "approver")
    private String approver;
    
    @Column(name = "approveDate")
    private Date approveDate;
    
    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "createdDate")
    private Date createdDate;
    
    @Column(name = "submitDate")
    private Date submitDate;
    
    @Column(name = "submitBy")
    private String submitBy;

	@Column(name = "paymentDate")
	private Date paymentDate;

	@Column(name = "finalApprovalRemarks")
	private String finalApprovalRemarks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public PPSLMInventoryTransaction getInventoryTransaction() {
		return inventoryTransaction;
	}

	public void setInventoryTransaction(PPSLMInventoryTransaction inventoryTransaction) {
		this.inventoryTransaction = inventoryTransaction;
	}
//
//	public Integer getMainAccountId() {
//		return mainAccountId;
//	}
//
//	public void setMainAccountId(Integer mainAccountId) {
//		this.mainAccountId = mainAccountId;
//	}

	public String getApproverRemarks() {
		return approverRemarks;
	}

	public void setApproverRemarks(String approverRemarks) {
		this.approverRemarks = approverRemarks;
	}

	public String getReferenceNum() {
		return referenceNum;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCountry() {
		return bankCountry;
	}

	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getSubmitBy() {
		return submitBy;
	}

	public void setSubmitBy(String submitBy) {
		this.submitBy = submitBy;
	}


	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getFinalApprovalRemarks() {
		return finalApprovalRemarks;
	}

	public void setFinalApprovalRemarks(String finalApprovalRemarks) {
		this.finalApprovalRemarks = finalApprovalRemarks;
	}

	public Integer getMainAccountId() {
		return mainAccountId;
	}

	public void setMainAccountId(Integer mainAccountId) {
		this.mainAccountId = mainAccountId;
	}

//	public String getBankCountryCode() {
//		return bankCountryCode;
//	}
//
//	public void setBankCountryCode(String bankCountryCode) {
//		this.bankCountryCode = bankCountryCode;
//	}
}
