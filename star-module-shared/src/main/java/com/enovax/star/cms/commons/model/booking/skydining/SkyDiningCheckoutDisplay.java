package com.enovax.star.cms.commons.model.booking.skydining;

import com.enovax.star.cms.commons.model.booking.CheckoutDisplay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningCheckoutDisplay extends CheckoutDisplay {

  private List<SkyDiningFunCartDisplayProduct> skyDiningProducts = new ArrayList<>();

  public List<SkyDiningFunCartDisplayProduct> getSkyDiningProducts() {
    return skyDiningProducts;
  }

  public void setSkyDiningProducts(List<SkyDiningFunCartDisplayProduct> skyDiningProducts) {
    this.skyDiningProducts = skyDiningProducts;
  }
}
