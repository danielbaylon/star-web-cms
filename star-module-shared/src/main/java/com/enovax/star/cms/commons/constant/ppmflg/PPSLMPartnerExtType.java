package com.enovax.star.cms.commons.constant.ppmflg;

public enum PPSLMPartnerExtType {

    WingsOfTimePurchaseEnabled("wotReservationEnabled");

    public final String code;

    private PPSLMPartnerExtType(String code) {
        this.code = code;
    }
}
