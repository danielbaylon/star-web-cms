
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_B2BPINViewLineTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_B2BPINViewLineTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_B2BPINViewLineTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_B2BPINViewLineTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_B2BPINViewLineTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcb2BPINViewLineTable"
})
public class ArrayOfSDCB2BPINViewLineTable {

    @XmlElement(name = "SDC_B2BPINViewLineTable", nillable = true)
    protected List<SDCB2BPINViewLineTable> sdcb2BPINViewLineTable;

    /**
     * Gets the value of the sdcb2BPINViewLineTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcb2BPINViewLineTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCB2BPINViewLineTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCB2BPINViewLineTable }
     * 
     * 
     */
    public List<SDCB2BPINViewLineTable> getSDCB2BPINViewLineTable() {
        if (sdcb2BPINViewLineTable == null) {
            sdcb2BPINViewLineTable = new ArrayList<SDCB2BPINViewLineTable>();
        }
        return this.sdcb2BPINViewLineTable;
    }

}
