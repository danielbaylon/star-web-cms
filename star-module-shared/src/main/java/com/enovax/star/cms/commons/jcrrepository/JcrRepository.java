package com.enovax.star.cms.commons.jcrrepository;

import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import info.magnolia.cms.security.SilentSessionOp;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.SimpleContext;
import info.magnolia.context.SystemContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.qom.*;
import java.util.Map;

/**
 * Created by jennylynsze on 3/17/16.
 */
public class JcrRepository {

    protected static final Logger log = LoggerFactory.getLogger(JcrRepository.class);

    private static Session getSession(final String workspace) throws RepositoryException {
        try {
            return MgnlContext.getJCRSession(workspace);
        } catch (Exception e) {
            MgnlContext.setInstance(new SimpleContext((Map) Components.getComponent(SystemContext.class)));
            return MgnlContext.doInSystemContext(new SilentSessionOp<Session>(workspace) {
                @Override
                public Session doExec(Session session) throws Throwable {
                    return session;
                }
            });
        }
    }

    public static Iterable<Node> query(String workspace, String type, String statement) throws RepositoryException {
        return query(workspace, type, statement, false);
    }

    public static Iterable<Node> query(String workspace, String type, String statement, boolean isSelector) throws RepositoryException {
        Session session = getSession(workspace);
        QueryManager manager = session.getWorkspace().getQueryManager();
        Query query = manager.createQuery(statement, "xpath");


        QueryResult result = query.execute();
        if (isSelector) {
            return NodeUtil.asIterable(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getRows(), type)));
        }
        return NodeUtil.asIterable(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), type)));
    }

    public static Iterable<Node> getNodes(final JcrWorkspace workspace) {
        return getNodes(workspace, null);
    }

    public static Iterable<Node> getNodes(final JcrWorkspace workspace, final String orderBy) {
        final String type = JcrWorkspace.getNodeTypeFromWorkspace(workspace);
        String jcrQuery = String.format("//element(*,%s)", type);

        if(!StringUtils.isEmpty(orderBy)) {
            jcrQuery = jcrQuery + " order by @" + orderBy;
        }

        try {
            return query(workspace.getWorkspaceName(), type, jcrQuery);
        } catch (RepositoryException e) {
            log.warn("CMS JCR query error [{}].", jcrQuery, e);
            return null;
        }
    }

    public static Node getParentNode(String workspaceName, String path)throws RepositoryException {
        final Session session = getSession(workspaceName);
        final Node pathNode = session.getNode(path);
        return pathNode;
    }

    public static Node createNode(String workspaceName, String path, String nodeName, String nodeType) throws RepositoryException {
        final Session session = getSession(workspaceName);
        final Node newNode;

        if(!StringUtils.isEmpty(path) && !path.endsWith("/")) {
            path = path + "/";
        }

        if (!session.nodeExists(path + nodeName)) {
            final Node pathNode = session.getNode(path);
            newNode = pathNode.addNode(nodeName,nodeType);
            session.save();  //save the JCR
            return newNode;
        }

        return null;
    }

    public static void updateNode(String workspaceName, Node node) throws RepositoryException  {
        final Session session = getSession(workspaceName);
        session.save();
    }

    public static boolean nodeExists(String workspaceName, String path) throws RepositoryException {
        final Session session = getSession(workspaceName);
        return session.nodeExists(path);
    }


    public static QueryObjectModelFactory getQOMFactory(String workspace) throws RepositoryException {

        Session session = getSession(workspace);

        return session.getWorkspace().getQueryManager().getQOMFactory();

    }


    public static Iterable<Node> execute(QueryObjectModelFactory factory, String nodeType, Source source, Constraint constraint) throws RepositoryException {

        return execute(factory, nodeType, source, constraint, null, null);

    }



    public static Iterable<Node> execute(QueryObjectModelFactory factory, String nodeType, Source source, Constraint constraint, Ordering[] orderings) throws RepositoryException {

        return execute(factory, nodeType, source, constraint, orderings, null);

    }



    public static Iterable<Node> execute(QueryObjectModelFactory factory, String nodeType, Source source, Constraint constraint, Ordering[] orderings, Column[] columns) throws RepositoryException {

        QueryObjectModel query = factory.createQuery(source, constraint, orderings, columns);

        QueryResult result = query.execute();

        return NodeUtil.asIterable(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), nodeType)));

    }

    public static Iterable<Node> execute(QueryObjectModelFactory factory, String nodeType, Source source, Constraint constraint, Ordering[] orderings, Column[] columns, long limit, long offset) throws RepositoryException {

        QueryObjectModel query = factory.createQuery(source, constraint, orderings, columns);

        query.setLimit(limit);
        query.setOffset(offset);
        QueryResult result = query.execute();

        return NodeUtil.asIterable(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), nodeType)));

    }


}
