package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavanya on 7/11/16.
 */
public enum TransMegaStatus {
    Successful,
    Failed,
    Incomplete,
    Refunded;

    public static List<String> getChildStatuses(TransMegaStatus megaStatus) {
        final List<String> items = new ArrayList<>();
        switch (megaStatus) {
            case Successful:
                items.add(TicketStatus.Available.toString());
                items.add(TicketStatus.Revalidated.toString());
                items.add(TicketStatus.Refunded.toString());
                items.add(TicketStatus.Expired.toString());
                items.add(TicketStatus.Expiring.toString());
                items.add(TicketStatus.Forfeited.toString());
                break;
            case Failed:
                items.add(TicketStatus.Failed.toString());
                break;
            case Refunded:
                items.add(TicketStatus.Refunded.toString());
                break;
            case Incomplete:
                items.add(TicketStatus.Reserved.toString());
                items.add(TicketStatus.Incomplete.toString());
                break;
        }
        return items;
    }
}
