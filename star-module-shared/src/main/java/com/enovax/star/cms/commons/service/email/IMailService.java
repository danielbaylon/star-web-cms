package com.enovax.star.cms.commons.service.email;

public interface IMailService {

    Mailer getMailer();

    String getDefaultSender();

    void setDefaultSender(String sender);

    void sendEmail(MailProperties mailProps, MailCallback mailCallback) throws Exception;
    
    MailProperties prepareMailProps(String trackingId, String subject, String body, boolean isBodyHtml, String[] toUsers);

}
