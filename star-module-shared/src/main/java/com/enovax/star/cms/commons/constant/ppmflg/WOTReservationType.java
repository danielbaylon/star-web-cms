package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 6/25/16.
 */
public enum WOTReservationType {
    Purchase("Purchase"),
    Reserve("Reserve");

    public String description;

    WOTReservationType(String description) {
        this.description = description;
    }
}
