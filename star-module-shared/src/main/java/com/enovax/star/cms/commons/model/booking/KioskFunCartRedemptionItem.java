package com.enovax.star.cms.commons.model.booking;

public class KioskFunCartRedemptionItem extends FunCartItem {

	// Override
	// LineId
	// Qty
	// EventGroupId
	// EventLineId
	// name
	// printing

	private String expiryDate; // String

	private String inventTransId;

	private String invoiceId;

	private String isCombined;

	private String itemId;
	private String mediaTypeId;
	private String openValidityId;
	private String originalQty;
	private String qtyRedeemed;
	private String referenceId;
	private String refRecId;
	private String salesId;
	private String sessionNo;
	private String sourceRecId;
	private String startDateTime;
	private String ticketDate;

	private String ticketDateParm;
	private String userId;
	private String eventDate;
	private String noOfPax;
	private String defineTicketDate;

	private KioskRedemptionEventGroupList eventGroupList;

	private String isCapacity;
	private String allowChangeEventDate;

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getInventTransId() {
		return inventTransId;
	}

	public void setInventTransId(String inventTransId) {
		this.inventTransId = inventTransId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getIsCombined() {
		return isCombined;
	}

	public void setIsCombined(String isCombined) {
		this.isCombined = isCombined;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getMediaTypeId() {
		return mediaTypeId;
	}

	public void setMediaTypeId(String mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}

	public String getOpenValidityId() {
		return openValidityId;
	}

	public void setOpenValidityId(String openValidityId) {
		this.openValidityId = openValidityId;
	}

	public String getOriginalQty() {
		return originalQty;
	}

	public void setOriginalQty(String originalQty) {
		this.originalQty = originalQty;
	}

	public String getQtyRedeemed() {
		return qtyRedeemed;
	}

	public void setQtyRedeemed(String qtyRedeemed) {
		this.qtyRedeemed = qtyRedeemed;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRefRecId() {
		return refRecId;
	}

	public void setRefRecId(String refRecId) {
		this.refRecId = refRecId;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(String sessionNo) {
		this.sessionNo = sessionNo;
	}

	public void setSourceRecId(String sourceRecId) {
		this.sourceRecId = sourceRecId;
	}
	
	public String getSourceRecId() {
		return sourceRecId;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(String ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getTicketDateParm() {
		return ticketDateParm;
	}

	public void setTicketDateParm(String ticketDateParm) {
		this.ticketDateParm = ticketDateParm;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(String noOfPax) {
		this.noOfPax = noOfPax;
	}

	public String getDefineTicketDate() {
		return defineTicketDate;
	}

	public void setDefineTicketDate(String defineTicketDate) {
		this.defineTicketDate = defineTicketDate;
	}

	public KioskRedemptionEventGroupList getEventGroupList() {
		return eventGroupList;
	}

	public void setEventGroupList(KioskRedemptionEventGroupList eventGroupList) {
		this.eventGroupList = eventGroupList;
	}

	public String getIsCapacity() {
		return isCapacity;
	}

	public void setIsCapacity(String isCapacity) {
		this.isCapacity = isCapacity;
	}

	public String getAllowChangeEventDate() {
		return allowChangeEventDate;
	}

	public void setAllowChangeEventDate(String allowChangeEventDate) {
		this.allowChangeEventDate = allowChangeEventDate;
	}

}
