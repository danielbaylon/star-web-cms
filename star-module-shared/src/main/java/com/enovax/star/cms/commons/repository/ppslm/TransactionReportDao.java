package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;
import com.enovax.star.cms.commons.model.partner.ppslm.TransReportModel;

/**
 * Created by lavanya on 8/11/16.
 */
public interface TransactionReportDao {
    public ReportResult<TransReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                         String orderBy, boolean asc, boolean isCount) throws Exception;
}
