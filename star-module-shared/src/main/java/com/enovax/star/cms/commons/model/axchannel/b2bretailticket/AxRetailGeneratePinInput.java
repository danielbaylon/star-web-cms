package com.enovax.star.cms.commons.model.axchannel.b2bretailticket;

import com.enovax.star.cms.commons.constant.axchannel.AxPinType;

import java.math.BigDecimal;
import java.util.Date;

public class AxRetailGeneratePinInput {

    private boolean allowPartialRedemption = false;
    private String customerAccount;
    private String description = "";
    private Date endDateTime;
    private Date eventDate;
    private String eventGroupId = "";
    private String eventLineId = "";
    private String inventTransId;
    private String invoiceId;
    private boolean combineTicket = false;
    private boolean groupTicket = false;
    private String itemId;
    private BigDecimal lineNumber = BigDecimal.ZERO;
    private String mediaType;
    private String packageName;
    private BigDecimal qty;
    private BigDecimal qtyPerProduct;
    private String referenceId;
    private String retailVariantId = "";
    private String salesId;
    private Date startDateTime;
    private String transactionId;

    private String guestName = "";
    private String ccLast4Digits;
    private AxPinType pinType;

    public boolean isAllowPartialRedemption() {
        return allowPartialRedemption;
    }

    public void setAllowPartialRedemption(boolean allowPartialRedemption) {
        this.allowPartialRedemption = allowPartialRedemption;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getInventTransId() {
        return inventTransId;
    }

    public void setInventTransId(String inventTransId) {
        this.inventTransId = inventTransId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public boolean isCombineTicket() {
        return combineTicket;
    }

    public void setCombineTicket(boolean combineTicket) {
        this.combineTicket = combineTicket;
    }

    public boolean isGroupTicket() {
        return groupTicket;
    }

    public void setGroupTicket(boolean groupTicket) {
        this.groupTicket = groupTicket;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(BigDecimal lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getQtyPerProduct() {
        return qtyPerProduct;
    }

    public void setQtyPerProduct(BigDecimal qtyPerProduct) {
        this.qtyPerProduct = qtyPerProduct;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public AxPinType getPinType() {
        return pinType;
    }

    public void setPinType(AxPinType pinType) {
        this.pinType = pinType;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getCcLast4Digits() {
        return ccLast4Digits;
    }

    public void setCcLast4Digits(String ccLast4Digits) {
        this.ccLast4Digits = ccLast4Digits;
    }
}
