package com.enovax.star.cms.commons.model.axchannel.inventtable;

/**
 * Created by jennylynsze on 12/9/16.
 */
public class AxRetailTemplateXML {
    String templateName;
    String templateDescription;
    String xmlDocument;

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getXmlDocument() {
        return xmlDocument;
    }

    public void setXmlDocument(String xmlDocument) {
        this.xmlDocument = xmlDocument;
    }
}
