
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventGroupLineEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventGroupLineEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataAreaID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventAllocationCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfEventAllocation" minOccurs="0"/>
 *         &lt;element name="EventCapacityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventEndTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EventGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventLineId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventStartTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EventTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RecID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="RecVersion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventGroupLineEntity", propOrder = {
    "dataAreaID",
    "eventAllocationCollection",
    "eventCapacityId",
    "eventEndTime",
    "eventGroupId",
    "eventLineId",
    "eventName",
    "eventStartTime",
    "eventTime",
    "recID",
    "recVersion"
})
public class EventGroupLineEntity {

    @XmlElementRef(name = "DataAreaID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataAreaID;
    @XmlElementRef(name = "EventAllocationCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEventAllocation> eventAllocationCollection;
    @XmlElementRef(name = "EventCapacityId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventCapacityId;
    @XmlElement(name = "EventEndTime")
    protected Integer eventEndTime;
    @XmlElementRef(name = "EventGroupId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventGroupId;
    @XmlElementRef(name = "EventLineId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventLineId;
    @XmlElementRef(name = "EventName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventName;
    @XmlElement(name = "EventStartTime")
    protected Integer eventStartTime;
    @XmlElement(name = "EventTime")
    protected Integer eventTime;
    @XmlElement(name = "RecID")
    protected Long recID;
    @XmlElement(name = "RecVersion")
    protected Integer recVersion;

    /**
     * Gets the value of the dataAreaID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataAreaID() {
        return dataAreaID;
    }

    /**
     * Sets the value of the dataAreaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataAreaID(JAXBElement<String> value) {
        this.dataAreaID = value;
    }

    /**
     * Gets the value of the eventAllocationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventAllocation }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEventAllocation> getEventAllocationCollection() {
        return eventAllocationCollection;
    }

    /**
     * Sets the value of the eventAllocationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventAllocation }{@code >}
     *     
     */
    public void setEventAllocationCollection(JAXBElement<ArrayOfEventAllocation> value) {
        this.eventAllocationCollection = value;
    }

    /**
     * Gets the value of the eventCapacityId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventCapacityId() {
        return eventCapacityId;
    }

    /**
     * Sets the value of the eventCapacityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventCapacityId(JAXBElement<String> value) {
        this.eventCapacityId = value;
    }

    /**
     * Gets the value of the eventEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEventEndTime() {
        return eventEndTime;
    }

    /**
     * Sets the value of the eventEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEventEndTime(Integer value) {
        this.eventEndTime = value;
    }

    /**
     * Gets the value of the eventGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventGroupId() {
        return eventGroupId;
    }

    /**
     * Sets the value of the eventGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventGroupId(JAXBElement<String> value) {
        this.eventGroupId = value;
    }

    /**
     * Gets the value of the eventLineId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventLineId() {
        return eventLineId;
    }

    /**
     * Sets the value of the eventLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventLineId(JAXBElement<String> value) {
        this.eventLineId = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventName(JAXBElement<String> value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEventStartTime() {
        return eventStartTime;
    }

    /**
     * Sets the value of the eventStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEventStartTime(Integer value) {
        this.eventStartTime = value;
    }

    /**
     * Gets the value of the eventTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEventTime(Integer value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the recID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecID() {
        return recID;
    }

    /**
     * Sets the value of the recID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecID(Long value) {
        this.recID = value;
    }

    /**
     * Gets the value of the recVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecVersion() {
        return recVersion;
    }

    /**
     * Sets the value of the recVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecVersion(Integer value) {
        this.recVersion = value;
    }

}
