package com.enovax.star.cms.commons.model.axchannel.upcrosssell;

import java.util.List;

/**
 * Created by jensen on 25/6/16.
 */
public class AxRetailUpCrossSellProduct {


    private boolean active;
    private Long channel;
    private String itemId;
    private Integer minimumQty;
    private Integer multipleQty;
    private Long recId;
    private Integer tableAll;
    private String unitId;
    private List<AxRetailUpCrossSellLine> lines;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getMinimumQty() {
        return minimumQty;
    }

    public void setMinimumQty(Integer minimumQty) {
        this.minimumQty = minimumQty;
    }

    public Integer getMultipleQty() {
        return multipleQty;
    }

    public void setMultipleQty(Integer multipleQty) {
        this.multipleQty = multipleQty;
    }

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }

    public Integer getTableAll() {
        return tableAll;
    }

    public void setTableAll(Integer tableAll) {
        this.tableAll = tableAll;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public List<AxRetailUpCrossSellLine> getLines() {
        return lines;
    }

    public void setLines(List<AxRetailUpCrossSellLine> lines) {
        this.lines = lines;
    }

}
