package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.Date;

/**
 * Created by jennylynsze on 5/17/16.
 */
public class MixMatchPkgItemVM {
    private int id;
    private int packageId;
    private String receiptNum;
    private int transactionItemId;
    private String displayName;
    private String displayDetails;
    private Date dateOfVisit;
    private String listingId;
    private String itemCode;
    private int qty;
    private int qtyRedeemed;
    private Integer itemId;
    private String transItemType;
    private String ticketType;
    private String itemType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public int getTransactionItemId() {
        return transactionItemId;
    }

    public void setTransactionItemId(int transactionItemId) {
        this.transactionItemId = transactionItemId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getTransItemType() {
        return transItemType;
    }

    public void setTransItemType(String transItemType) {
        this.transItemType = transItemType;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public int getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(int qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }
}
