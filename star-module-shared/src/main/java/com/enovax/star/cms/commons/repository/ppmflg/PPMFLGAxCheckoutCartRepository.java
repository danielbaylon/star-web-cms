package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGAxCheckoutCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jennylynsze on 8/18/16.
 */
@Repository
public interface PPMFLGAxCheckoutCartRepository extends JpaRepository<PPMFLGAxCheckoutCart, Integer> {

    PPMFLGAxCheckoutCart findByReceiptNum(String receiptNum);
}
