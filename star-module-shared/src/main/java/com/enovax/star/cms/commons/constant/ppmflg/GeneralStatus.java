package com.enovax.star.cms.commons.constant.ppmflg;

public enum GeneralStatus {
    Active("A","Active"),
    Deleted("D","Deleted"),
    Inactive("I","Inactive"),
    Backend("B","Backend");

    public final String code;
    public final String label;

    private GeneralStatus(String code, String label) {
        this.code = code;
        this.label = label;
    }
    public static String getLabel(String code){
        for(GeneralStatus tempObj: values()){
            if(tempObj.code.equals(code)){
                return tempObj.label;
            }
        }
        return null;
    }
}
