package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMTASubAccountRepository extends JpaRepository<PPSLMTASubAccount, Integer> {

    List<PPSLMTASubAccount> findByUsername(String username);

    PPSLMTASubAccount  findFirstByUsername(String username);

    PPSLMTASubAccount findById(Integer id);

    List<PPSLMTASubAccount> findByMainUser(PPSLMTAMainAccount mainAccount);

    @Query("SELECT p FROM PPSLMTASubAccount p WHERE p.mainUser=:mainAccount AND p.id!=:userId ")
    List<PPSLMTASubAccount> find(@Param("mainAccount") PPSLMTAMainAccount mainAccount,@Param("userId") int userId);

    int countByIdAndStatus(Integer id, String name);
//    @Query("from PPSLMTASubAccount where status = 'A' ")
 //   int countByMainUser(PPSLMTAMainAccount mainAccount);
}
