
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_PinTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_PinTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_PinTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_PinTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_PinTable", propOrder = {
    "sdcPinTable"
})
public class ArrayOfSDCPinTable {

    @XmlElement(name = "SDC_PinTable", nillable = true)
    protected List<SDCPinTable> sdcPinTable;

    /**
     * Gets the value of the sdcPinTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcPinTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCPinTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCPinTable }
     * 
     * 
     */
    public List<SDCPinTable> getSDCPinTable() {
        if (sdcPinTable == null) {
            sdcPinTable = new ArrayList<SDCPinTable>();
        }
        return this.sdcPinTable;
    }

}
