package com.enovax.star.cms.commons.model.axchannel.inventtable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AxRetailProductExtData {

    private String absoluteCapacityId;
    private String accessId;
    private String dataAreaId;
    private String defaultEventLineId;
    private boolean defineOpenDate;
    private String eventGroupId;
    private List<AxRetailProductExtEventGroup> eventGroups = new ArrayList<>();
    private String facilityId;
    private List<AxRetailProductExtProductVariant> productVariants = new ArrayList<>();
    private boolean isCapacity;
    private boolean isPackage;
    private boolean isTransport;
    private boolean isTicketing;
    private String itemId;
    private String mediaTypeDescription;
    private String mediaTypeId;
    private boolean isMember;
    private String membershipTypeCode;
    private boolean nonReturnable;
    private Date openEndDateTime;
    private Date openStartDateTime;
    private String openValidityRuleId;
    private String operationId;
    private boolean ownAttraction;
    private String packageId;
    private List<AxRetailProductExtPackageLine> packageLines = new ArrayList<>();
    private int printerType;
    private int printing;
    private String productId;
    private String productName;
    private String recId;
    private String templateDescription;
    private String templateName;
    private String usageValidityRuleId;
    private int noOfPax;

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDefaultEventLineId() {
        return defaultEventLineId;
    }

    public void setDefaultEventLineId(String defaultEventLineId) {
        this.defaultEventLineId = defaultEventLineId;
    }

    public boolean isDefineOpenDate() {
        return defineOpenDate;
    }

    public void setDefineOpenDate(boolean defineOpenDate) {
        this.defineOpenDate = defineOpenDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public List<AxRetailProductExtEventGroup> getEventGroups() {
        return eventGroups;
    }

    public void setEventGroups(List<AxRetailProductExtEventGroup> eventGroups) {
        this.eventGroups = eventGroups;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public List<AxRetailProductExtProductVariant> getProductVariants() {
        return productVariants;
    }

    public void setProductVariants(List<AxRetailProductExtProductVariant> productVariants) {
        this.productVariants = productVariants;
    }

    public boolean isCapacity() {
        return isCapacity;
    }

    public void setCapacity(boolean capacity) {
        isCapacity = capacity;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public void setPackage(boolean aPackage) {
        isPackage = aPackage;
    }

    public boolean isTransport() {
        return isTransport;
    }

    public void setTransport(boolean transport) {
        isTransport = transport;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMediaTypeDescription() {
        return mediaTypeDescription;
    }

    public void setMediaTypeDescription(String mediaTypeDescription) {
        this.mediaTypeDescription = mediaTypeDescription;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public String getMembershipTypeCode() {
        return membershipTypeCode;
    }

    public void setMembershipTypeCode(String membershipTypeCode) {
        this.membershipTypeCode = membershipTypeCode;
    }

    public boolean isNonReturnable() {
        return nonReturnable;
    }

    public void setNonReturnable(boolean nonReturnable) {
        this.nonReturnable = nonReturnable;
    }

    public Date getOpenEndDateTime() {
        return openEndDateTime;
    }

    public void setOpenEndDateTime(Date openEndDateTime) {
        this.openEndDateTime = openEndDateTime;
    }

    public Date getOpenStartDateTime() {
        return openStartDateTime;
    }

    public void setOpenStartDateTime(Date openStartDateTime) {
        this.openStartDateTime = openStartDateTime;
    }

    public String getOpenValidityRuleId() {
        return openValidityRuleId;
    }

    public void setOpenValidityRuleId(String openValidityRuleId) {
        this.openValidityRuleId = openValidityRuleId;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public boolean isOwnAttraction() {
        return ownAttraction;
    }

    public void setOwnAttraction(boolean ownAttraction) {
        this.ownAttraction = ownAttraction;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public List<AxRetailProductExtPackageLine> getPackageLines() {
        return packageLines;
    }

    public void setPackageLines(List<AxRetailProductExtPackageLine> packageLines) {
        this.packageLines = packageLines;
    }

    public int getPrinterType() {
        return printerType;
    }

    public void setPrinterType(int printerType) {
        this.printerType = printerType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getUsageValidityRuleId() {
        return usageValidityRuleId;
    }

    public void setUsageValidityRuleId(String usageValidityRuleId) {
        this.usageValidityRuleId = usageValidityRuleId;
    }

    public boolean isTicketing() {
        return isTicketing;
    }

    public void setTicketing(boolean ticketing) {
        isTicketing = ticketing;
    }

    public int getPrinting() {
        return printing;
    }

    public void setPrinting(int printing) {
        this.printing = printing;
    }

    public int getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(int noOfPax) {
        this.noOfPax = noOfPax;
    }
}
