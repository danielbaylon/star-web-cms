
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cartId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerAccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiptEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="onlineSalesPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cartId",
    "customerAccountId",
    "receiptEmail",
    "onlineSalesPool"
})
@XmlRootElement(name = "SaveWOTOrder", namespace = "http://tempuri.org/")
public class SaveWOTOrder {

    @XmlElementRef(name = "cartId", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cartId;
    @XmlElementRef(name = "CustomerAccountId", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerAccountId;
    @XmlElementRef(name = "receiptEmail", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> receiptEmail;
    @XmlElementRef(name = "onlineSalesPool", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> onlineSalesPool;

    /**
     * Gets the value of the cartId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCartId() {
        return cartId;
    }

    /**
     * Sets the value of the cartId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCartId(JAXBElement<String> value) {
        this.cartId = value;
    }

    /**
     * Gets the value of the customerAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerAccountId() {
        return customerAccountId;
    }

    /**
     * Sets the value of the customerAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerAccountId(JAXBElement<String> value) {
        this.customerAccountId = value;
    }

    /**
     * Gets the value of the receiptEmail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReceiptEmail() {
        return receiptEmail;
    }

    /**
     * Sets the value of the receiptEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReceiptEmail(JAXBElement<String> value) {
        this.receiptEmail = value;
    }

    /**
     * Gets the value of the onlineSalesPool property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOnlineSalesPool() {
        return onlineSalesPool;
    }

    /**
     * Sets the value of the onlineSalesPool property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOnlineSalesPool(JAXBElement<String> value) {
        this.onlineSalesPool = value;
    }

}
