package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarInputUpdateCart {

    private String cartId;
    private String customerId;

    private List<AxStarInputUpdateCartItem> items;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<AxStarInputUpdateCartItem> getItems() {
        return items;
    }

    public void setItems(List<AxStarInputUpdateCartItem> items) {
        this.items = items;
    }
}
