package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransactionItem;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by jennylynsze on 8/12/16.
 */
public class PPMFLGInventoryTransactionItemSpecification extends BaseSpecification<PPMFLGInventoryTransactionItem> {
}
