package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMETicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 8/15/16.
 */
@Repository
public interface PPSLMETicketRepository  extends JpaRepository<PPSLMETicket, Integer> {

    List<PPSLMETicket> findByPackageId(Integer packageId);
}
