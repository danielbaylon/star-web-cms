package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by lavanya on 1/11/16.
 */
public class CountryMktShareRptFilterVM {
    private String startDateStr;
    private String endDateStr;
    private String orgName;
    private Date startDate;
    private Date toDate;
    private Integer[] paIds;

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Date getStartDate() {
        if (startDateStr != null&&!"".equals(startDateStr)) {
            try {
                startDate = NvxDateUtils.parseDate(startDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                return startDate;
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getToDate() {
        if (endDateStr != null&&!"".equals(endDateStr)) {
            try {
                toDate = NvxDateUtils.parseDate(endDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                return toDate;
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer[] getPaIds() {
        return paIds;
    }

    public void setPaIds(Integer[] paIds) {
        this.paIds = paIds;
    }

}
