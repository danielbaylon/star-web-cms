package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houtao on 17/8/16.
 */
@Entity(name = PPMFLGDepositTransaction.TABLE_NAME)
public class PPMFLGDepositTransaction {

    public static final String TABLE_NAME = "PPMFLGDepositTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "receiptNum", nullable = false)
    private String receiptNum;

    @Column(name = "transType")
    private String transType;

    @Column(name = "mainAccountId", nullable = false)
    private Integer mainAccountId;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "isSubAccountTrans", nullable = false)
    private Boolean subAccountTrans;

    @Column(name = "totalAmount", nullable = false)
    private BigDecimal totalAmount;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "tmMerchantId")
    private String tmMerchantId;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "tmStatus")
    private String tmStatus;

    @Column(name = "tmRedirectUrl")
    private String tmRedirectUrl;

    @Column(name = "tmApprovalCode")
    private String tmApprovalCode;

    @Column(name = "tmErrorCode")
    private String tmErrorCode;

    @Column(name = "tmErrorMessage")
    private String tmErrorMessage;

    @Column(name = "paymentType")
    private String paymentType;

    @Column(name = "tenderTypeId")
    private String tenderTypeId;

    @Column(name = "ip")
    private String ip;

    @Column(name = "maskedCc")
    private String maskedCc;

    @Column(name = "gstRate")
    private BigDecimal gstRate;

    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "sessionId")
    private String sessionId;

    @Column(name = "axRequestText")
    private String axRequestText;

    @Column(name = "axResponseText")
    private String axResponseText;

    @Column(name = "tmRedirectDate")
    private Date tmRedirectDate;

    @Column(name = "tmResponseDate")
    private Date tmResponseDate;

    @Column(name = "tmVoidDate")
    private Date tmVoidDate;

    @Column(name = "tmVoidStatus")
    private String tmVoidStatus;

    @Column(name = "tmVoidResponse")
    private String tmVoidResponse;

    @Column(name = "tmQuerySentDate")
    private Date tmQuerySentDate;

    @Column(name = "tmQuerySentStatus")
    private String tmQuerySentStatus;

    @Column(name = "tmQuerySentResponse")
    private String tmQuerySentResponse;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public String getTmErrorCode() {
        return tmErrorCode;
    }

    public void setTmErrorCode(String tmErrorCode) {
        this.tmErrorCode = tmErrorCode;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMaskedCc() {
        return maskedCc;
    }

    public void setMaskedCc(String maskedCc) {
        this.maskedCc = maskedCc;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(String tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAxRequestText() {
        return axRequestText;
    }

    public void setAxRequestText(String axRequestText) {
        this.axRequestText = axRequestText;
    }

    public String getAxResponseText() {
        return axResponseText;
    }

    public void setAxResponseText(String axResponseText) {
        this.axResponseText = axResponseText;
    }

    public String getTmRedirectUrl() {
        return tmRedirectUrl;
    }

    public void setTmRedirectUrl(String tmRedirectUrl) {
        this.tmRedirectUrl = tmRedirectUrl;
    }

    public Date getTmRedirectDate() {
        return tmRedirectDate;
    }

    public void setTmRedirectDate(Date tmRedirectDate) {
        this.tmRedirectDate = tmRedirectDate;
    }

    public Date getTmResponseDate() {
        return tmResponseDate;
    }

    public void setTmResponseDate(Date tmResponseDate) {
        this.tmResponseDate = tmResponseDate;
    }

    public Date getTmVoidDate() {
        return tmVoidDate;
    }

    public void setTmVoidDate(Date tmVoidDate) {
        this.tmVoidDate = tmVoidDate;
    }

    public String getTmVoidStatus() {
        return tmVoidStatus;
    }

    public void setTmVoidStatus(String tmVoidStatus) {
        this.tmVoidStatus = tmVoidStatus;
    }

    public String getTmVoidResponse() {
        return tmVoidResponse;
    }

    public void setTmVoidResponse(String tmVoidResponse) {
        this.tmVoidResponse = tmVoidResponse;
    }

    public Date getTmQuerySentDate() {
        return tmQuerySentDate;
    }

    public void setTmQuerySentDate(Date tmQuerySentDate) {
        this.tmQuerySentDate = tmQuerySentDate;
    }

    public String getTmQuerySentStatus() {
        return tmQuerySentStatus;
    }

    public void setTmQuerySentStatus(String tmQuerySentStatus) {
        this.tmQuerySentStatus = tmQuerySentStatus;
    }

    public String getTmQuerySentResponse() {
        return tmQuerySentResponse;
    }

    public void setTmQuerySentResponse(String tmQuerySentResponse) {
        this.tmQuerySentResponse = tmQuerySentResponse;
    }
}
